/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.mac;

import java.awt.AWTEvent;
import java.awt.Image;
import java.awt.PopupMenu;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.KeyEvent;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import eu.omp.irap.mac.handler.AboutHandler;
import eu.omp.irap.mac.handler.PreferencesHandler;
import eu.omp.irap.mac.handler.QuitHandler;


/**
 * Class to easily register specifics actions to Mac OS X.
 *
 * @author M. Boiziot
 */
public class MacHandling {

	private static MacHandling instance;
	private Class<?> appClass;
	private Object app;

	private AboutHandler aboutHandler;
	private PreferencesHandler preferencesHandler;
	private QuitHandler quitHandler;

	private boolean aboutProxyfied;
	private boolean preferenceProxyfied;
	private boolean quitProxyfied;


	/**
	 * Constructor.
	 */
	private MacHandling() {
		try {
			appClass = Class.forName("com.apple.eawt.Application");
			Method m = appClass.getMethod("getApplication", new Class<?>[0]);
			app = m.invoke(null, (Object[])null);
		} catch (ClassNotFoundException | IllegalAccessException |
				IllegalArgumentException | InvocationTargetException |
				NoSuchMethodException | SecurityException e) {
		}
	}

	/**
	 * Create if needed an instance of {@link MacHandling} then return it.
	 *
	 * @return the {@link MacHandling} instance.
	 */
	public static MacHandling getInstance() {
		if (instance == null) {
			instance = new MacHandling();
		}
		return instance;
	}

	/**
	 * Return if the Mac OS X specific functions are supported here.
	 *
	 * @return true if the MAC OS X specific functions are supported here, false
	 *  otherwise.
	 */
	public boolean isSupported() {
		return app != null;
	}

	/**
	 * Register an action to do on "Quit" click from the Mac OS X menu.
	 *
	 * @param quitAction The action to do.
	 * @return true if the action was registered, false otherwise.
	 */
	public boolean setQuitAction(final Runnable quitAction) {
		if (!isSupported()) {
			return false;
		}

		getQuitHandler().setRunnable(quitAction);

		if (!quitProxyfied) {
			try {
				Class<?> quitClass = Class.forName("com.apple.eawt.QuitHandler");
				Object ob = Proxy.newProxyInstance(quitClass.getClassLoader(),
						new Class[] { quitClass }, getQuitHandler());
				Method methodSetQuitHandler = appClass.getMethod(
						"setQuitHandler", new Class[] { quitClass });
				quitClass.cast(ob);
				methodSetQuitHandler.invoke(app, ob);
				quitProxyfied = true;
			} catch (ClassNotFoundException | NoSuchMethodException |
					SecurityException | IllegalAccessException |
					IllegalArgumentException | InvocationTargetException e) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Register an action to do on "Preferences" click from the Mac OS X menu.
	 *
	 * @param prefAction The action to do.
	 * @return true if the action was registered, false otherwise.
	 */
	public boolean setPreferencesAction(final Runnable prefAction) {
		if (!isSupported()) {
			return false;
		}

		getPreferencesHandler().setRunnable(prefAction);
		if (!preferenceProxyfied) {
			try {
				Class<?> preferencesClass = Class.forName(
						"com.apple.eawt.PreferencesHandler");
				Object ob = Proxy.newProxyInstance(
						preferencesClass.getClassLoader(),
						new Class[] { preferencesClass },
						getPreferencesHandler());
				Method methodSetPreferencesHandler = appClass.getMethod(
						"setPreferencesHandler", new Class[] { preferencesClass });
				preferencesClass.cast(ob);
				methodSetPreferencesHandler.invoke(app, ob);
				preferenceProxyfied = true;
			} catch (ClassNotFoundException | NoSuchMethodException |
					SecurityException | IllegalAccessException |
					IllegalArgumentException | InvocationTargetException e) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Register an action to do on "About" click from the Mac OS X menu.
	 *
	 * @param aboutAction The action to do.
	 * @return true if the action was registered, false otherwise.
	 */
	public boolean setAboutAction(final Runnable aboutAction) {
		if (!isSupported()) {
			return false;
		}

		getAboutHandler().setRunnable(aboutAction);
		if (!aboutProxyfied) {
			try {
				Class<?> aboutClass = Class.forName("com.apple.eawt.AboutHandler");
				Object ob = Proxy.newProxyInstance(aboutClass.getClassLoader(),
						new Class[] { aboutClass }, getAboutHandler());
				Method methodSetAboutHandler = appClass.getMethod(
						"setAboutHandler", new Class[] { aboutClass });
				aboutClass.cast(ob);
				methodSetAboutHandler.invoke(app, ob);
				aboutProxyfied = true;
			} catch (ClassNotFoundException | NoSuchMethodException |
					SecurityException | IllegalAccessException |
					IllegalArgumentException | InvocationTargetException e) {
				return false;
			}
		}
		return true;
	}


	/**
	 * Create if needed then return the {@link AboutHandler}.
	 *
	 * @return the {@link AboutHandler}.
	 */
	private AboutHandler getAboutHandler() {
		if (aboutHandler == null) {
			aboutHandler = new AboutHandler();
		}
		return aboutHandler;
	}

	/**
	 * Create if needed then return the {@link PreferencesHandler}.
	 *
	 * @return the {@link PreferencesHandler}.
	 */
	private PreferencesHandler getPreferencesHandler() {
		if (preferencesHandler == null) {
			preferencesHandler = new PreferencesHandler();
		}
		return preferencesHandler;
	}

	/**
	 * Create if needed then return the {@link QuitHandler}.
	 *
	 * @return the {@link QuitHandler}.
	 */
	private QuitHandler getQuitHandler() {
		if (quitHandler == null) {
			quitHandler = new QuitHandler();
		}
		return quitHandler;
	}

	/**
	 * Set an image for the dock icon.
	 *
	 * @param img The image to set.
	 * @return true if image was set, false otherwise.
	 */
	public boolean setDockIconImage(Image img) {
		if (!isSupported() || img == null) {
			return false;
		}
		try {
			Method methodSetDockIconImage = appClass.getMethod(
					"setDockIconImage", new Class[] { Image.class });
			methodSetDockIconImage.invoke(app, img);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException e) {
			return false;
		}
		return true;
	}

	/**
	 * Add a menu on the icon of the dock.
	 *
	 * @param menu The menu to add.
	 * @return true if the menu was set, false otherwise.
	 */
	public boolean setDockMenu(PopupMenu menu) {
		if (!isSupported() || menu == null) {
			return false;
		}
		try {
			Method methodSetDockMenu = appClass.getMethod("setDockMenu",
					new Class[] { PopupMenu.class });
			methodSetDockMenu.invoke(app, menu);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException e) {
			return false;
		}
		return true;

	}

	/**
	 * Add a badge on the icon of the dock.
	 *
	 * @param badge The text of the badge to add.
	 * @return true if the badge was set, false otherwise.
	 */
	public boolean setDockIconBadge(String badge) {
		if (!isSupported()) {
			return false;
		}
		try {
			Method methodSetDockIconBadge = appClass.getMethod(
					"setDockIconBadge", new Class[] { String.class });
			methodSetDockIconBadge.invoke(app, badge);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException e) {
			return false;
		}
		return true;
	}

	/**
	 * Replace the Apple/Command input event by control.
	 */
	public static void replaceCommandByControl() {
		Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {
			@Override
			public void eventDispatched(AWTEvent event) {
				KeyEvent kev = (KeyEvent) event;
				if (kev.getID() == KeyEvent.KEY_PRESSED
						|| kev.getID() == KeyEvent.KEY_RELEASED
						|| kev.getID() == KeyEvent.KEY_PRESSED) {
					if ((kev.getModifiersEx() & KeyEvent.META_DOWN_MASK) != 0
							&& !((kev.getModifiersEx() & KeyEvent.CTRL_DOWN_MASK) != 0)) {
						kev.consume();
						KeyEvent fake = new KeyEvent(kev.getComponent(), kev.getID(),
								kev.getWhen(),
								(kev.getModifiersEx() & ~KeyEvent.META_DOWN_MASK)
										| KeyEvent.CTRL_DOWN_MASK,
								kev.getKeyCode(), kev.getKeyChar());
						Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(fake);
					}
				}
			}
		}, KeyEvent.KEY_EVENT_MASK);
	}

}
