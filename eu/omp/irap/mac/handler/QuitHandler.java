/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.mac.handler;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * Basic class to handle Quit action from Mac OS X.
 *
 * @author M. Boiziot
 */
public class QuitHandler extends RunnableContener implements InvocationHandler {


	/**
	 * Creates a {@link QuitHandler} to handle Quit action from Mac OS X.
	 */
	public QuitHandler() {
		super();
	}

	/**
	 * Handle call to
	 * "public void handleQuitRequestWith(AppEvent.QuitEvent e, QuitResponse response)"
	 *  and run the #{@link QuitHandler#getRunnable()} action if there is
	 *  one defined.
	 *
	 * @param proxy the proxy instance that the method was invoked on.
	 * @param method The method invoked.
	 * @param args The arguments.
	 * @return null.
	 * @throws Throwable Should not be used here.
	 * @see java.lang.reflect.InvocationHandler#invoke(java.lang.Object, java.lang.reflect.Method, java.lang.Object[])
	 */
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		if (method.getName().equals("handleQuitRequestWith") && args != null && args.length == 2) {
			if (haveRunnable()) {
				getRunnable().run();
			}

			// If we are still here, the user did not close the application, cancel it then.
			Object qr = args[1];
			Class<?> qrClass = qr.getClass();
			Method methodCancelQuit = qrClass.getMethod("cancelQuit", new Class[] {});
			methodCancelQuit.invoke(qr, new Object[0]);
		}
		return null;
	}

}
