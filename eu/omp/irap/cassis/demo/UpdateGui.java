/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.demo;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

/**
 * Basic GUI class for the downloader.
 *
 * @author M. Boiziot
 */
public class UpdateGui extends JDialog implements ThreadCompleteListener {

	private static final long serialVersionUID = 1L;
	private JLabel label;
	private JPanel southPanel;
	private UpdateThread thread;
	private String xmlUrl;


	/**
	 * The constructor.
	 *
	 * @param parentComponent The parent component of this gui.
	 * @param xmlUrl The URL to the file to parse to check the files to update.
	 */
	public UpdateGui(Frame parentComponent, String xmlUrl) {
		super(parentComponent, "Cassis demo files downloader", true);
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.setSize(new Dimension(460, 220));
		this.xmlUrl = xmlUrl;
		getContentPane().setLayout(new BorderLayout());
		label = new JLabel("<html>CASSIS was launched in online mode who require some additional files. " +
				"They will be downloaded automatically.<br><br>" +
				"Some features are only available in the full version of CASSIS. " +
				"You can download it from http://cassis.irap.omp.eu.</html>");

		label.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		getContentPane().add(label, BorderLayout.CENTER);

		southPanel = new JPanel();
		southPanel.setBorder(BorderFactory.createEmptyBorder(0, 20, 20, 20));
		getContentPane().add(southPanel, BorderLayout.SOUTH);
		JProgressBar progressBar = new JProgressBar();
		progressBar.setIndeterminate(true);

		southPanel.add(progressBar);
	}

	/**
	 * Start the update thread.
	 */
	private void startThread() {
		thread = new UpdateThread(xmlUrl);
		thread.addListener(this);
		thread.start();
	}

	/**
	 * @see eu.omp.irap.cassis.demo.ThreadCompleteListener#notifyEndOfWork(java.lang.Thread)
	 */
	@Override
	public void notifyEndOfWork(Thread thread) {
		if (thread instanceof UpdateThread) {
			UpdateThread updateThread = (UpdateThread) thread;

			// Get the statut of the operation...
			int statut = updateThread.getStatut();

			if (statut == UpdateThread.NO_ERROR) {
				stopThread();
				this.dispose();
			} else {
				String errorText;

				if (statut == UpdateThread.XML_DOWNLOAD_ERROR) {
					errorText = "<html>There was an error while downloading files. " +
							"Please check your internet connection then restart CASSIS.";
				} else if (statut == UpdateThread.XML_PARSING_ERROR) {
					errorText = "<html>There was an error while parsing the xml file. " +
							"Please restart CASSIS. If the problem occur again, please inform the CASSIS team.";
				} else if (statut == UpdateThread.PROCESSING_ERROR) {
					errorText = "<html>There was an error while creating the files. " +
							"Please check your filesystem (space, right to write on it...) then restart CASSIS.";
				} else {
					errorText = "There was an unknown error...";
				}

				errorText += "<br><br>You can try to start CASSIS anyway but we can not ensure it will work properly...</html>";
				label.setText(errorText);

				southPanel.removeAll();

				JButton exitButton = new JButton("Exit");
				exitButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						System.exit(0);
					}
				});

				JButton startButton = new JButton("Start");
				startButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						((JDialog)((JButton)e.getSource()).getParent()).dispose();
					}
				});

				JPanel tmpPanel = new JPanel(new BorderLayout());
				JPanel tmpPanel2 = new JPanel();
				tmpPanel2.add(exitButton);
				tmpPanel2.add(startButton);
				tmpPanel.add(tmpPanel2, BorderLayout.CENTER);
				southPanel.add(tmpPanel);
				repaint();
				stopThread();
			}
		}
	}

	/**
	 * Stop/Remove the thread.
	 */
	private void stopThread() {
		thread = null;
	}

	/**
	 * Start the Thread and set the JFrame visible.
	 */
	public void start() {
		startThread();
		this.setVisible(true);
	}
}
