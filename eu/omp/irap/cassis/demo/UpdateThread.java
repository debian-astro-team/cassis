/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.demo;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import eu.omp.irap.cassis.gui.util.FileUtils;

/**
 * Basic thread for the update/download of the necessary files for the demo.
 *
 * @author M. Boiziot
 */
public class UpdateThread extends Thread {

	public static final int NO_ERROR = 0;
	public static final int XML_DOWNLOAD_ERROR = 1;
	public static final int XML_PARSING_ERROR = 2;
	public static final int PROCESSING_ERROR = 3;
	private String xmlUrl;
	private int statut = NO_ERROR;
	private List<ThreadCompleteListener> listeners;


	/**
	 * Constructor.
	 *
	 * @param xmlUrl The url of the xml file.
	 */
	public UpdateThread(String xmlUrl) {
		this.xmlUrl = xmlUrl;
		this.listeners = new ArrayList<>();
	}

	/**
	 * Add a ThreadCompleteListener.
	 *
	 * @param listener The listener to add.
	 */
	public final void addListener(final ThreadCompleteListener listener) {
		listeners.add(listener);
	}

	/**
	 * Remove a listener.
	 *
	 * @param listener The listener to remove.
	 */
	public final void removeListener(final ThreadCompleteListener listener) {
		listeners.remove(listener);
	}

	/**
	 * Do the work : download the xml file, parse it and download/update the necessary files.
	 */
	private void doRun() {
		File xmlFile = FileUtils.download(xmlUrl, true);
		if (xmlFile == null) {
			statut = XML_DOWNLOAD_ERROR;
			return;
		}

		CassisDemoFileParser parser = new CassisDemoFileParser(xmlFile);
		List<CassisDemoFile> listCassisDemoFile = parser.parseFile();
		if (listCassisDemoFile == null) {
			statut = XML_PARSING_ERROR;
			return;
		}

		for (CassisDemoFile cdf : listCassisDemoFile) {
			downloadCdf(cdf);
		}
	}

	/**
	 * Download a {@link CassisDemoFile} if necessary.
	 *
	 * @param cdf The {@link CassisDemoFile}.
	 */
	private void downloadCdf(CassisDemoFile cdf) {
		if (cdf.existLocaly()) {
			if (!cdf.isTheSameChecksum() && !cdf.isPropertiesFile()) {
				cdf.delete();
				if (!cdf.download()) {
					statut = PROCESSING_ERROR;
				}
			}
		} else {
			if (!cdf.download()) {
				statut = PROCESSING_ERROR;
			}
		}
	}

	/**
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {
		try {
			doRun();
		} finally {
			notifyListeners();
		}
	}

	/**
	 * Return the current statut of the thread.
	 *
	 * @return the current statut of the thread.
	 */
	public int getStatut() {
		return statut;
	}

	/**
	 * Notify the listeners of the end of the work.
	 */
	private final void notifyListeners() {
		for (ThreadCompleteListener listener : listeners) {
			listener.notifyEndOfWork(this);
		}
	}
}
