/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.demo;

import java.io.File;

import eu.omp.irap.cassis.gui.util.ChecksumUtil;
import eu.omp.irap.cassis.gui.util.FileUtils;
import eu.omp.irap.cassis.properties.Software;

/**
 * Class who represent a file for the online version with its parameters.
 *
 * @author M. Boiziot
 */
public class CassisDemoFile {

	private static final String LOCAL_CASSIS_DIRECTORY = Software.getOnlineModePath();

	private String localPartialPath;
	private String url;
	private String checksum;
	private boolean propertiesFile;


	/**
	 * Constructor.
	 *
	 * @param localPartialPath partial path of the file (beside the cassis path)
	 * @param url The URL of the file on Cassis webserver
	 * @param checksum The checksum of the file (md5).
	 * @param propertiesFile If the file is a properties file.
	 */
	public CassisDemoFile(String localPartialPath, String url, String checksum, boolean propertiesFile) {
		this.localPartialPath = localPartialPath;
		this.url = url;
		this.checksum = checksum;
		this.propertiesFile = propertiesFile;
	}

	/**
	 * @return The complete local path of the file.
	 */
	public String getLocalPath() {
		return LOCAL_CASSIS_DIRECTORY + File.separator + localPartialPath;
	}

	/**
	 * @return if the file exist on the local filesystem.
	 */
	public boolean existLocaly() {
		return new File(getLocalPath()).exists();
	}

	/**
	 * Check if the checksum of the local file is the same that the one referenced.
	 *
	 * @return if the checksum (md5) is the same.
	 */
	public boolean isTheSameChecksum() {
		String currentMd5 = ChecksumUtil.getMd5(getLocalPath());
		return checksum.equals(currentMd5);
	}

	/**
	 * Delete the local file.
	 *
	 * @return if the delete was successfull.
	 */
	public boolean delete() {
		if (!existLocaly()) {
			return false;
		}
		File file = new File(getLocalPath());
		return file.delete();
	}

	/**
	 * Download the file.
	 *
	 * @return If the download was successfull.
	 */
	public boolean download() {
		File theFile = FileUtils.download(url, false);
		if (theFile == null) {
			return false;
		}
		return FileUtils.move(theFile, new File(getLocalPath()));
	}

	/**
	 * Return if the file is a properties file.
	 *
	 * @return true if the file is a properties file, false otherwise.
	 */
	public boolean isPropertiesFile() {
		return propertiesFile;
	}

}
