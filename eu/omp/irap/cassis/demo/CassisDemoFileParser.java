/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.demo;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Parser for the file filesList.xml for the online version.
 *
 * @author M. Boiziot
 */
public class CassisDemoFileParser {

	private static final Logger LOGGER = LoggerFactory.getLogger(CassisDemoFileParser.class);

	private static final String LOCAL_PATH = "localPath";
	private static final String URL = "url";
	private static final String MD5 = "md5";
	private static final String PROPERTIES_FILE = "propertiesFile";
	private File fileToParse;


	/**
	 * Constructor.
	 *
	 * @param file The File to parse.
	 */
	public CassisDemoFileParser(File file) {
		fileToParse = file;
	}

	/**
	 * Parse the file.
	 *
	 * @return The list of CassisDemoFile referenced on the parsed file.
	 */
	public List<CassisDemoFile> parseFile() {
		try (FileInputStream fileInputStream = new FileInputStream(fileToParse)) {
			List<CassisDemoFile> listCassisDemoFile = new ArrayList<>();
			DocumentBuilderFactory docBuilderFact = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFact.newDocumentBuilder();
			Document doc = docBuilder.parse(fileInputStream);
			doc.getDocumentElement().normalize();
			NodeList nodeList = doc.getElementsByTagName("file");
			int nbFiles = nodeList.getLength();

			String localPath;
			String url;
			String md5;
			boolean propertiesFile;
			for (int cpt = 0; cpt < nbFiles; cpt++) {
				Element element = (Element) nodeList.item(cpt);
				localPath = element.getAttribute(LOCAL_PATH).trim();
				url = element.getAttribute(URL).trim();
				md5 = element.getAttribute(MD5).trim();
				propertiesFile = Boolean.valueOf(element.getAttribute(PROPERTIES_FILE).trim());
				listCassisDemoFile.add(new CassisDemoFile(localPath, url, md5, propertiesFile));
			}
			return listCassisDemoFile;
		} catch (Exception e) {
			LOGGER.error("An exception occured while parsing the file", e);
			return null;
		}
	}

}
