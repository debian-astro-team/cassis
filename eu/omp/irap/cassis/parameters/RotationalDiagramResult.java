/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.parameters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import eu.omp.irap.cassis.gui.model.parameter.rotationaltuning.TYPE_TEMPERATURE;

/**
 * Class decribing the result of a RotaionalDiagram computing.
 *
 * @author M. Boiziot
 */
public class RotationalDiagramResult {

	private List<RotationalDiagramMoleculeResult> listMolResult;
	private String pathSourceFile;
	private Map<Integer, Set<Double>> freqNotFound;
	private TYPE_TEMPERATURE typeTemperature;
	private String multipletMessage;
	private String blendedMessage;


	/**
	 * Constructor.
	 *
	 * @param pathFileSource Path of the file used as source.
	 */
	public RotationalDiagramResult(String pathFileSource) {
		this.pathSourceFile = pathFileSource;
		this.listMolResult = new ArrayList<>();
		this.freqNotFound = new HashMap<>(0);
	}

	/**
	 * Add a {@link RotationalDiagramMoleculeResult}.
	 *
	 * @param molResult The {@link RotationalDiagramMoleculeResult} to add to the result.
	 */
	public void addMoleculeResult(RotationalDiagramMoleculeResult molResult) {
		listMolResult.add(molResult);
	}

	/**
	 * Return the result.
	 *
	 * @return  the result.
	 */
	public List<RotationalDiagramMoleculeResult> getResult() {
		return listMolResult;
	}

	/**
	 * Return the path of the data file used as source.
	 *
	 * @return The path of the data file used as source.
	 */
	public String getPathSourceFile() {
		return pathSourceFile;
	}

	/**
	 * Add a frequencies not found for a tag.
	 *
	 * @param tag The tag of the frequency not found.
	 * @param freq The frequency not found.
	 */
	public void addFrequencyNotFound(int tag, double freq) {
		if (freqNotFound.containsKey(tag)) {
			freqNotFound.get(tag).add(freq);
		} else {
			Set<Double> set = new HashSet<>(1);
			set.add(freq);
			freqNotFound.put(tag, set);
		}
	}

	/**
	 * Return the frequencies not found in the database inside a Set (frequencies), inside a Map (tag).
	 *
	 * @return The frequencies not found in the database.
	 */
	public Map<Integer, Set<Double>> getFrequenciesNotFound() {
		return freqNotFound;
	}

	/**
	 * Return the temperature type.
	 *
	 * @return the temperature type.
	 */
	public TYPE_TEMPERATURE getTypeTemperature() {
		return typeTemperature;
	}

	/**
	 * Set the temperature type.
	 *
	 * @param newTypeTemperature the temperature type to set.
	 */
	public void setTypeTemperature(TYPE_TEMPERATURE newTypeTemperature) {
		this.typeTemperature = newTypeTemperature;
	}

	/**
	 * Return the multiplet message.
	 *
	 * @return the multiplet message or <code>null</code> if there is not multiplet.
	 */
	public String getMultipletMessage() {
		return multipletMessage;
	}

	/**
	 * Set the multiplet message.
	 *
	 * @param msg The multiplet message.
	 */
	public void setMultipletMessage(String msg) {
		this.multipletMessage = msg;
	}

	@Override
	public String toString() {
		return "RotationalDiagramResult [listMolResult=" + listMolResult
				+ ", pathSourceFile=" + pathSourceFile + ", freqNotFound=" + freqNotFound
				+ ", typeTemperature=" + typeTemperature + ", multipletMessage="
				+ multipletMessage + "]";
	}

	public void setBlendedMessage(String blendedMessage) {
		this.blendedMessage = blendedMessage;
	}

	public String getBlendedMessage() {
		return blendedMessage;
	}

}
