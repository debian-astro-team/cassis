/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.parameters;

import java.text.DecimalFormat;

import eu.omp.irap.cassis.common.CassisDecimalFormat;
import eu.omp.irap.cassis.database.access.MoleculeDescriptionDB;

/**
 * Utility class to regroup all functions creating a line identification message.
 *
 * @author M. Boiziot
 */
public class LineIdentificationUtils {


	/**
	 * Create the identification for a line in Comet or LabAbsorption.
	 *
	 * @param molName The name.
	 * @param qn The quantum numbers.
	 * @param molTag The tag.
	 * @param freq The frequency (in MHz).
	 * @param eupk The eup.
	 * @param aij The aij.
	 * @param tau The tau.
	 * @param gup The gup.
	 * @return The identification message for the line with the given parameters.
	 */
	private static String createIdentification(String molName, String qn,
			int molTag, double freq, double eupk, double aij, double tau, int gup) {
		return molName + " " + MoleculeDescriptionDB.constitueLine(qn) + " Tag = "
				+ molTag + "\n\u03BD = " + CassisDecimalFormat.FREQUENCY_FORMAT.format(freq)
				+ " MHz Eup = " + CassisDecimalFormat.EUP_FORMAT.format(eupk) + " K Aij = "
				+ CassisDecimalFormat.AIJ_FORMAT.format(aij) + "\nGup = " + gup
				+ " Tau = " + CassisDecimalFormat.TAU_FORMAT.format(tau);
	}

	/**
	 * Create the identification for a line in LabAbsorptionAction.
	 *
	 * @param molName The name.
	 * @param qn The quantum numbers.
	 * @param molTag The tag.
	 * @param freq The frequency (in MHz).
	 * @param eupk The eup.
	 * @param aij The aij.
	 * @param tau The tau.
	 * @param gup The gup.
	 * @return The identification message for the line with the given parameters.
	 */
	public static String createLabAbsoptionIdentification(String molName, String qn,
			int molTag, double freq, double eupk, double aij, double tau, int gup) {
		return createIdentification(molName, qn, molTag, freq, eupk, aij, tau, gup);
	}

	/**
	 * Create the identification for a molecule in Comet.
	 *
	 * @param molName The name.
	 * @param qn The quantum numbers.
	 * @param molTag The tag.
	 * @param freq The frequency (in MHz).
	 * @param eupk The eup.
	 * @param aij The aij.
	 * @param tau The tau.
	 * @param gup The gup.
	 * @return The identification message for the line with the given parameters.
	 */
	public static String createCometIdentification(String molName, String qn,
			int molTag, double freq, double eupk, double aij, double tau, int gup) {
		return createIdentification(molName, qn, molTag, freq, eupk, aij, tau, gup);
	}

	/**
	 * Create the identification for a line in AstroModel.
	 *
	 * @param molName The name.
	 * @param qn The quantum numbers.
	 * @param molTag The tag.
	 * @param freq The frequency (in MHz).
	 * @param error The error frequency (in MHz).
	 * @param eupk The eup.
	 * @param aij The aij.
	 * @param componentName The name of the component.
	 * @param vlsr The VLSR.
	 * @param tau The tau.
	 * @param radex true if RADEX, false otherwise.
	 * @param tex The tex.
	 * @param dsbMode true if dsbMode, false otherwise.
	 * @param secondeBand true if second band, false otherwise.
	 * @param dsbValue The dsb.
	 * @param gup The gup.
	 * @return The identification message for the line with the given parameters.
	 */
	public static String createAstroModelIdentification(String molName, String qn,
			int molTag, double freq, double error, double eupk, double aij,
			String componentName, double vlsr, double tau, boolean radex,
			double tex, boolean dsbMode, boolean secondeBand, double dsbValue,
			int gup) {
		String fromDsb = "";
		double lsbUsbDisplay = dsbValue;
		if (dsbMode || secondeBand) {
			if (secondeBand)
				lsbUsbDisplay = -1 * dsbValue;
			if (lsbUsbDisplay == 1)
				fromDsb = " (from LSB)";
			else
				fromDsb = " (from USB)";
		}
		String id = molName + " " + MoleculeDescriptionDB.constitueLine(qn) + " Tag = "
				+ molTag + "\n\u03BD = " + CassisDecimalFormat.FREQUENCY_FORMAT.format(freq)
				+ "(±" + CassisDecimalFormat.FREQUENCY_FORMAT.format(error) + ") MHz "
				+ "Eup = " + CassisDecimalFormat.EUP_FORMAT.format(eupk) + " K Aij = "
				+ CassisDecimalFormat.AIJ_FORMAT.format(aij) + " Gup = " + gup + fromDsb + "\n"
				+ componentName + " (Vlsr = " + CassisDecimalFormat.TWO_DEC_FORMAT.format(vlsr) + ')'
				+ " Tau = " + CassisDecimalFormat.TAU_FORMAT.format(tau) ;

		if (radex)
			id += " Tex = " + CassisDecimalFormat.TWO_POS_DEC_FORMAT.format(tex) + " (RADEX)";
		else
			id += " Tex = " + (double) Math.round(tex * 1000) / 1000 + " (LTE)";
		return id;
	}

	/**
	 * Create the identification for a line in LineModel.
	 *
	 * @param molName The name.
	 * @param qn The quantum numbers.
	 * @param molTag The tag.
	 * @param freq The frequency (in MHz).
	 * @param error The error frequency (in MHz).
	 * @param eupk The eup.
	 * @param aij The aij.
	 * @param gup The gup.
	 * @return The identification message for the line with the given parameters.
	 */
	public static String createLineModelIdentification(String molName, String qn,
			int molTag, double freq, double error, double eupk, double aij, int gup) {
		return molName + MoleculeDescriptionDB.constitueLine(qn) + " Tag = " +
				molTag + "\n\u03BD = " + CassisDecimalFormat.FREQUENCY_FORMAT.format(freq)
				+ "(±" + CassisDecimalFormat.FREQUENCY_FORMAT.format(error) + ") MHz\n"
				+ "Eup = " + CassisDecimalFormat.EUP_FORMAT.format(eupk) + " K "
				+ "Aij = " + CassisDecimalFormat.AIJ_FORMAT.format(aij)
				+ " Gup = " + gup;
	}

	/**
	 * Create the identification for a line in LoomisModel.
	 *
	 * @param molName The name.
	 * @param qn The quantum numbers.
	 * @param molTag The tag.
	 * @param freq The frequency (in MHz).
	 * @param eupk The eup.
	 * @param aij The aij.
	 * @param tex The tex.
	 * @param gup The gup.
	 * @param density The density.
	 * @return The identification message for the line with the given parameters.
	 */
	public static String createLoomisIdentification(String molName, String qn, int molTag,
			double freq, double eupk, double aij, double tex, double density, int gup) {
		return molName + MoleculeDescriptionDB.constitueLine(qn) + " Tag = "
				+ molTag + "\n\u03BD = "
				+ CassisDecimalFormat.FREQUENCY_FORMAT.format(freq) + " MHz Eup = "
				+ CassisDecimalFormat.EUP_FORMAT.format(eupk) + " K Aij = "
				+ CassisDecimalFormat.AIJ_FORMAT.format(aij) + " Gup = " + gup
				+ "\nTex = " + tex + " K  N(Sp) = " + density + " cm\u207B\u00B2";
	}

	/**
	 * Create the Line identification message for the given parameters.
	 *
	 * @param species The species name.
	 * @param transition The transition (quantum number).
	 * @param restFrequency The rest frequency.
	 * @param eup The eup.
	 * @param aij The aij.
	 * @param vlsr The VLSR.
	 * @param gup The gup.
	 * @return The identification message for the line with the given parameters.
	 */
	public static String createRotationalLineIdentification(String species, String transition,
			double restFrequency, double eup, double aij, double vlsr, int gup) {
		StringBuilder sb = new StringBuilder();
		sb.append(species).append(" ").append(transition).append('\n');
		sb.append("v = ").append(CassisDecimalFormat.FREQUENCY_FORMAT.format(restFrequency));
		sb.append(" MHz  Eup = ").append(CassisDecimalFormat.EUP_FORMAT.format(eup));
		sb.append(" K\nAij = ").append(CassisDecimalFormat.AIJ_FORMAT.format(aij));
		sb.append(" Gup = ").append(gup).append(" Vlsr = ");
		sb.append(CassisDecimalFormat.VLSR_FORMAT.format(vlsr)).append(" km/s");

		return sb.toString();
	}

	/**
	 * Create the Line identification message for the given parameters.
	 *
	 * @param species The species name.
	 * @param transition The transition (quantum number).
	 * @param restFrequency The rest frequency.
	 * @param eup The eup.
	 * @param aij The aij.
	 * @param deltaFreq The delta frequency.
	 * @param vlsr The VLSR.
	 * @param snr The signal on noise ratio.
	 * @return The identification message for the line with the given parameters.
	 */
	public static String createSpectralLineListIdentification(String species, String transition,
			double restFrequency, double eup, double aij, double deltaFreq,
			double vlsr, double snr) {
		StringBuilder sb = new StringBuilder();
		sb.append(species).append(" ").append(transition).append('\n');
		sb.append("v = ").append(CassisDecimalFormat.FREQUENCY_FORMAT.format(restFrequency));
		sb.append(" MHz  Eup = ").append(CassisDecimalFormat.EUP_FORMAT.format(eup));
		sb.append(" K  Aij = ").append(CassisDecimalFormat.AIJ_FORMAT.format(aij)).append('\n');
		sb.append("deltaFreq = ").append(CassisDecimalFormat.TWO_DEC_EXP_FORMAT.format(deltaFreq));
		sb.append(" MHz  Vlsr = ").append(CassisDecimalFormat.VLSR_FORMAT.format(vlsr)).append(" km/s");
		sb.append("\nsnr = ").append(CassisDecimalFormat.TWO_DEC_FORMAT.format(snr));

		return sb.toString();
	}

	/**
	 * Create the identification message for an unknown line.
	 *
	 * @param frequency The frequency (MHz).
	 * @param deltaFreq The delta frequency (MHz).
	 * @param vlsr The VLSR.
	 * @param snr The signal on noise ratio.
	 * @return The identification message for the line with the given parameters.
	 */
	public static String createSpectralLineListUnknownIdentification(double frequency,
			double deltaFreq, double vlsr, double snr) {
		StringBuilder sb = new StringBuilder();
		sb.append("Unknown\n");
		sb.append("v = ").append(CassisDecimalFormat.FREQUENCY_FORMAT.format(frequency));
		sb.append(" MHz\ndeltaFreq = ").append(CassisDecimalFormat.TWO_DEC_EXP_FORMAT.format(deltaFreq));
		sb.append(" MHz  Vlsr = ").append(CassisDecimalFormat.VLSR_FORMAT.format(vlsr));
		sb.append(" km/s\nsnr = ").append(CassisDecimalFormat.TWO_DEC_FORMAT.format(snr));

		return sb.toString();
	}

	/**
	 * Create the identification message from an 8 columns file.
	 *
	 * @param name The name.
	 * @param freq The frequency (in MHz).
	 * @param eupk The eup.
	 * @param aij The aij.
	 * @return The identification message for the line with the given parameters.
	 */
	public static String createHeightColumnsFileIdentification(String name, double freq,
			double eupk, double aij) {
		StringBuilder sb = new StringBuilder();
		sb.append(name).append('\n');
		sb.append("v = ").append(freq).append("MHz  Eup = ");
		sb.append(eupk).append("K  Aij = ").append(aij);
		return sb.toString();
	}

	/**
	 * Create the identification message from an old Fit file result.
	 *
	 * @param name The name.
	 * @param freq The frequency (in MHz).
	 * @param eupk The eup.
	 * @param aij The aij.
	 * @param dobsFreq The delta frequency (in MHz).
	 * @param vlsr The VLSR.
	 * @return The identification message for the line with the given parameters.
	 */
	public static String createOldFitFileIdentification(String name, double freq,
			double eupk, double aij, double dobsFreq, double vlsr) {
		DecimalFormat df = CassisDecimalFormat.getFormatForNumber(String.valueOf(freq));
		StringBuilder sb = new StringBuilder();
		sb.append(name).append('\n');
		sb.append("v = ").append(freq).append("MHz  Eup = ").append(eupk);
		sb.append("K  Aij = ").append(aij).append('\n');
		sb.append("deltaFreq = ").append(df.format(dobsFreq));
		sb.append("MHz  Vlsr = ").append(vlsr).append(" km/s");
		return sb.toString();
	}
}
