/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.parameters;

import java.util.ArrayList;
import java.util.List;

/**
 * Class describing the information read in a rotational file.
 *
 * @author M. Boiziot
 */
public class RotationalFile {

	private List<RotationalMolecule> moleculeList;
	private boolean tmb;
	private boolean allFluxFirstMomentToZero;
	private List<BeamInfo> listBeamDone;


	/**
	 * Constructor.
	 */
	public RotationalFile() {
		this(null);
	}

	/**
	 * Constructor.
	 *
	 * @param listMols The list of {@link RotationalMolecule} read in the file.
	 */
	public RotationalFile(List<RotationalMolecule> listMols) {
		this.moleculeList = listMols;
		this.tmb = false;
		this.listBeamDone = new ArrayList<>();
	}

	/**
	 * Return the list of {@link RotationalMolecule}.
	 *
	 * @return the list of {@link RotationalMolecule}.
	 */
	public List<RotationalMolecule> getMoleculeList() {
		return moleculeList;
	}

	/**
	 * Set the list of {@link RotationalMolecule}.
	 *
	 * @param listMols The list of {@link RotationalMolecule} to set.
	 */
	public void setMoleculeList(List<RotationalMolecule> listMols) {
		this.moleculeList = listMols;
	}

	/**
	 * Return if the information are on tmb.
	 *
	 * @return if the information are on tmb.
	 */
	public boolean isTmb() {
		return tmb;
	}

	/**
	 * Set the value for Tmb.
	 *
	 * @param tmb the new value for Tmb.
	 */
	public void setTmb(boolean tmb) {
		this.tmb = tmb;
	}

	/**
	 * Set the value for allFluxFirstMomentToZero
	 *
	 * @param allFluxFirstMomentToZero The new value
	 */
	public void setAllFluxFirstMomentToZero(boolean allFluxFirstMomentToZero) {
		this.allFluxFirstMomentToZero = allFluxFirstMomentToZero;
	}

	/**
	 * @return If the value of the flux first moment is equal to 0 for all lines.
	 */
	public boolean isAllFluxFirstMomentToZero() {
		return allFluxFirstMomentToZero;
	}

	/**
	 * Return if the beam dilution operation is allowed for a component.
	 *
	 * @param tag The tag of the component.
	 * @param compNumber The number of the component.
	 * @return if the beam dilution operation is allowed.
	 */
	public boolean isBeamAllowed(int tag, int compNumber) {
		for (BeamInfo bi : listBeamDone) {
			if (bi.getTag() == tag && bi.getComponentNumber() == compNumber) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Marks the beam dilution operation as done for a component.
	 *
	 * @param tag The tag of the component.
	 * @param compNumber The number of the component.
	 */
	public void setBeamDone(int tag, int compNumber) {
		if (isBeamAllowed(tag, compNumber)) {
			listBeamDone.add(new BeamInfo(tag, compNumber));
		}
	}


	/**
	 * Basic class to represent the information needed here for beam.
	 *
	 * @author M. Boiziot
	 */
	private class BeamInfo {

		private int tag;
		private int compNumber;


		/**
		 * Constructor.
		 *
		 * @param tag The tag.
		 * @param compNumber The component number.
		 */
		private BeamInfo(int tag, int compNumber) {
			this.tag = tag;
			this.compNumber = compNumber;
		}

		/**
		 * Return the tag.
		 *
		 * @return the tag.
		 */
		private int getTag() {
			return tag;
		}

		/**
		 * Return the component number.
		 *
		 * @return the component number.
		 */
		private int getComponentNumber() {
			return compNumber;
		}
	}
}
