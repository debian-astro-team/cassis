/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.parameters;

import java.text.DecimalFormat;
import java.util.List;

import eu.omp.irap.cassis.common.CassisDecimalFormat;

/**
 * This class represent a point and all its information.
 *
 * @author Granier Vincent
 * @since 6 juin 2005
 * @author Rose Nerriere since 05-06/2007
 * @author glorian since 2008
 */
public class PointInformation implements Cloneable {

	public static enum VERSION {OLD, NEW, NEW_2014}

	private double wFirstMoment;
	private double deltaWFirstMoment;
	private double fwhmFirstMoment;

	private double wFit;
	private double deltaWFit;
	private double fwhmGaussianFit;
	private double fwhmLorentzianFit;

	private double wCalc;
	private double deltaWCalc;
	private double nu;
	private double lnNuOnGu;
	private double eup;
	private String telescope;

	private double sizeSource;
	private boolean taTmbSelect;
	private boolean isDoubleEup;
	private int haveManyPoints;
	private String moleNameQuantique;
	private double aij;
	private double gup = Double.NaN;
	private VERSION version;
	private double rms;
	private double calibration;
	private double deltaV;
	private RotationalSelectionData rotationalSelectionData;

	private double nuFit;
	private double velocity;
	private double deltaNuFit;
	private double deltaFwhmGaussianFit;
	private double intensityFit;
	private double deltaFwhmLorentzianFit;
	private double nuOfIntensityMax;
	private double deltaIntensityFit;
	private double deltaVelocity;
	private double velocityOfIntensityMax;
	private double intensityMax;
	private List<RotationalSelectionData> allowedType;
	private double coeff = Double.NaN;
	private boolean blendedLine;
	private double tau;
	private boolean multipleted = false;


	/**
	 * Constructor.
	 */
	public PointInformation() {
	}

	public PointInformation(double nu, String telescope, boolean isDouble) {
		this.nu = nu;
		this.telescope = telescope;
		this.isDoubleEup = isDouble;
		this.haveManyPoints = 0;
		this.version = VERSION.OLD;
	}

	/**
	 * @param nu
	 *            the nu to set
	 */
	public void setNu(double nu) {
		this.nu = nu;
	}

	/**
	 * Change W.
	 *
	 * @param w
	 *            new W
	 */
	public void setW(double w) {
		switch (rotationalSelectionData) {
		case VOIGHT_TYPE_DATA:
		case GAUSSIAN_TYPE_DATA:
		case LORENTZIAN_TYPE_DATA:
			this.wFit = w;
			break;
		case FIRST_TYPE_DATA:
			this.wFirstMoment = w;
			break;
		default:
			this.wFirstMoment = w;
			break;
		}
	}

	/**
	 * Change W. calc
	 *
	 * @param wCalc
	 *            new W
	 */
	public void setWCalc(double wCalc) {
		this.wCalc = wCalc;
	}

	/**
	 * Change the interval file.
	 *
	 * @param i
	 *            new interval
	 */
	public void setDeltaW(double i) {
		switch (rotationalSelectionData) {
		case VOIGHT_TYPE_DATA:
		case GAUSSIAN_TYPE_DATA:
		case LORENTZIAN_TYPE_DATA:
			this.deltaWFit = i;
			break;
		case FIRST_TYPE_DATA:
			this.deltaWFirstMoment = i;
			break;
		default:
			this.deltaWFirstMoment = i;
			break;
		}
	}

	/**
	 * Change the interval file calc.
	 *
	 * @param i
	 *            new interval
	 */
	public void setDeltaWCalc(double i) {
		this.deltaWCalc = i;
	}

	/**
	 * Change the fWHM.
	 *
	 * @param f
	 *            new fWHM
	 */
	public void setFwhm(double f) {
		switch (rotationalSelectionData) {
		case VOIGHT_TYPE_DATA:
			this.fwhmGaussianFit = f;
			this.fwhmLorentzianFit = f;
			break;
		case GAUSSIAN_TYPE_DATA:
			this.fwhmGaussianFit = f;
			break;
		case LORENTZIAN_TYPE_DATA:
			this.fwhmLorentzianFit = f;
			break;
		case FIRST_TYPE_DATA:
			this.fwhmFirstMoment = f;
			break;
		default:
			this.fwhmFirstMoment = f;
			break;
		}
	}

	/**
	 * Change the lnNuOnGu value.
	 *
	 * @param val
	 *            new lnNuOnGu
	 */
	public void setLnNuOnGu(double val) {
		this.lnNuOnGu = val;
	}

	/**
	 * Change the EuPerk value.
	 *
	 * @param val
	 *            new EuPerk
	 */
	public void setEup(double val) {
		this.eup = val;
	}

	/**
	 * Change if the point is double eup or not.
	 *
	 * @param isDouble true if double eup, false otherwise.
	 */
	public void setIsDoubleEup(boolean isDouble) {
		this.isDoubleEup = isDouble;
	}

	/**
	 * Return the number of point linked.
	 *
	 * @return int
	 */
	public int getManyPoints() {
		return this.haveManyPoints;
	}

	/**
	 * Change the number of points linked.
	 *
	 * @param manyPoints The number of points linked.
	 */
	public void setManyPoints(int manyPoints) {
		this.haveManyPoints = manyPoints;
	}

	/**
	 * Return if the point has a double eup or not.
	 *
	 * @return boolean
	 */
	public boolean getIsDoubleEup() {
		return this.isDoubleEup;
	}

	/**
	 * Return W.
	 *
	 * @return W
	 */
	public double getW() {
		double res;
		switch (rotationalSelectionData) {
		case VOIGHT_TYPE_DATA:
		case GAUSSIAN_TYPE_DATA:
		case LORENTZIAN_TYPE_DATA:
			res = getWFit();
			break;
		case FIRST_TYPE_DATA:
			res = getWFirstMoment();
			break;
		default:
			res = 0;
			break;
		}
		return res;
	}

	/**
	 * Return WCalc.
	 *
	 * @return W
	 */
	public double getWCalc() {
		return wCalc;
	}

	/**
	 * Return nu.
	 *
	 * @return nu
	 */
	public final double getNu() {
		return nu;
	}

	/**
	 * Return telescope.
	 *
	 * @return the name of the telescope.
	 */
	public String getTelescope() {
		return telescope;
	}

	/**
	 * Change telescope.
	 *
	 * @param telescope The telescope to set.
	 */
	public void setTelescope(String telescope) {
		this.telescope = telescope;
	}

	/**
	 * Return the interval file.
	 *
	 * @return interval
	 */
	public double getDeltaW() {
		double res = 0.0;
		switch (rotationalSelectionData) {
		case VOIGHT_TYPE_DATA:
		case GAUSSIAN_TYPE_DATA:
		case LORENTZIAN_TYPE_DATA:
			res = getDeltaWFit();
			break;
		case FIRST_TYPE_DATA:
			res = getDeltaWFirstMoment();
			break;
		default:
			res = 0;
			break;
		}
		return res;
	}

	/**
	 * Return the interval modified.
	 *
	 * @return interval
	 */
	public double getDeltaWCalc() {
		return deltaWCalc;
	}

	/**
	 * Return the lnNuOnGu value.
	 *
	 * @return lnNuOnGu
	 */
	public double getLnNuOnGu() {
		return lnNuOnGu;
	}

	/**
	 * Return the euPerK.
	 *
	 * @return euPerK
	 */
	public double getEup() {
		return eup;
	}

	/**
	 * Return fWHM.
	 *
	 * @return Double
	 */
	public double getFwhm() {
		double res;

		switch (rotationalSelectionData) {
		case VOIGHT_TYPE_DATA:
			res = Math.sqrt(getFwhmGaussianFit() * getFwhmGaussianFit()
					+ getFwhmLorentzianFit() * getFwhmLorentzianFit());
			break;
		case GAUSSIAN_TYPE_DATA:
			res = getFwhmGaussianFit();
			break;
		case LORENTZIAN_TYPE_DATA:
			res = getFwhmLorentzianFit();
			break;
		case FIRST_TYPE_DATA:
			res = getFwhmFirstMoment();
			break;
		default:
			res = 0;
			break;
		}

		return res;
	}

	/**
	 * Return the size source.
	 *
	 * @return the size source.
	 */
	public double getSizeSource() {
		return this.sizeSource;
	}

	/**
	 * Change the size source.
	 *
	 * @param sizeSource The size source to set.
	 */
	public void setSizeSource(double sizeSource) {
		this.sizeSource = sizeSource;
	}

	/**
	 * Return true if the taTmb box is selected.
	 *
	 * @return if the taTmb box is selected
	 */
	public boolean getTaTmbSelect() {
		return this.taTmbSelect;
	}

	/**
	 * Change the value of taTmbSelect.
	 *
	 * @param taTmbSelect the value of taTmbSelect to set.
	 */
	public void setTaTmbSelect(boolean taTmbSelect) {
		this.taTmbSelect = taTmbSelect;
	}

	/**
	 * Return the aij.
	 *
	 * @return the aij.
	 */
	public double getAij() {
		return aij;
	}

	/**
	 * @return the moleNameQuantique
	 */
	public String getMoleNameQuantique() {
		return moleNameQuantique;
	}

	/**
	 * @param moleNameQuantique
	 *            the moleNameQuantique to set
	 */
	public void setMoleNameQuantique(String moleNameQuantique) {
		this.moleNameQuantique = moleNameQuantique;
	}

	/**
	 * @param aij
	 *            the aij to set
	 */
	public void setAij(double aij) {
		this.aij = aij;
	}

	/**
	 * Return the gup.
	 *
	 * @return gup
	 */
	public double getGup() {
		return gup;
	}

	/**
	 * Change the gup value.
	 *
	 * @param val
	 *            new gup
	 */
	public void setGup(double val) {
		this.gup = val;
	}

	public void setVersion(VERSION val) {
		this.version = val;
	}

	public VERSION getVersion() {
		return version;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PointInformation [wFirstMoment=");
		builder.append(wFirstMoment);
		builder.append(", deltaWFirstMoment=");
		builder.append(deltaWFirstMoment);
		builder.append(", fwhmFirstMoment=");
		builder.append(fwhmFirstMoment);
		builder.append(", wFit=");
		builder.append(wFit);
		builder.append(", deltaWFit=");
		builder.append(deltaWFit);
		builder.append(", fwhmGaussianFit=");
		builder.append(fwhmGaussianFit);
		builder.append(", fwhmLorentzianFit=");
		builder.append(fwhmLorentzianFit);
		builder.append(", wCalc=");
		builder.append(wCalc);
		builder.append(", deltaWCalc=");
		builder.append(deltaWCalc);
		builder.append(", nu=");
		builder.append(nu);
		builder.append(", lnNuOnGu=");
		builder.append(lnNuOnGu);
		builder.append(", eup=");
		builder.append(eup);
		builder.append(", telescope=");
		builder.append(telescope);
		builder.append(", sizeSource=");
		builder.append(sizeSource);
		builder.append(", taTmbSelect=");
		builder.append(taTmbSelect);
		builder.append(", isDoubleEup=");
		builder.append(isDoubleEup);
		builder.append(", haveManyPoints=");
		builder.append(haveManyPoints);
		builder.append(", moleNameQuantique=");
		builder.append(moleNameQuantique);
		builder.append(", aij=");
		builder.append(aij);
		builder.append(", gup=");
		builder.append(gup);
		builder.append(", version=");
		builder.append(version);
		builder.append(", rms=");
		builder.append(rms);
		builder.append(", calibration=");
		builder.append(calibration);
		builder.append(", deltaV=");
		builder.append(deltaV);
		builder.append(", rotationalSelectionData=");
		builder.append(rotationalSelectionData);
		builder.append(", nuFit=");
		builder.append(nuFit);
		builder.append(", velocity=");
		builder.append(velocity);
		builder.append(", deltaNuFit=");
		builder.append(deltaNuFit);
		builder.append(", deltaFwhmGaussianFit=");
		builder.append(deltaFwhmGaussianFit);
		builder.append(", intensityFit=");
		builder.append(intensityFit);
		builder.append(", deltaFwhmLorentzianFit=");
		builder.append(deltaFwhmLorentzianFit);
		builder.append(", nuOfIntensityMax=");
		builder.append(nuOfIntensityMax);
		builder.append(", deltaIntensityFit=");
		builder.append(deltaIntensityFit);
		builder.append(", deltaVelocity=");
		builder.append(deltaVelocity);
		builder.append(", velocityOfIntensityMax=");
		builder.append(velocityOfIntensityMax);
		builder.append(", intensityMax=");
		builder.append(intensityMax);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(aij);
		result = prime * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(calibration);
		result = prime * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(coeff);
		result = prime * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(deltaFwhmGaussianFit);
		result = prime * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(deltaFwhmLorentzianFit);
		result = prime * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(deltaIntensityFit);
		result = prime * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(deltaNuFit);
		result = prime * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(deltaV);
		result = prime * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(deltaVelocity);
		result = prime * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(deltaWCalc);
		result = prime * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(deltaWFirstMoment);
		result = prime * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(deltaWFit);
		result = prime * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(eup);
		result = prime * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(fwhmFirstMoment);
		result = prime * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(fwhmGaussianFit);
		result = prime * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(fwhmLorentzianFit);
		result = prime * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(gup);
		result = prime * result + (int) (temp ^ temp >>> 32);
		result = prime * result + haveManyPoints;
		temp = Double.doubleToLongBits(intensityFit);
		result = prime * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(intensityMax);
		result = prime * result + (int) (temp ^ temp >>> 32);
		result = prime * result + (isDoubleEup ? 1231 : 1237);
		temp = Double.doubleToLongBits(lnNuOnGu);
		result = prime * result + (int) (temp ^ temp >>> 32);
		result = prime * result
				+ ((moleNameQuantique == null) ? 0 : moleNameQuantique.hashCode());
		temp = Double.doubleToLongBits(nu);
		result = prime * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(nuFit);
		result = prime * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(nuOfIntensityMax);
		result = prime * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(rms);
		result = prime * result + (int) (temp ^ temp >>> 32);
		result = prime
				* result
				+ ((rotationalSelectionData == null) ? 0 : rotationalSelectionData
						.hashCode());
		temp = Double.doubleToLongBits(sizeSource);
		result = prime * result + (int) (temp ^ temp >>> 32);
		result = prime * result + (taTmbSelect ? 1231 : 1237);
		temp = Double.doubleToLongBits(tau);
		result = prime * result + (int) (temp ^ temp >>> 32);
		result = prime * result + ((telescope == null) ? 0 : telescope.hashCode());
		temp = Double.doubleToLongBits(velocity);
		result = prime * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(velocityOfIntensityMax);
		result = prime * result + (int) (temp ^ temp >>> 32);
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		temp = Double.doubleToLongBits(wCalc);
		result = prime * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(wFirstMoment);
		result = prime * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(wFit);
		result = prime * result + (int) (temp ^ temp >>> 32);
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof PointInformation)) {
			return false;
		}
		PointInformation other = (PointInformation) obj;
		if (Double.doubleToLongBits(aij) != Double.doubleToLongBits(other.aij)) {
			return false;
		}
		if (blendedLine != other.blendedLine) {
			return false;
		}
		if (Double.doubleToLongBits(calibration) != Double
				.doubleToLongBits(other.calibration)) {
			return false;
		}
		if (Double.doubleToLongBits(coeff) != Double.doubleToLongBits(other.coeff)) {
			return false;
		}
		if (Double.doubleToLongBits(deltaFwhmGaussianFit) != Double
				.doubleToLongBits(other.deltaFwhmGaussianFit)) {
			return false;
		}
		if (Double.doubleToLongBits(deltaFwhmLorentzianFit) != Double
				.doubleToLongBits(other.deltaFwhmLorentzianFit)) {
			return false;
		}
		if (Double.doubleToLongBits(deltaIntensityFit) != Double
				.doubleToLongBits(other.deltaIntensityFit)) {
			return false;
		}
		if (Double.doubleToLongBits(deltaNuFit) != Double
				.doubleToLongBits(other.deltaNuFit)) {
			return false;
		}
		if (Double.doubleToLongBits(deltaV) != Double.doubleToLongBits(other.deltaV)) {
			return false;
		}
		if (Double.doubleToLongBits(deltaVelocity) != Double
				.doubleToLongBits(other.deltaVelocity)) {
			return false;
		}
		if (Double.doubleToLongBits(deltaWCalc) != Double
				.doubleToLongBits(other.deltaWCalc)) {
			return false;
		}
		if (Double.doubleToLongBits(deltaWFirstMoment) != Double
				.doubleToLongBits(other.deltaWFirstMoment)) {
			return false;
		}
		if (Double.doubleToLongBits(deltaWFit) != Double
				.doubleToLongBits(other.deltaWFit)) {
			return false;
		}
		if (Double.doubleToLongBits(eup) != Double.doubleToLongBits(other.eup)) {
			return false;
		}
		if (Double.doubleToLongBits(fwhmFirstMoment) != Double
				.doubleToLongBits(other.fwhmFirstMoment)) {
			return false;
		}
		if (Double.doubleToLongBits(fwhmGaussianFit) != Double
				.doubleToLongBits(other.fwhmGaussianFit)) {
			return false;
		}
		if (Double.doubleToLongBits(fwhmLorentzianFit) != Double
				.doubleToLongBits(other.fwhmLorentzianFit)) {
			return false;
		}
		if (Double.doubleToLongBits(gup) != Double.doubleToLongBits(other.gup)) {
			return false;
		}
		if (haveManyPoints != other.haveManyPoints) {
			return false;
		}
		if (Double.doubleToLongBits(intensityFit) != Double
				.doubleToLongBits(other.intensityFit)) {
			return false;
		}
		if (Double.doubleToLongBits(intensityMax) != Double
				.doubleToLongBits(other.intensityMax)) {
			return false;
		}
		if (isDoubleEup != other.isDoubleEup) {
			return false;
		}
		if (Double.doubleToLongBits(lnNuOnGu) != Double.doubleToLongBits(other.lnNuOnGu)) {
			return false;
		}
		if (moleNameQuantique == null) {
			if (other.moleNameQuantique != null) {
				return false;
			}
		} else if (!moleNameQuantique.equals(other.moleNameQuantique)) {
			return false;
		}
		if (Double.doubleToLongBits(nu) != Double.doubleToLongBits(other.nu)) {
			return false;
		}
		if (Double.doubleToLongBits(nuFit) != Double.doubleToLongBits(other.nuFit)) {
			return false;
		}
		if (Double.doubleToLongBits(nuOfIntensityMax) != Double
				.doubleToLongBits(other.nuOfIntensityMax)) {
			return false;
		}
		if (Double.doubleToLongBits(rms) != Double.doubleToLongBits(other.rms)) {
			return false;
		}
		if (rotationalSelectionData != other.rotationalSelectionData) {
			return false;
		}
		if (Double.doubleToLongBits(sizeSource) != Double
				.doubleToLongBits(other.sizeSource)) {
			return false;
		}
		if (taTmbSelect != other.taTmbSelect) {
			return false;
		}
		if (Double.doubleToLongBits(tau) != Double.doubleToLongBits(other.tau)) {
			return false;
		}
		if (telescope == null) {
			if (other.telescope != null) {
				return false;
			}
		} else if (!telescope.equals(other.telescope)) {
			return false;
		}
		if (Double.doubleToLongBits(velocity) != Double.doubleToLongBits(other.velocity)) {
			return false;
		}
		if (Double.doubleToLongBits(velocityOfIntensityMax) != Double
				.doubleToLongBits(other.velocityOfIntensityMax)) {
			return false;
		}
		if (version != other.version) {
			return false;
		}
		if (Double.doubleToLongBits(wCalc) != Double.doubleToLongBits(other.wCalc)) {
			return false;
		}
		if (Double.doubleToLongBits(wFirstMoment) != Double
				.doubleToLongBits(other.wFirstMoment)) {
			return false;
		}
		if (Double.doubleToLongBits(wFit) != Double.doubleToLongBits(other.wFit)) {
			return false;
		}
		return true;
	}

	public void setRms(double rms) {
		this.rms = rms;
	}

	public void setCalibration(double calibration) {
		this.calibration = calibration;
	}

	public void setDeltaV(double deltaV) {
		this.deltaV = deltaV;
	}

	public double getRms() {
		return rms;
	}

	public double getCalibration() {
		return calibration;
	}

	public double getDeltaV() {
		return deltaV;
	}

	public double getWFirstMoment() {
		return wFirstMoment;
	}

	public void setWFirstMoment(double wFirstMoment) {
		this.wFirstMoment = wFirstMoment;
	}

	public double getDeltaWFirstMoment() {
		return deltaWFirstMoment;
	}

	public void setDeltaWFirstMoment(double deltaWFirstMoment) {
		this.deltaWFirstMoment = deltaWFirstMoment;
	}

	public double getFwhmFirstMoment() {
		return fwhmFirstMoment;
	}

	public void setFwhmFirstMoment(double fwhmFirstMoment) {
		this.fwhmFirstMoment = fwhmFirstMoment;
	}

	public double getWFit() {
		return wFit;
	}

	public void setWFit(double wFit) {
		this.wFit = wFit;
	}

	public double getDeltaWFit() {
		return deltaWFit;
	}

	public void setDeltaWFit(double deltaWFit) {
		this.deltaWFit = deltaWFit;
	}

	public double getFwhmGaussianFit() {
		return fwhmGaussianFit;
	}

	public void setFwhmGaussianFit(double fwhmGaussianFit) {
		this.fwhmGaussianFit = fwhmGaussianFit;
	}

	public double getFwhmLorentzianFit() {
		return fwhmLorentzianFit;
	}

	public void setFwhmLorentzianFit(double fwhmLorentzianFit) {
		this.fwhmLorentzianFit = fwhmLorentzianFit;
	}

	public void setTypeData(RotationalSelectionData rotationalSelectionData) {
		this.rotationalSelectionData = rotationalSelectionData;
	}

	@Override
	public PointInformation clone() throws CloneNotSupportedException {
		PointInformation res = new PointInformation();
		res.aij = aij;
		res.blendedLine = blendedLine;
		res.calibration = calibration;
		res.coeff = coeff;
		res.deltaV = deltaV;
		res.deltaWCalc = deltaWCalc;
		res.deltaWFirstMoment = deltaWFirstMoment;
		res.deltaWFit = deltaWFit;
		res.eup = eup;
		res.fwhmFirstMoment = fwhmFirstMoment;
		res.fwhmGaussianFit = fwhmGaussianFit;
		res.fwhmLorentzianFit = fwhmLorentzianFit;
		res.gup = gup;
		res.haveManyPoints = haveManyPoints;
		res.isDoubleEup = isDoubleEup;
		res.lnNuOnGu = lnNuOnGu;
		res.moleNameQuantique = moleNameQuantique;
		res.nu = nu;
		res.rms = rms;
		res.rotationalSelectionData = rotationalSelectionData;
		res.sizeSource = sizeSource;
		res.taTmbSelect = taTmbSelect;
		res.tau = tau;
		res.telescope = telescope;
		res.version = version;
		res.wCalc = wCalc;
		res.wFirstMoment = wFirstMoment;
		res.wFit = wFit;
		return res;
	}

	public void setNuFit(double nuFit) {
		this.nuFit = nuFit;
	}

	public double getNuFit() {
		return this.nuFit;
	}

	public double getVelocity() {
		return this.velocity;
	}

	public void setDeltaNuFit(double deltaNuFit) {
		this.deltaNuFit = deltaNuFit;
	}

	public double getDeltaNuFit() {
		return this.deltaNuFit;
	}

	public double getDeltaFwhmGaussianFit() {
		return this.deltaFwhmGaussianFit;
	}

	public double getIntensityFit() {
		return this.intensityFit;
	}

	public double getDeltaFwhmLorentzianFit() {
		return this.deltaFwhmLorentzianFit;
	}

	public double getNuOfIntensityMax() {
		return this.nuOfIntensityMax;
	}

	public double getDeltaIntensityFit() {
		return this.deltaIntensityFit;
	}

	public double getVelocityOfIntensityMax() {
		return this.velocityOfIntensityMax;
	}

	public double getIntensityMax() {
		return this.intensityMax;
	}

	public void setVelocity(double velocity) {
		this.velocity = velocity;
	}

	public void setDeltaVelocity(double deltaVelocity) {
		this.deltaVelocity = deltaVelocity;
	}

	public double getDeltaVelocity() {
		return this.deltaVelocity;
	}

	public void setIntensityFit(double intensityFit) {
		this.intensityFit = intensityFit;
	}

	public void setDeltaIntensityFit(double deltaIntensityFit) {
		this.deltaIntensityFit = deltaIntensityFit;
	}

	public void setDeltaFwhmGaussianFit(double deltaFwhmGaussianFit) {
		this.deltaFwhmGaussianFit = deltaFwhmGaussianFit;
	}

	public void setDeltaFwhmLorentzianFit(double deltaFwhmLorentzianFit) {
		this.deltaFwhmLorentzianFit = deltaFwhmLorentzianFit;
	}

	public void setNuOfIntensityMax(double nuOfIntensityMax) {
		this.nuOfIntensityMax = nuOfIntensityMax;
	}

	public void setIntensityMax(double intensityMax) {
		this.intensityMax = intensityMax;
	}

	public void setVelocityOfIntensityMax(double velocityOfIntensityMax) {
		this.velocityOfIntensityMax = velocityOfIntensityMax;
	}

	public String getPopupInfo() {
		String info;
		DecimalFormat format = new DecimalFormat("###0.000");
		DecimalFormat tauFormat = new DecimalFormat("0.00E0");
		switch (version) {
			case OLD:
				info = "\u03BD = " + format.format(nu) + " MHz";
				if (tau != 0.0) {
					info = info + " Tau = " + tauFormat.format(tau);
				}
				info = info + "\nFlux = " + format.format(getW())
						+ " K.km/s \u2202Flux = "+ format.format(getDeltaW())
						+ " K.km/s FWHM = " + format.format(getFwhm()) + " km/s";
				break;
			case NEW:
			case NEW_2014:
			default:
				info = moleNameQuantique + " \u03BD = " + format.format(nu)
						+ " MHz\nEup = " + format.format(getEup()) + " K aij = "
						+ CassisDecimalFormat.AIJ_FORMAT.format(aij) + " Gup = "
						+ getGup();
				if (tau != 0.0) {
					info = info + " Tau = " + tauFormat.format(tau);
				}
				info = info + "\nFlux = " + format.format(getW())
						+ " K.km/s \u2202Flux = " + format.format(getDeltaW())
						+ " K.km/s FWHM = " + format.format(getFwhm()) + " km/s";
		}
		return info;
	}

	/**
	 * Return the current {@link RotationalSelectionData} selected.
	 *
	 * @return the current {@link RotationalSelectionData} selected.
	 */
	public RotationalSelectionData getRotationalSelectionData() {
		return rotationalSelectionData;
	}

	/**
	 * Return the List of {@link RotationalSelectionData} allowed.
	 *
	 * @return the List of {@link RotationalSelectionData} allowed.
	 */
	public List<RotationalSelectionData> getAllowedType() {
		return allowedType;
	}

	/**
	 * Set the List of {@link RotationalSelectionData} allowed for this
	 * {@link PointInformation}.
	 *
	 * @param type
	 *            the List of {@link RotationalSelectionData} allowed.
	 */
	public void setAllowedType(List<RotationalSelectionData> type) {
		this.allowedType = type;
	}

	public boolean hasCoeff() {
		return !Double.isNaN(coeff);
	}

	public double getCoeff() {
		return coeff;
	}

	public void setCoeff(double coeffValue) {
		this.coeff = coeffValue;
	}

	/**
	 * Return if the point is blended.
	 *
	 * @return if the point is blended.
	 */
	public boolean isBlendedLine() {
		return blendedLine;
	}

	/**
	 * Set if the point is blended.
	 *
	 * @param blended The blended value.
	 */
	public void setBlendedLine(boolean blended) {
		this.blendedLine = blended;
	}

	/**
	 * Return the tau of the point.
	 *
	 * @return the tau of the point.
	 */
	public double getTau() {
		return tau;
	}

	/**
	 * Set a new tau value to the point.
	 *
	 * @param newTau the new tau value.
	 */
	public void setTau(double newTau) {
		this.tau = newTau;
	}

	/**
	 * Set if the point have multiplet (other line with).
	 *
	 * @param multipled true if the point have multiplet, false otherwise.
	 */
	public void setMultiplet(boolean multipled) {
		this.multipleted = multipled;
	}

	/**
	 * Return if the point is a multiplet.
	 *
	 * @return if the point is a multiplet.
	 */
	public boolean isMultipleted() {
		return multipleted;
	}
}
