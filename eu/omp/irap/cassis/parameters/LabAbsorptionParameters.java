/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*Copyright Institut de Recherche en Astrophysique et Planétologie
 *contributor(s) : Jean-Michel Glorian (April 12, 2011)
 *
 *jglorian@irap.omp.eu
 *
 *This software is a computer program whose purpose is to analyze,
 *compare and model astrophysics data.
 *
 *This software is governed by the CeCILL-C license under French law and
 *abiding by the rules of distribution of free software.  You can  use,
 *modify and/ or redistribute the software under the terms of the CeCILL-C
 *license as circulated by CEA, CNRS and INRIA at the following URL
 *"http: *www.cecill.info".
 *
 *As a counterpart to the access to the source code and  rights to copy,
 *modify and redistribute granted by the license, users are provided only
 *with a limited warranty  and the software's author,  the holder of the
 *economic rights,  and the successive licensors  have only  limited
 *liability.
 *
 *In this respect, the user's attention is drawn to the risks associated
 *with loading,  using,  modifying and/or developing or reproducing the
 *software by the user in light of its specific status of free software,
 *that may mean  that it is complicated to manipulate,  and  that  also
 *therefore means  that it is reserved for developers  and  experienced
 *professionals having in-depth computer knowledge. Users are therefore
 *encouraged to load and test the software's suitability as regards their
 *requirements in conditions enabling the security of their systems and/or
 *data to be ensured and,  more generally, to use and operate it in the
 *same conditions as regards security.
 *The fact that you are presently reading this means that you have had
 *knowledge of the CeCILL-C license and that you accept its terms.
 */
package eu.omp.irap.cassis.parameters;

import java.util.ArrayList;
import java.util.List;

import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.common.SideBand;
import eu.omp.irap.cassis.gui.model.parameter.modeltuning.TuningMode;

/**
 * Class to set the input parameters of the labAbsorption Model.
 *
 * @author glorian
 */
public class LabAbsorptionParameters {

	/**
	 * Mode of the tuning : RANGE or LINE
	 */
	private TuningMode tuningMode;

	/**
	 * Frequency minimum in Mhz, must set the tuningMode in TuningMode.RANGE.
	 * Default value = 460 000.
	 */
	private double freqMin;

	/**
	 * Frequency maximum in Mhz, must set the tuningMode in TuningMode.RANGE.
	 * Default value = 462 000.
	 */
	private double freqMax;

	/**
	 * Line requency in Mhz, must set the tuningMode in TuningMode.LINE.
	 * NaN by default.
	 */
	private double freqLine;
	private double bandWidth;
	private boolean doubleSideBand;
	private SideBand dsb;
	private double eupMin;
	private double eupMax;
	private double aijMin;
	private double aijMax;
	private double deltaFreq;
	private double noise;
	private ContinuumParameters continuumParameters;
	private double temperature;
	private double pressure;
	private double length;
	private List<MoleculeDescription> listMolecule;
	private TelescopeParameters telescopeParameters;


	/**
	 * Constructor to create LabAbsorptionParameters with default parameters.
	 */
	public LabAbsorptionParameters() {
		tuningMode = TuningMode.RANGE;
		freqMin = 460000.;
		freqMax = 462000.;
		freqLine = Double.NaN;
		bandWidth = 0.;
		doubleSideBand = false;
		dsb = null;
		eupMin = 0.;
		eupMax = 150.;
		aijMin = 0.;
		aijMax = Double.MAX_VALUE;
		deltaFreq = 1.;
		noise = 0.;
		continuumParameters = new ContinuumParameters(1.);
		temperature = 296.;
		pressure = 3.3;
		length = 102.54;
		listMolecule = new ArrayList<>();
	}

	public boolean isRangeMode() {
		return TuningMode.RANGE.equals(tuningMode);
	}

	public double getFreqMin() {
		return freqMin;
	}

	public double getFreqMax() {
		return freqMax;
	}

	public boolean isLineMode() {
		return TuningMode.LINE.equals(tuningMode);
	}

	public double getFreqLine() {
		return freqLine;
	}

	public double getBandWith() {
		return bandWidth;
	}

	public boolean isDoubleSideBand() {
		return doubleSideBand;
	}

	public double getDsbValue() {
		double val = 0.;
		if (SideBand.LSB.equals(dsb))
			val = 1.;
		else if (SideBand.USB.equals(dsb))
			val = -1.;
		return val;
	}

	public double getEupMin() {
		return eupMin;
	}

	public double getEupMan() {
		return eupMax;
	}

	public double getAijMin() {
		return aijMin;
	}

	public double getDeltaFreq() {
		return deltaFreq;
	}

	public double getNoise() {
		return noise;
	}

	public ContinuumParameters getContinuumParameters() {
		return continuumParameters;
	}

	public double getTemperature() {
		return temperature;
	}

	public double getPressure() {
		return pressure;
	}

	public double getLength() {
		return length;
	}

	public List<MoleculeDescription> getListMolecule() {
		return listMolecule;
	}

	/**
	 * @return the tuningMode
	 */
	public final TuningMode getTuningMode() {
		return tuningMode;
	}

	/**
	 * @return the bandWidth
	 */
	public final double getBandWidth() {
		return bandWidth;
	}

	/**
	 * @return the dsb
	 */
	public final SideBand getDsb() {
		return dsb;
	}

	/**
	 * @return the eupMax
	 */
	public final double getEupMax() {
		return eupMax;
	}

	/**
	 * @param tuningMode
	 *            the tuningMode to set
	 */
	public final void setTuningMode(TuningMode tuningMode) {
		this.tuningMode = tuningMode;
	}

	/**
	 * @param freqMin
	 *            the freqMin to set
	 */
	public final void setFreqMin(double freqMin) {
		this.freqMin = freqMin;
	}

	/**
	 * @param freqMax
	 *            the freqMax to set
	 */
	public final void setFreqMax(double freqMax) {
		this.freqMax = freqMax;
	}

	/**
	 * @param freqLine
	 *            the freqLine to set
	 */
	public final void setFreqLine(double freqLine) {
		this.freqLine = freqLine;
	}

	/**
	 * @param bandWidth
	 *            the bandWidth to set
	 */
	public final void setBandWidth(double bandWidth) {
		this.bandWidth = bandWidth;
	}

	/**
	 * @param doubleSideBand
	 *            the doubleSideBand to set
	 */
	public final void setDoubleSideBand(boolean doubleSideBand) {
		this.doubleSideBand = doubleSideBand;
	}

	/**
	 * @param dsb
	 *            the dsb to set
	 */
	public final void setDsb(SideBand dsb) {
		this.dsb = dsb;
	}

	/**
	 * @param eupMin
	 *            the eupMin to set
	 */
	public final void setEupMin(double eupMin) {
		this.eupMin = eupMin;
	}

	/**
	 * @param eupMax
	 *            the eupMax to set
	 */
	public final void setEupMax(double eupMax) {
		this.eupMax = eupMax;
	}

	/**
	 * @param aijMin
	 *            the aijMin to set
	 */
	public final void setAijMin(double aijMin) {
		this.aijMin = aijMin;
	}

	/**
	 * @param deltaFreq
	 *            the deltaFreq to set
	 */
	public final void setDeltaFreq(double deltaFreq) {
		this.deltaFreq = deltaFreq;
	}

	/**
	 * @param noise
	 *            the noise to set
	 */
	public final void setNoise(double noise) {
		this.noise = noise;
	}

	/**
	 * @param continuumParameters
	 *            the continuumParameters to set
	 */
	public final void setContinuumParameters(ContinuumParameters continuumParameters) {
		this.continuumParameters = continuumParameters;
	}

	/**
	 * @param temperature
	 *            the temperature to set
	 */
	public final void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	/**
	 * @param pressure
	 *            the pressure to set
	 */
	public final void setPressure(double pressure) {
		this.pressure = pressure;
	}

	/**
	 * @param length
	 *            the length to set
	 */
	public final void setLength(double length) {
		this.length = length;
	}

	/**
	 * @param listMolecule
	 *            the listMolecule to set
	 */
	public final void setListMolecule(List<MoleculeDescription> listMolecule) {
		this.listMolecule = listMolecule;
	}

	/**
	 * @return the telescopeParameters
	 */
	public final TelescopeParameters getTelescopeParameters() {
		return telescopeParameters;
	}

	/**
	 * @param telescopeParameters
	 *            the telescopeParameters to set
	 */
	public final void setTelescopeParameters(TelescopeParameters telescopeParameters) {
		this.telescopeParameters = telescopeParameters;
	}

	public double getAijMax() {
		return aijMax;
	}

	public void setAijMax(double thresAijMax) {
		this.aijMax = thresAijMax;

	}

}
