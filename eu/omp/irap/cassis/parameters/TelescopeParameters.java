/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.parameters;

import java.util.List;

public class TelescopeParameters {

	protected List<Double> frequencyList;
	protected List<Double> intermediateList;
	protected List<Double> usbList;
	protected List<Double> lsbList;
	private Double diameter;

	/**
	 * @return the frequencyList
	 */
	public final List<Double> getFrequencyList() {
		return frequencyList;
	}

	/**
	 * @return the intermediateList
	 */
	public final List<Double> getIntermediateList() {
		return intermediateList;
	}

	/**
	 * @return the usbList
	 */
	public final List<Double> getUsbList() {
		return usbList;
	}

	/**
	 * @return the lsbList
	 */
	public final List<Double> getLsbList() {
		return lsbList;
	}

	/**
	 * @param frequencyList
	 *            the frequencyList to set
	 */
	public final void setFrequencyList(List<Double> frequencyList) {
		this.frequencyList = frequencyList;
	}

	/**
	 * @param intermediateList
	 *            the intermediateList to set
	 */
	public final void setIntermediateList(List<Double> intermediateList) {
		this.intermediateList = intermediateList;
	}

	/**
	 * @param usbList
	 *            the usbList to set
	 */
	public final void setUsbList(List<Double> usbList) {
		this.usbList = usbList;
	}

	/**
	 * @param lsbList
	 *            the lsbList to set
	 */
	public final void setLsbList(List<Double> lsbList) {
		this.lsbList = lsbList;
	}

	public Double getDiameter() {
		return diameter;
	}

	public final void setDiameter(Double diameter) {
		this.diameter = diameter;
	}

}
