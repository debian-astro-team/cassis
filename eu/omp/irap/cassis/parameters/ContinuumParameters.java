/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.parameters;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.ParamArrayDescription;
import eu.omp.irap.cassis.common.ParameterDescription;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.file.CassisFileFilter;
import eu.omp.irap.cassis.file.FileReaderCassis;
import eu.omp.irap.cassis.properties.Software;

/**
 * @author Laure Tamisier
 * @author Jean-Michel Glorian
 */
public class ContinuumParameters {

	private static final Logger LOGGER = LoggerFactory.getLogger(ContinuumParameters.class);
	public static final String DEFAULT_0 = "Continuum 0 [K]";
	public static final String DEFAULT_1 = "Continuum 1 [K]";

	// Values of Continuum Panel
	private String selectedContinuum;

	private double continuumAlpha = 0.;
	private double continuumBeta = 0.;
	private double continuum = 0.;
	private List<Double> freqContinuumList = new ArrayList<>();
	private List<Double> valueContinuumList = new ArrayList<>();

	private XAxisCassis xAxisCassis = XAxisCassis.getXAxisFrequency(UNIT.GHZ);
	private YAxisCassis yAxisCassis = YAxisCassis.getYAxisKelvin();

	/**
	 * @author glorian
	 */
	enum ContinuumType {
		CONSTANT, POWERLAW, LIST
	}
	private ContinuumType continuumType = ContinuumType.CONSTANT;


	/**
	 * Create Continuum with (x, y) coordonate x in GHZ, y in Kelvin.
	 *
	 * @param x The x coordonate in GHz.
	 * @param y The y coordonate in Kelvin.
	 */
	public ContinuumParameters(List<Double> x, List<Double> y) {
		this.freqContinuumList = x;
		this.valueContinuumList = y;
		this.continuumType = ContinuumType.LIST;
	}

	/**
	 * Create continnum object with formula : alpha x ^ beta.
	 *
	 * @param alpha The alpha value.
	 * @param beta The beta value.
	 */
	public ContinuumParameters(double alpha, double beta) {
		this.continuumAlpha = alpha;
		this.continuumBeta = beta;
		this.continuumType = ContinuumType.POWERLAW;

	}

	/**
	 * Default constructor, set the continuum to 0 Kelvin.
	 */
	public ContinuumParameters() {
	}

	/**
	 * Constructor.
	 *
	 * @param val The value of the constant continuum
	 */
	public ContinuumParameters(double val) {
		continuum = val;
		continuumType = ContinuumType.CONSTANT;
	}

	/**
	 * Constructor.
	 *
	 * @param continuum the name of the continum.
	 *  It can be the path of the coninuum file,
	 *  {@link ContinuumParameters#DEFAULT_0} or
	 *  {@link ContinuumParameters#DEFAULT_1}.
	 */
	public ContinuumParameters(String continuum) {
		super();
		setSelectedContinuum(continuum);
	}

	/**
	 * Constructor. Create the continuum parameters depending on the mapParameter.
	 * The key can be "continuum"; "continuumFreqList" and "continuumValueList";
	 * "continuumAlpha" and "continuumBeta".
	 *
	 * @param mapParameter The map contening the parameters.
	 */
	public ContinuumParameters(Map<String, ParameterDescription> mapParameter) {
		this();
		if (mapParameter.containsKey("continuum")) {
			continuum = mapParameter.get("continuum").getValue();
			continuumType = ContinuumType.CONSTANT;
		}
		else if (mapParameter.containsKey("continuumFreqList")) {
			freqContinuumList = ((ParamArrayDescription) mapParameter.get("continuumFreqList")).getParameterList();
			valueContinuumList = ((ParamArrayDescription) mapParameter.get("continuumValueList")).getParameterList();
			continuumType = ContinuumType.LIST;
		}
		else {
			continuumType = ContinuumType.POWERLAW;
			continuumAlpha = mapParameter.get("continuumAlpha").getValue();
			continuumBeta = mapParameter.get("continuumBeta").getValue();
		}
	}

	/**
	 * Resize a continuum between a frequency Min and a Frequency Max if the
	 *  continuum is not ContinuumType.LIST then return the same Continuum.
	 *
	 * @param freqmin The minimum frequency.
	 * @param freqmax The maximum frequency.
	 * @return the continuum parameters.
	 */
	public ContinuumParameters resize(double freqmin, double freqmax) {
		List<Double> continuumX = new ArrayList<>();
		List<Double> continuumY = new ArrayList<>();
		boolean debut = true;
		boolean fin = false;
		boolean visite = false;
		int i = 0;

		if (ContinuumType.POWERLAW.equals(this.continuumType))
			return this;
		if (ContinuumType.CONSTANT.equals(this.continuumType))
			return this;

		while (!fin && i < freqContinuumList.size()) {
			if (freqContinuumList.get(i) >= freqmin && freqContinuumList.get(i) <= freqmax) {
				if (debut) {
					if (i > 0) {
						continuumX.add(freqContinuumList.get(i - 1));
						continuumY.add(valueContinuumList.get(i - 1));
					}
					debut = false;
				}
				continuumX.add(freqContinuumList.get(i));
				continuumY.add(valueContinuumList.get(i));
				visite = true;
			}
			else if (freqContinuumList.get(i) > freqmax) {
				if (visite) {
					continuumX.add(freqContinuumList.get(i));
					continuumY.add(valueContinuumList.get(i));
				}
				else {
					if (i > 0) {
						continuumX.add(freqContinuumList.get(i - 1));
						continuumY.add(valueContinuumList.get(i - 1));
					}
					continuumX.add(freqContinuumList.get(i));
					continuumY.add(valueContinuumList.get(i));
				}
				fin = true;
			}
			i++;
		}

		return new ContinuumParameters(continuumX, continuumY);
	}

	/**
	 * Initialize the different continuum parameters
	 * with a constant continuum with 0 Kelvin by default.
	 */
	public void resetContinuumParameters() {
		selectedContinuum = DEFAULT_0;
		continuumType = ContinuumType.CONSTANT;
		freqContinuumList.clear();
		valueContinuumList.clear();
		continuumAlpha = 0.;
		continuumBeta = 0.;
	}

	public void readContinuumParameters(String selectedContinuum) {
		resetContinuumParameters();
		this.selectedContinuum = selectedContinuum;
		boolean continuumReaded = false;

		if (DEFAULT_0.equals(selectedContinuum)) {
			continuum = 0.;
			continuumType = ContinuumType.CONSTANT;
			continuumReaded = true;
		}
		else if (DEFAULT_1.equals(selectedContinuum)) {
			continuum = 1.;
			continuumType = ContinuumType.CONSTANT;
			continuumReaded = true;
		}
		else {
			try {
				boolean changed = false;
				File continuumFile = new File(selectedContinuum);
				if (!continuumFile.exists() || continuumFile.isDirectory()) {
					continuumFile = new File(Software.getContinuumPath() + File.separator + continuumFile.getName());
					changed = true;
				}

				if (CassisFileFilter.isDataFileForCassis(continuumFile.getName())) {
					CassisSpectrum spectrum = FileReaderCassis.createCassisSpectrumFromFile(continuumFile);
					if (spectrum == null) {
						continuumReaded = false;
					} else {
						for (int cpt = 0; cpt < spectrum.getNbChannel(); cpt++) {
							freqContinuumList.add(spectrum.getSignalFrequency(cpt));
							valueContinuumList.add(spectrum.getValue(cpt));
						}
						continuumType = ContinuumType.LIST;
						continuumReaded = true;
					}
				}
				if (! continuumReaded) {
					try (BufferedReader in =
							Software.getModePath().getContinuum(continuumFile.getPath())) {
						readContinuumFromClassicFormat(in);
						continuumReaded = true;
						if (changed) {
							this.selectedContinuum = continuumFile.getAbsolutePath();
						}
					}
				}
			} catch (Exception e) {
				LOGGER.error("Error while reading continuum parameters", e);
				continuumReaded = false;
			}

			if (!continuumReaded) {
				LOGGER.warn("Error during the read of the continuum, use 0 by default");
				resetContinuumParameters();
			}
		}
	}

	/**
	 * Read the continuum parameters from:
	 * 1. specific format = freq  \t  value
	 * 2. powerlaw format = + alpha beta
	 * 3. constant = value
	 *
	 * @param in The BufferedReader of the element to read.
	 * @throws Exception In case of Exception.
	 */
	public void readContinuumFromClassicFormat(BufferedReader in) throws Exception {
		StringTokenizer token;
		String line = readHeader(in);
		do {
			if (line != null && !line.startsWith("//") && !"".equals(line.trim())) {
				token = new StringTokenizer(line, " \t"); // delimiter = ' ' + '\t'
				if (line.startsWith("+")) {
					if (token.countTokens() != 3) {
						throw new Exception("Bad format for the powerlaw");
					}
					// Read '+' sign
					token.nextToken();
					// Read continuum alpha
					continuumAlpha = Double.parseDouble(token.nextToken());
					// Read continuum beta
					continuumBeta = Double.parseDouble(token.nextToken());
					continuumType = ContinuumType.POWERLAW;
					return;
				}
				else if (token.countTokens() == 1) { // constant
					continuum = Double.parseDouble(token.nextToken());
					continuumType = ContinuumType.CONSTANT;
					return;
				}
				else if (token.countTokens() >= 2) {
					double val = Double.parseDouble(token.nextToken());
					double value2 = Double.parseDouble(token.nextToken());
					if (xAxisCassis.isInverted()) {
						freqContinuumList.add(0, xAxisCassis.convertToMHzFreq(val, null));
						valueContinuumList.add(0, yAxisCassis.convertToKelvin(value2));
					}
					else {
						freqContinuumList.add(xAxisCassis.convertToMHzFreq(val, null));
						valueContinuumList.add(yAxisCassis.convertToKelvin(value2));
					}
					continuumType = ContinuumType.LIST;
				}
				else {
					throw new Exception("unknown format");
				}
			}
		} while ((line = in.readLine()) != null);
	}

	/**
	 * Read the header of continuum format to set the wAxis and YAxis unit.
	 *
	 * @param br The buffered reader.
	 * @return the next line read after the header.
	 * @throws IOException in case of IO error while reading the reader.
	 */
	public String readHeader(BufferedReader br) throws IOException {
		String line = null;
		do {
			line = br.readLine();
			if (line != null && line.startsWith("//")) {
				String[] metadata = line.replace("//", "").split(":");
				if (metadata.length == 2) {
					String key = metadata[0].trim().toLowerCase();
					String value = metadata[1].trim();
					if ("unitx".equals(key)) {
						xAxisCassis = XAxisCassis.getXAxisCassis(value);
					}
					else if ("unity".equals(key)) {
						if (value.equals(UNIT.JANSKY.getValString())) {
							yAxisCassis = YAxisCassis.getYAxisJansky();
						}
						else if (value.equals(UNIT.KELVIN.getValString())) {
							yAxisCassis = YAxisCassis.getYAxisKelvin();
						}
					}
				}
			}
		} while (line != null && line.startsWith("//"));

		return line;
	}

	/**
	 * Set selected continuum in combobox.
	 *
	 * @param continuum : continuum name selected
	 */
	public void setSelectedContinuum(String continuum) {
		selectedContinuum = continuum;
		readContinuumParameters(selectedContinuum);
	}

	/**
	 * Get selected continuum name.
	 *
	 * @return Selected continuum name
	 */
	public String getSelectedContinuum() {
		return selectedContinuum;
	}

	public double getContinuum() {
		return continuum;
	}

	/**
	 * Set the continuum constante value
	 * @param continuum the constante value of the continuum in K
	 */
	public void setContinuum(double continuum) {
		this.continuum = continuum;
		continuumType = ContinuumType.CONSTANT;

	}

	public double getContinuumAlpha() {
		return continuumAlpha;
	}

	public double getContinuumBeta() {
		return continuumBeta;
	}

	public List<Double> getFreqContinuumList() {
		return freqContinuumList;
	}

	public boolean isPowerlaw() {
		return ContinuumType.POWERLAW.equals(continuumType);
	}

	public List<Double> getValueContinuumList() {
		return valueContinuumList;
	}

	public XAxisCassis getXAxisCassis() {
		return xAxisCassis;
	}

	public YAxisCassis getYAxisCassis() {
		return yAxisCassis;
	}

	/**
	 * Return extrapolate continuum for the given frequency.
	 *
	 * @param frequency
	 *            frequency
	 * @return continuum value
	 * @throws Exception
	 */
	private double getExtrapolateContinuum(double frequency, List<Double> continuumFreqList,
			List<Double> continuumValueList) throws IndexOutOfBoundsException {
		double continuumValue = continuum;

		if (continuumFreqList != null && continuumValueList != null && !continuumFreqList.isEmpty()
				&& !continuumValueList.isEmpty()) {
			if (frequency < continuumFreqList.get(0)) {
				throw new IndexOutOfBoundsException("Check continuum file: frequency out of range.");
			}
			else if (!continuumValueList.isEmpty()) {
				if (Math.abs(frequency - continuumFreqList.get(0)) < 1.10E-6)
					continuumValue = continuumValueList.get(0);
				else {
					double intermediateValue = 0;
					int g = 0; // Left
					int m = -1; // Center
					int d = continuumFreqList.size() - 1; // Right

					while (d - g > 1) {
						m = Math.abs((g + d) / 2); /* division entiere */
						intermediateValue = continuumFreqList.get(m);
						if (Math.abs(frequency - intermediateValue) < 1.10E-6)
							break;
						if (frequency < intermediateValue)
							d = m;
						else {
							g = m;
						}
					}

					if (Math.abs(frequency - intermediateValue) < 1.10E-6)
						return continuumValueList.get(m);
					try {
						double yd = continuumValueList.get(d);
						double yg = continuumValueList.get(g);
						double xd = continuumFreqList.get(d);
						double xg = continuumFreqList.get(g);
						continuumValue = yd + (yd - yg) / (xd - xg) * (frequency - xd);
					} catch (IndexOutOfBoundsException e) {
						LOGGER.error("Check continuum file: frequency out of range.", e);
					}
				}
			}
		}
		return continuumValue;
	}

	/**
	 * Return the continuum value for the provided frequency.
	 *
	 * @param frequencySignal The frequency.
	 * @return the value of the continuum at the given frequency.
	 */
	public double getContinuumValue(double frequencySignal) {
		double tmpContinuum = 0;
		if (isPowerlaw())
			tmpContinuum = Math.pow(getContinuumAlpha() * frequencySignal, getContinuumBeta());
		else if (ContinuumType.LIST.equals(continuumType))
			tmpContinuum = getExtrapolateContinuum(frequencySignal, getFreqContinuumList(), getValueContinuumList());
		else
			tmpContinuum = getContinuum();
		return tmpContinuum;
	}

	public boolean isConstantContinuum() {
		return ContinuumType.CONSTANT.equals(continuumType);
	}

}
