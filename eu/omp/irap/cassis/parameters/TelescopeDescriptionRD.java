/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.parameters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.Telescope;

/**
 * Class which allow to read the file telescope to calculate the diameter of the
 * telescope.
 *
 * @author Rose Nerriere
 * @since 08/06/07
 */
public class TelescopeDescriptionRD implements Serializable {

	private static final long serialVersionUID = 3834586599978643760L;
	private static final Logger LOGGER = LoggerFactory.getLogger(TelescopeDescriptionRD.class);

	private String nameTelescope;
	private double diameterTelescope;
	private ArrayList<Double> frequencyList;
	private ArrayList<Double> effValueList;
	private ArrayList<Double> intermediateFrequency = new ArrayList<>();


	public TelescopeDescriptionRD(String nameTel) {
		// delete the " " at the end of the file name
		this.nameTelescope = nameTel.trim();
		frequencyList = new ArrayList<>();
		effValueList = new ArrayList<>();
		readTelescopeParameters(nameTelescope);
	}

	/**
	 * Read telescope parameters.
	 *
	 * @param filename The name of the telescope or its path.
	 */
	public void readTelescopeParameters(String filename) {
		StringTokenizer tok;
		double interFreq;

		try (BufferedReader in = new BufferedReader(
				Telescope.getTelescope(filename))) {
			String line;

			frequencyList.clear();
			effValueList.clear();

			while ((line = in.readLine()) != null) {
				if (!line.startsWith("//")) {
					tok = new StringTokenizer(line, " \t");
					if (tok.countTokens() == 1)
						diameterTelescope = Double.parseDouble(tok.nextToken());

					else {
						frequencyList.add(Double.valueOf(tok.nextToken()));
						effValueList.add(Double.valueOf(tok.nextToken()));
						// Get intermediate frequency in MHz
						interFreq = Double.parseDouble(tok.nextToken()) * 1000.;
						intermediateFrequency.add(Double.valueOf(interFreq));
					}
				}
			}
		} catch (IOException ioe) {
			LOGGER.error("Error while reading the telescope file {}", filename, ioe);
		}
	}

	/**
	 * Return the diameter of the telescope.
	 *
	 * @return the diameter of the telescope.
	 */
	public double getTelescopeDiameter() {
		return this.diameterTelescope;
	}

	/**
	 * Return the list of frequencies.
	 *
	 * @return the list of frequencies.
	 */
	public ArrayList<Double> getFrequencyList() {
		return this.frequencyList;
	}

	/**
	 * Return the list of efficiency values.
	 *
	 * @return the list of efficiency values.
	 */
	public ArrayList<Double> getEffValueList() {
		return this.effValueList;
	}

	/**
	 * Serialize theTelescopeDescription.
	 *
	 * @param out The destination {@link ObjectOutputStream}.
	 * @throws IOException in case of error during the writing.
	 */
	private void writeObject(final ObjectOutputStream out) throws IOException {
		out.writeObject(diameterTelescope);
		out.writeObject(effValueList);
		out.writeObject(frequencyList);
	}

	/**
	 * Get Min Frequency.
	 *
	 * @return the freqMin
	 */
	public double getMinFreq() {
		if (frequencyList.isEmpty())
			return 0;
		else
			return frequencyList.get(0);
	}

	/**
	 * Get Max Frequency.
	 *
	 * @return the frequency max
	 */
	public double getMaxFreq() {
		if (frequencyList.isEmpty())
			return 0;
		else
			return frequencyList.get(frequencyList.size() - 1);
	}

}
