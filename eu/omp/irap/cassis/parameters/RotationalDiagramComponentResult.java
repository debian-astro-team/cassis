/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.parameters;

import java.util.List;

/**
 * Class decribing the result for a Rotational Diagram component.
 *
 * @author M. Boiziot
 */
public class RotationalDiagramComponentResult {

	private int componentNumber;
	private RotationalSelectionData type;
	private List<PointInformation> result;
	private List<PointInformation> resultEupDiff;
	private boolean beamCorrection;


	/**
	 * Constructor.
	 *
	 * @param componentNumber The number of the component.
	 * @param type The type of {@link RotationalSelectionData} used.
	 * @param result The result.
	 * @param resultEupDiff The eup diff result.
	 * @param beamCorrection If a beam correction was applied to the component.
	 */
	public RotationalDiagramComponentResult(int componentNumber,
			RotationalSelectionData type, List<PointInformation> result,
			List<PointInformation> resultEupDiff, boolean beamCorrection) {
		this.componentNumber = componentNumber;
		this.type = type;
		this.result = result;
		this.resultEupDiff = resultEupDiff;
		this.beamCorrection = beamCorrection;
	}

	/**
	 * Return the result (as List of {@link PointInformation}).
	 *
	 * @return The result.
	 */
	public List<PointInformation> getResult() {
		return result;
	}

	/**
	 * Return the eup diff result (as List of {@link PointInformation}).
	 *
	 * @return The eup diff result.
	 */
	public List<PointInformation> getResultEupDiff() {
		return resultEupDiff;
	}

	/**
	 * Return the number of the component.
	 *
	 * @return The number of the component.
	 */
	public int getComponentNumber() {
		return componentNumber;
	}

	/**
	 * Return the type of {@link RotationalSelectionData} used for the component.
	 *
	 * @return The type of {@link RotationalSelectionData} used for the component.
	 */
	public RotationalSelectionData getType() {
		return type;
	}

	/**
	 * Return if a beam correction was applied to the component.
	 *
	 * @return true if a beam correction was applied, false otherwise.
	 */
	public boolean isBeamCorrection() {
		return beamCorrection;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RotationalDiagramComponentResult [componentNumber=" + componentNumber
				+ ", type=" + type + ", result=" + result + ", resultEupDiff="
				+ resultEupDiff + ", beamCorrection=" + beamCorrection + "]";
	}

}
