/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.parameters;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RotationalMolecule {

	private static final Logger LOGGER = LoggerFactory.getLogger(RotationalMolecule.class);

	private String nameMol;
	private int tag;
	private List<RotationalComponent> components;


	public RotationalMolecule(int tag, String nameMol) {
		this.tag = tag;
		this.nameMol = nameMol;
		components = new ArrayList<>();
	}

	/**
	 * Return the name of the molecule.
	 *
	 * @return the name of the molecule.
	 */
	public String getNameMol() {
		return nameMol;
	}

	/**
	 * Return the tag of the molecule.
	 *
	 * @return Integer
	 */
	public int getTag() {
		return this.tag;
	}

	public boolean contains(int comp) {
		for (int index = 0; index < components.size(); index++){
			RotationalComponent compo = components.get(index);
			if (compo.getNumCompo() == comp) {
				return true;
			}
		}
		return false;
	}

	public RotationalComponent addComponent(int tag, int comp) {
		RotationalComponent component = new RotationalComponent(tag, comp);
		components.add(component);
		return component;

	}

	public RotationalComponent getComponent(int comp) {
		RotationalComponent res = null;
		for (int index = 0; index < components.size() && res == null ; index++){
			RotationalComponent compo = components.get(index);
			int numCompo = compo.getNumCompo();
			if (numCompo == comp)
				res =  compo;
		}
		return res;
	}

	public List<RotationalComponent> getComponentsList() {
		return components;
	}

	public List<Integer> getComponentsNumberList() {
		List<Integer> list = new ArrayList<>();
		for (RotationalComponent compo : components) {
			list.add(compo.getNumCompo());
		}
		return list;
	}

	public boolean isOldVersion() {
		boolean val;
		try {
			val = components.get(0).getListPoints().get(0).getVersion() == PointInformation.VERSION.OLD;
		} catch (Exception e) {
			LOGGER.debug("Exception while trying to determinate if the file is an old version", e);
			val = true;
		}
		return val;
	}
}
