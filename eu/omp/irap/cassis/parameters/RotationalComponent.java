/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.parameters;


import java.util.ArrayList;
import java.util.List;

/**
 * Class describing a rotational component.
 *
 * @author M. Boiziot
 */
public class RotationalComponent {

	private List<PointInformation> listPoints;
	private int numCompo;
	private RotationalSelectionData dataType = RotationalSelectionData.FIRST_TYPE_DATA;
	private int tag;


	public RotationalComponent(int tag, int comp) {
		this.tag = tag;
		numCompo = comp;
		listPoints = new ArrayList<>();
	}

	/**
	 * Return the component number.
	 *
	 * @return the component number.
	 */
	public int getNumCompo() {
		return numCompo;
	}

	/**
	 * Add a {@link PointInformation} to the component.
	 *
	 * @param point The point to add.
	 */
	public void addPoint(PointInformation point) {
		listPoints.add(point);
	}

	/**
	 * Set the {@link RotationalSelectionData}.
	 *
	 * @param dataType the new data type.
	 */
	public void setDataType(RotationalSelectionData dataType) {
		this.dataType = dataType;
	}

	/**
	 * Return the data type.
	 *
	 * @return the data type.
	 */
	public RotationalSelectionData getDataType() {
		return dataType;
	}

	/**
	 * Return the list of {@link PointInformation}.
	 *
	 * @return the list of {@link PointInformation}.
	 */
	public List<PointInformation> getListPoints() {
		return listPoints;
	}

	/**
	 * Return the tag.
	 *
	 * @return the tag.
	 */
	public int getTag() {
		return tag;
	}

	/**
	 * Return the source size if all {@link PointInformation} have the same
	 * source size or {@link Double#NaN} if the points have different source size.
	 *
	 * @return the source size of points or NaN if they have different source size.
	 */
	public double getSourceSize() {
		if (sameSourceSize()) {
			return listPoints.get(0).getSizeSource();
		}
		return Double.NaN;
	}

	/**
	 * Return if all points have the same source size.
	 *
	 * @return true if the points have the same source size, false otherwise.
	 */
	private boolean sameSourceSize() {
		if (listPoints.size() <= 1) {
			return true;
		}
		double ss = listPoints.get(0).getSizeSource();
		for (int i = 1; i < listPoints.size(); i++) {
			if (listPoints.get(i).getSizeSource() != ss) {
				return false;
			}
		}
		return true;
	}
}
