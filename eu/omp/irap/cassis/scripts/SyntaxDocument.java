/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/**
 * Source: http://www.discoverteenergy.com/files/SyntaxDocument.java
 * Modified: Constructor, getOtherToken, getSingleLineDelimiter, isDelimiter
 * Methods added: createStyle, isMethod, isVariable, isNumber, isSubdelimiter, isAttribute, isClass
 * Variables added: method, variable, number, delimiter, attribute, classname
 */
package eu.omp.irap.cassis.scripts;

import java.awt.Color;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Element;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

@SuppressWarnings("serial")
public class SyntaxDocument extends DefaultStyledDocument {
	private DefaultStyledDocument doc;
	private Element rootElement;

	private boolean multiLineComment;
	private MutableAttributeSet normal, delimiter;
	protected MutableAttributeSet keyword, comment, quote, number, method, variable, attribute, classname;

	protected Set<String> keywords;


	public SyntaxDocument() {
		doc = this;
		rootElement = doc.getDefaultRootElement();
		final String newline = System.getProperty("line.separator");
		putProperty(DefaultEditorKit.EndOfLineStringProperty, newline);

		// Defines styles
		Color keywordColor = Color.BLUE;
		Color commentColor = Color.RED.darker();
		Color stringColor = new Color(0, 128, 0); // dark green
		Color numberColor = Color.MAGENTA.darker();
		Color methodColor = new Color(0xB03060); // maroon
		Color variableColor = new Color(128, 128, 128); // dark gray
		Color attrColor = new Color(102, 102, 0); // dark orange
		Color classColor = new Color(128, 128, 128); // dark maroon

		normal = createStyle("", Color.WHITE, Color.BLACK, false, false);
		comment = createStyle("", Color.WHITE, commentColor, false, false);
		keyword = createStyle("", Color.WHITE, keywordColor, false, false);
		quote = createStyle("", Color.WHITE, stringColor, false, false);
		method = createStyle("", Color.WHITE, methodColor, false, false);
		variable = createStyle("", Color.WHITE, variableColor, false, false);
		number = createStyle("", Color.WHITE, numberColor, false, false);
		delimiter = createStyle("", Color.WHITE, Color.BLACK, false, false);
		attribute = createStyle("", Color.WHITE, attrColor, false, false);
		classname = createStyle("", Color.WHITE, classColor, true, false);

		keywords = new HashSet<String>(Arrays.asList(new String[] { "abstract", "boolean", "break", "byte", "byvalue", "case", "cast",
				"catch", "char", "class", "const", "continue", "def", "default", "do", "double", "elif", "else",
				"extends", "false", "False", "final", "finally", "float", "for", "from", "future", "generic", "goto",
				"if", "implements", "import", "in", "inner", "instanceof", "int", "interface", "long", "native", "new",
				"null", "operator", "outer", "package", "private", "protected", "public", "rest", "return", "self",
				"short", "static", "super", "switch", "synchronized", "this", "throw", "throws", "transient", "true",
				"True", "try", "var", "void", "volatile", "while" }));
	}

	/**
	 * Create a style
	 */
	private static MutableAttributeSet createStyle(String name, Color bg, Color fg, boolean bold, boolean italic) {
		SimpleAttributeSet style = new SimpleAttributeSet();
		StyleConstants.setFontFamily(style, JythonModel.FONTNAME);
		StyleConstants.setFontSize(style, JythonModel.FONT_SIZE);
		StyleConstants.setBackground(style, bg);
		StyleConstants.setForeground(style, fg);
		StyleConstants.setBold(style, bold);
		StyleConstants.setItalic(style, italic);
		return style;
	}

	/**
	 * Update the style color
	 */
	protected static void updateStyleColor(MutableAttributeSet style, Color newColor) {
		StyleConstants.setForeground(style, newColor);
	}

	/**
	 * Override to apply syntax highlighting after the document has been updated
	 */
	@Override
	public void insertString(int offset, String str, AttributeSet a) throws BadLocationException {
		if (str.equals("{"))
			str = addMatchingBrace(offset);

		super.insertString(offset, str, a);
		processChangedLines(offset, str.length());
	}

	/**
	 * Override to apply syntax highlighting after the document has been updated
	 */
	@Override
	public void remove(int offset, int length) throws BadLocationException {
		super.remove(offset, length);
		processChangedLines(offset, 0);
	}

	public void refresh() {
		try {
			processChangedLines(0, doc.getLength());
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Determine how many lines have been changed, then apply highlighting to each line
	 */
	public void processChangedLines(int offset, int length) throws BadLocationException {
		String content = doc.getText(0, doc.getLength());

		// The lines affected by the latest document update
		int startLine = rootElement.getElementIndex(offset);
		int endLine = rootElement.getElementIndex(offset + length);

		// Make sure all comment lines prior to the start line are commented
		// and determine if the start line is still in a multi-line comment
		setMultiLineComment(commentLinesBefore(content, startLine));

		// Do the actual highlighting
		for (int i = startLine; i <= endLine; i++) {
			applyHighlighting(content, i);
		}

		// Resolve highlighting to the next end multi-line delimiter
		if (isMultiLineComment())
			commentLinesAfter(content, endLine);
		else
			highlightLinesAfter(content, endLine);
	}

	/**
	 * Highlight lines when a multi-line comment is still 'open' (ie. matching end delimiter has not yet been encountered)
	 */
	private boolean commentLinesBefore(String content, int line) {
		int offset = rootElement.getElement(line).getStartOffset();

		// Start of comment not found, nothing to do
		int startDelimiter = lastIndexOf(content, getStartDelimiter(), offset - 2);
		if (startDelimiter < 0)
			return false;

		// Matching start/end of comment found, nothing to do
		int endDelimiter = indexOf(content, getEndDelimiter(), startDelimiter);
		if (endDelimiter < offset & endDelimiter != -1)
			return false;

		// End of comment not found, highlight the lines
		doc.setCharacterAttributes(startDelimiter, offset - startDelimiter + 1, comment, false);
		return true;
	}

	/**
	 * Highlight comment lines to matching end delimiter
	 */
	private void commentLinesAfter(String content, int line) {
		int offset = rootElement.getElement(line).getEndOffset();

		// End of comment not found, nothing to do
		int endDelimiter = indexOf(content, getEndDelimiter(), offset);
		if (endDelimiter < 0)
			return;

		// Matching start/end of comment found, comment the lines
		int startDelimiter = lastIndexOf(content, getStartDelimiter(), endDelimiter);
		if (startDelimiter < 0 || startDelimiter <= offset) {
			doc.setCharacterAttributes(offset, endDelimiter - offset + 1, comment, false);
		}
	}

	/**
	 * Highlight lines to start or end delimiter
	 */
	private void highlightLinesAfter(String content, int line) throws BadLocationException {
		int offset = rootElement.getElement(line).getEndOffset();

		// Start/End delimiter not found, nothing to do
		int startDelimiter = indexOf(content, getStartDelimiter(), offset);
		int endDelimiter = indexOf(content, getEndDelimiter(), offset);

		if (startDelimiter < 0)
			startDelimiter = content.length();

		if (endDelimiter < 0)
			endDelimiter = content.length();

		int currentDelimiter = Math.min(startDelimiter, endDelimiter);

		if (currentDelimiter < offset)
			return;

		// Start/End delimiter found, reapply highlighting
		int endLine = rootElement.getElementIndex(currentDelimiter);
		for (int i = line + 1; i < endLine; i++) {
			Element branch = rootElement.getElement(i);
			Element leaf = doc.getCharacterElement(branch.getStartOffset());
			AttributeSet as = leaf.getAttributes();

			if (as.isEqual(comment))
				applyHighlighting(content, i);
		}
	}

	/**
	 * Parse the line to determine the appropriate highlighting
	 */
	private void applyHighlighting(String content, int line) throws BadLocationException {
		int startOffset = rootElement.getElement(line).getStartOffset();
		int endOffset = rootElement.getElement(line).getEndOffset() - 1;

		int lineLength = endOffset - startOffset;
		int contentLength = content.length();

		if (endOffset >= contentLength)
			endOffset = contentLength - 1;

		// check for multi-line comments
		// (always set the comment attribute for the entire line)
		if (endingMultiLineComment(content, startOffset, endOffset) || isMultiLineComment()
				|| startingMultiLineComment(content, startOffset, endOffset)) {
			doc.setCharacterAttributes(startOffset, endOffset - startOffset + 1, comment, false);
			return;
		}

		// set normal attributes for the line
		doc.setCharacterAttributes(startOffset, lineLength, normal, true);

		// check for single line comment
		int index = content.indexOf(getSingleLineDelimiter(), startOffset);

		if ((index > -1) && (index < endOffset)) {
			doc.setCharacterAttributes(index, endOffset - index + 1, comment, false);
			endOffset = index - 1;
		}

		// check for tokens
		checkForTokens(content, startOffset, endOffset);
	}

	/**
	 * Does this line contain the start delimiter
	 */
	private boolean startingMultiLineComment(String content, int startOffset, int endOffset)
			throws BadLocationException {
		int index = indexOf(content, getStartDelimiter(), startOffset);

		if ((index < 0) || (index > endOffset))
			return false;
		else {
			setMultiLineComment(true);
			return true;
		}
	}

	/**
	 * Does this line contain the end delimiter
	 */
	private boolean endingMultiLineComment(String content, int startOffset, int endOffset) throws BadLocationException {
		int index = indexOf(content, getEndDelimiter(), startOffset);

		if ((index < 0) || (index > endOffset))
			return false;
		else {
			setMultiLineComment(false);
			return true;
		}
	}

	/**
	 * We have found a start delimiter and are still searching for the end delimiter
	 */
	private boolean isMultiLineComment() {
		return multiLineComment;
	}

	private void setMultiLineComment(boolean value) {
		multiLineComment = value;
	}

	/**
	 * Parse the line for tokens to highlight
	 */
	private void checkForTokens(String content, int startOffset, int endOffset) {
		while (startOffset <= endOffset) {
			// skip the delimiters to find the start of a new token
			while (isDelimiter(content.substring(startOffset, startOffset + 1))) {
				if (startOffset < endOffset)
					startOffset++;
				else
					return;
			}

			// Extract and process the entire token
			if (isQuoteDelimiter(content.substring(startOffset, startOffset + 1)))
				startOffset = getQuoteToken(content, startOffset, endOffset);
			else
				startOffset = getOtherToken(content, startOffset, endOffset);
		}
	}

	private int getQuoteToken(String content, int startOffset, int endOffset) {
		String quoteDelimiter = content.substring(startOffset, startOffset + 1);
		String escapeString = getEscapeString(quoteDelimiter);

		int index;
		int endOfQuote = startOffset;

		// skip over the escape quotes in this quote
		index = content.indexOf(escapeString, endOfQuote + 1);
		while ((index > -1) && (index < endOffset)) {
			endOfQuote = index + 1;
			index = content.indexOf(escapeString, endOfQuote);
		}

		// now find the matching delimiter
		index = content.indexOf(quoteDelimiter, endOfQuote + 1);
		if ((index < 0) || (index > endOffset))
			endOfQuote = endOffset;
		else
			endOfQuote = index;

		doc.setCharacterAttributes(startOffset, endOfQuote - startOffset + 1, quote, false);

		return endOfQuote + 1;
	}

	private int getOtherToken(String content, int startOffset, int endOffset) {
		int endOfToken = startOffset + 1;
		while (endOfToken <= endOffset) {
			if (isDelimiter(content.substring(endOfToken, endOfToken + 1))) {
				if (isSubdelimiter(content.substring(endOfToken, endOfToken + 1))) {
					doc.setCharacterAttributes(endOfToken, 1, delimiter, false);
				}
				break;
			}

			endOfToken++;
		}

		String token = content.substring(startOffset, endOfToken);
		if (isKeyword(token)) {
			doc.setCharacterAttributes(startOffset, endOfToken - startOffset, keyword, false);
		}
		return endOfToken + 1;
	}

	protected boolean isMethod(String token) {
		return false;
	}

	protected boolean isVariable(String token) {
		return false;
	}

	protected boolean isNumber(String token) {
		return false;
	}

	protected boolean isAttribute(String token) {
		return false;
	}

	protected boolean isClass(String token) {
		return false;
	}

	/**
	 * Assume the needle will the found at the start/end of the line
	 */
	private int indexOf(String content, String needle, int offset) {
		int index;

		while ((index = content.indexOf(needle, offset)) != -1) {
			String text = getLine(content, index).trim();

			if (text.startsWith(needle) || text.endsWith(needle))
				break;
			else
				offset = index + 1;
		}

		return index;
	}

	/**
	 * Assume the needle will the found at the start/end of the line
	 */
	private int lastIndexOf(String content, String needle, int offset) {
		int index;

		while ((index = content.lastIndexOf(needle, offset)) != -1) {
			String text = getLine(content, index).trim();
			if (text.startsWith(needle) || text.endsWith(needle))
				break;
			else
				offset = index - 1;
		}

		return index;
	}

	private String getLine(String content, int offset) {
		int line = rootElement.getElementIndex(offset);
		Element lineElement = rootElement.getElement(line);
		int start = lineElement.getStartOffset();
		int end = lineElement.getEndOffset();
		return content.substring(start, end - 1);
	}

	/**
	 * Override for other languages
	 */
	protected boolean isDelimiter(String character) {
		String operands = ";:{}()[]+-/%<=>!&|^~*.,";

		if (Character.isWhitespace(character.charAt(0)) || operands.indexOf(character) != -1)
			return true;
		return false;
	}

	protected boolean isSubdelimiter(String character) {
		String operands = "+-/%<=>!&|^~*";

		if (Character.isWhitespace(character.charAt(0)) || operands.indexOf(character) != -1)
			return true;
		return false;
	}

	/**
	 * Override for other languages
	 */
	protected boolean isQuoteDelimiter(String character) {
		String quoteDelimiters = "\"'";

		if (quoteDelimiters.indexOf(character) < 0)
			return false;
		return true;
	}

	/**
	 * Override for other languages
	 */
	protected boolean isKeyword(String token) {
		return keywords.contains(token);
	}

	/**
	 * Override for other languages
	 */
	protected String getStartDelimiter() {
		return "/*";
	}

	/**
	 * Override for other languages
	 */
	protected String getEndDelimiter() {
		return "*/";
	}

	/**
	 * Override for other languages
	 */
	protected String getSingleLineDelimiter() {
		return "#"; // Python
	}

	/**
	 * Override for other languages
	 */
	protected String getEscapeString(String quoteDelimiter) {
		return "\\" + quoteDelimiter;
	}

	protected String addMatchingBrace(int offset) throws BadLocationException {
		StringBuffer whiteSpace = new StringBuffer();
		int line = rootElement.getElementIndex(offset);
		int i = rootElement.getElement(line).getStartOffset();

		while (true) {
			String temp = doc.getText(i, 1);
			if (temp.equals(" ") || temp.equals("\t")) {
				whiteSpace.append(temp);
				i++;
			}
			else
				break;
		}

		return "{\n" + whiteSpace.toString() + "\t\n" + whiteSpace.toString() + "}";
	}

}
