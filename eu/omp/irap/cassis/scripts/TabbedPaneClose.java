/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.scripts;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import eu.omp.irap.cassis.scripts.util.ScriptUtil;

@SuppressWarnings("serial")
public class TabbedPaneClose extends JTabbedPane implements ChangeListener {

	private JythonView view;
	private int count;
	private List<String> tabFullTitles;
	private static final Icon regIcon = ScriptUtil.createImageIcon("icons/close_gray.gif");
	private static final Icon overIcon = ScriptUtil.createImageIcon("icons/close_color.gif");


	public TabbedPaneClose() {
		tabFullTitles = new ArrayList<String>();
	}

	public void setParentPanel(JythonView view) {
		ScriptUtil.log("setParentPanel");
		this.view = view;
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		if (getSelectedIndex() == 0) {// Open a new tab
			appendDefaultTab();
		}
		else {
			// Update the main frame's title
			if (view.getFrame() != null) {
				view.getFrame().setTitle(JythonModel.SCRIPT_TITLE + " - " + getSelectedTabFullTitle());
			}
			// Update the toolbar
			JPanel p = (JPanel) getComponentAt(getSelectedIndex());
			JScrollPane scroll = (JScrollPane) p.getComponent(0); // see JythonPanel.createCompleteEditor()
			MyTextEditor myEditor = (MyTextEditor) scroll.getViewport().getView();
			view.updateButtonStatus(myEditor.getButtonStatus());
			// Set the editor's focus
			view.getEditor().requestFocus();
		}
	}

	/**
	 * Override 'insertTab' not 'addTab', coz the later calls the former
	 */
	@Override
	public void insertTab(String title, Icon icon, Component component, String tooltip, int index) {
		super.insertTab(title, icon, component, tooltip, index);
		setTabComponentAt(index, new TabButton(this, title, icon)); // responsible for rendering the title for the specified tab

		String fullTitle = ScriptUtil.getScriptPath() + File.separatorChar + title;
		tabFullTitles.add(index, fullTitle);
	}

	private void insertDefaultTab(int index) {
		count++;
		String title = "new" + count + ".py";
		view.createCompleteEditor();
		insertTab(title, null, view.getTabComponent(index), null, index);
		setSelectedIndex(index); // --> invoke stateChanged()
	}

	public void appendDefaultTab() {
		insertDefaultTab(getTabCount());
	}

	@Override
	public void remove(int index) {
		if (index > 0) {// Ensure to not close the '+' tab
			JPanel p = (JPanel) getComponentAt(index);
			JScrollPane scroll = (JScrollPane) p.getComponent(0); // see JythonPanel.createCompleteEditor()
			Component comp = scroll.getViewport().getView();
			if ((comp instanceof JTextPane)
					&& ((MyTextEditor) comp).save(getTabTitle(index), view, false, false, false, null)) {
				tabFullTitles.remove(index);
				view.removeEditor(index);
				super.remove(index);
			}
		}
	}

	/**
	 * Save a tab and its container
	 */
	public boolean save(int index, boolean append, boolean calledWindowClosing) {
		if (index > 0) {// Ensure to not choose the '+' tab
			JPanel p = (JPanel) getComponentAt(index);
			JScrollPane scroll = (JScrollPane) p.getComponent(0); // see JythonPanel.createCompleteEditor()
			Component com = scroll.getViewport().getView();
			if (com instanceof JTextPane) {
				return ((MyTextEditor) com).save(getTabTitle(index), view, true, calledWindowClosing, append,
						getTabFullTitle(index));
			}
		}
		return true;
	}

	public boolean saveAll(boolean calledWindowClosing) {
		int tabCount = getTabCount();
		int selectedTabIndex = getSelectedIndex();
		boolean append = false;
		for (int i = 1; i < tabCount; i++) {
			setSelectedIndex(i);
			if (!save(i, append, calledWindowClosing)) {// Cancel was chosen
				return false;
			}
			append = true;
		}
		setSelectedIndex(selectedTabIndex);
		return true;
	}

	/**
	 * Get the full script name
	 */
	public String getTabFullTitle(int index) {
		return tabFullTitles.get(index);
	}

	/**
	 * Get the full script name
	 */
	public String getSelectedTabFullTitle() {
		return tabFullTitles.get(getSelectedIndex());
	}

	public void setTabFullTitle(int index, String value) {
		tabFullTitles.set(index, value);
	}

	/**
	 * Set the full script name
	 */
	public void setSelectedTabFullTitle(String value) {
		tabFullTitles.set(getSelectedIndex(), value);
		if (view.getFrame() != null) {
			view.getFrame().setTitle(JythonModel.SCRIPT_TITLE + " - " + getSelectedTabFullTitle());
		}
	}

	/**
	 * Get the short script name
	 */
	public String getTabTitle(int index) {
		return getTitleAt(index);
	}

	/**
	 * Get the short script name
	 */
	public String getSelectedTabTitle() {
		return getTitleAt(getSelectedIndex());
	}

	/**
	 * Set the short script name
	 */
	public void setSelectedTabTitle(String value) {
		setTitleAt(getSelectedIndex(), value);
	}

	/**
	 * TabButton
	 */
	class TabButton extends JPanel implements ActionListener {
		private final JTabbedPane tabbedPane;

		public TabButton(final JTabbedPane tabbedPane, String title, Icon icon) {
			super(new FlowLayout(FlowLayout.LEFT, 0, 0));
			this.tabbedPane = tabbedPane;
			setOpaque(false);

			// Make JLabel read titles from JTabbedPane
			JLabel label = new JLabel() {
				@Override
				public String getText() {
					int i = tabbedPane.indexOfTabComponent(TabButton.this);
					if (i != -1) {
						return tabbedPane.getTitleAt(i);
					}
					return null;
				}
			};
			// Add more space between the label and the button
			label.setBorder(BorderFactory.createEmptyBorder(5, 5, 0, 5));
			if (icon == null) {// Not the 'Add' tab
				add(label);
			}

			Icon pRegIcon = regIcon;
			Icon pOverIcon = overIcon;
			if (icon != null) {
				pRegIcon = pOverIcon = icon;
			}
			JButton button = new JButton(pRegIcon);
			button.setBorder(BorderFactory.createEmptyBorder(5, 5, 0, 5));
			button.addActionListener(this);
			button.setBorderPainted(false);
			button.setContentAreaFilled(false);
			button.setRolloverIcon(pOverIcon);
			button.setPressedIcon(pOverIcon);
			button.setPreferredSize(new Dimension(16, 16));
			add(button);
		}

		@Override
		public void actionPerformed(ActionEvent ae) {
			int index = indexOfTabComponent(this);
			if (index != -1 && ae.getSource() instanceof JButton) {
				if (index == 0) {// Open a new tab
					((TabbedPaneClose) tabbedPane).appendDefaultTab();
				}
				else if (tabbedPane.getTabCount() > 1) {// Close the tab which was clicked
					((TabbedPaneClose) tabbedPane).remove(index); // see also stateChanged(...)
				}
			}
		}
	}

}
