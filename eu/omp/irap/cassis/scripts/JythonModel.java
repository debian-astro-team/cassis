/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.scripts;

import java.util.Properties;

import eu.omp.irap.cassis.scripts.util.ScriptUtil;


public class JythonModel {

	public static final String NEW_LINE = System.getProperty("line.separator");
	public static final String BLANK_LINE = "";
	public static final String osname = System.getProperty("os.name");
	public static final String userhome = System.getProperty("user.home");
	public static final String userdir = System.getProperty("user.dir");
	public static final String SCRIPT_TITLE = "Jython Script";
	public static final String JYTHON_PROMPT = "$$$ ";
	public static final String COMMENT_STRING = "#";
	public static final String TAB_STRING = "    ";
	public static final String LAST_SCRIPTS_FILENAME = "jython_last.ini";
	public static final String METHODS_FILENAME = "jython_methods.txt";
	public static final String VARIABLES_FILENAME = "jython_variables.txt";
	public static final String ATTRIBUTES_FILENAME = "jython_attributes.txt";
	public static final String KEYWORDS_FILENAME = "jython_keywords.txt";
	public static final String CLASSES_FILENAME = "jython_classes.txt";
	public static final String OPTION_FILENAME = "jython_options.txt";
	public static final int DEBUG_MODE = 0, RUN_MODE = 1, CONSOLE_MODE = 2;
	public static final String FONTNAME = (osname != null && osname.equals("Linux") ? "Monospaced" : "Consolas");
	public static final int FONT_SIZE = (FONTNAME.equals("Consolas") ? 14 : 15);
	public static final String FIND_REPLACE_FILENAME = "jython_find.ini";
	private static boolean invisibleChars;
	private static boolean autoCleanVar = true;
	public static final String JYTHON_SETTINGS_FILENAME = "jython.properties";


	public JythonModel() {
		readProperties();
	}

	public static boolean getInvisibleChars() {
		return invisibleChars;
	}

	public static void setInvisibleChars(boolean b) {
		invisibleChars = b;
	}

	public static void setAutoCleanVar(boolean newValue) {
		autoCleanVar = newValue;
	}

	public static boolean getAutoCleanVar() {
		return autoCleanVar;
	}

	private static void readProperties() {

		Properties prop =ScriptUtil.getProperties();
		autoCleanVar = Boolean.parseBoolean(prop.getProperty("clearVar", "true"));
	}
}
