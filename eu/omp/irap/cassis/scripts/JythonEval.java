/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.scripts;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import eu.omp.irap.cassis.scripts.util.ScriptUtil;

public class JythonEval implements Runnable {

	private volatile boolean stopRequested;
	private static ScriptEngineManager manager;
	private static ScriptEngine engine;
	private Thread worker;
	private JTextAreaWriter jythonWriter;
	private String contain;
	private JythonControl control;
	private int type;
	private boolean isAlive;


	public JythonEval(JythonControl control, String contain, int type) {
		this.control = control;
		this.contain = contain;
		initScriptEngine();
		this.type = type;
		isAlive = false;
		worker = new Thread(this);
		worker.setDaemon(true);
		worker.start();
	}

	private void initScriptEngine() {
		if (manager == null) {
			manager = new ScriptEngineManager(Thread.currentThread().getContextClassLoader());
		}
		if (manager != null) {
			if (engine == null) {
				engine = manager.getEngineByName("jython");
			}
			if (engine == null) {
				System.out.println("engine null");
				ScriptUtil.log("jython.jar not found");
				JOptionPane.showMessageDialog(null, "jython.jar not found", JythonModel.SCRIPT_TITLE,
						JOptionPane.ERROR_MESSAGE);
				return;
			}

			jythonWriter = new JTextAreaWriter(control.getView().getConsole());

			engine.getContext().setWriter(jythonWriter);
			engine.getContext().setErrorWriter(jythonWriter);
		}
	}

	@Override
	public void run() {
		if (manager == null || engine == null) {
			return;
		}

		isAlive = true;
		stopRequested = false;

		try (BlockBufferedReader br = new BlockBufferedReader(new BufferedReader(new StringReader(contain)))) {
			// Evaluate the script
			try {
				evalBlock(br);
			} catch (final ScriptException se) {
				SwingUtilities.invokeAndWait(new Runnable() {
					@Override
					public void run() {
						if (!stopRequested) {
							control.getView().println("");
							control.getView().println("@" + se.getMessage());
							control.getView().println("");
							control.getView().printlnSign();
						}
					}
				});
			} catch (InterruptedException e) {
				ScriptUtil.log("JythonEval/run: Aborted script");
				isAlive = false;
			} catch (OutOfMemoryError ome) {
				ScriptUtil.log("JythonEval/run: Out of memory: Java heap space");
				isAlive = false;
				SwingUtilities.invokeAndWait(new Runnable() {
					@Override
					public void run() {
						JOptionPane.showMessageDialog(null, "Out of memory: Java heap space." + JythonModel.NEW_LINE
								+ "Click 'OK' and wait for a moment...", JythonModel.SCRIPT_TITLE,
								JOptionPane.ERROR_MESSAGE);
					}
				});
			} catch (IOException ioe) {
				ScriptUtil.log("JythonEval/run: " + ioe.toString());
				isAlive = false;
			} catch (InvocationTargetException ite) {
				ScriptUtil.log("JythonEval/run: " + ite.getCause().toString());
				isAlive = false;
			}

			// Update 'console'
			SwingUtilities.invokeAndWait(new Runnable() {
				@Override
				public void run() {
					if (!stopRequested) {
						control.setEnableComponents(type, true);
						control.lockConsole(false);
					}
				}
			});
		} catch (InterruptedException ie) {
			// Do nothing.
		} catch (Exception e) {
			e.printStackTrace();
		}
		isAlive = false;
	}

	@SuppressWarnings("deprecation")
	public void stopRequest() {
		stopRequested = true;
		if (worker != null) {
			worker.interrupt();
			worker.stop();
		}
		jythonWriter.flush();
	}

	private void evalBlock(BlockBufferedReader br) throws ScriptException, IOException, InterruptedException, OutOfMemoryError,
			InvocationTargetException {
		if (type == JythonModel.RUN_MODE || type == JythonModel.CONSOLE_MODE) {
			if (type == JythonModel.RUN_MODE && JythonModel.getAutoCleanVar()) {
				// Clean previous variables
				clearVar();
			}

			// Evaluate the entire script
			engine.eval(br);

			// Add the result to 'console'
			SwingUtilities.invokeAndWait(new Runnable() {
				@Override
				public void run() {
					if (!stopRequested) {
						control.getView().printlnSign();
						((MyTextEditor) control.getView().getEditor()).disableLineSelection();
					}
				}
			});
			return;
		}

		else if (type == JythonModel.DEBUG_MODE) {
			int count = ((MyTextEditor) control.getView().getEditor()).getSelectedLine();
			int current = 1;
			String block = null;
			int size = 0;
			boolean blockFound = false;
			boolean execute = true;

			while (!br.isEof() && !blockFound) {
				block = br.getBlock();
				size = br.getBlockCount();

				if (execute && (current == count || count < current+size)) {
					if (block == null || block.trim().equals("")) {
						execute = false;
						current += size;
					} else {
						blockFound = true;
					}
				} else {
					if (!execute && !block.trim().equals("")) {
						((MyTextEditor) control.getView().getEditor()).setIndicator(current -1);
						((MyTextEditor) control.getView().getEditor()).setSelectedLine(current - 1);
						return;
					} else {
						current += size;
					}
				}
			}

			if (!execute) {
				return;
			}

			if (!stopRequested && worker.isAlive()) {
				final int fcount = current - 1;
				final String fblock = block;
				SwingUtilities.invokeAndWait(new Runnable() {
					@Override
					public void run() {
						((MyTextEditor) control.getView().getEditor()).setIndicator(fcount);
						control.getView().print(fblock + JythonModel.NEW_LINE);
					}
				});
			}

			// Evaluate
			engine.eval(block);
			// Calculate position of the arrow
			int nextPos = 0;
			boolean found = false;
			try {
				if (!br.getBlock().trim().equals("") && !block.startsWith("#")) {
					nextPos = current + size - 1;
				} else {
					nextPos = br.getBlockCount() + current + size;
					while (!br.isEof() && !found) {
						block = br.getBlock();
						nextPos += br.getBlockCount();
						if (!block.trim().equals(""))
							found = true;

					}
					nextPos -= br.getBlockCount();
					nextPos--;
				}
			} catch (NullPointerException npe) {
				nextPos = 0;
				control.getView().println("Script completes, return to the beginning.");
			}
			// Add the result to 'console'
			if (!stopRequested && worker.isAlive()) {
				final int flastBlockCount = nextPos;
				SwingUtilities.invokeAndWait(new Runnable() {
					@Override
					public void run() {
						control.getView().printlnSign();
						((MyTextEditor) control.getView().getEditor()).setIndicator(flastBlockCount);
					}
				});
			}
		}
	}

	public boolean isAlive() {
		return isAlive;
	}

	/**
	 * Remove the jython variables if possible and call the java garbage collector.
	 */
	public static void clearVar() {
		if (engine == null) {
			return;
		}

		engine.getBindings(ScriptContext.ENGINE_SCOPE).clear();
		Runtime.getRuntime().gc();
	}
}
