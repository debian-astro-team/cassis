/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.scripts;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

@SuppressWarnings("serial")
public class JythonFrame extends JFrame {

	private static boolean instanced;
	private static JythonFrame jythonInstance;
	private JythonView jythonView;
	private Image iconImage;


	public JythonFrame(Image icon) {
		this.iconImage = icon;
		start();
	}

	/**
	 * Create the GUI and show it. For thread safety, this method should be invoked from the event-dispatching thread.
	 */
	private void createAndShowGUI() {
		// Create and set up the window
		setTitle(JythonModel.SCRIPT_TITLE);
		jythonView = new JythonView();
		jythonView.setParentFrame(this);
		getContentPane().add(jythonView, BorderLayout.CENTER);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		setIconImage(iconImage);

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowActivated(WindowEvent e) {
				super.windowActivated(e);
				jythonView.getEditor().requestFocus();
			}

			@Override
			public void windowClosing(WindowEvent e) {
				if (!jythonView.saveAll(true)) {// Cancel was chosen
					return;
				}
				JythonEval.clearVar();
				super.windowClosing(e);
				instanced = false;
				JythonFrame.this.dispose();
			}
		});

		// Display the window
		pack();
	}

	public void start() {
		// Schedule a job for the event-dispatching thread:
		// creating and showing this application's GUI
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				createAndShowGUI();
			}
		});
	}

	/**
	 * @return the jythonView
	 */
	public final JythonView getJythonView() {
		return jythonView;
	}

	public static JythonFrame getInstance(Image icon) {
		if (!instanced) {
			instanced = true;
			jythonInstance = new JythonFrame(icon);
		}
		return jythonInstance;
	}

	/**
	 * @return the instanced
	 */
	public static final boolean isInstanced() {
		return instanced;
	}

	public static void main(String[] args) {
		JythonFrame instance = getInstance(null);
		instance.setVisible(true);
		instance.toFront();
	}
}
