/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.scripts;

import java.awt.Adjustable;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.TabSet;
import javax.swing.text.TabStop;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;

import eu.omp.irap.cassis.scripts.util.ScriptUtil;

@SuppressWarnings("serial")
public class JythonView extends JPanel {

	private static final int WIDTH = 1024, HEIGHT = 700;
	// Editor
	private List<JTextPane> editors;
	private List<JList<String>> lines;
	private List<JPanel> tabComponents;
	private JTabbedPane tabbedPaneTop;
	private JPopupMenu editorPopup;

	// Console
	private JTextArea console;
	private JScrollPane consoleScroll;
	private JTabbedPane tabbedPaneBottom;
	private JPopupMenu consolePopup;

	// Split pane = editor + console
	private JSplitPane splitPane;

	// Toolbar
	private JButton newButton, loadScriptButton, saveScriptButton,
			saveAsScriptButton, printButton;
	private JButton undoButton, redoButton, incrementalSearchButton,
			findAndReplaceButton, goToLineButton, shiftLeftButton, shiftRightButton;
	private JButton stopButton, runButton, runAllButton, clearVarButton;
	private JTextField incrementalSearchTextField;

	// DB
	private JythonModel model;
	private JythonControl control;

	// Parent JFrame
	private JythonFrame frame;

	private final Icon arrowIcon = ScriptUtil.createImageIcon("icons/runlast16.gif");
	private boolean seeInvisibleCharacters;


	public JythonView() {
		model = new JythonModel();
		initComponents();
		control = new JythonControl(this, model);
	}

	public void setParentFrame(JythonFrame frame) {
		ScriptUtil.log("setParentFrame");
		this.frame = frame;

		((TabbedPaneClose) tabbedPaneTop).setParentPanel(this);

		control.loadLastScripts();
	}

	private void initComponents() {
		setLayout(new BorderLayout());
		Dimension size = new Dimension(WIDTH, HEIGHT);
		setSize(size); // for Java Bean
		setPreferredSize(size); // for Java

		editors = new ArrayList<JTextPane>();
		lines = new ArrayList<JList<String>>();
		tabComponents = new ArrayList<JPanel>();

		// Create a top pane
		createEmptyEditor(); // for '+' tab
		createCompleteEditor(); // for new tab

		// Create a tabbed pane
		tabbedPaneTop = new TabbedPaneClose();
		tabbedPaneTop.addTab("", ScriptUtil.createImageIcon("icons/newtab.gif"), null, "Open a new tab");
		tabbedPaneTop.addTab("new.py", null, getTabComponent(1), null);
		tabbedPaneTop.setSelectedIndex(1);
		tabbedPaneTop.addChangeListener((ChangeListener) tabbedPaneTop);

		// Create a bottom pane
		console = new JTextArea() {
			// Force auto-scroll to bottom
			@Override
			public synchronized void append(String str) {
				if (str != null) {
					Document doc = getDocument();
					if (doc.getLength() + str.length() >= Integer.MAX_VALUE) {
						try {
							doc.remove(0, Integer.MAX_VALUE / 2);
						} catch (BadLocationException e) {
							e.printStackTrace();
						}
					}
					super.append(str);
					select(doc.getLength(), doc.getLength());
				}
			}
		};
		console.setLineWrap(true);
		console.setFont(new Font(JythonModel.FONTNAME, Font.PLAIN, JythonModel.FONT_SIZE));
		console.setText(JythonModel.JYTHON_PROMPT);
		console.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent me) {
				if (me.getButton() == MouseEvent.BUTTON3) {// Right click
					consolePopup.show(me.getComponent(), me.getX(), me.getY());
				}
				else if (me.getButton() == MouseEvent.BUTTON1) {
					if (me.getClickCount() == 2 || me.isControlDown()) {// Double click or Ctrl-click
						control.selectErrorLine();
					}
				}
			}
		});
		console.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				control.keyPressedInConsole(e);
			}

			@Override
			public void keyTyped(KeyEvent e) {
				control.keyTypedInConsole(e);
			}
		});

		((AbstractDocument)console.getDocument()).setDocumentFilter(new DocumentFilter() {

			@Override
			public void replace(FilterBypass fb, int offset, int length,
					String text, AttributeSet attrs)
							throws BadLocationException {
				if (text.split("\r\n|\r|\n", -1).length > 1) {
					super.replace(fb, offset, length, text.split("\r\n|\r|\n", -1)[0], attrs);
					return;
				}
				super.replace(fb, offset, length, text, attrs);
			}
		});

		consoleScroll = new JScrollPane(console, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		consoleScroll.setMinimumSize(new Dimension(WIDTH, 170));
		createEmptyBorder(consoleScroll);

		// Create a tabbed pane
		tabbedPaneBottom = new JTabbedPane();
		tabbedPaneBottom.addTab("Console", ScriptUtil.createImageIcon("icons/console_view.gif"), consoleScroll, null);
		createEmptyBorder(tabbedPaneBottom);

		// Create a split pane with the two scroll panes in it
		splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, tabbedPaneTop, tabbedPaneBottom);
		splitPane.setOneTouchExpandable(true);
		// Make the bottom component to stay the same size and the top component
		// to be flexible when the split pane gets bigger
		splitPane.setResizeWeight(1.0);

		add(splitPane, BorderLayout.CENTER);
		createToolbar();
		createConsolePopup();
		createEditorPopup();
		revalidate();
	}

	private void createEmptyEditor() {
		// Add all to the ArrayLists
		editors.add(new JTextPane());
		lines.add(new JList<String>());
		tabComponents.add(new JPanel());
	}

	protected void createCompleteEditor() {
		// Editor
		SyntaxDocument syntaxDoc = new MySyntaxDocument();
		String configPath = ScriptUtil.getConfigPath() + File.separatorChar;
		((MySyntaxDocument) syntaxDoc).setMethodsList(configPath + JythonModel.METHODS_FILENAME);
		((MySyntaxDocument) syntaxDoc).setVariablesList(configPath + JythonModel.VARIABLES_FILENAME);
		((MySyntaxDocument) syntaxDoc).setAttributesList(configPath + JythonModel.ATTRIBUTES_FILENAME);
		((MySyntaxDocument) syntaxDoc).setKeywordsList(configPath + JythonModel.KEYWORDS_FILENAME);
		((MySyntaxDocument) syntaxDoc).setClassesList(configPath + JythonModel.CLASSES_FILENAME);
		((MySyntaxDocument) syntaxDoc).setColors(configPath + JythonModel.OPTION_FILENAME);

		final JTextPane editor = new MyTextEditor() {
			@Override
			public boolean getScrollableTracksViewportWidth() {// No word wrapped
				return false;
			}
		};
		editor.setFont(new Font(JythonModel.FONTNAME, Font.PLAIN, JythonModel.FONT_SIZE));
		editor.setBackground(Color.white);
		editor.setText("");
		setTabs(editor, 4);
		editor.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent me) {
				if (me.getButton() == MouseEvent.BUTTON3) {// Right-click
					editorPopup.show(me.getComponent(), me.getX(), me.getY());
				}
			}
		});
		editor.setEditorKit(new ExtendedEditorKit(syntaxDoc));
		editor.putClientProperty(ExtendedEditorKit.DISPLAY_INVISIBLE_CHARS, Boolean.TRUE);

		// Editor scroll pane
		final JScrollPane editorScroll = new JScrollPane(editor, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		editorScroll.setMinimumSize(new Dimension(Integer.MAX_VALUE, 170));
		editorScroll.getViewport().setBackground(Color.WHITE); // Remove the silver white background when activating no word wrapped
		createEmptyBorder(editorScroll); // remove the fin line between editor and line number
		editorScroll.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent me) {
				if (me.getButton() == MouseEvent.BUTTON3) {// Right-click
					editorPopup.show(me.getComponent(), me.getX(), me.getY());
				}
				else if (me.getButton() == MouseEvent.BUTTON1) {// Left-click
					editorScroll.getViewport().getView().requestFocus();
				}
			}
		});

		// Line number
		final JList<String> line = new JList<String>(new DefaultListModel<String>() {
			@Override
			public String getElementAt(int index) {
				return String.format("%02d", 1 + index);
			}
		});
		line.setFont(new Font(JythonModel.FONTNAME, Font.PLAIN, JythonModel.FONT_SIZE));
		line.setFixedCellHeight(line.getFontMetrics(line.getFont()).getHeight());
		line.setFixedCellWidth(45);
		line.setForeground(Color.black);
		line.setBackground(new Color(240, 240, 240));
		line.setAlignmentX(CENTER_ALIGNMENT);
		line.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		line.setCellRenderer(new DefaultListCellRenderer() {
			final Color selColor = new Color(200, 200, 255);

			@Override
			public Component getListCellRendererComponent(JList<?> list,
					Object value/* value to display */,
					int index /* cell index */,
					boolean iss /* is the cell selected */,
					boolean chf/* the list and the cell have the focus */) {
				// The DefaultListCellRenderer class will take care of the JLabels text property,
				// it's foreground and background colors, and so on.
				JPanel p = new JPanel(new BorderLayout());
				{
					p.add(new JLabel(value.toString()), BorderLayout.WEST);
					if (iss) {
						p.add(new JLabel(arrowIcon), BorderLayout.EAST);
						p.setBackground(selColor);
						((MyTextEditor) getEditor()).setSelectedLine(index+1);
					}
				}
				p.setOpaque(true);
				return p;
			}
		});
		((DefaultListModel<String>) line.getModel()).setSize(1);

		JScrollPane lineScroll = new JScrollPane(line, ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		lineScroll.getVerticalScrollBar().setModel(editorScroll.getVerticalScrollBar().getModel()); // synchronize the line numbers panel with the current editor
		lineScroll.setBorder(new EmptyBorder(new Insets(4, 0, 0, 0)));

		// Trick: use a horizontal bar replacing the editorscroll's
		JScrollBar hbar = new JScrollBar(Adjustable.HORIZONTAL);
		hbar.setModel(editorScroll.getHorizontalScrollBar().getModel());

		// Tab panel
		JPanel p = new JPanel(new BorderLayout());
		p.add(editorScroll, BorderLayout.CENTER);
		p.add(lineScroll, BorderLayout.WEST);
		p.add(hbar, BorderLayout.SOUTH);
		((MyTextEditor) editor).setLine(line);
		createListenerForEditorDocument(editor);

		// Undo/redo
		createUndoRedo(editor);
		// Keystrokes
		addBinding(editor);

		// Add all to the ArrayLists
		editors.add(editor);
		lines.add(line);
		tabComponents.add(p);
	}

	public void removeEditor(int index) {
		editors.remove(index);
		lines.remove(index);
		tabComponents.remove(index);
	}

	/**
	 * Refresh all MyTextPane editors
	 */
	public void refreshAllEditors() {
		for (int i = 1; i < editors.size(); i++) {
			MyTextEditor pane = (MyTextEditor) editors.get(i);
			MySyntaxDocument syntaxDoc = (MySyntaxDocument) pane.getDocument();
			// Apply changes of colors
			syntaxDoc.setColors(ScriptUtil.getConfigPath() + File.separatorChar
					+ JythonModel.OPTION_FILENAME);
			// Refresh the syntax coloring
			syntaxDoc.refresh();
		}
	}

	/**
	 * Refresh the current selected MyTextPane editor
	 */
	public void refreshCurrentEditor() {
		int i = tabbedPaneTop.getSelectedIndex();
		MyTextEditor pane = (MyTextEditor) editors.get(i);
		MySyntaxDocument syntaxDoc = (MySyntaxDocument) pane.getDocument();
		// Apply changes of colors
		syntaxDoc.setColors(ScriptUtil.getConfigPath() + File.separatorChar
				+ JythonModel.OPTION_FILENAME);
		// Refresh the syntax coloring
		syntaxDoc.refresh();
	}

	/**
	 * Popup inside the console
	 */
	private void createConsolePopup() {
		consolePopup = new JPopupMenu();
		JMenuItem menuItem = new JMenuItem("Clear", ScriptUtil.createImageIcon("icons/close_color.gif"));
		menuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.clearConsole();
			}
		});
		consolePopup.add(menuItem);
	}

	/**
	 * Popup inside the editor
	 */
	private void createEditorPopup() {
		// Popup menu
		editorPopup = new JPopupMenu();

		// Run
		JMenuItem menuItemRun = new JMenuItem("Run", ScriptUtil.createImageIcon("icons/run16.gif"));
		menuItemRun.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.runScript(true);
			}
		});

		// Run All
		JMenuItem menuItemRunAll = new JMenuItem("Run All", ScriptUtil.createImageIcon("icons/runAll16.gif"));
		menuItemRunAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.runScript(false);
			}
		});

		JMenuItem menuItemStop = new JMenuItem("Stop", ScriptUtil.createImageIcon("icons/stop16.gif"));
		menuItemStop.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.stopScript();
			}
		});

		// Cut
		JMenuItem menuItemCut = new JMenuItem("Cut", ScriptUtil.createImageIcon("icons/cut_edit16.gif"));
		menuItemCut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.cutEdit();
			}
		});

		// Copy
		JMenuItem menuItemCopy = new JMenuItem("Copy", ScriptUtil.createImageIcon("icons/copy_edit16.gif"));
		menuItemCopy.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.copyEdit();
			}
		});

		// Paste
		JMenuItem menuItemPaste = new JMenuItem("Paste", ScriptUtil.createImageIcon("icons/paste_edit16.gif"));
		menuItemPaste.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.pasteEdit();
			}
		});

		// Find/Replace
		JMenuItem menuItemFindAndReplace = new JMenuItem("Find/Replace", ScriptUtil.createImageIcon("icons/search16.gif"));
		menuItemFindAndReplace.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.findReplace();
			}
		});

		// Comment
		JMenuItem menuItemComment = new JMenuItem("Toggle Comment", ScriptUtil.createImageIcon("icons/comment16.gif"));
		menuItemComment.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.commentOut();
			}
		});

		// Clear
		JMenuItem menuItemClear = new JMenuItem("Clear", ScriptUtil.createImageIcon("icons/close_color.gif"));
		menuItemClear.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.clearScript();
			}
		});

		// Show/Hide invisible character
		JMenuItem menuItemShowHide = new JMenuItem("Show/hide invisible characters", ScriptUtil.createImageIcon("icons/mark16.png"));
		menuItemShowHide.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				seeInvisibleCharacters = !seeInvisibleCharacters;
				control.displayInvisibleChars(seeInvisibleCharacters);
			}
		});

		// Options
		JMenuItem menuItemOptions = new JMenuItem("Options", ScriptUtil.createImageIcon("icons/preferences16.png"));
		menuItemOptions.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.options();
			}
		});

		// Save All script
		JMenuItem menuItemSaveAll = new JMenuItem("Save All", ScriptUtil.createImageIcon("icons/saveall16.gif"));
		menuItemSaveAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				saveAll(false);
			}
		});

		editorPopup.add(menuItemRun);
		editorPopup.add(menuItemRunAll);
		editorPopup.add(menuItemStop);
		editorPopup.addSeparator();
		editorPopup.add(menuItemCut);
		editorPopup.add(menuItemCopy);
		editorPopup.add(menuItemPaste);
		editorPopup.addSeparator();
		editorPopup.add(menuItemFindAndReplace);
		editorPopup.add(menuItemComment);
		editorPopup.add(menuItemClear);
		editorPopup.add(menuItemShowHide);
		editorPopup.addSeparator();
		editorPopup.add(menuItemOptions);
		editorPopup.add(menuItemSaveAll);
	}

	private void createListenerForEditorDocument(final JTextPane editor) {
		editor.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				setStar();
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				setStar();
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
			}

			private void setStar() {
				int count = editor.getDocument().getDefaultRootElement().getElementCount();
				MyTextEditor myEditor = (MyTextEditor) editor;
				((DefaultListModel<String>) myEditor.getLine().getModel()).setSize(count);

				if (!myEditor.isModified()) {
					myEditor.setModified(true);
					refreshEditorTab();
					return;
				}
			}
		});
	}

	/**
	 * Create an empty border
	 */
	private void createEmptyBorder(JComponent comp) {
		comp.setBorder(BorderFactory.createEmptyBorder());
	}

	/**
	 * Create the toolbar
	 */
	private void createToolbar() {
		// Create the toolbar
		JToolBar toolbar = new JToolBar();
		FlowLayout flowLayout = new FlowLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		toolbar.setLayout(flowLayout);
		toolbar.setOpaque(false);
		toolbar.setBorderPainted(false);
		toolbar.setFloatable(false);
		add(toolbar, BorderLayout.PAGE_START);

		// Buttons
		newButton = new JButton(ScriptUtil.createImageIcon("icons/new16.gif"));
		newButton.setToolTipText("New... (Ctrl+N)");
		setCorrectParametersToButton(newButton);
		newButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				((TabbedPaneClose)tabbedPaneTop).appendDefaultTab();
			}
		});

		loadScriptButton = new JButton(ScriptUtil.createImageIcon("icons/openFile16.gif"));
		loadScriptButton.setToolTipText("Load Script (Ctrl+O)");
		setCorrectParametersToButton(loadScriptButton);
		loadScriptButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.loadScript();
			}
		});

		saveScriptButton = new JButton(ScriptUtil.createImageIcon("icons/save16.gif"));
		saveScriptButton.setToolTipText("Save (Ctrl+S)");
		setCorrectParametersToButton(saveScriptButton);
		saveScriptButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.saveScript(false);
			}
		});

		saveAsScriptButton = new JButton(ScriptUtil.createImageIcon("icons/saveAs16.gif"));
		saveAsScriptButton.setToolTipText("Save As...");
		setCorrectParametersToButton(saveAsScriptButton);
		saveAsScriptButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.saveScript(true);
			}
		});

		printButton = new JButton(ScriptUtil.createImageIcon("icons/print16.gif"));
		printButton.setToolTipText("Print (Ctrl-P)");
		setCorrectParametersToButton(printButton);
		printButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.print();
			}
		});

		undoButton = new JButton(ScriptUtil.createImageIcon("icons/undo16.gif"));
		undoButton.setToolTipText("Undo (Ctrl-Z)");
		setCorrectParametersToButton(undoButton);
		undoButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (((MyTextEditor) getEditor()).canUndo())
					((MyTextEditor) getEditor()).undo();
			}
		});

		redoButton = new JButton(ScriptUtil.createImageIcon("icons/redo16.gif"));
		redoButton.setToolTipText("Redo (Ctrl-Y)");
		setCorrectParametersToButton(redoButton);
		redoButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (((MyTextEditor) getEditor()).canRedo())
					((MyTextEditor) getEditor()).redo();
			}
		});

		incrementalSearchButton = new JButton(ScriptUtil.createImageIcon("icons/incrementalSearch16.gif"));
		incrementalSearchButton.setToolTipText("Incremental Search (Ctrl-I)");
		setCorrectParametersToButton(incrementalSearchButton);
		incrementalSearchButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.incrementalSearch();
			}
		});

		findAndReplaceButton = new JButton(ScriptUtil.createImageIcon("icons/search16.gif"));
		findAndReplaceButton.setToolTipText("Find/Replace... (Ctrl-F)");
		setCorrectParametersToButton(findAndReplaceButton);
		findAndReplaceButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.findReplace();
			}
		});

		goToLineButton = new JButton(ScriptUtil.createImageIcon("icons/goToLine16.gif"));
		goToLineButton.setToolTipText("Go To Line (Ctrl-L)");
		setCorrectParametersToButton(goToLineButton);
		goToLineButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				goToLine();
			}
		});

		shiftLeftButton = new JButton(ScriptUtil.createImageIcon("icons/shiftLeft16.gif"));
		shiftLeftButton.setToolTipText("Shift Left");
		setCorrectParametersToButton(shiftLeftButton);
		shiftLeftButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.shiftTab();
			}
		});

		shiftRightButton = new JButton(ScriptUtil.createImageIcon("icons/shiftRight16.gif"));
		shiftRightButton.setToolTipText("Shift Right");
		setCorrectParametersToButton(shiftRightButton);
		shiftRightButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.tab(true);
			}
		});

		stopButton = new JButton(ScriptUtil.createImageIcon("icons/stop16.gif"));
		stopButton.setToolTipText("Stop");
		setCorrectParametersToButton(stopButton);
		stopButton.setEnabled(false);
		stopButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.stopScript();
			}
		});

		runButton = new JButton(ScriptUtil.createImageIcon("icons/run16.gif"));
		runButton.setToolTipText("Run (F5)");
		setCorrectParametersToButton(runButton);
		runButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.runScript(true);
			}
		});

		runAllButton = new JButton(ScriptUtil.createImageIcon("icons/runAll16.gif"));
		runAllButton.setToolTipText("Run All (Ctrl+F5)");
		setCorrectParametersToButton(runAllButton);
		runAllButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.runScript(false);
			}
		});

		clearVarButton = new JButton(ScriptUtil.createImageIcon("icons/remove.gif"));
		clearVarButton.setToolTipText("Delete all Jython variables (Ctrl+F6)");
		setCorrectParametersToButton(clearVarButton);
		clearVarButton.setEnabled(false);
		clearVarButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JythonEval.clearVar();
			}
		});

		toolbar.add(newButton);
		toolbar.add(loadScriptButton);
		toolbar.add(saveScriptButton);
		toolbar.add(saveAsScriptButton);
		toolbar.add(printButton);
		toolbar.addSeparator();
		toolbar.add(undoButton);
		toolbar.add(redoButton);
		toolbar.add(incrementalSearchButton);
		toolbar.add(findAndReplaceButton);
		toolbar.add(goToLineButton);
		toolbar.add(shiftLeftButton);
		toolbar.add(shiftRightButton);
		toolbar.addSeparator();
		toolbar.add(stopButton);
		toolbar.add(runButton);
		toolbar.add(runAllButton);
		toolbar.addSeparator();
		toolbar.add(clearVarButton);
		toolbar.addSeparator();
		toolbar.addSeparator();
		toolbar.add(getIncrementalSearchTextField());
		getIncrementalSearchTextField().setVisible(false);
	}

	private void setCorrectParametersToButton(JButton button) {
		button.setOpaque(false);
		button.setContentAreaFilled(false);
		button.setBorderPainted(false);
	}

	/**
	 * @return The control
	 */
	public JythonControl getControl() {
		return control;
	}

	/**
	 * Get the selected tab's editor
	 */
	public JTextPane getEditor() {
		return editors.get(tabbedPaneTop.getSelectedIndex());
	}

	public boolean isEditorModified() {
		return ((MyTextEditor) editors.get(tabbedPaneTop.getSelectedIndex())).isModified();
	}

	public void setEditorModified(boolean b) {
		((MyTextEditor) editors.get(tabbedPaneTop.getSelectedIndex())).setModified(b);
	}

	/**
	 * Get the selected tab component
	 */
	public JPanel getTabComponent(int index) {
		return tabComponents.get(index);
	}

	/**
	 * Refresh the selected tab
	 */
	public void refreshEditorTab() {
		int index = tabbedPaneTop.getSelectedIndex();
		if (isEditorModified()) {// Modify the selected tab's title
			String tabTitle = tabbedPaneTop.getTitleAt(index);
			tabbedPaneTop.setTitleAt(index, "*" + tabTitle);
		}
		// Resize the selected tab's size
		JPanel p = (JPanel) tabbedPaneTop.getTabComponentAt(index);
		p.revalidate();
	}

	/**
	 * Implementing Undo and Redo
	 */
	public void createUndoRedo(JTextPane editor) {
		// Create CompoundUndoManager
		if (editor instanceof MyTextEditor)
			((MyTextEditor) editor).setCompoundUndoManager(new CompoundUndoManager(editor));
	}

	/**
	 * Associating Text Actions With Key Strokes
	 */
	private void addBinding(final JTextPane editor) {
		final String CTRLZ = "Ctrl-Z";
		final String CTRLY = "Ctrl-Y";
		final String CTRLF = "Ctrl-F";
		final String CTRLSHIFTC = "Ctrl-Shift-C";
		final String CTRLSLASH = "Ctrl-Slash";
		final String CTRLS = "Ctrl-S";
		final String TAB = "Tab";
		final String SHIFTTAB = "Shift-Tab";
		final String F5 = "F5";
		final String CTRLF5 = "Ctrl-F5";
		final String CTRLF11 = "Ctrl-F11";
		final String CTRLSHIFTS = "Ctrl-Shift-S";
		final String CTRLI = "Ctrl-I";
		final String CTRLP = "Ctrl-P";
		final String CTRLL = "Ctrl-L";
		final String CTRLO = "Ctrl-O";
		final String CTRLN = "Ctrl-N";

		InputMap inputMap = editor.getInputMap();
		ActionMap actionMap = editor.getActionMap();

		// Ctrl-Z
		KeyStroke key = KeyStroke.getKeyStroke(KeyEvent.VK_Z, Event.CTRL_MASK);
		inputMap.put(key, CTRLZ);
		actionMap.put(CTRLZ, new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (((MyTextEditor) editor).canUndo())
						((MyTextEditor) editor).undo();
				} catch (CannotUndoException ex) {
					ScriptUtil.log("Unable to undo: " + ex);
					ex.printStackTrace();
				}
			}
		});

		// Ctrl-Y
		key = KeyStroke.getKeyStroke(KeyEvent.VK_Y, Event.CTRL_MASK);
		inputMap.put(key, CTRLY);
		actionMap.put(CTRLY, new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (((MyTextEditor) editor).canRedo())
						((MyTextEditor) editor).redo();
				} catch (CannotRedoException ex) {
					ScriptUtil.log("Unable to redo: " + ex);
					ex.printStackTrace();
				}
			}
		});

		// Ctrl-F
		key = KeyStroke.getKeyStroke(KeyEvent.VK_F, Event.CTRL_MASK);
		inputMap.put(key, CTRLF);
		actionMap.put(CTRLF, new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.findReplace();
			}
		});

		// Ctrl-Shift-C
		Action commentAction = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.commentOut();
			}
		};
		key = KeyStroke.getKeyStroke(KeyEvent.VK_C, Event.CTRL_MASK | Event.SHIFT_MASK);
		inputMap.put(key, CTRLSHIFTC);
		actionMap.put(CTRLSHIFTC, commentAction);

		// Ctrl-/
		key = KeyStroke.getKeyStroke(KeyEvent.VK_COLON, Event.CTRL_MASK);
		inputMap.put(key, CTRLSLASH);
		actionMap.put(CTRLSLASH, commentAction);

		// Ctrl-S
		key = KeyStroke.getKeyStroke(KeyEvent.VK_S, Event.CTRL_MASK);
		inputMap.put(key, CTRLS);
		actionMap.put(CTRLS, new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.saveScript(false);
			}
		});

		// Ctrl-Shift-S
		key = KeyStroke.getKeyStroke(KeyEvent.VK_S, Event.CTRL_MASK | Event.SHIFT_MASK);
		inputMap.put(key, CTRLSHIFTS);
		actionMap.put(CTRLSHIFTS, new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				saveAll(false);
			}
		});

		// TAB
		key = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0);
		inputMap.put(key, TAB);
		actionMap.put(TAB, new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.tab(false);
			}
		});

		// Shift-TAB
		key = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, Event.SHIFT_MASK);
		inputMap.put(key, SHIFTTAB);
		actionMap.put(SHIFTTAB, new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.shiftTab();
			}
		});

		// F5 ScriptUtil
		key = KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0);
		inputMap.put(key, F5);
		actionMap.put(F5, new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.runScript(true);
			}
		});

		// Ctrl+F5 Run
		key = KeyStroke.getKeyStroke(KeyEvent.VK_F5, Event.CTRL_MASK);
		inputMap.put(key, CTRLF5);
		actionMap.put(CTRLF5, new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.runScript(false);
			}
		});

		// Ctrl+F1 Clear Jython variables
		key = KeyStroke.getKeyStroke(KeyEvent.VK_F11, Event.CTRL_MASK);
		inputMap.put(key, CTRLF11);
		actionMap.put(CTRLF11, new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JythonEval.clearVar();
			}
		});

		// Ctrl+I Incremental Search
		key = KeyStroke.getKeyStroke(KeyEvent.VK_I, Event.CTRL_MASK);
		inputMap.put(key, CTRLI);
		actionMap.put(CTRLI, new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.incrementalSearch();
			}
		});

		// Ctrl+P Print
		key = KeyStroke.getKeyStroke(KeyEvent.VK_P, Event.CTRL_MASK);
		inputMap.put(key, CTRLP);
		actionMap.put(CTRLP, new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.print();
			}
		});

		// Ctrl+L Go To Line
		key = KeyStroke.getKeyStroke(KeyEvent.VK_L, Event.CTRL_MASK);
		inputMap.put(key, CTRLL);
		actionMap.put(CTRLL, new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				goToLine();
			}
		});

		// Ctrl+O Load script
		key = KeyStroke.getKeyStroke(KeyEvent.VK_O, Event.CTRL_MASK);
		inputMap.put(key, CTRLO);
		actionMap.put(CTRLO, new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.loadScript();
			}
		});

		// Ctrl+N Open new tab
		key = KeyStroke.getKeyStroke(KeyEvent.VK_N, Event.CTRL_MASK);
		inputMap.put(key, CTRLN);
		actionMap.put(CTRLN, new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				((TabbedPaneClose)tabbedPaneTop).appendDefaultTab();
			}
		});
	}

	/**
	 * Set tab size, place this function before any add____Listener()
	 */
	private void setTabs(JTextPane textPane, int charactersPerTab) {
		FontMetrics fm = textPane.getFontMetrics(textPane.getFont());
		int charWidth = fm.charWidth('w');
		int tabWidth = charWidth * charactersPerTab;
		TabStop[] tabs = new TabStop[100];
		for (int j = 0; j < tabs.length; j++) {
			int tab = j + 1;
			tabs[j] = new TabStop(tab * tabWidth);
		}

		TabSet tabSet = new TabSet(tabs);
		SimpleAttributeSet attributes = new SimpleAttributeSet();
		StyleConstants.setTabSet(attributes, tabSet);
		int length = textPane.getDocument().getLength();
		textPane.getStyledDocument().setParagraphAttributes(0, length, attributes, false); // trick: 'false' for newline's height
	}

	/**
	 * Save All tabs
	 */
	public boolean saveAll(boolean calledWindowClosing) {
		return control.saveAll(calledWindowClosing);
	}

	public void print(String msg) {
		console.append(msg);
	}

	public void println(String msg) {
		console.append(msg + JythonModel.NEW_LINE);
	}

	public void printlnSign() {
		console.append(JythonModel.NEW_LINE + JythonModel.JYTHON_PROMPT);
	}

	/**
	 * Update the status of the run/runAll/stop/clearVar button.
	 * @param bs
	 */
	public void updateButtonStatus(boolean[] bs) {
		runAllButton.setEnabled(bs[0]);
		runButton.setEnabled(bs[1]);
		stopButton.setEnabled(bs[2]);
		clearVarButton.setEnabled(bs[3]);
	}

	/**
	 * @return the console
	 */
	public final JTextArea getConsole() {
		return console;
	}

	/**
	 * Display a popup for going to a line X.
	 */
	private void goToLine() {
		String value = JOptionPane.showInputDialog(null, "Go to line number...", "Go to line", JOptionPane.QUESTION_MESSAGE);
		if (value != null) {
			try {
				int line = Integer.parseInt(value);
				control.goToLineNumber(line);
			} catch (NumberFormatException nfe) {
				JOptionPane.showMessageDialog(null, "Please provide an integer.", "Go to line error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	/**
	 * @return the {@link JythonFrame}
	 */
	public JythonFrame getFrame() {
		return frame;
	}

	/**
	 * @return the {@link JTextField} incrementalSearchTextField
	 */
	public JTextField getIncrementalSearchTextField() {
		if (incrementalSearchTextField == null) {
			incrementalSearchTextField = new JTextField();
			incrementalSearchTextField.setPreferredSize(new Dimension(130, 24));
			incrementalSearchTextField.setBackground(Color.YELLOW);
		}
		return incrementalSearchTextField;
	}

	public JTabbedPane getTabbedPaneTop() {
		return tabbedPaneTop;
	}
}
