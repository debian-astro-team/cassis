/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.scripts;

import java.awt.Font;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.print.PrinterException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.Document;

import eu.omp.irap.cassis.scripts.util.ScriptUtil;

public class JythonControl {

	private JythonView view;
	private JythonModel model;
	private List<String> cmdHistory;
	private int currCmdHistory;

	private static final int NB_BACKUP_FILE = 10;


	public JythonControl(JythonView view, JythonModel model) {
		this.view = view;
		this.model = model;
		testScriptEngine();
		// Set history parameters
		cmdHistory = new ArrayList<String>();
		currCmdHistory = 0;
	}

	private void testScriptEngine() {
		ScriptEngineManager manager = new ScriptEngineManager(Thread.currentThread().getContextClassLoader());
		if (manager != null) {
			ScriptEngine engine = manager.getEngineByName("jython");
			if (engine == null) {
				ScriptUtil.log("jython.jar not found");
				JOptionPane.showMessageDialog(null, "jython.jar not found", JythonModel.SCRIPT_TITLE,
						JOptionPane.ERROR_MESSAGE);
			}
			else {
				ScriptUtil.log("jython.jar OK");
			}
			engine = null;
		}
		manager = null;
	}

	/**
	 * Load a script from file
	 */
	public void loadScript() {
		try {
			JFileChooser loadFile = ScriptUtil.getJFileChooser();
			loadFile.setFileFilter(new JythonFileFilter());


			int ret = loadFile.showOpenDialog(view);
			if (ret == JFileChooser.APPROVE_OPTION) {
				loadScript(loadFile.getSelectedFile(), true);
			}

			// Set the editor's focus
			view.getEditor().requestFocus();

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public boolean loadScript(String scriptname, boolean openNewTab) {
		return loadScript(new File(scriptname), openNewTab);
	}

	public boolean loadScript(File scriptfile, boolean openNewTab) {
		if (openNewTab) {
			((TabbedPaneClose) (view.getTabbedPaneTop())).appendDefaultTab();
		}

		String currFilePath = scriptfile.getAbsolutePath();
		ScriptUtil.log("loadScript(scriptfile): " + currFilePath);
		if (readScriptData(scriptfile)) {
			((TabbedPaneClose) view.getTabbedPaneTop()).setSelectedTabFullTitle(currFilePath);
			((TabbedPaneClose) view.getTabbedPaneTop()).setSelectedTabTitle(scriptfile.getName());
			refreshSelectedTab();
			resetCompoundUndoManager();
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Load the last scripts if available
	 */
	public void loadLastScripts() {
		try {
			view.getFrame().setVisible(false);
			String[] filenames = getLastScripts();
			if (filenames != null && filenames.length > 0) {
				ScriptUtil.log("loadLastScripts...");
				boolean newtab = false;
				for (String filename : filenames) {
					if (new File(filename).exists()) {
						newtab = loadScript(filename, newtab);
					}
				}
			}

			// Set the editor's focus
			view.getEditor().requestFocus();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			view.getFrame().setVisible(true);
		}
	}

	private boolean readScriptData(File file) {
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			view.getEditor().setText("");
			StringBuilder buffer = new StringBuilder((int) file.length());
			Document doc = view.getEditor().getDocument();
			try {
				String line;
				while ((line = br.readLine()) != null) {
					buffer.append(line);
					buffer.append(JythonModel.NEW_LINE);
				}
				doc.insertString(0, buffer.toString(), null);
			} catch (IOException ioe) {
			} catch (BadLocationException ble) {
			}

			view.getEditor().select(doc.getLength(), doc.getLength()); // force to scroll to bottom
		} catch (FileNotFoundException fnfe) {
			ScriptUtil.log("readScriptData: " + fnfe + " AND not throw exception");
			view.getConsole().append(fnfe.toString() + JythonModel.NEW_LINE); // print exceptions to console
			view.printlnSign();
			return false;
		} catch (IOException e) {
		}
		return true;
	}

	/**
	 * Save a script to file
	 */
	public boolean saveScript(boolean saveAs) {
		int ret = JFileChooser.APPROVE_OPTION;
		try {
			JFileChooser loadFile = ScriptUtil.getJFileSaver();
			loadFile.setFileFilter(new JythonFileFilter());

			// Show it
			String tabTitle = ((TabbedPaneClose) view.getTabbedPaneTop()).getSelectedTabFullTitle();
			loadFile.setSelectedFile(new File(tabTitle));
			if (!(new File(tabTitle)).exists() || saveAs) {
				ret = loadFile.showSaveDialog(view);
			}
			if (ret == JFileChooser.APPROVE_OPTION) {
				File file = loadFile.getSelectedFile();
				String currFilePath = file.getAbsolutePath();
				ScriptUtil.log("saveScript: " + currFilePath);
				writeScriptData(file);
				if (view.getFrame() != null) {
					view.getFrame().setTitle(JythonModel.SCRIPT_TITLE + " - " + currFilePath);
				}
				((TabbedPaneClose) view.getTabbedPaneTop()).setSelectedTabFullTitle(currFilePath);
				((TabbedPaneClose) view.getTabbedPaneTop()).setSelectedTabTitle(file.getName());
				refreshSelectedTab();
			}

			// Set the editor's focus
			view.getEditor().requestFocus();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return (ret == JFileChooser.APPROVE_OPTION);
	}

	private void writeScriptData(File file) {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			Document doc = view.getEditor().getDocument();
			try {
				bw.write(doc.getText(0, doc.getLength()));
			} catch (BadLocationException ble) {
			}
		} catch (IOException ioe) {
			ScriptUtil.log("writeScriptData: " + ioe);
		}
	}

	/**
	 * Read the last script paths if available from file
	 *
	 * @return null if not found or empty file, otherwise the last script paths
	 */
	private String[] getLastScripts() {
		try (BufferedReader br = new BufferedReader(new FileReader(
				ScriptUtil.getConfigPath() + File.separatorChar + JythonModel.LAST_SCRIPTS_FILENAME))) {
			ArrayList<String> lines = new ArrayList<String>();
			String l = null;
			try {
				while ((l = br.readLine()) != null) {
					if (!l.trim().equals("")) {
						lines.add(l);
					}

				}
			} catch (IOException ioe) {
			}

			String[] empty = new String[] {};
			return (lines.size() > 0 ? lines.toArray(empty) : null);
		} catch (FileNotFoundException e) {
			ScriptUtil.log("getLastScripts: " + e);
		} catch (IOException e1) {
		}
		return null;
	}

	/**
	 * Write a script name to the history
	 */
	public void addScriptnameToHistory(String scriptname, boolean append) {
		try (FileWriter writer = new FileWriter(ScriptUtil.getConfigPath()
				+ File.separatorChar + JythonModel.LAST_SCRIPTS_FILENAME,
				append)) {
			writer.write(scriptname);
			writer.write(JythonModel.NEW_LINE);
		} catch (IOException ioe) {
			ScriptUtil.log("addScriptnameToHistory: " + ioe);
		}
	}

	/**
	 * Refresh the selected tab (size)
	 */
	private void refreshSelectedTab() {
		view.setEditorModified(false);
		view.refreshEditorTab();
	}

	/**
	 * Reset CompoundUndoManager
	 */
	private void resetCompoundUndoManager() {
		view.createUndoRedo(view.getEditor());
	}

	void setEnableComponents(int mode, boolean b) {
		boolean[] bs = new boolean[] { b, (b || mode == JythonModel.RUN_MODE) ? b : true, !b, b };
		((MyTextEditor) view.getEditor()).setButtonStatus(bs);
		view.updateButtonStatus(bs);
	}

	void lockConsole(boolean b) {
		view.getConsole().setEditable(!b);
	}

	/**
	 * Run a script
	 */
	public void runScript(boolean debugMode) {
		autoBackupScript();
		((MyTextEditor) view.getEditor()).runScript(this, debugMode);
	}

	/**
	 * Stop a script
	 */
	public void stopScript() {
		((MyTextEditor) view.getEditor()).stopScript(this);
	}

	/**
	 * Run a single line script
	 */
	public void runSingleLine() {
		autoBackupScript();
		((MyTextEditor) view.getEditor()).runSingleLine(this);
	}

	private void autoBackupScript() {
		try {
			String backupFolderPath = ScriptUtil.getScriptPath() + File.separatorChar + "backup";
			File backupFolder = new File(backupFolderPath);

			if (!backupFolder.exists())
				backupFolder.mkdir();

			removeOldBackupFiles();

			Calendar cal = Calendar.getInstance();
			File backupFile = new File(String.format("%s%sscript%02d%02d%02d.py", backupFolder, File.separator,
					cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND)));
			writeScriptData(backupFile);
		} catch (Exception ioe) {
			ScriptUtil.log("autoBackupScript: " + ioe);
		}
	}

	public void clearScript() {
		view.getEditor().setText("");
		view.getEditor().requestFocus();
	}

	public void clearConsole() {
		view.getConsole().setText(JythonModel.JYTHON_PROMPT);
	}

	/**
	 * Prevent to press DEL, BACKSPACE, TAB or ENTER at the prompt, keyPressed() treats the special keys
	 */
	public void keyPressedInConsole(KeyEvent e) {
		JTextArea console = view.getConsole();
		int lineCount = console.getLineCount();
		try {
			// Get the last line
			int beginPos = console.getLineStartOffset(lineCount - 1);

			// Treat keys
			int key = e.getKeyCode();
			if (key == KeyEvent.VK_BACK_SPACE || key == KeyEvent.VK_DELETE
					|| key == KeyEvent.VK_TAB || key == KeyEvent.VK_ENTER) {
				// Get the caret position
				int pos = console.getCaretPosition();
				String selectedText = console.getSelectedText();

				if (pos < beginPos + JythonModel.JYTHON_PROMPT.length()
						|| (pos == beginPos + JythonModel.JYTHON_PROMPT.length()
						&& key == KeyEvent.VK_BACK_SPACE && selectedText == null)) {
					e.consume(); // Kill this event
					return;
				}

				if (selectedText != null) {
					int selectedPos = console.getSelectionStart();
					if (selectedPos < beginPos + JythonModel.JYTHON_PROMPT.length()) {
						e.consume();
						return;
					}
				}

				// Kill 'enter'
				if (key == KeyEvent.VK_ENTER) {
					e.consume();
					runSingleLine();
					return;
				}
			}
			else if (key == KeyEvent.VK_UP) {
				// Get the caret position
				int pos = console.getCaretPosition();
				int endPos = console.getLineEndOffset(lineCount - 1);
				if (pos >= beginPos + JythonModel.JYTHON_PROMPT.length()) {
					e.consume();
					String cmd = getHistoryUp();
					if (cmd != null) {
						console.select(beginPos, endPos);
						console.replaceSelection(cmd);
					}
				}
			}
			else if (key == KeyEvent.VK_DOWN) {
				// Get the caret position
				int pos = console.getCaretPosition();
				int endPos = console.getLineEndOffset(lineCount - 1);
				if (pos >= beginPos + JythonModel.JYTHON_PROMPT.length()) {
					e.consume();
					String cmd = getHistoryDown();
					if (cmd != null) {
						console.select(beginPos, endPos);
						console.replaceSelection(cmd);
					}
				}
			}
		} catch (BadLocationException e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * Prevent to type at the prompt keyTyped(), treats the normal keys
	 */
	public void keyTypedInConsole(KeyEvent e) {
		JTextArea console = view.getConsole();
		int lineCount = console.getLineCount();
		try {
			// Get the last line
			int beginPos = console.getLineStartOffset(lineCount - 1);

			// Treat keys
			int pos = console.getCaretPosition();
			if (pos < beginPos + JythonModel.JYTHON_PROMPT.length()) {
				e.consume(); // Kill this event
				return;
			}

			String selectedText = console.getSelectedText();
			if (selectedText != null) {
				int selectedPos = console.getSelectionStart();
				if (selectedPos < beginPos + JythonModel.JYTHON_PROMPT.length()) {
					e.consume();
				}
			}
		} catch (BadLocationException e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * Perform a built-in action
	 */
	private void performBuiltInAction(String actionName) {
		view.getEditor().getActionMap().get(actionName).actionPerformed(null);
	}

	/**
	 * Comment and uncomment a single line or multi-lines
	 */
	public void commentOut() {
		JTextPane editor = view.getEditor();
		Document doc = editor.getDocument();
		int currPos = editor.getCaretPosition();
		int selStart = editor.getSelectionStart();
		int selEnd = editor.getSelectionEnd();

		try {
			if (selStart == selEnd) // no selected text
			{
				// Perform the built-in 'beginLineAction' action
				performBuiltInAction(DefaultEditorKit.beginLineAction);
				// Get the caret position at there
				int beginPos = editor.getCaretPosition();

				// Perform the built-in 'endLineAction' action
				performBuiltInAction(DefaultEditorKit.endLineAction);
				// Get the caret position at there
				int endPos = editor.getCaretPosition();
				// Reset (trick)
				editor.setCaretPosition(currPos);

				// Get the character/sub-string at the caret position
				int len = JythonModel.COMMENT_STRING.length();
				String ch = doc.getText(beginPos, endPos - beginPos);
				int sharpPos = ch.indexOf(JythonModel.COMMENT_STRING);
				if (ch.trim().startsWith(JythonModel.COMMENT_STRING)) {
					// Remove the comment string
					doc.remove(beginPos, len + sharpPos);
					// Reset the caret
					if (currPos != beginPos + sharpPos) {
						editor.setCaretPosition(currPos - len - sharpPos);
					}
				}
				else {
					// Insert the comment string "#"
					doc.insertString(beginPos, JythonModel.COMMENT_STRING, null);
					// Reset the caret
					editor.setCaretPosition(currPos + len);
				}
			}
			else {
				// Set the caret position to the selection start
				editor.setCaretPosition(selStart);
				// Perform the built-in 'beginLineAction' action
				performBuiltInAction(DefaultEditorKit.beginLineAction);
				// Get the caret position at there
				int beginPos = editor.getCaretPosition();
				int selFirst = beginPos;

				int len = JythonModel.COMMENT_STRING.length();
				while (beginPos < selEnd) {
					// Get the character at the caret position
					String ch = doc.getText(beginPos, len);
					if (ch.equals(JythonModel.COMMENT_STRING)) {
						// Remove the comment string
						doc.remove(beginPos, len);
						selEnd -= len;
					}
					else {
						// Insert the comment string "#"
						doc.insertString(beginPos, JythonModel.COMMENT_STRING, null);
						selEnd += len;
					}

					// Move to next line
					performBuiltInAction(DefaultEditorKit.downAction);
					performBuiltInAction(DefaultEditorKit.beginLineAction);
					// Get the new caret position
					int tmp = editor.getCaretPosition();
					if (tmp == beginPos) { // Last line?
						break;
					}
					beginPos = tmp;
				}

				// Reset the caret
				editor.setCaretPosition(selEnd);
				editor.setSelectionStart(selFirst);
				editor.setSelectionEnd(selEnd);
			}
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Add a {@link JythonModel#TAB_STRING} on the current selected lines.
	 */
	public void tab(boolean fromShiftButton) {
		JTextPane editor = view.getEditor();
		Document doc = editor.getDocument();
		int selStart = editor.getSelectionStart();
		int selEnd = editor.getSelectionEnd();

		try {
			if (selStart == selEnd) {
				if (fromShiftButton)
					performBuiltInAction(DefaultEditorKit.beginLineAction);
				doc.insertString(editor.getCaretPosition(), JythonModel.TAB_STRING, null);
			}
			else {
				// Set the caret position to the selection start
				editor.setCaretPosition(selStart);
				// Perform the built-in 'beginLineAction' action
				performBuiltInAction(DefaultEditorKit.beginLineAction);
				// Get the caret position at there
				int beginPos = editor.getCaretPosition();

				int len = JythonModel.TAB_STRING.length();
				while (beginPos < selEnd) {
					// Insert the TAB string
					doc.insertString(beginPos, JythonModel.TAB_STRING, null);
					selEnd += len;

					// Move to next line
					// Bug with word wrapped
					performBuiltInAction(DefaultEditorKit.downAction);
					performBuiltInAction(DefaultEditorKit.beginLineAction);
					// Get the new caret position
					int tmp = editor.getCaretPosition();
					if (tmp == beginPos) { // Last line?
						break;
					}
					beginPos = tmp;
				}

				// Reset the caret
				editor.setCaretPosition(selEnd);
				editor.setSelectionStart(selStart + len);
				editor.setSelectionEnd(selEnd);
			}
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Remove a {@link JythonModel#TAB_STRING} on the current selected lines if possible.
	 */
	public void shiftTab() {
		JTextPane editor = view.getEditor();
		Document doc = editor.getDocument();
		int currPos = editor.getCaretPosition();
		int selStart = editor.getSelectionStart();
		int selEnd = editor.getSelectionEnd();

		try {
			if (selStart == selEnd) {
				// Perform the built-in 'beginLineAction' action
				performBuiltInAction(DefaultEditorKit.beginLineAction);
				// Get the caret position at there
				int beginPos = editor.getCaretPosition();

				// Perform the built-in 'endLineAction' action
				performBuiltInAction(DefaultEditorKit.endLineAction);
				// Get the caret position at there
				int endPos = editor.getCaretPosition();
				// Reset (trick)
				editor.setCaretPosition(currPos);

				// Get the character/sub-string at the caret position
				int len = JythonModel.TAB_STRING.length();
				String ch = doc.getText(beginPos, endPos - beginPos);
				if (ch.startsWith(JythonModel.TAB_STRING)) {
					// Remove the TAB string
					doc.remove(beginPos, len);
					// Reset the caret
					if (currPos != beginPos) {
						editor.setCaretPosition(currPos - len);
					}
				}
			}
			else {
				// Set the caret position to the selection start
				editor.setCaretPosition(selStart);
				// Perform the built-in 'beginLineAction' action
				performBuiltInAction(DefaultEditorKit.beginLineAction);
				// Get the caret position at there
				int beginPos = editor.getCaretPosition();

				int len = JythonModel.TAB_STRING.length();
				int lines = 0;
				while (beginPos < selEnd) {
					lines++;
					// Get the character at the caret position
					String ch = doc.getText(beginPos, len);
					if (ch.equals(JythonModel.TAB_STRING)) {
						// Remove the TAB string
						doc.remove(beginPos, len);
						selEnd -= len;
						if (lines == 1) {
							selStart -= len;
						}
					}

					// Move to next line
					performBuiltInAction(DefaultEditorKit.downAction);
					performBuiltInAction(DefaultEditorKit.beginLineAction);
					// Get the new caret position
					int tmp = editor.getCaretPosition();
					if (tmp == beginPos) { // Last line?
						break;
					}
					beginPos = tmp;
				}

				// Reset the caret
				editor.setCaretPosition(selEnd);
				editor.setSelectionStart(selStart);
				editor.setSelectionEnd(selEnd);
			}
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Cut, copy, paste edit
	 */
	public void cutEdit() {
		performBuiltInAction(DefaultEditorKit.cutAction);
	}

	public void copyEdit() {
		performBuiltInAction(DefaultEditorKit.copyAction);
	}

	public void pasteEdit() {
		performBuiltInAction(DefaultEditorKit.pasteAction);
	}

	/**
	 * Get the previous command script
	 */
	private String getHistoryUp() {
		if (currCmdHistory > 0) {
			currCmdHistory--;
			// Set the last line
			return (JythonModel.JYTHON_PROMPT + cmdHistory.get(currCmdHistory));
		}
		else if (cmdHistory.size() > 0) {
			return (JythonModel.JYTHON_PROMPT + cmdHistory.get(currCmdHistory));
		}
		return null;
	}

	/**
	 * Get the next command script
	 */
	private String getHistoryDown() {
		if (currCmdHistory < cmdHistory.size() - 1) {
			currCmdHistory++;
			// Set the last line
			return (JythonModel.JYTHON_PROMPT + cmdHistory.get(currCmdHistory));
		}
		else if (currCmdHistory == cmdHistory.size() - 1 && cmdHistory.size() > 0) {
			currCmdHistory++;
			return (JythonModel.JYTHON_PROMPT);
		}
		return null;
	}

	public void selectErrorLine() {
		JTextArea console = view.getConsole();
		int currPos = console.getCaretPosition();

		// Perform the built-in 'beginLineAction' action
		console.getActionMap().get(DefaultEditorKit.beginLineAction).actionPerformed(null);
		// Get the caret position at there
		int beginPos = console.getCaretPosition();

		// Perform the built-in 'endLineAction' action
		console.getActionMap().get(DefaultEditorKit.endLineAction).actionPerformed(null);
		// Get the caret position at there
		int endPos = console.getCaretPosition();
		// Reset
		console.setCaretPosition(currPos);

		try {
			// Get line
			String line = console.getText(beginPos, endPos - beginPos);
			// Find the error number's beginning position
			String atLineNumber = "at line number ";
			int i = line.indexOf(atLineNumber);
			if (i > 0) {
				i += atLineNumber.length();
				// Find the error number's end position
				String atColumnNumber = "at column number";
				int j = line.indexOf(atColumnNumber, i);
				j = (j > 0 ? j - 1 : line.length());
				// Get the number
				String num = line.substring(i, j);
				int x = Integer.parseInt(num);
				// Go to #x
				goToLineNumber(x);
			}
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}

	public void goToLineNumber(int x) {
		JTextPane editor = view.getEditor();
		Document doc = editor.getDocument();
		try {
			String contain = doc.getText(0, doc.getLength());
			int pos = 0;
			for (int i = 0; i < x - 1; i++) {
				int index = contain.indexOf(JythonModel.NEW_LINE, pos);
				if (index >= 0) {
					pos = index + 1;
				}
				else {
					break;
				}
			}
			// Move to line x
			editor.setCaretPosition(pos);
			editor.scrollRectToVisible(editor.modelToView(pos));

			int endLinePos = contain.indexOf(JythonModel.NEW_LINE, pos);
			endLinePos = (endLinePos >= 0 ? endLinePos : contain.length());
			editor.select(pos, endLinePos);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		editor.requestFocus();
	}

	/**
	 * Save All tabs
	 */
	public boolean saveAll(boolean calledWindowClosing) {
		return ((TabbedPaneClose) view.getTabbedPaneTop()).saveAll(calledWindowClosing);
	}

	/**
	 * Options
	 */
	public void options() {
		new JythonOptions(this);
	}

	/**
	 * Find/Replace
	 */
	public void findReplace() {
		performBuiltInAction(DefaultEditorKit.copyAction);
		JythonFindReplace.createInstance(this);
	}

	/**
	 * Show/Hide invisible characters
	 */
	public void displayInvisibleChars(boolean b) {
		JythonModel.setInvisibleChars(b);
		view.refreshCurrentEditor();
	}

	/**
	 * @return the view
	 */
	public final JythonView getView() {
		return view;
	}

	/**
	 * @return cmdHistory
	 */
	public List<String> getCmdHistory() {
		return cmdHistory;
	}

	/**
	 * @return the model !
	 */
	public JythonModel getModel() {
		return model;
	}

	/**
	 * Display an incremental search operation tool
	 */
	public void incrementalSearch() {
		final JTextField textField = view.getIncrementalSearchTextField();
		final IncrementalSearchOperation iso = new IncrementalSearchOperation(view.getEditor(), textField);
		textField.setVisible(true);
		view.revalidate();
		textField.requestFocus();
		textField.getDocument().addDocumentListener(iso);
		textField.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {}

			@Override
			public void keyReleased(KeyEvent e) {}

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					iso.continueSearch();
				}
				else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					textField.setVisible(false);
					textField.setText("");
					textField.getDocument().removeDocumentListener(iso);
					for (KeyListener aKeyListener : textField.getKeyListeners()) {
						textField.removeKeyListener(aKeyListener);
					}
					for (FocusListener aFocusListener : textField.getFocusListeners()) {
						textField.removeFocusListener(aFocusListener);
					}
				}

			}
		});

		textField.addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {
				textField.setVisible(false);
				textField.setText("");
				textField.getDocument().removeDocumentListener(iso);
				for (KeyListener aKeyListener : textField.getKeyListeners()) {
					textField.removeKeyListener(aKeyListener);
				}
				for (FocusListener aFocusListener : textField.getFocusListeners()) {
					textField.removeFocusListener(aFocusListener);
				}
			}

			@Override
			public void focusGained(FocusEvent e) {}
		});
	}

	public void print() {
		JTextPane textPane = view.getEditor();
		Font oldFont = textPane.getFont();
		textPane.setFont(new Font(oldFont.getName(), oldFont.getStyle(), 1));
		textPane.revalidate();
		textPane.repaint();
		try {
			textPane.print(null, null, true, null, null, true);
		} catch (PrinterException e) {
			JOptionPane.showMessageDialog(null, "Printer error", "Printer error", JOptionPane.ERROR_MESSAGE);
		} finally {
			textPane.setFont(oldFont);
		}
	}

	private void removeOldBackupFiles() {
		File[] listFiles = new File(ScriptUtil.getScriptPath()
				+ File.separatorChar + "backup").listFiles();
		int nbFile = listFiles.length;
		if (nbFile >= NB_BACKUP_FILE) {
			Arrays.sort(listFiles, new Comparator<File>() {
				public int compare(File f1, File f2) {
			    	return Long.valueOf(f2.lastModified()).compareTo(f1.lastModified());
			    }
			});

			for (int i = NB_BACKUP_FILE - 1; i < nbFile; i++) {
				listFiles[i].delete();
			}
		}
	}

	public void updateCurrCmdHistory() {
		this.currCmdHistory = getCmdHistory().size();
	}
}
