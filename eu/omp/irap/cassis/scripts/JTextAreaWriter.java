/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.scripts;

import java.io.Writer;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;


public class JTextAreaWriter extends Writer {

	private JTextArea textArea;
	private Vector<String> buffer = new Vector<String>();
	private static final int LINES = 1000;
	private static final int LINES_BEFORE_CLEAN = 1200;


	public JTextAreaWriter(final JTextArea textArea) {
		this.textArea = textArea;
	}

	@Override
	public void write(final int i) {
		write(String.valueOf(i));
	}

	@Override
	public void write(final char[] buffer) {
		write(new String(buffer));
	}

	@Override
	public void write(final char[] buffer, final int off, final int len) {
		write(new String(buffer, off, len));
	}

	@Override
	public void write(final String string) {
		buffer.add(string);
	}

	@Override
	public void flush() {
		// Get the lines from buffer then clean it.
		ArrayList<String> linesList;
		synchronized (buffer) {
			if (buffer.isEmpty())
				return;
			linesList = new ArrayList<String>(buffer);
			buffer.clear();
		}

		// Create a new String with StringBuilder.
		final StringBuilder sb = new StringBuilder();
		for (final String line : linesList) {
			sb.append(line);
		}

		// Clean the JTextArea if needed and add the lines to it.
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				final int linesNb = textArea.getLineCount();
				if (linesNb > LINES_BEFORE_CLEAN) {
					try {
						textArea.replaceRange("", 0, textArea.getLineEndOffset(linesNb - LINES));
					} catch (BadLocationException ble) {
					}
				}
				textArea.append(sb.toString());
			}
		});
	}

	@Override
	public void close() {
		flush();
	}

}
