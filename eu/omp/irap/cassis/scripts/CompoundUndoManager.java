/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.scripts;

import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.JTextComponent;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.CompoundEdit;
import javax.swing.undo.UndoManager;
import javax.swing.undo.UndoableEdit;

/**
 ** This class will merge individual edits into a single larger edit. That is, characters entered sequentially will be grouped together and undone as a group. Any attribute changes
 * will be considered as part of the group and will therefore be undone when the group is undone.
 */

@SuppressWarnings("serial")
public class CompoundUndoManager extends UndoManager implements UndoableEditListener, DocumentListener {
	public CompoundEdit compoundEdit;
	private JTextComponent editor;

	// These fields are used to help determine whether the edit is an
	// incremental edit. For each character added the offset and length
	// should increase by 1 or decrease by 1 for each character removed.
	private int lastOffset;
	private int lastLength;


	public CompoundUndoManager(JTextComponent editor) {
		this.editor = editor;
		editor.getDocument().addUndoableEditListener(this);
	}

	/**
	 ** Add a DocumentLister before the undo is done so we can position the Caret correctly as each edit is undone.
	 */
	@Override
	public void undo() {
		editor.getDocument().addDocumentListener(this);
		super.undo();
		editor.getDocument().removeDocumentListener(this);
	}

	/**
	 ** Add a DocumentLister before the redo is done so we can position the Caret correctly as each edit is redone.
	 */
	@Override
	public void redo() {
		editor.getDocument().addDocumentListener(this);
		super.redo();
		editor.getDocument().removeDocumentListener(this);
	}

	/**
	 ** Whenever an UndoableEdit happens the edit will either be absorbed by the current compound edit or a new compound edit will be started
	 */
	@Override
	public void undoableEditHappened(UndoableEditEvent e) {
		// Start a new compound edit
		if (compoundEdit == null) {
			compoundEdit = startCompoundEdit(e.getEdit());
			lastLength = editor.getDocument().getLength();
			return;
		}

		// Check for an attribute change
		if (e.getEdit() instanceof AbstractDocument.DefaultDocumentEvent ) {
			AbstractDocument.DefaultDocumentEvent event = (AbstractDocument.DefaultDocumentEvent) e.getEdit();
			if (event.getType().equals(DocumentEvent.EventType.CHANGE)) {
				compoundEdit.addEdit(e.getEdit());
				return;
			}
		}

		// Check for an incremental edit or backspace.
		// The change in Caret position and Document length should be either
		// 1 or -1 .
		int offsetChange = editor.getCaretPosition() - lastOffset;
		int lengthChange = editor.getDocument().getLength() - lastLength;
		if (Math.abs(offsetChange) == 1 && Math.abs(lengthChange) == 1) {
			compoundEdit.addEdit(e.getEdit());
			lastOffset = editor.getCaretPosition();
			lastLength = editor.getDocument().getLength();
			return;
		}

		// Not incremental edit, end previous edit and start a new one
		compoundEdit.end();
		compoundEdit = startCompoundEdit(e.getEdit());
	}

	/**
	 ** Each CompoundEdit will store a group of related incremental edits (ie. each character typed or backspaced is an incremental edit)
	 */
	private CompoundEdit startCompoundEdit(UndoableEdit anEdit) {
		// Track Caret and Document information of this compound edit
		lastOffset = editor.getCaretPosition();
		lastLength = editor.getDocument().getLength();
		// The compound edit is used to store incremental edits
		compoundEdit = new MyCompoundEdit();
		compoundEdit.addEdit(anEdit);
		// The compound edit is added to the UndoManager. All incremental
		// edits stored in the compound edit will be undone/redone at once
		addEdit(compoundEdit);
		return compoundEdit;
	}

	/**
	 * Implement DocumentListener Updates to the Document as a result of Undo/Redo will cause the Caret to be repositioned
	 */
	@Override
	public void insertUpdate(final DocumentEvent e) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				int offset = e.getOffset() + e.getLength();
				offset = Math.min(offset, editor.getDocument().getLength());
				editor.setCaretPosition(offset);
			}
		});
	}

	@Override
	public void removeUpdate(DocumentEvent e) {
		editor.setCaretPosition(e.getOffset());
	}

	@Override
	public void changedUpdate(DocumentEvent e) {
	}

	class MyCompoundEdit extends CompoundEdit {
		@Override
		public boolean isInProgress() {
			// in order for the canUndo() and canRedo() methods to work
			// assume that the compound edit is never in progress
			return false;
		}

		@Override
		public void undo() throws CannotUndoException {
			// End the edit so future edits don't get absorbed by this edit
			if (compoundEdit != null)
				compoundEdit.end();
			super.undo();
			// Always start a new compound edit after an undo
			compoundEdit = null;
		}
	}
}
