/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.scripts;

import java.awt.Container;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Shape;

import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.LabelView;
import javax.swing.text.Position;
import javax.swing.text.Segment;
import javax.swing.text.StyledEditorKit;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;

@SuppressWarnings("serial")
public class ExtendedEditorKit extends StyledEditorKit {

	public static final String DISPLAY_INVISIBLE_CHARS = "invisible chars";
	protected static ViewFactory defaultFactory;
	private Document doc;


	public ExtendedEditorKit(Document doc) {
		this.doc = doc;
	}

	@Override
	public ViewFactory getViewFactory() {
		if (defaultFactory == null) {
			defaultFactory = new ExtendedViewFactory(super.getViewFactory());
		}
		return defaultFactory;
	}

	@Override
	public Document createDefaultDocument() {
		return doc;
	}

	public static class ExtendedViewFactory implements ViewFactory {
		private ViewFactory parentViewFactory;

		public ExtendedViewFactory(ViewFactory parentViewFactory) {
			super();
			this.parentViewFactory = parentViewFactory;
		}

		@Override
		public View create(Element elem) {
			if (AbstractDocument.ContentElementName.equals(elem.getName())) {
				return new ExtendedLabelView(elem);
			}
			return parentViewFactory.create(elem);
		}
	}

	public static class ExtendedLabelView extends LabelView {
		public ExtendedLabelView(Element elem) {
			super(elem);
		}

		/**
		 * Fetch a reference to the text that occupies the given range.
		 * This is normally used by the GlyphPainter to determine what characters
		 * it should render glyphs for.
		 *
		 * @param p0  the starting document offset >= 0
		 * @param p1  the ending document offset >= p0
		 * @return    the <code>Segment</code> containing the text
		 */
		@Override
		public Segment getText(int p0, int p1) {
			Segment text = super.getText(p0, p1);
			Container c = this.getContainer();
			if (c instanceof JTextComponent) {
				if (JythonModel.getInvisibleChars()) {
					char array[] = new char[text.array.length];
					System.arraycopy(text.array, 0, array, 0, text.array.length);
					for (int i = 0; i < text.array.length; i++) {
						switch (text.array[i]) {
							case ' ':
								array[i] = '\u00B7';
								break;
							case '\u00A0':
								array[i] = '\u00B0';
								break;
							case '\n':
								array[i] = '\u00B6';
								break;
						}
					}
					text.array = array;
				}
			}
			return text;
		}

		@Override
		public void paint(Graphics g, Shape a) {
			super.paint(g, a);
			boolean isShowParagraphs = JythonModel.getInvisibleChars();
			if (isShowParagraphs) {
				try {
					Rectangle r = a instanceof Rectangle ? (Rectangle) a : a.getBounds();
					String labelStr = getDocument().getText(getStartOffset(), getEndOffset() - getStartOffset());
					int x0 = modelToView(getStartOffset(), new Rectangle(r.width, r.height), Position.Bias.Forward)
							.getBounds().x;
					for (int i = 0; i < labelStr.length(); i++) {
						int x = modelToView(i + getStartOffset(), new Rectangle(r.width, r.height),
								Position.Bias.Forward).getBounds().x
								- x0;
						String s = labelStr.charAt(i) == '\t' ? "\u00BB" : null;
						if (s != null) {
							g.setFont(getFont());
							int w = g.getFontMetrics().stringWidth(s);
							Rectangle clip = new Rectangle(r.x + x, r.y, 2 * w, r.height);
							Shape oldClip = g.getClip();
							g.setClip(clip);
							g.drawString(s, r.x + x, r.y + g.getFontMetrics().getMaxAscent());
							g.setClip(oldClip);
						}
					}
				} catch (BadLocationException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
