/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.scripts;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;

import eu.omp.irap.cassis.scripts.util.ScriptUtil;

@SuppressWarnings("serial")
public class JythonFindReplace extends JDialog {

	private JythonControl control;
	private JComboBox<String> findCB;
	private JComboBox<String> replaceCB;
	private JCheckBox caseSensitive;
	private JCheckBox wholeWord;
	private JCheckBox wrapSearch;
	private JCheckBox forward;
	private JButton findButton;
	private JButton replaceButton;
	private JButton replaceFindButton;
	private JButton replaceAllButton;
	private JLabel info;
	private static boolean created;
	private static JythonFindReplace jfr;
	 private static Logger logger =
			 Logger.getLogger(JythonFindReplace.class.getName());


	public JythonFindReplace(JythonControl control) {
		this.control = control;
		initComponents();
		start();
	}

	private void initComponents() {
		Container container = getContentPane();

		// Top panel
		GridLayout gl = new GridLayout(2, 1);
		gl.setVgap(5);
		JPanel top = new JPanel(gl);
		{
			Box hbox1 = new Box(BoxLayout.X_AXIS);
			{
				JLabel l = new JLabel("Find:");
				l.setPreferredSize(new Dimension(100, 32));
				hbox1.add(l);

				findCB = new JComboBox<>();
				findCB.setPreferredSize(new Dimension(400, 32));
				findCB.setEditable(true);
				((JTextComponent) findCB.getEditor().getEditorComponent()).getDocument().addDocumentListener(
						new DocumentListener() {
							@Override
							public void removeUpdate(DocumentEvent e) {
								checkInputs();
							}

							@Override
							public void insertUpdate(DocumentEvent e) {
								checkInputs();
							}

							@Override
							public void changedUpdate(DocumentEvent e) {
								checkInputs();
							}
						});
				findCB.getEditor().addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						find();
					}
				});
				hbox1.add(findCB);
			}
			top.add(hbox1);

			Box hbox2 = new Box(BoxLayout.X_AXIS);
			{
				JLabel l = new JLabel("Replace with:");
				l.setPreferredSize(new Dimension(100, 32));
				hbox2.add(l);

				replaceCB = new JComboBox<>();
				replaceCB.setPreferredSize(new Dimension(400, 32));
				replaceCB.setEditable(true);
				hbox2.add(replaceCB);
			}
			top.add(hbox2);
			top.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		}

		caseSensitive = new JCheckBox("Case sensitive", true);
		caseSensitive.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				saveDialog();
			}
		});
		wholeWord = new JCheckBox("Whole word");
		wholeWord.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				saveDialog();
			}
		});
		wrapSearch = new JCheckBox("Wrap search", true);
		wrapSearch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				saveDialog();
			}
		});
		forward = new JCheckBox("Forward search", true);
		forward.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				saveDialog();
			}
		});

		Box vbox = new Box(BoxLayout.Y_AXIS);
		{
			vbox.add(Box.createVerticalStrut(10));
			vbox.add(caseSensitive);
			vbox.add(wholeWord);
			vbox.add(wrapSearch);
			vbox.add(forward);
			vbox.add(Box.createGlue());
			vbox.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		}

		findButton = new JButton("Find");
		findButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				find();
			}
		});

		replaceButton = new JButton("Replace");
		replaceButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				replace();
			}
		});

		replaceFindButton = new JButton("Replace/Find");
		replaceFindButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				replaceAndFind();
			}
		});

		replaceAllButton = new JButton("Replace All");
		replaceAllButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				replaceAll();
			}
		});

		JButton close = new JButton("Close");
		close.setPreferredSize(new Dimension(100, 24));
		close.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				exit();
			}
		});

		Box bottom = new Box(BoxLayout.X_AXIS);
		{
			info = new JLabel("");
			bottom.add(info);
			bottom.add(Box.createGlue());
			bottom.add(findButton);
			bottom.add(replaceButton);
			bottom.add(replaceFindButton);
			bottom.add(replaceAllButton);
			bottom.add(close);
			bottom.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		}

		// Assembly
		container.setLayout(new BorderLayout());
		container.add(top, BorderLayout.NORTH);
		container.add(vbox, BorderLayout.CENTER);
		container.add(bottom, BorderLayout.SOUTH);
	}

	private void saveDialog() {
		try (FileWriter writer = new FileWriter(
				ScriptUtil.getConfigPath() + File.separatorChar
				+ JythonModel.FIND_REPLACE_FILENAME)) {
			writer.write(caseSensitive.isSelected() ? '1' : '0');
			writer.write(' ');

			writer.write(wholeWord.isSelected() ? '1' : '0');
			writer.write(' ');

			writer.write(wrapSearch.isSelected() ? '1' : '0');
			writer.write(' ');

			writer.write(forward.isSelected() ? '1' : '0');
			writer.write(JythonModel.NEW_LINE);
		} catch (IOException ioe) {
			logger.warning("saveDialog: " + ioe);
		}
	}

	private void loadDialog() {
		try (BufferedReader br = new BufferedReader(new FileReader(
				ScriptUtil.getConfigPath() + File.separatorChar
				+ JythonModel.FIND_REPLACE_FILENAME))) {
			String line = null;
			try {
				line = br.readLine();
				if (line != null){
					String[] items = line.split("[ ]");
					if (items != null && items.length == 4) {
						caseSensitive.setSelected(items[0].equals("1"));
						wholeWord.setSelected(items[1].equals("1"));
						wrapSearch.setSelected(items[2].equals("1"));
						forward.setSelected(items[3].equals("1"));
					}
				}
			} catch (IOException ioe) {}
		} catch (FileNotFoundException e) {
			logger.warning("loadDialog: " + e);
		} catch (IOException e1) {
		}
	}

	private void exit() {
		created = false;
		saveDialog();
		dispose();
	}

	private void checkInputs() {
		if (findCB.getEditor().getItem().equals("")) {
			setEnabledButtons(false, false, false, false);
		}
		else {
			setEnabledButtons(true, true, true, true);
		}
	}

	private void setEnabledButtons(boolean bFind, boolean bReplace, boolean bReplaceFind, boolean bReplaceAll) {
		findButton.setEnabled(bFind);
		replaceButton.setEnabled(bReplace);
		replaceFindButton.setEnabled(bReplaceFind);
		replaceAllButton.setEnabled(bReplaceAll);
	}

	private void find() {
		// Get the search text
		String searchText = findCB.getEditor().getItem().toString();
		info.setText("");

		// Get the content of the current editor
		JTextPane editor = control.getView().getEditor();
		Document doc = editor.getDocument();
		int currPos = editor.getCaretPosition();
		try {
			String text = doc.getText(0, doc.getLength());

			if (forward.isSelected()) {
				int index = smartSearch(text, searchText, currPos, caseSensitive.isSelected(), wholeWord.isSelected());
				if (index >= 0) { // found
					editor.setSelectionStart(index);
					editor.setSelectionEnd(index + searchText.length());
				}
				else {
					info.setText("String Not Found");

					if (wrapSearch.isSelected()) { // wrapped search
						index = smartSearch(text, searchText, 0, caseSensitive.isSelected(), wholeWord.isSelected());
						if (index >= 0) { // found
							editor.setSelectionStart(index);
							editor.setSelectionEnd(index + searchText.length());
							info.setText("Wrapped search");
						}
					}
				}
			}
			else {
				int index = smartBackwardSearch(text, searchText, currPos, caseSensitive.isSelected(),
						wholeWord.isSelected());
				if (index >= 0) { // found
					editor.setCaretPosition(index + searchText.length());
					editor.moveCaretPosition(index);
				}
				else {
					info.setText("String Not Found");

					if (wrapSearch.isSelected()) { // wrapped search
						index = smartBackwardSearch(text, searchText, text.length(), caseSensitive.isSelected(),
								wholeWord.isSelected());
						if (index >= 0) { // found
							editor.setCaretPosition(index + searchText.length());
							editor.moveCaretPosition(index);
							info.setText("Wrapped search");
						}
					}
				}
			}
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}

	private static int smartSearch(String text, String substr, int fromIndex, boolean bCaseSensitive, boolean bWholeWord) {
		String patternString = bCaseSensitive ? "" : "(?i)";
		if (bWholeWord) {
			patternString += "\\b(" + Pattern.quote(substr) + ")\\b";
		}
		else {
			patternString += "(" + Pattern.quote(substr) + ")";
		}

		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(text);
		if (matcher.find(fromIndex)) { // found
			return matcher.start();
		}
		return -1;
	}

	private static int smartBackwardSearch(String text, String substr, int fromIndex, boolean bCaseSensitive,
			boolean bWholeWord) {
		String patternString = bCaseSensitive ? "" : "(?i)";
		if (bWholeWord) {
			patternString += "\\b(" + substr + ")\\b";
		}
		else {
			patternString += "(" + substr + ")";
		}

		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(text);

		int storedMatch = -1;
		while (matcher.find()) {
			if (matcher.start() < fromIndex) {
				storedMatch = matcher.start();
			}
			else {
				break;
			}
		}
		return storedMatch;
	}

	private void replace() {
		JTextPane editor = control.getView().getEditor();
		String replaceText = replaceCB.getEditor().getItem().toString();
		int index = editor.getSelectionStart();
		editor.replaceSelection(replaceText);

		if (forward.isSelected()) {
			editor.setSelectionStart(index);
			editor.setSelectionEnd(index + replaceText.length());
		}
		else {
			editor.setCaretPosition(index + replaceText.length());
			editor.moveCaretPosition(index);
		}
	}

	private void replaceAndFind() {
		replace();
		find();
	}

	private void replaceAll() {
		// Get the search and replace texts
		String searchText = findCB.getEditor().getItem().toString();
		String replaceText = replaceCB.getEditor().getItem().toString();
		info.setText("");

		// Get the content of the current editor
		JTextPane editor = control.getView().getEditor();
		Document doc = editor.getDocument();
		// Backup the current caret position
		int currPos = editor.getCaretPosition();
		try {
			StringBuilder text = new StringBuilder(doc.getText(0, doc.getLength()));

			int index = smartSearch(text.toString(), searchText, 0, caseSensitive.isSelected(), wholeWord.isSelected());
			if (index >= 0) { // found
				do {
					// Select the text found
					editor.setSelectionStart(index);
					editor.setSelectionEnd(index + searchText.length());
					// Replace (editor)
					replace();
					// Replace (buffer)
					text.replace(index, index + searchText.length(), replaceText);
					// Find next
					index = smartSearch(text.toString(), searchText, editor.getCaretPosition(),
							caseSensitive.isSelected(), wholeWord.isSelected());
				} while (index >= 0);

				// Restore the caret position
				editor.setCaretPosition(currPos);
			}
			else {
				info.setText("String Not Found");
			}
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the GUI and show it. For thread safety, this method should be invoked from the event-dispatching thread.
	 */
	private void createAndShowGUI() {
		// Create and set up the window
		setTitle("Find/Replace");
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setLocationRelativeTo(control.getView());
		setAlwaysOnTop(true);
		setEnabledButtons(false, false, false, false);
		loadDialog();
		((JTextComponent) findCB.getEditor().getEditorComponent()).paste();
		((JTextComponent) findCB.getEditor().getEditorComponent()).selectAll();
		((JTextComponent) replaceCB.getEditor().getEditorComponent()).paste();
		((JTextComponent) replaceCB.getEditor().getEditorComponent()).selectAll();

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				exit();
			}
		});

		// Display the window
		setPreferredSize(new Dimension(700, 280));
		pack();
		setVisible(true);
	}

	public void start() {
		// Schedule a job for the event-dispatching thread:
		// creating and showing this application's GUI
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createInstance(JythonControl control) {
		if (!created) {
			created = true;
			jfr = new JythonFindReplace(control);
		}
		else {
			jfr.setVisible(true);
			jfr.toFront();
		}
	}

}
