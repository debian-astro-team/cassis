/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.scripts;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;

import eu.omp.irap.cassis.scripts.util.ScriptUtil;

@SuppressWarnings("serial")
public class JythonOptions extends JDialog {

	private JythonControl control;
	private JPanel coloring;
	private ColorButton keywordButton, commentButton, stringButton, numberButton, methodButton, varButton, attrButton;
	private ColorButton[] buttons;
	private JTabbedPane tabs;
	private JCheckBox varCleanCheckBox;


	public JythonOptions(JythonControl control) {
		this.control = control;
		initComponents();
		setModal(true);
		start();
	}

	private void initComponents() {
		// Coloring panel
		coloring = new JPanel();
		coloring.setLayout(new BoxLayout(coloring, BoxLayout.PAGE_AXIS));
		keywordButton = addLabelColorButton("Keyword", new Color(-16763956));
		commentButton = addLabelColorButton("Comment", new Color(-4521938));
		stringButton = addLabelColorButton("String", new Color(-13395712));
		numberButton = addLabelColorButton("Number", new Color(-10092442));
		methodButton = addLabelColorButton("Method", new Color(-5230496));
		varButton = addLabelColorButton("Variable", new Color(-8355712));
		attrButton = addLabelColorButton("Attribute", new Color(-10066432));
		coloring.add(Box.createGlue());

		// Create a list of references
		buttons = new ColorButton[] { keywordButton, commentButton, stringButton, numberButton, methodButton,
				varButton, attrButton };
		readColorsFromFile();

		// Backup panel
		JPanel backupPanel = new JPanel();
		backupPanel.setLayout(new BoxLayout(backupPanel, BoxLayout.PAGE_AXIS));
		JButton clearButton = new JButton("Clean up");
		clearButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				removeBackupFiles();
			}
		});
		backupPanel.add(new JLabel("Removes files automatically backed by CASSIS."));
		backupPanel.add(clearButton);

		// Bottom panel
		JPanel bottom = new JPanel(new BorderLayout());
		JButton ok = new JButton("OK");
		ok.setPreferredSize(new Dimension(100, 24));
		ok.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				writeColorsToFile();
				control.getView().refreshAllEditors();

				exit();
			}
		});

		JButton cancel = new JButton("Cancel");
		cancel.setPreferredSize(new Dimension(100, 24));
		cancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				exit();
			}
		});

		JButton restore = new JButton("Use defaults");
		restore.setPreferredSize(new Dimension(150, 24));
		restore.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				restoreToDefaultColor();
			}
		});

		JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		p.add(restore);
		p.add(ok);
		p.add(cancel);

		bottom.add(p, BorderLayout.CENTER);
		bottom.add(new JSeparator(), BorderLayout.NORTH);
		bottom.setBorder(BorderFactory.createEmptyBorder(25, 0, 0, 0));

		// Var
		JPanel panelVar = new JPanel();
		varCleanCheckBox = new JCheckBox("Automatically clean variables", JythonModel.getAutoCleanVar());
		varCleanCheckBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				boolean value = varCleanCheckBox.isSelected();
				setAutoCleanVar(value);
			}
		});
		panelVar.add(varCleanCheckBox);

		tabs = new JTabbedPane();
		tabs.setBorder(UIManager.getBorder("OptionPane.border"));
		tabs.addTab("Syntax Coloring", coloring);
		tabs.addTab("Backup", backupPanel);
		tabs.addTab("Variable", panelVar);

		Container container = getContentPane();
		container.add(tabs, BorderLayout.CENTER);
		container.add(bottom, BorderLayout.SOUTH);
	}

	private void setAutoCleanVar(boolean newValue) {
		JythonModel.setAutoCleanVar(newValue);
		ScriptUtil.saveProperties("clearVar", String.valueOf(newValue));
	}

	private void removeBackupFiles() {
		File dir = new File(ScriptUtil.getScriptPath() + File.separatorChar + "backup");
		if (dir.exists()) {
			File[] files = dir.listFiles();
			for (File f : files) {
				if (f.isFile()) {
					f.delete();
				}
			}
		}
	}

	protected void restoreToDefaultColor() {
		for (int i = 0; i < buttons.length; i++) {
			buttons[i].restoreColor();
		}
		coloring.repaint();
	}

	protected void writeColorsToFile() {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(
				ScriptUtil.getConfigPath() + File.separatorChar
				+ JythonModel.OPTION_FILENAME))) {
			for (int i = 0; i < buttons.length; i++) {
				buttons[i].writeColorToFile(bw);
			}
		} catch (IOException ioe) {
			ScriptUtil.log("JythonOptions/writeColorsToFile: " + ioe.toString());
		}
	}

	private void readColorsFromFile() {
		try (BufferedReader br = new BufferedReader(new FileReader(
				ScriptUtil.getConfigPath() + File.separatorChar
				+ JythonModel.OPTION_FILENAME))) {
			try {
				String line;
				for (int i = 0; i < buttons.length; i++) {
					if ((line = br.readLine()) != null && !line.trim().equals("")) {
						buttons[i].setColor(new Color(Integer.parseInt(line)));
					}
				}
			} catch (IOException e) {} catch (NumberFormatException nfe) {
				ScriptUtil.log("JythonOptions/readColorsFromFile: " + nfe.toString());
			}
		} catch (FileNotFoundException fnfe) {
			ScriptUtil.log("JythonOptions/readColorsFromFile: " + fnfe.toString());
		} catch (IOException e1) {
		}
	}

	private ColorButton addLabelColorButton(String title, Color c) {
		// Label
		JLabel label = new JLabel(title);
		label.setPreferredSize(new Dimension(100, 24));

		// Color Button
		ColorButton button = new ColorButton(c);

		// Panel = (label, button)
		JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
		p.add(label);
		p.add(button);

		// Add
		coloring.add(p);

		return button;
	}

	class ColorButton extends JButton {
		private Color defaultColor, color;
		private static final int DX = 10, DY = 5;

		public ColorButton(Color c) {
			this.defaultColor = c;
			this.color = c;
			setPreferredSize(new Dimension(100, 24));
			addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					Color newColor = JColorChooser.showDialog(ColorButton.this, "Choose Color", color);
					if (newColor != null) {
						color = newColor;
					}
				}
			});
		}

		@Override
		/**
		 * paint() method actually delegates the work of painting to three protected methods: paintComponent, paintBorder, and paintChildren
		 */
		public void paintComponent(Graphics gfx) {
			// if you do not invoker super's implementation you must honor the opaque property, that is if this component is opaque, you must completely fill in the background in a
			// non-opaque color. If you do not honor the opaque property you will likely see visual artifacts
			super.paintComponent(gfx);

			Dimension dim = getSize();
			gfx.setColor(color);
			gfx.fillRect(DX, DY, dim.width - 2 * DX, dim.height - 2 * DY);
			gfx.setColor(Color.BLACK);
			gfx.drawRect(DX, DY, dim.width - 2 * DX, dim.height - 2 * DY);
		}

		public void setColor(Color c) {
			color = c;
		}

		public void restoreColor() {
			color = defaultColor;
		}

		public void writeColorToFile(BufferedWriter bw) throws IOException {
			bw.write(color.getRGB() + "" + JythonModel.NEW_LINE); // color.getAlpha()=255, so color.getRGB() is negative
		}
	};

	/**
	 * Create the GUI and show it. For thread safety, this method should be invoked from the event-dispatching thread.
	 */
	private void createAndShowGUI() {
		// Create and set up the window
		setTitle("Options");
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setLocationRelativeTo(control.getView());

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				exit();
			}
		});

		// Display the window
		setSize(new Dimension(400, 400));
		pack();
		setVisible(true);
	}

	public void start() {
		// Schedule a job for the event-dispatching thread:
		// creating and showing this application's GUI
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				createAndShowGUI();
			}
		});
	}

	private void exit() {
		ScriptUtil.log("before GC: " + Runtime.getRuntime().freeMemory());
		dispose();
		ScriptUtil.log("after GC: " + Runtime.getRuntime().freeMemory());
	}

}
