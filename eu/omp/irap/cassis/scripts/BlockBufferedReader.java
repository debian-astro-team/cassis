/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.scripts;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Block Buffered Reader
 */
public class BlockBufferedReader extends BufferedReader {

	private BufferedReader br;
	private boolean eof;
	private String lastLine;
	private int count;


	public BlockBufferedReader(BufferedReader br) {
		super(br);
		this.br = br;
	}

	public String getBlock() throws IOException {
		count = 0;
		int level = 0;
		return getBlock(level);
	}

	private String getBlock(int level) throws IOException {
		count++;
		// Read a line
		String line;
		if (lastLine == null) {
			line = br.readLine();
		}
		else {
			line = lastLine;
			lastLine = null;
		}

		// EOF?
		if (line == null) {
			eof = true;
			count--; // Don't count NULL line
			return null;
		}

		// Blank line, comment line
		String linetrim = line.trim();
		if (linetrim.equals(JythonModel.BLANK_LINE) ||
				linetrim.startsWith(JythonModel.COMMENT_STRING)) {
			if (level == 0) {// Blank line at root
				return JythonModel.BLANK_LINE;
			}
			else {// Blank line in the current block
				String nextBlock = getBlock(level);
				return (nextBlock == null ? JythonModel.BLANK_LINE : JythonModel.NEW_LINE + nextBlock);
			}
		}

		line = line.replaceAll("\\s+$", "");

		if (line.endsWith("(")) {
			int parCount = 1;
			StringBuffer stringBuffer = new StringBuffer();
			boolean end = false;
			stringBuffer.append(line);
			while (!end) {
				line = br.readLine();

				if (line == null) {
					break;
				}
				line = line.replaceAll("\\s+$", "");
				if (!line.isEmpty() && !line.startsWith("#")) {
					stringBuffer.append(JythonModel.NEW_LINE);

					// Check the ( and add them if necessary to the parCount
					if (getPosForChar('(', line).size() >= 1) {
						List<Integer> posA = getPosForChar('"', line);
						List<Integer> posB = getPosForChar('(', line);

						if (posA.size() == 0) {
							parCount += posB.size();
						} else {
							for (int i = posA.size() - 1; i >= 0; i--) {
								if (isEscaped(line, i)) {
									posA.remove(i);
								}
							}

							if (posA.size() == 2 && posB.size() == 1 && !(posA.get(0) < posB.get(0) && posA.get(1) > posB.get(0))) {
								parCount++;
							} else {
								boolean endProcessing = false, inString = false;
								while (!endProcessing) {
									if (posA.size() == 0) {
										parCount += posB.size();
										endProcessing = true;
									} else if(posA.get(0) < posB.get(0)) {
										inString = !inString;
										posA.remove(0);
									} else {
										if (!inString)
											parCount++;
										posB.remove(0);
										if (posB.size() == 0)
											end = true;
									}
								}
							}
						}
					}
					// Check the ) and remove them if necessary from the parCount
					if (getPosForChar(')', line).size() >= 1) {
						List<Integer> posA2 = getPosForChar('"', line);
						List<Integer> posB2 = getPosForChar(')', line);

						if (posA2.size() == 0) {
							parCount -= posB2.size();
						} else {
							for (int i = posA2.size() - 1; i >= 0; i--) {
								if (isEscaped(line, i)) {
									posA2.remove(i);
								}
							}

							if (posA2.size() == 2 && posB2.size() == 1 && !(posA2.get(0) < posB2.get(0) && posA2.get(1) > posB2.get(0))) {
								parCount--;
							} else {
								boolean endProcessing = false, inString = false;
								while (!endProcessing) {
									if (posA2.size() == 0) {
										parCount -= posB2.size();
										endProcessing = true;
									} else if(posA2.get(0) < posB2.get(0)) {
										inString = !inString;
										posA2.remove(0);
									} else {
										if (!inString)
											parCount--;
										posB2.remove(0);
										if (posB2.size() == 0)
											end = true;
									}
								}
							}
						}
					}

					stringBuffer.append(line);

					if (line.endsWith(")") && parCount == 0) {
						end = true;
					}
				}
				count++;
			}
			return stringBuffer.toString();
		}

		// for, while, if, else, def... line
		if (line.endsWith(":")) {
			if (level == 0) {// Nested block
				String nextBlock = getBlock(level + 1);
				return line + (nextBlock == null ? JythonModel.BLANK_LINE : JythonModel.NEW_LINE + nextBlock);
			}
			else {
				if (line.startsWith(JythonModel.TAB_STRING)) {// Nested block
					String nextBlock = getBlock(level + 1);
					return line + (nextBlock == null ? JythonModel.BLANK_LINE : JythonModel.NEW_LINE + nextBlock);
				}
				else {// Beginning of a new block
					lastLine = line;
					count--; // Don't count NULL line
					return null;
				}
			}
		}

		// Normal line?
		if (level == 0) {// Single line
			return line;
		}
		else {
			if (line.startsWith(JythonModel.TAB_STRING)) {
				String nextBlock = getBlock(level);
				return line + (nextBlock == null ? JythonModel.BLANK_LINE : JythonModel.NEW_LINE + nextBlock);
			}
			else {// Beginning of a new block
				lastLine = line;
				count--; // Don't count NULL line
				return null;
			}
		}
	}

	private List<Integer> getPosForChar(char theChar, String theString) {
		List<Integer> returnList = new ArrayList<Integer>();
		for (int i = 0; i < theString.length(); i++) {
			if (theString.charAt(i) == theChar) {
				returnList.add(i);
			}
		}
		return returnList;
	}

	private boolean isEscaped(String chaine, int pos) {
		if (pos == 0)
			return false;

		if (chaine.charAt(pos-1) == '\\')
			return true;
		return false;
	}

	public boolean isEof() {
		return eof;
	}

	public int getBlockCount() {
		return count;
	}
}
