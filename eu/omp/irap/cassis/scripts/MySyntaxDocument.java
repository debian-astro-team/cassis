/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.scripts;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import eu.omp.irap.cassis.scripts.util.ScriptUtil;

@SuppressWarnings("serial")
public class MySyntaxDocument extends SyntaxDocument {

	private List<String> methods, variables, attributes, classes;


	public MySyntaxDocument() {
		setMethodsList(new String[] { "lteRadexModel", "print", "write", "LTERadexTablePanel", "PanelView",
				"LTERadexTableModel", "LTERadexTableControl", "ComponentDescription", "RadexMoleculeDescription",
				"TopPanelTelescopeModel", "TopPanelModel", "ArrayList", "str", "File", "FileManagerVOTable",
				"CommentedSpectrum", "molecule", "component", "createComponents", "createMolecules", "output" });

		setVariablesList(new String[] { "dir", "step", "result", "radexPanel", "lteModel", "control", "prefix",
				"configName", "index" });

		setAttributesList(new String[] { "line", "eupMax", "telescope", "tmb2ta", "deltaFreq", "components", "model",
				"vlsr", "molecules", "size", "moltag", "Nmol", "FWHM", "tex", "DSB", "LSB", "USB", "eupMin", "aijmin",
				"iMin", "telescopefile", "bandwidth", "rms", "continuum", "tbg" });

		setClassesList(new String[] { "File", "System" });
	}

	/**
	 * Set a methods list
	 */
	public void setMethodsList(String[] list) {
		methods = new ArrayList<String>(Arrays.asList(list));
	}

	public void setMethodsList(String filename) {
		List<String> al = readFile(filename);
		if (al != null) {
			methods = al;
		}
	}

	private List<String> readFile(String filename) {
		List<String> al = new ArrayList<String>();

		try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
			try {
				String line;
				while ((line = br.readLine()) != null) {
					if (!line.trim().equals("")) {
						al.add(line);
					}
				}
			} catch (IOException ioe) {
			}
		} catch (FileNotFoundException fnfe) {
			ScriptUtil.log("MySyntaxDocument/readFile: " + fnfe.toString());
			return null;
		} catch (IOException e) {
		}
		return al;
	}

	@Override
	protected boolean isMethod(String token) {
		return (methods.contains(token));
	}

	/**
	 * Set a variables list
	 */
	public void setVariablesList(String[] list) {
		variables = new ArrayList<String>(Arrays.asList(list));
	}

	public void setVariablesList(String filename) {
		List<String> al = readFile(filename);
		if (al != null) {
			variables = al;
		}
	}

	@Override
	protected boolean isVariable(String token) {
		return (variables.contains(token));
	}

	@Override
	protected boolean isNumber(String token) {
		try {
			Double.valueOf(token);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}

	/**
	 * Set a attributes list
	 */
	public void setAttributesList(String[] list) {
		attributes = new ArrayList<String>(Arrays.asList(list));
	}

	public void setAttributesList(String filename) {
		List<String> al = readFile(filename);
		if (al != null) {
			attributes = al;
		}
	}

	@Override
	protected boolean isAttribute(String token) {
		return (attributes.contains(token));
	}

	/**
	 * Set colors
	 */
	public void setColors(String filename) {
		List<String> al = readFile(filename);
		if (al != null && al.size() == 7) {
			try {
				updateStyleColor(keyword, convert(al.get(0)));
				updateStyleColor(comment, convert(al.get(1)));
				updateStyleColor(quote, convert(al.get(2)));
				updateStyleColor(number, convert(al.get(3)));
				updateStyleColor(method, convert(al.get(4)));
				updateStyleColor(variable, convert(al.get(5)));
				updateStyleColor(attribute, convert(al.get(6)));
			} catch (NumberFormatException nfe) {
				ScriptUtil.log("MySyntaxDocument/setColors: " + nfe.toString());
			}
		}
	}

	private Color convert(String rgb) throws NumberFormatException {
		return new Color(Integer.parseInt(rgb));
	}

	/**
	 * Set a keywords list
	 */
	public void setKeywordsList(String[] list) {
		keywords.clear();
		keywords.addAll(Arrays.asList(list));
	}

	public void setKeywordsList(String filename) {
		List<String> al = readFile(filename);
		if (al != null) {
			keywords.clear();
			keywords.addAll(al);
		}
	}

	/**
	 * Set a classes list
	 */
	public void setClassesList(String[] list) {
		classes = new ArrayList<String>(Arrays.asList(list));
	}

	public void setClassesList(String filename) {
		List<String> al = readFile(filename);
		if (al != null) {
			classes = al;
		}
	}

	@Override
	protected boolean isClass(String token) {
		return (classes.contains(token));
	}

}
