/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.scripts;

import java.awt.Color;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;


/**
 * This class allow to implement an incremental search operation.
 * The class by itself is not enought. We need some more listener
 * like used in {@link JythonControl#incrementalSearch()} for a fully
 * working incremental search.
 *
 * @author M. Boiziot
 */
public class IncrementalSearchOperation implements DocumentListener {

    private JTextPane textPane;
    private Matcher matcher;
    private JTextField textField;

    /**
     * Constructor of the class.
     * @param textPane The {@link JTextPane} where we search.
     * @param textField the {@link JTextField} where we writing our search.
     */
    public IncrementalSearchOperation(JTextPane textPane, JTextField textField) {
        this.textPane = textPane;
        this.textField = textField;
    }

    /**
     * Search in the document.
     *
     * @param document The document.
     */
    private void search(Document document) {
        try {
            String query = document.getText(0,document.getLength());
            Pattern pattern = Pattern.compile(Pattern.quote(query));
            Document content = textPane.getDocument();
            String body = content.getText(0,content.getLength());
            matcher = pattern.matcher(body);
            continueSearch();
        } catch (BadLocationException e) {
           // Do nothing in that case.
        }
    }

    /**
     * HightLight the text if we found what we looked for.
     * Set the search text in RED if we don't found.
     */
    public void continueSearch() {
        if (matcher != null) {
            if (matcher.find()) {
            	textField.setForeground(Color.BLACK);
                textPane.getCaret().setDot(matcher.start());
                textPane.getCaret().moveDot(matcher.end());
                textPane.getCaret().setSelectionVisible(true);
            }
            else {
            	textField.setForeground(Color.RED);
            }
        }
    }

    /* (non-Javadoc)
     * @see javax.swing.event.DocumentListener#insertUpdate(javax.swing.event.DocumentEvent)
     */
    public void insertUpdate(DocumentEvent docEvent) {
        search(docEvent.getDocument());
    }

    /* (non-Javadoc)
     * @see javax.swing.event.DocumentListener#removeUpdate(javax.swing.event.DocumentEvent)
     */
    public void removeUpdate(DocumentEvent docEvent) {
        search(docEvent.getDocument());
    }

    /* (non-Javadoc)
     * @see javax.swing.event.DocumentListener#changedUpdate(javax.swing.event.DocumentEvent)
     */
    public void changedUpdate(DocumentEvent docEvent) {
        search(docEvent.getDocument());
    }
}
