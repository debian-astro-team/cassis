/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.scripts;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Document;
import javax.swing.text.Highlighter;
import javax.swing.text.Highlighter.HighlightPainter;

import eu.omp.irap.cassis.scripts.util.ScriptUtil;

@SuppressWarnings("serial")
public class MyTextEditor extends JTextPane {
	private boolean modified;
	private CompoundUndoManager undoManager;
	private JList<String> line;
	private boolean[] buttonStatus;
	private JythonEval jyEval;
	private Highlighter highLighter;
	private HighlightPainter painter;
	private boolean working;
	private int selectedLine = 1; // Starting to 1.
	private static int caretClicked = -1;


	public MyTextEditor() {
		super(); // implicitly
		initButtonStatus();
		setMouseDbClicked();
	}

	private void initButtonStatus() {
		buttonStatus = new boolean[] { true, true, false, false }; // run, debug, stop, clean
	}

	void setButtonStatus(boolean[] bs) {
		System.arraycopy(bs, 0, buttonStatus, 0, bs.length);
	}

	/**
	 * @return the buttonStatus
	 */
	public boolean[] getButtonStatus() {
		return buttonStatus;
	}

	private void setMouseDbClicked() {
		addMouseListener(new MouseAdapter() {
			/**
			 * Override mousePressed, not mouseClicked
			 */
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					if (e.getClickCount() == 1) {
						caretClicked = getCaretPosition();
					}
					else if (e.getClickCount() == 2) {
						String text = getSelectedText();
						if (caretClicked != -1 && text.length() > 0) {
							int startPos = getSelectionStart();

							int fromIndex = caretClicked - startPos;
							String[] items = text.split("[.]");
							if (items.length > 1) {
								int begin = 0, end = -1;
								for (String item : items) {
									begin = end + 1;
									end += item.length() + 1;
									if (fromIndex >= begin && fromIndex <= end) {
										break;
									}
								}

								setSelectionStart(startPos + begin);
								setSelectionEnd(startPos + end);
							}

							caretClicked = -1;
						}
					}
				}
			}
		});
	}

	/**
	 * Run a script
	 */
	synchronized void runScript(JythonControl control, boolean debugMode) {
		try {
			// Get the contain from the editor
			Document doc = getDocument();
			int len = doc.getLength();
			String contain = doc.getText(0, len);
			// Trick: ensure that a NEWLINE is added at the end of the document
			int posNewLine = contain.lastIndexOf(JythonModel.NEW_LINE);

			if (posNewLine != len - JythonModel.NEW_LINE.length()) {
				doc.insertString(len, JythonModel.NEW_LINE, null);
				len = doc.getLength();
				contain = doc.getText(0, len);
			}

			// Run the script
			if (!debugMode) {
				control.setEnableComponents(JythonModel.RUN_MODE, false);
				control.lockConsole(true);
				control.getView().print("Running... " + JythonModel.NEW_LINE);
				if (jyEval == null || !jyEval.isAlive()) {
					jyEval = new JythonEval(control, contain, JythonModel.RUN_MODE);
				}
			}
			else {
				if (jyEval == null || !jyEval.isAlive()) {
					control.setEnableComponents(JythonModel.DEBUG_MODE, false);
					control.lockConsole(true);
					jyEval = new JythonEval(control, contain, JythonModel.DEBUG_MODE);
				}
				else {
					if (control.getView().getConsole().isEditable()) {
						synchronized (jyEval) {
							control.lockConsole(true);
							jyEval.notify();
						}
					}
				}
			}

			// Set the editor's focus
			requestFocus();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Stop a script
	 */
	public void stopScript(JythonControl control) {
		if (jyEval == null) {
			return;
		}
		// Stop request
		jyEval.stopRequest();
		// Notify
		synchronized (jyEval) {
			jyEval.notify();
		}
		jyEval = null;

		// Update 'console'
		control.getView().println("Aborted.");
		control.getView().println("");
		control.getView().printlnSign();
		control.setEnableComponents(JythonModel.DEBUG_MODE, true);
		control.lockConsole(false);
		disableLineSelection();

		// Set the editor's focus
		requestFocus();
	}

	/**
	 * Run a single line script
	 */
	public void runSingleLine(JythonControl control) {
		JTextArea console = control.getView().getConsole();
		int lineCount = console.getLineCount();
		try {
			// Get the last line (command)
			int begPos = console.getLineStartOffset(lineCount - 1);
			int endPos = console.getLineEndOffset(lineCount - 1);
			String lastLine = console.getText(begPos, endPos - begPos);


			// Get the command without 'jythonSign'
			if (lastLine.startsWith(JythonModel.JYTHON_PROMPT))
				lastLine = lastLine.substring(JythonModel.JYTHON_PROMPT.length(), lastLine.length()).trim();

			// History of commands
			control.getCmdHistory().add(lastLine);
			control.updateCurrCmdHistory();
			control.getView().println("");
			// Run the command
			if (jyEval == null || !jyEval.isAlive()) {
				jyEval = new JythonEval(control, lastLine, JythonModel.CONSOLE_MODE);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Not yet tested
	 */
	void initHighLighter() {
		highLighter = new DefaultHighlighter();
		painter = new DefaultHighlighter.DefaultHighlightPainter(new Color(175, 175, 255));
		setHighlighter(highLighter);
		addCaretListener(new CaretListener() {
			@Override
			public void caretUpdate(CaretEvent e) {
				resetHighlight();
			}
		});
	}

	/**
	 * Caret position has changed, remove the highlight, not yet tested
	 */
	private void resetHighlight() {
		highLighter.removeAllHighlights();
		if (!working) {
			working = true;
			try {
				// Get the current caret
				int currPos = getCaretPosition();
				getActionMap().get(DefaultEditorKit.beginLineAction).actionPerformed(null);
				int begLinePos = getCaretPosition();
				getActionMap().get(DefaultEditorKit.endLineAction).actionPerformed(null);
				int endLinePos = getCaretPosition();
				setCaretPosition(currPos);
				// Highlight
				highLighter.addHighlight(begLinePos, endLinePos, painter);
			} catch (BadLocationException ble) {}
			working = false;
		}
	}

	public boolean isModified() {
		return modified;
	}

	public void setModified(boolean b) {
		modified = b;
	}

	public void setCompoundUndoManager(CompoundUndoManager cum) {
		removeCompoundUndoManager();
		undoManager = cum;
	}

	public CompoundUndoManager getCompoundUndoManager() {
		return undoManager;
	}

	public void removeCompoundUndoManager() {
		if (undoManager != null) {
			undoManager.end();
		}
		undoManager = null;
	}

	public boolean canUndo() {
		return undoManager.canUndo();
	}

	public void undo() {
		undoManager.undo();
	}

	public boolean canRedo() {
		return undoManager.canRedo();
	}

	public void redo() {
		undoManager.redo();
	}

	public boolean save(String tabTitle, JythonView jythonView, boolean calledFromSaveAll, boolean calledWindowClosing,
			boolean append, String tabFullTitle) {
		if (calledFromSaveAll) {
			jythonView.getControl().addScriptnameToHistory(tabFullTitle, append);
		}

		if (!isModified()) {
			return true;
		}

		String msg = "'" + tabTitle.substring(1) + "' has been modified. Save changes?";
		int option = calledFromSaveAll && !calledWindowClosing ? JOptionPane.YES_OPTION : JOptionPane
				.showConfirmDialog(null, msg, JythonModel.SCRIPT_TITLE, JOptionPane.YES_NO_CANCEL_OPTION,
						JOptionPane.QUESTION_MESSAGE);
		if (option == JOptionPane.CANCEL_OPTION || option == JOptionPane.CLOSED_OPTION) {
			ScriptUtil.log("canceled");
			return false;
		}
		else if (option == JOptionPane.YES_OPTION) {
			return jythonView.getControl().saveScript(false);
		}
		return true;
	}

	public void setLine(JList<String> line) {
		this.line = line;
	}

	public JList<String> getLine() {
		return line;
	}

	public synchronized void disableLineSelection() {
		line.clearSelection();
	}

	public synchronized void setIndicator(int index) {
		getCaret().setVisible(false);
		int indexEnd = (index >= line.getModel().getSize() - 1 ? line.getModel().getSize() - 1 : index + 1);
		line.setSelectedIndex(index);
		line.scrollRectToVisible(line.getCellBounds(index, indexEnd));
		line.getParent().repaint();
		getParent().repaint();
		getCaret().setVisible(true);
	}

	/**
	 * By selected, we mean where the arrow is.
	 * @param num The number of the line (starting at 1).
	 */
	public void setSelectedLine(int num) {
		selectedLine = num;
	}

	/**
	 * Return the selected line, the number where the arrow is.
	 * @return The selected line.
	 */
	public int getSelectedLine() {
		return selectedLine;
	}
}
