/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import eu.omp.irap.cassis.database.access.CassisDataBaseConnection;
import eu.omp.irap.cassis.database.access.CdmsDataBaseConnection;
import eu.omp.irap.cassis.database.access.DataBaseConnection;
import eu.omp.irap.cassis.database.access.FileDataBaseConnection;
import eu.omp.irap.cassis.database.access.NistDataBaseConnection;

/**
 * Utility class for CASSIS file databases. The class allow to handle path of
 *  file database and correct it if necessary to create a
 *  {@link DataBaseConnection} object.
 *
 * @author M. Boiziot
 */
public class FileDatabaseUtils {

	public enum FILE_DB_TYPE { CDMS, JPL, NIST, CASSIS, UNKNOW }

	/**
	 * Return the last folder path of the given path. The given path must exists
	 *  or the function will return null.
	 * This function also add a File.separatorChar at the end of the returned path.
	 *
	 * @param path The path.
	 * @return The folder path of the given path, or null if the given path does
	 *  not exists.
	 */
	public static String getFolder(String path) {
		File element = new File(path);
		String newPath = null;
		if (element.exists()) {
			if (element.isFile()) {
				newPath = element.getParent();
			} else {
				newPath = path;
			}
			if (newPath != null && !newPath.endsWith(File.separator)) {
				newPath += File.separatorChar;
			}
			return newPath;
		}
		return null;
	}

	/**
	 * Return if the folder at the given path contains a "partition_function.html" file.
	 *
	 * @param pathFolder The path of the folder.
	 * @return true if the folder contains the "partition_function.html" file,
	 *  false otherwise.
	 */
	public static boolean containsPartitionFunctionHtml(String pathFolder) {
		return new File(pathFolder + File.separatorChar +
				"partition_function.html").exists();
	}

	/**
	 * Return if the folder at the given path contains a "catdir.cat" file.
	 *
	 * @param pathFolder The path of the folder.
	 * @return true if the folder contains the "catdir.cat" file, false otherwise.
	 */
	public static boolean containsCatdirCat(String pathFolder) {
		return new File(pathFolder + File.separatorChar + "catdir.cat").exists();
	}

	/**
	 * Return the if the folder at the given path is a CDMS database.
	 *
	 * @param pathFolder The path of the folder.
	 * @return true if the folder is a CDMS database, false otherwise.
	 */
	public static boolean isCdms(String pathFolder) {
		return containsPartitionFunctionHtml(pathFolder);
	}

	/**
	 * Return the if the folder at the given path is a JPL database.
	 *
	 * @param pathFolder The path of the folder.
	 * @return true if the folder is a JPL database, false otherwise.
	 */
	public static boolean isJpl(String pathFolder) {
		return containsCatdirCat(pathFolder);
	}

	/**
	 * Return the if the folder at the given path can be a NIST database.
	 *  Note that this test is really weak and must return a wrong result.
	 *  Please test with CDMS or JPL first.
	 *
	 * @param pathFolder The path of the folder.
	 * @return true if the folder can be a NIST database, false otherwise.
	 */
	public static boolean isNist(String pathFolder) {
		File[] files = new File(pathFolder).listFiles();
		for (File file : files) {
			if (file.getName().endsWith(".html")) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Return the if the folder at the given path can be a CASSIS database.
	 *
	 * @param pathFolder The path of the folder.
	 * @return true if the folder can be a CASSIS database, false otherwise.
	 */
	public static boolean isCassis(String pathFolder) {
		boolean res = true;
		File[] files = new File(pathFolder).listFiles();
		if (files.length == 0) {
			res = false;
		}
		Set<String> setFileCat = new HashSet<String>();
		Set<String> setFileTxt = new HashSet<String>();

		for (int i = 0; res && i < files.length; i++) {
			if (files[i].getName().endsWith(".cat")) {
				setFileCat.add(files[i].getName().replace(".cat", ""));
			}else if (files[i].getName().endsWith(".txt")) {
				setFileTxt.add(files[i].getName().replace(".txt", ""));
			}
		}

		for (String val : setFileCat) {
			res = res && setFileTxt.contains(val);
		}


		return res;
	}

	/**
	 * Compute then return the type of file database.
	 *
	 * @param path The path of the database.
	 * @return the type of file database.
	 */
	public static FILE_DB_TYPE getType(String path) {
		String folder = getFolder(path);
		if (isCdms(folder)) {
			return FILE_DB_TYPE.CDMS;
		} else if (isJpl(folder)) {
			return FILE_DB_TYPE.JPL;
		} else if (isNist(folder)) {
			return FILE_DB_TYPE.NIST;
		} else if (isCassis(folder)) {
			return FILE_DB_TYPE.CASSIS;
		}
		else {
			return FILE_DB_TYPE.UNKNOW;
		}
	}

	/**
	 * Create then return the correct File DatabaseConnection type for the given path.
	 *
	 * @param path The path of the database.
	 * @return The DatabaseConnection or null if this is not a File database.
	 */
	public static DataBaseConnection getConnection(String path) {
		String folder = getFolder(path);
		DataBaseConnection connection;
		switch (getType(path)) {
		case CDMS:
			connection = new CdmsDataBaseConnection(folder);
			break;
		case JPL:
			connection = new FileDataBaseConnection(folder);
			break;
		case NIST:
			connection = new NistDataBaseConnection(folder);
			break;
		case CASSIS :
			connection = new CassisDataBaseConnection(folder);
			break;

		default:
			// We can not do anything here. There is not File database in the given path.
			connection = null;
			break;
		}
		return connection;
	}

	/**
	 *
	 * @param molTag
	 * @return
	 */
	public static String getNameFileTag(int molTag) {
		String nameFile  = String.valueOf(molTag);
		while (nameFile.length() < 6) {
			nameFile = "0"+ nameFile;
		}
		nameFile = "c" + nameFile;
		return nameFile;
	}

	public static String getCatNameFileTag(int molTag) {
		return getNameFileTag(molTag) + ".cat";
	}

	public static String getPartNameFileTag(int molTag) {
		return getNameFileTag(molTag) + ".txt";
	}
}
