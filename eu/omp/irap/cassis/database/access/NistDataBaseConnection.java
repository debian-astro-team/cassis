/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.access;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import eu.omp.irap.cassis.common.Formula;

public class NistDataBaseConnection extends FileDataBaseConnection {
	private final Logger logger = Logger.getLogger(NistDataBaseConnection.class.getName());
	final double cst = Formula.hPLANCK_cgs*Formula.cLIGHT_cgs / Formula.kBOLTZMANN_cgs;

	private final String[][] atomes = {
			{ "H", "He", "Li", "Be", "B", "C", "N", "O", "F", "Ne", "Na", "Mg", "Al", "Si", "P",
					"S", "Cl", "Ar", "K", "Ca", "Sc", "Ti", "V", "Cr", "Mn", "Fe", "Co", "Ni",
					"Cu", "Zn", "Ga", "Ge", "As", "Se", "Br", "Kr", "Rb", "Sr", "Y", "Zr", "Nb",
					"Mo", "Tc", "Ru", "Rh", "Pd", "Ag", "Cd", "In", "Sn", "Sb", "Te", "I", "Xe",
					"Cs", "Ba", "La", "Ce", "Pr", "Nd", "Pm", "Sm", "Eu", "Gd", "Tb", "Dy", "Ho",
					"Er", "Tm", "Yb", "Lu", "Hf", "Ta", "W", "Re", "Os", "Ir", "Pt", "Au", "Hg",
					"Tl", "Pb", "Bi", "Po", "At", "Rn", "Fr", "Ra", "Ac", "Th", "Pa", "U", "Np",
					"Pu", "Am", "Cm", "Bk", "Cf", "Es", "Fm", "Md", "No", "Lr", "Rf" },
			{ "1", "4", "7", "9", "11", "12", "14", "16", "19", "20", "23", "24", "27", "28", "31",
					"32", "35", "40", "39", "41", "45", "48", "51", "52", "55", "56", "59", "58",
					"64", "65", "70", "73", "75", "79", "80", "84", "85", "88", "89", "91", "93",
					"96", "98", "101", "103", "106", "108", "112", "115", "119", "122", "128",
					"127", "131", "133", "137", "139", "140", "141", "144", "145", "150", "152",
					"157", "159", "163", "165", "167", "169", "173", "175", "178", "181", "184",
					"186", "190", "192", "195", "197", "201", "204", "207", "208", "209", "210",
					"222", "223", "226", "227", "232", "231", "238", "237", "244", "243", "246",
					"247", "251", "252", "257", "258", "259", "262", "261" },
			{ "Hydrogen", "Helium", "Lithium", "Beryllium", "Boron", "Carbon", "Nitrogen",
					"Oxygen", "Fluorine", "Neon", "Sodium", "Magnesium", "Aluminum", "Silicon",
					"Phosphorus", "Sulfur", "Chlorine", "Argon", "Potassium", "Calcium",
					"Scandium", "Titanium", "Vanadium", "Chromium", "Manganese", "Iron", "Cobalt",
					"Nickel", "Copper", "Zinc", "Gallium", "Germanium", "Arsenic", "Selenium",
					"Bromine", "Krypton", "Rubidium", "Strontium", "Yttrium", "Zirconium",
					"Niobium", "Molybdenum", "Technetium", "Ruthenium", "Rhodium", "Palladium",
					"Silver", "Cadmium", "Indium", "Tin", "Antimony", "Tellurium", "Iodine",
					"Xenon", "Cesium", "Barium", "Lanthanum", "Cerium", "Praseodymium",
					"Neodymium", "Promethium", "Samarium", "Europium", "Gadolinium", "Terbium",
					"Dysprosium", "Holmium", "Erbium", "Thulium", "Ytterbium", "Lutetium",
					"Hafnium", "Tantalum", "Tungsten", "Rhenium", "Osmium", "Iridium", "Platinum",
					"Gold", "Mercury", "Thallium", "Lead", "Bismuth", "Polonium", "Astatine",
					"Radon", "Francium", "Radium", "Actinium", "Thorium", "Protactinium",
					"Uranium", "Neptunium", "Plutonium", "Americium", "Curium", "Berkelium",
					"Californium", "Einsteinium", "Fermium", "Mendelevium", "Nobelium",
					"Lawrencium", "Rutherfordium" } };

	public NistDataBaseConnection(String path) {
		super(path);
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.cassis.database.DataBaseConnection#getMoleculeDescriptionDB(cassis.database.SimpleMoleculeDescriptionDB)
	 */
	@Override
	public MoleculeDescriptionDB getMoleculeDescriptionDB(
			SimpleMoleculeDescriptionDB simpleMolDB) throws UnknowMoleculeException {
		List<SimpleMoleculeDescriptionDB> allMoleculeDescriptionDB = getAllMoleculeDescriptionDB();
		for (SimpleMoleculeDescriptionDB moleculeDescriptionDB : allMoleculeDescriptionDB) {
			if (moleculeDescriptionDB.getTag() == simpleMolDB.getTag()) {
				return (MoleculeDescriptionDB) moleculeDescriptionDB;
			}
		}
		throw new UnknowMoleculeException(simpleMolDB.getTag());
	}

	@Override
	public String getMolName(int molTag) {
		String name;
		try {
			SimpleMoleculeDescriptionDB mol = getMoleculeDescriptionDB(
					new SimpleMoleculeDescriptionDB(molTag, null));
			name = mol.getName();
		} catch (UnknowMoleculeException ume) {
			name = NOT_IN_DATABASE;
		}
		return name;
	}

	@Override
	public List<SimpleMoleculeDescriptionDB> getAllMoleculeDescriptionDB() {
		if (speciesDescriptionDB == null) {
			speciesDescriptionDB = new ArrayList<SimpleMoleculeDescriptionDB>();
			for (int i = 0; i < atomes[0].length; i++) {
				String name = atomes[0][i];
				final String massAtomic = atomes[1][i];
				try (BufferedReader atomeReader = new BufferedReader(new InputStreamReader(
						new URL(path + name + ".html").openStream()))) {
					String line = getFirstLine(atomeReader);
					String[] atomeNumber = new String[200]; // name
					while ((line != null) && (!line.contains("-----------"))) {

						String[] infos = line.split("\\|");
						String numIsotope = infos[0].substring(2).trim();

						if (infos.length >= 14 && !numIsotope.equals("") && !infos[2].trim().isEmpty()
								&& !infos[6].trim().isEmpty() && !infos[13].trim().isEmpty()) {

							getAtomeNumber(atomeNumber, numIsotope);

						}
						line = atomeReader.readLine();
					}

					int tag;
					for (int o = 0; atomeNumber[o] != null; o++) {
						int num = convertRomanToNumber(atomeNumber[o]);
						if (num < 10) {
							tag = Integer.parseInt(massAtomic + "10" + num);
						}
						else {
							tag = Integer.parseInt(massAtomic + "1" + num);
						}

						double[] qlog = getQLog(name + " " + atomeNumber[o]);
						double[] temperatures = {1000, 500, 300, 225, 150, 75, 37.5, 18.75, 9.375};

						speciesDescriptionDB.add(new MoleculeDescriptionDB(tag, name + " " + atomeNumber[o], temperatures, qlog));
					}
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
		return speciesDescriptionDB;
	}


	public double[] getQLog(String name) {
		double[] qlog = new double[9];
		String line1 = "";
		try (BufferedReader qLogReader = new BufferedReader(new InputStreamReader(
				new URL(path + name + ".html").openStream()))) {
			// Take care of the beginning of informations in web page
			line1 = skipHeaderOfQLog(line1, qLogReader);

			if (line1 == null) {
				for (int i = 0; i < qlog.length; i++) {
					qlog[i] = -1000;
				}
			} else {
				for (int i = 0; i < qlog.length; i++) {
					qlog[i] = 0;
				}

				double[] temperatures = {1000, 500, 300, 225, 150, 75, 37.5, 18.75, 9.375};

				while ((line1 != null)&&!line1.contains("----------")) {
					String[] infos = line1.split("\\|");
					infos[2] = infos[2].trim();
					infos[3] = infos[3].trim();

					if ((!infos[2].equals("")) && (!infos[3].equals(""))) {
						double j;
						if (infos[2].contains("?")) {
							infos[2] = infos[2].replace("?", "");
						}

						if (infos[2].contains(",")) {
							String[] infos2 = infos[2].split(",");
							infos[2] = infos2[infos2.length/2];
						}

						if (infos[2].contains("or")) {
							String[] infos2 = infos[2].split("or");
							infos[2] = infos2[infos2.length/2];
						}

						if (infos[2].contains("/"))
						{
							double d1 = Double.parseDouble(infos[2].substring(0, infos[2].indexOf("/")));
							double d2 = Double.parseDouble(infos[2].substring(infos[2].indexOf("/")+1));
							j = d1/d2;
						} else {
							j = Double.parseDouble(infos[2]);
						}

						if (infos[3].endsWith(">")) {
							String[] l = infos[3].split("<a");
							if (l[0].equals("")) {
								l = l[1].split("</a>");
								infos[3] = l[1];
							} else {
								infos[3] = l[0];
							}
						}
						double level = Double.parseDouble(infos[3]);

						for (int i=0; i<temperatures.length; i++) {
							qlog[i] = qlog[i] +((2*j+1)*Math.exp(-cst * (level/temperatures[i])));
						}
					}
					try {
						line1 = qLogReader.readLine();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				for (int i=0; i<qlog.length; i++) {
					qlog[i] = Math.log10(qlog[i]);
				}
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return qlog;
	}

	/**
	 * @param line1
	 * @param qLogReader
	 * @return
	 */
	private String skipHeaderOfQLog(String line1, BufferedReader qLogReader) {
		boolean header = true;
		while(header) {
		    try {
				line1 = qLogReader.readLine();
		    } catch (Exception e2) {
		        logger.severe("Error while reading catalog header: " + e2);
		    }
		    if (line1 == null) {
				header = false;
			} else {
				if (line1.startsWith("-------")) {
					for (int j1=0; j1<4 ; j1++) {
						try {
							line1 = qLogReader.readLine();
					    } catch (Exception e3) {
					        logger.severe("Error while reading catalog header: " + e3);
					    }
					}
					header = false;
				}
		    }
		}
		return line1;
	}

	/**
	 * @param atomeNumber
	 * @param atomeLineNumber
	 * @param value
	 */
	private void getAtomeNumber(String[] atomeNumber, String value) {
		boolean trouve = false;
		for (int m = 0; m < atomeNumber.length && !trouve && atomeNumber[m] != null; m++) {
			if (atomeNumber[m].equals(value)) {
				trouve = true;
			}
		}

		int n = 0;
		if (! trouve) {
			while (atomeNumber[n] != null) {
				n++;
			}
			atomeNumber[n] = value;
		}
	}

	/**
	 * @param atomeReader
	 * @return
	 * @throws IOException
	 */
	private String getFirstLine(BufferedReader atomeReader) throws IOException {
		String line = null;

		do {
			line = atomeReader.readLine();
		} while (line != null && !line.startsWith("-------"));

		do {
			line = atomeReader.readLine();
		} while (line != null && !line.startsWith("-------"));

		return atomeReader.readLine();
	}

	public int convertRomanToNumber(String roman) {
		int i = 0;
		int j = 0;
		int k = 0;
		int l = 0;
		char c;
		for (j = 0; j < roman.length(); j++) {
			c = roman.charAt(j);
			switch (c) {
			case 'I':
				k = 1;
				break;
			case 'V':
				k = 5;
				break;
			case 'X':
				k = 10;
				break;
			case 'L':
				k = 50;
				break;
			case 'C':
				k = 100;
				break;
			default:
				k = 0;
				break;
			}
			if (k <= l) {
				i = i + k;
			} else {
				i = i - 2 * l + k;
			}
			l = k;
		}
		return i;
	}

	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(
			SimpleMoleculeDescriptionDB molecule, double freqMin,
			double freqMax) {

		List<LineDescriptionDB> lines = new ArrayList<LineDescriptionDB>();
		String name = molecule.getName().split(" ")[0];
		try (BufferedReader atomeReader = new BufferedReader(new InputStreamReader(
				new URL(path + name + ".html").openStream()))) {
			String line = getFirstLine(atomeReader);

			while ((line != null) && (!line.contains("-----------"))) {
				String[] infos = line.split("\\|");
				String numIsotope = infos[0].substring(2).trim();

				if (numIsotope.equals(molecule.getName().split(" ")[1])&&

						infos.length >= 14 && !numIsotope.equals("") && !infos[2].trim().isEmpty()
						&& !infos[6].trim().isEmpty() && !infos[13].trim().isEmpty()) {

					double micron = 0;
					final String micronInfo = infos[2].trim();
					if (micronInfo.endsWith(">")) {
						String[] m = micronInfo.split("<a");
						micron = Double.parseDouble(m[0]);
					} else {
						micron = Double.parseDouble(micronInfo);
					}


					double fMHz = (((1 / micron) * (Formula.C * Math.pow(10, 6))) / 1000000);

					if (fMHz >= freqMin && fMHz <= freqMax) {
						double aint = Double.parseDouble(infos[4]);

						double elowValue = 0;
						double eupValue = 0;
						final String eupAndElow = infos[6].trim();
						if (!eupAndElow.contains("                     ")) {
							String[] eLowAndUp = eupAndElow.split("-");
							
							String eLowInfo = eLowAndUp[0].trim();
							while(eLowInfo.contains("<")) {
								int beginIndex = eLowInfo.indexOf("<");
								int endIndex = eLowInfo.indexOf(">");
								String substring = eLowInfo.substring(beginIndex, endIndex+1);
								eLowInfo = eLowInfo.replace(substring, "").trim();
							}
							eLowInfo= eLowInfo.replaceFirst("\\[", "").trim();
							eLowInfo = eLowInfo.replaceFirst("\\]", "").trim();
							eLowInfo= eLowInfo.replaceFirst("\\(", "").trim();
							eLowInfo = eLowInfo.replaceFirst("\\)", "").trim();
							eLowInfo = eLowInfo.replaceFirst("\\+x", "").trim();
							eLowInfo = eLowInfo.replaceFirst("\\+r", "").trim();
							eLowInfo = eLowInfo.replaceFirst("\\+k", "").trim();
							eLowInfo = eLowInfo.replaceFirst("\\?", "").trim();
							eLowInfo = eLowInfo.replaceFirst("&dagger;", "").trim();
							eLowInfo = eLowInfo.replaceFirst("\\+y", "").trim();
							eLowInfo = eLowInfo.replaceFirst("u", "").trim();
							
						    elowValue = Double.parseDouble(eLowInfo);


							String eupInfo = eLowAndUp[1].trim();
							while(eupInfo.contains("<")) {
								int beginIndex = eupInfo.indexOf("<");
								int endIndex = eupInfo.indexOf(">");
								String substring = eupInfo.substring(beginIndex, endIndex+1);
								eupInfo = eupInfo.replace(substring, "").trim();
							}
							eupInfo = eupInfo.replaceFirst("\\[", "").trim();
							eupInfo = eupInfo.replaceFirst("\\]", "").trim();
							eupInfo = eupInfo.replaceFirst("\\(", "").trim();
							eupInfo = eupInfo.replaceFirst("\\)", "").trim();
							eupInfo = eupInfo.replaceFirst("\\+x", "").trim();
							eupInfo = eupInfo.replaceFirst("\\+r", "").trim();
							eupInfo = eupInfo.replaceFirst("\\+k", "").trim();
							eupInfo = eupInfo.replaceFirst("\\?", "").trim();
							eupInfo = eupInfo.replaceFirst("&dagger;", "").trim();
							eupInfo = eupInfo.replaceFirst("\\+y", "").trim();
							eupInfo = eupInfo.replaceFirst("u", "").trim();
							eupValue = Double.parseDouble(eupInfo);
						}

						String qn = "?:?";
						try {
							String jLow = infos[9].trim();
							if (jLow.isEmpty()) {
								jLow = "?";
							}

							String jUp = infos[12].trim();
							if (jUp.isEmpty()) {
								jUp = "?";
							}
							qn = jUp +":"+jLow;
						} catch (Exception e) {
							e.printStackTrace();
						}

						int gup = 0;
						try {
							final String[] iguSplit = infos[13].split("-");
							String igu = "1";
							if (iguSplit.length ==2) {
								igu = iguSplit[1].trim();
							}

							if (igu.contains("?")) {
								igu = igu.replace("?", "");
							}
							if (igu.isEmpty()) {
								igu = "1";
							}
							gup = Integer.parseInt(igu);
						} catch (Exception e) {
							e.printStackTrace();
						}

						lines.add(new LineDescriptionDB(molecule.getTag(), fMHz, qn,0.0 , aint, elowValue, gup, 0.0, Formula.calcEUpKComet(eupValue)));
					}

				}
				line = atomeReader.readLine();
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		return lines;
	}
	
	public static void main(String[] args) {
		String val = "<span id=\"001001.001.000001\" class=\"en_span\" onclick=\"selectById('001001.001.000001')\" onmouseover=\"setMOn(this)\" onmouseout=\"setMOff(this)\">0.0000000000 </span>";
		String replaceFirst = val.replaceFirst("<span .*\">", "lllllllllllllllllllllllllllllllllllll");
		System.out.println(replaceFirst);		
				
	}
}
