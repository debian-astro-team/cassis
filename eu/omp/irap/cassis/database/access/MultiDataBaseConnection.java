/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.access;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


/**
 * Database implementation to uses multiple databases at the same time.
 *
 * @author M. Boiziot
 */
public class MultiDataBaseConnection implements DataBaseConnection, ClosableDataBaseConnection {

	private List<DataBaseConnection> databases;
	private Map<Integer, DataBaseConnection> mapTagDb;
	private List<SimpleMoleculeDescriptionDB> listSpecies;


	/**
	 * Constructor.
	 *
	 * @param databases The list of databases used to create this one.
	 */
	public MultiDataBaseConnection(List<DataBaseConnection> databases) {
		if (databases == null) {
			throw new IllegalArgumentException("The list of databases can not be null.");
		}
		this.databases = databases;
	}

	/**
	 * Return the {@link DataBaseConnection} who contains the species with the given tag.
	 *
	 * @param tag The tag.
	 * @return the {@link DataBaseConnection} who contains the species with the given tag
	 *  or null if not found.
	 */
	private DataBaseConnection getDataBase(int tag) {
		return mapTagDb.get(tag);
	}

	/**
	 * Compute then return a Map of {@link List} of {@link SimpleMoleculeDescriptionDB}
	 *  ordered by {@link DataBaseConnection}.
	 *
	 * @param speciesList The list of species to order by database.
	 * @return the list of species ordered by {@link DataBaseConnection}.
	 */
	private Map<DataBaseConnection, List<SimpleMoleculeDescriptionDB>>
			getSpeciesByDatabase(List<SimpleMoleculeDescriptionDB> speciesList) {
		Map<DataBaseConnection, List<SimpleMoleculeDescriptionDB>> mapDbListSpecies =
				new HashMap<>();
		for (SimpleMoleculeDescriptionDB species : speciesList) {
			DataBaseConnection dbc = getDataBase(species.getTag());
			List<SimpleMoleculeDescriptionDB> list;
			if (mapDbListSpecies.containsKey(dbc)) {
				list = mapDbListSpecies.get(dbc);
			} else {
				list = new ArrayList<SimpleMoleculeDescriptionDB>();
				mapDbListSpecies.put(dbc, list);
			}
			list.add(species);
		}

		return mapDbListSpecies;
	}

	/**
	 * Return the {@link MoleculeDescriptionDB} for the given {@link SimpleMoleculeDescriptionDB}.
	 *
	 * @param simpleMolDB The {@link SimpleMoleculeDescriptionDB}.
	 * @return the {@link MoleculeDescriptionDB} corresponding to the given
	 *  {@link SimpleMoleculeDescriptionDB}.
	 * @throws UnknowMoleculeException If the molecule is not found in the database.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getMoleculeDescriptionDB(eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB)
	 */
	@Override
	public MoleculeDescriptionDB getMoleculeDescriptionDB(
			SimpleMoleculeDescriptionDB simpleMolDB) throws UnknowMoleculeException {
		DataBaseConnection dbc = getDataBase(simpleMolDB.getTag());
		if (dbc != null) {
			return dbc.getMoleculeDescriptionDB(simpleMolDB);
		}
		throw new UnknowMoleculeException(simpleMolDB.getTag());
	}

	/**
	 * Return the list of all {@link SimpleMoleculeDescriptionDB} who are in the database.
	 *
	 * @return the list of all {@link SimpleMoleculeDescriptionDB} who are in the database.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getAllMoleculeDescriptionDB()
	 */
	@Override
	public List<SimpleMoleculeDescriptionDB> getAllMoleculeDescriptionDB() {
		if (listSpecies == null) {
			listSpecies = new ArrayList<SimpleMoleculeDescriptionDB>();
			mapTagDb = new HashMap<>();
			for (DataBaseConnection dbc : databases) {
				List<SimpleMoleculeDescriptionDB> lSpecies = dbc.getAllMoleculeDescriptionDB();
				for (SimpleMoleculeDescriptionDB species : lSpecies) {
					mapTagDb.put(species.getTag(), dbc);
				}
				listSpecies.addAll(lSpecies);
			}
		}
		return listSpecies;
	}

	/**
	 * Return the molecule name of the given molecule tag.
	 *
	 * @param molTag The molecule tag.
	 * @return the molecule name or {@link DataBaseConnection#NOT_IN_DATABASE}
	 *  if the molecule is not in database.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getMolName(int)
	 */
	@Override
	public String getMolName(int molTag) {
		DataBaseConnection dbc = getDataBase(molTag);
		if (dbc != null) {
			return dbc.getMolName(molTag);
		}
		return DataBaseConnection.NOT_IN_DATABASE;
	}

	/**
	 * Return the database of the given molecule tag.
	 *
	 * @param molTag The molecule tag.
	 * @return the database of the given molecule tag.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getDataBaseOf(int)
	 */
	@Override
	public String getDataBaseOf(int molTag) {
		DataBaseConnection dbc = getDataBase(molTag);
		if (dbc != null) {
			return dbc.getDataBaseOf(molTag);
		}
		return DataBaseConnection.NOT_IN_DATABASE;
	}

	/**
	 * Computation of the partition function.
	 *
	 * @param tex The temperature.
	 * @param molTag The molecule tag.
	 * @return the partition function.
	 * @throws UnknowMoleculeException If the molecule is not found in the database.
	 */
	@Override
	public double buildQt(double tex, int molTag) throws UnknowMoleculeException {
		DataBaseConnection dbc = getDataBase(molTag);
		if (dbc != null) {
			dbc.buildQt(tex, molTag);
		}
		throw new UnknowMoleculeException(molTag);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see eu.omp.irap.cassis.database.getPartitionFunction#buildQt(int)
	 */
	@Override
	public PartitionFunction getPartitionFunction(int molTag, boolean force) throws UnknowMoleculeException {
		DataBaseConnection dbc = getDataBase(molTag);
		if (dbc != null) {
			dbc. getPartitionFunction(molTag, true);
		}
		throw new UnknowMoleculeException(molTag);
	}


	/**
	 * Return a list of all {@link LineDescriptionDB} of the the given
	 *  {@link SimpleMoleculeDescriptionDB} between freqMinComp and freqMaxComp.
	 *
	 * @param molecule The {@link SimpleMoleculeDescriptionDB}.
	 * @param freqMinComp The minimum frequency (in MHz).
	 * @param freqMaxComp The maximum frequency (in MHz).
	 * @return a list of all {@link LineDescriptionDB} for the given parameters.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getLineDescriptionDB(eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB, double, double)
	 */
	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(
			SimpleMoleculeDescriptionDB molecule, double freqMinComp,
			double freqMaxComp) {
		DataBaseConnection dbc = getDataBase(molecule.getTag());
		if (dbc != null) {
			return dbc.getLineDescriptionDB(molecule, freqMinComp, freqMaxComp);
		}
		return Collections.emptyList();
	}

	/**
	 * Return a list of all {@link LineDescriptionDB} of the the given
	 *  {@link SimpleMoleculeDescriptionDB} with the given parameters.
	 *
	 * @param simpleMolDB The {@link SimpleMoleculeDescriptionDB}.
	 * @param frequencyMin The minimum frequency (in MHz).
	 * @param frequencyMax The maximum frequency (in MHz).
	 * @param otherThresEupMin The minimum eup value.
	 * @param otherThresEupMax The maximum eup value.
	 * @param otherThresAijMin The minimum aij value.
	 * @param otherTreshAijMax The maximum aij value.
	 * @return a list of all {@link LineDescriptionDB} for the given parameters.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getLineDescriptionDB(eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB, double, double, double, double, double, double)
	 */
	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(
			SimpleMoleculeDescriptionDB simpleMolDB, double frequencyMin,
			double frequencyMax, double otherThresEupMin, double otherThresEupMax,
			double otherThresAijMin, double otherTreshAijMax) {
		DataBaseConnection dbc = getDataBase(simpleMolDB.getTag());
		if (dbc != null) {
			return dbc.getLineDescriptionDB(simpleMolDB, frequencyMin,
					frequencyMax, otherThresEupMin, otherThresEupMax,
					otherThresAijMin, otherTreshAijMax);
		}
		return Collections.emptyList();
	}

	/**
	 * Return a list of all {@link LineDescriptionDB} of the the given list of
	 *  {@link SimpleMoleculeDescriptionDB} with the given parameters.
	 *
	 * @param listSimpleMolDB The list of {@link SimpleMoleculeDescriptionDB}.
	 * @param frequencyMin The minimum frequency (in MHz).
	 * @param frequencyMax The maximum frequency (in MHz).
	 * @param otherThresEupMin The minimum eup value.
	 * @param otherThresEupMax The maximum eup value.
	 * @param otherThresAijMin The minimum aij value.
	 * @param otherTreshAijMax The maximum aij value.
	 * @return a list of all {@link LineDescriptionDB} for the given parameters.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getLineDescriptionDB(java.util.List, double, double, double, double, double, double)
	 */
	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(
			List<SimpleMoleculeDescriptionDB> listSimpleMolDB, double frequencyMin,
			double frequencyMax, double otherThresEupMin, double otherThresEupMax,
			double otherThresAijMin, double otherTreshAijMax) {
		List<LineDescriptionDB> list = new ArrayList<LineDescriptionDB>();
		Map<DataBaseConnection, List<SimpleMoleculeDescriptionDB>> mapDbListSpecies =
				getSpeciesByDatabase(listSimpleMolDB);
		for (Entry<DataBaseConnection, List<SimpleMoleculeDescriptionDB>> entry :
				mapDbListSpecies.entrySet()) {
			DataBaseConnection dbc = entry.getKey();
			List<SimpleMoleculeDescriptionDB> speciesList = entry.getValue();
			list.addAll(dbc.getLineDescriptionDB(speciesList,
					frequencyMin, frequencyMax, otherThresEupMin, otherThresEupMax,
					otherThresAijMin, otherTreshAijMax));
		}
		return list;
	}

	/**
	 * Get the SimpleMoleculeDescriptionDB corresponding to the given id.
	 *
	 * @param id The molecule tag.
	 * @return The {@link SimpleMoleculeDescriptionDB} corresponding molecule tag,
	 *  or null if not found.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getSimpleMolecule(int)
	 */
	@Override
	public SimpleMoleculeDescriptionDB getSimpleMolecule(int id) {
		DataBaseConnection dbc = getDataBase(id);
		if (dbc != null) {
			return dbc.getSimpleMolecule(id);
		}
		return null;
	}

	/**
	 * Return a list of all {@link LineDescriptionDB} for the given
	 *  {@link SimpleMoleculeDescriptionDB}.
	 *
	 * @param mol The {@link SimpleMoleculeDescriptionDB}.
	 * @return the list of all {@link LineDescriptionDB} for the given
	 *  {@link SimpleMoleculeDescriptionDB}.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getLineDescriptionDB(eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB)
	 */
	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(SimpleMoleculeDescriptionDB mol) {
		DataBaseConnection dbc = getDataBase(mol.getTag());
		if (dbc != null) {
			return dbc.getLineDescriptionDB(mol);
		}
		return Collections.emptyList();
	}

	/**
	 * Return the identification of the database.
	 *
	 * @return the identification of the database.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getId()
	 */
	@Override
	public String getId() {
		return "MultiDataBaseConnection";
	}

	/**
	 * Close all database who have/can be closed.
	 *
	 * @see eu.omp.irap.cassis.database.access.ClosableDataBaseConnection#close()
	 */
	@Override
	public void close() {
		for (DataBaseConnection dbc : databases) {
			if (dbc instanceof ClosableDataBaseConnection) {
				ClosableDataBaseConnection cdbc = (ClosableDataBaseConnection) dbc;
				cdbc.close();
			}
		}
	}
}
