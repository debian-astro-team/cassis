/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.access;

import java.io.File;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import eu.omp.irap.cassis.database.DatabaseConstants;
import eu.omp.irap.cassis.database.creation.tools.integrity.Database;
import eu.omp.irap.cassis.database.creation.tools.integrity.DatabaseUtils;

/**
 * SQLite database implementation for CASSIS.
 */
public class SqliteDataBaseConnection implements ClosableDataBaseConnection {

	private static final int ITAG_INDEX_LQ = 1;
	private static final int FMHZ_INDEX_LQ = 2;
	private static final int QN_INDEX_LQ = 3;
	private static final int ERR_INDEX_LQ = 4;
	private static final int AINT_INDEX_LQ = 5;
	private static final int ELOW_INDEX_LQ = 6;
	private static final int IGU_INDEX_LQ = 7;
	private static final int GAMMA_SELF_INDEX_LQ = 8;
	private static final int EUP_INDEX_LQ = 9;

	private static final int QLOG_INDEX_QL = 1;
	private static final int TEMP_INDEX_QL = 2;

	private static final Logger logger = Logger
			.getLogger(SqliteDataBaseConnection.class.getName());

	private static final String DESCRIPTION =
			"/descriptions/sqlite_" + DatabaseConstants.DB_VERSION + ".xml";

	private Connection sqlConnUser;
	private static final String URL = "jdbc:sqlite:";
	private static final String JDBC_DRIVER = "org.sqlite.JDBC";
	private String sqliteBase;
	private boolean memoryDb;
	protected transient PreparedStatement pstmtSelectQlogs;
	public final int MAX_PARTITION_SIZE = 250;
	private List<SimpleMoleculeDescriptionDB> listSimpleMolDesDB;
	private Map<Integer, int[]> idMol;
	private PreparedStatement pstmtSelectRangeTransition;


	/**
	 * Create a new connection to a SQLite database without keeping it in memory.
	 *
	 * @param base The path of the SQLite database file.
	 */
	public SqliteDataBaseConnection(String base) {
		this(base, false);
	}

	/**
	 * Create a new connection to a SQLite database.
	 *
	 * @param base The path of the SQLite database file.
	 * @param memoryDb True if the database should be loaded in memory, false otherwise.
	 */
	public SqliteDataBaseConnection(String base, boolean memoryDb) {
		this.sqliteBase = base;
		this.memoryDb = memoryDb;
		this.idMol = new HashMap<Integer, int[]>();
		try {
			checkFileExist();
			checkDatabase();
		} catch (Exception e) {
			logger.warning(e.getMessage());
		}

	}

	/**
	 * Return the {@link MoleculeDescriptionDB} for the given molecule tag.
	 *
	 * @param tag The tag of the molecule.
	 * @return the {@link MoleculeDescriptionDB} corresponding to the given
	 *  molecule tag.
	 * @throws UnknowMoleculeException If the molecule is not found in the database.
	 */
	public MoleculeDescriptionDB getMoleculeDescriptionDB(int tag)
			throws UnknowMoleculeException {
		MoleculeDescriptionDB moleculeDescriptionDB = null;
		ResultSet rsMolecules = null;
		ResultSet rsQlogs = null;
		Statement stmt = null;
		try {
			double[] qlogsList = new double[MAX_PARTITION_SIZE];
			double[] tempList =  new double[MAX_PARTITION_SIZE];
			String queryString = "SELECT name, id_trans_min, id_trans_max FROM catdir WHERE tag= " + tag;
			stmt = getSqlConn().createStatement();

			// Select molecule with this tag
			rsMolecules = stmt.executeQuery(queryString);

			if (rsMolecules.next()) {
				String molName = rsMolecules.getString(1);
				int idTransMin = rsMolecules.getInt(2);
				int idTransMax = rsMolecules.getInt(3);

				if (!idMol.containsKey(tag)) {
					idMol.put(tag, new int[] {idTransMin, idTransMax});
				}
				rsQlogs = sqlSelectQlogs(tag);

				int cpt = 0;
				while (rsQlogs.next()) {
					qlogsList[cpt] = rsQlogs.getDouble(QLOG_INDEX_QL);
					tempList[cpt] = rsQlogs.getDouble(TEMP_INDEX_QL);
					cpt++;
				}
				qlogsList = Arrays.copyOf(qlogsList, cpt);
				tempList = Arrays.copyOf(tempList, cpt);
				moleculeDescriptionDB = new MoleculeDescriptionDB(tag, molName,
						tempList, qlogsList);
			} else {
				throw new UnknowMoleculeException(tag);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (rsMolecules != null) {
				try {
					rsMolecules.close();
				} catch (SQLException e) {
				}
			}
			if (rsQlogs != null) {
				try {
					rsQlogs.close();
				} catch (SQLException e) {
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
				}
			}
		}
		return moleculeDescriptionDB;
	}

	/**
	 * Return the {@link MoleculeDescriptionDB} for the given {@link SimpleMoleculeDescriptionDB}.
	 *
	 * @param simpleMolDB The {@link SimpleMoleculeDescriptionDB}.
	 * @return the {@link MoleculeDescriptionDB} corresponding to the given
	 *  {@link SimpleMoleculeDescriptionDB}.
	 * @throws UnknowMoleculeException If the molecule is not found in the database.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getMoleculeDescriptionDB(eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB)
	 */
	@Override
	public MoleculeDescriptionDB getMoleculeDescriptionDB(
			SimpleMoleculeDescriptionDB simpleMolDB)
			throws UnknowMoleculeException {
		return getMoleculeDescriptionDB(simpleMolDB.getTag());
	}

	/**
	 * Get the Qlogs for the given molecule tag.
	 *
	 * @param tag The molecule tag.
	 * @return The {@link ResultSet}.
	 */
	private ResultSet sqlSelectQlogs(final int tag) {
		ResultSet rsMoleculeQlogs = null;

		try {
			getPstmtSelectQlogs().setInt(1, tag);
			rsMoleculeQlogs = getPstmtSelectQlogs().executeQuery();
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "Error while selecting Qlogs of " + tag, e);
		}
		return rsMoleculeQlogs;
	}

	/**
	 * Prepare a statement to select Qlogs.
	 * <b>Warning : If the SELECT is changed, QLOG_INDEX_QL and TEMP_INDEX_QL
	 * must be modified to.</b>
	 *
	 * @return The {@link PreparedStatement}.
	 * @throws SQLException In case of SQL error.
	 */
	private PreparedStatement getPstmtSelectQlogs() throws SQLException {
		if (pstmtSelectQlogs == null) {
			String sqlSelectQlogs = "SELECT qlog, temp FROM cassis_parti_funct"
					+ " WHERE tag= ?  ORDER BY temp DESC";
			pstmtSelectQlogs = getSqlConn().prepareStatement(sqlSelectQlogs);
		}
		return pstmtSelectQlogs;
	}

	/**
	 * Create if needed then return the {@link Connection} to the SQLite database.
	 *
	 * @return the {@link Connection} to the SQLite database.
	 */
	public Connection getSqlConn() {
		String name = "";
		try {
			if (sqlConnUser == null || sqlConnUser.isClosed()) {
				try {
					Class.forName(JDBC_DRIVER);
				} catch (java.lang.ClassNotFoundException e) {
					logger.log(Level.SEVERE, "JDBC Driver not found", e);
				}

				// Connect to database
				try {
					if (new File(sqliteBase).exists()) {
						name = sqliteBase;
					} else if (sqliteBase.startsWith("./")) {
						name = DatabaseProperties.getDefaultPath()
								+ File.separator + sqliteBase.substring(2);
					} else {
						name = sqliteBase;
					}

					if (memoryDb) {
						boolean error = false;
						sqlConnUser = DriverManager.getConnection("jdbc:sqlite:");
						try (Statement stat = sqlConnUser.createStatement()) {
							stat.executeUpdate("restore from '" + name + "'");
						} catch (SQLException sqle) {
							sqle.printStackTrace();
							error = true;
						}

						if (!error) {
							try (Statement stat = sqlConnUser.createStatement()) {
								stat.executeQuery("SELECT id FROM cassis_databases");
							} catch (SQLException sqle) {
								error = true;
							}
						}

						if (error) {
							logger.log(Level.SEVERE, "Failed to use in memory database.");
							sqlConnUser = null;
						}
					}

					if (!memoryDb || sqlConnUser == null) {
						sqlConnUser = DriverManager.getConnection(URL + name);
					}
					sqlConnUser.setAutoCommit(true);
				} catch (SQLException e) {
					sqlConnUser = null;
				}
			}
		} catch (SQLException e) {
			while (e != null) {
				if (e.getErrorCode() == 1044) {
					logger.info("Unknown database : " + URL + name+ e.getMessage());
					e = e.getNextException();
				} else {
					logger.severe("Error while connecting to local cassis database...");
					logger.severe("Message: " + e.getMessage());
					logger.severe("SQLState: " + e.getSQLState());
					logger.severe("ErrorCode: " + e.getErrorCode());
					e = e.getNextException();
					logger.severe("");
				}
			}
		}

		return sqlConnUser;
	}

	/**
	 * Return the list of all {@link SimpleMoleculeDescriptionDB} who are in the database.
	 *
	 * @return the list of all {@link SimpleMoleculeDescriptionDB} who are in the database.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getAllMoleculeDescriptionDB()
	 */
	@Override
	public List<SimpleMoleculeDescriptionDB> getAllMoleculeDescriptionDB() {
		if (listSimpleMolDesDB == null) {
			listSimpleMolDesDB = new ArrayList<SimpleMoleculeDescriptionDB>(500);

			ResultSet rs = null;
			Statement stmt = null;
			String queryString = "SELECT tag, catdir.name, cassis_databases.name FROM catdir INNER JOIN cassis_databases ON catdir.id_database = cassis_databases.id";
			try {
				stmt = getSqlConn().createStatement();
				rs = stmt.executeQuery(queryString);
				SimpleMoleculeDescriptionDB smddb;
				while (rs.next()) {
					smddb = new SimpleMoleculeDescriptionDB(rs.getInt(1),
							rs.getString(2));
					smddb.setSource(rs.getString(3));
					listSimpleMolDesDB.add(smddb);
				}
			} catch (SQLException e) {
				logger.log(Level.SEVERE, "Error while selecting a molecule", e);
			} finally {
				if (rs != null) {
					try {
						rs.close();
					} catch (SQLException e) {
					}
				}
				if (stmt != null) {
					try {
						stmt.close();
					} catch (SQLException e) {
					}
				}
			}
		}
		return listSimpleMolDesDB;
	}

	/**
	 * Return the molecule name of the given molecule tag.
	 *
	 * @param molTag The molecule tag.
	 * @return the molecule name or {@link DataBaseConnection#NOT_IN_DATABASE}
	 *  if the molecule is not in database.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getMolName(int)
	 */
	@Override
	public String getMolName(int molTag) {
		String name = DataBaseConnection.NOT_IN_DATABASE;
		ResultSet sqlSelecName = null;
		Statement stmt = null;
		try {
			stmt = getSqlConn().createStatement();
			sqlSelecName = stmt.executeQuery("SELECT name FROM catdir where tag = " + molTag);
			if (sqlSelecName.next()) {
				name =  sqlSelecName.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (sqlSelecName != null) {
				try {
					sqlSelecName.close();
				} catch (SQLException e) {
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
				}
			}
		}

		return name;
	}

	/**
	 * Computation of the partition function.
	 *
	 * @param tex The temperature.
	 * @param molTag The molecule tag.
	 * @return the partition function.
	 * @throws UnknowMoleculeException If the molecule is not found in the database.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#buildQt(double, int)
	 */
	@Override
	public double buildQt(double tex, int molTag)
			throws UnknowMoleculeException {
		return getMoleculeDescriptionDB(molTag).buildQt(tex);
	}

	/**
	 * Return a list of all {@link LineDescriptionDB} of the the given
	 *  {@link SimpleMoleculeDescriptionDB} between freqMinComp and freqMaxComp.
	 *
	 * @param molecule The {@link SimpleMoleculeDescriptionDB}.
	 * @param freqMinComp The minimum frequency (in MHz).
	 * @param freqMaxComp The maximum frequency (in MHz).
	 * @return a list of all {@link LineDescriptionDB} for the given parameters.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getLineDescriptionDB(eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB, double, double)
	 */
	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(
			SimpleMoleculeDescriptionDB molecule, double freqMinComp,
			double freqMaxComp) {
		List<LineDescriptionDB> lineList = new ArrayList<LineDescriptionDB>(150);

		ResultSet rsTrans = null;
		try {
			rsTrans = sqlSelectTransitionsOfMolecules(molecule.getTag(),
					freqMinComp, freqMaxComp);
			while (rsTrans.next()) {
				lineList.add(new LineDescriptionDB(rsTrans.getInt(ITAG_INDEX_LQ),
						rsTrans.getDouble(FMHZ_INDEX_LQ), rsTrans.getString(QN_INDEX_LQ),
						rsTrans.getDouble(ERR_INDEX_LQ), rsTrans.getDouble(AINT_INDEX_LQ),
						rsTrans.getDouble(ELOW_INDEX_LQ), rsTrans.getInt(IGU_INDEX_LQ),
						rsTrans.getDouble(GAMMA_SELF_INDEX_LQ), rsTrans.getDouble(EUP_INDEX_LQ)));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rsTrans != null) {
				try {
					rsTrans.close();
				} catch (SQLException e) {
				}
			}
		}
		return lineList;
	}

	/**
	 * Create and execute a query for getting the transitions for the given
	 *  molecule tag, between the freqStart and freqEnd.
	 *
	 * @param tag The molecule tag.
	 * @param freqStart The start/minimum frequency (MHz).
	 * @param freqEnd The end/maximum frequency (MHz).
	 * @return The {@link ResultSet} for the query.
	 */
	private ResultSet sqlSelectTransitionsOfMolecules(int tag,
			final double freqStart, final double freqEnd) {
		ResultSet rsMolecule = null;
		try {
			if (!idMol.containsKey(tag)) {
				updateIndexMinMax(tag);
			}
			getPstmtSelectMoleculesRangeTransition().setInt(1,
					idMol.get(tag)[0]);
			getPstmtSelectMoleculesRangeTransition().setInt(2,
					idMol.get(tag)[1]);
			getPstmtSelectMoleculesRangeTransition().setDouble(3, freqStart);
			getPstmtSelectMoleculesRangeTransition().setDouble(4, freqEnd);
			getPstmtSelectMoleculesRangeTransition().setInt(5, tag);
			rsMolecule = getPstmtSelectMoleculesRangeTransition()
					.executeQuery();
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "Error while selecting a transition", e);
		}
		return rsMolecule;
	}

	/**
	 * Prepare a statement to select all the transition of a molecule with a
	 * frequency start and a frequency end.
	 * <b>Warning :</b> If the SELECT is changed : The something_INDEX_LQ
	 * variables must be changed and others queries using them too.
	 *
	 * @return The {@link PreparedStatement}.
	 * @throws SQLException In case of SQL error.
	 */
	private PreparedStatement getPstmtSelectMoleculesRangeTransition() throws SQLException {
		if (pstmtSelectRangeTransition == null) {
			String sqlSelectRangeTr = "SELECT itag, fMHz, qn, err, aint, elow, igu, gamma_self, eup FROM transitions "
					+ "WHERE id_transitions >= ? AND id_transitions <= ? AND fMHz >= ? AND fMHz <= ? AND itag = ?";
			pstmtSelectRangeTransition = getSqlConn().prepareStatement(sqlSelectRangeTr);
		}
		return pstmtSelectRangeTransition;
	}

	/**
	 * Return a list of all {@link LineDescriptionDB} of the the given
	 *  {@link SimpleMoleculeDescriptionDB} with the given parameters.
	 *
	 * @param simpleMolDB The {@link SimpleMoleculeDescriptionDB}.
	 * @param frequencyMin The minimum frequency (in MHz).
	 * @param frequencyMax The maximum frequency (in MHz).
	 * @param otherThresEupMin The minimum eup value.
	 * @param otherThresEupMax The maximum eup value.
	 * @param otherThresAijMin The minimum aij value.
	 * @param otherTreshAijMax The maximum aij value.
	 * @return a list of all {@link LineDescriptionDB} for the given parameters.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getLineDescriptionDB(eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB, double, double, double, double, double, double)
	 */
	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(
			SimpleMoleculeDescriptionDB simpleMolDB, double frequencyMin,
			double frequencyMax, double otherThresEupMin,
			double otherThresEupMax, double otherThresAijMin,
			double otherTreshAijMax) {
		List<LineDescriptionDB> lineList = new ArrayList<LineDescriptionDB>(150);
		ResultSet rsTransitions = null;
		Statement stmtTransitions = null;
		try {
			int tag = simpleMolDB.getTag();

			if (!idMol.containsKey(tag)) {
				updateIndexMinMax(tag);
			}

			int idTransMin = idMol.get(tag)[0];
			int idTransMax = idMol.get(tag)[1];

			String query = SqliteDataBaseConnection.createAnotherQuery(
					frequencyMin, frequencyMax, otherThresEupMin,
					otherThresEupMax, otherThresAijMin, otherTreshAijMax,
					idTransMin, idTransMax, tag);

			try {
				// Create a result set containing all data from my_table
				stmtTransitions = getSqlConn().createStatement();

				// Select molecule with this tag
				rsTransitions = stmtTransitions.executeQuery(query);
			} catch (SQLException e) {
				logger.log(Level.SEVERE, "Error while selecting a molecule", e);
			}

			while (rsTransitions.next()) {
				lineList.add(new LineDescriptionDB(rsTransitions.getInt(ITAG_INDEX_LQ),
						rsTransitions.getDouble(FMHZ_INDEX_LQ), rsTransitions.getString(QN_INDEX_LQ),
						rsTransitions.getDouble(ERR_INDEX_LQ), rsTransitions.getDouble(AINT_INDEX_LQ),
						rsTransitions.getDouble(ELOW_INDEX_LQ), rsTransitions.getInt(IGU_INDEX_LQ),
						rsTransitions.getDouble(GAMMA_SELF_INDEX_LQ), rsTransitions.getDouble(EUP_INDEX_LQ)));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rsTransitions != null) {
				try {
					rsTransitions.close();
				} catch (SQLException e) {
				}
			}
			if (stmtTransitions != null) {
				try {
					stmtTransitions.close();
				} catch (SQLException e) {
				}
			}
		}

		return lineList;
	}

	/**
	 * Create a query to get LineDescriptionDB.
	 * <b>Warning :</b> If the SELECT is changed : The something_INDEX_LQ
	 * variables must be changed and others queries using them too.
	 *
	 * @param freqStart The freqency min.
	 * @param freqEnd The frequency max.
	 * @param eupMin The eup min.
	 * @param eupMax The eup max.
	 * @param aijMin The aij min.
	 * @param aijMax The aij max.
	 * @param idTransMin The id of the transitions min.
	 * @param idTransMax The id of the transitions max.
	 * @param tag The tag.
	 * @return The query.
	 */
	private static String createAnotherQuery(double freqStart, double freqEnd,
			double eupMin, double eupMax, double aijMin, double aijMax,
			int idTransMin, int idTransMax, int tag) {
		StringBuilder query = new StringBuilder(150);
		query.append("SELECT itag, fMhz, qn, err, aint, elow, igu, gamma_self, eup FROM `transitions`");
		query.append(" WHERE id_transitions >= ").append(idTransMin);
		query.append(" AND id_transitions <= ").append(idTransMax);
		query.append(" AND fMHz >= ").append(freqStart);
		query.append(" AND fMHz <= ").append(freqEnd);
		if (eupMin > 0.0) {
			query.append(" AND eup >= ").append(eupMin);
		}
		if (eupMax != Double.MAX_VALUE) {
			query.append(" AND eup <= ").append(eupMax);
		}
		if (aijMin > 0.0) {
			query.append(" AND aint >= ").append(aijMin);
		}
		if (aijMax != Double.MAX_VALUE) {
			query.append(" AND aint <= ").append(aijMax);
		}
		query.append(" AND itag = ").append(tag);
		query.append(";");
		return query.toString();
	}

	/**
	 * Create a query to get LineDescriptionDB.
	 * <b>Warning :</b> If the SELECT is changed : The something_INDEX_LQ
	 * variables must be changed and others queries using them too.
	 *
	 * @param listSimpleMolDB The list of molecules we want.
	 * @param frequencyMin The frequency min.
	 * @param frequencyMax The frequency max.
	 * @param otherThresEupMin The eup min.
	 * @param otherThresEupMax The eup max.
	 * @param otherThresAijMin The aij min.
	 * @param otherTreshAijMax The aij max.
	 * @return The query.
	 */
	private static String createQuery(List<SimpleMoleculeDescriptionDB> listSimpleMolDB,
			double frequencyMin, double frequencyMax, double otherThresEupMin,
			double otherThresEupMax, double otherThresAijMin,
			double otherTreshAijMax) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT itag, fMHz, qn, err, aint, elow, igu, gamma_self, eup FROM `transitions` WHERE itag IN (");
		int i = 0;
		for (; i < listSimpleMolDB.size() - 1; i++) {
			sb.append(listSimpleMolDB.get(i).getTag()).append(", ");
		}
		if (!listSimpleMolDB.isEmpty()) {
			sb.append(listSimpleMolDB.get(i).getTag());
		}

		sb.append(") AND fMHz >= ").append(frequencyMin);
		sb.append(" AND fMHz <= ").append(frequencyMax);
		if (otherThresEupMin > 0.0) {
			sb.append(" AND eup >= ").append(otherThresEupMin);
		}
		if (otherThresEupMax != Double.MAX_VALUE) {
			sb.append(" AND eup <= ").append(otherThresEupMax);
		}
		if (otherThresAijMin > 0.0) {
			sb.append(" AND aint >= ").append(otherThresAijMin);
		}
		if (otherTreshAijMax != Double.MAX_VALUE) {
			sb.append(" AND aint <= ").append(otherTreshAijMax);
		}
		sb.append(";");

		return sb.toString();
	}

	/**
	 * Return a list of all {@link LineDescriptionDB} of the the given list of
	 *  {@link SimpleMoleculeDescriptionDB} with the given parameters.
	 *
	 * @param listSimpleMolDB The list of {@link SimpleMoleculeDescriptionDB}.
	 * @param frequencyMin The minimum frequency (in MHz).
	 * @param frequencyMax The maximum frequency (in MHz).
	 * @param otherThresEupMin The minimum eup value.
	 * @param otherThresEupMax The maximum eup value.
	 * @param otherThresAijMin The minimum aij value.
	 * @param otherTreshAijMax The maximum aij value.
	 * @return a list of all {@link LineDescriptionDB} for the given parameters.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getLineDescriptionDB(java.util.List, double, double, double, double, double, double)
	 */
	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(
			List<SimpleMoleculeDescriptionDB> listSimpleMolDB,
			double frequencyMin, double frequencyMax, double otherThresEupMin,
			double otherThresEupMax, double otherThresAijMin,
			double otherTreshAijMax) {
		List<LineDescriptionDB> lineDescriptionDBs = new ArrayList<LineDescriptionDB>(500);

		String query = SqliteDataBaseConnection.createQuery(listSimpleMolDB,
				frequencyMin, frequencyMax, otherThresEupMin, otherThresEupMax,
				otherThresAijMin, otherTreshAijMax);

		ResultSet rsTransitions = null;
		Statement stmt = null;
		try {
			stmt = getSqlConn().createStatement();
			rsTransitions = stmt.executeQuery(query);

			while (rsTransitions.next()) {
				lineDescriptionDBs.add(new LineDescriptionDB(rsTransitions.getInt(ITAG_INDEX_LQ),
						rsTransitions.getDouble(FMHZ_INDEX_LQ), rsTransitions.getString(QN_INDEX_LQ),
						rsTransitions.getDouble(ERR_INDEX_LQ), rsTransitions.getDouble(AINT_INDEX_LQ),
						rsTransitions.getDouble(ELOW_INDEX_LQ), rsTransitions.getInt(IGU_INDEX_LQ),
						rsTransitions.getDouble(GAMMA_SELF_INDEX_LQ), rsTransitions.getDouble(EUP_INDEX_LQ)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (rsTransitions != null) {
				try {
					rsTransitions.close();
				} catch (SQLException e) {
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
				}
			}
		}
		return lineDescriptionDBs;
	}

	/**
	 * Close reference of the database.
	 *
	 * @see java.lang.Object#finalize()
	 */
	@Override
	public void finalize() throws Throwable {
		if (pstmtSelectRangeTransition != null) {
			pstmtSelectRangeTransition.close();
		}

		if (pstmtSelectQlogs != null) {
			pstmtSelectQlogs.close();
		}

		sqlConnUser.close();
		super.finalize();
	}

	/**
	 * Check if a file is a sqlite database for CASSIS.
	 *
	 * @param pathFile The absolute path of the file to test.
	 * @return if it is a sqlite database for CASSIS.
	 */
	public static boolean checkSQLiteFile(String pathFile) {
		if (!new File(pathFile).exists()) {
			return false;
		}

		Connection connection = null;
		Statement stat = null;

		try {
			Class.forName(JDBC_DRIVER);
		} catch (ClassNotFoundException e) {
			return false;
		}
		try {
			connection = DriverManager.getConnection(URL + pathFile);
			stat = connection.createStatement();
			stat.executeQuery("SELECT * FROM cassis_databases");
		} catch (SQLException e) {
			return false;
		} finally {
			if (stat != null) {
				try {
					stat.close();
				} catch (SQLException e) {
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
				}
			}
		}
		return true;
	}

	/**
	 * This is not implemented yet.
	 *
	 * @param molTag The molecule tag.
	 * @return This is not implemented.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getDataBaseOf(int)
	 */
	@Override
	public String getDataBaseOf(int molTag) {
		return "TODO";
	}

	/**
	 * Return the identification of the database, the file name.
	 *
	 * @return the identification of the database, the file name.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getId()
	 */
	@Override
	public String getId() {
		return new File(sqliteBase).getName();
	}

	/**
	 * Get the SimpleMoleculeDescriptionDB corresponding to the given id.
	 *
	 * @param id The molecule tag.
	 * @return The {@link SimpleMoleculeDescriptionDB} corresponding molecule tag,
	 *  or null if not found.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getSimpleMolecule(int)
	 */
	@Override
	public SimpleMoleculeDescriptionDB getSimpleMolecule(int id) {
		List<SimpleMoleculeDescriptionDB> allMoleculeDescriptionDB = getAllMoleculeDescriptionDB();
		for (SimpleMoleculeDescriptionDB simpleMoleculeDescriptionDB : allMoleculeDescriptionDB) {
			if (simpleMoleculeDescriptionDB.getTag() == id) {
				return simpleMoleculeDescriptionDB;
			}
		}
		return null;
	}

	/**
	 * Close the database.
	 *
	 * @see eu.omp.irap.cassis.database.access.ClosableDataBaseConnection#close()
	 */
	@Override
	public void close() {
		if (pstmtSelectRangeTransition != null) {
			try {
				pstmtSelectRangeTransition.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (pstmtSelectQlogs != null) {
			try {
				pstmtSelectQlogs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (sqlConnUser != null) {
			try {
				sqlConnUser.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Return a list of all {@link LineDescriptionDB} for the given
	 *  {@link SimpleMoleculeDescriptionDB}.
	 *
	 * @param mol The {@link SimpleMoleculeDescriptionDB}.
	 * @return the list of all {@link LineDescriptionDB} for the given
	 *  {@link SimpleMoleculeDescriptionDB}.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getLineDescriptionDB(eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB)
	 */
	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(SimpleMoleculeDescriptionDB mol) {
		return getLineDescriptionDB(mol, Double.MIN_VALUE, Double.MAX_VALUE);
	}

	/**
	 * Check the schema of the database and throws an {@link DatabaseException}
	 *  if the schema is wrong from what we expect.
	 */
	private void checkDatabase() {
		InputStream is = SqliteDataBaseConnection.class.getResourceAsStream(DESCRIPTION);
		if (is == null) {
			throw new DatabaseException("No description found",
					DatabaseException.ERROR.NO_DESCRIPTION);
		} else {
			if (getSqlConn() != null) {
				Database exp = DatabaseUtils.createDatabaseFromXml(is);
				Database db = DatabaseUtils.createDatabaseFromSqlite(getSqlConn());
				if (!db.contains(exp)) {
					throw new DatabaseException("Wrong schema used.",
							DatabaseException.ERROR.WRONG_SCHEMA);
				}
			} else {
				throw new DatabaseException("No connection",
						DatabaseException.ERROR.NO_CONNECTION);
			}
		}
	}

	/**
	 * Update the indexes min and max of the transitions of the species with the given tag.
	 *
	 * @param tag The tag of the species.
	 */
	private void updateIndexMinMax(int tag) {
		ResultSet rsMolecules = null;
		Statement stmt = null;
		try {
			if (!idMol.containsKey(tag)) {
				String queryString = "SELECT id_trans_min, id_trans_max FROM catdir WHERE tag= " + tag;
				stmt = getSqlConn().createStatement();

				// Select molecule with this tag
				rsMolecules = stmt.executeQuery(queryString);

				if (rsMolecules.next()) {
					int idTransMin = rsMolecules.getInt(1);
					int idTransMax = rsMolecules.getInt(2);
					idMol.put(tag, new int[] {idTransMin, idTransMax});
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (rsMolecules != null) {
				try {
					rsMolecules.close();
				} catch (SQLException e) {
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
				}
			}
		}
	}

	/**
	 * Check if the database file exist and throw a DatabaseException if the
	 *  file does not exist.
	 */
	private void checkFileExist() throws DatabaseException {
		if (!new File(sqliteBase).exists()) {
			throw new DatabaseException("The database file does not exist",
					DatabaseException.ERROR.NO_CONNECTION);
		}
	}

	@Override
	public PartitionFunction getPartitionFunction(int molTag, boolean force) throws UnknowMoleculeException {
		return PartitionFunction.getPartitionFunction(getMoleculeDescriptionDB(molTag));
	}
}
