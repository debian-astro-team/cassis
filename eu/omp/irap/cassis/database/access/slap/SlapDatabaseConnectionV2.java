/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.access.slap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import eu.omp.irap.cassis.common.Formula;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.database.access.LineDescriptionDB;
import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB;
import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB.TypeSpecies;
import eu.omp.irap.vespa.epntapclient.votable.model.Field;
import eu.omp.irap.vespa.epntapclient.votable.model.Info;
import eu.omp.irap.vespa.epntapclient.votable.model.Resource;
import eu.omp.irap.vespa.epntapclient.votable.model.Table;
import eu.omp.irap.vespa.epntapclient.votable.model.VOTABLE;
import eu.omp.irap.vespa.votable.utils.CantSendQueryException;
import eu.omp.irap.vespa.votable.votable.VOTableException;
import eu.omp.irap.vespa.votable.votable.VOTableException.CantParseVOTableException;
import eu.omp.irap.vespa.votable.votable.VOTableXMLParser;
import eu.omp.irap.vespa.votable.votabledata.VOTableData;
import eu.omp.irap.vespa.votable.votabledata.VOTableDataParser;


public class SlapDatabaseConnectionV2 extends DefaultSlapDataBaseConnection{

	public static final String GET_SPECIES = "species";
	public static final String GET_LINES = "lines";
	private List<SimpleMoleculeDescriptionDB> listSimpleMolDescDb;
	protected static int cassisSlapTag = 20000;
	private static final Logger LOGGER = Logger.getLogger(SlapDatabaseConnectionV2.class.getName());
	public static final double ELOW_MAX_OTHER_SPECIES = Double.MAX_VALUE;

	public SlapDatabaseConnectionV2(String url) {
		super(url);
	}

	/**
	 * We lack this information on SLAP online database, return a List with only a
	 * fake molecule.
	 *
	 * @return return a List with only a fake molecule.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getAllMoleculeDescriptionDB()
	 */
	@Override
	public List<SimpleMoleculeDescriptionDB> getAllMoleculeDescriptionDB() {
		if (listSimpleMolDescDb == null) {
			map.clear();
			listSimpleMolDescDb = new ArrayList<SimpleMoleculeDescriptionDB>(1);
			String request = createSpeciesQuery(url);
			String result= null;
			if (!local){
				try {
					result = getResultFile(request);
				} catch (CantSendQueryException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else {
				result = url;
			}
			VOTABLE voTable = null;
			try {
				voTable = VOTableXMLParser.parse(result);
			} catch (CantParseVOTableException e) {
				e.printStackTrace();
			}
			String serviceName = "???";
			List<Resource> resources = voTable.getRESOURCE();
			for (Resource resource : resources) {
				if ("results".equalsIgnoreCase(resource.getType())) {
					List<Info> infos = resource.getINFO();
					for (Info info : infos) {
						if ("SERVICE_NAME".equals(info.getName())) {
							serviceName = info.getValueAttribute();
						}
					}
					break;
				}
			}



			Table table = (Table)voTable.getRESOURCE().get(0).getLINKAndTABLEOrRESOURCE().get(0);
			VOTableDataParser dataParser = new VOTableDataParser("Slap data", table);

			final int SPECIES_NAME = 0;
			final int ION_CHARGE = 1;
			final int SPECIES_TYPE = 2;
			final int INCHIKEY = 3;


			List<Field> fieldsNodes = dataParser.getFieldsNodes();
			String[] names =  new String[] { "SPECIES_NAME", "ION_CHARGE",
											"SPECIES_TYPE",
					   					    "INCHIKEY"};
			String[] ucds =  new String[] {"phys.atmol.element", "phys.atmol.ionization",
										   "",
										   ""};
			String[] utypes = new String[] {"ssldm:Species.name", "ssldm:Species.ionCharge",
											"ssldm:Species.type",
											"ssldm:Species.inChiKey"};
			int nbParameters = names.length;
			int[] index = new int[nbParameters];
			Arrays.fill(index, -1);
			for (int i = 0; i < fieldsNodes.size(); i++) {
				Field field  = fieldsNodes.get(i);
				String attributeName = field.getName()==null?"":field.getName();
				String attributeUcd = field.getUcd()==null?"":field.getUcd();
				String attributeUtype = field.getUtype()==null?"":field.getUtype();


				for (int j = 0; j < nbParameters; j++) {
					if (!attributeName.isEmpty() && attributeName.equalsIgnoreCase(names[j]) ||
						!attributeUcd.isEmpty() && attributeUcd.equalsIgnoreCase(ucds[j]) ||
						!attributeUtype.isEmpty() && attributeUtype.equalsIgnoreCase(utypes[j])) {
						index[j] = i;
						break;
					}
				}
			}


			try {
				TypeSpecies typeSpecies;
				int tag = 0;
				String name = "";
				String moleculeId = "";
				String ionCharge = "";
				VOTableData parseData = dataParser.parseData();
				int nbRows = parseData.getNbRows();
				for (int i = 0; i < nbRows; i++) {
					name = parseData.getCell(i, index[SPECIES_NAME]).toString();
					typeSpecies = SimpleMoleculeDescriptionDB.getTypeSpecies(
							parseData.getCell(i, index[SPECIES_TYPE]).toString());
					moleculeId = parseData.getCell(i, index[INCHIKEY]).toString();
					ionCharge = parseData.getCell(i, index[ION_CHARGE]).toString();
					tag = cassisSlapTag++;
					if (!"0".equals(ionCharge)) {
						name = name + "  " + ionCharge;
					}
					SimpleMoleculeDescriptionDB molecule = new SimpleMoleculeDescriptionDB(tag, name, moleculeId);
					map.put(tag, name);
					molecule.setSource(serviceName);
					molecule.setType(typeSpecies);
					listSimpleMolDescDb.add(molecule);
				}


			} catch (VOTableException e) {
				e.printStackTrace();
			}
		}

		return listSimpleMolDescDb;

	}


	private String createSpeciesQuery(String url) {
		String res = url;
		if (!url.endsWith("/")) {
			res = res + "/" + GET_SPECIES;
		}else {
			res = res + GET_SPECIES;
		}
		return res;
	}

	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(List<SimpleMoleculeDescriptionDB> mols, double freqMinComp,
			double freqMaxComp) {

		List<LineDescriptionDB> lines = new ArrayList<LineDescriptionDB>();
		final int WAVELENGTH_INDICE = 0;
//		final int IDENTIFICATION_INDICE = 1;
		final int LOWER_LEVEL_ENERGY_INDICE = 2;
		final int UPPER_LEVEL_ENERGY_INDICE = 3;
//		final int LOWER_LEVEL_NAME_INDICE = 4;
		final int UPPER_LEVEL_NAME_INDICE = 5;
		final int EINSTEIN_A_INDICE = 6;
		final int LOWER_LEVEL_ELEMENT_INDICE = 7;
//		final int LOWER_LEVEL_IONCHARGE_INDICE = 8;
//		final int LOWER_LEVEL_ELEMENT_INCHIKEY_INDICE = 9;

		String request = createLinesQuery(url, freqMinComp, freqMaxComp);
		LOGGER.warning("request=" + request);
		String result = null;
		if (!local) {
			try {
				result = getResultFile(request);
			} catch (CantSendQueryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			result = url;
		}

		VOTABLE voTable = null;
		try {
			voTable = VOTableXMLParser.parse(result);
		} catch (CantParseVOTableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Table table = (Table) voTable.getRESOURCE().get(0).getLINKAndTABLEOrRESOURCE().get(0);
		VOTableDataParser dataParser = new VOTableDataParser("Slap data", table);

		List<Field> fieldsNodes = dataParser.getFieldsNodes();
		String[] names = new String[] { "WAVELENGTH", "IDENTIFICATION", "LOWER_LEVEL_ENERGY", "UPPER_LEVEL_ENERGY",
				"LOWER_LEVEL_NAME", "UPPER_LEVEL_NAME", "EINSTEIN_A", "LOWER_LEVEL_ELEMENT", "LOWER_LEVEL_IONCHARGE",
				"LOWER_LEVEL_ELEMENT_INCHIKEY"};
		String[] ucds = new String[] { "", "", "", "",
				"", "", "", "", "",
				""};
		String[] utypes = new String[] { "ssldm:Line.wavelength.value", "ssldm:Line.title",
				"ssldm:Line.lowerLevel.energy.value", "ssldm:Line.upperLevel.energy.value",
				"ssldm:Line.lowerLevel.name", "ssldm:Line.upperLevel.name",
				"ssldm:Line.einsteinA.value", "ssldm:Line.lowerLevel.element.name",
				"ssldm:Line.lowerLevel.element.ionCharge",
				"ssldm:Line.lowerLevel.element.inChiKey"};

		int[] index = new int[ucds.length];
		Arrays.fill(index, -1);
		for (int i = 0; i < fieldsNodes.size(); i++) {
			Field field = fieldsNodes.get(i);
			String attributeName = field.getName() == null ? "" : field.getName();
			String attributeUcd = field.getUcd() == null ? "" : field.getUcd();
			String attributeUtype = field.getUtype() == null ? "" : field.getUtype();

			for (int j = 0; j < utypes.length; j++) {
				if (!attributeName.isEmpty() && attributeName.equalsIgnoreCase(names[j])
						|| !attributeUtype.isEmpty() && attributeUtype.equalsIgnoreCase(utypes[j])
						|| !attributeUcd.isEmpty() && attributeUcd.equalsIgnoreCase(ucds[j])
						) {
					index[j] = i;
					break;
				}
			}
		}
		UNIT levelEnergyUnit = UNIT.JOULE;
		if (index[UPPER_LEVEL_ENERGY_INDICE] != -1) {
			levelEnergyUnit = UNIT.toUnit(fieldsNodes.get(index[UPPER_LEVEL_ENERGY_INDICE]).getUnit());

		}

		try {
			dataParser.parseData();
		} catch (VOTableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		VOTableData data = dataParser.getData();
		for (int i = 0; i < data.getNbRows(); i++) {

			String name = data.getColumn(index[LOWER_LEVEL_ELEMENT_INDICE]).get(i).toString();
			if (isInList(mols, name)) {
				double frequency = Double.valueOf(data.getColumn(index[WAVELENGTH_INDICE]).get(i).toString());
				frequency = convertMeterToMhz(frequency) ;

				String quantumNumbers = data.getColumn(index[UPPER_LEVEL_NAME_INDICE]).get(i).toString();
				double error = 0;
				double eup = 0;
				double aint = 0;
				double eLow = 0;
				try {
					aint = Double.valueOf(data.getColumn(index[EINSTEIN_A_INDICE]).get(i).toString());
				} catch (NumberFormatException e) {
				}
				try {
					eup = Double.valueOf(data.getColumn(index[UPPER_LEVEL_ENERGY_INDICE]).get(i).toString());
					if (levelEnergyUnit.equals(UNIT.JOULE)) {
						eup = Formula.calcEUpk(eup);
					} else if (levelEnergyUnit.equals(UNIT.CM_MINUS_1)) {
						eup = Formula.calcEUpKComet(eup);
					}
				} catch (NumberFormatException e) {
				}
				int tag = addMolRefIfNeeded(name,  "");
				try {
					eLow = Double.valueOf(data.getColumn(index[LOWER_LEVEL_ENERGY_INDICE]).get(i).toString());
				} catch (NumberFormatException e) {
				}
				eLow = Formula.calcEUpk(eLow);

				if (eLow < ELOW_MAX_OTHER_SPECIES) {
					LineDescriptionDB line = new LineDescriptionDB(tag, frequency, quantumNumbers, error, aint, .0, 0, .0, eup);
					lines.add(line);
				}
			}
		}

		return lines;

	}

	/**
	 * Create the query.
	 *
	 * @param url The URL of the database.
	 * @param freqMin The minimum frequency.
	 * @param freqMax The maximum frequency.
	 * @return the query.
	 */
	protected static String createLinesQuery(String url, double freqMin, double freqMax) {
		String formatedUrl = url;

		if (!url.endsWith("/")) {
			formatedUrl = formatedUrl + "/" + GET_LINES;
		}else {
			formatedUrl = formatedUrl + GET_LINES;
		}


		if (!formatedUrl.contains("?")) {
			formatedUrl += "?";
		} else if (url.charAt(url.length() - 1) != '?'
				&& url.charAt(url.length() - 1) != '&') {
			formatedUrl += "&";
		}

		double maxWavelength = convertMhzToMeter(freqMin);
		double minWavelength = convertMhzToMeter(freqMax);

		String tmpString = String.valueOf(minWavelength) + "%20"
				+ String.valueOf(maxWavelength);

		return formatedUrl + "WAVELENGTH=" + tmpString;
	}


}
