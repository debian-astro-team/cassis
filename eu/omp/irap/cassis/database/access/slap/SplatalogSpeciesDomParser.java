/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.access.slap;

import java.io.File;
import java.util.HashSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SplatalogSpeciesDomParser {

	/**
	 * Parse a part of the file like http://www.cv.nrao.edu/php/splat/advanced.php
	 * with the list of molecules
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			//http://www.cv.nrao.edu/php/splat/advanced.php
			File inputFile = new File(args[0]);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			NodeList nList = doc.getElementsByTagName("option");
			System.out.println("----------------------------" + nList.getLength());
			HashSet<String> hashSet = new HashSet<String>();

			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
//               System.out.println("CLass : "
//                  + eElement.getAttribute("class"));
					String textContent = eElement.getTextContent();

					hashSet.add(eElement.getAttribute("class"));

//			System.out.println("First Name : " + textContent);
					String[] split = textContent.split(" - ", 2);

					String[] split2 = split[0].split(" ", 2);
					String name = split2[1];
//					String val2 = split[1];
					if (split[1].contains(" - ")) {
						String[] split3 = split[1].split(" -", 2);
						name = name + " - " + split3[0];
//						val2 = split3[1];
					}
//					System.out.println(split2[0] + "\t\t " + val1 + "\t\t " + val2);
					Integer tag = Integer.valueOf(split2[0]);
					System.out.println("list.add(new SimpleMoleculeDescriptionDB(" +
								tag + ", \"" +  name+ "\"));");
					System.out.println("map.put("
							+ tag
							+ ", \""
							+ name
							+ "\");");
				}
			}
//			for (String string : hashSet) {
//				System.out.println(string);
//			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
