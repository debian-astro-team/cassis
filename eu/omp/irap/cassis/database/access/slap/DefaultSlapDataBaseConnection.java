/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/**
 *
 */
package eu.omp.irap.cassis.database.access.slap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import eu.omp.irap.cassis.database.access.DataBaseConnection;
import eu.omp.irap.cassis.database.access.LineDescriptionDB;
import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB;
import eu.omp.irap.cassis.database.access.SlapDataBaseConnection;
import eu.omp.irap.vespa.epntapclient.votable.model.Field;
import eu.omp.irap.vespa.epntapclient.votable.model.Table;
import eu.omp.irap.vespa.epntapclient.votable.model.VOTABLE;
import eu.omp.irap.vespa.votable.utils.CantSendQueryException;
import eu.omp.irap.vespa.votable.votable.VOTableException;
import eu.omp.irap.vespa.votable.votable.VOTableXMLParser;
import eu.omp.irap.vespa.votable.votable.VOTableException.CantParseVOTableException;
import eu.omp.irap.vespa.votable.votabledata.VOTableData;
import eu.omp.irap.vespa.votable.votabledata.VOTableDataParser;

/**
 * @author jglorian
 *
 */
public class DefaultSlapDataBaseConnection extends SlapDataBaseConnection {

	private static final Logger LOGGER = Logger.getLogger(DefaultSlapDataBaseConnection.class.getName());
	private static final String ALL_MOLECULES = "ALL MOLECULES";
	private static final int TAG_MOL = -1;

	public DefaultSlapDataBaseConnection(String url) {
		this.url = url;
		this.map = new HashMap<Integer, String>();
	}

	public DefaultSlapDataBaseConnection(String url, boolean local) {
		this(url);
		this.local = local;
	}

	/**
	 * Get the SimpleMoleculeDescriptionDB corresponding to the given id.
	 *
	 * @param id The molecule tag.
	 * @return The {@link SimpleMoleculeDescriptionDB} corresponding molecule tag,
	 *         or null if not found.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getSimpleMolecule(int)
	 */
	@Override
	public SimpleMoleculeDescriptionDB getSimpleMolecule(int id) {
		List<SimpleMoleculeDescriptionDB> allMoleculeDescriptionDB = getAllMoleculeDescriptionDB();
		for (SimpleMoleculeDescriptionDB simpleMoleculeDescriptionDB : allMoleculeDescriptionDB) {
			if (simpleMoleculeDescriptionDB.getTag() == id) {
				return simpleMoleculeDescriptionDB;
			}

		}
		return null;
	}

	/**
	 * Return the molecule name of the given molecule tag.
	 *
	 * @param molTag The molecule tag.
	 * @return the molecule name or {@link DataBaseConnection#NOT_IN_DATABASE} if
	 *         the molecule is not in database.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getMolName(int)
	 */
	@Override
	public String getMolName(int molTag) {
		if (map.containsKey(molTag)) {
			return map.get(molTag);
		}
		return NOT_IN_DATABASE;
	}


	/**
	 * Return all {@link LineDescriptionDB} between freqMinComp and freqMaxComp.
	 *
	 * @param molecule    Not used here.
	 * @param freqMinComp The minimum frequency (in MHz).
	 * @param freqMaxComp The maximum frequency (in MHz).
	 * @return all {@link LineDescriptionDB} between freqMinComp and freqMaxComp.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getLineDescriptionDB(eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB,
	 *      double, double)
	 */
	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(SimpleMoleculeDescriptionDB molecule, double freqMinComp,
			double freqMaxComp) {
		List<SimpleMoleculeDescriptionDB> mols = new ArrayList<SimpleMoleculeDescriptionDB>(1);
		mols.add(molecule);
		return getLineDescriptionDB(mols, freqMinComp, freqMaxComp);
	}

	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(List<SimpleMoleculeDescriptionDB> mols, double freqMinComp,
			double freqMaxComp) {
		List<LineDescriptionDB> lines = new ArrayList<LineDescriptionDB>();
		final int CATALOGUE_INDICE = 0;
		final int NAME_INDICE = 1;
		final int FREQ_INDICE = 2;
		final int QN_INDICE = 3;
		final int EINSTEINA_INDICE = 4;
		final int EUPK_INDICE = 5;

		String request = createLinesQuery(url, freqMinComp, freqMaxComp);
		LOGGER.warning("request=" + request);
		String result = null;
		if (!local) {
			try {
				result = getResultFile(request);
			} catch (CantSendQueryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			result = url;
		}

		VOTABLE voTable = null;
		try {
			voTable = VOTableXMLParser.parse(result);
		} catch (CantParseVOTableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Table table = (Table) voTable.getRESOURCE().get(0).getLINKAndTABLEOrRESOURCE().get(0);
		VOTableDataParser dataParser = new VOTableDataParser("Slap data", table);

		List<Field> fieldsNodes = dataParser.getFieldsNodes();
		String[] names = new String[] { "catalog name", "chemicalname", "frequency", "quantum numbers", "aij",
				"upperstateenergyK" };
		String[] ucds = new String[] { "", "phys.atmol.element", "em.freq;spect.line",
				"phys.atmol.transition;spect.line", "", "" };
		String[] utypes = new String[] { "", "ssldm:Line.initialElement.name", "", "", "ssldm:Line.einsteinA", "" };

		int[] index = new int[ucds.length];
		Arrays.fill(index, -1);
		for (int i = 0; i < fieldsNodes.size(); i++) {
			Field field = fieldsNodes.get(i);
			String attributeName = field.getName() == null ? "" : field.getName();
			String attributeUcd = field.getUcd() == null ? "" : field.getUcd();
			String attributeUtype = field.getUtype() == null ? "" : field.getUtype();

			for (int j = 0; j < utypes.length; j++) {
				if (!attributeName.isEmpty() && attributeName.equalsIgnoreCase(names[j])
						|| !attributeUcd.isEmpty() && attributeUcd.equalsIgnoreCase(ucds[j])
						|| !attributeUtype.isEmpty() && attributeUtype.equalsIgnoreCase(utypes[j])) {
					index[j] = i;
					break;
				}
			}
		}

		try {
			dataParser.parseData();
		} catch (VOTableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		VOTableData data = dataParser.getData();
		for (int i = 0; i < data.getNbRows(); i++) {

			String catalogueName = data.getColumn(index[CATALOGUE_INDICE]).get(i).toString();
			String name = data.getColumn(index[NAME_INDICE]).get(i).toString();
			if (isInList(mols, name)) {
				double frequency = Double.valueOf(data.getColumn(index[FREQ_INDICE]).get(i).toString());
				String quantumNumbers = data.getColumn(index[QN_INDICE]).get(i).toString();
				double error = 0;
				double eup = 0;
				double aint = 0;
				try {
					aint = Double.valueOf(data.getColumn(index[EINSTEINA_INDICE]).get(i).toString());
				} catch (NumberFormatException e) {
				}
				try {
					eup = Double.valueOf(data.getColumn(index[EUPK_INDICE]).get(i).toString());
				} catch (NumberFormatException e) {
				}
				int tag = addMolRefIfNeeded(name, catalogueName + " : " + name + " ");
				lines.add(new LineDescriptionDB(tag, frequency, quantumNumbers, error, aint, .0, 0, .0, eup));
			}
		}

		return lines;

	}

	protected  static boolean isInList(List<SimpleMoleculeDescriptionDB> mols, String name) {
		if (mols.size() == 1 && mols.get(0).getName().equals(ALL_MOLECULES)) {
			return true;
		}
		for (SimpleMoleculeDescriptionDB molecule : mols) {
			if (molecule.getName().equals(name)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * We lack this information on SLAP online database, return a List with only a
	 * fake molecule.
	 *
	 * @return return a List with only a fake molecule.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getAllMoleculeDescriptionDB()
	 */
	@Override
	public List<SimpleMoleculeDescriptionDB> getAllMoleculeDescriptionDB() {
		List<SimpleMoleculeDescriptionDB> list = new ArrayList<SimpleMoleculeDescriptionDB>(1);
		list.add(new SimpleMoleculeDescriptionDB(TAG_MOL, ALL_MOLECULES));
		return list;
	}

	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(List<SimpleMoleculeDescriptionDB> listSimpleMolDB,
			double frequencyMin, double frequencyMax, double otherThresEupMin, double otherThresEupMax,
			double otherThresAijMin, double otherTreshAijMax) {

		List<LineDescriptionDB> result = new ArrayList<LineDescriptionDB>();
		for (SimpleMoleculeDescriptionDB simpleMolDB : listSimpleMolDB) {
			result.addAll(getLineDescriptionDB(simpleMolDB, frequencyMin, frequencyMax));
		}
		return result;
	}



}
