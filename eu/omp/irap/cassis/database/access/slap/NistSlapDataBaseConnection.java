/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.access.slap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import eu.omp.irap.cassis.database.access.LineDescriptionDB;
import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB;
import eu.omp.irap.vespa.epntapclient.votable.model.Field;
import eu.omp.irap.vespa.epntapclient.votable.model.Table;
import eu.omp.irap.vespa.epntapclient.votable.model.VOTABLE;
import eu.omp.irap.vespa.votable.utils.CantSendQueryException;
import eu.omp.irap.vespa.votable.votable.VOTableException;
import eu.omp.irap.vespa.votable.votable.VOTableException.CantParseVOTableException;
import eu.omp.irap.vespa.votable.votable.VOTableXMLParser;
import eu.omp.irap.vespa.votable.votabledata.VOTableData;
import eu.omp.irap.vespa.votable.votabledata.VOTableDataParser;

/**
 * SLAP database implementation for CASSIS for NIST
 */
public class NistSlapDataBaseConnection extends DefaultSlapDataBaseConnection {

	// NIST ATOMIC SPECTRA http://physics.nist.gov/cgi-bin/ASD/slap.pl?

	/**
	 * Create a new connection to a SLAP database.
	 *
	 * @param url The URL of the database.
	 */
	public NistSlapDataBaseConnection(String url) {
		super(url);
	}


	@Override
	public String getDataBaseOf(int molTag) {
		return "NIST SLAP";
	}



	/**
	 * Return all {@link LineDescriptionDB} between freqMinComp and freqMaxComp.
	 *
	 * @param molecule Not used here.
	 * @param freqMinComp The minimum frequency (in MHz).
	 * @param freqMaxComp The maximum frequency (in MHz).
	 * @return all {@link LineDescriptionDB} between freqMinComp and freqMaxComp.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getLineDescriptionDB(eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB, double, double)
	 */
	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(
			SimpleMoleculeDescriptionDB molecule, double freqMinComp,
			double freqMaxComp) {
		List<LineDescriptionDB> lines = new ArrayList<LineDescriptionDB>();
		final int WAVE_LENGTH = 0;
		final int TITLE = 1;
		final int INI_LEV = 2;
		final int FIN_LEV = 3;


			String request = createLinesQuery(url, freqMinComp, freqMaxComp);
			String result= null;
			if (!local){
				try {
					result = getResultFile(request);
				} catch (CantSendQueryException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else {
				result = url;
			}
			VOTABLE voTable = null;
			try {
				voTable = VOTableXMLParser.parse(result);
			} catch (CantParseVOTableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Table table = (Table)voTable.getRESOURCE().get(0).getLINKAndTABLEOrRESOURCE().get(0);
			VOTableDataParser dataParser = new VOTableDataParser("Slap data", table);

			List<Field> fieldsNodes = dataParser.getFieldsNodes();
			String[] names =  new String[] { "Wavelength",
											"Title",
					   					    "IniLev", "FinLev"};
			String[] ucds =  new String[] {"",
										   "",
										   "", ""};
			String[] utypes = new String[] {"",
											"",
											"", ""};

			int[] index = new int[ucds.length];
			Arrays.fill(index, -1);
			for (int i = 0; i < fieldsNodes.size(); i++) {
				Field field  = fieldsNodes.get(i);
				String attributeName = field.getName()==null?"":field.getName();
				String attributeUcd = field.getUcd()==null?"":field.getUcd();
				String attributeUtype = field.getUtype()==null?"":field.getUtype();


				for (int j = 0; j < utypes.length; j++) {
					if (!attributeName.isEmpty() && attributeName.equalsIgnoreCase(names[j]) ||
						!attributeUcd.isEmpty() && attributeUcd.equalsIgnoreCase(ucds[j]) ||
						!attributeUtype.isEmpty() && attributeUtype.equalsIgnoreCase(utypes[j])) {
						index[j] = i;
						break;
					}
				}
			}


			try {
				dataParser.parseData();
			} catch (VOTableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			VOTableData data = dataParser.getData();
			for (int i = 0; i < data.getNbRows(); i++) {
				String catalogueName = "NIST";

				String name = extractName(data.getColumn(index[TITLE]).get(i).toString());
				double frequency = convertMeterToMhz(Double.valueOf(data.getColumn(index[WAVE_LENGTH]).get(i).toString()));
				String quantumNumbers = data.getColumn(index[FIN_LEV]).get(i).toString() + "-"+data.getColumn(index[INI_LEV]).get(i).toString() ;
				double error =0;
				double eup =0;
				double aint = 0;
				int tag = addMolRefIfNeeded(name, catalogueName + " : "+name + " ");
				lines.add(new LineDescriptionDB(tag,frequency, quantumNumbers, error,
						aint, .0, 0, .0, eup));

			}

		return lines;

	}


	/**
	 * Extract from the title of Nist slam data the name of  the species
	 * @param title
	 * @return
	 */
	private String extractName(String title) {
		String[] split = title.split(" ");
		return split[0] + " " + split[1];
	}
}
