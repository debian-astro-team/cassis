/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.access;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class allowing to manage history of the databases used.
 *
 * @author M. Boiziot
 */
public class DatabaseHistory {

	private static final int INDICE_TYPE = 0;
	private static final int INDICE_PATH = 1;
	private File dbHistoryFile;
	private long lastReadTime = -1;
	private Map<TypeDataBase, List<String>> map;
	private static DatabaseHistory instance;


	/**
	 * Creates a DatabaseHistory. <b>Warning</b> this should not be used
	 *  directly in CASSIS for getting the usual database history file.
	 *  Use {@link DatabaseHistory#getInstance()} instead.
	 *
	 * @param filePath The path of the file to use.
	 */
	public DatabaseHistory(String filePath) {
		dbHistoryFile = new File(filePath);
		map = new HashMap<TypeDataBase, List<String>>();
	}

	/**
	 * Create an instance if needed then return it.
	 *
	 * @return The instance of DatabaseHistory.
	 */
	public static DatabaseHistory getInstance() {
		if (instance == null) {
			instance = new DatabaseHistory(DatabaseProperties.getHistoryPath()
					+ File.separator
					+ DatabaseProperties.DATABASE_HISTORY_FILE_NAME);
		}
		return instance;
	}

	/**
	 * Return a list of path readed in the file for database type provided.
	 *
	 * @param typeDb The type of database from we want the list of path.
	 * @return The list of path for the TypeDataBase provided.
	 */
	public List<String> getDb(TypeDataBase typeDb) {
		if (readFile() && map.containsKey(typeDb)) {
			return map.get(typeDb);
		}
		return new ArrayList<String>(0);
	}

	/**
	 * Add a database type with it's path to the history file.
	 *
	 * @param typeDb The TypeDataBase.
	 * @param path The path of the database.
	 * @return If it was added to the file.
	 */
	public boolean addDatabase(TypeDataBase typeDb, String path) {
		if (typeDb == TypeDataBase.NO) {
			return false;
		}

		clear();
		readFile();
		if (exists(typeDb, path)) {
			return false;
		}

		if (typeDb == TypeDataBase.SQLITE && !new File(path).exists()) {
			return false;
		}

		try (FileWriter fw = new FileWriter(dbHistoryFile, true)) {
			String string = typeDb + "\t" + path + '\n';
			fw.append(string);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Read the file.
	 *
	 * @return if the file was correctly readed.
	 */
	private boolean readFile() {
		if (dbHistoryFile.exists()) {
			if (dbHistoryFile.lastModified() > lastReadTime) {
				lastReadTime = System.currentTimeMillis();
				map.clear();

				try (BufferedReader br = new BufferedReader(new InputStreamReader(
						new FileInputStream(dbHistoryFile)))) {
					String line = br.readLine();
					String[] obj;
					TypeDataBase type;
					String path;
					while (line != null) {
						obj = line.split("\t");
						if (obj.length >= 2) {
							type = TypeDataBase.valueOf(obj[INDICE_TYPE]);
							path = obj[INDICE_PATH].trim();

							if (!map.containsKey(type)) {
								map.put(type, new ArrayList<String>());
							}

							if (!map.get(type).contains(path)) {
								map.get(type).add(path);
							}
						}
						line = br.readLine();
					}
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Check if the provided database exist.
	 *
	 * @param typeDb The TypeDataBase.
	 * @param path The path of database.
	 * @return if it exists.
	 */
	public boolean exists(TypeDataBase typeDb, String path) {
		return map.containsKey(typeDb) && map.get(typeDb).contains(path);
	}

	/**
	 * Check if the provided database exist.
	 *
	 * @param path The path of database.
	 * @return if it exists.
	 */
	public boolean exists(String path) {
		for (List<String> list : map.values()) {
			if (list.contains(path)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Clear the list of known databases.
	 */
	public void clear() {
		map.clear();
		lastReadTime = -1;
	}
}
