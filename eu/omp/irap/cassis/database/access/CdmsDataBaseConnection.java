/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.access;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CdmsDataBaseConnection extends FileDataBaseConnection{

	final static double[] TEMPERATURES = {1000, 500, 300, 225, 150, 75, 37.5, 18.75, 9.375, 5.000, 2.725};

	public CdmsDataBaseConnection(String path) {
		super(path);
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.cassis.database.DataBaseConnection#getAllMoleculeDescriptionDB()
	 */
	@Override
	public List<SimpleMoleculeDescriptionDB> getAllMoleculeDescriptionDB() {
		if (speciesDescriptionDB == null) {
			speciesDescriptionDB = new ArrayList<SimpleMoleculeDescriptionDB>();


			try (BufferedReader catalogReader = new BufferedReader(new InputStreamReader(
					new URL(path + "partition_function.html").openStream()))) {

				String line = null;
				do {
					try {
						line = catalogReader.readLine();
						if (line != null && !line.trim().equals("") && line.length() >= 153 && line.charAt(0) != '<' &&
							!line.startsWith(" tag") && !line.startsWith("======")) {

							final MoleculeDescriptionDB mol = createMolecule(line);
							speciesDescriptionDB.add(mol);
						}
					} catch (IOException e) {
						line = null;
					}
				} while (line != null);

			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		return speciesDescriptionDB;
	}

	/**
	 * @param line
	 * @return
	 */
	protected static MoleculeDescriptionDB createMolecule(String line) {
		int tag = Integer.parseInt(line.substring(0, 6).trim());
		double[] zlp = new double[TEMPERATURES.length];

		String moleculeName = line.substring(7, 29).trim();
		String subline = line.substring(30, 38).trim();

		int indice = 0;
		double[] newTemp = new double[TEMPERATURES.length];
		double qlogAienstein300 = 0;


		for (int k = 0; k <TEMPERATURES.length; k++) {
			subline = line.substring(39 + k * 13, 51 + k * 13).trim();
			if (!subline.startsWith("--")) {
				zlp[indice] = Double.parseDouble(subline);
				newTemp[indice] = TEMPERATURES[k];
				indice ++;
			}
			if (k== 2){ //300K
				qlogAienstein300 = Double.parseDouble(subline);
			}
		}
		newTemp = Arrays.copyOf(newTemp, indice);
		zlp = Arrays.copyOf(zlp, indice);

		final MoleculeDescriptionDB mol = new MoleculeDescriptionDB(tag, moleculeName,
				newTemp, zlp);

		mol.setQlogAienstein300(qlogAienstein300);
		return mol;
	}

}
