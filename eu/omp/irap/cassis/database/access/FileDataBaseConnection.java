/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.access;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import eu.omp.irap.cassis.common.Formula;
import eu.omp.irap.cassis.database.FileDatabaseUtils;

public class FileDataBaseConnection implements DataBaseConnection {

	protected String path;
	protected ArrayList<SimpleMoleculeDescriptionDB> speciesDescriptionDB;


	public FileDataBaseConnection(String path) {
		if (path != null && !path.startsWith("file:")) {
			this.path = "file:" + path;
		} else {
			this.path = path;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * eu.omp.irap.cassis.database.DataBaseConnection#getMoleculeDescriptionDB
	 * (cassis.database.SimpleMoleculeDescriptionDB)
	 */
	@Override
	public MoleculeDescriptionDB getMoleculeDescriptionDB(
			SimpleMoleculeDescriptionDB simpleMoleculDB) throws UnknowMoleculeException {
		MoleculeDescriptionDB mol = getMoleculeDescriptionDB(simpleMoleculDB.getTag());
		if (mol == null) {
			throw new UnknowMoleculeException(simpleMoleculDB.getTag());
		}
		return mol;
	}

	private MoleculeDescriptionDB getMoleculeDescriptionDB(int tag) {
		List<SimpleMoleculeDescriptionDB> allMoleculeDescriptionDB = getAllMoleculeDescriptionDB();
		for (SimpleMoleculeDescriptionDB moleculeDescriptionDB : allMoleculeDescriptionDB) {
			if (moleculeDescriptionDB.getTag() == tag) {
				return (MoleculeDescriptionDB) moleculeDescriptionDB;
			}
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.cassis.database.DataBaseConnection#getMolName(int)
	 */
	@Override
	public String getMolName(int molTag) {
		final SimpleMoleculeDescriptionDB moleculeDescriptionDB = getMoleculeDescriptionDB(molTag);
		if (moleculeDescriptionDB == null) {
			return DataBaseConnection.NOT_IN_DATABASE;
		}
		return moleculeDescriptionDB.getName();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see eu.omp.irap.cassis.database.DataBaseConnection#buildQt(double, int)
	 */
	@Override
	public double buildQt(double tex, int molTag) throws UnknowMoleculeException {
		MoleculeDescriptionDB mol = getMoleculeDescriptionDB(molTag);
		if (mol == null) {
			throw new UnknowMoleculeException(molTag);
		}
		return mol.buildQt(tex);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see eu.omp.irap.cassis.database.getPartitionFunction#buildQt(int)
	 */
	@Override
	public PartitionFunction getPartitionFunction(int molTag, boolean force) throws UnknowMoleculeException {
		MoleculeDescriptionDB mol = getMoleculeDescriptionDB(molTag);
		if (mol == null) {
			throw new UnknowMoleculeException(molTag);
		}
		return PartitionFunction.getPartitionFunction(mol);
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * eu.omp.irap.cassis.database.DataBaseConnection#getLineDescriptionDB(cassis
	 * .database.MoleculeDescriptionDB, double, double)
	 */
	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(
			SimpleMoleculeDescriptionDB simplMolecule, double freqMinComp,
			double freqMaxComp) {
		MoleculeDescriptionDB molecule = (MoleculeDescriptionDB)simplMolecule;

		String nameFile =  path + FileDatabaseUtils.getCatNameFileTag(molecule.getTag());

		List<LineDescriptionDB> lineDescriptionDBs = new ArrayList<LineDescriptionDB>();
		try (BufferedReader catalogReader = new BufferedReader(new InputStreamReader(new URL(nameFile).openStream()))) {
			boolean finish = false;
			String line = null;
			do {
				line = catalogReader.readLine();
				if (line == null || line.equals("")) {
					break;
				}

				double fMHz = Double.parseDouble(line.substring(0, 13).trim());
				if (fMHz > freqMaxComp) {
					finish = true;
				} else if (fMHz < freqMinComp) {
					continue;
				} else {
					int tag = molecule.getTag();
					double error = Double.parseDouble(line.substring(13, 21).trim());
					String quanticNumbers = extractNbQuantic(line, tag);
					double elow =  Double.parseDouble(line.substring(31, 41).trim());

					final double eLowJ = Formula.calcELowJ(elow, 0.0);
					final double eUpJ = Formula.calcEUpJ(eLowJ, fMHz);

					double eup = eUpJ / Formula.K;

					int igu = Integer.parseInt(tradCaractere(line.substring(41, 44).trim()));
					double gammaself = Integer.parseInt(line.substring(44, 51).trim());
					double aint = Double.parseDouble(line.substring(21, 29).trim());
					final double zlp0 = 300.;

					double zp0 = molecule.buildQt(zlp0);
					if (molecule.getQlogAienstein300() != 0) {
						zp0 =  Math.pow(10.0, molecule.getQlogAienstein300());
					}

					double aEinstein = getAEinstein(aint, fMHz, zp0, zlp0, igu, eLowJ, eUpJ, tag);
					LineDescriptionDB lineDescriptionDB =
							new LineDescriptionDB(tag, fMHz, quanticNumbers, error, aEinstein, elow,
									igu, gammaself, eup);
					lineDescriptionDBs.add(lineDescriptionDB);
				}

			} while (!finish);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return lineDescriptionDBs;
	}

	/**
	 * @param line
	 * @param tag the tag of the molecule of the transitions
	 * @return String representing the nbquantic
	 */
	public static String extractNbQuantic(final String line, final int tag) {
		if (tag==33502) {//methanols
			return line.substring(56, 58).trim() + ":"+ line.substring(58, 61).trim()+ ":"+
								line.substring(61, 64).trim()+ ":"+  line.substring(64, 67).trim()+ ":"+
								line.substring(67, 70).trim()+ ":"+  line.substring(70, 73).trim()+ ":"+
								line.substring(73, 76).trim()+ ":"+  line.substring(76, line.length()).trim();
		}

		int indiceUp = 55; //c l'inverse --> correspond to begin up
		String subline = line.substring(51, 55).trim();

		if (tag == 39003) { //no space beetween tag and qnq
			indiceUp = 54;
			subline = line.substring(50, 54).trim();
		}

		int iqnform = Integer.parseInt(subline);
		int nqn = iqnform %10;

		int h = (iqnform %100) / 10;
		int nbQuanticMax = 6;

		if (nqn > nbQuanticMax) {
			nbQuanticMax = nqn;
		}


		int indiceLow = indiceUp + nbQuanticMax*2; // --> correspond to begin lnw

		String nquTest = line.substring(indiceUp + 2 * (nqn-1), indiceUp + 2*nqn).trim();
		if (nquTest.equals("")) {
			nqn--;
		}
		final String[] result = new String[nqn*2];
		for (int i = 0; i < nqn; i++) {
			String nqu = line.substring(indiceUp, indiceUp + 2).trim();
			if (nqu.equals("")) {
				nqu = " ";
			} else {
				nqu = tradCaractere(nqu);
			}
			result[i] = nqu;// qup
			indiceUp += 2;

			String nql = line.substring(indiceLow, indiceLow + 2).trim();
			if (nql.equals("")) {
				nql = " ";
			} else {
				nql = tradCaractere(nql);
			}
			result[i + nqn] = nql;// qul
			indiceLow += 2;

		}
		int hi = 0;

		//for i : 0..2 Nbquantic(last-i) =  Nbquantic(last-i) - 0.5 if last bit -i of h = 1
		for (int i =0; i < 3; i++) {
			hi = (h/(int)Math.pow(2.0, i))%2;
			if (hi ==1) {
				result[nqn-i-1] = String.valueOf(Double.parseDouble(result[nqn-i-1])-0.5);
				result[nqn * 2 -i -1] = String.valueOf(Double.parseDouble(result[nqn *2 -i -1])-0.5);
			}
		}
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < result.length - 1; i++) {
			sb.append(result[i]).append(':');
		}
		sb.append(result[result.length-1]);
		return sb.toString();
	}

	public static String tradCaractere(String j) {
		try {
			final String letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			final String lettersMin = "abcdefghijklmnopqrstuvwxyz";

			// --- replace A->10, B->11, etc.
			final char car = j.charAt(0);

			StringBuilder sb = new StringBuilder();

			int index = Integer.MIN_VALUE;
			for (int k = 0; k < 26; k++) {
				if (car==letters.charAt(k) || car==lettersMin.charAt(k)) {
					if (car==lettersMin.charAt(k)) {
						index = -(k+1);
					} else{
						index = k + 10;
					}
					sb.append(index);

					break;
				}
			}
			if (index != Integer.MIN_VALUE) {
				for (int cpt = 1; cpt < j.length(); cpt++) {
					sb.append(j.charAt(cpt));
				}

				j = sb.toString();
			}
		} catch (StringIndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return j;
	}

	private double getAEinstein(double aint, double fMHz, double zp0,
			double zlp0, double igu, double eLowJ, double eUpJ, double tag) {

		final double i300 = Math.pow(10.0, aint);
		double aEinstein = Formula.calcAEinstein(i300, fMHz, zp0, igu, eLowJ,
				zlp0, eUpJ);

		// --- special case for CH3OH CDMS
		if (tag == 33502) {
			// --- sgMu2 : [?] (Formula *P4A)
			final double sgMu2 = Math.pow(10.0, aint);
			// --- a : Einstein constant for this transition (Formula *P5A)
			aEinstein = 1.16395e-20 * fMHz * fMHz * fMHz * sgMu2 / igu;
		}
		return aEinstein;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * eu.omp.irap.cassis.database.DataBaseConnection#getLineDescriptionDB(cassis
	 * .database.SimpleMoleculeDescriptionDB, double, double, double, double,
	 * double, double)
	 */
	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(
			SimpleMoleculeDescriptionDB simpleMolDB, double frequencyMin,
			double frequencyMax, double otherThresEupMin,
			double otherThresEupMax, double otherThresAijMin,
			double otherTreshAijMax) {
		List<LineDescriptionDB> lineDescriptionDBs = new ArrayList<LineDescriptionDB>();
		List<LineDescriptionDB> lineDescriptionDB = getLineDescriptionDB(simpleMolDB, frequencyMin,
				frequencyMax);

		for (LineDescriptionDB lineDescriptionDB2 : lineDescriptionDB) {
			if (lineDescriptionDB2.getEup() > otherThresEupMin
					&& lineDescriptionDB2.getEup() < otherThresEupMax
					&& lineDescriptionDB2.getAint() > otherThresAijMin
					&& lineDescriptionDB2.getAint() < otherTreshAijMax) {
				lineDescriptionDBs.add(lineDescriptionDB2);
			}

		}
		return lineDescriptionDBs;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * eu.omp.irap.cassis.database.DataBaseConnection#getAllMoleculeDescriptionDB
	 * ()
	 */
	@Override
	public List<SimpleMoleculeDescriptionDB> getAllMoleculeDescriptionDB() {
		if (speciesDescriptionDB == null) {
			speciesDescriptionDB = new ArrayList<SimpleMoleculeDescriptionDB>();
			try (BufferedReader catalogReader = new BufferedReader(new InputStreamReader(
					new URL(path + "catdir.cat").openStream()))) {
				int tag = -1;
				String line = null;
				do {
					try {
						line = catalogReader.readLine();
						if (line != null && !line.trim().equals("")) {
							tag = Integer.parseInt(line.substring(0, 6).trim());
							double[] temperatures = { 300, 225, 150, 75, 37.5,
									18.75, 9.375 };
							double[] zlp = new double[temperatures.length];
							String moleculeName = line.substring(7, 20).trim();
							String subline = line.substring(20, 26).trim();
							for (int k = 0; k < 7; k++) {
								subline = line
										.substring(27 + k * 7, 33 + k * 7)
										.trim();
								zlp[k] = Double.valueOf(subline);
							}

							final MoleculeDescriptionDB mol = new MoleculeDescriptionDB(
									tag, moleculeName, temperatures, zlp);
							speciesDescriptionDB.add(mol);

							double qlogAienstein300 = zlp[0];
							if (line.length() >= 84) {
								subline = line.substring(79, 84).trim();
								if (subline.compareTo("") != 0) {
									qlogAienstein300 = Double.valueOf(subline);

								}
							}
							mol.setQlogAienstein300(qlogAienstein300);
						}
					} catch (IOException e) {
						e.printStackTrace();
						line = null;
					}
				} while (line != null);

			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		return Collections.unmodifiableList(speciesDescriptionDB);
	}

	@Override
	public String getDataBaseOf(int molTag) {
		return "TODO";
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * eu.omp.irap.cassis.database.DataBaseConnection#getLineDescriptionDB(java
	 * .util.List, double, double, double, double, double, double)
	 */
	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(
			List<SimpleMoleculeDescriptionDB> listSimpleMolDB,
			double frequencyMin, double frequencyMax, double otherThresEupMin,
			double otherThresEupMax, double otherThresAijMin,
			double otherTreshAijMax) {

		List<LineDescriptionDB> lineDescriptionDBFinal = new ArrayList<LineDescriptionDB>();
		List<LineDescriptionDB> lineDescriptionDBs = new ArrayList<LineDescriptionDB>();
		MoleculeDescriptionDB mol;
		for (SimpleMoleculeDescriptionDB simpleMolDB : listSimpleMolDB) {
			mol = getMoleculeDescriptionDB(simpleMolDB.getTag());
			if (mol != null) {
				lineDescriptionDBs.addAll(getLineDescriptionDB(mol,
						frequencyMin, frequencyMax));
			}
		}

		for (LineDescriptionDB lineDescriptionDB2 : lineDescriptionDBs) {
			if (lineDescriptionDB2.getEup() > otherThresEupMin
					&& lineDescriptionDB2.getEup() < otherThresEupMax
					&& lineDescriptionDB2.getAint() > otherThresAijMin
					&& lineDescriptionDB2.getAint() < otherTreshAijMax) {
				lineDescriptionDBFinal.add(lineDescriptionDB2);
			}
		}

		return lineDescriptionDBFinal;
	}

	@Override
	public String getId() {
		return new File(path).getName();
	}

	@Override
	public SimpleMoleculeDescriptionDB getSimpleMolecule(int id) {
		List<SimpleMoleculeDescriptionDB> allMoleculeDescriptionDB = getAllMoleculeDescriptionDB();
		for (SimpleMoleculeDescriptionDB simpleMoleculeDescriptionDB : allMoleculeDescriptionDB) {
			if (simpleMoleculeDescriptionDB.getTag() == id) {
				return simpleMoleculeDescriptionDB;
			}

		}
		return null;
	}

	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(SimpleMoleculeDescriptionDB mol) {
		return getLineDescriptionDB(mol, Double.MIN_VALUE, Double.MAX_VALUE);
	}
}
