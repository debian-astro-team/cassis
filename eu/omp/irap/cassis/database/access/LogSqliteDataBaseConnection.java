/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.access;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * @author glorian
 *
 */
public class LogSqliteDataBaseConnection extends SqliteDataBaseConnection {

	private BufferedWriter writer;

	public LogSqliteDataBaseConnection(String base) {
		this(base, false);

	}

	public LogSqliteDataBaseConnection(String dbName, boolean inMemoryDb) {
		super(dbName, inMemoryDb);
		try {
			if (!Files.exists(Paths.get("logsqlite.txt"))) {
				Files.createFile(Paths.get("logsqlite.txt"));
			}
			writer = Files.newBufferedWriter(Paths.get("logsqlite.txt"), Charset.defaultCharset(),
					StandardOpenOption.APPEND);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void finalize() throws Throwable {
		super.finalize();
		writer.close();
	}

	@Override
	public MoleculeDescriptionDB getMoleculeDescriptionDB(SimpleMoleculeDescriptionDB simpleMolDB)
			throws UnknowMoleculeException {
		final MoleculeDescriptionDB moleculeDescriptionDB = super.getMoleculeDescriptionDB(simpleMolDB);
		try {
			writer.append("Tag=" + simpleMolDB.getTag());
			writer.newLine();
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return moleculeDescriptionDB;
	}

	@Override
	public List<SimpleMoleculeDescriptionDB> getAllMoleculeDescriptionDB() {
		final List<SimpleMoleculeDescriptionDB> allMoleculeDescriptionDB = super.getAllMoleculeDescriptionDB();
		try {

			for (SimpleMoleculeDescriptionDB simpleMoleculeDescriptionDB : allMoleculeDescriptionDB) {
				writer.append("Tag=" + simpleMoleculeDescriptionDB.getTag());
				writer.newLine();
				writer.flush();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return allMoleculeDescriptionDB;
	}

	@Override
	public MoleculeDescriptionDB getMoleculeDescriptionDB(int tag) throws UnknowMoleculeException {
		MoleculeDescriptionDB moleculeDescriptionDB = super.getMoleculeDescriptionDB(tag);
		try {
			writer.append("Tag=" + moleculeDescriptionDB.getTag());
			writer.newLine();
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return moleculeDescriptionDB;
	}

	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(SimpleMoleculeDescriptionDB molecule, double freqMinComp,
			double freqMaxComp) {
		final List<LineDescriptionDB> lineDescriptionDB = super.getLineDescriptionDB(molecule, freqMinComp,
				freqMaxComp);
		try {
			for (LineDescriptionDB lineDescriptionDB2 : lineDescriptionDB) {
				writer.append("tag=" + lineDescriptionDB2.getTag());
				writer.newLine();
				writer.append("lines=" + lineDescriptionDB2.getFrequency());
				writer.newLine();
				writer.flush();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return lineDescriptionDB;
	}

	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(SimpleMoleculeDescriptionDB simpleMolDB, double frequencyMin,
			double frequencyMax, double otherThresEupMin, double otherThresEupMax, double otherThresAijMin,
			double otherTreshAijMax) {
		final List<LineDescriptionDB> lineDescriptionDB = super.getLineDescriptionDB(simpleMolDB, frequencyMin,
				frequencyMax, otherThresEupMin, otherThresEupMax, otherThresAijMin, otherTreshAijMax);
		try {
			for (LineDescriptionDB lineDescriptionDB2 : lineDescriptionDB) {
				writer.append("tag=" + lineDescriptionDB2.getTag());
				writer.newLine();
				writer.append("lines=" + lineDescriptionDB2.getFrequency());
				writer.newLine();
				writer.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return lineDescriptionDB;
	}

	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(List<SimpleMoleculeDescriptionDB> listSimpleMolDB,
			double frequencyMin, double frequencyMax, double otherThresEupMin, double otherThresEupMax,
			double otherThresAijMin, double otherTreshAijMax) {
		final List<LineDescriptionDB> lineDescriptionDB = super.getLineDescriptionDB(listSimpleMolDB, frequencyMin,
				frequencyMax, otherThresEupMin, otherThresEupMax, otherThresAijMin, otherTreshAijMax);
		try {
			for (LineDescriptionDB lineDescriptionDB2 : lineDescriptionDB) {
				writer.append("tag=" + lineDescriptionDB2.getTag());
				writer.newLine();
				writer.append("lines=" + lineDescriptionDB2.getFrequency());
				writer.newLine();
				writer.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return lineDescriptionDB;
	}

	/**
	 * Create a config to create a sqlite database from a log sqlite file
	 *
	 * @param args
	 * 		args[0] : url of the database we want to extract
	 * 		args[1] : log sqlite file
	 * 		args[2]: name of the output dbconf file
	 *
	 */
	public static void main(String[] args) {
		String dbName = args[0];
		DataBaseConnection createDataBaseConnection = AccessDataBase.createDataBaseConnection(TypeDataBase.SQLITE,
				dbName, false);
		List<SimpleMoleculeDescriptionDB> allMoleculeDescriptionDB = createDataBaseConnection
				.getAllMoleculeDescriptionDB();

		int[] tags = new int[allMoleculeDescriptionDB.size()];
		for (int i = 0; i < tags.length; i++) {
			tags[i] = allMoleculeDescriptionDB.get(i).getTag();

		}
		TreeMap<Integer, ArrayList<Double>> map = new TreeMap<Integer, ArrayList<Double>>();
		try (BufferedReader reader = Files.newBufferedReader(Paths.get(args[1]))) {

			String readLine = reader.readLine();
			while (readLine != null) {
				if (readLine.startsWith("tag")) {
					Integer tagInteger = Integer.valueOf(readLine.replace("tag=", ""));
					if (!map.containsKey(tagInteger)) {
						map.put(tagInteger, new ArrayList<Double>());
					}
					readLine = reader.readLine();
					Double freq = Double.valueOf(readLine.replace("lines=", ""));
					if (!map.get(tagInteger).contains(freq))
						map.get(tagInteger).add(freq);
				}
				readLine = reader.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(args[2]))){

			addLine(writer, "1Name=" + new File(dbName).getName());
			addLine(writer, "1Path=" + dbName);
			addLine(writer, "1Type=SQLITE");
			addLine(writer, "1Enabled=true");

			int cptFilters = 1;
			StringBuffer buffer = new StringBuffer();
			for (int i = 0; i < tags.length; i++) {
				if (map.keySet().contains(tags[i])) {
					buffer.append("t");
				} else {
					buffer.append("f");
				}
			}
			addLine(writer, "1Active="+ buffer.toString());
			addLine(writer, "AutoUpdate=f");
			for (Integer key : map.keySet()) {

				for (Double freq : map.get(key)) {
					addLine(writer, "#Filters " + cptFilters);
					addLine(writer, "Tag_" + cptFilters + "=" + key);

					addLine(writer, "Constraint_" + cptFilters + "_1_Restrictable=FREQUENCY");
					addLine(writer, "Constraint_" + cptFilters + "_1_Type=EQUAL");
					addLine(writer, "Constraint_" + cptFilters + "_1_Value=" + freq);
					addLine(writer, "");
					cptFilters++;

				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}


	}

	private static void addLine(BufferedWriter writer, String val) throws IOException {
		writer.append(val );
		writer.newLine();
		writer.flush();
	}



}
