/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.access;

import java.util.ArrayList;
import java.util.List;

/**
 * Empty database implementation.
 */
public class NoDataBaseConnection implements DataBaseConnection {

	/*
	 * Throw an exception as there is no molecule in this database type.
	 *
	 * @param simpleMolDB Not used here.
	 * @return a fake {@link MoleculeDescriptionDB}.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getMoleculeDescriptionDB(eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB)
	 * @throws UnknowMoleculeException Always.
	 */
	@Override
	public MoleculeDescriptionDB getMoleculeDescriptionDB(
			SimpleMoleculeDescriptionDB simpleMolDB) throws UnknowMoleculeException {
		throw new UnknowMoleculeException(simpleMolDB.getTag());
	}

	/**
	 * Return an empty List of {@link SimpleMoleculeDescriptionDB}.
	 *
	 * @return an empty List of {@link SimpleMoleculeDescriptionDB}.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getAllMoleculeDescriptionDB()
	 */
	@Override
	public List<SimpleMoleculeDescriptionDB> getAllMoleculeDescriptionDB() {
		return new ArrayList<SimpleMoleculeDescriptionDB>();
	}

	/**
	 * Return {@value DataBaseConnection#NOT_IN_DATABASE}.
	 *
	 * @param molTag Not used here.
	 * @return {@value DataBaseConnection#NOT_IN_DATABASE}.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getMolName(int)
	 */
	@Override
	public String getMolName(int molTag) {
		return NOT_IN_DATABASE;
	}

	/**
	 * Throw an exception, there is not species here.
	 *
	 * @param tex Not used here.
	 * @param molTag Not used here.
	 * @return Nothing as there is an exception.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#buildQt(double, int)
	 * @throws UnknowMoleculeException Always.
	 */
	@Override
	public double buildQt(double tex, int molTag) throws UnknowMoleculeException {
		throw new UnknowMoleculeException(molTag);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see eu.omp.irap.cassis.database.getPartitionFunction#buildQt(int)
	 */

	@Override
	public PartitionFunction getPartitionFunction(int molTag, boolean force) throws UnknowMoleculeException {
		throw new UnknowMoleculeException(molTag);
	}


	/*
	 * Return an empty List of {@link LineDescriptionDB}.
	 *
	 * @param molecule Not used here.
	 * @param freqMinComp Not used here.
	 * @param freqMaxComp Not used here.
	 * @return an empty List of {@link LineDescriptionDB}.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getLineDescriptionDB(eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB, double, double)
	 */
	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(
			SimpleMoleculeDescriptionDB molecule, double freqMinComp,
			double freqMaxComp) {
		return new ArrayList<LineDescriptionDB>();
	}

	/**
	 * Return an empty List of {@link LineDescriptionDB}.
	 *
	 * @param simpleMolDB Not used here.
	 * @param frequencyMin Not used here.
	 * @param frequencyMax Not used here.
	 * @param otherThresEupMin Not used here.
	 * @param otherThresEupMax Not used here.
	 * @param otherThresAijMin Not used here.
	 * @param otherTreshAijMax Not used here.
	 * @return an empty List of {@link LineDescriptionDB}.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getLineDescriptionDB(eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB, double, double, double, double, double, double)
	 */
	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(
			SimpleMoleculeDescriptionDB simpleMolDB, double frequencyMin,
			double frequencyMax, double otherThresEupMin,
			double otherThresEupMax, double otherThresAijMin,
			double otherTreshAijMax) {
		return new ArrayList<LineDescriptionDB>();
	}

	/**
	 * This is not implemented.
	 *
	 * @param molTag Not used here.
	 * @return This is not implemented.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getDataBaseOf(int)
	 */
	@Override
	public String getDataBaseOf(int molTag) {
		return "TODO";
	}

	/**
	 * Return an empty List of {@link LineDescriptionDB}.
	 *
	 * @param listSimpleMolDB Not used here.
	 * @param frequencyMin Not used here.
	 * @param frequencyMax Not used here.
	 * @param otherThresEupMin Not used here.
	 * @param otherThresEupMax Not used here.
	 * @param otherThresAijMin Not used here.
	 * @param otherTreshAijMax Not used here.
	 * @return an empty List of {@link LineDescriptionDB}.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getLineDescriptionDB(java.util.List, double, double, double, double, double, double)
	 */
	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(
			List<SimpleMoleculeDescriptionDB> listSimpleMolDB,
			double frequencyMin, double frequencyMax, double otherThresEupMin,
			double otherThresEupMax, double otherThresAijMin,
			double otherTreshAijMax) {
		return new ArrayList<LineDescriptionDB>();
	}

	/**
	 * Return "None".
	 *
	 * @return "None".
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getId()
	 */
	@Override
	public String getId() {
		return "None";
	}

	/**
	 * Return null, there is nothing in this database.
	 *
	 * @param id Not used here.
	 * @return <code>null</code>.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getSimpleMolecule(int)
	 */
	@Override
	public SimpleMoleculeDescriptionDB getSimpleMolecule(int id) {
		return null;
	}

	/**
	 * Return an empty List of {@link LineDescriptionDB}.
	 *
	 * @param mol Not used here.
	 * @return an empty List of {@link LineDescriptionDB}.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getLineDescriptionDB(eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB)
	 */
	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(SimpleMoleculeDescriptionDB mol) {
		return getLineDescriptionDB(mol, Double.MIN_VALUE, Double.MAX_VALUE);
	}

}
