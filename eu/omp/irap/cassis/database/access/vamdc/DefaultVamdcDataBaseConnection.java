/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.access.vamdc;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

import org.vamdc.xsams.schema.AtomType;
import org.vamdc.xsams.schema.AtomicIonType;
import org.vamdc.xsams.schema.AtomicStateType;
import org.vamdc.xsams.schema.Atoms;
import org.vamdc.xsams.schema.DataSeriesType;
import org.vamdc.xsams.schema.DataType;
import org.vamdc.xsams.schema.ElementSymbolType;
import org.vamdc.xsams.schema.IsotopeType;
import org.vamdc.xsams.schema.MolecularStateType;
import org.vamdc.xsams.schema.MoleculeType;
import org.vamdc.xsams.schema.Molecules;
import org.vamdc.xsams.schema.PartitionFunctionType;
import org.vamdc.xsams.schema.Radiative;
import org.vamdc.xsams.schema.RadiativeTransitionType;
import org.vamdc.xsams.schema.ValueType;
import org.vamdc.xsams.schema.WlType;
import org.vamdc.xsams.schema.XSAMSData;

import eu.omp.irap.cassis.common.Formula;
import eu.omp.irap.cassis.common.axes.XAxisWaveLength;
import eu.omp.irap.cassis.common.axes.XAxisWaveNumber;
import eu.omp.irap.cassis.database.access.LineDescriptionDB;
import eu.omp.irap.cassis.database.access.MoleculeDescriptionDB;
import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB;
import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB.TypeSpecies;
import eu.omp.irap.cassis.database.access.UnknowMoleculeException;
import eu.omp.irap.cassis.database.access.VamdcDataBaseConnection;

public class DefaultVamdcDataBaseConnection extends VamdcDataBaseConnection{


	protected static int cassisVamdcTag = 100000;
	final private String MINUS_PLUS = "±";
	public DefaultVamdcDataBaseConnection(String url) {
		this.url = url;
	}




	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * eu.omp.irap.cassis.database.DataBaseConnection#getAllMoleculeDescriptionDB
	 * ()
	 */
	@Override
	public List<SimpleMoleculeDescriptionDB> getAllMoleculeDescriptionDB() {
		if (speciesDBs == null) {
			speciesDBs = new ArrayList<SimpleMoleculeDescriptionDB>();
			String query = url + ALL_SPECIES_QUERY;

			XSAMSData readXsams = search(query);
			if (readXsams == null) {
				logger.warning("Result null.");
				return speciesDBs;
			}
			Molecules molecules = readXsams.getSpecies().getMolecules();
			if (molecules != null) {
				for (int cpt = 0; cpt < molecules.getMolecules().size(); cpt++) {
					speciesDBs.add(addMolecules(molecules.getMolecules().get(cpt)));
				}
			}

			Atoms atoms = readXsams.getSpecies().getAtoms();
			if (atoms != null) {
//				if (DEBUG) {
//					writeXsamsFile(readXsams, new File("atoms.xml"));
//				}

				for (int cpt = 0; cpt < atoms.getAtoms().size(); cpt++) {
					addAtoms(atoms.getAtoms().get(cpt));
				}
			}
		}
		return speciesDBs;
	}

	/**
	 * @param molecules
	 * @param cpt
	 * @return
	 */
	protected MoleculeDescriptionDB addMolecules(MoleculeType mol) {

		String name = getName(mol);
		boolean isCompleted = true;
		List<Double> partitionFunctionT = null;
		List<Double> partitionFunctionQ = null;
		for (PartitionFunctionType part : mol.getMolecularChemicalSpecies().getPartitionFunctions()) {
			DataSeriesType t = part.getT();
			partitionFunctionT = t.getDataList().getValues();
			logger.warning("Unit Temperature : " + t.getUnits());
			logger.warning("Levels Temperature : " + t.getDataList().getValues());
			DataSeriesType q = part.getQ();
			logger.warning("Unit Value: " + q.getUnits());
			logger.warning("Values : " + q.getDataList().getValues());
			partitionFunctionQ = q.getDataList().getValues();
		}
		double molWeight = getMolWeight(mol);

		String inchiKey = mol.getMolecularChemicalSpecies().getInChIKey();
		logger.warning("inchiKey :" + inchiKey);
		String inchi = mol.getMolecularChemicalSpecies().getInChI();
		logger.warning("inchi :" + inchi);

		int tag = getTag();

		double[] tempList = new double[] { 0. };
		double[] qlogsList = new double[] { 0. };


		if (partitionFunctionT == null || partitionFunctionT.isEmpty()
				|| partitionFunctionQ == null || partitionFunctionQ.isEmpty()) {
			logger.warning("No partition function");
			isCompleted = false;
		} else {
			tempList = new double[partitionFunctionT.size()];
			qlogsList = new double[partitionFunctionT.size()];

			final int size = qlogsList.length;
			for (int i = 0; i < size; i++) {
				tempList[size - i - 1] = partitionFunctionT.get(i);
				qlogsList[size - i - 1] = Math.log10(partitionFunctionQ.get(i));
			}
		}

		final MoleculeDescriptionDB moleculeDB =
				new MoleculeDescriptionDB(tag, name, tempList, qlogsList);

		moleculeDB.setMoleculeID(mol.getSpeciesID());
		moleculeDB.setMolecularMass(molWeight);
		moleculeDB.setSource(getSource(mol));
		addMolIdRequestable(mol.getSpeciesID(), mol);
		moleculeDB.setType(TypeSpecies.MOLECULE);
		moleculeDB.setCompleted(isCompleted);
		return moleculeDB;
	}







	protected double getMolWeight(MoleculeType mol) {
		double molWeight = 0.;
		try {
			molWeight = mol.getMolecularChemicalSpecies().getStableMolecularProperties()
					.getMolecularWeight().getValue().getValue();
		} catch (Exception e) {
			logger.warning("ERROR:no molecules weight for " + mol.getSpeciesID() + e.getMessage());
		}
		return molWeight;
	}



	protected String getName(MoleculeType mol) {
		String name = mol.getMolecularChemicalSpecies().getOrdinaryStructuralFormula().getValue();
		String comment = mol.getMolecularChemicalSpecies().getComment();
		logger.warning("Comment : " + comment);
		try {
			String[] split = comment.split(";");
			if (split.length > 1) {
				String namePlus = split[split.length - 1];
				namePlus = namePlus.replaceAll(Matcher.quoteReplacement("$"), "");
				name = name + " (" + namePlus.trim() + ")";
			}
		} catch (Exception e) {
			logger.warning("ERROR:No name finds for " + mol.getSpeciesID());
		}
		return name;
	}

	private void addAtoms(AtomType atomType) {
		ElementSymbolType elementSymbol = atomType.getChemicalElement().getElementSymbol();
		if (elementSymbol != null) {
			String symbolName = elementSymbol.value();

			if (atomType.getIsotopes() != null && !atomType.getIsotopes().isEmpty()) {
				IsotopeType isotopeType = atomType.getIsotopes().get(0);

				for (int i = 0; i < isotopeType.getIons().size(); i++) {
					AtomicIonType atomicIonType = isotopeType.getIons().get(i);
					int tag = getTag();
					int massNumber = getMassNumber(isotopeType);

					String source = getSource(atomicIonType);
					String moleculeName = getName(symbolName, atomicIonType, massNumber);
					MoleculeDescriptionDB moleculeDB = new MoleculeDescriptionDB(tag, moleculeName,
							new double[] {}, new double[] {});
					moleculeDB.setSource(source);
					moleculeDB.setMoleculeID(getAtomSpeciesId(atomicIonType));
					moleculeDB.setType(TypeSpecies.ATOM);
					speciesDBs.add(moleculeDB);
					addAtomIdRequestable(moleculeDB.getMoleculeID(), atomType);
				}
			}
		}
	}



	protected int getMassNumber(IsotopeType isotopeType) {
		int massNumber = UNKNOW_MASS;
		if (isotopeType.getIsotopeParameters() != null) {
			massNumber = isotopeType.getIsotopeParameters().getMassNumber();
		}
		return massNumber;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * eu.omp.irap.cassis.database.DataBaseConnection#getLineDescriptionDB(cassis
	 * .database.MoleculeDescriptionDB, double, double)
	 */
	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(SimpleMoleculeDescriptionDB mol,
			double freqMinComp, double freqMaxComp) {
		List<LineDescriptionDB> lines = new ArrayList<LineDescriptionDB>();
		String citation = "";
		if(mol.getType().equals(TypeSpecies.MOLECULE)){
			String query = createSimpleQuery(url, getMoleculeParameterRequestable(),
					getMoleculeValueRequestable(mol), freqMinComp, freqMaxComp);

			XSAMSHeaderAndData readXsams = search(query);

			if (readXsams != null) {
				citation = getXsamsCitation(readXsams);
				final int molTag = mol.getTag();
				Molecules molecules = readXsams.getSpecies().getMolecules();
				if (molecules != null) {
					Map<String, MolecularStateType> molecularStateMap = getMolecularStateMap(molecules);
					Radiative radiative = readXsams.getProcesses().getRadiative();
					lines.addAll(getMolLines(0, Double.MAX_VALUE, 0, Double.MAX_VALUE, radiative,
							molecularStateMap, molTag));
				}
			}
		}else {
			String query = createSimpleQuery(url, getAtomParameterRequestable(),
					getAtomValueRequestable(mol), freqMinComp, freqMaxComp);

			XSAMSHeaderAndData readXsams = search(query);
			if (readXsams != null) {
				citation = getXsamsCitation(readXsams);
				final int molTag = mol.getTag();

				Atoms atoms = readXsams.getSpecies().getAtoms();
				if (atoms != null) {
					for (AtomType atomType : atoms.getAtoms()) {
						for (AtomicIonType atomicIonType : atomType.getIsotopes().get(0).getIons()) {
							if (getAtomSpeciesId(atomicIonType).equals(mol.getMoleculeID())){
								Map<String, AtomicStateType> hashTrHashMap = new HashMap<String, AtomicStateType>();
								for (AtomicStateType atomicStateType : atomicIonType.getAtomicStates()) {
									hashTrHashMap.put(atomicStateType.getStateID(), atomicStateType);
								}
								Radiative radiative = readXsams.getProcesses().getRadiative();
								lines.addAll(getAtomsLines(molTag, 0, Double.MAX_VALUE, 0, Double.MAX_VALUE,
										hashTrHashMap, radiative));
							}
						}
					}
				}
			}
		}

		for (LineDescriptionDB lineDescriptionDB : lines) {
			lineDescriptionDB.setCitation(citation);
		}

		return lines;
	}


	/**
	 * Create a query for retrieving transitions for only a molecule/atom.
	 *
	 * @param url
	 *            The URL of the VAMDC service
	 * @param moleculeType
	 *            The name of the identifiers.
	 * @param moleculeId
	 *            The ID of the molecule/atom in the VAMDC service.
	 * @param freqMinComp
	 *            The frequence min.
	 * @param freqMaxComp
	 *            The frequence max.
	 * @return
	 */
	protected static String createSimpleQuery(String url,
			String parameterRequestable,
			String valueRequestable,
			double freqMinComp, double freqMaxComp) {
		StringBuilder sb = new StringBuilder();
		sb.append(url);
		sb.append("sync?REQUEST=DOQUERY&LANG=VSS2&FORMAT=XSAMS&QUERY=SELECT*WHERE+(");
		sb.append(parameterRequestable);
		sb.append("='");
		sb.append(valueRequestable);
		sb.append("')");

		double radTransWavelengthMin = convertFromMhzFreq(freqMaxComp);
		double radTransWavelengthMax = convertFromMhzFreq(freqMinComp);
		if (radTransWavelengthMin > 0.0 || radTransWavelengthMax != Double.MAX_VALUE) {
			sb.append("+AND+(");
			boolean alreadyOp = false;
			if (radTransWavelengthMin > 0.0) {
				sb.append("RadTransWavelength>=");
				sb.append(radTransWavelengthMin);
				alreadyOp = true;
			}
			if (radTransWavelengthMax != Double.MAX_VALUE) {
				if (alreadyOp) {
					sb.append("+AND+");
				}
				sb.append("RadTransWavelength<=");
				sb.append(radTransWavelengthMax);
			}
			sb.append(")");
		}
		return sb.toString();
	}




	/**
	 *
	 */
	private Map<String, MolecularStateType> getMolecularStateMap(Molecules molecules) {
		Map<String, MolecularStateType> hashTrHashMap = new HashMap<String, MolecularStateType>();
		for (MoleculeType mol : molecules.getMolecules()) {
			List<MolecularStateType> molecularStates = mol.getMolecularStates();
			for (MolecularStateType molecularStateType : molecularStates) {
				String idTransitions = molecularStateType.getStateID();
				logger.warning("ID of transitions =" + idTransitions);
				hashTrHashMap.put(idTransitions, molecularStateType);
			}
		}
		return hashTrHashMap;
	}

	/**
	 * @param otherThresEupMin
	 * @param otherThresEupMax
	 * @param otherTreshAijMax
	 * @param otherThresAijMin
	 * @param lineDescriptionDBs
	 * @param readXsams
	 * @param molecularStateMap
	 * @param molTag
	 */
	private List<LineDescriptionDB> getMolLines(double otherThresEupMin, double otherThresEupMax,
			double otherThresAijMin, double otherTreshAijMax, Radiative radiative,
			Map<String, MolecularStateType> molecularStateMap, int molTag) {
		List<LineDescriptionDB> lines = new ArrayList<LineDescriptionDB>();

		for (RadiativeTransitionType rad : radiative.getRadiativeTransitions()) {

			String initialStateRef = null;
			String finalStateRef = null;
			try {
				initialStateRef = ((MolecularStateType) rad.getLowerStateRef()).getStateID();
				finalStateRef = ((MolecularStateType) rad.getUpperStateRef()).getStateID();
			} catch (NullPointerException e) {
				continue;
			}
			MolecularStateType lowerMolecularStateType = molecularStateMap.get(initialStateRef);
			MolecularStateType upperMolecularStateType = molecularStateMap.get(finalStateRef);
			if (lowerMolecularStateType == null || upperMolecularStateType == null) {
				continue;
			}

			WaveSelections waveSelections = extractWave(rad);
			if (waveSelections == null) {
				continue;
			}

			logger.warning("freq = " + waveSelections.getFreq());
			logger.warning("freq error = " + waveSelections.getFreqError());

			double freq  = waveSelections.getFreq();
			double freqError = waveSelections.getFreqError();

			int gUp;
			try {
				gUp = upperMolecularStateType.getMolecularStateCharacterisation()
						.getTotalStatisticalWeight();
			} catch (Exception e) {
				gUp = 0;
				logger.warning("Exception while reading the gUp, value set to 0." + e.getMessage());
			}

			double elow;
			try {
				elow = lowerMolecularStateType.getMolecularStateCharacterisation().getStateEnergy()
						.getValue().getValue();
			} catch (Exception e) {
				elow = 0.0;
				logger.warning("Exception while reading the elow, value set to 0.0."
						+ e.getMessage());
			}
			final double eLowJ = Formula.calcELowJ(elow, 0.0);
			final double eUpJ = Formula.calcEUpJ(eLowJ, freq);
			double eup = eUpJ / Formula.K;

			String quantumUpper = "?";
			String quantumLower = "?";
			try {
				quantumUpper = CaseTypeUtil.getQuantumNumbers(upperMolecularStateType);
				quantumLower = CaseTypeUtil.getQuantumNumbers(lowerMolecularStateType);
			} catch (NullPointerException e) {
			}

			double aEinstein=0.0;
			try {
				aEinstein = rad.getProbabilities().get(0).getTransitionProbabilityA().getValue()
						.getValue();
			} catch (NullPointerException e) {
				aEinstein = 0.0;
			}
			logger.warning("Aeinst = " + aEinstein);

			// TODO get the gammaself ...
			double gammaself = 0;

			if (aEinstein >= otherThresAijMin && aEinstein <= otherTreshAijMax
					&& eup >= otherThresEupMin && eup <= otherThresEupMax) {
				final LineDescriptionDB line = new LineDescriptionDB(molTag, freq, '\n' + quantumUpper + " - \n"
								+ quantumLower + '\n', freqError, aEinstein, elow, gUp, gammaself, eup);
				line.setOtherFreqs(waveSelections.getOtherFreqs());
				lines.add(line);
			}
		}
		return lines;
	}




	/**
	 * @param rad
	 * @return
	 */
	public WaveSelections extractWave(RadiativeTransitionType rad) {
		WaveSelections waveSelections = null;
		{
		List<DataType> frequencies = rad.getEnergyWavelength().getFrequencies();
		if (!frequencies.isEmpty()) {
			waveSelections = extractWave(frequencies);
		} else {
			final List<DataType> wavenumbers = rad.getEnergyWavelength().getWavenumbers();
			if (!wavenumbers.isEmpty()) {
				waveSelections = extractWave(wavenumbers);
			} else {
				final List<WlType> wavelengths = rad.getEnergyWavelength().getWavelengths();
				double freq;
				double freqError = 0.;

				if (!wavelengths.isEmpty()) {
					double val = wavelengths.get(0).getValue().getValue();
					String unit = wavelengths.get(0).getValue().getUnits();
					freq = XAxisWaveLength.getXAxisCassis(unit).convertToMHzFreq(val);
					List<String> otherFreq= new ArrayList<String>();
					waveSelections = new WaveSelections(freq, freqError, otherFreq);
				}
			}
		}
		}
		return waveSelections;
	}


	/**
	 * @param frequencies
	 * @return
	 */
	public WaveSelections extractWave(List<DataType> frequencies) {
		WaveSelections waveSelections;
		double minAccurancy = Double.MAX_VALUE;
		String bestFreq = "";
		double freq;
		double freqError;
		List<String> otherFreq= new ArrayList<String>();

		for (DataType dataType : frequencies) {
			final ValueType value = dataType.getValue();
			String unit = value.getUnits();
			final double val = value.getValue();
			freq = XAxisWaveNumber.getXAxisCassis(unit).convertToMHzFreq(val);
			freqError = 0;
			try {
				freqError = XAxisWaveNumber.getXAxisCassis(unit).convertToMHzFreq(dataType.getAccuracies().get(0).getValue());
			} catch (NullPointerException e) {
			}
			final String freqString = freq + MINUS_PLUS + freqError;
			otherFreq.add(freqString);
			if (freqError < minAccurancy) {
				minAccurancy = freqError;
				bestFreq = freqString;
			}
		}
		otherFreq.remove(bestFreq);
		freq = Double.valueOf(bestFreq.split(MINUS_PLUS)[0]);
		freqError = Double.valueOf(bestFreq.split(MINUS_PLUS)[1]);

		waveSelections = new WaveSelections(freq, freqError, otherFreq);
		return waveSelections;
	}
	private class WaveSelections {

		private double freq;
		private double freqError;
		private List<String> otherFreqs;

		public WaveSelections(double freq, double freqError, List<String> otherFreqs) {
			this.freq = freq;
			this.freqError = freqError;
			this.otherFreqs = otherFreqs;
		}


		public double getFreq() {
			return freq;
		}


		public double getFreqError() {
			return freqError;
		}


		public List<String> getOtherFreqs() {
			return otherFreqs;
		}
	}

	/**
	 * @param lineDescriptionDBs
	 * @param molTag
	 * @param otherThresEupMin
	 * @param otherThresEupMax
	 * @param otherTreshAijMax
	 * @param otherThresAijMin
	 * @param hashTrHashMap
	 * @param radiative
	 * @return
	 */
	protected List<LineDescriptionDB> getAtomsLines(final int molTag, double otherThresEupMin,
			double otherThresEupMax, double otherThresAijMin, double otherTreshAijMax,
			Map<String, AtomicStateType> hashTrHashMap, Radiative radiative) {
		List<LineDescriptionDB> lines = new ArrayList<LineDescriptionDB>();

		for (RadiativeTransitionType rad : radiative.getRadiativeTransitions()) {

			String initialStateRef = null;
			String finalStateRef = null;
			try {
				initialStateRef = ((AtomicStateType) rad.getLowerStateRef()).getStateID();
				finalStateRef = ((AtomicStateType) rad.getUpperStateRef()).getStateID();
			} catch (NullPointerException e) {
				e.printStackTrace();
				continue;
			}
			AtomicStateType lowerAtomicStateType = hashTrHashMap.get(initialStateRef);
			AtomicStateType upperAtomicStateType = hashTrHashMap.get(finalStateRef);
			if (lowerAtomicStateType == null || upperAtomicStateType == null) {
				continue;
			}


			double freq = 0.;
			double freqError = 0.;
			final List<DataType> frequencies = rad.getEnergyWavelength().getFrequencies();
			if (frequencies.isEmpty()) {
				final List<DataType> wavenumbers = rad.getEnergyWavelength().getWavenumbers();
				if (wavenumbers.isEmpty()) {
					final List<WlType> wavelengths = rad.getEnergyWavelength().getWavelengths();
					if (!wavelengths.isEmpty()) {
						double val = wavelengths.get(RITZ_INDICE).getValue().getValue();
						String unit = wavelengths.get(RITZ_INDICE).getValue().getUnits();
						freq = XAxisWaveLength.getXAxisCassis(unit).convertToMHzFreq(val);
					}
				} else {
					double val = wavenumbers.get(RITZ_INDICE).getValue().getValue();
					String unit = wavenumbers.get(RITZ_INDICE).getValue().getUnits();
					freq = XAxisWaveNumber.getXAxisCassis(unit).convertToMHzFreq(val);
				}
			} else {
				DataType frequencyBalise = frequencies.get(RITZ_INDICE);
				freq = frequencyBalise.getValue().getValue();
				try {
					freqError = frequencyBalise.getAccuracies().get(0).getValue();
				} catch (NullPointerException e) {
				}
			}
			logger.warning("freq = " + freq);
			logger.warning("freq error = " + freqError);


			int gUp;
			try {
				gUp = upperAtomicStateType.getAtomicNumericalData().getStatisticalWeight()
						.intValue();
			} catch (Exception e) {
				gUp = 0;
				logger.warning("Exception while reading the gUp, value set to 0." + e.getMessage());
			}

			double elow;
			try {
				elow = lowerAtomicStateType.getAtomicNumericalData().getStateEnergy().getValue()
						.getValue();
			} catch (Exception e) {
				elow = 0.0;
				logger.warning("Exception while reading the elow, value set to 0.0."
						+ e.getMessage());
			}
			final double eLowJ = Formula.calcELowJ(elow, 0.0);
			final double eUpJ = Formula.calcEUpJ(eLowJ, freq);
			double eup = eUpJ / Formula.K;

			String quantumUpper = "?";
			String quantumLower = "?";
			try {
				quantumUpper = "J = "
						+ upperAtomicStateType.getAtomicQuantumNumbers().getTotalAngularMomentum()
								.toString();
				quantumLower = "J = "
						+ lowerAtomicStateType.getAtomicQuantumNumbers().getTotalAngularMomentum()
								.toString();
			} catch (NullPointerException e) {
			}

			double aEinstein = 0;
			try {
				aEinstein = rad.getProbabilities().get(0).getTransitionProbabilityA().getValue()
						.getValue();
			} catch (NullPointerException e) {
				aEinstein = 0;
			}
			logger.warning("Aeinst = " + aEinstein);
			if (aEinstein >= otherThresAijMin && aEinstein <= otherTreshAijMax
					&& eup >= otherThresEupMin && eup <= otherThresEupMax) {
				// TODO get the gammaself ...
				double gammaself = 0;
				String quantumNumbers ='\n' + quantumUpper + " - \n" + quantumLower + '\n';
				if ("?".equals(quantumUpper)){
					quantumNumbers = "";
				}
				lines.add(new LineDescriptionDB(molTag, freq,quantumNumbers, freqError, aEinstein, elow, gUp, gammaself, eup));
			}
		}
		return lines;
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * eu.omp.irap.cassis.database.DataBaseConnection#getLineDescriptionDB(java
	 * .util.List, double, double, double, double, double, double)
	 */
	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(
			List<SimpleMoleculeDescriptionDB> listSimpleMolDB, double frequencyMin,
			double frequencyMax, double otherThresEupMin, double otherThresEupMax,
			double otherThresAijMin, double otherTreshAijMax) {
		List<LineDescriptionDB> res = new ArrayList<LineDescriptionDB>();
		int size = listSimpleMolDB.size();
		int index = 0;

		while (index != size) {
			List<SimpleMoleculeDescriptionDB> subListSimpleMolDB = new ArrayList<SimpleMoleculeDescriptionDB>(
					DEPTH);
			for (int cpt = 0; index < size && cpt < DEPTH; cpt++) {
				subListSimpleMolDB.add(listSimpleMolDB.get(index));
				index++;
			}
			res.addAll(getLineDB(subListSimpleMolDB, frequencyMin, frequencyMax, otherThresEupMin,
					otherThresEupMax, otherThresAijMin, otherTreshAijMax));
		}

		//Double check
		for (Iterator<LineDescriptionDB> iterator = res.iterator(); iterator.hasNext();) {
			LineDescriptionDB lineDescriptionDB = (LineDescriptionDB) iterator.next();
			if (lineDescriptionDB.getFrequency()< frequencyMin || lineDescriptionDB.getFrequency()>frequencyMax){
				iterator.remove();
			}
		}

		return res;
	}



	/**
	 * @param listSimpleMolDB
	 * @param frequencyMin
	 * @param frequencyMax
	 * @param otherThresEupMin
	 * @param otherThresEupMax
	 * @param otherThresAijMin
	 * @param otherTreshAijMax
	 * @param lineDescriptionDBs
	 * @return
	 */
	private List<LineDescriptionDB> getLineDB(List<SimpleMoleculeDescriptionDB> listSimpleMolDB,
			double frequencyMin, double frequencyMax, double otherThresEupMin,
			double otherThresEupMax, double otherThresAijMin, double otherTreshAijMax) {
		String query = "";
		List<LineDescriptionDB> lines = new ArrayList<LineDescriptionDB>();

		//Separation of atoms and molecules
		Map<String, Integer> mapMoleIDTag = new HashMap<String, Integer>(listSimpleMolDB.size());
		Map<String, Integer> mapAtomIDTag = new HashMap<String, Integer>(listSimpleMolDB.size());
		List<SimpleMoleculeDescriptionDB> listMols = new ArrayList<SimpleMoleculeDescriptionDB>();
		List<SimpleMoleculeDescriptionDB> listAtoms = new ArrayList<SimpleMoleculeDescriptionDB>();
		for (SimpleMoleculeDescriptionDB mol : listSimpleMolDB) {
			try {
				MoleculeDescriptionDB moleculeDescriptionDB = getMoleculeDescriptionDB(mol);
				if (mol.getType().equals(TypeSpecies.MOLECULE)){
					mapMoleIDTag.put(moleculeDescriptionDB.getMoleculeID(), mol.getTag());
					listMols.add(moleculeDescriptionDB);
				} else {
					mapAtomIDTag.put(moleculeDescriptionDB.getMoleculeID(), mol.getTag());
					listAtoms.add(moleculeDescriptionDB);
				}
			} catch (UnknowMoleculeException ume) {
				// We can not get the lines as the species is unknown.
			}
		}

		if (local){
			XSAMSHeaderAndData readXsams = search("");
			if (readXsams != null){
				lines.addAll(getMoleculeLines(readXsams, mapMoleIDTag,
						otherThresEupMin, otherThresEupMax, otherThresAijMin, otherTreshAijMax));

				lines.addAll(getAtomsLines(readXsams, mapAtomIDTag,
						otherThresEupMin, otherThresEupMax, otherThresAijMin, otherTreshAijMax));
			}
		}else  {
			if (!listMols.isEmpty()){
				query = createMolQuery(listMols, frequencyMin, frequencyMax, otherThresAijMin,
						otherTreshAijMax);
				XSAMSHeaderAndData readXsams = search(query);
				//writeXsamsFile(readXsams, new File("molecule.xml"));
				if (readXsams != null){

					lines.addAll(getMoleculeLines(readXsams, mapMoleIDTag,
							otherThresEupMin, otherThresEupMax, otherThresAijMin, otherTreshAijMax));
				}
			}
			if (!listAtoms.isEmpty()){
				query = createAtomQuery(listAtoms, frequencyMin, frequencyMax, otherThresAijMin,
						otherTreshAijMax);
				XSAMSHeaderAndData readXsams = search(query);
				if (readXsams != null){
					lines.addAll(getAtomsLines(readXsams, mapAtomIDTag,
							otherThresEupMin, otherThresEupMax, otherThresAijMin, otherTreshAijMax));
				}
			}
		}

		return lines;
	}

	private List<LineDescriptionDB>  getAtomsLines(XSAMSHeaderAndData readXsams,
			Map<String, Integer> mapAtomIDTag,
			double otherThresEupMin, double otherThresEupMax,
			double otherThresAijMin, double otherTreshAijMax) {

		List<LineDescriptionDB> lines = new ArrayList<LineDescriptionDB>();
		Atoms atoms = readXsams.getSpecies().getAtoms();
		String citation = getXsamsCitation(readXsams);
		if (atoms != null) {
			int molTag;
			for (AtomType atomType : atoms.getAtoms()) {
				for (AtomicIonType atomicIonType : atomType.getIsotopes().get(0).getIons()) {
					if (!mapAtomIDTag.containsKey(getAtomSpeciesId(atomicIonType))) {
						continue;
					}
					Map<String, AtomicStateType> hashTrHashMap = new HashMap<String, AtomicStateType>();
					for (AtomicStateType atomicStateType : atomicIonType.getAtomicStates()) {
						hashTrHashMap.put(atomicStateType.getStateID(), atomicStateType);
					}

					molTag = mapAtomIDTag.get(getAtomSpeciesId(atomicIonType));
					Radiative radiative = readXsams.getProcesses().getRadiative();
					lines.addAll(getAtomsLines(molTag, otherThresEupMin, otherThresEupMax,
							otherThresAijMin, otherTreshAijMax, hashTrHashMap, radiative));
				}
			}
			for (LineDescriptionDB lineDescriptionDB : lines) {
				lineDescriptionDB.setCitation(citation);
			}
		}
		return lines;
	}

	private String getXsamsCitation(XSAMSHeaderAndData readXsams) {
		if (readXsams.isTruncated())
			logger.warning("Truncated values");
		return readXsams.getIdCitation();
	}




	private List<LineDescriptionDB>  getMoleculeLines(XSAMSHeaderAndData readXsams, Map<String, Integer> mapMoleIDTag,
			double otherThresEupMin, double otherThresEupMax,
			double otherThresAijMin, double otherTreshAijMax) {

		List<LineDescriptionDB> lines = new ArrayList<LineDescriptionDB>();
		Molecules molecules = readXsams.getSpecies().getMolecules();

		if (molecules != null) {
			String citation = getXsamsCitation(readXsams);
			List<MolecularStateType> molecularStates;
			Map<String, MolecularStateType> molecularStateMap;
			int molTag;
			for (MoleculeType mol : molecules.getMolecules()) {
				if (!mapMoleIDTag.containsKey(mol.getSpeciesID())) {
					continue;
				}
				molecularStates = mol.getMolecularStates();

				molecularStateMap = new HashMap<String, MolecularStateType>(molecularStates.size());
				for (MolecularStateType molecularStateType : molecularStates) {
					molecularStateMap.put(molecularStateType.getStateID(), molecularStateType);
				}
				molTag = mapMoleIDTag.get(mol.getSpeciesID());
				Radiative radiative = readXsams.getProcesses().getRadiative();
				lines.addAll(getMolLines(otherThresEupMin, otherThresEupMax, otherThresAijMin,
						otherTreshAijMax, radiative, molecularStateMap, molTag));
			}
			for (LineDescriptionDB lineDescriptionDB : lines) {
				lineDescriptionDB.setCitation(citation);
			}
		}
		return lines;
	}


	protected String createMolQuery(List<SimpleMoleculeDescriptionDB> listSimpleMolDB,
			double frequencyMin, double frequencyMax, double aijMin, double aijMax) {
		int molNumber = listSimpleMolDB.size();
		StringBuilder sb = new StringBuilder(url);

		sb.append("sync?REQUEST=doQuery&LANG=VSS2&FORMAT=XSAMS&QUERY=SELECT*WHERE+(");
		sb.append(getMoleculeParameterRequestable()).append("+IN+(");

		for (int i=0; i < molNumber - 1; i++) {
			sb.append('\'')
					.append(getMoleculeValueRequestable(listSimpleMolDB.get(i)))
					.append('\'').append(",");
		}
		if (molNumber > 0) {
			sb.append('\'')
					.append(getMoleculeValueRequestable(listSimpleMolDB.get(molNumber-1)))
					.append('\'');
		}
		sb.append(")");

		getTresholdOfQuery(frequencyMin, frequencyMax, aijMin, aijMax, sb);
		return sb.toString();
	}

	protected String createAtomQuery(List<SimpleMoleculeDescriptionDB> listSimpleMolDB,
			double frequencyMin, double frequencyMax, double aijMin, double aijMax) {

		//Remove same id
		ArrayList<String> uniqueAtoms = new ArrayList<String>();
		for (SimpleMoleculeDescriptionDB species : listSimpleMolDB) {
			String atomValueRequestable = getAtomValueRequestable(species);
			if (!uniqueAtoms.contains(atomValueRequestable)){
				uniqueAtoms.add(atomValueRequestable);
			}
		}
		int molNumber = uniqueAtoms.size();
		StringBuilder sb = new StringBuilder(url);

		sb.append("sync?REQUEST=doQuery&LANG=VSS2&FORMAT=XSAMS&QUERY=SELECT*WHERE+(");
		sb.append(getAtomParameterRequestable()).append("+IN+(");

		for (int i =0; i < molNumber - 1; i++) {
			sb.append('\'')
					.append(uniqueAtoms.get(i))
					.append('\'').append(",");
		}
		if (molNumber > 0) {
			sb.append('\'')
					.append(uniqueAtoms.get( molNumber - 1))
					.append('\'');
		}
		sb.append(")");

		getTresholdOfQuery(frequencyMin, frequencyMax, aijMin, aijMax, sb);
		return sb.toString();
	}


	private void getTresholdOfQuery(double frequencyMin, double frequencyMax, double aijMin,
			double aijMax, StringBuilder sb) {
		double radTransWavelengthMin = convertFromMhzFreq(frequencyMax);
		double radTransWavelengthMax = convertFromMhzFreq(frequencyMin);
		if (radTransWavelengthMin > 0.0 || radTransWavelengthMax != Double.MAX_VALUE || aijMin > 0
				|| aijMax != Double.MAX_VALUE) {
			boolean alreadyOp = false;
			sb.append(")+AND+(");
			if (radTransWavelengthMin > 0.0) {
				sb.append("RadTransWavelength>=").append(radTransWavelengthMin);
				alreadyOp = true;
			}
			if (radTransWavelengthMax != Double.MAX_VALUE) {
				if (alreadyOp) {
					sb.append("+AND+");
				} else {
					alreadyOp = true;
				}
				sb.append("RadTransWavelength<=").append(radTransWavelengthMax);
			}
			if (aijMin > 0.0) {
				if (alreadyOp) {
					sb.append("+AND+");
				} else {
					alreadyOp = true;
				}
				sb.append("RadTransProbabilityA>=").append(aijMin);
			}
			if (aijMax != Double.MAX_VALUE) {
				if (alreadyOp) {
					sb.append("+AND+");
				} else {
					alreadyOp = true;
				}
				sb.append("RadTransProbabilityA<=").append(aijMax);
			}
			sb.append(")");
		}
	}



	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * eu.omp.irap.cassis.database.DataBaseConnection#getLineDescriptionDB(cassis
	 * .database.SimpleMoleculeDescriptionDB, double, double, double, double,
	 * double, double)
	 */
	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(SimpleMoleculeDescriptionDB simpleMolDB,
			double frequencyMin, double frequencyMax, double otherThresEupMin,
			double otherThresEupMax, double otherThresAijMin, double otherTreshAijMax) {
		List<SimpleMoleculeDescriptionDB> listSimpleMolDB = new ArrayList<>();
		listSimpleMolDB.add(simpleMolDB);
		return getLineDescriptionDB(listSimpleMolDB, frequencyMin, frequencyMax, otherThresEupMin,
				otherThresEupMax, otherThresAijMin, otherTreshAijMax);
	}

	@Override
	protected String getMoleculeValueRequestable(SimpleMoleculeDescriptionDB mol) {
		return mapMolIdRequestable.get(mol.getMoleculeID());
	}

	protected String getSource(MoleculeType mol) {
		String source = simpleSourceName;
		String[] split2 = mol.getSpeciesID().split("-");
		if (split2 != null && split2.length > 1) {
			source = split2[0];
		}
		return source;
	}

	@Override
	protected String getAtomParameterRequestable() {
		return "AtomSymbol";
	}

	@Override
	protected String getAtomValueRequestable(SimpleMoleculeDescriptionDB mol) {
		return mapAtomIdRequestable.get(mol.getMoleculeID());
	}

	protected void addAtomIdRequestable(String atomId, AtomType atomType) {
		mapAtomIdRequestable.put(atomId,
				atomType.getChemicalElement().getElementSymbol().value());
	}

	protected String getSource(AtomicIonType atomicIonType) {
		String source = simpleSourceName;
		String[] split2 = getAtomSpeciesId(atomicIonType).split("-");
		if (split2 != null && split2.length == 2) {
			source = split2[0];
		}
		return source;
	}

	protected String getAtomSpeciesId(AtomicIonType atomicIonType) {
		return atomicIonType.getSpeciesID();
	}


	protected String getName(String symbolName, AtomicIonType atomicIonType, int massNumber) {
		String surname = "?";
		ElementSymbolType isoelectronicSequence = atomicIonType.getIsoelectronicSequence();
		if (isoelectronicSequence != null) {
			surname = isoelectronicSequence.value();
		} else {
			Integer ionCharge = atomicIonType.getIonCharge();
			if (ionCharge != null) {
				surname = integerToRoman(ionCharge + 1);
			}

		}

		String moleculeName;


		if (massNumber == UNKNOW_MASS) {
			moleculeName = symbolName + " " + surname;
		} else {
			moleculeName = symbolName + " " + surname + ", A=" + massNumber;
		}
		return moleculeName;
	}

	protected XSAMSHeaderAndData search(final String request) {
		XSAMSHeaderAndData result = null;
		try {
			if (!local){
				result = readXsamsURL(new URL(request));
			} else  {
				result = readXsamsURL(new URL(this.url));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	protected int getTag() {
		return cassisVamdcTag++;
	}

	protected String getMolSpeciesId(MoleculeType mol) {
		return mol.getSpeciesID();
	}

	@Override
	protected String getMoleculeParameterRequestable() {
		return "InchiKey";
	}

	protected String getMoleculeValueRequestable(MoleculeType mol) {
		return mol.getMolecularChemicalSpecies().getInChIKey();
	}

	protected void addMolIdRequestable(String moleculeID, MoleculeType mol) {
		mapMolIdRequestable.put(moleculeID,mol.getMolecularChemicalSpecies().getInChIKey());
	}

}


