/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.access.vamdc;

import org.vamdc.xsams.schema.AtomicIonType;
import org.vamdc.xsams.schema.MoleculeType;

import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB;

public class CdmsJplVamdcDataBaseConnection extends DefaultVamdcDataBaseConnection {

	public CdmsJplVamdcDataBaseConnection(String url) {
		super(url);
	}

	protected int getTag(MoleculeType mol) {
		int tag = 1;
		try {
			tag = Integer
					.parseInt(mol.getMolecularChemicalSpecies().getComment().split("-")[0].trim());
		} catch (Exception e) {
			tag = 1;
		}

		if (tag == 1) {
			if (getMolWeight(mol) != 0) {
				tag = (int) (getMolWeight(mol) * 1000);
			}
		}
		return tag;
	}

	protected int getTag(AtomicIonType atomicIonType) {
		int tag = 0;
		String[] split = getAtomSpeciesId(atomicIonType).split("-");
		if (split != null && split.length == 2) {
			try {
				tag = Integer.parseInt(split[1]);
			} catch (NumberFormatException e) {
			}
		}
		return tag;
	}

	protected void addMolIdRequestable(String moleculeID, MoleculeType mol) {
		mapMolIdRequestable.put(moleculeID, getMolSpeciesId(mol));
	}

	protected String getMolSpeciesId(MoleculeType mol) {
		String speciesID = mol.getSpeciesID();
		logger.warning("ID : " + speciesID);

		try {
			speciesID = (speciesID.split("-")[1]);
		} catch (Exception e) {
			logger.warning("SpeciesID error." + e.getMessage());
		}
		return speciesID;
	}

	@Override
	protected String getMoleculeParameterRequestable() {
		return "MoleculeSpeciesID";
	}

	@Override
	protected String getAtomParameterRequestable() {
		return "SpeciesID";
	}

	@Override
	protected String getAtomValueRequestable(SimpleMoleculeDescriptionDB mol) {
		String speciesID = mol.getMoleculeID();
		try {
			speciesID = (speciesID.split("-")[1]);
		} catch (Exception e) {
			logger.warning("SpeciesID error." + e.getMessage());
		}
		return speciesID;

	}

}
