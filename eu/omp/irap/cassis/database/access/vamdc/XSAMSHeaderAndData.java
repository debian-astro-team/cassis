/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.access.vamdc;

import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.vamdc.xsams.schema.Environments;
import org.vamdc.xsams.schema.Functions;
import org.vamdc.xsams.schema.Methods;
import org.vamdc.xsams.schema.ProcessesType;
import org.vamdc.xsams.schema.Sources;
import org.vamdc.xsams.schema.SpeciesType;
import org.vamdc.xsams.schema.XSAMSData;

public class XSAMSHeaderAndData extends XSAMSData {

	private XSAMSData xsamsData;
	private boolean truncated = false;
	private String idCitation = "";

	public XSAMSHeaderAndData(XSAMSData xsamsData) {
		this.xsamsData = xsamsData;

	}

	/**
	 * @return the xsamsData
	 */
	public XSAMSData getXsamsData() {
		return xsamsData;
	}

	/**
	 * @param xsamsData
	 *            the xsamsData to set
	 */
	public void setXsamsData(XSAMSData xsamsData) {
		this.xsamsData = xsamsData;
	}

	/**
	 * @return the truncated
	 */
	public boolean isTruncated() {
		return truncated;
	}

	/**
	 * @param truncated
	 *            the truncated to set
	 */
	public void setTruncated(boolean truncated) {
		this.truncated = truncated;
	}

	/**
	 * @return the idCitation
	 */
	public String getIdCitation() {
		return idCitation;
	}

	/**
	 * @param idCitation
	 *            the idCitation to set
	 */
	public void setIdCitation(String idCitation) {
		this.idCitation = idCitation;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.vamdc.xsams.schema.XSAMSData#getEnvironments()
	 */
	@Override
	public Environments getEnvironments() {
		return xsamsData.getEnvironments();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.vamdc.xsams.schema.XSAMSData#setEnvironments(org.vamdc.xsams.schema.
	 * Environments)
	 */
	@Override
	public void setEnvironments(Environments value) {
		xsamsData.setEnvironments(value);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.vamdc.xsams.schema.XSAMSData#getSpecies()
	 */
	@Override
	public SpeciesType getSpecies() {

		return xsamsData.getSpecies();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.vamdc.xsams.schema.XSAMSData#setSpecies(org.vamdc.xsams.schema.
	 * SpeciesType)
	 */
	@Override
	public void setSpecies(SpeciesType value) {

		xsamsData.setSpecies(value);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.vamdc.xsams.schema.XSAMSData#getProcesses()
	 */
	@Override
	public ProcessesType getProcesses() {

		return xsamsData.getProcesses();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.vamdc.xsams.schema.XSAMSData#setProcesses(org.vamdc.xsams.schema.
	 * ProcessesType)
	 */
	@Override
	public void setProcesses(ProcessesType value) {

		xsamsData.setProcesses(value);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.vamdc.xsams.schema.XSAMSData#getSources()
	 */
	@Override
	public Sources getSources() {

		return xsamsData.getSources();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.vamdc.xsams.schema.XSAMSData#setSources(org.vamdc.xsams.schema.
	 * Sources)
	 */
	@Override
	public void setSources(Sources value) {

		xsamsData.setSources(value);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.vamdc.xsams.schema.XSAMSData#getMethods()
	 */
	@Override
	public Methods getMethods() {

		return xsamsData.getMethods();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.vamdc.xsams.schema.XSAMSData#setMethods(org.vamdc.xsams.schema.
	 * Methods)
	 */
	@Override
	public void setMethods(Methods value) {

		xsamsData.setMethods(value);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.vamdc.xsams.schema.XSAMSData#getFunctions()
	 */
	@Override
	public Functions getFunctions() {

		return xsamsData.getFunctions();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.vamdc.xsams.schema.XSAMSData#setFunctions(org.vamdc.xsams.schema.
	 * Functions)
	 */
	@Override
	public void setFunctions(Functions value) {

		xsamsData.setFunctions(value);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.vamdc.xsams.schema.XSAMSData#getComments()
	 */
	@Override
	public String getComments() {

		return xsamsData.getComments();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.vamdc.xsams.schema.XSAMSData#setComments(java.lang.String)
	 */
	@Override
	public void setComments(String value) {

		xsamsData.setComments(value);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.vamdc.xsams.schema.XSAMSData#toString()
	 */
	@Override
	public String toString() {

		return xsamsData.toString();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.vamdc.xsams.schema.XSAMSData#append(org.jvnet.jaxb2_commons.locator.
	 * ObjectLocator, java.lang.StringBuilder,
	 * org.jvnet.jaxb2_commons.lang.ToStringStrategy)
	 */
	@Override
	public StringBuilder append(ObjectLocator locator, StringBuilder buffer,
			ToStringStrategy strategy) {

		return xsamsData.append(locator, buffer, strategy);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.vamdc.xsams.schema.XSAMSData#appendFields(org.jvnet.jaxb2_commons.
	 * locator.ObjectLocator, java.lang.StringBuilder,
	 * org.jvnet.jaxb2_commons.lang.ToStringStrategy)
	 */
	@Override
	public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer,
			ToStringStrategy strategy) {
		return xsamsData.appendFields(locator, buffer, strategy);
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see org.vamdc.xsams.schema.XSAMSData#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object object) {
		return xsamsData.equals(object);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.vamdc.xsams.schema.XSAMSData#clone()
	 */
	@Override
	public Object clone() {
		return xsamsData.clone();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.vamdc.xsams.schema.XSAMSData#copyTo(java.lang.Object)
	 */
	@Override
	public Object copyTo(Object target) {
		return xsamsData.copyTo(target);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.vamdc.xsams.schema.XSAMSData#copyTo(org.jvnet.jaxb2_commons.locator.
	 * ObjectLocator, java.lang.Object,
	 * org.jvnet.jaxb2_commons.lang.CopyStrategy)
	 */
	@Override
	public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
		return xsamsData.copyTo(locator, target, strategy);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.vamdc.xsams.schema.XSAMSData#createNewInstance()
	 */
	@Override
	public Object createNewInstance() {
		return xsamsData.createNewInstance();
	}
}
