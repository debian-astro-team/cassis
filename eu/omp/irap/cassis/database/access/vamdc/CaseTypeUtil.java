/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.access.vamdc;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.vamdc.xsams.cases.common.BaseCase;
import org.vamdc.xsams.schema.AtomicQuantumNumbersType;
import org.vamdc.xsams.schema.AtomicStateType;
import org.vamdc.xsams.schema.MolecularStateType;

public class CaseTypeUtil {
	private final static int MAX_QUANTUM_NUMBERS_LENGTH = 60;

	private CaseTypeUtil() {
	}

	/**
	 * @param molecularState
	 * @return case type
	 */
	public static CaseType getMoleculeCaseType(MolecularStateType molecularState) {
		if (molecularState != null) {
			BaseCase baseCase = molecularState.getCases().get(0);
			if (baseCase != null) {
				try {
					return CaseType.valueOf(baseCase.getCaseID().toUpperCase());
				} catch (IllegalArgumentException e) {
					return CaseType.UNKNOWN_CASE;
				}
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public static String getQuantumNumbers(MolecularStateType molecularStateType) {
		CaseType caseType = CaseTypeUtil.getMoleculeCaseType(molecularStateType);

		if (caseType == null) {
			// TODO LOG
			return "";
		}

		Map<String, String> res = null;

		if (caseType.equals(CaseType.ASYMCS)) {
			res = getQuantumNumbersAsymcs(
					((org.vamdc.xsams.cases.asymcs.Case) molecularStateType.getCases().get(0))
							.getQNs());
		} else if (caseType.equals(CaseType.ASYMOS)) {
			res = getQuantumNumbersAsymos(
					((org.vamdc.xsams.cases.asymos.Case) molecularStateType.getCases().get(0))
							.getQNs());
		} else if (caseType.equals(CaseType.DCS)) {
			res = getQuantumNumbersDcs(
					((org.vamdc.xsams.cases.dcs.Case) molecularStateType.getCases().get(0))
							.getQNs());
		} else if (caseType.equals(CaseType.HUNDA)) {
			res = getQuantumNumbersHunda(
					((org.vamdc.xsams.cases.hunda.Case) molecularStateType.getCases().get(0))
							.getQNs());
		} else if (caseType.equals(CaseType.HUNDB)) {
			res = getQuantumNumbersHundb(
					((org.vamdc.xsams.cases.hundb.Case) molecularStateType.getCases().get(0))
							.getQNs());
		} else if (caseType.equals(CaseType.LPOS)) {
			res = getQuantumNumbersLpos(
					((org.vamdc.xsams.cases.lpos.Case) molecularStateType.getCases().get(0))
							.getQNs());
		} else if (caseType.equals(CaseType.LTCS)) {
			res = getQuantumNumbersLtcs(
					((org.vamdc.xsams.cases.ltcs.Case) molecularStateType.getCases().get(0))
							.getQNs());
		} else if (caseType.equals(CaseType.LTOS)) {
			res = getQuantumNumbersLtos(
					((org.vamdc.xsams.cases.ltos.Case) molecularStateType.getCases().get(0))
							.getQNs());
		} else if (caseType.equals(CaseType.NLTCS)) {
			res = getQuantumNumbersNltcs(
					((org.vamdc.xsams.cases.nltcs.Case) molecularStateType.getCases().get(0))
							.getQNs());
		} else if (caseType.equals(CaseType.NLTOS)) {
			res = getQuantumNumbersNltos(
					((org.vamdc.xsams.cases.nltos.Case) molecularStateType.getCases().get(0))
							.getQNs());
		} else if (caseType.equals(CaseType.SPHCS)) {
			res = getQuantumNumbersSphcs(
					((org.vamdc.xsams.cases.sphcs.Case) molecularStateType.getCases().get(0))
							.getQNs());
		} else if (caseType.equals(CaseType.SPHOS)) {
			res = getQuantumNumbersSphos(
					((org.vamdc.xsams.cases.sphos.Case) molecularStateType.getCases().get(0))
							.getQNs());
		} else if (caseType.equals(CaseType.STCS)) {
			res = getQuantumNumbersStcs(
					((org.vamdc.xsams.cases.stcs.Case) molecularStateType.getCases().get(0))
							.getQNs());
		} else if (caseType.equals(CaseType.LPCS)) {
			res = getQuantumNumbersLpcs(
					((org.vamdc.xsams.cases.lpcs.Case) molecularStateType.getCases().get(0))
							.getQNs());
		} else if (caseType.equals(CaseType.GEN)) {
			res = getQuantumNumbersGen(
					((org.vamdc.xsams.cases.gen.Case) molecularStateType.getCases().get(0))
							.getQNs());
		}
		StringBuilder builder = new StringBuilder();
		builder.append(caseType + " - ");
		int size = 0;
		for (String key : res.keySet()) {
			String str = key + '=' + res.get(key) + ", ";
			size += str.length();

			if (size > MAX_QUANTUM_NUMBERS_LENGTH) {
				str = str + '\n';
				size = 0;
			}

			builder.append(str);
		}
		return builder.toString();
	}

	private static Map<String, String> getQuantumNumbersGen(org.vamdc.xsams.cases.gen.QNs qNs) {
		Map<String, String> val = new HashMap<String, String>();
		if (qNs.getElecStateLabel() != null) {
			val.put("ElecState", qNs.getElecStateLabel());
		}
		if (qNs.getSyms() != null) {
			for (int i = 0; i < qNs.getSyms().size(); i++) {
				val.put("Sysm" + i + "Group", qNs.getSyms().get(i).getGroup());
				val.put("Sysm" + i + "Name", qNs.getSyms().get(i).getName());
				val.put("Sysm" + i + "Value", qNs.getSyms().get(i).getValue());
			}
		}
		if (qNs.getQNS() != null) {
			for (int i = 0; i < qNs.getQNS().size(); i++) {
				val.put("QNS" + i + "Group", qNs.getQNS().get(i).getName());
				val.put("QNS" + i + "Group", String.valueOf(qNs.getQNS().get(i).getValue()));
			}
		}

		return val;
	}

	private static Map<String, String> getQuantumNumbersLpcs(org.vamdc.xsams.cases.lpcs.QNs qNs) {
		Map<String, String> val = new HashMap<String, String>();
		if (qNs.getElecStateLabel() != null) {
			val.put("ElecState", qNs.getElecStateLabel());
		}
		if (qNs.getAsSym() != null) {
			val.put("AsSysm", qNs.getAsSym());
		}
		if (qNs.getF() != null) {
			val.put("F" + "Value", String.valueOf(qNs.getF().getValue()));
			val.put("F" + "NuclearSpinRef", qNs.getF().getNuclearSpinRef());
		}

		if (qNs.getFjs() != null) {
			for (int i = 0; i < qNs.getFjs().size(); i++) {
				val.put("Fjs" + i + "NuclearSpinRef", qNs.getFjs().get(i).getNuclearSpinRef());
				val.put("Fjs" + i + "Value", String.valueOf(qNs.getFjs().get(i).getValue()));
				val.put("Fjs" + i + "J", String.valueOf(qNs.getFjs().get(i).getJ()));
			}
		}
		if (qNs.getIS() != null) {
			for (int i = 0; i < qNs.getIS().size(); i++) {
				val.put("IS" + i + "NuclearSpinRef", qNs.getIS().get(i).getNuclearSpinRef());
				val.put("IS" + i + "Value", String.valueOf(qNs.getIS().get(i).getValue()));
				val.put("IS" + i + "Id", String.valueOf(qNs.getIS().get(i).getId()));
			}
		}
		if (qNs.getJ() != null) {
			val.put("J", String.valueOf(qNs.getJ()));
		}
		if (qNs.getKronigParity() != null) {
			val.put("KronigParity", qNs.getKronigParity());
		}
		if (qNs.getL() != null) {
			val.put("L", String.valueOf(qNs.getL()));
		}
		if (qNs.getLis() != null) {
			for (int i = 0; i < qNs.getLis().size(); i++) {
				val.put("Lis" + i + "Mode", String.valueOf(qNs.getLis().get(i).getMode()));
				val.put("Lis" + i + "Value", String.valueOf(qNs.getLis().get(i).getValue()));
			}
		}
		if (qNs.getParity() != null) {
			val.put("Parity", qNs.getParity());
		}
		if (qNs.getRS() != null) {
			for (int i = 0; i < qNs.getRS().size(); i++) {
				val.put("RS" + i + "Name", qNs.getRS().get(i).getName());
				val.put("RS" + i + "Value", String.valueOf(qNs.getRS().get(i).getValue()));
			}
		}
		if (qNs.getVibInv() != null) {
			val.put("VibInv", qNs.getVibInv());
		}
		if (qNs.getVibRefl() != null) {
			val.put("VibRefl", qNs.getVibRefl());
		}

		if (qNs.getVibSym() != null) {
			val.put("VibSym" + "Group", qNs.getVibSym().getGroup());
			val.put("VibSym" + "Value", qNs.getVibSym().getValue());
		}
		if (qNs.getVis() != null) {
			for (int i = 0; i < qNs.getVis().size(); i++) {
				val.put("Vis" + i + "Mode", String.valueOf(qNs.getVis().get(i).getMode()));
				val.put("Vis" + i + "Value", String.valueOf(qNs.getVis().get(i).getValue()));
			}
		}
		return val;
	}

	private static Map<String, String> getQuantumNumbersStcs(org.vamdc.xsams.cases.stcs.QNs qNs) {
		Map<String, String> val = new HashMap<String, String>();
		if (qNs.getElecStateLabel() != null) {
			val.put("ElecState", qNs.getElecStateLabel());
		}
		if (qNs.getF() != null) {
			val.put("F" + "Value", String.valueOf(qNs.getF().getValue()));
			val.put("F" + "NuclearSpinRef", qNs.getF().getNuclearSpinRef());
		}

		if (qNs.getFjs() != null) {
			for (int i = 0; i < qNs.getFjs().size(); i++) {
				val.put("Fjs" + i + "NuclearSpinRef", qNs.getFjs().get(i).getNuclearSpinRef());
				val.put("Fjs" + i + "Value", String.valueOf(qNs.getFjs().get(i).getValue()));
				val.put("Fjs" + i + "J", String.valueOf(qNs.getFjs().get(i).getJ()));
			}
		}
		if (qNs.getIS() != null) {
			for (int i = 0; i < qNs.getIS().size(); i++) {
				val.put("IS" + i + "NuclearSpinRef", qNs.getIS().get(i).getNuclearSpinRef());
				val.put("IS" + i + "Value", String.valueOf(qNs.getIS().get(i).getValue()));
				val.put("IS" + i + "Id", String.valueOf(qNs.getIS().get(i).getId()));
			}
		}
		if (qNs.getJ() != null) {
			val.put("J", String.valueOf(qNs.getJ()));
		}

		if (qNs.getK() != null) {
			val.put("K", String.valueOf(qNs.getK()));
		}

		if (qNs.getLis() != null) {
			for (int i = 0; i < qNs.getLis().size(); i++) {
				val.put("Lis" + i + "Mode", String.valueOf(qNs.getLis().get(i).getMode()));
				val.put("Lis" + i + "Value", String.valueOf(qNs.getLis().get(i).getValue()));
			}
		}

		if (qNs.getLS() != null) {
			for (int i = 0; i < qNs.getLS().size(); i++) {
				val.put("LS" + i, String.valueOf(qNs.getLS().get(i)));
			}
		}

		if (qNs.getParity() != null) {
			val.put("Parity", qNs.getParity());
		}
		if (qNs.getRotSym() != null) {
			val.put("RotSym" + "Group", qNs.getRotSym().getGroup());
			val.put("RotSym" + "Value", qNs.getRotSym().getValue());

		}
		if (qNs.getRovibSym() != null) {
			val.put("RovibSym" + "Group", qNs.getRovibSym().getGroup());
			val.put("RovibSym" + "Value", qNs.getRovibSym().getValue());
		}

		if (qNs.getRS() != null) {
			for (int i = 0; i < qNs.getRS().size(); i++) {
				val.put("RS" + i + "Name", qNs.getRS().get(i).getName());
				val.put("RS" + i + "Value", String.valueOf(qNs.getRS().get(i).getValue()));
			}
		}
		if (qNs.getVibInv() != null) {
			val.put("VibInv", qNs.getVibInv());
		}
		if (qNs.getVibSym() != null) {
			val.put("VibSym" + "Group", qNs.getVibSym().getGroup());
			val.put("VibSym" + "Value", qNs.getVibSym().getValue());
		}
		if (qNs.getVis() != null) {
			for (int i = 0; i < qNs.getVis().size(); i++) {
				val.put("Vis" + i + "Mode", String.valueOf(qNs.getVis().get(i).getMode()));
				val.put("Vis" + i + "Value", String.valueOf(qNs.getVis().get(i).getValue()));
			}
		}

		return val;
	}

	private static Map<String, String> getQuantumNumbersSphos(org.vamdc.xsams.cases.sphos.QNs qNs) {
		Map<String, String> val = new HashMap<String, String>();
		if (qNs.getElecStateLabel() != null) {
			val.put("ElecState", qNs.getElecStateLabel());
		}
		if (qNs.getElecInv() != null) {
			val.put("ElecInv", qNs.getElecInv());
		}

		if (qNs.getElecSym() != null) {
			val.put("ElecSym" + "Group", qNs.getElecSym().getGroup());
			val.put("ElecSym" + "Value", qNs.getElecSym().getValue());
		}
		if (qNs.getF() != null) {
			val.put("F" + "Value", String.valueOf(qNs.getF().getValue()));
			val.put("F" + "NuclearSpinRef", qNs.getF().getNuclearSpinRef());
		}
		if (qNs.getFjs() != null) {
			for (int i = 0; i < qNs.getFjs().size(); i++) {
				val.put("Fjs" + i + "NuclearSpinRef", qNs.getFjs().get(i).getNuclearSpinRef());
				val.put("Fjs" + i + "Value", String.valueOf(qNs.getFjs().get(i).getValue()));
				val.put("Fjs" + i + "J", String.valueOf(qNs.getFjs().get(i).getJ()));
			}
		}
		if (qNs.getI() != null) {
			val.put("I" + "Id", qNs.getI().getId());
			val.put("I" + "NuclearSpinRef", qNs.getI().getNuclearSpinRef());
			val.put("I" + "Value", String.valueOf(qNs.getI().getValue()));
		}
		if (qNs.getJ() != null) {
			val.put("J", String.valueOf(qNs.getJ()));
		}
		if (qNs.getLis() != null) {
			for (int i = 0; i < qNs.getLis().size(); i++) {
				val.put("Lis" + i + "Mode", String.valueOf(qNs.getLis().get(i).getMode()));
				val.put("Lis" + i + "Value", String.valueOf(qNs.getLis().get(i).getValue()));
			}
		}
		if (qNs.getN() != null) {
			val.put("N", String.valueOf(qNs.getN()));
		}

		if (qNs.getParity() != null) {
			val.put("Parity", qNs.getParity());
		}
		if (qNs.getRotSym() != null) {
			val.put("RotSym" + "Group", qNs.getRotSym().getGroup());
			val.put("RotSym" + "Value", qNs.getRotSym().getValue());
		}

		if (qNs.getRovibSym() != null) {
			val.put("RovibSym" + "Group", qNs.getRovibSym().getGroup());
			val.put("RovibSym" + "Value", qNs.getRovibSym().getValue());
		}

		if (qNs.getRS() != null) {
			for (int i = 0; i < qNs.getRS().size(); i++) {
				val.put("RS" + i + "Name", qNs.getRS().get(i).getName());
				val.put("RS" + i + "Value", String.valueOf(qNs.getRS().get(i).getValue()));
			}
		}
		if (qNs.getS() != null) {
			val.put("S", String.valueOf(qNs.getS()));
		}

		if (qNs.getSyms() != null) {
			for (int i = 0; i < qNs.getSyms().size(); i++) {
				val.put("Sysm" + i + "Group", qNs.getSyms().get(i).getGroup());
				val.put("Sysm" + i + "Name", qNs.getSyms().get(i).getName());
				val.put("Sysm" + i + "Value", qNs.getSyms().get(i).getValue());
			}
		}
		if (qNs.getVibSym() != null) {
			val.put("VibSym" + "Group", qNs.getVibSym().getGroup());
			val.put("VibSym" + "Value", qNs.getVibSym().getValue());
		}
		if (qNs.getVis() != null) {
			for (int i = 0; i < qNs.getVis().size(); i++) {
				val.put("Vis" + i + "Mode", String.valueOf(qNs.getVis().get(i).getMode()));
				val.put("Vis" + i + "Value", String.valueOf(qNs.getVis().get(i).getValue()));
			}
		}
		return val;
	}

	private static Map<String, String> getQuantumNumbersSphcs(org.vamdc.xsams.cases.sphcs.QNs qNs) {
		Map<String, String> val = new HashMap<String, String>();
		if (qNs.getElecStateLabel() != null) {
			val.put("ElecState", qNs.getElecStateLabel());
		}
		if (qNs.getF() != null) {
			val.put("F" + "Value", String.valueOf(qNs.getF().getValue()));
			val.put("F" + "NuclearSpinRef", qNs.getF().getNuclearSpinRef());
		}
		if (qNs.getFjs() != null) {
			for (int i = 0; i < qNs.getFjs().size(); i++) {
				val.put("Fjs" + i + "NuclearSpinRef", qNs.getFjs().get(i).getNuclearSpinRef());
				val.put("Fjs" + i + "Value", String.valueOf(qNs.getFjs().get(i).getValue()));
				val.put("Fjs" + i + "J", String.valueOf(qNs.getFjs().get(i).getJ()));
			}
		}
		if (qNs.getI() != null) {
			val.put("I" + "Id", qNs.getI().getId());
			val.put("I" + "NuclearSpinRef", qNs.getI().getNuclearSpinRef());
			val.put("I" + "Value", String.valueOf(qNs.getI().getValue()));
		}
		if (qNs.getJ() != null) {
			val.put("J", String.valueOf(qNs.getJ()));
		}
		if (qNs.getLis() != null) {
			for (int i = 0; i < qNs.getLis().size(); i++) {
				val.put("Lis" + i + "Mode", String.valueOf(qNs.getLis().get(i).getMode()));
				val.put("Lis" + i + "Value", String.valueOf(qNs.getLis().get(i).getValue()));
			}
		}
		if (qNs.getParity() != null) {
			val.put("Parity", qNs.getParity());
		}
		if (qNs.getRotSym() != null) {
			val.put("RotSym" + "Group", qNs.getRotSym().getGroup());
			val.put("RotSym" + "Value", qNs.getRotSym().getValue());
		}
		if (qNs.getRovibSym() != null) {
			val.put("RovibSym" + "Group", qNs.getRovibSym().getGroup());
			val.put("RovibSym" + "Value", qNs.getRovibSym().getValue());
		}
		if (qNs.getRS() != null) {
			for (int i = 0; i < qNs.getRS().size(); i++) {
				val.put("RS" + i + "Name", qNs.getRS().get(i).getName());
				val.put("RS" + i + "Value", String.valueOf(qNs.getRS().get(i).getValue()));
			}
		}
		if (qNs.getSyms() != null) {
			for (int i = 0; i < qNs.getSyms().size(); i++) {
				val.put("Sysm" + i + "Group", qNs.getSyms().get(i).getGroup());
				val.put("Sysm" + i + "Name", qNs.getSyms().get(i).getName());
				val.put("Sysm" + i + "Value", qNs.getSyms().get(i).getValue());
			}
		}
		if (qNs.getVibSym() != null) {
			val.put("VibSym" + "Group", qNs.getVibSym().getGroup());
			val.put("VibSym" + "Value", qNs.getVibSym().getValue());
		}
		if (qNs.getVis() != null) {
			for (int i = 0; i < qNs.getVis().size(); i++) {
				val.put("Vis" + i + "Mode", String.valueOf(qNs.getVis().get(i).getMode()));
				val.put("Vis" + i + "Value", String.valueOf(qNs.getVis().get(i).getValue()));
			}
		}
		return val;
	}

	private static Map<String, String> getQuantumNumbersNltos(org.vamdc.xsams.cases.nltos.QNs qNs) {
		Map<String, String> val = new HashMap<String, String>();
		if (qNs.getElecStateLabel() != null) {
			val.put("ElecState", qNs.getElecStateLabel());
		}
		if (qNs.getAsSym() != null) {
			val.put("AsSysm", qNs.getAsSym());
		}
		if (qNs.getElecSym() != null) {
			val.put("ElecSym" + "Group", qNs.getElecSym().getGroup());
			val.put("ElecSym" + "Value", qNs.getElecSym().getValue());
		}
		if (qNs.getF() != null) {
			val.put("F" + "Value", String.valueOf(qNs.getF().getValue()));
			val.put("F" + "NuclearSpinRef", qNs.getF().getNuclearSpinRef());
		}

		if (qNs.getF1() != null) {
			val.put("F1" + "NuclearSpinRef", qNs.getF1().getNuclearSpinRef());
			val.put("F1" + "Value", String.valueOf(qNs.getF1().getValue()));
		}

		if (qNs.getF2() != null) {
			val.put("F2" + "NuclearSpinRef", qNs.getF2().getNuclearSpinRef());
			val.put("F2" + "Value", String.valueOf(qNs.getF2().getValue()));
		}

		if (qNs.getI() != null) {
			val.put("I" + "Id", qNs.getI().getId());
			val.put("I" + "NuclearSpinRef", qNs.getI().getNuclearSpinRef());
			val.put("I" + "Value", String.valueOf(qNs.getI().getValue()));
		}
		if (qNs.getJ() != null) {
			val.put("J", String.valueOf(qNs.getJ()));
		}
		if (qNs.getKa() != null) {
			val.put("Ka", String.valueOf(qNs.getKa()));
		}
		if (qNs.getKc() != null) {
			val.put("Kc", String.valueOf(qNs.getKc()));
		}
		if (qNs.getKronigParity() != null) {
			val.put("KronigParity", qNs.getKronigParity());
		}
		if (qNs.getN() != null) {
			val.put("N", String.valueOf(qNs.getN()));
		}

		if (qNs.getParity() != null) {
			val.put("Parity", qNs.getParity());
		}
		if (qNs.getRS() != null) {
			for (int i = 0; i < qNs.getRS().size(); i++) {
				val.put("RS" + i + "Name", qNs.getRS().get(i).getName());
				val.put("RS" + i + "Value", String.valueOf(qNs.getRS().get(i).getValue()));
			}
		}
		if (qNs.getS() != null) {
			val.put("S", String.valueOf(qNs.getS()));
		}

		if (qNs.getV1() != null) {
			val.put("V1", String.valueOf(qNs.getV1()));
		}
		if (qNs.getV2() != null) {
			val.put("V2", String.valueOf(qNs.getV2()));
		}
		if (qNs.getV3() != null) {
			val.put("V3", String.valueOf(qNs.getV3()));
		}
		return val;
	}

	private static Map<String, String> getQuantumNumbersNltcs(org.vamdc.xsams.cases.nltcs.QNs qNs) {
		Map<String, String> val = new HashMap<String, String>();
		if (qNs.getElecStateLabel() != null) {
			val.put("ElecState", qNs.getElecStateLabel());
		}
		if (qNs.getAsSym() != null) {
			val.put("AsSysm", qNs.getAsSym());
		}
		if (qNs.getF() != null) {
			val.put("F" + "Value", String.valueOf(qNs.getF().getValue()));
			val.put("F" + "NuclearSpinRef", qNs.getF().getNuclearSpinRef());
		}
		if (qNs.getF1() != null) {
			val.put("F1" + "NuclearSpinRef", qNs.getF1().getNuclearSpinRef());
			val.put("F1" + "Value", String.valueOf(qNs.getF1().getValue()));
		}
		if (qNs.getF2() != null) {
			val.put("F2" + "NuclearSpinRef", qNs.getF2().getNuclearSpinRef());
			val.put("F2" + "Value", String.valueOf(qNs.getF2().getValue()));
		}
		if (qNs.getI() != null) {
			val.put("I" + "Id", qNs.getI().getId());
			val.put("I" + "NuclearSpinRef", qNs.getI().getNuclearSpinRef());
			val.put("I" + "Value", String.valueOf(qNs.getI().getValue()));
		}
		if (qNs.getJ() != null) {
			val.put("J", String.valueOf(qNs.getJ()));
		}
		if (qNs.getKa() != null) {
			val.put("Ka", String.valueOf(qNs.getKa()));
		}
		if (qNs.getKc() != null) {
			val.put("Kc", String.valueOf(qNs.getKc()));
		}
		if (qNs.getKronigParity() != null) {
			val.put("KronigParity", qNs.getKronigParity());
		}
		if (qNs.getParity() != null) {
			val.put("Parity", qNs.getParity());
		}
		if (qNs.getRS() != null) {
			for (int i = 0; i < qNs.getRS().size(); i++) {
				val.put("RS" + i + "Name", qNs.getRS().get(i).getName());
				val.put("RS" + i + "Value", String.valueOf(qNs.getRS().get(i).getValue()));
			}
		}
		if (qNs.getV1() != null) {
			val.put("V1", String.valueOf(qNs.getV1()));
		}
		if (qNs.getV2() != null) {
			val.put("V2", String.valueOf(qNs.getV2()));
		}
		if (qNs.getV3() != null) {
			val.put("V3", String.valueOf(qNs.getV3()));
		}
		return val;
	}

	private static Map<String, String> getQuantumNumbersLtos(org.vamdc.xsams.cases.ltos.QNs qNs) {
		Map<String, String> val = new HashMap<String, String>();
		if (qNs.getElecStateLabel() != null) {
			val.put("ElecState", qNs.getElecStateLabel());
		}
		if (qNs.getAsSym() != null) {
			val.put("AsSysm", qNs.getAsSym());
		}
		if (qNs.getElecInv() != null) {
			val.put("ElecInv", qNs.getElecInv());
		}
		if (qNs.getElecRefl() != null) {
			val.put("ElecRefl", qNs.getElecRefl());
		}

		if (qNs.getF() != null) {
			val.put("F" + "Value", String.valueOf(qNs.getF().getValue()));
			val.put("F" + "NuclearSpinRef", qNs.getF().getNuclearSpinRef());
		}
		if (qNs.getF1() != null) {
			val.put("F1" + "NuclearSpinRef", qNs.getF1().getNuclearSpinRef());
			val.put("F1" + "Value", String.valueOf(qNs.getF1().getValue()));
		}
		if (qNs.getF2() != null) {
			val.put("F2" + "NuclearSpinRef", qNs.getF2().getNuclearSpinRef());
			val.put("F2" + "Value", String.valueOf(qNs.getF2().getValue()));
		}
		if (qNs.getI() != null) {
			val.put("I" + "Id", qNs.getI().getId());
			val.put("I" + "NuclearSpinRef", qNs.getI().getNuclearSpinRef());
			val.put("I" + "Value", String.valueOf(qNs.getI().getValue()));
		}
		if (qNs.getJ() != null) {
			val.put("J", String.valueOf(qNs.getJ()));
		}
		if (qNs.getKronigParity() != null) {
			val.put("KronigParity", qNs.getKronigParity());
		}
		if (qNs.getL2() != null) {
			val.put("L2", String.valueOf(qNs.getL2()));
		}

		if (qNs.getLambda() != null) {
			val.put("Lambda", String.valueOf(qNs.getLambda()));
		}

		if (qNs.getN() != null) {
			val.put("N", String.valueOf(qNs.getN()));
		}
		if (qNs.getParity() != null) {
			val.put("Parity", qNs.getParity());
		}
		if (qNs.getR() != null) {
			val.put("R", qNs.getR().getName());
			val.put("R", String.valueOf(qNs.getR().getValue()));
		}

		if (qNs.getS() != null) {
			val.put("S", String.valueOf(qNs.getS()));
		}
		if (qNs.getV1() != null) {
			val.put("V1", String.valueOf(qNs.getV1()));
		}
		if (qNs.getV2() != null) {
			val.put("V2", String.valueOf(qNs.getV2()));
		}
		if (qNs.getV3() != null) {
			val.put("V3", String.valueOf(qNs.getV3()));
		}
		return val;
	}

	private static Map<String, String> getQuantumNumbersLtcs(org.vamdc.xsams.cases.ltcs.QNs qNs) {
		Map<String, String> val = new HashMap<String, String>();
		if (qNs.getElecStateLabel() != null) {
			val.put("ElecState", qNs.getElecStateLabel());
		}
		if (qNs.getAsSym() != null) {
			val.put("AsSysm", qNs.getAsSym());
		}
		if (qNs.getF() != null) {
			val.put("F" + "Value", String.valueOf(qNs.getF().getValue()));
			val.put("F" + "NuclearSpinRef", qNs.getF().getNuclearSpinRef());
		}
		if (qNs.getF1() != null) {
			val.put("F1" + "NuclearSpinRef", qNs.getF1().getNuclearSpinRef());
			val.put("F1" + "Value", String.valueOf(qNs.getF1().getValue()));
		}
		if (qNs.getF2() != null) {
			val.put("F2" + "NuclearSpinRef", qNs.getF2().getNuclearSpinRef());
			val.put("F2" + "Value", String.valueOf(qNs.getF2().getValue()));
		}
		if (qNs.getI() != null) {
			val.put("I" + "Id", qNs.getI().getId());
			val.put("I" + "NuclearSpinRef", qNs.getI().getNuclearSpinRef());
			val.put("I" + "Value", String.valueOf(qNs.getI().getValue()));
		}
		if (qNs.getJ() != null) {
			val.put("J", String.valueOf(qNs.getJ()));
		}
		if (qNs.getKronigParity() != null) {
			val.put("KronigParity", qNs.getKronigParity());
		}
		if (qNs.getL2() != null) {
			val.put("L2", String.valueOf(qNs.getL2()));
		}
		if (qNs.getParity() != null) {
			val.put("Parity", qNs.getParity());
		}
		if (qNs.getR() != null) {
			val.put("R", qNs.getR().getName());
			val.put("R", String.valueOf(qNs.getR().getValue()));
		}
		if (qNs.getV1() != null) {
			val.put("V1", String.valueOf(qNs.getV1()));
		}
		if (qNs.getV2() != null) {
			val.put("V2", String.valueOf(qNs.getV2()));
		}
		if (qNs.getV3() != null) {
			val.put("V3", String.valueOf(qNs.getV3()));
		}
		return val;
	}

	private static Map<String, String> getQuantumNumbersLpos(org.vamdc.xsams.cases.lpos.QNs qNs) {
		Map<String, String> val = new HashMap<String, String>();
		if (qNs.getElecStateLabel() != null) {
			val.put("ElecState", qNs.getElecStateLabel());
		}
		if (qNs.getAsSym() != null) {
			val.put("AsSysm", qNs.getAsSym());
		}
		if (qNs.getElecInv() != null) {
			val.put("ElecInv", qNs.getElecInv());
		}

		if (qNs.getElecRefl() != null) {
			val.put("ElecRefl", qNs.getElecRefl());
		}
		if (qNs.getF() != null) {
			val.put("F" + "Value", String.valueOf(qNs.getF().getValue()));
			val.put("F" + "NuclearSpinRef", qNs.getF().getNuclearSpinRef());
		}
		if (qNs.getFjs() != null) {
			for (int i = 0; i < qNs.getFjs().size(); i++) {
				val.put("Fjs" + i + "NuclearSpinRef", qNs.getFjs().get(i).getNuclearSpinRef());
				val.put("Fjs" + i + "Value", String.valueOf(qNs.getFjs().get(i).getValue()));
				val.put("Fjs" + i + "J", String.valueOf(qNs.getFjs().get(i).getJ()));
			}
		}
		if (qNs.getI() != null) {
			val.put("I" + "Id", qNs.getI().getId());
			val.put("I" + "NuclearSpinRef", qNs.getI().getNuclearSpinRef());
			val.put("I" + "Value", String.valueOf(qNs.getI().getValue()));
		}
		if (qNs.getJ() != null) {
			val.put("J", String.valueOf(qNs.getJ()));
		}
		if (qNs.getKronigParity() != null) {
			val.put("KronigParity", qNs.getKronigParity());
		}
		if (qNs.getL() != null) {
			val.put("L", String.valueOf(qNs.getL()));
		}
		if (qNs.getLambda() != null) {
			val.put("Lambda", String.valueOf(qNs.getLambda()));
		}
		if (qNs.getLis() != null) {
			for (int i = 0; i < qNs.getLis().size(); i++) {
				val.put("Lis" + i + "Mode", String.valueOf(qNs.getLis().get(i).getMode()));
				val.put("Lis" + i + "Value", String.valueOf(qNs.getLis().get(i).getValue()));
			}
		}
		if (qNs.getN() != null) {
			val.put("N", String.valueOf(qNs.getN()));
		}

		if (qNs.getParity() != null) {
			val.put("Parity", qNs.getParity());
		}
		if (qNs.getRS() != null) {
			for (int i = 0; i < qNs.getRS().size(); i++) {
				val.put("RS" + i + "Name", qNs.getRS().get(i).getName());
				val.put("RS" + i + "Value", String.valueOf(qNs.getRS().get(i).getValue()));
			}
		}
		if (qNs.getS() != null) {
			val.put("S", String.valueOf(qNs.getS()));
		}

		if (qNs.getVibInv() != null) {
			val.put("VibInv", qNs.getVibInv());
		}
		if (qNs.getVibRefl() != null) {
			val.put("VibRefl", qNs.getVibRefl());
		}

		if (qNs.getVis() != null) {
			for (int i = 0; i < qNs.getVis().size(); i++) {
				val.put("Vis" + i + "Mode", String.valueOf(qNs.getVis().get(i).getMode()));
				val.put("Vis" + i + "Value", String.valueOf(qNs.getVis().get(i).getValue()));
			}
		}
		return val;
	}

	private static Map<String, String> getQuantumNumbersHundb(org.vamdc.xsams.cases.hundb.QNs qNs) {
		Map<String, String> val = new HashMap<String, String>();
		if (qNs.getElecStateLabel() != null) {
			val.put("ElecState", qNs.getElecStateLabel());
		}
		if (qNs.getAsSym() != null) {
			val.put("AsSysm", qNs.getAsSym());
		}
		if (qNs.getElecInv() != null) {
			val.put("ElecInv", qNs.getElecInv());
		}

		if (qNs.getElecRefl() != null) {
			val.put("ElecRefl", qNs.getElecRefl());
		}
		if (qNs.getF() != null) {
			val.put("F" + "Value", String.valueOf(qNs.getF().getValue()));
			val.put("F" + "NuclearSpinRef", qNs.getF().getNuclearSpinRef());
		}
		if (qNs.getF1() != null) {
			val.put("F1" + "NuclearSpinRef", qNs.getF1().getNuclearSpinRef());
			val.put("F1" + "Value", String.valueOf(qNs.getF1().getValue()));
		}
		if (qNs.getI() != null) {
			val.put("I" + "Id", qNs.getI().getId());
			val.put("I" + "NuclearSpinRef", qNs.getI().getNuclearSpinRef());
			val.put("I" + "Value", String.valueOf(qNs.getI().getValue()));
		}
		if (qNs.getJ() != null) {
			val.put("J", String.valueOf(qNs.getJ()));
		}
		if (qNs.getKronigParity() != null) {
			val.put("KronigParity", qNs.getKronigParity());
		}
		if (qNs.getLambda() != null) {
			val.put("Lambda", String.valueOf(qNs.getLambda()));
		}
		if (qNs.getN() != null) {
			val.put("N", String.valueOf(qNs.getN()));
		}

		if (qNs.getParity() != null) {
			val.put("Parity", qNs.getParity());
		}
		if (qNs.getR() != null) {
			val.put("R", qNs.getR().getName());
			val.put("R", String.valueOf(qNs.getR().getValue()));
		}
		if (qNs.getS() != null) {
			val.put("S", String.valueOf(qNs.getS()));
		}

		if (qNs.getSpinComponentLabel() != null) {
			val.put("SpinComponentLabel", String.valueOf(qNs.getSpinComponentLabel()));
		}

		if (qNs.getV() != null) {
			val.put("V", String.valueOf(qNs.getV()));
		}

		return val;
	}

	private static Map<String, String> getQuantumNumbersHunda(org.vamdc.xsams.cases.hunda.QNs qNs) {
		Map<String, String> val = new HashMap<String, String>();
		if (qNs.getElecStateLabel() != null) {
			val.put("ElecState", qNs.getElecStateLabel());
		}
		if (qNs.getAsSym() != null) {
			val.put("AsSysm", qNs.getAsSym());
		}
		if (qNs.getElecInv() != null) {
			val.put("ElecInv", qNs.getElecInv());
		}

		if (qNs.getElecRefl() != null) {
			val.put("ElecRefl", qNs.getElecRefl());
		}
		if (qNs.getF() != null) {
			val.put("F" + "Value", String.valueOf(qNs.getF().getValue()));
			val.put("F" + "NuclearSpinRef", qNs.getF().getNuclearSpinRef());
		}
		if (qNs.getF1() != null) {
			val.put("F1" + "NuclearSpinRef", qNs.getF1().getNuclearSpinRef());
			val.put("F1" + "Value", String.valueOf(qNs.getF1().getValue()));
		}
		if (qNs.getI() != null) {
			val.put("I" + "Id", qNs.getI().getId());
			val.put("I" + "NuclearSpinRef", qNs.getI().getNuclearSpinRef());
			val.put("I" + "Value", String.valueOf(qNs.getI().getValue()));
		}
		if (qNs.getJ() != null) {
			val.put("J", String.valueOf(qNs.getJ()));
		}
		if (qNs.getKronigParity() != null) {
			val.put("KronigParity", qNs.getKronigParity());
		}
		if (qNs.getLambda() != null) {
			val.put("Lambda", String.valueOf(qNs.getLambda()));
		}

		if (qNs.getOmega() != null) {
			val.put("Omega", String.valueOf(qNs.getOmega()));
		}
		if (qNs.getParity() != null) {
			val.put("Parity", qNs.getParity());
		}
		if (qNs.getR() != null) {
			val.put("R", qNs.getR().getName());
			val.put("R", String.valueOf(qNs.getR().getValue()));
		}
		if (qNs.getS() != null) {
			val.put("S", String.valueOf(qNs.getS()));
		}
		if (qNs.getSigma() != null) {
			val.put("Sigma", String.valueOf(qNs.getSigma()));
		}
		if (qNs.getV() != null) {
			val.put("V", String.valueOf(qNs.getV()));
		}

		return val;
	}

	private static Map<String, String> getQuantumNumbersDcs(org.vamdc.xsams.cases.dcs.QNs qNs) {
		Map<String, String> val = new HashMap<String, String>();
		if (qNs.getElecStateLabel() != null) {
			val.put("ElecState", qNs.getElecStateLabel());
		}
		if (qNs.getAsSym() != null) {
			val.put("AsSysm", qNs.getAsSym());
		}
		if (qNs.getF() != null) {
			val.put("F" + "Value", String.valueOf(qNs.getF().getValue()));
			val.put("F" + "NuclearSpinRef", qNs.getF().getNuclearSpinRef());
		}
		if (qNs.getF1() != null) {
			val.put("F1" + "NuclearSpinRef", qNs.getF1().getNuclearSpinRef());
			val.put("F1" + "Value", String.valueOf(qNs.getF1().getValue()));
		}
		if (qNs.getI() != null) {
			val.put("I" + "Id", qNs.getI().getId());
			val.put("I" + "NuclearSpinRef", qNs.getI().getNuclearSpinRef());
			val.put("I" + "Value", String.valueOf(qNs.getI().getValue()));
		}
		if (qNs.getJ() != null) {
			val.put("J", String.valueOf(qNs.getJ()));
		}
		if (qNs.getKronigParity() != null) {
			val.put("KronigParity", qNs.getKronigParity());
		}
		if (qNs.getParity() != null) {
			val.put("Parity", qNs.getParity());
		}
		if (qNs.getRS() != null) {
			for (int i = 0; i < qNs.getRS().size(); i++) {
				val.put("RS" + i + "Name", qNs.getRS().get(i).getName());
				val.put("RS" + i + "Value", String.valueOf(qNs.getRS().get(i).getValue()));
			}
		}
		if (qNs.getV() != null) {
			val.put("V", String.valueOf(qNs.getV()));
		}
		return val;
	}

	private static Map<String, String> getQuantumNumbersAsymos(
			org.vamdc.xsams.cases.asymos.QNs qNs) {
		Map<String, String> val = new HashMap<String, String>();
		if (qNs.getElecStateLabel() != null) {
			val.put("ElecState", qNs.getElecStateLabel());
		}
		if (qNs.getElecInv() != null) {
			val.put("ElecInv", qNs.getElecInv());
		}

		if (qNs.getElecSym() != null) {
			val.put("ElecSym" + "Group", qNs.getElecSym().getGroup());
			val.put("ElecSym" + "Value", qNs.getElecSym().getValue());
		}
		if (qNs.getF() != null) {
			val.put("F" + "Value", String.valueOf(qNs.getF().getValue()));
			val.put("F" + "NuclearSpinRef", qNs.getF().getNuclearSpinRef());
		}
		if (qNs.getFjs() != null) {
			for (int i = 0; i < qNs.getFjs().size(); i++) {
				val.put("Fjs" + i + "NuclearSpinRef", qNs.getFjs().get(i).getNuclearSpinRef());
				val.put("Fjs" + i + "Value", String.valueOf(qNs.getFjs().get(i).getValue()));
				val.put("Fjs" + i + "J", String.valueOf(qNs.getFjs().get(i).getJ()));
			}
		}
		if (qNs.getI() != null) {
			val.put("I" + "Id", qNs.getI().getId());
			val.put("I" + "NuclearSpinRef", qNs.getI().getNuclearSpinRef());
			val.put("I" + "Value", String.valueOf(qNs.getI().getValue()));
		}
		if (qNs.getJ() != null) {
			val.put("J", String.valueOf(qNs.getJ()));
		}
		if (qNs.getKa() != null) {
			val.put("Ka", String.valueOf(qNs.getKa()));
		}
		if (qNs.getKc() != null) {
			val.put("Kc", String.valueOf(qNs.getKc()));
		}
		if (qNs.getN() != null) {
			val.put("N", String.valueOf(qNs.getN()));
		}

		if (qNs.getParity() != null) {
			val.put("Parity", qNs.getParity());
		}
		if (qNs.getRotSym() != null) {
			val.put("RotSym" + "Group", qNs.getRotSym().getGroup());
			val.put("RotSym" + "Value", qNs.getRotSym().getValue());
		}
		if (qNs.getRovibSym() != null) {
			val.put("RovibSym" + "Group", qNs.getRovibSym().getGroup());
			val.put("RovibSym" + "Value", qNs.getRovibSym().getValue());
		}
		if (qNs.getRS() != null) {
			for (int i = 0; i < qNs.getRS().size(); i++) {
				val.put("RS" + i + "Name", qNs.getRS().get(i).getName());
				val.put("RS" + i + "Value", String.valueOf(qNs.getRS().get(i).getValue()));
			}
		}
		if (qNs.getS() != null) {
			val.put("S", String.valueOf(qNs.getS()));
		}
		if (qNs.getVibInv() != null) {
			val.put("VibInv", qNs.getVibInv());
		}
		if (qNs.getVibSym() != null) {
			val.put("VibSym" + "Group", qNs.getVibSym().getGroup());
			val.put("VibSym" + "Value", qNs.getVibSym().getValue());
		}
		if (qNs.getVis() != null) {
			for (int i = 0; i < qNs.getVis().size(); i++) {
				val.put("Vis" + i + "Mode", String.valueOf(qNs.getVis().get(i).getMode()));
				val.put("Vis" + i + "Value", String.valueOf(qNs.getVis().get(i).getValue()));
			}
		}
		return val;
	}

	private static Map<String, String> getQuantumNumbersAsymcs(
			org.vamdc.xsams.cases.asymcs.QNs qNs) {
		Map<String, String> val = new HashMap<String, String>();
		if (qNs.getElecStateLabel() != null) {
			val.put("ElecState", qNs.getElecStateLabel());
		}
		if (qNs.getF() != null) {
			val.put("F" + "Value", String.valueOf(qNs.getF().getValue()));
			val.put("F" + "NuclearSpinRef", qNs.getF().getNuclearSpinRef());
		}
		if (qNs.getFjs() != null) {
			for (int i = 0; i < qNs.getFjs().size(); i++) {
				val.put("Fjs" + i + "NuclearSpinRef", qNs.getFjs().get(i).getNuclearSpinRef());
				val.put("Fjs" + i + "Value", String.valueOf(qNs.getFjs().get(i).getValue()));
				val.put("Fjs" + i + "J", String.valueOf(qNs.getFjs().get(i).getJ()));
			}
		}
		if (qNs.getI() != null) {
			val.put("I" + "Id", qNs.getI().getId());
			val.put("I" + "NuclearSpinRef", qNs.getI().getNuclearSpinRef());
			val.put("I" + "Value", String.valueOf(qNs.getI().getValue()));
		}
		if (qNs.getJ() != null) {
			val.put("J", String.valueOf(qNs.getJ()));
		}
		if (qNs.getKa() != null) {
			val.put("Ka", String.valueOf(qNs.getKa()));
		}
		if (qNs.getKc() != null) {
			val.put("Kc", String.valueOf(qNs.getKc()));
		}
		if (qNs.getParity() != null) {
			val.put("Parity", qNs.getParity());
		}
		if (qNs.getRotSym() != null) {
			val.put("RotSym" + "Group", qNs.getRotSym().getGroup());
			val.put("RotSym" + "Value", qNs.getRotSym().getValue());
		}
		if (qNs.getRovibSym() != null) {
			val.put("RovibSym" + "Group", qNs.getRovibSym().getGroup());
			val.put("RovibSym" + "Value", qNs.getRovibSym().getValue());
		}
		if (qNs.getVibInv() != null) {
			val.put("VibInv", qNs.getVibInv());
		}
		if (qNs.getVibSym() != null) {
			val.put("VibSym" + "Group", qNs.getVibSym().getGroup());
			val.put("VibSym" + "Value", qNs.getVibSym().getValue());
		}
		if (qNs.getVis() != null) {
			for (int i = 0; i < qNs.getVis().size(); i++) {
				val.put("Vis" + i + "Mode", String.valueOf(qNs.getVis().get(i).getMode()));
				val.put("Vis" + i + "Value", String.valueOf(qNs.getVis().get(i).getValue()));
			}
		}
		return val;
	}

	public static String getQuantumNumbers(AtomicStateType atomicStateType) {
		String res = "";

		return res;
	}

	public static void main(String[] args) {
		AtomicQuantumNumbersType atomicQuantumNumbers = new AtomicQuantumNumbersType();
		Method[] methods = atomicQuantumNumbers.getClass().getMethods();
		for (Method method : methods) {
			String name = method.getName();
			if (name.startsWith("get")&& !(name.equals("getClass") || name.equals("getParent"))){
				System.out.println(name);
				String name2 = method.getReturnType().getName();
				if (!name2.equals("java.lang.Double")){
					System.out.println("\t"+ name2);
					Method[] methods2 = method.getReturnType().getMethods();
					for (Method method2 : methods2) {
						String name3 = method2.getName();
						if ((name3.startsWith("get") || name3.equals("value"))&&
							!(name3.equals("getClass") || name3.equals("getParent"))){
							System.out.println("\t\t"+name3);
						}
					}
				}
			}
		}
	}

}
