/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.access;

import java.util.Arrays;

public class MoleculeDescriptionDB extends SimpleMoleculeDescriptionDB {

	private double[] tempList = new double[] { 10 };
	private double[] qlogsList = new double[] { 10 };
	private double qlogAienstein300;
	private boolean isCompleted = true;

	/**
	 *
	 * @param tag
	 * @param name
	 * @param tempList
	 * @param qlogsList
	 */
	public MoleculeDescriptionDB(int tag, String name, double[] tempList,
			double[] qlogsList) {
		super(tag, name);
		if (tempList != null) {
			this.tempList = Arrays.copyOf(tempList, tempList.length);
		}
		if (qlogsList != null) {
			this.qlogsList = Arrays.copyOf(qlogsList, qlogsList.length);
		}
	}

	public double[] getTemp() {
		return tempList;
	}

	public void setTemp(double[] tempList) {
		this.tempList = Arrays.copyOf(tempList, tempList.length);
	}

	public double[] getQlog() {
		return qlogsList;
	}

	public void setQlog(double[] qlogList) {
		this.qlogsList = Arrays.copyOf(qlogList, qlogList.length);
	}

	/**
	 * Computation of the partition function
	 *
	 * @param temperature
	 * @return
	 */
	public double buildQt(double temperature) {
		double[] zlpArray = qlogsList;
		double[] zptArray = tempList;

		// Check moleculaire in the list file
		if (SqlPartitionMole.inSqlPartitionMoleList(tag)) {
			SqlPartitionMole sqlPartitionMole = new SqlPartitionMole();
			sqlPartitionMole.loadSqlPartitionMoleFile(tag);
			zlpArray = sqlPartitionMole.getZplArray();
			zptArray = sqlPartitionMole.getZptArray();
		}

		// calcul qt by temperature
		if (zptArray.length == 0) {
			return 0;
		}
		return sqlPartitionFunction2(temperature, zptArray, zlpArray);
	}

	/**
	 * Compute the value of the partition function of temperature
	 * with the linear function (zptArray, zlpArray)
	 * @param temperature : value to compute
	 * @param zptArray : array of temperature in K
	 * @param zlpArray : array of the qlogs corresponding to the param zptArray
	 * @return the partition number
	 */
	public static double sqlPartitionFunction2(final double temperature,
			final double[] zptArray, final double[] zlpArray) {
		double qt = 1.;

		int n = zptArray.length;
		double[] zp = new double[n];

		for (int i = 0; i < n; i++) {
			zp[i] = Math.pow(10.0, zlpArray[i]);

		}
		// --- linear interpolation of the partition function (Formula P6)
		// --- find the highest temperature for which zp[] is valid

		if (zptArray[0] <= temperature) {
			qt = zp[0];
		} else if (zptArray[n - 1] >= temperature) {
			qt = zp[n - 1];
		} else {
			for (int i = 0; i < n; i++) {
				if (zptArray[i] <= temperature) {

					qt = zp[i - 1]
							+ ((zp[i] - zp[i - 1])
									* (zptArray[i - 1] - temperature) / (zptArray[i - 1] - zptArray[i]));
					break;
				}
			}
		}
		return qt;
	}

	public static String constitueLine(String cq) {
		String cquTrans;
		String cqlTrans;
		try {
			cquTrans = "";
			cqlTrans = "";
			String[] cqTab = cq.split(":");
			int size = cqTab.length / 2;
			for (int i = 0; i < size - 1; i++) {
				cquTrans += cqTab[i] + " ";
				cqlTrans += cqTab[i + size] + " ";
			}
			cquTrans += cqTab[size - 1];
			cqlTrans += cqTab[size * 2 - 1];
		} catch (Exception e) {
			return "(" + cq + ")";
		}

		return "(" + cquTrans + " _ " + cqlTrans + ")";
	}

	/**
	 * Constitue the jUp or the jLow with the first quantic number
	 *
	 * @param j
	 * @return an int : the jUp or the jLow
	 */
	public static int constituteTheJ(String j) {
		try {
			return Integer.parseInt(j);
		} catch (Exception e) {
			e.printStackTrace();
			// the number is not a integer
			double jDble;
			jDble = Double.parseDouble(j);
			// we add 0.5 for find the number in the original database
			jDble = jDble + 0.5;
			return (int) jDble;
		}
	}

	/**
	 * @return the qlogAienstein300
	 */
	public double getQlogAienstein300() {
		return qlogAienstein300;
	}

	/**
	 * @param qlogAienstein300 the qlogAienstein300 to set
	 */
	public void setQlogAienstein300(double qlogAienstein300) {
		this.qlogAienstein300 = qlogAienstein300;
	}

	@Override
	public String toString() {
		return super.toString() +
				"MoleculeDescriptionDB [tempList=" + Arrays.toString(tempList) + ", qlogsList="
				+ Arrays.toString(qlogsList) + ", qlogAienstein300=" + qlogAienstein300 +
				", isCompleted=" + isCompleted +"]";
	}

	public void setCompleted(boolean isCompleted) {
		this.isCompleted  = isCompleted;

	}

	public boolean isCompleted() {
		return isCompleted;
	}
}
