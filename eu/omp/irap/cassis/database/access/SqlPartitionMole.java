/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.access;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Sql partition moleculaire file management
 *
 * @author thachtn, glorian
 *
 */
public class SqlPartitionMole {

	public static final double[] ZPT = { 1000, 500, 300, 225, 150, 75, 37.5,
			18.75, 9.375, /* 0. */};

	private static final Logger logger = Logger
			.getLogger(SqlPartitionMole.class.getName());
	private static List<Integer> sqlPartitionMoleList;
	private static boolean firstReaded = true;
	private static final String SQL_PART_MOLE_PATH = DatabaseProperties
			.getPartionMolePath();

	/**
	 *  field zptArray : temperatures in descending order
	 */
	private double[] zptArray;
	/* qts */
	private double[] zplArray;


	/**
	 * Constructor
	 */
	public SqlPartitionMole() {
	}

	/**
	 * @return the zptArray
	 */
	public double[] getZptArray() {
		return zptArray;
	}

	/**
	 * @return the zplArray
	 */
	public double[] getZplArray() {
		return zplArray;
	}

	/**
	 * get sql partion mole tags list
	 *
	 * @return the list of tag
	 */
	public static List<Integer> getSqlPartionMoleTagList() {
		if (firstReaded) {
			sqlPartitionMoleList = new ArrayList<Integer>();
			File dir = new File(SQL_PART_MOLE_PATH);
			if (!dir.exists()) {
				firstReaded = false;
				return sqlPartitionMoleList;
			}

			String[] files = dir.list(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					return name.endsWith(".txt");
				}
			});

			for (int i = 0; i < files.length; i++) {
				String tag = files[i].substring(0, files[i].length() - 4);
				try {
					int tagInt = Integer.parseInt(tag);
					sqlPartitionMoleList.add(tagInt);
				} catch (NumberFormatException e) {
					logger.severe("Sql partion file name invalid." + e.getMessage());
				}
			}

			firstReaded = false;
		}
		return sqlPartitionMoleList;
	}

	/**
	 * check molecule tag in the list
	 *
	 * @param tag
	 * @return true if the tag is in the list
	 */
	public static boolean inSqlPartitionMoleList(int tag) {
		sqlPartitionMoleList = getSqlPartionMoleTagList();
		return sqlPartitionMoleList.contains(tag);
	}

	/**
	 * load sql partition mole from file
	 *
	 * @param tag
	 */
	public void loadSqlPartitionMoleFile(int tag) {
		if (sqlPartitionMoleList.contains(tag)) {
			List<Double> zptList = new ArrayList<Double>();
			List<Double> zplList = new ArrayList<Double>();
			File file = new File(SQL_PART_MOLE_PATH + File.separator
					+ Integer.toString(tag) + ".txt");

			try (BufferedReader dataBR = new BufferedReader(new FileReader(file))) {
				String line = dataBR.readLine(); // Description
				line = dataBR.readLine(); // Column description
				while ((line = dataBR.readLine()) != null) {
					line = reduce(line);
					if (!line.startsWith("//")){
						String[] dd = line.split(" ");
						Double zpt = Double.parseDouble(dd[0]);
						Double zpl = Double.parseDouble(dd[1]);
						zptList.add(zpt);
						zplList.add(zpl);
					}
				}
				int n = zptList.size();
				zptArray = new double[n];
				zplArray = new double[n];
				//reverse the order of the temperature
				for (int i = 0; i < n; i++) {
					zptArray[n - i - 1] = zptList.get(i);
					zplArray[n - i - 1] = zplList.get(i);
				}
			} catch (IOException e) {
				logger.severe(e.getMessage());
			} catch (Exception e) {
				logger.severe("Format sqlPartionMole file invalid.");
			}
		}
	}

	public static String reduce(String line) {
		line = line.replace('\t', ' ');
		line = line.trim();
		while (line.contains("  ")) {
			line = line.replaceAll("  ", " ");
		}
		if (line.startsWith(" ")) {
			line = line.substring(1);
		}
		return line;
	}

	public static String getPartMolePath() {
		return SQL_PART_MOLE_PATH;
	}

	public static void setFirstReaded(boolean firstReaded) {
		SqlPartitionMole.firstReaded = firstReaded;
	}

}
