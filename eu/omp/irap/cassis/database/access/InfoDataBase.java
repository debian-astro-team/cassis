/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.access;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Class to keep the parameters of the Database, reading and writing it on file.
 */
public class InfoDataBase {

	private static final String DATABASE_PROPERTIE = "dataBaseName";
	private static final String DATABASE_TYPE_PROPERTIE = "sgbd";
	private static final String DATABASE_ON_MEMORY = "databaseOnMemory";

	private static InfoDataBase instance;

	private Runnable postDbChangeAction;
	private String name = "No";
	private TypeDataBase typeDb = TypeDataBase.NO;
	private boolean inMemoryDatabase;

	private static final Logger logger = Logger.getLogger(InfoDataBase.class.getName());


	/**
	 * Create an instance of the class, read database parameters values on file.
	 *
	 * @return the instance of {@link InfoDataBase}.
	 */
	public static InfoDataBase getInstance() {
		if (instance == null) {
			instance = new InfoDataBase();
			Properties properties = new Properties();
			File file = new File(DatabaseProperties.getPropertyFile());
			try (FileInputStream fis = new FileInputStream(file)) {
				properties.load(fis);
				if (properties != null) {
					instance.typeDb = TypeDataBase.valueOf(properties.getProperty(DATABASE_TYPE_PROPERTIE).toUpperCase());
					instance.name = properties.getProperty(DATABASE_PROPERTIE);
					instance.inMemoryDatabase = Boolean.parseBoolean(properties.getProperty(DATABASE_ON_MEMORY, "false"));
				}
			} catch (Exception e) {
				logger.severe(e.getMessage());
			}
		}
		return instance;
	}

	/**
	 * Change the database.
	 *
	 * @param typeDb The type of database to use (String version of {@link TypeDataBase}).
	 * @param name The name/path/URL of the dababase.
	 * @param inMemoryDb If the database should be on memory (on SQLite only).
	 */
	public void setDataBaseType(String typeDb, String name, boolean inMemoryDb) {
		if (typeDb != null) {
			this.typeDb = TypeDataBase.valueOf(typeDb.toUpperCase());
			this.name = name;
			this.inMemoryDatabase = inMemoryDb;
			saveConfiguration(typeDb, name, inMemoryDb);
			AccessDataBase.initConnection();
			DatabaseHistory.getInstance().addDatabase(this.typeDb, this.name);
			AccessDataBase.getDataBaseConnection().getAllMoleculeDescriptionDB();
			if (postDbChangeAction != null) {
				postDbChangeAction.run();
			}
		}
	}

	/**
	 * Change the database using an already created DataBaseConnection object.
	 *
	 * @param typeDb The type of database to use (String version of {@link TypeDataBase}).
	 * @param name The name/path/URL of the dababase.
	 * @param inMemoryDb If the database should be on memory (on SQLite only).
	 * @param db The DataBaseConnection to use.
	 */
	public void setDataBaseType(String typeDb, String name, boolean inMemoryDb,
			DataBaseConnection db) {
		if (typeDb != null) {
			this.typeDb = TypeDataBase.valueOf(typeDb.toUpperCase());
			this.name = name;
			this.inMemoryDatabase = inMemoryDb;
			saveConfiguration(typeDb, name, inMemoryDb);
			AccessDataBase.initConnection(db);
			DatabaseHistory.getInstance().addDatabase(this.typeDb, this.name);
			if (postDbChangeAction != null) {
				postDbChangeAction.run();
			}
		}
	}

	/**
	 * Return the database name/path/url.
	 *
	 * @return the database name/path/url.
	 */
	public String getDbname() {
		return name;
	}

	/**
	 * Change the databse name/path/url.
	 *
	 * @param absolutePath The new value to use for database name/path/url.
	 */
	public void setDbname(String absolutePath) {
		setDataBaseType(typeDb.toString(), absolutePath, inMemoryDatabase);
	}

	/**
	 * Return the {@link TypeDataBase} currently used.
	 *
	 * @return the {@link TypeDataBase} currently used.
	 */
	public TypeDataBase getTypeDb() {
		return typeDb;
	}

	/**
	 * Change the {@link TypeDataBase} to use.
	 *
	 * @param typeDb The new {@link TypeDataBase} to use.
	 */
	public void setTypeDb(TypeDataBase typeDb) {
		setDataBaseType(typeDb.toString(), name, inMemoryDatabase);
	}

	/**
	 * Return a {@link String} with information on current database.
	 *
	 * @return a {@link String} with information on current database.
	 */
	public static String getInfo() {
		StringBuilder sb = new StringBuilder();
		if (InfoDataBase.getInstance().getTypeDb() == TypeDataBase.NO) {
			sb.append("no database selected");
		} else {
			sb.append("database from ");
			sb.append(InfoDataBase.getInstance().getTypeDb());
			sb.append(" ");
			sb.append("(");
			sb.append(AccessDataBase.getDataBaseConnection().getId());
			sb.append(")");
		}
		return sb.toString();
	}

	/**
	 * Return a {@link String} version of the currently used {@link TypeDataBase}.
	 *
	 * @return a {@link String} version of the currently used {@link TypeDataBase}.
	 */
	public String getDatabaseType() {
		return this.typeDb.name();
	}

	/**
	 * Return if the database should be in memory, for SQLite only.
	 *
	 * @return if the database should be in memory, for SQLite only.
	 */
	public boolean isInMemoryDatabase() {
		return inMemoryDatabase;
	}

	/**
	 * Set an action to execute after a database change.
	 *
	 * @param action The action to execute after a database change.
	 */
	public void setPostDatabaseChangeAction(Runnable action) {
		this.postDbChangeAction = action;
	}

	/**
	 * Save configuration.
	 *
	 * @param typeDb The type of database to use (String version of {@link TypeDataBase}).
	 * @param name The name/path/URL of the dababase.
	 * @param inMemoryDb If the database should be on memory (on SQLite only).
	 */
	private void saveConfiguration(String typeDb, String name, boolean inMemoryDb) {
		Properties props = new Properties();
		FileInputStream fis = null;
		FileOutputStream fos = null;
		try {
			File file = new File(DatabaseProperties.getPropertyFile());
			fis = new FileInputStream(file);
			props.load(fis);
			props.setProperty(DATABASE_PROPERTIE, name);
			props.setProperty(DATABASE_TYPE_PROPERTIE, typeDb);
			props.setProperty(DATABASE_ON_MEMORY, String.valueOf(inMemoryDb));
			fos = new FileOutputStream(file);
			props.store(fos, "Cassis properties");
		} catch (Exception e) {
			logger.severe(e.getMessage());
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
				}
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
				}
			}
		}
	}
}
