/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.access;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.vamdc.xsams.io.IOSettings;
import org.vamdc.xsams.io.JAXBContextFactory;
import org.vamdc.xsams.io.Output;
import org.vamdc.xsams.schema.XSAMSData;

import eu.omp.irap.cassis.common.Formula;
import eu.omp.irap.cassis.database.access.vamdc.CdmsJplVamdcDataBaseConnection;
import eu.omp.irap.cassis.database.access.vamdc.DefaultVamdcDataBaseConnection;
import eu.omp.irap.cassis.database.access.vamdc.MultiVamdcDatabaseConnection;
import eu.omp.irap.cassis.database.access.vamdc.XSAMSHeaderAndData;

public class VamdcDataBaseConnection implements DataBaseConnection {

	private static final String VAMDC_TRUNCATED = "VAMDC-TRUNCATED";
	public static final String VAMDC_REQUEST_TOKEN="VAMDC-REQUEST-TOKEN";
	private static final String HTTP_USER_AGENT = "HTTP_USER_AGENT";
	public static final String VAMDC_QUERY_UUID = "https://querystore.vamdc.eu/AssociationService?";
	public static final String VAMDC_QUERY_CITATION = "http://cite.vamdc.eu/references.html?";
	protected String url;
	protected List<SimpleMoleculeDescriptionDB> speciesDBs;
	protected static Logger logger = Logger.getLogger(VamdcDataBaseConnection.class.getName());
	private static String httpUserAgent = "CASSIS";
	protected String simpleSourceName="???";
	protected static final boolean DEBUG = false;
	protected static final short RITZ_INDICE = 0;
	protected static final int DEPTH = 50;
	protected static final int UNKNOW_MASS = -1;
	private VamdcDataBaseConnection dataBaseConnection = null;
	protected boolean local = false;
	static {
		logger.setLevel(Level.OFF);
	}

	protected HashMap<String, String> mapMolIdRequestable= new HashMap<String, String>();
	protected HashMap<String, String> mapAtomIdRequestable= new HashMap<String, String>();

	public static final String ALL_SPECIES_QUERY = "sync?REQUEST=DOQUERY&LANG=VSS2&FORMAT=XSAMS&QUERY=SELECT+SPECIES";
	private static String[] split;


	public VamdcDataBaseConnection() {
	}

	public VamdcDataBaseConnection(String url) {
		this.url = url;
		if (url.split(";").length > 1){
			dataBaseConnection = new MultiVamdcDatabaseConnection(url);
		}else if (url.equals("http://cdms.ph1.uni-koeln.de/cdms/tap/")) {
			simpleSourceName = "CDMS";
			dataBaseConnection = new CdmsJplVamdcDataBaseConnection(url);
		} else if (url.equals("http://cdms.ph1.uni-koeln.de/jpl/tap/")) {
			simpleSourceName = "JPL";
			dataBaseConnection = new CdmsJplVamdcDataBaseConnection(url);
		} else if (url.equals("http://pml.nist.gov:8000/nodes/asd/tap/")){
			simpleSourceName = "NIST";
			dataBaseConnection = new DefaultVamdcDataBaseConnection(url);

		} else if (url.equals("http://sesam.obspm.fr/12.07/vamdc/tap/")){
			simpleSourceName = "SESAM";
			dataBaseConnection = new DefaultVamdcDataBaseConnection(url);
		} else {
			simpleSourceName = "VAMDC";
			dataBaseConnection = new DefaultVamdcDataBaseConnection(url);
		}
	}

	public VamdcDataBaseConnection(String url, boolean local) {
		this(url);
		simpleSourceName = "VAMDC_FILE";
		dataBaseConnection.setLocal(local);
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * eu.omp.irap.cassis.database.DataBaseConnection#getMoleculeDescriptionDB
	 * (cassis.database.SimpleMoleculeDescriptionDB)
	 */
	@Override
	public MoleculeDescriptionDB getMoleculeDescriptionDB(
			SimpleMoleculeDescriptionDB simpleMolDB) throws UnknowMoleculeException {
		List<SimpleMoleculeDescriptionDB> allMoleculeDescriptionDB = getAllMoleculeDescriptionDB();
		for (SimpleMoleculeDescriptionDB moleculeDescriptionDB : allMoleculeDescriptionDB) {
			if (moleculeDescriptionDB.getTag() == simpleMolDB.getTag()) {
				return (MoleculeDescriptionDB) moleculeDescriptionDB;
			}
		}
		throw new UnknowMoleculeException(simpleMolDB.getTag());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see eu.omp.irap.cassis.database.DataBaseConnection#
	 * getAllMoleculeDescriptionDB ()
	 */
	@Override
	public List<SimpleMoleculeDescriptionDB> getAllMoleculeDescriptionDB() {
		return dataBaseConnection.getAllMoleculeDescriptionDB();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see eu.omp.irap.cassis.database.DataBaseConnection#getLineDescriptionDB(
	 * cassis .database.MoleculeDescriptionDB, double, double)
	 */
	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(SimpleMoleculeDescriptionDB mol,
			double freqMinComp, double freqMaxComp) {
		return dataBaseConnection.getLineDescriptionDB(mol, freqMinComp, freqMaxComp);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see eu.omp.irap.cassis.database.DataBaseConnection#getLineDescriptionDB(
	 * cassis .database.SimpleMoleculeDescriptionDB, double, double, double,
	 * double, double, double)
	 */
	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(SimpleMoleculeDescriptionDB simpleMolDB,
			double frequencyMin, double frequencyMax, double otherThresEupMin,
			double otherThresEupMax, double otherThresAijMin, double otherTreshAijMax) {
		return dataBaseConnection.getLineDescriptionDB(simpleMolDB, frequencyMin, frequencyMax,
				otherThresEupMin, otherThresEupMax, otherThresAijMin, otherTreshAijMax);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see eu.omp.irap.cassis.database.DataBaseConnection#getDataBaseOf(int)
	 */
	@Override
	public String getDataBaseOf(int molTag) {
		return dataBaseConnection.getDataBaseOf(molTag);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * eu.omp.irap.cassis.database.DataBaseConnection#getLineDescriptionDB(java
	 * .util.List, double, double, double, double, double, double)
	 */
	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(
			List<SimpleMoleculeDescriptionDB> listSimpleMolDB, double frequencyMin,
			double frequencyMax, double otherThresEupMin, double otherThresEupMax,
			double otherThresAijMin, double otherTreshAijMax) {

		return dataBaseConnection.getLineDescriptionDB(listSimpleMolDB, frequencyMin, frequencyMax,
				otherThresEupMin, otherThresEupMax, otherThresAijMin, otherTreshAijMax);
	}


	public static XSAMSHeaderAndData readXsamsURL(URL url) {
		IOSettings.httpConnectTimeout.setIntValue(600000);
		IOSettings.httpDataTimeout.setIntValue(600000);
		logger.warning("Url " + url);
		try {
			Map<String,String> urlParameters = new HashMap<String,String>();
			URLConnection conn = url.openConnection();
			// Allow gzip encoding
			if (IOSettings.compress.getIntValue() == 1)
				conn.setRequestProperty("Accept-Encoding", "gzip");
			// Set timeouts
			conn.setConnectTimeout(IOSettings.httpConnectTimeout.getIntValue());
			conn.setReadTimeout(IOSettings.httpDataTimeout.getIntValue());
			conn.addRequestProperty(HTTP_USER_AGENT, getHttpUserAgent());
			checkHttpResultCode(url, conn);

			Map<String, List<String>> map = conn.getHeaderFields();
			if (map != null && (map.get("Last-Modified") != null)){
				logger.warning("Last modified = " + map.get("Last-Modified").toString());
			}

			List<String> list = map.get("Access-Control-Expose-Headers");
			if (list != null){
				for (String string : list) {
					for (String string2 : string.split(",")) {
						String key = string2.trim();
						if (key.equals(VAMDC_TRUNCATED)) {
							urlParameters.put(key, map.get(key).toString());
						}else if (key.equals(VAMDC_REQUEST_TOKEN)){
							urlParameters.put(key, map.get(key).get(0));
						}
					}
				}
			}

			InputStream responseStream = conn.getInputStream();
			String contentEncoding = conn.getContentEncoding();
			if ("gzip".equalsIgnoreCase(contentEncoding)) {
				responseStream = new GZIPInputStream(responseStream);
			}
			XSAMSHeaderAndData readGetUrl1 = new XSAMSHeaderAndData(readStream(responseStream));
			readGetUrl1.setIdCitation(urlParameters.get(VAMDC_REQUEST_TOKEN));
			readGetUrl1.setTruncated(urlParameters.get(VAMDC_TRUNCATED)!=null);
			XSAMSHeaderAndData readGetUrl = readGetUrl1;

			return readGetUrl;
		} catch (JAXBException e) {
			e.printStackTrace();
		} catch (IOException e) {
			if (!(e.getMessage() != null && e.getMessage().contains("204"))) {
				e.printStackTrace();
			}
		}
		return null;
	}


	/**
	 *
	 * @param url
	 * @throws IOException
	 */
	public static Map<String,String> readHeadUrl(URL url) throws IOException {
		URLConnection conn = url.openConnection();
		Map<String,String> urlParameters = new HashMap<String,String>();
		conn.setConnectTimeout(IOSettings.httpConnectTimeout.getIntValue());
		conn.setReadTimeout(IOSettings.httpDataTimeout.getIntValue());

		conn.addRequestProperty(HTTP_USER_AGENT, getHttpUserAgent());
		if (conn instanceof HttpURLConnection){
			HttpURLConnection httpc = (HttpURLConnection) conn;
			httpc.setRequestMethod("HEAD");
			checkHttpResultCode(url, conn);

			Map<String, List<String>> map = conn.getHeaderFields();
			if (map != null && (map.get("Last-Modified") != null)){
				logger.warning("Last modified = " + map.get("Last-Modified").toString());
			}

			List<String> list = map.get("Access-Control-Expose-Headers");
			if (list != null){
				for (String string : list) {
					for (String string2 : string.split(",")) {
						String key = string2.trim();
						if (key.equals(VAMDC_TRUNCATED)) {
							urlParameters.put(key, map.get(key).toString());
						}else if (key.equals(VAMDC_REQUEST_TOKEN)){
							urlParameters.put(key, map.get(key).get(0));
						}
					}
				}
			}
		}
		return urlParameters;

	}
	public static String getHttpUserAgent() {
		return httpUserAgent;
	}
	public static void setHttpUserAgent(String agent) {
		httpUserAgent = agent;
	}

	public static XSAMSData readStream(InputStream source) throws JAXBException {
		if (source == null)
			throw new IllegalArgumentException("Input stream should not be null");
		Unmarshaller u = JAXBContextFactory.getUnmarshaller();
		return (XSAMSData) u.unmarshal(source);
	}

	private static void checkHttpResultCode(URL adress, URLConnection conn) throws IOException {
		if (adress.getProtocol().equals("http") || adress.getProtocol().equals("https")) {
			HttpURLConnection httpc = (HttpURLConnection) conn;
			if (httpc.getResponseCode() != 200)
				throw new IOException("Server responded with code " + httpc.getResponseCode());
		}
	}



	/**
	 * Convert freq MHz in Angstrom.
	 *
	 * @param freq
	 *            frequency in MHz.
	 * @return The converted frequency in Angstrom.
	 */
	protected static double convertFromMhzFreq(double freq) {
		if (freq == Double.MAX_VALUE || freq == Double.POSITIVE_INFINITY) {
			return 0.0;
		} else if (freq == Double.MIN_VALUE || freq == Double.NEGATIVE_INFINITY || freq == 0.0) {
			return Double.MAX_VALUE;
		}
		double v = Formula.C / freq / 1E-4;
		return (v > 0) ? v : 0.0;
	}

	/**
	 * @param readXsams
	 */
	protected void writeXsamsFile(XSAMSData readXsams, File file) {
		try {
			file.createNewFile();
			IOSettings.prettyprint.setIntValue(1);
			Output.writeFile(readXsams, file);
		} catch (IOException | JAXBException e) {
			e.printStackTrace();
		}
	}

	public static String integerToRoman(int n) {
		String roman = "";
		int repeat;
		int[] magnitude = { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };
		String[] symbol = { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" };
		repeat = n / 1;
		for (int x = 0; n > 0; x++) {
			repeat = n / magnitude[x];
			for (int i = 1; i <= repeat; i++) {
				roman = roman + symbol[x];
			}
			n = n % magnitude[x];
		}
		return roman;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see eu.omp.irap.cassis.database.DataBaseConnection#getMolName(int)
	 */
	@Override
	public String getMolName(int molTag) {
		String name;
		try {
			SimpleMoleculeDescriptionDB mol = getMoleculeDescriptionDB(
					new SimpleMoleculeDescriptionDB(molTag, null));
			name = mol.getName();
		} catch (UnknowMoleculeException ume) {
			name = NOT_IN_DATABASE;
		}
		return name;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see eu.omp.irap.cassis.database.DataBaseConnection#buildQt(double, int)
	 */
	@Override
	public double buildQt(double tex, int molTag) throws UnknowMoleculeException {
		return getMoleculeDescriptionDB(new SimpleMoleculeDescriptionDB(molTag, null)).buildQt(tex);
	}

	@Override
	public PartitionFunction getPartitionFunction(int molTag, boolean force) throws UnknowMoleculeException {
		return PartitionFunction.getPartitionFunction(getMoleculeDescriptionDB(new SimpleMoleculeDescriptionDB(molTag, null)));
	}


	@Override
	public String getId() {
		String id;
		if (url.equals("http://cdms.ph1.uni-koeln.de/cdms/tap/")) {
			id = "CDMS";
		} else if (url.equals("http://cdms.ph1.uni-koeln.de/jpl/tap/")) {
			id = "JPL";
		} else {
			id = url;
		}
		return id;
	}

	@Override
	public SimpleMoleculeDescriptionDB getSimpleMolecule(int id) {
		List<SimpleMoleculeDescriptionDB> allMoleculeDescriptionDB = getAllMoleculeDescriptionDB();
		for (SimpleMoleculeDescriptionDB simpleMoleculeDescriptionDB : allMoleculeDescriptionDB) {
			if (simpleMoleculeDescriptionDB.getTag() == id) {
				return simpleMoleculeDescriptionDB;
			}
		}
		return null;
	}

	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(SimpleMoleculeDescriptionDB mol) {
		return getLineDescriptionDB(mol, Double.MIN_VALUE, Double.MAX_VALUE);
	}


	protected String getMoleculeParameterRequestable() {
		return "MoleculeSpeciesID";
	}

	protected String getMoleculeValueRequestable(SimpleMoleculeDescriptionDB mol) {
		return mapMolIdRequestable.get(mol.getMoleculeID());
	}

	protected String getAtomParameterRequestable() {
		return "InChIKey";
	}

	protected String getAtomValueRequestable(SimpleMoleculeDescriptionDB mol) {
		return mapAtomIdRequestable.get(mol.getMoleculeID());
	}

	public void setLocal(boolean local) {
		this.local = local;

	}

	public static URL getReferences(final String token, String emailUser, String client) {
		URL urlRes = null;
		try {

			URL url = new URL(VamdcDataBaseConnection.VAMDC_QUERY_UUID +
					"queryToken=" + token + "&" +
					"email=" + emailUser + "&" +
					"usedClient=" + client);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			logger.info("Sending get request : " + url);
			con.setRequestMethod("GET");
			// add request header : con.setRequestProperty("User-Agent",
			// USER_AGENT);

			int responseCode = con.getResponseCode();
			logger.info("Response code : " + responseCode);

			// Reading response from input Stream
			BufferedReader in = new BufferedReader(
					new InputStreamReader(con.getInputStream()));
			String output;
			StringBuffer response = new StringBuffer();

			while ((output = in.readLine()) != null) {
				response.append(output);
			}
			String uuid = response.toString();
			String[] splitUuid = uuid.split(",");
			if (splitUuid.length > 1) {
				for (int i = 0; i < splitUuid.length; i++) {
					String string = splitUuid[i];
					string = string.replaceAll("\"", "");
					string = string.replaceAll("\"", "");
					string = string.replaceAll("\\{", "");
					string = string.replaceAll("\\}", "");
					if (string.startsWith("UUIDCorrectlyAssociated")) {
						split = string.split(":");
						if (split.length >1) {
							uuid = split[1];
						}
					}
				}
			}
			String spec = VamdcDataBaseConnection.VAMDC_QUERY_CITATION + "uuid="+uuid;
			urlRes = new URL(spec);

		} catch (IOException e2) {
			e2.printStackTrace();
		}
		return urlRes;
	}
}
