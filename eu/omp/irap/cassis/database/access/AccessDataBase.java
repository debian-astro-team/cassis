/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.access;

import eu.omp.irap.cassis.database.FileDatabaseUtils;

/**
 * Class to gain access to a database from the settings in InfoDataBase.
 */
public class AccessDataBase  {

	private static DataBaseConnection dataBaseConnection;


	/**
	 * Create if needed then return the {@link DataBaseConnection}.
	 *
	 * @return the {@link DataBaseConnection}
	 */
	public static DataBaseConnection getDataBaseConnection() {
		if (dataBaseConnection == null) {
			TypeDataBase typeDb = InfoDataBase.getInstance().getTypeDb();
			final String dbname = InfoDataBase.getInstance().getDbname();
			boolean inMemoryDb = InfoDataBase.getInstance().isInMemoryDatabase();
			dataBaseConnection = createDataBaseConnection(typeDb, dbname, inMemoryDb);
		}
		return dataBaseConnection;
	}

	/**
	 * Create then return z {@link DataBaseConnection} with the given parameters.
	 * This one is not set as used database but <b>only</b> created.
	 *
	 * @param typeDb The type of database.
	 * @param dbName The name/path/URL of the database.
	 * @param inMemoryDb True to set it as a in memory database (only work for SQLite),
	 *  false otherwise.
	 * @return The created {@link DataBaseConnection}.
	 */
	public static DataBaseConnection createDataBaseConnection(TypeDataBase typeDb, String dbName, boolean inMemoryDb) {
		DataBaseConnection dbConnection;
		if (typeDb == TypeDataBase.FILE) {
			dbConnection = FileDatabaseUtils.getConnection(dbName);
		} else if (typeDb == TypeDataBase.SQLITE) {
			dbConnection = new SqliteDataBaseConnection(dbName, inMemoryDb);
		} else if (typeDb == TypeDataBase.VAMDC) {
			dbConnection = new VamdcDataBaseConnection(dbName);
		} else if (typeDb == TypeDataBase.VAMDC_FILE) {
			dbConnection = new VamdcFileDataBaseConnection(dbName);
		} else if (typeDb == TypeDataBase.SLAP) {
			dbConnection = new SlapDataBaseConnection(dbName);
		} else if (typeDb == TypeDataBase.SLAP_FILE) {
			dbConnection = new SlapFileDataBaseConnection(dbName);
		} else {
			dbConnection = new NoDataBaseConnection();
		}
		return dbConnection;
	}

	/**
	 * Close the current database if needed, then reset it to <code>null</code>.
	 */
	public static void initConnection() {
		if (dataBaseConnection instanceof ClosableDataBaseConnection) {
			((ClosableDataBaseConnection) dataBaseConnection).close();
		}
		dataBaseConnection = null;
	}

	/**
	 * Close the current database if needed, then init the {@link DataBaseConnection}
	 *  to the provided one.
	 *
	 * @param baseConnection The database connection to use.
	 */
	public static void initConnection(DataBaseConnection baseConnection) {
		if (dataBaseConnection instanceof ClosableDataBaseConnection) {
			((ClosableDataBaseConnection) dataBaseConnection).close();
		}
		dataBaseConnection = baseConnection;
	}

}
