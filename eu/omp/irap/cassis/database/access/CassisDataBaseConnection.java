/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.access;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CassisDataBaseConnection extends FileDataBaseConnection {

	private static final Logger LOGGER = LoggerFactory.getLogger(CassisDataBaseConnection.class);

	public CassisDataBaseConnection(String folder) {
		super(folder);
	}


	@Override
	public List<SimpleMoleculeDescriptionDB> getAllMoleculeDescriptionDB() {
		if (speciesDescriptionDB == null) {
			speciesDescriptionDB = new ArrayList<SimpleMoleculeDescriptionDB>();

			List<String> partitionFile;
			try {
				partitionFile = getParitionFile(new URI(path).toURL().getPath());
			} catch (MalformedURLException e) {
				partitionFile = null;
				e.printStackTrace();
			} catch (URISyntaxException e) {
				partitionFile = null;
				e.printStackTrace();
			}
			String line = null;
			if (partitionFile != null) {
				for (String file : partitionFile) {
					int tag = Integer.valueOf(new File(file).getName().replace("c", "").replace(".txt", ""));
					try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
						line = reader.readLine();//example : DNC_HFS, ID= 28608
						String moleculeName = line.split(",")[0].trim();
						line = reader.readLine();//TEMPERATURE - Q(SPIN-ROT.) - log Q(SPIN-ROT.)
//						String[] columns = line.split("-");
						Map<Double, Double> map = new TreeMap<Double, Double> ();

						do {
							line = reader.readLine();//300.000      1478.4340    3.1698
							if (line != null) {
								String[] tempVal = line.trim().split("\\s+");
								if (tempVal.length == 3) {
									map.put(Double.valueOf(tempVal[0]), Double.valueOf(tempVal[2]));
								}
							}

						} while (line != null);

						double[] zptArray = new double[map.size()];
						double[] zlpArray = new double[map.size()];

						int cpt = 0;
						for (Entry<Double,Double> entry : map.entrySet()) {
							zptArray[cpt] = entry.getKey();
							zlpArray[cpt] = entry.getValue();
							cpt++;
						}

						final MoleculeDescriptionDB mol = new MoleculeDescriptionDB(
								tag, moleculeName, zptArray, zlpArray);
						speciesDescriptionDB.add(mol);
						mol.setQlogAienstein300(map.get(300.0));


					}  catch (Exception ioe) {
						LOGGER.error("Error while reading database file " + file, ioe);
					}
				}
			}
		}
		return Collections.unmodifiableList(speciesDescriptionDB);
	}


	public static  List<String> getParitionFile(String path2) {
		List<String> result = null;
		try (Stream<Path> walk = Files.walk(Paths.get(path2))) {

			result = walk.map(x -> x.toString())
					.filter(f -> f.endsWith(".txt")).collect(Collectors.toList());
//			result.forEach(System.out::println);

		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}


	@Override
	public double buildQt(double tex, int molTag) throws UnknowMoleculeException {

		MoleculeDescriptionDB moleculeDescriptionDB = getMoleculeDescriptionDB(new SimpleMoleculeDescriptionDB(molTag, ""));
		return moleculeDescriptionDB.buildQt(tex);
	}
}
