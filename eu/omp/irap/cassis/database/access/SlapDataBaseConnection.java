/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.access;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import eu.omp.irap.cassis.common.Formula;
import eu.omp.irap.cassis.database.access.slap.DefaultSlapDataBaseConnection;
import eu.omp.irap.cassis.database.access.slap.NistSlapDataBaseConnection;
import eu.omp.irap.cassis.database.access.slap.SplataSlapDataBaseConnection;
import eu.omp.irap.cassis.database.access.slap.SlapDatabaseConnectionV2;
import eu.omp.irap.vespa.votable.utils.CantSendQueryException;
import eu.omp.irap.vespa.votable.utils.Network;

/**
 * SLAP database implementation for CASSIS.
 * Note that SLAP database lack a lot of needed informations for CASSIS.
 */
public class SlapDataBaseConnection implements DataBaseConnection {

	// http://esavo02.esac.esa.es:8080/slap/jsp/slap.jsp?REQUEST=queryData&WAVELENGTH=5.1E-6/5.6E-6
	// &FORMAT=METADATA
	//
	// IASD http://esavo.esac.esa.int/slap/jsp/slapBeta.jsp?
	// VUV-LERMA-LUTH http://linelists.obspm.fr/transitions.php?base=molat&
	// NIST ATOMIC SPECTRA http://physics.nist.gov/cgi-bin/ASD/slap.pl?
	// CIELO SLAP http://esavo02:8080/cieloslapToolKit/cieloslap.jsp? KO
	// CHIANTI SLAP http://msslxv.mssl.ucl.ac.uk:8080/chianti_slap/DALToolKitServlet? KO
	// STSCI SLAP http://archdev.stsci.edu/slap/search.php?
	// LERMA http://linelists.obspm.fr/transitions.php?base=cdms_jpl_basecol&
	// https://find.nrao.edu/splata-slap/slap?REQUEST=queryData&WAVELENGTH=0.00260075/0.00260080&VERB=3


	protected String url;

	protected Map<Integer, String> map;
	private int currentTag;
	protected boolean local=false;
	private SlapDataBaseConnection dataBaseConnection;


	public SlapDataBaseConnection() {
	}

	/**
	 * Create a new connection to a SLAP database.
	 *
	 * @param url The URL of the database.
	 */
	public SlapDataBaseConnection(String url) {
		this.url = url;
		this.map = new HashMap<Integer, String>();

		if (url.equals("http://physics.nist.gov/cgi-bin/ASD/slap.pl?")) {
			dataBaseConnection = new NistSlapDataBaseConnection(url);
		} else if (url.equals("https://find.nrao.edu/splata-slap/slap?")||
				(url.contains("splatalog"))){
			dataBaseConnection = new SplataSlapDataBaseConnection(url);
		}  else if (url.contains("vamdc/slap")){
			dataBaseConnection = new SlapDatabaseConnectionV2(url);
		}
		else {
			dataBaseConnection = new DefaultSlapDataBaseConnection(url);
		}
//		dataBaseConnection = new NistSlapDataBaseConnection(url);
	}

	public SlapDataBaseConnection(String url, boolean local) {
		this(url);
		setLocal(local);
		dataBaseConnection.setLocal(local);

	}

	private void setLocal(boolean local) {
		this.local = local;

	}

	/**
	 * Return the {@link MoleculeDescriptionDB} without temp and qlog for the
	 *  given {@link SimpleMoleculeDescriptionDB},
	 *
	 * @param simpleMolDB The {@link SimpleMoleculeDescriptionDB}.
	 * @return the {@link MoleculeDescriptionDB} without temp and qlog.
	 * @throws UnknowMoleculeException If the molecule is not found in the database.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getMoleculeDescriptionDB(eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB)
	 */
	@Override
	public MoleculeDescriptionDB getMoleculeDescriptionDB(
			SimpleMoleculeDescriptionDB simpleMolDB)
			throws UnknowMoleculeException {
		return new MoleculeDescriptionDB(simpleMolDB.getTag(),
				simpleMolDB.getName(), new double[] {}, new double[] {});
	}

	/**
	 * We lack this information on SLAP online database, return a List with only
	 *  a fake molecule.
	 *
	 * @return return a List with only a fake molecule.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getAllMoleculeDescriptionDB()
	 */
	@Override
	public List<SimpleMoleculeDescriptionDB> getAllMoleculeDescriptionDB() {
		return dataBaseConnection.getAllMoleculeDescriptionDB();
	}

	/**
	 * Return the molecule name of the given molecule tag.
	 *
	 * @param molTag The molecule tag.
	 * @return the molecule name or {@link DataBaseConnection#NOT_IN_DATABASE}
	 *  if the molecule is not in database.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getMolName(int)
	 */
	@Override
	public String getMolName(int molTag) {
		return dataBaseConnection.getMolName(molTag);
	}

	/**
	 * Return "SLAP"
	 *
	 * @param molTag Not used here.
	 * @return "SLAP"
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getDataBaseOf(int)
	 */
	@Override
	public String getDataBaseOf(int molTag) {
		return "SLAP";
	}

	/**
	 * Return 0 as we lack the information needed for this.
	 *
	 * @param tex Not used here.
	 * @param molTag Not used here.
	 * @return 0.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#buildQt(double, int)
	 */
	@Override
	public double buildQt(double tex, int molTag)
			throws UnknowMoleculeException {
		return 0;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see eu.omp.irap.cassis.database.getPartitionFunction#buildQt(int)
	 */
	@Override
	public PartitionFunction getPartitionFunction(int molTag, boolean force) throws UnknowMoleculeException {
		return PartitionFunction.getPartitionFunction(getMoleculeDescriptionDB(getSimpleMolecule(molTag)));
	}
	/**
	 * Return all {@link LineDescriptionDB} between freqMinComp and freqMaxComp.
	 *
	 * @param molecule Not used here.
	 * @param freqMinComp The minimum frequency (in MHz).
	 * @param freqMaxComp The maximum frequency (in MHz).
	 * @return all {@link LineDescriptionDB} between freqMinComp and freqMaxComp.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getLineDescriptionDB(eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB, double, double)
	 */
	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(
			SimpleMoleculeDescriptionDB molecule, double freqMinComp,
			double freqMaxComp) {
		return dataBaseConnection.getLineDescriptionDB(molecule, freqMinComp, freqMaxComp);


	}

	protected String getResultFile(String request) throws CantSendQueryException {
		return Network.saveQuery(request, "slap", ".votable");

	}

	/**
	 * Return all {@link LineDescriptionDB} between frequencyMin and frequencyMax.
	 *
	 * @param simpleMolDB Not used here.
	 * @param frequencyMin The minimum frequency (in MHz).
	 * @param frequencyMax The maximum frequency (in MHz).
	 * @param otherThresEupMin Not used here.
	 * @param otherThresEupMax Not used here.
	 * @param otherThresAijMin Not used here.
	 * @param otherTreshAijMax Not used here.
	 * @return all {@link LineDescriptionDB} between frequencyMin and frequencyMax.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getLineDescriptionDB(eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB, double, double, double, double, double, double)
	 */
	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(
			SimpleMoleculeDescriptionDB simpleMolDB, double frequencyMin,
			double frequencyMax, double otherThresEupMin,
			double otherThresEupMax, double otherThresAijMin,
			double otherTreshAijMax) {
		return dataBaseConnection.getLineDescriptionDB(simpleMolDB, frequencyMin, frequencyMax);
	}

	/**
	 * Return all {@link LineDescriptionDB} between frequencyMin and frequencyMax.
	 *
	 * @param listSimpleMolDB Not used here.
	 * @param frequencyMin The minimum frequency (in MHz).
	 * @param frequencyMax The maximum frequency (in MHz).
	 * @param otherThresEupMin Not used here.
	 * @param otherThresEupMax Not used here.
	 * @param otherThresAijMin Not used here.
	 * @param otherTreshAijMax Not used here.
	 * @return all {@link LineDescriptionDB} between frequencyMin and frequencyMax.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getLineDescriptionDB(java.util.List, double, double, double, double, double, double)
	 */
	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(
			List<SimpleMoleculeDescriptionDB> listSimpleMolDB,
			double frequencyMin, double frequencyMax, double otherThresEupMin,
			double otherThresEupMax, double otherThresAijMin,
			double otherTreshAijMax) {

		return dataBaseConnection.getLineDescriptionDB(listSimpleMolDB, frequencyMin, frequencyMax);
	}

	protected List<LineDescriptionDB> getLineDescriptionDB(List<SimpleMoleculeDescriptionDB> listSimpleMolDB,
			double frequencyMin, double frequencyMax) {
		return dataBaseConnection.getLineDescriptionDB(listSimpleMolDB, frequencyMin, frequencyMax);
	}

	/**
	 * Return the url of the database.
	 *
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getId()
	 */
	@Override
	public String getId() {
		return url;
	}

	/**
	 * Create the query.
	 *
	 * @param url The URL of the database.
	 * @param freqMin The minimum frequency.
	 * @param freqMax The maximum frequency.
	 * @return the query.
	 */
	protected static String createLinesQuery(String url, double freqMin, double freqMax) {
		String formatedUrl = url;


		if (!url.contains("?")) {
			formatedUrl += "?";
		} else if (url.charAt(url.length() - 1) != '?'
				&& url.charAt(url.length() - 1) != '&') {
			formatedUrl += "&";
		}

		double maxWavelength = convertMhzToMeter(freqMin);
		double minWavelength = convertMhzToMeter(freqMax);

		String tmpString = String.valueOf(minWavelength) + '/'
				+ String.valueOf(maxWavelength);

		return formatedUrl + "REQUEST=queryData&WAVELENGTH=" + tmpString;
	}

	/**
	 * Convert MHz to meter.
	 *
	 * @param val The value in MHz.
	 * @return The converted value in meter.
	 */
	public static double convertMhzToMeter(double val) {
		return Formula.C / val / 1E6;
	}

	/**
	 * Convert meter to MHz.
	 *
	 * @param val The value in meter.
	 * @return The converted value in MHz.
	 */
	public static double convertMeterToMhz(double val) {
		return Formula.C / val / 1E6;
	}



	/**
	 * Add molecule as known in an internal database from this database if needed.
	 *
	 * @param name The name of the molecule.
	 * @param otherName The name of the molecule.
	 * @return The tag of the molecule.
	 */
	protected int addMolRefIfNeeded(String name , String otherName) {
		if (!map.containsValue(otherName) && !map.containsValue(name)) {
			int tag = currentTag--;
			map.put(tag, otherName);
			return tag;
		} else {
			for (Entry<Integer, String> me : map.entrySet()) {
				if (me.getValue().equals(name)) {
					return me.getKey();
				}
			}
		}
		return 0; // Never be executed.
	}

	/**
	 * Get the SimpleMoleculeDescriptionDB corresponding to the given id.
	 *
	 * @param id The molecule tag.
	 * @return The {@link SimpleMoleculeDescriptionDB} corresponding molecule tag,
	 *  or null if not found.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getSimpleMolecule(int)
	 */
	@Override
	public SimpleMoleculeDescriptionDB getSimpleMolecule(int id) {
		return dataBaseConnection.getSimpleMolecule(id);
	}

	/**
	 * Return all {@link LineDescriptionDB} in this database.
	 *
	 * @param mol Not used here.
	 * @return all {@link LineDescriptionDB} in this database.
	 * @see eu.omp.irap.cassis.database.access.DataBaseConnection#getLineDescriptionDB(eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB)
	 */
	@Override
	public List<LineDescriptionDB> getLineDescriptionDB(SimpleMoleculeDescriptionDB mol) {
		return dataBaseConnection.getLineDescriptionDB(mol, Double.MIN_VALUE, Double.MAX_VALUE);
	}
}
