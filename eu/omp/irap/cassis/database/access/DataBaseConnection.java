/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.access;

import java.util.List;

/**
 * Interface that each database type need to implement for being able to be used in CASSIS.
 */
public interface DataBaseConnection {

	String NOT_IN_DATABASE = "Not in Database";

	/**
	 * Return the {@link MoleculeDescriptionDB} for the given {@link SimpleMoleculeDescriptionDB}.
	 *
	 * @param simpleMolDB The {@link SimpleMoleculeDescriptionDB}.
	 * @return the {@link MoleculeDescriptionDB} corresponding to the given
	 *  {@link SimpleMoleculeDescriptionDB}.
	 * @throws UnknowMoleculeException If the molecule is not found in the database.
	 */
	MoleculeDescriptionDB getMoleculeDescriptionDB(SimpleMoleculeDescriptionDB simpleMolDB) throws UnknowMoleculeException;

	/**
	 * Return the list of all {@link SimpleMoleculeDescriptionDB} who are in the database.
	 *
	 * @return the list of all {@link SimpleMoleculeDescriptionDB} who are in the database.
	 */
	List<SimpleMoleculeDescriptionDB> getAllMoleculeDescriptionDB();

	/**
	 * Return the molecule name of the given molecule tag.
	 *
	 * @param molTag The molecule tag.
	 * @return the molecule name or {@link DataBaseConnection#NOT_IN_DATABASE}
	 *  if the molecule is not in database.
	 */
	String getMolName(int molTag);

	/**
	 * Return the database of the given molecule tag.
	 *
	 * @param molTag The molecule tag.
	 * @return the database of the given molecule tag.
	 */
	String getDataBaseOf(int molTag);

	/**
	 * Computation of the partition function.
	 *
	 * @param tex The temperature.
	 * @param molTag The molecule tag.
	 * @return the partition function.
	 * @throws UnknowMoleculeException If the molecule is not found in the database.
	 */
	double buildQt(double tex, int molTag) throws UnknowMoleculeException;

	/**
	 * Get the PartitionFunction for a molecule identified by the molTag
	 * @param molTag
	 * @param force TODO
	 * @return a PartitionFunction with the range and the orgigin of the partition function
	 * @throws UnknowMoleculeException
	 *
	 */
	PartitionFunction getPartitionFunction(int molTag, boolean force) throws UnknowMoleculeException;

	/**
	 * Return a list of all {@link LineDescriptionDB} of the the given
	 *  {@link SimpleMoleculeDescriptionDB} between freqMinComp and freqMaxComp.
	 *
	 * @param molecule The {@link SimpleMoleculeDescriptionDB}.
	 * @param freqMinComp The minimum frequency (in MHz).
	 * @param freqMaxComp The maximum frequency (in MHz).
	 * @return a list of all {@link LineDescriptionDB} for the given parameters.
	 */
	List<LineDescriptionDB> getLineDescriptionDB(
			SimpleMoleculeDescriptionDB molecule, double freqMinComp,
			double freqMaxComp);

	/**
	 * Return a list of all {@link LineDescriptionDB} of the the given
	 *  {@link SimpleMoleculeDescriptionDB} with the given parameters.
	 *
	 * @param simpleMolDB The {@link SimpleMoleculeDescriptionDB}.
	 * @param frequencyMin The minimum frequency (in MHz).
	 * @param frequencyMax The maximum frequency (in MHz).
	 * @param otherThresEupMin The minimum eup value.
	 * @param otherThresEupMax The maximum eup value.
	 * @param otherThresAijMin The minimum aij value.
	 * @param otherTreshAijMax The maximum aij value.
	 * @return a list of all {@link LineDescriptionDB} for the given parameters.
	 */
	List<LineDescriptionDB> getLineDescriptionDB(
			SimpleMoleculeDescriptionDB simpleMolDB, double frequencyMin,
			double frequencyMax, double otherThresEupMin,
			double otherThresEupMax, double otherThresAijMin,
			double otherTreshAijMax);

	/**
	 * Return a list of all {@link LineDescriptionDB} of the the given list of
	 *  {@link SimpleMoleculeDescriptionDB} with the given parameters.
	 *
	 * @param listSimpleMolDB The list of {@link SimpleMoleculeDescriptionDB}.
	 * @param frequencyMin The minimum frequency (in MHz).
	 * @param frequencyMax The maximum frequency (in MHz).
	 * @param otherThresEupMin The minimum eup value.
	 * @param otherThresEupMax The maximum eup value.
	 * @param otherThresAijMin The minimum aij value.
	 * @param otherTreshAijMax The maximum aij value.
	 * @return a list of all {@link LineDescriptionDB} for the given parameters.
	 */
	List<LineDescriptionDB> getLineDescriptionDB(
			List<SimpleMoleculeDescriptionDB> listSimpleMolDB,
			double frequencyMin, double frequencyMax,
			double otherThresEupMin, double otherThresEupMax,
			double otherThresAijMin, double otherTreshAijMax);

	/**
	 * Return the identification of the database (usually, the URL or file name).
	 *
	 * @return the identification of the database.
	 */
	String getId();

	/**
	 * Get the SimpleMoleculeDescriptionDB corresponding to the given id.
	 *
	 * @param id The molecule tag.
	 * @return The {@link SimpleMoleculeDescriptionDB} corresponding molecule tag,
	 *  or null if not found.
	 */
	SimpleMoleculeDescriptionDB getSimpleMolecule(int id);

	/**
	 * Return a list of all {@link LineDescriptionDB} for the given
	 *  {@link SimpleMoleculeDescriptionDB}.
	 *
	 * @param mol The {@link SimpleMoleculeDescriptionDB}.
	 * @return the list of all {@link LineDescriptionDB} for the given
	 *  {@link SimpleMoleculeDescriptionDB}.
	 */
	List<LineDescriptionDB> getLineDescriptionDB(SimpleMoleculeDescriptionDB mol);


}
