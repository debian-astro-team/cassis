/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.access;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Logger;

public class PartitionFunction {
	private double minValue;
	private double maxValue;
	private String origin;

	private static final Logger logger = Logger
			.getLogger(PartitionFunction.class.getName());

	public PartitionFunction(double minValue, double maxValue, String origin) {
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.origin = origin;
	}

	public double getMinValue() {
		return minValue;
	}

	public double getMaxValue() {
		return maxValue;
	}

	public String getOrigin() {
		return origin;
	}

	public boolean isInRange(double tex) {
		return tex >= minValue && tex <= maxValue;
	}

	public static PartitionFunction getPartitionFunction(MoleculeDescriptionDB moleculeDescriptionDB) {
		double[] texTab = moleculeDescriptionDB.getTemp();

		String origin = "database";
		SqlPartitionMole.setFirstReaded(true);
		// Check moleculaire in the list file
		if (SqlPartitionMole.inSqlPartitionMoleList(moleculeDescriptionDB.getTag())) {
			origin = SqlPartitionMole.getPartMolePath();
			SqlPartitionMole sqlPartitionMole = new SqlPartitionMole();
			sqlPartitionMole.loadSqlPartitionMoleFile(moleculeDescriptionDB.getTag());
			texTab = sqlPartitionMole.getZptArray();
		}
		double min = Double.MAX_VALUE;
		double max = Double.MIN_VALUE;
		if (texTab != null && texTab.length>0) {
			min = texTab[texTab.length-1];
			max =  texTab[0];
		}
		return new PartitionFunction(min, max, origin);
	}

	public void createFile(MoleculeDescriptionDB mol) {
		String file = SqlPartitionMole.getPartMolePath() + "/" + mol.getTag() + ".txt";
		try (BufferedWriter out = new BufferedWriter(new FileWriter(file))) {
			out.write("//" + mol.getSource()+ "\t" + mol.getName()+"\n"+
					  "//Temp(K)\tlog(Z)\n");
			SqlPartitionMole partitionMole = new SqlPartitionMole();
			partitionMole.loadSqlPartitionMoleFile(mol.getTag());
			double val = 0;

			double[] temp = mol.getTemp();
			double[] qlog = mol.getQlog();
			int len = temp.length;
			for (int i = 0; i < len; i++) {
				val = temp[len-i-1];
				out.write(Double.toString(val) + '\t');
				val = qlog[len-i-1];
				out.write(Double.toString(val));
				if (i + 1 != len) {
					out.write(System.lineSeparator());
				}
			}
		} catch (IOException e) {
			logger.warning(e.getMessage());
		}
	}
}
