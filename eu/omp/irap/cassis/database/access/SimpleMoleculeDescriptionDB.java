/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.access;

public class SimpleMoleculeDescriptionDB {


	public enum TypeSpecies {
		MOLECULE, ATOM, PARTICLE;

	}
	protected int tag;
	private String name = "test";
	private String moleculeID;
	private double molecularMass;
	private String source = "unknown";
	private TypeSpecies type = TypeSpecies.MOLECULE;


	public SimpleMoleculeDescriptionDB(int tag, String name) {
		this(tag, name, "");
	}

	public SimpleMoleculeDescriptionDB(int tag, String name, String moleculeID) {
		this.tag = tag;
		this.name = name;
		this.moleculeID = moleculeID;
		this.molecularMass = tag / 1000;
	}

	/**
	 * @return the tag
	 */
	public int getTag() {
		return tag;
	}

	/**
	 * @return the name
	 */
	public final String getName() {
		return name;
	}

	public String getMoleculeID() {
		return moleculeID;
	}

	public void setMoleculeID(String moleculeID) {
		this.moleculeID = moleculeID;
	}

	public double getMolecularMass() {
		return molecularMass;
	}

	public void setMolecularMass(double molecularMass) {
		this.molecularMass = molecularMass;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSource() {
		return source;
	}

	public void setTag(int tag) {
		this.tag = tag;
	}


	/**
	 * @return the type
	 */
	public TypeSpecies getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(TypeSpecies type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(molecularMass);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((moleculeID == null) ? 0 : moleculeID.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return prime * result + tag;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SimpleMoleculeDescriptionDB other = (SimpleMoleculeDescriptionDB) obj;
		if (Double.doubleToLongBits(molecularMass) != Double.doubleToLongBits(other.molecularMass)) {
			return false;
		}
		if (moleculeID == null) {
			if (other.moleculeID != null) {
				return false;
			}
		} else if (!moleculeID.equals(other.moleculeID)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (source == null) {
			if (other.source != null) {
				return false;
			}
		} else if (!source.equals(other.source)) {
			return false;
		}
		if (type == null) {
			if (other.type != null) {
				return false;
			}
		} else if (!type.equals(other.type)) {
			return false;
		}
		if (tag != other.tag) {
			return false;
		}

		return true;
	}

	public static TypeSpecies getTypeSpecies(String val) {
		TypeSpecies typeSpecies = TypeSpecies.MOLECULE;
		if (val.equalsIgnoreCase("atom")) {
			typeSpecies = TypeSpecies.ATOM;
		} else if(val.equalsIgnoreCase("particle")) {
			typeSpecies = TypeSpecies.PARTICLE;
		}
		return typeSpecies;
	}

	@Override
	public String toString() {
		return "SimpleMoleculeDescriptionDB [tag=" + tag + ", name=" + name + ", moleculeID="
				+ moleculeID + ", molecularMass=" + molecularMass + ", source=" + source
				+ ", type=" + type +  "]";
	}
}
