/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.access;

import java.util.List;

/**
 * This class describes a Line.
 */
public class LineDescriptionDB {

	private double frequency;
	private String quanticNumbers;
	private double error;
	private double aint;
	private double elow;
	private int igu;
	private double gammaself;
	private int tag;
	private double eup;
	private String citation = "";
	private List<String> otherFreqs;


	/**
	 * Constructs a new LineDescriptionDB with the given parameters.
	 *
	 * @param tag The tag.
	 * @param frequency The frequency in MHz.
	 * @param quanticNumbers The quantics numbers.
	 * @param error The error
	 * @param aint The aint.
	 * @param elow The elow
	 * @param igu The igu.
	 * @param gammaself The gamma self.
	 * @param eup The eup.
	 */
	public LineDescriptionDB(int tag, double frequency, String quanticNumbers,
			double error, double aint, double elow, int igu, double gammaself,
			double eup) {
		super();
		this.tag = tag;
		this.frequency = frequency;
		this.quanticNumbers = quanticNumbers;
		this.error = error;
		this.aint = aint;
		this.elow = elow;
		this.igu = igu;
		this.gammaself = gammaself;
		this.eup = eup;
	}

	/**
	 * Return the frequency in MHz.
	 *
	 * @return the frequency the frequency in MHz.
	 */
	public final double getFrequency() {
		return frequency;
	}

	/**
	 * Return the quantics numbers.
	 *
	 * @return the quantics numbers.
	 */
	public final String getQuanticNumbers() {
		return quanticNumbers;
	}

	/**
	 * Return the error.
	 *
	 * @return the error.
	 */
	public final double getError() {
		return error;
	}

	/**
	 * Einstein coefficient [/s]
	 *
	 * @return the aint.
	 */
	public final double getAint() {
		return aint;
	}

	/**
	 * Return the elow.
	 *
	 * @return the elow.
	 */
	public double getElow() {
		return elow;
	}

	/**
	 * Set the elow.
	 *
	 * @param elow The new elow value.
	 */
	public final void setElow(double elow) {
		this.elow = elow;
	}

	/**
	 * Return the igu.
	 *
	 * @return the igu.
	 */
	public final int getIgu() {
		return igu;
	}

	/**
	 * Return the gamma self.
	 *
	 * @return the gamma self.
	 */
	public double getGammaSelf() {
		return gammaself;
	}

	/**
	 * Return the eup.
	 *
	 * @return the eup.
	 */
	public double getEup() {
		return eup;
	}

	/**
	 * Set the eup.
	 *
	 * @param eup The new eup value.
	 */
	public void setEup(double eup) {
		this.eup = eup;
	}

	/**
	 * Return the tag.
	 *
	 * @return the tag.
	 */
	public int getTag() {
		return tag;
	}

	/**
	 * Set the tag.
	 *
	 * @param tag The new tag value.
	 */
	public void setTag(int tag) {
		this.tag = tag;
	}

	/**
	 * @return the citation
	 */
	public String getCitation() {
		return citation;
	}

	/**
	 * @param citation the citation to set
	 */
	public void setCitation(String citation) {
		this.citation = citation;
	}

	/**
	 * Create a hashcode.
	 *
	 * @return the hashcode.
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(aint);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(elow);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(error);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(eup);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(frequency);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(gammaself);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + igu;
		result = prime * result + ((quanticNumbers == null) ? 0 : quanticNumbers.hashCode());
		return prime * result + tag;
	}

	/**
	 * Test the equality with another object.
	 *
	 * @return if the objects are equals.
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		LineDescriptionDB other = (LineDescriptionDB) obj;
		if (Double.doubleToLongBits(aint) != Double.doubleToLongBits(other.aint)) {
			return false;
		}
		if (Double.doubleToLongBits(elow) != Double.doubleToLongBits(other.elow)) {
			return false;
		}
		if (Double.doubleToLongBits(error) != Double.doubleToLongBits(other.error)) {
			return false;
		}
		if (Double.doubleToLongBits(eup) != Double.doubleToLongBits(other.eup)) {
			return false;
		}
		if (Double.doubleToLongBits(frequency) != Double.doubleToLongBits(other.frequency)) {
			return false;
		}
//		if (Double.doubleToLongBits(gammaself) != Double.doubleToLongBits(other.gammaself)) {
//			return false;
//		}
		if (igu != other.igu) {
			return false;
		}
		if (quanticNumbers == null) {
			if (other.quanticNumbers != null) {
				return false;
			}
		} else if (!quanticNumbers.equals(other.quanticNumbers)) {
			return false;
		}
		if (tag != other.tag) {
			return false;
		}
		return true;
	}

	/**
	 * Create and return a String representation of the object.
	 *
	 * @return the String representation of the object.
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LineDescriptionDB [frequency=" + frequency + ", quanticNumbers=" + quanticNumbers
				+ ", error=" + error + ", aint=" + aint + ", elow=" + elow + ", igu=" + igu
				+ ", gammaself=" + gammaself + ", tag=" + tag + ", eup=" + eup + "]";
	}

	public void setOtherFreqs(List<String> otherFreqs) {
		this.otherFreqs = otherFreqs;

	}


	public List<String> getOtherFreqs() {
		return otherFreqs;
	}

}
