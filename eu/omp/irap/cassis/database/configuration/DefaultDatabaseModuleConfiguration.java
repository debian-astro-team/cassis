/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * Simple implementation of the {@link InterfaceDatabaseModuleConfiguration}.
 * Keep the configuration in a map, destroyed once the application stop.
 *
 * @author M. Boiziot
 */
public class DefaultDatabaseModuleConfiguration implements InterfaceDatabaseModuleConfiguration {

	private Map<String, String> map;


	/**
	 * Constructor.
	 */
	public DefaultDatabaseModuleConfiguration() {
		map = new HashMap<>();
	}

	/**
	 * Return the last folder path for the given key or null if not found.
	 * This implementation only use the value on memory.
	 *
	 * @param key The key.
	 * @return the last folder path for the given key or null if not found.
	 * @see eu.omp.irap.cassis.database.configuration.InterfaceDatabaseModuleConfiguration#getLastFolder(java.lang.String)
	 */
	@Override
	public String getLastFolder(String key) {
		return map.get(key);
	}

	/**
	 * Set the last folder path for a key. This store it only on a map destroyed
	 *  once the application stop.
	 *
	 * @param key The key.
	 * @param path The path to set.
	 * @see eu.omp.irap.cassis.database.configuration.InterfaceDatabaseModuleConfiguration#setLastFolder(java.lang.String, java.lang.String)
	 */
	@Override
	public void setLastFolder(String key, String path) {
		map.put(key, path);
	}

	/**
	 * Return null.
	 *
	 * @return null.
	 * @see eu.omp.irap.cassis.database.configuration.InterfaceDatabaseModuleConfiguration#getDatabaseConfigPath()
	 */
	@Override
	public String getDatabaseConfigPath() {
		return null;
	}

	/**
	 * Return null.
	 *
	 * @return null.
	 * @see eu.omp.irap.cassis.database.configuration.InterfaceDatabaseModuleConfiguration#getPartionMolePath()
	 */
	@Override
	public String getPartionMolePath() {
		return null;
	}

	/**
	 * Return null.
	 *
	 * @return null.
	 * @see eu.omp.irap.cassis.database.configuration.InterfaceDatabaseModuleConfiguration#getDatabasePath()
	 */
	@Override
	public String getDatabasePath() {
		return null;
	}

	/**
	 * Return false.
	 *
	 * @return false.
	 * @see eu.omp.irap.cassis.database.configuration.InterfaceDatabaseModuleConfiguration#isOnlineMode()
	 */
	@Override
	public boolean isOnlineMode() {
		return false;
	}

}
