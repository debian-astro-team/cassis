/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation;

import java.awt.Dimension;
import java.util.Locale;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.LookAndFeel;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.plaf.metal.MetalLookAndFeel;
import javax.swing.plaf.metal.OceanTheme;

import eu.omp.irap.cassis.database.creation.tools.log.SimpleLogger;

/**
 * The main class.</br> This class contains the main method and extends
 * {@link JFrame}
 *
 * @author bpenavayre
 *
 */
public final class DbCreationMain extends JFrame {

	private static final long serialVersionUID = 8816995896247127387L;


	/**
	 * Create the {@link JFrame} and use as {@link ContentPane}</br> the
	 * {@link DbCreationPanel} given in parameter
	 *
	 * @param dbPanel
	 *            instance of {@link DbCreationPanel},</br> it contains all the
	 *            graphical components to be displayed in the main
	 *            {@link JFrame}
	 */
	private DbCreationMain(DbCreationPanel dbPanel) {
		super("Database creation");
		ToolTipManager.sharedInstance().setInitialDelay(0);
		setContentPane(dbPanel);
		setMinimumSize(new Dimension(650, 300));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		pack();
	}

	/**
	 * Initialize the {@link LookAndFeel},</br> Instantiate the Controller by
	 * giving him a new Model, create and send the View's Panel from the
	 * controller to {@link DbCreationMain} and set the View visible
	 *
	 * @param args
	 *            not used
	 */
	public static void main(String[] args) {
		Locale.setDefault(Locale.ENGLISH);
		String lookAndFeel = UIManager.getCrossPlatformLookAndFeelClassName();

		try {
			UIManager.setLookAndFeel(lookAndFeel);
			MetalLookAndFeel.setCurrentTheme(new OceanTheme());
			UIManager.setLookAndFeel(new MetalLookAndFeel());
		} catch (Exception e) {
			Logger logger = SimpleLogger.getLogger(Logger.GLOBAL_LOGGER_NAME);
			logger.severe("Couldn't get specified look and feel ("
					+ lookAndFeel + "), for some reason.");
			logger.severe("Using the default look and feel.");
			logger.severe(e.getMessage());
		}
		DbCreationMain dbView = new DbCreationMain(new DbCreationPanel());
		dbView.setVisible(true);
		dbView.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
