/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB;
import eu.omp.irap.cassis.database.configuration.DatabaseConfiguration;
import eu.omp.irap.cassis.database.creation.extraction.PrepareExtractionForGui;
import eu.omp.irap.cassis.database.creation.filters.FiltersControl;
import eu.omp.irap.cassis.database.creation.importation.SelectionEvent;
import eu.omp.irap.cassis.database.creation.importation.gui.DatabaseTabElement;
import eu.omp.irap.cassis.database.creation.importation.gui.add.AddDatabaseControl;
import eu.omp.irap.cassis.database.creation.tools.DatabaseContainer;
import eu.omp.irap.cassis.database.creation.tools.PartiFunctChangeFrame;
import eu.omp.irap.cassis.database.creation.tools.log.SimpleLogger;

/**
 * The main controller (MVC).</br> This class is the link between
 * {@link DbCreationModel} and {@link DbCreationPanel}
 *
 * @author bpenavayre
 *
 */
public class DbCreationControl implements ModelListener, ActionListener {

	private static final String CONFIG_EXTENTION_NAME = "dbconf";
	private static final String CONFIG_EXTENTION = "." + CONFIG_EXTENTION_NAME;
	private static final String CONFIG_DESCRIPTION = "Database configuration (*.dbconf)";

	private DbCreationModel dbModel;
	private DbCreationPanel dbPanel;
	private FiltersControl filtersController;
	private boolean changeTableState;
	private Logger logger;


	/**
	 * Save a link with the Model (dbModel),</br> adds itself as a
	 * {@link ModelListener}</br> to it and set the default value of all
	 * {@link DbCreationControl} variables
	 *
	 * @param dbModel
	 *            The data model (MVC)
	 */
	public DbCreationControl(DbCreationPanel dbPanel) {
		this.dbModel = new DbCreationModel();
		this.changeTableState = false;
		this.dbPanel = dbPanel;
		this.filtersController = null;
		logger = SimpleLogger.getLogger(getClass().getName());
		dbModel.addModelListener(this);
	}

	/**
	 * Instantiate the View's panel {@link DbCreationPanel} if it's not the
	 * case</br> then return it
	 *
	 * @return DbCreationPanel
	 */
	public DbCreationPanel getViewPanel() {
		return dbPanel;
	}

	/**
	 * Receive events from {@link DbCreationModel}, and either use it or send it
	 * to sub controllers
	 */
	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (event.getSource().equals(DbCreationModel.FIRE_ADD)) {
			DatabaseContainer dbContainer = (DatabaseContainer) event.getValue();
			processDatabaseAdded(dbContainer); // Added !
		} else if (event.getSource().equals(DbCreationModel.FIRE_ERROR)) {
			showErrorOptionPane(event);
		} else if (event.getSource().equals(DbCreationModel.FIRE_REMOVE)) {
			DatabaseContainer dbContainer = (DatabaseContainer) event.getValue();
			processDatabaseRemoved(dbContainer);
		} else if (event.getSource().equals(DbCreationModel.FIRE_REMOVE_ALL)) {
			processAllDatabaseRemoved();
		} else if (event.getSource().equals(DbCreationModel.FIRE_CHANGE_STATE)) {
			DatabaseContainer dbContainer = (DatabaseContainer) event.getValue();
			boolean state = (Boolean) event.getOrigin();
			dbPanel.changeDatabaseState(dbContainer, state);
		} else if (event.getSource().equals(DatabaseTabElement.CHANGE_TABLE_SELECTION_STATE)) {
			JTable table = (JTable) event.getValue();
			DatabaseContainer dbContainer = (DatabaseContainer) event.getOrigin();
			changeTableSelectionState(table, dbContainer);
		} else if (event.getSource().equals(DatabaseTabElement.CHANGE_FUNCTION_PART_INTERFACE)) {
			SimpleMoleculeDescriptionDB molecule = (SimpleMoleculeDescriptionDB) event.getOrigin();
			PartiFunctChangeFrame.changeFunctionPartInterface(molecule);
		} else if (event.getSource().equals(DatabaseTabElement.CHANGE_STATE_MOLECULE)) {
			SelectionEvent selectionEvent = (SelectionEvent) event.getValue();
			dbModel.changeStateMolecule(selectionEvent.getDatabase(),
					selectionEvent.getTag(), selectionEvent.isSelected());
		}
	}

	/**
	 * Show a {@link JOptionPane} error message using the parameter's content.
	 *
	 * @param event
	 *            {@link ModelChangedEvent}
	 */
	private void showErrorOptionPane(ModelChangedEvent event) {
		JOptionPane.showConfirmDialog(dbPanel, (String) event.getValue(),
				(String) event.getOrigin(), JOptionPane.DEFAULT_OPTION,
				JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * Process the click on "Add database" tab.
	 */
	public void addDatabaseClicked() {
		final JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		final AddDatabaseControl addDatabaseControl = new AddDatabaseControl(dbModel);
		frame.setContentPane(addDatabaseControl.getView());
		frame.setTitle(AddDatabaseControl.TITLE);
		frame.pack();
		frame.setMinimumSize(frame.getPreferredSize());
		frame.setLocationRelativeTo(dbPanel);
		frame.setVisible(true);
		addDatabaseControl.getView().getValidateButton().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
	}

	/**
	 * Switches the current {@link JTable}'s row selection's state
	 *
	 * @param table
	 *            the {@link JTable} which selection is to be changed
	 */
	public void changeTableSelectionState(JTable table,
			DatabaseContainer database) {
		int col = table.getColumnCount() - 1;
		if (database.isEnabled()) {
			if (table.getSelectedRowCount() == 0) {
				dbModel.changeStateOfAllMoleculesInDb(database,
						changeTableState);
			} else {
				for (int row : table.getSelectedRows()) {
					table.setValueAt(changeTableState, row, col);
					dbModel.changeStateMolecule(database, database
							.getMolecules().get(row).getTag(), changeTableState);
				}
			}
			changeTableState ^= true;
		}
	}

	/**
	 * Handles the button panel on the bottom of the main frame
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == dbPanel.getFiltersButton()) {
			showAFilterView();
		} else if (e.getSource() == dbPanel.getSaveButton()) {
			onSaveConfigButton();
		} else if (e.getSource() == dbPanel.getLoadButton()) {
			onLoadConfigButton();
		} else if (e.getSource() == dbPanel.getCreateButton()) {
			createDatabase();
		}
	}

	/**
	 * Creates a new {@link FiltersControl}</br> if required and show its view.
	 */
	private void showAFilterView() {
		if (filtersController == null) {
			filtersController = new FiltersControl(dbModel.getFilterModel(),
					dbPanel);
		}
		filtersController.getView().setVisible(true);
	}

	/**
	 * Finds through {@link JFileChooser} the file to use as configuration
	 * file,</br> asks the user if should override in case the file exist and
	 * then if all went well</br> send it to {@link DbCreationModel} through
	 * {@link DbCreationModel#initSaveConfig(File) initSaveConfig}.
	 */
	private void onSaveConfigButton() {
		File configFile = null;
		JFileChooser fc = new CassisJFileChooser(
				DatabaseConfiguration.getInstance().getDatabaseConfigPath(),
				DatabaseConfiguration.getInstance().getLastFolder("database-config"));
		fc.setDialogType(JFileChooser.SAVE_DIALOG);
		fc.setFileFilter(new FileNameExtensionFilter(CONFIG_DESCRIPTION,
				CONFIG_EXTENTION_NAME));
		if (fc.showSaveDialog(dbPanel) == JFileChooser.APPROVE_OPTION) {
			try {
				File file = fc.getSelectedFile();
				String name = file.getAbsolutePath();
				if (!file.getName().endsWith(CONFIG_EXTENTION)) {
					name += CONFIG_EXTENTION;
				}
				configFile = new File(name);
				if (configFile.exists()) {
					String message = "Configuration " + configFile.getName()
							+ " already exists.\nDo you want to replace it?";
					int answer = JOptionPane.showConfirmDialog(dbPanel,
							message, "Replace existing configuration file?",
							JOptionPane.YES_NO_OPTION);
					if (answer == JOptionPane.NO_OPTION) {
						configFile = null;
					}
				}
				if (configFile != null) {
					dbModel.initSaveConfig(configFile);
				}
				DatabaseConfiguration.getInstance().setLastFolder(
						"database-config", fc.getSelectedFile().getParent());
			} catch (Exception e) {
				logger.severe(e.getMessage());
			}
		}
	}

	/**
	 * Finds through {@link JFileChooser} the file to use as configuration
	 * file,</br> checks if the is ok and then transmit it to
	 * {@link DbCreationModel} through
	 * {@link DbCreationModel#initLoadConfigConnection(String)
	 * initLoadConfigConnection}
	 */
	private void onLoadConfigButton() {
		JFileChooser fc = new CassisJFileChooser(
				DatabaseConfiguration.getInstance().getDatabaseConfigPath(),
				DatabaseConfiguration.getInstance().getLastFolder("database-config"));
		fc.setFileFilter(new FileNameExtensionFilter(CONFIG_DESCRIPTION,
				CONFIG_EXTENTION_NAME));
		if (fc.showOpenDialog(dbPanel) == JFileChooser.APPROVE_OPTION) {
			File selectedFile = fc.getSelectedFile();
			if (selectedFile != null) {
				try {
					dbModel.initLoadConfigConnection(selectedFile.getPath(),
							dbPanel);
				} catch (IOException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(dbPanel,
							"Unable to read config file. ", "Alert",
							JOptionPane.ERROR_MESSAGE);
				}
			}
			DatabaseConfiguration.getInstance().setLastFolder(
					"database-config", fc.getSelectedFile().getParent());
		}
	}

	private void processDatabaseRemoved(DatabaseContainer dbContainer) {
		DatabaseTabElement dte = dbPanel.removeDatabase(dbContainer);
		dte.removeListenersAndReferences();
	}

	private void processDatabaseAdded(DatabaseContainer dbContainer) {
		DatabaseTabElement dte = dbPanel.addDatabase(dbContainer);
		dte.addListeners(dbModel);
		dte.addModelListener(this);
	}

	private void processAllDatabaseRemoved() {
		for (DatabaseTabElement dte : dbPanel.removeAllDatabases()) {
			dte.removeListenersAndReferences();
		}
	}

	public DbCreationModel getModel() {
		return dbModel;
	}

	/**
	 * Create the database.
	 *
	 * @return The path of the new database created, or null if there was an error.
	 */
	public String createDatabase() {
		return PrepareExtractionForGui.openSave(dbModel, dbPanel);
	}
}
