/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import eu.omp.irap.cassis.common.BufferedWriterProperty;
import eu.omp.irap.cassis.common.events.DataModel;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB;
import eu.omp.irap.cassis.database.access.TypeDataBase;
import eu.omp.irap.cassis.database.creation.filters.FiltersModel;
import eu.omp.irap.cassis.database.creation.importation.CorrectUnknownId;
import eu.omp.irap.cassis.database.creation.importation.ImportDatabase;
import eu.omp.irap.cassis.database.creation.importation.gui.selection.SelectionWorker;
import eu.omp.irap.cassis.database.creation.importation.gui.wait.GuiImportLauncher;
import eu.omp.irap.cassis.database.creation.tools.DatabaseContainer;
import eu.omp.irap.cassis.database.creation.tools.FullMolecule;
import eu.omp.irap.cassis.database.creation.tools.log.SimpleLogger;

/**
 * The model (MVC) class,</br> this class contains all the important data
 *
 * @author bpenavayre
 *
 */
public class DbCreationModel extends DataModel {

	public static final String FIRE_ERROR = "openError";
	public static final String FIRE_REMOVE_ALL = "removeAllDatabases";
	public static final String FIRE_REMOVE = "removeDatabase";
	public static final String FIRE_CHANGE_STATE = "changeEnableState";
	public static final String FIRE_ADD = "addDatabase";

	private static final String FIRE_CHANGE_ALL_MOL_STATE = "changeAllMoleculeState";
	private static final String FIRE_CHANGE_MOL_STATE = "changeMoleculeState";
	private static final int MAX_CHARS_PER_LINE = 100;

	private final Logger logger;

	private List<DatabaseContainer> databaseList;
	private FiltersModel filterModel;


	/**
	 * The constructor, set the listeners and set everything to its default
	 * value
	 */
	public DbCreationModel() {
		logger = SimpleLogger.getLogger(getClass().getName());
		databaseList = new ArrayList<DatabaseContainer>();
		filterModel = new FiltersModel(databaseList);
		addModelListener(filterModel);
	}

	/**
	 * Return the databaseList
	 *
	 * @return databaseList
	 */
	public List<DatabaseContainer> getDatabaseList() {
		return databaseList;
	}

	/**
	 * Return the filterModel
	 *
	 * @return {@link FiltersModel}
	 */
	public FiltersModel getFilterModel() {
		return filterModel;
	}

	/**
	 * Import the content of the new database for non graphical usage
	 *
	 * @param newDatabase
	 *            the new {@link DatabaseContainer}
	 */
	public void addDatabase(DatabaseContainer newDatabase) {
		addDatabase(newDatabase, null, false);
	}

	/**
	 * This is the default importation method for {@link DatabaseContainer}
	 * s,</br> {@link #fireDataChanged(ModelChangedEvent) fire}
	 * {@link #FIRE_ADD} if worked
	 *
	 * @param newDatabase
	 *            the new {@link DatabaseContainer}
	 * @param guiParent
	 *            the Graphical User Interface's parent to be set, if null
	 *            the</br> importation will be non-graphical (Jython for
	 *            instance)
	 * @param allowSelectionIfGui
	 *            if the type of the database is {@link TypeDataBase#VAMDC}
	 *            ,</br> guiParent is not null and this is true,</br> the
	 *            selection import will be used ( {@link SelectionWorker} )
	 */
	public void addDatabase(DatabaseContainer newDatabase, Object guiParent,
			boolean allowSelectionIfGui) {
		int result = prepareAndLaunchImport(newDatabase, guiParent,
				allowSelectionIfGui);
		if (result == ImportDatabase.IMPORT_SUCCESS
				|| result == ImportDatabase.IMPORT_OK_WIH_ERROR) {
			cleanContent(newDatabase);
			if (!checkContent(newDatabase)) {
				return;
			}
			databaseList.add(newDatabase);
			fireDataChanged(new ModelChangedEvent(FIRE_ADD, newDatabase));
			return;
		}
		if (result == ImportDatabase.IMPORT_FAILED) {
			fireDataChanged(new ModelChangedEvent(FIRE_ERROR,
					"An error occured,\n the extraction of "
							+ newDatabase.getDatabaseName() + " failed.", "Failure"));
		}
	}

	private int prepareAndLaunchImport(DatabaseContainer newDatabase,
			Object guiParent, boolean allowSelectionIfGui) {
		if (guiParent != null) {
			return GuiImportLauncher.launchImportWithGui(newDatabase,
					guiParent, allowSelectionIfGui);
		}
		return ImportDatabase.doImport(newDatabase, null);
	}

	private void cleanContent(DatabaseContainer newDatabase) {
		CorrectUnknownId.doCorrect(newDatabase);
		Iterator<FullMolecule> iter = newDatabase.getMolecules().iterator();
		while (iter.hasNext()) {
			if (iter.next().getTransitionSize() <= 0) {
				iter.remove();
			}
		}
	}

	private boolean checkContent(DatabaseContainer newDatabase) {
		boolean molecules = newDatabase.getMolecules() != null
				&& !newDatabase.getMolecules().isEmpty();
		if (molecules && newDatabase.checkTransitions()) {
			return true;
		}
		if (molecules) {
			String error = String.format(
					"The database %s has no molecules and so not added.",
					newDatabase.getDatabaseName());
			fireDataChanged(new ModelChangedEvent(FIRE_ERROR, error,
					"Molecules missing"));
			return false;
		}
		String error = String.format(
				"The database %s has no transitions and so not added.",
				newDatabase.getDatabaseName());
		fireDataChanged(new ModelChangedEvent(FIRE_ERROR, error,
				"Transitions missing"));
		return false;
	}

	/**
	 * Remove a database from the list
	 *
	 * @param database
	 *            the database to remove
	 */
	public void removeDatabase(DatabaseContainer database) {
		databaseList.remove(database);
		fireDataChanged(new ModelChangedEvent(FIRE_REMOVE, database));
	}

	/**
	 * Remove all databases from the list
	 */
	public void removeAllDatabases() {
		databaseList.clear();
		CorrectUnknownId.reset();
		fireDataChanged(new ModelChangedEvent(FIRE_REMOVE_ALL));
	}

	/**
	 * Change the state a database, taking the database and</br> the newState of
	 * the database as arguments
	 *
	 * @param database
	 *            database to enable/disable
	 * @param newEnableState
	 *            the new state to set
	 */
	public void changeStateDatabase(DatabaseContainer database,
			boolean newEnableState) {
		database.setEnabled(newEnableState);
		fireDataChanged(new ModelChangedEvent(FIRE_CHANGE_STATE, database,
				newEnableState));
	}

	/**
	 * Change the state of a molecule in the given {@link DatabaseContainer}
	 *
	 * @param db
	 *            the {@link DatabaseContainer}
	 * @param tag
	 *            the tag of the molecule
	 * @param state
	 *            the new state
	 */
	public void changeStateMolecule(DatabaseContainer db, int tag, boolean state) {
		int moleculeIndex = 0;
		for (SimpleMoleculeDescriptionDB molecule : db.getMolecules()) {
			if (molecule.getTag() == tag) {
				db.setMoleculeState(moleculeIndex, state);
				fireDataChanged(new ModelChangedEvent(FIRE_CHANGE_MOL_STATE,
						tag));
			}
			moleculeIndex++;
		}
	}

	private void createErrorMessage(List<String> listErrors, String error,
			String consequence, String title, boolean isTimePresent) {
		StringBuilder nstr = new StringBuilder();
		String[] timeString = { " was ", " were " };
		if (isTimePresent) {
			timeString[0] = " is ";
			timeString[1] = " are ";
		}
		int i = 0;
		nstr.append(listErrors.get(0));
		if (listErrors.size() > 1) {
			while (++i < listErrors.size() - 1) {
				nstr.append(", " + listErrors.get(i));
			}
			nstr.append(" and " + listErrors.get(i));
			nstr.append(timeString[1] + error + " and therefor were " + consequence);
		} else {
			nstr.append(timeString[0] + error + " and therefor was " + consequence);
		}
		i = 0;
		while (i < nstr.length()) {
			i += MAX_CHARS_PER_LINE;
			while (i < nstr.length() && nstr.charAt(i) != ' ') {
				i++;
			}
			if (i < nstr.length()) {
				nstr.setCharAt(i, '\n');
			}
		}
		fireDataChanged(new ModelChangedEvent(FIRE_ERROR, nstr.toString(),
				title));
	}

	/**
	 * The non-graphical loading launcher.
	 *
	 * @param str
	 * @throws IOException
	 */
	public void initLoadConfigConnection(String str) throws IOException {
		initLoadConfigConnection(str, null);
	}

	/**
	 * Initialize the connection with the configuration file
	 *
	 * @param path
	 *            the path of the file
	 * @param guiParent
	 *            the graphical parent of the future importation dialog
	 * @throws IOException
	 */
	public void initLoadConfigConnection(String path, Object guiParent)
			throws IOException {
		File checkOkFile = new File(path);
		if (checkOkFile.isFile() && checkOkFile.canRead()) {
			try (FileInputStream fis = new FileInputStream(path)) {
				Properties prop = new Properties();
				prop.load(fis);
				loadConfig(prop, guiParent);
			}
		}
	}

	@Override
	public void loadConfig(Properties prop) throws IOException {
		loadConfig(prop, null);
	}

	/**
	 * @param prop
	 *            the {@link Properties} object, link with the configuration
	 *            file
	 * @param guiParent
	 *            the graphical parent of the future importation dialog
	 * @throws IOException
	 */
	private void loadConfig(Properties prop, Object guiParent)
			throws IOException {
		int cpt = 0;
		removeAllDatabases();
		DatabaseContainer newDatabase = null;
		List<String> listErrors = new ArrayList<String>();
		while (prop.containsKey(++cpt + "Name")) {
			newDatabase = new DatabaseContainer("", "", null);
			newDatabase.loadConfig(prop, cpt);
			if (checkIfPathExist(newDatabase)) {
				addDatabase(newDatabase, guiParent, false);
			} else {
				listErrors.add(newDatabase.getDatabaseName());
			}
		}
		if (!listErrors.isEmpty()) {
			createErrorMessage(listErrors, "not found", "not added",
					"database(s) missing", false);
		}
		filterModel.loadConfig(prop);
	}

	private boolean checkIfPathExist(DatabaseContainer newDatabase) {
		if (newDatabase.getType().isUrl()) {
			return true;
		}
		File checkIfExist = new File(newDatabase.getPath());
		return checkIfExist.exists() && checkIfExist.isFile()
				&& checkIfExist.canRead();
	}

	/**
	 * Initialize the connection with the new configuration file to fill
	 *
	 * @param configFile
	 */
	public void initSaveConfig(File configFile) {
		try (BufferedWriterProperty out = new BufferedWriterProperty(new FileWriter(configFile))) {
			out.write("# " + new Date() + '\n');
			saveConfig(out);
		} catch (IOException e) {
			logger.severe(e.getMessage());
		}
	}

	@Override
	public void saveConfig(BufferedWriterProperty out) throws IOException {
		int cpt = 1;
		for (DatabaseContainer database : databaseList) {
			database.setCpt(cpt);
			database.saveConfig(out);
			cpt++;
			out.flush();
		}
		filterModel.saveConfig(out);
	}

	/**
	 * @return true if the model contains at least one active molecule
	 */
	public boolean constainsAtLeastOneActiveMolecule() {
		for (DatabaseContainer db : databaseList) {
			if (!db.isEnabled()) {
				continue;
			}
			for (SimpleMoleculeDescriptionDB mol : db.getMolecules()) {
				if (db.getMoleculeState(mol)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @return true is contains at least one active database
	 */
	public boolean constainsAtLeastOneActiveDatabase() {
		for (DatabaseContainer db : databaseList) {
			if (db.isEnabled()) {
				return true;
			}
		}
		return false;
	}

	public void changeStateOfAllMoleculesInDb(DatabaseContainer database,
			boolean changeTableState) {
		for (int i = 0; i < database.getMolecules().size(); i++) {
			database.setMoleculeState(i, changeTableState);
		}
		fireDataChanged(new ModelChangedEvent(FIRE_CHANGE_ALL_MOL_STATE, database, changeTableState));
	}
}
