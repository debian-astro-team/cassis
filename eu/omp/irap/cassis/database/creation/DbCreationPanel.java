/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.MatteBorder;

import eu.omp.irap.cassis.database.creation.importation.gui.DatabaseTabElement;
import eu.omp.irap.cassis.database.creation.tools.DatabaseContainer;

/**
 * This class is the main panel of the project.</br></br>
 *
 * Extends JPanel
 *
 * @author bpenavayre
 *
 */
public class DbCreationPanel extends JPanel {

	private static final long serialVersionUID = -723426483266533800L;
	private static final String FILTERS = "Filters";
	private static final String SAVE = "Save config";
	private static final String LOAD = "Load config";
	private static final String CREATE = "Create Database";
	private static final int WIN_X = 1300;
	private static final int WIN_Y = 350;

	private JTabbedPane tab;
	private DbCreationControl dbControl;
	private JPanel buttonsPanel;
	private JButton filtersButton;
	private JButton saveButton;
	private JButton loadButton;
	private JButton createButton;
	private List<DatabaseTabElement> listDbTabElement;


	/**
	 * The constructor
	 */
	public DbCreationPanel() {
		this.listDbTabElement = new ArrayList<DatabaseTabElement>();
		this.dbControl = new DbCreationControl(this);
		initContent();
	}

	private void initContent() {
		setLayout(new BorderLayout());
		JPanel top = new JPanel(new GridBagLayout());
		fillTop(top);
		add(top, BorderLayout.PAGE_START);
		add(getTabbedPane(), BorderLayout.CENTER);
		add(getButtonsPanel(), BorderLayout.PAGE_END);
	}

	public JButton getCreateButton() {
		if (createButton == null) {
			createButton = new JButton(CREATE);
			createButton.addActionListener(dbControl);
		}
		return createButton;
	}

	public JButton getFiltersButton() {
		if (filtersButton == null) {
			filtersButton = new JButton(FILTERS);
			filtersButton.addActionListener(dbControl);
		}
		return filtersButton;
	}

	public JButton getSaveButton() {
		if (saveButton == null) {
			saveButton = new JButton(SAVE);
			saveButton.addActionListener(dbControl);
		}
		return saveButton;
	}

	public JButton getLoadButton() {
		if (loadButton == null) {
			loadButton = new JButton(LOAD);
			loadButton.addActionListener(dbControl);
		}
		return loadButton;
	}

	private void fillTop(JPanel top) {
		GridBagConstraints c = new GridBagConstraints();

		c.gridwidth = 1;
		c.weightx = 0;
		c.anchor = GridBagConstraints.BASELINE_LEADING;
		c.insets = new Insets(5, 14, 5, 0);
		top.add(getFiltersButton(), c);
		c.weightx = 1;
		top.add(getSaveButton(), c);
	}

	/**
	 * Return the tabbedPane component of the view
	 *
	 * @return tab : the tabbedPane
	 */
	public JTabbedPane getTabbedPane() {
		if (tab == null) {
			tab = new JTabbedPane(SwingConstants.TOP, JTabbedPane.SCROLL_TAB_LAYOUT);
			final JPanel tabComp = new JPanel(new BorderLayout());
			JLabel label = new JLabel("Click on me to add database");
			label.setBorder(BorderFactory.createEmptyBorder(3, 0, 3, 0));
			tabComp.add(label);
			tabComp.setOpaque(false);
			tab.addTab(null, null, null, null);
			tab.setTabComponentAt(0, tabComp);
			tabComp.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					dbControl.addDatabaseClicked();
				}
			});
			tab.setPreferredSize(new Dimension(WIN_X, WIN_Y));
			tab.setBorder(new MatteBorder(0, 5, 0, 10, getBackground()));
		}
		return tab;
	}

	public JPanel getButtonsPanel() {
		if (buttonsPanel == null) {
			buttonsPanel = new JPanel(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			c.gridx = 0;
			c.weightx = 1;
			c.weighty = 1;
			c.gridwidth = 1;
			c.insets.bottom = 5;
			c.insets.top = 5;
			c.insets.left = 10;
			c.anchor = GridBagConstraints.BASELINE_LEADING;
			buttonsPanel.add(getFiltersButton(), c);
			c.anchor = GridBagConstraints.BASELINE_TRAILING;
			c.insets.left = 0;
			c.insets.right = 6;
			c.gridx++;
			buttonsPanel.add(new JLabel("Configuration file:"), c);
			c.gridx++;
			c.anchor = GridBagConstraints.BASELINE_TRAILING;
			c.weightx = 0;
			buttonsPanel.add(getSaveButton(), c);
			c.gridx++;
			c.weightx = 1;
			c.anchor = GridBagConstraints.BASELINE_LEADING;
			buttonsPanel.add(getLoadButton(), c);
			c.anchor = GridBagConstraints.BASELINE_TRAILING;
			c.gridx++;
			c.insets.right = 10;
			buttonsPanel.add(getCreateButton(), c);
			buttonsPanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
		}
		return buttonsPanel;
	}

	public DatabaseTabElement addDatabase(DatabaseContainer dbContainer) {
		DatabaseTabElement dte = new DatabaseTabElement(dbContainer);
		int index = tab.getTabCount() - 1;
		tab.insertTab(dbContainer.getDatabaseName(), null, dte.getScrollPane(),
				dbContainer.getDatabaseName(), index);
		tab.setTabComponentAt(index, dte.getButtonTab());
		tab.setSelectedIndex(index);
		listDbTabElement.add(dte);
		return dte;
	}

	public DatabaseTabElement removeDatabase(DatabaseContainer dbContainer) {
		Iterator<DatabaseTabElement> it = listDbTabElement.iterator();
		while (it.hasNext()) {
			DatabaseTabElement dte = it.next();
			if (dte.getDatabaseContainer().equals(dbContainer)) {
				it.remove();
				int index = tab.indexOfComponent(dte.getScrollPane());
				if (index != -1) {
					tab.removeTabAt(index);
				}
				return dte;
			}
		}
		return null;
	}

	public List<DatabaseTabElement> removeAllDatabases() {
		List<DatabaseTabElement> removed = new ArrayList<DatabaseTabElement>(listDbTabElement);
		listDbTabElement.clear();
		while (tab.getTabCount() > 1) {
			tab.removeTabAt(0);
		}
		return removed;
	}

	public void changeDatabaseState(DatabaseContainer dbContainer, boolean state) {
		for (DatabaseTabElement dte : listDbTabElement) {
			if (dte.getDatabaseContainer().equals(dbContainer)) {
				dte.applyChangeEnableState(state);
			}
		}
	}

	public DbCreationControl getControl() {
		return dbControl;
	}
}
