/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.filters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import eu.omp.irap.cassis.common.BufferedWriterProperty;
import eu.omp.irap.cassis.common.events.DataModel;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.database.access.LineDescriptionDB;
import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB;
import eu.omp.irap.cassis.database.creation.DbCreationModel;
import eu.omp.irap.cassis.database.creation.filters.filter.ConstraintType;
import eu.omp.irap.cassis.database.creation.filters.filter.Filter;
import eu.omp.irap.cassis.database.creation.filters.filter.FilterConstraint;
import eu.omp.irap.cassis.database.creation.filters.filter.Restrictable;
import eu.omp.irap.cassis.database.creation.tools.DatabaseContainer;
import eu.omp.irap.cassis.database.creation.tools.FullMolecule;

/**
 * Extends {@link DataModel} and implements {@link ModelChangedEvent} </br>
 * Receive event from {@link DbCreationModel} and transmit theses with new
 * one</br> from itself to its controllers Filter model,
 *
 * @author bpenavayre
 *
 */
public class FiltersModel extends DataModel implements ModelListener {

	public static final String MODEL_CHANGE = "ModelChange";
	public static final String AUTO_UPDATE = "AutoUpdate";
	public static final String NEW_FILTER = "newFilter";
	public static final String EDIT_FILTER = "editFilter";
	public static final String REMOVE_FILTER = "removeFilter";

	private List<Filter> filterList;
	private List<DatabaseContainer> databaseList;
	private List<Integer> uniqueDatabaseId;
	private int numberOfTransitions;
	private boolean autoUpdate;


	/**
	 * Initiate data's default value
	 *
	 * @param databaseList
	 */
	public FiltersModel(List<DatabaseContainer> databaseList) {
		numberOfTransitions = 0;
		uniqueDatabaseId = new ArrayList<Integer>();
		filterList = new ArrayList<Filter>();
		autoUpdate = false;
		this.databaseList = databaseList;
	}

	/**
	 * @return the list of {@link DatabaseContainer}
	 */
	public List<DatabaseContainer> getDatabaseList() {
		return databaseList;
	}

	/**
	 * @return the list of {@link FilterObject}
	 */
	public List<Filter> getFilterList() {
		return filterList;
	}

	/**
	 * @return the number of filters
	 */
	public int getNumberOfFilters() {
		return filterList.size();
	}

	/**
	 * @return the total number of transitions after filter
	 */
	public int getNumberOfTransitions() {
		return numberOfTransitions;
	}

	/**
	 * @return the list of unique molecules id
	 */
	public List<Integer> getUniqueIdList() {
		return uniqueDatabaseId;
	}

	/**
	 * Fill the list {@link #uniqueDatabaseId}
	 */
	public void fillUniqueDatabaseIdList() {
		if (!uniqueDatabaseId.isEmpty()) {
			uniqueDatabaseId.clear();
		}
		uniqueDatabaseId.add(Filter.ALL_TAGS);
		for (DatabaseContainer dbContainer : databaseList) {
			if (dbContainer.isEnabled()) {
				for (SimpleMoleculeDescriptionDB mol : dbContainer.getMolecules()) {
					if (dbContainer.getMoleculeState(mol)) {
						int test = checkIfElementUniqueInDatabaseList(mol.getTag());
						if (test != Filter.ALL_TAGS) {
							uniqueDatabaseId.add(test);
						}
					}
				}
			}
		}
	}

	private int checkIfElementUniqueInDatabaseList(int tag) {
		for (Integer id : uniqueDatabaseId) {
			if (tag == id) {
				return Filter.ALL_TAGS;
			}
		}
		return tag;
	}

	/**
	 * @param transition
	 * @param source The source database.
	 * @return true if given transition is not filtered
	 */
	public boolean checkIfTransitionIsOkWithAllFilters(
			LineDescriptionDB transition, String source) {
		if (filterList.isEmpty()) {
			return true;
		}
		boolean containsAll = false;
		for (Filter filter : filterList) {
			if (filter.getTag() == Filter.ALL_TAGS) {
				containsAll = true;
				break;
			}
		}
		return checkTranstionWithAllInFilters(transition, source, containsAll);
	}

	private boolean checkTranstionWithAllInFilters(LineDescriptionDB transition,
			String source, boolean containsAll) {
		boolean containsTransition = false;
		for (Filter filter : filterList) {
			if (filter.isTransitionIdOk(transition)) {
				if (filter.isTransitionOk(transition, source)) {
					return true;
				}
				containsTransition = true;
			}
		}
		return !containsTransition && !containsAll;
	}

	/**
	 * Update the total number of transition and the number of transitions</br>
	 * for each molecule
	 *
	 * @param isManualUpdate
	 */
	public void updateNumberOfTransitions(boolean isManualUpdate) {
		if (!isManualUpdate && !autoUpdate) {
			return;
		}
		numberOfTransitions = 0;

		for (DatabaseContainer db : databaseList) {
			if (!db.isEnabled()) {
				continue;
			}

			for (FullMolecule mol : db.getMolecules()) {
				if (!db.getMoleculeState(mol)
						&& (mol.getTransitionSize() != 0 || mol.getTransitions()
								.isEmpty())) {
					continue;
				}
				numberOfTransitions += updateMolNumberOfTransitions(mol);
			}
		}
	}

	/**
	 * Update the molecule number of transitions respecting the filters
	 *  and return the number.
	 *
	 * @param mol The molecule.
	 * @return The number of transitions on the given mol respecting the filters.
	 */
	private int updateMolNumberOfTransitions(FullMolecule mol) {
		int molNumberOfTransitions = 0;
		for (LineDescriptionDB line : mol.getTransitions()) {
			if (checkIfTransitionIsOkWithAllFilters(line, mol.getSource())) {
				molNumberOfTransitions++;
			}
		}
		mol.setTransitionSize(molNumberOfTransitions);
		return molNumberOfTransitions;
	}

	private void updateForOneFilter(Filter filter) {
		if (filter.getTag() == Filter.ALL_TAGS) {
			updateNumberOfTransitions(false);
			return;
		}
		if (!autoUpdate) {
			return;
		}

		numberOfTransitions = 0;
		int molNumberOfTransitions;

		for (DatabaseContainer db : databaseList) {
			if (!db.isEnabled()) {
				continue;
			}
			for (FullMolecule mol : db.getMolecules()) {
				if (filter.getTag() != mol.getTag()) {
					numberOfTransitions += mol.getTransitions().size();
					continue;
				}
				molNumberOfTransitions = 0;
				for (LineDescriptionDB line : mol.getTransitions()) {
					if (checkIfTransitionIsOkWithAllFilters(line, mol.getSource())) {
						numberOfTransitions++;
						molNumberOfTransitions++;
					}
				}
				mol.setTransitionSize(molNumberOfTransitions);
			}
		}
	}

	private void databaseElementRemoved() {
		if (!autoUpdate) {
			return;
		}
		numberOfTransitions = 0;
		for (DatabaseContainer db : databaseList) {
			if (!db.isEnabled()) {
				continue;
			}
			for (FullMolecule mol : db.getMolecules()) {
				if (db.getMoleculeState(mol)) {
					numberOfTransitions += mol.getTransitions().size();
				}
			}
		}
	}

	private void databaseAdded(DatabaseContainer newDatabase) {
		if (!autoUpdate) {
			return;
		}
		int numberForOneMol;

		if (!newDatabase.isEnabled()) {
			return;
		}
		for (FullMolecule mol : newDatabase.getMolecules()) {
			if (!newDatabase.getMoleculeState(mol)) {
				continue;
			}
			if (getNumberOfFilters() == 0) {
				numberOfTransitions += mol.getTransitionSize();
				continue;
			}
			numberForOneMol = 0;
			for (LineDescriptionDB line : mol.getTransitions()) {
				if (checkIfTransitionIsOkWithAllFilters(line, mol.getSource())) {
					numberForOneMol++;
				}
			}
			mol.setTransitionSize(numberForOneMol);
			numberOfTransitions += numberForOneMol;
		}
	}

	/**
	 * Add a filter (wether its a new or an already existant)
	 *
	 * @param filter
	 * @param index
	 */
	public void addFilter(Filter filter, int index) {
		if (index < 0 || getNumberOfFilters() <= index) {
			addNewFilter(filter);
		} else {
			editFilter(filter, index);
		}
	}

	public void addNewFilter(Filter filter) {
		filterList.add(filter);
		updateForOneFilter(filter);
		fireDataChanged(new ModelChangedEvent(NEW_FILTER, filter));
	}

	private void editFilter(Filter filter, int index) {
		updateForOneFilter(filter);
		fireDataChanged(new ModelChangedEvent(EDIT_FILTER, filter, index));
	}

	/**
	 * Remove a filter
	 *
	 * @param index
	 */
	public void removeFilter(int index) {
		Filter filter = filterList.get(index);
		filterList.remove(filter);
		updateForOneFilter(filter);
		fireDataChanged(new ModelChangedEvent(REMOVE_FILTER, index));
	}

	/**
	 * Remove all filters
	 */
	public void removeAllFilters() {
		while (!filterList.isEmpty()) {
			removeFilter(0);
		}
	}

	@Override
	public void saveConfig(BufferedWriterProperty out) throws IOException {
		out.write(AUTO_UPDATE + '=');
		out.write(autoUpdate ? 't' : 'f');
		out.newLine();
		int cptFilter = 1;
		for (Filter filter : filterList) {
			out.write("#Filters " + cptFilter);
			out.newLine();
			out.write("Tag_" + cptFilter + '=' + filter.getTag());
			out.newLine();
			int cptConstraint = 1;
			for (FilterConstraint fc : filter.getConstraints()) {
				String base = "Constraint_" + cptFilter + '_' + cptConstraint + '_';
				out.write(base + "Restrictable=" + fc.getRestrictable().name());
				out.newLine();
				out.write(base + "Type=" + fc.getType().name());
				out.newLine();
				out.write(base + "Value=" + fc.getValue());
				out.newLine();
				cptConstraint++;
			}
			out.newLine();
			cptFilter++;
		}
	}

	@Override
	public void loadConfig(Properties prop) throws IOException {
		int cptFilter = 1;
		removeAllFilters();
		autoUpdate = prop.getProperty(AUTO_UPDATE).equals("t");
		fireDataChanged(new ModelChangedEvent(AUTO_UPDATE, autoUpdate));
		while (prop.containsKey("Tag_" + cptFilter)) {
			int tag = Integer.parseInt(prop.getProperty("Tag_" + cptFilter));
			Filter filter = new Filter(tag);
			int cptConstraint = 1;
			String base = "Constraint_" + cptFilter + '_' + cptConstraint + '_';
			while (prop.containsKey(base + "Restrictable")
					&& prop.containsKey(base + "Type")
					&& prop.containsKey(base + "Value")) {
				Restrictable restrictable = Restrictable.valueOf(prop.getProperty(base + "Restrictable"));
				ConstraintType type = ConstraintType.valueOf(prop.getProperty(base + "Type"));
				String value = prop.getProperty(base + "Value");
				filter.addConstraint(new FilterConstraint(restrictable, type, value));
				cptConstraint++;
				base = "Constraint_" + cptFilter + '_' + cptConstraint + '_';
			}
			addNewFilter(filter);
			cptFilter++;
		}
	}

	/**
	 * Receive events from {@link DbCreationModel} and fire a</br>
	 * {@link #MODEL_CHANGE} event for his listeners
	 */
	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (!event.getSource().equals(DbCreationModel.FIRE_ERROR)) {
			fillUniqueDatabaseIdList();
			if (event.getSource().equals(DbCreationModel.FIRE_REMOVE)
					|| event.getSource().equals(
							DbCreationModel.FIRE_CHANGE_STATE)) {
				databaseElementRemoved();
			} else if (event.getSource().equals(DbCreationModel.FIRE_ADD)) {
				databaseAdded((DatabaseContainer) event.getValue());
			} else {
				updateNumberOfTransitions(false);
			}
			fireDataChanged(new ModelChangedEvent(MODEL_CHANGE));
		}
	}

	/**
	 * Set {@link #autoUpdate} to the opposite value
	 */
	public void changeAutoUpdate() {
		autoUpdate ^= true;
		updateNumberOfTransitions(false);
		fireDataChanged(new ModelChangedEvent(AUTO_UPDATE, autoUpdate));
	}

	/**
	 * @return {@link #autoUpdate}
	 */
	public boolean getAutoUpdate() {
		return autoUpdate;
	}
}
