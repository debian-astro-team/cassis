/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.filters.filter;

import java.util.logging.Logger;

import eu.omp.irap.cassis.database.access.LineDescriptionDB;

/**
 * Define a filter constraint.
 *
 * @author M. Boiziot
 */
public class FilterConstraint {


	private static final Logger LOGGER = Logger.getLogger(FilterConstraint.class.getName());

	private Restrictable restrictable;
	private String value;
	private ConstraintType type;


	/**
	 * Constructor.
	 *
	 * @param restrictable The restrictable (on what to do the restriction).
	 * @param type The type of filter/restriction.
	 * @param value The value of the restriction.
	 */
	public FilterConstraint(Restrictable restrictable, ConstraintType type, String value) {
		this.restrictable = restrictable;
		this.value = value;
		this.type = type;
	}

	/**
	 * Return the restrictable.
	 *
	 * @return the restrictable.
	 */
	public Restrictable getRestrictable() {
		return restrictable;
	}

	/**
	 * Return the value.
	 *
	 * @return the value.
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Return the type of constraint.
	 *
	 * @return the type of constraint.
	 */
	public ConstraintType getType() {
		return type;
	}


	/**
	 * Change the restrictable.
	 *
	 * @param restrictable The new restrictable to set.
	 */
	public void setRestrictable(Restrictable restrictable) {
		this.restrictable = restrictable;
	}

	/**
	 * Change the value.
	 *
	 * @param value The new value to set.
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Change the type.
	 *
	 * @param type The new type to set.
	 */
	public void setType(ConstraintType type) {
		this.type = type;
	}

	/**
	 * Return if the constraint is valid.
	 *
	 * @return true if the constraint is valid, false otherwise.
	 */
	public boolean isValid() {
		return areParametersValid() && isValueValid();
	}

	/**
	 * Return if the parameters are valid (not null nor empty).
	 *
	 * @return true if the parameters are valid, false otherwise.
	 */
	private boolean areParametersValid() {
		return !(restrictable == null || type == null
				|| value == null || value.isEmpty());
	}

	/**
	 * Check then return if the value is valid according to the restrictable.
	 *
	 * @return true if the value is valid according to the restrictable, false otherwise.
	 */
	private boolean isValueValid() {
		boolean valid = true;
		if (restrictable.getTypeValue() == Double.class) {
			try {
				Double.parseDouble(value);
			} catch (NumberFormatException nfe) {
				valid = false;
			}
		} else if (restrictable.getTypeValue() == Integer.class) {
			try {
				Integer.parseInt(value);
			} catch (NumberFormatException nfe) {
				valid = false;
			}
		} else if (restrictable.getTypeValue() == String.class) {
			return !"".equals(value);
		}
		return valid;
	}

	/**
	 * Return the value as an int. <b>Warning: can be used only on some
	 *  restrictable and the filter must be valid.</b>
	 *
	 * @return the value as an int.
	 */
	public int getValueAsInt() {
		return Integer.parseInt(value);
	}

	/**
	 * Return the value as a double. <b>Warning: can be used only on some
	 *  restrictable and the filter must be valid.</b>
	 *
	 * @return the value as a double.
	 */
	public double getValueAsDouble() {
		return Double.parseDouble(value);
	}

	/**
	 * Check then return if the transition is valid with this constraint.
	 *
	 * @param transition The transition to check.
	 * @param source The source database.
	 * @return true if the transition is valid with this constraint, false otherwise.
	 */
	public boolean isTransitionValid(LineDescriptionDB transition, String source) {
		try {
			if (restrictable.getTypeValue() == Double.class) {
				return checkDoubleValue(getDoubleValueFromTransition(transition), getValueAsDouble());
			} else if (restrictable.getTypeValue() == Integer.class) {
				return checkIntValue(getIntValueFromTransition(transition), getValueAsInt());
			} else if (restrictable == Restrictable.QUANTUM_NUMBERS) {
				return checkStringValue(transition.getQuanticNumbers(), getValue());
			} else if (restrictable == Restrictable.DATABASE) {
				return getValue() != null && getValue().equals(source);
			}
		} catch (NumberFormatException nfe) {
			LOGGER.warning("Value must be a number for restrictable "
					+ restrictable.toString() +". Transition marked as invalid.");
			return false;
		} catch (IllegalStateException ise) {
			LOGGER.severe("Wrong type used for restrictable. Transition marked as invalid.");
			return false;
		}
		LOGGER.severe("Unknown restrictable. Transition marked as invalid.");
		return false;
	}

	/**
	 * Return the int value of the element of the transition according to the
	 *  restrictable of the constraint
	 *
	 * @param transition The transition.
	 * @return the int value of the element of the transition according to the
	 *  restrictable of the constraint
	 */
	private int getIntValueFromTransition(LineDescriptionDB transition) {
		switch (restrictable) {
		case GUP:
			return transition.getIgu();
		case TAG:
			return transition.getTag();
		default:
			throw new IllegalStateException("Unknown restrictable or wrong type used");
		}
	}

	/**
	 * Return the double value of the element of the transition according to the
	 *  restrictable of the constraint
	 *
	 * @param transition The transition.
	 * @return the double value of the element of the transition according to the
	 *  restrictable of the constraint
	 */
	private double getDoubleValueFromTransition(LineDescriptionDB transition) {
		switch (restrictable) {
		case AIJ:
			return transition.getAint();
		case ELOW:
			return transition.getElow();
		case FREQUENCY_ERROR:
			return transition.getError();
		case EUP:
			return transition.getEup();
		case FREQUENCY:
			return transition.getFrequency();
		case GAMMA_SELF:
			return transition.getGammaSelf();
		default:
			throw new IllegalStateException("Unknown restrictable or wrong type used");
		}
	}

	/**
	 * Check then return if the int value of the transition is valid according
	 *  to the value and type of constraint of the Constraint.
	 *
	 * @param transitionValue The value of the transition.
	 * @param filterValue The value of the filter.
	 * @return true if the transition is valid, false otherwise.
	 */
	private boolean checkIntValue(int transitionValue, int filterValue) {
		int difference = transitionValue - filterValue;
		if (difference != 0) {
			if (type == ConstraintType.DIFFERENT ||
					difference < 0 && (type == ConstraintType.INFERIOR || type == ConstraintType.INFERIOR_EQUAL) ||
					difference > 0 && (type == ConstraintType.SUPERIOR || type == ConstraintType.SUPERIOR_EQUAL)) {
				return true;
			}
			return false;
		} else if (type == ConstraintType.EQUAL) {
			return true;
		}
		return false;
	}

	/**
	 * Check then return if the double value of the transition is valid according
	 *  to the value and type of constraint of the Constraint.
	 *
	 * @param transitionValue The value of the transition.
	 * @param filterValue The value of the filter.
	 * @return true if the transition is valid, false otherwise.
	 */
	private boolean checkDoubleValue(double transitionValue, double filterValue) {
		double difference = transitionValue - filterValue;
		if (difference != 0) {
			if (type == ConstraintType.DIFFERENT ||
					difference < 0 && (type == ConstraintType.INFERIOR || type == ConstraintType.INFERIOR_EQUAL) ||
					difference > 0 && (type == ConstraintType.SUPERIOR || type == ConstraintType.SUPERIOR_EQUAL)) {
				return true;
			}
			return false;
		} else if (type == ConstraintType.EQUAL) {
			return true;
		}
		return false;
	}

	/**
	 * Check then return if the String value of the transition is valid according
	 *  to the value and type of constraint of the Constraint.
	 *
	 * @param transitionValue The value of the transition.
	 * @param filterValue The value of the filter.
	 * @return true if the transition is valid, false otherwise.
	 */
	private boolean checkStringValue(String transitionValue, String filterValue) {
		if (type == ConstraintType.EQUAL) {
			return transitionValue.equals(filterValue);
		} else if (type == ConstraintType.DIFFERENT) {
			return !transitionValue.equals(filterValue);
		} else {
			// Not supported for String...
			return false;
		}
	}

	/**
	 * Return a String representation of the constraint.
	 *
	 * @return a String representation of the constraint.
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FilterConstraint [" + restrictable.getDisplayWithUnit() + ' '
				+ type + ' ' + value + ']';
	}
}
