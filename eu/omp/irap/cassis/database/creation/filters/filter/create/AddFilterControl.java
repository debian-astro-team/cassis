/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.filters.filter.create;

import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ActionMap;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.database.creation.filters.FiltersControl;
import eu.omp.irap.cassis.database.creation.filters.FiltersView;
import eu.omp.irap.cassis.database.creation.filters.filter.ConstraintType;
import eu.omp.irap.cassis.database.creation.filters.filter.Filter;
import eu.omp.irap.cassis.database.creation.filters.filter.FilterConstraint;
import eu.omp.irap.cassis.database.creation.filters.filter.Restrictable;
import eu.omp.irap.cassis.database.creation.tools.MouseListenerForTextFields;

/**
 * Controller for {@link AddFilterView},</br> makes the link with its parent
 * controller {@link FiltersControl}.
 *
 * @author bpenavayre
 * @author M. Boiziot
 */
public class AddFilterControl extends WindowAdapter implements ModelListener {

	private AddFilterView addFilterView;
	private FiltersControl filterControl;
	private Filter filter;
	private PopUpIdSelection idSelection;
	private MouseListener textFieldRightClick;


	/**
	 * Set himselef as listener for {@link FiltersControl} and
	 * {@link FiltersView}
	 *
	 * @param filterControl
	 */
	public AddFilterControl(FiltersControl filterControl) {
		this.filterControl = filterControl;
		filterControl.getView().addWindowListener(this);
		filterControl.addListenerToModel(this);
	}

	/**
	 * Create the view and set the {@link FilterObject} field
	 *
	 * @param filter
	 * @param filterView
	 */
	public void createView(Filter filter, FiltersView filterView) {
		this.filter = filter;
		addFilterView = new AddFilterView(this, filter, filterView);
		filterView.addWindowListener(this);
	}

	/**
	 * @return {@link FiltersControl#getUniqueIdMoleculeList()}
	 */
	public List<Integer> getUniqueIdMoleculeVector() {
		return filterControl.getUniqueIdMoleculeList();
	}

	/**
	 * Find the molecule id to use for the filter.
	 */
	public void findAndChooseTheMoleculeId() {
		if (idSelection != null) {
			return;
		}
		idSelection = new PopUpIdSelection(
				filterControl.getUniqueIdMoleculeList(), this, addFilterView);
		idSelection.addWindowListener(this);
	}

	/**
	 * Change the {@link JComboBox}'s id and get rid of the
	 * {@link PopUpIdSelection}
	 *
	 * @param result
	 */
	public void setComboBoxIdTo(Object result) {
		addFilterView.getTagComboBox().setSelectedItem(result);
		idSelection.dispose();
		idSelection = null;
	}

	/**
	 * Check then return the list of invalid {@link ConstraintView}.
	 *
	 * @return the list of invalid {@link ConstraintView}.
	 */
	private List<ConstraintView> getInvalidConstraints() {
		List<ConstraintView> cvList = addFilterView.getConstraintViewList();
		Restrictable restrictable;
		String value;
		ConstraintType type;
		List<ConstraintView> invalidCvList = new ArrayList<ConstraintView>();
		for (ConstraintView cv : cvList) {
			value = cv.getValueTextField().getText();
			restrictable = cv.getSelectedRestrictable();
			type = cv.getSelectedType();
			if (!checkConstraint(restrictable, type, value)) {
				invalidCvList.add(cv);
			}
		}
		return invalidCvList;
	}

	/**
	 * Remove the given {@link ConstraintView}s.
	 *
	 * @param constraints The list of constraints to remove.
	 */
	private void deleteInvalidConstraints(List<ConstraintView> constraints) {
		for (ConstraintView cv : constraints) {
			addFilterView.removeConstraintView(cv);
		}
		addFilterView.getCenter().repaint();
	}

	/**
	 * Check if the constraint with the given parameters is valid.
	 *
	 * @param restrictable The restrictable.
	 * @param type The type.
	 * @param value The value.
	 * @return true if the constraint is valid, false otherwise.
	 */
	private boolean checkConstraint(Restrictable restrictable, ConstraintType type, String value) {
		FilterConstraint fc = new FilterConstraint(restrictable, type, value);
		return fc.isValid();
	}

	private void fillTheFilter(boolean edit) {
		if (edit) {
			filter.setTag(addFilterView.getSelectedTag());
		}
		int place = 0;
		List<ConstraintView> cvList = addFilterView.getConstraintViewList();

		for (ConstraintView cv : cvList) {
			Restrictable restrictable = cv.getSelectedRestrictable();
			ConstraintType type = cv.getSelectedType();
			String value = cv.getValueTextField().getText();
			if (edit) {
				editFilter(place, restrictable, type, value);
			} else {
				createFilter(place, restrictable, type, value);
			}
			place++;
		}
		if (edit) {
			filter.clean(place);
		}
	}

	/**
	 * Edit the filter by changing the parameter of a FilterConstraint.
	 *
	 * @param index The index of the FilterConstraint to edit.
	 * @param restrictable The new Restrictable to set.
	 * @param type The new ConstraintType to set.
	 * @param value The new value to set.
	 */
	private void editFilter(int index, Restrictable restrictable, ConstraintType type,
			String value) {
		FilterConstraint constraint = filter.getConstraint(index);
		constraint.setRestrictable(restrictable);
		constraint.setType(type);
		constraint.setValue(value);
	}

	/**
	 * Create a Filter if necessary (place = 0) with a FilterConstraint with the
	 *  given parameter, or add the FilterConstraint with the given parameters
	 *  to the current Filter.
	 *
	 * @param index The index.
	 * @param restrictable The Restrictable to set.
	 * @param type The ConstraintType to set.
	 * @param value The value to set.
	 */
	private void createFilter(int index, Restrictable restrictable, ConstraintType type, String value) {
		if (index == 0) {
			filter = new Filter(addFilterView.getSelectedTag(),
					new FilterConstraint(restrictable, type, value));
		} else {
			filter.addConstraint(new FilterConstraint(restrictable, type, value));
		}
	}

	private void addTheFilter() {
		boolean editOrCreate = filter != null;
		fillTheFilter(editOrCreate);
		filterControl.sendFilterToModel(filter);
		if (editOrCreate) {
			addFilterView.dispose();
			filterControl.getView().removeWindowListener(this);
			filterControl.removeListenerToModel(this);
		}
		addFilterView.clean();
		filter = null;
	}

	public void addOrInform() {
		List<ConstraintView> invalidConstraints = getInvalidConstraints();
		if (invalidConstraints.isEmpty()) {
			addTheFilter();
			return;
		}
		String message;
		String title;
		if (invalidConstraints.size() == addFilterView.getConstraintViewList().size()) {
			message = "All constraints are invalid.\nPlease correct first.";
			title = "Invalid constraints!";
			JOptionPane.showMessageDialog(addFilterView, message, title,
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		if (invalidConstraints.size() == 1) {
			message = "One constraint is invalid.\nDo you wish to delete it?";
			title = "One constraint is invalid!";
		} else {
			message = invalidConstraints.size()
					+ " constraints are invalid.\nDo you wish to delete those?";
			title = "Multiple constraints invalid!";
		}
		if (JOptionPane.showConfirmDialog(addFilterView, message, title,
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			deleteInvalidConstraints(invalidConstraints);
		}
	}

	/**
	 * Update view when selected restrictable changed.
	 *
	 * @param cv The constraint where the restrictable changed.
	 */
	public void restrictableChanged(ConstraintView cv) {
		cv.getUnitLabel().setText(cv.getSelectedRestrictable().getUnit());
		if (cv.getSelectedRestrictable() == Restrictable.QUANTUM_NUMBERS) {
			cv.getTypeComboBox().setSelectedItem(ConstraintType.EQUAL);
			cv.getTypeComboBox().setEnabled(false);
		} else if (!cv.getTypeComboBox().isEnabled()) {
			cv.getTypeComboBox().setEnabled(true);
		}
	}

	@Override
	public void windowClosing(WindowEvent windowEvent) {
		if (windowEvent.getSource() == null
				|| windowEvent.getSource() == idSelection) {
			if (idSelection != null) {
				idSelection.removeWindowListener(this);
			}
			idSelection = null;
			return;
		}
		addFilterView.removeWindowListener(this);
		filterControl.getView().removeWindowListener(this);
		filterControl.removeListenerToModel(this);
		if (windowEvent.getSource() instanceof FiltersView) {
			addFilterView.dispose();
		}
	}

	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (event.getValue() != null) {
			return;
		}
		addFilterView.getContentPane().validate();
		JComboBox<?> tempCombo = addFilterView.getTagComboBox();
		Object oldSelection = tempCombo.getSelectedItem();
		if (!filterControl.getUniqueIdMoleculeList().contains(oldSelection)) {
			tempCombo.setSelectedIndex(0);
		}
	}

	/**
	 * Initialize if necessary and return the {@link MouseListener} showing
	 *  {@link PopUpIdSelection} when doing a right click.
	 *
	 * @param map The ActionMap.
	 * @return the mouse listener.
	 */
	public MouseListener getRightClick(ActionMap map) {
		if (textFieldRightClick == null) {
			textFieldRightClick = new MouseListenerForTextFields(map);
		}
		return textFieldRightClick;
	}

	/**
	 * Return the filter.
	 *
	 * @return the filter.
	 */
	public Filter getFilter() {
		return filter;
	}
}
