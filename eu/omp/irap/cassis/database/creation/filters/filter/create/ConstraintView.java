/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.filters.filter.create;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import eu.omp.irap.cassis.database.creation.filters.filter.ConstraintType;
import eu.omp.irap.cassis.database.creation.filters.filter.FilterConstraint;
import eu.omp.irap.cassis.database.creation.filters.filter.Restrictable;
import eu.omp.irap.cassis.database.creation.tools.ViewTool;

/**
 * Graphical representation of a {@link FilterConstraint}.
 *
 * @author M. Boiziot
 */
public class ConstraintView extends JPanel {

	private static final long serialVersionUID = -3724976518287031254L;

	private FilterConstraint constraint;
	private JButton closeButton;
	private JComboBox<Restrictable> restrictableComboBox;
	private JComboBox<ConstraintType> typeComboBox;
	private JTextField valueTextField;
	private JLabel unitLabel;


	/**
	 * Constructor.
	 *
	 * @param constraint The constraint, can be null (create GUI element empty
	 *  in that case).
	 */
	public ConstraintView(FilterConstraint constraint) {
		super(new GridBagLayout());
		this.constraint = constraint;
		this.setBorder(ViewTool.lineBorderInsets(Color.GRAY, 5, 5, 5, 15));
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1;
		c.weighty = 6;
		c.gridwidth = 1;
		c.anchor = GridBagConstraints.LINE_START;

		JLabel label = (JLabel) getTypeComboBox().getRenderer();
		label.setHorizontalAlignment(JLabel.CENTER);
		label = new JLabel("Value: ");

		this.add(getRestrictableComboBox(), c);
		c.gridx++;
		this.add(getTypeComboBox(), c);
		c.gridx++;
		c.weightx = 0;
		this.add(label, c);
		c.gridx++;
		c.insets.right = 2;
		this.add(getValueTextField(), c);
		c.gridx++;
		c.insets.right = 0;
		this.add(getUnitLabel(), 4);
		c.anchor = GridBagConstraints.LINE_END;
		c.weightx = 1;
		c.gridx++;
		c.gridwidth = GridBagConstraints.REMAINDER;
		this.add(getCloseButton(), c);
		this.setBackground(Color.WHITE);
	}

	/**
	 * Create if necessary then return the unit label.
	 *
	 * @return the unit label.
	 */
	public JLabel getUnitLabel() {
		if (unitLabel == null) {
			String unit = constraint == null ?
					((Restrictable)getRestrictableComboBox().getSelectedItem()).getUnit()
					: constraint.getRestrictable().getUnit();
			unitLabel = new JLabel(unit);
		}
		return unitLabel;
	}

	/**
	 * Create if necessary then return the Close/X button.
	 *
	 * @return the Close/X button.
	 */
	public JButton getCloseButton() {
		if (closeButton == null) {
			closeButton = new JButton("X");
		}
		return closeButton;
	}

	/**
	 * Create if necessary then return the  the Restrictable JComboBox.
	 *
	 * @return the Restrictable JComboBox.
	 */
	public JComboBox<Restrictable> getRestrictableComboBox() {
		if (restrictableComboBox == null) {
			restrictableComboBox = new JComboBox<Restrictable>(Restrictable.values());
			if (constraint != null) {
				restrictableComboBox.setSelectedItem(constraint.getRestrictable());
			}
		}
		return restrictableComboBox;
	}

	/**
	 * Create if necessary then return the  the Type(ConstraintType) JComboBox.
	 *
	 * @return the Type(ConstraintType) JComboBox.
	 */
	public JComboBox<ConstraintType> getTypeComboBox() {
		if (typeComboBox == null) {
			typeComboBox = new JComboBox<ConstraintType>(ConstraintType.values());
			if (constraint != null) {
				typeComboBox.setSelectedItem(constraint.getType());
			}
		}
		return typeComboBox;
	}

	/**
	 * Create if necessary then return the value JTextField.
	 *
	 * @return the value JTextField.
	 */
	public JTextField getValueTextField() {
		if (valueTextField == null) {
			valueTextField = new JTextField(8);
			if (constraint != null) {
				valueTextField.setText(constraint.getValue());
			}
		}
		return valueTextField;
	}

	/**
	 * Return the selected element of the Type JComboBox.
	 *
	 * @return the selected element of the Type JComboBox.
	 */
	public ConstraintType getSelectedType() {
		return (ConstraintType) getTypeComboBox().getSelectedItem();
	}

	/**
	 * Return the selected element of the Restrictable JComboBox.
	 *
	 * @return the selected element of the Restrictable JComboBox.
	 */
	public Restrictable getSelectedRestrictable() {
		return (Restrictable) getRestrictableComboBox().getSelectedItem();
	}

	/**
	 * Remove the listeners.
	 */
	public void removeListeners() {
		ConstraintView.removeActionListeners(getCloseButton());
		ConstraintView.removeActionListeners(getRestrictableComboBox());
		ConstraintView.removeActionListeners(getTypeComboBox());
		ConstraintView.removeMouseListeners(getValueTextField());
	}

	/**
	 * Removes all {@link ActionListener} from the provided {@link AbstractButton}.
	 *
	 * @param component The button.
	 */
	private static void removeActionListeners(AbstractButton component) {
		for (ActionListener al : component.getActionListeners()) {
			component.removeActionListener(al);
		}
	}

	/**
	 * Removes all {@link MouseListener}.
	 *
	 * @param component The component.
	 */
	private static void removeMouseListeners(JComponent component) {
		for (MouseListener ml : component.getMouseListeners()) {
			component.removeMouseListener(ml);
		}
	}

	/**
	 * Removes all {@link ActionListener} from the provided {@link JComboBox}.
	 *
	 * @param component The JComboBox.
	 */
	private static void removeActionListeners(JComboBox<?> component) {
		for (ActionListener al : component.getActionListeners()) {
			component.removeActionListener(al);
		}
	}
}
