/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.filters.filter;

/**
 * Describe object who can be restricted on a filter.
 *
 * @author M. Boiziot
 */
public enum Restrictable {

	AIJ("Aij", "s¯¹", Double.class),
	DATABASE("Database", "", String.class),
	ELOW("Elow", "cm¯¹", Double.class),
	FREQUENCY_ERROR("Frequency Error", "MHz", Double.class),
	EUP("Eup", "K", Double.class),
	FREQUENCY("Frequency", "MHz", Double.class),
	GAMMA_SELF("GammaSelf", "", Double.class),
	GUP("Gup", "", Integer.class),
	QUANTUM_NUMBERS("Quantum Numbers", "", String.class),
	TAG("Tag", "", Integer.class);


	private String display;
	private String unit;
	private Class<?> typeValue;


	/**
	 * Constructor.
	 *
	 * @param display How to display the filter.
	 * @param unit The unit of the restrictable.
	 * @param typeValue The type of value who should restricted.
	 */
	private Restrictable(String display, String unit, Class<?> typeValue) {
		this.display = display;
		this.unit = unit;
		this.typeValue = typeValue;
	}

	/**
	 * Return the unit.
	 *
	 * @return the unit.
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * Return the type of value who should be restricted.
	 *
	 * @return the type of value who should be restricted.
	 */
	public Class<?> getTypeValue() {
		return typeValue;
	}

	/**
	 * Return a displayable representation with the unit if there is one.
	 *
	 * @return a displayable representation with the unit if there is one.
	 */
	public String getDisplayWithUnit() {
		return unit.isEmpty() ? display : display + '(' + unit + ")";
	}

	/**
	 * Return the display value of the Restrictable.
	 *
	 * @return the display value of the Restrictable.
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return display;
	}
}
