/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.filters.filter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.SpringLayout.Constraints;

import eu.omp.irap.cassis.database.access.LineDescriptionDB;
import eu.omp.irap.cassis.database.creation.tools.DatabaseContainer;
import eu.omp.irap.cassis.database.creation.tools.FullMolecule;

/**
 * This class describe a filter. Here a filter is something that we apply on a specific
 * tag of all tag. It contains a list of {@link Constraints}.
 *
 * @author M. Boiziot
 */
public class Filter {

	public static final int ALL_TAGS = 0;
	public static final String ALL_DATABASE_STRING = "All";

	private int tag;
	private List<FilterConstraint> constraints;
	private int numberOfTransitions;


	/**
	 * Basic constructor. Apply to all tag and with no constraint.
	 */
	public Filter() {
		this(ALL_TAGS);
	}

	/**
	 * Constructor. Apply to the given tag and with no constraint.
	 *
	 * @param tag The tag.
	 */
	public Filter(int tag) {
		this(tag, new ArrayList<FilterConstraint>());
	}

	/**
	 * Constructor. Apply to given tag (or {@link #ALL_TAGS}) and add the given constraint.
	 *
	 * @param tag The tag (or {@link #ALL_TAGS}.
	 * @param constraint The constraint to add.
	 */
	public Filter(int tag, FilterConstraint constraint) {
		this(tag, new ArrayList<FilterConstraint>());
		this.constraints.add(constraint);
	}

	/**
	 * Constructor. Apply to a given tag (or {@link #ALL_TAGS}) and use the given list of
	 *  constraints.
	 *
	 * @param tag The tag (or {@link #ALL_TAGS}.
	 * @param constraints The non null list of constraints.
	 */
	public Filter(int tag, List<FilterConstraint> constraints) {
		this.tag = tag;
		this.constraints = constraints;
	}

	/**
	 * Remove the constraint with the given index.
	 *
	 * @param index the constraint to remove.
	 */
	public void removeConstraint(int index) {
		constraints.remove(index);
	}

	/**
	 * Return the tag.
	 *
	 * @return the tag.
	 */
	public int getTag() {
		return tag;
	}

	/**
	 * Return if the tag of the filter apply to the given transition.
	 *
	 * @param transition The transition to check.
	 * @return true if the tag of the filter apply to the transition, false otherwise.
	 */
	public boolean isTransitionIdOk(LineDescriptionDB transition) {
		return isOnAllTags() || (transition != null && transition.getTag() == tag);
	}

	/**
	 * Check then return if the transition is valid with all the constraints of
	 *  the filter.
	 *
	 * @param transition The transition to check.
	 * @param source The source database.
	 * @return true if the transition is valid with all the constraints of the
	 *  filter, false otherwise.
	 */
	public boolean isTransitionOk(LineDescriptionDB transition, String source) {
		for (FilterConstraint constraint : constraints) {
			if (!constraint.isTransitionValid(transition, source)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Return the number of transitions OK with this filter.
	 *
	 * @return the number of transitions OK with this filter.
	 */
	public int getNumberOfTransitions() {
		return numberOfTransitions;
	}

	/**
	 * Return if the filter apply on all species or not.
	 *
	 * @return true if the filter apply to all tag/species, false otherwise.
	 */
	public boolean isOnAllTags() {
		return tag == ALL_TAGS;
	}

	/**
	 * Add a given constraint to the filter.
	 *
	 * @param constraint The constraint to add.
	 */
	public void addConstraint(FilterConstraint constraint) {
		constraints.add(constraint);
	}

	/**
	 * Compute and update the number of transitions OK with this filter.
	 *
	 * @param databaseList The database list.
	 */
	public void updateNumberOfTransitions(List<DatabaseContainer> databaseList) {
		numberOfTransitions = 0;
		for (DatabaseContainer db : databaseList) {
			if (!db.isEnabled()) {
				continue;
			}
			for (FullMolecule mol : db.getMolecules()) {
				if ((tag != ALL_TAGS && mol.getTag() != tag) || !db.getMoleculeState(mol)) {
					continue;
				}
				for (LineDescriptionDB line : mol.getTransitions()) {
					if (isTransitionOk(line, mol.getSource())) {
						numberOfTransitions++;
					}
				}
			}
		}
	}

	/**
	 * Change the tag where the filter apply.
	 *
	 * @param tag The new tag.
	 */
	public void setTag(int tag) {
		this.tag = tag;
	}

	/**
	 * Return an <b>unmodifiable</b> list of the constraints.
	 *
	 * @return an <b>unmodifiable</b> list of the constraints.
	 */
	public List<FilterConstraint> getConstraints() {
		return Collections.unmodifiableList(constraints);
	}

	/**
	 * Return the constraint at the given index.
	 *
	 * @param index The index.
	 * @return the constraint at the given index.
	 */
	public FilterConstraint getConstraint(int index) {
		return constraints.get(index);
	}

	/**
	 * Remove all constraint with at the index equal or superior to the given index.
	 *
	 * @param indexStart The index where the removal should start.
	 */
	public void clean(int indexStart) {
		while (indexStart < constraints.size()) {
			constraints.remove(indexStart);
		}
	}

	/**
	 * Return the HTML representation of the Filter.
	 *
	 * @return the HTML representation of the Filter.
	 */
	public String getHtmlRepresentation() {
		StringBuilder sb = new StringBuilder();
		sb.append("<html></br><div style=\"text-align:right\">");
		if (constraints.isEmpty()) {
			sb.append("No restriction");
		} else {
			sb.append("Filter on ");
			String res = isOnAllTags() ? " all species" : "on tag " + tag;
			sb.append(res);
			sb.append(" where ");
			FilterConstraint fc = constraints.get(0);
			sb.append(fc.getRestrictable());
			sb.append(" ");
			sb.append(fc.getType().getHtmlDisplay());
			sb.append(" ");
			sb.append(fc.getValue());
			for (int i = 1; i < constraints.size(); i++) {
				fc = constraints.get(i);
				sb.append("<br/>and ");
				sb.append(fc.getRestrictable());
				sb.append(" ");
				sb.append(fc.getType().getHtmlDisplay());
				sb.append(" ");
				sb.append(fc.getValue());
			}
		}
		sb.append("</div><br/></html>");
		return sb.toString();
	}

	/**
	 * Return a String representation of the object.
	 *
	 * @return a String representation of the object.
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("Filter on ");
		if (isOnAllTags()) {
			sb.append("all species");
		} else {
			sb.append("species tag ").append(String.valueOf(tag));
		}
		sb.append('\n');
		sb.append("[\n");
		for (FilterConstraint fc : constraints) {
			sb.append("\t").append(fc.toString()).append('\n');
		}
		sb.append(']');
		return sb.toString();
	}


}
