/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.filters.filter.create;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;

import eu.omp.irap.cassis.database.creation.filters.FiltersModel;
import eu.omp.irap.cassis.database.creation.filters.filter.Filter;
import eu.omp.irap.cassis.database.creation.filters.filter.FilterConstraint;
import eu.omp.irap.cassis.database.creation.tools.ViewTool;

/**
 * View for adding a new filter to the {@link FiltersModel}
 *
 * @author bpenavayre
 * @author M. Boiziot
 */
public class AddFilterView extends JFrame {

	private static final long serialVersionUID = -191412540677477358L;

	private static final String TITLE_EDIT = "Edit filter";
	private static final String TITLTE_ADD = "New filter";

	private AddFilterControl addFilterControl;
	private JPanel center;
	private List<Integer> uniqueIdList;
	private JButton validateButton;
	private JButton addConstraintButton;
	private JComboBox<Object> tagComboBox;
	private List<ConstraintView> listConstraintView;
	private JScrollPane centerScrollPane;


	/**
	 * Standard gui constructor,</br>
	 * calls super-constructor init fields,etc...
	 *
	 * @param addFilterControl
	 * @param filter
	 * @param guiParent
	 */
	public AddFilterView(AddFilterControl addFilterControl,
			Filter filter, Component guiParent) {
		super(filter == null ? TITLTE_ADD : TITLE_EDIT);
		this.listConstraintView = new ArrayList<ConstraintView>();
		this.addFilterControl = addFilterControl;
		uniqueIdList = addFilterControl.getUniqueIdMoleculeVector();
		if (uniqueIdList.isEmpty()) {
			uniqueIdList.add(Filter.ALL_TAGS);
		}
		setResizable(false);
		setMinimumSize(new Dimension(500, 150));
		createPanel(filter);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		pack();
		setLocationRelativeTo(guiParent);
		setVisible(true);
	}

	private void createPanel(Filter filter) {
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(fillAndReturnTop(
				new JPanel(new FlowLayout(FlowLayout.LEADING))),
				BorderLayout.PAGE_START);
		panel.add(
				fillAndReturnCenter(new JPanel(new GridLayout(0, 1)), filter),
				BorderLayout.CENTER);
		panel.add(fillAndReturnBottom(new JPanel(new BorderLayout())),
				BorderLayout.PAGE_END);
		panel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		setContentPane(panel);
	}

	/**
	 * @return the molecules id from the {@link JComboBox}
	 */
	public int getSelectedTag() {
		if (getTagComboBox().getSelectedIndex() == 0) {
			return Filter.ALL_TAGS;
		}
		return (int) getTagComboBox().getSelectedItem();
	}

	private JPanel fillAndReturnTop(JPanel panel) {
		panel.setBorder(ViewTool.titleBorderInsets("Species", 12, 5, 0, 10));
		panel.setPreferredSize(new Dimension(500, 45));
		panel.add(getTagComboBox());
		return panel;
	}

	private JScrollPane fillAndReturnCenter(JPanel panel, Filter filter) {
		centerScrollPane = new JScrollPane(panel);
		centerScrollPane.setBorder(BorderFactory.createTitledBorder("Constraints"));
		centerScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		centerScrollPane.setMaximumSize(new Dimension(500, 420));
		centerScrollPane.setPreferredSize(new Dimension(500, 57));
		if (filter == null) {
			ConstraintView constraintView = createAndReturnChoicePanel(null);
			listConstraintView.add(constraintView);
			panel.add(constraintView);
		} else {
			editFilter(filter, panel);
		}
		center = panel;
		return centerScrollPane;
	}

	/**
	 * @return the central panel
	 */
	public JPanel getCenter() {
		return center;
	}

	private void editFilter(Filter filter, JPanel panel) {
		for (FilterConstraint constraint : filter.getConstraints()) {
			addConstraintView(constraint, panel, false);
		}
		validate();
	}

	/**
	 * Create a {@link ConstraintView} from a {@link FilterConstraint} and add
	 *  the needed listeners.
	 *
	 * @param constraint The constraint.
	 * @return the created {@link ConstraintView}.
	 */
	private ConstraintView createAndReturnChoicePanel(FilterConstraint constraint) {
		final ConstraintView constraintView = new ConstraintView(constraint);
		constraintView.getCloseButton().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (listConstraintView.size() > 1) {
					removeConstraintView(constraintView);
					getCenter().repaint();
				}
			}
		});
		constraintView.getRestrictableComboBox().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addFilterControl.restrictableChanged(constraintView);
			}
		});
		constraintView.getValueTextField().addMouseListener(addFilterControl.getRightClick(
				constraintView.getValueTextField().getActionMap()));
		return constraintView;
	}

	/**
	 * Add a new {@link ConstraintView}.
	 *
	 * @param constraint The {@link FilterConstraint} used a base for the
	 *  {@link ConstraintView}.
	 * @param panel The panel used to add the {@link ConstraintView}.
	 * @param update true to update the GUI, false otherwise.
	 */
	private void addConstraintView(FilterConstraint constraint, JPanel panel, boolean update) {
		ConstraintView constraintView = createAndReturnChoicePanel(constraint);
		panel.add(constraintView);
		listConstraintView.add(constraintView);
		if (listConstraintView.size() != 1 && centerScrollPane.getSize().height < 420) {
			Dimension dim = centerScrollPane.getPreferredSize();
			dim.height += 35;
			centerScrollPane.setPreferredSize(dim);
			pack();
		}
		if (update) {
			validate();
			centerScrollPane.getViewport().scrollRectToVisible(constraintView.getBounds());
		}
	}

	/**
	 * Add a new Constraint panel
	 */
	public void addNewConstraintView() {
		addConstraintView(null, center, true);
	}

	private JPanel fillAndReturnBottom(JPanel panel) {
		panel.add(getAddConstraintButton(), BorderLayout.WEST);
		panel.add(getValidateButton(), BorderLayout.EAST);
		panel.setPreferredSize(new Dimension(500, 40));
		panel.setBorder(ViewTool.lineBorderInsets(Color.GRAY, 7, 5, 7, 5));
		return panel;
	}

	/**
	 * Clean the view (remove all ConstraintView).
	 */
	public void clean() {
		for (ConstraintView cv : listConstraintView) {
			center.remove(cv);
		}
		listConstraintView.clear();
		addNewConstraintView();
		validate();
		pack();
	}

	/**
	 * Create if necessary then return the "Add a constraint" button.
	 *
	 * @return the "Add a constraint" button.
	 */
	public JButton getAddConstraintButton() {
		if (addConstraintButton == null) {
			addConstraintButton = new JButton("Add a constraint");
			addConstraintButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					addNewConstraintView();
				}
			});
		}
		return addConstraintButton;
	}

	/**
	 * Create if necessary then return the "Validate" button.
	 *
	 * @return the "Validate" button.
	 */
	public JButton getValidateButton() {
		if (validateButton == null) {
			validateButton = new JButton("Validate");
			validateButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					addFilterControl.addOrInform();
				}
			});
		}
		return validateButton;
	}

	/**
	 * Return the list of ConstraintView.
	 *
	 * @return the list of ConstraintView.
	 */
	public List<ConstraintView> getConstraintViewList() {
		return listConstraintView;
	}

	/**
	 * Return the tag JComboBox.
	 *
	 * @return the tag JComboBox.
	 */
	public JComboBox<Object> getTagComboBox() {
		if (tagComboBox == null) {
			tagComboBox = new JComboBox<Object>(new ComboBoxModelForUniqListOfId(
					uniqueIdList));
			if (addFilterControl.getFilter() != null &&
					uniqueIdList.contains(addFilterControl.getFilter().getTag())) {
				tagComboBox.setSelectedItem(addFilterControl.getFilter().getTag());
			}
			tagComboBox.setPreferredSize(new Dimension(100, 20));
			tagComboBox.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (SwingUtilities.isRightMouseButton(e)) {
						addFilterControl.findAndChooseTheMoleculeId();
					}
				}
			});
		}
		return tagComboBox;
	}

	/**
	 * Remove a {@link ConstraintView}.
	 *
	 * @param cv The {@link ConstraintView} to remove.
	 */
	public void removeConstraintView(ConstraintView cv) {
		getCenter().remove(cv);
		listConstraintView.remove(cv);
		cv.removeListeners();
		validate();
	}
}
