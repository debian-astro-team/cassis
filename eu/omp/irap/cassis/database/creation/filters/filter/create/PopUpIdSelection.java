/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.filters.filter.create;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import eu.omp.irap.cassis.database.creation.filters.filter.Filter;
import eu.omp.irap.cassis.database.creation.tools.MouseListenerForTextFields;

/**
 * {@link JFrame} showing when right clicking on the {@link JComboBox} in
 * {@link AddFilterView}.</br>
 * Gui tool to search the wished id to use
 *
 * @author bpenavayre
 *
 */
public class PopUpIdSelection extends JFrame {

	private static final long serialVersionUID = 6189417818438399340L;
	private AddFilterControl control;
	private JList<Object> list;


	/**
	 * Creates the {@link JFrame} set the wished parameters and add listeners.
	 *
	 * @param listOfId
	 * @param control
	 * @param guiParent
	 */
	public PopUpIdSelection(List<Integer> listOfId, AddFilterControl control,
			Component guiParent) {
		super("Id finder");
		this.control = control;
		JPanel panel = new JPanel(new BorderLayout());
		final JTextField textField = new JTextField(8);
		textField.addMouseListener(new MouseListenerForTextFields(textField
				.getActionMap()));
		panel.add(textField, BorderLayout.NORTH);
		DefaultListModel<Object> model = new DefaultListModel<Object>();
		for (int id : listOfId) {
			if (id == Filter.ALL_TAGS) {
				model.addElement(Filter.ALL_DATABASE_STRING);
			} else {
				model.addElement(id);
			}
		}
		list = new JList<Object>(model);
		list.setSelectedIndex(0);
		panel.add(new JScrollPane(list), BorderLayout.CENTER);
		textField.requestFocus();
		textField.addKeyListener(new FieldKeyAdaptater(listOfId, model, list,
				this));
		list.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					exitWithSelection(list.getSelectedValue());
				} else if (e.getKeyCode() == KeyEvent.VK_UP
						&& list.getSelectedIndex() == 0) {
					textField.requestFocus();
				}
			}
		});
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (SwingUtilities.isLeftMouseButton(e)) {
					exitWithSelection(list.getSelectedValue());
				}
			}
		});

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setContentPane(panel);
		setSize(360, 200);
		setLocationRelativeTo(guiParent);
		setVisible(true);
	}

	/**
	 * @param result the selected id
	 */
	public void exitWithSelection(Object result) {
		if (!list.isSelectionEmpty()) {
			control.setComboBoxIdTo(result);
		}
	}
}
