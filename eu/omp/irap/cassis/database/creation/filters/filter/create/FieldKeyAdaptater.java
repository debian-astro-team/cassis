/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.filters.filter.create;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JTextField;

import eu.omp.irap.cassis.database.creation.filters.filter.Filter;


/**
 * Keybord listener for {@link PopUpIdSelection}
 *
 * @author bpenavayre
 *
 */
public class FieldKeyAdaptater extends KeyAdapter {

	private List<Integer> listOfId;
	private DefaultListModel<Object> model;
	private JList<Object> list;
	private PopUpIdSelection parent;


	/**
	 * Set fields
	 *
	 * @param listOfId
	 * @param model
	 * @param list
	 * @param parent
	 */
	public FieldKeyAdaptater(List<Integer> listOfId,
			DefaultListModel<Object> model, JList<Object> list,
			PopUpIdSelection parent) {
		this.listOfId = listOfId;
		this.model = model;
		this.list = list;
		this.parent = parent;
	}

	private boolean compareIntString(int toCompare, String result) {
		if (result.isEmpty()) {
			return true;
		}
		int newVal = toCompare;
		int pow = 10;
		while (newVal > pow) {
			pow *= 10;
		}
		pow /= 10;
		for (int i = 0; i < result.length() && pow > 0; i++) {
			if (result.charAt(i) - '0' != (char) (newVal / pow)) {
				return false;
			}
			newVal = newVal % pow;
			pow /= 10;
		}
		return true;
	}

	private boolean compareAllString(String userString) {
		if (userString.isEmpty()) {
			return true;
		}
		if (userString.length() > Filter.ALL_DATABASE_STRING.length()) {
			return false;
		}
		for (int i = 0; i < userString.length(); i++) {
			if (userString.charAt(i) != Filter.ALL_DATABASE_STRING
					.charAt(i)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			parent.exitWithSelection(list.getSelectedValue());
		} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			list.requestFocus();
			list.setSelectedIndex(1);
		} else {
			editList(((JTextField) e.getSource()).getText());
		}
	}

	private void editList(String result) {
		removeIncorrectElements(result);
		addCorrectElements(result);
		if (!model.isEmpty()) {
			list.setSelectedIndex(0);
		}
		list.validate();
	}

	private void removeIncorrectElements(String result) {
		for (int i = 0; i < model.size(); i++) {
			if ((i != Filter.ALL_TAGS && !compareIntString(
					(int) model.getElementAt(i), result))
					|| (i == Filter.ALL_TAGS && compareAllString(result))) {
				model.remove(i);
				i--;
			}
		}
	}

	private void addCorrectElements(String result) {
		for (int id : listOfId) {
			if (id != Filter.ALL_TAGS
					&& compareIntString(id, result) && !model.contains(id)) {
				model.addElement(id);
			}
			if (id == Filter.ALL_TAGS
					&& compareAllString(result)
					&& !model.contains(Filter.ALL_DATABASE_STRING)) {
				model.addElement(Filter.ALL_DATABASE_STRING);
			}
		}
	}
}
