/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.filters;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.SwingConstants;
import javax.swing.ToolTipManager;

import eu.omp.irap.cassis.database.creation.filters.filter.Filter;

/**
 * Frame responsible for giving a view over the {@link FiltersModel}
 *
 * @author bpenavayre
 *
 */
public class FiltersView extends JFrame {

	private static final long serialVersionUID = 6917495296127632583L;
	private static final Color BG_COLOR = new Color(246, 246, 246);
	private JPanel center;
	private JCheckBox autoUpdate;
	private int numberOfFilters;
	private FiltersControl filterControl;


	/**
	 * Save link with controller and set wished values
	 *
	 * @param filterControl
	 * @param guiParent
	 */
	public FiltersView(FiltersControl filterControl, Component guiParent) {
		this.filterControl = filterControl;
		setVisible(false);
		setTitle("Add Filters");
		JPanel filterPanel = new JPanel(new BorderLayout());
		filterPanel.setBorder(BorderFactory.createEtchedBorder());
		filterPanel.add(createTop(), BorderLayout.PAGE_START);
		filterPanel.add(createCenter(), BorderLayout.CENTER);
		filterPanel.add(createBottomPanel(), BorderLayout.PAGE_END);
		filterPanel.setPreferredSize(new Dimension(550, 500));
		setMinimumSize(new Dimension(490, 235));
		setContentPane(filterPanel);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		pack();
		setLocationRelativeTo(guiParent);
		numberOfFilters = 0;
	}

	private JPanel createTop() {
		JPanel panel = new JPanel(new BorderLayout());
		autoUpdate = new JCheckBox("Auto update the number of transitions");
		autoUpdate.setSelected(filterControl.getAutoUpdate());
		autoUpdate.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				filterControl.changeAutoUpdate();
			}
		});
		panel.add(autoUpdate, BorderLayout.LINE_START);
		JButton update = new JButton("Update manually");
		update.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				filterControl.doUpdate();
			}
		});
		panel.add(update, BorderLayout.LINE_END);
		panel.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 5));
		return panel;
	}

	private JScrollPane createCenter() {
		center = new JPanel(new GridLayout(0, 1));
		center.setBackground(BG_COLOR);
		JScrollPane scroll = new JScrollPane(center);
		scroll.setAutoscrolls(true);
		scroll.setPreferredSize(new Dimension(550, 450));
		scroll.setBorder(BorderFactory.createEmptyBorder(15, 5, 5, 5));
		scroll.setViewportBorder(BorderFactory.createLineBorder(Color.BLACK));
		scroll.getVerticalScrollBar().setUnitIncrement(99);
		return scroll;
	}

	private JPanel createBottomPanel() {
		JPanel bottom = new JPanel(new BorderLayout());
		JButton addButton = new JButton("Add filter");
		addButton
				.setToolTipText("The filters work together as mathematical union.");
		ToolTipManager.sharedInstance().registerComponent(addButton);
		addButton.addActionListener(filterControl);
		bottom.add(addButton, BorderLayout.WEST);
		JPanel total = new JPanel(new FlowLayout(FlowLayout.CENTER));
		total.add(new JLabel(
				"<html>Total of transitions<br/><div style align = center>"
						+ filterControl.getTransitions() + "</div></html>"));
		bottom.add(total, BorderLayout.CENTER);
		JButton closeButton = new JButton("Close");
		closeButton.addActionListener(filterControl);
		bottom.add(closeButton, BorderLayout.EAST);
		bottom.setPreferredSize(new Dimension(550, 45));
		bottom.setBorder(BorderFactory.createEmptyBorder(2, 5, 10, 5));
		return bottom;
	}

	/**
	 * @return the central panel
	 */
	public JPanel getCenter() {
		return center;
	}

	/**
	 * change the state of the {@link JCheckBox} {@link #autoUpdate}
	 * @param newState
	 */
	public void setAutoUpdateState(boolean newState) {
		autoUpdate.setSelected(newState);
	}

	private void addRemoveButton(JPanel filterPanel) {
		JButton closeButton = new JButton("X");
		JPanel panelButton = new JPanel(new GridBagLayout());
		panelButton.setBackground(BG_COLOR);
		closeButton.setPreferredSize(new Dimension(45, 45));
		panelButton.add(closeButton);
		closeButton.addActionListener(filterControl);
		filterPanel.add(panelButton, BorderLayout.EAST);
	}

	private void addEditButton(JPanel filterPanel, final Filter filter) {
		JButton editButton = new JButton("Edit");
		editButton.setPreferredSize(new Dimension(61, 45));
		JPanel panelButton = new JPanel(new GridBagLayout());
		panelButton.setBackground(BG_COLOR);
		panelButton.add(editButton);
		editButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				filterControl.createViewAddFilter(filter);
			}
		});
		filterPanel.add(panelButton, BorderLayout.WEST);
	}

	/**
	 * Add a new filter to the view with the {@link FilterObject}
	 * @param filter
	 */
	public final void newFilterOn(Filter filter) {
		JPanel filterPanel = new JPanel(new BorderLayout());
		JLabel label = new JLabel(filter.getHtmlRepresentation());
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setVerticalAlignment(SwingConstants.CENTER);
		filterPanel.setBackground(BG_COLOR);
		filterPanel.setBorder(BorderFactory.createTitledBorder("Filter n° "
				+ ++numberOfFilters));
		filterPanel.add(label, BorderLayout.CENTER);
		label = new JLabel(
				"<html>Number of transitions with this filter:<br/><div style align = center>"
						+ filter.getNumberOfTransitions() + "</div></html>");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		label.setOpaque(true);
		label.setBackground(Color.WHITE);
		filterPanel.add(label, BorderLayout.PAGE_END);
		addRemoveButton(filterPanel);
		addEditButton(filterPanel, filter);
		center.add(filterPanel);
		JViewport view = (JViewport) center.getParent();
		view.validate();
		view.scrollRectToVisible(filterPanel.getBounds());
	}

	/**
	 * Change an existing view filter with new data.
	 *
	 * @param filterPanel
	 * @param filter
	 */
	public void editFilter(JPanel filterPanel, Filter filter) {
		JLabel label = (JLabel) filterPanel.getComponent(0);
		label.setText(filter.getHtmlRepresentation());
		filterPanel.getParent().validate();
	}

	/**
	 * Recalculate the view
	 *
	 * @param filterList
	 */
	public void updateViewWithModelData(List<Filter> filterList) {
		int index = 0;
		while (index < filterList.size()) {
			newFilterOn(filterList.get(index));
			index++;
		}
		if (index == 0) {
			return;
		}
		center.getParent().validate();
		center.getParent().repaint();
	}

	/**
	 * Remove a filter's view
	 *
	 * @param index
	 */
	public void removeFilter(int index) {
		center.remove(index);
		int i = index;
		while (i < center.getComponentCount()) {
			if (center.getComponent(i) instanceof JPanel) {
				JPanel panel = (JPanel) center.getComponent(i);
				panel.setBorder(BorderFactory.createTitledBorder("Filter n° "
						+ (i + 1)));
			}
			i++;
		}
		numberOfFilters--;
		center.getParent().validate();
		center.getParent().repaint();
	}

}
