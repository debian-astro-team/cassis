/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.filters;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.database.creation.filters.filter.Filter;
import eu.omp.irap.cassis.database.creation.filters.filter.create.AddFilterControl;

/**
 * {@link FiltersView}'s controller,</br>
 * responsible for its creation and handling its events.
 *
 * @author bpenavayre
 *
 */
public class FiltersControl implements ActionListener, ModelListener {

	private FiltersView filterView;
	private FiltersModel filterModel;


	public FiltersControl(FiltersModel model, Component guiParent) {
		this.filterModel = model;
		filterView = new FiltersView(this, guiParent);
		filterView.updateViewWithModelData(filterModel.getFilterList());
		filterModel.addModelListener(this);
	}

	/**
	 * Return the object {@link FiltersView}, the {@link JFrame} of the filter interface
	 *
	 * @return filterView
	 */
	public FiltersView getView() {
		return filterView;
	}

	/**
	 * @return the number of transtions from the model
	 */
	public int getTransitions() {
		return filterModel.getNumberOfTransitions();
	}

	/**
	 * @return the list of unique molecules
	 */
	public List<Integer> getUniqueIdMoleculeList() {
		return filterModel.getUniqueIdList();
	}

	/**
	 * Handles {@link JButton} events from {@link FiltersView}
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (!(e.getSource() instanceof JButton)) {
			return;
		}
		JButton button = (JButton) e.getSource();
		String title = button.getText();
		if (title.equals("X")) {
			filterModel.removeFilter(fromDeleteToIndex(button.getParent()));
		} else {
			bottomButtonsClicked(title);
		}
	}

	private int fromDeleteToIndex(Component deleteButtonPanel) {
		int index = 0;
		Component filterPanel = deleteButtonPanel.getParent();
		Container center = filterPanel.getParent();
		while (index < center.getComponentCount()
				&& filterPanel != center.getComponents()[index]) {
			index++;
		}
		return index;
	}

	private void bottomButtonsClicked(String title) {
		if (title.equals("Close")) {
			filterView.setVisible(false);
			return;
		}
		createViewAddFilter(null);
	}

	/**
	 * Create a controller and view
	 * @param filter
	 */
	public void createViewAddFilter(Filter filter) {
		AddFilterControl addFilterControl = new AddFilterControl(this);
		addFilterControl.createView(filter, filterView);
	}

	/**
	 * sends the new filter to the model
	 *
	 * @param filter
	 *            the filter object
	 */
	public void sendFilterToModel(Filter filter) {
		int index = filterModel.getFilterList().indexOf(filter);
		filterModel.addFilter(filter, index);
	}

	/**
	 * Receive event from {@link FiltersModel} and act on them
	 */
	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (event.getSource() == FiltersModel.NEW_FILTER) {
			filterView.newFilterOn((Filter) event.getValue());
		} else if (event.getSource() == FiltersModel.REMOVE_FILTER) {
			filterView.removeFilter((int) event.getValue());
		} else if (event.getSource() == FiltersModel.EDIT_FILTER) {
			editFilter((Filter) event.getValue(), (int) event.getOrigin());
		} else if (event.getSource() == FiltersModel.AUTO_UPDATE) {
			filterView.setAutoUpdateState((boolean) event.getValue());
		}

		if (filterModel.getAutoUpdate()) {
			doUpdate();
		}
	}

	private void editFilter(Filter filter, int index) {
		JPanel panel = (JPanel) filterView.getCenter().getComponent(index);
		filterView.editFilter(panel, filter);
	}

	private void updateNumberOfFilters() {
		List<Filter> list = filterModel.getFilterList();
		for (Filter filter : list) {
			filter.updateNumberOfTransitions(filterModel.getDatabaseList());
			JPanel panelFilter = (JPanel) filterView.getCenter().getComponent(
					list.indexOf(filter));
			JLabel numberOfTransitions = (JLabel) panelFilter.getComponent(1);
			numberOfTransitions
					.setText("<html>Number of transitions with this filter:<br/><div style align = center>"
							+ filter.getNumberOfTransitions() + "</div></html>");
		}
		updateTotalNumberView();
	}

	private void updateTotalNumberView() {
		JPanel panel = (JPanel) filterView.getCenter().getParent().getParent()
				.getParent().getComponent(2);
		panel = (JPanel) panel.getComponent(1);
		JLabel numberOfTransitions = (JLabel) panel.getComponent(0);
		numberOfTransitions
				.setText("<html>Total of transitions<br/><div style align = center>"
						+ filterModel.getNumberOfTransitions()
						+ "</div></html>");
	}

	/**
	 * @param listener add a listener to {@link FiltersModel}
	 */
	public void addListenerToModel(ModelListener listener) {
		filterModel.addModelListener(listener);
	}

	/**
	 * @param listener remove a listener to {@link FiltersModel}
	 */
	public void removeListenerToModel(ModelListener listener) {
		filterModel.removeModelListener(listener);
	}

	/**
	 * Call {@link FiltersModel#changeAutoUpdate()}
	 */
	public void changeAutoUpdate() {
		filterModel.changeAutoUpdate();
	}

	/**
	 * @return {@link FiltersModel#getAutoUpdate()}
	 */
	public boolean getAutoUpdate() {
		return filterModel.getAutoUpdate();
	}

	/**
	 * Actions to do when {@link FiltersView}'s update button is clicked
	 */
	public void doUpdate() {
		filterModel.updateNumberOfTransitions(true);
		updateNumberOfFilters();
	}

}
