/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.importation.options;

import java.util.Iterator;
import java.util.List;

import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB;
import eu.omp.irap.cassis.database.creation.tools.DatabaseContainer;
import eu.omp.irap.cassis.database.creation.tools.FullMolecule;

/**
 * Tool class,</br> contains all the functions required to create the the
 * molecule selection option for import.</br>
 *
 * @author bpenavayre
 *
 */
public final class SelectionOption {

	private static final String INTERVAL_SEP = ";";
	private static final String MIN_MAX_SEP = "-";
	/**
	 * If the database option is not set we assume that all the transitions</br>
	 * should be imported
	 */
	private static final String ALL = null;


	private SelectionOption() {
	}

	/**
	 * Create a string out of a list of selected tags and the total number of
	 * molecules in the database
	 *
	 * @param listOfImport
	 * @param size
	 * @return
	 */
	public static String createAndGetResultString(
			final List<Integer> listOfImport, int size) {
		if (listOfImport.size() == size) {
			return ALL;
		}

		StringBuilder builder = new StringBuilder();
		for (int tag : listOfImport) {
			builder.append(tag).append(INTERVAL_SEP);
		}
		return builder.toString();
	}



	/**
	 * Remove the molecules contained in the option of the {@link DatabaseContainer}</br>
	 * for all non-catdir types
	 *
	 * @param newDatabase
	 */
	public static void removeUnselectedMolecules(DatabaseContainer newDatabase) {
		removeUnselectedMolecules(newDatabase, (String) newDatabase.getOption());
	}

	/**
	 * Clear the list of {@link FullMolecule} of all the not selected values
	 *
	 * @param newDatabase
	 * @param option the string containing the list of molecules to keep
	 */
	public static synchronized void removeUnselectedMolecules(
			DatabaseContainer newDatabase, String option) {
		if (option == ALL) {
			return;
		}
		String[] intervals = option.split(INTERVAL_SEP);
		String[] minMax;
		int[][] listOfStartEnd = new int[intervals.length][2];
		int i = 0;
		for (String inter : intervals) {
			minMax = inter.split(MIN_MAX_SEP);
			listOfStartEnd[i][0] = Integer.parseInt(minMax[0]);
			listOfStartEnd[i][1] = minMax.length > 1 ? Integer
					.parseInt(minMax[1]) : listOfStartEnd[i][0];
			i++;
		}
		Iterator<FullMolecule> iter = newDatabase.getMolecules()
				.iterator();
		SimpleMoleculeDescriptionDB mol;
		while (iter.hasNext()) {
			mol = iter.next();
			if (shouldDelete(mol.getTag(), listOfStartEnd)) {
				iter.remove();
			}
		}
	}

	private static boolean shouldDelete(int value, int[][] listOfStartEnd) {
		for (int[] row : listOfStartEnd) {
			if (value >= row[0] && value <= row[1]) {
				return false;
			}
		}
		return true;
	}

}
