/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.importation.options.vamdc.services;

import java.awt.Component;
import java.io.PrintStream;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JDialog;
import javax.swing.SwingWorker;

import eu.omp.irap.cassis.database.creation.tools.log.LoggerOutput;
import eu.omp.irap.cassis.database.creation.tools.log.SimpleLogger;
import eu.omp.irap.cassis.database.creation.tools.log.gui.GuiHandler;
import eu.omp.irap.cassis.database.creation.tools.log.gui.LogGui;

/**
 * Extends {@link SwingWorker} should be called through
 * {@link #getListOfProviderGui(Component) getListOfProviderGui}.</br> It's role
 * is to return {@link GetServiceList#getListVamdcProvider()
 * getListVamdcProvider},</br> to show a {@link JDialog} and to redirect the
 * {@link System#err stderr} to a</br> {@link LogGui} contained in that dialog.
 *
 * @see ServiceLogDialog
 * @see LoggerOutput
 * @see GuiHandler
 * @see LogGui
 *
 * @author bpenavayre
 *
 */
public final class ServiceListWorker extends SwingWorker<List<Provider>, Void> {

	private static final Logger logger = SimpleLogger
			.getLogger(ServiceListWorker.class.getName());

	private PrintStream sysErrSave;
	private JDialog dialog;


	/**
	 * Set field, save the {@link System#err stderr} and replace it with
	 * {@link LoggerOutput}
	 *
	 * @param dialog
	 */
	private ServiceListWorker(JDialog dialog) {
		this.dialog = dialog;
		sysErrSave = System.err;
		System.setErr(new PrintStream(new LoggerOutput(logger, Level.INFO)));
	}

	/**
	 * Set the {@link JDialog} visible
	 */
	public void showView() {
		dialog.setVisible(true);
	}

	@Override
	protected List<Provider> doInBackground() {
		return GetServiceList.getListVamdcProvider();
	}

	@Override
	protected void done() {
		System.setErr(sysErrSave);
		dialog.setVisible(false);
	}

	/**
	 * Use this class through this method.</br> Creates the
	 * {@link ServiceLogDialog} and the {@link ServiceListWorker},</br>then execute
	 * and return the result of the {@link ServiceListWorker}
	 *
	 * @param guiParent
	 *            ancestor of the new {@link ServiceLogDialog}
	 * @return the list of available Vamdc services
	 */
	public static List<Provider> getListOfProviderGui(Component guiParent) {
		ServiceLogDialog dialog = new ServiceLogDialog(logger, guiParent);
		ServiceListWorker worker = new ServiceListWorker(dialog);
		worker.execute();
		worker.showView();
		try {
			List<Provider> result = worker.get();
			dialog.dispose();
			return result;
		} catch (InterruptedException | ExecutionException
				| CancellationException e) {
			e.printStackTrace();
			dialog.failed();
			return null;
		}
	}
}
