/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.importation.options.file;

import javax.swing.JCheckBox;
import javax.swing.JRadioButton;

/**
 * This class holds options for File databases (catdir). It contains the methods
 * and value to identify the current sub-type and option.
 *
 * @author bpenavayre
 *
 */
public final class FileDatabaseOptions {

	/**
	 * Value for CDMS database type
	 */
	public static final int CDMS_TYPE = 1;
	/**
	 * Value for JPL database type
	 */
	public static final int JPL_TYPE = 2;
	/**
	 * Value for NIST database type
	 */
	public static final int NIST_TYPE = 3;

	/**
	 * Value for NIST database type
	 */
	public static final int CASSIS_TYPE = 4;

	/**
	 * Value to use if the Elow and Eup should be corrected
	 */
	public static final int CORRECT_ELOW = 10;
	/**
	 * Value to use if the Elow and Eup shouldn't be corrected
	 */
	public static final int DONT_CORRECT = 0;

	/**
	 * @see #toString()
	 */
	private static final String OPTIONS_SEP = ";";

	private int importOption;
	private String selectionOption;


	/**
	 * Undo a #toString of the class
	 *
	 * @param toParse
	 */
	public FileDatabaseOptions(String toParse) {
		String[] options = toParse.split(OPTIONS_SEP);
		if (options.length < 1) {
			return;
		}
		importOption = Integer.parseInt(options[0]);
		if (options.length >= 2) {
			selectionOption = options[1];
		}
	}

	/**
	 * Default constructor
	 *
	 * @param importOption
	 */
	public FileDatabaseOptions(int importOption) {
		this.importOption = importOption;
	}

	/**
	 * Set the selection option of molecules
	 *
	 * @param selectionOption
	 */
	public void setSelectionOption(String selectionOption) {
		this.selectionOption = selectionOption;
	}

	/**
	 * @param correct
	 *            if this checkbox is selected the return value will contain
	 *            {@link #CORRECT_ELOW} otherwise {@link #DONT_CORRECT}
	 * @param cdms
	 *            if selected will return a value containing C#CDMC
	 * @param jpl
	 * @param Cassis
	 * @return
	 */
	public static int getOptionFromView(JCheckBox correct, JRadioButton cdms,
			JRadioButton jpl, JRadioButton buttonCassis) {
		int option = correct.isSelected() ? CORRECT_ELOW : DONT_CORRECT;
		if (cdms.isSelected()) {
			return option + CDMS_TYPE;
		}
		if (jpl.isSelected()) {
			return option + JPL_TYPE;
		}
		if (buttonCassis.isSelected()) {
			return option + CASSIS_TYPE;
		}
		return option + NIST_TYPE;
	}

	/**
	 * This method returns the type hold in the given integer,</br> for example
	 * if this value is "{@link FileDatabaseOptions#CORRECT_ELOW CORRECT_ELOW} +
	 * {@link FileDatabaseOptions#NIST_TYPE NIST_TYPE}"</br> this would return
	 * {@link FileDatabaseOptions#NIST_TYPE NIST_TYPE}
	 *
	 * @param option
	 *            containing the type and option
	 * @return the type value contained in the option
	 */
	public int getType() {
		return importOption % CORRECT_ELOW;
	}

	/**
	 * This method determines if the integer given in parameter holds</br>
	 * {@link FileDatabaseOptions#CORRECT_ELOW CORRECT_ELOW} or not
	 *
	 * @param option
	 *            containing the type and option
	 * @return true if should correct the elow and eup using the elow_min else
	 *         false
	 */
	public boolean shouldCorrect() {
		return importOption >= CORRECT_ELOW;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(importOption);
		if (selectionOption != null) {
			builder.append(OPTIONS_SEP).append(selectionOption);
		}
		return builder.toString();
	}

	/**
	 * @return the selected molecules to import
	 */
	public String getSelectionOption() {
		return selectionOption;
	}
}
