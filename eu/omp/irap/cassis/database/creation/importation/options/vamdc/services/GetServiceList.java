/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.importation.options.vamdc.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.vamdc.registry.client.Registry;
import org.vamdc.registry.client.Registry.Service;
import org.vamdc.registry.client.RegistryCommunicationException;
import org.vamdc.registry.client.RegistryFactory;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import eu.omp.irap.cassis.database.creation.tools.log.SimpleLogger;

/**
 * Tool class that serves us to find the list of the available Vamdc provider
 *
 * @author bpenavayre
 *
 */
public final class GetServiceList {

	static final long DOWNLOAD_CHUNK_SIZE = 64 * 1024;

	/**
	 * Utility class so no constructor.
	 */
	private GetServiceList() {
	}

	/**
	 * @return the list of available Vamdc provider
	 */
	public static List<Provider> getListVamdcProvider() {
		Collection<String> ivoaiDs = getReg().getIVOAIDs(Service.VAMDC_TAP);
		List<Provider> providers = new ArrayList<Provider>();
		Set<String> invalidIvoaId = getInvalidIVoaId();
		for (String ivoaiD : ivoaiDs) {
			if (invalidIvoaId.contains(ivoaiD)) {
				continue;
			}

			final String vamdcUrl = getReg().getMirrors(ivoaiD).get(0).TAPEndpoint.toString();
			if (isCassisProvider(ivoaiD)){
				Provider provider = new Provider(ivoaiD, vamdcUrl);
				provider.setName(getReg().getResourceMetadata(ivoaiD).getTitle());
				providers.add(provider);
			}
		}
		return providers;
	}

	public static boolean isCassisProvider(String ivoaiD) {
		boolean res = false;
		try {
			Collection<String> listReturnable = getListReturnableOfService(ivoaiD);
			if ((listReturnable.contains("MoleculeSpeciesID") || listReturnable.contains("AtomSpeciesID"))
					&& (listReturnable.contains("RadTransFrequency")
							|| listReturnable.contains("RadTransWavelength")
							|| listReturnable.contains("RadTransWavenumber")))
				res = true;


		} catch (Exception e) {
			System.out.println("Cant retreive returnable for " + ivoaiD);
		}
		return res;
	}

	/**
	 * @param ivoaiD
	 * @return
	 */
	private static Collection<String> getListReturnableOfService(String ivoaiD) {
		Collection<String> listReturnable = null;
		try {
			File downloadFile = downloadTempFile(getReg().getMirrors(ivoaiD).get(0).CapabilitiesEndpoint.toString());

//			File downloadFile = downloadTempFile(getReg().getCapabilitiesURL(ivoaiD).toString());
			Collection<String> returnables1 = new HashSet<String>();
			try (FileInputStream fileInputStream = new FileInputStream(downloadFile)) {
				DocumentBuilderFactory docBuilderFact = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = docBuilderFact.newDocumentBuilder();
				Document doc = docBuilder.parse(fileInputStream);
				doc.getDocumentElement().normalize();
				NodeList nodeList = doc.getElementsByTagName("returnable");
				int nbVersion = nodeList.getLength();

				for (int cpt = 0; cpt < nbVersion; cpt++) {
					Element element = (Element) nodeList.item(cpt);
					returnables1.add(element.getFirstChild().getNodeValue());
				}
			} catch (DOMException e) {
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			Collection<String> returnables = returnables1;
			listReturnable = returnables;
		} catch (Exception e) {
			System.out.println("no returnable for " + ivoaiD);
			listReturnable = new HashSet<String>();
		}

		return listReturnable;
	}

	/**
	 * Download a file 'url' from the software site on temp directory.
	 * The file is marked to be deleted once the JVM stop.
	 */
	public static File downloadTempFile(String url) throws Exception {
		URL urlSource = new URL(url);
		// Temporary file on HDD
		File tmpFile = File.createTempFile("cassis", ".xml");
		tmpFile.deleteOnExit();

		try (ReadableByteChannel rbc = Channels.newChannel(urlSource.openStream());
				FileOutputStream fileOutputStream = new FileOutputStream(tmpFile);
				FileChannel out = fileOutputStream.getChannel()) {
			long pos = 0;
			long bytesRead;
			while (true) {
				bytesRead = out.transferFrom(rbc, pos, DOWNLOAD_CHUNK_SIZE);
				pos += bytesRead;
				if (bytesRead == 0) {
					break;
				}
			}
		}
		return tmpFile;
	}

	public static Registry getReg() {
		Registry reg = null;
		try {
			reg = RegistryFactory.getClient();
		} catch (RegistryCommunicationException e) {
			e.printStackTrace();
			SimpleLogger.getLogger(GetServiceList.class.getName());
		}
		return reg;
	}

	/**
	 * Block theses services because they don't respect common behavior
	 *
	 * @return list to discard
	 */
	public static Set<String> getInvalidIVoaId() {
		HashSet<String> invalidIvoaId = new HashSet<String>();
		// invalidIvoaId.add("ivo://vamdc/DESIRE-moscow");
		// invalidIvoaId.add("ivo://vamdc/vald-Moscow");
		// invalidIvoaId.add("ivo://vamdc/lund");
		return invalidIvoaId;
	}

	public static List<Provider> getDefaultVamdcProviderForCassis(){
		List<Provider> res = new ArrayList<Provider>();
		res.add(new Provider(
				"Stark-b",
				"http://stark-b.obspm.fr/12.07/vamdc/tap/",
				"ivo://vamdc/stark-b/tap-xsams"));
		res.add(new Provider(
				"VALD sub-set in Moscow (obs)",
				"http://vald.inasan.ru/vald-node/tap/",
				"ivo://vamdc/vald-Moscow"));
		res.add(new Provider(
				"TOPbase : VAMDC-TAP interface",
				"http://topbase.obspm.fr/12.07/vamdc/tap/",
				"ivo://vamdc/TOPbase/tap-xsams"));
		res.add(new Provider(
				"GeCaSDa: Gemane Calculated Spectroscopic Database",
				"http://vamdc.icb.cnrs.fr/gecasda/tap/",
				"ivo://vamdc/dijon-GeH4-lines"));
		res.add(new Provider(
				"Water internet Accessible Distributed Information System",
				"http://vamdc.saga.iao.ru/node/wadis/tap/",
				"ivo://vamdc/wadis/vamdc-tap"));
		res.add(new Provider(
				"TFMeCaSDa - CF4 Calculated Spectroscopic Database",
				"http://vamdc.icb.cnrs.fr/tfmecasda/tap/",
				"ivo://vamdc/dijon-CF4-lines"));
		res.add(new Provider(
				"MeCaSDa - Methane Calculated Spectroscopic Database",
				"http://vamdc.icb.cnrs.fr/mecasda-12.07/tap/",
				"ivo://vamdc/dijon-methane-lines"));
		res.add(new Provider(
				"VALD (atoms)",
				"http://vald.astro.uu.se/atoms-12.07/tap/",
				"ivo://vamdc/vald/uu/django"));
		res.add(new Provider(
				"NIST Atomic Spectra Database",
				"https://pml.nist.gov:8000/nodes/asd/tap/",
				"ivo://vamdc/nist/vamdc-tap_12.07"));
		res.add(new Provider(
				"Carbon Dioxide Spectroscopic Databank 1000K (VAMDC-TAP)",
				"http://lts.iao.ru/node/cdsd-1000-xsams1/tap/",
				"ivo://vamdc/cdsd-1000"));
		res.add(new Provider(
				"JPL database: VAMDC-TAP service",
				"https://cdms.astro.uni-koeln.de/jpl/tap/",
				"ivo://vamdc/JPLdev"));
		res.add(new Provider(
				"Spectr-W3",
				"http://spectr-w3.snz.ru/vamdc/tap/",
				"ivo://vamdc/spectr-w3"));
		res.add(new Provider(
				"CDMS",
				"https://cdms.astro.uni-koeln.de/cdms/tap/",
				"ivo://vamdc/cdms/vamdc-tap_12.07"));
		res.add(new Provider(
				"SpEctroScopy of Atoms and Molecules",
				"http://sesam.obspm.fr/12.07/vamdc/tap/",
				"ivo://vamdc/sesam/tap-xsams"));
		res.add(new Provider(
				"SHeCaSDa - SF6 Calculated Spectroscopic Database",
				"http://vamdc.icb.cnrs.fr/shecasda/tap/",
				"ivo://vamdc/dijon-SF6-lines"));
		res.add(new Provider(
				"RuCaSDa: Ruthenium tetroxide Calculated Spectroscopic Database",
				"http://vamdc.icb.cnrs.fr/rucasda/tap/",
				"ivo://vamdc/dijon-RuO4-lines"));
		return res;
	}
}
