/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.importation.options.salp.services;

import java.util.ArrayList;
import java.util.List;

/**
 * Tool class that servers us to get the list of available Urls for Slap
 *
 * @author bpenavayre
 *
 */
public final class SlapUrlList {

	/**
	 * The static list,</br>
	 * static to avoid to recreate it for every call.
	 */
	private static List<String> listService;


	private SlapUrlList() {
	}

	/**
	 * For now there's no method to get this list without doing it manually,</br>
	 * to change if possible.
	 *
	 * @return the list of urls for Slap
	 */
	public static List<String> getList() {
		if (listService == null) {
			listService = new ArrayList<String>();
			listService.add("http://esavo.esac.esa.int/slap/jsp/slapBeta.jsp?");
			listService.add("http://linelists.obspm.fr/transitions.php?base=molat&");
			listService.add("http://physics.nist.gov/cgi-bin/ASD/slap.pl?");
			listService.add("http://esavo02:8080/cieloslapToolKit/cieloslap.jsp?");
			listService.add("http://msslxv.mssl.ucl.ac.uk:8080/chianti_slap/DALToolKitServlet?");
			listService.add("http://archdev.stsci.edu/slap/search.php?");
			listService.add("http://linelists.obspm.fr/transitions.php?base=cdms_jpl_basecol&");
		}
		return listService;
	}

}
