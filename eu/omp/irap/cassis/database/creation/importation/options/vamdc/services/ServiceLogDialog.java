/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.importation.options.vamdc.services;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import eu.omp.irap.cassis.database.creation.tools.log.gui.GuiHandler;
import eu.omp.irap.cassis.database.creation.tools.log.gui.LogGui;

/**
 * Extends {@link JDialog} implements {@link ActionListener},</br>
 * A modal dialog containing a {@link JLabel} and a {@link LogGui}.
 * @author bpenavayre
 *
 */
public class ServiceLogDialog extends JDialog implements ActionListener {

	private static final long serialVersionUID = -7788786811951146687L;
	private static final int SIZE_X = 500;
	private static final String DEFAULT_TITLE = "Downloading...";
	private static final String ERROR_TITLE = "Download failed";
	private static final String CLOSE = "Close";
	private static final String INFORMATION_ICON_STRING = "OptionPane.informationIcon";
	private static final String MESSAGE = "Dowloading the list of urls, please wait.";

	private JPanel panel;
	private JLabel label;
	private BorderLayout layout;


	public ServiceLogDialog(Logger logger, Component guiParent) {
		setTitle(DEFAULT_TITLE);
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		layout = new BorderLayout(0, 5);
		panel = new JPanel(layout);
		panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 0, 0));
		Icon icon = javax.swing.UIManager.getIcon(INFORMATION_ICON_STRING);
		label = new JLabel(MESSAGE,
				icon, SwingConstants.CENTER);
		label.setFont(new Font(Font.DIALOG, Font.PLAIN, 14));
		label.setIconTextGap(20);
		panel.add(label, BorderLayout.PAGE_START);
		JScrollPane scroll = new JScrollPane(new GuiHandler(logger).getLogGui());
		scroll.setPreferredSize(new Dimension(SIZE_X, 400));
		scroll.setBorder(BorderFactory.createTitledBorder("Log"));
		panel.add(scroll, BorderLayout.CENTER);
		setContentPane(panel);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setMinimumSize(new Dimension(400, 300));
		pack();
		setLocationRelativeTo(guiParent);
	}

	/**
	 * If this method is called the top of this dialog's panel is deleted
	 * and a {@link JButton} is set at the bottom of the panel.
	 * Set to this button this class as {@link ActionListener}
	 */
	public void failed() {
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setModalityType(ModalityType.MODELESS);
		panel.remove(label);
		layout.setVgap(0);
		JButton close = new JButton(CLOSE);
		close.addActionListener(this);
		panel.add(close, BorderLayout.PAGE_END);
		setTitle(ERROR_TITLE);
		pack();
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		dispose();
	}

}
