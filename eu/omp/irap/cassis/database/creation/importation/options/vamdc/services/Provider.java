/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.importation.options.vamdc.services;

/**
 * This class contains all the useful informations on the vamdc provider
 *
 * @author bpenavayre
 *
 */
public class Provider {

	private static final String IVO_STRING = "ivo://vamdc/";

	private String name;
	private String url;
	private String ivoId= "";


	/**
	 * Constructor
	 *
	 * @param name
	 *            the name of the database mines the {@link #IVO_STRING}
	 * @param url
	 *            its url
	 */
	public Provider(String name, String url) {
		if (name.startsWith(IVO_STRING)) {
			this.name = name.substring(IVO_STRING.length());
			this.setIvoId(name);
		} else {
			this.name = name;
		}
		this.url = url;
	}

	/**
	 * Constructor.
	 *
	 * @param name Name of the database
	 * @param url URL of the database.
	 * @param ivoId The IVOID.
	 */
	public Provider(String name, String url, String ivoId) {
		this.name = name;
		this.url = url;
		this.ivoId = ivoId;
	}

	@Override
	public String toString() {
		return name;
	}

	public void setName(String shortName) {
		name = shortName;

	}

	public String getName() {
		return name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the ivoId
	 */
	public String getIvoId() {
		return ivoId;
	}

	/**
	 * @param ivoId the ivoId to set
	 */
	public void setIvoId(String ivoId) {
		this.ivoId = ivoId;
	}
}
