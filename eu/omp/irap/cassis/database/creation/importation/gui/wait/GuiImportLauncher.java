/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.importation.gui.wait;

import java.awt.Component;
import java.awt.Dialog.ModalityType;
import java.awt.Window;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.PrintStream;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

import javax.swing.SwingWorker;

import org.apache.commons.io.output.NullOutputStream;

import eu.omp.irap.cassis.database.access.TypeDataBase;
import eu.omp.irap.cassis.database.creation.importation.ImportDatabase;
import eu.omp.irap.cassis.database.creation.importation.gui.selection.SelectionWorker;
import eu.omp.irap.cassis.database.creation.tools.DatabaseContainer;
import eu.omp.irap.cassis.database.creation.tools.log.SimpleLogger;

/**
 * Extends {@link SwingWorker} and Implements {@link PropertyChangeListener}
 * .</br></br> Handles the importation of the database, finds the right
 * {@link SwingWorker} to call and handles a {@link WaitFrame}.</br> Use the
 * static method
 * {@link #launchImportWithGui(DatabaseContainer, Object, boolean)
 * launchImportWithGui} to use it.</br>
 *
 * If the {@link WaitFrame} is closed this will trigger an
 * {@link PropertyChangeEvent}</br> received by this
 * {@link #propertyChange(PropertyChangeEvent)} calling {@link #callForCancel()}
 *
 * @author bpenavayre
 *
 */
public final class GuiImportLauncher extends SwingWorker<Integer, Void>
		implements PropertyChangeListener {

	private static final Logger LOGGER = SimpleLogger
			.getLogger(GuiImportLauncher.class.getName());

	/**
	 * Modal dialog
	 */
	private WaitFrame frame;
	/**
	 * The correct {@link SwingWorker} depending of the situation
	 */
	private SwingWorker<Integer, ?> worker;
	/**
	 * Save the {@link System#err}
	 */
	private PrintStream sysErr;

	private int result;

	/**
	 * Launch the Graphical User Interface version of the import of a new</br>
	 * database by instantiate this class executing it, showing the dialog
	 * and</br> returning the result of its {@link SwingWorker#get() get} method
	 *
	 * @param newDatabase
	 *            the new {@link DatabaseContainer} to fill
	 * @param guiParent
	 *            the {@link Component} to use for
	 *            {@link Window#setLocationRelativeTo(Component)}
	 * @param allowSelection
	 *            for {@link TypeDataBase#VAMDC Vamdc} type use or not the
	 *            selection worker
	 * @return the result of the import : see {@link ImportDatabase}
	 */
	public static int launchImportWithGui(DatabaseContainer newDatabase,
			Object guiParent, boolean allowSelection) {
		GuiImportLauncher launcher = new GuiImportLauncher(newDatabase,
				(Component) guiParent, allowSelection);
		launcher.execute();
		launcher.setVisible();
		try {
			return launcher.get();
		} catch (InterruptedException | ExecutionException e) {
			LOGGER.severe(e.getMessage());
			return ImportDatabase.IMPORT_FAILED;
		} catch (CancellationException e) {
			LOGGER.severe(e.getMessage());
			return ImportDatabase.IMPORT_CANCEL;
		} finally {
			// Hugly hack to avoid memory leak.
			launcher.frame = null;
			launcher.worker = null;
			launcher = null;
		}
	}

	private GuiImportLauncher(DatabaseContainer newDatabase,
			Component guiParent, boolean allowSelection) {
		frame = new WaitFrame(newDatabase.getDatabaseName(), guiParent, this,
				LOGGER);
		if (allowSelection && !newDatabase.getType().isSlapSubType()) {
			worker = new SelectionWorker(newDatabase, frame, LOGGER);
		} else {
			worker = new DefaultWaitWorker(newDatabase, LOGGER);
		}
	}

	/**
	 * Set the Modal dialog {@link WaitFrame} to visible
	 */
	private void setVisible() {
		frame.setVisible(true);
	}

	@Override
	protected Integer doInBackground() {
		worker.execute();
		result = ImportDatabase.IMPORT_FAILED;
		try {
			result = worker.get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
			result = ImportDatabase.IMPORT_FAILED;
		} catch (CancellationException e) {
			e.printStackTrace();
			result = ImportDatabase.IMPORT_CANCEL;
		}
		return result;
	}

	/**
	 * When receiving an {@link PropertyChangeEvent} trigger cancel
	 *
	 * @see #callForCancel()
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		callForCancel();
	}

	/**
	 * Intercept the {@link System#err} replace it by {@link NullOutputStream}
	 * </br> and call for cancel to avoid false errors being displayed
	 */
	private void callForCancel() {
		sysErr = System.err;
		sysErr.flush();
		System.setErr(new PrintStream(new NullOutputStream()));
		worker.cancel(true);
	}

	@Override
	protected void done() {
		frame.setVisible(false);
		frame.setModalityType(ModalityType.MODELESS);
		if (result == ImportDatabase.IMPORT_CANCEL) {
			System.err.flush();
			if (sysErr != null) {
				System.setErr(sysErr);
			}
		}
		if (result == ImportDatabase.IMPORT_FAILED
				|| result == ImportDatabase.IMPORT_OK_WIH_ERROR) {
			frame.setToError();
			frame.setVisible(true);
		} else {
			frame.dispose();
		}
	}
}
