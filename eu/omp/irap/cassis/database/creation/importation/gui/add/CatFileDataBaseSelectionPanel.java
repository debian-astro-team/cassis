/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.importation.gui.add;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.database.FileDatabaseUtils;
import eu.omp.irap.cassis.database.access.TypeDataBase;
import eu.omp.irap.cassis.database.configuration.DatabaseConfiguration;
import eu.omp.irap.cassis.database.creation.importation.options.file.FileDatabaseOptions;
import eu.omp.irap.cassis.database.creation.tools.DatabaseContainer;

@SuppressWarnings("serial")
public class CatFileDataBaseSelectionPanel extends SelectionPanel implements
		SelectionPanelInterface {

	private JRadioButton buttonCdms;
	private JRadioButton buttonJpl;
	private JRadioButton buttonNist;
	private JRadioButton buttonCassis;
	private JCheckBox correctElowCheckBox;


	public CatFileDataBaseSelectionPanel() {
		super();

		optionPanel.setBorder(BorderFactory.createTitledBorder("Options"));
		optionPanel.setLayout(new BorderLayout(0, 5));
		optionPanel.add(getCorrectElowCheckBox(), BorderLayout.NORTH);
		JPanel formatPanel = new JPanel(new GridLayout(4, 1));
		formatPanel.setBorder(BorderFactory.createTitledBorder("Format"));
		ButtonGroup buttonGroup = new ButtonGroup();
		buttonGroup.add(getButtonJpl());
		buttonGroup.add(getButtonCdms());
		buttonGroup.add(getButtonNist());
		buttonGroup.add(getButtonCassis());

		formatPanel.add(getButtonJpl());
		formatPanel.add(getButtonCdms());
		formatPanel.add(getButtonNist());
		formatPanel.add(getButtonCassis());
		optionPanel.add(formatPanel, BorderLayout.CENTER);
		selectButton.setText("Select folder");
		selectButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				chooseFile();
			}
		});
		ressourceLabel.setText("Path");
	}

	public JCheckBox getCorrectElowCheckBox() {
		if (correctElowCheckBox == null) {
			correctElowCheckBox = new JCheckBox("Correct elow with the min of elow");
		}
		return correctElowCheckBox;
	}

	public JRadioButton getButtonNist() {
		if (buttonNist == null) {
			buttonNist = new JRadioButton("NIST");
		}
		return buttonNist;
	}

	public JRadioButton getButtonCassis() {
		if (buttonCassis == null) {
			buttonCassis = new JRadioButton("CASSIS");
		}
		return buttonCassis;
	}

	public JRadioButton getButtonCdms() {
		if (buttonCdms == null) {
			buttonCdms = new JRadioButton("CDMS");
			buttonCdms.setSelected(true);
		}
		return buttonCdms;
	}

	public JRadioButton getButtonJpl() {
		if (buttonJpl == null) {
			buttonJpl = new JRadioButton("JPL");
		}
		return buttonJpl;
	}

	protected File chooseFile() {
		JFileChooser fc = new CassisJFileChooser(
				DatabaseConfiguration.getInstance().getDatabasePath(),
				DatabaseConfiguration.getInstance().getLastFolder("catfile-database"));
		fc.setMultiSelectionEnabled(false);
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int resultOfChoosing = fc.showOpenDialog(this);
		if (resultOfChoosing == JFileChooser.ERROR_OPTION) {
			JOptionPane.showMessageDialog(this, SelectionPanel.JFILECHOOSER_ERROR);
		}

		if (resultOfChoosing == JFileChooser.APPROVE_OPTION) {
			File result = fc.getSelectedFile();
			String pathFolder = result.getPath();
			selectedRessourceTextField.setText(pathFolder);
			nameTextField.setText(result.getName());

			if (FileDatabaseUtils.isJpl(pathFolder)) {
				getButtonJpl().setSelected(true);
			} else if (FileDatabaseUtils.isCdms(pathFolder)) {
				getButtonCdms().setSelected(true);
			} else if (FileDatabaseUtils.isNist(pathFolder)) {
				getButtonNist().setSelected(true);
			} else if (FileDatabaseUtils.isCassis(pathFolder)) {
				getButtonCassis().setSelected(true);
			} else {
				JOptionPane.showMessageDialog(this, "Unknow format");
			}
			DatabaseConfiguration.getInstance().setLastFolder(
					"catfile-database", fc.getSelectedFile().getAbsolutePath());
		}

		return fc.getCurrentDirectory();
	}

	@Override
	public DatabaseContainer getData() {
		if (selectedRessourceTextField.getText().isEmpty()) {
			return null;
		}
		int option = FileDatabaseOptions.getOptionFromView(correctElowCheckBox, buttonCdms,
				buttonJpl, buttonCassis);
		return new DatabaseContainer(nameTextField.getText(), selectedRessourceTextField.getText(),
				TypeDataBase.FILE, new FileDatabaseOptions(option));
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame("Cat File DataBase Selection");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setContentPane(new CatFileDataBaseSelectionPanel());
		frame.pack();
	}

}
