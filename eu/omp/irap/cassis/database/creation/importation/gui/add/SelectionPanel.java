/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.importation.gui.add;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import eu.omp.irap.cassis.database.creation.tools.DatabaseContainer;
import eu.omp.irap.cassis.database.creation.tools.MouseListenerForTextFields;

/**
 * This class extends a JPanel, its role is to be able to</br> change certain of
 * its components. For instance : change the scrollViewComp </br> (the central
 * Component of this panel),</br> edit the text of a Label, etc...
 *
 * The editors are all implementing {@link GeneralEditSelectionPanel}</br> they
 * can be found in the package editPanel
 *
 * @author bpenavayre
 *
 */
public abstract class SelectionPanel extends JPanel implements SelectionPanelInterface {

	private static final long serialVersionUID = 4736871862342624219L;
	protected JTextField selectedRessourceTextField;
	protected JButton selectButton;
	protected JLabel ressourceLabel;
	protected GridBagConstraints c;

	protected JPanel detailsPanel;
	protected JTextField nameTextField;

	protected JPanel optionPanel;

	public static final String JFILECHOOSER_ERROR = "An error occured!";

	/**
	 * The constructor,</br> set every thing to its default value
	 *
	 * @param selectedRessourceTextField
	 * @param scrollViewComp
	 * @param ressourceLabel
	 * @param buttonTitle
	 */
	public SelectionPanel() {
		setLayout(new GridBagLayout());
		setBorder(new TitledBorder("Selection"));
		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.fill = GridBagConstraints.BOTH;
		c.anchor = GridBagConstraints.BASELINE_LEADING;
		c.insets.left = 10;
		c.insets.right = 10;
		c.insets.top = 5;
		c.insets.bottom = 5;
		this.ressourceLabel = new JLabel("Label");
		add(this.ressourceLabel, c);

		c.gridx++;
		c.weightx = 1;
		c.anchor = GridBagConstraints.BASELINE;
		c.insets.left = 0;

		add(getSelectedRessourceTextField(), c);

		c.gridx++;
		c.weightx = 0;
		c.insets.right = 0;

		add(getSelectButton(), c);

		c.gridy++;
		c.gridx = 0;
		c.weighty = 1;
		c.weightx = 1;
		c.gridwidth = 0;
		c.insets.bottom = 0;
		c.gridy++;
		c.gridx = 0;
		c.weighty = 1;
		c.weightx = 1;
		c.gridwidth = 0;
		c.insets.bottom = 0;
		add(getDetailsPanel(), c);
	}

	private JPanel getDetailsPanel() {
		if (detailsPanel == null) {
			detailsPanel = new JPanel(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = 0;
			c.fill = GridBagConstraints.BOTH;
			c.anchor = GridBagConstraints.BASELINE_LEADING;
			c.insets.left = 10;
			c.insets.right = 5;
			c.insets.top = 10;
			c.insets.bottom = 5;
			JLabel nameLabel = new JLabel("Name");
			detailsPanel.add(nameLabel, c);

			c.gridx++;
			c.weightx = 1;
			c.insets.left = 0;
			detailsPanel.add(getNameTextField(), c);

			c.gridy++;
			c.gridx = 0;
			c.weighty = 1;
			c.weightx = 1;
			c.gridwidth = 0;
			c.insets.left = 5;
			c.insets.top = 0;
			detailsPanel.add(getOptionPanel(), c);
		}
		return detailsPanel;
	}

	protected JPanel getOptionPanel() {
		if (optionPanel == null){
			optionPanel = new JPanel();
		}
		return optionPanel;
	}

	/**
	 * @return
	 */
	public JTextField getNameTextField() {
		if (nameTextField == null) {
			nameTextField = new JTextField();
		}
		return nameTextField;
	}

	public JTextField getSelectedRessourceTextField() {
		if (selectedRessourceTextField == null) {
			selectedRessourceTextField = new JTextField(8);
			selectedRessourceTextField.addMouseListener(new MouseListenerForTextFields(
					selectedRessourceTextField.getActionMap()));
		}
		return selectedRessourceTextField;
	}

	/**
	 * @return the button
	 */
	public JButton getSelectButton() {
		if (selectButton == null) {
			selectButton = new JButton("select button");
		}
		return selectButton;
	}

	@SuppressWarnings("serial")
	public static void main(String[] args) {
		JFrame frame = new JFrame("Selection Panel");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setContentPane(new SelectionPanel() {

		});
		frame.pack();
	}

	@Override
	public DatabaseContainer getData() {
		return null;
	}
}
