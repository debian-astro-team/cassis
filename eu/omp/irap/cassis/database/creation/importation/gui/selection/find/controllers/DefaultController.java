/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.importation.gui.selection.find.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import eu.omp.irap.cassis.database.creation.importation.gui.selection.SelectionTableModel;

/**
 * Abstract class,
 * subclass filters the {@link SelectionTableModel} with value contained in a List<'T'>
 *
 * @author bpenavayre
 *
 * @param <T> type of the data to handle
 */
public abstract class DefaultController<T> {

	public static final int DONT_CORRECT = 0;
	public static final int DO_CORRECT = 1;
	public static final int STOP = 2;

	/**
	 * Parse the rows of the {@link SelectionTableModel}</br>
	 * and either delete, continue or stop according the return value</br>
	 * of the abstract method {@link #correctOrNot(List)}
	 *
	 * @param model
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void editSelection(SelectionTableModel model) {
		Iterator<Vector> iter = model.getDataVector().iterator();
		List<T> row;
		List<Integer> listOfRowsToDelete = new ArrayList<Integer>();
		int index = 0;
		while (iter.hasNext()) {
			row = iter.next();
			switch (correctOrNot(row)) {
			case DO_CORRECT:
				listOfRowsToDelete.add(index);
				break;
			case STOP:
				return;
			}
			index++;
		}
		int diff = 0;
		for (int i : listOfRowsToDelete) {
			model.removeRow(i - diff++);
		}
		clear();
	}

	/**
	 * Use the row and return a result :</br>
	 * 	-{@link #DONT_CORRECT} is nothing to be done</br>
	 *  -{@link #DO_CORRECT} if the row should be deleted</br>
	 *  -{@link #STOP} if should stop parsing
	 * @param row
	 * @return
	 */
	protected abstract int correctOrNot(List<T> row);

	/**
	 * Clear Object containing options
	 */
	public abstract void clear();

}
