/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.importation.gui.add;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import eu.omp.irap.cassis.database.access.TypeDataBase;
import eu.omp.irap.cassis.database.creation.tools.DatabaseContainer;

/**
 * This class is the view of the interface of database importation
 *
 * @author bpenavayre
 */
public class AddDatabasePanel extends JPanel implements ActionListener {

	private static final long serialVersionUID = 8918323306067943234L;

	private static final String EMPTY_MESSAGE = "<html>No database added.<br>Please add at least one database.</html>";
	private static final String EMPTY_TITLE = "No database";

	private static final int WIN_X = 500;
	private static final int WIN_Y = 400;

	private AddDatabaseControl control;
	private JComboBox<TypeDataBase> comboBoxTypeDataBase;
	private JPanel selectionDecorPanel;
	private SelectionPanel selectionPanel;
	private JButton validate;
	private CatFileDataBaseSelectionPanel catFileDataBaseSelectionPanel;
	private JPanel bottomPanel;
	private SelectionPanel emptySelectionPanel;
	private SqliteDataBaseSelectionPanel sqliteDataBaseSelectionPanel;
	private VamdcDataBaseSelectionPanel vamdcDataBaseSelectionPanel;
	private SlapDataBaseSelectionPanel slapDataBaseSelectionPanel;
	private SlapFileDataBaseSelectionPanel slapFileDataBaseSelectionPanel;
	private VamdcFileDataBaseSelectionPanel vamdcFileDataBaseSelectionPanel;


	/**
	 * The constructor,</br> set everything to its default value
	 *
	 * @see #createAndReturnPanel()
	 *
	 * @param control
	 *            the controller
	 * @param mainView
	 *            this project's main panel
	 */
	public AddDatabasePanel(AddDatabaseControl control) {
		super(new BorderLayout());
		this.control = control;

		setPreferredSize(new Dimension(WIN_X, WIN_Y));
		add(createTop(), BorderLayout.PAGE_START);

		add(getSelectionDecorPanel(), BorderLayout.CENTER);
		add(createBottom(), BorderLayout.PAGE_END);

		changeSelectionPanel();
	}

	private void changeSelectionPanel() {
		TypeDataBase typeDatabase = getComboBoxTypeDataBase().
				getItemAt(getComboBoxTypeDataBase().getSelectedIndex());
		switch (typeDatabase) {
			case FILE:
				setFileSelectionPanel();
				break;
			case SQLITE:
				setSqliteSelectionPanel();
				break;
			case VAMDC:
				setVamdcSelectionPanel();
				break;
			case VAMDC_FILE:
				setVamdcFileSelectionPanel();
				break;
			case SLAP:
				setSlapSelectionPanel();
				break;
			case SLAP_FILE:
				setSlapFileSelectionPanel();
				break;
			default :
				setVisible(emptySelectionPanel);
		}
		selectionDecorPanel.repaint();
	}

	private void setSlapFileSelectionPanel() {
		if (slapFileDataBaseSelectionPanel == null) {
			slapFileDataBaseSelectionPanel = new SlapFileDataBaseSelectionPanel();
			selectionDecorPanel.add(slapFileDataBaseSelectionPanel, BorderLayout.CENTER);
		}
		setVisible(slapFileDataBaseSelectionPanel);
	}

	private void setSlapSelectionPanel() {
		if (slapDataBaseSelectionPanel == null) {
			slapDataBaseSelectionPanel = new SlapDataBaseSelectionPanel();
			selectionDecorPanel.add(slapDataBaseSelectionPanel, BorderLayout.CENTER);
		}
		setVisible(slapDataBaseSelectionPanel);
	}

	private void setVamdcFileSelectionPanel() {
		if (vamdcFileDataBaseSelectionPanel == null) {
			vamdcFileDataBaseSelectionPanel = new VamdcFileDataBaseSelectionPanel();
			selectionDecorPanel.add(vamdcFileDataBaseSelectionPanel, BorderLayout.CENTER);
		}
		setVisible(vamdcFileDataBaseSelectionPanel);
	}

	private void setVamdcSelectionPanel() {
		if (vamdcDataBaseSelectionPanel == null) {
			vamdcDataBaseSelectionPanel = new VamdcDataBaseSelectionPanel();
			selectionDecorPanel.add(vamdcDataBaseSelectionPanel, BorderLayout.CENTER);
		}
		setVisible(vamdcDataBaseSelectionPanel);
	}

	private void setSqliteSelectionPanel() {
		if (sqliteDataBaseSelectionPanel == null) {
			sqliteDataBaseSelectionPanel = new SqliteDataBaseSelectionPanel();
			selectionDecorPanel.add(sqliteDataBaseSelectionPanel, BorderLayout.CENTER);
		}
		setVisible(sqliteDataBaseSelectionPanel);
	}

	private void setFileSelectionPanel() {
		if (catFileDataBaseSelectionPanel == null) {
			catFileDataBaseSelectionPanel = new CatFileDataBaseSelectionPanel();
			selectionDecorPanel.add(catFileDataBaseSelectionPanel, BorderLayout.CENTER);
		}
		setVisible(catFileDataBaseSelectionPanel);
	}

	private void setVisible(SelectionPanel selectionPanelInterface) {
		selectionPanel = selectionPanelInterface;
		if (catFileDataBaseSelectionPanel != null){
			catFileDataBaseSelectionPanel.setVisible(false);
		}
		if (emptySelectionPanel != null){
			emptySelectionPanel.setVisible(false);
		}
		if (sqliteDataBaseSelectionPanel != null){
			sqliteDataBaseSelectionPanel.setVisible(false);
		}
		if (vamdcDataBaseSelectionPanel != null){
			vamdcDataBaseSelectionPanel.setVisible(false);
		}
		if (vamdcFileDataBaseSelectionPanel != null){
			vamdcFileDataBaseSelectionPanel.setVisible(false);
		}
		if (slapDataBaseSelectionPanel != null){
			slapDataBaseSelectionPanel.setVisible(false);
		}
		if (slapFileDataBaseSelectionPanel != null){
			slapFileDataBaseSelectionPanel.setVisible(false);
		}

		selectionPanelInterface.setVisible(true);
	}

	/**
	 * @return the current type of database
	 */
	public TypeDataBase getTypeDatabase() {
		return (TypeDataBase) comboBoxTypeDataBase.getSelectedItem();
	}

	private JPanel createTop() {
		JPanel top = new JPanel(new FlowLayout(FlowLayout.LEADING, 20, 0));
		top.setBorder(new TitledBorder("Type"));
		top.setPreferredSize(new Dimension(WIN_X, 50));
		top.add(getComboBoxTypeDataBase());
		return top;
	}

	@SuppressWarnings("serial")
	public JPanel getSelectionDecorPanel() {
		if (selectionDecorPanel == null) {
			selectionDecorPanel = new JPanel(new BorderLayout());
			emptySelectionPanel = new SelectionPanel(){

			};
			selectionDecorPanel.add(emptySelectionPanel,BorderLayout.CENTER);
		}
		return selectionDecorPanel;
	}

	private JPanel createBottom() {
		if(bottomPanel == null){
			bottomPanel = new JPanel(new FlowLayout(FlowLayout.TRAILING, 10, 10));
			JButton validate = getValidateButton();
			bottomPanel.setPreferredSize(new Dimension(WIN_X, 40));
			bottomPanel.add(validate, BorderLayout.LINE_END);
		}
		return bottomPanel;
	}

	public JButton getValidateButton() {
		if (validate == null) {
			validate = new JButton("Validate");
			validate.addActionListener(control);
		}
		return validate;
	}

	public  JComboBox<TypeDataBase> getComboBoxTypeDataBase() {
		if (comboBoxTypeDataBase == null) {
			TypeDataBase[] typeDataBaseArray = Arrays.copyOf(TypeDataBase.values(),
					TypeDataBase.values().length - 1);
			comboBoxTypeDataBase = new JComboBox<TypeDataBase>(typeDataBaseArray);
			comboBoxTypeDataBase.addActionListener(this);
			comboBoxTypeDataBase.setPreferredSize(new Dimension(150, 20));
		}
		return comboBoxTypeDataBase;
	}

	/**
	 * Listen to {@link ActionEvent} fired by {@link JComboBox} {@link #comboBoxTypeDataBase}
	 * and change the current panel editor accordingly
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		changeSelectionPanel();
	}

	public SelectionPanelInterface getEditSelectionPanel() {
		return selectionPanel;
	}

	public SelectionPanel getSelectionPanel() {
		return selectionPanel;
	}

	/**
	 * Get the new database and set its type using the {@link JComboBox}
	 *
	 * @return the return of {@link GeneralEditSelectionPanel#getData() getData}
	 */
	public DatabaseContainer getData() {
		DatabaseContainer newDatabase = selectionPanel.getData();
		if (newDatabase != null) {
			newDatabase.setType((TypeDataBase) getComboBoxTypeDataBase().getSelectedItem());
		}
		return newDatabase;
	}

	/**
	 * Show through {@link JOptionPane} a confirm dialog displaying an error
	 */
	public void showErrorMessage() {
		JOptionPane.showConfirmDialog(this, EMPTY_MESSAGE, EMPTY_TITLE,
				JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
	}
}
