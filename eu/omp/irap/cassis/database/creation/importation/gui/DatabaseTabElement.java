/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.importation.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import eu.omp.irap.cassis.common.events.ListenerManager;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB;
import eu.omp.irap.cassis.database.creation.DbCreationModel;
import eu.omp.irap.cassis.database.creation.importation.SelectionEvent;
import eu.omp.irap.cassis.database.creation.tools.ButtonTabComponent;
import eu.omp.irap.cassis.database.creation.tools.DatabaseContainer;
import eu.omp.irap.cassis.database.creation.tools.TableDataModel;

public class DatabaseTabElement extends ListenerManager {

	public static final String CHANGE_TABLE_SELECTION_STATE = "changeTableSelectionState";
	public static final String CHANGE_FUNCTION_PART_INTERFACE = "changeFunctionPartInterface";
	public static final String CHANGE_STATE_MOLECULE = "changeStateMolecule";
	private ButtonTabComponent buttonTab;
	private JTable table;
	private DatabaseContainer dbContainer;
	private JScrollPane scrollPane;
	private MouseAdapter mouseAdapter;


	public DatabaseTabElement(DatabaseContainer dbContainer) {
		this.dbContainer = dbContainer;
	}

	public ButtonTabComponent getButtonTab() {
		if (buttonTab == null) {
			buttonTab = new ButtonTabComponent(dbContainer.getDatabaseName());
			buttonTab.setEnableState(dbContainer.isEnabled());
		}
		return buttonTab;
	}

	public JTable getTable() {
		if (table == null) {
			@SuppressWarnings("serial")
			TableDataModel model = new TableDataModel(dbContainer) {
				@Override
				public void setValueAt(Object value, int row, int col) {
					if (col != TableDataModel.INDICE_SELECTION) {
						return;
					}

					fireDataChanged(new ModelChangedEvent(CHANGE_STATE_MOLECULE,
							new SelectionEvent(dbContainer,
									dbContainer.getMolecules().get(row).getTag(),
									(boolean) value)));
				}
			};
			table = new JTable(model);
			table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
			table.setRowSelectionAllowed(true);
			table.setColumnSelectionAllowed(false);
			TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(
					table.getModel());
			sorter.setSortable(table.getColumnCount() - 1, false);
			table.setRowSorter(sorter);
			table.setOpaque(true);

			ToolTipManager toolTips = ToolTipManager.sharedInstance();
			toolTips.registerComponent(table);

			DefaultTableCellRenderer render = new DefaultTableCellRenderer();
			render.setHorizontalAlignment(SwingConstants.CENTER);
			for (int i = 0; i < table.getColumnCount() - 1; i++) {
				table.getColumnModel().getColumn(i).setCellRenderer(render);
			}
			table.setFillsViewportHeight(true);
			table.getTableHeader().addMouseListener(getMouseAdapter());
			table.addMouseMotionListener(getMouseAdapter());
			table.addMouseListener(getMouseAdapter());
			if (!dbContainer.isEnabled()) {
				render.setForeground(Color.LIGHT_GRAY);
			}
		}
		return table;
	}

	private MouseAdapter getMouseAdapter() {
		if (mouseAdapter == null) {
			mouseAdapter = new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					DatabaseTabElement.this.mouseClicked(e);
				}
				@Override
				public void mouseMoved(MouseEvent e) {
					DatabaseTabElement.this.mouseMoved(e);
				}
			};
		}
		return mouseAdapter;
	}

	public JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane(getTable());
			if (!dbContainer.isEnabled()) {
				changeStateRecursively(scrollPane, dbContainer.isEnabled());
			}
		}
		return scrollPane;
	}

	/**
	 * Take a {@link Component} and the new state to be set and</br> change all
	 * it's components' state recursively with the new one.
	 *
	 * @param comp
	 * @param newEnableState
	 */
	private void changeStateRecursively(Component comp, boolean newEnableState) {
		if (comp instanceof Container) {
			Component[] comps = ((Container) comp).getComponents();
			for (Component childComponent : comps) {
				childComponent.setEnabled(newEnableState);
				changeStateRecursively(childComponent, newEnableState);
			}
		}
	}

	public DatabaseContainer getDatabaseContainer() {
		return dbContainer;
	}

	/**
	 * Mouse listener for {@link #table} and {@link #table}'s column's headers
	 */
	private void mouseClicked(MouseEvent e) {
		if (e.getSource() == table) {
			int row = table.rowAtPoint(e.getPoint());
			if (row < 0) {
				return;
			}
			row = table.convertRowIndexToModel(row);
			if (SwingUtilities.isRightMouseButton(e)) {
				changeFunctionPart(row);
			}
		} else if (table.getColumnModel().getColumnIndexAtX(e.getX()) == table
				.getColumnCount() - 1) {
			fireDataChanged(new ModelChangedEvent(CHANGE_TABLE_SELECTION_STATE, getTable(), dbContainer));
		}
	}

	private void mouseMoved(MouseEvent e) {
		Point p = e.getPoint();
		if (!table.getBounds().contains(p)) {
			return;
		}
		int row = table.rowAtPoint(p);
		if (row < 0) {
			return;
		}
		row = table.convertRowIndexToModel(table.rowAtPoint(p));
		int col = table.columnAtPoint(p);
		col = table.convertColumnIndexToModel(col);
		String result = null;
		if (col > 1 && col < 8) {
			result = table.getValueAt(row, col).toString();
		}
		table.putClientProperty(JTable.TOOL_TIP_TEXT_KEY, result);
		ToolTipManager.sharedInstance().mouseMoved(e);
	}

	/**
	 * By taking the index of the database and the index of the molecule in the
	 * database we launch the interface allowing us to alter the partition
	 * function's data
	 *
	 * @param databaseIndex
	 * @param indexInTable
	 */
	private void changeFunctionPart(int indexInTable) {
		int index = 0;
		for (SimpleMoleculeDescriptionDB molecule : dbContainer.getMolecules()) {
			int indexOfMolecule = dbContainer.getMolecules().indexOf(molecule);
			if (index == indexInTable) {
				fireDataChanged(new ModelChangedEvent(CHANGE_FUNCTION_PART_INTERFACE, table, molecule));
				break;
			}
			if (dbContainer.getMoleculeState(indexOfMolecule)) {
				index++;
			}
		}
	}

	public void addListeners(final DbCreationModel model) {
		getButtonTab().getEnableCheckBox().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				model.changeStateDatabase(dbContainer,
						getButtonTab().getEnableCheckBox().isSelected());
			}
		});
		getButtonTab().getButtonDelete().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				model.removeDatabase(dbContainer);
			}
		});
	}

	public void removeListenersAndReferences() {
		removeAllListener();
		AbstractButton component = buttonTab.getEnableCheckBox();
		for (ActionListener al : component.getActionListeners()) {
			component.removeActionListener(al);
		}
		AbstractButton component1 = buttonTab.getButtonDelete();
		for (ActionListener al : component1.getActionListeners()) {
			component1.removeActionListener(al);
		}
		table.getTableHeader().removeMouseListener(getMouseAdapter());
		table.removeMouseMotionListener(getMouseAdapter());
		table.removeMouseListener(getMouseAdapter());
		mouseAdapter = null;
		((TableDataModel)table.getModel()).removeDatabaseReference();
		table = null;
		dbContainer = null;
	}

	public void applyChangeEnableState(boolean state) {
		table.getSelectionModel().clearSelection();
		for (int i = 0; i < table.getColumnCount() - 1; i++) {
			DefaultTableCellRenderer render = (DefaultTableCellRenderer) table
					.getColumnModel().getColumn(i).getCellRenderer();
			if (!state) {
				render.setForeground(Color.LIGHT_GRAY);
			} else {
				render.setForeground(Color.BLACK);
			}
			table.getColumnModel().getColumn(i).setCellRenderer(render);
		}
		changeStateRecursively(getScrollPane(), state);
	}
}
