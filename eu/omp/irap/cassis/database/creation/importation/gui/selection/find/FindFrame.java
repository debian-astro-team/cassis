/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.importation.gui.selection.find;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import eu.omp.irap.cassis.database.creation.importation.gui.selection.SelectionController;
import eu.omp.irap.cassis.database.creation.importation.gui.selection.SelectionTableModel;
import eu.omp.irap.cassis.database.creation.importation.gui.selection.find.controllers.NameController;
import eu.omp.irap.cassis.database.creation.importation.gui.selection.find.controllers.ShowOnlyController;
import eu.omp.irap.cassis.database.creation.importation.gui.selection.find.controllers.TagController;

/**
 * View responsible to edit the {@link SelectionTableModel}
 *
 * @author bpenavayre
 *
 */
public class FindFrame extends JDialog {

	private static final long serialVersionUID = 9009201903098702322L;

	private FindController control;
	private JButton clear;
	private JButton validate;
	private JButton unHide;


	/**
	 * Constructor,</br>
	 * set constructor and options.
	 *
	 * @param parentControl
	 * @param guiParent
	 */
	public FindFrame(SelectionController parentControl, JDialog guiParent) {
		super(guiParent, "Find molecules and hide the rest");
		control = new FindController(this, parentControl);
		setPreferredSize(new Dimension(450, 230));
		setMinimumSize(new Dimension(400, 230));
		setContentPane(createAndReturnPanel());
		setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
		pack();
		setLocationRelativeTo(guiParent);
		setVisible(true);
	}

	private JPanel createAndReturnPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(createAndReturnCenter(), BorderLayout.CENTER);
		panel.add(createAndReturnBottom(), BorderLayout.PAGE_END);
		return panel;
	}

	private JPanel createAndReturnCenter() {
		JPanel panel = new JPanel(new BorderLayout());
		panel.setBorder(BorderFactory.createTitledBorder("Find"));
		panel.add(createFirstLine(), BorderLayout.CENTER);
		panel.add(createAndReturnSecondLine(), BorderLayout.PAGE_END);
		return panel;
	}

	private JPanel createFirstLine() {
		JPanel panel = new JPanel(new GridBagLayout());

		JTextField text = new JTextField(8);
		control.setMouseListener(text);
		NameController nameController = new NameController(text, 1);
		control.addController(nameController);

		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.insets.left = 5;
		panel.add(new JLabel("All names that"), c);
		c.insets.right = 5;
		panel.add(createAndReturnRadioPanelComp(nameController), c);
		c.weightx = 1;
		c.weighty = 1;
		c.gridheight = 1;
		c.ipady = 10;
		c.insets.right = 0;
		c.fill = GridBagConstraints.HORIZONTAL;
		panel.add(text, c);
		return panel;
	}

	private JPanel createAndReturnRadioPanelComp(ActionListener listener) {
		JPanel panel = new JPanel(new GridLayout(3, 1));
		ButtonGroup group = new ButtonGroup();
		JRadioButton starts = new JRadioButton("starts with");
		starts.addActionListener(listener);
		starts.setSelected(true);
		JRadioButton contains = new JRadioButton("contains");
		contains.addActionListener(listener);
		JRadioButton ends = new JRadioButton("ends with");
		ends.addActionListener(listener);
		group.add(starts);
		group.add(contains);
		group.add(ends);
		panel.add(starts);
		panel.add(contains);
		panel.add(ends);
		return panel;
	}

	private JPanel createAndReturnSecondLine() {
		JPanel panel = new JPanel(new FlowLayout());
		panel.add(new JLabel("Select all tags from"));
		JTextField from = new JTextField(8);
		control.setMouseListener(from);
		panel.add(from);
		panel.add(new JLabel("to"));
		JTextField to = new JTextField(8);
		control.setMouseListener(to);
		control.addController(new TagController(from, to, 0));
		panel.add(to);
		return panel;
	}

	private JPanel createAndReturnBottom() {
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(createAndReturnShowPanel(), BorderLayout.CENTER);
		panel.add(createAndReturnButtonPanel(), BorderLayout.PAGE_END);
		return panel;
	}

	private JPanel createAndReturnShowPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		panel.setBorder(BorderFactory.createTitledBorder("Show only"));
		ButtonGroup group = new ButtonGroup();
		JRadioButton checked = new JRadioButton("Selected molecules");
		group.add(checked);
		JRadioButton unchecked = new JRadioButton("The remaining ones");
		group.add(unchecked);
		ShowOnlyController showOnly = new ShowOnlyController(checked,
				unchecked, group, 2);
		control.addController(showOnly);
		panel.add(checked, BorderLayout.LINE_START);
		panel.add(unchecked, BorderLayout.LINE_END);
		return panel;
	}

	private JPanel createAndReturnButtonPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(1, 2, 1, 1));
		JPanel lineStartPanel = new JPanel(new BorderLayout(3, 0));
		clear = new JButton("clear");
		clear.addActionListener(control);
		lineStartPanel.add(clear, BorderLayout.LINE_START);
		unHide = new JButton("Un-hide all");
		unHide.addActionListener(control);
		lineStartPanel.add(unHide, BorderLayout.LINE_END);
		panel.add(lineStartPanel, BorderLayout.LINE_START);
		validate = new JButton("Validate");
		validate.addActionListener(control);
		panel.add(validate, BorderLayout.LINE_END);
		return panel;
	}

	/**
	 * @return the button {@link #clear}
	 */
	public JButton getClearButton() {
		return clear;
	}

	/**
	 * @return the button {@link #validate}
	 */
	public JButton getValidateButton() {
		return validate;
	}

	/**
	 * @return the button {@link #unHide}
	 */
	public Object getUnHideButton() {
		return unHide;
	}
}
