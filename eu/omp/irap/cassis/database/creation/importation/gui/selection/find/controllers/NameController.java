/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.importation.gui.selection.find.controllers;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

/**
 * Filter on the name of the molecules
 *
 * @author bpenavayre
 *
 */
public class NameController extends DefaultController<String> implements
		ActionListener {

	private static final int INDEX_START_WITH = 0;
	private static final int INDEX_CONTAINS = 1;

	private int typeOfComp;
	private int columnIndex;
	private JTextField textField;


	/**
	 * Constructor
	 *
	 * @param textField
	 * @param column
	 */
	public NameController(JTextField textField, int column) {
		this.textField = textField;
		columnIndex = column;
		typeOfComp = 0;
	}

	/**
	 * find view's index for active {@link ButtonGroup} and change {@link #typeOfComp}
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		getIndexOfRadioButton((JRadioButton) e.getSource());
	}

	private void getIndexOfRadioButton(JRadioButton button) {
		Container parent = button.getParent();
		int i = 0;
		for (Component comp : parent.getComponents()) {
			if (comp == button) {
				typeOfComp = i;
				return;
			}
			i++;
		}
	}

	@Override
	protected int correctOrNot(List<String> row) {
		String name = row.get(columnIndex);
		String text = textField.getText();
		if (text.isEmpty()) {
			return DONT_CORRECT;
		}
		if (typeOfComp == INDEX_START_WITH) {
			return name.startsWith(text) ? DONT_CORRECT : DO_CORRECT;
		}
		if (typeOfComp == INDEX_CONTAINS) {
			return name.contains(text) ? DONT_CORRECT : DO_CORRECT;
		}
		return name.endsWith(text) ? DONT_CORRECT : DO_CORRECT;
	}

	@Override
	public void clear() {
		textField.setText(null);
	}

}
