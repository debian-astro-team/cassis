/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.importation.gui.selection;

import java.util.List;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB;
import eu.omp.irap.cassis.database.creation.tools.FullMolecule;

/**
 * {@link JTable} model for {@link SelectionView}
 *
 * @author bpenavayre
 *
 */
public class SelectionTableModel extends DefaultTableModel {

	private static final long serialVersionUID = 3062078526765990395L;
	private static final String[] COLUMNS = { "Tag", "Species Name", "Import" };
	private static final int INDEX_COLUMN_TAG = 0;
	private static final int INDEX_COLUMN_NAME = 1;
	public static final int INDEX_COLUMN_IMPORT = 2;


	/**
	 * Create empty model with only the column identifiers
	 */
	public SelectionTableModel() {
		setColumnIdentifiers(COLUMNS);
	}

	/**
	 * Fill the model using the list of molecules and their current state
	 *
	 * @param list
	 * @param listOfActive
	 */
	public void fillTheModel(List<FullMolecule> list, List<Integer> listOfActive) {
		Object[] newDataRow = { null, null, false };
		for (SimpleMoleculeDescriptionDB mol : list) {
			addRow(editRowWithMolecule(newDataRow, mol, listOfActive.contains(mol.getTag())));
		}
	}

	private Object[] editRowWithMolecule(Object[] newDataRow,
			SimpleMoleculeDescriptionDB mol, boolean listVal) {
		newDataRow[INDEX_COLUMN_TAG] = mol.getTag();
		newDataRow[INDEX_COLUMN_NAME] = mol.getName();
		newDataRow[INDEX_COLUMN_IMPORT] = listVal;
		return newDataRow;
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
		case INDEX_COLUMN_TAG:
			return Integer.class;
		case INDEX_COLUMN_NAME:
			return String.class;
		case INDEX_COLUMN_IMPORT:
		default:
			return Boolean.class;
		}
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return column == INDEX_COLUMN_IMPORT;
	}

}
