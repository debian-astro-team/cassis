/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.importation.gui.selection.find.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

import eu.omp.irap.cassis.database.creation.importation.gui.selection.SelectionView;

/**
 * Filter molecules on their current state in the {@link SelectionView}
 *
 * @author bpenavayre
 *
 */
public class ShowOnlyController extends DefaultController<Boolean> implements
		ActionListener {

	private static final int FILTER_NON_SELECTED = 1;
	private static final int FILTER_SELECTED = 2;

	private ButtonGroup group;
	private Object old;
	private int value;
	private int columnIndex;


	/**
	 * Constructor
	 *
	 * @param checked
	 * @param unchecked
	 * @param group
	 * @param column
	 */
	public ShowOnlyController(JRadioButton checked, JRadioButton unchecked,
			ButtonGroup group, int column) {
		this.group = group;
		value = 0;
		columnIndex = column;
		checked.addActionListener(this);
		unchecked.addActionListener(this);
	}

	/**
	 * Change the options to find the correct thing to do.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		JRadioButton current = (JRadioButton) e.getSource();
		if (current.equals(old)) {
			group.clearSelection();
			value = 0;
		} else {
			value = current.getParent().getComponent(0) == current ? 1 : 2;
		}
		old = current;
	}

	@Override
	protected int correctOrNot(List<Boolean> row) {
		boolean val = row.get(columnIndex);
		if (value == FILTER_NON_SELECTED) {
			return val ? DO_CORRECT : DONT_CORRECT;
		}
		if (value == FILTER_SELECTED) {
			return !val ? DO_CORRECT : DONT_CORRECT;
		}
		return DONT_CORRECT;
	}

	@Override
	public void clear() {
		group.clearSelection();
		old = null;
		value = 0;
	}

}
