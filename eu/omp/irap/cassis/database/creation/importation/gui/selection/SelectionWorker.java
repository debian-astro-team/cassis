/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.importation.gui.selection;

import java.util.List;
import java.util.logging.Logger;

import javax.swing.SwingWorker;

import eu.omp.irap.cassis.database.creation.importation.ImportDatabase;
import eu.omp.irap.cassis.database.creation.importation.gui.wait.WaitFrame;
import eu.omp.irap.cassis.database.creation.importation.type.tools.TypeRelativeImportLauncher;
import eu.omp.irap.cassis.database.creation.tools.DatabaseContainer;
import eu.omp.irap.cassis.database.creation.tools.events.WaitForValidateListener;

/**
 * Extends {@link WaitForValidateListener},</br> {@link SwingWorker} responsible
 * for importation with the {@link SelectionView} showing up</br> once the
 * molecules have been set waiting for the disposal of the said view and
 * importing then transitions.
 *
 * @author bpenavayre
 *
 */
public class SelectionWorker extends WaitForValidateListener<Integer, Boolean> {

	private static final boolean IMPORT_MOLECULE_END = true;
	private static final boolean MOLECULE_SELECTION_END = false;
	private static final String IMPORT_MOLECULE_LOG = "Getting the molecules of the database %s.";
	private static final String IMPORT_MOLECULE_FAILED = "The importation of the database's molecules failed.";

	private WaitFrame dialog;
	private SelectionController control;
	private TypeRelativeImportLauncher importer;
	private Logger logger;


	/**
	 * Instantiate a {@link SelectionController} and
	 * {@link TypeRelativeImportLauncher}
	 *
	 * @param newDatabase
	 * @param waitMessage
	 * @param logger
	 */
	public SelectionWorker(DatabaseContainer newDatabase,
			WaitFrame waitMessage, Logger logger) {
		this.logger = logger;
		control = new SelectionController(newDatabase);
		importer = ImportDatabase.getImportationProtocol(newDatabase, logger);
		control.add(this);
		dialog = waitMessage;
	}

	/**
	 * Import molecules, show molecules selection, wait for disposal and get
	 * transitions
	 */
	@Override
	protected Integer doInBackground() {
		logger.info(String.format(IMPORT_MOLECULE_LOG, importer.getDatabase()
				.getDatabaseName()));
		if (!importer.setMolecules()) {
			logger.severe(IMPORT_MOLECULE_FAILED);
			return ImportDatabase.IMPORT_FAILED;
		}
		publish(IMPORT_MOLECULE_END);
		control.init();
		waitHere();
		if (!control.wasValidated()) {
			return ImportDatabase.IMPORT_CANCEL;
		}
		publish(MOLECULE_SELECTION_END);
		return importer.setTransitions();
	}

	/**
	 * Either show the {@link SelectionView} or get rid of it
	 */
	@Override
	protected void process(List<Boolean> list) {
		for (boolean guiAction : list) {
			if (guiAction == IMPORT_MOLECULE_END) {
				if (isCancelled()) {
					continue;
				}
				control.showView(dialog);
			} else {
				control.getRidOfView();
			}
		}
	}

	@Override
	protected void done() {
		control.remove(this);
	}
}
