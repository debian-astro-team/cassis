/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.importation.gui.add;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import eu.omp.irap.cassis.database.access.TypeDataBase;
import eu.omp.irap.cassis.database.creation.importation.options.salp.services.SlapUrlList;
import eu.omp.irap.cassis.database.creation.tools.DatabaseContainer;

@SuppressWarnings("serial")
public class SlapDataBaseSelectionPanel extends SelectionPanel implements SelectionPanelInterface {

	private JList<String> jList;
	private DefaultListModel<String> model;
	private static List<String> listSlap;

	private static final String ERROR_MESSAGE = "<html>Couldn't retrieve the providers list.<br>Check if your internet connection is working.</html>";
	private static final String ERROR_TITLE = "List of services unreachable";


	public SlapDataBaseSelectionPanel() {
		super();

		selectButton.setText("Select in list");
		selectButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				chooseFile();

			}
		});

		ressourceLabel.setText("SLAP Service");
	}

	protected void chooseFile() {
		final JFrame frame = new JFrame();
		Container contentPane = frame.getContentPane();
		contentPane.add(getProviderList(), BorderLayout.CENTER);


		JPanel butttonsPanel = new JPanel();
		JButton validateButton = new JButton("Add to selection");
		validateButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (jList.getSelectedIndex() != -1){
					selectedRessourceTextField.setText(jList.getSelectedValue());
					String[] names = jList.getSelectedValue().replace("http://", "").split("[.]");
					String name = names[0];
					if (names.length>1){
						name = name + "-"+names[1];
					}
					getNameTextField().setText(name);
				}
				frame.dispose();
			}
		});
		JButton refreshButton = new JButton("Refresh list of SLAP providers");
		refreshButton.setEnabled(false);
		refreshButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setSlapList(true);
			}
		});
		butttonsPanel.add(refreshButton);
		butttonsPanel.add(validateButton);
		contentPane.add(butttonsPanel, BorderLayout.SOUTH);
		frame.setVisible(true);
		frame.pack();
	}

	/**
	 * @param center
	 */
	private JList<String> getProviderList() {
		if (jList == null) {
			jList = new JList<String>();
			model = new DefaultListModel<String>();
			jList.setModel(model);
			jList.setBorder(BorderFactory.createTitledBorder("List of VAMDC providers"));
			setSlapList(false);
		}
		return jList;
	}

	/**
	 * fill the list of provider for Vamdc
	 *
	 * @param center
	 */
	protected void setSlapList(boolean force) {
		if ((listSlap == null)  || force){
			listSlap = SlapUrlList.getList();
		}
		if (listSlap == null) {
			showLoadListError(jList);
			return;
		}

		for (String url :  listSlap) {
			model.addElement(url);
		}
	}


	private void showLoadListError(Container center) {
		JOptionPane.showConfirmDialog(center, ERROR_MESSAGE, ERROR_TITLE,
				JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
	}

	@Override
	public DatabaseContainer getData() {
		final String filePath = selectedRessourceTextField.getText();
		if (filePath.isEmpty()) {
			return null;
		}
		return new DatabaseContainer(nameTextField.getText(), selectedRessourceTextField.getText(),
				TypeDataBase.SLAP, null);
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame("SLAP DataBase Selection");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setContentPane(new SlapDataBaseSelectionPanel());
		frame.pack();
	}
}
