/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.importation.gui.selection;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Window;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.plaf.basic.BasicTextFieldUI;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.text.JTextComponent;

/**
 * Selection {@link JDialog}
 *
 * @author bpenavayre
 *
 */
public class SelectionView extends JDialog {

	public static final String TITLE = "Select Species";

	public static final int ALL = 0;
	public static final int NONE = 1;
	public static final int FIND = 2;
	public static final int VALIDATE_KEY = 3;

	private static final long serialVersionUID = 8096069144704559764L;
	private static final int SIZE_X = 650;
	private static final String ERROR_MESSAGE = "<html>The Selection is empty.<br>Please choose at least one molecule.</html>";
	private static final String ICON_PATH = "/icons/question_mark.gif";
	private static final String TOOL_TIP_STRING = "<html>Importing all the database's content might take a while.<br>Use this to specify your selection.</html>";

	private boolean allSelected = false;
	private SelectionController control;
	private JTable table;
	private JScrollPane scrollPane;
	private JButton all;
	private JButton none;
	private JButton find;
	private JButton importSelect;


	/**
	 * Set everything to wished values.
	 *
	 * @param control
	 */
	public SelectionView(SelectionController control, Window parent) {
		super(parent);
		setTitle(TITLE);
		this.control = control;
		setMinimumSize(new Dimension(SIZE_X, 350));
		setContentPane(createAndGetPanel());
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		pack();
		setModalityType(ModalityType.APPLICATION_MODAL);
	}

	private JPanel createAndGetPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(createAndGetTop(), BorderLayout.PAGE_START);
		panel.add(getScrollPane(), BorderLayout.CENTER);
		panel.add(createAndGetButtom(), BorderLayout.PAGE_END);
		return panel;
	}

	private JPanel createAndGetTop() {
		JPanel panel = new JPanel(new BorderLayout());
		JPanel subPanel = new JPanel(new BorderLayout(10, 2));
		subPanel.add(new JLabel("Import:"), BorderLayout.LINE_START);
		subPanel.add(getAllButton(), BorderLayout.CENTER);
		subPanel.add(getNoneButton(), BorderLayout.LINE_END);
		JLabel questionMark = new JLabel(new ImageIcon(
				SelectionView.class.getResource(ICON_PATH)));
		questionMark.setToolTipText(TOOL_TIP_STRING);
		ToolTipManager.sharedInstance().registerComponent(questionMark);
		panel.add(questionMark, BorderLayout.LINE_START);
		panel.add(subPanel, BorderLayout.LINE_END);
		panel.setBorder(BorderFactory.createEmptyBorder(3, 0, 3, 20));
		panel.setPreferredSize(new Dimension(SIZE_X, 30));
		return panel;
	}

	public JButton getNoneButton() {
		if (none == null) {
			none = new JButton("None");
			none.addActionListener(control);
		}
		return none;
	}
	public JButton getFindButton() {
		if (find == null) {
			find = new JButton("find");
			find.addActionListener(control);
		}
		return find;
	}

	public JButton getAllButton() {
		if (all == null) {
			all = new JButton("All");
			all.addActionListener(control);
		}
		return all;
	}

	/**
	 * Select a line based on a column and a searched text on the table and
	 *  close the dialog.
	 *
	 * @param selectedColumn The selected column.
	 * @param dialog The dialog to close.
	 * @param text The searched text.
	 */
	private void selectLine(int selectedColumn, final JDialog dialog, String text) {
		table.clearSelection();
		int row = 0;
		boolean find = false;
		TableModel tm = control.getTableModel();
		int nbRow = tm.getRowCount();
		for (; row < nbRow; row++) {
			if (String.valueOf(tm.getValueAt(row, selectedColumn)).equals(text)) {
				find = true;
				break;
			}
		}

		if (find) {
			row = table.getRowSorter().convertRowIndexToView(row);
			table.setRowSelectionInterval(row, row);
			int value = (getScrollPane().getVerticalScrollBar().getMaximum() -
					getScrollPane().getVerticalScrollBar().getMinimum()) / nbRow;
			getScrollPane().getVerticalScrollBar().setValue(value * (row - 4));
		}
		getTable().requestFocus();
		dialog.dispose();
	}

	/**
	 * Create if needed then return the table.
	 *
	 * @return the table.
	 */
	private JTable getTable() {
		if (table == null) {
			table = new JTable(control.getTableModel());
			table.setAutoCreateRowSorter(true);
			table.setFillsViewportHeight(true);
			table.setOpaque(true);
			DefaultTableCellRenderer render = new DefaultTableCellRenderer();
			render.setHorizontalAlignment(SwingConstants.CENTER);
			for (int i = 0; i < table.getColumnCount() - 1; i++) {
				table.getColumnModel().getColumn(i).setCellRenderer(render);
			}
			table.getTableHeader().addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent me) {
					int columnVisible = table.getColumnModel().getColumnIndexAtX(me.getPoint().x);
					final int columnIndex = table.getColumnModel().getColumn(columnVisible).getModelIndex();
					if (SwingUtilities.isRightMouseButton(me)) {
						final JDialog dialog = new JDialog(SelectionView.this,
								"Search column (" + table.getColumnName(columnIndex) + ")");
						final JPanel panel = new JPanel(new BorderLayout());
						final JTextField textField = new JTextField(5);
						textField.setUI(new HintTextFieldUI(
								"Type your search here. Hit enter to validate a result."));
						panel.add(textField, BorderLayout.NORTH);
						final JList<String> list = new JList<String>();
						panel.add(new JScrollPane(list), BorderLayout.CENTER);
						textField.requestFocus();
						textField.addKeyListener(new KeyAdapter() {
							@Override
							public void keyReleased(KeyEvent e) {
								if (e.getKeyCode() == KeyEvent.VK_ENTER) {
									selectLine(columnIndex, dialog, String.valueOf(list.getSelectedValue()));
								} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
									list.requestFocus();
									list.setSelectedIndex(1);
								} else {
									TableModel tm = control.getTableModel();
									int rowSize = tm.getRowCount();
									List<String> listData = new ArrayList<String>();
									for (int row = 0; row < rowSize; row++) {
										String value = String.valueOf(tm.getValueAt(row, columnIndex));
										if (value.contains(textField.getText())) {
											listData.add(value);
										}
									}
									list.setListData(listData.toArray(new String[listData.size()]));
									list.setSelectedIndex(0);
									list.validate();
								}
							}
						});
						list.addKeyListener(new KeyAdapter() {
							@Override
							public void keyReleased(KeyEvent e) {
								if (e.getKeyCode() == KeyEvent.VK_ENTER) {
									selectLine(columnIndex, dialog, String.valueOf(list.getSelectedValue()));
								} else if (e.getKeyCode() == KeyEvent.VK_UP && list.getSelectedIndex() == 0) {
									textField.requestFocus();
								}
							}
						});
						list.addMouseListener(new MouseAdapter() {
							@Override
							public void mouseClicked(MouseEvent e) {
								super.mouseClicked(e);
								selectLine(columnIndex, dialog, String.valueOf(list.getSelectedValue()));
							}
						});
						dialog.setLocation(me.getLocationOnScreen());
						dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
						dialog.setContentPane(panel);
						dialog.setSize(360, 200);
						dialog.setVisible(true);
					} else if (SwingUtilities.isLeftMouseButton(me) &&
							columnIndex == SelectionTableModel.INDEX_COLUMN_IMPORT) {
						setAllTo(!allSelected);
					}
				}
			});
		}
		return table;
	}

	class HintTextFieldUI extends BasicTextFieldUI {

		private String hint;

		/**
		 * The constructor.
		 *
		 * @param theHint The hint to set.
		 */
		public HintTextFieldUI(String theHint) {
			this.hint = theHint;
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see javax.swing.plaf.basic.BasicTextUI#paintSafely(java.awt.Graphics)
		 */
		@Override
		protected void paintSafely(Graphics g) {
			super.paintSafely(g);
			JTextComponent textComponent = getComponent();
			if (hint != null && textComponent.getText().isEmpty()) {
				g.setColor(textComponent.getForeground().brighter().brighter().brighter());
				int padding = (textComponent.getHeight() - textComponent.getFont().getSize()) / 2;
				g.drawString(hint, 2, textComponent.getHeight() - padding - 1);
			}
		}
	}

	/**
	 * Create if needed then return the scrollpane.
	 *
	 * @return the scrollpane.
	 */
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane(getTable());
			scrollPane.setPreferredSize(new Dimension(SIZE_X, 300));
			scrollPane.getVerticalScrollBar().setUnitIncrement(100);
			scrollPane.setBorder(BorderFactory.createLineBorder(Color.BLACK));
			return scrollPane;
		}
		return scrollPane;
	}

	private JPanel createAndGetButtom() {
		JPanel panel = new JPanel(new BorderLayout());

		panel.add(getFindButton(), BorderLayout.LINE_START);
		panel.add(getImportSelectButton(), BorderLayout.LINE_END);
		panel.setPreferredSize(new Dimension(SIZE_X, 33));
		panel.setBorder(BorderFactory.createEmptyBorder(5, 3, 3, 3));
		return panel;
	}

	public JButton getImportSelectButton() {
		if (importSelect == null) {
			importSelect = new JButton("Import Selected");
			importSelect.addActionListener(control);
		}
		return importSelect;
	}

	/**
	 * Return a key assigned to a {@link JButton}
	 *
	 * @param eventSource
	 * @return
	 */
	public int getButtonKey(Object eventSource) {
		if (eventSource == getAllButton()) {
			return ALL;
		} else if (eventSource == none) {
			return NONE;
		} else if (eventSource == find) {
			return FIND;
		} else if (eventSource == getImportSelectButton()) {
			return VALIDATE_KEY;
		}
		return -1;
	}

	/**
	 * Change all the molecules to a new state
	 *
	 * @param newVal
	 */
	public void setAllTo(boolean newVal) {
		allSelected = newVal;
		for (int i = 0; i < table.getRowCount(); i++) {
			table.getModel().setValueAt(newVal, i, table.getColumnCount() - 1);
		}
		revalidate();
		repaint();
	}

	/**
	 * @param row
	 * @return the state of molecules in the row given in parameter
	 */
	public boolean getNewVal(int row) {
		return (boolean) table.getModel().getValueAt(row, 2);
	}

	/**
	 * @param row
	 * @return the tag of wished molecules
	 */
	public int getTag(int row) {
		return (int) table.getModel().getValueAt(row, 0);
	}

	/**
	 *  Show a {@link JOptionPane} displaying an error
	 */
	public void showErrorEmpty() {
		JOptionPane.showConfirmDialog(this, ERROR_MESSAGE, "Selection Empty",
				JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
	}

	public void removeListeners() {
		AbstractButton component = getFindButton();
		for (ActionListener al : component.getActionListeners()) {
			component.removeActionListener(al);
		}
		AbstractButton component1 = getAllButton();
		for (ActionListener al : component1.getActionListeners()) {
			component1.removeActionListener(al);
		}
		AbstractButton component2 = getNoneButton();
		for (ActionListener al : component2.getActionListeners()) {
			component2.removeActionListener(al);
		}
		AbstractButton component3 = getImportSelectButton();
		for (ActionListener al : component3.getActionListeners()) {
			component3.removeActionListener(al);
		}
		control = null;
	}
}
