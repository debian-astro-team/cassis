/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.importation.gui.add;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.database.configuration.DatabaseConfiguration;

@SuppressWarnings("serial")
public abstract class FileDataBaseSelectionPanel extends SelectionPanel implements SelectionPanelInterface {


	public FileDataBaseSelectionPanel() {
		super();
		c.gridy++;
		c.gridx = 0;
		c.weighty = 1;
		c.weightx = 1;
		c.gridwidth = 0;
		c.insets.bottom = 0;
		add(new JPanel(), c);

		selectButton.setText("Select file");
		selectButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				chooseFile();

			}
		});

		ressourceLabel.setText("File");
	}

	protected File chooseFile() {
		JFileChooser fc = new CassisJFileChooser(
				DatabaseConfiguration.getInstance().getDatabasePath(),
				DatabaseConfiguration.getInstance().getLastFolder("file-database"));
		fc.setMultiSelectionEnabled(false);
		fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		int resultOfChoosing = fc.showOpenDialog(this);
		if (resultOfChoosing == JFileChooser.ERROR_OPTION) {
			JOptionPane.showMessageDialog(this, SelectionPanel.JFILECHOOSER_ERROR);
		} else if (resultOfChoosing == JFileChooser.APPROVE_OPTION) {
			File result = fc.getSelectedFile();
			selectedRessourceTextField.setText(result.getAbsolutePath());
			nameTextField.setText(result.getName().split("[.]")[0]);
			String path = result.isFile() ? result.getParent() : result.getAbsolutePath();
			DatabaseConfiguration.getInstance().setLastFolder(
					"file-database", path);
		}

		return fc.getCurrentDirectory();
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setContentPane(new FileDataBaseSelectionPanel(){

		});
		frame.pack();
	}

}
