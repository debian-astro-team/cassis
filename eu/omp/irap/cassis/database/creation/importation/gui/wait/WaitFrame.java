/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.importation.gui.wait;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import eu.omp.irap.cassis.database.creation.tools.log.gui.GuiHandler;
import eu.omp.irap.cassis.database.creation.tools.log.gui.LogGui;

/**
 * Extends {@link JDialog},</br> this class has as panel a {@link JOptionPane}.
 *
 * @author bpenavayre
 *
 */
public class WaitFrame extends JDialog implements ActionListener {

	private static final long serialVersionUID = 2218293289766803685L;
	private static final String MESSAGE = "<html><center>The importation is taking place.<br>If you wish to stop the import click on cancel.<br>Otherwise please wait.<center></html>";
	private static final String TITLE = "Importation of %s...";
	private static final String TITLE_ERROR = "Error: importation failed";
	private static final String INFORMATION_ICON_STRING = "OptionPane.informationIcon";
	private static final String OPTION = "Cancel";
	private static final String CLOSE = "Close";

	private JPanel mainPanel;
	private JPanel topPanel;
	private JButton button;
	private LogGui logGui;
	private boolean shouldHandleDispose;


	/**
	 * Create the {@link JDialog} and set all the wished value</br> by using the
	 * parameters
	 *
	 * @param name
	 * @param guiParent
	 * @param propertyListener
	 * @param logger
	 */
	public WaitFrame(String name, Component guiParent,
			PropertyChangeListener propertyListener, Logger logger) {
		super((Window)SwingUtilities.getRoot(guiParent));
		shouldHandleDispose = false;
		addPropertyChangeListener(OPTION, propertyListener);
		setTitle(String.format(TITLE, name).toString());
		mainPanel = new JPanel(new BorderLayout());
		topPanel = new JPanel();
		topPanel.setPreferredSize(new Dimension(400, 70));
		topPanel.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
		Icon infoIcon = UIManager.getIcon(INFORMATION_ICON_STRING);
		JLabel label = new JLabel(MESSAGE, infoIcon, SwingConstants.CENTER);
		topPanel.add(label);
		mainPanel.add(topPanel, BorderLayout.PAGE_START);
		JPanel centerPanel = new JPanel(new BorderLayout());
		centerPanel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
		logGui = new GuiHandler(logger).getLogGui();
		JScrollPane scroll = new JScrollPane(logGui);
		scroll.setBorder(BorderFactory.createTitledBorder("Log"));
		scroll.setPreferredSize(new Dimension(400, 400));
		centerPanel.add(scroll);
		mainPanel.add(centerPanel);
		JPanel buttomPanel = new JPanel();
		button = new JButton(OPTION);
		button.addActionListener(this);
		buttomPanel.add(button);
		mainPanel.add(buttomPanel, BorderLayout.PAGE_END);
		setContentPane(mainPanel);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setResizable(true);
		pack();
		setLocationRelativeTo((Component) guiParent);
		setVisible(false);
	}

	/**
	 * Listen to {@link JButton} {@link #button} actions
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (!shouldHandleDispose) {
			firePropertyChange(OPTION, false, true);
		} else {
			dispose();
		}
	}

	/**
	 * Change the dialog from standard to Error config.
	 */
	public void setToError() {
		setTitle(TITLE_ERROR);
		button.setText(CLOSE);
		shouldHandleDispose = true;
		mainPanel.remove(topPanel);
		logGui.setForeground(Color.RED);
		pack();
	}

}
