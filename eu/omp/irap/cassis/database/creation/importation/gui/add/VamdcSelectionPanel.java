/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.importation.gui.add;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import eu.omp.irap.cassis.database.configuration.DatabaseConfiguration;
import eu.omp.irap.cassis.database.creation.importation.options.vamdc.services.GetServiceList;
import eu.omp.irap.cassis.database.creation.importation.options.vamdc.services.Provider;
import eu.omp.irap.cassis.database.creation.importation.options.vamdc.services.ServiceListWorker;

/**
 * JPanel allowing to select a VAMDC service.
 *
 * @author M. Boiziot
 */
public class VamdcSelectionPanel extends JPanel {

	private static final long serialVersionUID = 3632638744918315924L;

	private JButton validateButton;
	private JButton refreshButton;
	private JList<Provider> jList;
	private DefaultListModel<Provider> model;
	private static List<Provider> listVamdc;


	/**
	 * Constructor.
	 */
	public VamdcSelectionPanel() {
		super(new BorderLayout());
		add(new JScrollPane(getProviderList()), BorderLayout.CENTER);

		JPanel buttonsPanel = new JPanel();
		buttonsPanel.add(getRefreshButton());
		buttonsPanel.add(getValidateButton());
		add(buttonsPanel, BorderLayout.SOUTH);
	}

	/**
	 * Create if necessary then return the validate button "Add to selection".
	 *
	 * @return the validate button "Add to selection".
	 */
	public JButton getValidateButton() {
		if (validateButton == null) {
			validateButton = new JButton("Add to selection");
		}
		return validateButton;
	}

	/**
	 * Create if necessary then return the refresh button.
	 *
	 * @return the refresh button.
	 */
	public JButton getRefreshButton() {
		if (refreshButton == null) {
			refreshButton = new JButton("Refresh list of VAMDC providers");
			if (DatabaseConfiguration.getInstance().isOnlineMode()) {
				refreshButton.setEnabled(false);
				refreshButton.setToolTipText("This function is disabled in online mode");
			} else {
				refreshButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						setVamdcList(true);
					}
				});
			}
		}
		return refreshButton;
	}

	/**
	 * Create if necessary then return the JList containing the {@link Provider}s.
	 *
	 * @return the JList containing the {@link Provider}s.
	 */
	public JList<Provider> getProviderList() {
		if (jList == null) {
			jList = new JList<Provider>();
			model = new DefaultListModel<Provider>();
			jList.setModel(model);
			jList.setBorder(BorderFactory.createTitledBorder("List of VAMDC providers"));
			setVamdcList(false);
		}
		return jList;
	}

	/**
	 * Fill the list of provider for Vamdc
	 *
	 * @param force true to query the registry, false to use built-in list of Provider.
	 */
	protected void setVamdcList(boolean force) {
		if (force){
			listVamdc = ServiceListWorker.getListOfProviderGui(jList);
		} else if (listVamdc == null) {
			listVamdc = GetServiceList.getDefaultVamdcProviderForCassis();
		}
		if (listVamdc == null) {
			showLoadListError(jList);
			return;
		}

		model.clear();
		for (Provider provider : listVamdc) {
			model.addElement(provider);
		}
	}

	/**
	 * Display an error message.
	 *
	 * @param comp The parent compoenent.
	 */
	private void showLoadListError(Component comp) {
		JOptionPane.showConfirmDialog(comp,
				"<html>Couldn't retrieve the providers list.<br>"
				+ "Check if your internet connection is working.</html>",
				"List of services unreachable",
				JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
	}
}
