/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.importation.gui.selection;

import java.awt.Component;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import eu.omp.irap.cassis.database.access.TypeDataBase;
import eu.omp.irap.cassis.database.creation.importation.gui.selection.find.FindFrame;
import eu.omp.irap.cassis.database.creation.importation.options.SelectionOption;
import eu.omp.irap.cassis.database.creation.importation.options.file.FileDatabaseOptions;
import eu.omp.irap.cassis.database.creation.tools.DatabaseContainer;
import eu.omp.irap.cassis.database.creation.tools.events.WaitForValidateListener;

/**
 * Controller for the {@link SelectionView}, handles its events and when close
 * is requested and answered send to its {@link WaitForValidateListener}
 * validation to wake him
 *
 * @author bpenavayre
 *
 */
public class SelectionController implements ActionListener, TableModelListener {

	private DatabaseContainer newDatabase;
	private SelectionTableModel tableModel;
	private List<Integer> listOfImport;
	private SelectionView view;
	private FindFrame findFrame;
	private boolean validated;
	private WindowAdapter windowListener;
	private List<SelectionWorker> listeners;


	/**
	 * Standard controller
	 *
	 * @param newDatabase
	 */
	public SelectionController(DatabaseContainer newDatabase) {
		this.newDatabase = newDatabase;
		this.listeners = new ArrayList<SelectionWorker>(2);
		tableModel = new SelectionTableModel();
		validated = false;
	}

	/**
	 * Initialize data
	 */
	public void init() {
		listOfImport = new ArrayList<Integer>();
		tableModel.fillTheModel(newDatabase.getMolecules(), listOfImport);
	}

	/**
	 * Create, set and show a {@link SelectionView}
	 *
	 * @param guiParent
	 */
	public void showView(Component guiParent) {
		view = new SelectionView(this, (Window) SwingUtilities.getRoot(guiParent));
		windowListener = new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				if (!validated) {
					sendValidateToListeners();
				}
			}
		};
		view.addWindowListener(windowListener);
		tableModel.addTableModelListener(this);
		view.setLocationRelativeTo(guiParent);
		view.setVisible(true);
	}

	/**
	 * Clean the {@link JTable} model and refill it.
	 */
	public void backToStart() {
		tableModel.setRowCount(0);
		tableModel.fillTheModel(newDatabase.getMolecules(), listOfImport);
	}

	/**
	 * @return the {@link JTable}'s model
	 */
	public TableModel getTableModel() {
		return tableModel;
	}

	/**
	 * Handles actions for {@link SelectionView}'s buttons
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		switch (view.getButtonKey(e.getSource())) {
		case SelectionView.ALL:
			listOfImport.clear();
			view.setAllTo(true);
			break;
		case SelectionView.NONE:
			listOfImport.clear();
			view.setAllTo(false);
			break;
		case SelectionView.FIND:
			if (findFrame == null) {
				findFrame = new FindFrame(this, view);
			} else {
				findFrame.setVisible(true);
			}
			break;
		case SelectionView.VALIDATE_KEY:
			if (listOfImport.isEmpty()) {
				view.showErrorEmpty();
				return;
			}
			validated = true;
			String result = SelectionOption.createAndGetResultString(
					listOfImport, newDatabase.getMolecules().size());
			if (newDatabase.getType() != TypeDataBase.FILE) {
				newDatabase.setSelectionOption(result);
			} else {
				((FileDatabaseOptions)newDatabase.getOption()).setSelectionOption(result);
			}
			sendValidateToListeners();
			break;
		}
	}

	/**
	 * Dispose of all the views
	 */
	public void getRidOfView() {
		view.removeWindowListener(windowListener);
		tableModel.removeTableModelListener(this);
		view.removeListeners();
		view.dispose();
		if (findFrame != null) {
			findFrame.dispose();
		}
	}

	/**
	 * Change add or delete value in {@link #listOfImport}
	 *
	 * @param tag
	 * @param state
	 */
	public void updateListOfImport(Integer tag, boolean state) {
		if (state) {
			listOfImport.add(tag);
		} else {
			listOfImport.remove(tag);
		}
	}

	@Override
	public void tableChanged(TableModelEvent e) {
		if (e.getType() != TableModelEvent.UPDATE) {
			return;
		}
		if (e.getColumn() == 2) {
			updateListOfImport(view.getTag(e.getFirstRow()),
					view.getNewVal(e.getFirstRow()));
		}
	}

	/**
	 * @return {@link #validated}
	 */
	public boolean wasValidated() {
		return validated;
	}

	/**
	 * Triggers {@link WaitForValidateListener#validated() validated} from all
	 * the {@link WaitForValidateListener}s in this
	 */
	public void sendValidateToListeners() {
		for (SelectionWorker listener : listeners) {
			listener.validated();
		}
	}

	public void add(SelectionWorker worker) {
		listeners.add(worker);
	}

	public void remove(SelectionWorker worker) {
		listeners.remove(worker);
	}
}
