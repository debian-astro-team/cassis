/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.importation;

import java.util.List;

import eu.omp.irap.cassis.database.access.LineDescriptionDB;
import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB;
import eu.omp.irap.cassis.database.creation.tools.DatabaseContainer;
import eu.omp.irap.cassis.database.creation.tools.FullMolecule;

/**
 * Tool class that serve us to correct negatives tags (Unknown tags)
 *
 * @author bpenavayre
 *
 */
public final class CorrectUnknownId {


	private CorrectUnknownId() {
	}

	/**
	 * the static index for unknown tags
	 */
	private static int lastUnknowId = -1;

	private static int findMin(List<FullMolecule> list) {
		int min = 0;
		for (SimpleMoleculeDescriptionDB mol : list) {
			if (mol.getTag() < min) {
				min = mol.getTag();
			}
		}
		return min;
	}

	/**
	 * Set the {@link CorrectUnknownId#lastUnknowId} to its default value : -1
	 */
	public static void reset() {
		lastUnknowId = -1;
	}

	private static boolean isNecessary(List<FullMolecule> list) {
		int min = findMin(list);
		if (min == 0) {
			return false;
		}
		if (lastUnknowId == -1 && min < -1) {
			lastUnknowId = min;
			return false;
		}
		return true;
	}

	/**
	 * Method to use to correct a database's unknown tags
	 *
	 * @param database
	 */
	public static void doCorrect(DatabaseContainer database) {
		if (!isNecessary(database.getMolecules())) {
			return;
		}
		int tag;

		for (FullMolecule mol : database.getMolecules()) {
			tag = mol.getTag();
			if (tag < 0 && tag >= lastUnknowId) {
				mol.setTag(--lastUnknowId);
				for (LineDescriptionDB line : mol.getTransitions()) {
					line.setTag(lastUnknowId);
				}
			}
		}
	}
}
