/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.importation.type;

import java.io.File;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;

import eu.omp.irap.cassis.common.Formula;
import eu.omp.irap.cassis.database.access.CassisDataBaseConnection;
import eu.omp.irap.cassis.database.access.CdmsDataBaseConnection;
import eu.omp.irap.cassis.database.access.DataBaseConnection;
import eu.omp.irap.cassis.database.access.FileDataBaseConnection;
import eu.omp.irap.cassis.database.access.LineDescriptionDB;
import eu.omp.irap.cassis.database.access.NistDataBaseConnection;
import eu.omp.irap.cassis.database.access.NoDataBaseConnection;
import eu.omp.irap.cassis.database.creation.importation.ImportDatabase;
import eu.omp.irap.cassis.database.creation.importation.options.SelectionOption;
import eu.omp.irap.cassis.database.creation.importation.options.file.FileDatabaseOptions;
import eu.omp.irap.cassis.database.creation.importation.type.tools.TypeRelativeImportLauncher;
import eu.omp.irap.cassis.database.creation.tools.DatabaseContainer;
import eu.omp.irap.cassis.database.creation.tools.FullMolecule;

/**
 * Catdir import protocols
 *
 * @author bpenavayre, jglorian
 *
 */
public class CatdirImportProtocols extends TypeRelativeImportLauncher implements
		Comparator<LineDescriptionDB> {

	private FileDatabaseOptions option;
	private static boolean logTransitionsRemoved = false;
	private static boolean logNbTransitionsRemoved = false;


	/**
	 * @see TypeRelativeImportLauncher
	 *
	 * @param newDatabase
	 * @param logger
	 */
	public CatdirImportProtocols(DatabaseContainer newDatabase, Logger logger) {
		super(newDatabase, logger);
	}

	@Override
	public boolean setMolecules() {
		if (!importMolecules()) {
			return false;
		}
		changeSource(getDatabase().getDatabaseName());
		return true;
	}

	@Override
	public int setTransitions() {
		int result;
		SelectionOption.removeUnselectedMolecules(getDatabase(),
				option.getSelectionOption());
		result = importTransitions();
		if (result == ImportDatabase.IMPORT_FAILED) {
			return importTransitions();
		}
		cleanTransitionsForCatdirType();
		if (option.shouldCorrect()) {
			correctCatdirValues(getDatabase());
		}
		return result;
	}

	@Override
	public DataBaseConnection getConnection(String path) {
		option = (FileDatabaseOptions) getOption();
		String correctPath = path;
		if (!path.endsWith(File.separator)) {
			correctPath += File.separator;
		}

		switch (option.getType()) {
			case FileDatabaseOptions.CDMS_TYPE:
				return new CdmsDataBaseConnection(correctPath);
			case FileDatabaseOptions.JPL_TYPE:
				return new FileDataBaseConnection(correctPath);
			case FileDatabaseOptions.NIST_TYPE:
				return new NistDataBaseConnection(correctPath);
			case FileDatabaseOptions.CASSIS_TYPE:
				return new CassisDataBaseConnection(correctPath);
			default :
				return new NoDataBaseConnection();
		}
	}

	private void cleanTransitionsForCatdirType() {
		double[] temp = { 0, 0 };
		String[] qn = { null, null };
		List<LineDescriptionDB> transitions;
		for (FullMolecule mol : getDatabase().getMolecules()) {
			transitions = mol.getTransitions();
			int nbTransitions = transitions.size();
			for (int i = 1; i < transitions.size(); i++) {

				temp[0] = transitions.get(i).getFrequency();
				qn[0] = transitions.get(i).getQuanticNumbers();

				temp[1] = transitions.get(i - 1).getFrequency();
				qn[1] = transitions.get(i - 1).getQuanticNumbers();

				if (simpleAbs(temp[1] - temp[0]) < 10E-12
						&& qn[0].equals(qn[1])) {
					if (logTransitionsRemoved){
						System.out.println(transitions.get(i));
					}
					transitions.remove(i--);
				}
			}
			if (logNbTransitionsRemoved) {
				int nbTransitions2 = transitions.size();
				if (nbTransitions2 != nbTransitions) {

					System.out.println(mol.getSource() + "\tName:"+ mol.getName() +
							"\tTag:" + String.valueOf(mol.getTag())+
							"\tnbTransitions:"+ String.valueOf(nbTransitions-nbTransitions2));
				}
			}

		}
	}

	private double simpleAbs(double value) {
		return value < 0 ? -1 * value : value;
	}

	private void correctCatdirValues(DatabaseContainer databaseContainer) {
		double elowMin;
		double elow;

		for (FullMolecule mol : databaseContainer.getMolecules()) {
			elowMin = Collections.min(mol.getTransitions(), this).getElow();
			if (elowMin != 0){
				for (LineDescriptionDB line : mol.getTransitions()) {
					if (line.getElow() != 0) {
						elow = line.getElow();
						line.setElow(elow - elowMin);
						line.setEup(Formula.calcEUpk(elow, elowMin, line.getFrequency()));
					}
				}
			}
		}
	}

	/**
	 * Compare two {@link LineDescriptionDB}'s Elow.</br>
	 * Use this class as a {@link Comparator} to find the min of a list</br>
	 * of {@link LineDescriptionDB}
	 */
	@Override
	public int compare(LineDescriptionDB o1, LineDescriptionDB o2) {
		return Double.compare(o1.getElow(), o2.getElow());
	}


	public static void setLogTransitionsRemoved(boolean logTransitionsRemoved) {
		CatdirImportProtocols.logTransitionsRemoved = logTransitionsRemoved;
	}


	public static void setLogNbTransitionsRemoved(boolean logNbTransitionsRemoved) {
		CatdirImportProtocols.logNbTransitionsRemoved = logNbTransitionsRemoved;
	}

}
