/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.importation.type.tools;

import java.util.List;
import java.util.logging.Logger;

import eu.omp.irap.cassis.database.access.DataBaseConnection;
import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB;
import eu.omp.irap.cassis.database.creation.importation.ImportDatabase;
import eu.omp.irap.cassis.database.creation.tools.DatabaseContainer;

/**
 * Abstract import protocols
 *
 * @author bpenavayre
 *
 */
public abstract class TypeRelativeImportLauncher {

	private DatabaseContainer newDatabase;
	private DataBaseConnection connection;
	private Logger logger;


	/**
	 * Set the fields using the arguments and get the connection using the</br>
	 * abstract method {@link #getConnection(String)}
	 *
	 * @param newDatabase
	 * @param logger
	 */
	public TypeRelativeImportLauncher(DatabaseContainer newDatabase,
			Logger logger) {
		this.newDatabase = newDatabase;
		this.connection = getConnection(newDatabase.getPath());
		this.logger = logger;
	}

	/**
	 * Default import method use {@link #setMolecules()} to retrieve the
	 * molecules</br> and do the same with the transitions (
	 * {@link #setTransitions()})
	 *
	 * @return the import status : @see {@link ImportDatabase}
	 */
	public int setAll() {
		if (!setMolecules()) {
			return ImportDatabase.IMPORT_FAILED;
		}
		return setTransitions();
	}

	/**
	 * Should use the {@link DataBaseConnection} to retrieve the molecules</br>
	 * and then set theses in the {@link DatabaseContainer}
	 *
	 * @return true is success
	 */
	public abstract boolean setMolecules();

	/**
	 * Should use the {@link DataBaseConnection} to retrieve the
	 * transitions</br> and then set theses in the {@link DatabaseContainer}
	 *
	 * @return true is success
	 */
	public abstract int setTransitions();

	/**
	 * Should return the {@link DataBaseConnection} corresponding to the
	 * database's type
	 *
	 * @param path
	 * @return
	 */
	public abstract DataBaseConnection getConnection(String path);

	/**
	 * @return the {@link DatabaseContainer}'s path
	 */
	public String getPath() {
		return newDatabase.getPath();
	}

	/**
	 * @return the {@link DatabaseContainer}
	 */
	public DatabaseContainer getDatabase() {
		return newDatabase;
	}

	/**
	 * @return the {@link DatabaseContainer}'s option
	 */
	public Object getOption() {
		return newDatabase.getOption();
	}

	/**
	 * Change the source of all the molecules contained in the
	 * {@link DatabaseContainer}
	 *
	 * @param newSource
	 */
	public void changeSource(String newSource) {
		for (SimpleMoleculeDescriptionDB mol : newDatabase.getMolecules()) {
			mol.setSource(newSource);
		}
	}

	/**
	 * Import and set all the molecules
	 * @return state of import
	 */
	public boolean importMolecules() {
		if (connection == null) {
			return false;
		}
		List<SimpleMoleculeDescriptionDB> mols = connection
				.getAllMoleculeDescriptionDB();
		if (mols == null || mols.isEmpty()) {
			return false;
		}
		newDatabase.setMolecules(mols);
		return true;
	}

	/**
	 * Import and sets transitions
	 * @return the import state
	 */
	public int importTransitions() {
		return newDatabase.setLines(connection, logger);
	}
}
