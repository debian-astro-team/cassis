/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.importation;

import java.util.logging.Logger;

import eu.omp.irap.cassis.database.creation.importation.type.CatdirImportProtocols;
import eu.omp.irap.cassis.database.creation.importation.type.SlapFileImportProtocols;
import eu.omp.irap.cassis.database.creation.importation.type.SlapImportProtocols;
import eu.omp.irap.cassis.database.creation.importation.type.SqliteImportProtocols;
import eu.omp.irap.cassis.database.creation.importation.type.VamdcFileImportProtocols;
import eu.omp.irap.cassis.database.creation.importation.type.VamdcImportProtocols;
import eu.omp.irap.cassis.database.creation.importation.type.tools.TypeRelativeImportLauncher;
import eu.omp.irap.cassis.database.creation.tools.DatabaseContainer;

/**
 * Tool class
 *
 * @author bpenavayre
 *
 */
public final class ImportDatabase {

	public static final int IMPORT_SUCCESS = 0;
	public static final int IMPORT_CANCEL = 1;
	public static final int IMPORT_FAILED = 2;
	public static final int IMPORT_OK_WIH_ERROR = 3;


	private ImportDatabase() {
	}

	/**
	 * Default importation procedure
	 *
	 * @param newDatabase
	 * @param logger
	 * @return
	 */
	public static int doImport(DatabaseContainer newDatabase, Logger logger) {
		return getImportationProtocol(newDatabase, logger).setAll();
	}

	/**
	 * Retrieve the {@link TypeRelativeImportLauncher} corresponding to the</br>
	 * database's new type
	 *
	 * @param newDatabase
	 * @param logger
	 * @return {@link TypeRelativeImportLauncher}
	 */
	public static TypeRelativeImportLauncher getImportationProtocol(
			DatabaseContainer newDatabase, Logger logger) {
		switch (newDatabase.getType()) {
		case FILE:
			return new CatdirImportProtocols(newDatabase, logger);
		case SQLITE:
			return new SqliteImportProtocols(newDatabase, logger);
		case VAMDC:
			return new VamdcImportProtocols(newDatabase, logger);
		case VAMDC_FILE:
			return new VamdcFileImportProtocols(newDatabase, logger);
		case SLAP:
			return new SlapImportProtocols(newDatabase, logger);
		case SLAP_FILE:
			return new SlapFileImportProtocols(newDatabase, logger);
		default:
			return null;
		}
	}

}
