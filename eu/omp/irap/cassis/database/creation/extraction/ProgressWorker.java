/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.extraction;

import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;

import javax.swing.SwingWorker;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.database.creation.DbCreationModel;

/**
 * {@link SwingWorker} handling link between {@link ProgressBarDialog} and</br>
 * export process
 *
 * @author bpenavayre
 *
 */
public class ProgressWorker extends SwingWorker<Boolean, Integer> implements
		ModelListener {

	private File file;
	private DbCreationModel dbModel;
	private ProgressBarDialog progress;


	/**
	 * The default constructor,</br> takes the file in which the user wished to
	 * export the database</br> and the {@link DbCreationModel} containing all
	 * the data
	 *
	 * @param file
	 *            the new file
	 * @param dbModel
	 *            the data container
	 */
	public ProgressWorker(ProgressBarDialog progress, File file,
			DbCreationModel dbModel) {
		this.file = file;
		this.dbModel = dbModel;
		this.progress = progress;
	}

	/**
	 * Create the database.
	 *
	 * @return true if the database creation succeed, false otherwise.
	 */
	@Override
	protected Boolean doInBackground() {
		ExportNewDb export = new ExportNewDb(dbModel, Level.FINE);
		export.addModelListener(this);
		return export.createConnectionWithNewDatabase(file);
	}

	@Override
	protected void done() {
		progress.done();
	}

	@Override
	public void dataChanged(ModelChangedEvent event) {
		publish((int) event.getValue());
	}

	@Override
	protected void process(List<Integer> listOfValues) {
		int sum = 0;
		for (Integer value : listOfValues) {
			sum += value;
		}
		progress.setNewValue(sum);
	}

	/**
	 * Method to use to access to the class
	 *
	 * @param progress The {@link ProgressBarDialog}.
	 * @param dbModel The {@link DbCreationModel}.
	 * @param file The file used to create the new database.
	 * @return true if the database creation succeed, false otherwise.
	 */
	public static boolean startWorker(ProgressBarDialog progress,
			DbCreationModel dbModel, File file) {
		ProgressWorker worker = new ProgressWorker(progress, file, dbModel);
		worker.execute();
		progress.setVisible(true);
		try {
			return worker.get();
		} catch (InterruptedException | ExecutionException e) {
			return false;
		}
	}
}
