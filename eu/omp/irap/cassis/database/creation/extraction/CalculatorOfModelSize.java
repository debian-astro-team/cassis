/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.extraction;

import java.awt.Component;
import java.awt.Dialog.ModalityType;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.UIManager;

import eu.omp.irap.cassis.database.creation.DbCreationModel;
import eu.omp.irap.cassis.database.creation.tools.DatabaseContainer;
import eu.omp.irap.cassis.database.creation.tools.FullMolecule;
import eu.omp.irap.cassis.database.creation.tools.log.SimpleLogger;

/**
 * {@link SwingWorker} calculating the size to set for the future
 * {@link JProgressBar}
 *
 * @author bpenavayre
 *
 */
public final class CalculatorOfModelSize extends SwingWorker<Integer, Void> {

	private static final Logger LOGGER = SimpleLogger
			.getLogger(CalculatorOfModelSize.class.getName());
	private static final String TITLE = "Computation...";
	private static final String MESSAGE = "<html><center>Preparing the exportation,<br>Please wait.<center><html>";
	private static final String INFORMATION_ICON_STRING = "OptionPane.informationIcon";

	private DbCreationModel dbModel;
	private JDialog info;


	private CalculatorOfModelSize(DbCreationModel dbModel, JDialog info) {
		this.dbModel = dbModel;
		this.info = info;
	}

	private void showDialog() {
		info.setVisible(true);
	}

	@Override
	protected Integer doInBackground() {
		int max = 0;

		dbModel.getFilterModel().updateNumberOfTransitions(true);
		for (DatabaseContainer db : dbModel.getDatabaseList()) {
			if (db.isEnabled()) {
				for (FullMolecule molecule : db.getMolecules()) {
					if (!db.getMoleculeState(molecule)
							|| molecule.getTransitionSize() == 0) {
						continue;
					}
					max++;
					max += molecule.getQlog().length;
					max += molecule.getTransitionSize();
				}
			}
		}
		return max;
	}

	@Override
	public void done() {
		info.dispose();
	}

	/**
	 * Access this class through this method
	 *
	 * @param dbModel
	 * @param guiParent
	 * @return
	 */
	public static int calculate(DbCreationModel dbModel, Component guiParent) {
		CalculatorOfModelSize worker = new CalculatorOfModelSize(dbModel,
				createWaitingDialog(guiParent));
		worker.execute();
		worker.showDialog();
		try {
			return worker.get();
		} catch (InterruptedException | ExecutionException e) {
			LOGGER.severe(e.getMessage());
			return 0;
		}
	}

	private static JDialog createWaitingDialog(Component guiParent) {
		JDialog dialog = new JDialog();
		dialog.setTitle(TITLE);
		JPanel panel = new JPanel();
		Icon infoIcon = UIManager.getIcon(INFORMATION_ICON_STRING);
		JLabel label = new JLabel(MESSAGE, infoIcon, SwingUtilities.CENTER);
		label.setIconTextGap(10);
		panel.add(label);
		dialog.setContentPane(panel);
		dialog.setModalityType(ModalityType.APPLICATION_MODAL);
		dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		dialog.pack();
		dialog.setLocationRelativeTo(guiParent);
		dialog.setResizable(false);
		dialog.setVisible(false);
		return dialog;
	}

}
