/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.extraction;

import java.awt.Component;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.database.configuration.DatabaseConfiguration;
import eu.omp.irap.cassis.database.creation.DbCreationModel;

/**
 * Tool class launching the gui export of the databases
 *
 * @author bpenavayre
 *
 */
public final class PrepareExtractionForGui {

	private static final String ALL_READY_EXIST_ERROR = "Database %s already exists.\nDo you want to replace it?";
	private static final String NO_DATABASE = "No active database added, start by adding database(s).";
	private static final String NO_MOLECULE = "No molecule selected. Abording the operation.";


	private PrepareExtractionForGui() {
	}

	/**
	 * Check if the model is not empty</br> then send it to
	 *  {@link PrepareExtractionForGui#selectFile(DbCreationModel, Component)}.
	 *
	 * @param dbModel The data model.
	 * @param guiParent The owner gui.
	 * @return The path of the new database created, or null if there was an error.
	 */
	public static String openSave(DbCreationModel dbModel, Component guiParent) {
		if (dbModel.getDatabaseList().isEmpty()
				|| !dbModel.constainsAtLeastOneActiveDatabase()) {
			JOptionPane.showMessageDialog(guiParent, NO_DATABASE, "Empty",
					JOptionPane.ERROR_MESSAGE, null);
			return null;
		}
		if (!dbModel.constainsAtLeastOneActiveMolecule()) {
			JOptionPane.showMessageDialog(guiParent, NO_MOLECULE, "Empty",
					JOptionPane.ERROR_MESSAGE, null);
			return null;
		}
		return selectFile(dbModel, guiParent);
	}

	/**
	 * Show a dialog to select a file then transmit it with the model to
	 * {@link PrepareExtractionForGui#prepareFile(File, DbCreationModel)
	 * prepareFile}
	 *
	 * @param dbModel the data model.
	 * @param guiParent The owner.
	 * @return The path of the new database created, or null if there was an error.
	 */
	private static String selectFile(DbCreationModel dbModel, Component guiParent) {
		JFileChooser fc = new CassisJFileChooser(
				DatabaseConfiguration.getInstance().getDatabasePath(),
				DatabaseConfiguration.getInstance().getLastFolder("database-export"));
		fc.setDialogType(JFileChooser.SAVE_DIALOG);
		fc.setDialogTitle("Export new database");
		fc.setFileFilter(new FileNameExtensionFilter("SQLite Database (*.db)", "db"));
		File newDb = null;
		if (fc.showSaveDialog(guiParent) == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			String name = file.getAbsolutePath();
			if (!file.getName().endsWith(".db")) {
				name += ".db";
			}
			newDb = new File(name);
			if (newDb.exists()) {
				int answer = JOptionPane.showConfirmDialog(
						guiParent,
						String.format(ALL_READY_EXIST_ERROR,
								newDb.getName()),
						"Replace existing database file?",
						JOptionPane.YES_NO_OPTION);
				if (answer == JOptionPane.NO_OPTION) {
					newDb = null;
				}
			}
			if (newDb != null) {
				DatabaseConfiguration.getInstance().setLastFolder(
						"database-export", file.getParentFile().getAbsolutePath());
			}
		}
		if (newDb != null) {
			if (prepareFile(newDb, dbModel, guiParent)) {
				return newDb.getAbsolutePath();
			}
		}
		return null;
	}

	/**
	 * If the file exist and can be written in create an execute an instance of
	 * {@link ProgressWorker}
	 *
	 * @param file The file used to create the database.
	 * @param dbModel The {@link DbCreationModel}.
	 * @param guiParent The owner.
	 * @return true if the database creation succeed, false otherwise.
	 */
	private static boolean prepareFile(File file, DbCreationModel dbModel,
			Component guiParent) {
		boolean canWriteInFolder = file.getParentFile() != null ? file
				.getParentFile().canWrite() : false;
		if (canWriteInFolder) {
			if (file.exists()) {
				file.delete();
			}
			return ProgressWorker.startWorker(new ProgressBarDialog(dbModel,
					guiParent, ExportNewDb.getLogger()), dbModel, file);
		} else {
			JOptionPane.showMessageDialog(guiParent,
					"Cannot create the file here, try elsewhere.");
		}
		return false;
	}
}
