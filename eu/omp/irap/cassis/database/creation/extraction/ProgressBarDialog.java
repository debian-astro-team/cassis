/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.extraction;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;

import eu.omp.irap.cassis.database.creation.DbCreationModel;
import eu.omp.irap.cassis.database.creation.tools.log.gui.GuiHandler;
import eu.omp.irap.cassis.database.creation.tools.log.gui.LogGui;

/**
 * {@link JDialog} showing progress of the export process and log
 *
 * @author bpenavayre
 *
 */
public class ProgressBarDialog extends JDialog implements ActionListener {

	private static final long serialVersionUID = -67018465528048120L;

	private JProgressBar progress;
	private JButton done;


	/**
	 * Standard gui constructor
	 *
	 * @param model
	 * @param parent
	 * @param logger
	 */
	public ProgressBarDialog(DbCreationModel model, Component parent,
			Logger logger) {
		setTitle("Extraction");
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		progress = new JProgressBar(0, CalculatorOfModelSize.calculate(model,
				parent));
		setContentPane(createAndReturnPanel(logger));
		pack();
		setModalityType(ModalityType.APPLICATION_MODAL);
		setLocationRelativeTo(parent);
	}

	private JPanel createAndReturnPanel(Logger logger) {
		JPanel panel = new JPanel(new BorderLayout());
		panel.setBorder(BorderFactory
				.createTitledBorder("Creating the new Database..."));
		progress.setStringPainted(true);
		progress.setPreferredSize(new Dimension(500, 50));
		panel.add(progress, BorderLayout.PAGE_START);
		LogGui logGui = new GuiHandler(logger).getLogGui();
		JScrollPane scroll = new JScrollPane(logGui);
		scroll.addComponentListener(logGui);
		scroll.setBorder(BorderFactory.createTitledBorder("Log"));
		scroll.setPreferredSize(new Dimension(500, 300));
		JPanel buttomPanel = new JPanel(new BorderLayout());
		buttomPanel.setBorder(BorderFactory.createEmptyBorder(5, 2, 2, 2));
		buttomPanel.add(scroll, BorderLayout.CENTER);
		done = new JButton("Done");
		done.setVisible(false);
		done.addActionListener(this);
		buttomPanel.add(done, BorderLayout.PAGE_END);
		panel.add(buttomPanel, BorderLayout.CENTER);
		return panel;
	}

	/**
	 * Add a value to the {@link JProgressBar}
	 * @param value
	 */
	public void setNewValue(int value) {
		progress.setValue(value + progress.getValue());
	}

	/**
	 * To call once end reached, check if all went well.
	 */
	public void done() {
		if (progress.getValue() != progress.getMaximum()) {
			JOptionPane.showMessageDialog(this,
					"An error occured, the extraction is incomplete.",
					"Extraction failed", JOptionPane.ERROR_MESSAGE);
		}
		done.setVisible(true);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		dispose();
	}
}
