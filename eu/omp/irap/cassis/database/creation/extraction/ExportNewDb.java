/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.extraction;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import eu.omp.irap.cassis.common.events.ListenerManager;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.database.DatabaseConstants;
import eu.omp.irap.cassis.database.access.LineDescriptionDB;
import eu.omp.irap.cassis.database.access.MoleculeDescriptionDB;
import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB;
import eu.omp.irap.cassis.database.creation.DbCreationModel;
import eu.omp.irap.cassis.database.creation.tools.DatabaseContainer;
import eu.omp.irap.cassis.database.creation.tools.FullMolecule;
import eu.omp.irap.cassis.database.creation.tools.log.SimpleLogger;

/**
 * Class responsible for the creation and export of the new database
 *
 * @author bpenavayre
 *
 */
public class ExportNewDb extends ListenerManager {

	private static final Logger LOGGER = SimpleLogger
			.getLogger(ExportNewDb.class.getName());
	private static final String TRANSITIONS_LOG = "%d transitions added for tag %d";
	private static final String PARTI_FUNCT_LOG = "%d lines added in partition function";
	private static final String MOLECULE_LOG = "Added New molecule (%s) with tag %d";
	private static final int BATCH_MAX = 10000;
	private static final String TRANSITIONS = "INSERT INTO `transitions` (`fMHz`,`err`, `aint`, `elow`, `eup`, `igu`, `gamma_self`, `qn`, `itag`, `id_database`) "
			+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String UPDATE_CATDIR = "UPDATE `catdir` SET `id_trans_min`=?, `id_trans_max`=? WHERE tag=?";

	private DbCreationModel dbModel;
	private Connection sqlConnection;
	private List<DatabaseContainer> listOfDatabase;
	private List<String> listOfSources;


	/**
	 * Default constructor
	 *
	 * @param dbModel
	 *            the data model
	 * @param level
	 *            the level of the logger
	 */
	public ExportNewDb(DbCreationModel dbModel, Level level) {
		this.dbModel = dbModel;
		LOGGER.setLevel(level);
	}

	/**
	 * @return the logger of this class
	 */
	public static Logger getLogger() {
		return LOGGER;
	}

	private void setValue(int value) {
		if (value == 0) {
			return;
		}
		fireDataChanged(new ModelChangedEvent("setValue", value));
	}

	private void setLog(String format, Object... args) {
		LOGGER.info(String.format(format, args));
	}

	/**
	 * Export all the transitions
	 *
	 * @param listOfDatabase
	 *            the list of databases
	 * @param listOfSources
	 *            the list of the databases' source
	 */
	private void exportTransitions() {
		int moleculeBatchNumber = 0;

		try (PreparedStatement stmt = sqlConnection
				.prepareStatement(TRANSITIONS);
				PreparedStatement updateStmt = sqlConnection
						.prepareStatement(UPDATE_CATDIR)) {
			int[] transMinMax = { 1, 0 };
			for (DatabaseContainer db : ExportNewDb.getDatabaseEnabled(listOfDatabase)) {
				for (FullMolecule mol : db.getMolecules()) {
					if (!isMoleculeActiveAndFirst(db, mol)
							|| mol.getTransitionSize() == 0) {
						continue;
					}
					transMinMax[1] += addAllTransitionsForATag(stmt,
							mol.getTag());
					updateStmt.setInt(1, transMinMax[0]);
					updateStmt.setInt(2, transMinMax[1]);
					updateStmt.setInt(3, mol.getTag());
					updateStmt.addBatch();
					moleculeBatchNumber++;
					moleculeBatchNumber = executeBatchIfTooBig(
							moleculeBatchNumber, stmt, updateStmt,
							transMinMax[1] - transMinMax[0]);
					transMinMax[0] = transMinMax[1] + 1;
				}
			}
			if (transMinMax[1] - transMinMax[0] % BATCH_MAX != 0) {
				stmt.executeBatch();
			}
			if (moleculeBatchNumber != 0) {
				updateStmt.executeBatch();
			}
			sqlConnection.commit();
		} catch (SQLException e) {
			LOGGER.severe(e.getMessage());
		}
	}

	private boolean isMoleculeActiveAndFirst(DatabaseContainer db,
			SimpleMoleculeDescriptionDB mol) {
		return db.getMoleculeState(mol)
				&& isFirstOccurrenceWithTag(mol.getTag(), mol);
	}

	/**
	 * Check if the the {@link SimpleMoleculeDescriptionDB} given in
	 * parameters</br> is the first one in the list of databases with that
	 * tag.</br> This method serve us to find if we have to add the transitions
	 * with that tag</br> or if we already exported this data
	 *
	 *
	 * @param tag
	 *            the tag of the molecule in which we can find the transitions
	 * @param mol
	 *            the molecule
	 * @return true if this molecule is the first active occurrence with the
	 *         specified tag else false
	 */
	private boolean isFirstOccurrenceWithTag(int tag,
			SimpleMoleculeDescriptionDB mol) {
		for (DatabaseContainer db : ExportNewDb.getDatabaseEnabled(dbModel.getDatabaseList())) {
			for (SimpleMoleculeDescriptionDB otherMol : db.getMolecules()) {
				if (!db.getMoleculeState(otherMol)) {
					continue;
				}
				if (otherMol.equals(mol)) {
					return true;
				}
				if (otherMol.getTag() == tag) {
					return false;
				}
			}
		}
		return false;
	}

	/**
	 * Adds all the transitions with the specified molecule tag
	 *
	 * @param stmt
	 *            the {@link PreparedStatement} for the transitions
	 * @param listOfSources
	 *            the list of the databases' sources string
	 * @param tag
	 *            the tag of the transitions to add
	 * @return the number of transitions added
	 * @throws SQLException
	 */
	private int addAllTransitionsForATag(PreparedStatement stmt, int tag) {
		int count = 0;

		for (DatabaseContainer db : ExportNewDb.getDatabaseEnabled(dbModel.getDatabaseList())) {
			for (FullMolecule mol : db.getMolecules()) {
				if (mol.getTag() != tag || !db.getMoleculeState(mol)) {
					continue;
				}
				for (LineDescriptionDB line : mol.getTransitions()) {
					if (!dbModel.getFilterModel().checkIfTransitionIsOkWithAllFilters(
							line, mol.getSource())) {
						continue;
					}
					try {
						updateTransitionStatment(stmt, line,
								listOfSources.indexOf(mol.getSource()));
						count++;
						if (count % BATCH_MAX == 0) {
							stmt.executeBatch();
							stmt.clearBatch();
						}
					} catch (SQLException e) {
						LOGGER.severe(e.getMessage());
					}
				}
			}
		}
		setLog(TRANSITIONS_LOG, count, tag);
		return count;
	}

	/**
	 * Take the statement of transitions, the current transition and the index
	 * of the source</br> and fill with the content of the transition the
	 * statement.
	 *
	 * @param stmt
	 *            the transition's statement
	 * @param line
	 *            the transition
	 * @param sourceIndex
	 *            the index of the source
	 * @throws SQLException
	 */
	private void updateTransitionStatment(PreparedStatement stmt,
			LineDescriptionDB line, int sourceIndex) throws SQLException {
		stmt.setDouble(1, line.getFrequency());
		stmt.setDouble(2, line.getError());
		stmt.setDouble(3, line.getAint());
		stmt.setDouble(4, line.getElow());
		stmt.setDouble(5, line.getEup());
		stmt.setInt(6, line.getIgu());
		stmt.setDouble(7, line.getGammaSelf());
		stmt.setString(8, line.getQuanticNumbers());
		stmt.setInt(9, line.getTag());
		stmt.setInt(10, 1 + sourceIndex);
		stmt.addBatch();
		setValue(1);
	}

	/**
	 * Execute and clean both given {@link PreparedStatement}</br> if their
	 * number of batch is too big
	 *
	 * @param moleculeBatchNumber
	 *            number of batch for the update statement
	 * @param stmt
	 *            the statement for the transitions
	 * @param updateStmt
	 *            the statement to update the molecules
	 * @param lastMoleculeTansitionSize
	 *            the number of transitions added for the current molecule
	 * @return the new value of moleculeBatchNumber
	 * @throws SQLException
	 */
	private int executeBatchIfTooBig(int moleculeBatchNumber,
			PreparedStatement stmt, PreparedStatement updateStmt,
			int lastMoleculeTansitionSize) throws SQLException {
		if (moleculeBatchNumber / BATCH_MAX == 0) {
			return moleculeBatchNumber;
		}
		updateStmt.executeBatch();
		updateStmt.clearBatch();
		if (lastMoleculeTansitionSize % BATCH_MAX != 0) {
			stmt.executeBatch();
			stmt.clearBatch();
		}
		return 0;
	}

	/**
	 * Takes one molecule, gets its partition function</br> and add it to the
	 * statement.
	 *
	 * @param molecule
	 *            the molecule containing the partition function
	 * @param stmt
	 *            the {@link PreparedStatement}
	 * @throws SQLException
	 */
	private void exportPartitionFunctionOfOneMoleculeDescription(
			MoleculeDescriptionDB molecule, PreparedStatement stmt)
			throws SQLException {
		double[] temp = null;
		double[] qlog = null;
		temp = molecule.getTemp();
		qlog = molecule.getQlog();
		int newElements = 0;
		for (int i = 0; i < temp.length && i < qlog.length; i++) {
			int index = 1;
			stmt.setDouble(index++, temp[i]);
			stmt.setDouble(index++, qlog[i]);
			stmt.setInt(index, molecule.getTag());
			stmt.addBatch();
			newElements++;
		}
		setValue(newElements);
		setLog(PARTI_FUNCT_LOG, newElements);
	}

	/**
	 * This method browse the list of database and in those their list of
	 * molecules.</br> If we find a molecule with the tag specified that has a
	 * smaller general index</br> (earlier database or in same earlier)</br>
	 * this method stops.</br> Other wise we call
	 * {@link #exportPartitionFunctionOfOneMoleculeDescription(MoleculeDescriptionDB, PreparedStatement)
	 * exportPartitionFunctionOfOneMoleculeDescription}
	 *
	 * @param placeDatabase
	 *            the index of current the database in databaseList
	 * @param placeMol
	 *            the index of current the molecule in the its
	 *            {@link DatabaseContainer}
	 * @param tag
	 *            the tag of the current molecule
	 * @param stmt
	 *            the {@link PreparedStatement}
	 * @throws SQLException
	 */
	private void exportPartiFunction(int placeDatabase, int placeMol, int tag,
			PreparedStatement stmt) throws SQLException {
		int newPlaceDatabase = 0;

		for (DatabaseContainer db : ExportNewDb.getDatabaseEnabled(listOfDatabase)) {
			int newPlaceMol = 0;
			for (SimpleMoleculeDescriptionDB simple : db.getMolecules()) {
				if (simple.getTag() == tag && db.getMoleculeState(simple)
						&& simple instanceof MoleculeDescriptionDB) {
					if (newPlaceDatabase < placeDatabase
							|| (newPlaceDatabase == placeDatabase && newPlaceMol < placeMol)) {
						return;
					}
					exportPartitionFunctionOfOneMoleculeDescription(
							(MoleculeDescriptionDB) simple, stmt);
				}
				newPlaceMol++;
			}
			newPlaceDatabase++;
		}
	}

	/**
	 * @param catdirStmt
	 *            the catdir {@link PreparedStatement}
	 * @param partiFuncStmt
	 *            the partion function {@link PreparedStatement}
	 * @param listDatabase
	 *            the list of all the database
	 * @param listOfSources
	 *            the list of sources' names
	 * @throws SQLException
	 */
	private void fillCatdirAndPartitionFunctionStatements(
			PreparedStatement catdirStmt, PreparedStatement partiFuncStmt)
			throws SQLException {
		int dbIndex = 0;
		int moleculeIndex;
		int idDatabase;

		for (DatabaseContainer db : listOfDatabase) {
			if (db.isEnabled()) {
				moleculeIndex = 0;
				for (FullMolecule molecule : db.getMolecules()) {
					if (!db.getMoleculeState(molecule)
							|| molecule.getTransitionSize() == 0) {
						continue;
					}
					idDatabase = 0;
					while (idDatabase < listOfSources.size()
							&& !listOfSources.get(idDatabase).equals(
									molecule.getSource())) {
						idDatabase++;
					}
					idDatabase++;
					setLog(MOLECULE_LOG, molecule.getName(), molecule.getTag());
					exportPartiFunction(dbIndex, moleculeIndex,
							molecule.getTag(), partiFuncStmt);
					catdirStmt.setInt(1, molecule.getTag());
					catdirStmt.setString(2, molecule.getName());
					catdirStmt.setDouble(3, molecule.getMolecularMass());
					catdirStmt.setInt(4, idDatabase);
					catdirStmt.addBatch();
					setValue(1);

				}
				moleculeIndex++;
			}
			dbIndex++;
		}
	}

	/**
	 * Export the list of sources
	 *
	 * @param listOfSources
	 *            list of sources
	 * @param listOfUrls
	 *            list of sources' url
	 */
	private void exportListOfSources(List<String> listOfSources,
			List<String> listOfUrls) {
		String start = "INSERT INTO `cassis_databases` (`name`, `url`) VALUES (?, ?)";

		try (PreparedStatement stmt = sqlConnection.prepareStatement(start)) {
			int sourceIndex = 0;
			for (String source : listOfSources) {
				stmt.setString(1, source);
				stmt.setString(2, listOfUrls.get(sourceIndex));
				stmt.addBatch();
				sourceIndex++;
			}
			stmt.executeBatch();
			stmt.clearBatch();
			sqlConnection.commit();
		} catch (SQLException e) {
			LOGGER.severe(e.getMessage());
		}
	}

	/**
	 * Get the list of all the sources, calls
	 * {@link #exportListOfSources(List, List) exportListOfSources} then return
	 * it
	 *
	 * @return the list of all the sources
	 */
	private void getAndExportOfSourcesList() {
		listOfSources = new ArrayList<String>();
		List<String> listOfUrls = new ArrayList<String>();
		int moleculeIndex;
		for (DatabaseContainer db : ExportNewDb.getDatabaseEnabled(dbModel.getDatabaseList())) {
			List<FullMolecule> listMol = db.getMolecules();
			moleculeIndex = 0;
			for (SimpleMoleculeDescriptionDB molecule : listMol) {
				if (db.getMoleculeState(moleculeIndex)
						&& !listOfSources.contains(molecule.getSource())) {
					listOfSources.add(molecule.getSource());
					listOfUrls.add(db.getPath());
				}
				moleculeIndex++;
			}
		}
		if (!listOfSources.isEmpty()) {
			exportListOfSources(listOfSources, listOfUrls);
		}
	}

	/**
	 * Export everything
	 *
	 * @param listOfSources the list of all the databases sources
	 * @return true if the export worked, false otherwise.
	 */
	private boolean exportAll() {
		getAndExportOfSourcesList();
		String catdirStmtString = "INSERT INTO `catdir` "
				+ "(`tag`,`name`, `molecular_mass`, `id_database`) "
				+ "VALUES (?, ?, ?, ?)";
		String partiFuncStmtString = "INSERT INTO `cassis_parti_funct` (`temp`, `qlog`, `tag`) "
				+ "VALUES (?, ?, ?)";
		listOfDatabase = dbModel.getDatabaseList();
		try (PreparedStatement catdirStmt = sqlConnection
				.prepareStatement(catdirStmtString);
				PreparedStatement partiFuncStmt = sqlConnection
						.prepareStatement(partiFuncStmtString);) {
			fillCatdirAndPartitionFunctionStatements(catdirStmt, partiFuncStmt);
			catdirStmt.executeBatch();
			catdirStmt.clearBatch();
			partiFuncStmt.executeBatch();
			partiFuncStmt.clearBatch();
			exportTransitions();
			setDatabaseVersion(sqlConnection);
			sqlConnection.commit();
			return true;
		} catch (SQLException e) {
			LOGGER.severe(e.getMessage());
		}
		return false;
	}

	/**
	 * Create the tables in the new database
	 */
	private void createTables() {
		try (Statement stmt = sqlConnection.createStatement()) {
			stmt.executeUpdate("CREATE TABLE `catdir` "
					+ "(`tag` INT     NOT NULL, `name` varchar(30),"
					+ "`gamma_self_mean` double NOT NULL default '1',"
					+ "`molecular_mass` double NOT NULL default '0',"
					+ "`id_database` int NOT NULL default '0',"
					+ "`id_trans_min` int NOT NULL default '-1',"
					+ "`id_trans_max` int NOT NULL default '-1');"
					+ "CREATE TABLE `cassis_parti_funct` ("
					+ "`id_qlog` INTEGER PRIMARY KEY AUTOINCREMENT,"
					+ "`temp` double NOT NULL,`qlog` double NOT NULL,"
					+ "`tag` int NOT NULL) ;"
					+ "CREATE TABLE `transitions` ("
					+ "`id_transitions` INTEGER PRIMARY KEY AUTOINCREMENT,"
					+ "`fMHz` double default '0',"
					+ "`err` float NOT NULL default '0',"
					+ "`aint` double NOT NULL default '0',"
					+ "`elow` double NOT NULL default '0',"
					+ "`eup` double NOT NULL default '0',"
					+ "`igu` int(4) NOT NULL default '0',"
					+ "`itag` int NOT NULL,`qn` varchar(100) default '',"
					+ "`id_database` int(11) NOT NULL default '0',"
					+ "`gamma_self` double NOT NULL default '0',"
					+ "`gamma_self_error` double NOT NULL default '0') ;"
					+ "CREATE TABLE `cassis_databases` ("
					+ "`id` INTEGER PRIMARY KEY AUTOINCREMENT,"
					+ "`name` varchar(10) NOT NULL default '',"
					+ "`url` varchar(255) NOT NULL default '',"
					+ "`catalog` varchar(255) NOT NULL default '',"
					+ "`lastUpdate` datetime NOT NULL default '0000-00-00 00:00:00') ;");
			stmt.executeUpdate("CREATE INDEX fMHz ON `transitions` (fMHz ASC);");
			stmt.clearBatch();
			stmt.close();
		} catch (SQLException e) {
			LOGGER.severe(e.getMessage());
		}
	}

	/**
	 * Takes an empty file create the connection with the file</br> create its
	 * tables and then fill it through {@link #exportAll() exportAll}
	 *
	 * @param file the new database's file
	 * @return true if the database creation succeed, false otherwise.
	 */
	public boolean createConnectionWithNewDatabase(File file) {
		try {
			Class.forName("org.sqlite.JDBC");
			try (Connection sqlConnection = DriverManager
					.getConnection("jdbc:sqlite:" + file.getAbsolutePath())) {
				this.sqlConnection = sqlConnection;
				createTables();
				sqlConnection.setAutoCommit(false);
				return exportAll();
			} catch (SQLException e) {
				LOGGER.severe("SQLException, couldn't create connection with the file, extraction aborted.");
				LOGGER.severe(e.getMessage());
				return false;
			}
		} catch (ClassNotFoundException e) {
			LOGGER.severe("SQLite driver JDBC not found, extraction aborted.");
			LOGGER.severe(e.getMessage());
			return false;
		}
	}

	/**
	 * Create the new database without creating a JProgressBar and SwingWorker.
	 * This is the correct method to use when using Jython.
	 *
	 * @param path
	 *            path of the new file to create and fill
	 */
	public void launchExtraction(String path) {
		String correctedPath = path.endsWith(".db") ? path : path + ".db";
		createConnectionWithPath(correctedPath);
	}

	private boolean createConnectionWithPath(String path) {
		LOGGER.info("Tying to create database " + path);
		return createConnection(path,
				"Cannot create the file here, wait...");
	}

	private boolean createConnection(String path, String severe) {
		File file = new File(path);
		boolean canWriteInFolder = file.getParentFile() != null ? file
				.getParentFile().canWrite() : false;
		if (canWriteInFolder) {
			if (file.exists()) {
				LOGGER.info("The file exist, replacing...");
				file.delete();
			}
			return createConnectionWithNewDatabase(file);
		}
		LOGGER.severe(severe);
		return false;
	}

	/**
	 * Return the list of enabled database from the given list.
	 *
	 * @param dbList The list from which we search enabled database
	 * @return the list of enabled database.
	 */
	private static List<DatabaseContainer> getDatabaseEnabled(List<DatabaseContainer> dbList) {
		List<DatabaseContainer> enabledDb = new ArrayList<DatabaseContainer>();
		for (DatabaseContainer db : dbList) {
			if (db.isEnabled()) {
				enabledDb.add(db);
			}
		}
		return enabledDb;
	}

	/**
	 * Set the database version.
	 *
	 * @param connection The connection of the database.
	 */
	private void setDatabaseVersion(Connection connection) {
		String sql = "PRAGMA user_version = \"" + DatabaseConstants.DB_VERSION + "\";";
		try (Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery(sql)) {
		} catch (SQLException e) {
		}
	}
}
