/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.tools;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import eu.omp.irap.cassis.database.access.LineDescriptionDB;

/**
 * Extends {@link AbstractTableModel},</br> This class is the model to use when
 * adding a new tab to the JTabbedPane of</br> the DBCreationPanel we need to
 * create the associated JTable.
 *
 * @author bpenavayre, jglorian
 *
 */

public abstract class TableDataModel extends AbstractTableModel implements
		Comparator<LineDescriptionDB> {

	private static final long serialVersionUID = -2134820309387593217L;
	private static final String[] COLUMNS = { "Tag", "Name of specie", "Database","Eup min", "Eup max",
		"Aij min", "Aij max", "Freq min", "Freq max",
		"Number of transitions", "Selected" };

	private static final int INDICE_TAG = 0;
	private static final int INDICE_NAME = 1;
	private static final int INDICE_NAME_DATABASE = 2;
	private static final int INDICE_EUP_MIN = 3;
	private static final int INDICE_EUP_MAX = 4;
	private static final int INDICE_AIJ_MIN = 5;
	private static final int INDICE_AIJ_MAX = 6;
	private static final int INDICE_FREQ_MIN = 7;
	private static final int INDICE_FREQ_MAX = 8;
	private static final int INDICE_NUMBER_SELECTIONS = 9;
	public static final int INDICE_SELECTION = 10;

	enum LineDescriptionDBParameters {EUP, AIJ, FMHZ}

	private DatabaseContainer database;
	private LineDescriptionDBParameters type;


	/**
	 * Create the Model by using the values contained in the database and set
	 * the different listeners with dbControl
	 *
	 * @param database
	 *            the container of the data
	 */
	public TableDataModel(DatabaseContainer database) {
		this.database = database;
		type = LineDescriptionDBParameters.EUP;
	}

	@Override
	public abstract void setValueAt(Object value, int row, int col);

	@Override
	public int getRowCount() {
		if (database.getMolecules() != null) {
			return database.getMolecules().size();
		}
		return 0;
	}

	@Override
	public int getColumnCount() {
		return COLUMNS.length;
	}

	@Override
	public String getColumnName(int col) {
		return COLUMNS[col];
	}

	@Override
	public Object getValueAt(int row, int col) {
		FullMolecule mol = database.getMolecules().get(row);
		switch (col) {
		case INDICE_TAG:
			return mol.getTag();
		case INDICE_NAME:
			return mol.getName();
		case INDICE_NAME_DATABASE:
			return mol.getSource();
		case INDICE_EUP_MIN:
		case INDICE_EUP_MAX:
			return getMinOrMax(mol.getTransitions(), LineDescriptionDBParameters.EUP, col == INDICE_EUP_MIN).getEup();
		case INDICE_AIJ_MIN:
		case INDICE_AIJ_MAX:
			return getMinOrMax(mol.getTransitions(), LineDescriptionDBParameters.AIJ, col == INDICE_AIJ_MIN).getAint();
		case INDICE_FREQ_MIN:
		case INDICE_FREQ_MAX:
			return getMinOrMax(mol.getTransitions(), LineDescriptionDBParameters.FMHZ, col == INDICE_FREQ_MIN).getFrequency();
		case INDICE_NUMBER_SELECTIONS:
			int result = mol.getTransitionSize();
			if (result == 0) {
				setValueAt(false, row, INDICE_SELECTION);
			}
			return result;
		case INDICE_SELECTION:
			return database.getMoleculeState(mol);
		}
		return null;
	}


	private LineDescriptionDB getMinOrMax(List<LineDescriptionDB> transitions,
			LineDescriptionDBParameters eup, boolean isMin) {
		this.type = eup;

		if (isMin) {

			return Collections.min(transitions, this);
		}
		return Collections.max(transitions, this);
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		return col == INDICE_SELECTION;
	}

	@Override
	public Class<?> getColumnClass(int c) {
		switch (c) {
		case INDICE_TAG:
		case INDICE_NUMBER_SELECTIONS:
			return Integer.class;
		case INDICE_NAME:
		case INDICE_NAME_DATABASE:
			return String.class;
		case INDICE_SELECTION:
			return Boolean.class;
		default:
			return Double.class;
		}
	}

	/**
	 * Compare two {@link LineDescriptionDB}s using the value of {@link #type}</br>
	 * to get the fields to use to compare.
	 */
	@Override
	public int compare(LineDescriptionDB objectOne, LineDescriptionDB objectTwo) {
		if (type == LineDescriptionDBParameters.EUP) {
			return Double.compare(objectOne.getEup(), objectTwo.getEup());
		} else if (type == LineDescriptionDBParameters.AIJ) {
			return Double.compare(objectOne.getAint(), objectTwo.getAint());
		} else {
			return Double.compare(objectOne.getFrequency(),
					objectTwo.getFrequency());
		}
	}

	public void removeDatabaseReference() {
		database = null;
	}
}
