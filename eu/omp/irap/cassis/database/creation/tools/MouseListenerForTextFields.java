/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.tools;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.text.DefaultEditorKit;

/**
 * Use {@link ActionMap} to create a {@link JPopupMenu}</br>
 * and show it when the component that has as {@link MouseListener}
 * this instance is clicked using the right button.
 *
 * @author bpenavayre
 *
 */
public class MouseListenerForTextFields extends MouseAdapter {

	private JPopupMenu menu;


	/**
	 * Create the {@link JPopupMenu} using an {@link ActionMap}
	 *
	 * @param map
	 */
	public MouseListenerForTextFields(ActionMap map) {
		menu = new JPopupMenu();
		Action tempAction = map.get(DefaultEditorKit.copyAction);
		tempAction.putValue(Action.NAME, "Copy");
		menu.add(tempAction);
		tempAction = map.get(DefaultEditorKit.cutAction);
		tempAction.putValue(Action.NAME, "Cut");
		menu.add(tempAction);
		tempAction = map.get(DefaultEditorKit.pasteAction);
		tempAction.putValue(Action.NAME, "Paste");
		menu.add(tempAction);
		tempAction = map.get(DefaultEditorKit.selectAllAction);
		tempAction.putValue(Action.NAME, "Select all");
		menu.add(tempAction);
	}

	/**
	 * Show the {@link JPopupMenu} if right mouse button is clicked.
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		if (SwingUtilities.isRightMouseButton(e)) {
			menu.show((Component) e.getSource(), e.getX(), e.getY());
		}
	}

}
