/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.tools;

import java.awt.Color;
import java.awt.Component;
import java.awt.Insets;

import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

/**
 * Final tool class</br> use this class to stock useful public static methods
 * </br> to use for graphical purpose.
 *
 * @author bpenavayre
 *
 */
public final class ViewTool {

	private ViewTool() {
	}

	/**
	 * Create and return a {@link LineBorder} with custom insets
	 *
	 * @param c the color of the line border
	 * @param top
	 * @param left
	 * @param bottom
	 * @param right
	 * @return a LineBorder with changed insets
	 */
	public static Border lineBorderInsets(final Color c, final int top,
			final int left, final int bottom, final int right) {
		return new LineBorder(c) {

			private static final long serialVersionUID = 2873072362517270293L;

			@Override
			public Insets getBorderInsets(Component c) {
				return new Insets(top, left, bottom, right);
			}
		};
	}

	/**
	 * Create and return a new {@link TitledBorder} wit custom insets
	 *
	 * @param title the title of the Border
	 * @param top
	 * @param left
	 * @param bottom
	 * @param right
	 * @return a TitleBorder with changed insets
	 */
	public static Border titleBorderInsets(final String title, final int top,
			final int left, final int bottom, final int right) {
		return new TitledBorder(title) {

			private static final long serialVersionUID = -8553512648015548204L;

			@Override
			public Insets getBorderInsets(Component c) {
				return new Insets(top, left, bottom, right);
			}
		};
	}
}
