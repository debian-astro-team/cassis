/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.tools;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.database.access.MoleculeDescriptionDB;
import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB;
import eu.omp.irap.cassis.database.configuration.DatabaseConfiguration;
import eu.omp.irap.cassis.database.creation.tools.log.SimpleLogger;

/**
 * Allow the user to edit the function of partition for a specific molecule
 *
 * @author bpenavayre
 *
 */

public class PartiFunctChangeFrame extends JFrame implements ActionListener {

	private static final long serialVersionUID = 8323912023615731035L;
	private static final String ADD = "Add";
	private static final String DELETE = "Delete";
	private static final String EXPORT = "Export";
	private static final String VALIDATE = "Validate";

	private JTable table;
	private MoleculeDescriptionDB molecule;
	private Logger logger;


	/**
	 * Calls the super constructor ({@link JFrame}) and fill itself with the
	 * default content
	 *
	 * @param molecule
	 *            where to find and change the values
	 */
	public PartiFunctChangeFrame(MoleculeDescriptionDB molecule) {
		super("Edit partition function for the species " + molecule.getName() + " (" + molecule.getTag() + ')');
		logger = SimpleLogger.getLogger(getClass().getName());
		setVisible(false);
		this.molecule = molecule;
		PartiFunctTableModel model = new PartiFunctTableModel(
				molecule.getQlog(), molecule.getTemp());
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setContent(model);
		pack();
	}

	/**
	 * Static method to use to use this class
	 *
	 * @param molecule the molecule to edit
	 */
	public static PartiFunctChangeFrame changeFunctionPartInterface(
			SimpleMoleculeDescriptionDB molecule) {
		if (!(molecule instanceof MoleculeDescriptionDB)) {
			return null;
		}
		MoleculeDescriptionDB molDB = (MoleculeDescriptionDB) molecule;
		PartiFunctChangeFrame frame = new PartiFunctChangeFrame(molDB);
		frame.setVisible(true);
		return frame;
	}

	private void setContent(DefaultTableModel model) {
		JPanel mainPanel = new JPanel(new BorderLayout(0, 5));

		mainPanel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createLineBorder(Color.BLACK),
				BorderFactory.createEmptyBorder(15, 10, 5, 10)));
		JScrollPane scroll = createAndSetPanel(model);
		mainPanel.add(scroll, BorderLayout.CENTER);
		createAndSetBottomPanel(mainPanel);
		mainPanel.setPreferredSize(new Dimension(600, 250));
		setContentPane(mainPanel);
	}

	private JScrollPane createAndSetPanel(DefaultTableModel model) {
		table = new JTable(model);
		table.setAutoCreateRowSorter(true);
		table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		DefaultTableCellRenderer render = new DefaultTableCellRenderer();
		render.setHorizontalAlignment(SwingConstants.CENTER);
		for (int i = 0; i < table.getColumnCount(); i++) {
			table.getColumnModel().getColumn(i).setCellRenderer(render);
		}
		table.setRowSelectionAllowed(true);
		table.setColumnSelectionAllowed(false);
		table.setOpaque(true);
		table.setFillsViewportHeight(true);
		return new JScrollPane(table);
	}

	private void createAndSetBottomPanel(JPanel mainPanel) {
		JPanel bottomPanel = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.weightx = 1;
		c.weighty = 6;
		c.gridwidth = 1;
		c.anchor = GridBagConstraints.LINE_START;
		bottomPanel.add(new JLabel("Line : "));
		JButton button = new JButton(ADD);
		button.addActionListener(this);
		bottomPanel.add(button, c);
		c.weightx = GridBagConstraints.EAST;
		button = new JButton(DELETE);
		button.addActionListener(this);
		bottomPanel.add(button, c);
		c.anchor = GridBagConstraints.LINE_END;
		c.weightx = GridBagConstraints.RELATIVE;
		c.insets.right = 15;
		button = new JButton(EXPORT);
		button.addActionListener(this);
		bottomPanel.add(button, c);
		c.insets.right = 0;
		c.weightx = GridBagConstraints.REMAINDER;
		button = new JButton(VALIDATE);
		button.addActionListener(this);
		bottomPanel.add(button, c);
		mainPanel.add(bottomPanel, BorderLayout.PAGE_END);
	}

	/**
	 * Handles the action event for the buttons bottom panel
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton button = (JButton) e.getSource();
		if (button.getText().equals(DELETE)) {
			deleteLine();
		} else if (button.getText().equals(ADD)) {
			addLine();
		} else if (button.getText().equals(VALIDATE)) {
			validateClicked();
		} else {
			exportClicked();
		}
	}

	private void deleteLine() {
		int[] row = table.getSelectedRows();
		if (row == null || row.length == 0) {
			JOptionPane.showMessageDialog(this,
					"Please select at least one line first");
			return;
		}
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		int difference = 0;
		for (int rowIte : row) {
			model.removeRow(rowIte - difference);
			difference++;
		}
	}

	private void addLine() {
		if (table.getModel().getRowCount() > 250) {
			JOptionPane.showMessageDialog(this,
					"You can't have a list of more than 250 lines.");
			return;
		}
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		Object[] newLine = { 0.0, 0.0 };
		model.addRow(newLine);
	}

	private void validateClicked() {
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		int size = model.getRowCount();
		double[] qlog = new double[size];
		double[] temp = new double[size];
		for (int i = 0; i < size; i++) {
			qlog[i] = (double) model.getValueAt(i, 0);
			temp[i] = (double) model.getValueAt(i, 1);
		}
		molecule.setQlog(qlog);
		molecule.setTemp(temp);
		dispose();
	}

	private void exportClicked() {
		JFileChooser fc = new CassisJFileChooser(
				DatabaseConfiguration.getInstance().getPartionMolePath(),
				DatabaseConfiguration.getInstance().getLastFolder("partition-mole"));
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		if (fc.showDialog(this, "Save partition function") == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			if (file.exists()
					&& JOptionPane.showConfirmDialog(this,
							"File already exist, replace it?", "Replace",
							JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION) {
				file.delete();
			}
			try {
				file.createNewFile();
			} catch (IOException e) {
				logger.warning(e.getMessage());
			}
			fillTheFile(file);
			DatabaseConfiguration.getInstance().setLastFolder(
					"partition-mole", fc.getSelectedFile().getParent());
		}
	}

	private void fillTheFile(File file) {
		try (BufferedWriter out = new BufferedWriter(new FileWriter(file))) {
			out.write("//" + molecule.getSource() + "\t" + molecule.getName()+"\n"+
					  "//Temp(K)\tlog(Z)\n");
			PartiFunctTableModel model = (PartiFunctTableModel) table.getModel();
			double val = 0;
			int len = model.getRowCount();
			for (int i = 0; i < len; i++) {
				val = model.getTemp()[len-i-1];
				out.write(Double.toString(val) + '\t');
				val = model.getQlog()[len-i-1];
				out.write(Double.toString(val));
				if (i + 1 != len) {
					out.write(System.lineSeparator());
				}
			}
			JOptionPane.showMessageDialog(this, "Extraction successful.");
		} catch (IOException e) {
			logger.warning(e.getMessage());
		}
	}
}
