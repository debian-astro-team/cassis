/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.tools;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * A simple panel containing a {@link JCheckBox}, a {@link JLabel} and a
 * {@link JButton} Extends {@link JPanel}
 *
 * @author bpenavayre
 *
 */
public class ButtonTabComponent extends JPanel {

	private static final long serialVersionUID = -2415417639719001913L;
	private JButton buttonDelete;
	private JCheckBox enableCHeckBox;
	private JLabel label;


	/**
	 * Create the default tab component for the DbCreationPanel's
	 * JTabbedPane</br> with title the string "title", a checkBox and a delete
	 * button
	 *
	 * @param title
	 */
	public ButtonTabComponent(String title) {
		super(new BorderLayout());
		setOpaque(false);

		enableCHeckBox = new JCheckBox();
		enableCHeckBox.setSelected(true);
		enableCHeckBox.setOpaque(false);
		add(enableCHeckBox, BorderLayout.WEST);

		label = new JLabel(title);
		setName(title);

		add(label, BorderLayout.CENTER);
		buttonDelete = new JButton("X");
		buttonDelete.setOpaque(false);
		Dimension buttonSize = new Dimension(20, 20);
		buttonDelete.setSize(buttonSize);
		buttonDelete.setPreferredSize(buttonSize);
		buttonDelete.setMargin(new Insets(2, 2, 2, 2));
		add(buttonDelete, BorderLayout.EAST);
	}

	/**
	 * Change the title of the component with the parameter
	 *
	 * @param newLabelText
	 */
	public void changeLabelText(String newLabelText) {
		label.setText(newLabelText);
	}

	/**
	 * return the state of the comboBox
	 *
	 * @return
	 */
	public boolean isActivated() {
		return enableCHeckBox.isSelected();
	}

	/**
	 * return the comboBox
	 *
	 * @return
	 */
	public JCheckBox getEnableCheckBox() {
		return enableCHeckBox;
	}

	/**
	 * Change the state of the comboBox
	 *
	 * @param newEnableState
	 */
	public void setEnableState(boolean newEnableState) {
		enableCHeckBox.setSelected(newEnableState);
	}

	/**
	 * return the delete button
	 *
	 * @return
	 */
	public JButton getButtonDelete() {
		return buttonDelete;
	}

	/**
	 * remove all component's listeners
	 */
	public void removeAllListeners() {
		removeActionListeners();
		removeMouseListeners();
	}

	private void removeActionListeners() {
		ActionListener[] als = buttonDelete.getActionListeners();
		for (ActionListener al : als) {
			buttonDelete.removeActionListener(al);
		}
		als = enableCHeckBox.getActionListeners();
		for (ActionListener al : als) {
			enableCHeckBox.removeActionListener(al);
		}
	}

	private void removeMouseListeners() {
		MouseListener[] mls = buttonDelete.getMouseListeners();
		for (MouseListener ml : mls) {
			buttonDelete.removeMouseListener(ml);
		}
		mls = enableCHeckBox.getMouseListeners();
		for (MouseListener ml : mls) {
			enableCHeckBox.removeMouseListener(ml);
		}
	}
}
