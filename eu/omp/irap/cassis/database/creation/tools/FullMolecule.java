/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.tools;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import eu.omp.irap.cassis.database.access.DataBaseConnection;
import eu.omp.irap.cassis.database.access.LineDescriptionDB;
import eu.omp.irap.cassis.database.access.MoleculeDescriptionDB;
import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB;
import eu.omp.irap.cassis.database.access.UnknowMoleculeException;

/**
 * A molecule description containing all the data required to create a database.
 *
 * @author bpenavayre
 *
 */
public class FullMolecule extends MoleculeDescriptionDB {

	private static final String ERROR_ON_MOL = "Error while importing the full content of the molecule of tag %d.";
	private static final String ERROR_TRANSITION = "Error while importing the list of transitions for the molecule of tag %d.";

	private List<LineDescriptionDB> transitions;
	private int numberOfTransitions;


	/**
	 * Creates a copy of the {@link SimpleMoleculeDescriptionDB} given in
	 * parameters
	 *
	 * @param mol
	 */
	public FullMolecule(SimpleMoleculeDescriptionDB mol) {
		super(mol.getTag(), mol.getName(), null, null);
		setMoleculeID(mol.getMoleculeID());
		setSource(mol.getSource());
		setMolecularMass(mol.getMolecularMass());
		setType(mol.getType());
		if (mol instanceof MoleculeDescriptionDB) {
			setQlogAienstein300(((MoleculeDescriptionDB) mol).getQlogAienstein300());
		}

		numberOfTransitions = 0;
	}

	/**
	 * Retrieve the partition function using the {@link DataBaseConnection}
	 *
	 * @param connection
	 * @param logger
	 * @return success or failure
	 */
	public boolean setPartiFunc(DataBaseConnection connection, Logger logger) {
		MoleculeDescriptionDB fullMol = null;
		try {
			fullMol = connection.getMoleculeDescriptionDB(this);
		} catch (UnknowMoleculeException e) {
			log(logger, String.format(ERROR_ON_MOL, getTag()), Level.SEVERE);
			log(logger, e.getMessage(), Level.SEVERE);
			return false;
		}
		if (fullMol == null) {
			log(logger, String.format(ERROR_ON_MOL, getTag()), Level.SEVERE);
			return false;
		}
		setQlog(fullMol.getQlog());
		setTemp(fullMol.getTemp());
		return true;
	}

	/**
	 * Retrieve the transitions using the {@link DataBaseConnection}
	 *
	 * @param connection
	 * @param logger
	 * @return
	 */
	public boolean setTransitions(DataBaseConnection connection, Logger logger) {
		transitions = connection.getLineDescriptionDB(this, 0.0,
				Double.MAX_VALUE);
		if (transitions == null || transitions.isEmpty()) {
			log(logger, String.format(ERROR_TRANSITION, getTag()), Level.SEVERE);
			return false;
		}
		numberOfTransitions = transitions.size();
		return true;
	}

	private void log(Logger logger, String message, Level level) {
		if (logger == null) {
			return;
		}
		logger.log(level, message);
	}

	/**
	 * @return the list of transitions
	 */
	public List<LineDescriptionDB> getTransitions() {
		return transitions;
	}

	/**
	 * @return the number of active transitions (non-enabled because of</br> for
	 *         instance filters, etc...)
	 */
	public int getTransitionSize() {
		return numberOfTransitions;
	}

	/**
	 * Change the {@link #numberOfTransitions} with a new value
	 *
	 * @param newSize
	 */
	public void setTransitionSize(int newSize) {
		numberOfTransitions = newSize;
	}

}
