/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.tools.log;

import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Extends {@link OutputStream}.</br></br> Built a string and when finds a
 * {@link System#lineSeparator() lineSeparator} writes it into the logger
 *
 * @author bpenavayre
 *
 */
public class LoggerOutput extends OutputStream {

	private static final String LINE_SEP = System.lineSeparator();
	private static final int LINE_SEP_LENGTH = LINE_SEP.length();
	private static final char LAST_CHAR = LINE_SEP.charAt(LINE_SEP_LENGTH - 1);

	private StringBuilder builder;
	private Logger logger;
	private Level level;
	private int length;


	/**
	 * Default constructor
	 *
	 * @param logger
	 *            the logger you wish to write into
	 * @param level
	 *            the level of the messages to create
	 */
	public LoggerOutput(Logger logger, Level level) {
		this.logger = logger;
		this.level = level;
		builder = new StringBuilder();
		length = 0;
	}

	/**
	 * receive a char, append it to a {@link StringBuilder} up until it contains
	 * a line separator</br> then send it to the {@link Logger} and clean the
	 * {@link StringBuilder}
	 */
	@Override
	public void write(int newChar) throws IOException {
		if (newChar == 0) {
			return;
		}
		builder.append((char) newChar);
		length++;
		if (containsLineSep(newChar)) {
			logger.log(level, builder.substring(0, length - LINE_SEP_LENGTH));
			builder.setLength(0);
			length = 0;
		}
	}

	/**
	 * @param lastChar
	 *            the last received chars
	 * @return true if the {@link StringBuilder} contains a
	 *         {@link System#lineSeparator()} false otherwise
	 */
	private boolean containsLineSep(int lastChar) {
		if (lastChar != LAST_CHAR || builder.length() < LINE_SEP_LENGTH) {
			return false;
		}
		int i = builder.length() - 1;
		int j = LINE_SEP_LENGTH - 1;
		while (i >= 0 && j >= 0 && builder.charAt(i) == LINE_SEP.charAt(j)) {
			i--;
			j--;
		}
		return j == -1;
	}
}
