/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.tools.log.gui;

import java.awt.Insets;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.text.DefaultCaret;

/**
 * Extends {@link JTextArea} (through {@link LogGuiUnusedMethods}),</br> This
 * class mimic the behavior of a console
 *
 * @see LogGuiUnusedMethods
 * @author bpenavayre
 *
 */
public class LogGui extends LogGuiUnusedMethods {

	private static final long serialVersionUID = -441060446203216343L;
	private static final String COPY = "Copy selection";
	private static final String COPY_ALL = "Copy all";
	private static final String ERROR_KEY = "[ERROR]:";

	private JPopupMenu popupMenu;
	private boolean lastStringHadLineSep;
	private DefaultCaret caret;
	private Clipboard clip;


	/**
	 * Constructor setting wished GUI parameters
	 */
	public LogGui() {
		addMouseListener(this);
		lastStringHadLineSep = true;
		caret = (DefaultCaret) getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		setEditable(false);
		caret.setVisible(true);
		setMargin(new Insets(0, 5, 0, 0));
	}

	/**
	 * Append the log's message to current text
	 *
	 * @param log
	 *            the {@link LogRecord}
	 */
	public void setNewLog(LogRecord log) {
		if (!lastStringHadLineSep) {
			append(System.lineSeparator());
		}
		if (log.getLevel() == Level.SEVERE) {
			append(ERROR_KEY);
		}
		append(log.getMessage());
	}

	/**
	 * change {@link #lastStringHadLineSep} and do synchronized call to
	 * {@link JTextArea#append(String)}
	 */
	@Override
	public void append(final String newString) {
		lastStringHadLineSep = newString.endsWith(System.lineSeparator());

		Runnable r = new Runnable() {
			@Override
			public void run() {
				LogGui.super.append(newString);
			}
		};

		if (SwingUtilities.isEventDispatchThread()) {
			r.run();
		} else {
			SwingUtilities.invokeLater(r);
		}
	}

	/**
	 * Display the popup menu at the given place.
	 *
	 * @param placeClicked The place to show the popup menu.
	 */
	private void rightClick(Point placeClicked) {
		getPopupMenu().show(this, placeClicked.x, placeClicked.y);
	}

	/**
	 * Create the popup menu if needed then return it.
	 *
	 * @return the popup menu.
	 */
	private JPopupMenu getPopupMenu() {
		if (popupMenu == null) {
			popupMenu = new JPopupMenu();
			JMenuItem copy = new JMenuItem(COPY);
			copy.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					setLineContent(getSelectedText());
				}
			});
			JMenuItem copyAll = new JMenuItem(COPY_ALL);
			copyAll.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					copyAll();
				}
			});
			popupMenu.add(copy);
			popupMenu.add(copyAll);
		}
		return popupMenu;
	}

	/**
	 * Copy all the text.
	 */
	private void copyAll() {
		caret.setUpdatePolicy(DefaultCaret.NEVER_UPDATE);
		selectAll();
		setLineContent(getSelectedText());
		setSelectionStart(getSelectionEnd());
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
	}

	private void setLineContent(String line) {
		if (clip == null) {
			clip = Toolkit.getDefaultToolkit().getSystemClipboard();
		}
		clip.setContents(new StringSelection(line), this);
	}

	/**
	 * Opens a {@link JPopupMenu} on right click.
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		if (SwingUtilities.isRightMouseButton(e)) {
			rightClick(e.getPoint());
		}
	}

	/**
	 * Change caret position of this ( {@link JTextArea} ) when size changes
	 */
	@Override
	public void componentResized(ComponentEvent e) {
		setCaretPosition(getCaretPosition());
	}
}
