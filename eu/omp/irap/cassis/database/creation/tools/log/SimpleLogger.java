/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.tools.log;

import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Extends {@link SimpleFormatter} to edit the default format of
 * {@link java.util.loggig.Logger}.</br> Use the static method
 * {@link #getLogger(String)} to access it.
 *
 * @author bpenavayre
 *
 */
public final class SimpleLogger extends Formatter {

	private static final int INFO_VALUE = Level.INFO.intValue();
	private static final String INFO_AND_SUB = "%s%s";
	/**
	 * String to use in String formatter to get the error String.</br> The
	 * '%6$s' means that we use the sixth String argument transmitted to the
	 * formatter here {@link System#lineSeparator()}
	 */
	private static final String WARNING_AND_MORE = "In \"%s\" from %s%6$sCalled by %s reicieved%6$s%s: %s%s%6$s";

	private String classLogger;


	private SimpleLogger(String classLogger) {
		this.classLogger = classLogger;
	}

	@Override
	public String format(LogRecord record) {
		if (record.getLevel().intValue() <= INFO_VALUE) {
			return String.format(INFO_AND_SUB, record.getMessage(),
					System.lineSeparator());
		}
		return String.format(WARNING_AND_MORE, record.getSourceMethodName(),
				record.getSourceClassName(), classLogger, record.getLevel(),
				record.getMessage(), System.lineSeparator());
	}

	/**
	 * Use this method to get the SimpleLogger</br> Use the
	 * {@link Logger#getLogger(String)} method and</br> then set a new handler
	 * having {@link SimpleLogger this class} for Formatter
	 *
	 * @param classLogger
	 * @return
	 */
	public static Logger getLogger(String classLogger) {
		Logger log = Logger.getLogger(classLogger);
		if (log.getUseParentHandlers()) {
			log.setUseParentHandlers(false);
			ConsoleHandler console = new ConsoleHandler();
			console.setFormatter(new SimpleLogger(classLogger));
			log.addHandler(console);
		}
		return log;
	}
}
