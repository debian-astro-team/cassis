/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.tools.integrity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Class to basically represent a Database.
 *
 * @author M. Boiziot
 */
public class Database {

	private final int version;
	private final List<Table> tables;


	/**
	 * Constructor.
	 *
	 * @param version The version of the database.
	 */
	public Database(int version) {
		this(version, new ArrayList<Table>());
	}

	/**
	 * Constructor.
	 *
	 * @param version The version of the database.
	 * @param tables The list of tables of the database.
	 */
	public Database(int version, List<Table> tables) {
		this.version = version;
		if (tables != null) {
			this.tables = tables;
		} else {
			this.tables = new ArrayList<Table>();
		}
	}

	/**
	 * Return the version of the database.
	 *
	 * @return the version of the database.
	 */
	public int getVersion() {
		return version;
	}

	/**
	 * Return an unmodifiable list of the table of the database.
	 *
	 * @return an unmodifiable list of the table of the database.
	 */
	public List<Table> getTables() {
		return Collections.unmodifiableList(tables);
	}

	/**
	 * Add the given table to the list of tables of the database.
	 *
	 * @param table The table to add.
	 */
	public void add(Table table) {
		this.tables.add(table);
	}

	/**
	 * Add the given list of tables to the list of table of the database.
	 *
	 * @param tables The tables to add.
	 */
	public void addAll(List<Table> tables) {
		this.tables.addAll(tables);
	}

	/**
	 * Create an hashCode.
	 *
	 * @see java.lang.Object#hashCode()
	 * @return the hashCode.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + tables.hashCode();
		result = prime * result + version;
		return result;
	}

	/**
	 * Test the equality with another object.
	 * In order to be equals here, the two object must have the same version
	 *  and all theirs tables equals.
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 * @param obj the reference object with which to compare.
	 * @return true if the two objects are equals, false otherwise.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Database other = (Database) obj;
		if (version != other.version) {
			return false;
		}
		if (tables.size() != other.tables.size()) {
			return false;
		} else {
			for (Table table : tables) {
				if (!table.equals(other.getTable(table.getName()))) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Search the table with the given name then return it.
	 *
	 * @param name The name of the table to search.
	 * @return the table with the given name or null if not found.
	 */
	public Table getTable(String name) {
		for (Table table : tables) {
			if (table.getName().equals(name)) {
				return table;
			}
		}
		return null;
	}

	/**
	 * Check that this database contains all elements of the given database.
	 * Warning: DatabaseA.contains(DatabaseB) can be different from
	 *  DatabaseB.contains(DatabaseA).
	 * So: this does not test equality and not test the version.
	 *
	 * @param database The database containing all the elements we need.
	 * @return true if the database contains all the elements needed, false otherwise.
	 */
	public boolean contains(Database database) {
		for (Table table : database.getTables()) {
			Table t = this.getTable(table.getName());
			if (t == null || !t.contains(table)) {
				return false;
			}
		}
		return true;
	}
}
