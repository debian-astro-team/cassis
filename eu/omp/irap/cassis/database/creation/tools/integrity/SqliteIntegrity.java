/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.tools.integrity;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import eu.omp.irap.cassis.database.access.SqliteDataBaseConnection;

public class SqliteIntegrity {

	public static Logger logger = Logger.getLogger(SqliteIntegrity.class.getName());

	/**
	 * Return the names of the tables.
	 *
	 * @param connection
	 *            The connection object.
	 * @return the names of the tables.
	 */
	public static List<String> getTables(Connection connection) {
		List<String> tables = new ArrayList<String>();
		try (Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT name FROM sqlite_master"
						+ " WHERE type='table'")) {
			while (rs.next()) {
				tables.add(rs.getString("name"));
			}
		} catch (SQLException e) {
		}
		return tables;
	}

	/**
	 * Test the presence of the needed tables (not their schemas or content).
	 *
	 * @param connection
	 *            The connection object.
	 * @param neededTables
	 *            The list of name of the needed tables.
	 * @return true if there is all the needed table, false otherwise.
	 */
	public static boolean testTables(Connection connection, List<String> neededTables) {
		List<String> tables = getTables(connection);
		for (String table : neededTables) {
			if (!tables.contains(table)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Test that each molecules have at least a transition.
	 *
	 * @param connection
	 *            The connection object.
	 * @return true if all molecules have a least a transition.
	 */
	public static boolean testEachMoleculeHaveATransition(Connection connection) {
		try (Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT tag FROM catdir "
						+ "WHERE tag not in (SELECT DISTINCT itag FROM transitions);")) {
			return !rs.next();
		} catch (SQLException e) {
		}
		return false;
	}

	/**
	 * Test if the values of id_trans_min and id_trans_max are correct.
	 *
	 * @param connection
	 *            The connection object.
	 * @return true if the values of id_trans_min and id_trans_max are correct,
	 *         false otherwise.
	 */
	public static boolean testIdTransValue(Connection connection) {
		try (Statement stmt = connection.createStatement();
				ResultSet rs = stmt
						.executeQuery("SELECT tag, id_trans_min, id_trans_max, "
								+ "min(id_transitions) as eff_trans_min, max(id_transitions) as eff_trans_max "
								+ "FROM catdir, transitions "
								+ "WHERE transitions.itag = catdir.tag " + "GROUP BY tag "
								+ "HAVING id_trans_min != eff_trans_min "
								+ "  OR id_trans_max != eff_trans_max;")) {
			return !rs.next();
		} catch (SQLException e) {
		}
		return false;
	}

	/**
	 * Test if the values in column name in the table catdir are unique.
	 *
	 * @param connection
	 *            The connection object.
	 * @return true if the values in column name in the table catdir are unique.
	 */
	public static boolean testNameCatdirIsUnique(Connection connection) {
		try (Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT name FROM catdir "
						+ "GROUP BY name HAVING count(name) > 1;")) {
			return !rs.next();
		} catch (SQLException e) {
		}
		return false;
	}

	/**
	 * Test if the values of QN for each itag is unique for the table
	 * transitions.
	 *
	 * @param connection
	 *            The connection object.
	 * @return true if the values of QN for each itag is unique for the table
	 *         transitions.
	 */
	public static boolean testQnItagTransitionsIsUnique(Connection connection) {
		try (Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT itag, qn FROM transitions"
						+ " GROUP BY itag, qn HAVING count(*) > 1;")) {
			return !rs.next();
		} catch (SQLException e) {
		}
		return false;
	}

	/**
	 * Test if each id_database in catdir is a value of an id in table
	 * cassis_databases.
	 *
	 * @param connection
	 *            The connection object.
	 * @return true if each id_database in catdir is a value of an id in table
	 *         cassis_databases, false otherwise.
	 */
	public static boolean testIdDatabaseCatdir(Connection connection) {
		try (Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT DISTINCT id_database "
						+ "FROM catdir WHERE id_database NOT IN "
						+ "(SELECT DISTINCT id FROM cassis_databases);")) {
			return !rs.next();
		} catch (SQLException e) {
		}
		return false;
	}

	/**
	 * Test if each id_database in transitions is a value of an id in table
	 * cassis_databases.
	 *
	 * @param connection
	 *            The connection object.
	 * @return true if each id_database in transitions is a value of an id in
	 *         table cassis_databases, false otherwise.
	 */
	public static boolean testIdDatabaseTransitions(Connection connection) {
		try (Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT DISTINCT id_database "
						+ "FROM transitions WHERE id_database NOT IN "
						+ "(SELECT DISTINCT id FROM cassis_databases);")) {
			return !rs.next();
		} catch (SQLException e) {
		}
		return false;
	}

	/**
	 * Test if the schema of given tableName is the same as the given database
	 * (from connection).
	 *
	 * @param connection
	 *            The connection object.
	 * @param pathSchema
	 *            The path of the expected xml schema of the table.
	 * @return true if the schema of the database is the same as the one in the
	 *         pathSchema, false otherwise.
	 */
	public static boolean testSchemaIdentical(Connection connection, String pathSchema) {
		Database sqliteDatabase = DatabaseUtils.createDatabaseFromSqlite(connection);
		Database xmlDatabase = DatabaseUtils.createDatabaseFromXml(pathSchema);

		return xmlDatabase.equals(sqliteDatabase);
	}

	/**
	 * Test if the given database (from connection) contains the needed
	 * informations from the given schema in pathSchema.
	 *
	 * @param connection
	 *            The connection object.
	 * @param pathSchema
	 *            The path of the expected xml schema of the table.
	 * @return true the given database (from connection) contains the needed
	 *         informations from the given schema in pathSchema, false
	 *         otherwise.
	 */
	public static boolean testSchemaCompatible(Connection connection, String pathSchema) {
		Database sqliteDatabase = DatabaseUtils.createDatabaseFromSqlite(connection);
		Database xmlDatabase = DatabaseUtils.createDatabaseFromXml(pathSchema);

		return sqliteDatabase.contains(xmlDatabase);
	}

	/**
	 * Test if each tag in cassis_parti_function is a value of an id in catdir.
	 *
	 * @param connection
	 *            The connection object.
	 * @return true if each tag in cassis_parti_function is a value of an id in
	 *         catdir.
	 */
	public static boolean testTagCassisPartiFunct(Connection connection) {
		try (Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT DISTINCT tag "
						+ "FROM cassis_parti_funct WHERE tag NOT IN "
						+ "(SELECT DISTINCT tag FROM catdir);")) {
			return !rs.next();
		} catch (SQLException e) {
		}
		return false;
	}

	/**
	 * Test if the values of the column temp in cassis_parti_funct are correct.
	 * The values must be one of: 1000.0, 500.0, 300.0, 225.0, 150.0, 75.0,
	 * 37.5, 18.75, 9.375, 5.0, 2.725.
	 *
	 * @param connection
	 *            The connection object.
	 * @return true if the values of the column temp in cassis_parti_funct are
	 *         correct, false otherwise.
	 */
	public static boolean testTempValuesCassisPartiFunct(Connection connection) {
		try (Statement stmt = connection.createStatement();
				ResultSet rs = stmt
						.executeQuery("SELECT DISTINCT temp "
								+ "FROM cassis_parti_funct WHERE temp NOT IN "
								+ "(1000.0, 500.0, 300.0, 225.0, 150.0, 75.0, 37.5, 18.75, 9.375, 5.0, 2.725);")) {
			return !rs.next();
		} catch (SQLException e) {
		}
		return false;
	}

	/**
	 * Test if the values of the column temp in cassis_parti_funct are not -1000
	 * or if the value are not -1000
	 *
	 * @param connection
	 *            The connection object.
	 * @return true if the values of the column temp in cassis_parti_funct are
	 *         correct, false otherwise.
	 */
	public static boolean testTempAndQlogValuesCassisPartiFunct(Connection connection) {
		try (Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT temp, qlog "
						+ "FROM cassis_parti_funct WHERE temp = -1000 OR qlog =-1000")) {
			return !rs.next();
		} catch (SQLException e) {
		}
		return false;
	}

	/**
	 * Test if the two given bases have the same number of molecules.
	 *
	 * @param connection
	 *            The connection object of the first base.
	 * @param connectionTwo
	 *            The connection object of the second base.
	 * @return true if the two bases have the same number of molecules, false
	 *         otherwise.
	 */
	public static boolean testIfSameNumberOfMolecules(Connection connection,
			Connection connectionTwo) {
		return getNumberOfMolecules(connection) == getNumberOfMolecules(connectionTwo);
	}

	/**
	 * Test if the two given bases have the same number of transitions.
	 *
	 * @param connection
	 *            The connection object of the first base.
	 * @param connectionTwo
	 *            The connection object of the second base.
	 * @return true if the two bases have the same number of transitions, false
	 *         otherwise.
	 */
	public static boolean testIfSameNumberOfTransitions(Connection connection,
			Connection connectionTwo) {
		return getNumberOfTransitions(connection) == getNumberOfTransitions(connectionTwo);
	}

	/**
	 * Return the version of the database.
	 *
	 * @param connection
	 *            The connection object.
	 * @return the version of the database, -1 in case of error.
	 */
	public static int getVersion(Connection connection) {
		int version = -1;
		try (Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery("PRAGMA user_version;")) {
			version = rs.getInt("user_version");
		} catch (SQLException e) {
		}
		return version;
	}

	/**
	 * Return the number of transitions for the given database.
	 *
	 * @param connection
	 *            The connection object.
	 * @return the number of transitions for the given database or -1 in case of
	 *         error.
	 */
	public static int getNumberOfTransitions(Connection connection) {
		int transitions = -1;
		try (Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT count(*) FROM transitions;")) {
			transitions = rs.getInt(1);
		} catch (SQLException e) {
		}
		return transitions;
	}

	/**
	 * Return the number of molecules for the given database.
	 *
	 * @param connection
	 *            The connection object.
	 * @return the number of molecules for the given database or -1 in case of
	 *         error.
	 */
	public static int getNumberOfMolecules(Connection connection) {
		int transitions = -1;
		try (Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT count(*) FROM catdir;")) {
			transitions = rs.getInt(1);
		} catch (SQLException e) {
		}
		return transitions;
	}

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws SQLException, IOException {
		if (args.length < 1) {
			logger.severe("arg1 : path of the CASSIS sqlite database"
					+ "[filename=arg2 : path of file name reporting (databaseReport.html)]");
			return;
		}
		logger.info("Read parameters ...");
		logger.info("Begin database reporting ...");
		SqliteDataBaseConnection dataBaseConnection1 = new SqliteDataBaseConnection(args[0]);

		String filename="databaseReport.html";
		if (args.length > 1 ){
			for(int cpt = 1; cpt < args.length; cpt++){
				if(args[cpt].startsWith("filename=")){
					filename = args[cpt].split("=")[1];
				}
			}

		}
		logger.info("Create  reporting Informations for the base " + dataBaseConnection1.getId() + "...");
		BufferedWriter builder = new BufferedWriter(new FileWriter(filename));
		final Connection conn = dataBaseConnection1.getSqlConn();
		builder.append("<!DOCTYPE html>\n");
		builder.append("<html>\n");
		builder.append("<head>\n");
		builder.append("<meta charset=\"UTF-8\">\n");
		builder.append("<title> Informations for the base " + dataBaseConnection1.getId()
				+ "</title>\n");


		builder.append("<link rel=\"stylesheet\" href=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\">\n");
		builder.append("  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js\"></script>\n");
		builder.append("  <script src=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>\n");
		builder.append("</head>\n");

		builder.append("<h1> Informations for the base " + dataBaseConnection1.getId() + "</h1>\n");
		builder.append("<ul>\n");
		int version = SqliteIntegrity.getVersion(conn);
		builder.append("<li>Version = " + version + "</li>\n");

		int nbMol = SqliteIntegrity.getNumberOfMolecules(conn);
		builder.append("<li>NB species = " + nbMol + "</li>\n");
		int nbTrans = SqliteIntegrity.getNumberOfTransitions(conn);
		builder.append("<li>NB transitions = " + nbTrans + "</li>\n");
		builder.append("<li>Liste of Tables = " + SqliteIntegrity.getTables(conn) + "</li>\n");
		builder.append("</ul>\n");

		testOfTheDatabase(builder, conn);



		builder.append("</body>\n");
		builder.append("</html>\n");
		builder.flush();
		logger.info("End of the creation of the reporting ...");
		builder.close();

	}

	private static void testOfTheDatabase(BufferedWriter builder, final Connection conn) throws IOException {
		builder.append("<h2> Tests </h1>\n");
		builder.append("<ul>\n");
		builder.append("<li>testTempValuesCassisPartiFunct = "
				+ getColorBooleanReport(SqliteIntegrity.testTempValuesCassisPartiFunct(conn))
				+ "</li>\n");
		builder.append("<li>testEachMoleculeHaveATransition = "
				+ getColorBooleanReport(SqliteIntegrity.testEachMoleculeHaveATransition(conn))
				+ "</li>\n");
		builder.append("<li>testIdDatabaseCatdir = "
				+ getColorBooleanReport(SqliteIntegrity.testIdDatabaseCatdir(conn)) + "</li>\n");
		builder.append("<li>testIdDatabaseTransitions = "
				+ getColorBooleanReport(SqliteIntegrity.testIdDatabaseTransitions(conn))
				+ "</li>\n");
		builder.append("<li>testIdTransValue = "
				+ getColorBooleanReport(SqliteIntegrity.testIdTransValue(conn)) + "</li>\n");
//		builder.append("<li>testNameCatdirIsUnique = "
//				+ getColorBooleanReport(SqliteIntegrity.testNameCatdirIsUnique(conn)) + "</li>\n");
		builder.append("<li>testQnItagTransitionsIsUnique = "
				+ getColorBooleanReport(SqliteIntegrity.testQnItagTransitionsIsUnique(conn))
				+ "</li>\n");
		builder.append("<li>testTempValuesCassisPartiFunct = "
				+ getColorBooleanReport(SqliteIntegrity.testTempValuesCassisPartiFunct(conn))
				+ "</li>\n");
		builder.append("<li>testTempAndQlogValuesCassisPartiFunct = "
				+ getColorBooleanReport(SqliteIntegrity.testTempAndQlogValuesCassisPartiFunct(conn))
				+ "</li>\n");
		builder.append("</ul>\n");
	}



	private static String getColorBooleanReport(boolean val) {
		String res = "<span style=\"color:red\"> false </span>";
		if (val) {
			res = "<span style=\"color:green\"> true </span>";
		}
		return res;
	}
}
