/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.tools.integrity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Class to basically represent a table.
 *
 * @author M. Boiziot
 */
public class Table {

	private final String name;
	private final List<Attribute> attributes;


	/**
	 * Constructor.
	 *
	 * @param name The name of the table.
	 */
	public Table(String name) {
		this(name, new ArrayList<Attribute>());
	}

	/**
	 * Constructor.
	 *
	 * @param name The name of the table.
	 * @param attributes The list of attributes of the table.
	 */
	public Table(String name, List<Attribute> attributes) {
		this.name = name;
		if (attributes != null) {
			this.attributes = attributes;
		} else {
			this.attributes = new ArrayList<Attribute>();
		}
	}

	/**
	 * Return the name of the table.
	 *
	 * @return the name of the table.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Return an unmodifiable list of the attributes of the table.
	 *
	 * @return an unmodifiable list of the attributes of the table.
	 */
	public List<Attribute> getAttributes() {
		return Collections.unmodifiableList(attributes);
	}

	/**
	 * Add the given attribute to the list of attributes of the table.
	 *
	 * @param attribute The attribute to add.
	 */
	public void add(Attribute attribute) {
		this.attributes.add(attribute);
	}

	/**
	 * Add a list of attributes to the list of attributes of the table.
	 *
	 * @param attributes The attributes to add.
	 */
	public void addAll(List<Attribute> attributes) {
		this.attributes.addAll(attributes);
	}

	/**
	 * Create an hashCode.
	 *
	 * @see java.lang.Object#hashCode()
	 * @return the hashCode.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + attributes.hashCode();
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/**
	 * Test the equality with another object.
	 * In order to be equals here, the two object must have the same name and
	 *  all theirs attributes equals.
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 * @param obj the reference object with which to compare.
	 * @return true if the two objects are equals, false otherwise.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Table other = (Table) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}

		if (attributes.size() != other.attributes.size()) {
			return false;
		} else {
			for (Attribute attribute : attributes) {
				if (!attribute.equals(other.getAttribute(attribute.getName()))) {
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Search the attribute with the given name then return it.
	 *
	 * @param name The name of the attribute to search.
	 * @return the attribute with the given name or null if not found.
	 */
	private Attribute getAttribute(String name) {
		for (Attribute attribute : attributes) {
			if (attribute.getName().equals(name)) {
				return attribute;
			}
		}
		return null;
	}

	/**
	 * Create a string representation of the object.
	 *
	 * @see java.lang.Object#toString()
	 * @return a string representation of the object.
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Table [name=");
		builder.append(name);
		builder.append(", attributes=\n");
		for (Attribute attribute : attributes) {
			builder.append('\t').append(attribute.toString()).append('\n');
		}
		builder.append(']');
		return builder.toString();
	}

	/**
	 * Check that this Table contains all attributes of the given table.
	 * Warning: TableA.contains(TableB) can be different from
	 *  TableB.contains(TableA). This does not test equality!
	 *
	 * @param table The table containing all the attributes we need.
	 * @return true if the table contains all the attributes needed, false otherwise.
	 */
	public boolean contains(Table table) {
		if (table == null) {
			return false;
		}
		for (Attribute attribute : table.getAttributes()) {
			if (!attribute.equals(this.getAttribute(attribute.getName()))) {
				return false;
			}
		}
		return true;
	}
}
