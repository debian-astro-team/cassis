/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.tools.integrity;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Utility class to convert XML / {@link Database} / SQLite database.
 *
 * @author M. Boiziot
 */
public class DatabaseUtils {

	/**
	 * Create a {@link Database} from an XML definition.
	 *
	 * @param path The path of the file with XML definition.
	 * @return the database.
	 */
	public static Database createDatabaseFromXml(String path) {
		try (FileInputStream fileInputStream = new FileInputStream(path)) {
			return createDatabaseFromXml(fileInputStream);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Create a {@link Database} from an XML definition with an InputStream.
	 *  This function close the given stream.
	 *
	 * @param is The InputStream from which we read the xml.
	 * @return the database.
	 */
	public static Database createDatabaseFromXml(InputStream is) {
		try {
			DocumentBuilderFactory docBuilderFact = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFact.newDocumentBuilder();
			Document doc = docBuilder.parse(is);
			doc.getDocumentElement().normalize();
			Element dbElement = doc.getDocumentElement();
			int version = Integer.parseInt(dbElement.getAttribute("version"));

			Database db = new Database(version);

			NodeList nodeListTable = dbElement.getElementsByTagName("table");
			int nbTables = nodeListTable.getLength();

			String name;
			String type;
			int nbAttributes;
			Table table;
			NodeList nodeListAttribute;
			Element tableElement;
			Element attributeElement;

			for (int i = 0; i < nbTables; i++) {
				tableElement = (Element) nodeListTable.item(i);
				name = tableElement.getAttribute("name");
				table = new Table(name);
				nodeListAttribute = tableElement.getElementsByTagName("attribute");
				nbAttributes = nodeListAttribute.getLength();
				for (int j = 0; j < nbAttributes; j++) {
					attributeElement = (Element) nodeListAttribute.item(j);
					name = attributeElement.getAttribute("name");
					type = attributeElement.getAttribute("type");
					table.add(new Attribute(name, type));
				}
				db.add(table);
			}
			return db;
		} catch (Exception e) {
			return null;
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
				}
			}
		}
	}

	/**
	 * Create a {@link Database} from a SQLite database.
	 *
	 * @param connection The connection object.
	 * @return the database.
	 */
	public static Database createDatabaseFromSqlite(Connection connection) {
		Database db = new Database(SqliteIntegrity.getVersion(connection));

		// Get the names of the 'effectives' tables.
		List<String> tablesNames = SqliteIntegrity.getTables(connection);
		tablesNames.remove("sqlite_sequence");

		// Get the infos for each tables.
		for (String tableName : tablesNames) {
			db.add(createTableFromSqlite(connection, tableName));
		}
		return db;
	}

	/**
	 * Create a {@link Table} from a SQLite connection and a table name.
	 *
	 * @param connection The connection object.
	 * @param nameTable The name of the table to get the information.
	 * @return the {@link Table}.
	 */
	private static Table createTableFromSqlite(Connection connection, String nameTable) {
		Table table = new Table(nameTable);
		String sql = "PRAGMA table_info(" + nameTable + ");";
		try (Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery(sql)) {
			while (rs.next()) {
				table.add(new Attribute(rs.getString("name"), rs.getString("type")));
			}
		} catch (SQLException e) {
		}
		return table;
	}

	/**
	 * Create a XML representation of a {@link Database}.
	 *
	 * @param database The {@link Database}.
	 * @return the XML representation.
	 */
	public static String createXmlFromDatabase(Database database) {
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		sb.append("<database version=\"").append(database.getVersion()).append("\">\n");
		for (Table table : database.getTables()) {
			sb.append("\t<table name=\"").append(table.getName()).append("\">\n");
			for (Attribute attribute : table.getAttributes()) {
				sb.append("\t\t<attribute name=\"").append(attribute.getName())
						.append("\" type=\"").append(attribute.getType())
						.append("\" />\n");
			}
			sb.append("\t</table>\n");
		}
		sb.append("</database>\n");
		return sb.toString();
	}

	/**
	 * Create a XML representation from a SQLite connection.
	 *
	 * @param connection The connection object.
	 * @return the XML representation.
	 */
	public static String createXmlFromSqlite(Connection connection) {
		return createXmlFromDatabase(createDatabaseFromSqlite(connection));
	}
}
