/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.tools.integrity;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

import eu.omp.irap.cassis.database.access.LineDescriptionDB;
import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB;
import eu.omp.irap.cassis.database.access.SqliteDataBaseConnection;

/**
 * Class to create an HTML report on a database (with comparing it to another).
 */
public class WhatsNewInDatabase {

	private static final Logger logger = Logger.getLogger(WhatsNewInDatabase.class.getName());
	private static final String HTML_CHECK = "<font size=\"4\"><span style=\"color:#01DF01\">&#10003;</span></font>";
	private static final String HTML_CROSS = "<font size=\"4\"><span style=\"color:#FF0000\">&#10060;</span></font>";
	private static boolean withoutNist= false;
	private SqliteDataBaseConnection newSqliteConnection;
	private SqliteDataBaseConnection oldSqliteConnection;
	private String filename;
	private boolean complete;

	private String newDbName;
	private String oldDbName;
	private int newDbSpeciesSize;
	private int oldDbSpeciesSize;
	private int newDbTransitionsSize;
	private int oldDbTransitionsSize;
	private List<String> newDbListDatabase;
	private List<String> oldDbListDatabase;
	private List<String> allDbList;
	private Map<String, Integer> mapNameDbNumber;


	public static void main(String[] args) {
		if (args.length < 1) {
			logger.severe("arg1 : path of the new sqlite database, "
					+ "arg2 : path of the old sqlite database"
					+ "[filename= databaseReport.html], [complete : to have a complete comapraison]");
			return;
		}
		logger.info("Read parameters ...");
		SqliteDataBaseConnection newSqliteConnection = new SqliteDataBaseConnection(args[0], true);
		SqliteDataBaseConnection oldSqliteConnection = new SqliteDataBaseConnection(args[1], true);

		boolean complete = false;
		String filename="databaseReport.html";
		if (args.length > 2 ) {
			for(int cpt = 2; cpt < args.length; cpt++) {
				if(args[cpt].equals("complete")) {
					complete = true;
				} else if(args[cpt].startsWith("filename=")) {
					filename = args[cpt].split("=")[1];
				}
				 else if(args[cpt].startsWith("without_NIST")) {
					withoutNist = true;
				 }
			}

		}
		logger.info("Begin database reporting ...");
		try {
			WhatsNewInDatabase wnid = new WhatsNewInDatabase(
					newSqliteConnection, oldSqliteConnection, filename, complete);
			wnid.compareSqliteDatabase();
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "Error while selecting a molecule", e);
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error while selecting a molecule", e);
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		logger.info("End database reporting ...");
	}

	/**
	 * Constructor.
	 *
	 * @param newSqliteConnection {@link SqliteDataBaseConnection} to the new database
	 * @param oldSqliteConnection {@link SqliteDataBaseConnection} to the old database
	 * @param filename Name of the file for the HTML report.
	 * @param complete true to use a complete report, false otherwise.
	 */
	public WhatsNewInDatabase(SqliteDataBaseConnection newSqliteConnection,
			final SqliteDataBaseConnection oldSqliteConnection, String filename, boolean complete) {
		this.newSqliteConnection = newSqliteConnection;
		this.oldSqliteConnection = oldSqliteConnection;
		this.filename = filename;
		this.complete = complete;
	}

	/**
	 * Do the database report.
	 *
	 * @throws SQLException In case of SQL error.
	 * @throws IOException In case of IO error.
	 * @throws InterruptedException In case of Thread error.
	 * @throws ExecutionException In case of Thread error.
	 */
	public void compareSqliteDatabase() throws SQLException, IOException, InterruptedException, ExecutionException {
		initBaseInfos();

		BufferedWriter builder = new BufferedWriter(new FileWriter(filename));
		addTopGlobalDatabaseReport(builder);

		for (String databaseName : allDbList) {
			if (!databaseName.equals("NIST")|| withoutNist)
			createDatabaseReport(builder, databaseName);
		}
		builder.append("</body>\n</html>");
		builder.flush();
		builder.close();
	}

	/**
	 * Add the top of the report (header, database basic report, sub-database table).
	 *
	 * @param builder The writer.
	 * @throws IOException In case of IO error.
	 */
	private void addTopGlobalDatabaseReport(BufferedWriter builder) throws IOException {
		addHeader(builder);

		builder.append("<body>");
		builder.append("<h1>Comparaison of the new database \"").append(newDbName);
		builder.append("\" relatively to the old one \"").append(oldDbName).append("\".</h1>\n");

		builder.append("<div style=\"float:left;width:100%;\">");
		builder.append("<div style=\"float:left;width:25%;\">");
		addBase(builder);
		builder.append("</div>");

		builder.append("<div style=\"float:left;width:75%;\">");
		addDatabaseTable(builder);
		builder.append("</div>");
		builder.append("</div>");
	}

	/**
	 * Create the report for a sub-database.
	 *
	 * @param builder The writer.
	 * @param databaseName The name of the database for which we want the report.
	 * @throws SQLException In case of SQL error.
	 * @throws IOException In case of IO error.
	 * @throws InterruptedException In case of Thread error.
	 * @throws ExecutionException In case of Thread error.
	 */
	private void createDatabaseReport(BufferedWriter builder, String databaseName)
			throws IOException, SQLException, InterruptedException, ExecutionException {
		builder.append("<div id="+ databaseName + ">\n");
		builder.append("<h2>"+ databaseName + "</h2>\n");
		if (isNew(databaseName)) {
			builder.append("<b>This is a new database</b><br>\n");
		} else if (isRemoved(databaseName)) {
			builder.append("<b>This database was removed!</b><br>\n");
		}
		List<Species> newSpecies = getNewSpecies(databaseName);
		List<Species> deletedSpecies = getDeletedSpecies(databaseName);
		ChangeDatabase cd = getChange(databaseName);

		SameQn sq;
		if ("NIST".equals(databaseName) && !complete) {
			sq = new SameQn(new ArrayList<LineDescriptionDB>(0), new HashMap<Integer, String>(0));
		} else {
			sq = getSameQn(databaseName);
		}
		if (haveModification(newSpecies, deletedSpecies, cd, sq)) {
			addModifications(builder, newSpecies, deletedSpecies, cd, sq);
		} else if ("NIST".equals(databaseName) && !complete) {
			builder.append("No change on this database (QN not tested on NIST, use \"complete\" parameter to test it)<br>\n");
		} else {
			builder.append("No change on this database!<br>\n");
		}
	}

	/**
	 * Add the modifications to the writer (new/deleted/modified species and
	 *  species with duplicate QN).
	 *
	 * @param builder The writer
	 * @param newSpecies The list of new species.
	 * @param deletedSpecies The list of deleted species.
	 * @param cd {@link ChangeDatabase} object, having modified transitions.
	 * @param sq {@link SameQn} object, having transitions with same QN.
	 * @throws IOException In case of IO error.
	 */
	private void addModifications(BufferedWriter builder, List<Species> newSpecies,
			List<Species> deletedSpecies, ChangeDatabase cd, SameQn sq) throws IOException {
		builder.append("<ul>\n");
		addNewSpeciesIfNeeded(builder, newSpecies);
		addDeletedSpeciesIfNeeded(builder, deletedSpecies);
		addModifiedSpeciesIfNeeded(builder, cd);
		addSpeciesWithDuplicateQnIfNeeded(builder, sq);
		builder.append("</ul>\n");
	}

	/**
	 * Create the report for species with duplicate QN.
	 *
	 * @param builder The writer used to add the report.
	 * @param sq {@link SameQn} object, having transitions with same QN.
	 * @throws IOException In case of IO error.
	 */
	private void addSpeciesWithDuplicateQnIfNeeded(BufferedWriter builder, SameQn sq) throws IOException {
		if (sq.size() >= 1) {
			List<Species> sList = sq.getSpecies();
			builder.append("  <li>Species with duplicates QN: ").append(String.valueOf(sList.size())).append("</li>\n");
			builder.append("  <ul>\n");
			for (Species s : sList) {
				String divName = s.tag + "DupQn";
				List<List<LineDescriptionDB>> lDup = sq.getDuplicate(s.tag);
				int sizeQnObsComp = sq.getDuplicateWithObsCompSize(lDup);
				String name = s.name + " (" + s.tag + ") ~ (duplicate: " + lDup.size();
				if (sizeQnObsComp >= 1) {
					name += " (" + sizeQnObsComp + " caused by Meas/Comp transitions)";
				}
				name += "):";
				builder.append("    <li><a href=\"javascript:toggle('").append(divName).append("');\"><font color=\"#000000\">").append(name).append("</font></a></li>\n");
				builder.append("  <span id=\"").append(divName).append("\" style=\"display: none\">\n");
				builder.append("<br>\n");
				addDuplicateArray(builder, lDup);
				builder.append("  </span>\n");

			}
			builder.append("  </ul>\n");
		}
	}

	/**
	 * Create the report for modified species.
	 *
	 * @param builder The writer used to add the report.
	 * @param cd {@link ChangeDatabase} object, having modified transitions.
	 * @throws IOException In case of IO error.
	 */
	private void addModifiedSpeciesIfNeeded(BufferedWriter builder, ChangeDatabase cd) throws IOException {
		if (cd.haveChange()) {
			builder.append("  <li>Modified species: ").append(String.valueOf(cd.ctList.size())).append("</li>\n");
			builder.append("  <ul>\n");
			for (ChangeTransitions ct : cd.ctList) {
				String divName = ct.tag + "Transitions";
				builder.append("    <li><a href=\"javascript:toggle('").append(divName).append("');\"><font color=\"#000000\">").append(ct.toString()).append(" ~ (transitions: ");
				List<LineDescriptionDB> addedTrans = ct.addedLines;
				List<LineDescriptionDB> removedTrans = ct.removedLines;
				if (!addedTrans.isEmpty()) {
					String msg = addedTrans.size() +  " added";
					builder.append("<span style=\"color:#01DF01\">").append(msg).append("</span>");
				}
				if (!addedTrans.isEmpty() && !removedTrans.isEmpty()) {
					builder.append(" / ");
				}
				if (!removedTrans.isEmpty()) {
					String msg = removedTrans.size() + " removed";
					builder.append("<span style=\"color:#FF0000\">").append(msg).append("</span>");
				}
				builder.append("):</a></li>\n");
				builder.append("  <span id=\"").append(divName).append("\" style=\"display: none\">\n");
				builder.append("<br>\n");
				if (!addedTrans.isEmpty()) {
					addTransitionsArray(builder, "ad", addedTrans);
					builder.append("<br>\n");
				}
				if (!removedTrans.isEmpty()) {
					addTransitionsArray(builder, "re", removedTrans);
					builder.append("<br>\n");
				}
				builder.append("  </span>\n");
			}
			builder.append("  </ul>\n");
		}
	}

	/**
	 * Create the report for deleted species.
	 *
	 * @param builder The writer used to add the report.
	 * @param deletedSpecies The list of deleted species.
	 * @throws IOException In case of IO error.
	 */
	private void addDeletedSpeciesIfNeeded(BufferedWriter builder, List<Species> deletedSpecies) throws IOException {
		if (!deletedSpecies.isEmpty()) {
			builder.append("  <li>Deleted species: ").append(String.valueOf(deletedSpecies.size())).append("</li>\n");
			builder.append("  <ul>\n");
			for (Species s : deletedSpecies) {
				builder.append("    <li>").append(s.toString()).append(" ~ (transitions: ");
				List<LineDescriptionDB> removedTrans = getRemovedTransitions(s.tag);
				if (!removedTrans.isEmpty()) {
					String msg = removedTrans.size() + " removed";
					builder.append("<span style=\"color:#FF0000\">").append(msg).append("</span>");
				}
				builder.append(")</li>\n");
			}
			builder.append("  </ul>\n");
		}
	}

	/**
	 * Create the report for new species.
	 *
	 * @param builder The writer used to add the report.
	 * @param newSpecies The list of new species.
	 * @throws IOException In case of IO error.
	 */
	private void addNewSpeciesIfNeeded(BufferedWriter builder, List<Species> newSpecies) throws IOException {
		if (!newSpecies.isEmpty()) {
			builder.append("  <li>New species: ").append(String.valueOf(newSpecies.size())).append("</li>\n");
			builder.append("  <ul>\n");
			for (Species s : newSpecies) {
				builder.append("    <li>").append(s.toString()).append(" ~ (transitions: ");
				List<LineDescriptionDB> newTrans = getNewTransitions(s.tag);
				if (!newTrans.isEmpty()) {
					String msg = newTrans.size() + " news";
					builder.append("<span style=\"color:#01DF01\">").append(msg).append("</span>");
				}
				builder.append(")</li>\n");
			}
			builder.append("  </ul>\n");
		}
	}

	/**
	 * Add HTML header.
	 *
	 * @param builder The writer used to add the header.
	 * @throws IOException In case of IO error.
	 */
	private void addHeader(BufferedWriter builder) throws IOException {
		builder.append("<!DOCTYPE html>\n");
		builder.append("<html>\n");
		builder.append("<head>\n");
		builder.append("<meta charset=\"UTF-8\">\n");
		builder.append("<title>Comparaison of the new database ").append(newDbName);
		builder.append(" with the old base ").append(oldDbName).append("</title>\n");
		builder.append("<style>\n");
		builder.append("table, th, td {\n");
		builder.append("  border: 1px solid black;\n");
		builder.append("  border-collapse: collapse;\n");
		builder.append("  text-align: center;\n");
		builder.append("  padding: 6px;\n");
		builder.append("}\n");
		builder.append("td.re {\n");
		builder.append("  color: #FF0000\n");
		builder.append("}\n");
		builder.append("td.ad {\n");
		builder.append("  color: #01DF01\n");
		builder.append("}\n");
		builder.append("td.a {\n");
		builder.append("  color: #43a9de\n");
		builder.append("}\n");
		builder.append("td.b {\n");
		builder.append("  color: #3ac980\n");
		builder.append("}\n");
		builder.append("</style>\n");

		builder.append("<script language=\"javascript\">\n");
		builder.append("function toggle(idToToggle) {\n");
		builder.append("  var ele = document.getElementById(idToToggle);\n");
		builder.append("  if(ele.style.display == \"block\") {\n");
		builder.append("    ele.style.display = \"none\";\n");
		builder.append("  } else {\n");
		builder.append("    ele.style.display = \"block\";\n");
		builder.append("  }\n");
		builder.append("}\n");
		builder.append("</script>\n");

		builder.append("</head>\n");
		builder.flush();
	}

	/**
	 * Add base informations for the two databases.
	 *
	 * @param builder The writer used to add the content.
	 * @throws IOException In case of IO error.
	 */
	private void addBase(BufferedWriter builder) throws IOException {
		builder.append("<ul style=\"list-style-type:disc\">\n");
		builder.append("  <li><b>").append(newDbName).append("</b>:</li>\n");
		builder.append("  <ul style=\"list-style-type:circle\">\n");
		builder.append("    <li>Species: ").append(String.valueOf(newDbSpeciesSize)).append("</li>\n");
		builder.append("    <li>Transitions: ").append(String.valueOf(newDbTransitionsSize)).append("</li>\n");
		builder.append("    <li>Databases: ").append(String.valueOf(newDbListDatabase.size())).append("</li>\n");
		builder.append("  </ul>\n");
		builder.append("</ul>\n");

		builder.append("<ul style=\"list-style-type:disc\">\n");
		builder.append("  <li><b>").append(oldDbName).append("</b>:</li>\n");
		builder.append("  <ul style=\"list-style-type:circle\">\n");
		builder.append("    <li>Species: ").append(String.valueOf(oldDbSpeciesSize)).append("</li>\n");
		builder.append("    <li>Transitions: ").append(String.valueOf(oldDbTransitionsSize)).append("</li>\n");
		builder.append("    <li>Databases: ").append(String.valueOf(oldDbListDatabase.size())).append("</li>\n");
		builder.append("  </ul>\n");
		builder.append("</ul>\n");
	}

	/**
	 * Return the sorted list of database name based on two other list.
	 *
	 * @param listOne First database name list.
	 * @param listTwo Second database name list.
	 * @return the sorted list of database name.
	 */
	private List<String> getAllDb(List<String> listOne, List<String> listTwo) {
		Set<String> allDb = new HashSet<>(listOne);
		allDb.addAll(listTwo);
		List<String> all = new ArrayList<>(allDb);
		Collections.sort(all);
		return all;
	}

	/**
	 * Add a database row with included/excluded sign for each sub-database.
	 *
	 * @param builder The writer used to add the report.
	 * @param dbName The database name
	 * @param allDbList The list of all sub-databases.
	 * @param dbList The list of sub-databases that the database contains.
	 * @throws IOException In case of IO error.
	 */
	private void addTableList(BufferedWriter builder, String dbName,
			List<String> allDbList, List<String> dbList) throws IOException  {
		builder.append("  <tr>\n");
		builder.append("    <td><b>").append(dbName).append("</b></td>\n");
		for (String db : allDbList) {
			String sign = dbList.contains(db) ? HTML_CHECK : HTML_CROSS;
			builder.append("    <td>").append(sign).append("</td>\n");
		}
		builder.append("  </tr>\n");
	}

	/**
	 * Add the database table displaying the sub-database that each database contains.
	 *
	 * @param builder The writer used to add the report.
	 * @throws IOException In case of IO error.
	 */
	private void addDatabaseTable(BufferedWriter builder) throws IOException {
		builder.append("<table>\n");
		builder.append("  <tr>\n");
		builder.append("    <th>").append("Name").append("</th>\n");
		for (String db : allDbList) {
			builder.append("    <th><a href=\"#").append(db).append("\"><font color=\"#000000\">").append(db).append("</font></a></th>\n");
		}
		builder.append("  </tr>\n");

		addTableList(builder, newDbName, allDbList, newDbListDatabase);
		addTableList(builder, oldDbName, allDbList, oldDbListDatabase);

		builder.append("</table>\n");
		builder.flush();
	}

	/**
	 * Return the list of species for the given database connection for the given sub-database.
	 *
	 * @param dataBaseConnection The database connection.
	 * @param databaseName The sub-database name.
	 * @return the list of species.
	 * @throws SQLException In case of SQL error.
	 */
	public List<Species> getSpeciesFromcatdir(SqliteDataBaseConnection dataBaseConnection,
			String databaseName) throws SQLException {
		String query = "SELECT tag, name FROM catdir where id_database = " +
			mapNameDbNumber.get(databaseName) +" ORDER BY tag";
		List<Species> species = new ArrayList<>();
		try (Statement stmt = dataBaseConnection.getSqlConn().createStatement();
				ResultSet rsMolecules = stmt.executeQuery(query)) {
			while (rsMolecules.next()) {
				species.add(new Species(rsMolecules.getInt("tag"), rsMolecules.getString("name")));
			}
		}
		return species;
	}

	/**
	 * Return the list of new species for the given sub-database.
	 *
	 * @param databaseName The name of the sub-database for which we want the new species.
	 * @return the list of new species
	 * @throws SQLException In case of SQL error.
	 */
	private List<Species> getNewSpecies(String databaseName) throws SQLException {
		List<Species> speciesNewDb = getSpeciesFromcatdir(newSqliteConnection, databaseName);
		List<Species> speciesOldDb = getSpeciesFromcatdir(oldSqliteConnection, databaseName);
		speciesNewDb.removeAll(speciesOldDb);
		return speciesNewDb;
	}

	/**
	 * Return the list of deleted species for the given sub-database.
	 *
	 * @param databaseName The name of the sub-database for which we want the deleted species.
	 * @return the list of deleted species
	 * @throws SQLException In case of SQL error.
	 */
	private List<Species> getDeletedSpecies(String databaseName) throws SQLException {
		List<Species> speciesNewDb = getSpeciesFromcatdir(newSqliteConnection, databaseName);
		List<Species> speciesOldDb = getSpeciesFromcatdir(oldSqliteConnection, databaseName);
		speciesOldDb.removeAll(speciesNewDb);
		return speciesOldDb;
	}

	/**
	 * Initialize base informations for the two databases.
	 *
	 * @throws SQLException In case of SQL error.
	 */
	private void initBaseInfos() throws SQLException {
		newDbName = newSqliteConnection.getId();
		oldDbName = oldSqliteConnection.getId();

		newDbSpeciesSize = SqliteIntegrity.getNumberOfMolecules(newSqliteConnection.getSqlConn());
		oldDbSpeciesSize = SqliteIntegrity.getNumberOfMolecules(oldSqliteConnection.getSqlConn());

		newDbTransitionsSize = SqliteIntegrity.getNumberOfTransitions(newSqliteConnection.getSqlConn());
		oldDbTransitionsSize = SqliteIntegrity.getNumberOfTransitions(oldSqliteConnection.getSqlConn());

		newDbListDatabase = WhatsNewInDatabase.getDatabaseList(newSqliteConnection);
		oldDbListDatabase = WhatsNewInDatabase.getDatabaseList(oldSqliteConnection);
		allDbList = getAllDb(newDbListDatabase, oldDbListDatabase);

		mapNameDbNumber = getMapNameNumberDb();
	}

	/**
	 * Create and return a Map<String, Integer> having for key:value the
	 *  Sub-database name:index in the database.
	 *
	 * @return The created map.
	 * @throws SQLException In case of SQL error.
	 */
	private Map<String, Integer> getMapNameNumberDb() throws SQLException {
		ResultSet res = newSqliteConnection.getSqlConn().createStatement()
				.executeQuery("SELECT DISTINCT name, id FROM cassis_databases");
		Map<String, Integer> m = new HashMap<>();
		while (res.next()) {
			m.put(res.getString(1), res.getInt(2));
		}
		return m;
	}

	/**
	 * Return if a database have modification base on new/deleted/modified and
	 *  species with same QN.
	 *
	 * @param newSpecies The list of new species.
	 * @param deletedSpecies The list of deleted species.
	 * @param cd {@link ChangeDatabase} object, having modified transitions.
	 * @param sq {@link SameQn} object, having transitions with same QN.
	 * @return
	 */
	private boolean haveModification(List<Species> newSpecies, List<Species> deletedSpecies, ChangeDatabase cd, SameQn sq) {
		return !newSpecies.isEmpty() || !deletedSpecies.isEmpty() || cd.haveChange() || sq.size() >= 1;
	}

	/**
	 * Return if the sub-database is new.
	 *
	 * @param dbName The sub-database name.
	 * @return true if the sub-database if new, false otherwise.
	 */
	private boolean isNew(String dbName) {
		return newDbListDatabase.contains(dbName) && !oldDbListDatabase.contains(dbName);
	}

	/**
	 * Return if the sub-database is removed.
	 *
	 * @param dbName The sub-database name.
	 * @return true if the sub-database if removed, false otherwise.
	 */
	private boolean isRemoved(String dbName) {
		return !newDbListDatabase.contains(dbName) && oldDbListDatabase.contains(dbName);
	}

	/**
	 * Return the list of new transitions for the given tag.
	 *
	 * @param tag The tag.
	 * @return The list of new transitions.
	 */
	private List<LineDescriptionDB> getNewTransitions(int tag) {
		SimpleMoleculeDescriptionDB molNewDb = newSqliteConnection.getSimpleMolecule(tag);
		if (molNewDb == null) {
			return Collections.emptyList();
		}
		List<LineDescriptionDB> newDbTrans = newSqliteConnection.getLineDescriptionDB(molNewDb);
		SimpleMoleculeDescriptionDB molOldDb = oldSqliteConnection.getSimpleMolecule(tag);
		if (molOldDb == null) {
			return newDbTrans;
		} else {
			List<LineDescriptionDB> oldDbTrans = oldSqliteConnection.getLineDescriptionDB(molOldDb);
			newDbTrans.removeAll(oldDbTrans);
			return newDbTrans;
		}
	}

	/**
	 * Return the list of removed transitions for the given tag.
	 *
	 * @param tag The tag.
	 * @return The list of removed transitions.
	 */
	private List<LineDescriptionDB> getRemovedTransitions(int tag) {
		SimpleMoleculeDescriptionDB molOldDb = oldSqliteConnection.getSimpleMolecule(tag);
		if (molOldDb == null) {
			return Collections.emptyList();
		}
		List<LineDescriptionDB> oldDbTrans = oldSqliteConnection.getLineDescriptionDB(molOldDb);
		SimpleMoleculeDescriptionDB molNewDb = newSqliteConnection.getSimpleMolecule(tag);

		if (molNewDb == null) {
			return oldDbTrans;
		} else {
			List<LineDescriptionDB> newDbTrans = newSqliteConnection.getLineDescriptionDB(molNewDb);
			oldDbTrans.removeAll(newDbTrans);
			return oldDbTrans;
		}
	}

	/**
	 * Add a row for a line.
	 *
	 * @param builder The writer used to add the report.
	 * @param l The line.
	 * @param htmlClass The HTML class used.
	 * @throws IOException In case of IO error.
	 */
	private void addTransitionLine(BufferedWriter builder, LineDescriptionDB l, String htmlClass) throws IOException {
		builder.append("  <tr>\n");
		builder.append("    <td class=\"").append(htmlClass).append("\">").append(String.valueOf(l.getFrequency())).append("</td>\n");
		builder.append("    <td class=\"").append(htmlClass).append("\">").append(l.getQuanticNumbers()).append("</td>\n");
		builder.append("    <td class=\"").append(htmlClass).append("\">").append(String.valueOf(l.getError())).append("</td>\n");
		builder.append("    <td class=\"").append(htmlClass).append("\">").append(String.valueOf(l.getAint())).append("</td>\n");
		builder.append("    <td class=\"").append(htmlClass).append("\">").append(String.valueOf(l.getElow())).append("</td>\n");
		builder.append("    <td class=\"").append(htmlClass).append("\">").append(String.valueOf(l.getIgu())).append("</td>\n");
		builder.append("    <td class=\"").append(htmlClass).append("\">").append(String.valueOf(l.getEup())).append("</td>\n");
		builder.append("  </tr>\n");
	}

	/**
	 * Add a transitions array.
	 *
	 * @param builder The writer used to add the report.
	 * @param htmlClass The HTML class used.
	 * @param lines The list of lines/transitions.
	 * @throws IOException In case of IO error.
	 */
	private void addTransitionsArray(BufferedWriter builder, String htmlClass,
			List<LineDescriptionDB> lines) throws IOException {
		builder.append("<table>\n");
		builder.append("  <tr>\n");
		builder.append("    <th>Frequency</th>\n");
		builder.append("    <th>Quantum Numbers</th>\n");
		builder.append("    <th>Error</th>\n");
		builder.append("    <th>Aint</th>\n");
		builder.append("    <th>Elow</th>\n");
		builder.append("    <th>Igu</th>\n");
		builder.append("    <th>Eup</th>\n");
		builder.append("  </tr>\n");
		for (LineDescriptionDB line : lines) {
			addTransitionLine(builder, line, htmlClass);
		}
		builder.append("</table>\n");
	}

	/**
	 * Add a row for a duplicate line.
	 *
	 * @param builder The writer used to add the report.
	 * @param l The line.
	 * @param htmlClass The HTML class used.
	 * @throws IOException In case of IO error.
	 */
	private void addDuplicateTransitionLine(BufferedWriter builder, LineDescriptionDB l, String htmlClass) throws IOException {
		builder.append("  <tr>\n");
		builder.append("    <td class=\"").append(htmlClass).append("\">").append(String.valueOf(l.getFrequency())).append("</td>\n");
		builder.append("    <td class=\"").append(htmlClass).append("\">").append(l.getQuanticNumbers()).append("</td>\n");
		builder.append("    <td class=\"").append(htmlClass).append("\">").append(String.valueOf(l.getError())).append("</td>\n");
		builder.append("    <td class=\"").append(htmlClass).append("\">").append(String.valueOf(l.getAint())).append("</td>\n");
		builder.append("    <td class=\"").append(htmlClass).append("\">").append(String.valueOf(l.getElow())).append("</td>\n");
		builder.append("    <td class=\"").append(htmlClass).append("\">").append(String.valueOf(l.getIgu())).append("</td>\n");
		builder.append("    <td class=\"").append(htmlClass).append("\">").append(String.valueOf(l.getEup())).append("</td>\n");
		String obsComp = l.getGammaSelf() > 0 ? "Comp." : "Meas.";
		builder.append("    <td class=\"").append(htmlClass).append("\">").append(obsComp).append("</td>\n");
		builder.append("  </tr>\n");
	}

	/**
	 * Add an array for duplicate lines.
	 *
	 * @param builder The writer used to add the report.
	 * @param lDup The list of duplicate lines/transitions.
	 * @throws IOException In case of IO error.
	 */
	private void addDuplicateArray(BufferedWriter builder, List<List<LineDescriptionDB>> lDup) throws IOException {
		builder.append("<table>\n");
		builder.append("  <tr>\n");
		builder.append("    <th>Frequency</th>\n");
		builder.append("    <th>Quantum Numbers</th>\n");
		builder.append("    <th>Error</th>\n");
		builder.append("    <th>Aint</th>\n");
		builder.append("    <th>Elow</th>\n");
		builder.append("    <th>Igu</th>\n");
		builder.append("    <th>Eup</th>\n");
		builder.append("    <th>Meas/Comp</th>\n");
		builder.append("  </tr>\n");
		String classe = "a";
		for (List<LineDescriptionDB> lines : lDup) {
			for (LineDescriptionDB line : lines) {
				addDuplicateTransitionLine(builder, line, classe);
			}

			if ("a".equals(classe)) {
				classe = "b";
			} else {
				classe = "a";
			}
		}
		builder.append("</table>\n");
	}

	/**
	 * Check then return if a list of line have two types of line: computed and observed.
	 *
	 * @param ls The list of line.
	 * @return true if there is two line with one being computed and the other being
	 *  observed, false otherwise.
	 */
	boolean diffCompObs(List<LineDescriptionDB> ls) {
		return ls.size() == 2 &&
				((ls.get(0).getGammaSelf() > 0 && ls.get(1).getGammaSelf() < 0) ||
						(ls.get(1).getGammaSelf() > 0 && ls.get(0).getGammaSelf() < 0));
	}

	/**
	 * Get the {@link ListTrans} object for a given species.
	 *
	 * @param tag The tag of the species.
	 * @return the {@link ListTrans} object
	 */
	private ListTrans getListTrans(int tag) {
		SimpleMoleculeDescriptionDB molOldDb = oldSqliteConnection.getSimpleMolecule(tag);
		List<LineDescriptionDB> oldDbTrans = oldSqliteConnection.getLineDescriptionDB(molOldDb);
		SimpleMoleculeDescriptionDB molNewDb = newSqliteConnection.getSimpleMolecule(tag);
		List<LineDescriptionDB> newDbTrans = newSqliteConnection.getLineDescriptionDB(molNewDb);
		String name = newSqliteConnection.getMolName(tag);
		return new ListTrans(tag, name, newDbTrans, oldDbTrans);
	}

	/**
	 * Return the ordered list of tag in a sub-database.
	 *
	 * @param databaseName The sub-database name.
	 * @param connection The database connection.
	 * @return the ordered list of tag in a sub-database.
	 * @throws SQLException In case of SQL error.
	 */
	private List<Integer> getTagsInDatabase(String databaseName,
			SqliteDataBaseConnection connection) throws SQLException {
		List<Integer> l = new ArrayList<>();
		String query = "SELECT DISTINCT tag FROM catdir WHERE id_database = "
				+ mapNameDbNumber.get(databaseName) + " ORDER BY tag";

		try (Statement stmt = connection.getSqlConn().createStatement();
				ResultSet rsMolecules = stmt.executeQuery(query)) {
			while (rsMolecules.next()) {
				l.add(rsMolecules.getInt(1));
			}
		}
		return l;
	}

	/**
	 * Return the list of tag for the given sub-database who are both in the old
	 *  and new database.
	 *
	 * @param databaseName The sub-database name.
	 * @return the list of tag for the sub-database who are both in the old and
	 *  new database.
	 * @throws SQLException In case of SQL error.
	 */
	private List<Integer> getTagsInTwoDatabase(String databaseName) throws SQLException {
		List<Integer> tagsNews = getTagsInDatabase(databaseName, newSqliteConnection);
		List<Integer> tagsOld = getTagsInDatabase(databaseName, oldSqliteConnection);
		List<Integer> l = new ArrayList<>();

		for (int i : tagsNews) {
			if (tagsOld.contains(i)) {
				l.add(i);
			}
		}
		return l;
	}

	/**
	 * Return the {@link ChangeDatabase} object for the given sub-database.
	 *
	 * @param database The sub-database name.
	 * @return the {@link ChangeDatabase} object for the given sub-database.
	 * @throws SQLException In case of SQL error.
	 * @throws InterruptedException In case of Thread error.
	 * @throws ExecutionException In case of Thread error.
	 */
	private ChangeDatabase getChange(String database) throws SQLException, InterruptedException, ExecutionException {
		List<Integer> tagsInTwoDatabase = getTagsInTwoDatabase(database);
		List<ChangeTransitions> changes = new ArrayList<>(tagsInTwoDatabase.size());
		ExecutorService es = Executors.newFixedThreadPool(8);
		List<Future<ChangeTransitions>> lFuture = new ArrayList<>(tagsInTwoDatabase.size());
		for (int tag : tagsInTwoDatabase) {
			ListTrans lt = getListTrans(tag);
			lFuture.add(es.submit(new GetDiff(lt)));
		}

		for (Future<ChangeTransitions> f : lFuture) {
			ChangeTransitions ct = f.get();
			if (ct.haveChange()) {
				changes.add(ct);
			}
		}
		es.shutdown();
		lFuture.clear();
		return new ChangeDatabase(changes);
	}

	/**
	 * Return the list of transitions with the same QN as a {@link SameQn} object
	 *  for a given sub-database.
	 *
	 * @param databaseName The sub-database name.
	 * @return the list of transitions with the same QN as a {@link SameQn} object.
	 * @throws SQLException In case of SQL error.
	 */
	private SameQn getSameQn(String databaseName) throws SQLException {
		String query = "SELECT transitions.itag, fMHz, transitions.qn, err, aint, elow, igu, gamma_self, eup FROM"
				+ " (SELECT itag, qn FROM transitions WHERE id_database = " + mapNameDbNumber.get(databaseName)
				+ " GROUP BY itag, qn HAVING count(*) > 1 ORDER BY itag)"
				+ " AS sub, transitions WHERE sub.itag = transitions.itag AND sub.qn = transitions.qn"
				+ " ORDER BY transitions.itag, transitions.qn";
		List<LineDescriptionDB> lineDescriptionDBs = new ArrayList<LineDescriptionDB>();
		Map<Integer, String> mTagName = new HashMap<>();
		try (Statement stmt = newSqliteConnection.getSqlConn().createStatement();
				ResultSet rsTransitions = stmt.executeQuery(query)) {
			while (rsTransitions.next()) {
				lineDescriptionDBs.add(new LineDescriptionDB(rsTransitions.getInt(1),
						rsTransitions.getDouble(2), rsTransitions.getString(3),
						rsTransitions.getDouble(4), rsTransitions.getDouble(5),
						rsTransitions.getDouble(6), rsTransitions.getInt(7),
						rsTransitions.getDouble(8), rsTransitions.getDouble(9)));
				if (!mTagName.containsKey(rsTransitions.getInt(1))) {
					mTagName.put(rsTransitions.getInt(1), newSqliteConnection.getMolName(rsTransitions.getInt(1)));
				}
			}
		}
		return new SameQn(lineDescriptionDBs, mTagName);
	}

	/**
	 * Return the list of sub-database name for a given connection.
	 *
	 * @param dataBaseConnection The connection.
	 * @return the list of sub-database name.
	 * @throws SQLException In case of SQL error.
	 */
	public static List<String> getDatabaseList(SqliteDataBaseConnection dataBaseConnection)
			throws SQLException {
		ResultSet res = dataBaseConnection.getSqlConn().createStatement()
				.executeQuery("SELECT DISTINCT name FROM cassis_databases");
		List<String> baseNames = new ArrayList<String>();
		while (res.next()) {
			baseNames.add(res.getString(1));
		}
		return baseNames;
	}



	/**
	 * Class describing a species (tag and name).
	 */
	private class Species implements Cloneable {

		private int tag;
		private String name;


		/**
		 * Constructor.
		 *
		 * @param tag The tag.
		 * @param name The name.
		 */
		public Species(int tag, String name) {
			this.tag = tag;
			this.name = name;
		}

		/**
		 * Create hashCode.
		 *
		 * @return the hashCode.
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			result = prime * result + tag;
			return result;
		}

		/**
		 * Test equality with another object.
		 *
		 * @param obj The other object to the equality with.
		 * @return true if the two object are equals, false otherwise.
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Species other = (Species) obj;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			if (tag != other.tag)
				return false;
			return true;
		}

		/**
		 * Return a String representation of the object.
		 *
		 * @return a String representation of the object.
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return name + " (" + tag + ')';
		}
	}



	/**
	 * Class to store transitions/lines for a given species on the old and new database.
	 */
	private class ListTrans {

		private List<LineDescriptionDB> newsLines;
		private List<LineDescriptionDB> oldLines;
		private int tag;
		private String name;


		/**
		 * Constructor.
		 *
		 * @param tag Tag of the species.
		 * @param name Name of the species.
		 * @param newsLines Transitions/lines on the new database.
		 * @param oldLines Transitions/lines on the old database.
		 */
		public ListTrans(int tag, String name, List<LineDescriptionDB> newsLines, List<LineDescriptionDB> oldLines) {
			this.tag = tag;
			this.name = name;
			this.newsLines = newsLines;
			this.oldLines = oldLines;
		}
	}




	/**
	 * Class to store added and removed lines/transitions for a given species.
	 */
	private class ChangeTransitions {

		private List<LineDescriptionDB> addedLines;
		private List<LineDescriptionDB> removedLines;
		private int tag;
		private String name;


		/**
		 * Constructor.
		 *
		 * @param tag Tag of the species.
		 * @param name Name of the species.
		 * @param addedLines The list of added lines/transitions.
		 * @param removedLines The list of removed lines/transitions.
		 */
		public ChangeTransitions(int tag, String name, List<LineDescriptionDB> addedLines, List<LineDescriptionDB> removedLines) {
			this.tag = tag;
			this.name = name;
			this.addedLines = addedLines;
			this.removedLines = removedLines;
		}

		/**
		 * Return if there is a change (lines added or removed).
		 *
		 * @return if there is a change.
		 */
		private boolean haveChange() {
			return !addedLines.isEmpty() || !removedLines.isEmpty();
		}

		/**
		 * Return a String representation of the object.
		 *
		 * @return a String representation of the object.
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return name + " (" + tag + ')';
		}
	}




	/**
	 * Class to store all {@link ChangeTransitions} for a sub-database.
	 */
	private class ChangeDatabase {

		private List<ChangeTransitions> ctList;

		/**
		 * Constructor.
		 *
		 * @param ctList List of {@link ChangeTransitions}.
		 */
		public ChangeDatabase(List<ChangeTransitions> ctList) {
			this.ctList = ctList;
		}

		/**
		 * Return if there is a change.
		 *
		 * @return true if there is a change, false otherwise.
		 */
		public boolean haveChange() {
			return !ctList.isEmpty();
		}
	}



	/**
	 * Callable class to handle the operation for computing added and deleted
	 *  transitions for a species.
	 */
	private class GetDiff implements Callable<ChangeTransitions> {

		private ListTrans lt;


		/**
		 * Constructor.
		 *
		 * @param lt {@link ListTrans} object with transitions from new and old database.
		 */
		public GetDiff(ListTrans lt) {
			this.lt = lt;
		}

		/**
		 * Compute then return the added and deleted transitions.
		 *
		 * @return the {@link ChangeTransitions} object with added and deleted transitions.
		 * @see java.util.concurrent.Callable#call()
		 */
		@Override
		public ChangeTransitions call() throws Exception {
			List<LineDescriptionDB> oldDbTrans = lt.oldLines;
			List<LineDescriptionDB> newDbTrans = lt.newsLines;

			Iterator<LineDescriptionDB> itN = newDbTrans.iterator();
			while (itN.hasNext()) {
				LineDescriptionDB l = itN.next();
				if (oldDbTrans.remove(l)) {
					itN.remove();
				}
			}
			Iterator<LineDescriptionDB> itO = oldDbTrans.iterator();
			while (itO.hasNext()) {
				LineDescriptionDB l = itO.next();
				if (newDbTrans.remove(l)) {
					itO.remove();
				}
			}
			return new ChangeTransitions(lt.tag, lt.name, newDbTrans, oldDbTrans);
		}

	}



	/**
	 * Class to order line with same QN.
	 */
	private class SameQn {

		private List<List<LineDescriptionDB>> listDuplicateQn;
		private Map<Integer, String> mTagName;

		/**
		 * Constructor.
		 *
		 * @param lines List of lines.
		 * @param mTagName Map<tag, name species>
		 */
		public SameQn(List<LineDescriptionDB> lines, Map<Integer, String> mTagName) {
			this.mTagName = mTagName;
			this.listDuplicateQn = new ArrayList<>();
			handleLines(lines);
		}

		/**
		 * Handle lines and order them by list of same tag/qn.
		 *
		 * @param lines The list of lines.
		 */
		private void handleLines(List<LineDescriptionDB> lines) {
			List<LineDescriptionDB> l = new ArrayList<>(2);
			LineDescriptionDB previousLine = null;
			for (LineDescriptionDB line : lines) {
				if (previousLine == null || (line.getTag() == previousLine.getTag() && line.getQuanticNumbers().equals(previousLine.getQuanticNumbers()))) {
					l.add(line);
				} else {
					listDuplicateQn.add(l);
					l = new ArrayList<>(2);
					l.add(line);
				}
				previousLine = line;
			}
			if (l != null && !l.isEmpty()) {
				listDuplicateQn.add(l);
			}
		}

		/**
		 * Return the list of species.
		 *
		 * @return the list of species.
		 */
		public List<Species> getSpecies() {
			List<Species> lS = new ArrayList<>(mTagName.size());
			for (Entry<Integer, String> entry : mTagName.entrySet()) {
				lS.add(new Species(entry.getKey(), entry.getValue()));
			}
			return lS;
		}

		/**
		 * Return the number of lines with duplicate (as one for two lines with same tag/qn).
		 *
		 * @return the number of lines with duplicate
		 */
		public int size() {
			return listDuplicateQn.size();
		}

		/**
		 * Return the list of duplicates for the species with the given tag.
		 *
		 * @param tag The tag.
		 * @return the list of duplicates for the species with the given tag.
		 */
		private List<List<LineDescriptionDB>> getDuplicate(int tag) {
			List<List<LineDescriptionDB>> ll = new ArrayList<>();
			for (List<LineDescriptionDB> l : listDuplicateQn) {
				if (l.get(0).getTag() == tag) {
					ll.add(l);
				}
			}
			return ll;
		}

		/**
		 * Check then return if a list of line have two types of line: computed
		 *  and observed.
		 *
		 * @param ls The list of line.
		 * @return true if there is two line with one being computed and the other being
		 *  observed, false otherwise.
		 */
		private boolean diffCompObs(List<LineDescriptionDB> ls) {
			return ls.size() == 2 &&
					((ls.get(0).getGammaSelf() > 0 && ls.get(1).getGammaSelf() < 0) ||
							(ls.get(1).getGammaSelf() > 0 && ls.get(0).getGammaSelf() < 0));
		}

		/**
		 * Compute then return the number of duplicate who have same transition
		 *  computed and observed.
		 *
		 * @param lll The list of duplicates lines.
		 * @return the number of duplicate who have same transition computed and observed.
		 */
		public int getDuplicateWithObsCompSize(List<List<LineDescriptionDB>> lll) {
			int size = 0;
			for (List<LineDescriptionDB> ll : lll) {
				if (diffCompObs(ll)) {
					size++;
				}
			}
			return size;
		}
	}
}
