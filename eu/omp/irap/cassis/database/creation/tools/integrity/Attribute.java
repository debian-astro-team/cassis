/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.tools.integrity;

/**
 * Class to basically represent a database Attribute.
 *
 * @author M. Boiziot
 */
public class Attribute {

	public enum GENERAL_TYPE { BLOB, BOOLEAN, DATE, DATETIME, INTEGER, REAL, TEXT, UNKNOWN }
	private final String name;
	private final String type;


	/**
	 * Constructor.
	 *
	 * @param name Name of the attribute.
	 * @param type Type of the attribute.
	 */
	public Attribute(String name, String type) {
		this.name = name;
		this.type = type;
	}

	/**
	 * Return the name of the attribute.
	 *
	 * @return the name of the attribute.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Return the type of the attribute.
	 *
	 * @return the type of the attribute.
	 */
	public String getType() {
		return type;
	}

	/**
	 * Return the general type of the attribute.
	 *
	 * @return the general type of the attribute.
	 */
	public GENERAL_TYPE getGeneralType() {
		return Attribute.getGeneralType(type);
	}

	/**
	 * Return the general type of the given attribute type.
	 *
	 * @param attributeType The attribute type for which we want the general type.
	 * @return the general type of the given attribute type.
	 */
	public static GENERAL_TYPE getGeneralType(String attributeType) {
		GENERAL_TYPE generalType = GENERAL_TYPE.UNKNOWN;
		if (isReal(attributeType)) {
			 generalType = GENERAL_TYPE.REAL;
		} else if (isInteger(attributeType)) {
			generalType = GENERAL_TYPE.INTEGER;
		} else if (isText(attributeType)) {
			generalType = GENERAL_TYPE.TEXT;
		} else if (isBoolean(attributeType)) {
			generalType = GENERAL_TYPE.BOOLEAN;
		} else if (isDate(attributeType)) {
			generalType = GENERAL_TYPE.DATE;
		} else if (isDateTime(attributeType)) {
			generalType = GENERAL_TYPE.DATETIME;
		} else if (isBlob(attributeType)) {
			generalType = GENERAL_TYPE.BLOB;
		}
		return generalType;
	}

	/**
	 * Return if the given attribute type is a blob.
	 *
	 * @param attributeType The attribute type.
	 * @return true if the attribute type is a blob, false otherwise.
	 */
	private static boolean isBlob(String attributeType) {
		return attributeType.equalsIgnoreCase("blob");
	}

	/**
	 * Return if the given attribute type is a date.
	 *
	 * @param attributeType The attribute type.
	 * @return true if the attribute type is a date, false otherwise.
	 */
	private static boolean isDate(String attributeType) {
		return attributeType.equalsIgnoreCase("date");
	}

	/**
	 * Return if the given attribute type is a datetime.
	 *
	 * @param attributeType The attribute type.
	 * @return true if the attribute type is a datetime, false otherwise.
	 */
	private static boolean isDateTime(String attributeType) {
		return attributeType.equalsIgnoreCase("datetime");
	}

	/**
	 * Return if the given attribute type is a boolean.
	 *
	 * @param attributeType The attribute type.
	 * @return true if the attribute type is a boolean, false otherwise.
	 */
	private static boolean isBoolean(String attributeType) {
		return attributeType.toLowerCase().contains("bool");
	}

	/**
	 * Return if the given attribute type is a text.
	 *
	 * @param attributeType The attribute type.
	 * @return true if the attribute type is a text, false otherwise.
	 */
	private static boolean isText(String attributeType) {
		String lAtt = attributeType.toLowerCase();
		return lAtt.contains("char") || lAtt.contains("text") || lAtt.contains("clob");
	}

	/**
	 * Return if the given attribute type is an integer.
	 *
	 * @param attributeType The attribute type.
	 * @return true if the attribute type is an integer, false otherwise.
	 */
	private static boolean isInteger(String attributeType) {
		return attributeType.toLowerCase().contains("int");
	}

	/**
	 * Return if the given attribute type is a real.
	 *
	 * @param attributeType The attribute type.
	 * @return true if the attribute type is a real, false otherwise.
	 */
	private static boolean isReal(String attributeType) {
		String lAtt = attributeType.toLowerCase();
		return lAtt.contains("real") || lAtt.contains("double") ||
				lAtt.contains("float") || lAtt.contains("numeric") ||
				lAtt.contains("decimal");
	}

	/**
	 * Create an hashCode.
	 *
	 * @see java.lang.Object#hashCode()
	 * @return the hashCode.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	/**
	 * Test the equality with another object.
	 * In order to be equals here, the two object must have the same name and
	 *  the same <b>general</b> type.
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 * @param obj the reference object with which to compare.
	 * @return true if the two objects are equals, false otherwise.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Attribute other = (Attribute) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (getGeneralType() != other.getGeneralType()) {
			return false;
		}
		return true;
	}

	/**
	 * Create a string representation of the object.
	 *
	 * @see java.lang.Object#toString()
	 * @return a string representation of the object.
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Attribute [name=");
		builder.append(name);
		builder.append(", type=");
		builder.append(type);
		builder.append(']');
		return builder.toString();
	}

}
