/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.database.creation.tools;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import eu.omp.irap.cassis.database.access.DataBaseConnection;
import eu.omp.irap.cassis.database.access.LineDescriptionDB;
import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB;
import eu.omp.irap.cassis.database.access.TypeDataBase;
import eu.omp.irap.cassis.database.creation.importation.ImportDatabase;
import eu.omp.irap.cassis.database.creation.importation.options.SelectionOption;
import eu.omp.irap.cassis.database.creation.importation.options.file.FileDatabaseOptions;

/**
 * This class serve us to contain all the useful informations of a database
 *
 * @author bpenavayre
 *
 */
public class DatabaseContainer {

	private static final String IMPORT_TRANSITIONS_FORMAT = "Imported %d transitions for tag %d.";
	private static final String DOWNLOADING_TRANSITION = "Downloading all transitions for molecule of tag %d.";

	private String databaseName;
	private String path;
	private TypeDataBase type;
	private int cpt = -1;

	/**
	 * @see FileDatabaseOptions
	 * @see SelectionOption
	 **/
	private Object option;
	private boolean enabled;
	private List<FullMolecule> molecules;
	private List<Boolean> active;


	/**
	 * Create a database container with it's path and type.</br> This method is
	 * mainly for script usage (where the database's name is irrelevant)
	 *
	 * @param path
	 * @param type
	 */
	public DatabaseContainer(String path, TypeDataBase type) {
		this(null, path, type);
	}

	/**
	 * Constructor for special cases
	 *
	 * @see #option
	 *
	 * @param databaseName
	 * @param path
	 * @param type
	 * @param option
	 */
	public DatabaseContainer(String databaseName, String path,
			TypeDataBase type, Object option) {
		this(databaseName, path, type);
		this.option = option;
	}

	/**
	 * This is the main constructor, it sets the name, path and type of the
	 * database</br> and set the default values.
	 *
	 * @param databaseName
	 *            name of database
	 * @param path
	 *            the path of the database
	 * @param type
	 *            the type of the database
	 */
	public DatabaseContainer(String databaseName, String path, TypeDataBase type) {
		this.databaseName = databaseName;
		this.path = path;
		this.type = type;
		this.active = new ArrayList<Boolean>();
		this.enabled = true;
	}

	/**
	 * @return the name of the database
	 */
	public String getDatabaseName() {
		return databaseName;
	}

	/**
	 * Change the name of the database
	 *
	 * @param dbName
	 *            the name of the database
	 */
	public void setDatabaseName(String dbName) {
		this.databaseName = dbName;
	}

	/**
	 * @return the path of the database
	 */
	public String getPath() {
		return path;
	}

	/**
	 * Change the path of the database
	 *
	 * @param path
	 *            new path
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return type of the database {@link TypeDataBase}
	 */
	public TypeDataBase getType() {
		return this.type;
	}

	/**
	 * Change the type of the database
	 *
	 * @param type
	 *            new type
	 */
	public void setType(TypeDataBase type) {
		this.type = type;
	}

	/**
	 * Parse the string given in argument</br> for every char 't' add in the
	 * list of boolean "active" true</br> else add false
	 *
	 * @param value
	 *            string to parse
	 */
	public void setActive(String value) {
		if (value != null) {
			for (int i = 0; i < value.length(); i++) {
				active.add(value.charAt(i) == 't');
			}
		}
	}

	/**
	 * @param row
	 *            the row of the molecule
	 * @return the state of the molecule in the row given in parameter
	 */
	public boolean getMoleculeState(int row) {
		return active.get(row);
	}

	/**
	 * @param mol
	 *            the molecule for which we seek its state
	 * @return the state of the molecule with the molecule as parameter
	 */
	public boolean getMoleculeState(SimpleMoleculeDescriptionDB mol) {
		return active.get(molecules.indexOf(mol));
	}

	/**
	 * Sets the list of molecule with the one given in parameter,</br> the add
	 * the corresponding number of elements in the list in the active list</br>
	 * by using the default value "true"
	 *
	 * @param newMoleculesList
	 */
	public void setMolecules(List<SimpleMoleculeDescriptionDB> newMoleculesList) {
		molecules = new ArrayList<>(newMoleculesList.size());
		for (SimpleMoleculeDescriptionDB mol : newMoleculesList) {
			molecules.add(new FullMolecule(mol));
		}
		if (active == null) {
			active = new ArrayList<Boolean>();
		}
		for (int i = 0; i < molecules.size(); i++) {
			active.add(true);
		}
	}

	/**
	 * Import the transitions
	 *
	 * @param connection
	 *            the link with the database
	 * @return true if the importation finished correctly
	 */
	public int setLines(DataBaseConnection connection, Logger logger) {
		int result = ImportDatabase.IMPORT_SUCCESS;
		int numberOfErrors = 0;

		for (FullMolecule mol : molecules) {
			if (!mol.setPartiFunc(connection, logger)) {
				result = ImportDatabase.IMPORT_OK_WIH_ERROR;
				continue;
			}
			log(logger, String.format(DOWNLOADING_TRANSITION, mol.getTag()),
					Level.INFO);
			if (!mol.setTransitions(connection, logger)) {
				result = ImportDatabase.IMPORT_OK_WIH_ERROR;
				numberOfErrors++;
				continue;
			}
			log(logger, String.format(IMPORT_TRANSITIONS_FORMAT, mol
					.getTransitions().size(), mol.getTag()), Level.INFO);
		}
		if (numberOfErrors == molecules.size()) {
			return ImportDatabase.IMPORT_FAILED;
		}
		return result;
	}

	private void log(Logger logger, String message, Level level) {
		if (logger == null) {
			return;
		}
		logger.log(level, message);
	}

	/**
	 * @return true if the all list of transitions non-empty and non-null
	 */
	public boolean checkTransitions() {
		List<LineDescriptionDB> lines;
		for (FullMolecule mol : molecules) {
			lines = mol.getTransitions();
			if (lines == null || lines.isEmpty()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @return the list of molecules
	 */
	public List<FullMolecule> getMolecules() {
		return molecules;
	}

	/**
	 * Change the state of a molecule with its index
	 *
	 * @param mol
	 *            the index of the molecule
	 * @param newEnableState
	 *            the new state
	 */
	public void setMoleculeState(int index, boolean newEnableState) {
		active.set(index, newEnableState);
	}

	/**
	 * Used to convert the list of boolean to a parse-able string
	 *
	 * @param active
	 *            list of boolean
	 * @return the parse-able string
	 */
	private String listBooleanToString() {
		StringBuilder str = new StringBuilder(active.size());
		for (boolean isActif : active) {
			str.append(isActif ? 't' : 'f');
		}
		return str.toString();
	}

	/**
	 * Write a parsable output into a BufferedWriter to save the state</br> of
	 * this DatabaseContainer
	 *
	 * @param out
	 *            the BufferedWriter
	 * @throws IOException
	 */
	public void saveConfig(BufferedWriter out) throws IOException {
		out.write("#Component parameters " + cpt + '\n');
		out.newLine();
		out.write(cpt + "Name=" + databaseName);
		out.newLine();
		out.write(cpt + "Path=" + path);
		out.newLine();
		out.write(cpt + "Type=" + type);
		out.newLine();
		out.write(cpt + "Enabled=" + enabled);
		out.newLine();
		out.write(cpt + "Active=" + listBooleanToString());
		out.newLine();
		if (option != null) {
			out.write(cpt + "Option=" + option);
			out.newLine();
		}
		out.flush();
	}

	/**
	 * Load configuration from {@link Properties}
	 *
	 * @param prop
	 * @param cpt
	 */
	public void loadConfig(Properties prop, int cpt) {
		this.cpt = cpt;
		enabled = Boolean.valueOf(prop.getProperty(cpt + "Enabled", "true"));
		databaseName = prop.getProperty(cpt + "Name");
		path = prop.getProperty(cpt + "Path");
		type = TypeDataBase.valueOf(prop.getProperty(cpt + "Type"));
		setActive(prop.getProperty(cpt + "Active"));
		option = prop.getProperty(cpt + "Option");
		if (type.equals(TypeDataBase.FILE)) {
			option = new FileDatabaseOptions((String) option);
		}
	}

	/**
	 * Return the state of the database
	 *
	 * @return
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * Change the option variable
	 *
	 * @param option
	 */
	public void setOption(Object option) {
		this.option = option;
	}

	/**
	 * To set the option of molecules selection for importation
	 *
	 * @param selection
	 */
	public void setSelectionOption(String selection) {
		if (type.equals(TypeDataBase.FILE)) {
			((FileDatabaseOptions)getOption()).setSelectionOption(selection);
		} else {
			setOption(selection);
		}
	}

	/**
	 * @return the option
	 * @see #option
	 */
	public Object getOption() {
		return option;
	}

	/**
	 * Set the new state of the database
	 *
	 * @param newEnableState
	 *            the new state
	 */
	public void setEnabled(boolean newEnableState) {
		enabled = newEnableState;
	}

	/**
	 * Edit the index of this database for save/load purpose
	 *
	 * @param cpt
	 *            the index
	 */
	public void setCpt(int cpt) {
		this.cpt = cpt;
	}
}
