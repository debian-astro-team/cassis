/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.lineanalysis;

import java.util.List;

import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.gui.model.CassisResult;
import eu.omp.irap.cassis.gui.model.table.ModelIdentifiedInterface;

public class LineAnalysisResult implements CassisResult {

	private List<CommentedSpectrum> sumSpectrumList = null;
	private List<CommentedSpectrum> fileSpectrumList = null;
	private String telescope;
	private ModelIdentifiedInterface modelIdentifiedInterface = null;
	private int warning;


	public LineAnalysisResult(List<CommentedSpectrum> fileSpectrumList,
			List<CommentedSpectrum> sumSpectrumList) {

		this.sumSpectrumList = sumSpectrumList;
		this.fileSpectrumList = fileSpectrumList;
	}

	public List<CommentedSpectrum> getSumSpectrumList() {
		return sumSpectrumList;
	}

	public List<CommentedSpectrum> getFileSpectrumList() {
		return fileSpectrumList;
	}

	public void setTelescope(String telescope) {
		this.telescope = telescope;
	}

	public String getTelescope() {
		return telescope;
	}

	public ModelIdentifiedInterface getModelIdentifiedInterface() {
		return modelIdentifiedInterface;
	}

	public void setModelIdentifiedInterface(
			ModelIdentifiedInterface modelIdentifiedInterface) {
		this.modelIdentifiedInterface = modelIdentifiedInterface;
	}

	public void setWarning(int warning) {
		this.warning = warning;
	}

	public int getWarning() {
		return this.warning;
	}

	public XAxisCassis getAxisData(){
		return fileSpectrumList.get(0).getxAxisOrigin();
	}

	public int getNbPlots() {
		return getFileSpectrumList().size();
	}

	public boolean haveTheoriticalSpectra() {
		return getSumSpectrumList() != null;
	}

}
