/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.cassisd;

import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.cassisd.model.FileModel;
import eu.omp.irap.cassis.cassisd.model.LineAnalysisModel;
import eu.omp.irap.cassis.cassisd.model.OtherSpeciesModel;
import eu.omp.irap.cassis.cassisd.model.OtherSpeciesParameters;
import eu.omp.irap.cassis.cassisd.model.RotDiagramModel;
import eu.omp.irap.cassis.cassisd.model.synthetic.SyntheticModel;
import eu.omp.irap.cassis.common.CassisException;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.common.ParameterDescription;
import eu.omp.irap.cassis.common.Telescope;
import eu.omp.irap.cassis.common.TypeFrequency;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.database.access.AccessDataBase;
import eu.omp.irap.cassis.database.access.DataBaseConnection;
import eu.omp.irap.cassis.database.access.MoleculeDescriptionDB;
import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB;
import eu.omp.irap.cassis.database.access.UnknowMoleculeException;
import eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent.ComponentDescription;
import eu.omp.irap.cassis.lineanalysis.LineAnalysisParameters;
import eu.omp.irap.cassis.lineanalysis.LineAnalysisResult;
import eu.omp.irap.cassis.parameters.RotationalDiagramInput;
import eu.omp.irap.cassis.parameters.RotationalDiagramResult;
import eu.omp.irap.cassis.properties.Software;

public class ServerImpl implements Server {

	private static final Logger LOGGER = LoggerFactory.getLogger(ServerImpl.class);

	/**
	 * Constructor
	 */
	public ServerImpl() {
		 // Just create the an object with Server implementation
	}

	/**
	 * Return the temperature for the RotationnalDiagram.
	 */
	@Override
	public double getTemperatureRDCommand(double temperature, int tag) {
		DataBaseConnection db = AccessDataBase.getDataBaseConnection();
		MoleculeDescriptionDB molecule;
		try {
			molecule = db.getMoleculeDescriptionDB(new SimpleMoleculeDescriptionDB(tag, null));
		} catch (UnknowMoleculeException ume) {
			LOGGER.error("A molecule (tag: {}) is unknown", ume.getTag(), ume);
			JOptionPane.showMessageDialog(null, ume.getInterruptedMessage(),
					"Unknow molecule", JOptionPane.ERROR_MESSAGE);
			return Double.NaN;
		}

		return molecule.buildQt(temperature);
	}

	@Override
	public RotationalDiagramResult computeRotationalDiagram(
			RotationalDiagramInput input) throws UnknowMoleculeException {
		return new RotDiagramModel().compute(input);
	}

	/**
	 * Return 2 spectrum for the FileModel.
	 */
	@Override
	public CommentedSpectrum[] readFile(CassisSpectrum cassisSpectrum,
			Double frequencyMin, Double frequencyMax) throws CassisException {
		FileModel model = new FileModel(frequencyMin, frequencyMax);

		LOGGER.debug("Begin compute File Spectrum");
		CommentedSpectrum[] result = model.createSpectrum(cassisSpectrum);
		LOGGER.debug("End compute File Spectrum");
		return result;
	}

	@Override
	public CommentedSpectrum computeLteRadexSpectrum(Map<String, ParameterDescription> mapParameter,
			double[] freqs, List<ComponentDescription> listComponents, Telescope telescope) throws Exception {
		CommentedSpectrum commentedSpectrum = null;

		SyntheticModel syntheticModel = new SyntheticModel(mapParameter, freqs, listComponents, false, false, telescope);
		LOGGER.debug("Begin compute LTE-Radex Spectrum");

		commentedSpectrum = syntheticModel.createSpectrum();
		if (telescope.getName().startsWith("spire")) {
			commentedSpectrum.setyAxis(YAxisCassis.getYAxisJansky());
		}

		LOGGER.debug("End compute LTE-Radex Spectrum");
		return commentedSpectrum;
	}

	@Override
	public CommentedSpectrum computeLteRadexSpectrum(Map<String, ParameterDescription> mapParameter,
			 List<ComponentDescription> listComponents, Telescope telescope) throws Exception {
		return computeLteRadexSpectrum(mapParameter, null, listComponents, telescope);
	}

	@Override
	public LineAnalysisResult computeLineAnalysisSpectrum(
			LineAnalysisParameters parameters, CassisSpectrum cassisSpectrum)
			throws Exception {
		LineAnalysisModel analysisModel = new LineAnalysisModel(parameters);

		LOGGER.debug("Begin compute Line Analysis Spectrum");
		LineAnalysisResult lteLineResult = analysisModel.createSpectrumMultiMols(cassisSpectrum);
		LOGGER.debug("End compute Line Analysis Spectrum");
		return lteLineResult;
	}

	@Override
	public List<LineDescription> getOtherSpecies(List<MoleculeDescription> mols,
			double frequencyMin, double frequencyMax,
			double otherThresEupMin, double otherThresEupMax, double otherThresAij, double otherTreshAijMax,
			double vlsrPlot, double vlsrData,  boolean modeSignal, String comment, TypeFrequency typeFrequency) {
		List<LineDescription> listOfLines = null;

		try {
			OtherSpeciesModel otherSpecies = new OtherSpeciesModel(frequencyMin, frequencyMax,
					otherThresEupMin, otherThresEupMax, otherThresAij, otherTreshAijMax, vlsrPlot, vlsrData,
					modeSignal, comment, typeFrequency);
			listOfLines = otherSpecies.createLines(mols);
		} catch (Exception e) {
			LOGGER.error("Error while retrieving the other species lines", e);
			if (!Software.getUserConfiguration().isTestMode()) {
				JOptionPane.showMessageDialog(null, e.getMessage(), "Alert", JOptionPane.ERROR_MESSAGE);
			}
		}

		return listOfLines;
	}

	@Override
	public List<LineDescription> getOtherSpecies(List<MoleculeDescription> mols,
			double waveMin, double waveMax,XAxisCassis xAxisCassis, OtherSpeciesParameters treshold) {
		List<LineDescription> listOfLines = null;

		try {
			OtherSpeciesModel otherSpecies = new OtherSpeciesModel(waveMin, waveMax,xAxisCassis, treshold);
			listOfLines = otherSpecies.createLines(mols);
		} catch (Exception e) {
			LOGGER.error("Error while retrieving the other species lines", e);
			if (!Software.getUserConfiguration().isTestMode()) {
				JOptionPane.showMessageDialog(null, e.getMessage(), "Alert", JOptionPane.ERROR_MESSAGE);
			}
		}

		return listOfLines;
	}
}
