/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.cassisd;

import java.util.List;
import java.util.Map;

import eu.omp.irap.cassis.cassisd.model.OtherSpeciesParameters;
import eu.omp.irap.cassis.common.CassisException;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.common.ParameterDescription;
import eu.omp.irap.cassis.common.Telescope;
import eu.omp.irap.cassis.common.TypeFrequency;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.database.access.UnknowMoleculeException;
import eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent.ComponentDescription;
import eu.omp.irap.cassis.lineanalysis.LineAnalysisParameters;
import eu.omp.irap.cassis.lineanalysis.LineAnalysisResult;
import eu.omp.irap.cassis.parameters.RotationalDiagramInput;
import eu.omp.irap.cassis.parameters.RotationalDiagramResult;

/**
 * Server is an interface for all the operation which the CASSIS server can do.
 *
 * @author glorian
 * @see ServerImpl
 * @since Cassis 2.0
 */
public interface Server {

	double getTemperatureRDCommand(double t, int tag);

	RotationalDiagramResult computeRotationalDiagram(RotationalDiagramInput input) throws UnknowMoleculeException;

	CommentedSpectrum[] readFile(CassisSpectrum cassisSpectrum, Double frequencyMin,
			Double frequencyMax) throws CassisException;

	CommentedSpectrum computeLteRadexSpectrum(Map<String, ParameterDescription> mapParameter,
			List<ComponentDescription> listComponents, Telescope telescope) throws Exception;

	CommentedSpectrum computeLteRadexSpectrum(Map<String, ParameterDescription> mapParameter,
			double [] freqs, List<ComponentDescription> listComponents, Telescope telescope) throws Exception;

	LineAnalysisResult computeLineAnalysisSpectrum(LineAnalysisParameters parameters,
			CassisSpectrum spectrum) throws Exception;


	List<LineDescription> getOtherSpecies(List<MoleculeDescription> mols, double frequencyMin, double frequencyMax,
			double otherThresEupMin, double otherThresEupMax, double otherThresAij, double otherTreshAijMax,
			double vlsrPlot, double vlsrData, boolean modeSignal, String comment, TypeFrequency typeFrequency);

	List<LineDescription> getOtherSpecies(List<MoleculeDescription> mols, double waveMin,
			double waveMax, XAxisCassis xAxisCassis, OtherSpeciesParameters treshold);

}
