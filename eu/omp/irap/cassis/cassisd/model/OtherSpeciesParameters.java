/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.cassisd.model;

import eu.omp.irap.cassis.common.TypeFrequency;

/**
 * Simple class to store parameters for OtherSpecies.
 */
public class OtherSpeciesParameters {

	private double otherThresEupMin;
	private double otherThresEupMax;
	private double otherThresAijMin;
	private double otherTreshAijMax;
	private double vlsrPlot;
	private boolean modeSignal;
	private String comment;
	private double vlsrData;
	private TypeFrequency typeFrequency;


	/**
	 * Constructor.
	 *
	 * @param otherThresEupMin The value of otherThresEupMin.
	 * @param otherThresEupMax The value of otherThresEupMax.
	 * @param otherThresAijMin The value of otherThresAijMin.
	 * @param otherTreshAijMax The value of otherTreshAijMax.
	 * @param vlsrPlot The value of vlsrPlot.
	 * @param vlsrData The value of vlsrData.
	 * @param modeSignal The value of modeSignal.
	 * @param comment The value of comment.
	 * @param typeFrequency The value of typeFrequency.
	 */
	public OtherSpeciesParameters(double otherThresEupMin, double otherThresEupMax,
			double otherThresAijMin,double otherTreshAijMax,
			double vlsrPlot, double vlsrData,
			boolean modeSignal, String comment, TypeFrequency typeFrequency) {
		this.otherThresEupMin = otherThresEupMin;
		this.otherThresEupMax = otherThresEupMax;
		this.otherThresAijMin = otherThresAijMin;
		if (otherTreshAijMax == 0) {
			otherTreshAijMax = Double.MAX_VALUE;
		}
		this.otherTreshAijMax = otherTreshAijMax;
		this.vlsrPlot = vlsrPlot;
		this.vlsrData = vlsrData;
		this.modeSignal = modeSignal;
		this.comment = comment;
		this.typeFrequency = typeFrequency;

	}

	/**
	 * Return the otherThresEupMin value.
	 *
	 * @return the otherThresEupMin value.
	 */
	public double getOtherThresEupMin() {
		return otherThresEupMin;
	}

	/**
	 * Return the otherThresEupMax value.
	 *
	 * @return the otherThresEupMax value.
	 */
	public double getOtherThresEupMax() {
		return otherThresEupMax;
	}

	/**
	 * Return the otherThresAijMin value.
	 *
	 * @return the otherThresAijMin value.
	 */
	public double getOtherThresAijMin() {
		return otherThresAijMin;
	}

	/**
	 * Return the otherTreshAijMax value.
	 *
	 * @return the otherTreshAijMax value.
	 */
	public double getOtherTreshAijMax() {
		return otherTreshAijMax;
	}

	/**
	 * Return the vlsrPlot value.
	 *
	 * @return the vlsrPlot value.
	 */
	public double getVlsrPlot() {
		return vlsrPlot;
	}

	/**
	 * Return the modeSignal value.
	 *
	 * @return the modeSignal value.
	 */
	public boolean isModeSignal() {
		return modeSignal;
	}

	/**
	 * Return the comment value.
	 *
	 * @return the comment value.
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Return the vlsrData value.
	 *
	 * @return the vlsrData value.
	 */
	public double getVlsrData() {
		return vlsrData;
	}

	/**
	 * Return the TypeFrequency value.
	 *
	 * @return the TypeFrequency value.
	 */
	public TypeFrequency getTypeFrequency() {
		return typeFrequency;
	}

}
