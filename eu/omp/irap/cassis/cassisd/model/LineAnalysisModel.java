/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.cassisd.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.cassisd.model.synthetic.SyntheticModel;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.ChannelDescription;
import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.ParameterDescription;
import eu.omp.irap.cassis.common.Telescope;
import eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent.ComponentDescription;
import eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent.ComponentSpecies;
import eu.omp.irap.cassis.lineanalysis.LineAnalysisParameters;
import eu.omp.irap.cassis.lineanalysis.LineAnalysisResult;

/**
 * @author glorian
 *
 */
public class LineAnalysisModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(LineAnalysisModel.class);

	private Map<String, ParameterDescription> mapParameter;
	private List<ComponentDescription> listComponents;
	private Telescope telescope;
	private LineAnalysisParameters parameters;


	public LineAnalysisModel(LineAnalysisParameters parameters) {
		this.parameters = parameters;
		this.mapParameter = parameters.getMapParameter();
		this.listComponents =  parameters.getListComponents();
		this.telescope = parameters.getTelescope();
	}

	/**
	 * Create a list of list of spectrums.
	 *
	 * @param cassisSpectrum The cassis spectrum.
	 * @return an LineAnalysisResult.
	 * @throws Exception in case of error.
	 */
	public LineAnalysisResult createSpectrumMultiMols(CassisSpectrum cassisSpectrum) throws Exception {
		// Build result to send to client:
		// - spectrumList
		// |- sumSpectrum: contains the sum spectrum
		// |- fileSpectrumList: contains list of file spectrum
		LineModel lineSpectrumModel = new LineModel(parameters);
		List<CommentedSpectrum> fileSpectrumList =
				lineSpectrumModel.createSpectrumListMultiMols(cassisSpectrum);

		if (!fileSpectrumList.isEmpty()) {
			for (int i = 0; i < fileSpectrumList.size(); i++) {
				CommentedSpectrum commentedSpectrum = fileSpectrumList.get(i);
				commentedSpectrum.setyAxis(cassisSpectrum.getYAxis());
				commentedSpectrum.setCassisMetadataList(cassisSpectrum.getCassisMetadataList());
				commentedSpectrum.setOriginalMetadataList(cassisSpectrum.getOriginalMetadataList());
			}
		}

		List<CommentedSpectrum> sumSpectrumList = null;
		if (listComponents != null && !listComponents.isEmpty() &&
				(haveComponentWithMolecule(listComponents))) {
			sumSpectrumList = computeSyntheticSpectrums(fileSpectrumList);
		}
		if (!fileSpectrumList.isEmpty()) {
			LOGGER.debug("Number of spectrums: {}", fileSpectrumList.size());
		}

		final LineAnalysisResult lineAnalysisResult = new LineAnalysisResult(fileSpectrumList, sumSpectrumList);
		lineAnalysisResult.setWarning(lineSpectrumModel.getWarning());
		return lineAnalysisResult;
	}

	private boolean haveComponentWithMolecule(List<ComponentDescription> listComponents) {
		boolean res = false;
		for (ComponentDescription compo : listComponents) {
			if (!compo.isContinuum()) {
				ComponentSpecies compoSpecies = (ComponentSpecies)compo;
				res = res || !compoSpecies.getMoleculeList().isEmpty();
			}
		}
		return res;
	}

	private List<CommentedSpectrum> computeSyntheticSpectrums(List<CommentedSpectrum> fileSpectrumList)
			throws Exception {
		SyntheticModel syntheticModel;
		CommentedSpectrum currentSpectrum;
		List<CommentedSpectrum> sumSpectrumList = new ArrayList<>();
		// Compute the synthetic spectrum
		for (int i = 0; i < fileSpectrumList.size(); i++) {
			CommentedSpectrum fileSpectrum = fileSpectrumList.get(i);
			double vlsrFile = fileSpectrum.getVlsr();
			mapParameter = changeFrequencyParameters(mapParameter, fileSpectrum, vlsrFile, false);
			double lineFrequency = fileSpectrum.getListOfLines().get(0).getObsFrequency();
			mapParameter.put("line_freq", new ParameterDescription("line_freq", "Line Frequency [MHz]", lineFrequency,
					100., 1e10));

			List<ComponentDescription> listComposClone = new ArrayList<>();
			for (int cpt = 0; cpt < listComponents.size(); cpt++) {
				listComposClone.add((ComponentDescription) listComponents.get(cpt).clone());
			}

			// Create spectrum
			double[] freqObs = fileSpectrum.getFrequenciesSignal();

			syntheticModel = new SyntheticModel(mapParameter, freqObs, listComposClone, false, false, telescope);

			currentSpectrum = syntheticModel.createSpectrum();

			sumSpectrumList.add(currentSpectrum);
		}

		// Compress channels
		for (int i = 0; i < sumSpectrumList.size(); i++)
			compress(sumSpectrumList.get(i));

		return sumSpectrumList;
	}

	/**
	 * Change model parameter with file spectrum min/max frequencies.
	 */
	private Map<String, ParameterDescription> changeFrequencyParameters(
			Map<String, ParameterDescription> mapParameter, CommentedSpectrum fileSpectrum,
			double vlsr, boolean absorptionMode) {
		double minFreq = fileSpectrum.getFrequencySignalMin();
		double maxFreq = fileSpectrum.getFrequencySignalMax();

		double resolution = fileSpectrum.getDeltaF()[0];
		double oversampling = 1.;

		LOGGER.debug("Min = {}, Max = {}",minFreq, maxFreq);

		// Set model parameters min&max frequency
		boolean freqMinChange = false;
		boolean	freqMaxChange = false;
		boolean vlsrChange = false;
		boolean resolutionChange = false;

		if (mapParameter.containsKey("freqMin")) {
			mapParameter.put("freqMin", new ParameterDescription(minFreq));
			freqMinChange = true;
		}
		if (mapParameter.containsKey("freqMax")) {
			mapParameter.put("freqMax", new ParameterDescription(maxFreq));
			freqMaxChange = true;
		}

		if (mapParameter.containsKey("vlsr")) {
			mapParameter.put("vlsr", new ParameterDescription(vlsr));
			vlsrChange = true;
		}

		if (mapParameter.containsKey("oversampling"))
			oversampling = mapParameter.get("oversampling").getValue();
		if (mapParameter.containsKey("resolution")) {
			mapParameter.put("resolution", new ParameterDescription(resolution / oversampling));
			resolutionChange = true;
		}

		// Add new parameters
		if (!freqMinChange)
			mapParameter
					.put("freqMin", new ParameterDescription("freqMin", "Min frequency [MHz]", minFreq, 100., 1e10));
		if (!freqMaxChange)
			mapParameter
					.put("freqMax", new ParameterDescription("freqMax", "Max frequency [MHz]", maxFreq, 100., 1e10));
		if (!vlsrChange)
			mapParameter.put("vlsr", new ParameterDescription("vlsr", "Vlsr [MHz]", vlsr, 100., 1e10));

		if (!resolutionChange)
			mapParameter.put("resolution", new ParameterDescription("resolution", "Resolution [MHz]", resolution
					/ oversampling, 100., 1e10));

		mapParameter.put("noCompression", new ParameterDescription("noCompression", "noCompression", 1, 1., 1));
		mapParameter.put("lineAnalysis", new ParameterDescription("lineAnalysis", "lineAnalysis", 1, 1., 1));
		if (absorptionMode)
			mapParameter.put("vlsrEmission", new ParameterDescription("vlsrEmission", "Vlsr [MHz]", vlsr, 100., 1e10));

		return mapParameter;
	}

	/**
	 * Compress elements of Commented spectrum.
	 *
	 * @param spectrum
	 *            Spectrum to compress
	 */
	public void compress(CommentedSpectrum spectrum) {
		ChannelDescription nextChannel;
		double currentIntensity;
		double previousIntensity;

		if (spectrum == null) {
			return ;
		}
		if (spectrum.getFrequenciesSignal() == null)
			spectrum.setListOfChannels(new double[0], new double[0]);

		Iterator<ChannelDescription> channelIterator = spectrum.getListOfChannels().iterator();
		// Don't count first channel
		ChannelDescription lastchannel = spectrum.getListOfChannels().get(spectrum.getListOfChannels().size() - 1);

		if (channelIterator.hasNext()) {
			previousIntensity = channelIterator.next().getIntensity();

			if (channelIterator.hasNext()) {
				currentIntensity = channelIterator.next().getIntensity();

				while (channelIterator.hasNext()) {
					nextChannel = channelIterator.next();
					Double nextIntensity = nextChannel.getIntensity();
					if (nextChannel != lastchannel &&
						currentIntensity == previousIntensity && currentIntensity == nextIntensity) {
						channelIterator.remove();
					}
					previousIntensity = currentIntensity;
					currentIntensity = nextIntensity;
				}
			}
		}
	}
}
