/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.cassisd.model;

import java.util.ArrayList;
import java.util.List;

import eu.omp.irap.cassis.common.ChannelDescription;
import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.UtilArrayList;

/**
 * @author glorian
 *
 */
public class LinesDescription {

	/**
	 * Sort an ArrayList of LineDescription as the obsFrequency.
	 *
	 * @param listOfLines
	 *            an ArrayList of LineDescription
	 * @return listOfLinesSorted an ArrayList of LineDescription sorted by
	 *         obsFrequency
	 */
	public static List<LineDescription> sortLines(List<LineDescription> listOfLines) {
		return sortLines(listOfLines, 1);
	}

	/**
	 * Sort an ArrayList of LineDescription as the sorting parameters.
	 *
	 * @param listOfLines
	 *            an ArrayList of LineDescription
	 * @param sortingParam
	 *            On which parameter to sort<BR>
	 *            1: Sorting by Frequency<BR>
	 *            2: Sorting by Aij<BR>
	 *            3: Sorting by Eup
	 * @return listOfLinesSorted an ArrayList of LineDescription sorted by
	 *         obsFrequency
	 */
	public static List<LineDescription> sortLines(List<LineDescription> listOfLines, int sortingParam) {
		// create a new list of lines for sorting
		List<LineDescription> listOfLinesSorted = new ArrayList<>();
		int nline = listOfLines.size();

		// if there is no lines, return an empty list
		if (nline <= 0)
			return listOfLinesSorted;

		double[] tosort = new double[nline];
		int[] indexes = new int[nline];

		for (int k = 0; k < nline; k++) {
			LineDescription line = listOfLines.get(k);

			// Sorting by Frequency
			if (sortingParam == 1)
				tosort[k] = line.getObsFrequency();

			// Sorting by Eup
			else if (sortingParam == 3)
				tosort[k] = line.getEUpK();

			// Sorting by Aij
			else if (sortingParam == 4)
				tosort[k] = -line.getAij();

			indexes[k] = k;
		}

		UtilArrayList.quicksortindex(tosort, indexes, 0, nline - 1);

		// add lines sorted to the new list of line
		for (int k = 0; k < nline; k++) {
			LineDescription line = listOfLines.get(indexes[k]);
			listOfLinesSorted.add(line);
		}

		// return the result
		return listOfLinesSorted;
	}

	/**
	 * Adds the channelFlux element value to an ArrayList of LineDescription
	 * from a spectrum defined by an ArrayList of ChannelDescription. This
	 * method is usefull to add ticks for lines upon the observed spectrum.<BR>
	 * Note that input parameters must be sorted by frequencies.
	 *
	 * @param listOfLines
	 *            an ArrayList of LineDescription to be updates
	 * @param listOfChannels
	 *            an ArrayList of ChannelDescription to evaluate the value of
	 *            channelFlux
	 * @return listOfLine an ArrayList of LineDescription updated with
	 *         channelFlux
	 */
	// TODO A revoir
	public static List<LineDescription> linesUpdate(List<LineDescription> listOfLines,
			final List<ChannelDescription> listOfChannels) {
		int nline = listOfLines.size();
		int nchannel = listOfChannels.size();
		double obsFrequency;
		double frequency;
		double dFrequency;
		int kkdeb = 1;
		int kkfin = nchannel - 1;
		double freq1;
		double freq2;

		// check each line one by one
		for (int k = 0; k < nline; k++) {
			// get a line and put the channelFlux to 0
			LineDescription line = listOfLines.get(k);
			line.setChannelFlux(0.);
			listOfLines.set(k, line);
			line = listOfLines.get(k);
			obsFrequency = line.getObsFrequency();

			// read each channel of the listOfChannel given
			for (int kk = kkdeb; kk <= kkfin; kk++) {
				final ChannelDescription channel = listOfChannels.get(kk);
				frequency = channel.getFrequencySignal();
				dFrequency = channel.getDFrequencySignal();
				freq1 = frequency - dFrequency / 2.;
				freq2 = frequency + dFrequency / 2.;
				if (obsFrequency >= freq1 && obsFrequency <= freq2) {
					// update the ChannelFlux
					line.setChannelFlux(channel.getIntensity() + channel.getDIntensity());
					listOfLines.set(k, line);
					// the program will began here next time
					kkdeb = kk;
					break;
				}
			}
		}
		return listOfLines;
	}

}
