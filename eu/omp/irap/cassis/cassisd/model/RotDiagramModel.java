/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.cassisd.model;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import eu.omp.irap.cassis.common.Formula;
import eu.omp.irap.cassis.common.ProgressDialogConstants;
import eu.omp.irap.cassis.database.access.AccessDataBase;
import eu.omp.irap.cassis.database.access.DataBaseConnection;
import eu.omp.irap.cassis.database.access.LineDescriptionDB;
import eu.omp.irap.cassis.database.access.MoleculeDescriptionDB;
import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB;
import eu.omp.irap.cassis.database.access.UnknowMoleculeException;
import eu.omp.irap.cassis.gui.model.parameter.rotationaltuning.TYPE_TEMPERATURE;
import eu.omp.irap.cassis.gui.model.rotationaldiagram.RotationalComp;
import eu.omp.irap.cassis.gui.model.rotationaldiagram.RotationalMoleculeConf;
import eu.omp.irap.cassis.parameters.PointInformation;
import eu.omp.irap.cassis.parameters.RotationalComponent;
import eu.omp.irap.cassis.parameters.RotationalDiagramComponentResult;
import eu.omp.irap.cassis.parameters.RotationalDiagramInput;
import eu.omp.irap.cassis.parameters.RotationalDiagramMoleculeResult;
import eu.omp.irap.cassis.parameters.RotationalDiagramResult;
import eu.omp.irap.cassis.parameters.RotationalMolecule;
import eu.omp.irap.cassis.parameters.RotationalSelectionData;
import eu.omp.irap.cassis.parameters.TelescopeDescriptionRD;



public class RotDiagramModel {

	private boolean cancel;
	private boolean pointCancel;
	private MultipletInfo blendedLines = new MultipletInfo();

	public RotDiagramModel() {
		this.cancel = false;
	}

	public RotationalDiagramResult compute(RotationalDiagramInput input) throws UnknowMoleculeException {
		RotationalDiagramResult diagramResult = new RotationalDiagramResult(input.getPathFileSource());
		DataBaseConnection db = AccessDataBase.getDataBaseConnection();
		MoleculeDescriptionDB molecule;
		RotationalMoleculeConf molConf;

		boolean doTaToTmbCorrection = input.getTypeTemperature() == TYPE_TEMPERATURE.TA && input.isTaToTmbSelected();
		TYPE_TEMPERATURE tt = (input.getTypeTemperature() == TYPE_TEMPERATURE.TMB || input.isTaToTmbSelected()) ?
				TYPE_TEMPERATURE.TMB : TYPE_TEMPERATURE.TA;
		diagramResult.setTypeTemperature(tt);
		MultipletInfo multipletInfo = new MultipletInfo();
		for (RotationalMolecule rotationalMolecule : input.getListRotMol()) {
			molConf = input.getMolConf(rotationalMolecule.getTag());
			if (!molConf.isEnabled()) {
				continue;
			}
			RotationalDiagramMoleculeResult rdmr = new RotationalDiagramMoleculeResult(rotationalMolecule.getTag(), rotationalMolecule.getNameMol());
			molecule = db.getMoleculeDescriptionDB(new SimpleMoleculeDescriptionDB(rotationalMolecule.getTag(), null));
			for (RotationalComponent rotationalComponent : rotationalMolecule.getComponentsList()) {
				RotationalComp rcConf = input.getMolConf(rotationalMolecule.getTag()).getComponent(
						rotationalComponent.getNumCompo());
				if (!rcConf.isEnabled()) {
					continue;
				}
				boolean doBeamCorrection = rcConf.isBeamDilutionPossible() && rcConf.isBeamDilution();
				doParametersCorrection(rotationalComponent, molConf, doTaToTmbCorrection);

				List<PointInformation> result = new ArrayList<>();
				List<PointInformation> resultEupDiff = new ArrayList<>();
				for (PointInformation pointInformation : rotationalComponent.getListPoints()) {
					if (ProgressDialogConstants.workerInterrupted) {
						this.cancel = true;
						return null;
					}
					double nu = pointInformation.getNu();
					double deltaNu = Formula.calcDeltaNu(pointInformation.getFwhm(), nu);
					double freqMin = nu - deltaNu;
					double freqMax = nu + deltaNu;
					List<LineDescriptionDB> lines = db.getLineDescriptionDB(molecule, freqMin, freqMax);
					int nbRows = lines.size();
					if (nbRows == 0 && !pointCancel) {
						diagramResult.addFrequencyNotFound(rotationalMolecule.getTag(), nu);
					} else {
						calcPoint(molecule.getName(), lines, result, resultEupDiff,
								multipletInfo,	pointInformation, doBeamCorrection);
					}
				}
				boolean beamCorrectionDone = doBeamCorrection || !rcConf.isBeamDilutionPossible();
				rdmr.addComponentResult(new RotationalDiagramComponentResult(
						rotationalComponent.getNumCompo(), rcConf.getDataSelected(),
						result, resultEupDiff, beamCorrectionDone));
			}
			if (!rdmr.getComponentResultsList().isEmpty()) {
				diagramResult.addMoleculeResult(rdmr);
			}
		}
		diagramResult.setMultipletMessage(multipletInfo.getMultipletMessage());
		diagramResult.setBlendedMessage(blendedLines.getMultipletMessage());

		return diagramResult;
	}

	private void doParametersCorrection(RotationalComponent rc, RotationalMoleculeConf conf, boolean taToTmbCorrection) {
		boolean taTmb = taToTmbCorrection;
		RotationalComp comp = conf.getComponent(rc.getNumCompo());
		double sizeSource = comp.getSourceSize();

		RotationalSelectionData typeData = comp.getDataSelected();
		for (PointInformation point : rc.getListPoints()) {
			point.setTaTmbSelect(taTmb);
			point.setTypeData(typeData);
			if (!Double.isNaN(sizeSource)) {
				point.setSizeSource(sizeSource);
			}
		}
	}

	private void calcPoint(String molName, List<LineDescriptionDB> lines,
			List<PointInformation> result, List<PointInformation> resultEupDiff,
			MultipletInfo multipletInfo, PointInformation pointRot,
			boolean beamCorrection) {
		pointCancel = false;

		double nu = pointRot.getNu();
		double w = pointRot.getW();
		double deltaW = pointRot.getDeltaW();
		int nbRows = lines.size();
		boolean taTmbSelect = pointRot.getTaTmbSelect();
		TelescopeDescriptionRD telDesc = new TelescopeDescriptionRD(
				pointRot.getTelescope());
		double diameterTelescope = telDesc.getTelescopeDiameter();

		if (taTmbSelect && nu < telDesc.getMaxFreq() && nu > telDesc.getMinFreq()) {
			double calcTaTmbValue = Formula.calcTaTmb(nu,
					telDesc.getFrequencyList(), telDesc.getEffValueList());
			w = w / calcTaTmbValue;
			deltaW = deltaW / calcTaTmbValue;
		}

		if (nbRows == 1 && !pointCancel && !cancel) {
			LineDescriptionDB line = lines.get(0);
			computeLineRot(molName, pointRot, w, deltaW, diameterTelescope, line, beamCorrection, false);
			pointRot.setBlendedLine(false);
			result.add(pointRot);
		} else if (nbRows > 1 && !pointCancel && !cancel) {
			addResultAndResultEupDiff(molName, lines, result, resultEupDiff, multipletInfo,
					pointRot, nu, w, deltaW, diameterTelescope, beamCorrection);
		}
	}

	/**
	 * Search the closest lines to a given frequency.
	 *
	 * @param lines The lines to search on.
	 * @param freq The frequency.
	 * @return The closest lines to the given frequency.
	 */
	private List<LineDescriptionDB> getClosestLines(List<LineDescriptionDB> lines, double freq) {
		List<LineDescriptionDB> goodLines = new ArrayList<>(1);
		goodLines.add(lines.get(0));
		double minDelta = Double.MAX_VALUE;
		for (LineDescriptionDB l : lines) {
			double diff = Math.abs(freq - l.getFrequency());
			if (diff < minDelta) {
				goodLines.clear();
				minDelta = Math.abs(freq - l.getFrequency());
				goodLines.add(l);
			} else if (diff == minDelta) {
				goodLines.add(l);
			}
		}
		return goodLines;
	}

	private void addResultAndResultEupDiff(String molName, List<LineDescriptionDB> lines,
			List<PointInformation> result, List<PointInformation> resultEupDiff,
			MultipletInfo multipletInfo, PointInformation pointRot, double nu, double w,
			double deltaW, double diameterTelescope, boolean beamCorrection) {
		List<LineDescriptionDB> l = getClosestLines(lines, pointRot.getNu());
		LineDescriptionDB goodLine = l.get(0);

		double coeff = 0;
		List<LineDescriptionDB> goodEup = new ArrayList<>();
		List<LineDescriptionDB> diffEup = new ArrayList<>();

		for (LineDescriptionDB line : lines) {
			if (Math.abs(line.getEup() - goodLine.getEup()) <= 1) {
				goodEup.add(line);
				coeff += line.getAint() * line.getIgu();
			} else {
				diffEup.add(line);
			}
		}

		computeLineRot(molName, pointRot, w, deltaW, diameterTelescope, goodLine, beamCorrection, false);
		pointRot.setLnNuOnGu(Formula.calcLnNuOnGu(nu * 1e6, coeff, pointRot.getWCalc() * 1e5));
		pointRot.setCoeff(coeff);
		pointRot.setManyPoints(diffEup.size());
		pointRot.setBlendedLine(!diffEup.isEmpty());
		pointRot.setMultiplet(goodEup.size()>1);

		result.add(pointRot);
		addMultipletElements(goodEup, molName, multipletInfo);
		if (pointRot.isBlendedLine()){
			blendedLines.addElement(molName +MoleculeDescriptionDB.constitueLine(
					goodLine.getQuanticNumbers()), goodLine.getEup(), goodLine.getFrequency(), goodLine.getAint(), goodLine.getIgu());
		}
		for (LineDescriptionDB line : diffEup) {
			String name = molName + MoleculeDescriptionDB.constitueLine(
					line.getQuanticNumbers());
			blendedLines.addElement(name, line.getEup(), line.getFrequency(), line.getAint(), line.getIgu());
			PointInformation aPoint = new PointInformation();
			aPoint.setBlendedLine(true);
			aPoint.setTypeData(pointRot.getRotationalSelectionData());
			aPoint.setVersion(pointRot.getVersion());
			aPoint.setNu(line.getFrequency());
			aPoint.setFwhm(pointRot.getFwhm());
			aPoint.setSizeSource(pointRot.getSizeSource());
			aPoint.setDeltaW(deltaW);
			computeLineRot(molName, aPoint, w, deltaW, diameterTelescope, line, beamCorrection, true);
			aPoint.setW(w);

			aPoint.setManyPoints(0);
			resultEupDiff.add(aPoint);
		}
	}

	/**
	 * Add all multiplets to a multipletInfo.
	 *
	 * @param lines The multiplets lines which constitute the point.
	 * @param molName The name of the molecule.
	 * @param multipletInfo The multiplet info.
	 */
	private void addMultipletElements(List<LineDescriptionDB> lines, String molName,
			MultipletInfo multipletInfo) {
		if (lines.size()>1){
			for (LineDescriptionDB line : lines) {
				String name = molName + MoleculeDescriptionDB.constitueLine(
						line.getQuanticNumbers());
				multipletInfo.addElement(name, line.getEup(), line.getFrequency(),
						line.getAint(), line.getIgu());
			}
		}
	}

	private void computeLineRot(String molName, PointInformation currentMolecule,
			double w, double deltaW, double diameterTelescope,
			LineDescriptionDB line, boolean beamCorrection, boolean updateName) {
		String qn = line.getQuanticNumbers();
		double aint = line.getAint();
		double gu = line.getIgu();
		double elow = line.getElow();
		double nu = currentMolecule.getNu();

		double eup = Formula.calcEUpk(Formula.calcEUpJ(Formula.calcELowJ(elow, 0), nu));
		double sizeSource = currentMolecule.getSizeSource();
		double wTemp = w * 1e5;
		if (sizeSource > 0.0 && beamCorrection) {
			double facteur = Formula.beamCorrection(sizeSource, currentMolecule.getNu(), diameterTelescope);
			wTemp = wTemp * facteur;
			deltaW = deltaW * facteur;
		}

		// calculation of ln(Nu/gu)
		double wLnNuOnGu = Formula.calcLnNuOnGu(nu*1e6, aint, gu, wTemp);

		// initialisation of the PointInformation in order t
		// transmit the parameters
		currentMolecule.setWCalc(wTemp/1e5);
		currentMolecule.setDeltaWCalc(deltaW);
		currentMolecule.setEup(eup);
		currentMolecule.setLnNuOnGu(wLnNuOnGu);

		if (currentMolecule.getVersion() == PointInformation.VERSION.NEW
				|| currentMolecule.getVersion() == PointInformation.VERSION.NEW_2014) {
			if (updateName) {
				currentMolecule.setMoleNameQuantique(molName + MoleculeDescriptionDB.constitueLine(qn));
			}
			currentMolecule.setAij(aint);
			currentMolecule.setGup(gu);
		}
	}


	/**
	 * This class manages the multiplets messages.
	 *
	 * @author M. Boiziot
	 */
	private class MultipletInfo {

		private final List<MultipletElement> multiplets;


		/**
		 * Constructor.
		 */
		public MultipletInfo() {
			multiplets = new ArrayList<>();
		}

		/**
		 * Add a point if it does not already exist (without using lnOnNuGu for
		 *  checking if is already exist).
		 *
		 * @param name The name of the point.
		 * @param eup The eup of the point.
		 * @param freq The frequency of the point.
		 * @param aint The aint of the point.
		 * @param igu The igu of the point.
		 */
		public void addElement(String name, double eup, double freq, double aint, int igu) {
			if (!contains(name, eup, freq, aint, igu))	{
				multiplets.add(new MultipletElement(name, eup, freq, aint, igu));
			}
		}

		/**
		 * Check and return if the point with the given parameters exist.
		 *
		 * @param name The name of the point.
		 * @param eup The eup of the point.
		 * @param freq The frequency of the point.
		 * @param aint The aint of the point.
		 * @param igu The igu of the point.
		 * @return if the point exist.
		 */
		private boolean contains(String name, double eup, double freq, double aint, int igu) {
			for (MultipletElement m : multiplets) {
				if (m.getEup() == eup && m.getFreq() == freq && m.getAint() == aint &&
						m.getIgu() == igu && m.getName().equals(name)) {
					return true;
				}
			}
			return false;
		}

		/**
		 * Create and return the multiplet message.
		 *
		 * @return The multiplet message.
		 */
		public String getMultipletMessage() {
			if (multiplets.isEmpty()) {
				return null;
			}

			sortByEupAndFreq();
			StringBuilder sb = new StringBuilder();
			DecimalFormat df = new DecimalFormat("0.00E0");
			for (MultipletElement m : multiplets) {
				appendMultipletMessage(m, sb, df);
			}
			return sb.toString();
		}

		/**
		 * Sort the list of multiplet by eup and frequency.
		 */
		private void sortByEupAndFreq() {
			Collections.sort(multiplets);
		}

		/**
		 * Append a message for a {@link MultipletElement} to a {@link StringBuilder}.
		 *
		 * @param multiplet The multiplet for which we want the message.
		 * @param sb The {@link StringBuilder}.
		 * @param df The decimal format to use
		 */
		private void appendMultipletMessage(MultipletElement multiplet,
				StringBuilder sb, DecimalFormat df) {
			sb.append(" - ");
			sb.append(multiplet.getName()).append(", ");
			sb.append(multiplet.getFreq()).append(", ");
			sb.append(df.format(multiplet.getEup())).append(", ");
			sb.append(df.format(multiplet.getAint())).append(", ");
			sb.append(multiplet.getIgu()).append('\n');
		}
	}


	/**
	 * This class describe a multiplet.
	 *
	 * @author M. Boiziot
	 */
	private static class MultipletElement implements Comparable<MultipletElement>{

		private final String name;
		private final double eup;
		private final double freq;
		private final double aint;
		private final int igu;


		/**
		 * Constructor.
		 *
		 * @param name The name of the point.
		 * @param eup The eup of the point.
		 * @param freq The frequency of the point.
		 * @param aint The aint of the point.
		 * @param igu The igu of the point.
		 * @param lnNuOnGu The lnOnNuGu of the point.
		 */
		private MultipletElement(String name, double eup, double freq, double aint, int igu) {
			this.name = name;
			this.eup = eup;
			this.freq = freq;
			this.aint = aint;
			this.igu = igu;
		}

		/**
		 * Return the name.
		 *
		 * @return the name.
		 */
		public String getName() {
			return name;
		}

		/**
		 * Return the eup.
		 *
		 * @return the eup.
		 */
		public double getEup() {
			return eup;
		}

		/**
		 * Return the frequency.
		 *
		 * @return the frequency.
		 */
		public double getFreq() {
			return freq;
		}

		/**
		 * Return the aint.
		 *
		 * @return the aint.
		 */
		public double getAint() {
			return aint;
		}

		/**
		 * Return the igu.
		 *
		 * @return the igu.
		 */
		public int getIgu() {
			return igu;
		}

		/**
		 * Compare element by eup then frequency.
		 */
		@Override
		public int compareTo(MultipletElement o) {
			int returnValue = 0;
			if (eup < o.getEup()) {
				returnValue = -1;
			} else if (eup > o.getEup()) {
				returnValue = 1;
			} else {
				if (freq < o.freq) {
					returnValue = -1;
				} else if (freq > o.freq) {
					returnValue = 1;
				}
			}
			return returnValue;
		}
	}

}
