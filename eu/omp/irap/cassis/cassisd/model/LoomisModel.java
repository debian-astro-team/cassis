/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.cassisd.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.ChannelDescription;
import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.Formula;
import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.common.ProgressDialogConstants;
import eu.omp.irap.cassis.common.TypeFrequency;
import eu.omp.irap.cassis.database.access.AccessDataBase;
import eu.omp.irap.cassis.database.access.DataBaseConnection;
import eu.omp.irap.cassis.database.access.LineDescriptionDB;
import eu.omp.irap.cassis.database.access.MoleculeDescriptionDB;
import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB;
import eu.omp.irap.cassis.database.access.UnknowMoleculeException;
import eu.omp.irap.cassis.gui.model.loomisanalysis.LoomisModelParameters;
import eu.omp.irap.cassis.gui.model.loomisanalysis.LoomisWoodResult;
import eu.omp.irap.cassis.gui.model.parameter.UtilDatabase;
import eu.omp.irap.cassis.gui.model.parameter.threshold.ThresholdModel;
import eu.omp.irap.cassis.parameters.LineIdentificationUtils;

public class LoomisModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(LoomisModel.class);

	private final LoomisModelParameters parameters;


	public LoomisModel(LoomisModelParameters parameters) {
		this.parameters = parameters;
	}

	public List<LineDescription> createLines(List<LineDescription> lines, double tex, double density) throws UnknowMoleculeException {
		List<LineDescription> listOfLinesModel = new ArrayList<>(lines.size());

		for (LineDescription lineDescriptionFile : lines) {
			LineDescription lineDescription = (LineDescription) lineDescriptionFile.clone();

			double coeffMulti = density / 1E13;
			if ("SPIRE".equals(parameters.getTelescope())) {
				coeffMulti = coeffMulti / 3000;
			}
			else if ("PACS".equals(parameters.getTelescope())) {
				coeffMulti = coeffMulti / 3000;
			}

			double aintTex = computeIntensity(tex, coeffMulti, lineDescription);

			lineDescription.setVlsr(parameters.getVlsr());
			lineDescription.setVlsrData(0);
			lineDescription.setMaxIntensity(aintTex);

			listOfLinesModel.add(lineDescription);
		}

		return listOfLinesModel;
	}

	public CommentedSpectrum createSpectrum(List<LineDescription> lines,
			double tex, double density) throws Exception {
		CommentedSpectrum commentedSpectrum = null;

		List<ChannelDescription> listOfChannels = new ArrayList<>();
		List<LineDescription> listOfLinesModel = createLines(lines, tex, density);

		for (LineDescription lineDescription : listOfLinesModel) {
			listOfChannels.add(new ChannelDescription(lineDescription.getObsFrequency(), 0., 0.));
			listOfChannels.add(new ChannelDescription(lineDescription.getObsFrequency(), 0., lineDescription
					.getMaxIntensity()));
			listOfChannels.add(new ChannelDescription(lineDescription.getObsFrequency(), 0., 0.));
		}

		commentedSpectrum = new CommentedSpectrum(listOfLinesModel, listOfChannels, "Loomis Model");
		if (!listOfChannels.isEmpty()) {
			commentedSpectrum.setFreqRef(parameters.getFreqRef());
			commentedSpectrum.setVlsr(parameters.getVlsr());
			commentedSpectrum.setTypeFreq(parameters.getCassisSpectrum().getTypeFrequency());
		}

		return commentedSpectrum;
	}

	private double computeIntensity(double tex, double coeffMulti, LineDescription lineDescription) throws UnknowMoleculeException {
		double aEinstein = lineDescription.getAij();
		double freqMHz = lineDescription.getObsFrequency();
		double igu = lineDescription.getGu();
		double eUpJ = lineDescription.getEUpK() * Formula.K;

		double eLowJ = eUpJ - freqMHz * 1E6 * Formula.H;
		double val300K = 300;
		double part300K = AccessDataBase.getDataBaseConnection().buildQt(300, lineDescription.getMolTag());
		double aint = Formula.calcAint(aEinstein, freqMHz, igu, eLowJ, eUpJ, val300K, part300K);
		double partTex = AccessDataBase.getDataBaseConnection().buildQt(tex, lineDescription.getMolTag());
		return coeffMulti
				* Formula.calcLoomisIntensity(aint, val300K, part300K, tex, partTex, eLowJ / Formula.K,
						lineDescription.getEUpK());
	}

	public LoomisWoodResult compute() throws UnknowMoleculeException {
		CommentedSpectrum[] splitSpectrum = splitSpectrum();

		CommentedSpectrum[][] lineSpectrums = new CommentedSpectrum[parameters.getMoles().size()][splitSpectrum.length];
		Boolean haveLines1 = false;
		for (int cpt = 0; cpt < parameters.getMoles().size(); cpt++) {
			if (ProgressDialogConstants.workerInterrupted) {
				break;
			}

			MoleculeDescription mol = parameters.getMoles().get(cpt);
			List<ArrayList<LineDescription>> linesTab = getLines(mol);
			for (int cpt2 = 0; cpt2 < lineSpectrums[cpt].length; cpt2++) {
				if (ProgressDialogConstants.workerInterrupted) {
					break;
				}

				try {
					lineSpectrums[cpt][cpt2] = createSpectrum(linesTab.get(cpt2), mol.getTemperature(), mol.getDensity());
					if (!lineSpectrums[cpt][cpt2].getListOfLines().isEmpty()) {
						haveLines1 = true;
					}
				} catch (Exception e) {
					LOGGER.error("Error during the Loomis computation", e);
				}
			}

		}
		return new LoomisWoodResult(splitSpectrum, lineSpectrums,
				parameters.getContinuumParameters(), haveLines1,
				parameters.getTelescope());
	}

	public CommentedSpectrum[] splitSpectrum() {
		CommentedSpectrum[] result = null;

		Double min = parameters.getMin();
		Double max = parameters.getMax();
		Double valBand = parameters.getValBand();
		if (valBand == null || valBand == 0. || Double.isNaN(valBand)) {
			result = new CommentedSpectrum[1];
			parameters.getCassisSpectrum().computeCommentedSpectrum(min, max, result);
		}
		else {
			int nbSpectres = (int) Math.ceil(round(Math.abs(max - min) / valBand, 12));
			result = new CommentedSpectrum[nbSpectres];
			String dataName = null;
			List<Integer> listToRename = new ArrayList<>();

			for (int cpt = 0; cpt < nbSpectres; cpt++) {
				CommentedSpectrum[] resultTemp = new CommentedSpectrum[1];
				if (parameters.getCassisSpectrum().computeCommentedSpectrum(
						min + valBand * cpt, min + valBand * (cpt + 1),
						resultTemp)) {
					result[cpt] = resultTemp[0];
					if (dataName == null)
						dataName = resultTemp[0].getTitle();
				}
				else {
					List<ChannelDescription> channels = new ArrayList<>(2);
					channels.add(new ChannelDescription(min + valBand * cpt, 0., 0.));
					channels.add(new ChannelDescription(min + valBand * (cpt + 1), 0., 0.));
					CommentedSpectrum spectrum = null;
					if (dataName != null) {
						spectrum = new CommentedSpectrum(null, channels, dataName);
					} else {
						spectrum = new CommentedSpectrum(null, channels, "Empty spectrum");
						listToRename.add(cpt);
					}
					spectrum.setxAxisOrigin(parameters.getCassisSpectrum().getxAxisOrigin());
					spectrum.setyAxis(parameters.getCassisSpectrum().getYAxis());
					result[cpt] = spectrum;
				}
			}

			if (dataName != null && !"".equals(dataName) && !listToRename.isEmpty()) {
				for (int num : listToRename) {
					result[num].setTitle(dataName);
				}
			}
		}
		return result;
	}

	public List<ArrayList<LineDescription>> getLines(MoleculeDescription mol) throws UnknowMoleculeException {
		Double min = parameters.getMin();
		Double max = parameters.getMax();

		List<ArrayList<LineDescription>> res = new ArrayList<>();
		Double vlsr = parameters.getVlsr();
		Double valBand = parameters.getValBand();
		ThresholdModel tresholdmodel = parameters.getThresholdModel();
		if (valBand == null || valBand == 0. || Double.isNaN(valBand)) {
			Double freqRef = min + (max - min) / 2;
			Double freqRefDecal = Formula.calcFreqWithVlsr(freqRef, -vlsr, freqRef);
			res.add(getLines(mol, Formula.calcFreqWithVlsr(min, -vlsr, freqRefDecal),
					Formula.calcFreqWithVlsr(max, -vlsr, freqRefDecal), tresholdmodel, tresholdmodel.getQntm().getQuantumNumber(), vlsr));
		}
		else {
			int nbSpectres = (int) (Math.abs(max - min) / valBand) + 1;

			for (int cpt = 0; cpt < nbSpectres; cpt++) {
				Double freqRef = min + valBand * cpt + valBand / 2;
				Double freqRefDecal = Formula.calcFreqWithVlsr(freqRef, -vlsr, freqRef);

				res.add(getLines(mol, Formula.calcFreqWithVlsr(min + valBand * cpt, -vlsr, freqRefDecal),
						Formula.calcFreqWithVlsr(min + valBand * (cpt + 1), -vlsr, freqRefDecal),tresholdmodel,
						tresholdmodel.getQntm().getQuantumNumber(), vlsr));
			}
		}
		return res;
	}

	public ArrayList<LineDescription> getLines(MoleculeDescription mol, Double freqMin,
			Double freqMax, ThresholdModel tresholdmodel, ArrayList<String> quantumFilter, Double vlsrSpec) throws UnknowMoleculeException {
		ArrayList<LineDescription> fileListOfLines = new ArrayList<>();
		DataBaseConnection db =AccessDataBase.getDataBaseConnection();
		MoleculeDescriptionDB molecule = db.getMoleculeDescriptionDB(
				new SimpleMoleculeDescriptionDB(mol.getTag(), mol.getName()));

		List<LineDescriptionDB> lineDescriptionDBList = db.getLineDescriptionDB(
				molecule, freqMin, freqMax, tresholdmodel.getThresEupMin(),
				tresholdmodel.getThresEupMax(), tresholdmodel.getThresAijMin(),
				tresholdmodel.getThresAijMax());

		for (LineDescriptionDB lineDescriptionDB2 : lineDescriptionDBList) {
			double fMHzTrans = lineDescriptionDB2.getFrequency();
			double eUpK = Formula.calcEUpk(lineDescriptionDB2.getElow(), 0, fMHzTrans);// eUpK : high energy state [K]

			if (eUpK > tresholdmodel.getThresEupMin() && eUpK < tresholdmodel.getThresEupMax()) {
				double vlsr = parameters.getCassisSpectrum().getVlsr();
				LineDescription line = addLine(vlsr, vlsrSpec, molecule.getTag(), molecule.getName(),
						lineDescriptionDB2.getQuanticNumbers(), fMHzTrans, lineDescriptionDB2.getError(),
						lineDescriptionDB2.getAint(), lineDescriptionDB2.getIgu(), eUpK,
						mol.getTemperature(), mol.getDensity(),
						parameters.getCassisSpectrum().getTypeFrequency());
				fileListOfLines.add(line);
			}
		}

		UtilDatabase.removeLineOverQuantumFilter(fileListOfLines, quantumFilter);

		return fileListOfLines;
	}

	private static LineDescription addLine(double vlsrData, double vlsrspec,
			int molTag, String molName, String qn, double fMHzTrans, double errTrans,
			double aTrans, int iguTrans, double eUpK, Double tex, Double density,
			TypeFrequency typeFrequency) {
		double aijOnNu2 = Formula.calcAijOnNu2(aTrans, iguTrans, fMHzTrans);
		String id = LineIdentificationUtils.createLoomisIdentification(molName,
				qn, molTag, fMHzTrans, eUpK, aTrans, tex, density, iguTrans);
		LineDescription lteLine = new LineDescription(fMHzTrans, errTrans, id, aijOnNu2,
				eUpK, iguTrans, aTrans, vlsrData, molName, molTag, qn);
		if (TypeFrequency.REST.equals(typeFrequency))
			lteLine.setFreqCompute(Formula.calcFreqWithVlsr(fMHzTrans, vlsrspec-vlsrData, fMHzTrans));
		else
			lteLine.setFreqCompute(Formula.calcFreqWithVlsr(fMHzTrans, vlsrspec, fMHzTrans));
		lteLine.setVlsr(vlsrspec);
		lteLine.setVlsrData(vlsrData);
		return lteLine;
	}

	/**
	 * Round value to the places digit.
	 *
	 * @param value The value.
	 * @param places The digit to round.
	 * @return The rounded value.
	 */
	public static double round(double value, int places) {
		return BigDecimal.valueOf(value).setScale(places, RoundingMode.HALF_UP).doubleValue();
	}

}
