/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.cassisd.model.synthetic;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.RadexException;
import eu.omp.irap.cassis.gui.util.ChecksumUtil;
import radex.RadexInputModel;
import radex.RadexOutputModel;

public class RadexExecution implements RadexExecutionInterface {

	private static final String G95_COMPILER = "g95";
	private static final String G77_COMPILER = "g77";
	private static final String GFORTRAN_COMPILER = "gfortran";
	private static final String IFORT_COMPILER = "ifort";
	private static String md5Radex = null;
	private static final Logger LOGGER = LoggerFactory.getLogger(RadexExecution.class);
	private static boolean outputOn = false;

	public List<RadexOutputModel> executeRadex(RadexInputModel radexInputModel) throws RadexException {
		return executeFortranRadex(radexInputModel);
	}
	private static  List<RadexOutputModel> executeFortranRadex(RadexInputModel radexInputModel) throws RadexException {
		RadexConfiguration radexConf = RadexConfiguration.getInstance();
		if (!radexConf.haveFortran()) {
			throw new RadexException("RADEX is not installed on you computer.\n"
					+ "Please install it as indicated in the CASSIS documentation.");
		}
		final String radexPath = new File(radexConf.getPath()).getPath();
		if (radexPath.contains(" ")) {
			throw new RadexException("RADEX can not work with a space in the path.");
		}
		String log = RadexConfiguration.getInstance().getLog();
		String inputFile = RadexConfiguration.getInstance().getInput();
		String outPutFile = RadexConfiguration.getInstance().getOutput();
		boolean windowsOS = System.getProperty("os.name").startsWith("Windows");

		String radexIncPath = radexPath + File.separatorChar + "src" + File.separatorChar + radexConf.getConf();
		String radexInputPath = radexPath + File.separatorChar + inputFile;

		String radexMakefile = radexPath + File.separatorChar + "src" + File.separatorChar + "Makefile";

		if (!prepareBinary(radexInputModel, radexPath, log, windowsOS,
				radexIncPath, radexMakefile)) {
			return null;
		}


		writeRadexInputFile(radexInputModel, radexInputPath, outPutFile);

		runRadex(radexPath, inputFile, windowsOS);

		// read output and transform  in RadexResult
		return read(radexPath, outPutFile);
	}

	/**
	 * Prepare the radex binary.
	 *
	 * @param radexInputModel The radex input.
	 * @param radexPath The radex path.
	 * @param log The logfile path.
	 * @param windowsOS true if Windows, false otherwise.
	 * @param radexIncPath The path of radex.inc file.
	 * @param radexMakefile The path of radex makefile.
	 * @return true if the preparation was OK, false otherwise.
	 * @throws RadexException In case of error.
	 */
	private static boolean prepareBinary(RadexInputModel radexInputModel, final String radexPath,
			String log, boolean windowsOS, String radexIncPath, String radexMakefile)
					throws RadexException {
		try {
			writeConfigFileRadex(radexIncPath, radexInputModel.getRadat(), log, radexInputModel.getGeometry(), windowsOS);
			if (md5Radex == null || !md5Radex.equals(ChecksumUtil.getMd5(radexIncPath))) {
				cleanRadexFortran(radexPath, true, windowsOS);
				configureMakefile(radexMakefile, windowsOS);

				if (compil(radexPath, radexMakefile) == 0) {
					md5Radex = ChecksumUtil.getMd5(radexIncPath);
				}
			} else {
				cleanRadexFortran(radexPath, false, windowsOS);
			}
		} catch (IOException e) {
			LOGGER.error("Error while preparing radex binary", e);
			return false;
		}
		return true;
	}


	/**
	 * Run RADEX.
	 *
	 * @param radexPath Path of radex.
	 * @param inputFile The input file path.
	 * @param windowsOS true if Windows, false otherwise.
	 * @throws RadexException In case of error.
	 */
	private static void runRadex(final String radexPath, String inputFile, boolean windowsOS)
			throws RadexException {
		Process process = null;
		try {
			try {
				process = execute(radexPath, inputFile, windowsOS);
			} catch (IOException e) {
				throw new InterruptedException(e.getMessage());
			}
			process.waitFor();
		} catch (InterruptedException e) {
			LOGGER.error("Error while running radex", e);
			if (process != null) {
				process.destroy();
			}
			throw new RadexException("An Exception occured.");
		}
	}


	private static Process execute(String radexPath, String inputFile, boolean isWindowsOS) throws IOException,
			InterruptedException, RadexException {
		String radexExec;
		if (isWindowsOS) {
			radexExec = "radex.exe";
		} else {
			radexExec = "radex";
		}

		String exeFullPath = radexPath + File.separatorChar + "bin" + File.separatorChar + radexExec;

		ProcessBuilder pb = new ProcessBuilder(exeFullPath);
		pb.directory(new File(radexPath));
		pb.redirectInput(new File(radexPath + File.separatorChar + inputFile));
		if (outputOn) {
			pb.redirectOutput(ProcessBuilder.Redirect.INHERIT);
		} else {
			pb.redirectOutput(ProcessBuilder.Redirect.PIPE);
		}
		pb.redirectError(ProcessBuilder.Redirect.PIPE);
		Process p = pb.start();
		if (!outputOn) {
			consumeOutputStream(p);
		}
		handleErrorStream(p);
		return p;
	}

	private static void configureMakefile(String makeFile, boolean windowsOS) throws IOException {
		List<String> lines = Files.readAllLines(new File(makeFile).toPath(), Charset.defaultCharset());

		String compiler = getCompiler(lines);

		String opt = getCompilerParameters(compiler);

		List<String> linesOut = new ArrayList<>(lines.size());
		boolean foundFflagsLine = false;
		boolean foundExec = false;
		boolean foundBindDir = false;
		boolean foundStrip = false;
		boolean foundInstall = false;
		boolean foundCleanO = false;
		boolean foundCleanExec = false;
		for (String line : lines) {
			if (compiler != null && !foundFflagsLine && line.startsWith("FFLAGS")) {
				foundFflagsLine = true;
				line = opt;
			} else if (windowsOS) {
				if (!foundExec && "EXEC    = radex".equals(line)) {
					foundExec = true;
					line = "EXEC    = radex.exe";
				} else if (!foundBindDir && "BINDIR  = ../bin/".equals(line)) {
					foundBindDir = true;
					line = "BINDIR  = ..\\bin\\";
				} else if (!foundStrip && "	strip $@".equals(line)) {
					foundStrip = true;
					line = "#	strip $@";
				} else if (!foundInstall && "	install -m 755 -p -s $(EXEC) $(BINDIR)".equals(line)) {
					foundInstall = true;
					line = "	copy $(EXEC) $(BINDIR)";
				} else if (!foundCleanO && "	- rm *.o".equals(line)) {
					foundCleanO = true;
					line = "	del *.o";
				} else if (!foundCleanExec && "	- rm $(EXEC)".equals(line)) {
					foundCleanExec = true;
					line = "	del $(EXEC)";
				}
			}
			linesOut.add(line);
		}
		Files.write(new File(makeFile).toPath(), linesOut, Charset.defaultCharset());
	}

	/**
	 * Search the compiler used from the lines of the file.
	 *
	 * @param lines the list of lines to search in.
	 * @return return the compiler used or null if not found.
	 */
	private static String getCompiler(List<String> lines) {
		String line = getFcLine(lines);
		if (line != null) {
			if (line.contains(IFORT_COMPILER)) {
				return IFORT_COMPILER;
			} else if (line.contains(GFORTRAN_COMPILER)) {
				return GFORTRAN_COMPILER;
			} else if (line.contains(G77_COMPILER)) {
				return G77_COMPILER;
			} else if (line.contains(G95_COMPILER)) {
				return G95_COMPILER;
			}
		}
		return null;
	}

	/**
	 * Return the line starting with "FC".
	 *
	 * @param lines The list of lines to search in.
	 * @return the line starting with fc or null if not found.
	 */
	private static String getFcLine(List<String> lines) {
		for (String line : lines) {
			if (line.startsWith("FC")) {
				return line;
			}
		}
		return null;
	}



	/**
	 * Return parameters to use for a compiler.
	 *
	 * @param compiler The compiler.
	 * @return The parameters to use (can be null).
	 */
	private static String getCompilerParameters(String compiler) {
		String param;
		switch (compiler) {
			case IFORT_COMPILER:
				param = "FFLAGS += -fno-automatic -Wall -O2 -132";
				break;
			case G95_COMPILER:
				param = "FFLAGS += -O2 -ffree-line-length-huge";
				break;
			case GFORTRAN_COMPILER:
				param = "FFLAGS += -O2 -std=legacy -ffixed-line-length-none";
				break;
			case G77_COMPILER:
				param = "FFLAGS += -fno-automatic -O2 -ffixed-line-length-none";
				break;
			default:
				param = null;
				LOGGER.warn("Unknow compiler.");
		}
		return param;
	}

	private static List<RadexOutputModel> read(String radexPath, String outPutFile) throws RadexException {
		File outPut = new File(radexPath + File.separatorChar + outPutFile);
		if (!outPut.exists()) {
			throw new RadexException("An error occured with the RADEX Fortran.");
		}

		try (BufferedReader br =
				new BufferedReader(new InputStreamReader(new FileInputStream(outPut)))) {
			String line = br.readLine();
			List<RadexOutputModel> outputModels = new ArrayList<>();
			boolean data = false;

			while (line != null && !line.isEmpty()) {
				if (data) {
					StringTokenizer tok = new StringTokenizer(line);
					String[] split = new String[12];

					split[0] = tok.nextToken();
					tok.nextToken();
					split[1] = tok.nextToken();

					for (int i = 2; i < 12; i++) {
						split[i] = tok.nextToken();
					}

					double wavel;
					try {
						wavel = Double.valueOf(split[4]);
					} catch (NumberFormatException nfe) {
						LOGGER.warn("wavel is not a number: setting it to Double.NaN", nfe);
						wavel = Double.NaN;
					}

					try {
						RadexOutputModel outputModel = new RadexOutputModel(split[0].trim(), split[1].trim(),
								Double.valueOf(split[2]), Double.valueOf(split[3]), wavel,
								Double.valueOf(split[5]), Double.valueOf(split[6]), Double.valueOf(split[7]),
								Double.valueOf(split[8]), Double.valueOf(split[9]), Double.valueOf(split[10]),
								Double.valueOf(split[11]));
						outputModels.add(outputModel);
					} catch (NumberFormatException nfe) {
						LOGGER.error("Error in the radex output file", nfe);
					}
				} else if (line.startsWith("                   ")) {
					data = true;
				} else if (line.startsWith("Calculation finished in")) {
					String[] split = line.split("[\\s\"]+");
					if (split.length == 5) {
						try {
							Integer.valueOf(split[3]);
						} catch (NumberFormatException nfe) {
							// If it is not a number, it should be some * :
							// RADEX has not converged here.
							LOGGER.warn("Error didn't converge in 9999 iteration.", nfe);
							throw new RadexException("Radex didn't converge in 9999 iterations.");
						}
					}
				}
				line = br.readLine();
			}
			return outputModels;
		} catch (IOException ioe) {
			LOGGER.error("Error while reading RADEX output file", ioe);
		}
		return null;
	}

	private static void cleanRadexFortran(String radexPath, boolean cleanExe, boolean isWindowsOS) {
		File inputFile = new File(radexPath + File.separatorChar + RadexConfiguration.getInstance().getInput());
		File outputFile = new File(radexPath + File.separatorChar + RadexConfiguration.getInstance().getOutput());
		deleteFile(inputFile);
		deleteFile(outputFile);

		if (cleanExe) {
			String exe = isWindowsOS? "radex.exe" : "radex";
			File exeFile = new File(radexPath + File.separatorChar + "bin" + File.separatorChar + exe);
			deleteFile(exeFile);
		}
	}

	/**
	 * Delete given file if it exists, log it in case of failure.
	 *
	 * @param fileToDelete The file to delete.
	 */
	private static void deleteFile(File fileToDelete) {
		if (fileToDelete.exists() && !fileToDelete.delete()) {
			LOGGER.error("Unable to delete file '" + fileToDelete.getAbsolutePath() + "'.");
		}
	}

	/**
	 * Consume output stream. This avoid the process to be blocked when running.
	 *
	 * @param p The process.
	 */
	private static void consumeOutputStream(Process p) {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()))){
			String line = br.readLine();
			while (line != null) {
				line = br.readLine();
			}
		} catch (IOException e) {
			LOGGER.error("Error while handling radex output stream", e);
		}
	}

	private static void handleErrorStream(Process p) throws RadexException {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(p.getErrorStream()))) {
			String line = br.readLine();
			while (line != null) {
				System.err.println(line);
				if (" *  SINGULAR MATRIX A - NO SOLUTION".equals(line)) {
					p.destroy();
					throw new RadexException(
							"RADEX aborted due to an unrecovered error "
							+ "in the library SLATEC: Singular Matrix.");
				}
				line = br.readLine();
			}
		} catch (IOException e) {
			LOGGER.error("Error while handling radex error stream", e);
		}
	}

	private static int compil(String radexPath, String makeFile) throws IOException {
		ProcessBuilder make = new ProcessBuilder("make", "-f", makeFile);
		make.directory(new File(radexPath + File.separatorChar + "src"));
		make.redirectInput(ProcessBuilder.Redirect.INHERIT);
		if (outputOn)
			make.redirectOutput(ProcessBuilder.Redirect.INHERIT);
		make.redirectError(ProcessBuilder.Redirect.INHERIT);
		int waitFor;
		try {
			waitFor = make.start().waitFor();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			waitFor = 0;
		}
		return waitFor;
	}

	private static void writeConfigFileRadex(String radexConfigFile, String radat, String log, int geometry, boolean isWindowsOS) throws RadexException {
		if (isWindowsOS) {
			radat = radat.replace("\\", "\\\\");
		}
		replaceParameter(radexConfigFile, "radat", radat, true);
		replaceParameter(radexConfigFile, "logfile", log, true);
		replaceParameter(radexConfigFile, "method", String.valueOf(geometry), false);
		replaceCharacterNumber(radexConfigFile, 20, 200);
	}

	private static void replaceCharacterNumber(String sFile, int nbCharMaxLev,
			int nbCharPath) throws RadexException {
		String correctedLines = "";
		// Read
		try (BufferedReader br = new BufferedReader(new FileReader(sFile))) {
			String line;

			String pattern = "character\\*(\\d+)(\\s+)qnum\\(maxlev\\)";
			String replacement =
					"character*" + nbCharMaxLev + "$2qnum(maxlev)";

			String pattern2 =
					"character\\*(\\d+)(\\s+)outfile,molfile,radat,specref,logfile";
			String replacement2 = "character*" + nbCharPath +
					"$2outfile,molfile,radat,specref,logfile";

			while ((line = br.readLine()) != null) {
				line = line.replaceAll(pattern, replacement);
				line = line.replaceAll(pattern2, replacement2);
				correctedLines += line + '\n';
			}
		} catch (Exception e) {
			LOGGER.error("Error while updating radex file", e);
			throw new RadexException("Error during the configuration of the file.");
		}
		// Write
		try (FileOutputStream fos = new FileOutputStream(sFile)) {
			fos.write(correctedLines.getBytes());
		} catch (IOException e) {
			LOGGER.error("Error while writing file", e);
			throw new RadexException("Error during the configuration of the file.");
		}
	}

	private static void replaceParameter(String sFile, String parameter, String value, boolean string) throws RadexException {
		String correctedLines = "";
		// Read
		try (BufferedReader br = new BufferedReader(new FileReader(sFile))) {
			String line;

			String pattern;
			String replacement;
			if (string) {
				pattern = "parameter(\\s*)\\((\\s*)" + parameter + "(\\s*)=(\\s*)'(.*)'\\)";
				replacement = "parameter$1($2" + parameter + "$3=$4'" + value + "')";
			} else {
				pattern = "parameter(\\s*)\\((\\s*)" + parameter + "(\\s*)=(\\s*)(\\S+)\\)";
				replacement = "parameter$1($2" + parameter + "$3=$4" + value + ')';
			}
			while ((line = br.readLine()) != null) {
				line = line.replaceAll(pattern, replacement);
				correctedLines += line + '\n';
			}
		} catch (Exception e) {
			LOGGER.error("Error while updating radex file", e);
			throw new RadexException("Error during the configuration of the file.");
		}
		// Write
		try (FileOutputStream fos = new FileOutputStream(sFile)) {
			fos.write(correctedLines.getBytes());
		} catch (IOException e) {
			LOGGER.error("Error while writing file", e);
			throw new RadexException("Error during the configuration of the file.");
		}
	}

	private static void writeRadexInputFile(RadexInputModel radexInputModel, String inputFile,
			String outPutFile) {
		double cdmol = radexInputModel.getCdmol();
		if (cdmol == 1E25) {
			cdmol = 9.999999E24;
		}
		try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(inputFile))) {
			bufferedWriter.append(radexInputModel.getMoleFile() + "\n");
			bufferedWriter.append(outPutFile + "\n");
			bufferedWriter.append(radexInputModel.getFmin() + " " + radexInputModel.getFmax() + "\n");
			bufferedWriter.append(radexInputModel.getTkin() + "\n");
			bufferedWriter.append(radexInputModel.getNpart() + "\n");
			for (int i = 0; i < radexInputModel.getNpart(); i++) {
				bufferedWriter.append(radexInputModel.getPartners().get(i).getTypePartner()  + "\n");
				bufferedWriter.append(radexInputModel.getPartners().get(i).getDensity()  + "\n");
			}

			bufferedWriter.append(radexInputModel.getTbg()  + "\n");
			bufferedWriter.append(cdmol  + "\n");
			bufferedWriter.append(radexInputModel.getDeltav()  + "\n");
			bufferedWriter.append("0"  + "\n");
			bufferedWriter.flush();
		} catch (IOException e) {
			LOGGER.error("IOException while writing RADEX input file", e);
		}
	}

	/**
	 * Get the status of Radex output
	 *
	 * @return True if on, else False
	 */
	public static boolean isOutputOn() {
		return outputOn;
	}

	/**
	 * Set the outputOn value
	 *
	 * @param outputOn value of the outputOn
	 */
	public static void setOutputOn(boolean outputOn) {
		RadexExecution.outputOn = outputOn;
	}



}
