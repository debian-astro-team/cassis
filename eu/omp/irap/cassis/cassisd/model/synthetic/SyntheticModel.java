/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.cassisd.model.synthetic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.ChannelDescription;
import eu.omp.irap.cassis.common.CommentedComponentsSpectrum;
import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.ComponentAlgo;
import eu.omp.irap.cassis.common.ComponentInterface;
import eu.omp.irap.cassis.common.Formula;
import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.common.ParamArrayDescription;
import eu.omp.irap.cassis.common.ParameterDescription;
import eu.omp.irap.cassis.common.RadexException;
import eu.omp.irap.cassis.common.Telescope;
import eu.omp.irap.cassis.common.TypeFrequency;
import eu.omp.irap.cassis.common.UtilArrayList;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.database.access.AccessDataBase;
import eu.omp.irap.cassis.database.access.InfoDataBase;
import eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent.ComponentContinuum;
import eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent.ComponentDescription;
import eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent.ComponentSpecies;
import eu.omp.irap.cassis.gui.model.parameter.observing.ObservingMode;
import eu.omp.irap.cassis.gui.plot.tools.ToolsUtil;
import eu.omp.irap.cassis.parameters.ContinuumParameters;
import eu.omp.irap.cassis.properties.Software;

public class SyntheticModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(SyntheticModel.class);

	private static final int MINIMUM_SPIRE_OVERSAMPLING = 100;

	private double freqMin = Formula.freqMin;
	private double freqMax = Formula.freqMax;
	private double noise = 0.;
	private double lineFrequency = 0;
	private double vlsrDisplay = 0;
	private double vlsrOffset = 0;
	private double fwhmMaxMhz = Double.NEGATIVE_INFINITY;

	private double dsbValue = 0;
	private boolean secondeBand = false;
	private List<Double> lsbList;
	private List<Double> usbList;

	private boolean haveMolecule = false;

	private boolean tmbTaConv = false;
	private boolean dsbMode = false;

	private boolean lineAnalysis = false;
	private boolean compressSpectrum = true;
	private double bandwidthMhz = 0.;

	private List<ComponentDescription> listComponents;
	private List<ComponentSpecies> listComponentsWithoutContinuum;

	private Map<String, ParameterDescription> mapParameter;

	private double loFreq = 0;

	private double[] freqs;
	private TypeFrequency typeFrequency = TypeFrequency.REST;

	private Telescope telescope;

	private double tbg = 2.73;
	/**
	 * If start with spire_
	 * Redundant with telescope but speed the processing, as we don't have to test it several time.
	 */
	private boolean spireTelescope = false;

	private ContinuumParameters continuumParams = new ContinuumParameters(0.);

	private double[] jNuTbg;
	private double[] continuum;

	private boolean fsw = false;

	/**
	 *
	 * @param mapParameter
	 * @param freqs
	 * @param listComponents
	 * @param innerSpectrumVal
	 * @param dsbModeVal
	 * @param telescope
	 */
	public SyntheticModel(Map<String, ParameterDescription> mapParameter,
			double[] freqs, List<ComponentDescription> listComponents,
			boolean innerSpectrumVal, boolean dsbModeVal, Telescope telescope) {

		this.freqs = freqs;
		this.dsbMode = dsbModeVal;
		this.secondeBand = innerSpectrumVal;

		this.listComponents = listComponents;

		this.listComponentsWithoutContinuum = new ArrayList<>();
		for (ComponentDescription component : listComponents) {
			if (!component.isContinuum()) {
				listComponentsWithoutContinuum.add((ComponentSpecies)component);
			} else {//extract the continuum from the ComponentContinuum
				continuumParams = ((ComponentContinuum)component).getSelectedContinuumParameters();
			}
		}
		//extract the continuum from the map parameter
		if (mapParameter.containsKey("continuum")) {
			String continuum = mapParameter.get("continuum").getVarName();
			try {
				continuumParams = new ContinuumParameters(Double.parseDouble(continuum));
			} catch (NumberFormatException e) {

				continuumParams = new ContinuumParameters(continuum);
			}
		}

		this.mapParameter = mapParameter;
		this.telescope = telescope;
		if (telescope.getName().startsWith("spire")) {
			spireTelescope = true;
		}
		double vlsrSpectrum = 0.;

		tmbTaConv = false;
		compressSpectrum = true;
		lineAnalysis = false;

		freqMin = mapParameter.get("freqMin").getValue();
		freqMax = mapParameter.get("freqMax").getValue();

		if (mapParameter.containsKey("lineAnalysis") ||
			mapParameter.containsKey("script")) {
			if (!mapParameter.containsKey("haveThreshold")){
				mapParameter.put("thresEupMax", new ParameterDescription(Double.MAX_VALUE));
				mapParameter.put("thresEupMin", new ParameterDescription(0.));
				mapParameter.put("thresAijMin", new ParameterDescription(Double.MIN_VALUE));
				mapParameter.put("thresAijMax", new ParameterDescription(Double.MAX_VALUE));
			}

			lineFrequency = mapParameter.get("line_freq").getValue();

			bandwidthMhz = 2 * Math.max(Math.abs(lineFrequency - freqMin), Math.abs(lineFrequency- freqMax));
			compressSpectrum = !mapParameter.containsKey("noCompression");
		} else {
			lineFrequency = freqMin + (freqMax - freqMin) / 2;
			bandwidthMhz = freqMax - freqMin;
			if (mapParameter.containsKey("bandWidthSignal"))
				bandwidthMhz = mapParameter.get("bandWidthSignal").getValue();
			if (mapParameter.containsKey("lineFrequencySignal"))
				lineFrequency = mapParameter.get("lineFrequencySignal").getValue();
		}

		if (mapParameter.containsKey("lineAnalysis")) {
			lineAnalysis = true;
			if (mapParameter.containsKey("vlsr")) {
				vlsrSpectrum = mapParameter.get("vlsr").getValue();
			}
		}

		LOGGER.debug("Synthetic model:\n lineRef = {}\nfreqMin = {}, freqMax = {}",
				lineFrequency, freqMin, freqMax);

		if (mapParameter.containsKey("dsb"))
			dsbValue = mapParameter.get("dsb").getValue();

		if (!secondeBand && !lineAnalysis && mapParameter.containsKey("dsbMode"))
			dsbMode = mapParameter.get("dsbMode").getValue() == 1;

		if (dsbMode) {
			lsbList = ((ParamArrayDescription) mapParameter.get("lsbList")).getParameterList();
			usbList = ((ParamArrayDescription) mapParameter.get("usbList")).getParameterList();
		}

		noise = mapParameter.get("noise").getValue();

		if (mapParameter.containsKey("tmbTa")) {
			tmbTaConv = true;
		}

		// find max fwhm to compute on the border
		double fwhmMax = Double.NEGATIVE_INFINITY;
		int nbMol = 0;
		if (listComponentsWithoutContinuum != null) {
			for (ComponentSpecies comp : listComponentsWithoutContinuum) {
				List<MoleculeDescription> listMol = comp.getMoleculeList();
				nbMol += listMol.size();
				if (comp.getVlsr() == Double.MAX_VALUE) // case in linAnalysis
					comp.setVlsr(vlsrSpectrum);
				for (MoleculeDescription mol : comp.getMoleculeList())
					fwhmMax = Math.max(fwhmMax, mol.getVelocityDispersion());
			}
		}
		haveMolecule = nbMol != 0;

		fwhmMaxMhz = fwhmMax / Formula.CKm * lineFrequency;
		this.mapParameter.put("fwhmMaxMhz", new ParameterDescription(fwhmMaxMhz));

		vlsrDisplay = listComponentsWithoutContinuum.get(0).getVlsr();

		if (mapParameter.containsKey("lineAnalysis")) {
			vlsrDisplay = vlsrSpectrum;
			if (vlsrSpectrum == 0) {
				typeFrequency = TypeFrequency.SKY;
				vlsrOffset = 0;
			}
			else{
				vlsrOffset = vlsrSpectrum;
				typeFrequency = TypeFrequency.REST;
			}
		}
		else {
			typeFrequency  = TypeFrequency.valueOf(mapParameter.get("typeFreq").getVarName());
			if (TypeFrequency.REST.equals(typeFrequency)) {
				if (mapParameter.containsKey("vlsrOffset")) {
					vlsrDisplay = mapParameter.get("vlsrOffset").getValue();
					mapParameter.remove("vlsrOffset");
				}
				vlsrOffset = vlsrDisplay;
			} else{ // Frequency.SKY
				typeFrequency =TypeFrequency.SKY;
				vlsrOffset = 0.;
			}
		}

		this.mapParameter.put("vlsrOffset", new ParameterDescription(vlsrOffset));

		if (mapParameter.containsKey("loFreq"))
			loFreq = mapParameter.get("loFreq").getValue();

		if (mapParameter.containsKey("tbg"))
			tbg = mapParameter.get("tbg").getValue();

		if (mapParameter.containsKey("observingMode")) {
			ObservingMode value = ObservingMode.valueOf(mapParameter.get("observingMode").getVarName());
			if (value.equals(ObservingMode.FSW)) {
				fsw  = true;
			}
		}
	}

	/**
	 * Create the spectrum.
	 *
	 * @return the spectrum.
	 * @throws Exception in case of error.
	 */
	public CommentedComponentsSpectrum createSpectrum() throws Exception {
		if (!haveMolecule)
			return null;

		double offsetSecondBand = 0;
		if (secondeBand) {
			offsetSecondBand = 2 * (loFreq - lineFrequency);
		}

		double freqMinModel = freqMin + offsetSecondBand;
		double freqMaxModel = freqMax + offsetSecondBand;

		for (ComponentSpecies comp : listComponentsWithoutContinuum) {
			double vlsrDecalageCurrentCompo = vlsrOffset - comp.getVlsr();

			final double lineDecalVlsr = this.lineFrequency * (1 - vlsrDecalageCurrentCompo / Formula.CKm);
			double freqMinComp = lineDecalVlsr - bandwidthMhz / 2 + offsetSecondBand;
			double freqMaxComp = lineDecalVlsr + bandwidthMhz / 2 + offsetSecondBand;

			// to get the line in border
			freqMinComp = freqMinComp - 2 * fwhmMaxMhz;
			freqMaxComp = freqMaxComp + 2 * fwhmMaxMhz;

			freqMinModel = Math.min(freqMinModel, freqMinComp);
			freqMaxModel = Math.max(freqMaxModel, freqMaxComp);
		}

		mapParameter.put("freqMin", new ParameterDescription(freqMinModel));
		mapParameter.put("freqMax", new ParameterDescription(freqMaxModel));
		mapParameter.put("lineFrequency", new ParameterDescription(lineFrequency));
		mapParameter.put("bandWidthSignal", new ParameterDescription(bandwidthMhz));
		mapParameter.put("lineFrequencySignal", new ParameterDescription(lineFrequency));

		double[] freqsOrigin = null;
		if (!lineAnalysis && !mapParameter.containsKey("script")) {
			double dfreq = mapParameter.get("resolution").getValue();
			UNIT unitResolution = UNIT.valueOf(mapParameter.get("resolutionUnit").getVarName());
			freqs = computeFreqs(freqMinModel, freqMaxModel, dfreq, unitResolution);

		} else {

			if (lineAnalysis) {
				freqsOrigin = Arrays.copyOf(freqs, freqs.length);
				int oversampling1 = (int) mapParameter.get("oversampling").getValue();
				if (spireTelescope) {
					if (oversampling1 < MINIMUM_SPIRE_OVERSAMPLING) {
						oversampling1 = MINIMUM_SPIRE_OVERSAMPLING;
					}
				} else if (oversampling1 < 1) {
					oversampling1 = 1;
				} else if (oversampling1 > CommentedSpectrum.OVERSAMPLING_LIMIT) {
					oversampling1 = CommentedSpectrum.OVERSAMPLING_LIMIT;
				}
				freqs = UtilArrayList.oversample(freqs, oversampling1);
			} else if (mapParameter.containsKey("script") && spireTelescope) {
				freqsOrigin = Arrays.copyOf(freqs, freqs.length);
				freqs = UtilArrayList.oversample(freqs, MINIMUM_SPIRE_OVERSAMPLING);
			}
		}

		AstroModel astroModel = new AstroModel(mapParameter, freqs,
				listComponentsWithoutContinuum, secondeBand, dsbMode);
		CommentedComponentsSpectrum spectrumRes = null;
		boolean radexException = false;
		try {
			if (!astroModel.getLines()) {
				LOGGER.warn("No lines found");
			}
		} catch (RadexException e) {
			LOGGER.warn(e.getMessage());
			radexException= true;
		}
		if (!radexException) {
			jNuTbg = jNuTbg(freqs);
			double tOn[] = new double[freqs.length];
			continuum = computeContinuum(continuumParams, tmbTaConv, freqs);
			for (int i = 0; i < freqs.length; i++) {
				tOn[i] = jNuTbg[i] + continuum[i];
			}

			List<ComponentSpecies> compos []= separateInteractingCompo(listComponentsWithoutContinuum);

			List<ComponentSpecies> composNonInteracting = compos[0];
			if (!composNonInteracting.isEmpty()) {

				tOn = multiCompoNoInterating(freqs, tOn,
						composNonInteracting, secondeBand);
			}

			List<ComponentSpecies> composInteracting = compos[1];
			if (!composInteracting.isEmpty()) {
				tOn = multiCompoInterating(freqs, tOn, composInteracting, secondeBand);
			}

			if (fsw) {
				for (int i = 0; i < freqs.length; i++) {
					tOn[i] = tOn[i] - jNuTbg[i] -continuum[i];
				}
			} else {
				for (int i = 0; i < freqs.length; i++) {
					tOn[i] = tOn[i] - jNuTbg[i];
				}
			}

			spectrumRes = new CommentedComponentsSpectrum();
			spectrumRes.setFrequencies(freqs);
			spectrumRes.setIntensities(tOn);
			if (spireTelescope) {
				double[] intensityResample = SpireConvolution.computeIntensitiesForSpire(
						freqs, tOn, listComponentsWithoutContinuum.get(0).
											getListOfLines().get(0).getFreqCompute());
				spectrumRes.setIntensities(intensityResample);
			}
		} else {
			spectrumRes = null;
		}

		if (spectrumRes != null) {
			ChannelDescription.addNoise(spectrumRes.getIntensities(), noise);
			if (tmbTaConv) {
				addEfficiency(spectrumRes, telescope.getEffValueList(), telescope.getFrequencyList());
			}
		}

		// remove freq out of value;
		if (!secondeBand && spectrumRes != null) {
			removeFrequency(spectrumRes, freqMin, freqMax);
		}

		if (dsbMode) {
			spectrumRes = dsbSpectrumCreation(spectrumRes);
		} else if (!secondeBand && !haveLines(listComponentsWithoutContinuum)) {
			spectrumRes = null;
		}

		if (!secondeBand && spectrumRes != null) {
			if (compressSpectrum) {
				spectrumRes.compress(2,2);
			}

			spectrumRes.setFreqRef(lineFrequency);
			spectrumRes.setVlsr(vlsrDisplay);
			List<ComponentInterface> copy = new ArrayList<>(listComponentsWithoutContinuum);
			spectrumRes.setListOfComponent(copy);
		}
		if (spectrumRes != null) {
			spectrumRes.setTitle("LTE + RADEX");
			spectrumRes.setVlsr(vlsrDisplay);
			spectrumRes.setVlsrModel(listComponentsWithoutContinuum.get(0).getVlsr());
			if (dsbMode) {
				spectrumRes.setLoFreq(loFreq);
			} else {
				spectrumRes.setLoFreq(Double.NaN);
			}

			spectrumRes.setTypeFreq(typeFrequency);
		}

		if (spireTelescope) {
			double[] freqs2;
			if (lineAnalysis) {
				int oversampling = (int) mapParameter.get("oversampling").getValue();
				if (oversampling < 1) {
					oversampling = 1;
				} else if (oversampling > CommentedSpectrum.OVERSAMPLING_LIMIT) {
					oversampling = CommentedSpectrum.OVERSAMPLING_LIMIT;
				}
				freqs2 = UtilArrayList.oversample(freqsOrigin, oversampling);

				double[] intensity2 = ToolsUtil.resample(freqs2,
					spectrumRes.getFrequenciesSignal(), spectrumRes.getIntensities());

				spectrumRes.setFrequencies(freqs2);
				spectrumRes.setIntensities(intensity2);
			} else if (mapParameter.containsKey("script")) {
				double[] intensity2 = ToolsUtil.resample(freqsOrigin,
						spectrumRes.getFrequenciesSignal(), spectrumRes.getIntensities());

					spectrumRes.setFrequencies(freqsOrigin);
					spectrumRes.setIntensities(intensity2);
			}
		}
		if (spectrumRes != null) {
			addMetadata(spectrumRes);
		}
		return spectrumRes;
	}

	private double[] multiCompoNoInterating(double[] frequencies, double[] tBefore,
			List<ComponentSpecies> listComponent, boolean secondeBand) {

		final int nbFreq = frequencies.length;
		double[] deltaTOn = new double[nbFreq];
		double[] tOn = new double[nbFreq];

		final double telescopeDiameter = telescope.getDiameter();
		for (int i = 0; i < frequencies.length; i++) {
			tBefore[i] = jNuTbg[i] + continuum[i];
			for (ComponentSpecies compoSpecies : listComponent) {
				final double freqMhz = frequencies[i];
				final double tBeforeI = tBefore[i];
				final ComponentAlgo typeModel = compoSpecies.getTypeAlgo();
				List<MoleculeDescription> molecules = compoSpecies.getMoleculeList();
				final List<LineDescription> listOfLines = getLineOfDoubleSideBand(compoSpecies.getListOfLines(),secondeBand);

				double res = computeTon(freqMhz, tBeforeI, molecules, listOfLines, telescopeDiameter, typeModel);
				deltaTOn[i] = deltaTOn[i] + res;
			}
			tOn[i] = deltaTOn[i] + tBefore[i];
		}
		return tOn;
	}

	private double[] multiCompoInterating(double[] frequencies, double[] tOnNonIteracting,
			List<ComponentSpecies> listComponent, boolean secondeBand) {

		final int nbFreq = frequencies.length;
		double[] tOn = new double[nbFreq];
		double tBefore[] = tOnNonIteracting;
		final double telescopeDiameter = telescope.getDiameter();
		for (int i = 0; i < frequencies.length; i++) {
			for (ComponentSpecies compoSpecies : listComponent) {
				final double freqMhz = frequencies[i];
				final double tBeforeI = tBefore[i];
				final ComponentAlgo typeModel = compoSpecies.getTypeAlgo();
				List<MoleculeDescription> molecules = compoSpecies.getMoleculeList();
				final List<LineDescription> listOfLines = getLineOfDoubleSideBand(compoSpecies.getListOfLines(),secondeBand);

				double res = computeTon(freqMhz, tBeforeI, molecules, listOfLines, telescopeDiameter, typeModel);
				tOn[i] = res + tBefore[i];
				tBefore[i] = tOn[i];
			}
		}
		return tOn;
	}

	/**
	 * @param freqMhz
	 * @param tBeforeI
	 * @param molecules
	 * @param listOfLines
	 * @param telescopeDiameter
	 * @param typeModel
	 * @return
	 */
	public double computeTon(final double freqMhz, final double tBeforeI,
			List<MoleculeDescription> molecules, final List<LineDescription> listOfLines,
			final double telescopeDiameter, final ComponentAlgo typeModel) {
		final double tauCompo = sumTauTrans(freqMhz, listOfLines);
		Map<Integer, ArrayList<LineDescription>> separateLinesByMol = separateLinesByMol(listOfLines) ;

		//if radex
		if (typeModel.equals(ComponentAlgo.FULL_RADEX_ALGO)) {
			List<MoleculeDescription> newMolecules = new ArrayList<>();
			Map<Integer, ArrayList<LineDescription>> newSeparateLinesByMol =
					new HashMap<>();
			for (int j = 0; j < molecules.size(); j++) {
				final MoleculeDescription mol = molecules.get(j);
				final ArrayList<LineDescription> lines = separateLinesByMol.get(mol.getTag());
				if (lines != null) {
					for(int cpt = 0; cpt < lines.size(); cpt++) {
						MoleculeDescription newMol = (MoleculeDescription)mol.clone();
						newMol.setTag(newMol.getTag()*1000 + cpt);
						newMol.setTemperature(lines.get(cpt).getTex());
						newMolecules.add(newMol);
						ArrayList<LineDescription> newLines = new ArrayList<>();
						newLines.add(lines.get(cpt));
						newSeparateLinesByMol.put(newMol.getTag(),newLines);
					}
				}
			}
			molecules = newMolecules;
			separateLinesByMol = newSeparateLinesByMol;
		}

		double res =0;
		final double thetaMol=molecules.get(0).getSourceSize();
		for (int j = 0; j < molecules.size(); j++) {
			final MoleculeDescription mol = molecules.get(j);

			final ArrayList<LineDescription> lines = separateLinesByMol.get(mol.getTag());
			if (lines != null ) {
				final double tauMol = sumTauTrans(freqMhz, lines);
				final double jNuTexMol = jNu(freqMhz, mol.getTemperature());
				res = res + jNuTexMol* (1-Math.exp(-tauMol));
			}
		}
		res = res + tBeforeI*(Math.exp(-tauCompo)-1);
		res = res * fillingFactor(thetaMol, Formula.calcHpbw(freqMhz, telescopeDiameter)*
				Formula.coeffRadToArsec) ;
		return res;
	}

	private double[] computeContinuum(ContinuumParameters continuumParams, boolean tmbTaConv,
			double[] frequencies) {
		double continuum[] = new double [frequencies.length];
		for (int i = 0; i < continuum.length; i++) {
			double continuumVal = continuumParams.getContinuumValue(frequencies[i]);
			if (tmbTaConv) { // convert continuum to tmb
				continuumVal = removeEfficiency(continuumVal, frequencies[i],
						telescope.getEffValueList(), telescope.getFrequencyList());
			}
			continuum[i] = continuumVal;
		}

		return continuum;
	}

	/**
	 * @param theta1
	 * @param theta2
	 * @return
	 */
	protected double coveringFactor(double theta1, final double theta2) {
		double coveringFactor = (theta1 * theta1) / (theta2 * theta2);
		if (theta1 > theta2) {
			coveringFactor = 1;
		}
		return coveringFactor;

	}

	/**
	 * Calculates filling factor of region1 (with size theta1)
	 * in region 2 (with size theta2) with theta2 > theta1
	 * @param theta1
	 * @param theta2
	 * @return
	 */

	private double fillingFactor(double theta1, double theta2) {
		return (theta1 * theta1) / ((theta1 * theta1)+ (theta2 * theta2));
	}

	/**
	 * @param frequencies
	 * @return
	 */
	protected double[] jNuTbg(double[] frequencies) {
		double[] jNuTbg = new double[frequencies.length];
		for (int i = 0; i < jNuTbg.length; i++) {
			jNuTbg[i] = jNu(frequencies[i], tbg);
		}
		return jNuTbg;
	}

	private double sumTauTrans(double freqMhz, List<LineDescription> lines) {
		double res = 0;
		for (LineDescription line : lines) {
			double lineWidthMHz = Formula.calcLineWidthMHz(line.getDopplerWidth(), line.getFreqCompute());
			double sigma = Formula.calcSigma(lineWidthMHz, AstroModel.FWHM2SIGMA_GAUSSIAN);
			double tau = Formula.calcAintGaussian(line.getIntegralFlux(), line.getFreqCompute() - freqMhz, sigma);
			res = res + tau;
		}
		return res;
	}

	private double jNu(double freqMhz, double temperature) {
		double res = 0;
		double t0 = Formula.calcTO(freqMhz);
		res = t0 /(Math.exp(t0/temperature)-1);
		return res;
	}

	/**
	 * Compute the array of frequency of the spectrum in
	 * the range (freqMinModel, freqMaxModel)  with the deltaFreq dfreq, acoording to
	 * the unit of the resolution unitResolution
	 *
	 * @param freqMinModel
	 * @param freqMaxModel
	 * @param dfreq
	 * @param unitResolution
	 * @return the array of the frequency
	 */
	private double [] computeFreqs(double freqMinModel, double freqMaxModel, double dfreq,
			UNIT unitResolution) {
		double freqs[] = null;
		if (UNIT.MHZ.equals(unitResolution)){
			int nfreq = (int) Math.ceil((freqMaxModel - freqMinModel) / dfreq) + 1;

			// initialization of the synthetic spectrums
			freqs = new double[nfreq];
			for (int kfreq = 0; kfreq < nfreq; kfreq++) {
				freqs[kfreq] = freqMinModel + kfreq * dfreq;
			}
		} else {
			XAxisCassis  xAxisCassis= XAxisCassis.getXAxisCassis(unitResolution);
			if (xAxisCassis.isInverted()) {
				final double maxVal = xAxisCassis.convertFromMhzFreq(freqMinModel);
				final double minVal = xAxisCassis.convertFromMhzFreq(freqMaxModel);
				int nbPoints = (int) Math.ceil((maxVal-minVal) / dfreq) + 1;
				freqs = new double[nbPoints];
				for (int i = freqs.length-1; i>-1; i--) {
					freqs[i] = xAxisCassis.convertToMHzFreq(minVal + dfreq * i);
				}
			} else {
				final double minVal = xAxisCassis.convertFromMhzFreq(freqMinModel);
				final double maxVal = xAxisCassis.convertFromMhzFreq(freqMaxModel);
				int nbPoints = (int) Math.ceil((maxVal-minVal) / dfreq) + 1;
				freqs = new double[nbPoints];
				for (int i = 0; i <freqs.length; i++) {
					freqs[i] = xAxisCassis.convertToMHzFreq(minVal + dfreq * i);
				}
			}
		}
		return freqs;
	}

	/**
	 * Add metadata to the spectrum.
	 *
	 * @param spectrumRes The spectrum.
	 */
	private void addMetadata(CommentedComponentsSpectrum spectrumRes) {
		List<CassisMetadata> metadataList = new ArrayList<>();
		metadataList.add(new CassisMetadata("creation_date", new Date().toString(),
				"Creation date of this spectrum", null));
		metadataList.add(new CassisMetadata("creator", Software.getFriendlyVersion(),
				"Generator of this spectrum", null));
		metadataList.add(new CassisMetadata("database",
				InfoDataBase.getInstance().getTypeDb() + " ("
						+ AccessDataBase.getDataBaseConnection().getId() + ")",
				"Database used", null));
		if (dsbMode) {
			metadataList.add(new CassisMetadata("loFrequency",
					String.valueOf(loFreq), null, UNIT.MHZ.toString()));
		}
		metadataList.add(new CassisMetadata("model",
				listComponentsWithoutContinuum.get(0).getTypeAlgo().toString(),
				"Model used to compute this spectrum", null));
		String noiseUnit = spireTelescope ? UNIT.M_JANSKY.toString()
				: UNIT.M_KELVIN.toString();
		metadataList.add(new CassisMetadata("noise", String.valueOf(noise),
				"Noise added to the spectrum", noiseUnit));
		metadataList.add(new CassisMetadata("telescope", this.telescope.getName(),
				"Telescope", null));
		metadataList.add(new CassisMetadata("typeFrequency", typeFrequency.forSave(),
				null, null));
		metadataList.add(new CassisMetadata("vlsr", String.valueOf(vlsrDisplay), null,
				UNIT.KM_SEC_MOINS_1.toString()));
		metadataList.add(new CassisMetadata("vlsr_model",
				String.valueOf(listComponentsWithoutContinuum.get(0).getVlsr()), null,
				UNIT.KM_SEC_MOINS_1.toString()));
		spectrumRes.setCassisMetadataList(metadataList);
	}

	/**remove the channels of the spectrumRes
	 * out of the range (freqMin, freqMax)
	 * @param spectrumRes
	 * @param freqMin
	 * @param freqMax
	 */
	private void removeFrequency(CommentedComponentsSpectrum spectrumRes,
			double freqMin, double freqMax) {
		double[] frequenciesRes = spectrumRes.getFrequenciesSignal();
		double[] intensitiesRes = spectrumRes.getIntensities();
		int size = spectrumRes.getSize();
		int cpt = 0;
		double[] frequencies = new double[size];
		double[] intensities = new double[size];

		for (int index = 0; index < size; index++) {
			if ((frequenciesRes[index] > freqMin || Math.abs(frequenciesRes[index] - freqMin) < 1e-6)&&
				(frequenciesRes[index] < freqMax || Math.abs(frequenciesRes[index] - freqMax) < 1e-6)) {
				frequencies[cpt] = frequenciesRes[index];
				intensities[cpt] = intensitiesRes[index];
				cpt++;
			}
		}

		frequencies = Arrays.copyOf(frequencies, cpt);
		intensities = Arrays.copyOf(intensities, cpt);

		spectrumRes.setListOfChannels(frequencies, intensities);
	}

	/**
	 * Retrieve the lines for each species
	 *
	 * @param lines
	 * @return
	 */
	private HashMap<Integer,  ArrayList<LineDescription>> separateLinesByMol(
			List<LineDescription> lines) {
		HashMap<Integer, ArrayList<LineDescription>> res = new HashMap<>();

		for (LineDescription line : lines) {
			ArrayList<LineDescription> arrayList = res.get(line.getMolTag());
			if (arrayList == null){
				arrayList = new ArrayList<>();
				res.put(line.getMolTag(), arrayList);
			}
			arrayList.add(line);
		}
		return res;
	}

	private List<LineDescription> getLineOfDoubleSideBand(
			ArrayList<LineDescription> lines, boolean isDoubleSideBand) {
		List<LineDescription> res = new ArrayList<>();
		for (LineDescription line : lines) {
			if (isDoubleSideBand == line.isDoubleSideBand()) {
				res.add(line);
			}
		}
		return res;
	}

	/**
	 * Split the listcomponent in 2 : the none interacting compo and the interacting compo
	 * and remove component without molecules
	 * @param listSpeciesComponents
	 * @return an array
	 * [0] none interacting compo
	 * [1] interacting compo
	 */
	public static List<ComponentSpecies>[] separateInteractingCompo(
			List<ComponentSpecies> listSpeciesComponents) {

		@SuppressWarnings("unchecked")
		List<ComponentSpecies> [] res = new List[2];

		res[0] = new ArrayList<>();
		res[1] = new ArrayList<>();
		for (int cpt = 0; cpt < listSpeciesComponents.size(); cpt++) {
			ComponentSpecies componentSpecies = listSpeciesComponents.get(cpt);
			if (!componentSpecies.getMoleculeList().isEmpty()) {
				if (!componentSpecies.isInteracting()) {
					res[0].add(componentSpecies);
				} else {
					res[1].add(componentSpecies);
				}
			}
		}
		return res;
	}

	private void addEfficiency(CommentedSpectrum spectrum, List<Double> efficiencyList,
			List<Double> frequencyList) {
		double[] frequencies = spectrum.getFrequenciesSignal();
		double[] intensities = spectrum.getIntensities();
		for (int k = 0; k < spectrum.getSize(); k++) {
			double frequency = frequencies[k];
			double efficiency = UtilArrayList.getArrayValueAt(
									frequency, efficiencyList, frequencyList);
			intensities[k] = intensities[k] * efficiency;
		}
	}

	private double removeEfficiency(double intensity, double frequency,
			List<Double> efficiencyList, List<Double> frequencyList) {
		double efficiency =
				UtilArrayList.getArrayValueAt(frequency, efficiencyList, frequencyList);
		return intensity / efficiency;
	}

	/**
	 * Compute the dsb spectrum.
	 *
	 * @param spectrum
	 * @return CommentedSpectrum --> the spectrum
	 * @throws Exception
	 */
	private CommentedComponentsSpectrum dsbSpectrumCreation(
			CommentedComponentsSpectrum spectrum) throws Exception {
		SyntheticModel model = new SyntheticModel(mapParameter, freqs,
												listComponents, true, false, telescope);
		CommentedComponentsSpectrum dsbSpectrum = model.createSpectrum();

		if (dsbSpectrum != null) {
			removeFrequency(dsbSpectrum,
					freqMin + 2 * (loFreq - lineFrequency),
					freqMax + 2 * (loFreq - lineFrequency));
		}

		if (spectrum != null && dsbSpectrum != null &&
				haveLines(listComponentsWithoutContinuum)){
			CommentedSpectrum.dsbCompute(spectrum, dsbValue,
					dsbSpectrum, usbList, lsbList, telescope.getFrequencyList(), 0);
		}
		return spectrum;
	}

	/**
	 *
	 * @param listComponents
	 * @return
	 */
	private boolean haveLines(List<ComponentSpecies> listComponents) {
		boolean res = false;
		for (int i = 0; ! res && i < listComponents.size(); i++) {
			ComponentSpecies temp = listComponents.get(i);
			res = temp.getListOfLines().size()!=0;
		}
		return res;
	}
}
