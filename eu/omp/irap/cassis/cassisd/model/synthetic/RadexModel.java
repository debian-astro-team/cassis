/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.cassisd.model.synthetic;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.Molecule;
import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.common.RadexException;
import eu.omp.irap.cassis.properties.Software;
import radex.PartnerModel;
import radex.RadexInputModel;
import radex.RadexOutputModel;
import radex.utils.TypePartnerMap;

public class RadexModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(RadexModel.class);
	public static final String SPACE_IN_RADEX = "&";

	private RadexInputModel radexInputModel;
	private List<RadexOutputModel> radexResult;
	private static RadexExecutionInterface radexExecutionInterface = new RadexExecution();

	public RadexModel(){

	}
	/**
	 * Method = 1 :uniforme sphere Method = 2 :expanding sphere Method = 3: slab
	 * geometry = 0: uniforme sphere geometry = 1: slab geometry = 2: expanding
	 * sphere
	 *
	 * @param geometry
	 * @return
	 */
	private int geometryToMethod(int geometry) {
		if (geometry == 0)
			return 1;
		else if (geometry == 1)
			return 3;
		else
			return 2;
	}

	/**
	 * Return the list of {@link PartnerModel}.
	 *
	 * @param collision
	 *            The collision.
	 * @return a list of {@link PartnerModel}
	 */
	public static List<PartnerModel> getPartnersFromCollision(String collision) {
		if ("-yes-".equals(collision) || Molecule.NO_COLLISION.equals(collision))
			return new ArrayList<>();

		List<PartnerModel> list = new ArrayList<>();
		TypePartnerMap tpMap = new TypePartnerMap();

		String[] partners = collision.split("&");
		for (int i = 1; i < partners.length; i++) {
			partners[i] = partners[i].substring(1, partners[i].length() - 1);
			String[] ss = partners[i].split(";");

			String typePartner = ss[0];
			Integer id = tpMap.get(typePartner);
			double density = 1.0;
			try {
				density = Double.parseDouble(ss[1]);
			} catch (NumberFormatException nfe) {
				LOGGER.warn("Invalid collision form", nfe);
				return null;
			}
			PartnerModel pModel = new PartnerModel(id, typePartner, density);
			list.add(pModel);
		}
		return list;
	}

	public static String getMoleFileFromCollision(String collision) {
		String res = "";
		if ("-yes-".equals(collision) || Molecule.NO_COLLISION.equals(collision)) {
			res =  "";
		}	else if  (collision.indexOf('&')== -1){
			res = "";
		} else {
			res = collision.substring(0, collision.indexOf('&'));
		}
		return res;
	}

	private RadexInputModel calculInputRadex(MoleculeDescription molecule, double fmin, double fmax,
			double tbg, double fwhm, int geometry) {
		String molFile = getMoleFileFromCollision(molecule.getCollision());
		String radat = Software.getRadexMoleculesManager().getPathCollisionFile(molFile).getParent() + File.separatorChar;
		return new RadexInputModel(fmin, fmax, tbg, geometryToMethod(geometry), radat,
				getMoleFileFromCollision(molecule.getCollision()),
				getPartnersFromCollision(molecule.getCollision()).size(), molecule.getTKin(), fwhm,
				molecule.getDensity(), getPartnersFromCollision(molecule.getCollision()));
	}

	public boolean compute(MoleculeDescription molecule, double fmin, double fmax, double tbg,
			double fwhm, int geometry) throws RadexException {
		radexResult = null;
		radexInputModel = calculInputRadex(molecule, fmin, fmax, tbg, fwhm, geometry);

		if (radexInputModel.getPartners() != null) {
			checkRadexInput(radexInputModel);
			radexResult = radexExecutionInterface.executeRadex(radexInputModel);

		}
		return radexResult != null;
	}

	private void checkRadexInput(RadexInputModel rim) throws RadexException {
		if (rim.getCdmol() < 1E5 || rim.getCdmol() > 1E25) {
			throw new RadexException(
					"Radex input is invalid. The density must be between 1e5 and 1e25.");
		}
	}

	public List<RadexOutputModel> getResult() {
		return radexResult;
	}

	/**
	 * Search the line in Radex results.
	 *
	 * @param radexParamLines
	 *            containing the Radex convention for qn number:
	 *            qnUp1_qnUp2_qnLow1_qnLow2
	 * @param qn
	 *            The quantum number with CASSIS convention for qn number :
	 *            qnUp1:qnUp2:qnLow1:qnLow2
	 * @return The index of Radex parameter.
	 */
	public static int getRADEXParamIndic(List<RadexOutputModel> radexParamLines, String qn) {
		//
		String[] qnTemp = qn.split(":");
		String[] qnCassisUp = Arrays.copyOf(qnTemp, qnTemp.length / 2);
		String[] qnCassisLo = Arrays.copyOfRange(qnTemp, qnTemp.length / 2, qnTemp.length);

		int radexParamIndic = -1;

		for (int i = 0; i < radexParamLines.size() && radexParamIndic == -1; i++) {
			String qnRadexUp = radexParamLines.get(i).getQnumM().replaceAll(SPACE_IN_RADEX, " ");
			String qnRadexLo = radexParamLines.get(i).getQnumN().replaceAll(SPACE_IN_RADEX, " ");
			String[] qnRadexUpTab = qnRadexUp.split("_");
			String[] qnRadexLoTab = qnRadexLo.split("_");

			boolean possible = qnRadexUpTab.length == qnCassisUp.length
					&& qnRadexLoTab.length == qnCassisLo.length;

			for (int j = 0; j < qnRadexLoTab.length && possible; j++) {
				String qnOneRadexUp = qnRadexUpTab[j];
				String qnOneRadexLo = qnRadexLoTab[j];
				String qnOneCassisUp = qnCassisUp[j];
				String qnOneCassisLo = qnCassisLo[j];

				possible = qnOneRadexUp.equals(qnOneCassisUp) && qnOneRadexLo.equals(qnOneCassisLo);
				if (!possible) {
					// possibility to have 0.5 and 0.50 to compare ...
					try {
						double qnumMDouble = Double.parseDouble(qnOneRadexUp);
						double qnumNDouble = Double.parseDouble(qnOneRadexLo);
						double qnOneCassisUpDouble = Double.parseDouble(qnOneCassisUp);
						double qnOneCassisLoDouble = Double.parseDouble(qnOneCassisLo);
						possible = (Math.abs(qnumMDouble - qnOneCassisUpDouble) <= 1E-12)
								&& (Math.abs(qnumNDouble - qnOneCassisLoDouble) <= 1E-12);

					} catch (NumberFormatException e) {
						possible = false;
					}
				}
			}

			if (possible) {
				radexParamIndic = i;
			}
		}
		return radexParamIndic;
	}
	public static void setRadexExecutionInterface(RadexExecutionInterface radexExecution) {
		radexExecutionInterface = radexExecution;
	}

}
