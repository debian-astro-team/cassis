/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.cassisd.model.synthetic;

import java.util.Arrays;

import eu.omp.irap.cassis.gui.plot.tools.ToolsUtil;
import herschel.ia.numeric.Double1d;
import herschel.ia.numeric.toolbox.filter.Convolution;
import herschel.ia.numeric.toolbox.fit.GaussModel;

public class SpireConvolution {

	private static final double SPIRE_SPECTRAL_RESOLUTION = 2077.0;
	private static final double FWHM_GAUSS_SPIRE = SPIRE_SPECTRAL_RESOLUTION / (2. * Math.sqrt(2. * Math.log(2.)));


	public static double[] computeIntensitiesForSpire(double[] freqSpectrum,
			double[] listIntensitySpectrum, double freqCompute) {
		//Compute à 100 mhz around the fist line of the fist components with the vlsr
		int nbPointsBefore = 0;
		int nbPointsAfter = 0;
		final int middle = 5000;
		double[] freqAroundTheLine = new double[middle*2 +1];

		freqAroundTheLine[middle] = freqCompute;
		final double resolution = 100;
		double currentFreq = freqCompute - resolution;
		while (freqSpectrum[0]<= currentFreq) {
			nbPointsBefore++;
			freqAroundTheLine[middle-nbPointsBefore]= currentFreq;
			currentFreq -= resolution;
		}

		currentFreq = freqCompute + resolution;
		final int spectrumLength = freqSpectrum.length;
		while (freqSpectrum[spectrumLength-1]>= currentFreq) {
			nbPointsAfter++;

			freqAroundTheLine[middle+nbPointsAfter]= currentFreq;
			currentFreq += resolution;
		}

		freqAroundTheLine = Arrays.copyOfRange(freqAroundTheLine, middle-nbPointsBefore+1, middle + nbPointsAfter+1);

		double[] intensityResample  = ToolsUtil.resample(freqAroundTheLine,
				freqSpectrum, listIntensitySpectrum);

		Double1d wave = new Double1d(freqAroundTheLine);
		Double1d flux = new Double1d(intensityResample);
		double xo = freqAroundTheLine[0] + (freqAroundTheLine[freqAroundTheLine.length-1] - freqAroundTheLine[0]) / 2.;

		double deltaFreq = resolution;
		double io = deltaFreq / Math.sqrt(2.0 * Math.PI) / FWHM_GAUSS_SPIRE;
		GaussModel gaussModel = new GaussModel();
		double[] params = { io, xo, FWHM_GAUSS_SPIRE };
		gaussModel.setParameters(new Double1d(params));
		Double1d gaussVal = gaussModel.result(wave);

		Convolution f = new Convolution(gaussVal);
		Double1d convol = f.mutate(flux);

		int indCopyDeb = 0;
		int newSize = freqAroundTheLine.length;
		if (freqAroundTheLine[0] !=  freqSpectrum[0]) {
			indCopyDeb = 1;
			newSize++;
		}
		if (freqAroundTheLine[freqAroundTheLine.length-1] !=  freqSpectrum[spectrumLength-1]) {
			newSize ++;
		}
		double[] freqAroudTheLineResample = new double[newSize];
		double[] intensityToResample = new double[newSize];
		System.arraycopy(freqAroundTheLine, 0, freqAroudTheLineResample, indCopyDeb, freqAroundTheLine.length);
		System.arraycopy(convol.toArray(), 0, intensityToResample, indCopyDeb, freqAroundTheLine.length);
		if (freqAroundTheLine[0] !=  freqSpectrum[0]) {
			freqAroudTheLineResample[0] = freqSpectrum[0];
		}
		if (freqAroundTheLine[freqAroundTheLine.length-1] !=  freqSpectrum[spectrumLength-1]) {
			freqAroudTheLineResample[newSize-1] = freqSpectrum[spectrumLength-1];
		}

		intensityResample  = ToolsUtil.resample(freqSpectrum,
				freqAroudTheLineResample, intensityToResample);
		return intensityResample;
	}
}
