/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.cassisd.model.synthetic;

import java.io.File;

import eu.omp.irap.cassis.properties.Software;

public class RadexConfiguration {

	private static RadexConfiguration radexConfiguration;
	private String path;
	private String collisionpath;
	private String log;
	private String conf;
	private String input;
	private String output;



	/**
	 * Default constructor with default parameters.
	 */
	private RadexConfiguration() {
		this.path = Software.getCassisPath() + File.separatorChar + "Radex";
		this.log = "." +  File.separatorChar +"radex.log";
		this.conf = "radex.inc";
		this.input = "CASSIS.inp";
		this.output = "CASSIS.out";
		this.collisionpath = Software.getCassisPath() + File.separator +
				Software.DATABASE_FOLDER  + File.separator +
				Software.RADEX_COLLISION_FOLDER;
	}

	/**
	 * Create if needed the instance of {@link RadexConfiguration} then return it.
	 *
	 * @return the instance of {@link RadexConfiguration}.
	 */
	public static RadexConfiguration getInstance() {
		if (radexConfiguration == null) {
			radexConfiguration = new RadexConfiguration();
		}
		return radexConfiguration;
	}

	/**
	 * Destroy the instance, this is needed for Online mode to reload the
	 *  configuration with correct CASSIS path.
	 */
	public static void destroy() {
		radexConfiguration = null;
	}

	/**
	 * Return if there is Fortran RADEX installed.
	 * For it, test if path is not null and the file radex.inc exist.
	 * If it's the case, we consider that the user has Fortran RADEX.
	 *
	 * @return if there is Fortran RADEX installed.
	 */
	public boolean haveFortran() {
		String radexIncPath = new File(getPath()).getPath() +
				File.separatorChar + "src" + File.separatorChar + this.conf;
		return getPath() != null && !getPath().isEmpty() &&
				new File(radexIncPath).exists();
	}

	/**
	 * Return the path of RADEX Fortan.
	 *
	 * @return the path of RADEX Fortan.
	 */
	public String getPath() {
		return path;
	}

	/**
	 * Set the path of RADEX Fortan.
	 *
	 * @param path The path of RADEX Fortan to set.
	 */
	public void setPath(String path) {
		this.path = path;
	}


	/**
	 * Return the collision path of RADEX.
	 *
	 * @return the collision path of RADEX.
	 */
	public String getCollisionPath() {
		return collisionpath;
	}

	/**
	 * Set the collision path of RADEX.
	 *
	 * @param path The path of RADEX to set.
	 */
	public void setCollisionPath(String path) {
		this.collisionpath = path;

	}

	/**
	 * Return the path of log file.
	 *
	 * @return the path of log file.
	 */
	public String getLog() {
		return log;
	}

	/**
	 * Set the path of log file.
	 *
	 * @param log The path of log file.
	 */
	public void setLog(String log) {
		this.log = log;
	}

	/**
	 * Return the name of the configuration file.
	 *
	 * @return the name of the configuration file.
	 */
	public String getConf() {
		return conf;
	}

	/**
	 * Set the name of the configuration file.
	 *
	 * @param conf The new name of the configuration file.
	 */
	public void setConf(String conf) {
		this.conf = conf;
	}

	/**
	 * Return the RADEX input parameter file.
	 *
	 * @return the RADEX input parameter file.
	 */
	public String getInput() {
		return input;
	}

	/**
	 * Set the RADEX input parameter file.
	 *
	 * @param input The RADEX input parameter file.
	 */
	public void setInput(String input) {
		this.input = input;
	}

	/**
	 * Return the RADEX output file.
	 *
	 * @return the RADEX output file.
	 */
	public String getOutput() {
		return output;
	}

	/**
	 * Set the RADEX output file.
	 *
	 * @param output The new RADEX output file.
	 */
	public void setOutput(String output) {
		this.output = output;
	}

}
