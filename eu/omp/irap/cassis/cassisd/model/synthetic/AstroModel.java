/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.cassisd.model.synthetic;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.Formula;
import eu.omp.irap.cassis.common.GenericParameterDescription;
import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.Molecule;
import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.common.ParameterDescription;
import eu.omp.irap.cassis.common.ProgressDialogConstants;
import eu.omp.irap.cassis.common.RadexException;
import eu.omp.irap.cassis.database.access.AccessDataBase;
import eu.omp.irap.cassis.database.access.DataBaseConnection;
import eu.omp.irap.cassis.database.access.LineDescriptionDB;
import eu.omp.irap.cassis.database.access.MoleculeDescriptionDB;
import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB;
import eu.omp.irap.cassis.database.access.UnknowMoleculeException;
import eu.omp.irap.cassis.gui.model.parameter.UtilDatabase;
import eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent.ComponentSpecies;
import eu.omp.irap.cassis.parameters.LineIdentificationUtils;
import eu.omp.irap.cassis.properties.Software;
import radex.RadexOutputModel;

public class AstroModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(AstroModel.class);

	private List<ComponentSpecies> listComponents = null;
	private double vlsrOffset = 0;
	private boolean secondeBand = false;

	private boolean lineAnalysis = false;

	private double lineFrequency = 0;
	// Frequency of search of line.
	private double freqMin = Formula.freqMin;
	private double freqMax = Formula.freqMax;

	private double dsbValue = 0;
	private boolean dsbMode = false;

	private double thresEupMin = 0.;
	private double thresEupMax = 0.;
	private double thresAijMin = 0.;
	private double thresAijMax = Double.MAX_VALUE;
	private double diameter = 30.;

	private final double[] freqs;
	private List<String> quantumNumbersArray;
	public static final double FWHM2SIGMA_GAUSSIAN = 1. / (2 * Math.sqrt(Math.log(2)));
	private double tbg = 2.73;


	public AstroModel(Map<String, ParameterDescription> mapParameter, final double[] freqs,
			List<ComponentSpecies> listComponents, boolean innerSpectrumVal, boolean dsbModeVal) {
		this.freqs = freqs;
		this.listComponents = listComponents;
		this.dsbMode = dsbModeVal;
		this.secondeBand = innerSpectrumVal;

		if (mapParameter.containsKey("vlsrOffset"))
			this.vlsrOffset = mapParameter.get("vlsrOffset").getValue();

		if (mapParameter.containsKey("lineAnalysis")) {
			lineAnalysis = true;
		}
		freqMin = mapParameter.get("freqMin").getValue();
		freqMax = mapParameter.get("freqMax").getValue();

		lineFrequency = mapParameter.get("lineFrequency").getValue();

		if (mapParameter.containsKey("dsb"))
			dsbValue = mapParameter.get("dsb").getValue();

		if (!secondeBand && !lineAnalysis && mapParameter.containsKey("dsbMode"))
			dsbMode = mapParameter.get("dsbMode").getValue() == 1;

		diameter = mapParameter.get("telaper").getValue();
		thresEupMax = mapParameter.get("thresEupMax").getValue();
		thresEupMin = mapParameter.get("thresEupMin").getValue();
		thresAijMin = mapParameter.get("thresAijMin").getValue();
		thresAijMax = mapParameter.get("thresAijMax").getValue();

		if (mapParameter.containsKey("quantumNumbers") &&
				mapParameter.get("quantumNumbers") != null) {
					@SuppressWarnings("unchecked")
					ArrayList<String> arrayList = (ArrayList<String>) ((GenericParameterDescription) mapParameter
							.get("quantumNumbers")).getGenericValue();
					quantumNumbersArray = arrayList;
		}

		if (mapParameter.containsKey("tbg"))
			tbg = mapParameter.get("tbg").getValue();
	}

	/**
	 * Check and return if there is lines.
	 *
	 * @return true if there is lines, false otherwise.
	 * @throws UnknowMoleculeException if an used molecule is unknow in the database.
	 * @throws RadexException if there is a radex error.
	 */
	public boolean getLines() throws UnknowMoleculeException, RadexException {
		int numCompEmmi = 0;
		boolean haveLines = false;

		for (ComponentSpecies comp : listComponents) {

			if (ProgressDialogConstants.workerInterrupted) {
				break;
			}

			for (MoleculeDescription mol : comp.getMoleculeList()) {
				if (ProgressDialogConstants.workerInterrupted) {
					break;
				}

				List<LineDescription> listOfLines = getListofLines(freqMin, freqMax, comp, mol);

				if (listOfLines.isEmpty()) {
					LOGGER.debug("No transition found in the molecule {} for the component {}",
							mol.getName(), comp.getComponentName());
					continue;
				}

				if (LOGGER.isDebugEnabled()) {
					for (LineDescription line : listOfLines) {
						LOGGER.debug("Line in synthetic model : {}", line.getIdentification());
					}
				}

				calcTauFreqsByLine(listOfLines, freqs);

				for (LineDescription line : listOfLines) {
					line.setFromCompo(numCompEmmi - 1);
				}

				for (int f = 0; f < listOfLines.size(); f++)
					comp.addALine(listOfLines.get(f));

				haveLines = true;
			}
			numCompEmmi++;
		}
		return haveLines;
	}

	private List<LineDescription>  getListofLines(double freqMinComp, double freqMaxComp, ComponentSpecies component,
			MoleculeDescription mol) throws UnknowMoleculeException, RadexException {
		List<LineDescription> listOfLines = new ArrayList<>();
		int lteRadexMode = getLteRadexMode(component);

		boolean radexPossible = (lteRadexMode == 4 || lteRadexMode == 5) &&
				(Software.getRadexMoleculesManager().isRADEXMolecules(Integer.toString(mol.getTag())) ||
						!Molecule.NO_COLLISION.equals(mol.getCollision()));

		boolean ltePossible = lteRadexMode == 1 || lteRadexMode == 2 || lteRadexMode == 4;

		// Select molecule in the data base
		DataBaseConnection db = AccessDataBase.getDataBaseConnection();
		MoleculeDescriptionDB molecule = db.getMoleculeDescriptionDB(new SimpleMoleculeDescriptionDB(mol.getTag(), mol.getName()));

		LOGGER.debug("TIME Molecules selected: {}", System.currentTimeMillis());

		// Select transition
		List<LineDescriptionDB> lines = db.getLineDescriptionDB(molecule, freqMinComp, freqMaxComp
				,thresEupMin, thresEupMax, thresAijMin, thresAijMax);
		UtilDatabase.removeLineDBOverQuantumFilter(lines, quantumNumbersArray);
		if (lines.isEmpty())
			return listOfLines;

		double colDensity = mol.getDensity();
		double fwhm = mol.getVelocityDispersion();
		if (fwhm == 0)
			fwhm = 0.001;

		int geometry = getGeometry(component);

		List<RadexOutputModel> radexParamLines = null;

		if (!radexPossible) {
			if (lteRadexMode == 4)
				LOGGER.info(" => Impossible to process RADEX Model whith {} -> RADEX Model processing",
						molecule.getName());
			else if (lteRadexMode == 5)
				LOGGER.info(" => No collision file for {}. RADEX Model cannot be computed,"
						+ " no lines will be drawn for this species", molecule.getName());
		} else {
			RadexModel radexModel = new RadexModel();
			radexPossible = radexModel.compute(mol, freqMinComp / 1000, freqMaxComp / 1000,
					tbg, fwhm, geometry);
			radexParamLines = radexModel.getResult();
		}

		if (radexPossible || ltePossible) {
			boolean getRadexResult = false;;
			for (LineDescriptionDB line : lines) {
				double fMHzTrans = line.getFrequency();

				String qn = line.getQuanticNumbers();

				int radexParamIndic = -1;
				if (radexPossible) {
					radexParamIndic = RadexModel.getRADEXParamIndic(radexParamLines, qn);
				}

				final boolean haveRadexResult = radexPossible && radexParamIndic != -1;
				if (haveRadexResult) {
					getRadexResult = true;
				}
				if (haveRadexResult || ltePossible) {
					double tex = 0;
					if (haveRadexResult) {
						tex = radexParamLines.get(radexParamIndic).getTex();
					} else {
						tex = mol.getTemperature();
					}
					// --- Computation of the partition function
					double qt = molecule.buildQt(tex);

					double aTrans = line.getAint();
					double elowTrans = line.getElow();
					int iguTrans = line.getIgu();

					double eLowJ = Formula.calcELowJ(elowTrans, 0);
					double eUpJ = Formula.calcEUpJ(eLowJ, fMHzTrans);
					double nUp = Formula.calcNUp(colDensity, iguTrans, qt, eUpJ, tex);

					double tau0 = Formula.calcTau0(nUp, aTrans, fMHzTrans, fwhm, tex);

					if (haveRadexResult) {
						tau0 = radexParamLines.get(radexParamIndic).getTaul();
					}

					double thetaSource = mol.getSourceSize();

					// --- eUpK : high energy state [K] (Formula
					// E[K]=E[J]/k)
					double eUpK = Formula.calcEUpk(eUpJ);

					LineDescription newLine = addLine(component, tex, fwhm, tau0, molecule.getTag(), molecule.getName(),
							haveRadexResult, qn,fMHzTrans, line.getError(),
							aTrans, iguTrans, eUpK, thetaSource, diameter);
					newLine.setCitation(line.getCitation());
					if (radexPossible && radexParamIndic != -1) {
						newLine.setIdentification(newLine.getIdentification() + '\n' +
								"Collision file: " + RadexModel.getMoleFileFromCollision(mol.getCollision()));
					}
					if (line.getOtherFreqs() != null && !line.getOtherFreqs().isEmpty()) {
						newLine.setIdentification(newLine.getIdentification() + '\n' +
								"Other frequencies proposed: " + line.getOtherFreqs());
					}
					listOfLines.add(newLine);
				}
			}
			if (!getRadexResult && !ltePossible && listOfLines.isEmpty()) {
				throw new RadexException("No line find during the RADEX computation,\n"
						+ "Please verify your collision files");
			}
		}
		return listOfLines;
	}

	private int getGeometry(ComponentSpecies emmiComp) {
		// --- Line parameters specific to the Radex method.
		int geometry = 0;

		// Added par ThachTN pour le RADEX
		if (emmiComp.isSphereMode())
			geometry = 0; // Sphere mode
		else if (emmiComp.isSlabMode())
			geometry = 1; // SlabMode
		else if (emmiComp.isExpandingSphereMode())
			geometry = 2; // Expanding Sphere Mode
		return geometry;
	}

	private int getLteRadexMode(ComponentSpecies emmiComp) {
		int lteRadexMode = 0;
		if (emmiComp.isFullLteMode())
			lteRadexMode = 1;
		// Added par ThachTN pour le RADEX
		else if (emmiComp.isLteRadexMode())
			lteRadexMode = 4;
		else if (emmiComp.isFullRadexMode())
			lteRadexMode = 5;
		return lteRadexMode;
	}

	private LineDescription addLine(ComponentSpecies comp, double tex, double vexp, double tau0, int molTag,
			String molName, boolean radexPossible, String qn, double fMHzTrans, double errTrans, double aTrans,
			int iguTrans, double eUpK, double thetaSource, double diameter) {
		String identification = LineIdentificationUtils.createAstroModelIdentification(
				molName, qn, molTag, fMHzTrans, errTrans, eUpK, aTrans,
				comp.getComponentName(), comp.getVlsr(), tau0, radexPossible,
				tex, dsbMode, secondeBand, dsbValue, iguTrans);

		LineDescription line = null;
		// --- aijOnNu2 : criterium of the intrinseque line intensity [s]
		double aijOnNu2 = Formula.calcAijOnNu2(aTrans, iguTrans, fMHzTrans);

		line = new LineDescription(fMHzTrans, errTrans, vexp, tau0, null, identification,
				aijOnNu2, eUpK,  iguTrans, aTrans);

		line.setQuanticN(qn);
		line.setMolName(molName);
		line.setMolTag(molTag);
		line.setTex(tex);
		line.setThetaSource(thetaSource);
		line.setDiameter(diameter);

		double vlsr0 = Formula.CKm * (1 - line.getObsFrequency() / lineFrequency);
		double newvlsr = comp.getVlsr() + vlsr0;
		LOGGER.debug("vlsr :{}, vlsr0: {}, vlsrLine: {} for frequency {}",
				comp.getVlsr(),  vlsr0, newvlsr, line.getObsFrequency());
		line.setVlsr(comp.getVlsr());
		line.setVlsrData(vlsrOffset);
		line.setDoubleSideBand(secondeBand);
		line.setTau(tau0);
		return line;
	}

	/**
	 * Compute a synthetic spectrum as a list of channels (ArrayList of
	 * ChannelDescription) from a list of lines as an ArrayList of
	 * LineDescription. In this case, frequency sampling is constant (dfreq).
	 *
	 * @param listOfLines
	 *            an ArrayList of LineDescription
	 * @param freqs
	 *            array of frequency to compute for the synthetic spectrum [MHz]
	 *
	 * @return listOfChannels an ArrayList of ChannelDescription
	 */
	public double[] calcTauFreqsByLine(List<LineDescription> listOfLines, double[] freqs) {
		// conversion factors to transform a FWHM into the shape sigma parameter


		// compute the spectrum
		for (LineDescription line : listOfLines) {
			// observed central frequency
			double lineFreq = Formula.calcFreqWithVlsr(line.getObsFrequency(), line.getVlsr()- line.getVlsrData(),line.getObsFrequency());
			line.setFreqCompute(lineFreq);

		}
		return freqs;
	}

}
