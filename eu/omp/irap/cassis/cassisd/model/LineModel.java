/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.cassisd.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.CassisMultipleSpectrum;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.Formula;
import eu.omp.irap.cassis.common.GenericParameterDescription;
import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.common.ParameterDescription;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.database.access.AccessDataBase;
import eu.omp.irap.cassis.database.access.DataBaseConnection;
import eu.omp.irap.cassis.database.access.LineDescriptionDB;
import eu.omp.irap.cassis.database.access.MoleculeDescriptionDB;
import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB;
import eu.omp.irap.cassis.database.access.UnknowMoleculeException;
import eu.omp.irap.cassis.gui.model.parameter.UtilDatabase;
import eu.omp.irap.cassis.lineanalysis.LineAnalysisParameters;
import eu.omp.irap.cassis.parameters.LineIdentificationUtils;

/**
 * @author glorian
 * @since 17 avril 2008
 */
public class LineModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(LineModel.class);

	private CassisSpectrum cassisSpectrum = null;

	private boolean frequencyLimit = false;

	private double freqMin = 0;
	private double freqMax = 0;

	private double thresEupMax = 0.;
	private double thresEupMin = 0.;
	private double thresAijMin = 0.;
	private double thresAijMax = Double.MAX_VALUE;

	private double bandwidth = 60;
	private UNIT bandwidthUnit = UNIT.KM_SEC_MOINS_1;


	private List<String> quantumNumbersArray;
	private int warning = 0;

	private List<MoleculeDescription> mols;

	/**
	 * Constructor makes a new LTEFileModel.
	 *
	 * @param mapParameter The map of parameters.
	 * @param mol The molecule description.
	 */
	public LineModel(Map<String, ParameterDescription> mapParameter, MoleculeDescription mol) {
		mols = new ArrayList<>(1);
		mols.add(mol);

		if (mapParameter.containsKey("freqMin"))
			freqMin = mapParameter.get("freqMin").getValue();

		if (mapParameter.containsKey("freqMax"))
			freqMax = mapParameter.get("freqMax").getValue();

		frequencyLimit = freqMin != freqMax;

		thresEupMax = mapParameter.get("thresEupMax").getValue();
		thresEupMin = mapParameter.get("thresEupMin").getValue();
		thresAijMin = mapParameter.get("thresAijMin").getValue();
		thresAijMax = mapParameter.get("thresAijMax").getValue();

		if (mapParameter.containsKey("bandwidth"))
			bandwidth = mapParameter.get("bandwidth").getValue();
		if (mapParameter.containsKey("bandwidthUnit"))
			bandwidthUnit = UNIT.valueOf(mapParameter.get("bandwidthUnit").getVarName());

		if (mapParameter.containsKey("quantumNumbers")
				&& mapParameter.get("quantumNumbers") != null) {
			@SuppressWarnings("unchecked")
			ArrayList<String> arrayList = (ArrayList<String>) ((GenericParameterDescription) mapParameter
					.get("quantumNumbers")).getGenericValue();
			quantumNumbersArray = arrayList;
		}
	}

	public LineModel(LineAnalysisParameters parameters) {
		this(parameters.getMapParameter(), null);
		mols = parameters.getMols();
	}

	/**
	 * Create and return the spectrum list.
	 *
	 * @param cassisSpectrum The cassis spectrum.
	 * @return the spectrum list.
	 * @throws UnknowMoleculeException if a molecule is unknown in the current database.
	 */
	public List<CommentedSpectrum> createSpectrumList(CassisSpectrum cassisSpectrum)
			throws UnknowMoleculeException {
		return createSpectrumListMultiMols(cassisSpectrum);
	}

	/**
	 * Create and return the spectrum list.
	 *
	 * @param cassisSpectrum The cassis spectrum.
	 * @return the spectrum list.
	 * @throws UnknowMoleculeException if a molecule is unknown in the current database.
	 */
	public List<CommentedSpectrum> createSpectrumListMultiMols(CassisSpectrum cassisSpectrum)
			throws UnknowMoleculeException {
		LOGGER.info("Begin of create fileSpectrum");
		// ---------------- DECLARATION ------------------------------

		List<CommentedSpectrum> spectrumList = new ArrayList<>();
		List<LineDescription> fileListOfLines = new ArrayList<>();
		String fileTitle;
		// ---------------- FIN DECLARAION -------------------------------------

		this.cassisSpectrum = cassisSpectrum;
		calculeFrequenceMinMaxFromSpectrum();
		// Select molecule in the data base
		DataBaseConnection db = AccessDataBase.getDataBaseConnection();

		for (MoleculeDescription currentMol : mols) {
			MoleculeDescriptionDB molecule = db.getMoleculeDescriptionDB(
					new SimpleMoleculeDescriptionDB(
							currentMol.getTag(), currentMol.getName()));

			int molTag = molecule.getTag();
			String molName = molecule.getName();

			LOGGER.debug("LineModel on {}", molName);

			// Select all transition for this molecule
			List<LineDescriptionDB> lines = db.getLineDescriptionDB(molecule, freqMin, freqMax,
					thresEupMin, thresEupMax, thresAijMin, thresAijMax);

			for (LineDescriptionDB line : lines) {
				double fMHzTrans = line.getFrequency();
				double eUpK = Formula.calcEUpk(line.getElow(), 0, fMHzTrans);

				double vlsr = this.cassisSpectrum.getVlsr();
				LineDescription lineDes = addLine(vlsr, molTag, molName, line.getQuanticNumbers(),
						fMHzTrans, line.getError(), line.getAint(), line.getIgu(), eUpK);

				lineDes.setCitation(line.getCitation());

				if (line.getOtherFreqs() != null && !line.getOtherFreqs().isEmpty()) {
					lineDes.setIdentification(lineDes.getIdentification() + '\n' +
							"Other frequencies proposed: " + line.getOtherFreqs());
				}

				fileListOfLines.add(lineDes);
			}

			UtilDatabase.removeLineOverQuantumFilter(fileListOfLines, quantumNumbersArray);
		}

		fileListOfLines = LinesDescription.sortLines(fileListOfLines);

		fileTitle = this.cassisSpectrum.getTitle();
		XAxisCassis xAxisCassis = XAxisCassis.getXAxisCassis(bandwidthUnit);

		for (LineDescription lineDescription : fileListOfLines) {
			double freqMinSignal = 0;
			double freqMaxSignal = 0;
			double freqCentral = 0;
			int indice = 0;

			double vlsrLine = lineDescription.getVlsr();// -- Get vlsr

			freqCentral = lineDescription.getObsFrequency();
			indice = this.cassisSpectrum.getFrequencySignalIndex(freqCentral);

			// // Select frequency in the range

			// --- Set frequency bound frequencies
			// --- Set frequency bound indexes in the observer spectrum
			int kfreqMin = 0;
			int kfreqMax = 0;
			if (UNIT.KM_SEC_MOINS_1.equals(bandwidthUnit)) {
				freqMinSignal = Formula.calcSpectrumFreqMin(freqCentral, bandwidth);
				freqMaxSignal = Formula.calcSpectrumFreqMax(freqCentral, bandwidth);
			} else {
				Double freqcentralInCurrentAxis = xAxisCassis.convertFromMhzFreq(freqCentral);
				if (xAxisCassis.isInverted()){
					freqMinSignal = xAxisCassis.convertToMHzFreq(freqcentralInCurrentAxis+bandwidth/2);
					freqMaxSignal = xAxisCassis.convertToMHzFreq(freqcentralInCurrentAxis-bandwidth/2);
				} else {
					freqMaxSignal = xAxisCassis.convertToMHzFreq(freqcentralInCurrentAxis+bandwidth/2);
					freqMinSignal = xAxisCassis.convertToMHzFreq(freqcentralInCurrentAxis-bandwidth/2);

				}
			}

			kfreqMin = this.cassisSpectrum.getFrequencySignalIndex(freqMinSignal);
			kfreqMax = this.cassisSpectrum.getFrequencySignalIndex(freqMaxSignal);

			if (Math.abs(kfreqMax - kfreqMin) < 5 || indice < kfreqMin || indice > kfreqMax) {
				warning++;
				continue;
			}
//			double freqCentralPlot = freqMinSignal  + (freqMaxSignal - freqMinSignal) /2.0;
//
//			//line not in freqMin and freqMax
//			if (freqCentralPlot < this.cassisSpectrum.getSignalFrequency(kfreqMin) ||
//					freqCentralPlot > this.cassisSpectrum.getSignalFrequency(kfreqMax)	)
//				continue;

			int deb = kfreqMin;
			int fin = kfreqMax;

			if (deb > fin) {
				int temp = fin;
				fin = deb;
				deb = temp;
			}

			double minFrequency = Double.MAX_VALUE;
			double maxFrequency = Double.NEGATIVE_INFINITY;
			double signalFrequency = 0;

			double[] freq = new double[fin - deb + 1];
			double[] intensity = new double[fin - deb + 1];
			for (int k = deb; k <= fin; k++) {
				freq[k - deb] = this.cassisSpectrum.getSignalFrequency(k);
				signalFrequency = this.cassisSpectrum.getSignalFrequency(k);
				maxFrequency = Math.max(maxFrequency, signalFrequency);
				minFrequency = Math.min(minFrequency, signalFrequency);
				intensity[k - deb] = this.cassisSpectrum.getValue(k);
			}

			double spectrumFreqMin = 0;
			double spectrumFreqMax = 0;

			spectrumFreqMin = freqMinSignal;
			spectrumFreqMax = freqMaxSignal;

			/*--------------------------------------------------**/
			CommentedSpectrum spectrum;
			spectrum = createCommentedSpectrum(lineDescription, freq, intensity, fileTitle,
					fileListOfLines, spectrumFreqMin, spectrumFreqMax, freqCentral, vlsrLine);
			spectrum.setTypeFreq(cassisSpectrum.getTypeFrequency());
			double loFrequency = cassisSpectrum.getLoFrequency(freqCentral);

			if (cassisSpectrum instanceof CassisMultipleSpectrum
					&& ((CassisMultipleSpectrum) cassisSpectrum).isMultiple()) {
				double firstFreq = cassisSpectrum.getLoFrequency(freq[0]);
				for (int i = 1; i < freq.length; i++) {
					if (Math.abs(cassisSpectrum.getLoFrequency(freq[i]) - firstFreq) > 1e-6) {
						loFrequency = 0;
						break;
					}
				}
			}

			spectrum.setLoFreq(loFrequency);
			spectrum.setxAxisOrigin(cassisSpectrum.getxAxisOrigin());
			spectrumList.add(spectrum);
		}
		LOGGER.info("End of create fileSpectrum");
		return spectrumList;
	}

	private LineDescription addLine(double vlsr, int molTag, String molName, String qn,
			double fMHzTrans, double errTrans, double aTrans, int iguTrans, double eUpK) {
		double aijOnNu2 = Formula.calcAijOnNu2(aTrans, iguTrans, fMHzTrans);

		String id = LineIdentificationUtils.createLineModelIdentification(
				molName, qn, molTag, fMHzTrans, errTrans, eUpK, aTrans, iguTrans);
		// Create Line description of the transition and add to lteListOLines
		LineDescription lteLine = new LineDescription(fMHzTrans, errTrans, id,
				aijOnNu2, eUpK, iguTrans, aTrans, vlsr, molName, molTag, qn);
		lteLine.setVlsrData(vlsr);

		return lteLine;
	}

	private CommentedSpectrum createCommentedSpectrum(LineDescription lteLine, double[] newFreq,
			double[] newIntensity, String fileTitle, List<LineDescription> fileListOfLines,
			double spectrumFreqMin, double spectrumFreqMax, double freqCentral, double vlsrLine) {
		CommentedSpectrum fileSpectrum;

		// -- only send this unique line in this commented spectrum
		List<LineDescription> uniqueLine = new ArrayList<>();
		uniqueLine.add(lteLine);

		for (LineDescription line : fileListOfLines) {
			if (line != lteLine) {
				double freq = line.getObsFrequency();
				if ((freq >= spectrumFreqMin && freq <= spectrumFreqMax)
						|| (freq <= spectrumFreqMin && freq >= spectrumFreqMax))
					uniqueLine.add(line);
			}
		}

		fileSpectrum = new CommentedSpectrum(uniqueLine, newFreq, newIntensity, fileTitle);
		fileSpectrum.setFreqRef(freqCentral);
		fileSpectrum.setVlsr(vlsrLine);

		return fileSpectrum;
	}

	private void calculeFrequenceMinMaxFromSpectrum() {
		double freqMinData = 0;
		double freqMaxData = 0;

		freqMinData = this.cassisSpectrum.getSignalFrequency(0);
		freqMaxData = this.cassisSpectrum
				.getSignalFrequency(this.cassisSpectrum.getNbChannel() - 1);

		if (!frequencyLimit) {
			freqMin = freqMinData;
			freqMax = freqMaxData;
		} else {
			if (UNIT.KM_SEC_MOINS_1.equals(bandwidthUnit)) {
				freqMin = Formula.calcSpectrumFreqMin(freqMin, bandwidth);
				freqMax = Formula.calcSpectrumFreqMax(freqMax, bandwidth);
			} else {
				freqMin = freqMin - bandwidth / 2;
				freqMax = freqMax + bandwidth / 2;
			}

			if (freqMin < freqMinData)
				freqMin = freqMinData;
			if (freqMax > freqMaxData)
				freqMax = freqMaxData;
		}
	}

	public int getWarning() {
		return warning;
	}
}
