/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.cassisd.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.CassisDecimalFormat;
import eu.omp.irap.cassis.common.Formula;
import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.common.TypeFrequency;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.XAxisCassisException;
import eu.omp.irap.cassis.common.axes.XAxisFrequency;
import eu.omp.irap.cassis.database.access.AccessDataBase;
import eu.omp.irap.cassis.database.access.DataBaseConnection;
import eu.omp.irap.cassis.database.access.LineDescriptionDB;
import eu.omp.irap.cassis.database.access.MoleculeDescriptionDB;
import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB;
import eu.omp.irap.cassis.database.access.VamdcDataBaseConnection;

/**
 * @author glorian
 *
 */
public class OtherSpeciesModel {

	private static String lineIdentificationTemplate = "[[molNameValue]] [[quanticNumberValue]] [[tagLabel]] [[tagValue]]\n"
			+ "[[waveLabel]] [[waveValue]][[waveErrorLabelBegin]][[waveErrorValue]][[waveErrorLabelEnd]][[waveUnit]]\n"
			+ "[[elowLabel]][[elowValue]][[elowUnit]]"+"[[eupLabel]] [[eupValue]] [[eupUnit]]"
			+ " [[aijLabel]] [[aijValue]]"
			+ " [[gupLabel]] [[gupValue]]\n"
			+ "[[shiftWaveLabel]] [[shiftWaveValue]] [[shiftWaveUnit]]"
			+ " [[commentLabel]] [[commentValue]]\n"
			+ " [[citationLabel]] [[citationValue]]\n"
			+ " [[otherWave]]";

	private OtherSpeciesParameters treshold ;
	private double freqMinMhz=0;
	private double freqMaxMhz=0;
	private XAxisCassis xAxisCassis= XAxisCassis.getXAxisCassis(UNIT.MHZ);
	private static final Logger LOGGER = LoggerFactory.getLogger(OtherSpeciesModel.class);


	/**
	 *
	 * @param waveMin
	 * @param waveMax
	 * @param unit
	 * @param treshold
	 * @throws XAxisCassisException
	 */
	public OtherSpeciesModel(double waveMin, double waveMax, UNIT unit,
			OtherSpeciesParameters treshold) throws XAxisCassisException {
		if (UNIT.isVelocity(unit)) {
			throw new XAxisCassisException("Error with velocity axis, You must provide "
					+ "a XAxisCassis object with the referecny Frequency");
		}

		init(waveMin, waveMax, XAxisCassis.getXAxisCassis(unit), treshold);

	}

	/**
	 *
	 * @param waveMin
	 * @param waveMax
	 * @param axis
	 * @param treshold
	 */
	public OtherSpeciesModel(double waveMin, double waveMax, XAxisCassis axis,
			OtherSpeciesParameters treshold) throws XAxisCassisException {
		init(waveMin, waveMax, axis, treshold);
	}

	/**
	 *
	 * @param frequencyMin
	 * @param frequencyMax
	 * @param otherThresEupMin
	 * @param otherThresEupMax
	 * @param otherThresAij
	 * @param otherTreshAijMax
	 * @param vlsrPlot
	 * @param vlsrData
	 * @param modeSignal
	 * @param comment
	 * @param typeFrequency
	 */
	public OtherSpeciesModel(double frequencyMin, double frequencyMax, double otherThresEupMin,
			double otherThresEupMax, double otherThresAij, double otherTreshAijMax, double vlsrPlot,
			double vlsrData,
			boolean modeSignal, String comment, TypeFrequency typeFrequency) {
		init(frequencyMin, frequencyMax, XAxisCassis.getXAxisCassis(UNIT.MHZ),
				new OtherSpeciesParameters(otherThresEupMin, otherThresEupMax, otherThresAij,
				otherTreshAijMax, vlsrPlot, vlsrData, modeSignal,
				comment, typeFrequency));

	}
	/**
	 * @param waveMin
	 * @param waveMax
	 * @param axis
	 * @param treshold
	 */
	private void init(double waveMin, double waveMax, XAxisCassis axis, OtherSpeciesParameters treshold) {
		try {
			this.xAxisCassis = axis.clone();
		} catch (CloneNotSupportedException e) {
			LOGGER.error(e.getMessage());
		}
		this.treshold = treshold;
		double range[] = new double[] {waveMin, waveMax};
		double freqRangeInMhz[] = XAxisCassis.convert(range,xAxisCassis,
				XAxisFrequency.getXAxisFrequency(UNIT.MHZ));
		this.freqMinMhz = freqRangeInMhz[0];
		this.freqMaxMhz = freqRangeInMhz[1];

		double vlsrData = 0;
		if (TypeFrequency.REST.equals(treshold.getTypeFrequency())) {
			vlsrData = treshold.getVlsrData();
		}
		this.freqMinMhz = Formula.calcFreqWithVlsr(freqMinMhz, vlsrData - treshold.getVlsrPlot(), freqMinMhz);
		this.freqMaxMhz = Formula.calcFreqWithVlsr(freqMaxMhz, vlsrData - treshold.getVlsrPlot(), freqMaxMhz);
	}




	public List<LineDescription> createLines(List<MoleculeDescription> mols) {
		List<SimpleMoleculeDescriptionDB> listSimpleMolDB = new ArrayList<>(500);
		DataBaseConnection db = AccessDataBase.getDataBaseConnection();

		if (mols != null) {
			for (MoleculeDescription mol : mols) {
				if (mol.isCompute()) {
					listSimpleMolDB.add(db.getSimpleMolecule(mol.getTag()));
				}
			}
		}

		if (listSimpleMolDB.size() == 1) {
			return getListOfLines(listSimpleMolDB.get(0));
		} else {
			return getListOfLines(listSimpleMolDB);
		}
	}

	private List<LineDescription> getListOfLines(List<SimpleMoleculeDescriptionDB> listSimpleMolDB) {
		DataBaseConnection db = AccessDataBase.getDataBaseConnection();
		List<LineDescriptionDB> lines = db.getLineDescriptionDB(listSimpleMolDB,
				freqMinMhz, freqMaxMhz, treshold.getOtherThresEupMin(),
				treshold.getOtherThresEupMax(), treshold.getOtherThresAijMin(),
				treshold.getOtherTreshAijMax());
		return computeOtherSpecies(lines);
	}

	private List<LineDescription> getListOfLines(SimpleMoleculeDescriptionDB simpleMolDB) {
		DataBaseConnection db = AccessDataBase.getDataBaseConnection();
		List<LineDescriptionDB> lines = db.getLineDescriptionDB(simpleMolDB,
				this.freqMinMhz, this.freqMaxMhz, treshold.getOtherThresEupMin(),
				treshold.getOtherThresEupMax(), treshold.getOtherThresAijMin(),
				treshold.getOtherTreshAijMax());
		return computeOtherSpecies(lines);
	}

	private List<LineDescription> computeOtherSpecies(List<LineDescriptionDB> lines) {
		double fMHzTrans;
		double errTrans;
		double aTrans;
		double eupTrans;
		int iguTrans;
		int molTag;
		String molName;
		double aijOnNu2;

		List<LineDescription> listOfLines = new ArrayList<>(lines.size());
		for (LineDescriptionDB line : lines) {
			fMHzTrans = line.getFrequency();
			errTrans = line.getError();
			aTrans = line.getAint();
			eupTrans = line.getEup();
			iguTrans = line.getIgu();
			String qn = line.getQuanticNumbers();
			molTag = line.getTag();
			molName = AccessDataBase.getDataBaseConnection().getMolName(molTag);

			aijOnNu2 = Formula.calcAijOnNu2(aTrans, iguTrans, fMHzTrans);

			double wave = xAxisCassis.convertFromMhzFreq(fMHzTrans);
			double waveError = xAxisCassis.convertDeltaFromMhzFreq(fMHzTrans-errTrans, fMHzTrans+errTrans);


			HashMap<String, String> map = getParameterValueMapping(aTrans, eupTrans, molTag,
					molName, line, qn, wave,waveError);

			String id = replaceLineIdentificationValue(map);

			LineDescription lineDes = new LineDescription(fMHzTrans, errTrans, 0., 0.,
					null, id, aijOnNu2, eupTrans, iguTrans, aTrans);
			lineDes.setMolTag(molTag);
			lineDes.setVlsr(treshold.getVlsrPlot());
			lineDes.setVlsrData(treshold.getVlsrData());
			lineDes.setQuanticN(qn);
			lineDes.setMolName(molName);

			if (TypeFrequency.REST.equals(treshold.getTypeFrequency()))
				lineDes.setFreqCompute(Formula.calcFreqWithVlsr(fMHzTrans, treshold.getVlsrPlot()-treshold.getVlsrData(), fMHzTrans));
			else
				lineDes.setFreqCompute(Formula.calcFreqWithVlsr(fMHzTrans, treshold.getVlsrPlot(), fMHzTrans));

			lineDes.setDoubleSideBand(!treshold.isModeSignal());

			listOfLines.add(lineDes);
		}
		return listOfLines;
	}

	/**
	 * Create the mapping between the template otherSpecies line identification
	 * and the parameter of the line
	 *
	 * @param aTrans
	 * @param eupTrans
	 * @param molTag
	 * @param molName
	 * @param line
	 * @param qn
	 * @param wave
	 * @param waveError
	 * @return the mapping
	 */
	private HashMap<String, String> getParameterValueMapping(double aTrans, double eupTrans, int molTag, String molName,
			LineDescriptionDB line, String qn, double wave, double waveError) {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("molNameValue", molName);
		map.put("quanticNumberValue", MoleculeDescriptionDB.constitueLine(qn));
		map.put("tagLabel", "Tag =");
		map.put("tagValue", String.valueOf(molTag));
		map.put("waveLabel", xAxisCassis.getAxis().getSymbol()+" =");
		map.put("waveValue", CassisDecimalFormat.FREQUENCY_FORMAT.format(wave));
		map.put("waveErrorLabelBegin", "(±");
		map.put("waveErrorValue", CassisDecimalFormat.FREQUENCY_FORMAT.format(waveError));
		map.put("waveUnit", xAxisCassis.getUnit().getValString());
		map.put("waveErrorLabelEnd", ") ");
//			map.put("elowLabel", );
//			map.put("elowValue", );
//			map.put("elowUnit", );
		map.put("eupLabel", "Eup =");
		map.put("eupValue", CassisDecimalFormat.EUP_FORMAT.format(eupTrans));
		map.put("eupUnit", "K");
		map.put("aijLabel", "Aij =");
		map.put("aijValue", CassisDecimalFormat.AIJ_FORMAT.format(aTrans));
		map.put("aijUnit", "");
		map.put("gupLabel", "Gup =");
		map.put("gupValue", String.valueOf(line.getIgu()));
		map.put("gupUnit", "");
		map.put("shiftWaveLabel", "Vlsr =");
		map.put("shiftWaveValue", CassisDecimalFormat.TWO_DEC_FORMAT.format(treshold.getVlsrPlot()));
		map.put("shiftWaveUnit",  "km/s");
		if (this.treshold.getComment() != null && !this.treshold.getComment().isEmpty()) {
			map.put("commentLabel", "   ");
			map.put("commentValue", this.treshold.getComment());
		}
		if (line.getCitation() != null && !line.getCitation().isEmpty()) {
			map.put("citationLabel", VamdcDataBaseConnection.VAMDC_REQUEST_TOKEN+ "=");
			map.put("citationValue", line.getCitation());

		}
		if (line.getOtherFreqs() != null &&  !line.getOtherFreqs().isEmpty()) {
			map.put("otherWave", "Other frequencies proposed: " + line.getOtherFreqs());
		}
		return map;
	}


	/**
	 * Replace the template of OtherSpecies line identification
	 *
	 */
	public static void setLineIdentificationTemplate(String template) {
		lineIdentificationTemplate = template;
	}

	/**
	 * Get the template of OtherSpecies line identification
	 *
	 * @return the string of OtherSpecies line identification
	 */
	public static String getLineIdentificationTemplate() {
		return lineIdentificationTemplate;
	}

	/**
	 *
	 * @param map
	 * @return
	 */
	private static String replaceLineIdentificationValue(HashMap<String, String> map) {
		String res = lineIdentificationTemplate;
		for (String key : map.keySet()) {
			res= res.replaceFirst("\\[\\[" + key + "\\]\\]", map.get(key));
		}
		res = res.replaceAll("(\\[\\[)(.*?)(\\]\\])", "");
		return res.trim();
	}


	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "OtherSpeciesModel [frequencyMin=" + this.freqMinMhz + ", frequencyMax="
				+ this.freqMaxMhz + ", otherThresEupMin=" + treshold.getOtherThresEupMin() + ", otherThresEupMax=" + treshold.getOtherThresEupMax()
				+ ", otherThresAijMin=" + treshold.getOtherThresAijMin() + ", otherTreshAijMax=" + treshold.getOtherTreshAijMax() + ", vlsr="
				+ treshold.getVlsrPlot() + ", modeSignal=" + treshold.isModeSignal() + ", comment=" + treshold.getComment() + "]";
	}
}
