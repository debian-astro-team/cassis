/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.cassisd.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.CassisException;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.CommentedSpectrum;

/**
 * This model reads a spectrum file on the disk.
 */
public class FileModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(FileModel.class);
	private Double frequencyMin = 0.;
	private Double frequencyMax = 0.;


	/**
	 * Constructor.
	 *
	 * @param freqMin The minimum frequency.
	 * @param freqMax The maximum frequency.
	 */
	public FileModel(Double freqMin, Double freqMax) {
		frequencyMin = freqMin;
		frequencyMax = freqMax;
	}

	public CommentedSpectrum[] createSpectrum(CassisSpectrum cassisSpectrum) throws CassisException {
		CommentedSpectrum[] spectrumList = new CommentedSpectrum[2];
		spectrumList[0] = null;
		spectrumList[1] = null;
		boolean containsData = false;

		if (cassisSpectrum == null)
			throw new CassisException("Impossible to read the file");

		int nbChannels = cassisSpectrum.getNbChannel();

		boolean frequencyLimit = frequencyMin != 0 || frequencyMax != 0;
		if (!frequencyLimit) {
			frequencyMin = Double.NEGATIVE_INFINITY;
			frequencyMax = Double.MAX_VALUE;
		}

		containsData = cassisSpectrum.computeCommentedSpectrum(frequencyMin, frequencyMax, spectrumList);

		// no data in the limits
		if (!containsData)
			throw new CassisException("Frequency limits out of data file frequency range.");

		LOGGER.info("The spectrum has {} channels; displaying: {}", nbChannels, spectrumList[0].getSize());

		return spectrumList;
	}

}
