/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.common;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 * Class CassisDecimalFormat: definir les decimalformat dans Cassis
 *
 * @author thachtn
 *
 */
public class CassisDecimalFormat {
	private static final DecimalFormatSymbols formatSymbol = new DecimalFormatSymbols(Locale.US);

	/* frequency format */
	public static final DecimalFormat FREQUENCY_FORMAT = new DecimalFormat("0.00##", formatSymbol);

	/** frequency format in GHZ */
	public static final DecimalFormat FREQUENCY_GHZ_FORMAT = new DecimalFormat("0.####", formatSymbol);

	/* Aij format */
	public static final DecimalFormat AIJ_FORMAT = new DecimalFormat("0.00E0", formatSymbol);

	/* tau format */
	public static final DecimalFormat TAU_FORMAT = new DecimalFormat("0.000E0", formatSymbol);

	/* vlsr format in infopanel */
	public static final DecimalFormat VLSR_FORMAT = new DecimalFormat("0.###", formatSymbol);

	/* eup format in line identification */
	public static final DecimalFormat EUP_FORMAT = new DecimalFormat("0.00", formatSymbol);


	public static final DecimalFormatSymbols FORMAT_SYMBOL = new DecimalFormatSymbols(Locale.US);
	public static final DecimalFormat TWO_DEC_FORMAT = new DecimalFormat("0.00", FORMAT_SYMBOL);
	public static final DecimalFormat TWO_DEC_EXP_FORMAT = new DecimalFormat("0.00E0", FORMAT_SYMBOL);
	public static final DecimalFormat TWO_POS_DEC_FORMAT = new DecimalFormat("0.##", FORMAT_SYMBOL);


	/**
	 * Construct a decimal format with the same number after the dot
	 * of another number as a String.
	 *
	 * @param number The number as a String
	 * @return the {@link DecimalFormat}.
	 */
	public static DecimalFormat getFormatForNumber(String number) {
		StringBuilder sb = new StringBuilder("0.");
		if (number == null || number.isEmpty()) {
			sb.append('#');
		} else {
			int tmpNum = number.indexOf('.');
			if (tmpNum == -1) {
				sb.append('#');
			} else {
				tmpNum = (number.length() - 1) - tmpNum;
				while (tmpNum > 0) {
					sb.append('0');
					tmpNum--;
				}
			}
		}
		return new DecimalFormat(sb.toString(), formatSymbol);
	}
}
