/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.common;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Class representing a spectrum channel characterization.
 */
public class ChannelDescription {

	public static boolean nbChannelLimited; // limit number of
													// nbChannels
	public static final int NB_CHANNEL_LIMIT_SIZE = 100000; // How many channels
															// max?
	public static final int NB_CHANNEL_LIMIT_LOG = 10000; // How many channels
															// max?

	private double frequencySignal, dFrequencySignal; // Mhz
	private double frequencyImage, dFrequencyImage; // Mhz
	private double intensity, dIntensity; // dTa (K)

	private static final Logger LOGGER = Logger.getLogger("ChannelDescription");


	/**
	 * Constructor makes a ChannelDescription with the given parameters.
	 *
	 * @param frequency The frequency in MHz.
	 * @param dFrequency The dFrequency in MHz.
	 * @param intensity The intensity.
	 * @param dIntensity The dIntensity.
	 */
	public ChannelDescription(double frequency, double dFrequency, double intensity, double dIntensity) {
		super();

		this.frequencySignal = frequency;
		this.dFrequencySignal = dFrequency;
		this.intensity = intensity;
		this.dIntensity = dIntensity;
	}

	/**
	 * Constructor makes a ChannelDescription with the given parameters.
	 *
	 * @param frequency The frequency in MHz.
	 * @param dFrequency The dFrequency.
	 * @param intensity The intensity.
	 */
	public ChannelDescription(double frequency, double dFrequency, double intensity) {
		super();
		this.frequencySignal = frequency;
		this.dFrequencySignal = dFrequency;
		this.intensity = intensity;
	}

	/**
	 * Remove empty elements from channel list to compress it.
	 *
	 * @param channelList Channel list to compress
	 * @return compressed channel list
	 */
	public static List<ChannelDescription> compressChannelList(List<ChannelDescription> channelList) {
		// Remove empty channels
		Iterator<ChannelDescription> it = channelList.iterator();
		ChannelDescription currentChannel, nextChannel;
		double currentIntensity, previousIntensity;
		List<ChannelDescription> flatListOfChannel = new ArrayList<ChannelDescription>();

		// Don't count first channel
		if (it.hasNext()) {
			currentChannel = it.next();
			previousIntensity = currentChannel.getIntensity();
			// Keep first element
			flatListOfChannel.add(currentChannel);

			if (it.hasNext()) {
				currentChannel = it.next();
				currentIntensity = currentChannel.getIntensity();

				while (it.hasNext()) {
					nextChannel = it.next();
					double nextIntensity = nextChannel.getIntensity();
					if (currentIntensity != previousIntensity || currentIntensity != nextIntensity)
						flatListOfChannel.add(currentChannel);

					previousIntensity = currentIntensity;
					currentIntensity = nextIntensity;
					currentChannel = nextChannel;
				}
				// Keep last element
				flatListOfChannel.add(currentChannel);
			}
		}
		return flatListOfChannel;
	}

	/**
	 * Return the frequency.
	 *
	 * @return frequency
	 */
	public double getFrequencySignal() {
		return frequencySignal;
	}

	/**
	 * Return dFrequency.
	 *
	 * @return dFrequency
	 */
	public double getDFrequencySignal() {
		return dFrequencySignal;
	}

	/**
	 * Return the images frequencies.
	 *
	 * @return the images frequencies.
	 */
	public double getFrequencyImage() {
		return frequencyImage;
	}

	/**
	 * Change the images frequencies.
	 *
	 * @param frequencyIm the images frequencies.
	 */
	public void setFrequencyImage(double frequencyIm) {
		this.frequencyImage = frequencyIm;
	}

	/**
	 * Return the dFrequency Image.
	 *
	 * @return dFrequency_im The dFrequency image.
	 */
	public double getDFrequencyImage() {
		return dFrequencyImage;
	}

	/**
	 * Change the dFrequency image
	 *
	 * @param dFrequencyIm The dFrequency image.
	 */
	public void setDFrequencyImage(double dFrequencyIm) {
		this.dFrequencyImage = dFrequencyIm;
	}

	/**
	 * Return intensity.
	 *
	 * @return intensity
	 */
	public double getIntensity() {
		return intensity;
	}

	/**
	 * Change intensity.
	 *
	 * @param intensity The intensity to set.
	 */
	public void setIntensity(double intensity) {
		this.intensity = intensity;
	}

	/**
	 * Return dIntensity.
	 *
	 * @return dIntensity
	 */
	public double getDIntensity() {
		return dIntensity;
	}

	/**
	 * Change dIntensity.
	 *
	 * @param dIntensity The dIntensity to set.
	 */
	public void setDIntensity(final double dIntensity) {
		this.dIntensity = dIntensity;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "(" + frequencySignal + "," + intensity + ")";
	}

	/**
	 * Add noise to a list of channels.
	 *
	 * @param listOfChannels The list of channels.
	 * @param noise The noise to add.
	 */
	public static void addNoise(List<ChannelDescription> listOfChannels, final double noise) {
		try {
			ChannelDescription chanel;
			double noiseChannel;
			if (noise != 0.0) {
				for (int k = 0; k < listOfChannels.size(); k++) {
					chanel = listOfChannels.get(k);
					// noise K
					noiseChannel = Formula.calcWhiteNoise(noise / 1000);
					chanel.setIntensity(chanel.getIntensity() + noiseChannel);
				}
			}
		} catch (Exception e) {
			LOGGER.log(Level.FINER, "Error Noise: " + e);
		}
	}

	/**
	 * Add noise to a list of channels.
	 *
	 * @param listOfChannels The list of channels.
	 * @param noise  The noise to add.
	 */
	public static void addNoise(double[] listOfChannels, final double noise) {
		try {
			if (noise != 0.0) {
				for (int k = 0; k < listOfChannels.length; k++) {
					//noise in K
					listOfChannels[k] = listOfChannels[k] + Formula.calcWhiteNoise(noise / 1000);
				}
			}
		} catch (Exception e) {
			LOGGER.log(Level.FINER, "Error Noise: " + e);
		}
	}

	/**
	 * Return if the number of channels is limited.
	 *
	 * @return true if the number of channels is limited, false otherwise.
	 */
	public static boolean isNbChannelLimited() {
		return nbChannelLimited;
	}

	/**
	 * Change the if the channels number are limited.
	 *
	 * @param nbChannelLimited true if the channel number are limited, false otherwise.
	 */
	public static void setNbChannelLimited(boolean nbChannelLimited) {
		ChannelDescription.nbChannelLimited = nbChannelLimited;
	}

	/**
	 * Change the dFrequency.
	 *
	 * @param dFrequency The new dFrequency.
	 */
	public void setDFrequency(double dFrequency) {
		this.dFrequencySignal = dFrequency;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(dFrequencyImage);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(dFrequencySignal);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(dIntensity);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(frequencyImage);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(frequencySignal);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(intensity);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChannelDescription other = (ChannelDescription) obj;
		if (Double.doubleToLongBits(dFrequencyImage) != Double.doubleToLongBits(other.dFrequencyImage))
			return false;
		if (Double.doubleToLongBits(dFrequencySignal) != Double.doubleToLongBits(other.dFrequencySignal))
			return false;
		if (Double.doubleToLongBits(dIntensity) != Double.doubleToLongBits(other.dIntensity))
			return false;
		if (Double.doubleToLongBits(frequencyImage) != Double.doubleToLongBits(other.frequencyImage))
			return false;
		if (Double.doubleToLongBits(frequencySignal) != Double.doubleToLongBits(other.frequencySignal))
			return false;
		if (Double.doubleToLongBits(intensity) != Double.doubleToLongBits(other.intensity))
			return false;
		return true;
	}

}
