/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.common;

import java.util.ArrayList;
import java.util.List;


/**
 * {@link CommentedSpectrum} with the possibility to add a list of {@link ComponentInterface}.
 */
public class CommentedComponentsSpectrum extends CommentedSpectrum {

	private List<ComponentInterface> listComponent;


	/**
	 * Constructs a new empty {@link CommentedComponentsSpectrum}.
	 */
	public CommentedComponentsSpectrum() {
	}

	/**
	 * Constructs a new {@link CommentedComponentsSpectrum} with the given parameters.
	 *
	 * @param listOfLines The list of lines.
	 * @param listOfChannels The list of channels.
	 * @param title The title.
	 */
	public CommentedComponentsSpectrum(List<LineDescription> listOfLines,
			List<ChannelDescription> listOfChannels, String title) {
		super(listOfLines, listOfChannels, title);
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.cassis.common.CommentedSpectrum#getListOfLines()
	 */
	/**
	 * Return the list of lines.
	 *
	 * @return the list of lines
	 */
	@Override
	public List<LineDescription> getListOfLines() {
		List<LineDescription> listLines = new ArrayList<LineDescription>();
		List<ComponentInterface> compoList = listComponent;
		if (compoList != null) {
			for (ComponentInterface compo : compoList) {
				listLines.addAll(compo.getListOfLines());
			}
		}
		return listLines;
	}

	/**
	 * Set a list of components {@link ComponentInterface} (who return lines).
	 *
	 * @param listComponent The list of components.
	 */
	public void setListOfComponent(List<ComponentInterface> listComponent) {
		this.listComponent = listComponent;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	/**
	 * Create a hashCode.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((listComponent == null) ? 0 : listComponent.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see eu.omp.irap.cassis.common.CommentedSpectrum#equals(java.lang.Object)
	 */
	/**
	 * Test the equality with the given object.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;

		if ((obj == null) || (obj.getClass() != this.getClass()))
			return false;

		CommentedComponentsSpectrum comSpec = (CommentedComponentsSpectrum) obj;

		return super.equals(obj) &&

		((this.listComponent == comSpec.listComponent) || ((this.listComponent).equals(comSpec.listComponent)))
				&& (this.vlsr == comSpec.vlsr);
	}
}
