/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.common;

import java.util.List;

/**
 * This class allow us to stock param description.
 *
 * @author girard
 * @since 9 feb 2006
 */
public class ParamArrayDescription extends ParameterDescription {

	private List<Double> parameterList;


	/**
	 * Constructor makes a new standart ParamArrayDescription.
	 */
	public ParamArrayDescription() {
		super();
	}

	/**
	 * Constructor makes a new standart ParamArrayDescription.
	 *
	 * @param list
	 *            list of param
	 */
	public ParamArrayDescription(final List<Double> list) {
		super();
		parameterList = list;
	}

	/**
	 * Constructor makes a new ParamArrayDescription.
	 *
	 * @param name
	 *            name
	 * @param label
	 *            label
	 * @param value
	 *            value
	 * @param list
	 *            list of param
	 */
	public ParamArrayDescription(final String name, final String label, final double value,
			final List<Double> list) {
		super();

		setVarName(name);
		setLabel(label);
		setValue(value);
		setMinValue(Double.NEGATIVE_INFINITY);
		setMaxValue(Double.MAX_VALUE);

		parameterList = list;
	}

	/**
	 * Change the parameter list.
	 *
	 * @param list
	 *            of param
	 */
	public void setParameterList(final List<Double> list) {
		parameterList = list;
	}

	/**
	 * Return the parameter list.
	 *
	 * @return parameter list
	 */
	public List<Double> getParameterList() {
		return parameterList;
	}

	@Override
	public String toString() {
		StringBuilder val = new StringBuilder();
		if (parameterList != null) {
			for (int cpt = 0; cpt < parameterList.size(); cpt++) {
				Double d = parameterList.get(cpt);

				val.append(getVarName());
				val.append(cpt);
				val.append('=');
				val.append(d);
			}
		}
		return val.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((parameterList == null) ? 0 : parameterList.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if (this == obj)
			return true;

		if ((obj == null) || (obj.getClass() != this.getClass()))
			return false;

		ParamArrayDescription paramArrDes = (ParamArrayDescription) obj;

		result = ((this.parameterList == paramArrDes.parameterList) || ((this.parameterList)
				.equals(paramArrDes.parameterList)));

		return result;
	}
}
