/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.common;

import java.util.ArrayList;
import java.util.List;

/**
 * Class representing a metadata. A metadata have a name, value according name, a comment, and the unit value.
 *
 * @author Lea Foissac
 */
public class CassisMetadata implements Cloneable {

	public static final String VLSR = "vlsr";
	public static final String LOFREQ = "lofreq";
	public static final String TELESCOPE = "telescop";
	public static final String DSETS = "DSETS";
	public static final String DS = "DS";
	public static final String HDU_NAME = "HDU_NAME";
	public static final String FILE_TYPE = "FILE_TYPE";
	public static final String FITS_TYPE = "FITS_TYPE";
	public static final String THETA_MIN_BEAM = "THETA_MIN_BEAM";
	public static final String THETA_MAX_BEAM = "THETA_MAX_BEAM";
	public static final String THETA_MIN = "THETA_MIN";
	public static final String THETA_MAX= "THETA_MAX";
	public static final String TYPE_REGION = "TYPE_REGION";
	public static final String ELLIPTICAL_REGION = "ELLIPTICAL";
	public static final String GAUSSIAN_REGION = "GAUSSIAN";
	public static final String ORIGIN_DATA = "ORIGIN_DATA";

	private String name;
	private String value;
	private String comment;
	private String unit;


	/**
	 * Constructor which create CassisMetadata object with name, value, comment and unit given
	 *
	 * @param name
	 *            The metadata name
	 * @param value
	 *            The value according to name metadata
	 * @param comment
	 *            The comment
	 * @param unit
	 *            The unit value
	 */
	public CassisMetadata(String name, String value, String comment, String unit) {
		this.name = name;
		this.value = value;
		this.comment = comment;
		this.unit = unit;
	}

	/**
	 * Construtor
	 */
	public CassisMetadata() {
	}

	/**
	 * Return metadata name
	 *
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Return comment relative to metadata
	 *
	 * @return metadata comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Return value unit
	 *
	 * @return metadata unit
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * Return value relative to name
	 *
	 * @return metadata value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Change metadata name value
	 *
	 * @param name
	 *            The metadata name
	 *
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Change metadata value relative to name metadata
	 *
	 * @param value
	 *            The metadata value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Change metadata comment
	 *
	 * @param comment
	 *            The metadata comment
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * Change metadata unit
	 *
	 * @param unit
	 *            The metadata unit
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	@Override
	public CassisMetadata clone() throws CloneNotSupportedException {
		return new CassisMetadata(this.name, this.value, this.comment, this.unit);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((comment == null) ? 0 : comment.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((unit == null) ? 0 : unit.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof CassisMetadata)) {
			return false;
		}
		CassisMetadata other = (CassisMetadata) obj;
		if (comment == null) {
			if (other.comment != null) {
				return false;
			}
		} else if (!comment.equals(other.comment)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (unit == null) {
			if (other.unit != null) {
				return false;
			}
		} else if (!unit.equals(other.unit)) {
			return false;
		}
		if (value == null) {
			if (other.value != null) {
				return false;
			}
		} else if (!value.equals(other.value)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "CassisMetadata [name=" + name + ", value=" + value + ", comment="
				+ comment + ", unit=" + unit + "]";
	}

	/**
	 * Return a metadata according to name metadata in list, respecting the case.
	 *
	 * @param name The metadata name
	 * @param cassisMetadataList The metadata list.
	 * @return The metadata according to metadata name.
	 */
	public static CassisMetadata getCassisMetadataCase(String name,
			List<CassisMetadata> cassisMetadataList) {
		CassisMetadata finalCassisMetadata = null;
		if (cassisMetadataList != null){
			for (CassisMetadata cassisMetadata : cassisMetadataList) {
				if (cassisMetadata.getName().equals(name)) {
					finalCassisMetadata = cassisMetadata;
				}
			}
		}
		return finalCassisMetadata;
	}

	/**
	 * Return a metadata if contains name metadata in list, respecting the case.
	 *
	 * @param name The metadata name
	 * @param cassisMetadataList The metadata list.
	 * @return The metadata if contains the metadata name.
	 */
	public static CassisMetadata containsCassisMetadataCase(String name,
			List<CassisMetadata> cassisMetadataList) {
		CassisMetadata finalCassisMetadata = null;
		for (CassisMetadata cassisMetadata : cassisMetadataList) {
			if (cassisMetadata.getName().contains(name)) {
				finalCassisMetadata = cassisMetadata;
			}
		}
		return finalCassisMetadata;
	}


	/**
	 * Return a metadata if starts name metadata in list, respecting the case.
	 *
	 * @param name The metadata name
	 * @param cassisMetadataList The metadata list.
	 * @return The metadata if starts the metadata name.
	 */
	public static CassisMetadata startWithCassisMetadataCase(String name,
			List<CassisMetadata> cassisMetadataList) {
		CassisMetadata finalCassisMetadata = null;
		for (CassisMetadata cassisMetadata : cassisMetadataList) {
			if (cassisMetadata.getName().startsWith(name)) {
				finalCassisMetadata = cassisMetadata;
			}
		}
		return finalCassisMetadata;
	}

	/**
	 * Know if a metadata according to his name exists in a metadata list, respecting the case.
	 *
	 * @param name The metadata name.
	 * @param cassisMetadataList The metadata list.
	 * @return true if the metadata name exist in the metadata list, false otherwise.
	 */
	public static boolean isMetadataNameCase(String name, List<CassisMetadata> cassisMetadataList) {
		for (CassisMetadata cassisMetadata : cassisMetadataList) {
			if (cassisMetadata.getName().equals(name)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Check if a metadata with name, value is in the list
	 *
	 * @param metaDatas The list of metadata.
	 * @param name The name.
	 * @param value The value.
	 * @return True or False
	 */
	public static boolean isIn(List<CassisMetadata> metaDatas, String name, String value) {
		boolean find = false;
		for (int index = 0; !find && index < metaDatas.size(); index++) {
			CassisMetadata cassisMetadata = metaDatas.get(index);
			if (cassisMetadata.getName().trim().equals(name)&&
				cassisMetadata.getValue().trim().equals(value)){
				find = true ;
			}
		}
		return find;
	}

	/**
	 * Return the first metadata according to name metadata in list.
	 * This does not respect the case of the given name!
	 *
	 * @param name
	 *            The metadata name
	 * @param cassisMetadataList
	 *            The metadata list
	 * @return The metadata according to metadata name
	 */
	public static CassisMetadata getCassisMetadata(String name, List<CassisMetadata> cassisMetadataList) {
		CassisMetadata finalCassisMetadata = null;
		for (CassisMetadata cassisMetadata : cassisMetadataList) {
			if (cassisMetadata.getName().equalsIgnoreCase(name)) {
				finalCassisMetadata = cassisMetadata;
				break;
			}
		}
		return finalCassisMetadata;
	}

	/**
	 * Know if a metadata according to his name exists in a metadata list.
	 * This does not respect the case of the given name!
	 *
	 * @param name
	 *            The metadata name
	 * @param cassisMetadataList
	 *            The metadata list
	 * @return True if the metadata name exist in the metadata list and false if not
	 */
	public static boolean isMetadataName(String name, List<CassisMetadata> cassisMetadataList) {
		for (CassisMetadata cassisMetadata : cassisMetadataList) {
			if (cassisMetadata.getName().equalsIgnoreCase(name)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Search in the two provided list then return the metadata with the given name.
	 *
	 * @param cassisMetadataList The metadata list read in the file.
	 * @param additionalMetadataList Additional metadata list external to the file.
	 * @param name The name of the metadata to search.
	 * @return return metadata or null if not found.
	 */
	public static CassisMetadata getMetadata(List<CassisMetadata> cassisMetadataList,
			List<CassisMetadata> additionalMetadataList, String name) {
		CassisMetadata spectralAxisMetadata = null;
		if (additionalMetadataList != null) {
			spectralAxisMetadata = getCassisMetadataCase(name, additionalMetadataList);
		}
		if (spectralAxisMetadata == null) {
			spectralAxisMetadata = getCassisMetadataCase(name, cassisMetadataList);
		}
		return spectralAxisMetadata;
	}

	/**
	 * Replace the metadata with the given name in the list. It is replaced
	 *  (or added) with the given name and value.
	 *
	 * @param name name of the metadata to replace
	 * @param value value of the metadata to change
	 * @param metadataList list of the metadata
	 */
	public static void replace(String name, String value, List<CassisMetadata> metadataList) {
		CassisMetadata meta = getCassisMetadataCase(name, metadataList);
		if (meta != null){
			metadataList.remove(meta);
		}
		metadataList.add(new CassisMetadata(name, value, null, null));
	}

	/**
	 * Replace the metadata with the given name in the list. It is replaced
	 *  (or added) with the @param metadata
	 * @param name name of the metadata to replace
	 * @param metadata
	 * @param metadataList list of the metadata
	 */
	public static void replace(String name, CassisMetadata metadata, List<CassisMetadata> metadataList) {
		CassisMetadata meta = getCassisMetadataCase(name, metadataList);
		if (meta != null){
			metadataList.remove(meta);
		}
		metadataList.add(metadata);
	}

	public static List<CassisMetadata> clone(List<CassisMetadata> cassisMetadataList) {
		List<CassisMetadata> res = new ArrayList<CassisMetadata>();
		if (cassisMetadataList == null) {
			res = null;
		}
		if (res != null) {
			for (CassisMetadata cassisMetadata : cassisMetadataList) {
				try {
					res.add(cassisMetadata.clone());
				} catch (CloneNotSupportedException e) {

					e.printStackTrace();
				}
			}
		}
		return res;
	}


}
