/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.common;

import java.util.ArrayList;

/**
 * Class representing a line description.
 */
public class LineDescription implements Cloneable {

	private double obsFrequency; // MHz
	private double dObsFrequency; // MHz
	private double dopplerWidth; // km/s
	private double integralFlux; // K*km/s

	private ArrayList<Double> coefsProfile; // coefficients to compute a line
											// profile (depends on the model)

	private String identification;
	private double channelFlux; // Ta : flux of the associated channel
	private double aijOnNu2; // Aij*gu/nu2
	private double eUpK; // Equivalent temperature for High energy level
	private int gu; // gu value from database
	private double aij;
	private double tau;
	private String quanticN; // quantics numbers
	private int molTag;
	private String molName = "UNKNOW";
	private boolean isDoubleSideBand;
	private int numCompEmmi = -1;
	private double tex;
	private double thetaSource;
	private double diameter;
	private double maxIntensity;
	private double vlsr;
	private double tbg;
	/**
	 * compute frequency can be different from observation Frequency
	 * because the line can come from a model
	 */
	private double computeFrequency; // MHz
	private double vlsrData;
	private String citation="";


	/**
	 * Constructor makes a new standard line description.
	 */
	public LineDescription() {
		obsFrequency = 0.;
		computeFrequency = 0.;
		dObsFrequency = 0.;
		dopplerWidth = 0.;
		integralFlux = 0.;
		coefsProfile = new ArrayList<Double>();
		identification = "";
		channelFlux = 0.;
		aijOnNu2 = 0.;
		eUpK = 0.;
		gu = 0;
		aij = 0.;
		tau = 0.;
		quanticN = "";
	}

	/**
	 * Constructor makes a line description with the given parameters.
	 *
	 * @param obsFrequency The obsFrequency (MHz).
	 * @param dObsFrequency The dObsFrequency (MHz).
	 * @param dopplerWidth The dopplerWidth (km/s).
	 * @param integralFlux The integralFlux (K*km/s).
	 * @param coefsProfile The coefficients to compute a line profile.
	 * @param identification A String identification message.
	 * @param aijOnNu2 The aijOnNu2 (Aij*gu/nu2).
	 * @param eUpK The equivalent temperature for High energy level.
	 * @param gu The gu value from database.
	 * @param aij The aij.
	 */
	public LineDescription(double obsFrequency, double dObsFrequency, double dopplerWidth,
			double integralFlux,ArrayList<Double> coefsProfile, String identification,
			double aijOnNu2, double eUpK, int gu, double aij) {
		this();
		this.obsFrequency = obsFrequency;
		this.computeFrequency = obsFrequency;
		this.dObsFrequency = dObsFrequency;
		this.identification = identification;

		this.dopplerWidth = dopplerWidth;
		this.integralFlux = integralFlux;
		this.coefsProfile = coefsProfile;

		this.aijOnNu2 = aijOnNu2;
		this.eUpK = eUpK;
		this.gu = gu;
		this.aij = aij;
	}

	/**
	 * Constructor makes a line description with the given parameters.
	 *
	 * @param obsFrequency The obsFrequency (MHz).
	 * @param dObsFrequency The dObsFrequency (MHz).
	 * @param identification A String identification message.
	 * @param aijOnNu2 The (Aij*gu/nu2).
	 * @param upK The equivalent temperature for High energy level.
	 * @param gu The gu value from database.
	 * @param aij The aij.
	 * @param vlsr The vlsr.
	 * @param molName The molecule name.
	 * @param molTag The molecule tag.
	 * @param quanticN The quantics numbers.
	 */
	public LineDescription(double obsFrequency, double dObsFrequency,
			String identification,double aijOnNu2, double upK, int gu,
			double aij, double vlsr, String molName,int molTag, String quanticN) {
		this();
		this.obsFrequency = obsFrequency;
		this.computeFrequency = obsFrequency;
		this.dObsFrequency = dObsFrequency;
		this.identification = identification;

		this.aijOnNu2 = aijOnNu2;
		this.eUpK = upK;
		this.gu = gu;
		this.aij = aij;

		this.vlsr = vlsr;
		this.molName = molName;
		this.molTag = molTag;
		this.quanticN = quanticN;
	}

	/**
	 * Return the quantics numbers.
	 *
	 * @return quantics numbers
	 */
	public String getQuanticN() {
		return quanticN;
	}

	/**
	 * Change the quantics numbers.
	 *
	 * @param qtn The new quantics numbers.
	 */
	public void setQuanticN(String qtn) {
		this.quanticN = qtn;
	}

	/**
	 * Return the obsFrequency in MHz.
	 *
	 * @return the obsFrequency in MHz.
	 */
	public double getObsFrequency() {
		return obsFrequency;
	}

	/**
	 * Change the obsFrequency in MHz.
	 *
	 * @param obsFrequency The new obsFrequency in MHz.
	 */
	public void setObsFrequency(final double obsFrequency) {
		this.obsFrequency = obsFrequency;
		this.computeFrequency = obsFrequency;
	}

	/**
	 * Return the dObsFrequency in MHz.
	 *
	 * @return the dObsFrequency in MHz.
	 */
	public double getDObsFrequency() {
		return dObsFrequency;
	}

	/**
	 * Change the dObsFrequency in MHz.
	 *
	 * @param dObsFrequency The new dObsFrequency in MHz.
	 */
	public void setDObsFrequency(final double dObsFrequency) {
		this.dObsFrequency = dObsFrequency;
	}

	/**
	 * Return the dopplerWidth in km/s.
	 *
	 * @return the dopplerWidth in km/s.
	 */
	public double getDopplerWidth() {
		return dopplerWidth;
	}

	/**
	 * Change the dopplerWidth in km/s.
	 *
	 * @param dopplerWidth The new dopplerWidth in km/s.
	 */
	public void setDopplerWidth(final double dopplerWidth) {
		this.dopplerWidth = dopplerWidth;
	}

	/**
	 * Return the integral flux.
	 *
	 * @return the integral flux.
	 */
	public double getIntegralFlux() {
		return integralFlux;
	}

	/**
	 * Change the integral flux.
	 *
	 * @param integralFlux The new integral flux.
	 */
	public void setIntegralFlux(final double integralFlux) {
		this.integralFlux = integralFlux;
	}

	/**
	 * Return the coefficients to compute a line profile.
	 *
	 * @return the coefficients to compute a line profile.
	 */
	public ArrayList<Double> getCoefsProfile() {
		return coefsProfile;
	}

	/**
	 * Change the coefficients to compute a line profile.
	 *
	 * @param coefsProfile The new coefficients.
	 */
	public void setCoefsProfile(final ArrayList<Double> coefsProfile) {
		this.coefsProfile = coefsProfile;
	}

	/**
	 * Return the identification.
	 *
	 * @return the identification.
	 */
	public String getIdentification() {
		return identification;
	}

	/**
	 * Change the identification.
	 *
	 * @param identification The new identification.
	 */
	public void setIdentification(final String identification) {
		this.identification = identification;
	}

	/**
	 * Return the flux of the associated channel.
	 *
	 * @return the flux of the associated channel.
	 */
	public double getChannelFlux() {
		return channelFlux;
	}

	/**
	 * Change the flux of the associated channel.
	 *
	 * @param channelFlux The new flux.
	 */
	public void setChannelFlux(final double channelFlux) {
		this.channelFlux = channelFlux;
	}

	/**
	 * Return the aijOnNu2 (Aij*gu/nu2).
	 *
	 * @return the aijOnNu2 (Aij*gu/nu2).
	 */
	public double getAijOnNu2() {
		return aijOnNu2;
	}

	/**
	 * Change aijOnNu2 (Aij*gu/nu2).
	 *
	 * @param aijOnNu2 The new value of aijOnNu2.
	 */
	public void setAijOnNu2(final double aijOnNu2) {
		this.aijOnNu2 = aijOnNu2;
	}

	/**
	 * Return the equivalent temperature for High energy level.
	 *
	 * @return the equivalent temperature for High energy level.
	 */
	public double getEUpK() {
		return eUpK;
	}

	/**
	 * Change the equivalent temperature for High energy level.
	 *
	 * @param eUpK The new temperature.
	 */
	public void setEUpK(final double eUpK) {
		this.eUpK = eUpK;
	}

	/**
	 * Return the gu value from database.
	 *
	 * @return the gu value from database.
	 */
	public int getGu() {
		return gu;
	}

	/**
	 * Change the gu value.
	 *
	 * @param gu The new gu value.
	 */
	public void setGu(final int gu) {
		this.gu = gu;
	}

	/**
	 * Return the aij.
	 *
	 * @return the aij.
	 */
	public double getAij() {
		return aij;
	}

	/**
	 * Change the aij.
	 *
	 * @param aij The new aij.
	 */
	public void setAij(final double aij) {
		this.aij = aij;
	}

	/**
	 * Return the tau value.
	 *
	 * @return the tau value.
	 */
	public double getTau() {
		return tau;
	}

	/**
	 * Change the tau value.
	 *
	 * @param tau The new tau value.
	 */
	public void setTau(final double tau) {
		this.tau = tau;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	/**
	 * Create and return a String representation of the object.
	 */
	@Override
	public String toString() {
		String res = "\n\n" + "obsFrequency = " + obsFrequency + "\ndObsFrequency = " + dObsFrequency
				+ "\ndopplerWidth = " + dopplerWidth + "\nintegralFlux = " + integralFlux + "\nidentification = "
				+ identification + "\nchannelFlux = " + channelFlux
				+ "\naijOnNu2 = " + aijOnNu2 + "\neUpK = " + eUpK + "\ngu = " + gu + "\na = " + aij
			    + "\nvlsr = " + vlsr + "\ntau = " + tau + "\nquanticN = " + quanticN
				+ "\ncoefsProfile = " + coefsProfile;
		return res;
	}

	/**
	 * Return the name of the associated molecule.
	 *
	 * @return the name of the associated molecule.
	 */
	public final String getMolName() {
		return molName;
	}

	/**
	 * Return the tag of the associated molecule.
	 *
	 * @return the tag of the associated molecule.
	 */
	public final int getMolTag() {
		return molTag;
	}

	/**
	 * Change the origin component.
	 *
	 * @param numCompEmmi the origin component.
	 */
	public void setFromCompo(int numCompEmmi) {
		this.numCompEmmi = numCompEmmi;
	}

	/**
	 * Return the origin component.
	 *
	 * @return the origin component.
	 */
	public final int getNumCompEmmi() {
		return numCompEmmi;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	/**
	 * Clone the object.
	 */
	@Override
	public Object clone() {
		LineDescription line = null;
		try {
			line = (LineDescription) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		if (line != null) {
			line.obsFrequency = this.obsFrequency; // MHz
			line.computeFrequency = this.computeFrequency; // MHz
			line.dObsFrequency = this.dObsFrequency; // MHz
			line.dopplerWidth = this.dopplerWidth; // km/s
			line.integralFlux = this.integralFlux; // K*km/s
			ArrayList<Double> lineCoefsProfile = null;
			if (this.coefsProfile != null) {
				lineCoefsProfile = new ArrayList<Double>();
				for (Double d : this.coefsProfile)
					lineCoefsProfile.add(d);
			}
			line.coefsProfile = lineCoefsProfile;

			line.identification = this.identification;

			line.channelFlux = this.channelFlux;

			line.aijOnNu2 = this.aijOnNu2; // Aij*gu/nu2

			line.eUpK = this.eUpK; // Equivalent temperature for High energy
									// level
			line.gu = this.gu; // gu value from database
			line.aij = this.aij;
			line.vlsr = this.vlsr; // velocity

			line.tau = this.tau;

			line.quanticN = this.quanticN; // quantics numbers

			line.molTag = this.molTag;

			line.molName = this.molName;

			line.isDoubleSideBand = this.isDoubleSideBand;
			line.numCompEmmi = this.numCompEmmi;
		}
		return line;
	}

	/**
	 * Change the tex (temperature).
	 *
	 * @param tex The new tex value.
	 */
	public void setTex(double tex) {
		this.tex = tex;
	}

	/**
	 * Return the tex (temperature).
	 *
	 * @return the tex.
	 */
	public final double getTex() {
		return tex;
	}

	/**
	 * Change the molecule name.
	 *
	 * @param name the new molecule name.
	 */
	public void setMolName(String name) {
		this.molName = name;
	}

	/**
	 * Change the molecule tag.
	 *
	 * @param tag The new molecule tag.
	 */
	public final void setMolTag(int tag) {
		this.molTag = tag;
	}

	/**
	 * Change the thetaSource (size of the source).
	 *
	 * @param thetaSource The new thetasource value.
	 */
	public void setThetaSource(double thetaSource) {
		this.thetaSource = thetaSource;
	}

	/**
	 * Return the theta source (size of the source).
	 *
	 * @return the theta source (size of the source).
	 */
	public final double getThetaSource() {
		return thetaSource;
	}

	/**
	 * Change the diameter.
	 *
	 * @param diameter The new diameter.
	 */
	public void setDiameter(double diameter) {
		this.diameter = diameter;
	}

	/**
	 * Return the diameter.
	 *
	 * @return the diameter.
	 */
	public double getDiameter() {
		return this.diameter;
	}

	/**
	 * Change the maximum intensity.
	 *
	 * @param intensity The new maximum intensity value.
	 */
	public void setMaxIntensity(double intensity) {
		this.maxIntensity = intensity;
	}

	/**
	 * Return the maximum intensity.
	 *
	 * @return the maximum intensity.
	 */
	public final double getMaxIntensity() {
		return maxIntensity;
	}

	/**
	 * Change the vlsr.
	 *
	 * @param vlsr The new vlsr value.
	 */
	public void setVlsr(double vlsr) {
		this.vlsr = vlsr;
	}

	/**
	 * Return the vlsr.
	 *
	 * @return the vlsr.
	 */
	public final double getVlsr() {
		return vlsr;
	}

	/**
	 * Change the tbg (background temperature).
	 *
	 * @param tbg The new tbg value.
	 */
	public void setTbg(double tbg) {
		this.tbg = tbg;
	}

	/**
	 * Return the tbg (background temperature) value.
	 *
	 * @return the tbg (background temperature) value.
	 */
	public final double getTbg() {
		return tbg;
	}

	/**
	 * Return if the line come from image band.
	 *
	 * @return if the line come from image band.
	 */
	public final boolean isDoubleSideBand() {
		return isDoubleSideBand;
	}

	/**
	 * Change the double side band value (if the line come from image band).
	 *
	 * @param isDoubleSideBand The new double side band value.
	 */
	public final void setDoubleSideBand(boolean isDoubleSideBand) {
		this.isDoubleSideBand = isDoubleSideBand;
	}

	/**
	 * Change the compute frequency.
	 *
	 * @param lineFreq The compute frequency in MHz.
	 */
	public void setFreqCompute(double lineFreq) {
		computeFrequency = lineFreq;
	}

	/**
	 * Return the compute frequency in MHz.
	 *
	 * @return the compute frequency in MHz.
	 */
	public double getFreqCompute() {
		return computeFrequency;
	}

	/**
	 * Change the vlsr of the data.
	 *
	 * @param vlsrData The new vlsr of the data value.
	 */
	public void setVlsrData(double vlsrData) {
		this.vlsrData = vlsrData;
	}

	/**
	 * Return the vlsr of the data.
	 *
	 * @return the vlsr of the data.
	 */
	public double getVlsrData() {
		return this.vlsrData;
	}

	/**
	 * Return the citation of the data.
	 *
	 * @return the citation of the data.
	 */
	public String getCitation() {
		return citation;

	}
	/**
	 *
	 * @param citation the new citation parameter of the data value
	 */
	public void setCitation(String citation) {
		this.citation = citation;

	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	/**
	 * Create and return an hashCode.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(aij);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(aijOnNu2);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(channelFlux);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((coefsProfile == null) ? 0 : coefsProfile.hashCode());
		temp = Double.doubleToLongBits(computeFrequency);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(dObsFrequency);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(diameter);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(dopplerWidth);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(eUpK);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + gu;
		result = prime * result + ((identification == null) ? 0 : identification.hashCode());
		temp = Double.doubleToLongBits(integralFlux);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + (isDoubleSideBand ? 1231 : 1237);
		temp = Double.doubleToLongBits(maxIntensity);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((molName == null) ? 0 : molName.hashCode());
		result = prime * result + molTag;
		result = prime * result + numCompEmmi;
		temp = Double.doubleToLongBits(obsFrequency);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((quanticN == null) ? 0 : quanticN.hashCode());
		temp = Double.doubleToLongBits(tau);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(tbg);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(tex);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(thetaSource);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(vlsr);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(vlsrData);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	/**
	 * Test the equality with the given object.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LineDescription other = (LineDescription) obj;
		if (Double.doubleToLongBits(aij) != Double.doubleToLongBits(other.aij))
			return false;
		if (Double.doubleToLongBits(aijOnNu2) != Double.doubleToLongBits(other.aijOnNu2))
			return false;
		if (Double.doubleToLongBits(channelFlux) != Double.doubleToLongBits(other.channelFlux))
			return false;
		if (coefsProfile == null) {
			if (other.coefsProfile != null)
				return false;
		} else if (!coefsProfile.equals(other.coefsProfile))
			return false;
		if (Double.doubleToLongBits(computeFrequency) != Double.doubleToLongBits(other.computeFrequency))
			return false;
		if (Double.doubleToLongBits(dObsFrequency) != Double.doubleToLongBits(other.dObsFrequency))
			return false;
		if (Double.doubleToLongBits(diameter) != Double.doubleToLongBits(other.diameter))
			return false;
		if (Double.doubleToLongBits(dopplerWidth) != Double.doubleToLongBits(other.dopplerWidth))
			return false;
		if (Double.doubleToLongBits(eUpK) != Double.doubleToLongBits(other.eUpK))
			return false;
		if (gu != other.gu)
			return false;
		if (identification == null) {
			if (other.identification != null)
				return false;
		} else if (!identification.equals(other.identification))
			return false;
		if (Double.doubleToLongBits(integralFlux) != Double.doubleToLongBits(other.integralFlux))
			return false;
		if (isDoubleSideBand != other.isDoubleSideBand)
			return false;
		if (Double.doubleToLongBits(maxIntensity) != Double.doubleToLongBits(other.maxIntensity))
			return false;
		if (molName == null) {
			if (other.molName != null)
				return false;
		} else if (!molName.equals(other.molName))
			return false;
		if (molTag != other.molTag)
			return false;
		if (numCompEmmi != other.numCompEmmi)
			return false;
		if (Double.doubleToLongBits(obsFrequency) != Double.doubleToLongBits(other.obsFrequency))
			return false;
		if (quanticN == null) {
			if (other.quanticN != null)
				return false;
		} else if (!quanticN.equals(other.quanticN))
			return false;
		if (Double.doubleToLongBits(tau) != Double.doubleToLongBits(other.tau))
			return false;
		if (Double.doubleToLongBits(tbg) != Double.doubleToLongBits(other.tbg))
			return false;
		if (Double.doubleToLongBits(tex) != Double.doubleToLongBits(other.tex))
			return false;
		if (Double.doubleToLongBits(thetaSource) != Double.doubleToLongBits(other.thetaSource))
			return false;
		if (Double.doubleToLongBits(vlsr) != Double.doubleToLongBits(other.vlsr))
			return false;
		if (Double.doubleToLongBits(vlsrData) != Double.doubleToLongBits(other.vlsrData))
			return false;
		return true;
	}





}
