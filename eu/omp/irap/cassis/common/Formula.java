/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*******************************************************************************
 *                 CASSIS project ( http://cassis.irap.omp.eu )
 *******************************************************************************
 * Copyright (c) 2013, CNRS. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     - Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     - Neither the name of the CNRS nor the names of its contributors may be
 *       used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL CNRS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package eu.omp.irap.cassis.common;

import java.util.List;



/**
 * Formula.
 *
 * @author Alain KLOTZ
 * @author Laure TAMISIER
 * @author Jean-Michel GLORIAN
 */
public class Formula {

	public static final double CSTE_ROT_DIAGRAM = (8 * Math.PI * Formula.kBOLTZMANN_cgs)
			/ (Formula.hPLANCK_cgs * Math.pow(Formula.cLIGHT_cgs, 3));

	public static final double AINT_COEFF_JPL = 2.796410e-16;

	/** Planck constant [J*s] */
	public static final double H = 6.626070040e-34;

	/** Planck constant in cgs [erg*s] (1 erg = 10^−7 J) */
	public static final double hPLANCK_cgs = 6.626070040e-27;

	/** Light velocity [m/s] */
	public static final double C = 299792458;

	/** Light velocity [km/s] */
	public static final double CKm = 299792.458;

	/** Light velocity in CGS [cm/s] */
	public static final double cLIGHT_cgs = 2.997924580e10;

	/** Light velocity [cm/s] */
	public static final double c = 2.99792458e10;

	/** Boltzmann constant [J/K] */
	public static final double K = 1.38064852e-23;

	/** Boltzmann constant in cgs [erg*K] */
	public static final double kBOLTZMANN_cgs = 1.38064852e-16;

	/** tcmb cosmic millimeter background */
	public static final double tcmb = 2.73;

	/** Perfect gas constant [J/mol/K] */
	public static final double R = 8.3144;

	public static final double hcsurk = 1.43879;

	public static final double freqMin = 0;
	public static final double freqMax = Double.MAX_VALUE;
	public static final double param_n = 1e14;
	public static final double dfreq = 100.;

	// radius of the comet nucleus for which colden=1e17
	// H20/cm2 for Q=1.e28 H2O/s
	public static final double abiso = 1.0;

	public static final double coeffRadToArsec = 180 * 3600 / Math.PI;


	/**
	 * Compute the correction of the telescope.
	 *
	 * @param thetaSource The size of the observed source.
	 * @param fMHzTrans The frequency in MHz.
	 * @param diameter The telescope diameter.
	 * @return the result.
	 */
	public static double beamCorrection(double thetaSource, double fMHzTrans, double diameter) {
		// --- hpbw : Half Power Beam Width [arcsec] (Formula T3)
		double hpbw = Formula.calcHpbw(fMHzTrans, diameter);
		final double cosThetaFactor = 1 - Math.cos(thetaSource / coeffRadToArsec);
		double facteur = ((1 - Math.cos(hpbw)) + cosThetaFactor) / cosThetaFactor;
		return facteur;
	}

	/**
	 * Calculation of eLowJ --- eLowJ : low energy state [J] (Formula P3)
	 *
	 * @param elowTrans in cm-1
	 * @param eminTrans The minimum elow of the transitions for the molecule in cm-1
	 * @return the elow in [J] of the transition corrected by the minimum elow of the transitions
	 */
	public static double calcELowJ(double elowTrans, double eminTrans) {
		return ((elowTrans - eminTrans) * H * C * 100.);
	}

	/**
	 * Calculation of eUpJ --- eUpJ : high energy state [J] (Formula P3)
	 *
	 * @param eLowJ in Joule
	 * @param fMHzTrans Frequency i the transition in MHz
	 * @return calcEUpJ
	 */
	public static double calcEUpJ(double eLowJ, double fMHzTrans) {
		return (eLowJ + H * fMHzTrans * 1e6);
	}

	/**
	 * Calculation of the eUpK high energy state [K]
	 * (Formula E[K]=E[J]/k)
	 *
	 * @param eUpJ  High energy state in Joule
	 * @return high energy state [K]
	 */
	public static double calcEUpk(double eUpJ) {
		return (eUpJ / K);
	}

	/**
	 * Computation of the higth energy state eup in [K] from elow in [cm-1] and
	 *    frequency in [MHz].
	 *
	 * @param elow in [cm-1].
	 * @param eminTrans in |cm -1].
	 * @param fMHzTrans [MHz].
	 * @return the high energy state [K].
	 */
	public static double calcEUpk(double elow, double eminTrans, double fMHzTrans) {
		return Formula.calcEUpk(Formula.calcEUpJ(Formula.calcELowJ(elow, eminTrans), fMHzTrans));
	}

	/**
	 * Conversion of Energy E from [cm-1] to [K]
	 * E[K]=E[J]/k)
	 *
	 * @param energy in cm-1
	 * @return double
	 */
	public static double calcEUpKComet(double energy) {
		return (100. * H * C * energy / K);
	}

	/**
	 * Calculation of eup --- eup : high energy state [cm-1]
	 *
	 * @param elow low energy state in [cm-1]
	 * @param freq Frequency in MHz of the transitions
	 * @return the high energy state in cm-1
	 */
	public static double calcEup(double elow, double freq) {
		return (elow + 1.e6 * freq / c);
	}

	/**
	 * Calculation of TO --- tmbTheo : theoretical Tmb [K] (Formula Charlotte23)
	 *
	 * @param fMHzTrans in Mhz
	 * @return the theoretical Tmb [K]
	 */
	public static double calcTO(double fMHzTrans) {
		return (H * fMHzTrans * 1.e6 / K);
	}

	/**
	 * for PACS datacalcT1
	 * @param fMHzTrans Frequency of transitions in MHz
	 * @return brillanceToJy
	 */
	public static double brillanceToJy(double fMHzTrans) {
		return (2* kBOLTZMANN_cgs * Math.pow(fMHzTrans * 1.e6, 2) / (c * c)) * 4 * Math.PI * 1e23;
	}

	/**
	 * Calculation of tbmax
	 *
	 * @param tO : theoretical Tmb [K]
	 * @param tex : temperature of excitation [K]
	 * @return tbmax
	 */
	public static double calcTbmax(double tO, double tex) {
		return (tO * (1. / (Math.exp(tO / tex) - 1.) - 1. / (Math.exp(tO / tcmb) - 1.)));
	}

	/**
	 * Calculation of hpbw --- hpbw : Half Power Beam Width [radian] (Formula
	 * T3)
	 *
	 * @param fMHzTrans Frequency of the transition in MHz
	 * @param diameter Daimeter of the telescope
	 * @return the Half Power Beam Width [radian]
	 */
	public static double calcHpbw(double fMHzTrans, double diameter) {
		//1.22 * lamda / Diameter : formule de difraction
		return (1.22 * C / (fMHzTrans * 1e6 * diameter));
	}

	/**
	 * Calculation of tau --- tau : opacity (Formula L3)
	 *
	 * @param nUp  column density of the upper state [/cm2]
	 * @param aTrans Aeinstein coefficient
	 * @param fMHzTrans Frequency of the transition in MHz
	 * @param vexp width of the transition
	 * @param tex Temperature of excitation
	 * @return the opacity
	 */
	public static double calcTau0(double nUp, double aTrans, double fMHzTrans, double vexp, double tex) {
		return ((nUp * 1e4 * aTrans * C * C * C)
				/ (8 * Math.PI * Math.pow(fMHzTrans * 1.e6, 3.) * vexp * 1e3 * Math.sqrt(Math.PI) / (2 * Math.sqrt(Math
						.log(2)))) * (Math.exp(H * fMHzTrans * 1.e6 / K / tex) - 1.));
	}

	/**
	 * Calculation of nUp --- nUp : column density of the upper state [/cm2]
	 * (Formula L1)
	 *
	 * @param paramNu : column density
	 * @param iguTrans : High moment state
	 * @param qt value of the partition function
	 * @param eUpJ High energy state in Joule
	 * @param tex Temperature of excitation
	 * @return column density of the upper state [/cm2]
	 */
	public static double calcNUp(double paramNu, double iguTrans, double qt, double eUpJ, double tex) {
		return (((paramNu * iguTrans) / qt) * Math.exp(-eUpJ / (K * tex)));
	}

	/**
	 * Calculation of aijOnNu2 : criterium of the intrinseque line intensity [s]
	 *
	 * @param aTrans : Einstein coeff of the transition
	 * @param guTrans : g up of the transition
	 * @param fMHzTrans : frequency of the transition in MHz
	 * @return criterium of the intrinseque line intensity
	 */
	public static double calcAijOnNu2(double aTrans, int guTrans, double fMHzTrans) {
		return (aTrans * guTrans / (fMHzTrans * fMHzTrans * 1e12));
	}

	/**
	 * Calculation of the delta velocity in km/s
	 *
	 * @param deltaF : delta frequency in MHz
	 * @param freq0 : Reference frequency in MHz
	 * @return the delta velocity in km/s
	 */
	public static double calcDVForFile(double deltaF, double freq0) {
		return (CKm * (deltaF / freq0));
	}

	/**
	 * Calculation of velocity with in km/s
	 *
	 * @param freq : frequency in MHz
	 * @param freq0 : Reference frequency in MHz
	 * @param vlsr : offset of the velocity in km/s
	 * @return velocity with in km/s
	 */
	public static double calcVelocity(double freq, double freq0, double vlsr) {
		return ((1 - (freq / freq0)) * CKm) + vlsr;
	}

	/**
	 * Calculation of velocity with in km/s
	 *
	 * @param freq : frequency in MHz
	 * @param freq0 : Reference frequency in MHz
	 * @param vlsr : offset of the velocity in km/s
	 * @param signal boolean = true if signal mode
	 * @return velocity with in km/s
	 */
	public static double calcVelocity(double freq, double freq0, double vlsr, boolean signal) {
		if (signal)
			return ((1 - (freq / freq0)) * CKm) + vlsr;
		else
			return -((1 - (freq / freq0)) * CKm) + vlsr;
	}

	/**
	 * Calculation of vlsr source in MHz
	 *
	 * @param vlsrSource vlsr source in km/s
	 * @param lineFreq  Reference frequency in MHz
	 * @return vlsr source in MHz
	 */
	// TODO use like calcFreqWithVlsr = freq - calcVlsrSourceMHz
	public static double calcVlsrSourceMHz(double vlsrSource, double lineFreq) {
		return (vlsrSource / CKm * lineFreq);
	}

	/**
	 * Calculation of dfreq in km/s
	 *
	 * @param dfreq delta frequency in MHz
	 * @param freq frequency in MHz
	 * @return delta velocity in km/s
	 */
	public static double calcDFreqKms(double dfreq, double freq) {
		return (dfreq * CKm / freq);
	}

	/**
	 * Calculation of dsbFreq
	 *
	 * @param freq frequency in MHz
	 * @param dsbValue -1 if signal - if image
	 * @param intermediate frequency offset in MHz
	 * @return the double side band frequency
	 */
	public static double calcDsbFreq(final double freq, final double dsbValue, final double intermediate) {
		return (freq + dsbValue * intermediate);
	}

	/**
	 * Calculation of spectrum frequency min
	 *
	 * @param freqCentral : Central frequency in MHz
	 * @param bandwidth in km/s
	 * @return the frequency min in MHz
	 */
	public static double calcSpectrumFreqMin(double freqCentral, double bandwidth) {
		return (freqCentral * (1 - (bandwidth / 2) / (CKm)));
	}

	/**
	 * Calculation of spectrum frequency max
	 *
	 * @param freqCentral : Central frequency in MHz
	 * @param bandwidth in km/s
	 * @return the frequency max in MHz
	 */
	public static double calcSpectrumFreqMax(double freqCentral, double bandwidth) {
		return (freqCentral * (1 + (bandwidth / 2) / (CKm)));
	}

	/**
	 * Function of Yamamoto. Estimate by analytical approximation, precision
	 * calcColden
	 *
	 * @param x = ray of the lobe/length of scale of the molecule
	 * @return yama = number of molecules in the instrumental lobe / total
	 *         number of molecules in the coma
	 */
	public static double yama(double x) {
		double pi = Math.acos(-1);
		double y = x + 0.0156 * x * x + 1.0558 * x * x * x - 0.4560 * x * x * x * x + 0.1123 * x * x * x * x * x;
		return (y / (2. / pi + y));
	}

	/**
	 * Calculation of beta
	 *
	 * @param moleculeBeta  parameter for Comet Model
	 * @param rh  parameter for Comet Model
	 * @return the beta
	 */
	public static double calcBeta(double moleculeBeta, double rh) {
		return (moleculeBeta / (rh * rh));
	}

	/**
	 * Calculation of hnsurk  --- hnusurk : hnu/k [cgs]
	 *
	 * @param freq frequency in MHz
	 * @return  hnu/k [cgs]
	 */
	public static double calcHNsurk(double freq) {
		return (hcsurk * (1.e6 * freq) / c);
	}

	/**
	 * Calculation of sig --- sig : wavenumber of the transition [cm-1]
	 *
	 *
	 * @param freq frequency in MHz
	 * @return wavenumber of the transition [cm-1]
	 */
	public static double calcSig(double freq) {
		return (1.e6 * freq / c);
	}

	/**
	 * Calculation of thetel --- size of the lobe [radian]
	 *
	 * @param diameter Diameter of the telescope
	 * @param freq frequency in MHz
	 * @return size of the lobe
	 */
	public static double calcThetel(double diameter, double freq) {
		return (0.5 * 0.3822e3 / (diameter * freq));
	}

	/**
	 * Calculation of rho --- radius of the lobe at the comet topocentric
	 * distance [km]
	 *
	 * @param thetel Diameter of the telescope
	 * @param delta parameter of the comet model
	 * @return radius of the lobe at the comet topocentric
	 */
	public static double calcRho(double thetel, double delta) {
		return (thetel * 150.e6 * delta);
	}

	/**
	 * Calculation of gamma --- scale length [km]
	 *
	 * @param vexp parameter of the comet model
	 * @param beta parameter of the comet model
	 * @return scale length [km]
	 */
	public static double calcGamma(double vexp, double beta) {
		return (vexp / beta);
	}

	/**
	 * Calculation of colden --- column density [H20/cm-2]
	 *
	 * @param q : parameter of the comet model
	 * @param beta : parameter of the comet model
	 * @param rho :parameter of the comet model
	 * @param gamma :parameter of the comet model
	 * @return column density [H20/cm-2]
	 */
	public static double calcColden(double q, double beta, double rho, double gamma) {
		return (((1.e-10 * q) / beta) * yama(rho / gamma) / (Math.PI * rho * rho));
	}

	/**
	 * Calculation of deltanu --- deltanu : wavenumber of the transition [km-1]
	 *
	 * @param sig : ??
	 * @return  wavenumber of the transition [km-1]
	 */
	public static double calcDeltaNu(double sig) {
		return (1.e5 * sig);
	}

	/**
	 * Calculation of taudv --- taudv : opacity for that line
	 *
	 * @param aij : Aeinstein coefficient
	 * @param sig Frequency
	 * @param coldenUp : Colon density of the high state
	 * @param hnusurk : hnu / K
	 * @param tex : temperature of excitation
	 * @param deltanu : delta frequency
	 * @return calcTauDv opacity for that line
	 */
	public static double calcTauDv(double aij, double sig, double coldenUp, double hnusurk, double tex, double deltanu) {
		return ((aij / (8. * Math.PI * sig * sig)) * coldenUp * (Math.exp(hnusurk / tex) - 1.) / deltanu);
	}

	/**
	 * Calculation of Ta --- Ta : antenna temperature [K] at the emission peak
	 *
	 * @param tbmax the tbmax
	 * @param tau the tau
	 * @return antenna temperature [K] at the emmission peak
	 */
	public static double calcTa(double tbmax, double tau) {
		return (tbmax * (1 - Math.exp(-tau)));
	}

	/**
	 * Calculation of wobs --- wobs : equivalent width observed [K.km/s]
	 * (Formula T4)
	 *
	 * @param ta the ta
	 * @param vexp ; the vexp
	 * @return calcWobs equivalent width observed [K.km/s]
	 */
	public static double calcWobs(double ta, double vexp) {
		return (ta * vexp);
	}

	/**
	 * Calculation of dNuL --- dNuL : global half width of the lorentzian line
	 * [MHz] (Formula ?) --- dnuL : half width at half maximum for Lorentz
	 * broadening [MHz]
	 *
	 * @param pressure [mbar]
	 * @param temperature [K]
	 * @param gammaSelf the gamma self
	 * @return double [MHz]
	 */
	public static double calcDNuL(double pressure, double temperature, double gammaSelf) {
		return (1e-6 * (gammaSelf * (pressure / 1013.25) * c * (temperature / 296)));
	}

	/**
	 * Calculation of coreFactor --- coreFactor : Formula (3) of Pickett
	 *
	 * @param qt0 reference partition function value
	 * @param qt partition function value
	 * @param eLowJ [J]
	 * @param temperature [K]
	 * @param eUpJ [J]
	 * @param t0 [T]
	 * @return Calculation of coreFactor
	 */
	public static double calcCoreFactor(double qt0, double qt, double eLowJ, double temperature, double eUpJ, double t0) {
		return (qt0 / qt * (Math.exp(-eLowJ / (K * temperature)) - Math.exp(-eUpJ / (K * temperature))) / (Math
				.exp(-eLowJ / (K * t0)) - Math.exp(-eUpJ / (K * t0))));
	}

	/**
	 * Calculation of dnuD --- dnuD : half width at half maximum for Doppler
	 * broadening [MHz]
	 *
	 * @param fMHzTrans [MHz]
	 * @param twologtwo 2 * log(2)
	 * @param temperature [K]
	 * @param speciesMass [at this day we haven't determinate the unit of this param]
	 * @return half width at half maximum for Doppler broadening [MHz]
	 */
	public static double calcDNuD(double fMHzTrans, double twologtwo, double temperature, double speciesMass) {
		double protonMass = 1.672631e-24; // g
		return (fMHzTrans * 1e6 / C * Math.sqrt(twologtwo * K * temperature / ((speciesMass * protonMass) * 1e-3)) * 1e-6);
	}

	/**
	 * Calculation of alpha_m --- alpha_m : maximum of the absorption
	 * coefficient [/cm]
	 *
	 * @param i300Trans [nm2 * MHz]
	 * @param dnuD [MHz]
	 * @param dnuL [MHz]
	 * @param pressure [mbar]
	 * @param temperature [K]
	 * @param coreFactor [no unit] Pickett correction
	 * @return ximum of the absorption
	 * coefficient [cm-1]
	 */
	public static double calcAlphaM(double i300Trans, double dnuD, double dnuL, double pressure, double temperature,
			double coreFactor) {
		return (i300Trans * 1e-18 * coreFactor / (dnuL * Math.PI + dnuD * Math.sqrt(Math.PI / Math.log(2))) * pressure
				* 1e-3 * 101325 / K / temperature * 1e-2);
	}

	/**
	 * Calculation of dnuV --- alpha_m : maximum of the absorption coefficient [/cm]
	 *
	 * @param dnuD [MHz]
	 * @param dnuL [MHz]
	 * @return double [MHz]
	 */
	public static double calcDnuV(double dnuL, double dnuD) {
		return (0.5346 * dnuL + Math.sqrt(0.2166 * dnuL * dnuL + dnuD * dnuD));
	}

	/**
	 * Calculation of vexp --- vexp : full width expressed in velocity [km/s]
	 * (Formula ?)
	 *
	 * @param dnuL  [MHz]
	 * @param fMHzTrans Freqeucy in MHz
	 * @return calcVexp Full width expressed in velocity [km/s]
	 */
	public static double calcVexp(double dnuL, double fMHzTrans) {
		return (2 * dnuL * CKm / fMHzTrans);
	}

	/**
	 * Calculation of lineWidthMHz full line width (converted from km/s to MHz)
	 * (Formula TG1)
	 *
	 * @param vlsr Offset velocity in km/s
	 * @param lineFreq Refrence frequency in MHz
	 * @return Calculation of lineWidthMHz full line width
	 */
	// TODO same that calcVlsrSourceMHz
	public static double calcLineWidthMHz(double vlsr, double lineFreq) {
		return (vlsr / CKm * lineFreq);
	}

	/**
	 * Calculation of sigma [km/s]
	 *
	 * @param sigma fwhm2sigma_gaussian [km/s]
	 * @param lineFreq Frequency in MHz
	 * @return  Calculation of sigma [km/s]
	 */
	public static double calcSigmaKms(double lineFreq, double sigma) {
		return (sigma * CKm / lineFreq);
	}

	/**
	 * Calculation of sigma
	 *
	 * @param lineWidthMHz line with in MHz
	 * @param fwhm2 Full width half medium
	 * @return Calculation of sigma
	 */
	public static double calcSigma(double lineWidthMHz, double fwhm2) {
		return (lineWidthMHz * fwhm2);
	}

	/**
	 * Calculation of aint for Gaussian Gaussian function for antena temperature
	 * Tb(nu) [K] (Formula TG4)

	 * @param int0 the int0
	 * @param difFreq the difFreq
	 * @param sigma the sigma
	 * @return the Calculation of aint for Gaussian
	 */
	public static double calcAintGaussian(double int0, double difFreq, double sigma) {
		return (int0 * Math.exp(-(difFreq * difFreq) / (sigma * sigma)));
	}

	/**
	 * Calculation of aint for Lorentzian Lorentzian function (Formula ?)
	 * {@link CommentedSpectrum}
	 *
	 * @param difFreq the difFreq
	 * @param dnuL the dnuL
	 * @param int0 the int0
	 * @return the Calculation of aint for Lorentzian
	 */
	public static double calcAintLorentzian(double difFreq, double dnuL, double int0) {
		return (1 / (1. + (difFreq * difFreq) / (dnuL * dnuL)) * (1 - Math.exp(-int0)));
	}

	/**
	 *
	 * @param fv the fv
	 * @param int0 the int0
	 * @return calclAintVoigt
	 */
	public static double calclAintVoigt(double fv, double int0) {
		return (int0 * fv);
	}

	/**
	 * Compute the calcFv
	 *
	 * @param difFreq the difFreq
	 * @param sqrtlog2 the sqrtlog2
	 * @param gammaGaussian the gammaGaussian
	 * @param gammaLorentz the gammaLorentz
	 * @return calcFv
	 */
	public static double calcFv(double difFreq, double sqrtlog2, double gammaGaussian, double gammaLorentz) {
		double cL = calcCl(gammaLorentz, gammaGaussian);
		double cG = calcCG(gammaLorentz, gammaGaussian);
		double dnuV = calcDnuV(gammaLorentz, gammaGaussian);

		double first = cL * (1 / Math.PI) * (dnuV / (difFreq * difFreq + dnuV * dnuV));
		double second = cG * (sqrtlog2 / (Math.sqrt(Math.PI) * dnuV))
				* Math.exp((-(sqrtlog2 * sqrtlog2) * difFreq * difFreq) / (dnuV * dnuV));

		return (first + second) * 1e-6;
	}

	/**
	 * Compute the calcCl
	 *
	 * @param dnuL the dnuL parameter
	 * @param dnuD the dnuD parameter
	 * @return calcCl
	 */
	public static double calcCl(double dnuL, double dnuD) {
		double d = ((dnuL - dnuD) / (dnuL + dnuD));
		return (0.68188 + 0.61293 * d - 0.18384 * d * d - 0.11568 * d * d * d);
	}

	/**
	 * Compute the calcCG
	 *
	 * @param dnuL the dnuL parameter
	 * @param dnuD the dnuD parameter
	 * @return the calcCG
	 */
	public static double calcCG(double dnuL, double dnuD) {
		double d = ((dnuL - dnuD) / (dnuL + dnuD));
		return (0.32460 - 0.61825 * d + 0.17681 * d * d + 0.12109 * d * d * d);
	}

	/**
	 * Calculation of aEinstein
	 *
	 * @param i300 the i300 parameter
	 * @param fMHz frequency in MHz
	 * @param zp0 the zp0 parameter
	 * @param igu the igu parameter
	 * @param eLowJ Low energy state in Joule
	 * @param val300K the partition function value for 300 K
	 * @param eUpJ High energy state in Joule
	 * @return aEinstein value
	 */
	public static double calcAEinstein(double i300, double fMHz, double zp0, double igu, double eLowJ, double val300K,
			double eUpJ) {
		return (i300 * Math.pow(fMHz, 2) * (zp0 / igu) * 1
				/ (Math.exp(-eLowJ / (K * val300K)) - Math.exp(-eUpJ / (K * val300K))) * AINT_COEFF_JPL);
	}

	/**
	 * Calculation of aint
	 *
	 * @param aEinstein Aeinstein coefficient
	 * @param freqMHz frequency in MHz
	 * @param igu the igu parameter
	 * @param eLowJ Low energy state in Joule
	 * @param eUpJ High energy state in Joule
	 * @param val300K the partition function value for 300 K
	 * @param part300K the reference temperature
	 * @return the aint
	 */
	public static double calcAint(double aEinstein, double freqMHz, double igu, double eLowJ, double eUpJ,
			double val300K, double part300K) {
		return aEinstein * igu * (Math.exp(-eLowJ / (K * val300K)) - Math.exp(-eUpJ / (K * val300K)))
				/ (Math.pow(freqMHz, 2) * AINT_COEFF_JPL * part300K);
	}

	/**
	 * Computation of the loomis intensity
	 *
	 * @param aint Aeinstein coefficient
	 * @param val300K the partition function value for 300 K
	 * @param part300K the reference temperature
	 * @param tex Excitation temperature
	 * @param partTex value of the partition function for the tex temperature
	 * @param eLowK Low energy state in Kelvin
	 * @param eUpK High energy state in Kelvin
	 * @return the Loomis intensity
	 */
	public static double calcLoomisIntensity(double aint, double val300K, double part300K, double tex, double partTex,
			double eLowK, double eUpK) {
		return aint * part300K / partTex * (Math.exp(-eLowK / (tex)) - Math.exp(-eUpK / (tex)))
				/ (Math.exp(-eLowK / (val300K)) - Math.exp(-eUpK / (val300K)));
	}

	/**
	 * Calculation of colden up --- column density of the upper state [cm-2]
	 *
	 *
	 * @param abcom the abcom parameter
	 * @param colden the colon density parameter
	 * @param iguTrans the igu of the transition
	 * @param eup High energy state
	 * @param tex  Excitation temperature
	 * @param qt the partition function value
	 * @return column density of the upper state [cm-2]
	 */
	public static double calcColdenUp(double abcom, double colden, int iguTrans, double eup, double tex, double qt) {
		return (abcom * abiso * colden * iguTrans * Math.exp(-eup * hcsurk / tex) / qt);
	}

	/**
	 * Calculation of the white noise
	 *
	 * @param noise (K)
	 * @return the white noise
	 */
	public static double calcWhiteNoise(double noise) {
		double result = 0.0;
		final int nbFois = 48;

		double facteur = Math.sqrt(nbFois) / Math.sqrt(Math.PI);
		for (int i = 0; i < nbFois; i++) {
			result = result + (noise * (2.0 * Math.random() - 1.0));
		}
		return result / facteur;
	}

	/**
	 * Compute the frequency corrected by the vlsr
	 *
	 * @param freq Frequency in MHz
	 * @param vlsr offset velocity in km/s
	 * @param freqInter Reference frequency in MHz
	 * @return frequency corrected by the vlsr
	 */
	public static double calcFreqWithVlsr(double freq, double vlsr, double freqInter) {
		return (freq - ((vlsr / CKm) * freqInter));
	}

	/**
	 * Computation of calcLnNuOnGu for the rotational model
	 *
	 * @param nu frequency in MHz
	 * @param aint the aeinstein coeff
	 * @param gu the gu parameter of the transition
	 * @param wTemp W
	 * @return computation of calcLnNuOnGu for the rotational model
	 */
	public static double calcLnNuOnGu(double nu, double aint, double gu, double wTemp) {
		return Math.log(((nu * nu * wTemp) / (aint * gu))* CSTE_ROT_DIAGRAM);
	}

	/**
	 * Computation of calcLnNuOnGu for the rotational model
	 *
	 * @param nu frequency in MHz
	 * @param coeff the coeff parameter
	 * @param wTemp the W
	 * @return computation of calcLnNuOnGu for the rotational model
	 */
	public static double calcLnNuOnGu(double nu, double coeff, double wTemp) {
		return Math.log(((nu * nu * wTemp) / coeff) * CSTE_ROT_DIAGRAM);
	}

	/**
	 * Calculation of the W and the interval when taTmbBox is selected
	 *
	 * @param nu Frequency in MHz
	 * @param frequencyList Frequencies values in MHz
	 * @param effValueList  Efficiency values in MHz
	 * @return calcTaTmb the W
	 */
	public static double calcTaTmb(double nu,
			List<Double> frequencyList, List<Double> effValueList) {
		double pF = 0.0; // previous frequency for this
		double sF = 0.0; // next frequency for this
		double pC = 0.0; // previous correction for this
		double sC = 0.0; // next correction for this

		int indice = -1;
		// search the previous frequency
		for (int i = 1; i < frequencyList.size(); i++) {
			double pF2 = frequencyList.get(i - 1);
			if (pF2 < nu) {
				indice = i - 1;
			}
		}

		if (indice != -1) {
			pF = frequencyList.get(indice).doubleValue();
			sF = frequencyList.get(indice + 1).doubleValue();
			pC = effValueList.get(indice).doubleValue();
			sC = effValueList.get(indice + 1).doubleValue();
		}
		double dF = sF - pF;// dilution frequency
		double dC = sC - pC; // dilution correction
		double dLf = nu - pF;
		double cL = pC + dC * dLf / dF;// the value of correction
		return cL;
	}

	/**
	 * Calculation of the deltaNu which represents the interval of error for the
	 * deltaNu
	 *
	 * @param fwhm Full width half medium value
	 * @param nu frequency in MHz
	 * @return Double
	 */
	public static double calcDeltaNu(double fwhm, double nu) {
		return fwhm * nu / CKm;
	}

	/**
	 * Convert frequency with a lo frequency
	 *
	 * @param frequency  Frequency in MHz
	 * @param loFreq Lo Frequency in MHz
	 * @return convertFreqWithLoFreq
	 */
	public static double convertFreqWithLoFreq(double frequency, double loFreq) {
		return frequency - 2 * (frequency - loFreq);
	}


	/**
	 * Compute the beam extrapolate
	 *
	 * @param frequency Frequency in MHz
	 * @param freqList Frequencies values in MHz
	 * @param effList  Efficiency values in MHz
	 * @return the beam extrapolate
	 */
	public static double getExtrapolateBeam(double frequency, List<Double> freqList, List<Double> effList) {
		if (Math.abs(frequency - freqList.get(0)) < 1.10E-6) {
			return effList.get(0);
		}

		// g, m, d : left, middle, right
		double intermediateValue = 0;
		int g = 0;
		int d = freqList.size() - 1;
		int m = -1;

		while (d - g > 1) {
			m = Math.abs((g + d) / 2);
			intermediateValue = freqList.get(m);
			if (Math.abs(frequency - intermediateValue) < 1.10E-6) {
				break;
			}
			if (frequency < intermediateValue) {
				d = m;
			} else {
				g = m;
			}
		}

		if (Math.abs(frequency - intermediateValue) < 1.10E-6) {
			return effList.get(m);
		}

		double yd = effList.get(d);
		double yg = effList.get(g);
		double xd = freqList.get(d);
		double xg = freqList.get(g);

		return yd + (yd - yg) / (xd - xg) * (frequency - xd);
	}

	/**
	 * Compute the beam size str
	 *
	 * @param freq Frequency in MHz
	 * @param freqencyList Frequencies values in MHz
	 * @param effValueList  Efficiency values in MHz
	 * @return  beam size str
	 */
	public static double getBeamSizeStr(double freq, List<Double> freqencyList, List<Double> effValueList) {
		double beamSizeArcSec = Formula.getExtrapolateBeam(freq, freqencyList, effValueList);
		// Original formula
		//return 2. * Math.PI * (1.0 - Math.cos(beamSizeArcSec * Math.PI/2.0/3600./180.));
		// Optimised formula
		return 2. * Math.PI * (1.0 - Math.cos(beamSizeArcSec * Math.PI / 1296000.));
	}
}
