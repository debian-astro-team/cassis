/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.X_AXIS;
import eu.omp.irap.cassis.common.axes.YAxisCassis;

/**
 * This class defines a structure that includes a spectrum as a list of channels
 *   and a list of LineDescription.
 */
public class CommentedSpectrum {

	public static final int OVERSAMPLING_LIMIT = 50;

	private String id;

	private List<LineDescription> listOfLines;
	private String title = "";

	private double frequencySignalMin = Double.MAX_VALUE; // Mhz
	private double intensityMin = Double.MAX_VALUE; // dTa (K)
	private double frequencySignalMax = Double.NEGATIVE_INFINITY; // Mhz
	private double intensityMax = Double.NEGATIVE_INFINITY; // dTa (K)

	protected double vlsr;
	protected double vlsrModel;
	private double freqRef;
	private double loFreq;

	private XAxisCassis xAxis = XAxisCassis.getXAxisFrequency();
	private YAxisCassis yAxis = YAxisCassis.getYAxisKelvin();

	private double[] frequenciesSignal;
	private double[] intensities;

	private TypeFrequency typeFreq = TypeFrequency.REST;

	private double[] intensitiesInv;

	private List<CassisMetadata> originalMetadataList;
	private List<CassisMetadata> cassisMetadataList;


	/**
	 * Constructor makes a commentedSpectrum.
	 */
	public CommentedSpectrum() {
		this.id = "";
		this.loFreq = Double.NaN;
		setListOfLines(new ArrayList<LineDescription>());
		setListOfChannels(new ArrayList<ChannelDescription>());
		originalMetadataList = new ArrayList<CassisMetadata>();
		cassisMetadataList = new ArrayList<CassisMetadata>();
	}

	/**
	 * Constructor makes a commentedSpectrum giving title, lines, and channels.
	 *
	 * @param listOfLines The list of lines.
	 * @param listOfChannels The list of channels.
	 * @param title The spectrum's title.
	 */
	public CommentedSpectrum(List<LineDescription> listOfLines, List<ChannelDescription> listOfChannels, String title) {
		this.id = "";
		this.loFreq = Double.NaN;
		setListOfLines(listOfLines);
		setListOfChannels(listOfChannels);
		originalMetadataList = new ArrayList<CassisMetadata>();
		cassisMetadataList = new ArrayList<CassisMetadata>();
		this.title = title;
	}

	/**
	 * Constructor makes a commentedSpectrum giving title, lines, and frequencies/intensities.
	 *
	 * @param listOfLines The list of lines.
	 * @param frequencies The frequencies in MHz
	 * @param intensities The intensities in Kelvin
	 * @param title The spectrum's title.
	 */
	public CommentedSpectrum(List<LineDescription> listOfLines, double[] frequencies, double[] intensities,
			String title) {
		super();
		this.loFreq = Double.NaN;
		this.id = title;
		setListOfLines(listOfLines);
		setListOfChannels(frequencies, intensities);
		originalMetadataList = new ArrayList<CassisMetadata>();
		cassisMetadataList = new ArrayList<CassisMetadata>();
		this.title = title;

		xAxis.setUnit(UNIT.MHZ);
	}

	/**
	 * Return the spctrum's ID.
	 *
	 * @return the spctrum's ID.
	 */
	public String getId() {
		return id;
	}

	/**
	 * Change the spectrum's ID.
	 *
	 * @param id The new spectrum's ID.
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Return the list of lines.
	 *
	 * @return the list of lines
	 */
	public List<LineDescription> getListOfLines() {
		return listOfLines;
	}

	/**
	 * Change the list of lines.
	 *
	 * @param listOfLines The new list of lines
	 */
	public void setListOfLines(List<LineDescription> listOfLines) {
		this.listOfLines = listOfLines;
	}

	/**
	 * Change the list of channels.
	 *
	 * @param listOfChannels The new list of channels
	 */
	public void setListOfChannels(List<ChannelDescription> listOfChannels) {
		if (listOfChannels != null && !listOfChannels.isEmpty()) {
			ChannelDescription channel = null;
			double freqTemp = 0., currentIntensity = 0.;
			this.frequenciesSignal = new double[listOfChannels.size()];
			this.intensities = new double[listOfChannels.size()];
			this.intensityMax = Double.NEGATIVE_INFINITY;
			this.intensityMin = Double.MAX_VALUE;
			this.frequencySignalMax = Double.NEGATIVE_INFINITY;
			this.frequencySignalMin = Double.MAX_VALUE;

			for (int index = 0; index < listOfChannels.size(); index++) {
				channel = listOfChannels.get(index);
				freqTemp = channel.getFrequencySignal();
				currentIntensity = channel.getIntensity();
				this.frequenciesSignal[index] = freqTemp;
				this.intensities[index] = currentIntensity;
				if (this.frequencySignalMax < freqTemp)
					this.frequencySignalMax = freqTemp;

				if (this.intensityMax < currentIntensity)
					this.intensityMax = currentIntensity;

				if (this.frequencySignalMin > freqTemp)
					this.frequencySignalMin = freqTemp;

				if (this.intensityMin > currentIntensity)
					this.intensityMin = currentIntensity;
			}
		}
	}

	/**
	 * Change the list of channels.
	 *
	 * @param freqs The frquencies in MHz.
	 * @param intensities The intensities.
	 */
	public void setListOfChannels(double[] freqs, double[] intensities) {
		if ((freqs != null) && (freqs.length != 0)) {
			double currentFreqSignal, currentIntensity;
			this.frequenciesSignal = Arrays.copyOf(freqs, freqs.length);
			this.intensities = Arrays.copyOf(intensities, intensities.length);
			this.intensityMax = Double.NEGATIVE_INFINITY;
			this.intensityMin = Double.MAX_VALUE;
			this.frequencySignalMax = Double.NEGATIVE_INFINITY;
			this.frequencySignalMin = Double.MAX_VALUE;
			for (int index = 0; index < freqs.length; index++) {
				currentFreqSignal = freqs[index];
				currentIntensity = intensities[index];

				if (this.frequencySignalMax < currentFreqSignal)
					this.frequencySignalMax = currentFreqSignal;

				if (this.intensityMax < currentIntensity)
					this.intensityMax = currentIntensity;

				if (this.frequencySignalMin > currentFreqSignal)
					this.frequencySignalMin = currentFreqSignal;

				if (this.intensityMin > currentIntensity)
					this.intensityMin = currentIntensity;
			}
		}
	}

	/**
	 * Change the frequency and reset the intensities
	 *
	 * @param freqs The frequencies in MHz.
	 */
	public void setFrequencies(double[] freqs) {
		if ((freqs != null) && (freqs.length != 0)) {
			double currentFrequencySignal = 0.;
			this.frequenciesSignal = freqs;
			this.frequencySignalMax = Double.NEGATIVE_INFINITY;
			this.frequencySignalMin = Double.MAX_VALUE;
			this.intensities = new double[freqs.length];
			for (int index = 0; index < freqs.length; index++) {
				currentFrequencySignal = freqs[index];

				if (this.frequencySignalMax < currentFrequencySignal)
					this.frequencySignalMax = currentFrequencySignal;

				if (this.frequencySignalMin > currentFrequencySignal)
					this.frequencySignalMin = currentFrequencySignal;

			}
			this.intensityMax = 0.;
			this.intensityMin = 0.;
		}
	}

	/**
	 * Change the entensities.
	 *
	 * @param intensities The new intensities.
	 */
	public void setIntensities(double[] intensities) {
		if ((this.frequenciesSignal != null) && (this.frequenciesSignal.length == intensities.length)) {
			double currentIntensity = 0.;
			this.intensities = intensities;
			this.intensityMax = Double.NEGATIVE_INFINITY;
			this.intensityMin = Double.MAX_VALUE;
			for (int index = 0; index < frequenciesSignal.length; index++) {
				currentIntensity = intensities[index];

				if (this.intensityMax < currentIntensity)
					this.intensityMax = currentIntensity;

				if (this.intensityMin > currentIntensity)
					this.intensityMin = currentIntensity;
			}
		}
	}

	/**
	 * Return the title.
	 *
	 * @return title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Change the title.
	 *
	 * @param title
	 *            new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Compute dsb specrtum.
	 *
	 * @param spectrum The spectrum.
	 * @param dsbValue The dsb value.
	 * @param dsbSpectrum The dsb spectrum.
	 * @param usbList The usb list.
	 * @param lsbList The lsd list.
	 * @param frequencyList The frequency list.
	 * @param mode The mode : 0 for simple DSBCompute = 1 for absorptionDSBCompute.
	 * @return The dsb spectrum with corrected frequencies/intensities.
	 * @throws Exception In case of error...
	 */
	public static CommentedSpectrum dsbCompute(CommentedSpectrum spectrum, double dsbValue,
			CommentedSpectrum dsbSpectrum, List<Double> usbList, List<Double> lsbList,
			List<Double> frequencyList, int mode) throws Exception {
		// multiply current spectrum and dsbSpectrum by lsb/usb list
		double[] dsbFreq = dsbSpectrum.getFrequenciesSignal();
		double[] dsbIntensities = dsbSpectrum.getIntensities();

		double[] freq = spectrum.getFrequenciesSignal();
		double[] intensities = spectrum.getIntensities();
		int size = dsbSpectrum.getSize();

		List<Double> dsbList = lsbList;
		if (dsbValue == 1)
			dsbList = usbList;

		for (int i = 0; i < size; i++) {
			dsbIntensities[i] = dsbIntensities[i] * UtilArrayList.getArrayValueAt(dsbFreq[i], dsbList, frequencyList);
		}

		// TODO check if not invert gain (usbList, lsblist)
		for (int i = 0; i < size; i++) {
			intensities[i] = intensities[i] * UtilArrayList.getArrayValueAt(freq[i], dsbList, frequencyList);
		}

		// invert usb spectrum list of channels
		// UtilArrayList.reverseArrayList(dsbChannels);

		// merge new spectrum channels with current spectrum
		if (mode == 1) {
			 // Absorption mode
			for (int i = 0; i < size; i++) {
				double currentChannelInt = intensities[i];
				double dsbChannelInt = dsbIntensities[size - i - 1];

				if (dsbChannelInt == 0)
					dsbChannelInt = 1;

				intensities[i] = (currentChannelInt / 2) + (dsbChannelInt / 2);
			}
		} else {
			// normal ?
			for (int i = 0; i < size; i++) {
				try {
					double currentChannelInt = intensities[i];
					double dsbChannelInt = dsbIntensities[size - i - 1];
					intensities[i] = currentChannelInt + dsbChannelInt;
				} catch (Exception e) {// they are less dsbspectrumChannel than
										// spectrumChannels
										//					logger.error("there are less dsbspectrum Channels than spectrum Channels");
				}
			}
		}
		spectrum.setListOfChannels(freq, intensities);
		return spectrum;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	/**
	 * Return String info of the object.
	 */
	@Override
	public String toString() {
		String nblines = "0";
		if (listOfLines != null)
			nblines = String.valueOf(listOfLines.size());

		return "nbLines = " + nblines + " nbChannels = " + getSize() + " title = " + title;
	}

	/**
	 * Return the maximum signal frequency.
	 *
	 * @return the maximum signal frequency.
	 */
	public double getFrequencySignalMax() {
		return frequencySignalMax;
	}

	/**
	 * Return the minimum signal frequency.
	 *
	 * @return the minimum signal frequency.
	 */
	public double getFrequencySignalMin() {
		return frequencySignalMin;
	}

	/**
	 * Return the maximum intensity.
	 *
	 * @return the maximum intensity.
	 */
	public double getIntensityMax() {
		return intensityMax;
	}

	/**
	 * Return the minimum intensity.
	 *
	 * @return the minimum intensity.
	 */
	public double getIntensityMin() {
		return intensityMin;
	}

	/**
	 * Return the vlsr.
	 *
	 * @return the vlsr.
	 */
	public double getVlsr() {
		return vlsr;
	}

	/**
	 * Return the vlsr model (displayed in infopanel).
	 *
	 * @return the vlsr model.
	 */
	public double getVlsrModel() {
		return vlsrModel;
	}

	/**
	 * Change the vlsr model (displayed in infopanel).
	 *
	 * @param val The new vlsr model.
	 */
	public void setVlsrModel(double val) {
		vlsrModel = val;
	}

	/**
	 * Change the vlsr and vlsr model.
	 *
	 * @param val The new vlsr.
	 */
	public void setVlsr(double val) {
		vlsr = val;
		boolean find = false;
		List<CassisMetadata> cassisMetadataList = getCassisMetadataList();
		if (cassisMetadataList == null){
			cassisMetadataList = new ArrayList<CassisMetadata>();
		}
		for (CassisMetadata cassisMetadata : cassisMetadataList) {
			if (cassisMetadata.getName().equals("vlsr")){
				cassisMetadata.setValue(String.valueOf(val));
				find = true;
			}
		}
		if (!find){
			cassisMetadataList.add(new CassisMetadata("vlsr", String.valueOf(val),
					"Velocity", String.valueOf(UNIT.KM_SEC_MOINS_1)));
		}
		vlsrModel = val;
	}

	/**
	 * Return the intensities.
	 *
	 * @return the intensities.
	 */
	public double[] getIntensities() {
		return intensities;
	}

	/**
	 * Return the intensities in reversed order.
	 *
	 * @return the intensities in reversed order.
	 */
	public double[] getIntensitiesInv() {
		if (intensitiesInv == null) {
			int size = this.intensities.length;
			intensitiesInv = new double[size];

			for (int i = 0; i < size; i++) {
				intensitiesInv[size - i - 1] = this.intensities[i];
			}
		}
		return intensitiesInv;
	}

	/**
	 * Return the signal frequencies.
	 *
	 * @return the signal frequencies.
	 */
	public double[] getFrequenciesSignal() {
		return frequenciesSignal;
	}

	/**
	 * Return the lo frequency.
	 *
	 * @return the lo frequency.
	 */
	public final double getLoFreq() {
		return loFreq;
	}

	/**
	 * Change the lo frequency.
	 *
	 * @param loFreq The loFreq to set.
	 */
	public final void setLoFreq(double loFreq) {
		if (Double.isNaN(loFreq) || loFreq == 0) {
			this.loFreq = Double.NaN;
		} else {
			this.loFreq = loFreq;
		}
	}

	/**
	 * Change the x axis of origin of the spectrum.
	 *
	 * @param xAxisOrigin The xAxis of origin to set.
	 */
	public final void setxAxisOrigin(XAxisCassis xAxisOrigin) {
		this.xAxis = xAxisOrigin;
	}

	/**
	 * Return the origin x axis of the spectrum.
	 *
	 * @return the origin x axis of the spectrum.
	 */
	public final XAxisCassis getxAxisOrigin() {
		return xAxis;
	}

	/**
	 * Change the y axis.
	 *
	 * @param yAxis The yAxis to set.
	 */
	public void setyAxis(YAxisCassis yAxis) {
		this.yAxis = yAxis;
	}

	/**
	 * Return the y axis.
	 *
	 * @return the y axis.
	 */
	public YAxisCassis getyAxis() {
		return yAxis;
	}

	/**
	 * Return the x data for the given axis.
	 *
	 * @param xAxis The x axis.
	 * @return The x data in the unit of the x axis.
	 */
	public double[] getXData(XAxisCassis xAxis) {
		double[] val = getFrequenciesSignal();
		if (xAxis.getAxis().equals(X_AXIS.FREQUENCY_SIGNAL) && xAxis.getUnit().equals(UNIT.MHZ))
			return val;

		double[] res = new double[val.length];
		if (xAxis.isInverted()) {
			for (int cpt = 0; cpt < val.length; cpt++) {
				res[cpt] = xAxis.convertFromMhzFreq(val[val.length - 1 - cpt]);
			}
		}
		else {
			for (int cpt = 0; cpt < val.length; cpt++) {
				res[cpt] = xAxis.convertFromMhzFreq(val[cpt]);
			}
		}
		return res;
	}

	//TODO replace xAxis with boolean inverted
	/**
	 * Return the intensity for the given x axis as it may be reversed following the axis.
	 *
	 * @param xAxis The x axis.
	 * @return The intensities.
	 */
	public double[] getIntensities(XAxisCassis xAxis) {
		if (xAxis.isInverted())
			return getIntensitiesInv();
		else
			return getIntensities();
	}

	/**
	 *
	 * @param convertFluxAxis the unit we want
	 * @param inverted, to know if the data must be returned or not
	 * @return the intensities in the unit you want
	 */
	public double[] getIntensities(YAxisCassis convertFluxAxis, boolean inverted) {
		double[] convert = YAxisCassis.convert(intensities, yAxis, convertFluxAxis, frequenciesSignal, getCassisMetadataList());
		if (inverted) {
			convert = UtilArrayList.reverse(convert);
		}
		return convert;
	}

	/**
	 * Return the intensities of origin.
	 *
	 * @return the intensities of origin.
	 */
	public double[] getYOrigin() {
		double[] res;
		if (xAxis.isInverted()) {
			res = getIntensitiesInv();
		} else {
			res = getIntensities();
		}
		return res;
	}




	/**
	 * Return the deltas for the spectrum in MHz.
	 *
	 * @return the delta for the spectrum in MHz.
	 */
	public double[] getDeltaF() {
		if (getSize() < 2) {
			return new double[0];
		}
		// Get center values.
		double[] centerValues = new double[getSize() + 1];
		centerValues[0] = frequenciesSignal[0] - (Math.abs(frequenciesSignal[1] - frequenciesSignal[0]) / 2);
		for (int i = 0; i < getSize() - 1; i++) {
			centerValues[i+1] = frequenciesSignal[i] + (Math.abs(frequenciesSignal[i + 1] - frequenciesSignal[i]) / 2);
		}
		centerValues[centerValues.length - 1] = frequenciesSignal[getSize() - 1] + (Math.abs(frequenciesSignal[getSize() - 1] - frequenciesSignal[getSize() - 2]) / 2);

		// Compute the diff to get the delta in MHz.
		double[] deltasMhz = new double[getSize()];
		for (int i = 0; i < getSize(); i++) {
			deltasMhz[i] = Math.abs(centerValues[i + 1] - centerValues[i]);
		}
		return deltasMhz;
	}

	public double[] getDelta(XAxisCassis xAxis) {
		double[] delta;
		if (xAxis.getAxis().equals(X_AXIS.FREQUENCY_SIGNAL) && xAxis.getUnit().equals(UNIT.MHZ)) {
			delta = getDeltaF();
		} else if (xAxis.getAxis().equals(X_AXIS.VELOCITY_SIGNAL) && xAxis.getUnit().equals(UNIT.KM_SEC_MOINS_1)) {
			delta = getDeltaV();
		} else {
			if (getSize() < 2) {
				return new double[0];
			}

			double[] convAxis = getXData(xAxis);

			double[] centerValues = new double[getSize() + 1];
			centerValues[0] = convAxis[0] - (Math.abs(convAxis[1] - convAxis[0])/ 2);
			for (int i = 0; i < getSize() - 1; i++) {
				centerValues[i + 1] = convAxis[i] + (Math.abs(convAxis[i + 1] - convAxis[i]) / 2);
			}
			centerValues[centerValues.length - 1] = convAxis[getSize() - 1] + (Math.abs(convAxis[getSize() - 1] - convAxis[getSize() - 2]) / 2);

			delta = new double[getSize()];
			for (int i = 0; i < getSize(); i++) {
				delta[i] = Math.abs(centerValues[i + 1] - centerValues[i]);
			}
			if (xAxis.isInverted()) {
				double tmp;
				for (int i = 0; i < delta.length / 2; i++) {
					tmp = delta[i];
					delta[i] = delta[delta.length - i - 1];
					delta[delta.length - i - 1] = tmp;
				}
			}
		}
		return delta;
	}

	/**
	 * Return the number of channels.
	 *
	 * @return the number of channels.
	 */
	public int getSize() {
		return frequenciesSignal.length;
	}

	/**
	 * Change the reference frequency.
	 *
	 * @param freqRef The reference frequency to set.
	 */
	public void setFreqRef(double freqRef) {
		this.freqRef = freqRef;
	}

	/**
	 * Return the reference frequency.
	 *
	 * @return  the reference frequency.
	 */
	public double getFreqRef() {
		return freqRef;
	}

	/**
	 * Return the list of channels.
	 *
	 * @return the list of channels.
	 */
	public List<ChannelDescription> getListOfChannels() {
		List<ChannelDescription> channelDescriptions = new ArrayList<ChannelDescription>(getSize());
		for (int cpt = 0; cpt < frequenciesSignal.length; cpt++) {
			channelDescriptions.add(new ChannelDescription(frequenciesSignal[cpt], 0., intensities[cpt]));
		}
		return channelDescriptions;
	}

	/**
	 * Return the velocity signals.
	 *
	 * @return the velocity signals.
	 */
	public double[] getVelocitySignal() {
		int size = getSize();
		double[] velocity = new double[size];
		for (int index = 0; index < size; index++) {
			velocity[index] = Formula.calcVelocity(frequenciesSignal[size - index - 1], freqRef, vlsr);
		}

		return velocity;
	}

	/**
	 * Return the array of  deltaV.
	 *
	 * @return the array of  deltaV.
	 */
	public double[] getDeltaV() {
		double[] delta = getDeltaF();
		for (int i = 0; i < delta.length; i++) {
			delta[i] = Formula.calcDVForFile(delta[i], frequenciesSignal[i]);
		}
		return delta;
	}

	/**
	 * Create hashCode.
	 *
	 * @return the hashCode.
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(frequenciesSignal);
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + Arrays.hashCode(intensities);
		result = prime * result + Arrays.hashCode(intensitiesInv);
		result = prime * result
				+ ((listOfLines == null) ? 0 : listOfLines.hashCode());
		long temp = Double.doubleToLongBits(loFreq);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((xAxis == null) ? 0 : xAxis.hashCode());
		result = prime * result + ((yAxis == null) ? 0 : yAxis.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	/**
	 * Test the equality with the given object.
	 */
	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if (this == obj)
			return true;

		if ((obj == null) || (obj.getClass() != this.getClass()))
			return false;

		CommentedSpectrum comSpec = (CommentedSpectrum) obj;

		result = ((this.id == comSpec.id) || (this.id.equals(comSpec.id))) &&

		((this.listOfLines == comSpec.listOfLines) || ((this.listOfLines).equals(comSpec.listOfLines))) &&

		((this.title == comSpec.title) || (this.title.equals(comSpec.title)));

		return result;
	}

	/**
	 * Change the {@link TypeFrequency}.
	 *
	 * @param typeFreq The new {@link TypeFrequency} to set.
	 */
	public void setTypeFreq(TypeFrequency typeFreq) {
		this.typeFreq = typeFreq;
	}

	/**
	 * Return the {@link TypeFrequency} of the spectrum.
	 *
	 * @return the {@link TypeFrequency} of the spectrum.
	 */
	public TypeFrequency getTypeFreq() {
		return this.typeFreq;
	}


	/**
	 * Compress the commentedSpectrum.
	 *
	 * @param keepFirst The index where to start to compress.
	 * @param keepLast The index where to stop to compress.
	 */
	public void compress(int keepFirst, int keepLast) {
		if (getSize() < keepFirst + keepLast -1)
			return;
		int newSize = getSize();
		double[] newFreqs = new double[newSize];
		double[] newIntensity = new double[newSize];
		int currentSize = 0;

		for (int i = 0; i < keepFirst; i++) {
			newFreqs[currentSize] = frequenciesSignal[currentSize];
			newIntensity[currentSize] = intensities[currentSize];
			currentSize++;
		}

		double previousIntensity = newIntensity[keepFirst-1];
		double currentIntensity = intensities[keepFirst];
		double currentFreq =  frequenciesSignal[keepFirst];
		for (int i = keepFirst+1; i < newSize-keepLast+1;i++) {
			double nextIntensity = intensities[i];
			double nextFreq = frequenciesSignal[i];
			if (Math.abs(currentIntensity-previousIntensity) > 1E-12 ||
				Math.abs(currentIntensity-nextIntensity) > 1E-12) {
				newFreqs[currentSize] = currentFreq;
				newIntensity[currentSize] =currentIntensity;
				currentSize++;
			}

			previousIntensity = currentIntensity;
			currentIntensity = nextIntensity;
			currentFreq =  nextFreq;
		}

		for (int i = keepLast; i > 0; i--) {
			newFreqs[currentSize] = frequenciesSignal[newSize-i];
			newIntensity[currentSize] = intensities[newSize-i];
			currentSize++;
		}

		this.frequenciesSignal = Arrays.copyOf(newFreqs, currentSize);
		this.intensities  = Arrays.copyOf(newIntensity, currentSize);
	}

	/**
	 * Return if the sampling is regular.
	 *
	 * @return if the sampling is regular.
	 */
	public boolean isSamplingRegular() {
		int nbPoint = frequenciesSignal.length;
		if (nbPoint < 3) {
			return true;
		}
		double diff = frequenciesSignal[1] - frequenciesSignal[0];
		for (int i = 2; i < nbPoint; i++) {
			if (diff != frequenciesSignal[i] - frequenciesSignal[i-1]) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Getter for {@link CassisMetadata} list
	 *
	 * @return The {@link CassisMetadata} list
	 */
	public List<CassisMetadata> getCassisMetadataList() {
		return cassisMetadataList;
	}


	/**
	 * Getter the {@link CassisMetadata}  from cassisMetadataList
	 * @param name the name of the CassisMetadata to search
	 * @return The {@link CassisMetadata} value or null if not exist
	 */
	public CassisMetadata getCassisMetadata(String name) {

		CassisMetadata res= null ;
		for (int i = 0; i < cassisMetadataList.size() && res == null; i++) {
			CassisMetadata cassisMetadata = cassisMetadataList.get(i);
			if (cassisMetadata.getName().equals(name)){
				res = cassisMetadata;
			}
		}
		return res;
	}

	/**
	 * Getter for original {@link CassisMetadata} list
	 *
	 * @return The original {@link CassisMetadata} list
	 */
	public List<CassisMetadata> getOriginalMetadataList() {
		return originalMetadataList;
	}

	/**
	 * Setter for change the {@link CassisMetadata} list
	 *
	 * @param cassisMetadataList
	 *            The new {@link CassisMetadata} list
	 */
	public void setCassisMetadataList(List<CassisMetadata> cassisMetadataList) {
		if (cassisMetadataList == null) {
			cassisMetadataList = new ArrayList<CassisMetadata>();
		}
		this.cassisMetadataList = cassisMetadataList;
	}

	/**
	 * Setter for change the original {@link CassisMetadata} list
	 *
	 * @param originalMetadataList
	 *            The new original {@link CassisMetadata} list
	 */
	public void setOriginalMetadataList(List<CassisMetadata> originalMetadataList) {
		if (originalMetadataList == null) {
			originalMetadataList = new ArrayList<CassisMetadata>();
		}
		this.originalMetadataList = originalMetadataList;
		this.originalMetadataList = originalMetadataList;
	}
}
