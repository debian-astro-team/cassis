/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.common;

import java.util.Arrays;
import java.util.List;

/**
 * @author Laure Tamisier this class permits to work with an ArrayList like a
 *         list
 */
public class UtilArrayList {

	/**
	 * Return the value at the given frequency
	 *
	 * @param searchValue frequency to search in the frequencyList
	 * @param searchList list of value  in fucntion of frequencyList
	 * @param frequencyList the list of the frequencies
	 * @return value at the given frequency
	 */
	public static double getArrayValueAt(final double searchValue,
			final List<Double> searchList, List<Double> frequencyList) {
		double value = 0;
		double difference = Double.MAX_VALUE;
		double tmpDiff;
		int index = -1;

		if (!frequencyList.isEmpty()) {
			if (searchValue >= frequencyList.get(0)
					&& (searchValue <= frequencyList
							.get(frequencyList.size() - 1))) {
				for (int i = 0; i < frequencyList.size(); i++) {
					tmpDiff = Math.abs(searchValue - frequencyList.get(i));
					if (tmpDiff < difference) {
						index = i;
						difference = tmpDiff;
					}
				}
			} else {
				throw new IndexOutOfBoundsException(
						"The frequency "
								+ searchValue
								+ " MHz is out of range for the parameters of the telescope.");
			}
		}

		if (index != -1) {
			value = searchList.get(index);
		} else {
			throw new IndexOutOfBoundsException(
					"The frequency "
							+ searchValue
							+ " MHz is out of range for the parameters of the telescope.");

		}

		return value;
	}

	/**
	 * Retrun the index of the arrays freqs and aints that is the nearest of
	 * the freq0 value.
	 *
	 * @param freqs
	 *            an array of frequencies
	 * @param freq0
	 *            a value to search
	 * @param index1
	 *            the minimum index value to search in the array freqs
	 * @param index2
	 *            the maximum index value to search in the array freqs
	 * @return index found
	 */
	public static int quickSearchIndex(double[] freqs, double freq0,
			int index1, int index2) {
		int kfreqmin, kfreqmax, kfreq;
		double freq;

		// research by dichotomy
		kfreqmin = index1;
		kfreqmax = index2;
		kfreq = (kfreqmin + kfreqmax) >>> 1;
		while ((kfreq != kfreqmin) && (kfreq != kfreqmax)) {
			freq = freqs[kfreq];

			if (freq0 > freq)
				kfreqmin = kfreq;

			else
				kfreqmax = kfreq;

			kfreq = (kfreqmin + kfreqmax) >>> 1;
		}
		return kfreq;
	}

	/**
	 * Tools to sort an array of (double) and returns also the sorted indexes.
	 *
	 * @param arr
	 *            contains the array of (double) to sort
	 * @param indexes
	 *            contains the array of (int) associated to arr
	 * @param low
	 *            is the first index of the array of (double)
	 * @param high
	 *            is the last index of the array of (double)
	 */
	public static void quicksortindex(double[] arr, int[] indexes, int low,
			int high) {
		int i = low;
		int j = high;
		double y = 0;
		int yy = 0;

		// compare value
		double z = arr[(low + high) >>> 1];

		// partition
		do {
			// find member above
			while (arr[i] < z)
				i++;
			// find element below
			while (arr[j] > z)
				j--;
			if (i <= j) {
				// swap two elements
				y = arr[i];
				arr[i] = arr[j];
				arr[j] = y;
				yy = indexes[i];
				indexes[i] = indexes[j];
				indexes[j] = yy;
				i++;
				j--;
			}
		} while (i <= j);
		// recurse
		if (low < j)
			quicksortindex(arr, indexes, low, j);

		if (i < high)
			quicksortindex(arr, indexes, i, high);
	}

	public static void quicksortTab2(double[] arr, double[] arr2, int begin, int last) {
		sort2Tab2(arr, arr2,  begin, last+1);
	}

	/**
	 * From Arrays JDK ...
	 *
	 * @param a
	 * @param b
	 * @param c
	 * @param d
	 * @param fromIndex
	 * @param toIndex
	 */
	private static void sort2Tab2(double[] a, double[] b,
			int fromIndex, int toIndex) {
		final long NEG_ZERO_BITS = Double.doubleToLongBits(-0.0d);
		/*
		 * The sort is done in three phases to avoid the expense of using NaN
		 * and -0.0 aware comparisons during the main sort.
		 */

		/*
		 * Preprocessing phase: Move any NaN's to end of array, count the number
		 * of -0.0's, and turn them into 0.0's.
		 */
		int numNegZeros = 0;
		int i = fromIndex, n = toIndex;
		while (i < n) {
			if (a[i] != a[i]) {
				double swap = a[i];
				a[i] = a[--n];
				a[n] = swap;
			} else {
				if (a[i] == 0 && Double.doubleToLongBits(a[i]) == NEG_ZERO_BITS) {
					a[i] = 0.0d;
					numNegZeros++;
				}
				i++;
			}
		}

		// Main sort phase: quicksort everything but the NaN's
		sort1Tab2(a, b,fromIndex, n - fromIndex);

		// Postprocessing phase: change 0.0's to -0.0's as required
		if (numNegZeros != 0) {
			int j = binarySearch0(a, fromIndex, n, 0.0d); // posn of ANY zero
			do {
				j--;
			} while (j >= 0 && a[j] == 0.0d);

			// j is now one less than the index of the FIRST zero
			for (int k = 0; k < numNegZeros; k++)
				a[++j] = -0.0d;
		}
	}

	/**
	 * From Arrays JDK ...
	 * Sorts the specified sub-array of doubles into ascending order.
	 */
	private static void sort1Tab2 (double[] x, double[] x2, int off, int len) {
		// Insertion sort on smallest arrays
		if (len < 7) {
			for (int i = off; i < len + off; i++)
				for (int j = i; j > off && x[j - 1] > x[j]; j--)
					swapTab2(x, x2, j, j - 1);
			return;
		}

		// Choose a partition element, v
		int m = off + (len >> 1); // Small arrays, middle element
		if (len > 7) {
			int l = off;
			int n = off + len - 1;
			if (len > 40) { // Big arrays, pseudomedian of 9
				int s = len / 8;
				l = med3(x, l, l + s, l + 2 * s);
				m = med3(x, m - s, m, m + s);
				n = med3(x, n - 2 * s, n - s, n);
			}
			m = med3(x, l, m, n); // Mid-size, med of 3
		}
		double v = x[m];

		// Establish Invariant: v* (<v)* (>v)* v*
		int a = off, b = a, c = off + len - 1, d = c;
		while (true) {
			while (b <= c && x[b] <= v) {
				if (x[b] == v)
					swapTab2(x, x2, a++, b);
				b++;
			}
			while (c >= b && x[c] >= v) {
				if (x[c] == v)
					swapTab2(x, x2, c, d--);
				c--;
			}
			if (b > c)
				break;
			swapTab2(x, x2, b++, c--);
		}

		// Swap partition elements back to middle
		int s, n = off + len;
		s = Math.min(a - off, b - a);
		vecswapTab2(x, x2,off, b - s, s);
		s = Math.min(d - c, n - d - 1);
		vecswapTab2(x, x2, b, n - s, s);

		// Recursively sort non-partition-elements
		if ((s = b - a) > 1)
			sort1Tab2(x, x2, off, s);
		if ((s = d - c) > 1)
			sort1Tab2(x, x2, n - s, s);
	}

	/**
	 * Swaps x[a] with x[b].
	 */
	private static void swapTab2(double[] x, double[] x2, int a, int b) {
		double t = x[a];
		x[a] = x[b];
		x[b] = t;

		t = x2[a];
		x2[a] = x2[b];
		x2[b] = t;
	}

	/**
	 * Swaps x[a .. (a+n-1)] with x[b .. (b+n-1)].
	 */
	private static void vecswapTab2(double[] x, double[] x2, int a, int b, int n) {
		for (int i = 0; i < n; i++, a++, b++)
			swapTab2(x, x2, a, b);
	}

	public static void quicksortindex2(double[] arr, double[] arr2,
			double[] arr3, double[] arr4, double[] arr5, int[] arr6, int begin, int last) {

		sort2(arr, arr2, arr3, arr4, arr5, arr6, begin, last+1);
	}

	/**
	 * From Arrays JDK ...
	 *
	 * @param a
	 * @param b
	 * @param c
	 * @param d
	 * @param fromIndex
	 * @param toIndex
	 */
	private static void sort2(double[] a, double[] b, double[] c, double[] d, double[] e, int[] f,
			int fromIndex, int toIndex) {
		final long NEG_ZERO_BITS = Double.doubleToLongBits(-0.0d);
		/*
		 * The sort is done in three phases to avoid the expense of using NaN
		 * and -0.0 aware comparisons during the main sort.
		 */

		/*
		 * Preprocessing phase: Move any NaN's to end of array, count the number
		 * of -0.0's, and turn them into 0.0's.
		 */
		int numNegZeros = 0;
		int i = fromIndex, n = toIndex;
		while (i < n) {
			if (a[i] != a[i]) {
				double swap = a[i];
				a[i] = a[--n];
				a[n] = swap;
			} else {
				if (a[i] == 0 && Double.doubleToLongBits(a[i]) == NEG_ZERO_BITS) {
					a[i] = 0.0d;
					numNegZeros++;
				}
				i++;
			}
		}

		// Main sort phase: quicksort everything but the NaN's
		sort1(a, b, c, d, e, f,fromIndex, n - fromIndex);

		// Postprocessing phase: change 0.0's to -0.0's as required
		if (numNegZeros != 0) {
			int j = binarySearch0(a, fromIndex, n, 0.0d); // posn of ANY zero
			do {
				j--;
			} while (j >= 0 && a[j] == 0.0d);

			// j is now one less than the index of the FIRST zero
			for (int k = 0; k < numNegZeros; k++)
				a[++j] = -0.0d;
		}
	}

	/**
	 * From Arrays JDK ...
	 * Sorts the specified sub-array of doubles into ascending order.
	 */
	private static void sort1(double[] x, double[] x2, double[] x3,
			double[] x4, double[] x5, int[] x6, int off, int len) {
		// Insertion sort on smallest arrays
		if (len < 7) {
			for (int i = off; i < len + off; i++)
				for (int j = i; j > off && x[j - 1] > x[j]; j--)
					swap(x, x2, x3, x4, x5, x6, j, j - 1);
			return;
		}

		// Choose a partition element, v
		int m = off + (len >> 1); // Small arrays, middle element
		if (len > 7) {
			int l = off;
			int n = off + len - 1;
			if (len > 40) { // Big arrays, pseudomedian of 9
				int s = len / 8;
				l = med3(x, l, l + s, l + 2 * s);
				m = med3(x, m - s, m, m + s);
				n = med3(x, n - 2 * s, n - s, n);
			}
			m = med3(x, l, m, n); // Mid-size, med of 3
		}
		double v = x[m];

		// Establish Invariant: v* (<v)* (>v)* v*
		int a = off, b = a, c = off + len - 1, d = c;
		while (true) {
			while (b <= c && x[b] <= v) {
				if (x[b] == v)
					swap(x, x2, x3, x4,x5, x6, a++, b);
				b++;
			}
			while (c >= b && x[c] >= v) {
				if (x[c] == v)
					swap(x, x2, x3, x4, x5, x6,c, d--);
				c--;
			}
			if (b > c)
				break;
			swap(x, x2, x3, x4, x5, x6, b++, c--);
		}

		// Swap partition elements back to middle
		int s, n = off + len;
		s = Math.min(a - off, b - a);
		vecswap(x, x2, x3, x4, x5, x6,off, b - s, s);
		s = Math.min(d - c, n - d - 1);
		vecswap(x, x2, x3, x4, x5, x6, b, n - s, s);

		// Recursively sort non-partition-elements
		if ((s = b - a) > 1)
			sort1(x, x2, x3, x4, x5, x6, off, s);
		if ((s = d - c) > 1)
			sort1(x, x2, x3, x4, x5, x6, n - s, s);
	}

	/**
	 * Swaps x[a .. (a+n-1)] with x[b .. (b+n-1)].
	 */
	private static void vecswap(double[] x, double[] x2, double[] x3,
			double[] x4, double[] x5, int[] x6, int a, int b, int n) {
		for (int i = 0; i < n; i++, a++, b++)
			swap(x, x2, x3, x4, x5, x6,a, b);
	}

	/**
	 * Swaps x[a] with x[b].
	 */
	private static void swap(double[] x, double[] x2, double[] x3, double[] x4,
			double[] x5, int[] x6, int a, int b) {
		double t = x[a];
		x[a] = x[b];
		x[b] = t;

		t = x2[a];
		x2[a] = x2[b];
		x2[b] = t;

		t = x3[a];
		x3[a] = x3[b];
		x3[b] = t;

		t = x4[a];
		x4[a] = x4[b];
		x4[b] = t;

		t = x5[a];
		x5[a] = x5[b];
		x5[b] = t;

		int t1 = x6[a];
		x6[a] = x6[b];
		x6[b] = t1;

	}

	/**
	 * From Arrays JDK ...
	 * @param a
	 * @param fromIndex
	 * @param toIndex
	 * @param key
	 * @return int
	 */
	// Like public version, but without range checks.
	private static int binarySearch0(double[] a, int fromIndex, int toIndex,
			double key) {
		int low = fromIndex;
		int high = toIndex - 1;

		while (low <= high) {
			int mid = (low + high) >>> 1;
			double midVal = a[mid];

			int cmp;
			if (midVal < key) {
				cmp = -1; // Neither val is NaN, thisVal is smaller
			} else if (midVal > key) {
				cmp = 1; // Neither val is NaN, thisVal is larger
			} else {
				long midBits = Double.doubleToLongBits(midVal);
				long keyBits = Double.doubleToLongBits(key);
				cmp = (midBits == keyBits ? 0 : // Values are equal
						(midBits < keyBits ? -1 : // (-0.0, 0.0) or (!NaN, NaN)
								1)); // (0.0, -0.0) or (NaN, !NaN)
			}

			if (cmp < 0)
				low = mid + 1;
			else if (cmp > 0)
				high = mid - 1;
			else
				return mid; // key found
		}
		return -(low + 1); // key not found.
	}

	/**
	 * From Arrays JDK ...
	 * Returns the index of the median of the three indexed doubles.
	 */
	private static int med3(double[] x, int a, int b, int c) {
		return (x[a] < x[b] ? (x[b] < x[c] ? b : x[a] < x[c] ? c : a)
				: (x[b] > x[c] ? b : x[a] > x[c] ? c : a));
	}

	/**
	 * Oversample an array.
	 *
	 * @param array The array.
	 * @param oversampling The oversampling.
	 * @return The oversampled array.
	 */
	public static double[] oversample(double[] array, int oversampling) {
		double[] tabOversampled;
		int nbPoint = ((array.length-1) * oversampling) + 1;

		if (oversampling == 1)
			return Arrays.copyOf(array, array.length);

		tabOversampled = new double[nbPoint];
		double delta = 0.;

		for (int ind = 0; ind < array.length-1; ind++) {
			delta = (array[ind+1] - array[ind])/oversampling;
			tabOversampled[ind * oversampling] = array[ind];
			for (int i = 1; i < oversampling; i++) {
				tabOversampled[ind * oversampling + i] = array[ind] +delta * i;
			}
		}
		tabOversampled[tabOversampled.length-1] = array[array.length-1];
		return tabOversampled;
	}

	/**
	 * Reverse the array.
	 *
	 * @param tab The array.
	 * @return The reversed array.
	 */
	public static double[] reverse(double[] tab) {
		final int length = tab.length;
		double[] array = new double[length];

		for (int i = 0; i < length; i++) {
		     array[length-1-i] = tab[i];
		 }

		return array;
	}

	/**
	 * Create an array of int of the given size and fill it from 0 to size-1 by
	 *  incrementing the starting value 0 by 1 each time; then return the array.
	 *
	 * @param size The size of the array to create.
	 * @return the array.
	 */
	public static int[] incrementalFill(int size) {
		int[] array = new int[size];
		for (int i = 0; i < array.length; i++) {
			array[i] = i;
		}
		return array;
	}

	/**
	 * Create a new array by sorting the values of a two-dimensional array
	 *  following an indexes array.
	 *
	 * @param originalArray The two-dimensional array to sort. It sort the
	 *  second dimension of the array for each first dimension.
	 * @param indexes The indexes used to sort the array. It MUST be the same
	 *  length as the second array size of the originalArray.
	 * @return A new array with the value sorted.
	 */
	public static double[][] recreateTwoDimArraysSortedByIndexes(double[][] originalArray, int[] indexes) {
		double[][] newArray = new double[originalArray.length][originalArray[0].length];

		for (int s = 0; s < originalArray[0].length; s++) {
			int i = indexes[s];
			for (int f = 0; f < originalArray.length; f++) {
				newArray[f][s] = originalArray[f][i];
			}
		}
		return newArray;
	}

}
