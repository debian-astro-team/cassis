/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.common.gui;

import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Window;

/**
 * Utility class to handle screens and opening Window centered on the current screen.
 *
 * @author M. Boiziot
 */
public final class ScreenUtil {


	/**
	 * Must not be used... Utility class.
	 */
	private ScreenUtil() {}


	/**
	 * Return a {@link Rectangle} describing the screen on which is a provided {@link Window}.
	 * If the window is between two screen, the first one is chosen.
	 *
	 * @param window The window for which we want screen rectangle.
	 * @return The rectangle describing the screen on which is the window.
	 */
	public static Rectangle getScreenRectangle(Window window) {
		GraphicsDevice[] screens = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();
		if (window != null) {
			for (int i = 0; i < screens.length; i++) {
				GraphicsConfiguration screenConfig = screens[i].getDefaultConfiguration();
				Rectangle rectangle = screenConfig.getBounds();
				if (rectangle.contains(window.getLocation())) {
					return rectangle;
				}
			}
		}
		return screens[0].getDefaultConfiguration().getBounds();
	}

	/**
	 * Center a {@link Window} on the screen where is a reference {@link Window}.
	 * If the reference window is null, the JFrame to center is centered on the first screen.
	 *
	 * @param windowRef The reference window.
	 * @param windowToCenter The window to center.
	 */
	public static void center(Window windowRef, Window windowToCenter) {
		Rectangle r = ScreenUtil.getScreenRectangle(windowRef);
		int widthFrame = windowToCenter.getWidth();
		int heightFrame = windowToCenter.getHeight();
		int x = r.x + (r.width / 2) - (widthFrame / 2);
		int y = r.y + (r.height / 2) - (heightFrame / 2);
		windowToCenter.setLocation(x, y);
	}
}
