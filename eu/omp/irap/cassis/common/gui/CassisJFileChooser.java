/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.common.gui;

import java.awt.BorderLayout;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.Properties;

import javax.swing.Action;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * JFileChooser with CASSIS bookmarks.
 */
@SuppressWarnings("serial")
public class CassisJFileChooser extends JFileChooser {

	private static final String CASSIS_FOLDER = "CASSIS folder";
	private static final String DEFAULT_FOLDER = "Default folder";
	private static final String LAST_FOLDER = "Last folder";
	private static final String NB_FOLDERS = "nbfolders";
	private static final String PREFIX_FOLDER = "folder";

	private static BookmarkPropertiesInterface bookmarkProperties;

	private JPopupMenu popupMenu;
	private JButton button;
	private DefaultListModel<FolderElement> listModel;
	private JList<FolderElement> list;


	/**
	 * Constructor.
	 *
	 * @param defaultFolderPath The default folder path. null to remove it from the menu.
	 * @param lastFolderPath The last folder path. null to remove it from the menu.
	 */
	public CassisJFileChooser(String defaultFolderPath, String lastFolderPath) {
		super();
		this.listModel = new DefaultListModel<FolderElement>();
		if (bookmarkProperties == null) {
			bookmarkProperties = new BookmarkPropertiesDefault();
		}
		File cassisFolder = getFile(bookmarkProperties.getCassisFolder());
		File defaultFolder = getFile(defaultFolderPath);
		File lastFolder = getFile(lastFolderPath);

		ensuresFoldersExist(cassisFolder, defaultFolder, lastFolder);
		chooseCurrentDirectory(cassisFolder, defaultFolder, lastFolder);

		JScrollPane scrollPane = new JScrollPane(getList());
		JComponent accessory = new JPanel(new BorderLayout());
		accessory.add(scrollPane, BorderLayout.CENTER);
		accessory.add(getButton(), BorderLayout.SOUTH);
		setAccessory(accessory);

		addElements(cassisFolder, defaultFolder, lastFolder);

		setDetailsView();
	}

	/**
	 * Set the JFileChooser on detailed view.
	 */
	private void setDetailsView() {
		Action details = this.getActionMap().get("viewTypeDetails");
		if (details != null) {
			details.actionPerformed(null);
		}
	}

	/**
	 * Create a File for a path.
	 *
	 * @param path The path
	 * @return The File or null if the path was null.
	 */
	private File getFile(String path) {
		return path == null ? null : new File(path);
	}

	/**
	 * Add elements: the three base folders and the bookmarks.
	 *
	 * @param cassisFolder The CASSIS folder
	 * @param defaultFolder The default folder
	 * @param lastFolder The last folder
	 */
	private void addElements(File cassisFolder, File defaultFolder, File lastFolder) {
		addElement(CASSIS_FOLDER, cassisFolder);
		addElement(DEFAULT_FOLDER, defaultFolder);
		addElement(LAST_FOLDER, lastFolder);
		addBookmarks();
	}

	/**
	 * Add an element if file is not null.
	 *
	 * @param label The label of the element.
	 * @param file The file.
	 */
	private void addElement(String label, File file) {
		if (file == null) {
			return;
		}
		listModel.addElement(new FolderElement(label, file));
	}

	/**
	 * Create if necessary then return the pop-up menu with Delete item.
	 *
	 * @return the pop-up menu with Delete item.
	 */
	private JPopupMenu getPopupMenu() {
		if (popupMenu == null) {
			popupMenu = new JPopupMenu();
			JMenuItem deleteItem = new JMenuItem("Delete");
			popupMenu.add(deleteItem);
			deleteItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (!getList().isSelectionEmpty()) {
						String label = getList().getSelectedValue().getLabel();
						if (!isHardcodedElement(label)) {
							listModel.removeElement(getList().getSelectedValue());
							getList().invalidate();
							saveBookmarks();
						}
					}
				}
			});
		}
		return popupMenu;
	}

	/**
	 * Create if necessary then return the "Add current folder" JButton.
	 *
	 * @return the "Add current folder" JButton.
	 */
	private JButton getButton() {
		if (button == null) {
			button = new JButton("Add current folder");
			button.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					File currentDirectory = CassisJFileChooser.this.getCurrentDirectory();
					listModel.addElement(new FolderElement(currentDirectory.getName(), currentDirectory));
					getList().invalidate();
					saveBookmarks();
				}
			});
		}
		return button;
	}

	/**
	 * Create if necessary then return the JList of FolderElement.
	 *
	 * @return the JList of FolderElement.
	 */
	private JList<FolderElement> getList() {
		if (list == null) {
			list = new JList<FolderElement>(listModel) {
				@Override
				public String getToolTipText(MouseEvent event) {
					Point point = event.getPoint();
					int index = locationToIndex(point);
					if (index >= 0 && index < listModel.getSize()) {
						// locationToIndex return closest element:
						// check here if we are on an element.
						Rectangle r = list.getCellBounds(index, index);
						if (r != null && r.contains(point)) {
							return listModel.getElementAt(index).getFile().getAbsolutePath();
						}
					}
					return super.getToolTipText(event);
				}
			};

			list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			list.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent me) {
					if ((SwingUtilities.isRightMouseButton(me)|| me.isPopupTrigger())
							&& !list.isSelectionEmpty()) {
						getPopupMenu().show(list, me.getX(), me.getY());
					}
				}
			});
			list.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
				@Override
				public void valueChanged(ListSelectionEvent e) {
					if (!list.isSelectionEmpty()) {
						CassisJFileChooser.this.setCurrentDirectory(
								list.getSelectedValue().getFile());
					}
				}
			});
		}
		return list;
	}

	/**
	 * Choose the current directory. A folder can only be used if not null.
	 * The order is lastFolder, defaultFolder, cassisFolder or home directory.
	 *
	 * @param cassisFolder The CASSIS folder
	 * @param defaultFolder The default folder
	 * @param lastFolder The last folder
	 */
	private void chooseCurrentDirectory(File cassisFolder, File defaultFolder,
			File lastFolder) {
		if (lastFolder != null) {
			setCurrentDirectory(lastFolder);
		} else if (defaultFolder != null) {
			setCurrentDirectory(defaultFolder);
		} else if (cassisFolder != null) {
			setCurrentDirectory(cassisFolder);
		} else {
			setCurrentDirectory(new File(System.getProperty("user.home")));
		}
	}

	/**
	 * Ensure that the given folders exists if not null.
	 *
	 * @param cassisFolder The CASSIS folder
	 * @param defaultFolder The default folder
	 * @param lastFolder The last folder
	 */
	private void ensuresFoldersExist(File cassisFolder, File defaultFolder, File lastFolder) {
		ensuresFolderExist(cassisFolder);
		ensuresFolderExist(defaultFolder);
		ensuresFolderExist(lastFolder);
	}

	/**
	 * Add bookmarks to the list.
	 */
	private void addBookmarks() {
		Properties properties = bookmarkProperties.getBookmarkProperties();
		if (properties != null) {
			int nbFolders = Integer.parseInt(properties.getProperty("nbfolders", "0"));

			for (int cpt = 1; cpt < nbFolders + 1; cpt++) {
				String path = properties.getProperty("folder" + cpt);
				if (path != null) {
					File file = new File(path);
					addElement(file.getName(), file);
				}
			}
		}
	}

	/**
	 * Save bookmarks.
	 */
	protected void saveBookmarks() {
		Properties properties = new Properties();
		properties.setProperty(NB_FOLDERS, String.valueOf(
				listModel.size() - getHardcodedElementCount()));
		int currentElementNumber = 1;
		for (int cpt = getHardcodedElementCount(); cpt < listModel.size(); cpt++) {
			FolderElement folderElement = listModel.getElementAt(cpt);
			properties.setProperty(PREFIX_FOLDER + String.valueOf(currentElementNumber),
					folderElement.getFile().getAbsolutePath());
			currentElementNumber++;
		}
		bookmarkProperties.saveBookMarkProperties(properties);
	}

	/**
	 * Check then return if an element is hard-coded based on the label.
	 *
	 * @param label The label.
	 * @return true if the element is hard-coded, false otherwise.
	 */
	private boolean isHardcodedElement(String label) {
		return CASSIS_FOLDER.equals(label) || DEFAULT_FOLDER.equals(label) || LAST_FOLDER.equals(label);
	}

	/**
	 * Compute then return the count of hard-coded element.
	 *
	 * @return the count of hard-coded element.
	 */
	private int getHardcodedElementCount() {
		int cpt = 0;
		for (int i = 0; i < listModel.size(); i++) {
			FolderElement folderElement = listModel.getElementAt(i);
			if (isHardcodedElement(folderElement.getLabel())) {
				cpt++;
			}
		}
		return cpt;
	}

	/**
	 * Set a bookmark properties to do before created an instance
	 * @param bookmarkProperties the bookmark properties
	 */
	public static void setBookmarkProperties(BookmarkPropertiesInterface bookmarkProperties) {
		CassisJFileChooser.bookmarkProperties = bookmarkProperties;
	}

	/**
	 * Ensures that the given folder exist.
	 *
	 * @param folder The folder to creates if it does not exist.
	 */
	private void ensuresFolderExist(File folder) {
		if (folder == null) {
			return;
		}
		if (!folder.exists()) {
			folder.mkdirs();
		}
	}


	/**
	 * Simple container class for a File and a label.
	 */
	private class FolderElement {

		private File file;
		private String label;


		/**
		 * Constructor.
		 *
		 * @param label The label.
		 * @param file The file.
		 */
		public FolderElement(String label, File file) {
			this.label = label;
			this.file = file;

		}

		/**
		 * Return the label.
		 *
		 * @return the label.
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return label;
		}

		/**
		 * Return the label.
		 *
		 * @return the label.
		 */
		public final String getLabel() {
			return label;
		}

		/**
		 * Return the file.
		 *
		 * @return the file.
		 */
		public File getFile() {
			return file;
		}
	}


	public static void main(String[] args) {
		JFileChooser chooser = new CassisJFileChooser(null, null);
		JFrame frame = new JFrame("toto");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		chooser.showSaveDialog(frame);
	}

}
