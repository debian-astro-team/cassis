/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.common.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

/**
 * A collapsible panel. The size of this component is almost 0 when collapsed, so
 * it should be used in an appropriate layout.
 *
 * @author bastienkovac
 *
 */
@SuppressWarnings("serial")
public class CollapsiblePanel extends JPanel {

	private JPanel contentPanel;
	private String title;

	private boolean expanded;

	private JButton collapse;
	private JLabel titleLabel;


	/**
	 * Constructor
	 *
	 * @param contentPanel The panel representing the content
	 */
	public CollapsiblePanel(JPanel contentPanel) {
		this(contentPanel, "");
	}

	/**
	 * Constructor
	 *
	 * @param contentPanel The panel representing the content
	 * @param title The title to give to the collapsible panel
	 */
	public CollapsiblePanel(JPanel contentPanel, String title) {
		this(contentPanel, title, true);
	}

	/**
	 * Constructor
	 *
	 * @param contentPanel The panel representing the content
	 * @param title The title to give to the collapsible panel
	 * @param expanded The default state of the panel
	 */
	public CollapsiblePanel(JPanel contentPanel, String title, boolean expanded) {
		this(contentPanel, title, expanded, false);
	}

	/**
	 * Constructor
	 *
	 * @param contentPanel The panel representing the content
	 * @param title The title to give to the collapsible panel
	 * @param expanded The default state of the panel
	 * @param removeBorders If true, remove the borders of the content panel
	 */
	public CollapsiblePanel(JPanel contentPanel, String title, boolean expanded, boolean removeBorders) {
		super();
		this.contentPanel = contentPanel;
		if (removeBorders) {
			this.contentPanel.setBorder(null);
		}
		this.title = title;
		this.expanded = expanded;
		initLayout();
	}

	/**
	 * @return A JButton that let the user collapse and expand this panel
	 */
	public JButton getCollapse() {
		if (collapse == null) {
			collapse = new JButton("-");
			collapse.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					if (expanded) {
						collapsePanel();
						collapse.setText("+");
					} else {
						expandPanel();
						collapse.setText("-");
					}
				}
			});
			collapse.setOpaque(true);
			collapse.setMargin(new Insets(0, 5, 0, 5));
		}
		return collapse;
	}

	/**
	 * @return A JLabel that displays the title of this panel
	 */
	public JLabel getTitleLabel() {
		if (titleLabel == null) {
			titleLabel = new JLabel(title);
			titleLabel.setOpaque(true);
		}
		return titleLabel;
	}

	/**
	 * @return The content of this panel
	 */
	public JPanel getContentPanel() {
		return contentPanel;
	}

	/**
	 * Sets this panel in its collapsed state
	 */
	public void collapsePanel() {
		this.contentPanel.setVisible(false);
		expanded = false;
	}

	/**
	 * Sets this panel in its expanded state
	 */
	public void expandPanel() {
		this.contentPanel.setVisible(true);
		expanded = true;
	}

	private ComponentAdapter correctSize() {
		return new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				resizeComponent();
			}
		};
	}

	private void resizeComponent() {
		Dimension size = getSize();
		Insets insets = getInsets();

		contentPanel.setLocation(insets.left, insets.top);
		contentPanel.setSize(size.width - insets.left - insets.right, size.height - insets.top - insets.bottom);
		contentPanel.doLayout();

		collapse.setLocation(insets.left + 5, 0);
		collapse.setSize(getCollapse().getPreferredSize().width, insets.top);

		titleLabel.setLocation(collapse.getLocation().x + 35, 0);
		titleLabel.setSize(titleLabel.getPreferredSize().width, insets.top);
	}

	private void initLayout() {
		initBorder();
		setLayout(new BorderLayout());
		add(getCollapse());
		add(getTitleLabel());
		add(contentPanel, BorderLayout.CENTER);
		addComponentListener(correctSize());
	}

	private void initBorder() {
		Border in = BorderFactory.createEmptyBorder(10, 5, 10, 5);
		Border out = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "");
		Border inside = BorderFactory.createCompoundBorder(out, in);
		Border outside = BorderFactory.createEmptyBorder(3, 3, 3, 3);
		setBorder(BorderFactory.createCompoundBorder(outside, inside));
	}

}
