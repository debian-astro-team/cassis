/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*******************************************************************************
 *                 CASSIS project ( http://cassis.irap.omp.eu )
 *******************************************************************************
 * Copyright (c) 2013, CNRS. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     - Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     - Neither the name of the CNRS nor the names of its contributors may be
 *       used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL CNRS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package eu.omp.irap.cassis.common.axes;

import javax.swing.JComboBox;

/**
 * Simple class for some statics functions of XAxis.
 *
 * @author Mickael BOIZIOT, Jean-Michel GLORIAN
 */
public class XAxisCassisUtil {

	/**
	 * Return a string of the Type according to the XAxisCassis
	 *
	 * @param axis The axis
	 * @return The String value of the Type of unit.
	 */
	public static String getType(XAxisCassis axis) {
		if (axis instanceof XAxisFrequency) {
			return "Frequency";
		} else if (axis instanceof XAxisVelocity) {
			return "Velocity";
		} else if (axis instanceof XAxisWaveLength) {
			return "Wavelength";
		} else if (axis instanceof XAxisWaveNumber) {
			return "Wave number";
		} else if (axis instanceof XAxisEnergy) {
			return "Energy";
		}
		return "";
	}

	/**
	 * Change the items of a JComboBox according to the type provided.
	 *
	 * @param type The type to use.
	 * @param box The JComboBox to change.
	 */
	public static void changeXAxisAccordingToType(String type, JComboBox<XAxisCassis> box) {
		box.removeAllItems();
		for (XAxisCassis axis : XAxisCassis.getAllXAxisForType(type)) {
			box.addItem(axis);
		}
	}

	/**
	 * Change the value of a JComboBox  of "type" if necessary.
	 *
	 * @param box The JComboBox of XAxisCassis.
	 * @param box2 The JComboBox of "type".
	 */
	public static void changeType(JComboBox<XAxisCassis> box, JComboBox<String> box2) {
		if (box == null || box2 == null || box.getSelectedItem() == null) {
			return;
		}

		String type = getType((XAxisCassis)box.getSelectedItem());
		if (!box2.getSelectedItem().toString().equals(type)) {
			box2.setSelectedItem(type);
		}
	}

	/**
	 * Check if unit parameter is a X Axis of CASSIS
	 * @param unit unit of the AXIS
	 * @return true if the unit is a X Axis of CASSIS,
	 * 			false otherwise
	 */
	public static boolean isXAxisCassis(UNIT unit){
		return XAxisCassis.getXAxisCassis(unit).getUnit() == unit;
	}
}
