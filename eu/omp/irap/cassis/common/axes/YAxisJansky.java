/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*******************************************************************************
 *                 CASSIS project ( http://cassis.irap.omp.eu )
 *******************************************************************************
 * Copyright (c) 2013, CNRS. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     - Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     - Neither the name of the CNRS nor the names of its contributors may be
 *       used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL CNRS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package eu.omp.irap.cassis.common.axes;

import java.util.List;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.Formula;

/**
 * Class describing a Jansky YAxisCassis.
 *
 * @author Jean-Michel GLORIAN
 */
public class YAxisJansky extends YAxisCassis {

	/**
	 * Omega in arcsec
	 */
	private double omega=0;

	/**
	 * Constructs a new {@link YAxisJansky}.
	 *
	 * @param axis The {@link Y_AXIS}.
	 * @param unit The {@link UNIT}.
	 */
	public YAxisJansky(Y_AXIS axis, UNIT unit) {
		super(axis, unit);
	}

	public double getOmega() {
		return omega;
	}

	public void setOmega(double omega) {
		this.omega = omega;
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.cassis.common.axes.YAxisCassis#convertToKelvin(java.lang.Double)
	 */
	/**
	 * Do an <b>approximate</b> conversion to Kelvin.
	 *
	 * @param val The value to convert in the unit of the axis.
	 * @return The value in Kelvin.
	 */
	@Override
	public Double convertToKelvin(Double val) {
		return convertToKelvin(val, null);
	}

	/**
	 * Do an <b>approximate</b> conversion from Kelvin.
	 *
	 * @param val The value in Kelvin.
	 * @return The converted value in the unit of the axis.
	 */
	@Override
	public Double convertFromKelvin(Double val) {
		return convertFromKelvin(val, null);
	}

	@Override
	/**
	 * @param val : value in Jansky
	 * @param coeff : array with indice 0, value in MHz, indice 1 : omega parameter in arcsec if set
	 */
	public Double convertToKelvin(Double val, Double[] coeff) {
		Double res = val;
		if (coeff == null) {
			res = val /500.0;
		}
		else {
			double omega = this.omega;
			if (coeff.length == 2) {
				omega = coeff[1];
			}
			double freqInHz = coeff[0]*1e6;
			double jyToK = 1.e23 * 2. * Formula.kBOLTZMANN_cgs * Math.pow(freqInHz/Formula.c, 2) * omega;
			res = val / jyToK;
		}
		return res;
	}

	@Override
	/**
	 * @param val : value in Kelvin
	 * @param coeff : array with indice 0, value in MHz, indice 1 : omega parameter in arcsec if set
	 */
	public Double convertFromKelvin(Double val, Double[] coeff) {
		Double res = val;
		if (coeff == null) {
			res = val *500.0;
		}
		else {
			double omega = this.omega;
			if (coeff.length == 2) {
				omega = coeff[1];
			}
			double freqInHz = coeff[0]*1E6;
			double jyToK = 1.e23 * 2. * Formula.kBOLTZMANN_cgs * Math.pow(freqInHz/Formula.c, 2) * omega;
			res = val * jyToK;
		}
		return res;
	}

	public void computeOmega(List<CassisMetadata> metadatas) {
		CassisMetadata cassisMetadata = CassisMetadata.getCassisMetadata(CassisMetadata.THETA_MIN, metadatas);
		CassisMetadata cassisMetadata2 = CassisMetadata.getCassisMetadata(CassisMetadata.THETA_MAX, metadatas);
		CassisMetadata cassisMetadata3 = CassisMetadata.getCassisMetadata(CassisMetadata.TYPE_REGION, metadatas);

		double minorValue = (cassisMetadata != null)?Double.valueOf(cassisMetadata.getValue()) : 0.0 ;
		double majorValue = (cassisMetadata2 != null)?Double.valueOf(cassisMetadata2.getValue()) : 0.0 ;
		String type_region = (cassisMetadata2 != null)?cassisMetadata3.getValue() : "" ;


		double thetaMajRad = majorValue * (Math.PI/3600/180);
		double thetaMinRad = minorValue * (Math.PI/3600/180);
		omega = ( Math.PI/4.0/Math.log(2.0) ) * thetaMajRad * thetaMinRad;

		if (type_region == CassisMetadata.GAUSSIAN_REGION) {
			omega = ( Math.PI/4.0/Math.log(2.0) ) * thetaMajRad * thetaMinRad;
		} else if (type_region == CassisMetadata.ELLIPTICAL_REGION) {
			omega = Math.PI * thetaMajRad/2 * thetaMinRad/2;
		} else {
			omega = 0;
		}

	}

}
