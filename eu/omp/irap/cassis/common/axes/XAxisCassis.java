/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*******************************************************************************
 *                 CASSIS project ( http://cassis.irap.omp.eu )
 *******************************************************************************
 * Copyright (c) 2013, CNRS. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     - Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     - Neither the name of the CNRS nor the names of its contributors may be
 *       used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL CNRS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package eu.omp.irap.cassis.common.axes;

import eu.omp.irap.cassis.common.TypeFrequency;

/**
 * This abstract class describe an X axis in CASSIS.
 *
 * @author Jean-Michel GLORIAN
 */
public abstract class XAxisCassis implements Cloneable {

	protected X_AXIS axis = X_AXIS.UNKNOWN;
	protected UNIT unit = UNIT.UNKNOWN;
	protected double loFreq;
	private TypeFrequency typeFrequency = TypeFrequency.REST;


	/**
	 * Constructs a new {@link XAxisCassis}.
	 *
	 * @param axis The {@link X_AXIS}.
	 * @param unit The {@link UNIT}.
	 */
	protected XAxisCassis(X_AXIS axis, UNIT unit) {
		this.loFreq = Double.NaN;
		this.axis = axis;
		this.unit = unit;
	}

	/**
	 * Convert a value in the unit of the XAxisCassis to MHz.
	 *
	 * @param val The value to convert.
	 * @param coeff A coeff if needed.
	 * @return The value in MHz.
	 */
	public abstract Double convertToMHzFreq(Double val, Double[] coeff);

	/**
	 * Convert a value in MHz to the unit of the XAxisCassis.
	 *
	 * @param val The value to convert in MHz.
	 * @param coeff A coeff if needed.
	 * @return The value in the unit of the XAxisCassis.
	 */
	public abstract Double convertFromMhzFreq(Double val, Double[] coeff);

	/**
	 * Convert a value in the unit of the XAxisCassis to MHz.
	 *
	 * @param val The value to convert.
	 * @return The value in MHz.
	 */
	public abstract Double convertToMHzFreq(Double val);

	/**
	 * Convert a value in MHz to the unit of the XAxisCassis.
	 *
	 * @param val The value to convert in MHz.
	 * @return The value in the unit of the XAxisCassis.
	 */
	public abstract Double convertFromMhzFreq(Double val);

	/**
	 * Convert a delta in MHz to the unit of the XAxisCassis.
	 *
	 * @param value The delta to convert in MHz.
	 * @param freqRef A reference frequency.
	 * @return The delta in the unit of the XAxisCassis.
	 */
	public abstract double convertDeltaFromMhz(double value, double freqRef);

	/**
	 * Convert a delta in the unit of the XAxisCassis to MHz.
	 *
	 * @param value The delta to convert in MHz.
	 * @param freqRef A reference frequency.
	 * @return The delta in MHz.
	 */
	public abstract double convertDeltaToMhz(double value, double freqRef);

	/**
	 * Return the unit of the XAxisCassis.
	 *
	 * @return the unit of the XAxisCassis.
	 */
	public final UNIT getUnit() {
		return unit;
	}

	/**
	 * Change the unit.
	 *
	 * @param unit The new unit.
	 */
	public void setUnit(UNIT unit) {
		this.unit = unit;
	}

	/**
	 * Return the x axis.
	 *
	 * @return the x axis.
	 */
	public final X_AXIS getAxis() {
		return axis;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return unit.getValString();
	}

	/**
	 * Return a wavelength XAxisCassis in micrometer.
	 *
	 * @return a wavelength XAxisCassis in micrometer.
	 */
	public static XAxisCassis getXAxisWaveLength() {
		return new XAxisWaveLength(X_AXIS.WAVE_LENGTH, UNIT.MICRO_METER);
	}

	/**
	 * Return a wavenumber XAxisCassis in cm-1.
	 *
	 * @return a wavenumber XAxisCassis in cm-1.
	 */
	public static XAxisCassis getXAxisWaveNumber() {
		return new XAxisWaveNumber(X_AXIS.WAVE_NUMBER, UNIT.CM_MINUS_1);
	}

	/**
	 * Return a frequency XAxisCassis in MHz
	 *
	 * @return a frequency XAxisCassis in MHz
	 */
	public static XAxisCassis getXAxisFrequency() {
		return new XAxisFrequency(X_AXIS.FREQUENCY_SIGNAL, UNIT.MHZ);
	}

	/**
	 * Return a velocity XAxisCassis in km/s.
	 *
	 * @return a velocity XAxisCassis in km/s.
	 */
	public static XAxisVelocity getXAxisVelocity() {
		return new XAxisVelocity(X_AXIS.VELOCITY_SIGNAL, UNIT.KM_SEC_MOINS_1);
	}

	/**
	 * Return a energy XAxis in eV.
	 *
	 * @return a energy XAxis in eV.
	 */
	public static XAxisEnergy getXAxisEnergy() {
		return new XAxisEnergy(X_AXIS.ENERGY, UNIT.EV);
	}

	/**
	 * Return a wavelength XAxisCassis in the given unit.
	 *
	 * @param unit The unit.
	 * @return a wavelength XAxisCassis in the given unit.
	 */
	public static XAxisCassis getXAxisWaveLength(UNIT unit) {
		return new XAxisWaveLength(X_AXIS.WAVE_LENGTH, unit);
	}

	/**
	 * Return a wavenumber XAxisCassis in the given unit.
	 *
	 * @param unit The unit.
	 * @return a wavenumber XAxisCassis in the given unit.
	 */
	public static XAxisCassis getXAxisWaveNumber(UNIT unit) {
		return new XAxisWaveNumber(X_AXIS.WAVE_NUMBER, unit);
	}

	/**
	 * Return a frequency XAxisCassis in the given unit.
	 *
	 * @param unit The unit.
	 * @return a frequency XAxisCassis in the given unit.
	 */
	public static XAxisCassis getXAxisFrequency(UNIT unit) {
		return new XAxisFrequency(X_AXIS.FREQUENCY_SIGNAL, unit);
	}

	/**
	 * Return a velocity XAxisCassis in the given unit.
	 *
	 * @param unit The unit.
	 * @return a velocity XAxisCassis in the given unit.
	 */
	public static XAxisVelocity getXAxisVelocity(UNIT unit) {
		return new XAxisVelocity(X_AXIS.VELOCITY_SIGNAL, unit);
	}

	/**
	 * Return a energy XAxisCassis in the given unit.
	 *
	 * @param unit The unit.
	 * @return a energy XAxisCassis in the given unit.
	 */
	public static XAxisEnergy getXAxisEnergy(UNIT unit) {
		return new XAxisEnergy(X_AXIS.ENERGY, unit);
	}

	/**
	 * Return a X Axis with unknown values.
	 *
	 * @return a X Axis with unknown values.
	 */
	public static XAxisCassis getXAxisUnknown() {
		return new XAxisCassisUnknow(X_AXIS.UNKNOWN, UNIT.UNKNOWN);
	}

	/**
	 * Return a XAxisCassis in the given x axis and unit.
	 *
	 * @param xAxis The x axis.
	 * @param unit The unit.
	 * @return a XAxisCassis in the given x axis and unit.
	 */
	public static XAxisCassis getXAxisCassis(X_AXIS xAxis, UNIT unit) {
		XAxisCassis axis;
		switch (xAxis) {
			case FREQUENCY_SIGNAL:
				axis = XAxisCassis.getXAxisFrequency(unit);
				break;
			case VELOCITY_SIGNAL:
				axis = XAxisCassis.getXAxisVelocity(unit);
				break;
			case WAVE_LENGTH:
				axis = XAxisCassis.getXAxisWaveLength(unit);
				break;
			case WAVE_NUMBER:
				axis = XAxisCassis.getXAxisWaveNumber(unit);
				break;
			case ENERGY:
				axis = XAxisCassis.getXAxisEnergy(unit);
				break;
			default:
				axis = new XAxisCassisUnknow(X_AXIS.UNKNOWN, UNIT.UNKNOWN);
				break;
		}
		return axis;
	}

	/**
	 * Return a XAxisCassis corresponding to the given unit.
	 *
	 * @param unit The unit.
	 * @return a XAxisCassis corresponding to the given unit.
	 */
	public static XAxisCassis getXAxisCassis(UNIT unit) {
		XAxisCassis axis = XAxisCassis.getXAxisFrequency(UNIT.GHZ);
		if (unit == null)
			return axis;
		switch (unit) {
			case CM_MINUS_1:
				axis = XAxisCassis.getXAxisWaveNumber(unit);
				break;
			case KM_SEC_MOINS_1:
				axis = XAxisCassis.getXAxisVelocity(unit);
				break;
			case HZ:
			case MHZ:
			case KHZ:
			case GHZ:
			case THZ:
				axis = XAxisCassis.getXAxisFrequency(unit);
				break;
			case ANGSTROM:
			case NANO_METER:
			case MICRO_METER:
			case MM:
			case CM:
			case METER:
				axis = XAxisCassis.getXAxisWaveLength(unit);
				break;
			case EV:
			case KEV:
			case MEV:
			case GEV:
			case TEV:
			case PEV:
				axis = XAxisCassis.getXAxisEnergy(unit);
				break;
			default:
				break;
		}

		return axis;
	}

	/**
	 * Return a XAxisCassis corresponding to the given unit.
	 *
	 * @param unit The unit.
	 * @return a XAxisCassis corresponding to the given unit.
	 */
	public static XAxisCassis getXAxisCassis(String unit) {
		UNIT unitVal = UNIT.toUnit(unit);
		if (unitVal.equals(UNIT.UNKNOWN))
			return XAxisCassis.getXAxisFrequency(UNIT.GHZ);
		else
			return XAxisCassis.getXAxisCassis(unitVal);
	}

	/**
	 * Return if the XAxis is inverted compared to MHz
	 *
	 * @return if the XAxis is inverted.
	 */
	public abstract boolean isInverted();

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		XAxisCassis other = (XAxisCassis) obj;
		if (axis != other.axis)
			return false;
		if (Double.doubleToLongBits(loFreq) != Double.doubleToLongBits(other.loFreq))
			return false;
		if (typeFrequency != other.typeFrequency)
			return false;
		if (unit != other.unit)
			return false;
		return true;
	}

	/**
	 * Convert two frequencies into a delta in the unit of the XAxisCassis.
	 *
	 * @param f1 The first frequency.
	 * @param f2 The second frequency.
	 * @return The delta in the unit of the XAxisCassis.
	 */
	public double convertDeltaFromMhzFreq(double f1, double f2) {
		return Math.abs(this.convertFromMhzFreq(f1) - this.convertFromMhzFreq(f2))*0.5;
	}

	/**
	 * Return the Lo frequency.
	 *
	 * @return the Lo frequency.
	 */
	public double getLoFreq() {
		return loFreq;
	}

	/**
	 * Change the lo frequency.
	 *
	 * @param loFreq The lo frequency.
	 */
	public void setLoFreq(double loFreq) {
		this.loFreq = loFreq;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((axis == null) ? 0 : axis.hashCode());
		long temp;
		temp = Double.doubleToLongBits(loFreq);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result
				+ ((typeFrequency == null) ? 0 : typeFrequency.hashCode());
		result = prime * result + ((unit == null) ? 0 : unit.hashCode());
		return result;
	}

	/**
	 * Convert a value from an {@link XAxisCassis} to another {@link XAxisCassis}
	 *   using REST frequency.
	 *
	 * @param value The value.
	 * @param xAxisCassisBegin The original x axis.
	 * @param xAxisCassisEnd The destination x axis.
	 * @return The converted value.
	 */
	public static double convert(double value, XAxisCassis xAxisCassisBegin, XAxisCassis xAxisCassisEnd) {
		return convert(value, xAxisCassisBegin, xAxisCassisEnd, TypeFrequency.REST);
	}

	/**
	 * Convert a value from an {@link XAxisCassis} to another {@link XAxisCassis}.
	 *
	 * @param value The value.
	 * @param xAxisCassisBegin The original x axis.
	 * @param xAxisCassisEnd The destination x axis.
	 * @param typeFrequency The type of frequency.
	 * @return The converted value.
	 */
	public static double convert(double value, XAxisCassis xAxisCassisBegin,
			XAxisCassis xAxisCassisEnd, TypeFrequency typeFrequency) {
		XAxisCassis xAxisEndClone = xAxisCassisEnd;
		XAxisCassis xAxisBeginClone = xAxisCassisBegin;
		try {
			xAxisEndClone = xAxisCassisEnd.clone();
			xAxisBeginClone = xAxisCassisBegin.clone();
			if(typeFrequency.equals(TypeFrequency.SKY)) {
				xAxisEndClone.setVlsr(0.);
				xAxisBeginClone.setVlsr(0.);
			}
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}

		return xAxisEndClone.convertFromMhzFreq(xAxisBeginClone.convertToMHzFreq(value));
	}

	@Override
	public XAxisCassis clone() throws CloneNotSupportedException {
		return (XAxisCassis) super.clone();
	}

	/**
	 * Return the title label of the axis.
	 *
	 * @return the title label of the axis.
	 */
	public String getTitleLabel() {
		return axis + " [" + unit.getValString() + "]";
	}

	/**
	 * Change the vlsr, by default do nothing.
	 *
	 * @param vlsr The vlsr.
	 */
	public void setVlsr(double vlsr) {
	}

	/**
	 * Return the type frequency.
	 *
	 * @return the type frequency.
	 */
	public TypeFrequency getTypeFrequency() {
		return typeFrequency;
	}

	/**
	 * Change the type frequency.
	 *
	 * @param typeFrequency The new type frequency.
	 */
	public void setTypeFrequency(TypeFrequency typeFrequency) {
		this.typeFrequency = typeFrequency;
	}

	/**
	 * Return all type of XAxisCassis in a String array.
	 *
	 * @return a String array with all type of XAxisCassis.
	 */
	public static String[] getAllXAxisCassisType() {
		String[] listType = {
				"Frequency",
				"Velocity",
				"Wavelength",
				"Wave number",
				"Energy" };
		return listType;
	}

	/**
	 * Return all XAxisCassis for a type of frequency.
	 *
	 * @param type The type of frequency.
	 * @return an array with all XAxisCassis for the given type.
	 */
	public static XAxisCassis[] getAllXAxisForType(String type) {
		if ("Frequency".equals(type)) {
			XAxisCassis[] listXAxis = {
				XAxisCassis.getXAxisFrequency(UNIT.HZ),
				XAxisCassis.getXAxisFrequency(UNIT.KHZ),
				XAxisCassis.getXAxisFrequency(UNIT.MHZ),
				XAxisCassis.getXAxisFrequency(UNIT.GHZ),
				XAxisCassis.getXAxisFrequency(UNIT.THZ) };
			return listXAxis;
		} else if ("Velocity".equals(type)) {
			XAxisCassis[] listXAxis = {
					XAxisCassis.getXAxisVelocity() };
			return listXAxis;
		} else if ("Wavelength".equals(type)) {
			XAxisCassis[] listXAxis = {
				XAxisCassis.getXAxisWaveLength(UNIT.ANGSTROM),
				XAxisCassis.getXAxisWaveLength(UNIT.NANO_METER),
				XAxisCassis.getXAxisWaveLength(),
				XAxisCassis.getXAxisWaveLength(UNIT.MM),
				XAxisCassis.getXAxisWaveLength(UNIT.CM),
				XAxisCassis.getXAxisWaveLength(UNIT.METER)};
			return listXAxis;
		} else if ("Wave number".equals(type)) {
			XAxisCassis[] listXAxis = {
					XAxisCassis.getXAxisWaveNumber() };
			return listXAxis;
		} else if ("Energy".equals(type)) {
			XAxisCassis[] listXAxis = {
				XAxisCassis.getXAxisEnergy(UNIT.EV),
				XAxisCassis.getXAxisEnergy(UNIT.KEV),
				XAxisCassis.getXAxisEnergy(UNIT.MEV),
				XAxisCassis.getXAxisEnergy(UNIT.GEV),
				XAxisCassis.getXAxisEnergy(UNIT.TEV),
				XAxisCassis.getXAxisEnergy(UNIT.PEV) };
			return listXAxis;
		} else {
			XAxisCassis[] listXAxis = {};
			return listXAxis;
		}
	}

	/**
	 * Convert a delta from an x axis to another x axis.
	 *
	 * @param xAxisBase The original x axis.
	 * @param xAxisFinal The destination x axis.
	 * @param freqRef The reference frequency.
	 * @param value the value of the delta.
	 * @return the new delta.
	 */
	public static double convertDelta(XAxisCassis xAxisBase,
			 XAxisCassis xAxisFinal, double freqRef, double value) {
		return xAxisFinal.convertDeltaFromMhz(xAxisBase.convertDeltaToMhz(value, freqRef), freqRef);
	}

	/**
	 * Convert a range of wave from  xAxisCassis to xAxisCassis2
	 * @param range
	 * @param xAxisCassis
	 * @param xAxisCassis2
	 * @return the range in the xAxisCassis2 unit
	 */
	public static double[] convert(double[] range, XAxisCassis xAxisCassis,
			XAxisCassis xAxisCassis2) {
		double res[] = new double[2];
		res[0] = XAxisCassis.convert(range[0], xAxisCassis, xAxisCassis2);
		res[1] = XAxisCassis.convert(range[1], xAxisCassis, xAxisCassis2);

		if (res[0] > res[1]) {
			double temp = res[1];
			res[1] = res[0];
			res[0] = temp;
		}
		return res;
	}

}
