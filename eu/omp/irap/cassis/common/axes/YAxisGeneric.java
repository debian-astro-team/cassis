/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*******************************************************************************
 *                 CASSIS project ( http://cassis.irap.omp.eu )
 *******************************************************************************
 * Copyright (c) 2013, CNRS. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     - Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     - Neither the name of the CNRS nor the names of its contributors may be
 *       used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL CNRS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package eu.omp.irap.cassis.common.axes;

import java.util.Objects;

/**
 * Class describing a generic YAxisCassis (with unit that CASSIS can not use).
 *
 * @author Jean-Michel GLORIAN
 */
public class YAxisGeneric extends YAxisCassis {

	private String unitGeneric;


	/**
	 * Constructs a new {@link YAxisCassis} with unknown generic unit.
	 *
	 * @param axis The {@link Y_AXIS}, should be {@link Y_AXIS#UNKNOW}.
	 * @param unit The {@link UNIT}, should be {@link UNIT#UNKNOWN}.
	 */
	protected YAxisGeneric(Y_AXIS axis, UNIT unit) {
		super(axis, unit);
		unitGeneric = "???";
	}

	/**
	 * Constructs a new {@link YAxisCassis} with the given unit and no name.
	 *
	 * @param unit The generic unit.
	 */
	protected YAxisGeneric(String unit) {
		this(unit, null);
	}

	/**
	 * Constructs a new {@link YAxisCassis} with the given unit.
	 *
	 * @param unit The generic unit.
	 * @param informationName The name of the information displayed in the axis.
	 */
	protected YAxisGeneric(String unit, String informationName) {
		super(Y_AXIS.UNKNOW, UNIT.UNKNOWN, informationName);
		unitGeneric = unit;
	}

	/**
	 * Return the provided value... <b>Must not be used</b>.
	 *
	 * @param val The value.
	 * @return The value.
	 * @see eu.omp.irap.cassis.common.axes.YAxisCassis#convertToKelvin(java.lang.Double)
	 */
	@Override
	public Double convertToKelvin(Double val) {
		return convertToKelvin(val, null);
	}

	/**
	 * Return the provided value... <b>Must not be used</b>.
	 *
	 * @param val The value.
	 * @return The value.
	 * @see eu.omp.irap.cassis.common.axes.YAxisCassis#convertFromKelvin(java.lang.Double)
	 */
	@Override
	public Double convertFromKelvin(Double val) {
		return convertFromKelvin(val, null);
	}

	/**n
	 * Return the generic unit.
	 *
	 * @return the generic unit.
	 * @see eu.omp.irap.cassis.common.axes.YAxisCassis#toString()
	 */
	@Override
	public String toString() {
		if (informationName != null && !informationName.isEmpty()
				&& unitGeneric != null && !unitGeneric.isEmpty()) {
			return informationName + " [" + unitGeneric + "]";
		} else if ((informationName != null && !informationName.isEmpty())
				&& (unitGeneric == null || unitGeneric.isEmpty())) {
			return informationName;
		} else {
			return unitGeneric;
		}
	}

	/**
	 * Return the generic unit.
	 *
	 * @return the generic unit.
	 */
	public String getUnitGeneric() {
		return unitGeneric;
	}

	/**
	 * Change the generic unit.
	 *
	 * @param unitGeneric The new generic unit.
	 */
	public void setUnitGeneric(String unitGeneric) {
		this.unitGeneric = unitGeneric;
	}

	/**
	 * Return the unit as String, in the case the generic unit.
	 *
	 * @return the unit as String.
	 * @see eu.omp.irap.cassis.common.axes.YAxisCassis#getUnitString()
	 */
	@Override
	public String getUnitString() {
		return getUnitGeneric();
	}

	@Override
	public Double convertToKelvin(Double val, Double[] coeff) {
		return val;
	}

	@Override
	public Double convertFromKelvin(Double val, Double[] coeff) {
		return val;
	}

	/**
	 * Returns a hash code value for the object.
	 *
	 * @return hash code.
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(unitGeneric);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		YAxisGeneric other = (YAxisGeneric) obj;
		return Objects.equals(unitGeneric, other.unitGeneric);
	}

}
