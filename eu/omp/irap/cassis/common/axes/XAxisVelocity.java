/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*******************************************************************************
 *                 CASSIS project ( http://cassis.irap.omp.eu )
 *******************************************************************************
 * Copyright (c) 2013, CNRS. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     - Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     - Neither the name of the CNRS nor the names of its contributors may be
 *       used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL CNRS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package eu.omp.irap.cassis.common.axes;

import java.util.Objects;

import eu.omp.irap.cassis.common.Formula;

/**
 * Class describing a velocity XAxisCassis.
 *
 * @author Jean-Michel GLORIAN
 */
public class XAxisVelocity extends XAxisCassis {

	public static final int INDEX_COEFF_VLSR = 0;
	public static final int INDEX_COEFF_FREQ0 = 1;
	private double vlsr;
	private double freqRef;


	/**
	 * Constructs a new XAxisVelocity.
	 *
	 * @param xAxis The x axis.
	 * @param unit The unit.
	 */
	public XAxisVelocity(X_AXIS xAxis, UNIT unit) {
		super(xAxis, unit);
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.cassis.common.axes.XAxisCassis#convertToMHzFreq(java.lang.Double, java.lang.Double[])
	 */
	/**
	 * Convert a value in the unit of the XAxisCassis to MHz.
	 *
	 * @param val The value to convert.
	 * @param coeff An array with vlsr and freq0 at index given by
	 *    {@link #INDEX_COEFF_VLSR} and {@link #INDEX_COEFF_FREQ0}.
	 * @return The value in MHz.
	 */
	@Override
	public Double convertToMHzFreq(Double val, Double[] coeff) {
		double tmpVlsr = coeff[INDEX_COEFF_VLSR];
		double freq0 = coeff[INDEX_COEFF_FREQ0];
		return (1 - ((val - tmpVlsr) / Formula.CKm)) * freq0;
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.cassis.common.axes.XAxisCassis#convertFromMhzFreq(java.lang.Double, java.lang.Double[])
	 */
	/**
	 * Convert a value in MHz to the current velocity unit.
	 *
	 * @param val The value to convert in MHz.
	 * @param coeff An array with vlsr and freq0 at index given by
	 *    {@link #INDEX_COEFF_VLSR} and {@link #INDEX_COEFF_FREQ0}.
	 * @return The value in the current velocity unit.
	 */
	@Override
	public Double convertFromMhzFreq(Double val, Double[] coeff) {
		double tmpVlsr = coeff[INDEX_COEFF_VLSR];
		double freq0 = coeff[INDEX_COEFF_FREQ0];
		return Formula.calcVelocity(val, freq0, tmpVlsr);
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.cassis.common.axes.XAxisCassis#convertToMHzFreq(java.lang.Double)
	 */
	/**
	 * Convert a value in MHz to the current velocity unit using the currently
	 *    defined vlsr and reference frequency.
	 *
	 * @param val The value to convert.
	 * @return The value in the current velocity unit.
	 */
	@Override
	public Double convertToMHzFreq(Double val) {
		return convertToMHzFreq(val, new Double[] { vlsr, freqRef});
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.cassis.common.axes.XAxisCassis#convertFromMhzFreq(java.lang.Double)
	 */
	/**
	 * Convert a value in MHz to the current velocity unit using the currently
	 *    defined vlsr and reference frequency.
	 *
	 * @param val The value to convert in MHz.
	 * @return The value in the current velocity unit.
	 */
	@Override
	public Double convertFromMhzFreq(Double val) {
		return convertFromMhzFreq(val, new Double[] { vlsr, freqRef});
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.cassis.common.axes.XAxisCassis#isInverted()
	 */
	/**
	 * Return true. Velocity axis is inverted.
	 */
	@Override
	public boolean isInverted() {
		return true;
	}

	/**
	 * Change axis parameters.
	 *
	 * @param vlsr The VLSR.
	 * @param freqRef The reference frequency.
	 */
	public void setCoeff(double vlsr, double freqRef) {
		this.vlsr = vlsr;
		this.freqRef = freqRef;
	}

	/**
	 * Return the VLSR.
	 *
	 * @return the VLSR.
	 */
	public final double getVlsr() {
		return vlsr;
	}

	/**
	 * Return the reference frequency.
	 *
	 * @return the reference frequency.
	 */
	public final double getFreqRef() {
		return freqRef;
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.cassis.common.axes.XAxisCassis#setVlsr(double)
	 */
	/**
	 * Change the vlsr.
	 *
	 * @param vlsr The vlsr.
	 */
	@Override
	public final void setVlsr(double vlsr) {
		this.vlsr = vlsr;
	}

	/**
	 * Change the reference frequency.
	 *
	 * @param freqRef The new reference frequency.
	 */
	public final void setFreqRef(double freqRef) {
		this.freqRef = freqRef;
	}


	/**
	 * Test the equality with another object.
	 *
	 * @see eu.omp.irap.cassis.common.axes.XAxisCassis#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		return super.equals(obj) && vlsr == (((XAxisVelocity) obj).vlsr) &&
				freqRef == (((XAxisVelocity) obj).freqRef);
	}

	/**
	 * Returns a hash code value for the object.
	 *
	 * @return hash code.
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(freqRef, vlsr);
		return result;
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.cassis.common.axes.XAxisCassis#clone()
	 */
	/**
	 * Clone the object.
	 */
	@Override
	public XAxisVelocity clone() throws CloneNotSupportedException {
		XAxisVelocity xAxisVelocity = new XAxisVelocity(axis, unit);
		xAxisVelocity.setCoeff(vlsr, freqRef);
		xAxisVelocity.setTypeFrequency(getTypeFrequency());
		return xAxisVelocity;
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.cassis.common.axes.XAxisCassis#convertDeltaFromMhz(double, double)
	 */
	/**
	 * Convert a delta in MHz to the current velocity unit.
	 *
	 * @param value The delta to convert in MHz.
	 * @param freqRef The reference frequency.
	 * @return The delta in the current velocity unit.
	 */
	@Override
	public double convertDeltaFromMhz(double value, double freqRef) {
		return value / freqRef * Formula.CKm;
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.cassis.common.axes.XAxisCassis#convertDeltaToMhz(double, double)
	 */
	/**
	 * Convert a delta in current velocity unit to MHz.
	 *
	 * @param value The delta to convert in current velocity unit.
	 * @param freqRef The reference frequency.
	 * @return The delta in MHz.
	 */
	@Override
	public double convertDeltaToMhz(double value, double freqRef) {
		return value / Formula.CKm * freqRef;
	}

}
