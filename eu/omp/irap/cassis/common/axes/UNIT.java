/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*******************************************************************************
 *                 CASSIS project ( http://cassis.irap.omp.eu )
 *******************************************************************************
 * Copyright (c) 2013, CNRS. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     - Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     - Neither the name of the CNRS nor the names of its contributors may be
 *       used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL CNRS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

package eu.omp.irap.cassis.common.axes;

/**
 * Enumeration of units known in CASSIS.
 *
 * @author Jean-Michel GLORIAN
 */
public enum UNIT {

	UNKNOWN("Unknow", 1.),
	THZ("THz", 1E6), GHZ("GHz", 1E3), MHZ("MHz", 1.), KHZ("kHz", 1E-3), HZ("Hz", 1E-6),
	KM_SEC_MOINS_1("km/s", 1.),
	ANGSTROM("Ångström", 1E-4), NANO_METER("nm", 1E-3), MICRO_METER("\u00B5m", 1.),
	MM("mm", 1E3), CM("cm", 1E4), METER("m", 1E6),
	EV("eV", 1.), KEV("keV", 1E3), MEV("MeV", 1E6), GEV("GeV", 1E9), TEV("TeV", 1E12),
	PEV("PeV", 1E15),
	CM_MINUS_1("cm-1", 1.),
	JANSKY("Jy", 1.), M_JANSKY("mJy", 1E-3), KELVIN("K", 1.), M_KELVIN("mK", 1E-3),
	JANSKY_BY_BEAM("Jy/beam",1.0 ),
	JOULE("J", 1.0);

	private String valString;
	private final double coeff;


	/**
	 * Constructs a new {@link UNIT} element.
	 *
	 * @param val The reduced name of the unit.
	 * @param coeff The coefficient of the unit.
	 */
	private UNIT(String val, double coeff) {
		valString = val;
		this.coeff = coeff;
	}


	/**
	 * Return the reduced name of the unit.
	 *
	 * @return the reduced name of the unit.
	 */
	public final String getValString() {
		return valString;
	}

	/**
	 * Return the coefficient of the unit.
	 *
	 * @return the coefficient of the unit.
	 */
	public double getCoeff() {
		return coeff;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return valString;
	}

	/**
	 * Return the X band units.
	 *
	 * @return the X band units.
	 */
	public static UNIT[] getXBandUnit() {
		return new UNIT[]{UNIT.THZ, UNIT.GHZ, UNIT.MHZ, UNIT.KHZ, UNIT.HZ,
				UNIT.KM_SEC_MOINS_1,
				UNIT.ANGSTROM, UNIT.NANO_METER, UNIT.MICRO_METER, UNIT.MM, UNIT.CM, UNIT.METER,
				UNIT.CM_MINUS_1,
				UNIT.EV, UNIT.KEV, UNIT.MEV, UNIT.GEV, UNIT.TEV, UNIT.PEV};
	}

	/**
	 * Return the X units.
	 *
	 * @return the X units.
	 */
	public static UNIT[] getXValUnit() {
		return new UNIT[]{UNIT.THZ, UNIT.GHZ, UNIT.MHZ, UNIT.KHZ, UNIT.HZ,
				UNIT.ANGSTROM, UNIT.NANO_METER, UNIT.MICRO_METER, UNIT.MM, UNIT.CM, UNIT.METER,
				UNIT.CM_MINUS_1,
				UNIT.EV, UNIT.KEV, UNIT.MEV, UNIT.GEV, UNIT.TEV, UNIT.PEV};
	}

	/**
	 * Transforms a unit as a String to a UNIT element.
	 * This method is used for compatibility with CASSIS 2.9.
	 *
	 * @param s The unit.
	 * @return The UNIT element corresponding to the provided unit, or
	 *    {@link UNIT#UNKNOWN} if not found.
	 */
	public static UNIT toUnit(String s) {
		if ("THz".equalsIgnoreCase(s)) {
			return UNIT.THZ;
		}
		else if ("GHz".equalsIgnoreCase(s)) {
			return UNIT.GHZ;
		}
		else if ("MHz".equalsIgnoreCase(s)) {
			return UNIT.MHZ;
		}
		else if ("kHz".equalsIgnoreCase(s)) {
			return UNIT.KHZ;
		}
		else if ("Hz".equalsIgnoreCase(s)) {
			return UNIT.HZ;
		}
		else if ("Km/s".equalsIgnoreCase(s)) {
			return UNIT.KM_SEC_MOINS_1;
		}
		else if ("\u00B5m".equalsIgnoreCase(s) ||
				 "micrometer".equalsIgnoreCase(s) || "um".equalsIgnoreCase(s) ||
				 "microm".equalsIgnoreCase(s) || "microns".equalsIgnoreCase(s)
				 || "micron".equalsIgnoreCase(s)) {
			return UNIT.MICRO_METER;
		}
		else if ("mm".equalsIgnoreCase(s)) {
			return UNIT.MM;
		}
		else if ("cm".equalsIgnoreCase(s)) {
			return UNIT.CM;
		}
		else if ("m".equalsIgnoreCase(s)) {
			return UNIT.METER;
		}
		else if ("nm".equalsIgnoreCase(s)||
				 "1 nm".equalsIgnoreCase(s)) {
			return UNIT.NANO_METER;
		}
		else if ("ANGSTROM".equalsIgnoreCase(s)||
				 "Angstroms".equalsIgnoreCase(s)||
				 "A".equalsIgnoreCase(s) ||
				 "Ångström".equalsIgnoreCase(s) ||
				 "Ångströms".equalsIgnoreCase(s)) {
			return UNIT.ANGSTROM;
		}else if ("0.1 nm".equalsIgnoreCase(s) ||
				"10**-10 m".equalsIgnoreCase(s) ||
				"10**(-10)m".equalsIgnoreCase(s) ||
				"1E-10 L".equalsIgnoreCase(s)||
				"1E-10L".equalsIgnoreCase(s)) {
			return UNIT.ANGSTROM;
		}
		else if ("cm-1".equalsIgnoreCase(s)||
				 "1/cm".equalsIgnoreCase(s)) {
			return UNIT.CM_MINUS_1;
		}
		else if ("mJy".equalsIgnoreCase(s)) {
			return UNIT.M_JANSKY;
		}
		else if ("Jy".equalsIgnoreCase(s)) {
			return UNIT.JANSKY;
		}
		else if ("K".equalsIgnoreCase(s)) {
			return UNIT.KELVIN;
		}
		else if ("mK".equalsIgnoreCase(s)) {
			return UNIT.M_KELVIN;
		}
		else if ("eV".equalsIgnoreCase(s)) {
			return UNIT.EV;
		}
		else if ("keV".equalsIgnoreCase(s)) {
			return UNIT.KEV;
		}
		else if ("MeV".equalsIgnoreCase(s)) {
			return UNIT.MEV;
		}
		else if ("GeV".equalsIgnoreCase(s)) {
			return UNIT.GEV;
		}
		else if ("TeV".equalsIgnoreCase(s)) {
			return UNIT.TEV;
		}
		else if ("PeV".equalsIgnoreCase(s)) {
			return UNIT.PEV;
		} else if ("J".equalsIgnoreCase(s)) {
			return UNIT.JOULE;
		}

		return UNIT.UNKNOWN;
	}

	/**
	 * Check if the provided unit is compatible with energy level.
	 *
	 * @param unit
	 * @return true if this is a unit is compatible with energy level, false otherwise.
	 */
	public static boolean isEnergyLevelUnit(UNIT unit) {
		return unit.equals(UNIT.KELVIN) || unit.equals(UNIT.JOULE) ||unit.equals(UNIT.CM_MINUS_1);
	}

//	TODO
//	/**
//	 * Convert val from unit unitFrom to unit unitdest
//	 * @param val value to convert
//	 * @param unitFrom the unit of val
//	 * @param unitdest the unit we want to convert the val
//	 * @return the val converted or the val if unit ar not EnergyLevelUnit
//	 */
//	public double convertEnergyLevelUnit(double val, UNIT unitFrom, UNIT unitdest)

	/**
	 * Check if a provided unit is a WaveLength or Velocity unit.
	 *
	 * @param theUnit The unit.
	 * @return true if this is a wavelength or velocity unit, false otherwise.
	 */
	public static boolean isWaveLengthOrVelocity(UNIT theUnit) {
		return isWaveLength(theUnit) || isVelocity(theUnit);
	}

	/**
	 * Check if the provided unit is a frequency.
	 *
	 * @param unit The unit.
	 * @return true if this is a frequency, false otherwise.
	 */
	public static boolean isFrequency(UNIT unit) {
		return unit == UNIT.HZ || unit == UNIT.KHZ || unit == UNIT.MHZ ||
				unit == UNIT.GHZ || unit == UNIT.THZ;
	}

	/**
	 * Check if the provided unit is a velocity.
	 *
	 * @param unit The unit.
	 * @return true if this is a velocity, false otherwise.
	 */
	public static boolean isVelocity(UNIT unit) {
		return unit == UNIT.KM_SEC_MOINS_1;
	}

	/**
	 * Check if the provided unit is a wavelength.
	 *
	 * @param unit The unit.
	 * @return true if this is a wavelength, false otherwise.
	 */
	public static boolean isWaveLength(UNIT unit) {
		return unit == UNIT.ANGSTROM || unit == UNIT.NANO_METER ||
				unit == UNIT.MICRO_METER || unit == UNIT.MM ||
				unit == UNIT.CM || unit == UNIT.METER;
	}

	/**
	 * Check if the provided unit is a wave number.
	 *
	 * @param unit The unit.
	 * @return true if this is a wave number, false otherwise.
	 */
	public static boolean isWaveNumber(UNIT unit) {
		return unit == UNIT.CM_MINUS_1;
	}

	/**
	 * Check if the provided unit is an energy.
	 *
	 * @param unit The unit.
	 * @return true if this is an energy, false otherwise.
	 */
	public static boolean isEnergy(UNIT unit) {
		return unit == UNIT.EV || unit == UNIT.KEV || unit == UNIT.MEV ||
				unit == UNIT.GEV || unit == UNIT.TEV || unit == UNIT.PEV;
	}

	/**
	 * Check if the provided unit is a wave unit.
	 *
	 * @param unit The unit.
	 * @return true if this is a wave unit, false otherwise.
	 */
	public static boolean isWaveUnit(UNIT unit) {
		return isFrequency(unit) || isVelocity(unit) || isWaveLength(unit) ||
				isWaveNumber(unit) || isEnergy(unit);
	}
}
