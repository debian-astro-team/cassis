/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*******************************************************************************
 *                 CASSIS project ( http://cassis.irap.omp.eu )
 *******************************************************************************
 * Copyright (c) 2013, CNRS. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     - Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     - Neither the name of the CNRS nor the names of its contributors may be
 *       used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL CNRS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package eu.omp.irap.cassis.common.axes;

import java.util.List;

import eu.omp.irap.cassis.common.CassisMetadata;

/**
 * This abstract class describe an Y axis in CASSIS.
 *
 * @author Jean-Michel GLORIAN
 */
public abstract class YAxisCassis implements Cloneable{

	protected Y_AXIS axis = Y_AXIS.UNKNOW;
	protected UNIT unit = UNIT.UNKNOWN;
	protected String informationName;


	/**
	 * Constructs a new {@link YAxisCassis} with no information name.
	 *
	 * @param axis The {@link Y_AXIS}.
	 * @param unit The {@link UNIT}.
	 */
	protected YAxisCassis(Y_AXIS axis, UNIT unit) {
		this(axis, unit, null);
	}

	/**
	 * Constructs a new {@link YAxisCassis}.
	 *
	 * @param axis The {@link Y_AXIS}.
	 * @param unit The {@link UNIT}.
	 * @param informationName The name of the information displayed in the axis.
	 */
	protected YAxisCassis(Y_AXIS axis, UNIT unit, String informationName) {
		this.axis = axis;
		this.unit = unit;
		this.informationName = informationName;
	}

	/**
	 * Return the unit
	 *
	 * @return the unit.
	 */
	public final UNIT getUnit() {
		return unit;
	}

	/**
	 * Change the unit.
	 *
	 * @param unit The new unit.
	 */
	public void setUnit(UNIT unit) {
		this.unit = unit;
	}

	/**
	 * Return the y axis.
	 *
	 * @return the y axis.
	 */
	public final Y_AXIS getAxis() {
		return axis;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getTitleWithoutUnit() + " [" + unit.getValString() + "]";
	}

	/**
	 * Do an <b>approximate</b> conversion to Kelvin.
	 *
	 * @param val The value to convert in the unit of the axis.
	 * @return The value in Kelvin.
	 */
	public abstract Double convertToKelvin(Double val);

	/**
	 * Do an <b>approximate</b> conversion from Kelvin.
	 *
	 * @param val The value in Kelvin.
	 * @return The converted value in the unit of the axis.
	 */
	public abstract Double convertFromKelvin(Double val);

	/**
	 * Do an <b>approximate</b> conversion to Kelvin.
	 *
	 * @param val The value to convert in the unit of the axis.
	 * @param coeff useful parameters for conversion.
	 * @return The value in Kelvin.
	 */
	public abstract Double convertToKelvin(Double val, Double[] coeff);

	/**
	 * Do an <b>approximate</b> conversion from Kelvin.
	 *
	 * @param val The value in Kelvin.
	 * @param coeff useful parameters for conversion.
	 * @return The converted value in the unit of the axis.
	 */
	public abstract Double convertFromKelvin(Double val, Double[] coeff);

	/**
	 * Return a YAxisCassis with Kelvin unit.
	 *
	 * @return a YAxisCassis with Kelvin unit.
	 */
	public static YAxisKelvin getYAxisKelvin() {
		return new YAxisKelvin(Y_AXIS.KELVIN, UNIT.KELVIN);
	}

	/**
	 * Return a YAxisCassis with Jansky unit.
	 *
	 * @return a YAxisCassis with Jansky unit.
	 */
	public static YAxisJansky getYAxisJansky() {
		return new YAxisJansky(Y_AXIS.JANSKY, UNIT.JANSKY);
	}

	/**
	 * Return a YAxisCassis with Jansky by beam unit.
	 *
	 * @return a YAxisCassis with Jansky by beam unit.
	 */
	public static YAxisJanskyByBeam getYAxisJanskyByBeam() {
		return new YAxisJanskyByBeam(Y_AXIS.JANSKY_BY_BEAM, UNIT.JANSKY_BY_BEAM);
	}

	/**
	 * Return a YAxisCassis with the given unit.
	 *
	 * @param unit The unit.
	 * @return the YAxisCassis with the given unit
	 */
	public static YAxisCassis getYAxisCassis(String unit) {
		YAxisCassis axis;
		String unitToTest = unit == null ? UNIT.UNKNOWN.toString() : unit.trim();
		if (unitToTest.equalsIgnoreCase(UNIT.JANSKY.toString()) ||
				unitToTest.equalsIgnoreCase("("+UNIT.JANSKY.toString()+")")) {
			axis = YAxisCassis.getYAxisJansky();
		} else if (unitToTest.equals(UNIT.JANSKY_BY_BEAM.toString())) {
			axis = YAxisCassis.getYAxisJanskyByBeam();
		} else if (unitToTest.equalsIgnoreCase(UNIT.KELVIN.toString()) || unitToTest.equalsIgnoreCase("KELVIN")) {
			axis = YAxisCassis.getYAxisKelvin();
		} else {
			if ("1E-10 MT-3L-1".equals(unit)){
				unit = "10-17 erg/cm2/s/A";
			}
			axis = YAxisCassis.getYAxisGeneric(unit);
		}
		return axis;
	}

	/**
	 * Return a YAxisCassis with the given unit and information name.
	 *
	 * @param unit The unit.
	 * @param informationName The name of the information displayed in the axis.
	 * @return the YAxisCassis with the given unit
	 */
	public static YAxisCassis getYAxisCassis(String unit, String informationName) {
		YAxisCassis axis = getYAxisCassis(unit);
		if (informationName != null && !informationName.isEmpty() &&
				! UNIT.toUnit(unit).equals(UNIT.KELVIN) &&
						!UNIT.toUnit(unit).equals(UNIT.JANSKY) &&
								!UNIT.toUnit(unit).equals(UNIT.JANSKY_BY_BEAM)) {
			axis.setInformationName(informationName);
		}
		return axis;
	}

	/**
	 * Return a YAxisCassis with the given unit and information name.
	 *
	 * @param unit The unit.
	 * @param informationName The name of the information displayed in the axis.
	 * @return the YAxisCassis with the given unit
	 */
	public static YAxisCassis[] getAllYAxisCassis() {
		YAxisCassis[] yAxisCassis = new YAxisCassis[] {
				getYAxisKelvin(), getYAxisJansky(), getYAxisJanskyByBeam(), getYAxisGeneric("Unknow dimension")};

		return yAxisCassis;
	}


	/**
	 * Return a generic YAxisCassis.
	 *
	 * @param unit2 The unit who will be displayed.
	 * @return the generic YAxisCassis.
	 */
	private static YAxisCassis getYAxisGeneric(String unit2) {
		return new YAxisGeneric(unit2);
	}

	/**
	 * Returns a hash code value for the object.
	 *
	 * @return hash code.
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((axis == null) ? 0 : axis.hashCode());
		result = prime * result + ((unit == null) ? 0 : unit.hashCode());
		result = prime * result + ((informationName == null) ? 0 : informationName.hashCode());
		return result;
	}

	/**
	 * Test the equality with the given object.
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		YAxisCassis other = (YAxisCassis) obj;
		if (axis != other.axis)
			return false;
		if (unit != other.unit)
			return false;
		if (informationName == null) {
			if (other.informationName != null)
				return false;
		} else if (!informationName.equals(other.informationName))
			return false;

		return true;
	}

	/**
	 * Return the unit as String.
	 *
	 * @return the unit as String.
	 */
	public String getUnitString() {
		return unit.getValString();
	}

	/**
	 * Return the name of the information displayed in the axis.
	 *
	 * @return the name of the information displayed in the axis.
	 */
	public String getInformationName() {
		return informationName;
	}

	/**
	 * Change the name of the information displayed in the axis.
	 *
	 * @param informationName The name to set.
	 */
	public void setInformationName(String informationName) {
		this.informationName = informationName;
	}

	/**
	 * Return the title without the unit: the information name if there is one
	 *  or the axis.
	 *
	 * @return the title without the unit.
	 */
	public String getTitleWithoutUnit() {
		return (informationName != null && !informationName.isEmpty()) ?
				informationName : axis.toString();
	}

	@Override
	public YAxisCassis clone() throws CloneNotSupportedException {
		return (YAxisCassis) super.clone();
	}

	public static double convert(double fluxVal, YAxisCassis fluxAxis, YAxisCassis convertFluxAxis, double freqMhz,
			List<CassisMetadata> metadatas) {
		double res = Double.NaN;

		if (fluxAxis.equals(convertFluxAxis) || fluxAxis.getAxis().equals(Y_AXIS.UNKNOW)||
				convertFluxAxis.getAxis().equals(Y_AXIS.UNKNOW)) {
			res = fluxVal;
		} else if (fluxAxis.getAxis() == Y_AXIS.JANSKY && convertFluxAxis.getAxis() == Y_AXIS.KELVIN ) {
			YAxisJansky fluxAxisJansky = (YAxisJansky)fluxAxis;
			fluxAxisJansky.computeOmega(metadatas);
			res = fluxAxisJansky.convertToKelvin(fluxVal, new Double[] {freqMhz,fluxAxisJansky.getOmega() });
		} else if (fluxAxis.getAxis() == Y_AXIS.JANSKY_BY_BEAM && convertFluxAxis.getAxis() ==Y_AXIS.KELVIN ) {
			YAxisJanskyByBeam fluxAxisJanskyByBeam = (YAxisJanskyByBeam)fluxAxis;
			fluxAxisJanskyByBeam.computeOmega(metadatas);
			res = fluxAxisJanskyByBeam.convertToKelvin(fluxVal, new Double[] {freqMhz});

		} else if (fluxAxis.getAxis() == Y_AXIS.KELVIN && convertFluxAxis.getAxis() == Y_AXIS.JANSKY ) {
			YAxisJansky fluxAxisJansky = (YAxisJansky)convertFluxAxis;
			fluxAxisJansky.computeOmega(metadatas);
			res = fluxAxisJansky.convertFromKelvin(fluxVal, new Double[] {freqMhz});

		} else if(fluxAxis.getAxis() == Y_AXIS.KELVIN && convertFluxAxis.getAxis() == Y_AXIS.JANSKY_BY_BEAM ) {
			YAxisJanskyByBeam fluxAxisJanskyByBeam = (YAxisJanskyByBeam)convertFluxAxis;
			fluxAxisJanskyByBeam.computeOmega(metadatas);
			res = fluxAxisJanskyByBeam.convertFromKelvin(fluxVal, new Double[] {freqMhz});


		} else if (fluxAxis.getAxis() == Y_AXIS.JANSKY_BY_BEAM && convertFluxAxis.getAxis() == Y_AXIS.JANSKY ) {
			YAxisJanskyByBeam fluxAxisJanskyByBeam = (YAxisJanskyByBeam)fluxAxis;
			fluxAxisJanskyByBeam.computeOmega(metadatas);
			YAxisJansky fluxAxisJansky = (YAxisJansky)convertFluxAxis;
			fluxAxisJansky.computeOmega(metadatas);
			res = fluxAxisJanskyByBeam.convertToKelvin(fluxVal, new Double[] {freqMhz });
			res = fluxAxisJansky.convertFromKelvin(res, new Double[] {freqMhz});


		} else if(fluxAxis.getAxis() == Y_AXIS.JANSKY && convertFluxAxis.getAxis() == Y_AXIS.JANSKY_BY_BEAM ) {

			YAxisJansky fluxAxisJansky = (YAxisJansky)fluxAxis;
			fluxAxisJansky.computeOmega(metadatas);
			YAxisJanskyByBeam fluxAxisJanskyByBeam = (YAxisJanskyByBeam)convertFluxAxis;
			fluxAxisJanskyByBeam.computeOmega(metadatas);
			res = fluxAxisJansky.convertToKelvin(fluxVal, new Double[] {freqMhz});
			res = fluxAxisJanskyByBeam.convertFromKelvin(res, new Double[] {freqMhz});
		}
		return res;
	}

	public static double[] convert(double flux[], YAxisCassis fluxAxis, YAxisCassis convertFluxAxis,
			double freqMhz[], List<CassisMetadata> metadatas) {
		double res[] = null;
		if (fluxAxis.equals(convertFluxAxis)) {
			res = flux;
		} else {
			res = new double[flux.length];
			for (int i = 0; i < res.length; i++) {
				res[i] = convert(flux[i], fluxAxis, convertFluxAxis, freqMhz[i], metadatas);
			}
		}

		return res;
	}

	public static boolean isCompatible(YAxisCassis yAxisCassis1, YAxisCassis yAxisCassis2) {
		return	yAxisCassis1.equals(yAxisCassis2) ||
				Y_AXIS.isCompatible(yAxisCassis1.getAxis(), yAxisCassis2.getAxis());
	}
}
