/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*******************************************************************************
 *                 CASSIS project ( http://cassis.irap.omp.eu )
 *******************************************************************************
 * Copyright (c) 2013, CNRS. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     - Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     - Neither the name of the CNRS nor the names of its contributors may be
 *       used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL CNRS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package eu.omp.irap.cassis.common.axes;

/**
 * XAxisCassis with unknow values.
 *
 * @author Jean-Michel GLORIAN
 */
public class XAxisCassisUnknow extends XAxisCassis {

	/**
	 * Constructs a new XAxisCassisUnknow.
	 *
	 * @param axis The x axis.
	 * @param unit The unit.
	 */
	protected XAxisCassisUnknow(X_AXIS axis, UNIT unit) {
		super(axis, unit);
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.cassis.common.axes.XAxisCassis#convertToMHzFreq(java.lang.Double, java.lang.Double[])
	 */
	/**
	 * Return null.
	 */
	@Override
	public Double convertToMHzFreq(Double val, Double[] coeff) {
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.cassis.common.axes.XAxisCassis#convertFromMhzFreq(java.lang.Double, java.lang.Double[])
	 */
	/**
	 * Return null.
	 */
	@Override
	public Double convertFromMhzFreq(Double val, Double[] coeff) {
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.cassis.common.axes.XAxisCassis#convertToMHzFreq(java.lang.Double)
	 */
	/**
	 * Return null.
	 */
	@Override
	public Double convertToMHzFreq(Double val) {
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.cassis.common.axes.XAxisCassis#convertFromMhzFreq(java.lang.Double)
	 */
	/**
	 * Return null.
	 */
	@Override
	public Double convertFromMhzFreq(Double val) {
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.cassis.common.axes.XAxisCassis#isInverted()
	 */
	/**
	 * Return false.
	 */
	@Override
	public boolean isInverted() {
		return false;
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.cassis.common.axes.XAxisCassis#clone()
	 */
	@Override
	public XAxisCassisUnknow clone() throws CloneNotSupportedException {
		return new XAxisCassisUnknow(axis, unit);
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.cassis.common.axes.XAxisCassis#convertDeltaFromMhz(double, double)
	 */
	/**
	 * Return {@link Double#NaN}.
	 */
	@Override
	public double convertDeltaFromMhz(double value, double freqRef) {
		return Double.NaN;
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.cassis.common.axes.XAxisCassis#convertDeltaToMhz(double, double)
	 */
	/**
	 * Return {@link Double#NaN}.
	 */
	@Override
	public double convertDeltaToMhz(double value, double freqRef) {
		return Double.NaN;
	}

}
