/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.common;

import java.util.List;

/**
 * Class representing a molecule characterization.
 */
public class MoleculeDescription extends Molecule implements Cloneable {

	protected boolean compute;


	/**
	 * Constructs a new MoleculeDescription.
	 */
	public MoleculeDescription() {
		super();

		name = "Test";
		compute = false;
		density = 7e14;
		relativeAbundance = density / 7.5E22;
		temperature = 100.;
		velocityDispersion = 1;
		sourceSize = 3.0;
		dataSource = "unknown";
		tag = 0;
		tKin = 10.;
		gammaSelfMean = 0.0;
		molecularMass = 0.0;

		beta = 0;
		vexp = 0.;

		collision = "0";
	}

	/**
	 * Return if the molecule is selected.
	 *
	 * @return true if selected, false otherwise.
	 */
	public boolean isCompute() {
		return compute;
	}

	/**
	 * Change the selection of the molecule.
	 *
	 * @param selected The new selection value.
	 */
	public void setCompute(final boolean selected) {
		this.compute = selected;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append(tag);
		sb.append("\t");
		sb.append(dataSource);
		sb.append("\t");
		sb.append(collision);
		sb.append("\t");
		sb.append(density);
		sb.append("\t");
		sb.append(relativeAbundance);
		sb.append("\t");
		sb.append(beta);
		sb.append("\t");
		sb.append(temperature);
		sb.append("\t");
		sb.append(tKin);
		sb.append("\t");
		sb.append(velocityDispersion);
		sb.append("\t");
		sb.append(sourceSize);
		sb.append("\t");
		sb.append(vexp);
		sb.append("\t");

		return sb.toString();
	}

	/**
	 * Check and return if the molecule is in the provided list of molecule.
	 *
	 * @param listMol The list of molecule.
	 * @return true if the molecule is in the list, false otherwise.
	 */
	public boolean isIn(List<MoleculeDescription> listMol) {
		for (MoleculeDescription mol : listMol)
			if (this.isSameMolecule(mol))
				return true;

		return false;
	}

	/**
	 * Return the {@link MoleculeDescription} with the given tag from the provided list.
	 *
	 * @param tag The tag of the molecule we want.
	 * @param listMol The list of molecules.
	 * @return The molecule with the given tag or null if not found.
	 */
	public static MoleculeDescription getMoleculeDescription(int tag, List<MoleculeDescription> listMol) {
		for (MoleculeDescription mol : listMol) {
			if (mol.getTag() == tag) {
				return mol;
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#clone()
	 */
	/**
	 * Clone the molecule.
	 */
	@Override
	public Object clone() {
		MoleculeDescription mol = null;
		try {
			mol = (MoleculeDescription) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		if (mol != null) {
			mol.name = name;
			mol.compute = compute;
			mol.density = density;
			mol.beta = beta;
			mol.relativeAbundance = relativeAbundance;
			mol.temperature = temperature;
			mol.velocityDispersion = velocityDispersion;
			mol.sourceSize = sourceSize;
			mol.vexp = vexp;
			mol.tKin = tKin;
			mol.dataSource = dataSource;
			mol.tag = tag;
			mol.gammaSelfMean = gammaSelfMean;
			mol.molecularMass = molecularMass;
			mol.collision = collision;
		}

		return mol;
	}

	/**
	 * Return if the provided molecule is the same (same tag).
	 *
	 * @param mol The molecule to compare with.
	 * @return true if the molecule is the same, false otherwise.
	 */
	public boolean isSameMolecule(MoleculeDescription mol) {
		return tag == mol.getTag();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	/**
	 * Create a hashCode.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (compute ? 1231 : 1237);
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	/**
	 * Test the equality with the given object.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MoleculeDescription other = (MoleculeDescription) obj;
		if (compute != other.compute)
			return false;
		return true;
	}
}
