/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class representing a Telescope.
 *
 * @author Laure Tamisier
 * @author glorian
 */
public class Telescope {

	private static final Logger LOGGER = Logger.getLogger("Telescope");

	private String filename;
	private double diameter;

	private List<Double> frequencyList, effValueList, intermediateFrequency, lsbList, usbList;
	private double frequencyMin, frequencyMax;


	/**
	 * Constructs a new Telescope.
	 *
	 * @param filename The filename of the telescope (if he is in the telescope
	 *  folder, see {@link TelescopeUtil#getTelescopePath()}) or the complete
	 *  path to the telescope file.
	 */
	public Telescope(String filename) {
		this.filename = filename;
		diameter = 0.;
		frequencyList = new ArrayList<Double>();
		effValueList = new ArrayList<Double>();
		intermediateFrequency = new ArrayList<Double>();
		lsbList = new ArrayList<Double>();
		usbList = new ArrayList<Double>();
		frequencyMin = 0.;
		frequencyMax = 0.;
		if (!filename.equals("")) {
			setParameters();
		}
	}

	/**
	 * Return the list of telescope in the telescope directory.
	 *
	 * @return the list of telescope in the telescope directory.
	 */
	public static List<String> getListTelescopes() {
		return getList(TelescopeUtil.getTelescopePath());
	}

	/**
	 * Read telescope parameters and set the fields frequencyList, effValueList,
	 *  intermediateFrequency, lsbList, usbList
	 */
	protected void setParameters() {
		StringTokenizer tok;
		double interFreq;
		boolean firstFrequency = true;

		try (BufferedReader in = getTelescope(filename)) {
			if (in == null) {
				LOGGER.severe("Parameters of the telescope not set");
				return;
			}
			String line;
			while ((line = in.readLine()) != null) {
				if (!line.startsWith("//") && (!line.equals(""))) {
					tok = new StringTokenizer(line, " \t");
					if (tok.countTokens() == 1)
						diameter = Double.parseDouble(tok.nextToken());
					else {
						Double frequency = Double.valueOf(tok.nextToken());
						if (firstFrequency) {
							frequencyMin = frequency.doubleValue();
							firstFrequency = false;
						}
						else
							frequencyMax = frequency.doubleValue();
						frequencyList.add(frequency);

						effValueList.add(Double.valueOf(tok.nextToken()));
						// Get intermediate frequency in MHz
						interFreq = Double.parseDouble(tok.nextToken()) * 1000.;
						intermediateFrequency.add(new Double(interFreq));
						lsbList.add(Double.valueOf(tok.nextToken()));
						usbList.add(Double.valueOf(tok.nextToken()));
					}
				}
			}
		} catch (IOException | NullPointerException e) {
			LOGGER.log(Level.FINER, "Exception during the reading of telescope parameters: " + e);
		}
	}

	/**
	 * @return the effValueList
	 */
	public final List<Double> getEffValueList() {
		return effValueList;
	}

	/**
	 * Return the file name or path to the telescope.
	 *
	 * @return the file name of path to the telescope.
	 */
	public final String getFileName() {
		return filename;
	}

	/**
	 * @return the frequencyList
	 */
	public final List<Double> getFrequencyList() {
		return frequencyList;
	}

	/**
	 * Return the maximum frequency of the Telescope in MHz.
	 *
	 * @return the maximum frequency of the Telescope in MHz.
	 */
	public final double getFrequencyMax() {
		return frequencyMax;
	}

	/**
	 * Return the minimum frequency of the Telescope in MHz.
	 *
	 * @return the minimum frequency of the Telescope in MHz.
	 */
	public final double getFrequencyMin() {
		return frequencyMin;
	}

	/**
	 * Return the intermediate frequency of the Telescope in MHz.
	 *
	 * @return the intermediate frequency of the Telescope in MHz.
	 */
	public final List<Double> getIntermediateFrequency() {
		return intermediateFrequency;
	}

	/**
	 * @return the lsbList
	 */
	public final List<Double> getLsbList() {
		return lsbList;
	}

	/**
	 * Return the telescope diameter in meter.
	 *
	 * @return the telescope diameter in meter.
	 */
	public final double getDiameter() {
		return diameter;
	}

	/**
	 * @return the usbList
	 */
	public final List<Double> getUsbList() {
		return usbList;
	}

	/**
	 * Change the telescope file.
	 *
	 * @param fileName The new telescope file.
	 */
	public void setFilename(String fileName) {
		this.filename = fileName;
		diameter = 0.;
		frequencyList.clear();
		effValueList.clear();
		intermediateFrequency.clear();
		lsbList.clear();
		usbList.clear();
		frequencyMin = 0.;
		frequencyMax = 0.;
		if (!fileName.equals("")) {
			setParameters();
		}
	}

	/**
	 * Compute the line frequency corresponding to  the lo frequency and the side band
	 *
	 * @param loFreq local oscillator frequency in MHz
	 * @param sideBand side band SideBand.LSB or SideBand.USB
	 * @return the line frequency corresponding to  the lo frequency and the side band
	 */
	public double computeLineFreq(Double loFreq, SideBand sideBand) {
		double ifreq = getIntermediateFrequency(loFreq, getIntermediateFrequency(), getFrequencyList());
		ifreq = ifreq / 2;
		double fl;
		if (sideBand.equals(SideBand.LSB))
			fl = loFreq - ifreq;
		else
			fl = loFreq + ifreq;

		return fl;
	}

/**
 * Change the Lo freq when the user enter a lineFrequency
 *
 * @param frequencyLine frequency
 * @param sideBand side band SideBand.LSB or SideBand.USB
 * @return the local oscillator frequency
 */
	public double computeLoFreq(double frequencyLine, SideBand sideBand) {
		double ifreq = getIntermediateFrequency(frequencyLine, getIntermediateFrequency(), getFrequencyList());

		ifreq = ifreq / 2;
		double fl;
		if (sideBand.equals(SideBand.LSB))
			fl = frequencyLine + ifreq;
		else
			fl = frequencyLine - ifreq;

		return fl;
	}

	/**
	 * Return the intermediate frequency in order to calculate the frequency
	 *
	 * @param searchValue
	 * @param interFreqList
	 * @param frequencyList
	 * @return double
	 */
	private double getIntermediateFrequency(double searchValue, List<Double> interFreqList,
			List<Double> frequencyList) {
		double difference = Double.MAX_VALUE;
		double differenceInt = Double.MAX_VALUE;

		int indexOfInterFreq = 0;

		for (int i = 0; i < frequencyList.size(); i++) {
			differenceInt = Math.abs(searchValue - frequencyList.get(i));

			if (differenceInt < difference) {
				difference = differenceInt;
				indexOfInterFreq = i;
			}
		}
		if (interFreqList.isEmpty())
			return 0.0;
		else
			return interFreqList.get(indexOfInterFreq);
	}

	/**
	 * Indicates whether if the file name/path corresponds to a Telescope.
	 *
	 * @param fileName The filename or path
	 * @return if this is a telescope.
	 */
	public static boolean isTelescope(String fileName) {
		if (fileName == null || fileName.equals("")) {
			return false;
		}

		boolean res = false;
		List<String> listTelescopes = getListTelescopes();
		for (String val : listTelescopes) {
			if (val.equalsIgnoreCase(fileName))
				return true;
		}
		return res;
	}

	/**
	 * Return the name of the telescope, based on the filename.
	 *
	 * @return The name of the telescope.
	 */
	public String getName() {
		return Telescope.getNameStatic(filename);
	}

	/**
	 * Return the name of the telescope, based on the filename/path.
	 *
	 * @param path The path or filename.
	 * @return The name of the telescope.
	 */
	public static String getNameStatic(String path) {
		String[] tmp = path.split("/");
		if (tmp.length == 1) {
			if (File.separatorChar == '\\') {
				int idx = path.replaceAll("\\\\", "/").lastIndexOf("/");
				return idx >= 0 ? path.substring(idx + 1) : path;
			} else if (path.contains("\\") && !path.contains("/")) {
				String[] tmpTwo = path.split("\\\\");
				return (tmpTwo.length == 1) ? tmpTwo[0] : tmpTwo[tmpTwo.length-1];
			} else {
				return path;
			}
		} else {
			return tmp[tmp.length-1];
		}
	}

	/**
	 * Return the list of telescope in the given directory.
	 *
	 * @param directory The path of the directory.
	 * @return The list of telescope in the given directory or null if there was an error.
	 */
	protected static List<String> getList(final String directory) {
		List<String> telescopeList = new ArrayList<String>();
		String filename;
		File[] files = new File(directory).listFiles();
		if (files == null) {
			return null;
		} else {
			List<File> filesList = Arrays.asList(files);
			for (File file : filesList) {
				filename = file.getName();
				if (file.isFile() && !filename.startsWith("~") && !filename.startsWith(".")) {
					telescopeList.add(filename);
				}
			}
			Collections.sort(telescopeList);
		}
		return telescopeList;
	}

	public static List<String> getEfficiency(final String fullname) {
		List<String> efficiencyList = new ArrayList<String>();
		File file = new File(fullname);
		boolean exist = file.exists();
		File dir = exist ? file.getParentFile() : new File(TelescopeUtil.getTelescopePath());
		String shortname;
		if (exist) {
			shortname = file.getName();
		} else if (new File(TelescopeUtil.getTelescopePath() + File.separator + fullname).exists()) {
			shortname = fullname;
		} else {
			shortname = fullname.toLowerCase();
		}
		if (dir != null) {
			File[] files = dir.listFiles();
			for (File currentFile : files) {
				if (currentFile.isFile()) {
					String filename = currentFile.getName();
					if (!filename.startsWith(".") && !filename.startsWith("~")
							&& filename.startsWith(shortname)) {
						efficiencyList.add(exist ? dir.getAbsolutePath() + File.separator + filename : filename);
					}
				}
			}
			Collections.sort(efficiencyList);
		}
		return efficiencyList;
	}

	/**
	 * Return a BufferedReader of the telescope.
	 *
	 * @param telescope The name or the path of the telescope.
	 * @return The BufferedReader of the telescope, or null if not found.
	 */
	public static BufferedReader getTelescope(String telescope) {
		BufferedReader in = null;


		try {
			String allPath = isPathOfTelescope(telescope);
			if (allPath != null) {
				in = new BufferedReader(new FileReader(allPath));
			} else {
				in = null;
			}
		} catch (FileNotFoundException e) {
			LOGGER.log(Level.SEVERE, "Telescope file not found", e);
		}
		return in;
	}

	/**
	 * Return the true Path of the telescope or
	 * null if the path is not a path of a telescope
	 *
	 * @param telescope
	 * @return
	 */
	public static String isPathOfTelescope(String telescope) {
		String path = TelescopeUtil.getTelescopePath();
		String allPath = null;
		if (Telescope.isFileExist(telescope)) {
			allPath = telescope;
		} else if (Telescope.isFileExist(path + File.separator + telescope)) {
			allPath = path + File.separator + telescope;
		} else if (Telescope.isFileExist(path + File.separator + telescope.toLowerCase())) {
			allPath = path + File.separator + telescope.toLowerCase();
		}
		return allPath;
	}

	/**
	 * Check and return if the provided path is an existing file.
	 *
	 * @param path The path.
	 * @return true if the provided path is an existing file, false otherwise.
	 */
	public static boolean isFileExist(String path) {
		File f = new File(path);
		return f.exists() && f.isFile();
	}

	/**
	 * Return if the value is a telescope (a file or a telescope name inside
	 *  telescope folder).
	 *
	 * @param value The value to test
	 * @return true if this is a telescope, false otherwise.
	 */
	public static boolean exist(String value) {
		return new File(value).exists() || isTelescope(getNameStatic(value));
	}
}
