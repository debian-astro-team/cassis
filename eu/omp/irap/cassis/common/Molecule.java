/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.common;

/**
 * Class describing a molecule.
 */
public class Molecule {

	public static final String NO_COLLISION = "-no-";
	protected String name = "";
	protected double density; //density column
	protected double beta; // (s-1)
	protected double relativeAbundance; // (/H2)
	protected double temperature; // (K) Tex
	protected double velocityDispersion; // (km/s) FWHM
	protected double sourceSize; // (arcsec)
	protected double vexp; // (km/s)
	protected String dataSource = "";
	protected int tag = -1;
	protected double tKin; // (K)
	protected double gammaSelfMean;
	protected double molecularMass;
	protected String collision; //For radex algo


	/**
	 * Constructs a new Molecule.
	 */
	public Molecule() {
		super();
	}

	/**
	 * Return the name of the molecule.
	 *
	 * @return the name of the molecule.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Change the name of the molecule.
	 *
	 * @param name The new name.
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Return the density of the molecule.
	 *
	 * @return the density of the molecule.
	 */
	public double getDensity() {
		return density;
	}

	/**
	 * Change the density of the molecule.
	 *
	 * @param density The new density.
	 */
	public void setDensity(final double density) {
		this.density = density;
	}

	/**
	 * Return beta (s-1).
	 *
	 * @return beta
	 */
	public double getBeta() {
		return beta;
	}

	/**
	 * Change beta (s-1).
	 *
	 * @param beta The new beta.
	 */
	public void setBeta(final double beta) {
		this.beta = beta;
	}

	/**
	 * Return the relative abundance of the molecule.
	 *
	 * @return the relative abundance.
	 */
	public double getRelativeAbundance() {
		return relativeAbundance;
	}

	/**
	 * Change the relative abundance of the molecule.
	 *
	 * @param relativeAbundance The new relative abundance.
	 */
	public void setRelativeAbundance(final double relativeAbundance) {
		this.relativeAbundance = relativeAbundance;
	}

	/**
	 * Return the temperature of the molecule.
	 *
	 * @return  the temperature of the molecule.
	 */
	public double getTemperature() {
		return temperature;
	}

	/**
	 * Change the temperature of the molecule.
	 *
	 * @param temperature The new temperature (K)
	 */
	public void setTemperature(final double temperature) {
		this.temperature = temperature;
	}

	/**
	 * Return the velocity of dispertion of the molecule.
	 *
	 * @return the velocity of dispertion.
	 */
	public double getVelocityDispersion() {
		return velocityDispersion;
	}

	/**
	 * Change the velocity of dispertion of the molecule
	 *
	 * @param velocityDisp The new velocity of dispertion
	 */
	public void setVelocityDispersion(final double velocityDisp) {
		this.velocityDispersion = velocityDisp;
	}

	/**
	 * Return the source size of the molecule.
	 *
	 * @return the source size.
	 */
	public double getSourceSize() {
		return sourceSize;
	}

	/**
	 * Change the source size of the molecule.
	 *
	 * @param sourceSize The new source size.
	 */
	public void setSourceSize(final double sourceSize) {
		this.sourceSize = sourceSize;
	}

	/**
	 * Return the vo.
	 *
	 * @return the vo.
	 */
	public double getVexp() {
		return vexp;
	}

	/**
	 * Change the vo.
	 *
	 * @param vo The new vo.
	 */
	public void setVexp(final double vo) {
		this.vexp = vo;
	}

	/**
	 * Return the data source.
	 *
	 * @return the data source.
	 */
	public String getDataSource() {
		return dataSource;
	}

	/**
	 * Change the data source.
	 *
	 * @param dataSource The new data source.
	 */
	public void setDataSource(final String dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * Return the tag.
	 *
	 * @return the tag.
	 */
	public int getTag() {
		return tag;
	}

	/**
	 * Change the tag.
	 *
	 * @param tag The new tag.
	 */
	public void setTag(final int tag) {
		this.tag = tag;
	}

	/**
	 * Return the tKin (kinetic temperature, K).
	 *
	 * @return the tKin (kinetic temperature, K)..
	 */
	public double getTKin() {
		return this.tKin;
	}

	/**
	 * Change the tKin (kinetic temperature)
	 *
	 * @param tKin The new kinetic temperature in Kelvin.
	 */
	public void setTKin(final double tKin) {
		this.tKin = tKin;
	}

	/**
	 * Return the gamma self mean of the molecule.
	 *
	 * @return the gamma self mean.
	 */
	public double getGammaSelfMean() {
		return gammaSelfMean;
	}

	/**
	 * Change the gamma self mean.
	 *
	 * @param mean The new gamma self mean.
	 */
	public void setGammaSelfMean(final double mean) {
		this.gammaSelfMean = mean;
	}

	/**
	 * Change the molecular mass.
	 *
	 * @param molMass The new molecular mass.
	 */
	public void setMolecularMass(final double molMass) {
		this.molecularMass = molMass;
	}

	/**
	 * Return the molecular mass.
	 *
	 * @return the molecular mass.
	 */
	public double getMolecularMass() {
		return molecularMass;
	}

	/**
	 * Return the collision name.
	 *
	 * @return the collision name.
	 */
	public String getCollision() {
		return this.collision;
	}

	/**
	 * Change the collision number.
	 *
	 * @param col The new collision number.
	 */
	public void setCollision(final String col) {
		this.collision = col;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	/**
	 * Create and return an hashCode.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(beta);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((collision == null) ? 0 : collision.hashCode());
		result = prime * result + ((dataSource == null) ? 0 : dataSource.hashCode());
		temp = Double.doubleToLongBits(density);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(gammaSelfMean);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(molecularMass);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		temp = Double.doubleToLongBits(relativeAbundance);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(sourceSize);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(tKin);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + tag;
		temp = Double.doubleToLongBits(temperature);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(velocityDispersion);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(vexp);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	/**
	 * Test the equality with the given object.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Molecule other = (Molecule) obj;
		if (Double.doubleToLongBits(beta) != Double.doubleToLongBits(other.beta))
			return false;
		if (collision == null) {
			if (other.collision != null)
				return false;
		} else if (!collision.equals(other.collision))
			return false;
		if (dataSource == null) {
			if (other.dataSource != null)
				return false;
		} else if (!dataSource.equals(other.dataSource))
			return false;
		if (Double.doubleToLongBits(density) != Double.doubleToLongBits(other.density))
			return false;
		if (Double.doubleToLongBits(gammaSelfMean) != Double.doubleToLongBits(other.gammaSelfMean))
			return false;
		if (Double.doubleToLongBits(molecularMass) != Double.doubleToLongBits(other.molecularMass))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (Double.doubleToLongBits(relativeAbundance) != Double.doubleToLongBits(other.relativeAbundance))
			return false;
		if (Double.doubleToLongBits(sourceSize) != Double.doubleToLongBits(other.sourceSize))
			return false;
		if (Double.doubleToLongBits(tKin) != Double.doubleToLongBits(other.tKin))
			return false;
		if (tag != other.tag)
			return false;
		if (Double.doubleToLongBits(temperature) != Double.doubleToLongBits(other.temperature))
			return false;
		if (Double.doubleToLongBits(velocityDispersion) != Double.doubleToLongBits(other.velocityDispersion))
			return false;
		if (Double.doubleToLongBits(vexp) != Double.doubleToLongBits(other.vexp))
			return false;
		return true;
	}

}
