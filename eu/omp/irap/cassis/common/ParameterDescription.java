/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.common;


/**
 * This class allow us to make parameter descriptions.
 *
 * @author girard
 * @since 14 mars 2005
 */
public class ParameterDescription {

	private String varName;
	private String label;
	private double value;
	private double minValue;
	private double maxValue;


	/**
	 * Constructor makes a new standard parameter description with no values defined.
	 */
	public ParameterDescription() {
		super();
	}

	/**
	 * Constructor makes a new standard parameter description with a value defined.
	 * @param value the value of the parameter description
	 *
	 */
	public ParameterDescription(double value) {
		super();
		this.value = value;
	}

	/**
	 * Constructor makes a new parameter description.
	 *
	 * @param varName The variable name.
	 * @param label The label.
	 * @param value The value.
	 * @param minValue The minimum value.
	 * @param maxValue The maximum value.
	 */
	public ParameterDescription(final String varName, final String label, final double value, final double minValue,
			final double maxValue) {
		super();

		this.varName = varName;
		this.label = label;
		this.value = value;
		this.minValue = minValue;
		this.maxValue = maxValue;
	}

	/**
	 * Return the variable name.
	 *
	 * @return the variable name.
	 */
	public String getVarName() {
		return varName;
	}

	/**
	 * Change the variable name.
	 *
	 * @param varName The new variable name.
	 */
	public void setVarName(final String varName) {
		this.varName = varName;
	}

	/**
	 * Return the label.
	 *
	 * @return the label.
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Change the label.
	 *
	 * @param label The new label.
	 */
	public void setLabel(final String label) {
		this.label = label;
	}

	/**
	 * Return the value.
	 *
	 * @return the value.
	 */
	public double getValue() {
		return value;
	}

	/**
	 * Change the value.
	 *
	 * @param value The new value.
	 */
	public void setValue(final double value) {
		this.value = value;
	}

	/**
	 * Return the minimum value.
	 *
	 * @return the minimum value.
	 */
	public double getMinValue() {
		return minValue;
	}

	/**
	 * Change the minimum value.
	 *
	 * @param minValue The new minimum value.
	 */
	public void setMinValue(final double minValue) {
		this.minValue = minValue;
	}

	/**
	 * Return the maximum value.
	 *
	 * @return the maximum value.
	 */
	public double getMaxValue() {
		return maxValue;
	}

	/**
	 * Change the maximum value.
	 *
	 * @param maxValue The new maximum value.
	 */
	public void setMaxValue(final double maxValue) {
		this.maxValue = maxValue;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ParameterDescription [varName=");
		builder.append(varName);
		builder.append(", label=");
		builder.append(label);
		builder.append(", value=");
		builder.append(value);
		builder.append(", minValue=");
		builder.append(minValue);
		builder.append(", maxValue=");
		builder.append(maxValue);
		builder.append("]");
		return builder.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((label == null) ? 0 : label.hashCode());
		long temp;
		temp = Double.doubleToLongBits(maxValue);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(minValue);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(value);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((varName == null) ? 0 : varName.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if (this == obj)
			return true;

		if ((obj == null) || (obj.getClass() != this.getClass()))
			return false;

		ParameterDescription paramDes = (ParameterDescription) obj;

		result = (this.value == paramDes.value) && (this.minValue == paramDes.minValue)
				&& (this.maxValue == paramDes.maxValue) &&

				((this.varName == paramDes.varName) || (this.varName.equals(paramDes.varName))) &&

				((this.label == paramDes.label) || ((this.label).equals(paramDes.label)));

		return result;
	}
}
