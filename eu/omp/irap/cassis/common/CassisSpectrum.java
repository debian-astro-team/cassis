/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import eu.omp.irap.cassis.common.MultiScanCassisSpectrum.OPERATION;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.XAxisVelocity;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.common.axes.YAxisGeneric;

/**
 * Class representing the data of a spectrum
 */
public class CassisSpectrum implements Cloneable {


	private static final String X_ERROR_MSG = "No X axis units found, MHz chosen by default";
	private static final String Y_ERROR_MSG = "No Y axis units found, K chosen by default";
	private static final int NOT_FOUND = -1;
	private double[] frequencies;
	protected double[] intensities;
	private double[] imageFrequencies;
	private TypeFrequency typeFrequency = TypeFrequency.REST;
	private String title;

	private List<LineDescription> listOfLines = new ArrayList<LineDescription>(1);

	private List<CassisMetadata> originalMetadataList;
	private List<CassisMetadata> cassisMetadataList;

	private XAxisCassis xAxisOrigin = XAxisCassis.getXAxisFrequency();
	private YAxisCassis yAxis = YAxisCassis.getYAxisKelvin();
	private boolean yError;
	private boolean xError;
	public static final int NB_MAX_CHANNELS = 100000;


	/**
	 * Partial constructor.
	 *
	 * @param title The title of the spectrum.
	 * @param frequencies The array of frequencies of the spectrum in MHz.
	 * @param vlsr The VLSR of the spectrum.
	 * @param loFrequency The loFrequency of the spectrum.
	 */
	protected CassisSpectrum(String title, double[] frequencies, double vlsr, double loFrequency) {
		originalMetadataList = new ArrayList<CassisMetadata>();
		cassisMetadataList = new ArrayList<CassisMetadata>();
		this.title = title;
		this.frequencies = Arrays.copyOf(frequencies, frequencies.length);
		setVlsr(vlsr);
		setLoFrequency(loFrequency);
	}

	/**
	 * Constructor, construct a CASSIS Spectrum.
	 *
	 * @param title The title of the spectrum.
	 * @param frequencies The array of frequencies of the spectrum in MHz.
	 * @param intensities The intensities of the spectrum.
	 * @param vlsr The VLSR of the spectrum.
	 * @param lofrequency The loFrequency of the spectrum.
	 */
	protected CassisSpectrum(String title, double[] frequencies,
			double[] intensities, double vlsr, double lofrequency) {
		this(title, frequencies, vlsr, lofrequency);
		this.intensities = Arrays.copyOf(intensities, frequencies.length);
	}

	/**
	 * Set a new XAxisCassis of Origin.
	 *
	 * @param xAxisOrigin the xAxisOrigin to set
	 */
	public final void setxAxisOrigin(XAxisCassis xAxisOrigin) {
		this.xAxisOrigin = xAxisOrigin;
		if (xAxisOrigin instanceof XAxisVelocity){
			addCassisMetadata(new CassisMetadata("RefFreq",
						String.valueOf(((XAxisVelocity)xAxisOrigin).getFreqRef()), "",
						UNIT.MHZ.getValString()), true);
		}
	}

	/**
	 * Return the XAxisCassis of Origin.
	 *
	 * @return the XAxisCassis of Origin.
	 */
	public final XAxisCassis getxAxisOrigin() {
		CassisMetadata cassisMetadata = getCassisMetadata("xunit");
		if (cassisMetadata != null && cassisMetadata.getUnit() != null) {
			xAxisOrigin = XAxisCassis.getXAxisCassis(cassisMetadata.getUnit());

		}
		if (xAxisOrigin instanceof XAxisVelocity){
			((XAxisVelocity)xAxisOrigin).setVlsr(getVlsr());
			((XAxisVelocity)xAxisOrigin).setFreqRef(
					Double.valueOf(getCassisMetadata("RefFreq").getValue()));
		}
		return xAxisOrigin;
	}

	/**
	 * Return the number of channel of the spectrum.
	 *
	 * @return the number of channel of the spectrum.
	 */
	public int getNbChannel() {
		return this.frequencies.length;
	}

	/**
	 * Return the title of the spectrum.
	 *
	 * @return the title of the spectrum.
	 */
	public String getTitle() {
		return this.title;
	}

	/**
	 * Change the title of the spectrum.
	 *
	 * @param title The title of the spectrum to set.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Transform CassisSpectrum in CommentedSpectum.
	 *
	 * @return the commentedSpectrum or null if error
	 */
	public CommentedSpectrum computeCommentedSpectrum(){
		CommentedSpectrum[] spectrums= new CommentedSpectrum[1];
		CommentedSpectrum res = null;
		double freqMin = frequencies[0];
		double freqMax = frequencies[frequencies.length - 1];
		boolean ok = computeCommentedSpectrum(freqMin, freqMax, spectrums) ;
		if (ok){
			res = spectrums[0];
		}
		return res;
	}


	/**
	 * Compute a {@link CommentedSpectrum}.
	 *
	 * @param frequencyMin The minimum frequency to keep in MHz.
	 * @param frequencyMax The maximum frequency to keep in MHz.
	 * @param spectrumList An CommentedSpectrum array where the computed spectrum will be set.
	 * @return if the spectrum was computed.
	 */
	public boolean computeCommentedSpectrum(double frequencyMin, double frequencyMax,
			CommentedSpectrum[] spectrumList) {
		int minIndex = getMinIndex(frequencyMin, frequencyMax);
		int maxIndex = getMaxIndex(frequencyMin, frequencyMax);

		// Values found
		if (minIndex != NOT_FOUND && maxIndex != NOT_FOUND) {
			double[] newFreq = getFrequencies(minIndex, maxIndex + 1);
			double[] newIntensity = getIntensities(minIndex, maxIndex + 1);
			double freqCentral = newFreq[0] + Math.abs(newFreq[0] - newFreq[newFreq.length - 1]) / 2.0;

			CommentedSpectrum spectrum = createCommentedSpectrum(newFreq,
					newIntensity, freqCentral);
			spectrumList[0] = spectrum;
		}
		return minIndex != NOT_FOUND && maxIndex != NOT_FOUND;
	}

	/**
	 * Create a CommentedSpectrum with the given points and using the other
	 *  parameters from the CassisSpectrum.
	 *
	 * @param newFreq The frequencies.
	 * @param newIntensity The intensities.
	 * @param freqCentral The central frequency.
	 * @return the created CommentedSpectrum.
	 */
	private CommentedSpectrum createCommentedSpectrum(double[] newFreq,
			double[] newIntensity, double freqCentral) {
		CommentedSpectrum spectrum =
				new CommentedSpectrum(listOfLines, newFreq, newIntensity, title);
		spectrum.setFreqRef(freqCentral);
		spectrum.setxAxisOrigin(xAxisOrigin);
		spectrum.setyAxis(yAxis);
		spectrum.setVlsr(getVlsr());
		spectrum.setListOfLines(listOfLines);
		spectrum.setTitle(title);
		spectrum.setTypeFreq(typeFrequency);
		spectrum.setCassisMetadataList(CassisMetadata.clone(cassisMetadataList));
		spectrum.setOriginalMetadataList(CassisMetadata.clone(originalMetadataList));
		if (this instanceof CassisMultipleSpectrum && (((CassisMultipleSpectrum) this).isMultiple())) {
			spectrum.setLoFreq(Double.NaN);
		} else {
			spectrum.setLoFreq(getLoFrequency());
		}
		return spectrum;
	}

	/**
	 * Return the frequencies between the given minimum and maximum index.
	 *
	 * @param minIndex The minimum index.
	 * @param maxIndex The maximum index.
	 * @return the frequencies between the given minimum and maximum index.
	 */
	private double[] getFrequencies(int minIndex, int maxIndex) {
		return Arrays.copyOfRange(frequencies, minIndex, maxIndex);
	}

	/**
	 * Return the intensities between the given minimum and maximum index.
	 *
	 * @param minIndex The minimum index.
	 * @param maxIndex The maximum index.
	 * @return the intensities between the given minimum and maximum index.
	 */
	private double[] getIntensities(int minIndex, int maxIndex) {
		return Arrays.copyOfRange(intensities, minIndex, maxIndex);
	}

	/**
	 * Search in the frequencies array the index to use for the given frequency min.
	 *
	 * @param frequencyMin The minimum frequency.
	 * @param frequencyMax The maximum frequency.
	 * @return the index or NOT_FOUND if not found.
	 */
	private int getMinIndex(double frequencyMin, double frequencyMax) {
		int minIndex = NOT_FOUND;
		for (int i = 0; minIndex == NOT_FOUND && i < frequencies.length; i++) {
			double frequency = frequencies[i];
			if (frequency >= frequencyMin && frequency <= frequencyMax) {
				minIndex = i;
			}
		}
		return minIndex;
	}

	/**
	 * Search in the frequencies array the index to use for the given frequency max.
	 *
	 * @param frequencyMin The minimum frequency.
	 * @param frequencyMax The maximum frequency.
	 * @return the index or NOT_FOUND if not found.
	 */
	private int getMaxIndex(double frequencyMin, double frequencyMax) {
		int maxIndex = NOT_FOUND;
		for (int i = frequencies.length - 1; maxIndex == NOT_FOUND && i >= 0; i--) {
			double frequency = frequencies[i];
			if (frequency >= frequencyMin && frequency <= frequencyMax) {
				maxIndex = i;
			}
		}
		return maxIndex;
	}

	/**
	 * Search then return the index of the given frequency in the frequencies of the spectrum.
	 *
	 * @param frequency The frequency for which we want the index.
	 * @return The index.
	 */
	public int getFrequencySignalIndex(double frequency) {
		return getIndexFrequency(frequency, frequencies);
	}

	/**
	 * Return the closer index to the given frequency in the given array.
	 *
	 * @param frequency The frequency.
	 * @param frequencies The array of frequency. Must be sorted.
	 * @return The index.
	 */
	private int getIndexFrequency(double frequency, double[] frequencies) {
		int indLeft = 0; // Lower bound for dichotomy.
		int nbChannel = getNbChannel();
		int indRight = nbChannel - 1; // Upper bound for dichotomy.
		int indMiddle;
		double freqMiddle;

		if (getNbChannel() == 1) {
			return 0;
		}
		// Search by dichotomy.
		while (true) {
			indMiddle = (indLeft + indRight) / 2;
			if ((indMiddle == indLeft) || (indMiddle == indRight))
				break;
			freqMiddle = frequencies[indMiddle];

			if (frequency > freqMiddle)
				indLeft = indMiddle;
			else
				indRight = indMiddle;
		}
		final double deltaFreqMiddle = Math.abs(frequency - frequencies[indMiddle]);
		if (indMiddle == 0) {
			if (deltaFreqMiddle <= Math.abs(frequency - frequencies[indMiddle + 1])) {
				return 0;
			} else {
				return 1;
			}
		} else if (indMiddle == nbChannel) {
			if (deltaFreqMiddle <= Math.abs(frequency - frequencies[indMiddle - 1])) {
				return nbChannel;
			} else {
				return nbChannel - 1;
			}
		} else {
			double deltaFreqLeft = Math.abs(frequency - frequencies[indMiddle - 1]);
			double deltaFreqRigth = Math.abs(frequency - frequencies[indMiddle + 1]);
			int res = indMiddle;
			double deltaRes = deltaFreqMiddle;
			if (deltaRes > deltaFreqLeft) {
				deltaRes = deltaFreqLeft;
				res = indMiddle - 1;
			}
			if (deltaRes > deltaFreqRigth) {
				deltaRes = deltaFreqRigth;
				res = indMiddle + 1;
			}
			return res;
		}
	}

	/**
	 * Return the frequencies values of the spectrum.
	 *
	 * @return the frequencies values of the spectrum.
	 */
	public double[] getFrequencies() {
		return frequencies;
	}

	/**
	 * Return the intensities values of the spectrum.
	 *
	 * @return the intensities values of the spectrum.
	 */
	public double[] getIntensities() {
		return intensities;
	}

	/**
	 * Search and return the given signal frequency at the given indice.
	 *
	 * @param indice The indice.
	 * @return the signal frequency.
	 */
	public double getSignalFrequency(int indice) {
		return frequencies[indice];
	}

	/**
	 * Return the VLSR of the spectrum.
	 * 0.0 by default
	 *
	 * @return the VLSR of the spectrum.
	 */
	public double getVlsr() {
		double vlsr = 0.0;
		CassisMetadata cassisMetadata = getCassisMetadata(CassisMetadata.VLSR);
		if (cassisMetadata != null) {
			if (cassisMetadata.getUnit() !=null) {
				vlsr = Double.parseDouble(cassisMetadata.getValue());
			}
		}
		return vlsr;
	}

	/**
	 * Set the VLSR of the spectrum.
	 *
	 * @param vlsr The VLSR to set.
	 */
	public void setVlsr(double vlsr) {
		addCassisMetadata(new CassisMetadata(CassisMetadata.VLSR, String.valueOf(vlsr),
				"Vlsr of the spectrum", UNIT.KM_SEC_MOINS_1.toString()), true);

	}

	/**
	 * Return the intensity of the spectrum at the given index.
	 *
	 * @param index The index.
	 * @return the intensity.
	 */
	public double getValue(int index) {
		return intensities[index];
	}

	/**
	 * Create and return a new Cassis Spectrum with loFreq = 0.0, vlrs =0.0 and
	 *
	 * @param title The title of the spectrum.
	 * @param originWaves The wave of the spectrum in xAxisCassis parameter.
	 * @param originIntensities The intensities of the spectrum in yAxisCassis.
	 * @param xAxisCassis  The waves axis.
	 * @param yAxisCassis The intensities axis.
	 * @return the cassisSpectrum The new Cassis Spectrum with the given parameters.
	 */
	public static CassisSpectrum generateCassisSpectrumFromOriginUnit(
			String title, double[] originWaves, double[] originIntensities,
			XAxisCassis xAxisCassis, YAxisCassis yAxisCassis) {

		double[] frequencies = null;
		double[] intensities = null;

		if (xAxisCassis.getUnit().equals(UNIT.MHZ)){
			frequencies = originWaves;
			intensities = originIntensities;
		}else {
			int size = originWaves.length;
			frequencies = new double[size];
			intensities = new double[size];

			boolean inverted = xAxisCassis.isInverted();
			if (inverted){
				for (int i = 0; i < size; i++) {
					frequencies[i] = xAxisCassis.convertToMHzFreq(originWaves[size-i-1]);
					intensities[i] = originIntensities[size-i-1];
				}

			} else {
				for (int i = 0; i < size; i++) {
					frequencies[i] = xAxisCassis.convertToMHzFreq(originWaves[i]);
					intensities[i] = originIntensities[i];
				}
			}
		}


		CassisSpectrum spectrum = generateCassisSpectrum(title, Double.POSITIVE_INFINITY,
				frequencies, intensities, 0.0, 0.0, xAxisCassis, yAxisCassis);

		return spectrum;
	}

	/**
	 * Create and return a new Cassis Spectrum with loFreq = 0.0.
	 *
	 * @param title The title of the spectrum.
	 * @param frequencies The frequencies of the spectrum in MHz.
	 * @param intensities The intensities of the spectrum.
	 * @param vlsrValue The VLSR of the spectrum.
	 * @param xAxisCassis The x axis.
	 * @param yAxisCassis The y axis.
	 * @return the cassisSpectrum The new Cassis Spectrum with the given parameters.
	 */
	public static CassisSpectrum generateCassisSpectrum(String title, double[] frequencies, double[] intensities, double vlsrValue, XAxisCassis xAxisCassis, YAxisCassis yAxisCassis) {
		final Double intensityNull = Double.POSITIVE_INFINITY;
		CassisSpectrum spectrum = generateCassisSpectrum(title, intensityNull, frequencies, intensities, 0.0, vlsrValue, xAxisCassis, yAxisCassis);

		return spectrum;
	}

	/**
	 * Create and return a CassisSpectrum with the given parameters.
	 *
	 * @param title The title.
	 * @param intensityNull The value of intensityNull.
	 * @param frequencies The frequencies in MHz.
	 * @param intensities The intensities.
	 * @param loFrequency The loFrequency.
	 * @param vlsr The vlsr.
	 * @param xaxis The origin x axis of the data.
	 * @param yAxisCassis The y axis.
	 * @return The created CassisSpectrum.
	 */
	public static CassisSpectrum generateCassisSpectrum(String title, final Double intensityNull,
			double[] frequencies, double[] intensities, double loFrequency, double vlsr,
			XAxisCassis xaxis, YAxisCassis yAxisCassis) {
		int size = frequencies.length;
		int currentSize = 0;

		for (int i = 0; i < size; i++) {
			double currentIntensity = intensities[i];
			if (!(currentIntensity == Double.NEGATIVE_INFINITY || currentIntensity == Double.POSITIVE_INFINITY ||
				  Math.abs(intensityNull - currentIntensity) <= 1e-6)) {
				frequencies[currentSize] = frequencies[i];
				intensities[currentSize] = currentIntensity;
				currentSize++;
			}
		}
		frequencies = Arrays.copyOf(frequencies, currentSize);
		intensities = Arrays.copyOf(intensities, currentSize);

		UtilArrayList.quicksortTab2(frequencies, intensities, 0, currentSize - 1);

		currentSize = makeMean(frequencies, intensities);
		frequencies = Arrays.copyOf(frequencies, currentSize);
		intensities = Arrays.copyOf(intensities, currentSize);

		CassisSpectrum spectrum = new CassisSpectrum(title, frequencies, intensities, vlsr, loFrequency);
		spectrum.setxAxisOrigin(xaxis);
		spectrum.setYAxis(yAxisCassis);
		return spectrum;
	}

	/**
	 * Average intensities of the points with the same x values.
	 *
	 * @param frequencies  The frequencies.
	 * @param intensities The intensities.
	 * @return The number of significant points in the array.
	 */
	public static int makeMean(double[] frequencies, double[] intensities) {
		int currentSize = 0;
		int i = 0;
		int nbEquals = 0;
		double lastFrequency = frequencies[i];
		double sumIntensity = 0;
		while (i < frequencies.length) {
			if (i == frequencies.length - 1) {
				if (lastFrequency == frequencies[i]) {
					sumIntensity += intensities[i];
					nbEquals++;
					frequencies[currentSize] = lastFrequency;
					intensities[currentSize] = sumIntensity / nbEquals;
					currentSize++;
				} else {
					frequencies[currentSize] = lastFrequency;
					intensities[currentSize] = sumIntensity / nbEquals;
					currentSize++;
					frequencies[currentSize] = frequencies[i];
					intensities[currentSize] = intensities[i];
					currentSize++;
				}
			} else if ((lastFrequency != frequencies[i])) {
				frequencies[currentSize] = lastFrequency;
				intensities[currentSize] = sumIntensity / nbEquals;
				currentSize++;
				nbEquals = 1;
				sumIntensity = intensities[i];
			} else {
				sumIntensity += intensities[i];
				nbEquals++;
			}
			lastFrequency = frequencies[i];
			i++;
		}

		return currentSize;
	}

	/**
	 * Return the minimal frequency of the spectrum.
	 *
	 * @return the minimal frequency of the spectrum.
	 */
	public final Double getFreqMin() {
		return frequencies[0];
	}

	/**
	 * Return the maximum frequency of the spectrum.
	 *
	 * @return the maximum frequency of the spectrum.
	 */
	public final Double getFreqMax() {
		return frequencies[frequencies.length-1];
	}


	/**
	 * Return the maximum wave of the spectrum in the original X unit.
	 *
	 * @return the maximum wave of the spectrum in the original X unit.
	 */
	public final Double getOriginalWaveMax() {
		double res = 0;
		 XAxisCassis xOriginAxis = getxAxisOrigin();
		if (xOriginAxis.isInverted()){
			res = xOriginAxis.convertFromMhzFreq(getFreqMin());
		}else {
			res = xOriginAxis.convertFromMhzFreq(getFreqMax());
		}
		return res;
	}


	/**
	 * Return the maximum wave of the spectrum in the original X unit.
	 *
	 * @return the maximum wave of the spectrum in the original X unit.
	 */
	public final Double getOriginalWaveMin() {
		double res = 0;
		 XAxisCassis xOriginAxis = getxAxisOrigin();
		if (xOriginAxis.isInverted()){
			res = xOriginAxis.convertFromMhzFreq(getFreqMax());
		} else {
			res = xOriginAxis.convertFromMhzFreq(getFreqMin());
		}
		return res;
	}

	/**
	 * Getter for the original {@link CassisMetadata} list
	 *
	 * @return List containing all original metadata
	 */
	public List<CassisMetadata> getOriginalMetadataList() {
		return originalMetadataList;
	}

	/**
	 * Setter for the original {@link CassisMetadata} List
	 * if null it will be ignored
	 *
	 * @param originalMetadataList The new original {@link CassisMetadata} List
	 */
	public void setOriginalMetadataList(List<CassisMetadata> originalMetadataList) {
		if (originalMetadataList != null){
			this.originalMetadataList = originalMetadataList;
		}
	}

	/**
	 * Getter for the {@link CassisMetadata} list
	 *
	 * @return List containing metadata interpreted by cassis
	 */
	public List<CassisMetadata> getCassisMetadataList() {
		return cassisMetadataList;
	}

	/**
	 * Setter for the {@link CassisMetadata} List
	 * if null it will be ignored
	 *
	 * @param cassisMetadataList The new {@link CassisMetadata} List
	 */
	public void setCassisMetadataList(List<CassisMetadata> cassisMetadataList) {
		if (cassisMetadataList != null){
			this.cassisMetadataList = cassisMetadataList;
		}

	}

	/**
	 * Return a original metadata according to metadata name
	 * null if the metadata is not in the original metadata list
	 *
	 * @param metadataName The metadata name which we research
	 * @return the original metadata according to a metadata name
	 */
	public CassisMetadata getOriginalMetadata(String metadataName) {
		CassisMetadata res = null;
			for (CassisMetadata metadata : this.originalMetadataList) {
				if (metadataName.equalsIgnoreCase(metadata.getName())) {
					res = metadata;
					break;
				}
			}
		return res;
	}

	/**
	 * Return a cassis metadata according to metadata name
	 * null if the metadata is not in the cassis metadata list
	 *
	 * @param metadataName The metadata name which we research
	 * @return the cassis metadata according to a metadata name
	 */
	public CassisMetadata getCassisMetadata(String metadataName) {
		CassisMetadata res = null;
		for (CassisMetadata metadata : this.cassisMetadataList) {
			if (metadataName.equalsIgnoreCase(metadata.getName())) {
				res= metadata;
				break;
			}
		}
		return res;
	}

	/**
	 * Return the telescope
	 *
	 * @return the telescope
	 */
	public String getTelescope() {
		String result = null;
		CassisMetadata cassisMetadata = getCassisMetadata(CassisMetadata.TELESCOPE);
		if (cassisMetadata != null) {
			if (cassisMetadata.getValue() != null) {
				result = cassisMetadata.getValue();
			}
		}
		return result;
	}

	/**
	 * Return the original telescope extract from the original metadata list
	 *
	 * @return the original telescope
	 */
	public String getOriginalTelescope() {
		String result = null;
		boolean find = false;
		for (CassisMetadata cassisMetadata : this.originalMetadataList) {
			if (cassisMetadata.getName().toLowerCase().contains("instrume")) {
				try {
					Double.parseDouble(cassisMetadata.getValue().replace(',', '.'));
				} catch (NumberFormatException e) {
					result = cassisMetadata.getValue();
					find = true;
					break;
				}
			}
		}
		if (!find) {
			for (CassisMetadata cassisMetadata : this.originalMetadataList) {
				if (cassisMetadata.getName().toLowerCase().contains("telescop")) {
					try {
						Double.parseDouble(cassisMetadata.getValue().replace(',', '.'));
					} catch (NumberFormatException e) {
						result = cassisMetadata.getValue();
						break;
					}
				}
			}
		}
		return result;
	}

	/**
	 * @param cassisMetadata The metadata to add in the original metadata list
	 * @param force True if we want to force the replacement of a metadata if it already exist and false if not
	 * @return True if the metadata was added to original metadata list and false if not
	 */
	public boolean addOriginalMetadata(CassisMetadata cassisMetadata, boolean force) {
		boolean added = false;

		for (CassisMetadata metadata : this.originalMetadataList) {
			if (cassisMetadata.getName().equalsIgnoreCase(metadata.getName())) {
				if (force) {
					metadata.setName(cassisMetadata.getName());
					metadata.setValue(cassisMetadata.getValue());
					metadata.setComment(cassisMetadata.getComment());
					metadata.setUnit(cassisMetadata.getUnit());
					added = true;
					break;
				}
			}
		}

		if (!added) {
			this.originalMetadataList.add(cassisMetadata);
			added = true;
		}
		return added;
	}

	/**
	 * @param cassisMetadata The metadata to add in the cassis metadata list
	 * @param force True if we want to force the replacement of a metadata if it already exist and false if not
	 * @return True if the metadata was added to cassis metadata list and false if not
	 */
	public boolean addCassisMetadata(CassisMetadata cassisMetadata, boolean force) {
		boolean added = false;

		for (CassisMetadata metadata : this.cassisMetadataList) {
			if (cassisMetadata.getName().equalsIgnoreCase(metadata.getName())) {
				if (force) {
					metadata.setName(cassisMetadata.getName());
					metadata.setValue(cassisMetadata.getValue());
					metadata.setComment(cassisMetadata.getComment());
					metadata.setUnit(cassisMetadata.getUnit());
					added = true;
					break;
				}
			}
		}
		if (!added) {
			this.cassisMetadataList.add(cassisMetadata);
			added = true;
		}
		return added;
	}

	/**
	 * Change the Y axis.
	 *
	 * @param yAxis The new y axis.
	 */
	public void setYAxis(YAxisCassis yAxis) {
		this.yAxis = yAxis;
	}

	/**
	 * Return the Y axis.
	 *
	 * @return the y axis.
	 */
	public YAxisCassis getYAxis() {
		CassisMetadata cassisMetadata = getCassisMetadata("yunit");
		if (cassisMetadata != null) {
			if (cassisMetadata.getUnit() != null) {
				yAxis = YAxisCassis.getYAxisCassis(cassisMetadata.getUnit());
			}
		}
		return yAxis;
	}

	/**
	 * Change the lo frequency.
	 *
	 * @param loFrequency The new lo frequency.
	 */
	public void setLoFrequency(double loFrequency) {
		setLoFrequency(loFrequency, "Lo frequency of the spectrum");
	}

	/**
	 * Change the lo frequency.
	 *
	 * @param loFrequency The new lo frequency.
	 * @param comment about the lo frequency
	 */
	public void setLoFrequency(double loFrequency, String comment) {
		if (loFrequency == 0) {
			imageFrequencies = null;
		} else {
			final int nbChannel2 = getNbChannel();
			imageFrequencies = new double[nbChannel2];
			for (int cpt = 0; cpt < nbChannel2; cpt++) {
				final int indiceSignal = nbChannel2 - 1 - cpt;
				imageFrequencies[cpt] = frequencies[indiceSignal] + 2 * (loFrequency - frequencies[indiceSignal]);
			}
		}
		addCassisMetadata(new CassisMetadata(CassisMetadata.LOFREQ, String.valueOf(loFrequency),
				comment, UNIT.MHZ.toString()), true);
	}

	/**
	 * Return the lo frequency.
	 *
	 * @return the lo frequency
	 */
	public final double getLoFrequency() {
		double loFrequency  = 0.0;
		CassisMetadata cassisMetadata = getCassisMetadata(CassisMetadata.LOFREQ);
		if (cassisMetadata != null) {
			loFrequency = Double.parseDouble(cassisMetadata.getValue());
		}
		return loFrequency;
	}

	/**
	 * Change the list of lines.
	 *
	 * @param listOfLines  The list of lines to set.
	 */
	public final void setListOfLines(List<LineDescription> listOfLines) {
		this.listOfLines = listOfLines;
	}

	/**
	 * Return the {@link TypeFrequency} of the spectrum.
	 *
	 * @return the type frequency.
	 */
	public TypeFrequency getTypeFrequency() {
		return typeFrequency;
	}

	/**
	 * Change the {@link TypeFrequency}.
	 *
	 * @param typeFrequency The new {@link TypeFrequency}.
	 */
	public void setTypeFrequency(TypeFrequency typeFrequency) {
		this.typeFrequency = typeFrequency;
	}

	/**
	 * Return the image frequencies.
	 *
	 * @return the images frequencies.
	 */
	public double[] getImageFrequencies() {
		return imageFrequencies;
	}

	/**
	 * Return if there is an error on Y axis.
	 *
	 * @return true if there is an error, false otherwise.
	 */
	public boolean isyError() {
		return yError;
	}

	/**
	 * Change the y error value.
	 *
	 * @param yError
	 *            true if there is a error on y axis, false otherwise.
	 */
	public void setyError(boolean yError) {
		this.yError = yError;
	}

	/**
	 * Return if there is an error on X axis.
	 *
	 * @return true if there is an error, false otherwise.
	 */
	public boolean isxError() {
		return xError;
	}

	/**
	 * Change the x error value.
	 *
	 * @param xError true if there is a error on x axis, false otherwise.
	 */
	public void setxError(boolean xError) {
		this.xError = xError;
	}

	/**
	 * Compute and return the error message.
	 *
	 * @return the error message or null if there is no error.
	 */
	public String getErrorMessage() {
		if (!(xError || yError)) {
			return null;
		}
		StringBuilder sb = new StringBuilder();
		if (xError) {
			sb.append(X_ERROR_MSG);
			if (yError) {
				sb.append('\n');
			}
		}
		if (yError) {
			sb.append(Y_ERROR_MSG);
		}
		return sb.toString();
	}

	/**
	 * Return the lo frequency for the given frequency.
	 *
	 * @param freq The frequency.
	 * @return The lo frequency for the given frequency.
	 */
	public double getLoFrequency(double freq) {
		return getLoFrequency();
	}

	/**
	 * @return minimum intensity cassisSpectrum
	 */
	public double getIntensityMin() {
		double intensityMin = Double.MAX_VALUE;
		final double[] intens = this.getIntensities();
		for (int i = 0; i < intens.length; i++) {
			final double intensity = intens[i];
			if (intensity < intensityMin) {
				intensityMin = intensity;
			}
		}
		return intensityMin;
	}

	/**
	 * @return maximum intensity cassisSpectrum
	 */
	public double getIntensityMax() {
		double intensityMax = Double.MIN_VALUE;
		for (int i = 0; i < this.getIntensities().length; i++) {
			if (this.getIntensities()[i] > intensityMax) {
				intensityMax = this.getIntensities()[i];
			}
		}
		return intensityMax;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(frequencies);
		result = prime * result + Arrays.hashCode(imageFrequencies);
		result = prime * result + Arrays.hashCode(intensities);
		result = prime * result + ((listOfLines == null) ? 0 : listOfLines.hashCode());
		result = prime * result + ((originalMetadataList == null) ? 0 : originalMetadataList.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((typeFrequency == null) ? 0 : typeFrequency.hashCode());
		result = prime * result + ((xAxisOrigin == null) ? 0 : xAxisOrigin.hashCode());
		result = prime * result + (xError ? 1231 : 1237);
		result = prime * result + ((yAxis == null) ? 0 : yAxis.hashCode());
		result = prime * result + (yError ? 1231 : 1237);
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof CassisSpectrum)) {
			return false;
		}
		CassisSpectrum other = (CassisSpectrum) obj;

		if (!Arrays.equals(frequencies, other.frequencies)) {
			return false;
		}
		if (!Arrays.equals(imageFrequencies, other.imageFrequencies)) {
			return false;
		}
		if (!Arrays.equals(intensities, other.intensities)) {
			return false;
		}
		if (listOfLines == null) {
			if (other.listOfLines != null) {
				return false;
			}
		} else if (!listOfLines.equals(other.listOfLines)) {
			return false;
		}
		if (originalMetadataList == null) {
			if (other.originalMetadataList != null) {
				return false;
			}
		} else if (!originalMetadataList.equals(other.originalMetadataList)) {
			return false;
		}

		if (title == null) {
			if (other.title != null) {
				return false;
			}
		} else if (!title.equals(other.title)) {
			return false;
		}
		if (typeFrequency != other.typeFrequency) {
			return false;
		}
		if (xAxisOrigin == null) {
			if (other.xAxisOrigin != null) {
				return false;
			}
		} else if (!xAxisOrigin.equals(other.xAxisOrigin)) {
			return false;
		}
		if (xError != other.xError) {
			return false;
		}
		if (yAxis == null) {
			if (other.yAxis != null) {
				return false;
			}
		} else if (!yAxis.equals(other.yAxis)) {
			if (yAxis instanceof YAxisGeneric) {
				if (!yAxis.getUnitString().equals(other.yAxis.getUnitString())) {
					return false;
				}
			} else {
				return false;
			}
		}
		if (yError != other.yError) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	/**
	 * Return a String with the informations of the CassisSpectrum.
	 */
	@Override
	public String toString() {
		return "CassisSpectrum [frequencies=" + Arrays.toString(frequencies) + ", intensities="
				+ Arrays.toString(intensities) + ", imageFrequencies="
				+ Arrays.toString(imageFrequencies) + ", vlsr=" + getVlsr() + ", typeFrequency="
				+ typeFrequency + ", loFrequency=" + getLoFrequency() + ", titre=" + title
				+ ", listOfLines=" + listOfLines + ", nbChannel=" + getNbChannel() + ", freqMin="
				+ getFreqMin() + ", freqMax=" + getFreqMax() + ", originalMetadatasList="
				+ originalMetadataList + ", xAxisOrigin=" + xAxisOrigin + ", yAxis="
				+ yAxis + ", yError=" + yError + ", xError=" + xError + "]";
	}

	/**
	 * Merge a CassisSpetrum list in CassisSpectrum
	 *
	 * @param cassisSpectrumList the CassisSpectrum list to merge
	 * @return a CassisSpetrum list merged in a CassisSpectrum
	 */
	public static CassisSpectrum mergeCassisSpectrumList(List<CassisSpectrum> cassisSpectrumList) {
		CassisSpectrum cassisSpectrumMerge;
		if (cassisSpectrumList == null || cassisSpectrumList.isEmpty()) {
			cassisSpectrumMerge = null;
		} else if (cassisSpectrumList.size() == 1) {
			cassisSpectrumMerge = cassisSpectrumList.get(0);
			if (cassisSpectrumMerge instanceof MultiScanCassisSpectrum){
				cassisSpectrumMerge = ((MultiScanCassisSpectrum)cassisSpectrumMerge).getCassisSpectrum(OPERATION.ADD);
			}
		} else {
			// Collect information about the first spectrum
			CassisSpectrum firstCassisSpectrum = cassisSpectrumList.get(0);
			String title = firstCassisSpectrum.getTitle();
			double vlsrValue = firstCassisSpectrum.getVlsr();
			XAxisCassis xAxisCassis = firstCassisSpectrum.getxAxisOrigin();
			YAxisCassis yAxisCassis = firstCassisSpectrum.getYAxis();
			List<CassisMetadata> originalMetadataList = firstCassisSpectrum.getOriginalMetadataList();
			List<CassisMetadata> cassisMetadataList = firstCassisSpectrum.getCassisMetadataList();

			CassisSpectrum secondCassisSpectrum = cassisSpectrumList.get(1);

			if (secondCassisSpectrum.getNbChannel() < 2) {
				cassisSpectrumMerge = firstCassisSpectrum;
			} else if (firstCassisSpectrum.getNbChannel() < 2) {
				cassisSpectrumMerge = secondCassisSpectrum;
				title = secondCassisSpectrum.getTitle();
				vlsrValue = secondCassisSpectrum.getVlsr();
				xAxisCassis = secondCassisSpectrum.getxAxisOrigin();
				yAxisCassis = secondCassisSpectrum.getYAxis();
				originalMetadataList = secondCassisSpectrum.getOriginalMetadataList();
				cassisMetadataList = secondCassisSpectrum.getCassisMetadataList();
			} else {
				cassisSpectrumMerge = mergeTwoSpectra(firstCassisSpectrum,
						secondCassisSpectrum, title, vlsrValue, xAxisCassis, yAxisCassis);
			}
			// Merge other spectra
			for (int i = 2; i < cassisSpectrumList.size(); i++) {
				if (cassisSpectrumList.get(i).getNbChannel() > 1) {
					cassisSpectrumMerge = mergeTwoSpectra(cassisSpectrumMerge,
							cassisSpectrumList.get(i), title, vlsrValue, xAxisCassis, yAxisCassis);
				}
			}
			cassisSpectrumMerge.setOriginalMetadataList(originalMetadataList);
			cassisSpectrumMerge.setCassisMetadataList(cassisMetadataList);
		}
		return cassisSpectrumMerge;
	}

	private static CassisSpectrum mergeTwoSpectra(CassisSpectrum cassisSpectrum1,
			CassisSpectrum cassisSpectrum2, String title, double vlsrValue, XAxisCassis xAxisCassis,
			YAxisCassis yAxisCassis) {
		double[] frequencies = new double[cassisSpectrum1.getNbChannel()
				+ cassisSpectrum2.getNbChannel()];
		double[] intensities = new double[cassisSpectrum1.getNbChannel()
				+ cassisSpectrum2.getNbChannel()];
		System.arraycopy(cassisSpectrum1.getFrequencies(), 0, frequencies, 0,
				cassisSpectrum1.getNbChannel());
		System.arraycopy(cassisSpectrum2.getFrequencies(), 0, frequencies,
				cassisSpectrum1.getNbChannel(), cassisSpectrum2.getNbChannel());
		System.arraycopy(cassisSpectrum1.getIntensities(), 0, intensities, 0,
				cassisSpectrum1.getNbChannel());
		System.arraycopy(cassisSpectrum2.getIntensities(), 0, intensities,
				cassisSpectrum1.getNbChannel(), cassisSpectrum2.getNbChannel());
		CassisSpectrum generateCassisSpectrum = generateCassisSpectrum(title, frequencies,
				intensities, vlsrValue, xAxisCassis, yAxisCassis);
		return generateCassisSpectrum;
	}

	@Override
	public CassisSpectrum clone() throws CloneNotSupportedException {
		CassisSpectrum cassisSpectrum = new CassisSpectrum(title, frequencies,
				intensities, getVlsr(), getLoFrequency());
		cassisSpectrum.yError = yError;
		cassisSpectrum.xError = xError;
		cassisSpectrum.setTypeFrequency(typeFrequency);
		YAxisCassis yAxisCassis = YAxisCassis.getYAxisCassis(yAxis.getUnit().toString());
		if (yAxisCassis instanceof YAxisGeneric)
			((YAxisGeneric) yAxisCassis).setUnitGeneric(yAxis.toString());
		cassisSpectrum.setYAxis(yAxisCassis);
		cassisSpectrum.setxAxisOrigin(XAxisCassis.getXAxisCassis(xAxisOrigin.getUnit()));

		List<LineDescription> lines = new ArrayList<LineDescription>(1);
		for (LineDescription line : this.listOfLines) {
			lines.add((LineDescription) line.clone());
		}
		cassisSpectrum.setListOfLines(lines);

		List<CassisMetadata> originalMeta = new ArrayList<CassisMetadata>();
		for (CassisMetadata metadata : this.originalMetadataList) {
			originalMeta.add(new CassisMetadata(metadata.getName(), metadata.getValue(),
					metadata.getComment(), metadata.getUnit()));
		}
		cassisSpectrum.setOriginalMetadataList(originalMeta);

		List<CassisMetadata> cassisMeta = new ArrayList<CassisMetadata>();
		for (CassisMetadata metadata : this.cassisMetadataList) {
			cassisMeta.add(new CassisMetadata(metadata.getName(), metadata.getValue(),
					metadata.getComment(), metadata.getUnit()));
		}
		cassisSpectrum.setCassisMetadataList(cassisMeta);

		return cassisSpectrum;
	}

	/**
	 * Change the XAXis and do some conversion of the values if the old X-unit is known.
	 *
	 * @param xAxis The new xAxis to use.
	 */
	public void setXAxisConv(XAxisCassis xAxis) {
		XAxisCassis oldAxis = xAxisOrigin;
		this.xAxisOrigin = xAxis;
		if (oldAxis.getUnit() != UNIT.UNKNOWN) {
			for (int i = 0; i < frequencies.length; i++) {
				frequencies[i] = xAxis.convertToMHzFreq(oldAxis.convertFromMhzFreq(frequencies[i]));
			}
		}
		UtilArrayList.quicksortTab2(frequencies, intensities, 0, frequencies.length - 1);
	}

}
