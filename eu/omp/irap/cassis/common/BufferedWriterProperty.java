/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.common;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Date;

/**
 * BufferedWriter with some utility methods added.
 */
public class BufferedWriterProperty extends BufferedWriter {


	/**
	 * Constructs a new {@link BufferedWriterProperty} with the date added.
	 *
	 * @param out A Writer.
	 * @throws IOException In case of IO problem.
	 */
	public BufferedWriterProperty(Writer out) throws IOException {
		this(out, true);
	}

	/**
	 * Constructs a new {@link BufferedWriterProperty}.
	 *
	 * @param out A Writer.
	 * @param displayDate If the date should be added.
	 * @throws IOException In case of IO problem.
	 */
	public BufferedWriterProperty(Writer out, boolean displayDate) throws IOException {
		super(out);
		if (displayDate) {
			this.write("# " + new Date());
			this.newLine();
		}
	}

	/**
	 * Write a property as "key=value" then add a new line.
	 *
	 * @param key The key to add.
	 * @param val The value of the key.
	 * @throws IOException In case of IO problem.
	 */
	public void writeProperty(String key, String val) throws IOException {
		this.write(key + "=" + val);
		this.newLine();
	}

	/**
	 * Write a title/header value as "# name" then add a new line.
	 *
	 * @param name The title/header value.
	 * @throws IOException In case of IO problem.
	 */
	public void writeTitle(String name) throws IOException {
		this.write("# " + name);
		this.newLine();
		this.newLine();
	}

}
