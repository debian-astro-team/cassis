/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.common;

import java.util.Arrays;

/**
 * Class descibing a spectrum with multiple sub-spectrum.
 */
public class CassisMultipleSpectrum extends CassisSpectrum {

	private CassisSpectrum[] spectrums;


	/**
	 * Constructs a new {@link CassisMultipleSpectrum} with an initial
	 *   {@link CassisSpectrum}.
	 *
	 * @param cassisSpectrum The initial {@link CassisSpectrum}.
	 */
	public CassisMultipleSpectrum(CassisSpectrum cassisSpectrum) {
		super(cassisSpectrum.getTitle(), cassisSpectrum.getFrequencies(),
				cassisSpectrum.getIntensities(), cassisSpectrum.getVlsr(),
				cassisSpectrum.getLoFrequency());
	}

	/**
	 * Change the scans/spectrums.
	 *
	 * @param spectrums The spectrums.
	 */
	public void setScan(CassisSpectrum[] spectrums) {
		this.spectrums = Arrays.copyOf(spectrums, spectrums.length);
	}

	/**
	 * Return the scans.
	 *
	 * @return the scans.
	 */
	public CassisSpectrum[] getScans() {
		return spectrums;
	}

	/**
	 * Return if there is more than one spectrum.
	 *
	 * @return true if there is more than one spectrum, false otherwise.
	 */
	public boolean isMultiple() {
		return (spectrums.length > 1);
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.cassis.common.CassisSpectrum#getLoFrequency(double)
	 */
	/**
	 * Return the loFrequency of the scan contening the given frequency.
	 *
	 * @param freq The frequency.
	 * @return the loFrequency of the good scan depending on the freq parameter
	 *   or no scan having contening the given frequency.
	 */
	@Override
	public double getLoFrequency(double freq) {
		for (CassisSpectrum cassisSpectrum : spectrums) {
			if(freq >=cassisSpectrum.getFreqMin() && freq <= cassisSpectrum.getFreqMax())
				return cassisSpectrum.getLoFrequency();
		}
		return 0.;
	}

}
