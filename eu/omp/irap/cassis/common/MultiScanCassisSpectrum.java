/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.YAxisCassis;

/**
 * Sub-class of CassisSpectrum for spectrum who contains multiple scans array with same frequencies and metadata.
 *
 * @author M. Boiziot
 */
public class MultiScanCassisSpectrum extends CassisSpectrum {

	/**
	 * Operations allowed on sub-scans.
	 */
	public enum OPERATION { ADD, AVERAGE }
	private double[][] intensitiesSubScans;
	private List<Integer> allScanList;


	/**
	 * Constructor.
	 *
	 * @param title Title of the spectrum.
	 * @param frequencies The array of frequencies.
	 * @param intensitiesSubScans Two dimensions array like [scans][values]
	 * @param vlsrValue The VLSR.
	 * @param xAxisCassis The X axis.
	 * @param yAxisCassis The Y axis.
	 */
	private MultiScanCassisSpectrum(String title, double[] frequencies,
			double[][] intensitiesSubScans, double vlsrValue,
			XAxisCassis xAxisCassis, YAxisCassis yAxisCassis) {
		super(title, frequencies, vlsrValue, 0.0);
		setTitle(title);
		setxAxisOrigin(xAxisCassis);
		setYAxis(yAxisCassis);
		this.intensitiesSubScans = intensitiesSubScans;
		initAllScanList();
	}

	/**
	 * Get a CassisSpectrum for the selected scans with the given operation.
	 *
	 * @param scansIdList The list of scans to use, starting at 0.
	 * @param op The operation to do.
	 * @return The created CassisSpectrum.
	 */
	public CassisSpectrum getCassisSpectrum(List<Integer> scansIdList, OPERATION op) {
		double[] intensitiesSpec = getIntensities(scansIdList, op);
		CassisSpectrum cs = CassisSpectrum.generateCassisSpectrum(getTitle(),
				getFrequencies(), intensitiesSpec, getVlsr(), getxAxisOrigin(),
				getYAxis());
		cs.setLoFrequency(getLoFrequency());
		cs.setOriginalMetadataList(getOriginalMetadataList());
		cs.setCassisMetadataList(getCassisMetadataList());

		return cs;
	}

	/**
	 * Get a CassisSpectrum for the all scans with the given operation.
	 *
	 * @param op The operation to do.
	 * @return The created CassisSpectrum.
	 */
	public CassisSpectrum getCassisSpectrum(OPERATION op) {

		return getCassisSpectrum(allScanList, op) ;
	}

	/**
	 * Init the list of all the scans.
	 */
	private void initAllScanList() {
		if (allScanList == null) {
			allScanList = new ArrayList<>(intensitiesSubScans.length);
			for (int i = 0; i < intensitiesSubScans.length; i++) {
				allScanList.add(i);
			}
		}
	}

	/**
	 * Compute then return the intensities for the selected scans with the given
	 *  operation.
	 *
	 * @param scansIdList The list of scans to use, starting at 0.
	 * @param op The operation to do.
	 * @return the intensities for the selected scans with the given operation.
	 */
	private double[] getIntensities(List<Integer> scansIdList, OPERATION op) {
		double[] intensitiesSpec = getIntensities(scansIdList);
		if (op == OPERATION.AVERAGE) {
			int size = scansIdList.size();
			for (int i = 0; i < intensitiesSpec.length; i++) {
				intensitiesSpec[i] /= size;
			}
		}
		return intensitiesSpec;
	}

	/**
	 * Compute then return the array of the addition of the intensities for the
	 *  selected scans.
	 *
	 * @param scansIdList The list of scans to use, starting at 0.
	 * @return the array of the addition of the intensities for the selected scans.
	 */
	private double[] getIntensities(List<Integer> scansIdList) {
		double[] intensitiesSpec = new double[getNbChannel()];
		for (int i = 0; i < getNbChannel(); i++) {
			intensitiesSpec[i] = getIntensity(scansIdList, i);
		}
		return intensitiesSpec;
	}

	/**
	 * Compute then return the value of the addition at the given index for the
	 *  selected scans.
	 *
	 * @param scansIdList The list of scans to use, starting at 0.
	 * @param index The index.
	 * @return the value.
	 */
	private double getIntensity(List<Integer> scansIdList, int index) {
		double d = 0;
		for (int iScan : scansIdList) {
			d += intensitiesSubScans[iScan][index];
		}
		return d;
	}

	/**
	 * Return the number of scans.
	 *
	 * @return the number of scans.
	 */
	public int getNbScans() {
		return intensitiesSubScans.length;
	}

	/**
	 * Return the added values of all scans.
	 *
	 * @return the added values of all scans.
	 * @see eu.omp.irap.cassis.common.CassisSpectrum#getIntensities()
	 */
	@Override
	public double[] getIntensities() {
		if (intensities == null){
			intensities = getIntensities(allScanList);
		}
		return intensities;
	}

	/**
	 * Return the added value of all scans at the given index.
	 *
	 * @see eu.omp.irap.cassis.common.CassisSpectrum#getValue(int)
	 */
	@Override
	public double getValue(int index) {
		return getIntensity(allScanList, index);
	}

	/**
	 * Create and return the hashCode of the object.
	 *
	 * @return the hashCode of the object.
	 * @see eu.omp.irap.cassis.common.CassisSpectrum#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((allScanList == null) ? 0 : allScanList.hashCode());
		result = prime * result + Arrays.deepHashCode(intensitiesSubScans);
		return result;
	}

	/**
	 * Check the equality with another object.
	 *
	 * @param obj the object to compare to.
	 * @return true if the object are the same, false otherwise.
	 * @see eu.omp.irap.cassis.common.CassisSpectrum#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MultiScanCassisSpectrum other = (MultiScanCassisSpectrum) obj;
		if (allScanList == null) {
			if (other.allScanList != null)
				return false;
		} else if (!allScanList.equals(other.allScanList))
			return false;
		if (!Arrays.deepEquals(intensitiesSubScans, other.intensitiesSubScans))
			return false;
		return true;
	}

	/**
	 * Create a MultiScanCassisSpectrum and by sorting a copy of frequencies
	 *  array and the intensitiesSubScans according to it.
	 *
	 * @param title Title of the spectrum.
	 * @param frequencies The array of frequencies.
	 * @param intensitiesSubScans Two dimensions array like [scans][values]
	 * @param vlsrValue The VLSR.
	 * @param xAxisCassis The X axis.
	 * @param yAxisCassis The Y axis.
	 * @return the created MultiScanCassisSpectrum.
	 */
	public static MultiScanCassisSpectrum createMultiScanCassisSpectrum(
			String title, double[] frequencies, double[][] intensitiesSubScans,
			double vlsrValue, XAxisCassis xAxisCassis, YAxisCassis yAxisCassis) {
		double[] finalFrequencies = Arrays.copyOf(frequencies, frequencies.length);
		int[] indexes = UtilArrayList.incrementalFill(finalFrequencies.length);
		UtilArrayList.quicksortindex(finalFrequencies, indexes, 0, finalFrequencies.length - 1);
		double[][] finalIntensitiesSubScans =
				UtilArrayList.recreateTwoDimArraysSortedByIndexes(
						intensitiesSubScans, indexes);
		final MultiScanCassisSpectrum multiScanCassisSpectrum = new MultiScanCassisSpectrum(title, finalFrequencies,
				finalIntensitiesSubScans, vlsrValue, xAxisCassis, yAxisCassis);

		return multiScanCassisSpectrum;
	}

}
