/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit.computing;

import java.util.Arrays;

import eu.omp.irap.cassis.fit.util.FitCurve;

/**
 * @author bastienkovac
 *
 * Class used to estimate various component's parameters from a specific
 * range in the given data.
 *
 * Each estimation is computed only once to avoid unnecessary work
 *
 */
public class FitEstimator {

	private double start, end;
	private FitCurve curve;
	private double[] x, y;

	private double estimatedI0, estimatedX0, estimatedFwhm;
	private double estimatedSincFwhm;
	private double estimatedSinFreq;
	private int middleIndex;

	private boolean is0Estimated, isFwhmEstimated, isSincFwhmEstimated, isSinFreqEstimated;

	/**
	 * Constructor
	 *
	 * @param startValue First value of the range to estimate
	 * @param endValue Last value of the range to estimate
	 * @param curve Source data
	 */
	public FitEstimator(double startValue, double endValue, FitCurve curve) {
		this.curve = curve;
		this.start = startValue;
		this.end = endValue;
		initializeArrays();
	}

	/**
	 * Constructor from given x0 value
	 *
	 * @param x0 The x0 reference value
	 * @param curve Source data
	 */
	public FitEstimator(double x0, FitCurve curve) {
		this.curve = curve;
		this.start = curve.getMinX();
		this.end = curve.getMaxX();
		initializeArrays();
		this.estimatedX0 = x0;
		initFromX0();
	}

	private void initFromX0() {
		double currentDelta = Double.MAX_VALUE;
		for (int i = 0 ; i < x.length ; i++) {
			double previousDelta = currentDelta;
			currentDelta = getDelta(estimatedX0, x[i]);
			if (currentDelta > previousDelta) {
				this.middleIndex = i == 0 ? i : i - 1;
				this.estimatedI0 = y[middleIndex];
				break;
			}
		}
		is0Estimated = true;
	}

	/**
	 * Returns the estimated i0, which is the peak y-value of
	 * the data
	 *
	 * @return The estimated i0
	 */
	public double getEstimatedI0() {
		if (!is0Estimated) {
			estimateI0AndX0();
		}
		return estimatedI0;
	}

	/**
	 * Returns the estimated x0, which is the x-value corresponding to
	 * the peak y-value of the data
	 *
	 * @return The estimated x0
	 */
	public double getEstimatedX0() {
		if (!is0Estimated) {
			estimateI0AndX0();
		}
		return estimatedX0;
	}

	/**
	 * Returns the estimated FWHM, which is the width of the data at the
	 * height equals to half the peak y-value
	 *
	 * @return The estimated FWHM
	 */
	public double getEstimatedFWHM() {
		if (!isFwhmEstimated) {
			estimateFWHM();
		}
		return estimatedFwhm;
	}

	/**
	 * Returns the estimated FWHM for the SinC component, which
	 * is the distance between the first two x-values
	 * crossing 0 (starting from x0), divided by 2*pi
	 *
	 * @return The estimated FWHM for SinC component
	 */
	public double getEstimatedSincFWHM() {
		if (!isSincFwhmEstimated) {
			estimateSincFWHM();
		}
		return estimatedSincFwhm;
	}

	/**
	 * Returns the estimated frequency of a sinusoidal
	 *
	 * @return The estimated frequency of a sinusoidal
	 */
	public double getEstimatedSinFreq() {
		if (!isSinFreqEstimated) {
			estimateSinFreq();
		}
		return estimatedSinFreq;
	}

	/**
	 * Returns a {@link FitCurve} that only matches the range studied by
	 * this estimator
	 *
	 * @return A FitCurve corresponding to the range
	 */
	public FitCurve getRangeAsCurve() {
		double[] restrictedX = Arrays.copyOf(x, x.length);
		double[] restrictedY = Arrays.copyOf(y, y.length);

		int rangeLength = 0;
		for (int i = 0 ; i < x.length ; i++) {
			double xValue = x[i];
			if (xValue >= start && xValue <= end) {
				restrictedX[rangeLength] = xValue;
				restrictedY[rangeLength] = y[i];
				rangeLength++;
			}
		}
		restrictedX = Arrays.copyOf(restrictedX, rangeLength);
		restrictedY = Arrays.copyOf(restrictedY, rangeLength);
		return new FitCurve(curve.getName(), restrictedX, restrictedY);
	}

	private void initializeArrays() {
		x = curve.getX();
		y = curve.getY();
	}

	private void estimateI0AndX0() {
		double max = 0;
		for (int i = 0 ; i < x.length ; i++) {
			double xValue = x[i];
			if (xValue > start && xValue < end) {
				double yValue = y[i];
				if (yValue > max) {
					max = yValue;
					estimatedI0 = max;
					estimatedX0 = xValue;
					middleIndex = i;
				}
			}
		}
		is0Estimated = true;
	}

	private void estimateFWHM() {
		double currentDelta = Double.MAX_VALUE;
		double xBestDelta = getEstimatedX0();
		for (int i = middleIndex ; i >= 0 ; i--) {
			double previousDelta = currentDelta;
			currentDelta = getDelta(estimatedI0 / 2, y[i]);
			xBestDelta = x[i];
			if (currentDelta > previousDelta) {
				currentDelta = previousDelta;
				break;
			}
		}
		estimatedFwhm = (estimatedX0 - xBestDelta) * 2;
		isFwhmEstimated = true;
	}

	/**
	 * FWHM for sinc is the distance between the first two x values
	 * crossing 0 (starting from x0), divided by 2*pi
	 */
	private void estimateSincFWHM() {
		double firstPoint = getEstimatedX0();
		for (int i = middleIndex ; i >= 0 && y[i] > 0 ; i--) {
			firstPoint = x[i];
		}
		double secondPoint = getEstimatedX0();
		for (int i = middleIndex ; i < x.length && y[i] > 0 ; i++) {
			secondPoint = x[i];
		}
		estimatedSincFwhm = getDelta(firstPoint, secondPoint) / (2 * Math.PI);
		isSincFwhmEstimated = true;
	}

	/**
	 * The sinus frequency is the distance between x0 and x1, where x0 is chosen randomly
	 * and where x1 is the second time the corresponding y value is equal to y0.
	 *
	 * We compute this frequency by starting from a maximum of the curve, and draw a line
	 * between this maximum and each subsequent point. When the slope of this line is closest to
	 * 0, we compute the distance.
	 */
	private void estimateSinFreq() {
		double xBestDelta = getEstimatedX0();
		double currentSlope = Math.abs(getSlope(middleIndex, middleIndex));
		boolean isAscending = false;
		estimatedSinFreq = 1;
		for (int i = middleIndex + 1 ; i < y.length ; i++) {
			double previousSlope = currentSlope;
			currentSlope = Math.abs(getSlope(middleIndex, i));
			xBestDelta = x[i];
			if (!isAscending && currentSlope < previousSlope) {
				isAscending = true;
			}
			if (isAscending && currentSlope > previousSlope) {
				currentSlope = previousSlope;
				break;
			}
		}
		estimatedSinFreq = 1 / getDelta(getEstimatedX0(), xBestDelta);
		isSinFreqEstimated = true;
	}

	private double getSlope(int i0, int i1) {
		if (i0 == i1) {
			return 0;
		}
		return (y[i1] - y[i0]) / (x[i1] - x[i0]);
	}

	private double getDelta(double y0, double y1) {
		return Math.abs(y1 - y0);
	}

}
