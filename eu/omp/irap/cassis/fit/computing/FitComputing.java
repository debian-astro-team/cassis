/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit.computing;

import java.util.ArrayList;
import java.util.List;

import eu.omp.irap.cassis.fit.FitComponentManager;
import eu.omp.irap.cassis.fit.FitParametersModel;
import eu.omp.irap.cassis.fit.FitResult;
import eu.omp.irap.cassis.fit.util.Category;
import eu.omp.irap.cassis.fit.util.FitException;
import eu.omp.irap.cassis.fit.util.FitNormalizationUtil;
import eu.omp.irap.cassis.fit.util.enums.FitStyle;
import herschel.ia.numeric.Double1d;
import herschel.ia.numeric.toolbox.basic.StdDev;
import herschel.ia.numeric.toolbox.fit.AbstractModel;
import herschel.ia.numeric.toolbox.fit.IterativeFitter;
import herschel.ia.numeric.toolbox.fit.NullModel;
import herschel.ia.numeric.toolbox.fit.PolynomialModel;

/**
 * @author bastienkovac
 *
 * This class handles all the computation necessary to fit the given model.
 *
 */
public class FitComputing {

	private FitParametersModel parametersModel;
	private FitComponentManager managerModel;

	private FitNormalizationParameters nmParams;
	private Double1d normalizedXData, normalizedYData;
	private int numberOfIterations;
	private double offsetX;
	private double scaleX;
	private static int printfreq = 1;
	private static boolean showNumberOfIterations=false;
	private static boolean showTimeToFit=false;
	private static boolean showFitVerbose = false;

	/**
	 * Constructor
	 *
	 * @param parametersModel The model to fit
	 */
	public FitComputing(FitParametersModel parametersModel) {
		this.parametersModel = parametersModel;
		this.managerModel = this.parametersModel.getManager();
	}

	/**
	 * Compute the fit from the model
	 *
	 * @param computeFitting True if a fitting is performed, false if it is used to display the results
	 * @return An object holding every result from the fit, as well
	 * as the normalization parameters. If an error occured, a NullModel is returned
	 * @throws FitException If something went wrong during the fit
	 */
	public FitResult computeFit(boolean computeFitting) throws FitException {
		normalizeXData();
		return computeFitWithDataNormalized(computeFitting);
	}

	/**
	 * Compute the abstract Model with
	 *
	 * @param computeFitting True if a fitting is performed, false if it is used to display the results
	 * @return An object holding every result from the fit, as well
	 * as the normalization parameters. If an error occured, a NullModel is returned
	 * @throws FitException If something went wrong during the fit
	 */
	public FitResult computeFitWithDataNormalized(boolean computeFitting) throws FitException {
		AbstractModel compoundModel = new NullModel();
		try {
			compoundModel = managerModel.getCompoundModel(nmParams, computeFitting);
			if (!managerModel.areAllParametersFixed() && computeFitting) {
				compoundModel = computeFit(compoundModel);
			}
		} catch (Exception e) {
			throw getAppropriateException(e);
		}
		if (computeFitting) {
			parametersModel.setStdResidualError(computeStdResidualError(compoundModel));
			if (compoundModel.getNext() == null && compoundModel instanceof PolynomialModel) {
				parametersModel.setRms(computeRms());
			}
		}
		return buildResult(compoundModel, computeFitting);
	}

	private FitException getAppropriateException(Exception e) {
		if (e.getMessage() == null) {
			return new FitException("Fit Error", "An error has occured during the fit.", "");
		} else if (e instanceof ArithmeticException && e.getMessage().contains("Ill posed problem; not enough precision.")) {
			return new FitException(
					"Fit error: limits too broad",
					"The limits of the model are too broad to be computed.",
					"Please define more appropriate limits.");
		} else if (e instanceof IllegalArgumentException && "At least one model must be selected to compute fit".equals(e.getMessage())) {
			return new FitException(
					"Fit error: no selected component",
					"There is no component currently selected.",
					"Please select at least one component.");
		} else if (e instanceof RuntimeException && e.getMessage().contains("Matrix is singular.")) {
			return new FitException(
					"Fit error: matrix is singular",
					"The given parameters caused the Fitter to fail.",
					"Please check that your parameters and markers are coherent with the data.");
		}
		return new FitException("Fit error", e.getMessage(), "");
	}

	private FitResult buildResult(AbstractModel compoundModel, boolean fitComputed) {
		List<AbstractModel> extract = managerModel.extractIndividualComponents(compoundModel, fitComputed);
		List<String> extractNames = new ArrayList<>(extract.size());
		for (int i = 0 ; i < extract.size() ; i++) {
			managerModel.interpretResults(extract.get(i), i, nmParams, fitComputed);
			Category category = managerModel.getCategory(i, true);
			extractNames.add(category.getName() + " - Component " + (managerModel.getReducedIndex(i, true) + 1));
		}
		FitResult res = new FitResult(extract, compoundModel, nmParams);
		res.setExtractedNames(extractNames);
		return res;
	}

	private AbstractModel computeFit(AbstractModel abstractModel) {
		final double tolerance = parametersModel.getTolerance();
		final int nbIterations = parametersModel.getNbIterations();
		final FitStyle fitStyle = managerModel.getSelectedFitStyle();

		if (showFitVerbose) {
			double[] x = parametersModel.getStudiedCurve().getX();
			System.out.println("xdatasOrigin = " + "[ nbPoints : "+ x.length+ "; "+
					x[0] + "; " + x[x.length-1]+ "]");
			System.out.println("offsetX = " + offsetX + ", multX = " + scaleX);
			System.out.println("xdatasTofit = " + "[ nbPoints : "+ normalizedXData.getSize()+ "; "+
					normalizedXData.get(0) + "; " + normalizedXData.get(normalizedXData.getSize()-1)+ "]");
			System.out.println("Parameters = " + abstractModel.getParameters());
			System.out.println("tolerance = " + tolerance + ", nbIterations =" + nbIterations +
					",fitStyle = " +  fitStyle);
		}
		long currentTimeMillis = System.currentTimeMillis();

		abstractModel = computeFit(normalizedXData, normalizedYData,  fitStyle, nbIterations, tolerance, abstractModel);

		if (showFitVerbose) {
			System.out.println("abstractModel parameters fitted= " + abstractModel.getParameters());
		}
		if(showTimeToFit){
			System.out.println("Time = " + String.valueOf((System.currentTimeMillis() - currentTimeMillis)/1000) + "s");
		}

		if (showNumberOfIterations){
			System.out.println("Nb iterations = " + numberOfIterations);
		}
		return abstractModel;
	}

	/**
	 * @param abstractModel
	 * @param tolerance
	 * @param nbIterations
	 * @param fitStyle
	 * @param xdatas
	 * @param ydatas
	 */
	private AbstractModel computeFit(Double1d xdatas, Double1d ydatas,
			final FitStyle fitStyle, final int nbIterations,final double tolerance, AbstractModel abstractModel) {
		IterativeFitter fitter = getIterativeFitter(abstractModel, fitStyle, xdatas);
		fitter.setTolerance(tolerance);
		fitter.setMaxIterations(nbIterations);
		if (showFitVerbose){
			fitter.setVerbose(printfreq);
		}
		fitter.fit(ydatas);
		fitter.getStandardDeviation();
		numberOfIterations = fitter.getNumberOfIterations();
		return abstractModel;
	}

	/**
	 * The Y normalization is not currently implemented
	 */
	private void normalizeXData() {
		normalizedXData = new Double1d(parametersModel.getStudiedCurve().getX());
		normalizedYData = new Double1d(parametersModel.getStudiedCurve().getY());

		offsetX = FitNormalizationUtil.computeOffset(normalizedXData);
		normalizedXData = normalizedXData.add(offsetX);
		scaleX = FitNormalizationUtil.computeScale(normalizedXData); // Compute scale after we applied offset
		normalizedXData = normalizedXData.multiply(scaleX);

		this.nmParams = new FitNormalizationParameters(offsetX, scaleX, 0, 1);
	}

	private IterativeFitter getIterativeFitter(AbstractModel compoundModel, FitStyle fitstyle, Double1d xdata) {
		return fitstyle.getIterativeFitter(xdata, compoundModel);
	}

	/**
	 * Computes the standard deviation error of the residual
	 *
	 * @param model Compound model of the fit
	 * @return Std Error on the model
	 */
	private double computeStdResidualError(AbstractModel model) {
		Double1d yDataResult = model.result(normalizedXData);

		// Computing SSE
		double sse = 0;
		for (int i = 0 ; i < yDataResult.getSize() ; i++) {
			sse += Math.pow(normalizedYData.get(i) - yDataResult.get(i), 2);
		}
		// Computing MSE
		double mse = sse / (normalizedXData.getSize() - model.getNumberOfFittedParameters() - 1);
		// Return RMSE
		return Math.sqrt(mse);
	}

	/**
	 * Computes the RMS
	 *
	 * @return the RMS
	 */
	private double computeRms() {
		AbstractModel abstractModel = new PolynomialModel(1);
		final double tolerance = parametersModel.getTolerance();
		final int nbIterations = parametersModel.getNbIterations();
		final FitStyle fitStyle = managerModel.getSelectedFitStyle();

		abstractModel = computeFit(normalizedXData, normalizedYData,  fitStyle, nbIterations, tolerance, abstractModel);

		Double1d yPoly = abstractModel.result(normalizedXData);
		Double1d y = new Double1d();
		for (int cpt = 0 ; cpt < yPoly.getSize() ; cpt++) {
			y.append(normalizedYData.get(cpt) - yPoly.get(cpt));
		}
		return StdDev.FUNCTION.calc(y);
	}

	/**
	 * @return the numberOfIterations
	 */
	public int getNumberOfIterations() {
		return numberOfIterations;
	}


	public static int getPrintfreq() {
		return printfreq;
	}


	public static void setPrintfreq(int printfreq) {
		FitComputing.printfreq = printfreq;
	}


	public static boolean isShowNumberOfIterations() {
		return showNumberOfIterations;
	}


	public static void setShowNumberOfIterations(boolean showNumberOfIterations) {
		FitComputing.showNumberOfIterations = showNumberOfIterations;
	}


	public static boolean isShowTimeToFit() {
		return showTimeToFit;
	}


	public static void setShowTimeToFit(boolean showTimeToFit) {
		FitComputing.showTimeToFit = showTimeToFit;
	}


	public static boolean isShowFitVerbose() {
		return showFitVerbose;
	}


	public static void setShowFitVerbose(boolean showFitVerbose) {
		FitComputing.showFitVerbose = showFitVerbose;
	}

}
