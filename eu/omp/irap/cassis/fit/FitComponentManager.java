/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import eu.omp.irap.cassis.common.events.ListenerManager;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.fit.components.FitAbstractComponent;
import eu.omp.irap.cassis.fit.components.FitComponent;
import eu.omp.irap.cassis.fit.components.FitMultiComponent;
import eu.omp.irap.cassis.fit.components.FitParameter;
import eu.omp.irap.cassis.fit.components.impl.FitPolynomialComponent;
import eu.omp.irap.cassis.fit.computing.FitEstimator;
import eu.omp.irap.cassis.fit.computing.FitNormalizationParameters;
import eu.omp.irap.cassis.fit.util.Category;
import eu.omp.irap.cassis.fit.util.enums.FitParameterType;
import eu.omp.irap.cassis.fit.util.enums.FitStyle;
import eu.omp.irap.cassis.fit.util.enums.FitType;
import herschel.ia.numeric.Double1d;
import herschel.ia.numeric.Range;
import herschel.ia.numeric.toolbox.fit.AbstractModel;
import herschel.ia.numeric.toolbox.fit.ComboModel;
import herschel.ia.numeric.toolbox.fit.PolynomialModel;

/**
 * @author bastienkovac
 *
 * This class handles every component to be used during the fitting.
 * It allows adding or removing any {@link FitAbstractComponent}.
 *
 */
public class FitComponentManager extends ListenerManager {

	public static final String COMPONENT_MANAGER_COMPONENT_ADDED_EVENT = "componentManagerComponentAdded";
	public static final String COMPONENT_MANAGER_CLEARED_EVENT = "componentManagerCleared";
	public static final String COMPONENT_MANAGER_COMPONENT_REMOVED_EVENT = "componentManagerComponentRemoved";
	public static final String COMPONENT_MANAGER_ALL_SELECTED_EVENT = "componentManagerAllSelected";
	public static final String COMPONENT_MANAGER_STYLE_SELECTED_EVENT = "componentManagerSelectedStyle";
	public static final String COMPONENT_MANAGER_CATEGORY_REMOVED_EVENT = "componentManagerCategoryRemoved";
	public static final String COMPONENT_MANAGER_CLONED_COMPONENT = "componentManagerClonedComponent";

	public static final Category DEFAULT_CATEGORY = new Category("Other", Integer.MAX_VALUE);
	public static final Category BASELINE_CATEGORY = new Category("Baseline", Integer.MIN_VALUE);

	private static final String COMPONENT_LABEL = "Component ";
	private static final String CATEGORY_LABEL = "category";

	private SortedMap<Category, List<FitAbstractComponent>> categoryComponents;

	private FitStyle selectedFitStyle = FitStyle.LEVENBERG;

	private String estimableComponentId;


	/**
	 * Constructor
	 */
	public FitComponentManager() {
		this.categoryComponents = new TreeMap<>();
		this.categoryComponents.put(DEFAULT_CATEGORY, new ArrayList<FitAbstractComponent>());
		this.categoryComponents.put(BASELINE_CATEGORY, new ArrayList<FitAbstractComponent>());
	}

	/**
	 * Selects the given fit style. Also forwards the event to every component
	 * in the manager so their view can change accordingly.
	 *
	 * @param fitStyle The style to select
	 */
	public void selectFitStyle(FitStyle fitStyle) {
		if (this.selectedFitStyle != fitStyle) {
			this.selectedFitStyle = fitStyle;
			fireDataChanged(new ModelChangedEvent(COMPONENT_MANAGER_STYLE_SELECTED_EVENT, fitStyle));
			for (List<FitAbstractComponent> components : categoryComponents.values()) {
				for (FitAbstractComponent component : components) {
					component.fireDataChanged(new ModelChangedEvent(COMPONENT_MANAGER_STYLE_SELECTED_EVENT, fitStyle));
				}
			}
		}
	}

	/**
	 * @return The currently selected fit style
	 */
	public FitStyle getSelectedFitStyle() {
		return selectedFitStyle;
	}

	/**
	 * @return An unmodifiable list of all components currently held by the
	 * manager
	 */
	public List<FitAbstractComponent> getComponents() {
		List<FitAbstractComponent> comp = new ArrayList<>();
		for (List<FitAbstractComponent> list : categoryComponents.values()) {
			comp.addAll(list);
		}
		return comp;
	}

	/**
	 * Returns all the components linked to the given category
	 *
	 * @param category The category
	 * @return A list of all the components that belong to the category
	 */
	public List<FitAbstractComponent> getComponents(Category category) {
		return categoryComponents.get(category);
	}

	/**
	 * @return All the categories defined by the manager
	 */
	public Set<Category> getCategories() {
		return categoryComponents.keySet();
	}

	/**
	 * Removes the given category from the manager, with all its included components
	 *
	 * @param category The category to remove
	 */
	public void removeCategory(Category category) {
		this.categoryComponents.remove(category);
		fireDataChanged(new ModelChangedEvent(COMPONENT_MANAGER_CATEGORY_REMOVED_EVENT, category));
	}

	/**
	 * Returns the category for the given model, with the default one being returned
	 * if the model isn't in the manager
	 *
	 * @param model The model
	 * @return The correct category
	 */
	public Category getCategory(FitAbstractComponent model) {
		Category category = model.getComponentType().isBaseline() ? BASELINE_CATEGORY : DEFAULT_CATEGORY;
		for (Entry<Category, List<FitAbstractComponent>> entry : categoryComponents.entrySet()) {
			if (entry.getValue().contains(model)) {
				category = entry.getKey();
				break;
			}
		}
		return category;
	}

	/**
	 * Adds a component to the given category.
	 *
	 * @param category The category in which to add the component
	 * @param component The added component
	 * @param indexOfClone If the added component was a clone, the index of the cloned component
	 */
	public void addAbstractComponent(Category category, FitAbstractComponent component, int indexOfClone) {
		initList(category);
		if (indexOfClone >= 0) {
			initName(component, indexOfClone + 2);
			categoryComponents.get(category).add(indexOfClone + 1, component);
		} else {
			initName(component, categoryComponents.get(category).size() + 1);
			categoryComponents.get(category).add(component);
		}
		fireDataChanged(new ModelChangedEvent(COMPONENT_MANAGER_COMPONENT_ADDED_EVENT, component, category));
		component.fireDataChanged(new ModelChangedEvent(COMPONENT_MANAGER_STYLE_SELECTED_EVENT, selectedFitStyle));
	}

	private void initList(Category category) {
		if (!categoryComponents.containsKey(category)) {
			categoryComponents.put(category, new ArrayList<FitAbstractComponent>());
		}
	}

	/**
	 * Adds a component to the given category.
	 *
	 * @param category The category in which to add the component
	 * @param component The added component
	 */
	public void addAbstractComponent(Category category, FitAbstractComponent component) {
		addAbstractComponent(category, component, -1);
	}

	private void initName(FitAbstractComponent component, int index) {
		if (component instanceof FitMultiComponent && component.getName().isEmpty()) {
			component.setName(COMPONENT_LABEL + index + " : Multi-" + component.getComponentType().getLabel());
		} else if (component.getName().isEmpty()) {
			component.setName(COMPONENT_LABEL + index + " : " + component.getComponentType().getLabel());
		}
	}

	/**
	 * Adds the given {@link FitAbstractComponent} to the manager. It will be
	 * added to the default category, or the baseline one depending of its type
	 *
	 * @param component The component to add
	 */
	public void addAbstractComponent(FitAbstractComponent component) {
		if (component.getComponentType().isBaseline()) {
			addAbstractComponent(BASELINE_CATEGORY, component, -1);
		} else {
			addAbstractComponent(DEFAULT_CATEGORY, component, -1);
		}
	}

	/**
	 * Sets the given component as the one that will be estimated.
	 * This method is called on the last added component each time one is created
	 *
	 * @param component The component to estimate
	 */
	public void setEstimableComponent(FitAbstractComponent component) {
		this.estimableComponentId = component == null ? null : component.getId();
		for (List<FitAbstractComponent> components : categoryComponents.values()) {
			for (FitAbstractComponent comp : components) {
				if (comp.getId().equals(estimableComponentId)) {
					comp.setEstimated(!comp.isEstimated());
				} else {
					comp.setEstimated(false);
					if (comp.isMultiple()) {
						((FitMultiComponent) comp).setEstimableSibling(null);
					}
				}
			}
		}
	}

	/**
	 * Returns the component that is currently estimable by the manager.
	 * If no estimable component is set, returns null
	 *
	 * @return The estimable component, or null if no one exists
	 */
	public FitAbstractComponent getEstimableComponent() {
		for (List<FitAbstractComponent> components : categoryComponents.values()) {
			for (FitAbstractComponent comp : components) {
				if (comp.getId().equals(estimableComponentId)) {
					return comp;
				}
			}
		}
		return null;
	}

	/**
	 * Adds a {@link FitComponent} corresponding to the given {@link FitType}
	 * to the manager in the default category
	 *
	 * @param type The type of the builded component
	 */
	public void addIndividualComponent(FitType type) {
		FitAbstractComponent component = type.buildIndividualComponent();
		addAbstractComponent(component);
	}

	/**
	 * Adds a {@link FitComponent} corresponding to the given {@link FitType}
	 * to the manager in the given category
	 *
	 * @param category The category in which to add the component
	 * @param type The type of the builded component
	 */
	public void addIndividualComponent(Category category, FitType type) {
		FitAbstractComponent component = type.buildIndividualComponent();
		addAbstractComponent(category, component);
	}

	/**
	 * Adds a {@link FitMultiComponentModel} corresponding to the given {@link FitType}
	 * to the manager in the default category
	 *
	 * @param type The type of the builded component
	 * @param defaultSize The initial number of children
	 */
	public void addMultiComponent(FitType type, int defaultSize) {
		FitAbstractComponent component = type.buildMultiComponent(defaultSize);
		addAbstractComponent(component);
	}

	/**
	 * Adds a {@link FitMultiComponentModel} corresponding to the given {@link FitType}
	 * to the manager in the given category
	 *
	 * @param category The category in which to add the component
	 * @param type The type of the builded component
	 * @param defaultSize The initial number of children
	 */
	public void addMultiComponent(Category category, FitType type, int defaultSize) {
		FitAbstractComponent component = type.buildMultiComponent(defaultSize);
		addAbstractComponent(category, component);
	}

	/**
	 * Removes the given component from the manager
	 *
	 * @param component The component to remove
	 */
	public void removeComponent(FitAbstractComponent component) {
		boolean empty = false;
		Category category = null;
		for (Entry<Category, List<FitAbstractComponent>> entry : categoryComponents.entrySet()) {
			if (entry.getValue().remove(component)) {
				empty = entry.getValue().isEmpty();
				category = entry.getKey();
			}
		}
		updateTitles();
		fireDataChanged(new ModelChangedEvent(COMPONENT_MANAGER_COMPONENT_REMOVED_EVENT, component.getId()));
		if (empty && category != null) {
			removeCategory(category);
		}
	}

	/**
	 * Creates a component that is a copy of the given one, then adds it to the
	 * manager
	 *
	 * @param source The component to copy
	 */
	public void copyComponent(FitAbstractComponent source) {
		Category sourceCategory = source.getComponentType().isBaseline() ? BASELINE_CATEGORY : DEFAULT_CATEGORY;
		int indexOfClone = -1;
		for (Entry<Category, List<FitAbstractComponent>> entry : categoryComponents.entrySet()) {
			for (int i = 0 ; i < entry.getValue().size() ; i++) {
				if (entry.getValue().get(i).equals(source)) {
					sourceCategory = entry.getKey();
					indexOfClone = i;
					break;
				}
			}
		}
		FitAbstractComponent component = FitAbstractComponent.cloneComponent(source);
		addAbstractComponent(sourceCategory, component, indexOfClone);
		updateTitles();
		component.fireDataChanged(new ModelChangedEvent(COMPONENT_MANAGER_STYLE_SELECTED_EVENT, selectedFitStyle));
		fireDataChanged(new ModelChangedEvent(COMPONENT_MANAGER_CLONED_COMPONENT, component.getId(), source.getId()));
	}

	/**
	 * Set every component in the manager selected
	 *
	 * @param selected The value of selected
	 */
	public void setSelectedAll(boolean selected) {
		for (List<FitAbstractComponent> components : categoryComponents.values()) {
			for (FitAbstractComponent comp : components) {
				comp.setSelected(selected);
			}
		}
		fireDataChanged(new ModelChangedEvent(COMPONENT_MANAGER_ALL_SELECTED_EVENT, !selected));
	}

	/**
	 * Estimate the component that has been defined as estimable (only one at a time)
	 * If the component is multiple, we also set its next sibling as estimable if possible
	 *
	 * @param estimator The estimator
	 */
	public void estimateComponent(FitEstimator estimator) {
		for (List<FitAbstractComponent> components : categoryComponents.values()) {
			if (estimate(components, estimator)) {
				break;
			}
		}
	}

	private boolean estimate(List<FitAbstractComponent> components, FitEstimator estimator) {
		for (FitAbstractComponent component : components) {
			if (component.getId().equals(estimableComponentId) && component.isSelected()) {
				component.estimateComponent(estimator);
				estimateNext(component);
				return true;
			}
		}
		return false;
	}

	private void estimateNext(FitAbstractComponent previous) {
		if (previous.isMultiple()) {
			FitMultiComponent mModel = (FitMultiComponent) previous;
			if (!mModel.setNextEstimable()) {
				setEstimableComponent(null);
			}
		} else {
			setEstimableComponent(null);
		}
	}

	/**
	 * Deletes every component in the manager
	 */
	public void clearManager() {
		categoryComponents.clear();
		categoryComponents.put(DEFAULT_CATEGORY, new ArrayList<FitAbstractComponent>());
		categoryComponents.put(BASELINE_CATEGORY, new ArrayList<FitAbstractComponent>());
		fireDataChanged(new ModelChangedEvent(COMPONENT_MANAGER_CLEARED_EVENT));
	}

	/**
	 * Saves the manager in the given Properties
	 *
	 * @param properties The Properties object
	 */
	public void save(Properties properties) {
		properties.setProperty("fitStyle", String.valueOf(selectedFitStyle));
		int nbComponents = 0;
		int nbCategories = 0;
		for (Entry<Category, List<FitAbstractComponent>> entry : categoryComponents.entrySet()) {
			saveCategory(properties, entry.getKey(), nbCategories);
			List<FitAbstractComponent> components = entry.getValue();
			for (int i = 0 ; i < components.size() ; i++, nbComponents++) {
				components.get(i).saveComponent(nbComponents, properties, "");
				properties.setProperty("categoryIndexComponent" + nbComponents, String.valueOf(nbCategories));
				properties.setProperty("component" + nbComponents + "Selected", String.valueOf(components.get(i).isSelected()));
			}
			nbCategories++;
		}
		properties.setProperty("nbComponents", String.valueOf(nbComponents));
		properties.setProperty("nbCategories", String.valueOf(nbCategories));
	}

	private void saveCategory(Properties properties, Category category, int index) {
		properties.setProperty(CATEGORY_LABEL + index + "Name", category.getName());
		properties.setProperty(CATEGORY_LABEL + index + "Id", String.valueOf(category.getId()));
	}

	/**
	 * Load the manager from the given Properties
	 *
	 * @param properties The Properties object
	 */
	public void load(Properties properties) {
		clearManager();
		selectFitStyle(FitStyle.valueOf(properties.getProperty("fitStyle")));
		int nbCat = Integer.parseInt(properties.getProperty("nbCategories"));
		for (int i = 0 ; i < nbCat ; i++) {
			loadCategory(properties, i);
		}
		int nbComp = Integer.parseInt(properties.getProperty("nbComponents"));
		for (int i = 0 ; i < nbComp ; i++) {
			loadComponent(properties, i);
		}
	}

	private void loadCategory(Properties properties, int i) {
		int id = Integer.parseInt(properties.getProperty(CATEGORY_LABEL + i + "Id"));
		String name = properties.getProperty(CATEGORY_LABEL + i + "Name");
		categoryComponents.put(new Category(name, id), new ArrayList<FitAbstractComponent>());
	}

	private void loadComponent(Properties properties, int i) {
		Category compCategory = DEFAULT_CATEGORY;
		int indexCategory = Integer.parseInt(properties.getProperty("categoryIndexComponent" + i));
		int idCategory = Integer.parseInt(properties.getProperty(CATEGORY_LABEL + indexCategory + "Id"));
		for (Category category : categoryComponents.keySet()) {
			if (category.getId() == idCategory) {
				compCategory = category;
				break;
			}
		}
		addAbstractComponent(compCategory, FitAbstractComponent.loadComponent(properties, i));
		FitAbstractComponent comp = getComponent(i, false);
		comp.setSelected(Boolean.valueOf(properties.getProperty("component" + i + "Selected")));
	}

	/**
	 * Extract the individual {@link AbstractModel} corresponding to the
	 * component currently held by the manager from a given compound
	 * {@link AbstractModel}, corresponding to the general result of the fit.
	 *
	 * @param generalModel The compound result (That is the sum of every individual model) of the fit
	 * @param fitComputed Indicates if a fit has been computed (if not, the standard deviations are not available)
	 * @return A list of individual {@link AbstractModel}, each corresponding to one component of
	 * the manager
	 */
	public List<AbstractModel> extractIndividualComponents(AbstractModel generalModel, boolean fitComputed) {
		List<AbstractModel> extract = new ArrayList<>();
		int offset = 0;
		for (int i = 0 ; i < getNumberOfSelectedModels() ; i++) {
			AbstractModel isolate = generalModel.isolateModel(i);
			if (isolate instanceof ComboModel) {
				int nbReduced = isolate.getNumberOfParameters();
				Double1d params = new Double1d();
				for (int j = offset ; j < offset + nbReduced ; j++) {
					params.append(generalModel.getParameters(j));
				}
				isolate.setParameters(params);
			}
			if (!areAllParametersFixed() && fitComputed) {
				Range rangeStd = new Range(offset, offset + isolate.getNumberOfParameters());
				Double1d std = generalModel.getStandardDeviations().get(rangeStd);
				isolate.setStandardDeviations(std);
			}
			extract.add(isolate);
			offset += isolate.getNumberOfParameters();
		}
		return extract;
	}

	/**
	 * @return True if all parameters of all components in the manager
	 * are fixed
	 */
	public boolean areAllParametersFixed() {
		for (List<FitAbstractComponent> components : categoryComponents.values()) {
			for (FitAbstractComponent model : components) {
				if (model.isSelected() && !model.areAllParametersFixed()) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Returns a normalized {@link AbstractModel} that is the sum of every component's
	 * individual ones
	 *
	 * @param nmParams The normalization parameters
	 * @param computeFitting
	 * @return The compound model
	 * @throws IllegalArgumentException If no component is selected
	 */
	public AbstractModel getCompoundModel(FitNormalizationParameters nmParams, boolean computeFitting) throws IllegalArgumentException {
		if (!checkAtLeastOneSelected(categoryComponents.values())) {
			throw new IllegalArgumentException("At least one model must be selected to compute fit");
		}
		Double1d low = new Double1d();
		Double1d high = new Double1d();
		AbstractModel compound = null;
		for (int i = 0; i < getNumberOfSelectedModels(); i++) {
			FitAbstractComponent model = getComponent(i, true);
			buildLimits(low, high, model);
			AbstractModel component = model.getHipeModel(nmParams, selectedFitStyle);
			if (component instanceof PolynomialModel && computeFitting) {
				component.setParameters(new Double1d(new double[((FitPolynomialComponent)(model)).getDegree()+1]));
			}
			if (compound == null) {
				compound = component;
			} else {
				compound.addModel(component);
			}
		}
		for (int i = 0 ; i < low.length() ; i++) {
			compound.getPrior(i).setLimits(new Double1d(new double[] {low.get(i), high.get(i)}));
		}
		return compound;
	}

	private void buildLimits(Double1d low, Double1d high, FitAbstractComponent model) {
		if (!(model instanceof FitPolynomialComponent)) {
			low.append(buildLowLimits(model));
			high.append(buildHighLimits(model));
		} else {
			int degree = ((FitPolynomialComponent) model).getDegree();
			low.append(new Double1d(degree + 1, Double.NEGATIVE_INFINITY));
			high.append(new Double1d(degree + 1, Double.POSITIVE_INFINITY));
		}
	}

	private Double1d buildLowLimits(FitAbstractComponent model) {
		if (model.isMultiple()) {
			return buildLowLimits((FitMultiComponent) model);
		} else {
			return buildLowLimits((FitComponent) model);
		}
	}

	private Double1d buildLowLimits(FitComponent monoModel) {
		Double1d lowLimits = new Double1d();
		for (int i = 0 ; i < monoModel.getNbParameters() ; i++) {
			lowLimits.append(monoModel.getParameter(i).getLowLimit());
		}
		return lowLimits;
	}

	private Double1d buildLowLimits(FitMultiComponent multiModel) {
		Double1d low = new Double1d();
		FitComponent parent = multiModel.getParent();
		for (int k = 0 ; k < parent.getNbParameters() ; k++) {
			FitParameter param = parent.getParameter(k);
			low.append(getCorrectedValue(param, param.getLowLimit()));
		}
		for (int childIndex = 0 ; childIndex < multiModel.getNbChildren() ; childIndex++) {
			FitComponent child = multiModel.getChild(childIndex);
			for (int k = 0 ; k < child.getNbParameters() ; k++) {
				FitParameter param = parent.getParameter(k);
				if (!child.getParameter(k).isConstrained()) {
					low.append(getCorrectedValue(child.getParameter(k), param.getLowLimit()));
				}
			}
		}
		return low;
	}

	private double getCorrectedValue(FitParameter param, double value) {
		if (param.getParameterType().equals(FitParameterType.GAUSSIAN_FWHM)) {
			return value / FitParameterType.calcGaussianFwhmCoeff();
		}
		return value;
	}

	private Double1d buildHighLimits(FitAbstractComponent model) {
		if (model.isMultiple()) {
			return buildHighLimits((FitMultiComponent) model);
		} else {
			return buildHighLimits((FitComponent) model);
		}
	}

	private Double1d buildHighLimits(FitComponent monoModel) {
		Double1d highLimits = new Double1d();
		for (int i = 0 ; i < monoModel.getNbParameters() ; i++) {
			highLimits.append(monoModel.getParameter(i).getHighLimit());
		}
		return highLimits;
	}

	private Double1d buildHighLimits(FitMultiComponent multiModel) {
		Double1d high = new Double1d();
		FitComponent parent = multiModel.getParent();
		for (int k = 0 ; k < parent.getNbParameters() ; k++) {
			FitParameter param = parent.getParameter(k);
			high.append(getCorrectedValue(param, param.getHighLimit()));
		}
		for (int childIndex = 0 ; childIndex < multiModel.getNbChildren() ; childIndex++) {
			FitComponent child = multiModel.getChild(childIndex);
			for (int k = 0 ; k < child.getNbParameters() ; k++) {
				if (!child.getParameter(k).isConstrained()) {
					FitParameter param = parent.getParameter(k);
					if (!child.getParameter(k).isConstrained()) {
						high.append(getCorrectedValue(child.getParameter(k), param.getHighLimit()));
					}
				}
			}
		}
		return high;
	}

	/**
	 * Interprets the results of the i-th component with the given model.
	 *
	 * @param model The {@link AbstractModel} corresponding to the i-th component
	 * @param i Index of the component
	 * @param nmParams Normalization parameters
	 * @param fitComputed Indicates if the fit has been computed. If false, the standard deviations are not
	 * available
	 */
	public void interpretResults(AbstractModel model, int i, FitNormalizationParameters nmParams, boolean fitComputed) {
		getComponent(i, true).interpretResults(model, nmParams, fitComputed);
	}

	/**
	 * Iterates through the sorted map as if all the lists in the value collection
	 * were only one and returns the corresponding component
	 *
	 * @param i The index
	 * @param ignoreUnselected True if the method must ignore the unselected components in its
	 * iteration
	 * @return The component
	 */
	public FitAbstractComponent getComponent(int index, boolean ignoreUnselected) {
		int globalIndex = 0;
		for (Entry<Category, List<FitAbstractComponent>> entry : categoryComponents.entrySet()) {
			for (int i = 0 ; i < entry.getValue().size() ; i++) {
				if (globalIndex == index) {
					return entry.getValue().get(i);
				}
				if ((ignoreUnselected && entry.getValue().get(i).isSelected()) || !ignoreUnselected) {
					globalIndex++;
				}
			}
		}
		return null;
	}

	/**
	 * Iterates through the sorted map as if all the lists in the value collection
	 * were only one and returns the corresponding category
	 *
	 * @param i The index
	 * @param ignoreUnselected True if the method must ignore the unselected components in its
	 * iteration
	 * @return The category
	 */
	public Category getCategory(int index, boolean ignoreUnselected) {
		int globalIndex = 0;
		for (Entry<Category, List<FitAbstractComponent>> entry : categoryComponents.entrySet()) {
			for (int i = 0 ; i < entry.getValue().size() ; i++) {
				if (globalIndex == index) {
					return entry.getKey();
				}
				if ((ignoreUnselected && entry.getValue().get(i).isSelected()) || !ignoreUnselected) {
					globalIndex++;
				}
			}
		}
		return null;
	}

	/**
	 * Returns the reduced index corresponding to the one given as parameter, meaning
	 * the index of the category list it falls in
	 *
	 * @param index The "full" index
	 * @param ignoreUnselected True if the method must ignore the unselected components in its iteration
	 * @return The reduced index
	 */
	public int getReducedIndex(int index, boolean ignoreUnselected) {
		int globalIndex = 0;
		for (Entry<Category, List<FitAbstractComponent>> entry : categoryComponents.entrySet()) {
			for (int i = 0 ; i < entry.getValue().size() ; i++) {
				if (globalIndex == index) {
					return i;
				}
				if ((ignoreUnselected && entry.getValue().get(i).isSelected()) || !ignoreUnselected) {
					globalIndex++;
				}
			}
		}
		return index;
	}

	@Override
	public void removeAllListener() {
		super.removeAllListener();
		for (List<FitAbstractComponent> components : categoryComponents.values()) {
			for (FitAbstractComponent model : components) {
				model.removeAllListener();
			}
		}
	}

	@Override
	public void removeModelListener(ModelListener listener) {
		super.removeModelListener(listener);
		for (List<FitAbstractComponent> components : categoryComponents.values()) {
			for (FitAbstractComponent model : components) {
				model.removeModelListener(listener);
			}
		}
	}

	private int getNumberOfSelectedModels() {
		int i = 0;
		for (List<FitAbstractComponent> components : categoryComponents.values()) {
			for (FitAbstractComponent model : components) {
				if (model.isSelected()) {
					i++;
				}
			}
		}
		return i;
	}

	private boolean checkAtLeastOneSelected(Collection<List<FitAbstractComponent>> models) {
		for (List<FitAbstractComponent> components : models) {
			for (FitAbstractComponent model : components) {
				if (model.isSelected()) {
					return true;
				}
			}
		}
		return false;
	}

	private void updateTitles() {
		for (List<FitAbstractComponent> components : categoryComponents.values()) {
			for (int i = 0 ; i < components.size() ; i++) {
				FitAbstractComponent component = components.get(i);
				String title = component.getName().replaceAll("[0-9]+ :", String.valueOf(i + 1) + " :"); // Update index
				component.setName(title);
			}
		}
	}

}
