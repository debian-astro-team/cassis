/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit.components;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.fit.components.FitConstraint.FitConstraintType;
import eu.omp.irap.cassis.fit.components.impl.FitGaussianComponent;
import eu.omp.irap.cassis.fit.computing.FitNormalizationParameters;
import eu.omp.irap.cassis.fit.util.enums.FitParameterType;
import eu.omp.irap.cassis.fit.util.enums.FitStyle;
import eu.omp.irap.cassis.fit.util.enums.FitType;
import herschel.ia.numeric.Double1d;
import herschel.ia.numeric.Int1d;
import herschel.ia.numeric.toolbox.fit.AbstractModel;

/**
 * @author bastienkovac
 *
 * This class defines an individual fitting component.
 * <br>
 * A component is defined by a type (Gaussian, Polynomial etc), as well as a list
 * of parameters that depends on its type.
 * <br>
 * Most of the work is done in a generic manner, so the classes extending this one
 * have minimal implementation to do, mostly define the proper parameters to use
 * (see {@link FitGaussianComponent} for an example).
 *
 */
public abstract class FitComponent extends FitAbstractComponent {

	private static final String CONSTRAINT_TYPE_LABEL = "ConstraintType";

	// General events
	public static final String PARAMETER_VALUE_EVENT = "parameterValueEvent";
	public static final String PARAMETER_ERROR_EVENT = "parameterErrorEvent";
	public static final String COMPONENT_INTERPRETED_EVENT = "componentInterpretedEvent";
	public static final String COMPONENT_NOW_FIRST_CHILD_EVENT = "componentNowFirstChildEvent";

	// Parent events
	public static final String PARAMETER_FIXED_EVENT = "parameterFixedEvent";
	public static final String PARAMETER_LOW_LIMIT_EVENT = "parameterLowLimitEvent";
	public static final String PARAMETER_HIGH_LIMIT_EVENT = "parameterHighLimitEvent";

	// Child events
	public static final String PARAMETER_CONSTRAINT_EVENT = "parameterConstraintEvent";
	public static final String PARAMETER_FACTOR_EVENT = "parameterFactorEvent";


	private IFollow<FitComponent> followingInterface;
	private Map<FitParameterType, FitParameter> parameters;
	private boolean child;

	/**
	 * Constructor
	 *
	 * @param type The initial type of this component
	 */
	public FitComponent(FitType type) {
		super(type);
		this.parameters = new HashMap<>();
		buildParameters();
	}

	/**
	 * Sets the following interface, this should be called only on the first child
	 * of a {@link FitMultiComponent}, so the modifications on this model
	 * can be dispatched to the siblings returned by the interface's only method
	 *
	 * @param followingInterface The IFollow interface
	 */
	public void setFollowingInterface(IFollow<FitComponent> followingInterface) {
		this.followingInterface = followingInterface;
		fireDataChanged(new ModelChangedEvent(COMPONENT_NOW_FIRST_CHILD_EVENT));
	}

	/**
	 * Sets if this component is child of a {@link FitMultiComponent}, this is used
	 * for display purposes
	 *
	 * @param child The new child value
	 */
	public void setChild(boolean child) {
		this.child = child;
	}

	/**
	 * @return True if the component is child in a {@link FitMultiComponent}
	 */
	public boolean isChild() {
		return child;
	}

	/**
	 * @return True if the component is the first child of a {@link FitMultiComponent}
	 */
	public boolean isFirstChild() {
		return this.followingInterface != null && isChild();
	}

	/**
	 * Returns the parameter at the given index
	 *
	 * @param index The index of the parameter
	 * @return The parameter
	 */
	public FitParameter getParameter(int index) {
		for (FitParameter parameter : parameters.values()) {
			if (parameter.getIndex() == index) {
				return parameter;
			}
		}
		throw new IllegalArgumentException("Wrong index !");
	}

	/**
	 * Sets the fixed value for the parameter at the given index
	 *
	 * @param index Index of the parameter
	 * @param fixed New fixed value
	 */
	public void setParameterFixed(int index, boolean fixed) {
		FitParameter parameter = getParameter(index);
		parameter.setFixed(fixed);
		fireDataChanged(new ModelChangedEvent(PARAMETER_FIXED_EVENT, parameter, getId()));
	}

	/**
	 * Sets a constraint on the parameter at the given index. This is dispatched
	 * to every siblings if the IFollow interface is defined
	 *
	 * @param index Index of the parameter
	 * @param type Type of the constraint to set
	 */
	public void setParameterConstraint(int index, FitConstraintType type) {
		FitParameter parameter = getParameter(index);
		parameter.setConstraint(type);
		if (isFirstChild()) {
			for (FitComponent sibling : followingInterface.getSiblings()) {
				sibling.setParameterConstraint(index, type);
			}
		}
		fireDataChanged(new ModelChangedEvent(PARAMETER_CONSTRAINT_EVENT, parameter, getId()));
	}

	/**
	 * Sets the constraint factor for the parameter at the given index.
	 *
	 * @param index Index of the parameter
	 * @param factor New value of the factor
	 */
	public void setParameterFactor(int index, double factor) {
		FitParameter parameter = getParameter(index);
		parameter.getConstraint().setFactor(factor);
		fireDataChanged(new ModelChangedEvent(PARAMETER_FACTOR_EVENT, parameter, getId()));
	}

	/**
	 * Sets the low limit for the parameter at the given index.
	 *
	 * @param index Index of the parameter
	 * @param low New low limit value
	 */
	public void setParameterLowLimit(int index, double low) {
		FitParameter parameter = getParameter(index);
		parameter.setLowLimit(low);
		fireDataChanged(new ModelChangedEvent(PARAMETER_LOW_LIMIT_EVENT, parameter, getId()));
	}

	/**
	 * Sets the high limit for the parameter at the given index.
	 *
	 * @param index Index of the parameter
	 * @param high New value of the high limit
	 */
	public void setParameterHighLimit(int index, double high) {
		FitParameter parameter = getParameter(index);
		parameter.setHighLimit(high);
		fireDataChanged(new ModelChangedEvent(PARAMETER_HIGH_LIMIT_EVENT, parameter, getId()));
	}

	/**
	 * Sets the value for the parameter at the given index.
	 *
	 * @param index Index of the parameter
	 * @param value New value
	 */
	public void setParameterValue(int index, double value) {
		FitParameter parameter = getParameter(index);
		parameter.setValue(value);
		fireDataChanged(new ModelChangedEvent(PARAMETER_VALUE_EVENT, parameter, getId()));
		if (followingInterface != null && !isChild()) {
			for (FitComponent sibling : followingInterface.getSiblings()) {
				if (sibling.getParameter(index).isConstrained()) {
					sibling.setParameterValue(index, sibling.getParameter(index).getConstraint().constrainValue(value));
				}
			}
		}
	}

	/**
	 * Sets the error for the parameter at the given index.
	 *
	 * @param index Index of the parameter
	 * @param error New value of the error
	 */
	public void setParameterError(int index, double error) {
		FitParameter parameter = getParameter(index);
		parameter.setError(error);
		fireDataChanged(new ModelChangedEvent(PARAMETER_ERROR_EVENT, parameter, getId()));
	}

	/**
	 * Returns an array of the normalized values of this component's parameters
	 *
	 * @param nmParams The normalization parameters
	 * @param style Fit style used
	 * @return An array of normalized values
	 */
	public double[] getNormalizedParameters(FitNormalizationParameters nmParams, FitStyle style) {
		double[] paramValues = new double[getNbParameters()];
		for (int i = 0 ; i < getNbParameters() ; i++) {
			FitParameter param = getParameter(i);
			paramValues[i] = FitParameterType.getNormalizedValue(param, nmParams, style);
		}
		return paramValues;
	}

	/**
	 * Interprets an array of values by denormalizing them and replacing each parameter's value
	 * by the interpreted one
	 *
	 * @param values The values
	 * @param nmParams The normalization parameters
	 */
	public void interpretValues(Double1d values, FitNormalizationParameters nmParams) {
		for (int i = 0 ; i < getNbParameters() ; i++) {
			FitParameter param = getParameter(i);
			if (!param.isFixed()) {
				double interpretedValue = param.getDenormalizedValue(values.get(i), nmParams);
				setParameterValue(i, interpretedValue);
			}
		}
		fireDataChanged(new ModelChangedEvent(COMPONENT_INTERPRETED_EVENT));
	}

	/**
	 * Interprets an array of errors by denormalizing them and replacing each parameter's error
	 * by the interpreted one
	 *
	 * @param deviations The errors
	 * @param nmParams The normalization parameters
	 */
	public void interpretDeviations(Double1d deviations, FitNormalizationParameters nmParams) {
		for (int i = 0 ; i < getNbParameters() ; i++) {
			FitParameter param = getParameter(i);
			double interpretedError = param.getDenormalizedDeviation(deviations.get(i), nmParams);
			setParameterError(i, interpretedError);
		}
	}

	/**
	 * Adds a parameter to the component
	 *
	 * @param parameter
	 */
	protected void addParameter(FitParameter parameter) {
		parameters.put(parameter.getParameterType(), parameter);
	}

	@Override
	public int getNbParameters() {
		return parameters.size();
	}

	@Override
	public void setAllParametersFixed(boolean fixed) {
		for (FitParameter parameter : parameters.values()) {
			parameter.setFixed(fixed);
			fireDataChanged(new ModelChangedEvent(PARAMETER_FIXED_EVENT, parameter, getId()));
		}
	}

	@Override
	public void saveComponent(int index, Properties properties, String prefix) {
		String nameComponent = prefix + "simpleComponent" + index;
		properties.setProperty(nameComponent, String.valueOf(getComponentType()));

		for (int i = 0 ; i < getNbParameters() ; i++) {
			FitParameter parameter = getParameter(i);
			String nameParameter = nameComponent + "Parameter" + i;
			properties.setProperty(nameParameter + "Value", String.valueOf(parameter.getValue()));
			properties.setProperty(nameParameter + "Fixed", String.valueOf(parameter.isFixed()));
			properties.setProperty(nameParameter + "LowLimit", String.valueOf(parameter.getLowLimit()));
			properties.setProperty(nameParameter + "HighLimit", String.valueOf(parameter.getHighLimit()));
			if (parameter.isConstrained()) {
				properties.setProperty(nameParameter + CONSTRAINT_TYPE_LABEL, String.valueOf(parameter.getConstraint().getConstraintType()));
				properties.setProperty(nameParameter + "ConstraintFactor", String.valueOf(parameter.getConstraint().getFactor()));
			}
		}
	}

	@Override
	protected void loadFromProperties(Properties properties, int index, String prefix) {
		String nameComponent = prefix + "simpleComponent" + index;

		for (int i = 0 ; i < getNbParameters() ; i++) {
			String nameParameter = nameComponent + "Parameter" + i;
			setParameterValue(i, Double.valueOf(properties.getProperty(nameParameter + "Value")));
			setParameterFixed(i, Boolean.valueOf(properties.getProperty(nameParameter + "Fixed")));
			setParameterLowLimit(i, Double.valueOf(properties.getProperty(nameParameter + "LowLimit", "0.0")));
			setParameterHighLimit(i, Double.valueOf(properties.getProperty(nameParameter + "HighLimit", "0.0")));
			if (properties.containsKey(nameParameter + CONSTRAINT_TYPE_LABEL)) {
				setParameterConstraint(i, FitConstraintType.valueOf(properties.getProperty(nameParameter + CONSTRAINT_TYPE_LABEL)));
				setParameterFactor(i, Double.valueOf(properties.getProperty(nameParameter + "ConstraintFactor")));
			}
		}
	}

	@Override
	public void copyComponent(FitAbstractComponent source) {
		if (getComponentType().equals(source.getComponentType()) && source instanceof FitComponent) {
			for (int i = 0 ; i < getNbParameters() ; i++) {
				getParameter(i).copyParameter(((FitComponent) source).getParameter(i));
			}
		}
	}

	@Override
	public AbstractModel getHipeModel(FitNormalizationParameters nmParams, FitStyle style) {
		AbstractModel model = getComponentType().buildAppropriateModel();
		double[] values = getNormalizedParameters(nmParams, style);
		model.setParameters(new Double1d(values));
		Int1d fixedParams = new Int1d();
		for (int i = 0 ; i < getNbParameters() ; i++) {
			if (getParameter(i).isFixed() && FitStyle.LEVENBERG.equals(style)) {
				fixedParams.append(i);
			}
		}
		if (FitStyle.LEVENBERG.equals(style)) {
			model.keepFixed(fixedParams);
		}
		return model;
	}

	@Override
	public void interpretResults(AbstractModel resultModel, FitNormalizationParameters nmParams, boolean fitComputed) {
		interpretValues(resultModel.getParameters(), nmParams);
		if (!areAllParametersFixed() && fitComputed) {
			interpretDeviations(resultModel.getStandardDeviations(), nmParams);
		}
		fireDataChanged(new ModelChangedEvent(COMPONENT_INTERPRETED_EVENT));
	}

	@Override
	public boolean areAllParametersFixed() {
		for (int i = 0 ; i < getNbParameters() ; i++) {
			if (!getParameter(i).isFixed()) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean isMultiple() {
		return false;
	}

	@Override
	public String toString() {
		return getName();
	}

	/**
	 * @return A String version of the equation of the fitting model
	 */
	public String getEquation() {
		return null;
	}

	/**
	 * This method is called on the {@link FitComponent} constructor, it
	 * must buildt he different needed parameters and adds them to the component
	 */
	protected abstract void buildParameters();

}
