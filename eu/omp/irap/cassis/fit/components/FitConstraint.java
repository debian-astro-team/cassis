/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit.components;

/**
 * @author bastienkovac
 *
 * This class represents a constraint that is applied on a component's
 * parameter
 *
 */
public class FitConstraint {

	/**
	 * @author bastienkovac
	 *
	 * Enumeration of the various constraint types available
	 *
	 */
	public enum FitConstraintType {
		NONE("None"), EQUAL("Equal."), SHIFT("Shift."), SCALE("Mult."), SCALE_AUTO("Mult. Auto.");

		private String label;

		private FitConstraintType(String name) {
			this.label = name;
		}

		/**
		 * @return The label of the constraint
		 */
		public String getLabel() {
			return label;
		}

		public static final FitConstraintType[] DEFAULT_CONSTRAINTS = new FitConstraintType[] {
				FitConstraintType.NONE, FitConstraintType.EQUAL, FitConstraintType.SHIFT, FitConstraintType.SCALE
		};

		public static final FitConstraintType[] WIDTH_CONSTRAINTS = new FitConstraintType[] {
				FitConstraintType.NONE, FitConstraintType.EQUAL, FitConstraintType.SCALE, FitConstraintType.SCALE_AUTO
		};

	}

	private FitConstraintType constraintType;
	private double factor;


	private FitConstraint(double defaultFactor, FitConstraintType constraintType) {
		this.factor = defaultFactor;
		this.constraintType = constraintType;
	}

	/**
	 * @return The type of the constraint
	 */
	public FitConstraintType getConstraintType() {
		return constraintType;
	}

	/**
	 * @return The factor of the constraint
	 */
	public double getFactor() {
		return factor;
	}

	/**
	 * @param factor Sets the new factor of the constraint
	 */
	public void setFactor(double factor) {
		this.factor = factor;
	}

	/**
	 * Copy the given constraint
	 *
	 * @param source Constraint to copy
	 */
	public void copyConstraint(FitConstraint source) {
		this.constraintType = source.getConstraintType();
		this.factor = source.getFactor();
	}

	/**
	 * Returns a constrained value of the one given as argument.
	 * The constrained value depends on the constraint's type
	 *
	 * @param value The source value
	 * @return The constrained value
	 */
	public double constrainValue(double value) {
		switch (constraintType) {
		case SHIFT:
			return value + factor;
		case SCALE:
		case SCALE_AUTO:
			return value * factor;
		default:
			return value;
		}
	}

	/**
	 * Compute the factor of the constraint, given a constraining value
	 * and a constrained value
	 *
	 * @param constraining The constraining value
	 * @param constrained The constrained value
	 * @return The computed factor
	 */
	public double computeFactor(double constraining, double constrained) {
		switch (constraintType) {
		case SHIFT:
			return constrained - constraining;
		case SCALE:
		case SCALE_AUTO:
			return constrained / constraining;
		default:
			return 1;
		}
	}

	/**
	 * Method used to build a constraint matching the given constraint
	 * type
	 *
	 * @param type The constraint type
	 * @return The matching constraint
	 */
	public static FitConstraint getFitConstraint(FitConstraintType type) {
		switch (type) {
		case EQUAL:
			return getEqualityConstraint();
		case SHIFT:
			return getIntervalConstraint();
		case SCALE:
			return getScalingConstraint();
		case NONE:
			return getNoneConstraint();
		case SCALE_AUTO:
			return getAutoScalingConstraint();
		default:
			throw new IllegalArgumentException("The constraint " + type + " is not implement yet");
		}
	}

	private static FitConstraint getNoneConstraint() {
		return new FitConstraint(0., FitConstraintType.NONE);
	}

	private static FitConstraint getIntervalConstraint() {
		return new FitConstraint(0., FitConstraintType.SHIFT);
	}

	private static FitConstraint getEqualityConstraint() {
		return new FitConstraint(1., FitConstraintType.EQUAL);
	}

	private static FitConstraint getScalingConstraint() {
		return new FitConstraint(1., FitConstraintType.SCALE);
	}

	private static FitConstraint getAutoScalingConstraint() {
		return new FitConstraint(1., FitConstraintType.SCALE_AUTO);
	}

}
