/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit.components.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.text.NumberFormat;

import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.NumberFormatter;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.fit.components.FitAbstractComponent;
import eu.omp.irap.cassis.fit.components.FitComponent;
import eu.omp.irap.cassis.fit.components.gui.table.ComponentTable;
import eu.omp.irap.cassis.fit.components.impl.FitPolynomialComponent;
import eu.omp.irap.cassis.fit.util.enums.FitType;

/**
 * @author bastienkovac
 *
 * This class is used to display a {@link FitComponent}
 *
 */
@SuppressWarnings("serial")
public class FitComponentView extends FitAbstractComponentView implements ModelListener {

	private ComponentTable parametersTable;

	// Polynomial Only
	private JFormattedTextField degree;


	/**
	 * Constructor
	 *
	 * @param model The corresponding model
	 * @param selectable If the component can be selected or unselected
	 * @param cloneable If the component can be cloned
	 * @param clearable If the component can be cleared
	 * @param fixable If the component's parameters can be fixed
	 * @param foldable If the component can be folded
	 */
	public FitComponentView(FitComponent model, boolean selectable, boolean cloneable,
			boolean clearable, boolean fixable, boolean foldable) {
		super(model, selectable, cloneable, clearable, fixable, foldable);
		initLayout();
	}

	/**
	 * Returns a {@link JFormattedTextField} that let the user
	 * change the degree of the polynomial model. It is displayed
	 * only when the underlying model is a {@link FitPolynomialComponent}
	 *
	 * @return The {@link JFormattedTextField}
	 */
	public JFormattedTextField getDegree() {
		if (degree == null) {
			final FitPolynomialComponent poly = (FitPolynomialComponent) getSimpleComponent();
			NumberFormat format = NumberFormat.getIntegerInstance();
			format.setGroupingUsed(false);
			NumberFormatter formatter = new NumberFormatter(format);
			formatter.setAllowsInvalid(false);
			degree = new JFormattedTextField(formatter);
			degree.setColumns(4);
			degree.setHorizontalAlignment(JTextField.RIGHT);
			degree.setValue(poly.getDegree());
			degree.getDocument().addDocumentListener(new DocumentListener() {

				@Override
				public void removeUpdate(DocumentEvent e) {
				}

				@Override
				public void insertUpdate(DocumentEvent e) {
					poly.setDegree(Integer.parseInt(degree.getText()));
					setToolTipText(getSimpleComponent().getEquation());
				}

				@Override
				public void changedUpdate(DocumentEvent e) {
				}
			});
		}
		return degree;
	}

	/**
	 * @return A {@link JTable} that is used to display and manipulate
	 * the model's parameters
	 */
	public JTable getParametersTable() {
		if (parametersTable == null) {
			initTable();
		}
		return parametersTable;
	}

	@Override
	public void dataChanged(ModelChangedEvent e) {
		super.dataChanged(e);
		if (e.getSource().equals(FitAbstractComponent.MODEL_ESTIMATED_EVENT)
				&& !(getSimpleComponent() instanceof FitPolynomialComponent)) {
			getParametersTable().repaint();
		} else if (e.getSource().equals(FitComponent.COMPONENT_INTERPRETED_EVENT)) {
			getParametersTable().getTableHeader().setToolTipText(getSimpleComponent().getEquation());
		} else if (e.getSource().equals(FitPolynomialComponent.POLY_FACTORS_EVENT)) {
			getSubPanel().setToolTipText(getSimpleComponent().getEquation());
		}
	}

	private void initTable() {
		parametersTable = new ComponentTable(getSimpleComponent());
		parametersTable.setAutoCreateColumnsFromModel(false);
		parametersTable.setPreferredScrollableViewportSize(parametersTable.getPreferredSize());
		parametersTable.setFillsViewportHeight(true);
	}

	private void initPolynomialLayout() {
		JPanel panel = new JPanel();

		JLabel degreeLabel = new JLabel("Degree:");

		GroupLayout layout = new GroupLayout(panel);
		layout.setAutoCreateGaps(true);

		layout.setHorizontalGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup(Alignment.LEADING)
						.addComponent(degreeLabel))
				.addGroup(layout.createParallelGroup(Alignment.LEADING)
						.addComponent(getDegree())));

		layout.setVerticalGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(degreeLabel)
						.addComponent(getDegree())));

		panel.setLayout(layout);

		JPanel layoutPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		layoutPanel.add(panel);
		addComponentToSubPanel(layoutPanel);
		remove(getButtonFixAll());
		remove(getButtonCopy());
	}

	private void initRegularLayout() {
		JScrollPane scrollPane = new JScrollPane(getParametersTable());
		scrollPane.setBorder(null);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
		addComponentToSubPanel(scrollPane, BorderLayout.CENTER);
	}

	private void initLayout() {
		if (isBlockable()) {
			((JCheckBox) getLabelTitle()).setEnabled(true);
		}
		setSubPanelLayout(new BoxLayout(getSubPanel(), BoxLayout.Y_AXIS));
		if (getSimpleComponent().getComponentType().equals(FitType.POLY)) {
			initPolynomialLayout();
			getSubPanel().setToolTipText(getSimpleComponent().getEquation());
		} else {
			initRegularLayout();
			getParametersTable().getTableHeader().setToolTipText(getSimpleComponent().getEquation());
		}
		getSubPanel().setMaximumSize(new Dimension(Integer.MAX_VALUE, getPreferredSize().height));
	}

}
