/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit.components.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.fit.FitComponentManager;
import eu.omp.irap.cassis.fit.components.FitComponent;
import eu.omp.irap.cassis.fit.components.FitMultiComponent;
import eu.omp.irap.cassis.fit.gui.util.ButtonTitledPanel;

/**
 * @author bastienkovac
 *
 * This class is the view used to display a {@link FitMultiComponent}
 *
 */
@SuppressWarnings("serial")
public class FitMultiComponentsView extends FitAbstractComponentView implements ModelListener {

	private JButton addComponentButton;
	private JPanel centerPanel, southPanel;

	private List<FitComponentView> individualPanels;


	/**
	 * Constructor
	 *
	 * @param model The corresponding model
	 */
	public FitMultiComponentsView(FitMultiComponent model) {
		super(model, true, true, true, true, true);
		this.individualPanels = new ArrayList<>();
		initFoldButton();
		addComponentToSubPanel(getCenterPanel(), BorderLayout.CENTER);
		addComponentToSubPanel(getSouthPanel(), BorderLayout.SOUTH);
		initializeView();
	}

	private void initFoldButton() {
		getButtonFold().setSelected(getMultiComponent().isFolded());
		addButtonFoldListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				getMultiComponent().setFolded(getButtonFold().isSelected());
			}
		});
	}

	/**
	 * @return A {@link JButton} that is used to add a child to the model
	 */
	public JButton getAddComponentButton() {
		if (addComponentButton == null) {
			addComponentButton = new JButton("Add " + getMultiComponent().getComponentType().getLabel() + " Element");
			addComponentButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					getMultiComponent().addChild();
				}
			});
		}
		return addComponentButton;
	}

	private void updateChildPanelsVisibility() {
		for (int i = 1 ; i < this.individualPanels.size() ; i++) {
			this.individualPanels.get(i).setVisible(!getMultiComponent().isFolded());
		}
		getAddComponentButton().setVisible(!getMultiComponent().isFolded());
		revalidate();
		repaint();
	}

	private void setChildClearButtonEnabled() {
		if (individualPanels.size() == 2) {
			setChildClearButtonEnabled(getMultiComponent().getFirstChild().getId(), false);
		} else {
			setChildClearButtonEnabled(getMultiComponent().getFirstChild().getId(), true);
		}
	}

	@Override
	public void dataChanged(ModelChangedEvent e) {
		super.dataChanged(e);
		if (e.getSource().equals(FitMultiComponent.COMPONENT_ADDED_EVENT)) {
			final FitComponent model = (FitComponent) e.getValue();
			addChildPanel(model);
			setChildClearButtonEnabled();
		} else if (e.getSource().equals(FitMultiComponent.CHILD_REMOVED_EVENT)) {
			removePanel((String) e.getValue());
			setChildClearButtonEnabled();
		} else if (e.getSource().equals(FitComponentManager.COMPONENT_MANAGER_STYLE_SELECTED_EVENT)) {
			getMultiComponent().dispatchEvent(e);
		} else if (e.getSource().equals(FitMultiComponent.MODEL_FOLDED_EVENT)) {
			updateChildPanelsVisibility();
		}
	}

	private JPanel getSouthPanel() {
		if (southPanel == null) {
			southPanel = new JPanel();
			southPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
			southPanel.add(getAddComponentButton());
		}
		return southPanel;
	}

	private JPanel getCenterPanel() {
		if (centerPanel == null) {
			centerPanel = new JPanel();
		}
		return centerPanel;
	}

	private void setChildClearButtonEnabled(String idPanel, boolean enabled) {
		for (JPanel panel : individualPanels) {
			if (idPanel.equals(panel.getName())) {
				((ButtonTitledPanel) panel).getButtonClear().setEnabled(enabled);
			}
		}
	}

	private void addChildPanel(final FitComponent model) {
		FitComponentView panel = new FitComponentView(model, false, false, true, false, false);
		panel.remove(panel.getButtonFixAll());
		panel.addButtonClearListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				getMultiComponent().removeChild(model.getId());
			}
		});
		panel.addMouseListener(getListenerEstimable(model));
		individualPanels.add(panel);
		updateLayout();
	}

	private void removePanel(String name) {
		JPanel panelToRemove = null;
		for (JPanel panel : individualPanels) {
			if (name.equals(panel.getName())) {
				panelToRemove = panel;
				break;
			}
		}
		if (panelToRemove != null) {
			getCenterPanel().remove(panelToRemove);
			individualPanels.remove(panelToRemove);
		}
		getCenterPanel().revalidate();
	}

	private void initializeView() {
		FitComponentView parentPanel = new FitComponentView(getMultiComponent().getParent(), false, false, false, false, false);
		parentPanel.remove(parentPanel.getButtonClear());
		parentPanel.remove(parentPanel.getButtonFixAll());
		parentPanel.addMouseListener(getListenerEstimable(getMultiComponent().getParent()));
		this.individualPanels.add(parentPanel);
		for (int i = 0 ; i < getMultiComponent().getNbChildren() ; i++) {
			addChildPanel(getMultiComponent().getChild(i));
		}
		setChildClearButtonEnabled();
		updateLayout();
	}

	private MouseListener getListenerEstimable(final FitComponent model) {
		return new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				getMultiComponent().getChildEstimationAction().run();
				getMultiComponent().setEstimableSibling(model);
			}
		};
	}

	private void updateLayout() {
		getCenterPanel().setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.weightx = 1;
		constraints.weighty = 1;
		constraints.insets = new Insets(3, 3, 3, 3);
		int y = 0;
		for (JPanel panel : individualPanels) {
			constraints.gridx = 0;
			constraints.gridy = y;
			getCenterPanel().add(panel, constraints);
			y++;
		}
		updateChildPanelsVisibility();
		revalidate();
	}

}
