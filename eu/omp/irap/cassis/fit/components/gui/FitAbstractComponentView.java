/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit.components.gui;

import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JLabel;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.fit.FitComponentManager;
import eu.omp.irap.cassis.fit.components.FitAbstractComponent;
import eu.omp.irap.cassis.fit.components.FitComponent;
import eu.omp.irap.cassis.fit.components.FitMultiComponent;
import eu.omp.irap.cassis.fit.gui.util.ButtonTitledPanel;
import eu.omp.irap.cassis.fit.util.enums.FitStyle;

/**
 * Abstract representation of a component
 *
 * @author bastienkovac
 *
 */
@SuppressWarnings("serial")
public abstract class FitAbstractComponentView extends ButtonTitledPanel implements ModelListener {

	private FitAbstractComponent component;


	public FitAbstractComponentView(FitAbstractComponent component,
			boolean selectable, boolean cloneable, boolean clearable, boolean fixable, boolean foldable) {
		super(component.getName(), selectable, cloneable, foldable);
		if (selectable) {
			((JCheckBox) getLabelTitle()).setSelected(component.isSelected());
		}
		this.component = component;
		this.component.addModelListener(this);
		setName(this.component.getId());
		initBorderButtons(clearable, fixable);
		initListeners();
	}

	@Override
	public void dataChanged(ModelChangedEvent e) {
		if (e.getSource().equals(FitAbstractComponent.MODEL_SELECTED_EVENT)) {
			setAllEnabled(this, component.isSelected());
			handleSelectionCheckBox();
		} else if (e.getSource().equals(FitAbstractComponent.MODEL_NAME_EVENT)) {
			editTitle((String) e.getValue());
		} else if (e.getSource().equals(FitComponent.PARAMETER_FIXED_EVENT)) {
			handleFixAllButton();
		} else if (e.getSource().equals(FitComponentManager.COMPONENT_MANAGER_STYLE_SELECTED_EVENT)) {
			handleStyle((FitStyle) e.getValue());
		}
	}

	protected FitComponent getSimpleComponent() {
		return (FitComponent) component;
	}

	protected FitMultiComponent getMultiComponent() {
		return (FitMultiComponent) component;
	}

	protected void setSubPanelLayout(LayoutManager layout) {
		getSubPanel().setLayout(layout);
	}

	protected void addComponentToSubPanel(Component component) {
		addComponentToSubPanel(component, null);
	}

	protected void addComponentToSubPanel(Component component, Object constraint) {
		getSubPanel().add(component, constraint);
	}

	private void handleStyle(FitStyle style) {
		getButtonFixAll().setEnabled(!FitStyle.AMOEBA.equals(style));
	}

	private void handleSelectionCheckBox() {
		if (!isBlockable()) {
			return;
		}
		getLabelTitle().setEnabled(true);
		((JCheckBox) getLabelTitle()).setSelected(component.isSelected());
	}

	private void handleFixAllButton() {
		if (component.areAllParametersFixed()) {
			getButtonFixAll().setSelected(true);
			getButtonFixAll().setText("Unfix all");
		} else {
			getButtonFixAll().setSelected(false);
			getButtonFixAll().setText("  Fix all  ");
		}
	}

	private void editTitle(String newTitle) {
		if (isBlockable()) {
			JCheckBox checkBoxTitle = (JCheckBox) getLabelTitle();
			checkBoxTitle.setText(newTitle);
		} else {
			JLabel labelTitle = (JLabel) getLabelTitle();
			labelTitle.setText(newTitle);
		}
	}

	private void setAllEnabled(Container c, boolean enabled) {
		for (Component cmp : c.getComponents()) {
			cmp.setEnabled(enabled);
			if (c instanceof Container) {
				setAllEnabled((Container) cmp, enabled);
			}
		}
	}

	private void initListeners() {
		addButtonFixAllListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				component.setAllParametersFixed(getButtonFixAll().isSelected());
			}
		});
		if (isBlockable()) {
			final JCheckBox cb = (JCheckBox)getLabelTitle();
			cb.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					component.setSelected(cb.isSelected());
					getButtonFixAll().setEnabled(cb.isSelected());
					getButtonClear().setEnabled(cb.isSelected());
					getButtonCopy().setEnabled(cb.isSelected());
				}
			});
		}
	}

	private void initBorderButtons(boolean clearable, boolean fixable) {
		if (!clearable) {
			remove(getButtonClear());
		}
		if (!fixable) {
			remove(getButtonFixAll());
		}
	}

}
