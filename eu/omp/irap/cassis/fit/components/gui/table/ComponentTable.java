/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit.components.gui.table;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.text.DecimalFormat;
import java.util.Arrays;

import javax.swing.DefaultCellEditor;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import eu.omp.irap.cassis.fit.components.FitComponent;
import eu.omp.irap.cassis.fit.components.FitConstraint.FitConstraintType;
import eu.omp.irap.cassis.fit.components.impl.FitGaussianComponent;

/**
 * A JTable used to display every parameter of a Fit Component
 *
 * @author bastienkovac
 *
 */
@SuppressWarnings("serial")
public class ComponentTable extends JTable {

	private FitComponent component;
	private FitComponentTableModel fitTableModel;


	/**
	 * Constructor
	 *
	 * @param component
	 *            The component to display
	 */
	public ComponentTable(FitComponent component) {
		this.component = component;
		getTableHeader().setReorderingAllowed(false);
		if (component.isChild()) {
			fitTableModel = new FitChildComponentTableModel(component);
		} else {
			fitTableModel = new FitParentComponentTableModel(component);
		}
		setModel(fitTableModel);
		this.setFillsViewportHeight(true);
		this.setDefaultRenderer(Double.class, new DoubleRenderer());
		initNameRenderer();
		if (component.isChild()) {
			initConstraintsRenderer();
		}
		initTableHeadersWidth();
		initSelectionListener();
		setRowHeight(getRowHeight() + 3);
		putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
	}

	/**
	 * @return The TableModel of the table
	 */
	public FitComponentTableModel getFitTableModel() {
		return fitTableModel;
	}

	public class DoubleRenderer extends JTextField implements TableCellRenderer {

		private static final long serialVersionUID = 1L;

		public DoubleRenderer() {
			setOpaque(true); // MUST do this for background to show up
			this.setHorizontalAlignment(JTextField.RIGHT);
			this.setBorder(new EmptyBorder(0, 0, 0, 0));
			this.addFocusListener(new FocusAdapter() {

				@Override
				public void focusLost(FocusEvent e) {
					DoubleRenderer.this.validate();
				}
			});
		}

		@Override
		public Component getTableCellRendererComponent(JTable table, Object doubleVal, boolean isSelected,
				boolean hasFocus, int row, int column) {
			if (doubleVal == null) {
				return null;
			}
			String valString = "0.0";
			if (doubleVal.equals(Double.NaN)) {
				valString = "NaN";
			} else {
				Double val = (Double) doubleVal;
				String pattern;
				if ((Math.abs(val) >= 10000 || Math.abs(val) <= 0.001) && val !=0) {
					pattern = "0.00E0";
				} else {
					pattern = "0.00#";
				}

				DecimalFormat myFormatter = new DecimalFormat(pattern);
				valString = myFormatter.format(val);
			}
			this.setText(valString);

			return this;
		}
	}


	@Override
	public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
		Component c = super.prepareRenderer(renderer, row, column);
		Color bgColor = Color.WHITE;
		if (!isCellEditable(row, column)) {
			bgColor = new Color(235, 235, 235);
		}
		if (component.isEstimated()) {
			bgColor = new Color(211, 211, 255);
		}
		c.setBackground(bgColor);
		if (!isEnabled()) {
			c.setBackground(Color.LIGHT_GRAY);
		}
		return c;
	}


	@Override
	public TableCellEditor getCellEditor(int row, int column) {
		TableCellEditor editor = super.getCellEditor(row, column);
		if (column == 1 && component.isChild()) {
			FitConstraintType[] typesArray = component.getParameter(row).getParameterType().getAvailableConstraints();
			Arrays.sort(typesArray);
			JComboBox<FitConstraintType> types = new JComboBox<>(typesArray);
			types.setRenderer(new DefaultListCellRenderer() {
				@Override
				public Component getListCellRendererComponent(JList<?> list, Object value,
						int index, boolean isSelected, boolean cellHasFocus) {
					String label = ((FitConstraintType) value).getLabel();
					return super.getListCellRendererComponent(list, label, index, isSelected,
							cellHasFocus);
				}
			});
			editor = new DefaultCellEditor(types);
		} else if (editor instanceof DefaultCellEditor
				&& ((DefaultCellEditor) editor).getComponent() instanceof JTextField) {
			((DefaultCellEditor) editor).setClickCountToStart(1);
		}
		return editor;
	}

	/**
	 * Initializes the renderer used for the constraints
	 */
	private void initConstraintsRenderer() {
		getColumnModel().getColumn(1).setCellRenderer(new DefaultTableCellRenderer() {
			@Override
			public void setValue(Object value) {
				setText(((FitConstraintType) value).getLabel());
			}
		});
	}

	/**
	 * Initializes the renderer used for the name of the parameters
	 */
	private void initNameRenderer() {
		getColumnModel().getColumn(0).setCellRenderer(new DefaultTableCellRenderer() {
			@Override
			public Component getTableCellRendererComponent(JTable table, Object value,
					boolean isSelected, boolean hasFocus, int row, int column) {
				Component superR = super.getTableCellRendererComponent(table, value, isSelected,
						hasFocus, row, column);
				superR.setFont(superR.getFont().deriveFont(Font.BOLD));
				return superR;
			}
		});
	}

	/**
	 * Initializes the width of the columns
	 */
	private void initTableHeadersWidth() {
		int width;
		Component comp;
		for (int column = 0; column < getColumnCount(); column++) {
			comp = getTableHeader().getDefaultRenderer().getTableCellRendererComponent(this,
					getColumnModel().getColumn(column).getHeaderValue(), false, false, 0, 0);
			width = comp.getPreferredSize().width;
			for (int row = 0; row < getRowCount(); row++) {
				TableCellRenderer renderer = getCellRenderer(row, column);
				comp = prepareRenderer(renderer, row, column);
				width = Math.max(comp.getPreferredSize().width, width)
						+ getIntercellSpacing().width;
			}
			getColumnModel().getColumn(column).setPreferredWidth(width);
		}
	}

	/**
	 * Initializes the selection listener
	 */
	private void initSelectionListener() {
		getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		getSelectionModel().addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				editCellAt(getSelectedRow(), getSelectedColumn());
			}
		});
	}

	 public static void main(String[] args) {
		 JFrame frame = new JFrame("");
		 ComponentTable componentTable = new ComponentTable(new FitGaussianComponent());
		 componentTable.setAutoCreateColumnsFromModel(false);
		 componentTable.setPreferredScrollableViewportSize(componentTable.getPreferredSize());
		 componentTable.setFillsViewportHeight(true);

		 JScrollPane scrollPane = new JScrollPane(componentTable);
		 scrollPane.setBorder(null);
		 scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		 scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);

		 frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
		 frame.pack();
		 frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 frame.setVisible(true);
	 }

}
