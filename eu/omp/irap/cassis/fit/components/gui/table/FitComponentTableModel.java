/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit.components.gui.table;

import javax.swing.table.DefaultTableModel;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.fit.components.FitComponent;
import eu.omp.irap.cassis.fit.components.FitConstraint;
import eu.omp.irap.cassis.fit.components.FitConstraint.FitConstraintType;
import eu.omp.irap.cassis.fit.components.FitParameter;

/**
 * @author bastienkovac
 *
 * This class is an abstract representation of the TableModel that
 * is used to manipulate any component's parameters.
 *
 */
@SuppressWarnings("serial")
public abstract class FitComponentTableModel extends DefaultTableModel implements ModelListener {

	protected boolean[][] editable;
	protected FitComponent componentModel;

	private String[] currentColumns;


	/**
	 * Constructor
	 *
	 * @param model The model of which the parameters must be displayed
	 */
	public FitComponentTableModel(FitComponent model) {
		super();
		this.componentModel = model;
		this.componentModel.addModelListener(this);
		this.editable = new boolean[componentModel.getNbParameters()][];
		initEditable();
		currentColumns = getModelColumns();
	}

	@Override
	public int getRowCount() {
		return componentModel == null ? 0 : componentModel.getNbParameters();
	}

	@Override
	public int getColumnCount() {
		return currentColumns.length;
	}

	@Override
	public String getColumnName(int column) {
		return currentColumns[column];
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return this.editable[row][column];
	}

	@Override
	public void dataChanged(ModelChangedEvent e) {
		if (e.getSource().equals(FitComponent.PARAMETER_CONSTRAINT_EVENT)) {
			FitParameter parameter = (FitParameter) e.getValue();
			this.editable[parameter.getIndex()][2] = !isEqualityConstraint(parameter.getConstraint())
					&& !isNoneConstraint(parameter.getConstraint());
			this.editable[parameter.getIndex()][3] = isNoneConstraint(parameter.getConstraint());
		} else if (e.getSource().equals(FitComponent.COMPONENT_NOW_FIRST_CHILD_EVENT)) {
			for (int i = 0 ; i < componentModel.getNbParameters() ; i++) {
				this.editable[i][1] = true;
			}
		}
		fireTableDataChanged();
	}

	/**
	 * Checks if a given constraint is of type None
	 *
	 * @param constraint The type to check
	 * @return True if the constraint is None
	 */
	protected boolean isNoneConstraint(FitConstraint constraint) {
		return constraint.getConstraintType().equals(FitConstraintType.NONE);
	}

	/**
	 * Checks if a given constraint is of type Equal
	 *
	 * @param constraint The type to check
	 * @return True if the constraint is Equal
	 */
	protected boolean isEqualityConstraint(FitConstraint constraint) {
		return constraint.getConstraintType().equals(FitConstraintType.EQUAL);
	}

	@Override
	public abstract void setValueAt(Object aValue, int row, int column);

	@Override
	public abstract Object getValueAt(int row, int column);

	@Override
	public abstract Class<?> getColumnClass(int columnIndex);

	/**
	 * @return An array of String representing the headers of all the columns in
	 * the model
	 */
	protected abstract String[] getModelColumns();

	/**
	 * This method is called in the constructor to initialize the editable array.
	 * This array is of dimension [nbLines][nbColumns], and determines for each cell
	 * if it is editable or not
	 */
	protected abstract void initEditable();

}
