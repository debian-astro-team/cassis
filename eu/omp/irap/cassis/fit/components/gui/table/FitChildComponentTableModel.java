/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit.components.gui.table;

import eu.omp.irap.cassis.fit.components.FitComponent;
import eu.omp.irap.cassis.fit.components.FitConstraint.FitConstraintType;
import eu.omp.irap.cassis.fit.components.FitMultiComponent;
import eu.omp.irap.cassis.fit.components.FitParameter;

/**
 * @author bastienkovac
 *
 * This class is a Table Model used to display every {@link FitComponent} that is
 * a child of a {@link FitMultiComponent}
 *
 */
@SuppressWarnings("serial")
public class FitChildComponentTableModel extends FitComponentTableModel {


	/**
	 * Constructor
	 *
	 * @param model The model to display
	 */
	public FitChildComponentTableModel(FitComponent model) {
		super(model);
	}

	@Override
	protected String[] getModelColumns() {
		return new String[] { "Name", "Constraint", "Factor", "Value", "Error" };
	}

	@Override
	public void setValueAt(Object aValue, int row, int column) {
		switch(column) {
		case 1:
			if (!aValue.equals(componentModel.getParameter(row).getConstraint().getConstraintType()))
				componentModel.setParameterConstraint(row, (FitConstraintType) aValue);
			break;
		case 2:
			componentModel.setParameterFactor(row, (Double) aValue);
			break;
		case 3:
			componentModel.setParameterValue(row, (Double) aValue);
			break;
		default:
			break;
		}
		fireTableCellUpdated(row, column);
	}

	@Override
	public Object getValueAt(int row, int column) {
		switch(column) {
		case 0:
			return componentModel.getParameter(row).getName();
		case 1:
			return componentModel.getParameter(row).getConstraint().getConstraintType();
		case 2:
			return componentModel.getParameter(row).getConstraint().getFactor();
		case 3:
			return componentModel.getParameter(row).getValue();
		case 4:
			return componentModel.getParameter(row).getError();
		default:
			return null;
		}
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch(columnIndex) {
		case 0:
			return String.class;
		case 1:
			return FitConstraintType.class;
		case 2:
			return Double.class;
		case 3:
			return Double.class;
		case 4:
			return Double.class;
		default:
			return null;
		}
	}

	@Override
	protected void initEditable() {
		for (int i = 0 ; i < getRowCount() ; i++) {
			FitParameter parameter = componentModel.getParameter(i);
			boolean factorEditable = !isEqualityConstraint(parameter.getConstraint()) && !isNoneConstraint(parameter.getConstraint());
			this.editable[i] = new boolean[]{
					false, componentModel.isFirstChild(), factorEditable, !parameter.isConstrained(), false };
		}
	}

}
