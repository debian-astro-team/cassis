/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit.components;

import eu.omp.irap.cassis.fit.components.FitConstraint.FitConstraintType;
import eu.omp.irap.cassis.fit.computing.FitNormalizationParameters;
import eu.omp.irap.cassis.fit.util.enums.FitParameterType;

/**
 * @author bastienkovac
 *
 * This class represents a parameter of a fitting component
 *
 */
public class FitParameter {

	private final String name;
	private final FitParameterType parameterType;

	private double value; // Guess value, then actual computed one
	private boolean fixed;
	private double error; // Computed error

	private double lowLimit = 0., highLimit = 0.;

	private FitConstraint constraint;


	/**
	 * Constructor
	 *
	 * @param parameterType The type of the parameter
	 */
	public FitParameter(FitParameterType parameterType) {
		this.parameterType = parameterType;
		this.name = parameterType.getNameParameter();
		this.value = parameterType.getDefaultValue();
		this.constraint = FitConstraint.getFitConstraint(FitConstraintType.NONE);
	}

	/**
	 * @return The index corresponding to this parameter's type
	 */
	public int getIndex() {
		return parameterType.getIndex();
	}

	/**
	 * @return This parameter's type
	 */
	public FitParameterType getParameterType() {
		return parameterType;
	}

	/**
	 * @return The name of the parameter
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return The current value of the parameter
	 */
	public double getValue() {
		return value;
	}

	/**
	 * @param value The new value of the parameter
	 */
	public void setValue(double value) {
		this.value = value;
	}

	/**
	 * @return True is the parameter is fixed
	 */
	public boolean isFixed() {
		return fixed;
	}

	/**
	 * @param fixed If the parameter is fixed
	 */
	public void setFixed(boolean fixed) {
		this.fixed = fixed;
	}

	/**
	 * @return The error for this parameter
	 */
	public double getError() {
		return error;
	}

	/**
	 * @param error The new error for this parameter
	 */
	public void setError(double error) {
		this.error = error;
	}

	/**
	 * @return The low limit of the parameter
	 */
	public double getLowLimit() {
		return lowLimit;
	}

	/**
	 * @param low The new low limit of the parameter
	 */
	public void setLowLimit(double low) {
		this.lowLimit = low;
	}

	/**
	 * @return The high limit of the parameter
	 */
	public double getHighLimit() {
		return highLimit;
	}


	/**
	 * @param high The new high limit of the parameter
	 */
	public void setHighLimit(double high) {
		this.highLimit = high;
	}

	/**
	 * @return The constraint that is currently applied on the parameter
	 */
	public FitConstraint getConstraint() {
		return constraint;
	}

	/**
	 * Set a constraint on this parameter that is of the given type
	 *
	 * @param type The constraint type
	 */
	public void setConstraint(FitConstraintType type) {
		this.constraint = FitConstraint.getFitConstraint(type);
	}

	/**
	 * @return True if the constraint on this parameter is other than None
	 */
	public boolean isConstrained() {
		return !this.constraint.getConstraintType().equals(FitConstraintType.NONE);
	}

	/**
	 * Sets this parameter to be a copy of the one given
	 *
	 * @param source The parameter to copy
	 */
	public void copyParameter(FitParameter source) {
		if (!this.parameterType.equals(source.getParameterType())) {
			throw new IllegalArgumentException("The two parameters are of different types");
		}
		this.value = source.getValue();
		this.error = source.getError();
		this.fixed = source.isFixed();
		this.lowLimit = source.getLowLimit();
		this.highLimit = source.getHighLimit();
		this.constraint.copyConstraint(source.getConstraint());
	}

	/**
	 * Returns a denormalized value of the one given, depending on this parameter's type
	 *
	 * @param givenValue The value to denormalize
	 * @param nmParams The normalization parameters
	 * @return The denormalized value
	 */
	public double getDenormalizedValue(double givenValue, FitNormalizationParameters nmParams) {
		return parameterType.getDenormalizedValue(givenValue, nmParams);
	}

	/**
	 * Returns a denormalized value of the given error, depending on this parameter's type
	 *
	 * @param givenError The error to denormalize
	 * @param nmParams The normalization parameters
	 * @return The denormalized error
	 */
	public double getDenormalizedDeviation(double givenError, FitNormalizationParameters nmParams) {
		return parameterType.getDenormalizedDeviation(givenError, nmParams);
	}

	@Override
	public String toString() {
		return "[name=" + name + "\t fixed=" + fixed + "\t value=" + value + "\t error=" + error + "\t lowLimit=" + lowLimit + "\t highLimit=" + highLimit + "]";
	}

}
