/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit.components.impl;

import java.util.Arrays;
import java.util.Properties;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.fit.components.FitAbstractComponent;
import eu.omp.irap.cassis.fit.components.FitComponent;
import eu.omp.irap.cassis.fit.computing.FitEstimator;
import eu.omp.irap.cassis.fit.computing.FitNormalizationParameters;
import eu.omp.irap.cassis.fit.gui.util.FitGuiUtil;
import eu.omp.irap.cassis.fit.util.enums.FitStyle;
import eu.omp.irap.cassis.fit.util.enums.FitType;
import herschel.ia.numeric.Double1d;
import herschel.ia.numeric.toolbox.fit.AbstractModel;
import herschel.ia.numeric.toolbox.fit.PolynomialModel;

/**
 * @author bastienkovac
 *
 * The polynomial model follows the given equation :
 * <br>
 * <code>f( x:p )  =  ∑ pk * xk</code>
 * <br>
 * where the sum is over k running from 0 to degree (inclusive).
 * <br>
 * This model is an exception as it doesn't really have parameters, that is
 * why it overrides most of {@link FitComponent} methods, and has a distinct
 * display
 *
 */
public class FitPolynomialComponent extends FitComponent {

	public static final String POLY_FACTORS_EVENT = "polyFactorsEvent";

	private int degree;
	private double[] factors;
	private boolean verbose = false;

	/**
	 * Constructor
	 */
	public FitPolynomialComponent() {
		super(FitType.POLY);
	}

	/**
	 * Sets the degree of the polynomial
	 *
	 * @param degree The new degree
	 */
	public void setDegree(int degree) {
		this.degree = degree;
		this.factors = new double[degree + 1];
	}

	/**
	 * @return The degree of the polynomial
	 */
	public int getDegree() {
		return degree;
	}

	/**
	 * Returns the factor at the i-th index.
	 * The factors are in descending order of power
	 *
	 * @param i Index of the factor
	 * @return The factors for the (n - i)th degree (where n is the
	 * number of factors)
	 */
	public double getFactor(int i) {
		return factors[i];
	}

	@Override
	public String getEquation() {
		return getEquationPolyParam(factors);
	}

	/**
	 * return the String of the equation on the polynomial
	 *
	 * @param polyParam
	 * @return
	 */
	public static String getEquationPolyParam(double[] polyParam) {
		StringBuilder builder = new StringBuilder("<html>f(x) =");
		for (int i = polyParam.length - 1 ; i >= 1 ; i--) {
			if (i != polyParam.length && polyParam[i] < 0 ) {
				builder.append(" - ");
			} else if (i != polyParam.length - 1) {
				builder.append(" + ");
			} else {
				builder.append(' ');
			}
			builder.append(FitGuiUtil.formatDouble(Math.abs(polyParam[i])));
			builder.append("X");
			if (i > 1) {
				builder.append("<sup>");
				builder.append(i);
				builder.append("</sup>");
			}
		}
		if (polyParam[0] < 0) {
			builder.append(" - ");
		} else {
			builder.append(" + ");
		}
		builder.append(FitGuiUtil.formatDouble(Math.abs(polyParam[0])));
		builder.append("</html>");
		return builder.toString();
	}

	@Override
	public AbstractModel getHipeModel(FitNormalizationParameters nmParams, FitStyle style) {
		PolynomialModel model = new PolynomialModel(degree);
		double[] values = getNormalizedParameters(nmParams);
		model.setParameters(new Double1d(values));
		return model;
	}

	/**
	 * Returns an array of the normalized values of this component's parameters
	 *
	 * @param nmParams The normalization parameters
	 * @param style Fit style used
	 * @return An array of normalized values
	 */
	public double[] getNormalizedParameters(FitNormalizationParameters nmParams) {
		return getNormalizedParameters(nmParams, factors);

	}

	/**
	 * Returns an array of the normalized values of this component's parameters
	 *
	 * @param nmParams The normalization parameters
	 * @param factors An array of normalized values
	 * @return An array of denormalized values
	 */
	public static double[] getNormalizedParameters(FitNormalizationParameters nmParams, double[] factors) {
		return changeParametersNormalization(new FitNormalizationParameters(-nmParams.getOffsetX()*nmParams.getScaleX(),
				1.0/nmParams.getScaleX(), 0, 1), factors);

	}

	/**
	 * Returns an array of the de normalized values of this component's parameters
	 *
	 * @param nmParams The normalization parameters
	 * @param An array of normalized values
	 * @return An array of denormalized values
	 */
	public static double[] getDeNormalizedParameters(FitNormalizationParameters nmParams, double[] a_n) {
		return changeParametersNormalization(nmParams, a_n);

	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.cassis.fit.components.FitComponentModel#interpretResults(herschel.ia.numeric.toolbox.fit.AbstractModel, eu.omp.irap.cassis.fit.computing.FitNormalizationParameters)
	 *
	 * See PDF on Redmine for more precision (Wiki/Fitter une courbe/Coefficients Polynomial Fit)
	 *
	 */
	@Override
	public void interpretResults(AbstractModel resultModel, FitNormalizationParameters nmParams, boolean fitComputed) {
		double [] a_n = new double[degree + 1];
		for (int i = 0 ; i < a_n.length ; i++) {
			a_n[i] = resultModel.getFitParameters(i);
		}

		factors = getDeNormalizedParameters(nmParams, a_n);
		if (verbose) {
			System.out.println("factors =" + Arrays.toString(factors) + "a_n=" + Arrays.toString(a_n));
		}
		fireDataChanged(new ModelChangedEvent(POLY_FACTORS_EVENT));
	}

	/**
	 * Returns the coeff of polynome corrected by FitNormalizationParameters nmParams
	 *
	 * @param nmParams
	 * @param a_n
	 * @return
	 */
	public static double[] changeParametersNormalization(FitNormalizationParameters nmParams, double[] a_n) {
		double res[] = new double [a_n.length];
		for (int i = 0 ; i < a_n.length ; i++) {
			double sum = 0;
			for (int k = i ; k < a_n.length ; k++) {
				double tmpValue = a_n[k] * Math.pow(nmParams.getScaleX(), k) * getBinomialExpansion(k, i)* Math.pow(nmParams.getOffsetX(), k - i);
				sum += tmpValue;
			}
			res[i] = sum;
		}
		return res;
	}

	/**
	 * Returns the binomial expansion of n and k
	 *
	 * @param n
	 * @param k
	 * @return n! / k! * (n-k)!
	 */
	public static double getBinomialExpansion(int n, int k) {
		return getFactorial(n) / (double) (getFactorial(k) * getFactorial(n - k));
	}

	/**
	 * Naive implementation of the factorial, could lead
	 * to performance issues with big inputs
	 *
	 * @param n The input
	 * @return n!
	 */
	public static int getFactorial(int n) {
		int res = 1;
		for (int i = 1 ; i <= n ; i++) {
			res *= i;
		}
		return res;
	}

	@Override
	public void estimateComponent(FitEstimator estimator) {
		// No estimation
	}

	@Override
	public void copyComponent(FitAbstractComponent source) {
		if (source instanceof FitPolynomialComponent) {
			final FitPolynomialComponent fitPoly = (FitPolynomialComponent) source;
			setDegree(fitPoly.getDegree());
			this.factors = Arrays.copyOf(fitPoly.factors, fitPoly.factors.length);
//			this.a_n = Arrays.copyOf(fitPoly.a_n, fitPoly.a_n.length);
		}
	}

	@Override
	public void saveComponent(int index, Properties properties, String prefix) {
		String nameComponent = prefix + "simpleComponent" + index;
		properties.setProperty(nameComponent, String.valueOf(getComponentType()));
		properties.setProperty(nameComponent + "Degree", String.valueOf(degree));
	}

	@Override
	public int getNbParameters() {
		return 0;
	}

	@Override
	public void setAllParametersFixed(boolean fixed) {
		// Nothing to see here
	}

	@Override
	protected void loadFromProperties(Properties properties, int index, String prefix) {
		String nameComponent = prefix + "simpleComponent" + index;
		setDegree(Integer.parseInt(properties.getProperty(nameComponent + "Degree")));
	}

	@Override
	public boolean areAllParametersFixed() {
		return false;
	}

	@Override
	protected void buildParameters() {
		this.degree = 1;
		this.factors = new double[degree + 1];
	}

	public double[] getFactors() {
		return factors;
	}

	public void setFactor(double[] params) {
		this.factors = params;

	}


	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}
}
