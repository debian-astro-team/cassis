/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit.components.impl;

import eu.omp.irap.cassis.fit.components.FitComponent;
import eu.omp.irap.cassis.fit.components.FitParameter;
import eu.omp.irap.cassis.fit.computing.FitEstimator;
import eu.omp.irap.cassis.fit.computing.FitNormalizationParameters;
import eu.omp.irap.cassis.fit.util.enums.FitParameterType;
import eu.omp.irap.cassis.fit.util.enums.FitStyle;
import eu.omp.irap.cassis.fit.util.enums.FitType;
import herschel.ia.numeric.Double1d;
import herschel.ia.numeric.Int1d;
import herschel.ia.numeric.toolbox.fit.AbstractModel;
import herschel.ia.numeric.toolbox.fit.SineModel;

/**
 * @author bastienkovac
 *
 * This sinus model follows the given equation :
 * <br>
 * <code>f( x:p )  =  p1 * cos( 2 * π * p0 * x ) + p2 * sin( 2 * π * p0 * x )</code>
 * <br>
 *  where p0 = frequency, p1 = amplitude cosine and p2 = amplitude sine
 *
 */
public class FitSinusComponent extends FitComponent {


	/**
	 * Constructor
	 */
	public FitSinusComponent() {
		super(FitType.SIN);
	}

	@Override
	public AbstractModel getHipeModel(FitNormalizationParameters nmParams, FitStyle style) {
		SineModel model = new SineModel();
		if (!getParameter(0).isFixed()) {
			model.setMixedModel(new Int1d(new int[] { 1, 2 }));
		}
		double[] params = getNormalizedParameters(nmParams, style);
		model.setParameters(new Double1d(params));
		return model;
	}

	@Override
	protected void buildParameters() {
		addParameter(new FitParameter(FitParameterType.SIN_I0));
		addParameter(new FitParameter(FitParameterType.OMEGA));
		addParameter(new FitParameter(FitParameterType.PHI));
	}

	@Override
	public void estimateComponent(FitEstimator estimator) {
		setParameterValue(0, estimator.getEstimatedSinFreq());
		setParameterValue(1, estimator.getEstimatedI0());
		setParameterValue(2, estimator.getEstimatedI0());
	}

}
