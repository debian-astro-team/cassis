/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit.components;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.fit.components.FitConstraint.FitConstraintType;
import eu.omp.irap.cassis.fit.computing.FitEstimator;
import eu.omp.irap.cassis.fit.computing.FitNormalizationParameters;
import eu.omp.irap.cassis.fit.util.enums.FitParameterType;
import eu.omp.irap.cassis.fit.util.enums.FitStyle;
import eu.omp.irap.cassis.fit.util.enums.FitType;
import herschel.ia.numeric.Double1d;
import herschel.ia.numeric.Int1d;
import herschel.ia.numeric.toolbox.fit.AbstractModel;
import herschel.ia.numeric.toolbox.fit.ComboModel;

/**
 * @author bastienkovac
 *
 * This fitting component is multiple, which means it defines one parent component,
 * and any number of children component. (At least one).
 * <br>
 * The first child of this model defines the constraints that will be put on every child (including
 * itself).
 * <br>
 * The FitMultiComponentsModel works with the parent component being the base for all other one,
 * every parameters of the children components can be expressed as linked to the parent's corresponding one.
 * When a constraint is defined on a parameter by the first child, all parameters of every other child have the same
 * constraint type (but can have different factors).
 * <br>
 * For example, if we have the parent parameter X = 0.5, we could have x0 = 2*X, x1 = 0.5*X, x2 = 0.3*X etc..
 * <br>
 * This class both extends ListenerManager (so it can inform its view about its changes) and implements
 * ModelListener (so it can receive information about any change of state in the children components)
 *
 */
public class FitMultiComponent extends FitAbstractComponent implements ModelListener {

	private static final String PARENT_LABEL = "Parent";

	public static final String MODEL_CLEARED_EVENT = "multiModelClearEvent";
	public static final String COMPONENT_ADDED_EVENT = "componentAddedEvent";
	public static final String CHILD_REMOVED_EVENT = "childRemoved";
	public static final String CHILD_NAME_EVENT = "childNameEdited";
	public static final String MODEL_FOLDED_EVENT = "modelFoldedEvent";

	private FitComponent parent;
	private List<FitComponent> children;

	private Double1d multiModelParameters;

	private Runnable childEstimationAction;

	private boolean folded = false;



	/**
	 * Constructor with one parent and one child
	 *
	 * @param initialType The type of the "parent" and thus of the
	 * subsequent children
	 */
	public FitMultiComponent(FitType initialType) {
		super(initialType);
		this.children = new ArrayList<>();
		this.parent = getComponentType().buildIndividualComponent();
		this.parent.setName(PARENT_LABEL);
		addChild();
		setLinks();
	}

	/**
	 * Constructor with one parent and an initial number of children
	 *
	 * @param initialType The type of the "parent" and thus of the
	 * subsequent children
	 * @param initialNumberOfChildren The initial number of children
	 */
	public FitMultiComponent(FitType initialType, int initialNumberOfChildren) {
		this(initialType);
		while (this.children.size() < initialNumberOfChildren) {
			addChild();
		}
	}

	/**
	 * Constructor with given parent and initial children
	 *
	 * @param initialParent The initial parent
	 * @param initialChildren The initial children
	 */
	public FitMultiComponent(FitComponent initialParent, List<FitComponent> initialChildren) {
		super(initialParent.getComponentType());
		this.parent = initialParent;
		this.parent.setName(PARENT_LABEL);
		this.children = new ArrayList<>();
		for (FitComponent c : initialChildren) {
			addChild(c);
		}
		setLinks();
	}

	private void setLinks() {
		parent.setFollowingInterface(getParentLink());
		if (!this.children.isEmpty()) {
			this.children.get(0).setFollowingInterface(getFirstChildLink());
		}
	}

	private IFollow<FitComponent> getParentLink() {
		return new IFollow<FitComponent>() {
			@Override
			public List<FitComponent> getSiblings() {
				return Collections.unmodifiableList(children);
			}
		};
	}

	private IFollow<FitComponent> getFirstChildLink() {
		return new IFollow<FitComponent>() {
			@Override
			public List<FitComponent> getSiblings() {
				return Collections.unmodifiableList(children.subList(1, children.size()));
			}
		};
	}

	/**
	 * Dispatches the given event to every individual component
	 * in the multi-model (that is parent and children)
	 *
	 * @param e The event to dispatch
	 */
	public void dispatchEvent(ModelChangedEvent e) {
		this.parent.fireDataChanged(e);
		for (FitComponent child : children) {
			child.fireDataChanged(e);
		}
	}

	/**
	 * Returns the i-th child in the model
	 *
	 * @param i Index of the child
	 * @return The i-th child
	 */
	public FitComponent getChild(int i) {
		return children.get(i);
	}

	/**
	 * @return The number of children in this model
	 */
	public int getNbChildren() {
		return children.size();
	}

	/**
	 * Adds a child to the model (built from the parent's initial type)
	 */
	public void addChild() {
		FitComponent child = getComponentType().buildIndividualComponent();
		addChild(child);
	}

	/**
	 * Adds a child to the model, if its type matching the parent's initial type
	 *
	 * @param child Child to add
	 */
	public void addChild(FitComponent child) {
		if (child.getComponentType().equals(getComponentType())) {
			child.setName("Child " + (children.size() + 1));
			child.setChild(true);
			child.addModelListener(this);
			for (int i = 0 ; getNbChildren() > 0 && i < parent.getNbParameters() ; i++) {
				child.getParameter(i).setConstraint(getChild(0).getParameter(i).getConstraint().getConstraintType());
			}
			children.add(child);
			fireDataChanged(new ModelChangedEvent(COMPONENT_ADDED_EVENT, child));
		}
	}

	/**
	 * @return The first child, which is the one defining the constraints for
	 * every child (including itself)
	 */
	public FitComponent getFirstChild() {
		return this.children.get(0);
	}

	/**
	 * @return The parent component
	 */
	public FitComponent getParent() {
		return parent;
	}

	/**
	 * Selects the given component as the one that will be
	 * estimated. Switches the value of the estimated boolean
	 * for the given component
	 *
	 * @param sibling The component to estimate
	 */
	public void setEstimableSibling(FitComponent sibling) {
		if (sibling != null) {
			setEstimableSibling(sibling, !sibling.isEstimated());
		} else {
			setEstimableSibling(null, false);
		}
	}

	/**
	 * Selects the given component as the one that will be
	 * estimated.
	 *
	 * @param sibling The component to estimate
	 * @param estimated The value of estimated
	 */
	public void setEstimableSibling(FitComponent sibling, boolean estimated) {
		String estimableSiblingId = sibling == null ? null : sibling.getId();
		if (parent.getId().equals(estimableSiblingId)) {
			parent.setEstimated(estimated);
		} else {
			parent.setEstimated(false);
		}
		for (FitComponent child : children) {
			if (child.getId().equals(estimableSiblingId)) {
				child.setEstimated(estimated);
			} else {
				child.setEstimated(false);
			}
		}
	}

	/**
	 * @return The currently estimated sibling, or null if none is estimated
	 */
	public FitComponent getEstimatedSibling() {
		if (parent.isEstimated()) {
			return parent;
		}
		for (FitComponent child : children) {
			if (child.isEstimated()) {
				return child;
			}
		}
		return null;
	}

	/**
	 * @return A unmodifiable list of every child that is not the first one
	 */
	public List<FitComponent> getAdditionalChildren() {
		return Collections.unmodifiableList(this.children.subList(1, this.children.size()));
	}

	/**
	 * Removes the child that is identified by the given ID
	 *
	 * @param childId The id of the child
	 */
	public void removeChild(String childId) {
		FitComponent child = findChild(childId);
		children.remove(child);
		if (child.isFirstChild()) {
			IFollow<FitComponent> firstChildLink = new IFollow<FitComponent>() {
				@Override
				public List<FitComponent> getSiblings() {
					return Collections.unmodifiableList(children.subList(1, children.size()));
				}
			};
			this.children.get(0).setFollowingInterface(firstChildLink);
		}
		fireDataChanged(new ModelChangedEvent(CHILD_REMOVED_EVENT, childId));
		updateTitles();
	}

	/**
	 * Sets the next element from the current one as estimable.
	 * If there is no such element, returns false
	 *
	 * @return True if there was a next element to set to estimable
	 */
	public boolean setNextEstimable() {
		if (parent.isEstimated()) {
			setEstimableSibling(getChild(0));
			return true;
		} else {
			for (int i = 0 ; i < children.size() ; i++) {
				if (setNextChild(i)) {
					return true;
				}
			}
		}
		setEstimableSibling(null);
		return false;
	}

	private boolean setNextChild(int currentIndex) {
		if (children.get(currentIndex).isEstimated() && currentIndex + 1 < children.size()) {
			setEstimableSibling(getChild(currentIndex + 1));
			return true;
		}
		return false;
	}

	/**
	 * Sets an action to execute every time a sibling of this multi-component
	 * is set to be estimated
	 *
	 * @param childEstimationAction The action to execute as a Runnable
	 */
	public void setChildEstimationAction(Runnable childEstimationAction) {
		this.childEstimationAction = childEstimationAction;
	}

	/**
	 * @return The action to execute every time of sibling of this multi-component
	 * is set to be estimated, as a Runnable
	 */
	public Runnable getChildEstimationAction() {
		return childEstimationAction;
	}

	/**
	 * Sets the folded value, which indicates if the children must be hidden or not
	 *
	 * @param folded The new folded value
	 */
	public void setFolded(boolean folded) {
		this.folded = folded;
		fireDataChanged(new ModelChangedEvent(MODEL_FOLDED_EVENT));
	}

	/**
	 * @return True if the children must be hidden
	 */
	public boolean isFolded() {
		return folded;
	}

	@Override
	public void estimateComponent(FitEstimator estimator) {
		if (parent.isEstimated()) {
			parent.estimateComponent(estimator);
		} else {
			for (FitComponent child : children) {
				if (estimateChild(child, estimator)) {
					break;
				}
			}
		}
	}

	private boolean estimateChild(FitComponent child, FitEstimator estimator) {
		if (child.isEstimated()) {
			child.estimateComponent(estimator);
			double[] params = new double[child.getNbParameters()];
			// Values will change as factors are computed and applied, so we copy
			// the estimated value of the child
			for (int i = 0 ; i < child.getNbParameters() ; i++) {
				params[i] = child.getParameter(i).getValue();
			}
			for (int i = 0 ; i < child.getNbParameters() ; i++) {
				if (child.getParameter(i).isConstrained()) {
					double parentValue = parent.getParameter(i).getValue();
					double childValue = params[i];
					double factor = child.getParameter(i).getConstraint().computeFactor(parentValue, childValue);
					child.setParameterFactor(i, factor);
				}
			}
			return true;
		}
		return false;
	}

	@Override
	public void saveComponent(int index, Properties properties, String prefix) {
		String nameComponent = prefix + "multiComponent" + index;
		properties.setProperty(nameComponent, String.valueOf(getComponentType()));
		properties.setProperty("component" + index + "Selected", String.valueOf(isSelected()));
		properties.setProperty(nameComponent + "nbChildren", String.valueOf(children.size()));

		parent.saveComponent(index, properties, nameComponent + PARENT_LABEL);
		for (int i = 0 ; i < children.size() ; i++) {
			children.get(i).saveComponent(i, properties, nameComponent + "Child");
		}
	}

	@Override
	protected void loadFromProperties(Properties properties, int index, String prefix) {
		String nameComponent = prefix + "multiComponent" + index;
		parent.loadFromProperties(properties, index, nameComponent + PARENT_LABEL);
		for (int i = 0 ; i < children.size() ; i++) {
			children.get(i).loadFromProperties(properties, i, nameComponent + "Child");
		}
	}

	@Override
	public void copyComponent(FitAbstractComponent source) {
		if (getComponentType().equals(source.getComponentType()) && source instanceof FitMultiComponent) {
			FitMultiComponent multiSource = (FitMultiComponent) source;
			this.parent.copyComponent(multiSource.getParent());
			for (int i = 0 ; i < multiSource.getNbChildren() ; i++) {
				this.children.get(i).copyComponent(multiSource.getChild(i));
			}
			this.folded = ((FitMultiComponent) source).isFolded();
		}
	}

	@Override
	public void setSelected(boolean selected) {
		super.setSelected(selected);
		parent.setSelected(selected);
		for (FitComponent child : children) {
			child.setSelected(selected);
		}
	}

	@Override
	public void setAllParametersFixed(boolean fixed) {
		parent.setAllParametersFixed(fixed);
		for (FitComponent child : children) {
			child.setAllParametersFixed(fixed);
		}
	}

	@Override
	public boolean areAllParametersFixed() {
		return parent.areAllParametersFixed();
	}

	@Override
	public int getNbParameters() {
		return parent.getNbParameters();
	}

	@Override
	public AbstractModel getHipeModel(FitNormalizationParameters nmParams, FitStyle style) {
		ComboModel comboModel = new ComboModel(getComponentType().buildAppropriateModel(), getNbChildren() + 1);
		FitComponent firstChild = getFirstChild();
		for (int i = 0 ; i < firstChild.getNbParameters() ; i++) {
			if (firstChild.getParameter(i).isConstrained()) {
				FitConstraint constraint = firstChild.getParameter(i).getConstraint();
				switch (constraint.getConstraintType()) {
				case EQUAL:
					comboModel.setMulCombo(i, getEqualityConstraints());
					break;
				case SCALE:
				case SCALE_AUTO:
					comboModel.setMulCombo(i, getScalingConstraints(i));
					break;
				case SHIFT:
					comboModel.setAddCombo(i, getShiftingConstraints(i, nmParams.getScaleX()));
					break;
				default:
					throw new IllegalArgumentException(constraint.getConstraintType().getLabel() + " not implemented");
				}
			}
		}
		computeParametersValues(nmParams, style);
		comboModel.setParameters(multiModelParameters);
		computeLimits(comboModel, style);
		return comboModel;
	}

	private void computeLimits(ComboModel model, FitStyle style) {
		Int1d fixedParams = new Int1d();
		for (int i = 0 ; i < getNbParameters() ; i++) {
			if (parent.getParameter(i).isFixed() && FitStyle.LEVENBERG.equals(style)) {
				fixedParams.append(i);
			}
		}
		computeChildrenLimits(fixedParams,model, style);
	}

	private void computeChildrenLimits(Int1d fixedParams, ComboModel model, FitStyle style) {
		for (int i = 0 ; i < children.size() ; i++) {
			FitComponent child = children.get(i);
			for (int k = 0 ; k < child.getNbParameters() ; k++) {
				computeParameterLimit(child, k, style, fixedParams);
			}
		}
		if (FitStyle.LEVENBERG.equals(style)) {
			model.keepFixed(fixedParams);
		}
	}

	private void computeParameterLimit(FitComponent child, int k, FitStyle style, Int1d fixedParams) {
		if (!child.getParameter(k).isConstrained()) {
			if (parent.getParameter(k).isFixed()  && FitStyle.LEVENBERG.equals(style)) {
				fixedParams.append(fixedParams.get(fixedParams.getSize() - 1) + 1);
			}
		}
	}

	@Override
	public void interpretResults(AbstractModel resultModel, FitNormalizationParameters nmParams, boolean fitComputed) {
		ComboModel combo = (ComboModel) resultModel;
		int startIndex = 0;

		Double1d values = extractArray(combo.getExpandedParameters(), parent.getNbParameters(), startIndex);
		parent.interpretValues(values, nmParams);
		for (int i = 0 ; i < getNbChildren() ; i++) {
			startIndex += parent.getNbParameters();
			values = extractArray(combo.getExpandedParameters(), parent.getNbParameters(), startIndex);
			getChild(i).interpretValues(values, nmParams);
		}
		if (!areAllParametersFixed() && fitComputed) {
			interpretDeviations(combo, nmParams);
		}
	}

	@Override
	public void dataChanged(ModelChangedEvent e) {
		boolean constraintChanged = e.getSource().equals(FitComponent.PARAMETER_CONSTRAINT_EVENT);
		if (constraintChanged) {
			FitParameter param = (FitParameter) e.getValue();
			if (param.getConstraint().getConstraintType().equals(FitConstraintType.SCALE_AUTO)) {
				for (FitComponent child : children) {
					double x0Child = child.getParameter(FitParameterType.X0.getIndex()).getValue();
					double x0Parent = parent.getParameter(FitParameterType.X0.getIndex()).getValue();
					child.setParameterFactor(param.getIndex(), param.getConstraint().computeFactor(x0Parent, x0Child));
				}
			}
		}
		boolean factorChanged = e.getSource().equals(FitComponent.PARAMETER_FACTOR_EVENT);
		if (constraintChanged || factorChanged) {
			for (FitComponent child : children) {
				updateChildFactor(child);
			}
		}
	}

	private void updateChildFactor(FitComponent child) {
		for (int i = 0 ; i < child.getNbParameters() ; i++) {
			if (child.getParameter(i).isConstrained()) {
				double parentValue = parent.getParameter(i).getValue();
				child.setParameterValue(i, child.getParameter(i).getConstraint().constrainValue(parentValue));
			}
		}
	}

	@Override
	public boolean isMultiple() {
		return true;
	}


	@Override
	public void removeAllListener() {
		super.removeAllListener();
		parent.removeAllListener();
		for (FitComponent child : children) {
			child.removeAllListener();
		}
	}

	@Override
	public void removeModelListener(ModelListener listener) {
		super.removeModelListener(listener);
		parent.removeModelListener(listener);
		for (FitComponent child : children) {
			child.removeModelListener(listener);
		}
	}

	@Override
	public String toString() {
		return getName() + ": " + getNbChildren() + " children";
	}

	private void interpretDeviations(ComboModel combo, FitNormalizationParameters nmParams) {
		int startIndex = 0;
		Double1d deviations = extractArray(combo.getExpandedStandardDeviations(), parent.getNbParameters(), startIndex);
		parent.interpretDeviations(deviations, nmParams);

		for (int i = 0 ; i < getNbChildren() ; i++) {
			startIndex += parent.getNbParameters();
			deviations = extractArray(combo.getExpandedStandardDeviations(), parent.getNbParameters(), startIndex);
			getChild(i).interpretDeviations(deviations, nmParams);
		}
	}

	private Double1d extractArray(Double1d expanded, int sizeExtract, int startIndex) {
		Double1d extract = new Double1d();
		for (int i = startIndex ; i < startIndex + sizeExtract ; i++) {
			extract.append(expanded.get(i));
		}
		return extract;
	}

	private void computeParametersValues(FitNormalizationParameters nmParams, FitStyle style) {
		multiModelParameters = new Double1d(parent.getNormalizedParameters(nmParams, style));
		for (int i = 0 ; i < getNbChildren() ; i++) {
			double[] values = getChild(i).getNormalizedParameters(nmParams, style);
			for (int j = 0 ; j < values.length ; j++) {
				if (!getChild(i).getParameter(j).isConstrained()) {
					multiModelParameters.append(values[j]);
				}
			}
		}
	}

	private Double1d getEqualityConstraints() {
		Double1d equality = new Double1d();
		equality.append(1.);
		for (int i = 0 ; i < getNbChildren() ; i++) {
			equality.append(1.);
		}
		return equality;
	}

	private Double1d getScalingConstraints(int index) {
		Double1d scaling = new Double1d();
		scaling.append(1);
		for (int i = 0 ; i < getNbChildren() ; i++) {
			scaling.append(getChild(i).getParameter(index).getConstraint().getFactor());
		}
		return scaling;
	}

	private Double1d getShiftingConstraints(int index, double scaleX) {
		Double1d shifting = new Double1d();
		shifting.append(0);
		for (int i = 0 ; i < getNbChildren() ; i++) {
			shifting.append(getChild(i).getParameter(index).getConstraint().getFactor() * scaleX);
		}
		return shifting;
	}

	private void updateTitles() {
		for (int i = 0 ; i < children.size() ; i++) {
			FitAbstractComponent component = children.get(i);
			String title = component.getName().replaceAll("Child [0-9]+", "Child " + String.valueOf(i + 1)); // Update index
			component.setName(title);
		}
	}

	private FitComponent findChild(String childId) {
		for (FitComponent child : children) {
			if (child.getId().equals(childId)) {
				return child;
			}
		}
		throw new IllegalArgumentException("No child with ID " + childId);
	}

	public List<FitComponent> getChildren() {
		return Collections.unmodifiableList(children);
	}

}
