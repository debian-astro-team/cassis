/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit.components;

import java.util.Properties;
import java.util.UUID;

import eu.omp.irap.cassis.common.events.ListenerManager;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.fit.FitComponentManager;
import eu.omp.irap.cassis.fit.computing.FitEstimator;
import eu.omp.irap.cassis.fit.computing.Modelisable;
import eu.omp.irap.cassis.fit.util.enums.FitType;

/**
 * @author bastienkovac
 *
 * This class is an abstract representation of a fitting component, and can be added
 * to the {@link FitComponentManager}.
 *
 */
public abstract class FitAbstractComponent extends ListenerManager implements Modelisable {

	private static final String MULTI_COMPONENT_LABEL = "multiComponent";
	private static final String SIMPLE_COMPONENT_LABEL = "simpleComponent";

	public static final String MODEL_SELECTED_EVENT = "modelSelectedEvent";
	public static final String MODEL_ESTIMATED_EVENT = "modelEstimatedEvent";

	public static final String MODEL_NAME_EVENT = "modelNameEvent";

	private final String id;

	private String name;
	private FitType type;

	private boolean selected;
	private boolean estimated;


	/**
	 * Constructor
	 *
	 * @param type The initial type of the component
	 */
	public FitAbstractComponent(FitType type) {
		this.id = UUID.randomUUID().toString();
		this.name = "";
		this.type = type;
		this.selected = true;
	}

	/**
	 * @param name The new name of the component
	 */
	public void setName(String name) {
		this.name = name;
		fireDataChanged(new ModelChangedEvent(MODEL_NAME_EVENT, name));
	}

	/**
	 * @return The name of the component
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return A unique ID identifying the component
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the component selected (which determines if it will be used for
	 * fitting or not)
	 *
	 * @param selected The new selected value
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
		fireDataChanged(new ModelChangedEvent(MODEL_SELECTED_EVENT));
	}

	/**
	 * @return True if the component must be used in the fit
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * @return The type of the component
	 */
	public FitType getComponentType() {
		return type;
	}

	/**
	 * Sets if the component is currently estimated by the user
	 *
	 * @param estimated
	 */
	public void setEstimated(boolean estimated) {
		this.estimated = estimated;
		fireDataChanged(new ModelChangedEvent(MODEL_ESTIMATED_EVENT, estimated));
	}

	/**
	 * @return True if the component is currently estimated
	 */
	public boolean isEstimated() {
		return estimated;
	}

	/**
	 * Build and returns a {@link FitAbstractComponent} that is a copy
	 * of the one given as source
	 *
	 * @param source The component to copy
	 * @return The copied version
	 */
	public static FitAbstractComponent cloneComponent(FitAbstractComponent source) {
		if (source == null) {
			return null;
		} else if (!source.isMultiple()) {
			FitComponent model = source.getComponentType().buildIndividualComponent();
			model.copyComponent(source);
			return model;
		} else {
			int size = ((FitMultiComponent) source).getNbChildren();
			FitMultiComponent multiModel = source.getComponentType().buildMultiComponent(size);
			multiModel.copyComponent(source);
			return multiModel;
		}
	}

	/**
	 * Builds and returns a {@link FitAbstractComponent} that is loaded
	 * from the given properties object
	 *
	 * @param properties The Properties object
	 * @param index The index of the loaded component
	 * @return A component matching the properties
	 */
	public static FitAbstractComponent loadComponent(Properties properties, int index) {
		FitAbstractComponent loadedModel = null;
		if (properties.containsKey(SIMPLE_COMPONENT_LABEL + index)) {
			FitType type = FitType.valueOf(properties.getProperty(SIMPLE_COMPONENT_LABEL + index));
			loadedModel = type.buildIndividualComponent();
			loadedModel.loadFromProperties(properties, index, "");
		}
		if (properties.containsKey(MULTI_COMPONENT_LABEL + index)) {
			FitType type = FitType.valueOf(properties.getProperty(MULTI_COMPONENT_LABEL + index));
			int nbChildren = Integer.parseInt(properties.getProperty(MULTI_COMPONENT_LABEL + index + "nbChildren"));
			loadedModel = type.buildMultiComponent(nbChildren);
			loadedModel.loadFromProperties(properties, index, "");
		}
		if (loadedModel != null) {
			return loadedModel;
		}
		throw new IllegalArgumentException("Can't load the given property");
	}

	/**
	 * Estimates the component's parameter from the given estimator
	 *
	 * @param estimator The estimator
	 */
	public abstract void estimateComponent(FitEstimator estimator);

	/**
	 * Modify the components so it becomes a copy of the given source
	 *
	 * @param source The component to copy
	 */
	public abstract void copyComponent(FitAbstractComponent source);

	/**
	 * Saves the component in the given properties object
	 *
	 * @param index The index of the component in the {@link FitComponentManager}
	 * @param properties The Properties object
	 * @param prefix The prefix to use for every property saved
	 */
	public abstract void saveComponent(int index, Properties properties, String prefix);

	/**
	 * @return The total number of parameters in the component
	 */
	public abstract int getNbParameters();

	/**
	 * Sets all the parameters that can be fixed in the component to a new
	 * fixed value
	 *
	 * @param fixed The new fixed value
	 */
	public abstract void setAllParametersFixed(boolean fixed);

	/**
	 * @return True if every parameter that can be fixed is
	 */
	public abstract boolean areAllParametersFixed();

	/**
	 * @return True if the component is multiple
	 */
	public abstract boolean isMultiple();

	/**
	 * Loads the component from the given properties object
	 *
	 * @param properties The Properties object
	 * @param index The current index in the {@link FitComponentManager}
	 * @param prefix The prefix to use for every property loaded
	 */
	protected abstract void loadFromProperties(Properties properties, int index, String prefix);

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FitAbstractComponent other = (FitAbstractComponent) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
