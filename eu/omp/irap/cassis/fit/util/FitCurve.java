/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author bastienkovac
 *
 * This class holds the data as two arrays (x and y), and
 * implement a few utility methods.
 *
 * The arrays are copies of the ones given
 *
 */
public class FitCurve {

	private String name;
	private double[] x, y;

	private double minX, maxX, maxY;
	private int maxYIndex;

	private double vlsr, freqRef; // For integration in CASSIS


	/**
	 * Constructor
	 *
	 * /!\ The length of the data isn't checked, which could lead
	 * to errors when iterating over the two
	 *
	 * @param x The x data
	 * @param y The y data
	 */
	public FitCurve(double[] x, double[] y) {
		this("None", x, y);
	}

	/**
	 * Constructor
	 * <br>
	 * /!\ The length of the data isn't checked, which could lead
	 * to errors when iterating over the two if their dimension are
	 * different
	 *
	 * @param name The name of the curve
	 * @param x The x data
	 * @param y The y data
	 */
	public FitCurve(String name, double[] x, double[] y) {
		this.name = name;
		this.x = x;
		this.y = y;
		computeMinMax();
	}

	/**
	 * Constructor, copy the data source between the given range
	 *
	 * @param source Data source
	 * @param xMin minimum x value
	 * @param xMax maximum x value
	 */
	public FitCurve(FitCurve source, double xMin, double xMax) {
		this.name = source.getName();
		double[] x = source.getX(), y = source.getY();
		List<Double> xList = new ArrayList<>(), yList = new ArrayList<>();
		for (int i = 0 ; i < x.length ; i++) {
			if (x[i] >= xMin && x[i] <= xMax) {
				xList.add(x[i]);
				yList.add(y[i]);
			}
		}
		this.x = new double[xList.size()];
		this.y = new double[yList.size()];
		for (int i = 0 ; i < this.x.length ; i++) {
			this.x[i] = xList.get(i);
			this.y[i] = yList.get(i);
		}
		computeMinMax();
	}

	/**
	 * @param name The name of the curve
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return The name of the curve
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return A copy of the x-data held by the curve
	 */
	public double[] getX() {
		final double[] array = x;
		return Arrays.copyOf(array, array.length);
	}

	/**
	 * Replaces the current x array by another one
	 *
	 * @param x The new array
	 */
	public void setX(double[] x) {
		this.x = x;
	}

	/**
	 * Replaces the current y array by another one
	 *
	 * @param y The new array
	 */
	public void setY(double[] y) {
		this.y = y;
	}

	/**
	 * @return A copy of the y-data held by the curve
	 */
	public double[] getY() {
		final double[] array = y;
		return Arrays.copyOf(array, array.length);
	}

	/**
	 * @return The minimal x-value in the data
	 */
	public double getMinX() {
		return minX;
	}

	/**
	 * @return The maximal x-value in the data
	 */
	public double getMaxX() {
		return maxX;
	}


	/**
	 * @return The maximal y-value in the data
	 */
	public double getMaxY() {
		return maxY;
	}

	/**
	 * @return The index of the maximal y-value in the data
	 */
	public int getMaxYIndex() {
		return maxYIndex;
	}

	/**
	 * @return the vlsr
	 */
	public double getVlsr() {
		return vlsr;
	}

	/**
	 * @param vlsr the vlsr to set
	 */
	public void setVlsr(double vlsr) {
		this.vlsr = vlsr;
	}

	/**
	 * @return the freqRef
	 */
	public double getFreqRef() {
		return freqRef;
	}

	/**
	 * @param freqRef the freqRef to set
	 */
	public void setFreqRef(double freqRef) {
		this.freqRef = freqRef;
	}

	/**
	 * This method merges a list of FitCurve into only one.
	 * Overlapping values are added only once
	 *
	 * @param ranges The curve to merge
	 * @return A merged curve
	 */
	public static FitCurve mergeRanges(List<FitCurve> ranges) {
		List<FitCurve> disjointCurves = disjointOverlappingRanges(ranges);
		Collections.sort(disjointCurves, getComparator());
		FitCurve mergeCurve = disjointCurves.get(0);
		FitCurve finalResult = new FitCurve(mergeCurve.getName(), mergeCurve.getX(), mergeCurve.getY());
		for (int i = 1 ; i < disjointCurves.size() ; i++) {
			finalResult.addToTheEnd(disjointCurves.get(i));
		}
		return finalResult;
	}

	/**
	 * Takes a list of curves as parameters and return a list of curves where
	 * every overlapping value in the input was merged
	 *
	 * @param ranges The input
	 * @return The disjoint curves
	 */
	private static List<FitCurve> disjointOverlappingRanges(final List<FitCurve> ranges) {
		List<FitCurve> disjoint = new ArrayList<>();
		List<FitCurve> rangesCopy = new ArrayList<>(ranges);
		Collections.sort(rangesCopy, getComparator());
		FitCurve merge = null, rangeNext = null;
		boolean changed = false;
		if (rangesCopy.size() < 2) {
			return rangesCopy;
		}
		FitCurve current = rangesCopy.get(0);

		// Check every other for overlapping
		for (int i = 1 ; i < rangesCopy.size() ; i++) {
			rangeNext = rangesCopy.get(i);

			if (isOverlap(current, rangeNext)) {
				merge = makeMerge(current, rangeNext);
				changed = true;
				break;
			}
		}

		if (!changed) {
			disjoint.add(rangesCopy.get(0));
			rangesCopy.remove(0);
			disjoint.addAll(disjointOverlappingRanges(rangesCopy));
			return disjoint;
		} else {
			rangesCopy.remove(0);
			rangesCopy.remove(rangeNext);
			rangesCopy.add(merge);
			return disjointOverlappingRanges(rangesCopy);
		}
	}

	private static Comparator<FitCurve> getComparator() {
		return new Comparator<FitCurve>() {

			@Override
			public int compare(FitCurve o1, FitCurve o2) {
				double delta = o1.getMinX() - o2.getMinX();
				if (Double.doubleToRawLongBits(delta) == 0 && o1.getName().equals(o2.getName())) {
					return 0;
				} else if (delta < 0) {
					return -1;
				} else {
					return 1;
				}
			}

		};
	}

	private static boolean isInside(double xValue, FitCurve curve) {
		return xValue >= curve.getMinX() && xValue <= curve.getMaxX();
	}

	private static boolean isOverlap(FitCurve current, FitCurve next) {
		return isInside(next.getMinX(), current) || isInside(next.getMaxX(), current);
	}

	private void addToTheEnd(FitCurve other) {
		final double[] otherX = other.getX();
		final double[] otherY = other.getY();

		double[] newX = Arrays.copyOf(x, x.length + otherX.length);
		double[] newY = Arrays.copyOf(y, y.length + otherY.length);

		for (int i = 0 ; i < otherX.length ; i++) {
			newX[i + x.length] = otherX[i];
			newY[i + y.length] = otherY[i];
		}

		this.x = newX;
		this.y = newY;
		computeMinMax();
	}

	private static FitCurve makeMerge(FitCurve rangeOne, FitCurve rangeTwo) {
		FitCurve one = rangeOne;
		FitCurve two = rangeTwo;
		List<Double> xValues = new ArrayList<>(), yValues = new ArrayList<>();
		double[] xOne = one.getX(), xTwo = two.getX();
		double[] yOne = one.getY(), yTwo = two.getY();

		for (int i = 0 ; i < xOne.length + xTwo.length ; i++) {
			if (i < xOne.length) {
				xValues.add(xOne[i]);
				yValues.add(yOne[i]);
			} else {
				double value = xTwo[i - xOne.length];
				if (value <= one.getMaxX()) {
					continue;
				} else {
					xValues.add(xTwo[i - xOne.length]);
					yValues.add(yTwo[i - xOne.length]);
				}
			}
		}
		return createCurve(xValues, yValues);
	}

	private static FitCurve createCurve(List<Double> xValues, List<Double> yValues) {
		double[] xMerges = new double[xValues.size()], yMerges = new double[xValues.size()];
		for (int i = 0 ; i < xValues.size() ; i++) {
			xMerges[i] = xValues.get(i);
			yMerges[i] = yValues.get(i);
		}
		return new FitCurve(xMerges, yMerges);
	}

	private void computeMinMax() {
		minX = Double.MAX_VALUE;
		maxX = - Double.MAX_VALUE;
		maxY = - Double.MAX_VALUE;

		for (int i = 0 ; i < x.length ; i++) {
			if (x[i] > maxX) {
				maxX = x[i];
			}
			if (x[i] < minX) {
				minX = x[i];
			}
			if (y[i] > maxY) {
				maxY = y[i];
				maxYIndex = i;
			}
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(maxX);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(minX);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FitCurve other = (FitCurve) obj;
		if (Double.doubleToLongBits(maxX) != Double.doubleToLongBits(other.maxX))
			return false;
		if (Double.doubleToLongBits(minX) != Double.doubleToLongBits(other.minX))
			return false;
		if (x.length != other.getX().length)
			return false;
		if (!name.equals(other.name))
			return false;
		return true;
	}

}
