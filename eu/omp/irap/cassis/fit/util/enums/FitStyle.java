/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit.util.enums;

import herschel.ia.numeric.Double1d;
import herschel.ia.numeric.toolbox.fit.AbstractModel;
import herschel.ia.numeric.toolbox.fit.AmoebaFitter;
import herschel.ia.numeric.toolbox.fit.IterativeFitter;
import herschel.ia.numeric.toolbox.fit.LevenbergMarquardtFitter;

/**
 * @author bastienkovac
 *
 * Enumeration of the different fitters the module can use
 *
 */
public enum FitStyle {

	AMOEBA("AMOEBA Fitter"), LEVENBERG("LEVENBERG-MARQUARDT Fitter");

	private String label;


	private FitStyle(String label) {
		this.label = label;
	}

	/**
	 * @return The label of the fitter
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Returns an {@link IterativeFitter} fitter corresponding to this {@link FitStyle}
	 *
	 * @param normalizedXData The x-data normalized
	 * @param model The model to use in the fit
	 * @return The correct fitter
	 */
	public IterativeFitter getIterativeFitter(Double1d normalizedXData, AbstractModel model) {
		switch (this) {
		case AMOEBA:
			return new AmoebaFitter(normalizedXData, model);
		case LEVENBERG:
			return new LevenbergMarquardtFitter(normalizedXData, model);
		default:
			throw new IllegalArgumentException(label + " not implemented");
		}
	}

}
