/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit.util.enums;

import eu.omp.irap.cassis.fit.components.FitComponent;
import eu.omp.irap.cassis.fit.components.FitMultiComponent;
import eu.omp.irap.cassis.fit.components.impl.FitGaussianComponent;
import eu.omp.irap.cassis.fit.components.impl.FitLorentzianComponent;
import eu.omp.irap.cassis.fit.components.impl.FitPolynomialComponent;
import eu.omp.irap.cassis.fit.components.impl.FitSincComponent;
import eu.omp.irap.cassis.fit.components.impl.FitSinusComponent;
import eu.omp.irap.cassis.fit.components.impl.FitVoigtComponent;
import herschel.ia.numeric.toolbox.fit.AbstractModel;
import herschel.ia.numeric.toolbox.fit.GaussModel;
import herschel.ia.numeric.toolbox.fit.LorentzModel;
import herschel.ia.numeric.toolbox.fit.SincModel;
import herschel.ia.numeric.toolbox.fit.SineModel;
import herschel.ia.numeric.toolbox.fit.VoigtModel;

/**
 * @author bastienkovac
 *
 * Enumeration of the different fitting components that can be used
 * in this module
 *
 */
public enum FitType {

	GAUSS("Gaussian"), LORENTZ("Lorentzian"),
	VOIGT("Voigt"), SINC("Sinc"),
	POLY("Polynomial"), SIN("Sinus");

	private String label;

	private FitType(String label) {
		this.label = label;
	}

	/**
	 * @return The name of the fitting component
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Returns a {@link FitComponent} that matches this
	 * fitting component type
	 *
	 * @return The correct implementation of {@link FitComponent}
	 */
	public FitComponent buildIndividualComponent() {
		switch (this) {
		case GAUSS:
			return new FitGaussianComponent();
		case LORENTZ:
			return new FitLorentzianComponent();
		case SINC:
			return new FitSincComponent();
		case POLY:
			return new FitPolynomialComponent();
		case VOIGT:
			return new FitVoigtComponent();
		case SIN:
			return new FitSinusComponent();
		default:
			throw new IllegalArgumentException(label + " not yet implemented");
		}
	}

	/**
	 * Returns an {@link AbstractModel} that matches this
	 * fitting component type
	 *
	 * @return The correct implementation of {@link AbstractModel}
	 */
	public AbstractModel buildAppropriateModel() {
		switch (this) {
		case GAUSS:
			return new GaussModel();
		case LORENTZ:
			return new LorentzModel();
		case SINC:
			return new SincModel();
		case VOIGT:
			return new VoigtModel();
		case SIN:
			return new SineModel();
		default:
			throw new IllegalArgumentException(label + " not yet implemented");
		}
	}

	/**
	 * Builds a {@link FitMultiComponent} corresponding to this
	 * fitting component type, and having the given number of initial children.
	 *
	 * @param defaultSize The number of initial children
	 * @return The correct implementation of {@link FitMultiComponent}
	 */
	public FitMultiComponent buildMultiComponent(int defaultSize) {
		return new FitMultiComponent(this, defaultSize);
	}

	/**
	 * @return The components type that are Baseline
	 */
	public static FitType[] getBaselineComponents() {
		return new FitType[] { POLY, SIN };
	}

	/**
	 * @return  The components type that are Line
	 */
	public static FitType[] getLineComponents() {
		return new FitType[] { GAUSS, LORENTZ, VOIGT, SINC };
	}

	/**
	 * @return True if the component is a baseline (Sinus or Polynomial)
	 */
	public boolean isBaseline() {
		return this.equals(POLY) || this.equals(SIN);
	}

}
