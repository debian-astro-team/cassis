/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit.util.enums;

import eu.omp.irap.cassis.fit.components.FitConstraint.FitConstraintType;
import eu.omp.irap.cassis.fit.components.FitParameter;
import eu.omp.irap.cassis.fit.computing.FitNormalizationParameters;

/**
 * Enumeration of the different parameter types that can be found in the
 * fitting components.
 *
 * @author bastienkovac
 *
 */
public enum FitParameterType {

	/**
	 * Intensity of the function
	 */
	I0("i0", 1.0, 0, FitConstraintType.DEFAULT_CONSTRAINTS),
	/**
	 * Center value of the function
	 */
	X0("x0", 0.0, 1, FitConstraintType.DEFAULT_CONSTRAINTS),

	/**
	 * FWHM for a Gaussian
	 */
	GAUSSIAN_FWHM("FWHM", 1.0, 2, FitConstraintType.WIDTH_CONSTRAINTS),
	/**
	 * FWHM for a Lorentzian
	 */
	LORENTZIAN_FWHM("FWHM", 1.0, 2, FitConstraintType.WIDTH_CONSTRAINTS),
	/**
	 * FWHM for a Sinc
	 */
	SINC_FWHM("FWHM", 1.0, 2, FitConstraintType.WIDTH_CONSTRAINTS),

	/**
	 * Sigma of the Gaussian component of a Voigt Profile
	 */
	SIGMA_G("\u03C3G", 1.0, 2, FitConstraintType.WIDTH_CONSTRAINTS),
	/**
	 * Sigma of the Lorentzian component of a Voigt Profile
	 */
	SIGMA_L("\u03C3L", 1.0, 3, FitConstraintType.WIDTH_CONSTRAINTS),

	/**
	 * Intensity of a Sinus
	 */
	SIN_I0("i0", 1.0, 0, FitConstraintType.DEFAULT_CONSTRAINTS),
	/**
	 * Cosine amplitude
	 */
	OMEGA("\u03C9", 1.0, 1, FitConstraintType.DEFAULT_CONSTRAINTS),
	/**
	 * Sinine amplitude
	 */
	PHI("\u03C6", 1.0, 2, FitConstraintType.DEFAULT_CONSTRAINTS);

	private static final String NOT_YET_IMPLEMENTED = " not yet implemented";

	private static final double MIN_VAL_FWHM_LEVENBERG = 1E-6;

	private static final double FWHM_LORENTZIAN = 2;
	private static final double FWHM_SINC = 3.75;
	private static final double FWHM_GAUSSIAN = calcGaussianFwhmCoeff();

	private String nameParameter;
	private double defaultValue;
	private int index;
	private FitConstraintType[] availableConstraints;


	/**
	 * Constructor
	 *
	 * @param name Name of the parameter
	 * @param defaultValue Default value of the parameter
	 * @param index Index of the parameter
	 */
	private FitParameterType(String name, double defaultValue, int index, FitConstraintType[] availableConstraints) {
		this.nameParameter = name;
		this.defaultValue = defaultValue;
		this.index = index;
		this.availableConstraints = availableConstraints;
	}

	/**
	 * @return The constraints that can be applied to this parameter
	 */
	public FitConstraintType[] getAvailableConstraints() {
		return availableConstraints;
	}

	/**
	 * @return The name of the parameter
	 */
	public String getNameParameter() {
		return nameParameter;
	}

	/**
	 * @return The default value of the parameter
	 */
	public double getDefaultValue() {
		return defaultValue;
	}

	/**
	 * @return The index of the parameter in its corresponding component
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * Returns if the parameter with the given index matches a width
	 * parameter, like Sigma or FWHM
	 *
	 * @param index The index
	 * @return True if the parameter with the given index matches a width one
	 */
	public static boolean isWidthParameter(int index) {
		return index == 2 || index == 3;
	}

	/**
	 * Returns a normalized value of the given parameter, each parameter type having
	 * a different normalization equation
	 *
	 * @param param The parameter to normalize
	 * @param nmParams The normalization parameters
	 * @param style The fit style
	 * @return The normlized value of the parameter
	 */
	public static double getNormalizedValue(FitParameter param, FitNormalizationParameters nmParams, FitStyle style) {
		switch(param.getParameterType()) {
		case I0:
			return (param.getValue() + nmParams.getOffsetY()) * nmParams.getScaleY();
		case X0:
			return (param.getValue() + nmParams.getOffsetX()) * nmParams.getScaleX();
		case GAUSSIAN_FWHM:
			return normalizeFWHM(param, nmParams.getScaleX(), style, FWHM_GAUSSIAN);
		case SIGMA_G:
			return normalizeFWHM(param, nmParams.getScaleX(), style, FWHM_GAUSSIAN) / 2;
		case LORENTZIAN_FWHM:
			return normalizeFWHM(param, nmParams.getScaleX(), style, FWHM_LORENTZIAN);
		case SIGMA_L:
			return normalizeFWHM(param, nmParams.getScaleX(), style, FWHM_LORENTZIAN) / 2;
		case SINC_FWHM:
			return normalizeFWHM(param, nmParams.getScaleX(), style, FWHM_SINC);
		case SIN_I0:
		case OMEGA:
		case PHI:
			return param.getValue();
		default:
			throw new IllegalArgumentException("Normalization for " + param.getName() + NOT_YET_IMPLEMENTED);
		}
	}

	/**
	 * Denormalizes the given value according to this parameter type
	 *
	 * @param value The value to denormalize
	 * @param nmParams The normalization parameters
	 * @return The denormalized value
	 */
	public double getDenormalizedValue(double value, FitNormalizationParameters nmParams) {
		switch(this) {
		case I0:
			return value / nmParams.getScaleY() - nmParams.getOffsetY();
		case X0:
			return value / nmParams.getScaleX() - nmParams.getOffsetX();
		case GAUSSIAN_FWHM:
			return value / nmParams.getScaleX() * FWHM_GAUSSIAN;
		case SIGMA_G:
			return (value / nmParams.getScaleX() * FWHM_GAUSSIAN) / 2;
		case LORENTZIAN_FWHM:
			return value / nmParams.getScaleX() * FWHM_LORENTZIAN;
		case SIGMA_L:
			return (value / nmParams.getScaleX() * FWHM_LORENTZIAN) / 2;
		case SINC_FWHM:
			return value / nmParams.getScaleX();
		case SIN_I0:
		case OMEGA:
		case PHI:
			return value;
		default:
			throw new IllegalArgumentException("Value denormalization for " + nameParameter + NOT_YET_IMPLEMENTED);
		}
	}

	/**
	 * Denormalizes the given standard deviation according to this parameter type
	 *
	 * @param value The standard deviation to denormalize
	 * @param nmParams The normalization parameters
	 * @return The denormalized standard deviation
	 */
	public double getDenormalizedDeviation(double value, FitNormalizationParameters nmParams) {
		switch(this) {
		case I0:
			return value / nmParams.getScaleY();
		case X0:
			return value / nmParams.getScaleX();
		case GAUSSIAN_FWHM:
			return value / nmParams.getScaleX() * FWHM_GAUSSIAN;
		case SIGMA_G:
			return (value / nmParams.getScaleX() * FWHM_GAUSSIAN) / 2;
		case LORENTZIAN_FWHM:
			return value / nmParams.getScaleX() * FWHM_LORENTZIAN;
		case SIGMA_L:
			return (value / nmParams.getScaleX() * FWHM_LORENTZIAN) / 2;
		case SINC_FWHM:
			return value / nmParams.getScaleX();
		case SIN_I0:
		case OMEGA:
		case PHI:
			return value;
		default:
			throw new IllegalArgumentException("Deviation denormalization for " + nameParameter + NOT_YET_IMPLEMENTED);
		}
	}

	/**
	 * Normalize a FWHM parameter according to a given constant and style of fitting.
	 *
	 * @param param Parameter to normalize
	 * @param scaleX The scaling in X
	 * @param style The style of fitting
	 * @param constant The constant
	 * @return The normalized value
	 */
	private static double normalizeFWHM(FitParameter param, double scaleX, FitStyle style, double constant) {
		if (FitStyle.LEVENBERG.equals(style) && param.isFixed() && param.getValue() == 0) {
			param.setValue(MIN_VAL_FWHM_LEVENBERG * scaleX);
		}
		return param.getValue() / constant * scaleX;
	}

	/**
	 * Returns the coefficient used to go back and forth between
	 * the sigma of a Gaussian and its FWHM.
	 * Its value is <code>2 * sqrt(2 * ln(2))</code>
	 *
	 * @return The Gaussian FWHM coefficient
	 */
	public static double calcGaussianFwhmCoeff() {
		return 2 * Math.sqrt(2 * Math.log(2));
	}

}
