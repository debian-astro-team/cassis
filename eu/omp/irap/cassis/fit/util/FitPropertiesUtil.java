/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import eu.omp.irap.cassis.fit.FitComponentManager;
import eu.omp.irap.cassis.fit.FitParametersModel;
import eu.omp.irap.cassis.fit.components.FitAbstractComponent;
import eu.omp.irap.cassis.fit.components.FitComponent;
import eu.omp.irap.cassis.fit.util.enums.FitType;
import eu.omp.irap.cassis.fit.util.enums.FitStyle;

/**
 * @author bastienkovac
 *
 * Utility class to save the model
 *
 */
public class FitPropertiesUtil {

	private static final String NB_ITERATIONS = "nbIterations";
	private static final String FITTER = "fitter";
	private static final String OVERSAMPLING = "oversampling";

	private static final String NB_COMPONENTS = "nbComposants";
	private static final String COMPONENT = "composant";
	private static final String NB_PARAMETERS_COMPONENT = "nbParamsComposant";

	private static final String PARAMETER = "Param";
	private static final String PARAMETER_FIXED = "Blocked";
	private static final String PARAMETER_VALUE = "Value";


	private FitPropertiesUtil() {

	}

	/**
	 * Saves the given model in the given file.
	 *
	 * @param model The model to save
	 * @param propertyFile The file in which to save it
	 * @throws IOException If something went wrong during the saving.
	 */
	public static void saveFitModel(FitParametersModel model, File propertyFile) throws IOException {
		Properties properties = new Properties();
		try (FileOutputStream ouput = new FileOutputStream(propertyFile)) {
			model.save(properties);
			properties.store(ouput, "Fit Configuration");
		} catch (IOException e) {
			throw new IOException("An error has occured while saving the configuration", e);
		}
	}

	/**
	 * Loads the given file in the the given model. This method has compatibility
	 * with old Cassis' Fit module's properties files
	 *
	 * @param model The model in which the parameters will be loaded
	 * @param propertyFile The file from which to load
	 * @throws IOException If something went wrong during the loading
	 */
	public static void loadFitModel(FitParametersModel model, File propertyFile) throws IOException {
		Properties properties = new Properties();
		try (FileInputStream input = new FileInputStream(propertyFile)) {
			properties.load(input);
			if (isLegacy(properties)) {
				loadLegacyParametersModel(properties, model);
				loadLegacyManagerModel(properties, model.getManager());
			} else {
				model.load(properties);
			}
		} catch (IOException e) {
			throw new IOException("An error has occured while loading the configuration", e);
		}
	}

	private static void loadLegacyParametersModel(Properties properties, FitParametersModel parametersModel) {
		int nbIterations = Integer.parseInt(properties.getProperty(NB_ITERATIONS, "10000"));
		int oversampling = Integer.parseInt(properties.getProperty(OVERSAMPLING, "10"));
		parametersModel.setNbIterations(nbIterations);
		parametersModel.setOversampling(oversampling);
	}

	private static void loadLegacyManagerModel(Properties properties, FitComponentManager manager) {
		FitStyle style = FitStyle.valueOf(properties.getProperty(FITTER, "AMOEBA"));
		int nbComponents = Integer.parseInt(properties.getProperty(NB_COMPONENTS, "0"));

		manager.selectFitStyle(style);
		manager.clearManager();
		for (int i = 1 ; i <= nbComponents ; i++) {
			manager.addAbstractComponent(loadComponent(i, properties, COMPONENT + i));
		}
	}

	private static FitAbstractComponent loadComponent(int indexComponent, Properties properties, String nameComponent) {
		FitType type = FitType.valueOf(properties.getProperty(nameComponent));
		FitComponent model = type.buildIndividualComponent();

		int nbParameters = Integer.parseInt(properties.getProperty(NB_PARAMETERS_COMPONENT + indexComponent));
		for (int i = 0 ; i < nbParameters && !(type.equals(FitType.POLY)) ; i++) {
			String parameterKey = nameComponent + PARAMETER + (i + 1);
			model.setParameterValue(i, getValue(parameterKey, properties));
			model.setParameterFixed(i, isFixed(parameterKey, properties));
		}
		return model;
	}

	private static boolean isLegacy(Properties properties) {
		return properties.containsKey("xAxisUnit");
	}

	private static double getValue(String key, Properties properties) {
		return Double.valueOf(properties.getProperty(key + PARAMETER_VALUE));
	}

	private static boolean isFixed(String key, Properties properties) {
		return Boolean.valueOf(properties.getProperty(key + PARAMETER_FIXED));
	}

}
