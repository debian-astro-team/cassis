/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit.util;

import herschel.ia.numeric.Double1d;

/**
 * @author bastienkovac
 *
 * Utility class to compute normalization parameters
 *
 */
public class FitNormalizationUtil {

	private FitNormalizationUtil() {

	}

	/**
	 * Computes the offset that needs to be applied to the given array
	 * so the data is the most centered possible around 0
	 *
	 * @param data The data array
	 * @return The value of the offset
	 */
	public static double computeOffset(Double1d data) {
		double min = Math.min(data.get(0), data.get(data.getSize() - 1));
		double max = Math.max(data.get(0), data.get(data.getSize() - 1));
		double offset = 0.;
		if (!(min < 0 && max > 0))
			offset = -((max - min) / 2) - min;
		return offset;
	}

	/**
	 * Computes the scale factor that needs to be applied to the given array
	 * so the data's range is bigger than [-1;1]
	 *
	 * @param data The data array
	 * @return The value of the scale factor
	 */
	public static double computeScale(Double1d data) {
		double min = Math.min(data.get(0), data.get(data.getSize() - 1));
		double scale = 1.;
		if (Math.abs(min) < 1)
			scale = Math.abs(1 / min) * 10;
		return scale;
	}

}
