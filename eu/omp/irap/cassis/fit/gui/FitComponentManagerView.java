/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.fit.FitComponentManager;
import eu.omp.irap.cassis.fit.components.FitAbstractComponent;
import eu.omp.irap.cassis.fit.components.FitComponent;
import eu.omp.irap.cassis.fit.components.FitMultiComponent;
import eu.omp.irap.cassis.fit.components.gui.FitAbstractComponentView;
import eu.omp.irap.cassis.fit.components.gui.FitComponentView;
import eu.omp.irap.cassis.fit.components.gui.FitMultiComponentsView;
import eu.omp.irap.cassis.fit.gui.util.ButtonTitledPanel;
import eu.omp.irap.cassis.fit.util.Category;
import eu.omp.irap.cassis.fit.util.enums.FitStyle;
import eu.omp.irap.cassis.fit.util.enums.FitType;

/**
 * This class is used to display the parameters of the {@link FitComponentManager},
 * as well as the fitting components it holds.
 *
 * @author bastienkovac
 *
 */
@SuppressWarnings("serial")
public class FitComponentManagerView extends JPanel implements ModelListener {

	private static final int[] MULTI_MODEL_DEFAULT_SIZES = new int[] { 2, 3, 4 };

	private FitComponentManager manager;

	private JComboBox<FitStyle> fitStyles;
	private JButton menuButton;
	private JPopupMenu componentsMenu;

	private JMenu baselineComponent;
	private JMenu lineComponent;
	private JMenu multiComponent;

	private JMenuItem clearAll;
	private JMenuItem selectAll;
	private JMenuItem unselectAll;

	private JScrollPane scrollPane;
	private JPanel centerPanel;

	private SortedMap<Category, CategoryPanel> categoryPanels;


	/**
	 * Constructor
	 *
	 * @param manager The manager model
	 */
	public FitComponentManagerView(FitComponentManager manager) {
		super(new BorderLayout());
		this.manager = manager;
		this.manager.addModelListener(this);
		this.categoryPanels = new TreeMap<>();
		add(getTopPanel(), BorderLayout.NORTH);
		add(getScrollPane(), BorderLayout.CENTER);
		for (Category category : manager.getCategories()) {
			for (FitAbstractComponent component : manager.getComponents(category)) {
				addPanel(component, category);
			}
		}
	}

	/**
	 * Returns a combo-box that holds the available FitStyles.
	 * The one selected is the one that will be used for fitting
	 *
	 * @return The combo-box
	 */
	public JComboBox<FitStyle> getFitStyles() {
		if (fitStyles == null) {
			fitStyles = new JComboBox<>(FitStyle.values());
			fitStyles.setRenderer(new DefaultListCellRenderer() {
				@SuppressWarnings("rawtypes")
				@Override
				public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
						boolean cellHasFocus) {
					Object output = ((FitStyle) value).getLabel();
					return super.getListCellRendererComponent(list, output, index, isSelected, cellHasFocus);
				}
			});
			fitStyles.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					manager.selectFitStyle((FitStyle) fitStyles.getSelectedItem());
				}
			});
			fitStyles.setSelectedItem(manager.getSelectedFitStyle());
		}
		return fitStyles;
	}

	/**
	 * @return A {@link JPopupMenu} that let the user adds the various component
	 * to the manager, as well as performing various actions on them
	 */
	public JPopupMenu getComponentsMenu() {
		if (componentsMenu == null) {
			componentsMenu = new JPopupMenu();
			componentsMenu.add(getBaselineComponent());
			componentsMenu.add(getLineComponent());
			componentsMenu.add(getMultiComponent());
			componentsMenu.addSeparator();
			componentsMenu.add(getClearAll());
			componentsMenu.add(getSelectAll());
			componentsMenu.add(getUnselectAll());
		}
		return componentsMenu;
	}

	/**
	 * @return A {@link JMenu} that lists the available line fitting components
	 */
	public JMenu getLineComponent() {
		if (lineComponent == null) {
			lineComponent = new JMenu("Add Line Component");
			for (FitType type : FitType.getLineComponents()) {
				lineComponent.add(getComponentMenu(type));
			}
		}
		return lineComponent;
	}

	/**
	 * @return A {@link JMenu} that lists the available baseline fitting components
	 */
	public JMenu getBaselineComponent() {
		if (baselineComponent == null) {
			baselineComponent = new JMenu("Add Baseline Component");
			for (FitType type : FitType.getBaselineComponents()) {
				baselineComponent.add(getComponentMenu(type));
			}
		}
		return baselineComponent;
	}

	/**
	 * @return A {@link JMenu} that lists fitting components that can be used as multi-components
	 */
	public JMenu getMultiComponent() {
		if (multiComponent == null) {
			multiComponent = new JMenu("Add Multiline Component");
			for (FitType type : FitType.getLineComponents()) {
				multiComponent.add(getMultiComponentMenu(type));
			}
		}
		return multiComponent;
	}

	/**
	 * @return A {@link JMenuItem} that let the user clear every component
	 * currently in the manager
	 */
	public JMenuItem getClearAll() {
		if (clearAll == null) {
			clearAll = new JMenuItem("Clear all");
			clearAll.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					manager.clearManager();
				}
			});
		}
		return clearAll;
	}

	/**
	 * @return A {@link JMenuItem} that let the user select every component
	 * currently in the manager
	 */
	public JMenuItem getSelectAll() {
		if (selectAll == null) {
			selectAll = new JMenuItem("Enable all");
			selectAll.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					manager.setSelectedAll(true);
				}
			});
		}
		return selectAll;
	}

	/**
	 * @return A {@link JMenuItem} that let the user unselect every component
	 * currently in the manager
	 */
	public JMenuItem getUnselectAll() {
		if (unselectAll == null) {
			unselectAll = new JMenuItem("Disable all");
			unselectAll.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					manager.setSelectedAll(false);
				}
			});
		}
		return unselectAll;
	}

	@Override
	public Dimension getPreferredSize() {
		int width = getScrollPane().getPreferredSize().width;
		width += getScrollPane().getVerticalScrollBar().getPreferredSize().width;
		return new Dimension(width, getScrollPane().getPreferredSize().height);
	}

	@Override
	public void dataChanged(ModelChangedEvent e) {
		if (e.getSource().equals(FitComponentManager.COMPONENT_MANAGER_COMPONENT_ADDED_EVENT)) {
			FitAbstractComponent model = (FitAbstractComponent) e.getValue();
			Category category = (Category) e.getOrigin();
			addPanel(model, category);
		} else if (e.getSource().equals(FitComponentManager.COMPONENT_MANAGER_CLEARED_EVENT)) {
			clearPanels();
		} else if (e.getSource().equals(FitComponentManager.COMPONENT_MANAGER_COMPONENT_REMOVED_EVENT)) {
			removePanel((String) e.getValue());
		} else if (e.getSource().equals(FitComponentManager.COMPONENT_MANAGER_ALL_SELECTED_EVENT)) {
			setPanelsSelected((Boolean) e.getValue());
		} else if (e.getSource().equals(FitComponentManager.COMPONENT_MANAGER_STYLE_SELECTED_EVENT)) {
			getFitStyles().setSelectedItem(e.getValue());
		} else if (e.getSource().equals(FitComponentManager.COMPONENT_MANAGER_CATEGORY_REMOVED_EVENT)) {
			getCenterPanel().remove(categoryPanels.get(e.getValue()));
			categoryPanels.remove(e.getValue());
			updateLayout();
		}
	}

	private JPanel getTopPanel() {
		JPanel topPanel = new JPanel();
		topPanel.setBorder(BorderFactory.createTitledBorder("Component Manager"));

		topPanel.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.weightx = 1;
		constraints.weighty = 1;
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.gridx = 0;
		constraints.gridy = 0;
		topPanel.add(getMenuButton(), constraints);
		constraints.gridx += 1;
		topPanel.add(getFitStyles(), constraints);
		return topPanel;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			JPanel inner = new JPanel(new BorderLayout());
			inner.add(getCenterPanel(), BorderLayout.NORTH);
			scrollPane = new JScrollPane(inner);
		}
		return scrollPane;
	}

	private JPanel getCenterPanel() {
		if (centerPanel == null) {
			centerPanel = new JPanel();
		}
		return centerPanel;
	}

	private JButton getMenuButton() {
		if (menuButton == null) {
			menuButton = new JButton("Manage Components");
			menuButton.addMouseListener(new MouseAdapter() {
				@Override
				public void mousePressed(MouseEvent e) {
					getComponentsMenu().show(menuButton, 0, menuButton.getHeight());
				}
			});
		}
		return menuButton;
	}

	private JMenuItem getComponentMenu(final FitType typeToBuild) {
		JMenuItem componentItem = new JMenuItem(typeToBuild.getLabel());
		componentItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				manager.addIndividualComponent(typeToBuild);
			}
		});
		return componentItem;
	}

	private JMenu getMultiComponentMenu(final FitType typeToBuild) {
		JMenu componentMenu = new JMenu(typeToBuild.getLabel());
		for (int defaultSize : MULTI_MODEL_DEFAULT_SIZES) {
			String title = defaultSize == 1
					? Integer.toString(defaultSize) + " " + typeToBuild.getLabel() + " element"
					: Integer.toString(defaultSize) + " " + typeToBuild.getLabel() + " elements";
			JMenuItem defaultItem = new JMenuItem(title);
			final int size = defaultSize;
			defaultItem.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					manager.addMultiComponent(typeToBuild, size - 1);
				}
			});
			componentMenu.add(defaultItem);
		}
		JMenuItem customSize = new JMenuItem("Create x " + typeToBuild.getLabel() + " elements");
		customSize.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int size = getDefaultSize();
				if (size != -1) {
					manager.addMultiComponent(typeToBuild, size - 1);
				}
			}
		});
		componentMenu.add(customSize);
		return componentMenu;
	}

	private int getDefaultSize() {
		String answer = (String) JOptionPane.showInputDialog(
				getTopLevelAncestor(),
				"Please enter the initial number of elements",
				"Creating multi-line component",
				JOptionPane.PLAIN_MESSAGE,
				null,
				null,
				"1");
		int defaultSize = 1;
		try {
			defaultSize = Integer.parseInt(answer);
		} catch (NumberFormatException e) {
			defaultSize = -1;
			JOptionPane.showConfirmDialog(
					getTopLevelAncestor(),
					"The given input was not valid",
					"Wrong input",
					JOptionPane.OK_CANCEL_OPTION,
					JOptionPane.WARNING_MESSAGE);
		}
		return defaultSize;
	}

	private void setPanelsSelected(boolean selected) {
		for (CategoryPanel catPanel : categoryPanels.values()) {
			catPanel.setPanelsSelected(selected);
		}
	}

	private void removePanel(String idPanel) {
		for (CategoryPanel catPanel : categoryPanels.values()) {
			if (catPanel.removePanel(idPanel)) {
				break;
			}
		}
		getCenterPanel().revalidate();
	}

	private void clearPanels() {
		for (CategoryPanel panel : categoryPanels.values()) {
			getCenterPanel().remove(panel);
		}
		getCenterPanel().revalidate();
		categoryPanels.clear();
	}

	private void addPanel(final FitAbstractComponent model, final Category category) {
		FitAbstractComponentView btnPanel = null;
		if (model.isMultiple()) {
			btnPanel = new FitMultiComponentsView((FitMultiComponent) model);
		} else {
			btnPanel = new FitComponentView((FitComponent) model, true, true, true, true, false);
		}
		btnPanel.addButtonClearListener(getClearListener(model));
		btnPanel.addButtonCopyListener(getCopyListener(model));
		initPanelEstimationAction(btnPanel, model);
		if (!categoryPanels.containsKey(category)) {
			categoryPanels.put(category, new CategoryPanel(category));
		}
		categoryPanels.get(category).addPanel(btnPanel);
		updateLayout();
	}

	private void initPanelEstimationAction(final ButtonTitledPanel btnPanel, final FitAbstractComponent model) {
		if (model.isMultiple()) {
			Runnable childEstimationAction = new Runnable() {

				@Override
				public void run() {
					manager.setEstimableComponent(model);
				}
			};
			((FitMultiComponent) model).setChildEstimationAction(childEstimationAction);
		} else {
			btnPanel.addMouseListener(getEstimableListener(model));
		}
	}

	private MouseListener getEstimableListener(final FitAbstractComponent model) {
		return new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				manager.setEstimableComponent(model);
			}
		};
	}

	private ActionListener getCopyListener(final FitAbstractComponent model) {
		return new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				manager.copyComponent(model);
			}
		};
	}

	private ActionListener getClearListener(final FitAbstractComponent model) {
		return new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				manager.removeComponent(model);
			}
		};
	}

	private void updateLayout() {
		getCenterPanel().setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.weightx = 1;
		constraints.weighty = 1;
		constraints.insets = new Insets(5, 5, 5, 5);
		int y = 0;
		for (Entry<Category, CategoryPanel> entry : categoryPanels.entrySet()) {
			constraints.gridx = 0;
			constraints.gridy = y;
			if (categoryPanels.lastKey().equals(entry.getKey())) {
				constraints.weighty = 1.0;
			}
			getCenterPanel().add(entry.getValue(), constraints);
			y++;
		}
		revalidate();
	}

	/**
	 * A panel used to display a category and its content
	 *
	 * @author bastienkovac
	 *
	 */
	private class CategoryPanel extends ButtonTitledPanel {

		private List<FitAbstractComponentView> componentsPanels;
		private Category category;


		/**
		 * Constructor
		 *
		 * @param category The category to display
		 */
		public CategoryPanel(final Category category) {
			super(category.getName(), false, false, true);
			this.category = category;
			this.remove(getButtonFixAll());
			this.componentsPanels = new ArrayList<>();
			addButtonClearListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					manager.removeCategory(category);
				}
			});
			addButtonFoldListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					CategoryPanel.this.category.setFolded(getButtonFold().isSelected());
					updateVisibilityComponents();
				}
			});
			updateVisibilityComponents();
		}

		private void updateVisibilityComponents() {
			for (JPanel panel : componentsPanels) {
				panel.setVisible(!this.category.isFolded());
			}
			revalidate();
			repaint();
		}

		/**
		 * Adds a panel to the category panel
		 *
		 * @param panel The panel to add
		 */
		public void addPanel(FitAbstractComponentView panel) {
			componentsPanels.add(panel);
			updateCategoryLayout();
		}

		/**
		 * Sets all the panels of the category selected
		 *
		 * @param selected The selected flag
		 */
		public void setPanelsSelected(boolean selected) {
			for (JPanel panel : componentsPanels) {
				if (panel instanceof ButtonTitledPanel) {
					ButtonTitledPanel btnPanel = (ButtonTitledPanel) panel;
					if (btnPanel.isBlockable()) {
						((JCheckBox) btnPanel.getLabelTitle()).setSelected(!selected);
						btnPanel.getButtonFixAll().setEnabled(!selected);
						btnPanel.getButtonClear().setEnabled(!selected);
						btnPanel.getButtonCopy().setEnabled(!selected);
					}
				}
			}
		}

		/**
		 * Removes the panel with the given id
		 *
		 * @param idPanel The id of the panel to remove
		 * @return If the removal was successful
		 */
		public boolean removePanel(String idPanel) {
			boolean removed = false;
			for (JPanel panel : componentsPanels) {
				if (panel.getName().equals(idPanel)) {
					removed = componentsPanels.remove(panel);
					getSubPanel().remove(panel);
					updateCategoryLayout();
					break;
				}
			}
			return removed;
		}

		private void updateCategoryLayout() {
			getSubPanel().setLayout(new GridBagLayout());
			GridBagConstraints constraints = new GridBagConstraints();
			constraints.fill = GridBagConstraints.HORIZONTAL;
			constraints.anchor = GridBagConstraints.NORTHWEST;
			constraints.weightx = 1;
			constraints.weighty = 1;
			constraints.insets = new Insets(5, 5, 5, 5);
			int y = 0;
			for (JPanel panel : componentsPanels) {
				constraints.gridx = 0;
				constraints.gridy = y;
				getSubPanel().add(panel, constraints);
				y++;
			}
			revalidate();
		}

	}

}
