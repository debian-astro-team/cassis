/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.text.NumberFormat;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.NumberFormatter;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.fit.FitParametersModel;
import eu.omp.irap.cassis.fit.gui.util.JDoubleCassisTextField;
import eu.omp.irap.cassis.fit.util.FitCurve;

/**
 * @author bastienkovac
 *
 * This class is the parameters view of the module. It also acts as a controller
 * for the {@link FitParametersModel}, which holds the manager
 *
 */
@SuppressWarnings("serial")
public class FitParametersView extends JPanel implements ModelListener {

	private JComboBox<FitCurve> fitCurves;
	private JFormattedTextField nbIterations, oversampling, tolerance;
	private FitParametersModel model;

	private FitComponentManagerView managerView;

	private JPanel tmpParametersPanel, labelPanel;
	private JPanel parametersPanel;

	/**
	 * Constructor
	 *
	 * @param model The associated model
	 */
	public FitParametersView(FitParametersModel model) {
		super(new BorderLayout());
		this.model = model;

		add(getParametersPanel(), BorderLayout.NORTH);
		add(getManagerView(), BorderLayout.CENTER);
		this.model.addModelListener(this);
	}

	/**
	 * Returns the view corresponding to model's component manager
	 *
	 * @return The {@link FitComponentManagerView}
	 */
	public FitComponentManagerView getManagerView() {
		if (managerView == null) {
			managerView = new FitComponentManagerView(model.getManager());
		}
		return managerView;
	}


	/**
	 * Returns a combo-box that holds every data-set currently available in the model.
	 * The selected data-set is the one the fit will be performed on
	 *
	 * @return The combo-box
	 */
	public JComboBox<FitCurve> getFitCurves() {
		if (fitCurves == null) {
			fitCurves = new JComboBox<>();
			for (FitCurve curve : model.getFitCurves()) {
				fitCurves.addItem(curve);
			}
			fitCurves.setRenderer(new DefaultListCellRenderer() {
				@SuppressWarnings("rawtypes")
				@Override
				public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
						boolean cellHasFocus) {
					String name = ((FitCurve) value).getName();
					if (name != null) {
						String[] splitted = name.split("/");
						name = splitted[splitted.length - 1];
					}
					return super.getListCellRendererComponent(list, name, index, isSelected, cellHasFocus);
				}
			});
			fitCurves.setSelectedItem(model.getSourceCurve());
			fitCurves.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					model.setSourceCurve(fitCurves.getItemAt(fitCurves.getSelectedIndex()));
				}
			});
		}
		return fitCurves;
	}

	/**
	 * Returns a text field that allows the user to edit the maximum
	 * number of iterations of the fitter
	 *
	 * @return The text field
	 */
	public JFormattedTextField getNbIterations() {
		if (nbIterations == null) {
			NumberFormat format = NumberFormat.getIntegerInstance();
			format.setGroupingUsed(false);
			NumberFormatter formatter = new NumberFormatter(format);
			formatter.setAllowsInvalid(false);
			nbIterations = new JFormattedTextField(formatter);
			nbIterations.setColumns(9);
			nbIterations.setHorizontalAlignment(JTextField.RIGHT);
			nbIterations.setValue(model.getNbIterations());
			nbIterations.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					model.setNbIterations(Integer.parseInt(nbIterations.getText()));
				}
			});
		}
		return nbIterations;
	}

	/**
	 * Returns a text field that allows the user to edit the oversampling
	 * used to display the fit results
	 *
	 * @return The text field
	 */
	public JFormattedTextField getOversampling() {
		if (oversampling == null) {
			NumberFormat format = NumberFormat.getIntegerInstance();
			format.setGroupingUsed(false);
			NumberFormatter formatter = new NumberFormatter(format);
			formatter.setAllowsInvalid(false);
			oversampling = new JFormattedTextField(formatter);
			oversampling.setColumns(9);
			oversampling.setHorizontalAlignment(JTextField.RIGHT);
			oversampling.setValue(model.getOversampling());
			oversampling.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					model.setOversampling(Integer.parseInt(oversampling.getText()));
				}
			});
		}
		return oversampling;
	}

	/**
	 * Returns a text field that allows the user to edit the general
	 * tolerance of the fitter
	 *
	 * @return The text field
	 */
	public JFormattedTextField getTolerance() {
		if (tolerance == null) {
			tolerance = new JDoubleCassisTextField();
			tolerance.setValue(model.getTolerance());
			tolerance.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					double toleranceValue = FitParametersModel.DEFAULT_TOLERANCE;
					if (tolerance.getValue() instanceof Long) {
						toleranceValue = ((Long) tolerance.getValue()).doubleValue();
					}
					if (tolerance.getValue() instanceof Double) {
						toleranceValue = (Double) tolerance.getValue();
					}
					model.setTolerance(toleranceValue);
				}
			});
		}
		return tolerance;
	}

	@Override
	public void dataChanged(ModelChangedEvent e) {
		if (e.getSource().equals(FitParametersModel.MODEL_NB_ITERATIONS_EVENT)) {
			getNbIterations().setValue(e.getValue());
		} else if (e.getSource().equals(FitParametersModel.MODEL_OVERSAMPLING_EVENT)) {
			getOversampling().setValue(e.getValue());
		} else if (e.getSource().equals(FitParametersModel.MODEL_TOLERANCE_EVENT)) {
			getTolerance().setValue(e.getValue());
		} else if (e.getSource().equals(FitParametersModel.MODEL_CURVE_ADDED_EVENT)) {
			getFitCurves().removeAllItems();
			for (FitCurve c : model.getFitCurves()) {
				getFitCurves().addItem(c);
			}
		} else if (e.getSource().equals(FitParametersModel.MODEL_CURVE_SELECTED_EVENT)) {
			getFitCurves().setSelectedItem(model.getSourceCurve());
		} else if (e.getSource().equals(FitParametersModel.MODEL_CURVES_CLEARED)) {
			getFitCurves().removeAllItems();
		}
	}

	private JPanel getParametersPanel() {
		if(parametersPanel == null) {
			parametersPanel = new JPanel();
			parametersPanel.setBorder(BorderFactory.createTitledBorder("Parameters"));

			JLabel fitCurve = new JLabel("Curve to fit:");

			GroupLayout layout = new GroupLayout(parametersPanel);
			layout.setAutoCreateContainerGaps(true);
			layout.setAutoCreateGaps(true);

			layout.setHorizontalGroup(layout.createSequentialGroup()
					.addGroup(
							layout.createParallelGroup(Alignment.TRAILING)
								.addComponent(fitCurve)
								.addComponent(getLabelPanel()))
					.addGroup(
							layout.createParallelGroup(Alignment.LEADING)
								.addComponent(getFitCurves())
								.addComponent(getTmpParametersPanel()))
					);

			layout.setVerticalGroup(layout.createSequentialGroup()
					.addGroup(
							layout.createParallelGroup(Alignment.BASELINE)
								.addComponent(fitCurve)
								.addComponent(getFitCurves()))
					.addGroup(
							layout.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLabelPanel())
								.addComponent(getTmpParametersPanel()))
					);

			parametersPanel.setLayout(layout);
		}
		return parametersPanel;
	}

	private JPanel getLabelPanel() {
		if (labelPanel == null) {
			labelPanel = new JPanel();
			GroupLayout layout = new GroupLayout(labelPanel);
			layout.setAutoCreateGaps(true);

			JLabel nbIts = new JLabel("Nb. iterations:");
			JLabel toleranceLabel = new JLabel("Tolerance:");

			layout.setHorizontalGroup(layout.createSequentialGroup()
					.addGroup(
						layout.createParallelGroup(Alignment.TRAILING)
							.addComponent(nbIts)
							.addComponent(toleranceLabel))
					);

			layout.setVerticalGroup(layout.createSequentialGroup()
					.addComponent(nbIts)
					.addGap(10)
					.addComponent(toleranceLabel));


			labelPanel.setLayout(layout);
		}
		return labelPanel;
	}

	private JPanel getTmpParametersPanel() {
		if (tmpParametersPanel == null) {
			tmpParametersPanel = new JPanel();
			GroupLayout layout = new GroupLayout(tmpParametersPanel);
			layout.setAutoCreateGaps(true);

			JLabel oversamplingLabel = new JLabel("Oversampling fit:");

			layout.setHorizontalGroup(layout.createSequentialGroup()
					.addGroup(
						layout.createParallelGroup(Alignment.LEADING)
							.addComponent(getNbIterations())
							.addComponent(getTolerance()))
					.addComponent(oversamplingLabel)
					.addComponent(getOversampling()));

			layout.setVerticalGroup(layout.createSequentialGroup()
					.addGroup(
						layout.createParallelGroup(Alignment.BASELINE)
							.addComponent(getNbIterations())
							.addComponent(oversamplingLabel)
							.addComponent(getOversampling()))
					.addGroup(
						layout.createSequentialGroup()
							.addComponent(getTolerance())));


			tmpParametersPanel.setLayout(layout);
		}
		return tmpParametersPanel;
	}

}
