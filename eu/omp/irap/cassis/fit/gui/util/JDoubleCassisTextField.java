/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit.gui.util;

import java.awt.Color;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Locale;
import java.util.Map;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JTextField;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;


/**
 * A JTextField that blocks the user from losing focus if its input isn't correct.
 * A correct input can be parsed as a double or is equal to the character <code>'*'</code>
 * which matches Double.MAX_VALUE. A Map can be used to match certain strings with double values in the same
 * way.
 *
 * @author bastienkovac
 *
 */
@SuppressWarnings("serial")
public class JDoubleCassisTextField extends JFormattedTextField {

	private AbstractFormatterFactory decimal;
	private AbstractFormatterFactory scientifique;
	private DecimalFormat decimalFormat;
	private DecimalFormat scientificFormat;
	private Map<String, Double> symbols;
	private double valMin = 0.0001;
	private double valMax = 9999.999;


	/**
	 * Constructor
	 *
	 * @param decimalFormat The format to use to display double values
	 * @param scientificFormat The format to use when displaying values in scientific format
	 * @param symbols The accepted symbols, with their signification
	 */
	public JDoubleCassisTextField(DecimalFormat decimalFormat, DecimalFormat scientificFormat,
			Map<String, Double> symbols) {
		super();
		this.decimalFormat = decimalFormat;
		this.scientificFormat = scientificFormat;
		this.symbols = symbols;

		decimalFormat.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.US));
		scientificFormat.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.US));
		decimal = new DefaultFormatterFactory(new CassisFormateurFactory(decimalFormat));
		scientifique = new DefaultFormatterFactory(new CassisFormateurFactory(scientificFormat));
		setFormatterFactory(decimal);
		setColumns(9);
		setValue(Double.valueOf(0.0));
		setInputVerifier(new FormattedTextFieldVerifier());

		setHorizontalAlignment(JTextField.RIGHT);
	}

	/**
	 * Constructor
	 */
	public JDoubleCassisTextField() {
		this(new DecimalFormat("##0.0###"), new DecimalFormat("#.###E0"), new java.util.HashMap<String, Double>());
		symbols.put("*", Double.MAX_VALUE);
	}

	/**
	 * @see javax.swing.JFormattedTextField#setValue(java.lang.Object)
	 */
	@Override
	public void setValue(Object value) {
		super.setValue(value);
		if (value instanceof Double) {
			Double val = Math.abs((Double) value);
			if (val == 0) {
				setFormatterFactory(decimal);
			}
			else if (val > valMax || val < valMin) {
				setFormatterFactory(scientifique);
			}
			else {
				setFormatterFactory(decimal);
			}
		}
	}

	/**
	 * The object which verifies the input from the user
	 *
	 * @author bastienkovac
	 *
	 */
	public class FormattedTextFieldVerifier extends InputVerifier {
		@Override
		public boolean verify(JComponent input) {
			if (input instanceof JFormattedTextField) {
				JFormattedTextField ftf = (JFormattedTextField) input;
				AbstractFormatter formatter = ftf.getFormatter();
				if (formatter != null) {
					String text = ftf.getText();
					try {
						formatter.stringToValue(text);
						ftf.setBackground(Color.WHITE);

						return true;
					} catch (ParseException pe) {
						ftf.setBackground(Color.RED);
						return false;
					}
				}
			}
			return true;
		}

		@Override
		public boolean shouldYieldFocus(JComponent input) {
			return verify(input);
		}
	}

	/**
	 * A NumberFormatter which takes into account the accepted symbols
	 * of the field
	 *
	 * @author bastienkovac
	 *
	 */
	public class CassisFormateurFactory extends NumberFormatter {

		/**
		 * Constructor
		 *
		 * @param decimalformat The format to use to display double values
		 */
		public CassisFormateurFactory(DecimalFormat decimalformat) {
			super(decimalformat);
		}

		/**
		 * @see
		 * javax.swing.text.InternationalFormatter#valueToString(java.lang.Object
		 * )
		 */
		@Override
		public String valueToString(Object value) throws ParseException {
			if (value != null) {
				if (value instanceof Double) {
					return readDouble((Double) value);
				}
			}
			return super.valueToString(value);
		}

		/**
		 * Read a double value and return its string representation
		 *
		 * @param value The value
		 * @return Its string representation
		 */
		private String readDouble(Double value) {
			Double val = Math.abs(value);
			if (val == 0) {
				return decimalFormat.format(value);
			}
			else if (val > valMax || val < valMin) {
				return scientificFormat.format(value);
			}
			else {
				return decimalFormat.format(value);
			}
		}

		/**
		 * @see
		 * javax.swing.text.InternationalFormatter#stringToValue(java.lang.String
		 * )
		 */
		@Override
		public Object stringToValue(String text) throws ParseException {
			return super.stringToValue(text.toUpperCase());
		}
	}

}
