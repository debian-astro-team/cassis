/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit.gui.util;

import java.awt.Container;
import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.fit.util.FitException;

/**
 * @author bastienkovac
 *
 * Utility class to perform various GUI-related actions
 *
 */
public class FitGuiUtil {

	private FitGuiUtil() {

	}

	/**
	 * Opens a {@link JFileChooser} that let the user select a file in which
	 * to save, or from which to load, a configuration.
	 *
	 * @param parent The parent container
	 * @param saving True if the file will be used to save
	 * @param defaultPath The default folder of the file chooser
	 * @param lastPath The last used folder for the file chooser
	 * @return The selected file, or null if none was chosen
	 */
	public static File openComponentsPropertiesFile(Container parent, boolean saving, String defaultPath, String lastPath) {
		JFileChooser chooser = new CassisJFileChooser(defaultPath, lastPath);
		chooser.setFileFilter(new FileNameExtensionFilter("Cassis Fit Components", "cfc", "fitc"));
		int answer;
		if (saving) {
			answer = chooser.showSaveDialog(parent);
		} else {
			answer = chooser.showOpenDialog(parent);
		}
		if (answer == JFileChooser.APPROVE_OPTION) {
			File fixedFile = chooser.getSelectedFile();
			if (saving) {
				fixedFile = getSaveFile(parent, fixedFile);
			}
			return fixedFile;
		}
		return null;
	}

	private static File getSaveFile(Container parent, File fixedFile) {
		File saveFile = fixedFile;
		if (!fixedFile.getAbsolutePath().endsWith(".cfc")) {
			saveFile = new File(fixedFile.getAbsolutePath() + ".cfc");
		}
		if (saveFile.exists()) {
			String msg = "The file " + fixedFile.getName() + " already exists.\n" + "Do you want to overwrite it ?";
			int overwrite = JOptionPane.showConfirmDialog(
					parent,
					msg,
					"File already exists",
					JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE);
			if (overwrite != JOptionPane.YES_OPTION) {
				return null;
			}
		}
		return saveFile;
	}

	/**
	 * Opens a {@link JFileChooser} that let the user select a file in which
	 * to save, or from which to load, a configuration.
	 *
	 * @param parent The parent container
	 * @param saving True if the file will be used to save
	 * @return The selected file, or null if none was chosen
	 */
	public static File openComponentsPropertiesFile(Container parent, boolean saving) {
		return openComponentsPropertiesFile(parent, saving, "", "");
	}

	/**
	 * Display the given {@link FitException} as a JDialog
	 *
	 * @param parent The parent container
	 * @param error The error to display
	 */
	public static void displayError(Container parent, FitException error) {
		JOptionPane.showMessageDialog(parent,
				"<html>" + error.getMessageError() + "<br> " + error.getProbableSolution() + "</html>",
				error.getNameError(), JOptionPane.WARNING_MESSAGE);
	}

	/**
	 * Returns a string that is the scientific representation of the given value.
	 * If the number of decimals is less than 4, the classic representation
	 * is returned instead
	 *
	 * @param value The value to format
	 * @return The formatted value
	 */
	public static String formatDouble(double value) {
		NumberFormat formatter = new DecimalFormat("0.#####E0");
		if (value < -10e5 || value > 10e5 || (value > -10e-5 && value < 10e-5)) {
			return Double.doubleToRawLongBits(value) == 0 ? "0.0" : formatter.format(value);
		} else {
			formatter = new DecimalFormat("0.###");
			formatter.setMaximumFractionDigits(4);
			return formatter.format(value);
		}
	}

}
