/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit.gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.fit.FitParametersModel;
import eu.omp.irap.cassis.fit.gui.util.FitGuiUtil;

/**
 * A JPanel with the buttons linked to the basic operations of the Fit
 *
 * @author bastienkovac
 *
 */
@SuppressWarnings("serial")
public class FitActionView extends JPanel implements ModelListener {

	private FitParametersModel model;

	private JButton saveConfiguration, loadConfiguration, startFit;
	private JButton undoButton, redoButton;


	/**
	 * Constructor
	 *
	 * @param model Parameters Model
	 */
	public FitActionView(FitParametersModel model) {
		this.model = model;
		this.model.addModelListener(this);
		setBorder(BorderFactory.createTitledBorder("Actions"));
		setLayout(new GridLayout(3, 2, 5, 2));
		add(getSaveConfiguration());
		add(getLoadConfiguration());
		add(getUndoButton());
		add(getRedoButton());
		add(getStartFit());
	}

	/**
	 * Returns a button that is used to save the current configuration in a file
	 * chosen via a {@link JFileChooser}
	 *
	 * @return The button
	 */
	public JButton getSaveConfiguration() {
		if (saveConfiguration == null) {
			saveConfiguration = new JButton("Save Configuration");
			saveConfiguration.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					File savingFile = FitGuiUtil.openComponentsPropertiesFile(getTopLevelAncestor(), true);
					if (savingFile != null) {
						model.saveConfiguration(savingFile);
					}
				}
			});
		}
		return saveConfiguration;
	}

	/**
	 * Returns a button that is used to load the current configuration from a file
	 * chosen via a {@link JFileChooser}
	 *
	 * @return The button
	 */
	public JButton getLoadConfiguration() {
		if (loadConfiguration == null) {
			loadConfiguration = new JButton("Load Configuration");
			loadConfiguration.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					File loadingFile = FitGuiUtil.openComponentsPropertiesFile(getTopLevelAncestor(), false);
					if (loadingFile != null) {
						model.loadConfiguration(loadingFile);
					}
				}
			});
		}
		return loadConfiguration;
	}

	/**
	 * Returns a button that is used to start the fit from the model
	 *
	 * @return The button
	 */
	public JButton getStartFit() {
		if (startFit == null) {
			startFit = new JButton("Start Fit");
			startFit.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					model.performSafeFit();
				}
			});
		}
		return startFit;
	}

	/**
	 * @return A {@link JButton} that lets the user undo the last action
	 */
	public JButton getUndoButton() {
		if (undoButton == null) {
			undoButton = new JButton("Undo");
			undoButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					model.undo();
				}
			});
			undoButton.setEnabled(false);
		}
		return undoButton;
	}

	/**
	 * @return A {@link JButton} that lets the user redo the last undone action
	 */
	public JButton getRedoButton() {
		if (redoButton == null) {
			redoButton = new JButton("Redo");
			redoButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					model.redo();
				}
			});
			redoButton.setEnabled(false);
		}
		return redoButton;
	}

	@Override
	public void dataChanged(ModelChangedEvent e) {
		if (e.getSource().equals(FitParametersModel.MODEL_UNDO_REDO_EVENT)) {
			getUndoButton().setEnabled(!model.isUndoEmpty());
			getRedoButton().setEnabled(!model.isRedoEmpty());
		}
	}

}
