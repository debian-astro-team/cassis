/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import eu.omp.irap.cassis.common.events.ListenerManager;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.fit.computing.FitComputing;
import eu.omp.irap.cassis.fit.computing.FitEstimator;
import eu.omp.irap.cassis.fit.history.FitUndoRedo;
import eu.omp.irap.cassis.fit.history.ICommand;
import eu.omp.irap.cassis.fit.util.FitCurve;
import eu.omp.irap.cassis.fit.util.FitException;
import eu.omp.irap.cassis.fit.util.FitPropertiesUtil;

/**
 * @author bastienkovac
 *
 * This class is the main model of the fitting module,
 * it holds the general parameters (number of iterations, tolerance ...)
 * as well as a {@link FitComponentManager} which holds the components
 * that will be used for the fit.
 *
 */
public class FitParametersModel extends ListenerManager {

	public static final String MODEL_NB_ITERATIONS_EVENT = "nbIterationsEvent";
	public static final String MODEL_OVERSAMPLING_EVENT = "oversamplingEvent";
	public static final String MODEL_TOLERANCE_EVENT = "toleranceEvent";
	public static final String MODEL_LOAD_FAILED_EVENT = "loadingFailedEvent";
	public static final String MODEL_SAVE_FAILED_EVENT = "savingFailedEvent";
	public static final String MODEL_CURVE_SELECTED_EVENT = "curveSelectedEvent";
	public static final String MODEL_FIT_COMPUTED = "fitComputedEvent";
	public static final String MODEL_FIT_FAILED = "fitFailEvent";
	public static final String MODEL_CURVE_ADDED_EVENT = "curveAddedEvent";
	public static final String MODEL_CURVES_CLEARED = "curvesClearedEvent";

	public static final String MODEL_UNDO_REDO_EVENT = "modelUndoRedoEvent";

	public static final Double DEFAULT_TOLERANCE = 0.001;

	private int nbIterations = 10000;
	private int oversampling = 10;
	private double tolerance = DEFAULT_TOLERANCE;

	private FitCurve sourceCurve;
	private FitCurve studiedCurve;
	private List<FitCurve> fitCurves;
	private List<FitCurve> rangeStudy;

	private double stdResidualError;
	private double rms;

	private FitComponentManager manager;

	private FitUndoRedo undoRedoStacks;


	/**
	 * Constructor
	 */
	public FitParametersModel() {
		this.manager = new FitComponentManager();
		this.fitCurves = new ArrayList<>();
		this.rangeStudy = new ArrayList<>();
		this.undoRedoStacks = new FitUndoRedo();
	}

	/**
	 * Returns an unmodifiable list of the available input curves
	 *
	 * @return The list of possible input data
	 */
	public List<FitCurve> getFitCurves() {
		return Collections.unmodifiableList(fitCurves);
	}

	/**
	 * Adds a curve to the list of possible input data. The given
	 * curve is set as the currently studied one.
	 *
	 * @param fitCurve The curve to add
	 */
	public void addFitCurve(FitCurve fitCurve) {
		this.fitCurves.add(fitCurve);
		if (this.sourceCurve == null) {
			setSourceCurve(fitCurve);
		}
		fireDataChanged(new ModelChangedEvent(MODEL_CURVE_ADDED_EVENT, fitCurve));
	}

	/**
	 * Clears the curves available to be fitted
	 */
	public void clearFitCurves() {
		this.fitCurves.clear();
		this.sourceCurve = null;
		fireDataChanged(new ModelChangedEvent(MODEL_CURVES_CLEARED));
	}

	/**
	 * Returns the source curve
	 *
	 * @return The source curve
	 */
	public FitCurve getSourceCurve() {
		return sourceCurve;
	}

	/**
	 * Returns a FitCurve that represents the ranges to be fitted.
	 * If no range is defined, the source curve is returned instead
	 *
	 * @return The curve representing the data that will be fitted
	 */
	public FitCurve getStudiedCurve() {
		return studiedCurve;
	}

	/**
	 * Sets the curve to be used as source. If the new source is non-null and
	 * different, the previous studied curve is discarded and replaced by the
	 * new source, using the same markers
	 *
	 * @param sourceCurve The source curve
	 */
	public void setSourceCurve(FitCurve sourceCurve) {
		if (sourceCurve != null && !sourceCurve.equals(this.sourceCurve)) {
			if (rangeStudy.size() > 0) {
				forwardMarkers(sourceCurve);
			} else {
				this.studiedCurve = sourceCurve;
			}
			this.undoRedoStacks.reinitialize();
			fireDataChanged(new ModelChangedEvent(MODEL_UNDO_REDO_EVENT));
			this.sourceCurve = sourceCurve;
			if (!this.fitCurves.contains(sourceCurve)) {
				addFitCurve(sourceCurve);
			}
			fireDataChanged(new ModelChangedEvent(MODEL_CURVE_SELECTED_EVENT, sourceCurve));
		}
	}

	/**
	 * Keep markers when changing source curve, the new markers are
	 * from the new source, but with the same bounds (xMin and xMax)
	 * as the old markers
	 *
	 * @param newSource The new source curve
	 */
	private void forwardMarkers(FitCurve newSource) {
		List<FitCurve> newRanges = new ArrayList<>();
		for (FitCurve oldRange : rangeStudy) {
			newRanges.add(new FitCurve(newSource, oldRange.getMinX(), oldRange.getMaxX()));
		}
		rangeStudy.clear();
		for (FitCurve newRange : newRanges) {
			addRange(newRange);
		}
	}

	/**
	 * @return The manager model
	 */
	public FitComponentManager getManager() {
		return manager;
	}

	/**
	 * @return The maximum number of iterations for the fitter
	 */
	public int getNbIterations() {
		return nbIterations;
	}

	/**
	 * @return The oversampling value
	 */
	public int getOversampling() {
		return oversampling;
	}

	/**
	 * @return The general tolerance of the fitter
	 */
	public double getTolerance() {
		return tolerance;
	}

	/**
	 * @param nbIterations The maximum number of iterations for the fitter
	 */
	public void setNbIterations(int nbIterations) {
		this.nbIterations = nbIterations;
		fireDataChanged(new ModelChangedEvent(MODEL_NB_ITERATIONS_EVENT, nbIterations));
	}

	/**
	 * @param oversampling The oversampling value
	 */
	public void setOversampling(int oversampling) {
		this.oversampling = oversampling;
		fireDataChanged(new ModelChangedEvent(MODEL_OVERSAMPLING_EVENT, oversampling));
	}

	/**
	 * @param tolerance The general tolerance of the fitter
	 */
	public void setTolerance(double tolerance) {
		this.tolerance = tolerance;
		fireDataChanged(new ModelChangedEvent(MODEL_TOLERANCE_EVENT, tolerance));
	}

	/**
	 * Studies the data range defined by the given Estimator.
	 * Adds the range to the list of studied ranges, then combine all of them.
	 * The components are then estimated for the given range.
	 *
	 * @param estimator
	 */
	public void studyRange(FitEstimator estimator) {
		this.rangeStudy.add(estimator.getRangeAsCurve());
		this.manager.estimateComponent(estimator);
		this.studiedCurve = FitCurve.mergeRanges(rangeStudy);
	}

	/**
	 * Adds the given range to the ones studied, without estimating the
	 * components
	 *
	 * @param range The range to study
	 */
	public void addRange(FitCurve range) {
		this.rangeStudy.add(range);
		this.studiedCurve = FitCurve.mergeRanges(rangeStudy);
	}

	/**
	 * Removes every study range in which the given x value belongs
	 *
	 * @param x The x value
	 */
	public void removeRanges(double x) {
		Iterator<FitCurve> it = this.rangeStudy.iterator();
		while (it.hasNext()) {
			FitCurve next = it.next();
			if (x >= next.getMinX() && x <= next.getMaxX()) {
				it.remove();
			}
		}
		if (rangeStudy.isEmpty()) {
			this.studiedCurve = sourceCurve;
		} else {
			this.studiedCurve = FitCurve.mergeRanges(rangeStudy);
		}
	}

	/**
	 * Resets the studied ranges and sets the whole data set as studied source
	 */
	public void resetStudyRange() {
		this.rangeStudy.clear();
		this.studiedCurve = this.sourceCurve;
	}

	/**
	 * Resets the last studied range
	 */
	public void resetLastStudiedRange() {
		this.rangeStudy.remove(rangeStudy.size() - 1);
		this.studiedCurve = FitCurve.mergeRanges(rangeStudy);
	}

	/**
	 * Starts the fitting of the current studied data.
	 *
	 * @return the result of the safe fit
	 */
	public FitResult performSafeFit() {
		FitComputing computing = new FitComputing(this);
		FitResult resultFit;
		try {
			resultFit = computing.computeFit(true);
		} catch (FitException e) {
			fireDataChanged(new ModelChangedEvent(MODEL_FIT_FAILED, e));
			return null;
		}
		manager.setEstimableComponent(null);
		fireDataChanged(new ModelChangedEvent(MODEL_FIT_COMPUTED, resultFit));
		return resultFit;
	}

	/**
	 * Starts the fitting of the current studied data.
	 *
	 * @return The result of the fit
	 * @throws FitException If an error occurred during the fitting
	 */
	public FitResult performFit() throws FitException {
		return performFit(true);
	}

	private FitResult performFit(boolean startComputations) throws FitException {
		FitComputing computing = new FitComputing(this);
		FitResult resultFit = computing.computeFit(startComputations);
		manager.setEstimableComponent(null);
		return resultFit;
	}

	/**
	 * Saves the model in the given Properties
	 *
	 * @param properties The Properties object
	 */
	public void save(Properties properties) {
		properties.setProperty("nbIterations", String.valueOf(nbIterations));
		properties.setProperty("oversampling", String.valueOf(oversampling));
		properties.setProperty("tolerance", String.valueOf(tolerance));
		manager.save(properties);
	}

	/**
	 * Loads the model from the given properties
	 *
	 * @param properties The Properties object
	 */
	public void load(Properties properties) {
		setNbIterations(Integer.parseInt(properties.getProperty("nbIterations")));
		setOversampling(Integer.parseInt(properties.getProperty("oversampling")));
		setTolerance(Double.valueOf(properties.getProperty("tolerance")));
		manager.load(properties);
	}

	/**
	 * Saves the configuration in the given file
	 *
	 * @param configurationFile File in which to save the configuration
	 */
	public void saveConfiguration(File configurationFile) {
		try {
			FitPropertiesUtil.saveFitModel(this, configurationFile);
		} catch (IOException e) {
			fireDataChanged(new ModelChangedEvent(MODEL_SAVE_FAILED_EVENT, e.getMessage()));
		}
	}

	/**
	 * Loads the configuration from the given file
	 *
	 * @param configurationFile File from which to load the configuration
	 *
	 * @return True if the configuration is loaded, else False
	 */
	public boolean  loadConfiguration(File configurationFile) {
		boolean res = true;
		try {
			FitPropertiesUtil.loadFitModel(this, configurationFile);
		} catch (IOException e) {
			fireDataChanged(new ModelChangedEvent(MODEL_LOAD_FAILED_EVENT, e.getMessage()));
			res = false;
		}
		return res;
	}

	/**
	 * Adds a command to the undo/redo queues
	 *
	 * @param command The command to add
	 */
	public void addCommand(ICommand command) {
		this.undoRedoStacks.insertCommand(command);
		fireDataChanged(new ModelChangedEvent(MODEL_UNDO_REDO_EVENT));
	}

	/**
	 * Undo the last command
	 */
	public void undo() {
		this.undoRedoStacks.undo();
		fireDataChanged(new ModelChangedEvent(MODEL_UNDO_REDO_EVENT));
	}

	/**
	 * Redo the last undone command
	 */
	public void redo() {
		this.undoRedoStacks.redo();
		fireDataChanged(new ModelChangedEvent(MODEL_UNDO_REDO_EVENT));
	}

	/**
	 * Restore the model to its original state
	 */
	public void restoreOriginal() {
		this.undoRedoStacks.restoreOriginal();
		fireDataChanged(new ModelChangedEvent(MODEL_UNDO_REDO_EVENT));
	}

	/**
	 * @return True if the undo queue is empty
	 */
	public boolean isUndoEmpty() {
		return this.undoRedoStacks.isUndoEmpty();
	}

	/**
	 * @return True if the redo queue is empty
	 */
	public boolean isRedoEmpty() {
		return this.undoRedoStacks.isRedoEmpty();
	}

	/**
	 * Sets the standard deviation residual error value
	 *
	 * @param stdResidualError value
	 */
	public void setStdResidualError(Double stdResidualError) {
		this.stdResidualError = stdResidualError;
	}

	/**
	 * @return The standard deviation residual error value value of the fit
	 */
	public double getStdResidualError() {
		return stdResidualError;
	}

	/**
	 * Sets the rms value
	 *
	 * @param rms The rms
	 */
	public void setRms(double rms) {
		this.rms = rms;
	}

	/**
	 * @return The rms value
	 */
	public double getRms() {
		return rms;
	}

	@Override
	public void removeAllListener() {
		super.removeAllListener();
		manager.removeAllListener();
	}

	@Override
	public void removeModelListener(ModelListener listener) {
		super.removeModelListener(listener);
		manager.removeModelListener(listener);
	}

}
