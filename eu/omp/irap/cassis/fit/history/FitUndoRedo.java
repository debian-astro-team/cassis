/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit.history;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * @author bastienkovac
 *
 * This class is used to handle undo and redo actions, it follow the
 * design pattern Command.
 *
 */
public class FitUndoRedo {

	private Deque<ICommand> undo;
	private Deque<ICommand> redo;


	/**
	 * Constructor
	 */
	public FitUndoRedo() {
		undo = new ArrayDeque<ICommand>();
		redo = new ArrayDeque<ICommand>();
	}

	/**
	 * Adds a command to the undo queue, clears the redo queue and execute
	 * the command
	 *
	 * @param command The command to insert
	 */
	public void insertCommand(ICommand command) {
		command.executeCommand();
		this.undo.push(command);
		this.redo.clear();
	}

	/**
	 * Undo action
	 */
	public void undo() {
		if (!undo.isEmpty()) {
			ICommand top = undo.pop();
			this.redo.push(top);
			top.unexecuteCommand();
		}
	}

	/**
	 * Redo action
	 */
	public void redo() {
		if (!redo.isEmpty()) {
			ICommand top = redo.pop();
			this.undo.push(top);
			top.executeCommand();
		}
	}

	/**
	 * Pops the undo stack until we're back to the original state
	 */
	public void restoreOriginal() {
		while (undo.size() > 0) {
			undo();
		}
		reinitialize();
	}

	/**
	 * Reinitialize the undo/redo queue
	 */
	public void reinitialize() {
		this.undo.clear();
		this.redo.clear();
	}

	/**
	 * @return True if the undo queue is empty
	 */
	public boolean isUndoEmpty() {
		return undo.isEmpty();
	}

	/**
	 * @return True if the redo queue is empty
	 */
	public boolean isRedoEmpty() {
		return redo.isEmpty();
	}

}
