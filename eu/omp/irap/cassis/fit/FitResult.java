/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.fit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import eu.omp.irap.cassis.common.UtilArrayList;
import eu.omp.irap.cassis.fit.computing.FitNormalizationParameters;
import eu.omp.irap.cassis.fit.util.FitCurve;
import herschel.ia.numeric.Double1d;
import herschel.ia.numeric.toolbox.fit.AbstractModel;

/**
 * @author bastienkovac
 *
 * This class is used to hold the results of a fitting operation.
 * It contains the AbstractModel representing the general result of the
 * fit, as well as a list containing the extracted models corresponding to each
 * component.
 *
 * It also holds the parameters necessary to the denormalization of the results.
 *
 */
public class FitResult {

	private List<AbstractModel> extractedComponents;
	private List<String> extractedNames;
	private AbstractModel generalComponent;
	private FitNormalizationParameters nmParams;
	private static boolean showlog = false;

	/**
	 * Constructor
	 *
	 * @param extract The list of individual models
	 * @param general The compound model
	 * @param nmParams The normalization parameters
	 */
	public FitResult(List<AbstractModel> extract, AbstractModel general, FitNormalizationParameters nmParams) {
		this.extractedComponents = extract;
		this.generalComponent = general;
		this.nmParams = nmParams;
	}

	/**
	 * Sets the name to give to extracted components
	 *
	 * @param extractedNames The names of the extracted components
	 */
	public void setExtractedNames(List<String> extractedNames) {
		this.extractedNames = extractedNames;
	}

	/**
	 * @return The name of the extracted component at the i-th index
	 */
	public String getExtractedName(int i) {
		return extractedNames.get(i);
	}

	/**
	 * Returns a {@link FitCurve} representing the compound model
	 *
	 * @param source The source curve
	 * @param oversample The value of the oversampling needed for the curve
	 * @return A curve where the x values are the one of the source curve,
	 * and the y values are computed from the compound model
	 */
	public FitCurve getGeneralFitCurve(FitCurve source, int oversample) {
		FitCurve resultCurve = extractCurve(source, generalComponent, oversample);
		String name = "Compound Fit";
		resultCurve.setName(name);
		return resultCurve;
	}

	/**
	 * Returns a list of {@link FitCurve} where each element is a curve computed from
	 * an individual model
	 *
	 * @param source The source curve
	 * @param oversample The value of the oversampling needed for the curve
	 * @return A list where each element is a curve where the x values are the one
	 * of the source curve, and the y values are computed from the corresponding
	 * individual model
	 */
	public List<FitCurve> getExtractedFitCurves(FitCurve source, int oversample) {
		List<FitCurve> extract = new ArrayList<>();
		for (int i = 0 ; i < extractedComponents.size() ; i++) {
			String name = "Fit Component " + (i + 1);
			if (showlog) {
				System.out.println("**" + name);
				System.out.println("model = " + extractedComponents.get(i) + "parameters = "+ extractedComponents.get(i).getFitParameters());
				System.out.println("Source = [" + source.getMinX() + "; " + source.getMaxX() + "]");
			}
			FitCurve resultCurve = extractCurve(source, extractedComponents.get(i), oversample);

			resultCurve.setName(name);
			extract.add(resultCurve);
		}
		return extract;
	}

	private FitCurve extractCurve(FitCurve source, AbstractModel resultModel, int oversample) {
		double[] oversampleX = UtilArrayList.oversample(source.getX(), oversample);
		Double1d xToFit = new Double1d(Arrays.copyOf(oversampleX, oversampleX.length));
		xToFit.add(nmParams.getOffsetX());
		xToFit.multiply(nmParams.getScaleX());
		Double1d yFitted = resultModel.result(xToFit);
		return new FitCurve(oversampleX, yFitted.getArray());
	}

	/**
	 *  display or not the log
	 * @param showlog
	 */
	public static void setShowlog(boolean showlog) {
		FitResult.showlog = showlog;
	}


	public AbstractModel getGeneralComponent() {
		return generalComponent;
	}
}
