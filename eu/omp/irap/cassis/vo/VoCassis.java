/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.vo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.swing.JOptionPane;

import org.astrogrid.samp.Message;
import org.astrogrid.samp.Metadata;
import org.astrogrid.samp.Response;
import org.astrogrid.samp.Subscriptions;
import org.astrogrid.samp.client.ClientProfile;
import org.astrogrid.samp.client.HubConnection;
import org.astrogrid.samp.client.HubConnector;
import org.astrogrid.samp.client.SampException;
import org.astrogrid.samp.client.TrackedClientSet;
import org.astrogrid.samp.hub.Hub;
import org.astrogrid.samp.hub.HubServiceMode;
import org.astrogrid.samp.xmlrpc.StandardClientProfile;
import org.astrogrid.samp.xmlrpc.XmlRpcKit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.properties.Software;

/**
 * @author glorian
 *
 */
public class VoCassis {

	private static final Logger LOGGER = LoggerFactory.getLogger(VoCassis.class);
	private static VoCassis voCassis = null;

	private HubConnector connector = null;
	private Map<String, Response> responseMap;

	private final XmlRpcKit xmlRpcKit;
	private boolean isConnected = false;

	private static final String[] TRACKED_MTYPES = new String[] {
		VoValue.REGISTER_MTYPE, VoValue.UNREGISTER_MTYPE,
		VoValue.METADATA_MTYPE, VoValue.SUBSCRIPTIONS_MTYPE,
		VoValue.VOTABLE_MTYPE, VoValue.SSA_GENERIC_MTYPE,
		VoValue.FITS_MTYPE, VoValue.DISCONNECT_MTYPE,
		VoValue.SHUTDOWN_MTYPE };


	private VoCassis() {
		xmlRpcKit = XmlRpcKit.getInstance();
	}

	/**
	 * @return The instance of VoCassis.
	 */
	public static VoCassis getInstance() {
		if (voCassis == null) {
			voCassis = new VoCassis();
		}
		return voCassis;
	}

	/**
	 * Connect to the samp hub.
	 */
	public void connect() {
		try {
			if (isConnected) {
				JOptionPane.showMessageDialog(null, "Cassis is already connected!",
						"Already connected", JOptionPane.INFORMATION_MESSAGE);
				LOGGER.info("Cassis is already connected to SAMP Hub");
				return;
			}

			// Get profile.
			final ClientProfile cassisClient = xmlRpcKit == null ? StandardClientProfile.getInstance()
					: new StandardClientProfile(xmlRpcKit);
			final Metadata meta = createDefaultMetadata();
			final Subscriptions subs = createSubscriptions();
			connector = new HubConnector(cassisClient, new TrackedClientSet());

			connector.declareSubscriptions(subs);

			HubConnection connect = null;
			try {
				connect = connector.getConnection();
			} catch(SampException se) {
				LOGGER.debug("A SampException occured while trying to get the connection", se);
			}

			if (connect == null) {
				createHub();
				connect = connector.getConnection();
			}

			responseMap = Collections.synchronizedMap(new HashMap<>());
			responseMap.put("id1", null);

			connector.setAutoconnect(0);
			ReceiveFromVo answer = new ReceiveFromVo(TRACKED_MTYPES);
			connector.addMessageHandler(answer);

			connect.declareMetadata(meta);
			isConnected = true;
		} catch (SampException se) {
			LOGGER.error("A SampException occured while trying to connect to SAMP hub", se);
		}
		return;
	}

	/**
	 * Returns the default Metadata for the Cassis client.
	 *
	 * @return meta the Metadata for the samp Cassis client.
	 */
	private Metadata createDefaultMetadata() {
		Metadata meta = new Metadata();
		meta.setName("Cassis");
		String iconUrl = new File(Software.getCassisPath() + File.separatorChar +
				"ressource" + File.separatorChar + "logo_cassis.gif")
				.toURI().toASCIIString();
		meta.setIconUrl(iconUrl);
		meta.setDescriptionText("Cassis is a spectrum visualizer, analysis and modeling tool.");
		meta.setDocumentationUrl("http://cassis.irap.omp.eu/help/");

		return meta;
	}

	/**
	 * Create the Subscriptions used for the CASSIS client.
	 *
	 * @return The subscriptions for the SAMP CASSIS client.
	 */
	private Subscriptions createSubscriptions() {
		Subscriptions subs = new Subscriptions();
		subs.addMType(VoValue.PING_MTYPE);
		subs.addMType(VoValue.DISCONNECT_MTYPE);
		subs.addMType(VoValue.METADATA_MTYPE);
		subs.addMType(VoValue.VOTABLE_MTYPE);
		subs.addMType(VoValue.SHUTDOWN_MTYPE);
		subs.addMType(VoValue.SUBSCRIPTIONS_MTYPE);
		subs.addMType(VoValue.UNREGISTER_MTYPE);
		subs.addMType(VoValue.FITS_MTYPE);
		subs.addMType(VoValue.SSA_GENERIC_MTYPE);
		return subs;
	}

	/**
	 * Send a file votable file to a client through the samp protocol.
	 *
	 * @param file the file to send.
	 * @param clientId the id of the client ("All" or null for a broadcast)
	 * @return if the file has been sent without error detected.
	 */
	public boolean sendCurrentVoTable(File file, String clientId) {
		if (file == null || !file.exists() || getHubConnection() == null) {
			return false;
		}

		Message msg = createMessage(file);

		if (clientId == null || "All".equals(clientId)) {
			sendToAll(msg);
		} else {
			sendTo(clientId, msg);
		}
		return true;
	}

	/**
	 * Create a message to send using JSamp to send a file.
	 *
	 * @param file The file.
	 * @return The message created.
	 */
	private Message createMessage(File file) {
		Map<String, String> params = new HashMap<>();
		params.put("name", file.getName());
		params.put("url", "file://" + file.getAbsolutePath());
		return new Message(VoValue.VOTABLE_MTYPE, params);
	}

	/**
	 * Send a message to a client.
	 *
	 * @param clientId The client.
	 * @param msg The message.
	 * @return false if there was an error, true otherwise.
	 */
	private boolean sendTo(String clientId, final Message msg) {
		try {
			HubConnection hc = getHubConnection();
			if (hc != null) {
				hc.notify(clientId, msg);
			} else {
				LOGGER.error("No HubConnection. Unable to send message.");
			}
		} catch (SampException e) {
			LOGGER.error("Error while sending a SAMP message", e);
			return false;
		}
		return true;
	}

	/**
	 * Sent a message to all clients.
	 *
	 * @param msg The message to send.
	 * @return false if there was an error, true otherwise.
	 */
	private boolean sendToAll(final Message msg) {
		try {
			HubConnection hc = getHubConnection();
			if (hc != null) {
				hc.notifyAll(msg);
			} else {
				LOGGER.error("No HubConnection. Unable to send message.");
			}
		} catch (SampException se) {
			LOGGER.error("Error while sending a SAMP message to all clients", se);
			return false;
		}
		return true;
	}

	/**
	 * Disconnect from the hub.
	 */
	public void disconnect() {
		if (isConnected) {
			try {
				/*
				* If the hub was created by another app then closed
				* connector.getConnection() return null, but as it did not exist
				* anymore we do not have to unregister from it.
				* So, just mark the connection as not connected in this case.
				*/
				if (connector.getConnection() != null) {
					connector.getConnection().unregister();
				}
				isConnected = false;
			} catch (SampException se) {
				LOGGER.warn("Error while disconnecting from SAMP hub", se);
			}
		}
	}

	/**
	 * Set the state of connection to disconnected (without disconnecting,
	 * in that cas use disconnect). Usefull when receving a DISCONNECT event.
	 */
	public void setDisconnected() {
		isConnected = false;
	}

	/**
	 * Create a samp hub if there is not one already.
	 */
	public void createHub() {
		checkSampFile();
		try {
			Hub.runHub(HubServiceMode.NO_GUI);
			setDisconnected();
		} catch (IOException ioe) {
			LOGGER.error("An Exception occurred while creating the SAMP Hub", ioe);
		}
	}

	/**
	 * @return The HubConnection or null if there is an error.
	 */
	private HubConnection getHubConnection() {
		if (connector == null) {
			return null;
		}
		try {
			return connector.getConnection();
		} catch (SampException se) {
			LOGGER.error("Error while getting the SAMP connection", se);
			return null;
		}
	}

	/**
	 * Send a Map of the clients who can read votable files.
	 * The map contain the client Id has key and the corresponding
	 * client name as value.
	 *
	 * @return the Map of clients.
	 */
	@SuppressWarnings("rawtypes")
	public Map<String, String> getVoTableClients() {
		Map<String, String> clientList = new HashMap<>();
		try {
			@SuppressWarnings("unchecked")
			Map<String, HashMap> map = connector.getConnection().getSubscribedClients(VoValue.VOTABLE_MTYPE);

			for (Map.Entry<String, HashMap> e : map.entrySet()) {
				clientList.put(e.getKey().toString(), connector.getConnection().getMetadata(e.getKey()).getName());
			}
		}
		catch (SampException | NullPointerException e) {
			LOGGER.error("Error while retrieving the VoTable clients", e);
		}

		return clientList;
	}

	/**
	 * Check if the information in HOME/.samp file are OK.
	 * In some case, while stopping samp, the file remain and have bad information on it.
	 */
	private void checkSampFile() {
		String path = System.getProperty("user.home") + File.separatorChar + ".samp";
		File sampFile = new File(path);
		if (sampFile.exists()) {
			Properties prop = new Properties();
			try (FileInputStream fis = new FileInputStream(sampFile)) {
				prop.load(fis);
			} catch (Exception e) {
				LOGGER.warn("Exception while opening file", e);
			}
			String xmlrpcUrl = prop.getProperty("samp.hub.xmlrpc.url");
			if (xmlrpcUrl != null) {
				int idx = xmlrpcUrl.indexOf("/xmlrpc");
				boolean correction = false;
				if (idx > 7 && idx < xmlrpcUrl.length()) {
					xmlrpcUrl = xmlrpcUrl.substring(7, idx);
					correction = true;
				}
				if (correction && xmlrpcUrl.contains(":")) {
					Socket s = null;
					try {
						String host = xmlrpcUrl.split(":")[0];
						int port = Integer.parseInt(xmlrpcUrl.split(":")[1]);
						s = new Socket(host, port);
						s.close();
					} catch (ConnectException ce) {
						LOGGER.debug("ConnectionException while checking the SAMP Connection, can be normal", ce);
						if (sampFile.exists() && !sampFile.delete()) {
							LOGGER.error("Unable to delete samp file");
						}
					} catch (Exception e) {
						LOGGER.warn("Exception while checking the SAMP Connection", e);
					} finally {
						if (s != null) {
							try {
								s.close();
							} catch (IOException e) {
								LOGGER.warn("Exception while closing file", e);
							}
						}
					}
				}
			}
		}
	}
}
