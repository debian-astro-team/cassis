/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.vo;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.astrogrid.samp.Message;
import org.astrogrid.samp.client.AbstractMessageHandler;
import org.astrogrid.samp.client.HubConnection;
import org.astrogrid.samp.client.SampException;

import eu.omp.irap.cassis.gui.menu.action.OpenAction;
import eu.omp.irap.cassis.gui.util.FileUtils;

/**
 * @author thomas
 *
 */
public class ReceiveFromVo extends AbstractMessageHandler {


	public ReceiveFromVo(String[] mtypes) {
		super(mtypes);
	}

	/**
	 * @see org.astrogrid.samp.client.AbstractMessageHandler#processCall(org.astrogrid.samp.client.HubConnection, java.lang.String, org.astrogrid.samp.Message)
	 */
	@Override
	public Map<?, ?> processCall(HubConnection connection, String senderId,
			Message message) throws Exception {
		switch (message.getMType()) {
			// Disconnect
			case VoValue.DISCONNECT_MTYPE:
				VoCassis.getInstance().setDisconnected();
				break;

			// Known file type, open it.
			case VoValue.VOTABLE_MTYPE:
			case VoValue.FITS_MTYPE:
			case VoValue.SSA_GENERIC_MTYPE:
				if (needFiltering(connection, senderId, message)) {
					return null;
				}
			executeSampMessage(message);
				break;

			// Known MTypes, but nothing to do.
			case VoValue.REGISTER_MTYPE:
			case VoValue.UNREGISTER_MTYPE:
			case VoValue.METADATA_MTYPE:
			case VoValue.SUBSCRIPTIONS_MTYPE:
				break;

			case VoValue.SHUTDOWN_MTYPE:
				VoCassis.getInstance().setDisconnected();
				VoCassis.getInstance().connect();
				break;

			// Unknown MType, throw an Exception.
			default:
				throw new IllegalArgumentException(
						"Shouldn't have received MType " + message.getMType());
		}
		return null;
	}

	public void executeSampMessage(Message message) {
		final String url = (String) message.getParam("url");
		File file = FileUtils.downloadAndRenameIfNeeded(url, true);
		if (file != null) {

			@SuppressWarnings("unchecked")
			Map<String, String> params = (Map<String, String>) message.getParam("meta");//createParams(message);

			OpenAction.openCassisFileFromSamp(file, params);

		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Map<String, String> createParams(Message message) {
		Map<String, String> res = new HashMap<>();

		final Object param = message.getParam("meta");
		if (param instanceof Map){
			res = (Map)(param);

		}

		return res;
	}

	/**
	 * Check if we filter the message or not.
	 *
	 * @param connection The connection.
	 * @param senderId The sender id.
	 * @param message The message.
	 * @return true if we have to filter this message, false if not.
	 * @throws SampException In case of SampException.
	 */
	private boolean needFiltering(HubConnection connection, String senderId,
			Message message) throws SampException {
		if (VoValue.VOTABLE_MTYPE.equals(message.getMType()) &&
				"HerschelArchiveUI".equals(connection.getMetadata(senderId).getName())) {
			return true;
		}
		return false;
	}
}
