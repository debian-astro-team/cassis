/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.scripts;

import java.awt.Component;
import java.awt.HeadlessException;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.properties.Software;
import eu.omp.irap.cassis.scripts.JythonModel;
import eu.omp.irap.cassis.scripts.util.DefaultScriptConfig;

public class JythonEditorConfiguration extends DefaultScriptConfig {

	private static final Logger LOGGER = LoggerFactory.getLogger(JythonEditorConfiguration.class);


	@Override
	public JFileChooser getJFileSaver() {
		return new CassisJFileChooser(Software.getScriptPath(),
				Software.getLastFolder("script")) {

			private static final long serialVersionUID = 1L;

			@Override
			public void approveSelection() {
				File f = getSelectedFile();
				if (f != null) {
					if (f.exists()) {
						String msg = "The file \'" + f.getName() + "\' already exists. "
								+ JythonModel.NEW_LINE + "Do you want to replace it?";
						int option = JOptionPane.showConfirmDialog(this, msg,
								JythonModel.SCRIPT_TITLE, JOptionPane.YES_NO_OPTION,
								JOptionPane.QUESTION_MESSAGE);
						if (option == JOptionPane.NO_OPTION) {
							return;
						}
					}
					Software.setLastFolder("script", f.getParent());
				}
				super.approveSelection();
			}

			@Override
			public File getSelectedFile() {
				File f = super.getSelectedFile();
				if (f != null && !f.isDirectory() && !f.getName().endsWith(".py")) {
					return new File(f.getAbsolutePath() + ".py");
				}
				return f;
			}

			@Override
			public int showOpenDialog(Component parent) throws HeadlessException {
				int showOpenDialog = super.showOpenDialog(parent);
				if (showOpenDialog == JFileChooser.APPROVE_OPTION) {
					Software.setLastFolder("script",
							getCurrentDirectory().getAbsolutePath());
				}
				return showOpenDialog;
			}
		};
	}

	@Override
	public JFileChooser getJFileChooser() {
		return new CassisJFileChooser(Software.getScriptPath(),
				Software.getLastFolder("script")) {

			private static final long serialVersionUID = 1L;

			@Override
			public int showOpenDialog(Component parent) throws HeadlessException {
				int showOpenDialog = super.showOpenDialog(parent);
				if (showOpenDialog == JFileChooser.APPROVE_OPTION) {
					Software.setLastFolder("script",
							getCurrentDirectory().getAbsolutePath());
				}
				return showOpenDialog;
			}
		};
	}

	@Override
	public Properties getProperties() {
		Properties prop = new Properties();
		String path = Software.getPropertiesPath() + File.separator
				+ JythonModel.JYTHON_SETTINGS_FILENAME;
		if (new File(path).exists()) {
			try (FileReader fr = new FileReader(path)){
				prop.load(fr);
			} catch (IOException e) {
				LOGGER.error("Error while loading file {}", path, e);
			}
		}
		return prop;
	}

	@Override
	public String getScriptPath() {
		return Software.getScriptPath();
	}

	@Override
	public void log(String message) {
		LOGGER.debug(message);
	}

	@Override
	public void saveProperties(String cle, String value) {
		Properties prop = new Properties();
		String path = Software.getPropertiesPath() + File.separator
				+ JythonModel.JYTHON_SETTINGS_FILENAME;
		try (FileReader fr = new FileReader(path)) {
			prop.load(fr);
		} catch (IOException ioe) {
			LOGGER.error("Error while loading file {}", path, ioe);
		}

		try (BufferedOutputStream bos = new BufferedOutputStream(
				new FileOutputStream(path))) {
			prop.setProperty(cle, String.valueOf(value));
			prop.store(bos, null);
		} catch (IOException ioe) {
			LOGGER.error("Error while saving file {}", path, ioe);
		}
	}

	@Override
	public String getConfigPath() {
		return Software.getPropertiesPath();
	}

}
