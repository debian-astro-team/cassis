/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.popup;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.database.access.VamdcDataBaseConnection;
import eu.omp.irap.cassis.properties.Software;

/**
 * @author thachtn
 */
public class MessagePanel extends ResizablePanel {

	private static final Logger LOGGER = LoggerFactory.getLogger(MessagePanel.class);

	private static final long serialVersionUID = 2763774274015731543L;
	protected MessageModel model;
	/* Image to be drawn on the panel. */
	private transient Image image;
	private MessageControl control;

	public MessagePanel(JLayeredPane layeredPane) {
		this(new MessageControl(layeredPane), new MessageModel("No Message", 0));
		model.setVisiblePersitent(false);
	}

	public MessagePanel(MessageControl control, MessageModel model, boolean visiblePersistent) {
		this(control, model);
		model.setVisiblePersitent(visiblePersistent);
	}

	/**
	 * Constructor.
	 */
	private MessagePanel(MessageControl control, MessageModel model) {
		super();
		this.model = model;
		this.control = control;
		setSize(300, 50);

		setBackground(model.getBgColor());
		setOpaque(false);

		this.model.setReferencesUrl(extractReferences(this.model));
		this.model.setMessage(removeVamdcToken(this.model.getMessage()));
		MyBufferedImage mbi = new MyBufferedImage(this.model);
		image = mbi.getImage();

		addMouseListener(new MyMouseAdapter());
	}

	/**
	 * @param model
	 */
	public static String removeVamdcToken(String message) {
		StringBuilder stringBuilder = new StringBuilder();
		String[] split = message.split("\n");
		for (String string : split) {

			if (!string.startsWith(VamdcDataBaseConnection.VAMDC_REQUEST_TOKEN)){
				stringBuilder.append(string + "\n");
			}
		}
		String res = stringBuilder.toString();
		if (!res.isEmpty()) {
			res = stringBuilder.substring(0, stringBuilder.length()-1);
		}
		return res;
	}

	/**
	 * @param model
	 * @return
	 */
	public URL extractReferences(MessageModel model) {
		String[] split = model.getMessage().split("\n");
		String tokenAttribut = null;
		URL url = null;
		for (String string : split) {

			if (string.startsWith(VamdcDataBaseConnection.VAMDC_REQUEST_TOKEN)){
				tokenAttribut = string.replace(VamdcDataBaseConnection.VAMDC_REQUEST_TOKEN, "");
				tokenAttribut = tokenAttribut.replace("=", "");
			}
		}
		if (tokenAttribut != null){
			final String token = tokenAttribut;
			String emailUser = Software.EMAIL_USER;
			String client = Software.CLIENT;
			url = VamdcDataBaseConnection.getReferences(token, emailUser, client);
		}
		final URL uRlReferences = url;
		return uRlReferences;
	}

	/**
	 * @param split
	 * @param tokenAttribut
	 * @return
	 */
	public URL getURlReferences(String[] split, String tokenAttribut) {
		URL url = null;
		for (String string : split) {

			if (string.startsWith(VamdcDataBaseConnection.VAMDC_REQUEST_TOKEN)){
				tokenAttribut = string.replace(VamdcDataBaseConnection.VAMDC_REQUEST_TOKEN, "");
				tokenAttribut = tokenAttribut.replace("=", "");
			}
		}
		if (tokenAttribut != null){
			final String token = tokenAttribut;
			String emailUser = Software.EMAIL_USER;
			String client = Software.CLIENT;
			url = VamdcDataBaseConnection.getReferences(token, emailUser, client);
		}
		return url;
	}

	public Dimension getMyBufferedImageSize() {
		return new Dimension(image.getWidth(this), image.getHeight(this));
	}

	public void updatePopup() {
		Color col = getBufferedImage();
		Image original = image;
		double width = image.getWidth(this);
		double height = image.getHeight(this);
		image = original.getScaledInstance((int) width * model.getZoom() / 100,
				(int) height * model.getZoom() / 100, Image.SCALE_FAST);
		setBackground(col);
		repaint();
	}

	/**
	 * @return
	 */
	private Color getBufferedImage() {
		MyBufferedImage mbi = new MyBufferedImage(model);
		Color bgColor = model.getBgColor();
		float[] rgbColorComponents = bgColor.getRGBColorComponents(null);
		Color col = new Color(rgbColorComponents[0], rgbColorComponents[1], rgbColorComponents[2],
				model.getTransparency());

		mbi.setBackground(col);
		mbi.repaint();
		image = mbi.getImage();
		return col;
	}

	/**
	 * @return the model
	 */
	public MessageModel getModel() {
		return model;
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2d = (Graphics2D) g;
		AffineTransform origXform = g2d.getTransform();
		AffineTransform newXform = (AffineTransform) origXform.clone();

		double width = image.getWidth(this);
		double height = image.getHeight(this);

		double radian = Math.toRadians(model.getCurrentAngle());
		double newWidth = Math.abs(width * Math.cos(radian)) + Math.abs(height * Math.sin(radian));
		double newHeight = Math.abs(width * Math.sin(radian)) + Math.abs(height * Math.cos(radian));
		this.setSize((int) newWidth, (int) newHeight);

		// The center of rotation is the center of the panel
		int xRot = getWidth() / 2;
		int yRot = getHeight() / 2;
		newXform.rotate(Math.toRadians(model.getCurrentAngle()), xRot, yRot);
		g2d.setTransform(newXform);

		// Draw image centered in panel
		int x = (getWidth() - (int) width) / 2;
		int y = (getHeight() - (int) height) / 2;
		g2d.drawImage(image, x, y, this);
		g2d.setTransform(origXform);
	}

	class MyMouseAdapter extends MouseAdapter {

		@Override
		public void mouseClicked(MouseEvent e) {
//			System.out.println("Mouse clicked");
//			System.out.println(e.getClickCount() == 2);
//			System.out.println(e.getClickCount() == 1);
//			System.out.println(e.getClickCount() == 1);
//			if(e.getClickCount() == 2  && SwingUtilities.isLeftMouseButton(e)) {
//				MessageEditDialog dialog = new MessageEditDialog((JFrame) MessagePanel.this
//						.getTopLevelAncestor(), MessagePanel.this);
//				dialog.setVisible(true);
//			} else
			if (e.getClickCount() == 1  && SwingUtilities.isLeftMouseButton(e)){
				if (model.haveURL()) {
					URL url = model.getReferencesUrl();
						if (url != null){
							try {
								Desktop.getDesktop().browse(url.toURI());
							} catch (IOException | URISyntaxException ex) {
								LOGGER.error("Exception occured while trying to open the URL", ex);
							}
						}
				}
			} else if (SwingUtilities.isRightMouseButton(e) && model.isEditAllowed()) {
				final JPopupMenu menu = new JPopupMenu();
				// Create and add an 'Edit' item
				JMenuItem editItem = new JMenuItem("Edit");
				editItem.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						MessageEditDialog dialog = new MessageEditDialog((JFrame) MessagePanel.this
								.getTopLevelAncestor(), MessagePanel.this);
						dialog.setVisible(true);
					}
				});
				menu.add(editItem);

				// Fix item
				String nameItem = "";
				if (!model.isVisiblePersitent()) {
					nameItem = "Fix Message";
				}
				else {
					nameItem = "Unfix Message";
				}

				JMenuItem fixItem = new JMenuItem(nameItem);
				fixItem.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						model.setVisiblePersitent(!model.isVisiblePersitent());
					}
				});
				menu.add(fixItem);

				// Close item
				JMenuItem closeItem = new JMenuItem("Close");
				closeItem.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						control.removeMessagePanel(MessagePanel.this, true, model.getCurrentSpectrum());
					}
				});
				menu.add(closeItem);
				menu.show(MessagePanel.this,e.getX(),e.getY());
			}
		}
	}

	public void update() {
		Color col = getBufferedImage();
		setBackground(col);
		repaint();
	}

	public Dimension getSizeImage() {
		if (image == null) {
			return new Dimension(0, 0);
		}
		return new Dimension(image.getWidth(this), image.getHeight(this));
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JLayeredPane layeredPane = new JLayeredPane();
		final JPanel jPanel = new JPanel();
		frame.add(jPanel);
		jPanel.setLayout(new BorderLayout());
		jPanel.add(new JLabel("toto"), BorderLayout.CENTER);
		jPanel.add(layeredPane, -1);
		MessagePanel messagePanel = new MessagePanel(layeredPane);
		try {
			messagePanel.getModel().setReferencesUrl(new URL("http://cassis.irap.omp.eu"));
		} catch (MalformedURLException e) {
			LOGGER.error("Malformed URL.", e);
		}
		messagePanel.updatePopup();
		layeredPane.add(messagePanel);
		frame.setVisible(true);
	}

}
