/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.popup;

import java.awt.Component;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JPanel;

/**
 * @author thachtn
 */
@SuppressWarnings("serial")
public class ResizablePanel extends JPanel {

	/*drag flag*/
	private boolean drag = false;
	/*drag location*/
	private Point dragLocation = new Point();
	private MyMouseAdapter myMouseAdapter = new MyMouseAdapter();
	private static final double MARGE = 20.;


	public ResizablePanel() {
		super();
		addMouseMotionListener(new MyMouseMotionAdapter());
		addMouseListener(myMouseAdapter);
	}

	/**
	 * @return the drag
	 */
	public boolean isDrag() {
		return drag;
	}

	/**
	 * @param drag the drag to set
	 */
	public void setDrag(boolean drag) {
		this.drag = drag;
	}

	/**
	 * @return the dragLocation
	 */
	public Point getDragLocation() {
		return dragLocation;
	}

	/**
	 * @param dragLocation the dragLocation to set
	 */
	public void setDragLocation(Point dragLocation) {
		this.dragLocation = dragLocation;
	}

	class MyMouseMotionAdapter extends MouseMotionAdapter {
		@Override
		public void mouseDragged(MouseEvent e) {
			if (isDrag()) {
				if (getDragLocation().getX() > getWidth() - 10 && getDragLocation().getY() > getHeight() - 10) {
					setSize((int) (getSize().getWidth() + (e.getPoint().getX() - getDragLocation().getX())),
							(int) (getSize().getHeight() + (e.getPoint().getY() - getDragLocation().getY())));
					setDragLocation(e.getPoint());
					revalidate();
				}
				else {
					Component c = e.getComponent();
					Component parent = c.getParent();

					double xMin = MARGE;
					double xMax = parent.getSize().getWidth() - MARGE;
					double yMin = MARGE;
					double yMax = parent.getSize().getHeight() - MARGE;

					int x = c.getX() + (e.getX() - dragLocation.x);
					int y = c.getY() + (e.getY() - dragLocation.y);
					if (x < xMin)
						x = (int) xMin;
					if (x > xMax)
						x = (int) xMax;

					if (y < yMin)
						y = (int) yMin;
					if (y > yMax)
						y = (int) yMax;

					c.setLocation(x, y);
				}
			}
		}

	}

	/**
	 * @return the myMouseAdapter
	 */
	public MyMouseAdapter getMyMouseAdapter() {
		return myMouseAdapter;
	}

	class MyMouseAdapter extends MouseAdapter {
		@Override
		public void mousePressed(MouseEvent e) {
			drag = true;
			dragLocation = e.getPoint();
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			drag = false;
			dragLocation = new Point();
		}
	}

}
