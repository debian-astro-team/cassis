/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.popup;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Hashtable;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextPane;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author thachtn
 */
public class MessageEditDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageEditDialog.class);
	private MessagePanel msgPanel;
	// Message Text Area
	private JTextPane messageText = null;
	private JButton okBtn = null;
	private JButton cancelBtn = null;
	private JPanel contentPanel = null;
	private JComboBox<String> fontsCbx = null;
	private JComboBox<Integer> fontSizeCbx = null;
	private JComboBox<String> typefaceCbx = null;
	private JLabel rotationLbl = null;
	private JComboBox<Double> orientationCbx = null;
	private JButton colorBtn = null;
	private JButton colorBgBtn = null;
	private JPanel parameterPanel = null;
	private JPanel textPanel = null;
	private JPanel buttonPanel = null;
	private JSlider transparencySlider = null;
	private AbstractDocument doc;
	private MessageModel model;


	public MessageEditDialog(JFrame owner, MessagePanel msgPanel) {
		super(owner);
		this.msgPanel = msgPanel;
		this.doc = msgPanel.getModel().getDoc();
		this.model = msgPanel.getModel();
		setTitle("Text parametres");
		setContentPane(getContentPanel());
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				dispose();
			}
		});
		pack();
	}

	/**
	 * Get content panel.
	 *
	 * @return the content panel.
	 */
	public JPanel getContentPanel() {
		if (contentPanel == null) {
			contentPanel = new JPanel(new BorderLayout());
			contentPanel.add(getParameterPanel(), BorderLayout.NORTH);
			contentPanel.add(getTextPanel(), BorderLayout.CENTER);
			contentPanel.add(getButonPanel(), BorderLayout.PAGE_END);
		}
		return contentPanel;
	}

	void setGridBagConstraints(GridBagConstraints c, int fill, int gridx, int gridy, int weightx, int weighty) {
		c.fill = fill;
		c.gridx = gridx;
		c.gridy = gridy;
		c.weighty = weighty;
		c.weightx = weightx;
	}

	/**
	 * @return the parameterPanel
	 */
	public JPanel getParameterPanel() {
		if (parameterPanel == null) {
			parameterPanel = new JPanel(new GridBagLayout());

			JLabel fontLbl = new JLabel("Font : ");
			GridBagConstraints c = new GridBagConstraints();
			setGridBagConstraints(c, GridBagConstraints.EAST, 0, 0, 1, 1);
			parameterPanel.add(fontLbl, c);

			c = new GridBagConstraints();
			setGridBagConstraints(c, GridBagConstraints.BOTH, 1, 0, 2, 1);
			parameterPanel.add(getFontsCbx(), c);

			c = new GridBagConstraints();
			setGridBagConstraints(c, GridBagConstraints.EAST, 2, 0, 1, 1);
			parameterPanel.add(new JLabel("Size : "), c);

			c = new GridBagConstraints();
			setGridBagConstraints(c, GridBagConstraints.BOTH, 3, 0, 1, 1);
			parameterPanel.add(getFontSizeCbx(), c);

			c = new GridBagConstraints();
			setGridBagConstraints(c, GridBagConstraints.EAST, 0, 1, 1, 1);
			parameterPanel.add(new JLabel("Typeface : "), c);

			c = new GridBagConstraints();
			setGridBagConstraints(c, GridBagConstraints.BOTH, 1, 1, 2, 1);
			parameterPanel.add(getTypefaceCbx(), c);

			c = new GridBagConstraints();
			setGridBagConstraints(c, GridBagConstraints.EAST, 2, 1, 1, 1);
			parameterPanel.add(new JLabel("  Text Color : "), c);

			c = new GridBagConstraints();
			setGridBagConstraints(c, GridBagConstraints.NONE, 3, 1, 1, 1);
			parameterPanel.add(getColorBtn(), c);

			c = new GridBagConstraints();
			setGridBagConstraints(c, GridBagConstraints.EAST, 0, 2, 1, 1);
			parameterPanel.add(new JLabel("Orientation : "), c);

			c = new GridBagConstraints();
			setGridBagConstraints(c, GridBagConstraints.BOTH, 1, 2, 2, 1);
			parameterPanel.add(getOrientationCbx(), c);

			c = new GridBagConstraints();
			setGridBagConstraints(c, GridBagConstraints.EAST, 2, 2, 1, 1);
			parameterPanel.add(new JLabel("Bg Color : "), c);

			c = new GridBagConstraints();
			setGridBagConstraints(c, GridBagConstraints.NONE, 3, 2, 1, 1);
			parameterPanel.add(getColorBgBtn(), c);

			c = new GridBagConstraints();
			setGridBagConstraints(c, GridBagConstraints.EAST, 0, 3, 1, 1);
			parameterPanel.add(new JLabel("Transparency : "), c);

			c = new GridBagConstraints();
			setGridBagConstraints(c, GridBagConstraints.BOTH, 1, 3, 2, 1);
			parameterPanel.add(getTransparencySlider(), c);

			parameterPanel.setBorder(BorderFactory.createEmptyBorder(10, 1, 10, 1));
		}

		return parameterPanel;
	}

	/**
	 * @return the textPanel
	 */
	public JPanel getTextPanel() {
		if (textPanel == null) {
			textPanel = new JPanel(new BorderLayout());
			textPanel.add(getMessageText(), BorderLayout.CENTER);
		}
		return textPanel;
	}

	public JComboBox<String> getFontsCbx() {
		if (fontsCbx == null) {
			GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
			String[] fontnames = env.getAvailableFontFamilyNames();

			fontsCbx = new JComboBox<>(fontnames);
			fontsCbx.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					String fontname = (String) e.getItem();
					model.setFontname(fontname);
				}
			});

			if (model.getFontname() != null) {
				fontsCbx.setSelectedItem(model.getFontname());
			}
		}

		return fontsCbx;
	}

	public JComboBox<Integer> getFontSizeCbx() {
		if (fontSizeCbx == null) {
			Integer[] sizes = { 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 18, 20, 24, 28, 30, 40 };

			fontSizeCbx = new JComboBox<>(sizes);
			fontSizeCbx.setEditable(true);
			fontSizeCbx.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					int size = (Integer) fontSizeCbx.getSelectedItem();
					model.setFontSize(size);
				}
			});
			fontSizeCbx.setSelectedItem(model.getFontSize());
		}
		return fontSizeCbx;
	}

	/**
	 * Return the String corresponding to the typeface with the given parameters
	 *  in the type face combobox.
	 *
	 * @param isBold if is bold
	 * @param isItalic if is italic
	 * @param isUnderline if is underline
	 * @return the String corresponding to the typeface.
	 */
	private static String getTypeface(boolean isBold, boolean isItalic, boolean isUnderline) {
		if (!isBold && !isItalic && !isUnderline) {
			return "None";
		}
		StringBuilder sb = new StringBuilder();
		if (isBold) {
			sb.append("Bold ");
		}
		if (isItalic) {
			sb.append("Italic ");
		}
		if (isUnderline) {
			sb.append("Underline");
		}
		return sb.toString().trim();
	}

	/**
	 * @return the typeFaceCbx
	 */
	public JComboBox<String> getTypefaceCbx() {
		if (typefaceCbx == null) {
			String[] typefaceStrs = { "None", "Bold", "Italic", "Underline", "Bold Italic", "Bold Underline",
					"Italic Underline", "Bold Italic Underline" };

			typefaceCbx = new JComboBox<>(typefaceStrs);
			typefaceCbx.setSelectedItem(MessageEditDialog.getTypeface(
					model.isBold(), model.isItalic(), model.isUnderline()));
			typefaceCbx.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					String typeFace = (String) typefaceCbx.getSelectedItem();

					model.setBold(typeFace.contains("Bold"));
					model.setItalic(typeFace.contains("Italic"));
					model.setUnderline(typeFace.contains("Underline"));
				}
			});
		}
		return typefaceCbx;
	}

	/**
	 * Get the button panel.
	 *
	 * @return the button panel.
	 */
	public JPanel getButonPanel() {
		if (buttonPanel == null) {
			buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
			buttonPanel.add(getOkBtn());
			buttonPanel.add(getCancelBtn());
			buttonPanel.setBorder(BorderFactory.createEmptyBorder(10, 1, 10, 1));
		}
		return buttonPanel;
	}

	/**
	 * Get the OK button.
	 *
	 * @return the OK button.
	 */
	public JButton getOkBtn() {
		if (okBtn == null) {
			okBtn = new JButton("OK");
			okBtn.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					String message = messageText.getText();
					double rotation = (Double) getOrientationCbx().getSelectedItem();
					msgPanel.getModel().setCurrentAngle(rotation);
					msgPanel.getModel().setMessage(message);
					msgPanel.getModel().setDoc(doc);
					msgPanel.updatePopup();
					msgPanel.getModel().setVisiblePersitent(true);
					dispose();
				}
			});
		}
		return okBtn;
	}

	/**
	 * Get the cancel button.
	 *
	 * @return the cancel button.
	 */
	public JButton getCancelBtn() {
		if (cancelBtn == null) {
			cancelBtn = new JButton("Cancel");
			cancelBtn.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					dispose();
				}
			});
		}
		return cancelBtn;
	}

	/**
	 * @return the messageText
	 */
	public JTextPane getMessageText() {
		if (messageText == null) {
			if (doc == null) {
				doc = (AbstractDocument) new JTextPane().getStyledDocument();
				SimpleAttributeSet attr = new SimpleAttributeSet();

				StyleConstants.setFontFamily(attr, model.getFontname());
				StyleConstants.setFontSize(attr, model.getFontSize());
				StyleConstants.setForeground(attr, model.getColor());
				StyleConstants.setBold(attr, model.isBold());
				StyleConstants.setUnderline(attr, model.isUnderline());
				StyleConstants.setItalic(attr, model.isItalic());
				try {
					doc.insertString(doc.getLength(), model.getMessage(), attr);
				} catch (BadLocationException ble) {
					LOGGER.error("Couldn't insert initial text", ble);
				}
			}

			messageText = new JTextPane((StyledDocument) doc);
			float[] rgbColorComponents = model.getBgColor().getRGBColorComponents(null);
			messageText.setBackground(new Color(rgbColorComponents[0], rgbColorComponents[1], rgbColorComponents[2], model.getTransparency()));
		}
		return messageText;
	}

	/**
	 * @return the orientationCbx
	 */
	public JComboBox<Double> getOrientationCbx() {
		if (orientationCbx == null) {
			Double[] orientations = { 0., 90., 180., 270. };
			orientationCbx = new JComboBox<>(orientations);
			orientationCbx.setEditable(true);
			orientationCbx.setSelectedItem(model.getCurrentAngle());
		}
		return orientationCbx;
	}

	/**
	 * @return the colorBtn
	 */
	public JButton getColorBtn() {
		if (colorBtn == null) {
			colorBtn = new JButton();
			colorBtn.setBackground(model.getColor());

			colorBtn.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					OptionPaneColor jopColor = new OptionPaneColor(model.getColor());
					JDialog dialog = jopColor.createDialog(MessageEditDialog.this, "Change the color parameter ");
					dialog.setVisible(true);

					Object val = jopColor.getValue();
					if ("OK".equals(val)) {
						Color newColor = jopColor.getColor();
						model.setColor(newColor);
						colorBtn.setBackground(newColor);
					}
				}
			});
		}
		return colorBtn;
	}

	/**
	 * @return the colorBgBtn
	 */
	public JButton getColorBgBtn() {
		if (colorBgBtn == null) {
			colorBgBtn = new JButton("");
			colorBgBtn.setBackground(model.getBgColor());

			colorBgBtn.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					OptionPaneColor jopColor = new OptionPaneColor(model.getBgColor());
					JDialog dialog = jopColor.createDialog(MessageEditDialog.this, "Change the color parameter ");
					dialog.setVisible(true);

					Object val = jopColor.getValue();
					if ("OK".equals(val)) {
						Color newColor = jopColor.getColor();
						model.setBgColor(newColor);
						float[] rgbColorComponents = newColor.getRGBColorComponents(null);
						messageText.setBackground(new Color(rgbColorComponents[0], rgbColorComponents[1], rgbColorComponents[2], model.getTransparency()));
						colorBgBtn.setBackground(newColor);
						repaint();
					}
				}
			});
		}
		return colorBgBtn;
	}

	/**
	 * @return the doc
	 */
	public AbstractDocument getDoc() {
		return doc;
	}

	/**
	 * @return the rotationLbl
	 */
	public JLabel getRotationLbl() {
		if (rotationLbl == null) {
			rotationLbl = new JLabel("Rotation :");
		}
		return rotationLbl;
	}

	public JSlider getTransparencySlider() {
		if (transparencySlider == null) {
			int transparency = (int) (model.getTransparency() * 10);
			transparencySlider = new JSlider(0, 10, transparency);
			Hashtable<Integer, JLabel> labelTable = new Hashtable<>();
			labelTable.put(0, new JLabel("Transparent"));
			labelTable.put(10, new JLabel("Opaque"));
			transparencySlider.setLabelTable(labelTable);
			transparencySlider.setPaintLabels(true);

			transparencySlider.addChangeListener(new ChangeListener() {
				@Override
				public void stateChanged(ChangeEvent e) {
					int value = transparencySlider.getValue();
					float valueF = (float) value / 10.0f;
					model.setTransparency(valueF);

					float[] rgbColorComponents = model.getBgColor().getRGBColorComponents(null);
					messageText.setBackground(new Color(rgbColorComponents[0], rgbColorComponents[1], rgbColorComponents[2], model.getTransparency()));
					repaint();
				}
			});
		}
		return transparencySlider;
	}
}
