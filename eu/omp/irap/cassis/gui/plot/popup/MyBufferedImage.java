/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.popup;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.JWindow;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author thachtn
 */
public class MyBufferedImage extends ResizablePanel {

	private static final long serialVersionUID = -2323296173520077500L;
	private static final Logger LOGGER = LoggerFactory.getLogger(MyBufferedImage.class);

	private JTextPane messageText;
	private AbstractDocument doc;
	private JLabel urlLabel;


	public MyBufferedImage(MessageModel model) {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		JPanel contain = new JPanel(new BorderLayout());
		contain.setBackground(new  Color(0, 0, 0, 0));
		contain.setOpaque(false);
		messageText = new JTextPane();
		messageText.setBackground(new Color(0, 0, 0, 0));
		messageText.setOpaque(false);
		doc = getDoc(model);
		model.setDoc(doc); // update model
		contain.add(messageText, BorderLayout.CENTER);
		if (model.getReferencesUrl() != null) {
			String urlString = model.getReferencesUrl().toString();
			urlLabel = new JLabel("<HTML><a href='"
					+ urlString
					+ "'> "
					+ "VAMDC references"
					+ " </a></HTML>");

			urlLabel.setBackground(new Color(0, 0, 0, 0));
			urlLabel.setOpaque(false);
			contain.add(urlLabel, BorderLayout.SOUTH);
		}

		add(contain);
		setBackground(model.getBgColor());
	}

	public BufferedImage getImage() {
		// Create a temporary window
		JWindow win = new JWindow();
		win.setContentPane(this);
		win.pack(); // important
		win.doLayout();
		win.validate();

		// Create a buffered image
		BufferedImage image = new BufferedImage(win.getWidth(), win.getHeight(), BufferedImage.TYPE_INT_ARGB);
		// Get its Graphics2D which is used to draw into image
		Graphics2D g = image.createGraphics();
		// Draw
		win.getContentPane().paint(g);
		// Dispose the graphics context
		g.dispose();
		win.dispose();

		return image;
	}

	/**
	 * Configure and return the {@link AbstractDocument}.
	 *
	 * @param model The {@link MessageModel} contening the message and settings.
	 * @return the {@link AbstractDocument}.
	 */
	public AbstractDocument getDoc(MessageModel model) {
		if (doc == null) {
			doc = (AbstractDocument) messageText.getStyledDocument();

			SimpleAttributeSet attr = new SimpleAttributeSet();
			StyleConstants.setFontFamily(attr, model.getFontname());
			StyleConstants.setFontSize(attr, model.getFontSize());
			StyleConstants.setForeground(attr, model.getColor());
			StyleConstants.setBold(attr, model.isBold());
			StyleConstants.setUnderline(attr, model.isUnderline());
			StyleConstants.setItalic(attr, model.isItalic());

			try {
				doc.insertString(doc.getLength(), model.getMessage(), attr);
			} catch (BadLocationException ble) {
				LOGGER.error("Couldn't insert initial text", ble);
			}
		}

		return doc;
	}

	/**
	 * @return the messageText
	 */
	public JTextPane getMessageText() {
		return messageText;
	}

	public JComponent getUrlLabel() {
		return urlLabel;
	}

}
