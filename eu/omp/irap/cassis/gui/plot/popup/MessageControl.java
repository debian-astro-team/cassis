/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.popup;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLayeredPane;

import eu.omp.irap.cassis.gui.util.MyLayeredPane;

/**
 * Class: MessageControl
 * Contains a list of MessagePanel and the display of them
 *
 * @author thachtn
 *
 */
public class MessageControl {

	// Message Panel for point information
	private List<MessagePanel> images;
	private JLayeredPane layeredPane;
	private boolean allowNewImage;


	/**
	 * Constructor.
	 *
	 * @param layeredPane The layeredPane.
	 */
	public MessageControl(JLayeredPane layeredPane) {
		this.layeredPane = layeredPane;
		images = new ArrayList<>();
		this.allowNewImage = true;
	}

	/**
	 * Refresh popups.
	 *
	 * @param cat The id of the spectrum.
	 */
	public void refreshPopups(int cat) {
		removeNonPersistentMessages();
		repaintCat(cat);
	}

	/**
	 * Remove all messages non persistent.
	 */
	private void removeNonPersistentMessages() {
		List<MessagePanel> listNonPersistent = new ArrayList<>();
		for (MessagePanel image : images) {
			if (!image.getModel().isVisiblePersitent()) {
				listNonPersistent.add(image);
			}
		}

		for (MessagePanel element : listNonPersistent) {
			removeMessagePanel(element, false, -1);
		}
	}

	public void removeAllMessages(int type) {
		List<MessagePanel> removeList = new ArrayList<>();
		for (MessagePanel image : images) {
			if (image.getModel().getType() == type) {
				removeList.add(image);
			}
		}
		for (MessagePanel element : removeList) {
			removeMessagePanel(element, false, -1);
		}
		repaintCat(-1);
	}

	/**
	 * Remove all messages.
	 */
	public void removeAllMessages() {
		List<MessagePanel> listMessagePanel = new ArrayList<>(images);
		for (MessagePanel messagePanel : listMessagePanel) {
			removeMessagePanel(messagePanel, false, -1);
		}
	}

	/**
	 * Create and add an {@link MessagePanel}.
	 *
	 * @param info : info of message
	 * @param visiblePersistent true to set it persistent, false otherwise.
	 * @param x The x value of X_Axis
	 * @param y The: y value of Y_Axis
	 * @param currSpec The index of the spectrum.
	 * @return the added {@link MessagePanel}.
	 */
	public MessagePanel addMessagePanel(String info, boolean visiblePersistent, int x, int y,
			int currSpec) {
		if (!allowNewImage) {
			return null;
		}
		// Create a new one
		MessageModel model = new MessageModel(info, currSpec);
		MessagePanel image = new MessagePanel(this, model, visiblePersistent);

		int newx = x;
		int newy = y;
		Dimension imgDim = image.getMyBufferedImageSize();
		Dimension layDim = layeredPane.getSize();
		if (x + imgDim.getWidth() >= layDim.getWidth()) {
			newx = (int) (layDim.getWidth() - imgDim.getWidth());
			newx = newx < 0 ? 0 : newx;
		}
		if (y + imgDim.getHeight() >= layDim.getHeight()) {
			newy = (int) (layDim.getHeight() - imgDim.getHeight());
			newy = newy < 0 ? 0 : newy;
		}
		image.setLocation(newx, newy);

		images.add(image);

		// Add into the layeredPane and repaint it
		layeredPane.add(image, 0);//???
		repaintCat(currSpec);
		return image;
	}

	/**
	 * Remove the provided {@link MessagePanel}.
	 *
	 * @param msgPanel The {@link MessagePanel}.
	 * @param repaint true if we should repaint, false otherwise.
	 * @param currSpec The concerned spectrum id.
	 */
	public void removeMessagePanel(MessagePanel msgPanel, boolean repaint, int currSpec) {
		images.remove(msgPanel);
		layeredPane.remove(msgPanel);
		if (repaint) {
			repaintCat(currSpec);
		}
	}

	/**
	 * Return the layered pane.
	 *
	 * @return the layered pane.
	 */
	public JLayeredPane getLayeredPane() {
		return layeredPane;
	}

	public void repaintCat(int cat) {
		if (layeredPane instanceof MyLayeredPane) {
			((MyLayeredPane) layeredPane).repaintCat(cat);
		} else {
			layeredPane.repaint();
		}
	}

	public List<MessagePanel> getImages() {
		return images;
	}

	public void setAllowNewImage(boolean value) {
		this.allowNewImage = value;
	}
}
