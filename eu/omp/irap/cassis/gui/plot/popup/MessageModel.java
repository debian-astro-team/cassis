/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.popup;

import java.awt.Color;
import java.net.URL;

import javax.swing.text.AbstractDocument;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

/**
 * Class who contains information of Message.
 *
 * @author thachtn
 */
public class MessageModel {

	public static final int NO_SPECIAL_TYPE = 0;
	public static final int ROTATIONAL_FIT_TYPE = 1;
	public static final String DEFAULT_FONT_NAME = "Serif";
	public static final int DEFAULT_FONT_SIZE = 14;
	private String message;
	private int currSpec;
	private double currentAngle = 0;
	private int zoom = 100;
	private boolean visiblePersitent = false;
	private AbstractDocument doc = null;
	private Color foreGroundColor = Color.BLACK;
	private Color bgColor = Color.WHITE;
	private float transparency = 1.0f;
	private int fontSize = DEFAULT_FONT_SIZE;
	private String fontname = DEFAULT_FONT_NAME;
	private boolean bold = false;
	private boolean italic = false;
	private boolean underline = false;
	private boolean allowEdit = true;
	private int type;
	private URL referencesUrl = null;


	public MessageModel(String message, int currSpec) {
		this.message = message;
		this.currSpec = currSpec;
		this.type = NO_SPECIAL_TYPE;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message : the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the currentAngle
	 */
	public double getCurrentAngle() {
		return currentAngle;
	}

	/**
	 * @param currentAngle : the currentAngle to set
	 */
	public void setCurrentAngle(double currentAngle) {
		this.currentAngle = currentAngle;
	}

	/**
	 * @return the zoom
	 */
	public int getZoom() {
		return zoom;
	}

	/**
	 * @return the visiblePersitent
	 */
	public boolean isVisiblePersitent() {
		return visiblePersitent;
	}

	/**
	 * @return the doc
	 */
	public AbstractDocument getDoc() {
		return doc;
	}

	/**
	 * @return the color
	 */
	public Color getColor() {
		return foreGroundColor;
	}

	/**
	 * @return the fontSize
	 */
	public int getFontSize() {
		return fontSize;
	}

	/**
	 * @return the fontName
	 */
	public String getFontname() {
		return fontname;
	}

	/**
	 * @param zoom : the zoom to set
	 */
	public void setZoom(int zoom) {
		this.zoom = zoom;
	}

	/**
	 * @param visiblePersitent : the visiblePersitent to set
	 */
	public void setVisiblePersitent(boolean visiblePersitent) {
		this.visiblePersitent = visiblePersitent;
	}

	/**
	 * @param doc : the doc to set
	 */
	public void setDoc(AbstractDocument doc) {
		this.doc = doc;
	}

	/**
	 * @param color : the color to set
	 */
	public void setColor(Color color) {
		this.foreGroundColor = color;
		if (doc != null) {
			SimpleAttributeSet attr = new SimpleAttributeSet();
			StyleConstants.setForeground(attr, color);
			((StyledDocument) doc).setCharacterAttributes(0, message.length(), attr, false);
		}
	}

	/**
	 * @return the bgColor
	 */
	public Color getBgColor() {
		return bgColor;
	}

	/**
	 * @param bgColor : the bgColor to set
	 */
	public void setBgColor(Color bgColor) {
		this.bgColor = bgColor;
	}

	/**
	 * @param fontSize : the fontSize to set
	 */
	public void setFontSize(int fontSize) {
		this.fontSize = fontSize;
		if (doc != null) {
			SimpleAttributeSet attr = new SimpleAttributeSet();
			StyleConstants.setFontSize(attr, fontSize);
			((StyledDocument) doc).setCharacterAttributes(0, message.length(), attr, false);
		}
	}

	/**
	 * @param fontname : the font name to set
	 */
	public void setFontname(String fontname) {
		this.fontname = fontname;
		if (doc != null) {
			SimpleAttributeSet attr = new SimpleAttributeSet();
			StyleConstants.setFontFamily(attr, fontname);
			((StyledDocument) doc).setCharacterAttributes(0, message.length(), attr, false);
		}
	}

	/**
	 * @return the bold
	 */
	public boolean isBold() {
		return bold;
	}

	/**
	 * @return the italic
	 */
	public boolean isItalic() {
		return italic;
	}

	/**
	 * @param bold : the bold to set
	 */
	public void setBold(boolean bold) {
		this.bold = bold;
		if (doc != null) {
			SimpleAttributeSet attr = new SimpleAttributeSet();
			StyleConstants.setBold(attr, bold);
			((StyledDocument) doc).setCharacterAttributes(0, message.length(), attr, false);
		}
	}

	/**
	 * @param italic : the italic to set
	 */
	public void setItalic(boolean italic) {
		this.italic = italic;
		if (doc != null) {
			SimpleAttributeSet attr = new SimpleAttributeSet();
			StyleConstants.setItalic(attr, italic);
			((StyledDocument) doc).setCharacterAttributes(0, message.length(), attr, false);
		}
	}

	public int getCurrentSpectrum() {
		return currSpec;
	}

	public float getTransparency() {
		return transparency;
	}

	public void setTransparency(float value) {
		transparency = value;
	}

	public void setAllowEdit(boolean value) {
		allowEdit = value;
	}

	public boolean isEditAllowed() {
		return allowEdit;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getType() {
		return type;
	}

	public boolean isUnderline() {
		return underline;
	}

	public void setUnderline(boolean value) {
		this.underline = value;
		if (doc != null) {
			SimpleAttributeSet attr = new SimpleAttributeSet();
			StyleConstants.setUnderline(attr, underline);
			((StyledDocument) doc).setCharacterAttributes(0, message.length(), attr, false);
		}
	}

	public boolean haveURL() {
		return (referencesUrl != null);
	}

	public URL getReferencesUrl() {
		return referencesUrl;
	}

	public void setReferencesUrl(URL url) {
		this.referencesUrl = url;
	}
}
