/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.selection;

import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

@SuppressWarnings("serial")
//TODO panel not yet used to display species only in data selected by the user
//Can be also used for the FitPanel
public class SelectionPanel extends JPanel {

	private JCheckBox displaySelection;
	private JButton resetLastSelectionButton;
	private JButton resetAllSelectionsButton;

	public SelectionPanel() {
		super();
		this.setLayout(new GridLayout(1, 3, 4, 0));
		this.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				"Selections [with middle-click-and-drag]"));

		displaySelection = new JCheckBox();
		displaySelection.setText("Display");
		displaySelection.setSelected(true);
		displaySelection.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// fireDisplaySelectionClicked();
			}
		});
		this.add(displaySelection);

		resetLastSelectionButton = new JButton("Reset last");
		resetLastSelectionButton.setMargin(new Insets(0, 0, 0, 0));
		this.add(resetLastSelectionButton);
		resetLastSelectionButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// fireResetLastSelectionClicked();
			}
		});
		resetAllSelectionsButton = new JButton("Reset all");
		ActionListener resetListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// fireResetAllSelectionClicked();
			}
		};
		resetAllSelectionsButton.addActionListener(resetListener);
		this.add(resetAllSelectionsButton);
	}

}
