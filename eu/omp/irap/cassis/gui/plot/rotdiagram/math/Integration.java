/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 *   Class Integration
 *       interface IntegralFunction also required
 *
 *   Contains the methods for Gaussian-Legendre quadrature, the
 *   backward and forward rectangular rules and the trapezium rule
 *
 *   The function to be integrated is supplied by means of
 *       an interface, IntegralFunction
 *
 *   WRITTEN BY: Dr Michael Thomas Flanagan
 *
 *   DATE:	 February 2002
 *   UPDATE:  22 June 2003, 16 July 2006, 25 April 2007, 2 May 2007, 4 July 2008, 22 September 2008
 *
 *   DOCUMENTATION:
 *   See Michael Thomas Flanagan's Java library on-line web page:
 *   http://www.ee.ucl.ac.uk/~mflanaga/java/Integration.html
 *   http://www.ee.ucl.ac.uk/~mflanaga/java/
 *
 *   Copyright (c) 2002 - 2008 Michael Thomas Flanagan
 *
 * PERMISSION TO COPY:
 *
 * Permission to use, copy and modify this software and its documentation for NON-COMMERCIAL purposes is granted, without fee,
 * provided that an acknowledgement to the author, Dr Michael Thomas Flanagan at www.ee.ucl.ac.uk/~mflanaga, appears in all copies
 * and associated documentation or publications.
 *
 * Redistributions of the source code of this source code, or parts of the source codes, must retain the above copyright notice, this list of conditions
 * and the following disclaimer and requires written permission from the Michael Thomas Flanagan:
 *
 * Redistribution in binary form of all or parts of this class must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution and requires written permission from the Michael Thomas Flanagan:
 *
 * Dr Michael Thomas Flanagan makes no representations about the suitability or fitness of the software for any or for a particular purpose.
 * Dr Michael Thomas Flanagan shall not be liable for any damages suffered as a result of using, modifying or distributing this software
 * or its derivatives.
 *
 ***************************************************************************************/

package eu.omp.irap.cassis.gui.plot.rotdiagram.math;

import java.util.ArrayList;

// Numerical integration class
public class Integration {

	private CrigFunct integralFunc = null; // Function to be integrated
	private boolean setFunction = false; // = true when IntegralFunction set
	private double lowerLimit = Double.NaN; // Lower integration limit
	private double upperLimit = Double.NaN; // Upper integration limit
	private boolean setLimits = false; // = true when limits set

	private int glPoints = 0; // Number of points in the Gauss-Legendre
								// integration
	private boolean setGLpoints = false; // = true when glPoints set
	private double integralSum = 0.0D; // Sum returned by the numerical
										// integration method

	// ArrayLists to hold Gauss-Legendre Coefficients saving repeated
	// calculation
	private static ArrayList<Integer> gaussQuadIndex = new ArrayList<>(); // Gauss-Legendre
																					// indices
	private static ArrayList<double[]> gaussQuadDistArrayList = new ArrayList<>(); // Gauss-Legendre
																							// distances
	private static ArrayList<double[]> gaussQuadWeightArrayList = new ArrayList<>();// Gauss-Legendre
																							// weights

	// CONSTRUCTORS

	// Default constructor
	public Integration() {
	}

	// Constructor taking function to be integrated
	public Integration(CrigFunct intFunc) {
		this.integralFunc = intFunc;
		this.setFunction = true;
	}

	// Set limits
	void setLimits(double lowerLimit, double upperLimit) {
		this.lowerLimit = lowerLimit;
		this.upperLimit = upperLimit;
		this.setLimits = true;
	}

	// GAUSSIAN-LEGENDRE QUADRATURE

	// Numerical integration using n point Gaussian-Legendre quadrature
	// (instance method)
	// All parametes preset
	private double gaussQuad() {
		if (!this.setGLpoints)
			throw new IllegalArgumentException("Number of points not set");
		if (!this.setLimits)
			throw new IllegalArgumentException("One limit or both limits not set");
		if (!this.setFunction)
			throw new IllegalArgumentException("No integral function has been set");

		double[] gaussQuadDist = new double[glPoints];
		double[] gaussQuadWeight = new double[glPoints];
		double sum = 0.0D;
		double xplus = 0.5D * (upperLimit + lowerLimit);
		double xminus = 0.5D * (upperLimit - lowerLimit);
		double dx = 0.0D;
		boolean test = true;
		int k = -1;
		int kn = -1;

		// Get Gauss-Legendre coefficients, i.e. the weights and scaled
		// distances
		// Check if coefficients have been already calculated on an earlier call
		if (!Integration.gaussQuadIndex.isEmpty()) {
			for (k = 0; k < Integration.gaussQuadIndex.size(); k++) {
				Integer ki = Integration.gaussQuadIndex.get(k);
				if (ki.intValue() == this.glPoints) {
					test = false;
					kn = k;
				}
			}
		}

		if (test) {
			// Calculate and store coefficients
			Integration.gaussQuadCoeff(gaussQuadDist, gaussQuadWeight, glPoints);
			Integration.gaussQuadIndex.add(Integer.valueOf(glPoints));
			Integration.gaussQuadDistArrayList.add(gaussQuadDist);
			Integration.gaussQuadWeightArrayList.add(gaussQuadWeight);
		} else {
			// Recover coefficients
			gaussQuadDist = gaussQuadDistArrayList.get(kn);
			gaussQuadWeight = gaussQuadWeightArrayList.get(kn);
		}

		// Perform summation
		for (int i = 0; i < glPoints; i++) {
			dx = xminus * gaussQuadDist[i];
			sum += gaussQuadWeight[i] * this.integralFunc.function(xplus + dx);
		}
		this.integralSum = sum * xminus; // rescale
		return this.integralSum; // return value
	}

	// Numerical integration using n point Gaussian-Legendre quadrature
	// (instance method)
	// All parametes except the number of points in the Gauss-Legendre
	// integration preset
	double gaussQuad(int glPoints) {
		this.glPoints = glPoints;
		this.setGLpoints = true;
		return this.gaussQuad();
	}

	// Returns the distance (gaussQuadDist) and weight coefficients
	// (gaussQuadCoeff)
	// for an n point Gauss-Legendre Quadrature.
	// The Gauss-Legendre distances, gaussQuadDist, are scaled to -1 to 1
	// See Numerical Recipes for details
	private static void gaussQuadCoeff(double[] gaussQuadDist, double[] gaussQuadWeight, int n) {
		double z = 0.0D;
		double z1 = 0.0D;
		double pp = 0.0D;
		double p1 = 0.0D;
		double p2 = 0.0D;
		double p3 = 0.0D;

		double eps = 3e-11; // set required precision
		double x1 = -1.0D; // lower limit
		double x2 = 1.0D; // upper limit

		// Calculate roots
		// Roots are symmetrical - only half calculated
		int m = (n + 1) / 2;
		double xm = 0.5D * (x2 + x1);
		double xl = 0.5D * (x2 - x1);

		// Loop for each root
		for (int i = 1; i <= m; i++) {
			// Approximation of ith root
			z = Math.cos(Math.PI * (i - 0.25D) / (n + 0.5D));

			// Refinement on above using Newton's method
			do {
				p1 = 1.0D;
				p2 = 0.0D;

				// Legendre polynomial (p1, evaluated at z, p2 is polynomial of
				// one order lower) recurrence relationsip
				for (int j = 1; j <= n; j++) {
					p3 = p2;
					p2 = p1;
					p1 = ((2.0D * j - 1.0D) * z * p2 - (j - 1.0D) * p3) / j;
				}
				pp = n * (z * p1 - p2) / (z * z - 1.0D); // Derivative of p1
				z1 = z;
				z = z1 - p1 / pp; // Newton's method
			} while (Math.abs(z - z1) > eps);

			gaussQuadDist[i - 1] = xm - xl * z; // Scale root to desired
												// interval
			gaussQuadDist[n - i] = xm + xl * z; // Symmetric counterpart
			gaussQuadWeight[i - 1] = 2.0 * xl / ((1.0 - z * z) * pp * pp); // Compute
																			// weight
			gaussQuadWeight[n - i] = gaussQuadWeight[i - 1]; // Symmetric
																// counterpart
		}
	}
}
