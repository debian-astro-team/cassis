/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
*   Class   RotMath
*
*   Part of class Stat
*
*   USAGE:  Statistical functions
*
*   WRITTEN BY: Dr Michael Thomas Flanagan
*
*   DATE:    June 2002 as part of Fmath
*   AMENDED: 12 May 2003 Statistics separated out from Fmath as a new class
*   DATE:    18 June 2005, 5 January 2006, 25 April 2006, 12, 21 November 2006
*            4 December 2006 (renaming of cfd and pdf methods - older version also retained)
*            31 December 2006, March 2007, 14 April 2007, 19 October 2007, 27 February 2008
*            29 March 2008, 7 April 2008, 29 April 2008 - 13 May 2008, 22-31 May 2008,
*            4-10 June 2008, 27 June 2008, 2-5 July 2008, 23 July 2008, 31 July 2008,
*            2-4 August 2008,  20 August 2008, 5-10 September 2008, 19 September 2008,
*            28-30 September 2008 (probability Plot moved to separate class, ProbabilityPlot)
*            4-5 October 2008,  8-13 December 2008, 14 June 2009, 13-23 October 2009,
*            8 February 2010, 18-25 May 2010, 2 November 2010, 4 December 2010, 19-25 January 2011
*
*   DOCUMENTATION:
*   See Michael Thomas Flanagan's Java library on-line web page:
*   http://www.ee.ucl.ac.uk/~mflanaga/java/Stat.html
*   http://www.ee.ucl.ac.uk/~mflanaga/java/
*
*   Copyright (c) 2002 - 2011 Michael Thomas Flanagan
*
*   PERMISSION TO COPY:
*
*   Permission to use, copy and modify this software and its documentation for NON-COMMERCIAL purposes is granted, without fee,
*   provided that an acknowledgement to the author, Dr Michael Thomas Flanagan at www.ee.ucl.ac.uk/~mflanaga, appears in all copies
*   and associated documentation or publications.
*
*   Redistributions of the source code of this source code, or parts of the source codes, must retain the above copyright notice,
+   this list of conditions and the following disclaimer and requires written permission from the Michael Thomas Flanagan:
*
*   Redistribution in binary form of all or parts of this class must reproduce the above copyright notice, this list of conditions and
*   the following disclaimer in the documentation and/or other materials provided with the distribution and requires written permission
*   from the Michael Thomas Flanagan:
*
*   Dr Michael Thomas Flanagan makes no representations about the suitability or fitness of the software for any or for a particular purpose.
*   Dr Michael Thomas Flanagan shall not be liable for any damages suffered as a result of using, modifying or distributing this software
*   or its derivatives.
*
***************************************************************************************/


package eu.omp.irap.cassis.gui.plot.rotdiagram.math;


public class RotMath {

    private static boolean igSupress = false;
    //  Maximum number of iterations allowed in Incomplete Gamma Function calculations
    private static int igfiter = 1000;
    //  Tolerance used in terminating series in Incomplete Gamma Function calculations
    private static double igfeps = 1e-8;

    // A small number close to the smallest representable floating point number
    public static final double FPMIN = 1e-300;

    //  Lanczos Gamma Function approximation - N (number of coefficients -1)
    private static int lgfN = 6;
    //  Lanczos Gamma Function approximation - Coefficients
    private static double[] lgfCoeff = {1.000000000190015, 76.18009172947146, -86.50532032941677, 24.01409824083091, -1.231739572450155, 0.1208650973866179E-2, -0.5395239384953E-5};
    //  Lanczos Gamma Function approximation - small gamma
    private static double lgfGamma = 5.0;

	public RotMath() {

	}

	 // CHI SQUARE DISTRIBUTION AND CHI SQUARE FUNCTIONS

    // Chi-Square Cumulative Distribution Function
    // probability that an observed chi-square value for a correct model should be less than chiSquare
    // nu  =  the degrees of freedom
    public static double chiSquareCDF(double chiSquare, int nu){
            if(nu<=0)
                throw new IllegalArgumentException("The degrees of freedom [nu], " + nu + ", must be greater than zero");
            return incompleteGamma((double)nu/2.0D, chiSquare/2.0D);
    }

    // Regularised Incomplete Gamma Function P(a,x) = integral from zero to x of (exp(-t)t^(a-1))dt
    // Retained for backward compatibility
    private static double incompleteGamma(double a, double x){
        return regularisedGammaFunction(a, x);
    }

    // Regularised Incomplete Gamma Function P(a,x) = integral from zero to x of (exp(-t)t^(a-1))dt
    private static double regularisedGammaFunction(double a, double x){
            if(a<0.0D  || x<0.0D)
                throw new IllegalArgumentException("\nFunction defined only for a >= 0 and x>=0");

            igSupress = true;
            double igf = 0.0D;

            if(x!=0){
                if(x < a+1.0D){
                    // Series representation
                    igf = incompleteGammaSer(a, x);
                }
                else{
                    // Continued fraction representation
                    igf = incompleteGammaFract(a, x);
                }
                if(igf!=igf)
                    igf = 1.0 - crigfGaussQuad(a, x);
            }
            if(igf<0.0)
                igf = 0.0;
            igSupress = false;
            return igf;
    }

    // Regularised Incomplete Gamma Function P(a,x) = integral from zero to x of (exp(-t)t^(a-1))dt
    // Series representation of the function - valid for x < a + 1
    private static double incompleteGammaSer(double a, double x){
            if(a<0.0D  || x<0.0D)
                throw new IllegalArgumentException("\nFunction defined only for a >= 0 and x>=0");
            if(x>=a+1)
                throw new IllegalArgumentException("\nx >= a+1   use Continued Fraction Representation");

            double igf = 0.0D;

            if(x!=0.0D){

                int i = 0;
                boolean check = true;

                double acopy = a;
                double sum = 1.0/a;
                double incr = sum;
                double loggamma = logGamma(a);

                while(check){
                    ++i;
                    ++a;
                    incr *= x/a;
                    sum += incr;
                    if(Math.abs(incr) < Math.abs(sum)*igfeps){
                            igf = sum*Math.exp(-x+acopy*Math.log(x)- loggamma);
                            check = false;
                    }
                    if(i>=igfiter){
                            check=false;
                            igf = Double.NaN;
                            if(!igSupress){
                                System.out.println("\nMaximum number of iterations were exceeded in Stat.incompleteGammaSer().");
                                System.out.println("NaN returned.\nIncrement = "+String.valueOf(incr)+".");
                                System.out.println("Sum = "+String.valueOf(sum)+".\nTolerance =  "+String.valueOf(igfeps));
                            }
                    }
                }
            }

            return igf;
    }

    // Guassian quadrature estimation of the complementary regularised incomplete gamma function
    private static double crigfGaussQuad(double a, double x){
        double sum = 0.0;

        // set increment details
        double upper = 100.0*a;
        double range = upper - x;
        double incr = 0;
        if(upper>x && range >100){
            incr = range/1000;
        }
        else{
            upper = x + 100.0;
            range = 100.0;
            incr = 0.1;
        }
        int nIncr = (int)Math.round(range/incr);
        incr = range/nIncr;

        // Instantiate integration function
        CrigFunct f1 = new CrigFunct();
        f1.setA(a);
        f1.setB(logGammaFunction(a));

        // Instantiate Integration
        Integration intgn1 = new Integration(f1);
        double xx = x;
        double yy = x + incr;
        intgn1.setLimits(xx, yy);

        // Perform quadrature
        sum = intgn1.gaussQuad(64);
        for(int i=1; i<nIncr; i++){
            xx = yy;
            yy = xx + incr;
            intgn1.setLimits(xx, yy);
            sum += intgn1.gaussQuad(64);
        }
        return sum;
    }
 // Regularised Incomplete Gamma Function P(a,x) = integral from zero to x of (exp(-t)t^(a-1))dt
    // Continued Fraction representation of the function - valid for x >= a + 1
    // This method follows the general procedure used in Numerical Recipes for C,
    // The Art of Scientific Computing
    // by W H Press, S A Teukolsky, W T Vetterling & B P Flannery
    // Cambridge University Press,   http://www.nr.com/
    private static double incompleteGammaFract(double a, double x){
            if(a<0.0D  || x<0.0D)
                throw new IllegalArgumentException("\nFunction defined only for a >= 0 and x>=0");
            if(x<a+1)
                throw new IllegalArgumentException("\nx < a+1   Use Series Representation");

            double igf = 0.0D;

            if(x!=0.0D){

                int i = 0;
                double ii = 0;
                boolean check = true;

                double loggamma = logGamma(a);
                double numer = 0.0D;
                double incr = 0.0D;
                double denom = x - a + 1.0D;
                double first = 1.0D/denom;
                double term = 1.0D/FPMIN;
                double prod = first;

                while(check){
                    ++i;
                    ii = (double)i;
                    numer = -ii*(ii - a);
                    denom += 2.0D;
                    first = numer*first + denom;
                    if(Math.abs(first) < FPMIN){
                        first = FPMIN;
                    }
                    term = denom + numer/term;
                    if(Math.abs(term) < FPMIN){
                        term = FPMIN;
                     }
                    first = 1.0D/first;
                    incr = first*term;
                    prod *= incr;
                    if(Math.abs(incr - 1.0D) < igfeps)
                        check = false;
                    if(i>=igfiter){
                        check=false;
                        igf = Double.NaN;
                        if(!igSupress){
                            System.out.println("\nMaximum number of iterations were exceeded in Stat.incompleteGammaFract().");
                            System.out.println("NaN returned.\nIncrement - 1 = "+String.valueOf(incr-1)+".");
                            System.out.println("Tolerance =  "+String.valueOf(igfeps));
                        }
                    }
                }
                igf = 1.0D - Math.exp(-x+a*Math.log(x)-loggamma)*prod;
            }

            return igf;
    }

    // log to base e of the Gamma function
    // Lanczos approximation (6 terms)
    private static double logGammaFunction(double x){
            double xcopy = x;
            double fg = 0.0D;
            double first = x + lgfGamma + 0.5;
            double second = lgfCoeff[0];

            if(x>=0.0){
                    first -= (x + 0.5)*Math.log(first);
                    for(int i=1; i<=lgfN; i++)
                        second += lgfCoeff[i]/++xcopy;
                    fg = Math.log(Math.sqrt(2.0*Math.PI)*second/x) - first;
            }
            else{
                    fg = Math.PI/(gamma(1.0D-x)*Math.sin(Math.PI*x));

                    if(fg!=1.0/0.0 && fg!=-1.0/0.0){
                            if(fg<0){
                                     throw new IllegalArgumentException("\nThe gamma function is negative");
                            }
                            else{
                                    fg = Math.log(fg);
                            }
                    }
            }
            return fg;
    }

    // log to base e of the Gamma function
    // Lanczos approximation (6 terms)
    // Retained for backward compatibility
    private static double logGamma(double x){
            double xcopy = x;
            double fg = 0.0D;
            double first = x + lgfGamma + 0.5;
            double second = lgfCoeff[0];

            if(x>=0.0){
                    first -= (x + 0.5)*Math.log(first);
                    for(int i=1; i<=lgfN; i++)
                        second += lgfCoeff[i]/++xcopy;
                    fg = Math.log(Math.sqrt(2.0*Math.PI)*second/x) - first;
            }
            else{
                    fg = Math.PI/(gamma(1.0D-x)*Math.sin(Math.PI*x));

                    if(fg!=1.0/0.0 && fg!=-1.0/0.0){
                            if(fg<0){
                                     throw new IllegalArgumentException("\nThe gamma function is negative");
                            }
                            else{
                                    fg = Math.log(fg);
                            }
                    }
            }
            return fg;
    }

 // Gamma function
    // Lanczos approximation (6 terms)
    // retained for backward compatibity
    private static double gamma(double x){

            double xcopy = x;
            double first = x + lgfGamma + 0.5;
            double second = lgfCoeff[0];
            double fg = 0.0D;

            if(x>=0.0){
                    first = Math.pow(first, x + 0.5)*Math.exp(-first);
                    for(int i=1; i<=lgfN; i++)
                        second += lgfCoeff[i]/++xcopy;
                    fg = first*Math.sqrt(2.0*Math.PI)*second/x;
            }
            else{
                     fg = -Math.PI/(x*gamma(-x)*Math.sin(Math.PI*x));
            }
            return fg;
    }


}
