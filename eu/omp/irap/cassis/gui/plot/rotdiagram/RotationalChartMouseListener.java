/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import javax.swing.SwingUtilities;

import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.event.ChartProgressEvent;
import org.jfree.chart.event.ChartProgressListener;
import org.jfree.chart.plot.XYPlot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.gui.plot.popup.MessageControl;
import eu.omp.irap.cassis.gui.plot.util.LineInfoPopup;

/**
 * @author glorian
 *
 */
public class RotationalChartMouseListener extends MouseAdapter implements
		ChartProgressListener, ChartMouseListener, MouseMotionListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(RotationalChartMouseListener.class);

	private boolean isControlled = false;
	// Variables to save the upper X value and the upper Y value
	// in a particular instant
	private double upperX;

	private double upperY;
	// coordinate x of the click of the mouse
	private double mouseX;

	// coordinate y of the click of the mouse
	private double mouseY;

	private MouseEvent mouse;

	private MessageControl messageControl;

	private ChartPanel chartPanel;
	private RotationalModel model;
	private RotationalView view;
	private Rectangle2D.Double zoomRectangle;


	public RotationalChartMouseListener(RotationalView view, RotationalModel model) {
		super();
		this.chartPanel = view.getChartPanel();
		this.view = view;
		this.model = model;
		this.messageControl = new MessageControl(view.getLayeredPane());
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (SwingUtilities.isMiddleMouseButton(e) ||
				(SwingUtilities.isLeftMouseButton(e) && e.isAltDown())) {
			if (!model.getRotationalFitModel().isMultiSelectionAllowed()) {
				view.getRotationalFitView().showMessageNoMultiSelectionAllowed();
				return;
			}
			XYPlot plot = (XYPlot) this.chartPanel.getChart().getPlot();
			// zoom not operational
			chartPanel.setFillZoomRectangle(true);
			chartPanel.setMouseZoomable(false);

			// declaration of the coordinates of the first click
			ValueAxis xAxis = plot.getDomainAxis();
			ValueAxis yAxis = plot.getRangeAxis();
			mouseX = xAxis.java2DToValue(e.getX(), chartPanel.getScreenDataArea(), plot.getDomainAxisEdge());
			mouseY = yAxis.java2DToValue(e.getY(), chartPanel.getScreenDataArea(), plot.getRangeAxisEdge());

			view.getRotationalFitView().mouseBeginRectangleSelection();
		}
		else if (SwingUtilities.isLeftMouseButton(e)) {
			messageControl.refreshPopups(0);
			// zoom operational
			chartPanel.setMouseZoomable(true);
			chartPanel.setFillZoomRectangle(false);
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (SwingUtilities.isMiddleMouseButton(e) ||
				(SwingUtilities.isLeftMouseButton(e) && e.isAltDown())) {
			if (!model.getRotationalFitModel().isMultiSelectionAllowed()) {
				return;
			}
			XYPlot plot = (XYPlot) this.chartPanel.getChart().getPlot();
			// zoom not operational
			chartPanel.setFillZoomRectangle(true);
			chartPanel.setMouseZoomable(false);

			// declaration of the coordinates of the second click
			ValueAxis xAxis = plot.getDomainAxis();
			ValueAxis yAxis = plot.getRangeAxis();
			double eventX = xAxis.java2DToValue(e.getX(), chartPanel.getScreenDataArea(), plot.getDomainAxisEdge());
			double eventY = yAxis.java2DToValue(e.getY(), chartPanel.getScreenDataArea(), plot.getRangeAxisEdge());
			// if the first mouse click is superior than the second mouse click
			// x1<=>x2 and y1<=>y2
			if (eventX < mouseX) {
				double tmpX = eventX;
				eventX = mouseX;
				mouseX = tmpX;
			}
			if (eventY < mouseY) {
				double tmpY = eventY;
				eventY = mouseY;
				mouseY = tmpY;
			}

			view.getRotationalFitView().mouseEndRectangleFitReleased(mouseX, mouseY, eventX, eventY);
			if (zoomRectangle != null) {
				Graphics2D g2 = (Graphics2D) chartPanel.getGraphics();
				g2.setPaint(Color.BLUE);
				g2.draw(zoomRectangle);
				chartPanel.paintComponent(g2);
			}
		}
	}

	/**
	 * Intercept the changes in the progression of the chart layout.
	 *
	 * @param event
	 *            ChartProgressEvent
	 */
	@Override
	public void chartProgress(ChartProgressEvent event) {
		XYPlot plot = (XYPlot) this.chartPanel.getChart().getPlot();
		/* Display popup information when we click on the chart */
		if (event.getType() == ChartProgressEvent.DRAWING_FINISHED && isControlled) {
			double xValue = plot.getDomainCrosshairValue();
			double yValue = plot.getRangeCrosshairValue();
			if ((mouse.getModifiers() & InputEvent.BUTTON1_MASK) != 0)
				isControlled = false;

			// Only display popup if click is on the plot.
			int xScreen = mouse.getX();
			int yScreen = mouse.getY();
			Rectangle2D area = chartPanel.getScreenDataArea();
			if (area.contains(xScreen, yScreen)) {
				try {
					String info = model.getNearestLineInfo(xValue, yValue);
					if (!(info == null || "".equals(info))) {
						messageControl.addMessagePanel(info, false, xScreen, yScreen - 40, 0);
					}
				} catch (Exception e) {
					LOGGER.error("Error while initializating and displaying popup message", e);
				}
			}
		}

		if ((upperX != plot.getDomainAxis().getRange().getUpperBound() || upperY != plot.getRangeAxis().getRange()
				.getUpperBound())
				&& event.getType() == ChartProgressEvent.DRAWING_FINISHED) {
			upperX = plot.getDomainAxis().getRange().getUpperBound();
			upperY = plot.getRangeAxis().getRange().getUpperBound();
		}
		messageControl.repaintCat(0);
	}

	/**
	 * Intercept all click on the chart.
	 *
	 * @param event
	 *            ChartMouseEvent
	 */
	@Override
	public void chartMouseClicked(ChartMouseEvent event) {
		mouse = event.getTrigger();
		if (SwingUtilities.isLeftMouseButton(mouse)) {
			isControlled = true;
		}
	}

	@Override
	public void chartMouseMoved(ChartMouseEvent event) {
		mouse = event.getTrigger();
		XYPlot plot = chartPanel.getChart().getXYPlot();
		if (mouse.isShiftDown() && plot != null) {
			ValueAxis domainBottomAxis = plot.getDomainAxis();
			double mouseMoveX = domainBottomAxis
					.java2DToValue(mouse.getX(), chartPanel.getScreenDataArea(), plot.getDomainAxisEdge());

			ValueAxis yAxis = plot.getRangeAxis();
			double mouseMoveY = yAxis.java2DToValue(mouse.getY(), chartPanel.getScreenDataArea(), plot.getRangeAxisEdge());
			LineInfoPopup info = new LineInfoPopup();

			ArrayList<String> coord = new ArrayList<>();

			coord.add(plot.getDomainAxis().getLabel() + " = " + mouseMoveX);
			coord.add(plot.getRangeAxis().getLabel() + " = " +  mouseMoveY);

			info.display(mouse, coord);
		}
	}

	public void setUpperX(double upperBound) {
		upperX = upperBound;
	}

	public void setUpperY(double upperBound) {
		upperY = upperBound;
	}

	/**
	 * @see
	 * java.awt.event.MouseMotionListener#mouseDragged(java.awt.event.MouseEvent
	 * )
	 */
	@Override
	public void mouseDragged(MouseEvent e) {
		XYPlot plot = (XYPlot) this.chartPanel.getChart().getPlot();
		Graphics2D g2 = (Graphics2D) chartPanel.getGraphics();
		ValueAxis xAxis = plot.getDomainAxis();
		ValueAxis yAxis = plot.getRangeAxis();
		double eventX = xAxis.valueToJava2D(mouseX, chartPanel.getScreenDataArea(), plot.getDomainAxisEdge());
		double eventY = yAxis.valueToJava2D(mouseY, chartPanel.getScreenDataArea(), plot.getRangeAxisEdge());
		zoomRectangle = new Rectangle2D.Double(eventX, eventY, e.getX() - eventX, e.getY() - eventY);
		g2.setPaint(Color.BLUE);
		g2.draw(zoomRectangle);
		chartPanel.repaint();
	}

	public void removeAllMessages() {
		messageControl.removeAllMessages();
	}

	public MessageControl getMessageControl() {
		return messageControl;
	}
}
