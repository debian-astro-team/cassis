/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram.opacity;

import java.util.ArrayList;
import java.util.List;

/**
 * Simple class to list components in Opacity Correction who does not converge
 *  and create a message.
 *
 * @author M. Boiziot
 */
public class ConvergenceError {

	private final List<Element> errors;
	private final int iterations;


	/**
	 * Constructor.
	 *
	 * @param iterations Number of iterations during the OC.
	 */
	public ConvergenceError(int iterations) {
		this.errors = new ArrayList<>(4);
		this.iterations = iterations;
	}

	/**
	 * Return if there was a component who did not converge.
	 *
	 * @return true if there was a component who did not converge, false otherwise.
	 */
	public boolean hasError() {
		return !errors.isEmpty();
	}

	/**
	 * Add a component who did not converge.
	 *
	 * @param tag The tag of the component.
	 * @param compNumber The number of the component.
	 */
	public void addError(int tag, int compNumber) {
		errors.add(new Element(tag, compNumber));
	}

	/**
	 * Create and return the error message.
	 *
	 * @return the error message.
	 */
	public String getErrorMessage() {
		StringBuilder sb = new StringBuilder("No convergence in LTE within ");
		sb.append(iterations);
		sb.append(" iterations for:\n");

		for (Element e : errors) {
			sb.append("\t- Tag: ").append(e.getTag()).append(", Component ").append(e.getCompNumber()).append('\n');
		}

		return sb.toString();
	}



	/**
	 * Simple class describing a component (tag number and comp number).
	 *
	 * @author M. Boiziot
	 */
	private class Element {

		private int tag;
		private int compNumber;


		/**
		 * Constructor.
		 *
		 * @param tag The tag.
		 * @param compNumber The component number.
		 */
		private Element(int tag, int compNumber) {
			this.tag = tag;
			this.compNumber = compNumber;
		}

		/**
		 * Return the tag.
		 *
		 * @return the tag.
		 */
		public int getTag() {
			return tag;
		}

		/**
		 * Return the component number.
		 *
		 * @return the component number.
		 */
		public int getCompNumber() {
			return compNumber;
		}
	}
}
