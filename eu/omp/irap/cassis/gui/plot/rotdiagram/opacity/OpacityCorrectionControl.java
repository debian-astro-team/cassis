/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram.opacity;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.database.access.UnknowMoleculeException;

/**
 * Control for the Opacity Correction.
 *
 * @author M. Boiziot
 */
public class OpacityCorrectionControl implements ModelListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(OpacityCorrectionControl.class);

	private OpacityCorrectionModel model;
	private OpacityCorrectionView view;


	/**
	 * Constructor.
	 *
	 * @param view The view.
	 * @param model The model.
	 */
	public OpacityCorrectionControl(OpacityCorrectionView view,
			OpacityCorrectionModel model) {
		this.model = model;
		this.view = view;
		model.addModelListener(this);
	}

	/**
	 * Return the {@link OpacityCorrectionModel}.
	 *
	 * @return the model.
	 */
	public OpacityCorrectionModel getModel() {
		return model;
	}

	/**
	 * Return the {@link OpacityCorrectionView}.
	 *
	 * @return the view.
	 */
	public OpacityCorrectionView getView() {
		return view;
	}

	/**
	 * @see eu.omp.irap.cassis.common.events.ModelListener#dataChanged(eu.omp.irap.cassis.common.events.ModelChangedEvent)
	 */
	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (OpacityCorrectionModel.EPSILON_CHANGE_EVENT.equals(event.getSource())) {
			String epsilon = String.valueOf(model.getEpsilon());
			view.getEpsilonField().setText(epsilon);
		} else if (OpacityCorrectionModel.MAX_ITERATION_CHANGE_EVENT.equals(event.getSource())) {
			String maxIteration = String.valueOf(model.getMaxIteration());
			view.getIterationField().setText(maxIteration);
		} else if (OpacityCorrectionModel.NO_DATA_EVENT.equals(event.getSource())) {
			JOptionPane.showMessageDialog(view, "There is no data!",
					"No data!", JOptionPane.ERROR_MESSAGE);
		} else if (OpacityCorrectionModel.NO_GAUSSIAN_EVENT.equals(event.getSource())) {
			JOptionPane.showMessageDialog(view,
					"Opacity can only work with gaussian fit points.",
					"Opacity correction error", JOptionPane.ERROR_MESSAGE);
		} else if (OpacityCorrectionModel.DOES_NOT_CONVERGE_EVENT.equals(event.getSource())) {
			String message = (String) event.getValue();
			JOptionPane.showMessageDialog(view, message, "Opacity correction",
					JOptionPane.INFORMATION_MESSAGE);
		} else if (OpacityCorrectionModel.CHECK_SELECTION_BOX_EVENT.equals(event.getSource())){
			JOptionPane.showMessageDialog(view, "Warning: the opacity correction does not"
					+ " take into account the selection box(es) for the moment.\n"
					+ "The correction will be performed on all the points "
					+ "(that are not blended nor multiplets).",
					"Opacity correction",
					JOptionPane.WARNING_MESSAGE);
		} else if (OpacityCorrectionModel.CONTAINS_MULTIPLET_BLENDED_LINES_EVENT.equals(event.getSource())){
			JOptionPane.showMessageDialog(view, "Warning: the opacity correction does "
					+ "not take into account the blended lines nor the multiplets.",
					"Opacity correction",
					JOptionPane.WARNING_MESSAGE);
		} else if (OpacityCorrectionModel.CONTAINS_ONLY_MULTIPLET_BLENDED_LINES_EVENT.equals(event.getSource())){
			JOptionPane.showMessageDialog(view, "Warning: Your file only contains blended "
					+ "lines and/or multiplets, "
					+ "therefore the opacity correction cannot be performed.",
					"Opacity correction",
					JOptionPane.WARNING_MESSAGE);
		} else if (OpacityCorrectionModel.ERROR_EVENT.equals(event.getSource())) {
			JOptionPane.showMessageDialog(view, (String)event.getValue(),
					"Opacity correction", JOptionPane.WARNING_MESSAGE);
		}
	}

	/**
	 * Try to do the opacity correction correction and display an error message if needed.
	 */
	public void doOpacityCorrection() {
		try {
			if (model.checkConditions()){
				model.doOpacityCorrection();
			}
		} catch (UnknowMoleculeException ume) {
			LOGGER.error("A molecule (tag: {}) is unknown, stopping the operation", ume.getTag(), ume);
			JOptionPane.showMessageDialog(view, ume.getInterruptedMessage(),
					"Opacity Correction error", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Try to update model value of epsilon from the view epsilon field.
	 * Display an error message if there is an error and restore the original value.
	 */
	public void updateEpsilon() {
		double epsilonValue = Double.parseDouble(view.getEpsilonField().getText());
		try {
			model.setEpsilon(epsilonValue);
		} catch (IllegalArgumentException iae) {
			LOGGER.warn("Illegal epsilon value, it must be a number between 0 and 100", iae);
			view.getEpsilonField().setText(String.valueOf(model.getEpsilon()));
			JOptionPane.showMessageDialog(view, iae.getMessage(),
					"Invalid epsilon value", JOptionPane.ERROR_MESSAGE);
		}
	}
}
