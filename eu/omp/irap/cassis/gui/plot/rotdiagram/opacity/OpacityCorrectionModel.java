/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram.opacity;

import herschel.ia.numeric.Double1d;
import herschel.ia.numeric.toolbox.fit.Fitter;
import herschel.ia.numeric.toolbox.fit.PolynomialModel;

import java.awt.Color;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.Formula;
import eu.omp.irap.cassis.common.events.ListenerManager;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.database.access.AccessDataBase;
import eu.omp.irap.cassis.database.access.DataBaseConnection;
import eu.omp.irap.cassis.database.access.MoleculeDescriptionDB;
import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB;
import eu.omp.irap.cassis.database.access.UnknowMoleculeException;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalTypeCurve;
import eu.omp.irap.cassis.parameters.PointInformation;
import eu.omp.irap.cassis.parameters.RotationalDiagramComponentResult;
import eu.omp.irap.cassis.parameters.RotationalDiagramMoleculeResult;
import eu.omp.irap.cassis.parameters.RotationalDiagramResult;
import eu.omp.irap.cassis.parameters.RotationalSelectionData;

/**
 * Model of the Opacity Correction.
 *
 * @author M. Boiziot
 */
public class OpacityCorrectionModel extends ListenerManager {

	private static final Logger LOGGER = LoggerFactory.getLogger(OpacityCorrectionModel.class);

	public static final String CHECK_SELECTION_BOX_EVENT = "checkSelectionBoxEvent";
	public static final String CONTAINS_MULTIPLET_BLENDED_LINES_EVENT = "containsMultipletBlendedLinesEvent";
	public static final String CONTAINS_ONLY_MULTIPLET_BLENDED_LINES_EVENT = "containsOnlyMultipletBlendedLinesEvent";
	public static final String DOES_NOT_CONVERGE_EVENT = "doesNotConverge";
	public static final String EPSILON_CHANGE_EVENT = "epsilonChangeEvent";
	public static final String ERROR_EVENT = "error";
	public static final String MAX_ITERATION_CHANGE_EVENT = "maxIterationChangeEvent";
	public static final String NO_DATA_EVENT = "noDataEvent";
	public static final String NO_GAUSSIAN_EVENT = "noGaussianEvent";

	public static final double EPSILON_DEFAULT = 1;
	public static final int MAX_ITERATION_DEFAULT = 50;

	private static double maxTau = 100;
	private double epsilon;
	private int maxIteration;
	private OpacityInterface opacityInterface;


	/**
	 * Constructor.
	 *
	 * @param opacityInterface The {@link OpacityInterface}.
	 */
	public OpacityCorrectionModel(OpacityInterface opacityInterface) {
		this.epsilon = EPSILON_DEFAULT;
		this.maxIteration = MAX_ITERATION_DEFAULT;
		this.opacityInterface = opacityInterface;
	}

	/**
	 * Return the epsilon.
	 *
	 * @return the epsilon.
	 */
	public double getEpsilon() {
		return epsilon;
	}

	/**
	 * Change the value of epsilon (must be between 0 and 100).
	 *
	 * @param epsilon The new value of epsilon.
	 * @throws IllegalArgumentException if the value is not between 0 and 100.
	 */
	public void setEpsilon(double epsilon) throws IllegalArgumentException {
		if (epsilon < 0 || epsilon > 100) {
			throw new IllegalArgumentException(
					"The value of epsilon is a percentage and must be between 0 and 100.");
		}
		this.epsilon = epsilon;
		fireDataChanged(new ModelChangedEvent(EPSILON_CHANGE_EVENT));
	}

	/**
	 * Return the maximum number of iterations.
	 *
	 * @return the maximum number of iterations.
	 */
	public int getMaxIteration() {
		return maxIteration;
	}

	/**
	 * Change the maximum number of iterations.
	 *
	 * @param maxIteration The new maximum number of iterations.
	 */
	public void setMaxIteration(int maxIteration) {
		this.maxIteration = maxIteration;
		fireDataChanged(new ModelChangedEvent(MAX_ITERATION_CHANGE_EVENT));
	}

	/**
	 * Do an opacity correction with the current parameters.
	 *
	 * @throws UnknowMoleculeException if a species is not in the database.
	 */
	public void doOpacityCorrection() throws UnknowMoleculeException {
		RotationalDiagramResult res = opacityInterface.getCurrentResult();
		if (!isOpacityCorrectionPossible(res)) {
			return;
		}
		opacityInterface.removeOpacityCorrection();
		DataBaseConnection db = AccessDataBase.getDataBaseConnection();
		MoleculeDescriptionDB molecule;
		ConvergenceError ce = new ConvergenceError(maxIteration);
		for (RotationalDiagramMoleculeResult mr : res.getResult()) {
			molecule = db.getMoleculeDescriptionDB(new SimpleMoleculeDescriptionDB(mr.getTag(), null));
			for (RotationalDiagramComponentResult cr : mr.getComponentResultsList()) {
				if (cr.getType() == RotationalSelectionData.GAUSSIAN_TYPE_DATA) {
					System.out.println("Species: " + mr.getMolName() + "; Component " + cr.getComponentNumber());
					try {
						List<PointInformation> dataOc = OpacityCorrectionModel.opacityCorrection(
								cr.getResult(), molecule, epsilon, maxIteration);

						if (dataOc.isEmpty()) {
							ce.addError(mr.getTag(), cr.getComponentNumber());
						} else {
							Color color = opacityInterface.getColorDataSerie(mr.getTag(),
									cr.getComponentNumber()).brighter().brighter().brighter().brighter();

							opacityInterface.createResultAndResultDiff(mr.getTag(), cr.getComponentNumber(),
									"OpacityCorrection_" + mr.getTag() + "_" + cr.getComponentNumber(),
									"OpacityCorrectionDiff_" + mr.getTag() + "_" + cr.getComponentNumber(),
									"Opacity Correction", "Opacity Correction",
									RotationalTypeCurve.OPACITY_CORRECTION,
									RotationalTypeCurve.OPACITY_CORRECTION_DIFF,
									dataOc, new ArrayList<>(0), color);
						}
					} catch (OpacityCorrectionException oce) {
						LOGGER.error("Error during the execution of the opacity correction", oce);
						fireDataChanged(new ModelChangedEvent(ERROR_EVENT, oce.getMessage()));
					}
				}
			}
		}
		if (ce.hasError()) {
			fireDataChanged(new ModelChangedEvent(DOES_NOT_CONVERGE_EVENT, ce.getErrorMessage()));
		}
		opacityInterface.updateInfoModelOpacityCorrection();
	}

	/**
	 * Do an operation of opacity correction.
	 *
	 * @param listPi The {@link List} of {@link PointInformation} on which we want
	 *  to do the operation.
	 * @param mol The {@link MoleculeDescriptionDB} of the {@link PointInformation}.
	 * @param percentCompression The compression in %
	 * @param maxIteration The maximum number of iterations.
	 * @return the {@link List} of {@link PointInformation} after the operation,
	 *  an empty List if the opacity correction did not converge.
	 * @throws OpacityCorrectionException in case of error.
	 */
	protected static List<PointInformation> opacityCorrection(
			List<PointInformation> listPi, MoleculeDescriptionDB mol,
			double percentCompression, int maxIteration) throws OpacityCorrectionException {
		if (listPi == null) {
			return null;
		}

		int N = listPi.size() ;
		List<PointInformation> newPiList = new ArrayList<>(N);
		for (PointInformation point : listPi) {
			try {
				if (!point.isMultipleted()) {
					newPiList.add(point.clone());
				}
			} catch (CloneNotSupportedException e) {
				LOGGER.error("Can not clone a PointInformation during the opacity correction", e);
				return null;
			}
		}
		N = newPiList.size();

		if (N <= 1) {
			throw new OpacityCorrectionException(
					"You are trying to perform a rotational diagram on 1 point or less"
					+ " because of blended and/or multiplet components, which is impossible.\n"
					+ " Please read the document \"Formalism for the CASSIS software.\""
					+ " at http://cassis.irap.omp.eu/docs/RadiativeTransfer.pdf");
		}

		// To remove... Fast way to detect if some needed parameters are absent.
		for (PointInformation point : newPiList) {
			if (point.getVersion() != PointInformation.VERSION.NEW_2014 &&
				point.getVersion() != PointInformation.VERSION.NEW) {
				throw new OpacityCorrectionException("The data file used is an older version.\n"
						+ "CASSIS cannot, for the moment, deal with these older versions for the opacity correction.");
			}
		}

		final double eps = percentCompression / 100;
		Double1d lineFreqs = new Double1d();
		Double1d au = new Double1d();
		Double1d eu = new Double1d();
		Double1d gu = new Double1d();
		Double1d integInt = new Double1d();
		Double1d deltaV = new Double1d();
		Double1d errIntegInt = new Double1d();

		for (PointInformation point : newPiList) {
			double wCalc = point.getWCalc();
			errIntegInt.append(1.0 / Math.pow(point.getDeltaWCalc() / wCalc, 2));
			lineFreqs.append(point.getNu());
			au.append(point.getAij());
			eu.append(point.getEup());
			gu.append(point.getGup());
			integInt.append(wCalc);
			deltaV.append(point.getFwhm());
		}
		final double coeff = 8 * Math.PI * Formula.kBOLTZMANN_cgs * 1e5;
		Double1d auGu = au.copy().multiply(Formula.hPLANCK_cgs).multiply(gu).multiply(Math.pow(Formula.cLIGHT_cgs, 3));
		Double1d nuGu = integInt.copy().multiply(coeff).multiply(lineFreqs.copy().multiply(1e6).power(2)).divide(auGu);

		// //// Fit linaire( sans l'opacite ) : ln(Nu/gu)=lnNtot -
		// lnQ(tex)-Eu/kTex

		final Double1d xdata = eu;

		Double1d ydata = new Double1d();
		// lnOnNuGu
		for (int i = 0; i < N; i++) {
			ydata.append(Math.log(nuGu.get(i))); // ln(Nu/gu)
		}

		final PolynomialModel poly = new PolynomialModel(1);
		final Fitter fitter = new Fitter(xdata, poly);

		Double1d lineFit = fitter.fit(ydata, errIntegInt);
		double trot = -1. / lineFit.get(1);
		double qTrot = mol.buildQt(trot);
		double ntot = qTrot * Math.exp(lineFit.get(0));

		// Calcul de tau, Ctau + 1ere iteration
		double[] y2 = new double[N];
		double[] tauArray = new double[N];
		final double TAU_MIN = 1e-8;

		for (int i = 0; i < N; i++) {
			double nu = ntot * gu.get(i) * Math.exp(-eu.get(i) / trot) / qTrot;
			double tau = computeTau(trot, nu, lineFreqs.get(i) * 1e6, deltaV.get(i), au.get(i));
			tauArray[i] = tau;
			y2[i] = computeYLog(TAU_MIN, tau, nuGu.get(i));
		}

		boolean converge = OpacityCorrectionModel.converge(mol, maxIteration, N,
				eps, lineFreqs, au, eu, gu, deltaV, errIntegInt, nuGu, fitter,
				trot, qTrot, ntot, y2, TAU_MIN, tauArray);

		if (converge) {
			for (int i = 0; i < newPiList.size(); i++) {
				newPiList.get(i).setLnNuOnGu(y2[i]);
				newPiList.get(i).setTau(tauArray[i]);
			}
			return newPiList;
		} else {
			return Collections.emptyList();
		}
	}

	private static boolean converge(MoleculeDescriptionDB mol, int maxIteration,
			final int N, final double eps, Double1d lineFreqs, Double1d au,
			Double1d eu, Double1d gu, Double1d deltaV, Double1d errIntegInt,
			Double1d nuGu, final Fitter fitter, double trot, double qTrot,
			double ntot, double[] y2, final double TAU_MIN, double[] tauArray) {
		Double1d ydata2 = new Double1d(y2);
		Double1d lineFit2 = fitter.fit(ydata2, errIntegInt);
		double tTemp = lineFit2.get(1);
		double trot2 = -1. / tTemp;
		double qTrot2 = mol.buildQt(trot2);
		double nTemp = lineFit2.get(0);
		double ntot2 = qTrot2 * Math.exp(nTemp);

		// //// recalcul de Tex et Ntot par methode iterative
		double n1 = ntot;
		double n2 = ntot2;
		double t1 = trot;
		double t2 = trot2;
		double q1;
		double q2 = qTrot2;
		int it = 1;
		double tau2 = 0;

		while ((Math.abs((n2 - n1) / n1) > eps || Math.abs((t2 - t1) / t1) > eps) && it < maxIteration) {
			it++;
			n1 = n2;
			t1 = t2;
			q1 = q2;

			for (int i = 0; i < N; i++) {
				double nu2 = n1 * gu.get(i) * Math.exp(-eu.get(i) / t1) / q1;
				tau2 = computeTau(t1, nu2, lineFreqs.get(i) * 1e6, deltaV.get(i), au.get(i));
				tauArray[i] = tau2;
				y2[i] = computeYLog(TAU_MIN, tau2, nuGu.get(i));
			}
			Double1d ydata22 = new Double1d(y2);
			Double1d lineFit22 = fitter.fit(ydata22, errIntegInt);
			tTemp = lineFit22.get(1);
			t2 = -1. / tTemp;
			q2 = mol.buildQt(t2);
			nTemp = lineFit22.get(0);
			n2 = q2 * Math.exp(nTemp);
		}

		double tempDev = Double.NaN;
		double densityDev = Double.NaN;
		if (N != 2) {
			tempDev = fitter.getStandardDeviation().get(1) / Math.pow(tTemp, 2.);
			densityDev = n2 * fitter.getStandardDeviation().get(0);
		}
		OpacityCorrectionModel.printMessage(it, n2, densityDev, t2, tempDev, tauArray);

		return !(Math.abs((n2 - n1) / n1) > eps || Math.abs((t2 - t1) / t1) > eps)
				&& OpacityCorrectionModel.areTausOk(tauArray);
	}

	/**
	 * Print information message.
	 *
	 * @param iterations Number of iterations.
	 * @param density The density value.
	 * @param densityDev The density dev value.
	 * @param temperature The temperature.
	 * @param temperatureDev The temperature dev.
	 * @param tauArray The array of tau values.
	 */
	private static void printMessage(int iterations, double density, double densityDev,
			double temperature, double temperatureDev, double[] tauArray) {
		double densityLog = Math.floor(Math.log(density) / Math.log(10));
		double densityPower = Math.pow(10, densityLog);
		DecimalFormat df = new DecimalFormat("0.0");
		DecimalFormat df2 = new DecimalFormat("0.00");
		StringBuilder sb = new StringBuilder();
		sb.append("Nb iterations: ").append(iterations);
		sb.append(";  Tex: ").append(df.format(temperature));
		if (temperatureDev != 0) {
			sb.append(" (+/-").append(df.format(temperatureDev)).append(')');
		}
		sb.append(" K;  Nmol: ");

		sb.append(df2.format(density / densityPower));
		if (densityDev != 0) {
			sb.append(" (+/-").append(df.format(densityDev / densityPower)).append(')');
		}
		sb.append(" E").append(new DecimalFormat("0").format(densityLog)).append(" /cm\u00B2");
		System.out.println(sb.toString());
		System.out.println("Taus: " + OpacityCorrectionModel.formatDoubleArray(
				tauArray, new DecimalFormat("0.00E0")));
	}

	/**
	 * Check if the Taus are OK. (all tau <= maxTau)
	 *
	 * @param tauArray The taus.
	 * @return if the taus are OK
	 */
	private static boolean areTausOk(double[] tauArray) {
		for (int i = 0; i < tauArray.length; i++) {
			if (tauArray[i] > maxTau) {
				return false;
			}
		}
		return true;
	}

	private static String formatDoubleArray(double[] doubleArray, DecimalFormat df) {
		StringBuilder sb = new StringBuilder();
		sb.append('[');
		for (double d : doubleArray) {
			sb.append(df.format(d)).append(", ");
		}
		if (doubleArray.length >= 1) {
			sb.deleteCharAt(sb.length()-1);
			sb.deleteCharAt(sb.length()-1);
		}
		sb.append(']');
		return sb.toString();
	}

	private static double computeYLog(final double TAU_MIN, double tau2, final double iNuGU) {
		double Ctau2;
		if (tau2 < TAU_MIN) {
			Ctau2 = 1;
		} else {
			Ctau2 = tau2 / (1 - Math.exp(-tau2));
		}
		return Math.log(iNuGU * Ctau2);
	}

	private static double computeTau(double trot, double nu, final double freqInHz,
			final double ideltaV, final double aij) {
		return Math.pow(Formula.cLIGHT_cgs, 3) * aij * nu
				* (Math.exp(Formula.hPLANCK_cgs * freqInHz / (Formula.kBOLTZMANN_cgs * trot)) - 1)
				/ (8 * Math.PI * Math.pow(freqInHz, 3) * ideltaV * 1e5 * Math.pow(
						Math.PI / (Math.log(2) * 4), 0.5));
	}

	/**
	 * Check if the opacity correction is possible for a {@link RotationalDiagramResult}.
	 * To do that, check if the provided {@link RotationalDiagramResult} is not null,
	 * contains a result and contains at least one GAUSSIAN component.
	 *
	 * @param res The {@link RotationalDiagramResult} to check.
	 * @return true if the opacity correction is possible, false otherwise.
	 */
	private boolean isOpacityCorrectionPossible(RotationalDiagramResult res) {
		if (res == null || res.getResult().isEmpty()) {
			fireDataChanged(new ModelChangedEvent(NO_DATA_EVENT));
			return false;
		}
		boolean gaussPoints = containsGaussianPoints(res.getResult());
		if (!gaussPoints) {
			fireDataChanged(new ModelChangedEvent(NO_GAUSSIAN_EVENT));
		}
		return gaussPoints;
	}

	/**
	 * Check if there is at least one GAUSSIAN component on a List of
	 *  {@link RotationalDiagramMoleculeResult}.
	 *
	 * @param mols The RotationalDiagramMoleculesResult list to check.
	 * @return if there is a least one RotationalDiagramComponentResult is a GAUSSIAN.
	 */
	private boolean containsGaussianPoints(List<RotationalDiagramMoleculeResult> mols) {
		for (RotationalDiagramMoleculeResult mr : mols) {
			for (RotationalDiagramComponentResult cr : mr.getComponentResultsList()) {
				if (cr.getType() == RotationalSelectionData.GAUSSIAN_TYPE_DATA) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Change the maximum value of the Tau. This is the value from which we
	 *  consider that the Opacity Corrrection has not converged.
	 *
	 * @param newMaxTau the max value of the Tau.
	 */
	public static void setMaxTau(double newMaxTau) {
		maxTau = newMaxTau;
	}

	/**
	 * Return the maximum value of the Tau. This is the value from which we
	 *  consider that the Opacity Corrrection has not converged.
	 *
	 * @return the maximum value of the Tau
	 */
	public static double getMaxTau() {
		return maxTau;
	}

	public boolean checkConditions() {
		boolean res = true;
		if (opacityInterface.haveCurrentFitSelection()){
			fireDataChanged(new ModelChangedEvent(CHECK_SELECTION_BOX_EVENT));
		}
		RotationalDiagramResult currentResult = opacityInterface.getCurrentResult();

		int nbPoints = 0;
		int nbBlended = 0;
		int nbMultipleted = 0;
		List<RotationalDiagramMoleculeResult> result = currentResult.getResult();
		for (RotationalDiagramMoleculeResult rotationalDiagramMoleculeResult : result) {
			List<RotationalDiagramComponentResult> componentResultsList = rotationalDiagramMoleculeResult.getComponentResultsList();
			for (RotationalDiagramComponentResult rotationalDiagramComponentResult : componentResultsList) {
				List<PointInformation> result2 = rotationalDiagramComponentResult.getResult();
				for (PointInformation pointInformation : result2) {
					nbPoints++;
					if (pointInformation.isBlendedLine()){
						nbBlended++;
					}
					if (pointInformation.isMultipleted()){
						nbMultipleted++;
					}
				}
			}
		}

		if (nbPoints == nbBlended && nbPoints == nbMultipleted){
			res = false;
			fireDataChanged(new ModelChangedEvent(CONTAINS_ONLY_MULTIPLET_BLENDED_LINES_EVENT));
		} else  if (nbBlended > 0 || nbMultipleted > 0){
			fireDataChanged(new ModelChangedEvent(CONTAINS_MULTIPLET_BLENDED_LINES_EVENT));
		}

		return res;
	}
}
