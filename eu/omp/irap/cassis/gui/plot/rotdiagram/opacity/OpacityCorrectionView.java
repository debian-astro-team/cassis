/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram.opacity;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import eu.omp.irap.cassis.gui.util.GbcUtil;
import eu.omp.irap.cassis.gui.util.TextFieldFormatFilter;

/**
 * View for the Opacity Correction.
 *
 * @author M. Boiziot
 */
public class OpacityCorrectionView extends JPanel {

	private static final long serialVersionUID = 8549792462072543198L;
	private OpacityCorrectionControl control;
	private JTextField epsilonField;
	private JTextField iterationField;
	private JButton displayButton;


	/**
	 * Constructor.
	 *
	 * @param model The model.
	 */
	public OpacityCorrectionView(OpacityCorrectionModel model) {
		super(new BorderLayout());
		this.control = new OpacityCorrectionControl(this, model);
		initComponents();
	}

	/**
	 * Create the components.
	 */
	private void initComponents() {
		JPanel tmpPanel = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridwidth = 1;
		gbc.insets = new Insets(2, 2, 2, 2);

		GbcUtil.configureFillGridsAnchor(gbc, GridBagConstraints.NONE, 0, 0, GridBagConstraints.WEST);
		JLabel compressionLabel = new JLabel("Epsilon(%):");
		compressionLabel.setToolTipText(
				"Minimum value (percentage accepted between the n and n-1 value.");
		tmpPanel.add(compressionLabel, gbc);

		GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 0);
		tmpPanel.add(getEpsilonField(), gbc);

		GbcUtil.configureFillGrids(gbc, GridBagConstraints.NONE, 0, 1);
		JLabel iterationLabel = new JLabel("Max. Nb of iterations:");
		tmpPanel.add(iterationLabel, gbc);

		GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 1);
		tmpPanel.add(getIterationField(), gbc);

		this.add(new JLabel("Opacity Correction can only be done on Gaussians components."));
		this.add(tmpPanel, BorderLayout.CENTER);
		this.add(getDisplayButton(), BorderLayout.SOUTH);
	}

	/**
	 * Create if needed then return the Epsilon {@link JTextField}.
	 *
	 * @return the epsilon {@link JTextField}.
	 */
	public JTextField getEpsilonField() {
		if (epsilonField == null) {
			epsilonField = new JTextField(String.valueOf(control.getModel().getEpsilon()));
			epsilonField.setPreferredSize(new Dimension(100, 20));
			epsilonField.addKeyListener(new TextFieldFormatFilter(
					TextFieldFormatFilter.DECIMAL,
					String.valueOf(OpacityCorrectionModel.EPSILON_DEFAULT),
					TextFieldFormatFilter.POSITIVE));
			epsilonField.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.updateEpsilon();
				}
			});
			epsilonField.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					control.updateEpsilon();
				}
			});
		}
		return epsilonField;
	}

	/**
	 * Create if needed then return the iteration {@link JTextField}.
	 *
	 * @return the iteration {@link JTextField}.
	 */
	public JTextField getIterationField() {
		if (iterationField == null) {
			iterationField = new JTextField(String.valueOf(control.getModel().getMaxIteration()));
			iterationField.setPreferredSize(new Dimension(100, 20));
			iterationField.addKeyListener(new TextFieldFormatFilter(
					TextFieldFormatFilter.INTEGER,
					String.valueOf(OpacityCorrectionModel.MAX_ITERATION_DEFAULT),
					TextFieldFormatFilter.POSITIVE));
			iterationField.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					int iterationValue = Integer.parseInt(iterationField.getText());
					control.getModel().setMaxIteration(iterationValue);
				}
			});
			iterationField.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					int iterationValue = Integer.parseInt(iterationField.getText());
					control.getModel().setMaxIteration(iterationValue);
				}
			});

		}
		return iterationField;
	}

	/**
	 * Create if needed then return the "Display" {@link JButton}.
	 *
	 * @return the Display {@link JButton}.
	 */
	public JButton getDisplayButton() {
		if (displayButton == null) {
			displayButton = new JButton("Display");
			displayButton.setPreferredSize(new Dimension(185, 74));
			displayButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					control.doOpacityCorrection();
				}
			});
		}
		return displayButton;
	}

}
