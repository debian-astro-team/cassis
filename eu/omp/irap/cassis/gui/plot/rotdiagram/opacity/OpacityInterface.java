/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram.opacity;

import java.awt.Color;
import java.util.List;

import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalTypeCurve;
import eu.omp.irap.cassis.parameters.PointInformation;
import eu.omp.irap.cassis.parameters.RotationalDiagramResult;

/**
 * Interface needed for the Rotational Diagram Opacity Correction functionality.
 *
 * @author M. Boiziot
 */
public interface OpacityInterface {

	/**
	 * Remove all opacity correction series.
	 */
	void removeOpacityCorrection();

	/**
	 * Return the result of the Rotational Diagram.
	 *
	 * @return the result of the Rotational Diagram.
	 */
	RotationalDiagramResult getCurrentResult();

	/**
	 * Create and add a result and result diff.
	 *
	 * @param tag The tag.
	 * @param compNum The component number.
	 * @param keySerie1 The title of the first series.
	 * @param keySerie2 The title of the second series.
	 * @param realNameS1 The full name of the first series.
	 * @param realNameS2 The full name of the second series.
	 * @param rtcS1 The {@link RotationalTypeCurve} of the first series.
	 * @param rtcS2 The {@link RotationalTypeCurve} of the second series.
	 * @param result The List of {@link PointInformation} for the first series.
	 * @param resultEupDiff The List of {@link PointInformation} for the second series.
	 * @param baseColor The base color for the series.
	 */
	void createResultAndResultDiff(int tag, int compNum, String keySerie1,
			String keySerie2, String realNameS1, String realNameS2,
			RotationalTypeCurve rtcS1, RotationalTypeCurve rtcS2,
			List<PointInformation> result, List<PointInformation> resultEupDiff,
			Color baseColor);

	/**
	 * Return the {@link Color} of the Data series with the given parameters.
	 *
	 * @param tag The tag.
	 * @param compNum The component number.
	 * @return the color of the data series with the given parameters.
	 */
	Color getColorDataSerie(int tag, int compNum);

	/**
	 * Update the InfoModel for the Opacity Correction.
	 */
	void updateInfoModelOpacityCorrection();

	/**
	 *
	 * @return true if a fit Selection is underway
	 */
	boolean haveCurrentFitSelection();
}
