/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeriesCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.gui.ScreenUtil;
import eu.omp.irap.cassis.database.access.AccessDataBase;
import eu.omp.irap.cassis.file.EXTENSION;
import eu.omp.irap.cassis.gui.PanelFrame;
import eu.omp.irap.cassis.gui.plot.CassisViewInterface;
import eu.omp.irap.cassis.gui.plot.popup.MessageControl;
import eu.omp.irap.cassis.gui.plot.popup.MessageModel;
import eu.omp.irap.cassis.gui.plot.popup.MessagePanel;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.XYSeriesFitArea;
import eu.omp.irap.cassis.gui.plot.rotdiagram.fit.RotationalFitView;
import eu.omp.irap.cassis.gui.plot.rotdiagram.infopanel.RotationalInfoPanel;
import eu.omp.irap.cassis.gui.plot.rotdiagram.opacity.OpacityCorrectionView;
import eu.omp.irap.cassis.gui.plot.save.CassisSaveViewInterface;
import eu.omp.irap.cassis.gui.plot.save.SaveRotationalDiagram;
import eu.omp.irap.cassis.gui.plot.util.AxisToolsPanel;
import eu.omp.irap.cassis.gui.plot.util.ChartMovePanel;
import eu.omp.irap.cassis.gui.plot.util.PrintUtil;
import eu.omp.irap.cassis.gui.plot.util.StackMosaicModel;
import eu.omp.irap.cassis.gui.plot.util.StackMosaicPanel;
import eu.omp.irap.cassis.gui.plot.util.ToolsState;
import eu.omp.irap.cassis.gui.util.GraphicParameter;
import eu.omp.irap.cassis.gui.util.MyLayeredPane;
import eu.omp.irap.cassis.parameters.RotationalDiagramResult;

/**
 * General view for the Rotational.
 *
 * @author M. Boiziot
 */
public class RotationalView extends JPanel implements CassisViewInterface {

	private static final long serialVersionUID = -7757102256223379162L;
	private static final Logger LOGGER = LoggerFactory.getLogger(RotationalView.class);
	private RotationalControl control;
	private JTabbedPane tabManager;
	private RotationalInfoPanel infoPanel;
	private RotationalFitView rotationalFitView;
	private OpacityCorrectionView opacityCorrectionView;
	private JPanel westPanel;
	private MyLayeredPane layeredPane;
	private ChartPanel chartPanel;
	private JTabbedPane moveManager;
	private ChartMovePanel chartMovePanelX;
	private ChartMovePanel chartMovePanelY;
	private RotationalChartMouseListener chartMouseCassisListener;
	private XYPlot plot;
	private JFreeChart chart;
	private JScrollPane scrollPane;
	private ScrollPaneMessageFrame messageFrame;
	private CassisSaveViewInterface save;
	private JPanel saveValueView;


	/**
	 * Constructor.
	 */
	public RotationalView() {
		super(new BorderLayout());

		control = new RotationalControl(this, new RotationalModel());
		Dimension dim = new Dimension(800, 600);
		setPreferredSize(dim);
		setSize(dim);

		add(getTabManager(), BorderLayout.EAST);
		add(getWestPanel(), BorderLayout.CENTER);
	}

	/**
	 * Return the controller.
	 *
	 * @return the controller.
	 */
	public RotationalControl getControl() {
		return control;
	}

	/**
	 * Create if needed then return the {@link JTabbedPane} of the right panel.
	 *
	 * @return the {@link JTabbedPane} of the right panel.
	 */
	public final JTabbedPane getTabManager() {
		if (tabManager == null) {
			tabManager = new JTabbedPane();
			Dimension dim = new Dimension(350, 623);
			tabManager.setSize(dim);
			tabManager.setPreferredSize(dim);
			tabManager.addTab("InfoPanel", getInfoPanelScrollPane());
			tabManager.addTab("Fit", getRotationalFitView());
			tabManager.addTab("Opacity Correction", getOpacityCorrectionView());
			tabManager.addTab("Save", getSaveValueView());
		}
		return tabManager;
	}

	private JPanel getSaveValueView() {
		if(saveValueView == null){
			saveValueView = new JPanel();
			JLabel saveLabel = new JLabel("Save displayed values");
			saveValueView.add(saveLabel);
			JButton saveValueButton = new JButton("Save");
			saveValueView.add(saveValueButton);
			saveValueButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					control.saveValue();

				}
			});
		}
		return saveValueView;
	}

	/**
	 * Create if needed then return the InfoPanel {@link ScrollPane}.
	 *
	 * @return the InfoPanel {@link ScrollPane}.
	 */
	public JScrollPane getInfoPanelScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane(getInfoPanel(),
					ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
					ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			scrollPane.setPreferredSize(new Dimension(200, 400));
			scrollPane.getVerticalScrollBar().setUnitIncrement(10);
		}
		return scrollPane;
	}

	/**
	 * Create if needed then return the {@link RotationalInfoPanel}.
	 *
	 * @return the {@link RotationalInfoPanel}.
	 */
	public RotationalInfoPanel getInfoPanel() {
		if (infoPanel == null) {
			infoPanel = new RotationalInfoPanel(
					control.getModel().getRotationalInfoModel(), control);
		}
		return infoPanel;
	}

	/**
	 * Create if needed then return the {@link RotationalFitView}.
	 *
	 * @return the {@link RotationalFitView}.
	 */
	public RotationalFitView getRotationalFitView() {
		if (rotationalFitView == null) {
			rotationalFitView = new RotationalFitView(
					control.getModel().getRotationalFitModel());
		}
		return rotationalFitView;
	}

	/**
	 * Create if needed then return the {@link OpacityCorrectionView}.
	 *
	 * @return the {@link OpacityCorrectionView}.
	 */
	public OpacityCorrectionView getOpacityCorrectionView() {
		if (opacityCorrectionView == null) {
			opacityCorrectionView = new OpacityCorrectionView(
					control.getModel().getOpacityCorrectionModel());
		}
		return opacityCorrectionView;
	}

	/**
	 * Create if needed then return the West {@link JPanel}.
	 *
	 * @return the West {@link JPanel}.
	 */
	public final JPanel getWestPanel() {
		if (westPanel == null) {
			westPanel = new JPanel(new BorderLayout());

			getChartPanel().setLocation(getLayeredPane().getLocation());
			getLayeredPane().add(getChartPanel(), -1);
			getLayeredPane().addComponentListener(new ComponentAdapter() {
				@Override
				public void componentResized(ComponentEvent e) {
					super.componentResized(e);
					getChartPanel().setBounds(getLayeredPane().getBounds());
					revalidate();
					getLayeredPane().repaint();
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							placeFitAnnotations();
						}
					});
				}
			});

			westPanel.addComponentListener(new ComponentAdapter() {
				@Override
				public void componentResized(ComponentEvent e) {
					getChartPanel().validate();
					getLayeredPane().validate();
				}
			});

			westPanel.add(getLayeredPane(), BorderLayout.CENTER);

			westPanel.add(getMoveManager(), BorderLayout.SOUTH);
			chartMovePanelX.setChartPanel(chartPanel);
			chartMovePanelY.setChartPanel(chartPanel);
		}
		return westPanel;
	}

	/**
	 * Create if needed then return the {@link MyLayeredPane}.
	 *
	 * @return the {@link MyLayeredPane}.
	 */
	public MyLayeredPane getLayeredPane() {
		if (layeredPane == null) {
			layeredPane = new MyLayeredPane();
		}
		return layeredPane;
	}

	/**
	 * Create if needed then return the {@link ChartPanel}.
	 *
	 * @return the {@link ChartPanel}.
	 */
	public ChartPanel getChartPanel() {
		if (chartPanel == null) {
			chartPanel = new ChartPanel(getChart(), true);
			chartPanel.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent arg0) {
					updateRange();
				}
			});

			chartPanel.setHorizontalAxisTrace(false);
			chartPanel.setVerticalAxisTrace(false);
			chartPanel.setDomainZoomable(true);
			chartPanel.setRangeZoomable(true);
			chartPanel.setFillZoomRectangle(false);
			chartPanel.getChart().removeLegend();

			GraphicParameter.initializeGraphic("RotDiagram", chartPanel.getChart().getXYPlot(), chartPanel.getChart()
					.getXYPlot().getDomainAxis(), chartPanel.getChart().getXYPlot().getRangeAxis(), null);

			chartPanel.addMouseListener(getChartMouseCassisListener());
			chartPanel.addMouseMotionListener(getChartMouseCassisListener());
			chartPanel.addChartMouseListener(getChartMouseCassisListener());
			chartPanel.setMouseWheelEnabled(true);

			JFreeChart freeChart = chartPanel.getChart();
			freeChart.addProgressListener(getChartMouseCassisListener());
			freeChart.removeLegend();
		}
		return chartPanel;
	}

	/**
	 * Create if needed then return the bottom {@link JTabbedPane}.
	 *
	 * @return the bottom {@link JTabbedPane}.
	 */
	private JTabbedPane getMoveManager() {
		if (moveManager == null) {
			moveManager = new JTabbedPane();

			String[] listXAxis = { "Eup/k[K]" };
			AxisToolsPanel axisXToolsPanel = new AxisToolsPanel(chartPanel, getPlot().getDomainAxis(), listXAxis,
					"<HTML><p>X<sub>top</sub></p></HTML>", "<HTML><p>X<sub>bot</sub></p></HTML>", ToolsState.X);
			chartMovePanelX = axisXToolsPanel.getChartMovePanel();

			String[] listYAxis = { "ln(Nu/gu)" };
			AxisToolsPanel axisYToolsPanel = new AxisToolsPanel(chartPanel, getPlot().getRangeAxis(), listYAxis,
					"<HTML><p>Y<sub>top</sub></p></HTML>", "<HTML><p>Y<sub>bot</sub></p></HTML>", ToolsState.Y);
			chartMovePanelY = axisYToolsPanel.getChartMovePanel();

			StackMosaicPanel panelStack = new StackMosaicPanel(new StackMosaicModel());

			axisXToolsPanel.setName("x");
			axisYToolsPanel.setName("y");
			panelStack.setName("stack");

			moveManager.addTab("X Tools", axisXToolsPanel);
			moveManager.addTab("Y Tools", axisYToolsPanel);
			moveManager.addTab("Stack/Mosaic", panelStack);
			moveManager.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					updateRange();
				}
			});
		}
		return moveManager;
	}

	/**
	 * Update the range in the bottom {@link JTabbedPane}.
	 */
	private void updateRange() {
		String toolsState = moveManager.getSelectedComponent().getName();
		if ("x".equals(toolsState)) {
			double range = chartMovePanelX.getUpperBoundDomain() - chartMovePanelX.getLowerBoundDomain();
			chartMovePanelX.setRange(ChartMovePanel.arrondi(range, 2));
		} else if ("y".equals(toolsState)) {
			double range = chartMovePanelY.getUpperBoundRange() - chartMovePanelY.getLowerBoundRange();
			chartMovePanelY.setRange(ChartMovePanel.arrondi(range, 2));
		}
	}

	/**
	 * Create if needed then return the {@link JFreeChart}.
	 *
	 * @return the {@link JFreeChart}.
	 */
	public JFreeChart getChart() {
		if (chart == null) {
			chart = new JFreeChart(getPlot());
		}
		return chart;
	}

	/**
	 * Create if needed then return the {@link XYPlot}.
	 *
	 * @return the {@link XYPlot}.
	 */
	public XYPlot getPlot() {
		if (plot == null) {
			NumberAxis xAxis = new NumberAxis("Eup/k[K]");
			NumberAxis yAxis = new NumberAxis("ln(Nu/gu)");
			xAxis.setAutoRangeIncludesZero(false);
			yAxis.setAutoRangeIncludesZero(false);

			RotationalModel model = control.getModel();
//			 create the XYplot which contains renderer and dataset
			plot = new XYPlot(model.getDataStepPoint(), xAxis, yAxis,
					model.getStepRenderer());
			plot.setDataset(RotationalValue.DATA, model.getDataSeries());
			plot.setRenderer(RotationalValue.DATA, model.getDataRenderer());

			plot.setDataset(RotationalValue.ERROR, model.getErrorDataSeries());
			plot.setRenderer(RotationalValue.ERROR, model.getErrorDataRenderer());

			plot.setDataset(RotationalValue.FIT_RECTANGLE, model.getFitRectangleDataset());
			plot.setRenderer(RotationalValue.FIT_RECTANGLE, model.getFitRectangleRenderer());

			plot.setDataset(RotationalValue.FIT, model.getFitDataset());
			plot.setRenderer(RotationalValue.FIT, model.getFitRenderer());

			plot.setDomainGridlinesVisible(false);
			plot.setRangeGridlinesVisible(false);
			plot.setNoDataMessage("No data");

			plot.setRangeCrosshairVisible(true);
			plot.setDomainCrosshairVisible(true);

			plot.setRangeCrosshairLockedOnData(true);
			plot.setDomainCrosshairLockedOnData(true);

			initializeAxes(plot);
		}
		return plot;
	}

	/**
	 * Configure the axes of the given plot.
	 *
	 * @param plot The plot to configure.
	 */
	private void initializeAxes(XYPlot plot) {
		try {
			ArrayList<Integer> list = new ArrayList<>(2);
			list.add(0);
			list.add(1);

			// Domain
			plot.setDomainAxis(1, (ValueAxis) plot.getDomainAxis().clone());
			plot.mapDatasetToDomainAxes(RotationalValue.LINKS, list);
			plot.mapDatasetToDomainAxes(RotationalValue.DATA, list);
			plot.mapDatasetToDomainAxes(RotationalValue.FIT_RECTANGLE, list);
			plot.mapDatasetToDomainAxes(RotationalValue.FIT, list);
			plot.mapDatasetToDomainAxes(RotationalValue.ERROR, list);
			plot.getDomainAxis(0).setLowerMargin(0.10);
			plot.getDomainAxis(0).setUpperMargin(0.10);
			plot.getDomainAxis(1).setLowerMargin(0.10);
			plot.getDomainAxis(1).setUpperMargin(0.10);

			// Range
			plot.setRangeAxis(1, (ValueAxis) plot.getRangeAxis().clone());
			plot.mapDatasetToRangeAxes(RotationalValue.LINKS, list);
			plot.mapDatasetToRangeAxes(RotationalValue.DATA, list);
			plot.mapDatasetToRangeAxes(RotationalValue.FIT_RECTANGLE, list);
			plot.mapDatasetToRangeAxes(RotationalValue.FIT, list);
			plot.mapDatasetToRangeAxes(RotationalValue.ERROR, list);
			plot.getRangeAxis(0).setLowerMargin(0.10);
			plot.getRangeAxis(0).setUpperMargin(0.10);
			plot.getRangeAxis(1).setLowerMargin(0.10);
			plot.getRangeAxis(1).setUpperMargin(0.10);
		} catch (CloneNotSupportedException e) {
			LOGGER.error("Error during the axes initialization", e);
		}
	}

	/**
	 * @see java.awt.print.Printable#print(java.awt.Graphics, java.awt.print.PageFormat, int)
	 */
	@Override
	public int print(Graphics graphics, PageFormat pageFormat, int pageIndex)
			throws PrinterException {
		return PrintUtil.print(graphics, pageFormat, pageIndex,
				getSave().getImageToPrint());
	}

	/**
	 * Display a new result.
	 *
	 * @param result The result to display.
	 */
	public void displayResult(RotationalDiagramResult result) {
		getChartMouseCassisListener().removeAllMessages();
		getTabManager().setSelectedComponent(getInfoPanelScrollPane());
		control.getModel().setResult(result);
		checkErrors(result);
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.CassisViewInterface#getSave()
	 */
	@Override
	public CassisSaveViewInterface getSave() {
		if (save == null) {
			save = new SaveRotationalDiagram(this);
		}
		return save;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.CassisViewInterface#getTitle()
	 */
	@Override
	public String getTitle() {
		return "Rotational Diagram";
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.CassisViewInterface#getHelpUrl()
	 */
	@Override
	public String getHelpUrl() {
		return "RotationalView.html";
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.CassisViewInterface#getComponent()
	 */
	@Override
	public Component getComponent() {
		return this;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.CassisViewInterface#getSyntheticLineVisible()
	 */
	@Override
	public List<LineDescription> getSyntheticLineVisible() {
		return null;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.CassisViewInterface#getSyntheticLineVisible()
	 */
	@Override
	public List<LineDescription> getOtherSpeciesLineVisible() {
		return null;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.CassisViewInterface#checkIfSaveIsPossible(java.io.File, eu.omp.irap.cassis.file.EXTENSION)
	 */
	@Override
	public boolean checkIfSaveIsPossible(File file, EXTENSION ext) {
		if (file.exists()) {
			String message = "This file " + file.toString() + " already exists.\n"
					+ "Do you want to replace it?";
			int result = JOptionPane.showConfirmDialog(null, message,
					"Replace existing file?", JOptionPane.YES_NO_OPTION);
			if (result == JOptionPane.NO_OPTION) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Remove all fit annotations.
	 */
	public void removeAllFitAnnotations() {
		getChartMouseCassisListener().getMessageControl().removeAllMessages(MessageModel.ROTATIONAL_FIT_TYPE);
	}

	/**
	 * Create the annotation content for a fit serie.
	 *
	 * @param serie The serie.
	 * @return The message.
	 */
	private String createFitMessage(XYSeriesFitArea serie) {
		StringBuilder sb = new StringBuilder();
		sb.append("Comp ");
		sb.append(serie.getNumComponent());
		sb.append(" - ");
		sb.append(AccessDataBase.getDataBaseConnection().getMolName(serie.getTag()));
		sb.append(" - Block ");
		sb.append(serie.getBlockSource());
		sb.append('\n');
		String res = serie.getLineInformation().getInfoPanel();
		sb.append(' ');
		sb.append(res.substring(0, res.indexOf('K') + 1));
		sb.append("\n ");
		sb.append(res.substring(res.indexOf('K') + 2));
		return sb.toString();
	}

	/**
	 * Refresh all fit annotations.
	 */
	public void refreshFitAnnotations() {
		removeAllFitAnnotations();

		XYSeriesCollection col = control.getModel().getFitDataset();

		int nbSerie = col.getSeriesCount();
		MessageControl mc = getChartMouseCassisListener().getMessageControl();

		for (int i = 0; i < nbSerie; i++) {
			XYSeriesFitArea serie = (XYSeriesFitArea) col.getSeries(i);
			if (!serie.getLineInformation().isVisible()) {
				continue;
			}
			String msg = createFitMessage(serie);
			MessagePanel mp = mc.addMessagePanel(msg, true, 0, 0, 0);
			MessageModel mm = mp.getModel();
			mm.setType(MessageModel.ROTATIONAL_FIT_TYPE);
			mm.setColor(serie.getLineInformation().getColor());
			mm.setFontSize(13);
			mp.update();
		}
		placeFitAnnotations();
	}

	/**
	 * Place the fit annotations on the plot.
	 */
	public void placeFitAnnotations() {
		List<MessagePanel> messages = getChartMouseCassisListener().getMessageControl().getImages();
		Rectangle2D area = getChartPanel().getScreenDataArea();
		double xEnd = area.getX() + area.getWidth() - 10;
		double yStart = area.getY() + 10;
		double xCurrent = xEnd;
		double yAddValue = 0;
		int maxWidth = 0;
		for (MessagePanel message : messages) {
			if (message.getModel().getType() != MessageModel.ROTATIONAL_FIT_TYPE) {
				continue;
			}
			Dimension d = message.getSizeImage();
			if (d.width > maxWidth) {
				maxWidth = d.width;
			}

			int xLeft = (int)xCurrent - d.width;
			int yTop = (int)(yStart + yAddValue);
			if (yTop + d.height > area.getY() + area.getHeight()) {
				xCurrent -= maxWidth;
				xLeft = (int) (xCurrent - maxWidth) -5;
				yTop = (int) (area.getY() + 10);
				yAddValue = 0;
				if (xLeft < 0) {
					xLeft = 0;
				}
			}
			message.setLocation(xLeft, yTop);
			yAddValue += d.height;
		}
	}

	/**
	 * Check the provided result for errors and multiplet.
	 *
	 * @param result The result to check.
	 */
	private void checkErrors(RotationalDiagramResult result) {
		checkFrequenciesError(result);
		checkMultipletInfo(result);
	}

	/**
	 * Check the provided result for frequencies errors (frequencies not found in the database).
	 *
	 * @param result The result to check.
	 */
	private void checkFrequenciesError(RotationalDiagramResult result) {
		if (result.getFrequenciesNotFound().isEmpty()) {
			return;
		}
		JTextArea textArea = new JTextArea(createFrenquenciesErrorMsg(
				result.getFrequenciesNotFound()));
		textArea.setColumns(60);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setSize(textArea.getPreferredSize().width, 1);
		textArea.setBackground(null);

		JOptionPane.showMessageDialog(this, textArea, "Warning: database",
				JOptionPane.WARNING_MESSAGE);
	}

	/**
	 * Display a message if there is multiplet.
	 *
	 * @param result The result to check.
	 */
	private void checkMultipletInfo(RotationalDiagramResult result) {
		final String messageMultiplet = result.getMultipletMessage();
		final String messageBlended = result.getBlendedMessage();

		final String message = getMessage(messageMultiplet, messageBlended);

		if (message != null) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					if (messageFrame == null) {
						messageFrame = new ScrollPaneMessageFrame(
								"Warning: multiplets and/or blended lines", message);
					} else {
						messageFrame.setMessage(message);
					}
					messageFrame.setVisible(true);
					ScreenUtil.center(PanelFrame.getInstance(), messageFrame);
				}
			});
		} else if (messageFrame != null && messageFrame.isVisible()) {
			messageFrame.dispose();
		}
	}

	/**
	 * Create the message to display about multiplet and blended lines.
	 *
	 * @param messageMultiplet The message about multiplet.
	 * @param messageBlended The message about blended.
	 * @return The general message to display or null if both parameter are
	 *  null (no multiplet/blended).
	 */
	private String getMessage(String messageMultiplet, String messageBlended) {
		String messGeneral = "Transition, Frequency(MHz), Eup(K), Aij, Gup\n";
		String messMultiplet = "";
		String messBlended = "";
		if (messageMultiplet != null) {
			messMultiplet =  "--> multiplets (~same frequency, ~same upper energy):\n"
					+ "CASSIS can perform the rotational diagram analysis\n "
					+ "but not the opacity correction on those points \n"
					+ messageMultiplet;
		}
		if (messageBlended != null) {
			messBlended = "--> blended (~same frequency, different upper energy):\n "
					+ "CASSIS cannot perform the rotational diagram analysis on those points \n"
					+ messageBlended;
		}

		return (messageMultiplet == null && messageBlended == null) ?
				null : messGeneral + messMultiplet + messBlended;
	}

	/**
	 * Return if there is more than one frequency not found.
	 *
	 * @param m The map of errors with <Tag, Set<Freq>>.
	 * @return if there is more than one frequency not found.
	 */
	private static boolean multipleFreqNotFound(Map<Integer, Set<Double>> m) {
		if (m.size() > 1) {
			return true;
		}
		int i = 0;
		for (Set<Double> set : m.values()) {
			i += set.size();
		}
		return i > 1;
	}

	/**
	 * Create the errror message for frequencies not found in the database.
	 *
	 * @param m The map of errors with <Tag, Set<Freq>>.
	 * @return The error message.
	 */
	private static String createFrenquenciesErrorMsg(Map<Integer, Set<Double>> m) {
		StringBuilder sb = new StringBuilder();

		if (RotationalView.multipleFreqNotFound(m)) {
			sb.append("Some frenquencies were not found in the database (in MHz):\n");
		} else {
			sb.append("One frequency was not found in the database (in MHz):\n");
		}

		for (Integer tag : m.keySet()) {
			sb.append("  - Tag ").append(tag).append(": ");
			Double[] freqs = m.get(tag).toArray(new Double[m.get(tag).size()]);
			for (int i = 0; i < freqs.length - 1; i++) {
				sb.append(freqs[i]).append(", ");
			}
			sb.append(freqs[freqs.length - 1]).append('\n');
		}
		return sb.toString();
	}

	/**
	 * Create if needed then return the {@link RotationalChartMouseListener}.
	 *
	 * @return the {@link RotationalChartMouseListener}.
	 */
	public RotationalChartMouseListener getChartMouseCassisListener() {
		if (chartMouseCassisListener == null) {
			chartMouseCassisListener = new RotationalChartMouseListener(this, control.getModel());
		}
		return chartMouseCassisListener;
	}

	/**
	 * Restore the panel.
	 *
	 * @param panel The panel to restore.
	 */
	public void restorePanel(JComponent panel) {
		westPanel.add(panel, BorderLayout.CENTER);
		westPanel.revalidate();
		westPanel.repaint();
	}
}
