/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram.infopanel;

import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalSerieCassisListener;

public class RotationalInfoMoleculeView extends JPanel implements ModelListener {

	private static final long serialVersionUID = 8411650302015078293L;
	private RotationalInfoMolecule model;
	private List<RotationalInfoComponentView> ricViewList;
	private JButton deleteButton;
	private JPanel compPanel;
	private RotationalSerieCassisListener globalListener;


	public RotationalInfoMoleculeView(RotationalInfoMolecule model, RotationalSerieCassisListener rscl) {
		super();
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		this.model = model;
		this.globalListener = rscl;
		this.ricViewList = new ArrayList<>();
		initComponents();
		this.model.addModelListener(this);
	}

	private void initComponents() {
		this.setBorder(new TitledBorder(null, model.getName() + " [" + model.getTag() + ']', 2, 2, null));

		compPanel = new JPanel();
		compPanel.setLayout(new BoxLayout(compPanel, BoxLayout.Y_AXIS));
		add(compPanel);
		for (RotationalInfoComponent ric : model.getListCompo()) {
			RotationalInfoComponentView ricv = new RotationalInfoComponentView(ric, this.globalListener);
			addRicView(ricv);
		}
	}

	public JButton getDeleteButton() {
		if (deleteButton == null) {
			deleteButton = new JButton("x");
		}
		return deleteButton;
	}

	private void addRicView(RotationalInfoComponentView ricv) {
		ricViewList.add(ricv);
		compPanel.add(ricv);
		compPanel.revalidate();
		compPanel.repaint();
	}

	private void removeRicView(RotationalInfoComponentView ricv) {
		ricViewList.remove(ricv);
		compPanel.remove(ricv);
		compPanel.revalidate();
		compPanel.repaint();
	}

	private RotationalInfoComponentView getRicView(RotationalInfoComponent ric) {
		for (RotationalInfoComponentView ricv : ricViewList) {
			if (ricv.getModel().equals(ric)) {
				return ricv;
			}
		}
		return null;
	}

	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (RotationalInfoMolecule.ADD_INFO_COMPONENT_EVENT.equals(event.getSource())) {
			RotationalInfoComponent ric = (RotationalInfoComponent) event.getValue();
			RotationalInfoComponentView ricv = new RotationalInfoComponentView(ric, this.globalListener);
			addRicView(ricv);
		} else if (RotationalInfoMolecule.REMOVE_INFO_COMPONENT_EVENT.equals(event.getSource())) {
			RotationalInfoComponent ric = (RotationalInfoComponent) event.getValue();
			RotationalInfoComponentView ricv = getRicView(ric);
			if (ricv != null) {
				removeRicView(ricv);
			}
		}
	}

	public boolean isSameModel(RotationalInfoMolecule rim) {
		return model.equals(rim);
	}
}
