/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram.infopanel;

import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalCurvePanelModel;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalCurvePanelView;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalSerieCassisListener;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalTypeCurve;

public class RotationalInfoComponentView extends JPanel implements ModelListener {

	private static final long serialVersionUID = -5067520072928551077L;
	private RotationalInfoComponent model;
	private List<RotationalCurvePanelView> rotationalCurvePanelViewList;
	private JPanel compPanel;
	private RotationalSerieCassisListener globalListener;


	public RotationalInfoComponentView(RotationalInfoComponent model, RotationalSerieCassisListener rscl) {
		super();
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		this.model = model;
		this.globalListener = rscl;
		this.rotationalCurvePanelViewList = new ArrayList<>();
		this.model.addModelListener(this);
		initComponents();
	}

	private void initComponents() {
		this.setBorder(new TitledBorder(
				"Component " + model.getNumber() + ": " + model.getType()));

		compPanel = new JPanel();
		compPanel.setLayout(new BoxLayout(compPanel, BoxLayout.Y_AXIS));
		add(compPanel);
		for (RotationalCurvePanelModel rcpm : model.getCurvePanelModelList()) {
			RotationalCurvePanelView rcpv = new RotationalCurvePanelView(rcpm);
			addRotationalCurvePanelView(rcpv);
		}
	}

	public RotationalInfoComponent getModel() {
		return model;
	}

	private void addRotationalCurvePanelView(RotationalCurvePanelView rcpv) {
		rcpv.getControl().addRotationalSerieCassisListener(this.globalListener);
		if (rcpv.getControl().getModel().getSeries().getType() ==
				RotationalTypeCurve.OPACITY_CORRECTION) {
			// We want the OC just after the Original.
			rotationalCurvePanelViewList.add(1, rcpv);
			compPanel.removeAll();
			for (RotationalCurvePanelView comp : rotationalCurvePanelViewList) {
				compPanel.add(comp);
			}
		} else {
			rotationalCurvePanelViewList.add(rcpv);
			compPanel.add(rcpv);
		}

		compPanel.revalidate();
		compPanel.repaint();
	}

	private void removeRotationalCurvePanelView(RotationalCurvePanelView rcpv) {
		rotationalCurvePanelViewList.remove(rcpv);
		compPanel.remove(rcpv);
		compPanel.revalidate();
		compPanel.repaint();
	}

	private RotationalCurvePanelView getRotationalCurvePanelView(RotationalCurvePanelModel rcpm) {
		for (RotationalCurvePanelView rcpv : rotationalCurvePanelViewList) {
			if (rcpv.getControl().getModel().equals(rcpm)) {
				return rcpv;
			}
		}
		return null;
	}

	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (RotationalInfoComponent.CURVE_PANEL_ADD_EVENT.equals(event.getSource())) {
			RotationalCurvePanelModel rcpm = (RotationalCurvePanelModel) event.getValue();
			RotationalCurvePanelView rcpv = new RotationalCurvePanelView(rcpm);
			addRotationalCurvePanelView(rcpv);
		} else if (RotationalInfoComponent.CURVE_PANEL_REMOVE_EVENT.equals(event.getSource())) {
			RotationalCurvePanelModel rcpm = (RotationalCurvePanelModel) event.getValue();
			RotationalCurvePanelView rcpv = getRotationalCurvePanelView(rcpm);
			if (rcpv != null) {
				removeRotationalCurvePanelView(rcpv);
			}
		} else if (RotationalInfoComponent.FORCE_FIT_UPDATE_EVENT.equals(event.getSource())) {
			for (RotationalCurvePanelView rcpv : rotationalCurvePanelViewList) {
				if (rcpv.getControl().getModel().getSeries().getType() == RotationalTypeCurve.FIT) {
					rcpv.refreshView();
					rcpv.getControl().fireVisibilityChange();
				}
			}
		}
	}
}
