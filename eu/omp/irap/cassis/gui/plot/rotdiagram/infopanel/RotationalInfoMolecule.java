/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram.infopanel;

import java.util.ArrayList;
import java.util.List;

import eu.omp.irap.cassis.common.events.ListenerManager;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;

public class RotationalInfoMolecule extends ListenerManager {

	public static final String ADD_INFO_COMPONENT_EVENT = "addRotationalInfoComponent";
	public static final String REMOVE_INFO_COMPONENT_EVENT = "removeRotationalInfoComponent";
	private List<RotationalInfoComponent> listCompo;
	private String name;
	private int tag;


	public RotationalInfoMolecule(int molTag, String molName) {
		listCompo = new ArrayList<>();
		this.tag = molTag;
		this.name = molName;
	}

	public String getName() {
		return name;
	}

	public int getTag() {
		return tag;
	}

	public final List<RotationalInfoComponent> getListCompo() {
		return listCompo;
	}

	public RotationalInfoComponent getComponent(int numCompo) {
		for (RotationalInfoComponent comp : listCompo) {
			if (comp.getNumber() == numCompo) {
				return comp;
			}
		}
		return null;
	}

	public void addRotationalInfoComponent(RotationalInfoComponent ric) {
		listCompo.add(ric);
		fireDataChanged(new ModelChangedEvent(ADD_INFO_COMPONENT_EVENT, ric));
	}

	public void removeRotationalInfoComponent(RotationalInfoComponent ric) {
		listCompo.remove(ric);
		fireDataChanged(new ModelChangedEvent(REMOVE_INFO_COMPONENT_EVENT, ric));
	}
}
