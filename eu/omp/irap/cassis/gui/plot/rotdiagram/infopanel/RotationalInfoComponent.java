/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram.infopanel;

import java.util.ArrayList;
import java.util.List;

import eu.omp.irap.cassis.common.events.ListenerManager;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalCurvePanelModel;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalTypeCurve;
import eu.omp.irap.cassis.parameters.RotationalSelectionData;

public class RotationalInfoComponent extends ListenerManager {

	public static final String CURVE_PANEL_ADD_EVENT = "addRotationalCurvePanelEvent";
	public static final String CURVE_PANEL_REMOVE_EVENT = "removeRotationalCurvePanelEvent";
	public static final String FORCE_FIT_UPDATE_EVENT = "forceFitUpdateEvent";
	private RotationalSelectionData type;
	private int number;
	private List<RotationalCurvePanelModel> curvePanelModelList;


	public RotationalInfoComponent(int number, RotationalSelectionData type) {
		this.number = number;
		this.type = type;
		this.curvePanelModelList = new ArrayList<>(2);
	}

	public RotationalSelectionData getType() {
		return type;
	}

	public int getNumber() {
		return number;
	}

	public List<RotationalCurvePanelModel> getCurvePanelModelList() {
		return curvePanelModelList;
	}

	public void addCurvePanelModel(RotationalCurvePanelModel rcpm) {
		curvePanelModelList.add(rcpm);
		fireDataChanged(new ModelChangedEvent(CURVE_PANEL_ADD_EVENT, rcpm));
	}

	public void removeCurvePanelModel(RotationalCurvePanelModel rcpm) {
		curvePanelModelList.remove(rcpm);
		fireDataChanged(new ModelChangedEvent(CURVE_PANEL_REMOVE_EVENT, rcpm));
	}

	public void removeOpacityCorrection() {
		List<RotationalCurvePanelModel> rcpmToRemove = new ArrayList<>();
		for (RotationalCurvePanelModel rcpm : curvePanelModelList) {
			if (RotationalTypeCurve.isOpacityCorrection(rcpm.getSeries().getType())) {
				rcpmToRemove.add(rcpm);
			}
		}
		for (RotationalCurvePanelModel rcpm : rcpmToRemove) {
			removeCurvePanelModel(rcpm);
		}
	}

	public void removeFitSeries() {
		List<RotationalCurvePanelModel> rcpmToRemove = new ArrayList<>();
		for (RotationalCurvePanelModel rcpm : curvePanelModelList) {
			if (rcpm.getSeries().getType() == RotationalTypeCurve.FIT) {
				rcpmToRemove.add(rcpm);
			}
		}
		for (RotationalCurvePanelModel rcpm : rcpmToRemove) {
			removeCurvePanelModel(rcpm);
		}
	}

	public void updateFitSeries() {
		fireDataChanged(new ModelChangedEvent(FORCE_FIT_UPDATE_EVENT));
	}
}
