/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.chart.renderer.xy.XYErrorRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYStepRenderer;
import org.jfree.data.xy.XYIntervalSeriesCollection;
import org.jfree.data.xy.XYSeriesCollection;

import eu.omp.irap.cassis.common.events.ListenerManager;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.gui.plot.curve.config.ColorsCurve;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.Point;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalCurvePanelModel;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalTextAnnotation;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalTypeCurve;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.XYDataStepSeries;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.XYRotationalIntervalSeries;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.XYSeriesFitArea;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.XYSeriesFitBorder;
import eu.omp.irap.cassis.gui.plot.rotdiagram.fit.FitInterface;
import eu.omp.irap.cassis.gui.plot.rotdiagram.fit.FitRotationalRectangle;
import eu.omp.irap.cassis.gui.plot.rotdiagram.fit.RotationalFitModel;
import eu.omp.irap.cassis.gui.plot.rotdiagram.infopanel.RotationalInfoComponent;
import eu.omp.irap.cassis.gui.plot.rotdiagram.infopanel.RotationalInfoModel;
import eu.omp.irap.cassis.gui.plot.rotdiagram.infopanel.RotationalInfoMolecule;
import eu.omp.irap.cassis.gui.plot.rotdiagram.opacity.OpacityCorrectionModel;
import eu.omp.irap.cassis.gui.plot.rotdiagram.opacity.OpacityInterface;
import eu.omp.irap.cassis.parameters.PointInformation;
import eu.omp.irap.cassis.parameters.RotationalDiagramComponentResult;
import eu.omp.irap.cassis.parameters.RotationalDiagramMoleculeResult;
import eu.omp.irap.cassis.parameters.RotationalDiagramResult;

/**
 * Model of the Rotational.
 *
 * @author M. Boiziot
 */
public class RotationalModel extends ListenerManager implements OpacityInterface, FitInterface {

	public static final String REFRESH_ANNOTATIONS_EVENT = "refreshFitAnnotations";
	public static final String REMOVE_ANNOTATIONS_EVENT = "removeFitAnnotations";
	public static final String REMOVE_BLOCK_ANNOTATION_EVENT = "removeBlockAnnotation";
	public static final String REMOVE_ALL_BLOCK_ANNOTATIONS_EVENT = "removeAllBlockAnnotations";
	public static final String ADD_BLOCK_ANNOTATION_EVENT = "addBlockAnnotation";

	private OpacityCorrectionModel opacityCorrectionModel;
	private RotationalFitModel rotationalFitModel;
	private RotationalInfoModel rotationalInfoModel;
	private RotationalDiagramResult currentResult;

	private XYIntervalSeriesCollection dataSeries;
	private XYIntervalSeriesCollection errorDataSeries;
	private XYSeriesCollection dataStepPoint;
	private XYSeriesCollection fitRectangleDataset;
	private XYSeriesCollection fitDataset;

	private XYDotRenderer dataRenderer;
	private XYErrorRenderer errorDataRenderer;
	private XYStepRenderer fitRectangleRenderer;
	private XYItemRenderer fitRenderer;
	private XYStepRenderer stepRenderer;
	private List<RotationalTextAnnotation> fitBlockAnnotations;


	/**
	 * Constructor.
	 */
	public RotationalModel() {
		this.opacityCorrectionModel = new OpacityCorrectionModel(this);
		this.rotationalFitModel = new RotationalFitModel(this);
		this.rotationalInfoModel = new RotationalInfoModel();

		this.dataSeries = new XYIntervalSeriesCollection();
		this.errorDataSeries = new XYIntervalSeriesCollection();
		this.dataStepPoint = new XYSeriesCollection();
		this.fitRectangleDataset = new XYSeriesCollection();
		this.fitDataset = new XYSeriesCollection();

		this.dataRenderer = new XYDotRenderer();
		this.dataRenderer.setDotHeight(10);
		this.dataRenderer.setDotWidth(10);

		this.errorDataRenderer = new XYErrorRenderer();
		this.errorDataRenderer.setAutoPopulateSeriesPaint(false);
		this.errorDataRenderer.setDrawXError(false);

		this.stepRenderer = new XYStepRenderer();
		this.stepRenderer.setBasePaint(Color.LIGHT_GRAY);
		this.stepRenderer.setAutoPopulateSeriesPaint(false);

		this.fitRectangleRenderer = new XYStepRenderer();
		this.fitRectangleRenderer.setAutoPopulateSeriesPaint(false);
		this.fitRenderer = new StandardXYItemRenderer();

		this.fitBlockAnnotations = new ArrayList<>();
	}

	/**
	 * Return the step renderer. This is the renderer who display the links
	 *  between the points.
	 *
	 * @return the renderer who display the links between the points.
	 */
	public XYStepRenderer getStepRenderer() {
		return stepRenderer;
	}

	/**
	 * Return the DataStepPoint. This is the dataset for the links between points.
	 *
	 * @return the dataset of the links between points.
	 */
	public XYSeriesCollection getDataStepPoint() {
		return dataStepPoint;
	}

	/**
	 * Return the FitRectangleDataset. This is the dataset for the selection of
	 * the fit.
	 *
	 * @return the dataset of the selection of the fit.
	 */
	public XYSeriesCollection getFitRectangleDataset() {
		return fitRectangleDataset;
	}

	/**
	 * Return the FitRectangleRenderer. This is the renderer for the selection
	 * of the fit.
	 *
	 * @return the the renderer of the selection of the fit.
	 */
	public XYStepRenderer getFitRectangleRenderer() {
		return fitRectangleRenderer;
	}

	/**
	 * Return the fit dataset.
	 *
	 * @return the dataset of the fit.
	 */
	public XYSeriesCollection getFitDataset() {
		return fitDataset;
	}

	/**
	 * Return the fit renderer.
	 *
	 * @return the fit renderer.
	 */
	public XYItemRenderer getFitRenderer() {
		return fitRenderer;
	}

	/**
	 * Return the {@link OpacityCorrectionModel}.
	 *
	 * @return the {@link OpacityCorrectionModel}.
	 */
	public OpacityCorrectionModel getOpacityCorrectionModel() {
		return opacityCorrectionModel;
	}

	/**
	 * Return the {@link RotationalFitModel}.
	 *
	 * @return the {@link RotationalFitModel}.
	 */
	public RotationalFitModel getRotationalFitModel() {
		return rotationalFitModel;
	}

	/**
	 * Return the {@link RotationalInfoModel}.
	 *
	 * @return the {@link RotationalInfoModel}.
	 */
	public RotationalInfoModel getRotationalInfoModel() {
		return rotationalInfoModel;
	}

	/**
	 * Reset the rotational.
	 */
	public void cleanCurrentResult() {
		rotationalInfoModel.removeAll();
		rotationalFitModel.reset();
		dataSeries.removeAllSeries();
		errorDataSeries.removeAllSeries();
		dataStepPoint.removeAllSeries();
		currentResult = null;
	}

	/**
	 * Construct the infoModel with the data.
	 *
	 * @param newResult The rotational diagram result.
	 */
	public void constructDataInfoModel(RotationalDiagramResult newResult) {
		for (RotationalDiagramMoleculeResult molRes : newResult.getResult()) {
			RotationalInfoMolecule rim = new RotationalInfoMolecule(
					molRes.getTag(), molRes.getMolName());
			for (RotationalDiagramComponentResult compRes : molRes.getComponentResultsList()) {
				RotationalInfoComponent ric = new RotationalInfoComponent(
						compRes.getComponentNumber(), compRes.getType());

				XYRotationalIntervalSeries serieData = getDataSerie(molRes.getTag(),
						compRes.getComponentNumber(), RotationalTypeCurve.DATA);
				boolean haveNonBlended = serieData != null && !serieData.getPoints().isEmpty();
				boolean haveBlended = !getDataStepSerie(molRes.getTag(), compRes.getComponentNumber()).isEmpty();
				RotationalCurvePanelModel rcpm = new RotationalCurvePanelModel(
						serieData, false, haveBlended, haveNonBlended);
				ric.addCurvePanelModel(rcpm);
				rim.addRotationalInfoComponent(ric);
			}
			rotationalInfoModel.addRotationalInfoMolecule(rim);
		}
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.opacity.OpacityInterface#updateInfoModelOpacityCorrection()
	 */
	@Override
	public void updateInfoModelOpacityCorrection() {
		for (int i = 1; i < dataSeries.getSeriesCount(); i++) {
			XYRotationalIntervalSeries s = (XYRotationalIntervalSeries) dataSeries.getSeries(i);
			if (s.getType() == RotationalTypeCurve.OPACITY_CORRECTION) {
				rotationalInfoModel.getRotationalInfoMolecule(s.getTag())
						.getComponent(s.getNumComponent()).addCurvePanelModel(
								new RotationalCurvePanelModel(s));
			}
		}
	}

	/**
	 * Return the data serie with the given parameters or null if not found.
	 *
	 * @param tag The tag.
	 * @param compNum The component number.
	 * @param type The {@link RotationalTypeCurve}.
	 * @return the data serie with the given parameters or null if not found.
	 */
	public XYRotationalIntervalSeries getDataSerie(int tag, int compNum, RotationalTypeCurve type) {
		XYRotationalIntervalSeries tmpSerie;
		for (int i = 0; i < dataSeries.getSeriesCount(); i++) {
			tmpSerie = (XYRotationalIntervalSeries) dataSeries.getSeries(i);
			if (tmpSerie.getTag() == tag && tmpSerie.getNumComponent() == compNum
					&& tmpSerie.getType() == type) {
				return tmpSerie;
			}
		}
		return null;
	}

	/**
	 * Return the list of {@link XYDataStepSeries} serie with the given parameters.
	 *
	 * @param tag The tag.
	 * @param compNum The component number.
	 * @return The list of datastep serie with the given parameters.
	 */
	public List<XYDataStepSeries> getDataStepSerie(int tag, int compNum) {
		List<XYDataStepSeries> list = new ArrayList<>();
		XYDataStepSeries tmpSerie;
		for (int i = 0; i < dataStepPoint.getSeriesCount(); i++) {
			tmpSerie = (XYDataStepSeries) dataStepPoint.getSeries(i);
			if (tmpSerie.getTag() == tag && tmpSerie.getNumComponent() == compNum) {
				list.add(tmpSerie);
			}
		}
		return list;
	}

	/**
	 * Return the {@link PointInformation}s of the given {@link RotationalTypeCurve}
	 *  at the given position.
	 *
	 * @param type The {@link RotationalTypeCurve} for which we want the points.
	 * @param xValue The x position value.
	 * @param yValue The y position value.
	 * @return The {@link PointInformation}s.
	 */
	private List<PointInformation> getPointsInformation(RotationalTypeCurve type,
			double xValue, double yValue) {
		List<PointInformation> list = new ArrayList<>();
		XYRotationalIntervalSeries tmpSerie;
		for (int i = 0; i < dataSeries.getSeriesCount(); i++) {
			tmpSerie = (XYRotationalIntervalSeries) dataSeries.getSeries(i);
			if (tmpSerie.getType() == type) {
				for (PointInformation point : tmpSerie.getPoints()) {
					if (point.getEup() == xValue && point.getLnNuOnGu() == yValue) {
						list.add(point);
					}
				}
			}
		}
		return list;
	}

	/**
	 * Clean old result then set the new result.
	 *
	 * @param newResult The rotational diagram result.
	 */
	public void setResult(RotationalDiagramResult newResult) {
		cleanCurrentResult();
		this.currentResult = newResult;
		buildXYSeries(newResult);
		constructDataInfoModel(newResult);
		configureFitMultiSelection(newResult);
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.opacity.OpacityInterface#removeOpacityCorrection()
	 */
	@Override
	public void removeOpacityCorrection() {
		List<XYRotationalIntervalSeries> serieToRemove = new ArrayList<>();
		for (int i = 0; i < dataSeries.getSeriesCount(); i++) {
			XYRotationalIntervalSeries s = (XYRotationalIntervalSeries)
					dataSeries.getSeries(i);
			if (RotationalTypeCurve.isOpacityCorrection(s.getType())) {
				serieToRemove.add(s);
			}
		}
		for (XYRotationalIntervalSeries series : serieToRemove) {
			dataSeries.removeSeries(series);
			errorDataSeries.removeSeries(series);
		}
		rotationalInfoModel.removeOpacityCorrectionSeries();
	}

	/**
	 * Create then add to the datastep dataset a diff eup line series.
	 *
	 * @param xListLine The start points.
	 * @param x2ListLine The end points.
	 * @param tag The tag.
	 * @param numComp The component number.
	 * @param color The color of the the associated bended series.
	 */
	private void createLineEupDiff(List<Double> xListLine, List<Double> x2ListLine,
			int tag, int numComp, Color color) {
		if (!xListLine.isEmpty()) {
			Color topColor = color.darker().darker();
			int red = topColor.getRed();
			int green = topColor.getGreen();
			int blue = topColor.getBlue();
			int grayScaleIntensity = (int) (0.299 * red + 0.587 * green + 0.114 * blue + .5);
			Color colorLinks = new Color(grayScaleIntensity).brighter();

			for (int i = 0; i < xListLine.size(); i = i + 2) {
				// Serie of points to create the line in order to link pointsDiff
				// with its correspondant point
				XYDataStepSeries pointDiffEupLine = new XYDataStepSeries(
						"LineDiffEup_" + tag + '_' + numComp + '_' + i, tag, numComp);
				pointDiffEupLine.add(xListLine.get(i), xListLine.get(i + 1));
				pointDiffEupLine.add(x2ListLine.get(i), x2ListLine.get(i + 1));
				dataStepPoint.addSeries(pointDiffEupLine);
				int index = dataStepPoint.indexOf(pointDiffEupLine.getKey());
				pointDiffEupLine.getConfigCurve().setColor(colorLinks);
				pointDiffEupLine.getConfigCurve().setVisible(true);
				stepRenderer.setSeriesVisible(index, true);
				stepRenderer.setSeriesPaint(index, colorLinks);
			}
		}
	}

	/**
	 * Create and add the series for the rotational diagram result.
	 *
	 * @param newResult The result.
	 */
	public void buildXYSeries(RotationalDiagramResult newResult) {
		for (RotationalDiagramMoleculeResult mr : newResult.getResult()) {
			for (RotationalDiagramComponentResult cr : mr.getComponentResultsList()) {
				createResultAndResultDiff(mr.getTag(), cr.getComponentNumber(),
						"Points_" + mr.getTag() + "_" + cr.getComponentNumber(),
						"PointsDiff_" + mr.getTag() + "_" + cr.getComponentNumber(),
						"Original", "OriginalDiff",
						RotationalTypeCurve.DATA, RotationalTypeCurve.DATA_EUP_DIFF,
						cr.getResult(), cr.getResultEupDiff(),
						ColorsCurve.getNewColorRotationalData());
			}
		}
	}

	/**
	 * Extract blended {@link PointInformation} from the given List and return
	 *  the List of the blended {@link PointInformation}.
	 *
	 * @param result The list from which extract blended {@link PointInformation}.
	 * @return The list of blended {@link PointInformation}.
	 */
	private List<PointInformation> extractBlended(List<PointInformation> result) {
		List<PointInformation> blended = new ArrayList<>();
		Iterator<PointInformation> it = result.iterator();
		while (it.hasNext()) {
			PointInformation point = it.next();
			if (point.isBlendedLine()) {
				blended.add(point);
				it.remove();
			}
		}
		return blended;
	}

	/**
	 * Get the x/y and compute the sigma value for the blended point from Result
	 *  then add them on the series and List.
	 *
	 * @param blendedPoints The blended points.
	 * @param xListLine The list.
	 * @param series The series.
	 */
	private void createBlendedPointResult(List<PointInformation> blendedPoints,
			List<Double> xListLine, XYRotationalIntervalSeries series) {
		for (PointInformation pointCurrent : blendedPoints) {
			// determine the x, the y, the interval min and the interval max
			double x = pointCurrent.getEup();
			double y = pointCurrent.getLnNuOnGu();
			// calculation of sigma because the interval error must be equals
			double sigma = pointCurrent.getDeltaWCalc() / pointCurrent.getWCalc();
			series.addPointSigma(new Point(x, y, sigma));

			// insert into points the coordinates of the point
			series.add(x, x - 1, x + 1, y, y-sigma, y+sigma);

			// add points to create the line between the points with
			// different eup
			while (pointCurrent.getManyPoints() > 0) {
				xListLine.add(x);
				xListLine.add(y);
				pointCurrent.setManyPoints(pointCurrent.getManyPoints() - 1);
			}
		}
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.opacity.OpacityInterface#createResultAndResultDiff(int, int, java.lang.String, java.lang.String, java.lang.String, java.lang.String, eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalTypeCurve, eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalTypeCurve, java.util.List, java.util.List, java.awt.Color)
	 */
	@Override
	public void createResultAndResultDiff(int tag, int compNum, String keySerie1,
			String keySerie2, String realNameS1, String realNameS2,
			RotationalTypeCurve rtcS1, RotationalTypeCurve rtcS2,
			List<PointInformation> result, List<PointInformation> resultEupDiff,
			Color baseColor) {
		List<Double> xListLine = new ArrayList<>();
		List<Double> x2ListLine = new ArrayList<>();

		// Note, blended contains only the point from result here...
		List<PointInformation> blended = extractBlended(result);

		XYRotationalIntervalSeries series = new XYRotationalIntervalSeries(
				keySerie1, tag, compNum, realNameS1, result, rtcS1);
		series.getConfigCurve().setColor(baseColor);

		List<PointInformation> pointsDiffSerie = new ArrayList<>(blended);
		pointsDiffSerie.addAll(resultEupDiff);
		XYRotationalIntervalSeries seriesDiff = new XYRotationalIntervalSeries(
				keySerie2, tag, compNum, realNameS2, pointsDiffSerie, rtcS2);
		seriesDiff.getConfigCurve().setColor(baseColor.brighter().brighter());

		createPointResult(result, series);

		createBlendedPointResult(blended, xListLine, seriesDiff);

		createPointsResultDiff(resultEupDiff, x2ListLine, seriesDiff);

		createLineEupDiff(xListLine, x2ListLine, tag, compNum,
				seriesDiff.getConfigCurve().getColor());

		addDataSerie(series);
		addDataSerie(seriesDiff);
	}

	/**
	 * Add and configure a data series.
	 *
	 * @param series The series to add.
	 */
	private void addDataSerie(XYRotationalIntervalSeries series) {
		dataSeries.addSeries(series);
		errorDataSeries.addSeries(series);
		int index = dataSeries.indexOf(series.getKey());
		dataRenderer.setSeriesPaint(index, series.getConfigCurve().getColor());
		dataRenderer.setSeriesVisible(index, series.getConfigCurve().isVisible());
		int indexError = errorDataSeries.indexOf(series.getKey());
		errorDataRenderer.setSeriesPaint(indexError, series.getConfigCurve().getColor());
		errorDataRenderer.setSeriesVisible(indexError, series.getConfigCurve().isVisible());
	}

	/**
	 * Get the x/y and compute the sigma value for the points diff then add them
	 *  on the series and List.
	 *
	 * @param resultEupDiff The point list.
	 * @param x2ListLine An empty list who will be completed.
	 * @param seriesDiff The points diff series.
	 */
	private void createPointsResultDiff(List<PointInformation> resultEupDiff,
			List<Double> x2ListLine, XYRotationalIntervalSeries seriesDiff) {
		for (PointInformation pointCurrent : resultEupDiff) {
			// determine the the x, y and sigma.
			double x = pointCurrent.getEup();
			double y = pointCurrent.getLnNuOnGu();
			double sigma = pointCurrent.getDeltaWCalc() / pointCurrent.getWCalc();
			seriesDiff.addPointSigma(new Point(x, y, sigma));

			// insert into pointsDiff the coordinates of the interval
			seriesDiff.add(x, x - 1, x + 1, y, y - sigma, y + sigma);
			x2ListLine.add(x);
			x2ListLine.add(y);
		}
	}

	/**
	 * Compute the sigma for the points then add the points to the series and
	 *  to the {@link List}.
	 *
	 * @param result The points.
	 * @param xListLine The list.
	 * @param series The series.
	 */
	private void createPointResult(List<PointInformation> result,
			XYRotationalIntervalSeries series) {
		for (PointInformation pointCurrent : result) {
			// determine the x, the y, the interval min and the interval max
			double x = pointCurrent.getEup();
			double y = pointCurrent.getLnNuOnGu();
			// calculation of sigma because the interval error must be
			// equals
			double sigma = pointCurrent.getDeltaWCalc() / pointCurrent.getWCalc();
			series.addPointSigma(new Point(x, y, sigma));

			// insert into points the coordinates of the point
			series.add(x, x - 1, x + 1, y, y-sigma, y+sigma);
		}
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.fit.FitInterface#getPointList()
	 */
	@Override
	public List<Point> getPointList() {
		List<Point> points = new ArrayList<>();
		for (int i = 0; i < dataSeries.getSeriesCount(); i++) {
			XYRotationalIntervalSeries series = (XYRotationalIntervalSeries) dataSeries.getSeries(i);
			if (series.isReallyVisible()) {
				points.addAll(series.getPointSigmaList());
			}
		}
		return points;
	}

	/**
	 * Return the data dataset
	 *
	 * @return the data dataset.
	 */
	@Override
	public XYIntervalSeriesCollection getDataSeries() {
		return dataSeries;
	}

	/**
	 * Return the error data series.
	 *
	 * @return the error data series.
	 */
	public XYIntervalSeriesCollection getErrorDataSeries() {
		return errorDataSeries;
	}

	/**
	 * Return the data renderer.
	 *
	 * @return the data renderer.
	 */
	public XYDotRenderer getDataRenderer() {
		return dataRenderer;
	}

	/**
	 * Return the error data renderer.
	 *
	 * @return the error data renderer.
	 */
	public XYErrorRenderer getErrorDataRenderer() {
		return errorDataRenderer;
	}


	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.opacity.OpacityInterface#getCurrentResult()
	 */
	@Override
	public RotationalDiagramResult getCurrentResult() {
		return currentResult;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.opacity.OpacityInterface#getColorDataSerie(int, int)
	 */
	@Override
	public Color getColorDataSerie(int tag, int compNum) {
		return getDataSerie(tag, compNum, RotationalTypeCurve.DATA).getConfigCurve().getColor();
	}

	/**
	 * Return a String with the information of points at the position x/y.
	 *
	 * @param xValue The x position value.
	 * @param yValue The y position value.
	 * @return a String with the information of points at the position x/y.
	 */
	public String getNearestLineInfo(double xValue, double yValue) {
		StringBuilder sb = new StringBuilder();

		for (PointInformation point : getPointsInformation(
				RotationalTypeCurve.DATA, xValue, yValue)) {
			if (point.isMultipleted()){
				sb.append("MULTIPLET DATA: ").append(point.getPopupInfo()).append('\n');
			}else {
				sb.append("DATA: ").append(point.getPopupInfo()).append('\n');
			}

		}
		for (PointInformation point : getPointsInformation(
				RotationalTypeCurve.DATA_EUP_DIFF, xValue, yValue)) {
			sb.append("BLENDED DATA: ").append(point.getPopupInfo()).append('\n');
		}
		for (PointInformation point : getPointsInformation(
				RotationalTypeCurve.OPACITY_CORRECTION, xValue, yValue)) {
			sb.append("OPACITY CORRECTION: ").append(point.getPopupInfo()).append('\n');
		}
		for (PointInformation point : getPointsInformation(
				RotationalTypeCurve.OPACITY_CORRECTION_DIFF, xValue, yValue)) {
			sb.append("BLENDED OPACITY CORRECTION: ").append(point.getPopupInfo()).append('\n');
		}
		return sb.toString();
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.fit.FitInterface#removeAllFitRectangleSeries()
	 */
	@Override
	public void removeAllFitRectangleSeries() {
		fitRectangleDataset.removeAllSeries();
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.fit.FitInterface#removeFitRectangleSerie(eu.omp.irap.cassis.gui.plot.rotdiagram.curve.XYSeriesFitBorder)
	 */
	@Override
	public void removeFitRectangleSerie(XYSeriesFitBorder series) {
		fitRectangleDataset.removeSeries(series);
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.fit.FitInterface#addFitRectangleSerie(eu.omp.irap.cassis.gui.plot.rotdiagram.curve.XYSeriesFitBorder)
	 */
	@Override
	public void addFitRectangleSerie(XYSeriesFitBorder series) {
		fitRectangleDataset.addSeries(series);
		fitRectangleDataset.setAutoWidth(true);
		int index = fitRectangleDataset.indexOf(series);
		fitRectangleRenderer.setSeriesPaint(index, series.getConfigCurve().getColor());
		fitRectangleRenderer.setSeriesVisible(index, true);
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.fit.FitInterface#removeAllFitSeries()
	 */
	@Override
	public void removeAllFitSeries() {
		fitDataset.removeAllSeries();
		rotationalInfoModel.removeFitSeries();
		fireDataChanged(new ModelChangedEvent(REMOVE_ANNOTATIONS_EVENT));
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.fit.FitInterface#addFitSeries(java.util.List)
	 */
	@Override
	public void addFitSeries(List<XYSeriesFitArea> fitSeries) {
		for (XYSeriesFitArea aSerie : fitSeries) {
			if (!rotationalFitModel.isMultiSelectionAllowed()) {
				Color color = getDataSerie(aSerie.getTag(), aSerie.getNumComponent(),
						aSerie.getSourceType()).getConfigCurve().getColor();
				aSerie.setColor(color);
			}
			fitDataset.addSeries(aSerie);
			int index = fitDataset.indexOf(aSerie);
			fitRenderer.setSeriesPaint(index, aSerie.getConfigCurve().getColor());
			fitRenderer.setSeriesVisible(index, true);

			RotationalCurvePanelModel rcpm = new RotationalCurvePanelModel(aSerie, true, false, false);
			rcpm.setAnnotationDisplayed(true);
			rotationalInfoModel.getRotationalInfoMolecule(aSerie.getTag()).getComponent(
					aSerie.getNumComponent()).addCurvePanelModel(rcpm);
		}
		fireDataChanged(new ModelChangedEvent(REFRESH_ANNOTATIONS_EVENT));
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.fit.FitInterface#setFitSelectionVisible(boolean)
	 */
	@Override
	public void setFitSelectionVisible(boolean visible) {
		for (int i = 0; i < fitRectangleDataset.getSeriesCount(); i++) {
			fitRectangleRenderer.setSeriesVisible(i, visible);
		}
	}

	/**
	 * Return the informations needed to do an ascii save.
	 *
	 * @return the informations needed to do a ascii save.
	 */
	public List<XYRotationalIntervalSeries> getInfoToSave() {
		List<XYRotationalIntervalSeries> series = new ArrayList<>();
		for (int i = 0; i < dataSeries.getSeriesCount(); i++) {
			XYRotationalIntervalSeries serie = (XYRotationalIntervalSeries) dataSeries.getSeries(i);
			series.add(serie);
		}
		return series;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.fit.FitInterface#hideAllFitSeries()
	 */
	@Override
	public void hideAllFitSeries() {
		int nb = fitDataset.getSeriesCount();
		for (int i = 0; i < nb; i++) {
			XYSeriesFitArea series = (XYSeriesFitArea) fitDataset.getSeries(i);
			series.getConfigCurve().setVisible(false);
		}
		rotationalInfoModel.updateFitSeries();
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.fit.FitInterface#addBlockAnnotation(eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalTextAnnotation)
	 */
	@Override
	public void addBlockAnnotation(RotationalTextAnnotation annotation) {
		fitBlockAnnotations.add(annotation);
		fireDataChanged(new ModelChangedEvent(ADD_BLOCK_ANNOTATION_EVENT, annotation));
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.fit.FitInterface#removeBlockAnnotations(int)
	 */
	@Override
	public void removeBlockAnnotations(int blockNumber) {
		Iterator<RotationalTextAnnotation> iterator = fitBlockAnnotations.iterator();
		while (iterator.hasNext()) {
			RotationalTextAnnotation annotation = iterator.next();
			if (annotation.getBlockNumber() == blockNumber) {
				iterator.remove();
				fireDataChanged(new ModelChangedEvent(REMOVE_BLOCK_ANNOTATION_EVENT, annotation));
			}
		}
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.fit.FitInterface#removeBlockAnnotations()
	 */
	@Override
	public void removeBlockAnnotations() {
		fitBlockAnnotations.clear();
		fireDataChanged(new ModelChangedEvent(REMOVE_ALL_BLOCK_ANNOTATIONS_EVENT));
	}

	/**
	 * Return all block annotations with the given number.
	 *
	 * @param blockNumber The number for which we want the annotations.
	 * @return the annotations with the given number.
	 */
	public List<RotationalTextAnnotation> getBlockAnnotations(int blockNumber) {
		List<RotationalTextAnnotation> listAnnotation = new ArrayList<>();
		for (RotationalTextAnnotation annotation : fitBlockAnnotations) {
			if (annotation.getBlockNumber() == blockNumber) {
				listAnnotation.add(annotation);
			}
		}
		return listAnnotation;
	}

	/**
	 * Return all block annotations.
	 *
	 * @return all block annotations.
	 */
	public List<RotationalTextAnnotation> getBlockAnnotations() {
		return Collections.unmodifiableList(fitBlockAnnotations);
	}

	/**
	 * Check in the result if there is more than one component.
	 * If it is the case, disallow the multiselection on the fit. Else, allow it.
	 *
	 * @param result The result.
	 */
	private void configureFitMultiSelection(RotationalDiagramResult result) {
		boolean allowMultiSelection = true;
		if (result.getResult().size() > 1 ||
				result.getResult().get(0).getComponentResultsList().size() > 1) {
			allowMultiSelection = false;
		}
		rotationalFitModel.setMultiSelectionAllowed(allowMultiSelection);
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.fit.FitInterface#computeSelection()
	 */
	@Override
	public void computeSelection() {
		FitRotationalRectangle.reset();
		for (int i = 0; i < dataSeries.getSeriesCount(); i++) {
			XYRotationalIntervalSeries series = (XYRotationalIntervalSeries)
					dataSeries.getSeries(i);
			if (series.isReallyVisible()) {
				for (PointInformation point : series.getPoints()) {
					double x = point.getEup();
					double y = point.getLnNuOnGu();
					FitRotationalRectangle.setLeftBig(Math.min(x, FitRotationalRectangle.getLeftBig()));
					FitRotationalRectangle.setUpBig(Math.max(y, FitRotationalRectangle.getUpBig()));
					FitRotationalRectangle.setRightBig(Math.max(x, FitRotationalRectangle.getRightBig()));
					FitRotationalRectangle.setDownBig(Math.min(y, FitRotationalRectangle.getDownBig()));
				}
			}
		}
	}

	@Override
	public boolean haveCurrentFitSelection(){
		return rotationalFitModel.getNbSelection() > 0 ||
		rotationalFitModel.getNbCurrentSelection() > 0 ;
	}
}
