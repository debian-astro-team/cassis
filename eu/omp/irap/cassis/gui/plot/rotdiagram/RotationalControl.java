/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram;

import java.awt.Color;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import javax.swing.JFileChooser;

import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.chart.renderer.xy.XYErrorRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYStepRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.Point;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalCurveModelSerieInterface;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalCurvePanelModel;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalSerieCassisListener;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalTextAnnotation;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalTypeCurve;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.XYDataStepSeries;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.XYRotationalIntervalSeries;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.XYSeriesFitArea;
import eu.omp.irap.cassis.properties.Software;

/**
 * Control of the Rotational.
 *
 * @author M. Boiziot
 */
public class RotationalControl implements RotationalSerieCassisListener, ModelListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(RotationalControl.class);

	private RotationalView view;
	private RotationalModel model;


	/**
	 * Constructor.
	 *
	 * @param view The view.
	 * @param model The model.
	 */
	public RotationalControl(RotationalView view, RotationalModel model) {
		this.model = model;
		this.view = view;
		this.model.addModelListener(this);
	}

	/**
	 * Return the view.
	 *
	 * @return the view.
	 */
	public RotationalView getView() {
		return view;
	}

	/**
	 * Return the model.
	 *
	 * @return the model.
	 */
	public RotationalModel getModel() {
		return model;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalSerieCassisListener#rotationalSerieChange(eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalCurvePanelModel, boolean)
	 */
	@Override
	public void rotationalSerieChange(RotationalCurvePanelModel obj, boolean visibilityChange) {
		RotationalCurveModelSerieInterface serie = obj.getSeries();
		if (serie.getType() == RotationalTypeCurve.DATA) {
			handleTwoLevelSerieChange(serie, RotationalTypeCurve.DATA,
					RotationalTypeCurve.DATA_EUP_DIFF, obj, true, visibilityChange);
		} else if (serie.getType() == RotationalTypeCurve.OPACITY_CORRECTION) {
			handleTwoLevelSerieChange(serie, RotationalTypeCurve.OPACITY_CORRECTION,
					RotationalTypeCurve.OPACITY_CORRECTION_DIFF, obj, false,
					visibilityChange);
		} else if (serie.getType() == RotationalTypeCurve.FIT) {
			XYItemRenderer renderer = model.getFitRenderer();
			int index = model.getFitDataset().indexOf(serie.getKey());
			renderer.setSeriesPaint(index, serie.getConfigCurve().getColor());
			renderer.setSeriesVisible(index, serie.getConfigCurve().isVisible());
		}

		if (obj.haveAnnotation()) {
			annotationDisplayChange(obj);
		}
	}

	/**
	 * Process the change on a series with a "sublevel" like data or opacity correction.
	 *
	 * @param serieBase The base serie (or top serie).
	 * @param serieType The {@link RotationalTypeCurve} of the "topseries".
	 * @param subSerieType The {@link RotationalTypeCurve} of the "subseries".
	 * @param obj The {@link RotationalCurvePanelModel} associated with the series to change.
	 * @param updateLinks if the links must be updated.
	 * @param visibilityChange if it's a change of visibility. (will remove the fit)
	 */
	private void handleTwoLevelSerieChange(RotationalCurveModelSerieInterface serieBase,
			RotationalTypeCurve serieType, RotationalTypeCurve subSerieType,
			RotationalCurvePanelModel obj, boolean updateLinks, boolean visibilityChange) {
		XYDotRenderer renderer = model.getDataRenderer();
		XYErrorRenderer errorRenderer = model.getErrorDataRenderer();

		XYRotationalIntervalSeries series = model.getDataSerie(serieBase.getTag(),
				serieBase.getNumComponent(), serieType);
		series.setVisible(obj.areNonBlendedDisplayed());
		int indexSerie = model.getDataSeries().indexOf(serieBase.getKey());
		int indexErrorSerie = model.getErrorDataSeries().indexOf(serieBase.getKey());
		renderer.setSeriesPaint(indexSerie, serieBase.getConfigCurve().getColor());
		renderer.setSeriesVisible(indexSerie, obj.areNonBlendedDisplayed());
		errorRenderer.setSeriesPaint(indexErrorSerie, serieBase.getConfigCurve().getColor());
		errorRenderer.setSeriesVisible(indexErrorSerie, obj.areNonBlendedDisplayed());

		XYRotationalIntervalSeries subSerie = model.getDataSerie(serieBase.getTag(),
				serieBase.getNumComponent(), subSerieType);
		if (subSerie != null) {
			subSerie.getConfigCurve().setColor(
					serieBase.getConfigCurve().getColor().brighter().brighter());
			subSerie.getConfigCurve().setVisible(obj.areBlendedDisplayed());

			int indexSerieDiff = model.getDataSeries().indexOf(subSerie.getKey());
			int indexErrorSerieDiff = model.getErrorDataSeries().indexOf(subSerie.getKey());
			renderer.setSeriesPaint(indexSerieDiff, subSerie.getConfigCurve()
					.getColor());
			renderer.setSeriesVisible(indexSerieDiff, obj.areBlendedDisplayed());
			errorRenderer.setSeriesPaint(indexErrorSerieDiff, subSerie.getConfigCurve()
					.getColor());
			errorRenderer.setSeriesVisible(indexErrorSerieDiff, obj.areBlendedDisplayed());

			if (updateLinks) {
				linksDisplayChange(obj);
			}
		}

		if (visibilityChange) {
			model.getRotationalFitModel().resetAllSelections();
			model.getRotationalFitModel().removeAllFitSeries();
		}
	}

	/**
	 * Change the display of links.
	 *
	 * @param obj The {@link RotationalCurvePanelModel} associated with the series to change.
	 */
	private void linksDisplayChange(RotationalCurvePanelModel obj) {
		boolean visible = obj.areBlendedDisplayed();
		Color color = obj.getSeries().getConfigCurve().getColor();
		int red = color.getRed();
		int green = color.getGreen();
		int blue = color.getBlue();
		int grayScaleIntensity = (int) (0.299 * red + 0.587 * green + 0.114 * blue + .5);
		Color colorLinks = new Color(grayScaleIntensity).brighter();
		List<XYDataStepSeries> linksSeries = model.getDataStepSerie(
				obj.getTag(), obj.getNumComponent());
		if (!linksSeries.isEmpty()) {
			XYStepRenderer stepRenderer = model.getStepRenderer();
			for (XYDataStepSeries series : linksSeries) {
				series.getConfigCurve().setVisible(visible);
				series.getConfigCurve().setColor(colorLinks);
				int index = model.getDataStepPoint().indexOf(series);
				stepRenderer.setSeriesVisible(index, visible);
				stepRenderer.setSeriesPaint(index, colorLinks);
			}
		}
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalSerieCassisListener#annotationDisplayChange(eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalCurvePanelModel)
	 */
	@Override
	public void annotationDisplayChange(RotationalCurvePanelModel obj) {
		if (obj.getSeries() instanceof XYSeriesFitArea) {
			XYSeriesFitArea s = (XYSeriesFitArea) obj.getSeries();
			s.getLineInformation().setColor(s.getConfigCurve().getColor());
			s.getLineInformation().setVisible(obj.isAnnotationDisplayed());
		}
		view.refreshFitAnnotations();
	}

	/**
	 * @see eu.omp.irap.cassis.common.events.ModelListener#dataChanged(eu.omp.irap.cassis.common.events.ModelChangedEvent)
	 */
	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (RotationalModel.REMOVE_ANNOTATIONS_EVENT.equals(event.getSource())) {
			view.removeAllFitAnnotations();
		} else if (RotationalModel.REFRESH_ANNOTATIONS_EVENT.equals(event.getSource())) {
			view.refreshFitAnnotations();
		} else if (RotationalModel.ADD_BLOCK_ANNOTATION_EVENT.equals(event.getSource())) {
			RotationalTextAnnotation annotation = (RotationalTextAnnotation)
					event.getValue();
			view.getPlot().addAnnotation(annotation);
		} else if (RotationalModel.REMOVE_BLOCK_ANNOTATION_EVENT.equals(event.getSource())) {
			RotationalTextAnnotation annotation = (RotationalTextAnnotation)
					event.getValue();
			view.getPlot().removeAnnotation(annotation);
		} else if (RotationalModel.REMOVE_ALL_BLOCK_ANNOTATIONS_EVENT.equals(event.getSource())) {
			view.getPlot().clearAnnotations();
		}
	}

	public boolean saveValue() {
		CassisJFileChooser fc = new CassisJFileChooser(Software.getDataPath(),
				Software.getLastFolder("rotational-data-csv"));
		fc.setDialogTitle("Save your Rotational Data :");
		fc.setDialogType(JFileChooser.SAVE_DIALOG);
		fc.resetChoosableFileFilters();

		if (fc.showSaveDialog(view) == JFileChooser.APPROVE_OPTION){
			Software.setLastFolder("rotational-data-csv", fc.getCurrentDirectory().getAbsolutePath());
			int nbSeries = model.getDataSeries().getSeriesCount();
			final String absolutePathFile = fc.getSelectedFile().getAbsolutePath();
			String name = absolutePathFile;
			int lastIndexOfPoint = absolutePathFile.lastIndexOf('.');
			String ext = ".csv";
			String path = absolutePathFile;
			if (lastIndexOfPoint != -1){
				ext =absolutePathFile.substring(lastIndexOfPoint, absolutePathFile.length());
				path = absolutePathFile.substring(0, lastIndexOfPoint);
			}
			int ind = 0;
			for (int i = 0; i <nbSeries; i++) {
				 XYRotationalIntervalSeries serie = (XYRotationalIntervalSeries) model.getDataSeries().getSeries(i);
				 if (serie.isReallyVisible() && serie.getItemCount() > 0){
					ind++;
					if (nbSeries != 1){
						 	name = path +  "_"+ String.valueOf(ind) + ext;
					}
					 File file = new File(name);

					 try (BufferedWriter fileAscii = new BufferedWriter(new FileWriter(file))) {
						fileAscii.write("Eup (K)"+"\t"+ "Ln(Nu/gu)"+ "\t" +"err(Ln(Nu/gu))");

						for (Point p : serie.getPointSigmaList()){
							fileAscii.newLine();
							fileAscii.write(p.getX() + "\t");
							fileAscii.write(p.getY() + "\t");
							fileAscii.write(String.valueOf(p.getSigma()));
							fileAscii.flush();
						}
						fileAscii.newLine();
					} catch (IOException e) {
						LOGGER.error("Error during the save", e);
						return false;
					}
				 }
			}
			return true;
		}
		return false;
	}
}
