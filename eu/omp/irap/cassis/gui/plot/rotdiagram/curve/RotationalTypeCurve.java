/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram.curve;

/**
 * Enumeration of all kinds of Rotational series.
 *
 * @author M. Boiziot
 */
public enum RotationalTypeCurve {

	DATA, DATA_EUP_DIFF, OPACITY_CORRECTION, OPACITY_CORRECTION_DIFF, FIT, FIT_COMPO;


	/**
	 * Determine whether {@link RotationalTypeCurve} is a data {@link RotationalTypeCurve}.
	 *
	 * @param rtc The {@link RotationalTypeCurve} to check.
	 * @return if the provided {@link RotationalTypeCurve} is a Data type.
	 */
	public static boolean isData(RotationalTypeCurve rtc) {
		return rtc == DATA || rtc == DATA_EUP_DIFF;
	}

	/**
	 * Determine whether {@link RotationalTypeCurve} is an opacity correction
	 * {@link RotationalTypeCurve}.
	 *
	 * @param rtc The {@link RotationalTypeCurve} to check.
	 * @return if the provided {@link RotationalTypeCurve} is an opacity correction type.
	 */
	public static boolean isOpacityCorrection(RotationalTypeCurve rtc) {
		return rtc == OPACITY_CORRECTION || rtc == OPACITY_CORRECTION_DIFF;
	}

	/**
	 * Check if two {@link RotationalTypeCurve} are the same without taking into consideration
	 * the diff subtype.
	 *
	 * @param type1 The first {@link RotationalTypeCurve} to compare.
	 * @param type2 The second {@link RotationalTypeCurve} to compare.
	 * @return if the two {@link RotationalTypeCurve} provided are the same general type.
	 */
	public static boolean isCompatible(RotationalTypeCurve type1, RotationalTypeCurve type2) {
		if (type1 == type2) {
			return true;
		}
		if (RotationalTypeCurve.isData(type1) && RotationalTypeCurve.isData(type2)) {
			return true;
		}
		if (RotationalTypeCurve.isOpacityCorrection(type1) &&
				RotationalTypeCurve.isOpacityCorrection(type2)) {
			return true;
		}
		return false;
	}

	/**
	 * Return the top {@link RotationalTypeCurve} of a provided
	 * {@link RotationalTypeCurve}. (=The type witouth the diff).
	 *
	 * @param type The {@link RotationalTypeCurve} for which we want the top type.
	 * @return The top {@link RotationalTypeCurve}.
	 */
	public static RotationalTypeCurve getTopType(RotationalTypeCurve type) {
		if (RotationalTypeCurve.isData(type)) {
			return DATA;
		} else if (RotationalTypeCurve.isOpacityCorrection(type)) {
			return OPACITY_CORRECTION;
		} else {
			return type;
		}
	}

	/**
	 * Return a friendly name for a {@link RotationalTypeCurve}.
	 *
	 * @param t The type for which we want a friendly name.
	 * @return The friendly name of the {@link RotationalTypeCurve}.
	 */
	public static String getFriendlyName(RotationalTypeCurve t) {
		String r;
		switch (t) {
			case DATA:
			case DATA_EUP_DIFF:
				r = "Original";
				break;
			case OPACITY_CORRECTION:
			case OPACITY_CORRECTION_DIFF:
				r = "Opa. Co.";
				break;
			case FIT:
			case FIT_COMPO:
				r = "Fit";
				break;
			default:
				r = "Unknow";
				break;
		}
		return r;
	}
}
