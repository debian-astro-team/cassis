/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram.curve;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import eu.omp.irap.cassis.gui.plot.curve.JOptionPaneCurve;
import eu.omp.irap.cassis.gui.plot.curve.config.ConfigCurve;

public class RotationalCurvePanelView extends JPanel {

	private static final long serialVersionUID = -5949867759995834145L;
	private RotationalCurvePanelControl control;
	private JButton buttonColor;
	private JCheckBox checkBox;
	private JCheckBox annotationCheckBox;
	private JCheckBox blendedCheckBox;
	private JCheckBox nonBlendedCheckBox;
	private boolean displayCurveOptions;


	public RotationalCurvePanelView(RotationalCurvePanelModel model) {
		this(model, true);
	}

	public RotationalCurvePanelView(RotationalCurvePanelModel model, boolean displayCurveOptions) {
		super();
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		this.control = new RotationalCurvePanelControl(model, this);
		this.displayCurveOptions = displayCurveOptions;

		JPanel centralPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		centralPanel.add(getCheckBox());
		centralPanel.add(getButtonColor(this));

		if (getNameFromModel().length() > 30) {
			String tmpName = getNameFromModel();
			String name = "..." + tmpName.substring(tmpName.length() - 26);
			JLabel jLabel = new JLabel(name);
			jLabel.setToolTipText(getNameFromModel());
			centralPanel.add(jLabel);
		} else {
			JLabel jLabel = new JLabel(getNameFromModel());
			jLabel.setToolTipText(getNameFromModel());
			centralPanel.add(jLabel);
		}
		this.add(centralPanel);

		Dimension dim = new Dimension(Short.MAX_VALUE, 32);
		if (control.getModel().haveAnnotation() ||
				(control.getModel().haveBlended() && control.getModel().haveNonBlended())) {
			JPanel southPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
			southPanel.add(new JLabel("    "));
			if (control.getModel().haveAnnotation()) {
				southPanel.add(getAnnotationCheckBox());
			}
			if (control.getModel().haveNonBlended() && control.getModel().haveBlended()) {
				southPanel.add(getNonBlendedCheckBox());
				southPanel.add(getBlendedCheckBox());
			}
			this.add(southPanel);
			dim.height += 32;
		}
		this.setMaximumSize(dim);
	}

	public JCheckBox getBlendedCheckBox() {
		if (blendedCheckBox == null) {
			blendedCheckBox = new JCheckBox("Blended");
			blendedCheckBox.setSelected(control.getModel().getBlendedDisplayedValue());
			blendedCheckBox.setToolTipText("Transitions with close frequencies but different Eup.");
			blendedCheckBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.getModel().setBlendedDisplayed(blendedCheckBox.isSelected());
				}
			});
		}
		return blendedCheckBox;
	}

	public JCheckBox getNonBlendedCheckBox() {
		if (nonBlendedCheckBox == null) {
			nonBlendedCheckBox = new JCheckBox("Non-blended");
			nonBlendedCheckBox.setToolTipText(
					"Lines with a single frequency and Eup or with multiple frequencies but same Eup.");
			nonBlendedCheckBox.setSelected(control.getModel().getNonBlendedDisplayedValue());
			nonBlendedCheckBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.getModel().setNonBlendedDisplayed(nonBlendedCheckBox.isSelected());
				}
			});
		}
		return nonBlendedCheckBox;
	}

	public JCheckBox getAnnotationCheckBox() {
		if (annotationCheckBox == null) {
			annotationCheckBox = new JCheckBox("Annotation");
			annotationCheckBox.setSelected(control.getModel().getAnnotationDisplayedValue());
			annotationCheckBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.getModel().setAnnotationDisplayed(annotationCheckBox.isSelected());
				}
			});
		}
		return annotationCheckBox;
	}

	public boolean isDisplayCurveOptions() {
		return displayCurveOptions;
	}

	public final RotationalCurvePanelControl getControl() {
		return control;
	}

	public JCheckBox getCheckBox() {
		if (checkBox == null) {
			checkBox = new JCheckBox("", control.getModel().getSeries().getConfigCurve().isVisible());
			checkBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.handleCheckBoxAction();
				}
			});
		}
		return checkBox;
	}

	protected JButton getButtonColor(final JPanel panel) {
		if (buttonColor == null) {
			buttonColor = new JButton();
			buttonColor.setBackground(control.getModel().getSeries().getConfigCurve().getColor());

			buttonColor.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					RotationalCurveModelSerieInterface seriesCassis = control.getModel().getSeries();

					JOptionPaneCurve optionPane = new JOptionPaneCurve(seriesCassis.getConfigCurve(),
							isDisplayCurveOptions(),false);
					optionPane.setColor(buttonColor.getBackground());
					JDialog dialog = optionPane.createDialog(panel, "Change the curve parameters ");
					dialog.setVisible(true);

					Object val = optionPane.getValue();
					if ("OK".equals(val)) {
						Color newColor = optionPane.getColor();
						if (newColor != null) {
							seriesCassis.getConfigCurve().setColor(newColor);
							buttonColor.setBackground(newColor);
						}

						optionPane.validateParameters();
						control.fireCurveCassisChanged(control.getModel(), false);
					}
				}
			});
		}
		return buttonColor;
	}

	public JButton getButtonColor() {
		return buttonColor;
	}

	public final void setControl(RotationalCurvePanelControl control) {
		this.control = control;
		refreshView();
	}

	public void refreshView() {
		ConfigCurve configCurve = control.getModel().getSeries().getConfigCurve();
		getCheckBox().setSelected(configCurve.isVisible());
		getButtonColor(this).setBackground(configCurve.getColor());
		if (control.getModel().haveAnnotation()) {
			getAnnotationCheckBox().setSelected(
					control.getModel().isAnnotationDisplayed());
		}
		if (control.getModel().haveBlended()) {
			getBlendedCheckBox().setSelected(
					control.getModel().areBlendedDisplayed());
		}
		if (control.getModel().haveNonBlended()) {
			getNonBlendedCheckBox().setSelected(
					control.getModel().areNonBlendedDisplayed());
		}
	}

	public void addRotationalSerieCassisListener(RotationalSerieCassisListener
			rotationalSerieCassisListener) {
		control.addRotationalSerieCassisListener(rotationalSerieCassisListener);
	}

	public void removeRotationalSerieCassisListener(RotationalSerieCassisListener
			rotationalSerieCassisListener) {
		control.removeRotationalSerieCassisListener(rotationalSerieCassisListener);
	}

	public String getNameFromModel() {
		return control.getModel().getSeries().getRealName();
	}
}
