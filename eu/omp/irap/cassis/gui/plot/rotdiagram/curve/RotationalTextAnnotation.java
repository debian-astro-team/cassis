/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram.curve;

import java.awt.Color;
import java.awt.Font;

import org.jfree.chart.annotations.XYTextAnnotation;
import org.jfree.ui.TextAnchor;

/**
 * This class is a annotation for a fit selection.
 *
 * @author M. Boiziot
 */
public class RotationalTextAnnotation extends XYTextAnnotation {

	private static final long serialVersionUID = -2722179316753314939L;
	private static final Font NORMAL_FONT = new Font("SansSerif", Font.PLAIN, 16);
	private static final Font SAVE_FONT = new Font("SansSerif", Font.PLAIN, 24);
	private int blockNumber;


	/**
	 * Constructor.
	 *
	 * @param x the x-coordinate (in data space).
	 * @param y the y-coordinate (in data space).
	 * @param blockNumber The number of the selection block.
	 * @param color The color of the annotation.
	 */
	public RotationalTextAnnotation(double x, double y, int blockNumber, Color color) {
		super(String.valueOf(blockNumber), x, y);
		this.blockNumber = blockNumber;
		this.setPaint(color);
		this.setNormalFont();
		this.setTextAnchor(TextAnchor.TOP_LEFT);
	}

	/**
	 * Return the block number.
	 *
	 * @return return the block number.
	 */
	public int getBlockNumber() {
		return blockNumber;
	}

	/**
	 * Set the normal font.
	 */
	public final void setNormalFont() {
		this.setFont(NORMAL_FONT);
	}

	/**
	 * Set the font for the pdf save.
	 */
	public final void setSaveFont() {
		this.setFont(SAVE_FONT);
	}
}
