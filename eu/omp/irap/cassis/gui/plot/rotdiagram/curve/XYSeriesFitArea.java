/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram.curve;

import java.awt.Color;
import java.util.List;

import org.jfree.data.xy.XYSeries;

import eu.omp.irap.cassis.gui.plot.curve.config.ConfigCurve;
import eu.omp.irap.cassis.gui.plot.rotdiagram.fit.LineInformation;
import eu.omp.irap.cassis.gui.plot.rotdiagram.fit.RotationalFitComponent;

/**
 * Represents a Rotational Diagram fit series.
 *
 * @author M. Boiziot
 */
public class XYSeriesFitArea extends XYSeries implements RotationalCurveModelSerieInterface {

	private static final long serialVersionUID = -1840621401632994726L;
	private LineInformation lineInformation;
	private RotationalTypeCurve type;
	private ConfigCurve configCurve;
	private RotationalFitComponent rfc;
	private int block;


	/**
	 * Constructor.
	 *
	 * @param title Series title.
	 * @param type Type of the series.
	 * @param rfc The {@link RotationalFitComponent} used to create this fit series.
	 * @param block The number of the fit block/selection associated with the series.
	 */
	public XYSeriesFitArea(String title, RotationalTypeCurve type,
			RotationalFitComponent rfc, int block) {
		super(title);
		this.configCurve = new ConfigCurve();
		this.type = type;
		this.rfc = rfc;
		this.block = block;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalCurveModelSerieInterface#getConfigCurve()
	 */
	@Override
	public ConfigCurve getConfigCurve() {
		return configCurve;
	}

	/**
	 * Return the {@link LineInformation} of the series.
	 *
	 * @return the {@link LineInformation} of the series.
	 */
	public LineInformation getLineInformation() {
		return lineInformation;
	}

	/**
	 * Set a {@link LineInformation} to the series.
	 *
	 * @param li The {@link LineInformation} to set.
	 */
	public void setLineInformation(LineInformation li) {
		this.lineInformation = li;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalCurveModelSerieInterface#getType()
	 */
	@Override
	public RotationalTypeCurve getType() {
		return type;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalCurveModelSerieInterface#getTag()
	 */
	@Override
	public int getTag() {
		return rfc.getTag();
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalCurveModelSerieInterface#getRealName()
	 */
	@Override
	public String getRealName() {
		return "Fit (" + RotationalTypeCurve.getFriendlyName(getSourceType()) + ") - Block " + block;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalCurveModelSerieInterface#getNumComponent()
	 */
	@Override
	public int getNumComponent() {
		return rfc.getCompNumber();
	}

	/**
	 * Set a {@link Color} to the serie and to the associated {@link LineInformation}.
	 *
	 * @param color The color to set.
	 */
	public void setColor(Color color) {
		this.configCurve.setColor(color);
		this.lineInformation.setColor(color);
	}

	/**
	 * Return the {@link RotationalTypeCurve} of the fitted points.
	 *
	 * @return the {@link RotationalTypeCurve} of the fitted points.
	 */
	public RotationalTypeCurve getSourceType() {
		return rfc.getGeneralType();
	}

	/**
	 * Return the number of the fit block/selection associated with the series.
	 *
	 * @return the number of the fit block/selection associated with the series.
	 */
	public int getBlockSource() {
		return block;
	}

	/**
	 * Return the {@link List} of {@link Point} used to create this fit.
	 *
	 * @return the {@link List} of {@link Point} used to create this fit.
	 */
	public List<Point> getFitPoints() {
		return rfc.getListPoints();
	}
}
