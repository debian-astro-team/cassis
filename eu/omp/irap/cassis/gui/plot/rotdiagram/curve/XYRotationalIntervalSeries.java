/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram.curve;

import java.util.ArrayList;
import java.util.List;

import org.jfree.data.xy.XYIntervalSeries;

import eu.omp.irap.cassis.gui.plot.curve.config.ConfigCurve;
import eu.omp.irap.cassis.parameters.PointInformation;

/**
 * Represent a Rotational Diagram Series.
 *
 * @author M. Boiziot
 */
public class XYRotationalIntervalSeries extends XYIntervalSeries implements RotationalCurveModelSerieInterface {

	private static final long serialVersionUID = -354172436398461700L;
	private int tag;
	private int numComponent;
	private String realName;
	private List<PointInformation> pointInformations;
	private List<Point> pointSigmaList;
	private RotationalTypeCurve type;
	private ConfigCurve configCurve;
	private boolean visible;


	/**
	 * Constructor.
	 *
	 * @param key Series key.
	 * @param tag Tag associated with the series.
	 * @param numComponent Component number associated with the series.
	 * @param realName Full name of the series.
	 * @param points Points of the series.
	 * @param type Type of the series.
	 */
	public XYRotationalIntervalSeries(String key, int tag, int numComponent,
			String realName, List<PointInformation> points,
			RotationalTypeCurve type) {
		super(key);
		this.tag = tag;
		this.numComponent = numComponent;
		this.realName = realName;
		this.pointInformations = points;
		this.type = type;
		this.configCurve = new ConfigCurve();
		this.pointSigmaList = new ArrayList<>();
		this.visible = true;
	}

	/**
	 * Return the {@link List} of {@link PointInformation} of the series.
	 *
	 * @return the {@link List} of {@link PointInformation} of the series.
	 */
	public List<PointInformation> getPoints() {
		return pointInformations;
	}

	/**
	 * Add a Sigma {@link Point}.
	 *
	 * @param p The point to add.
	 */
	public void addPointSigma(Point p) {
		pointSigmaList.add(p);
	}

	/**
	 * Return the {@link List} of Sigma {@link Point}.
	 *
	 * @return The list of Sigma {@link Point}.
	 */
	public List<Point> getPointSigmaList() {
		return pointSigmaList;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalCurveModelSerieInterface#getRealName()
	 */
	@Override
	public String getRealName() {
		return realName;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalCurveModelSerieInterface#getType()
	 */
	@Override
	public RotationalTypeCurve getType() {
		return type;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalCurveModelSerieInterface#getConfigCurve()
	 */
	@Override
	public ConfigCurve getConfigCurve() {
		return configCurve;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalCurveModelSerieInterface#getTag()
	 */
	@Override
	public int getTag() {
		return tag;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalCurveModelSerieInterface#getNumComponent()
	 */
	@Override
	public int getNumComponent() {
		return numComponent;
	}

	/**
	 * Change the visibility value of the series.
	 *
	 * @param visible If the series should be visible.
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	/**
	 * Return if the series is really visible.
	 *
	 * @return if the series is really visible.
	 */
	public boolean isReallyVisible() {
		return visible && configCurve.isVisible();
	}
}
