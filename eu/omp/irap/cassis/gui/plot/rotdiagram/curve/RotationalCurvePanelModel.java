/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram.curve;

import eu.omp.irap.cassis.common.events.ListenerManager;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;

public class RotationalCurvePanelModel extends ListenerManager {

	public static final String SET_SERIE_EVENT = "setSerie";
	public static final String BLENDED_DISPLAYED_EVENT = "blendedDisplayed";
	public static final String NON_BLENDED_DISPLAYED_EVENT = "nonBlendedDisplayed";
	public static final String ANNOTATION_DISPLAYED_EVENT = "annotationDisplayed";
	private RotationalCurveModelSerieInterface series;
	private boolean haveAnnotation;
	private boolean haveBlended;
	private boolean haveNonBlended;
	private boolean annotationDisplayed;
	private boolean blendedDisplayed;
	private boolean nonBlendedDisplayed;


	public RotationalCurvePanelModel(RotationalCurveModelSerieInterface series) {
		this(series, false, false, true);
	}

	public RotationalCurvePanelModel(RotationalCurveModelSerieInterface series,
			boolean haveAnnotation, boolean haveBlended, boolean haveNonBlended) {
		super();
		this.series = series;
		this.haveAnnotation = haveAnnotation;
		if (haveAnnotation) {
			annotationDisplayed = true;
		}
		this.haveBlended = haveBlended;
		if (haveBlended) {
			blendedDisplayed = true;
		}
		this.haveNonBlended = haveNonBlended;
		if (haveNonBlended) {
			nonBlendedDisplayed = true;
		}
	}

	public RotationalCurveModelSerieInterface getSeries() {
		return series;
	}

	public void setSeries(RotationalCurveModelSerieInterface series,
			boolean visibilityChange) {
		this.series = series;
		fireDataChanged(new ModelChangedEvent(SET_SERIE_EVENT, this,
				Boolean.valueOf(visibilityChange)));
	}

	public boolean haveAnnotation() {
		return haveAnnotation;
	}

	public boolean getAnnotationDisplayedValue() {
		return annotationDisplayed;
	}

	public boolean isAnnotationDisplayed() {
		return annotationDisplayed && series.getConfigCurve().isVisible();
	}

	public void setAnnotationDisplayed(boolean newAnnotationDisplayedValue) {
		this.annotationDisplayed = newAnnotationDisplayedValue;
		fireDataChanged(new ModelChangedEvent(ANNOTATION_DISPLAYED_EVENT));
	}

	public int getTag() {
		return series.getTag();
	}

	public int getNumComponent() {
		return series.getNumComponent();
	}

	public boolean haveBlended() {
		return haveBlended;
	}

	public boolean haveNonBlended() {
		return haveNonBlended;
	}

	public boolean areBlendedDisplayed() {
		return blendedDisplayed && series.getConfigCurve().isVisible();
	}

	public boolean areNonBlendedDisplayed() {
		return nonBlendedDisplayed && series.getConfigCurve().isVisible();
	}

	public void setBlendedDisplayed(boolean newDisplayedValue) {
		this.blendedDisplayed = newDisplayedValue;
		fireDataChanged(new ModelChangedEvent(BLENDED_DISPLAYED_EVENT));
	}

	public void setNonBlendedDisplayed(boolean newDisplayedValue) {
		this.nonBlendedDisplayed = newDisplayedValue;
		fireDataChanged(new ModelChangedEvent(NON_BLENDED_DISPLAYED_EVENT));
	}

	public boolean getBlendedDisplayedValue() {
		return blendedDisplayed;
	}

	public boolean getNonBlendedDisplayedValue() {
		return nonBlendedDisplayed;
	}
}
