/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;


/**
 * This class is a simple frame with a {@link JScrollPane} contening a message
 * and two {@link JButton} to hide or close the {@link JFrame}.
 *
 * @author M. Boiziot
 */
public class ScrollPaneMessageFrame extends JFrame {

	private static final long serialVersionUID = 536857853061121199L;
	private JTextArea textArea;


	/**
	 * Constructor.
	 *
	 * @param title The title of the frame.
	 * @param message The message to add to the {@link JScrollPane}.
	 */
	public ScrollPaneMessageFrame(String title, String message) {
		super(title);
		JPanel contentPane = new JPanel(new BorderLayout());

		textArea = new JTextArea(message);
		textArea.setEditable(false);
		JScrollPane scrollPane = new JScrollPane(textArea);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		scrollPane.setPreferredSize(new Dimension(500, 400));

		contentPane.add(scrollPane, BorderLayout.CENTER);

		JPanel buttonPane = new JPanel();

		JButton hideButton = new JButton("Hide");
		hideButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ScrollPaneMessageFrame.this.setState(JFrame.ICONIFIED);
			}
		});
		buttonPane.add(hideButton);

		JButton closeButton = new JButton("Close");
		closeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ScrollPaneMessageFrame.this.dispose();
			}
		});
		buttonPane.add(closeButton);

		contentPane.add(buttonPane, BorderLayout.SOUTH);

		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setContentPane(contentPane);
		this.pack();
	}

	/**
	 * Change the message and restore the state of the JFrame to NORMAL.
	 *
	 * @param newMessage the new message to set.
	 */
	public void setMessage(String newMessage) {
		textArea.setText(newMessage);
		setState(JFrame.NORMAL);
	}
}
