/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram.fit;

import java.util.ArrayList;
import java.util.List;

import eu.omp.irap.cassis.gui.plot.rotdiagram.fit.FitError.ERROR;

/**
 * Utility class to display a message after a FitError.
 *
 * @author M. Boiziot
 */
public abstract class FitErrorMessageUtility {

	/**
	 * Utility class. Must not be used.
	 */
	private FitErrorMessageUtility() {
	}

	/**
	 * Construct a message for a {@link List} of {@link FitError}.
	 *
	 * @param fitErrors The errors.
	 * @return The message.
	 */
	public static String constructFitErrorMsg(List<FitError> fitErrors) {
		StringBuilder sb = new StringBuilder();
		if (FitErrorMessageUtility.haveMultipleErrorType(fitErrors)) {
			sb.append("Multiple errors occurred during the fitting:\n");
		} else {
			sb.append("An error occurred during the fitting:\n");
		}

		for (ERROR e : ERROR.values()) {
			List<FitError> errors = getFitErrorType(fitErrors, e);
			if (errors.isEmpty()) {
				continue;
			}
			sb.append(FitErrorMessageUtility.getErrorMsg(errors));
		}
		return sb.toString();
	}

	/**
	 * Return a message for a subtype of error.
	 *
	 * @param errors The errors. (Must be of the same type.)
	 * @return The message.
	 */
	private static String getErrorMsg(List<FitError> errors) {
		String msg;
		switch (errors.get(0).getErrorType()) {
			case NEGATIVE_TEMPERATURE:
				msg = FitErrorMessageUtility.getGenericErrorForMsg(errors,
						"  - Negative temperature for :\n");
				break;
			case UNKNOW_MOL_IN_DB:
				msg = FitErrorMessageUtility.getErrorForDB(errors);
				break;
			case UNKNOW:
			default:
				msg = FitErrorMessageUtility.getGenericErrorForMsg(errors, "\t- Unknow error for :\n");
		}
		return msg;
	}

	/**
	 * Create a message specific to database error.
	 *
	 * @param errors The errors.
	 * @return The message.
	 */
	private static String getErrorForDB(List<FitError> errors) {
		StringBuilder sb;
		List<Integer> unknowTags = FitErrorMessageUtility.getUniqueTags(errors);
		if (unknowTags.size() == 1) {
			sb = new StringBuilder("  - Unknow species in database for tag ");
			sb.append(unknowTags.get(0)).append('.');
		} else {
			sb = new StringBuilder("  - Unknow species in database for tags ");
			for (int i = 0; i < unknowTags.size() - 1; i++) {
				sb.append(unknowTags.get(i)).append(", ");
			}
			sb.append(unknowTags.get(unknowTags.size()-1));
			sb.append('.');
		}
		return sb.toString();
	}

	/**
	 * Return the {@link List} of unique tags of provided {@link FitError}s.
	 *
	 * @param errors The errors.
	 * @return The {@link List} of unique tags.
	 */
	private static List<Integer> getUniqueTags(List<FitError> errors) {
		List<Integer> l = new ArrayList<>();
		for (FitError error : errors) {
			if (!l.contains(error.getTag())) {
				l.add(error.getTag());
			}
		}
		return l;
	}

	/**
	 * Create a generic error message.
	 *
	 * @param errors The list of {@link FitError}.
	 * @param msg The First line message.
	 * @return the created message.
	 */
	private static String getGenericErrorForMsg(List<FitError> errors, String msg) {
		StringBuilder sb = new StringBuilder(msg);
		for (FitError error : errors) {
			sb.append("    -");
			sb.append(" tag: ").append(error.getTag());
			sb.append(", comp: ").append(error.getComponent());
			sb.append(", block: ").append(error.getBlock());
			sb.append('\n');
		}
		return sb.toString();
	}

	/**
	 * Return a {@link List} of {@link FitError} for a provited type of {@link ERROR}.
	 *
	 * @param fitErrors The base list of {@link FitError}.
	 * @param errorType The type of {@link ERROR}.
	 * @return the {@link List} of {@link FitError} for the provited type of {@link ERROR}.
	 */
	private static List<FitError> getFitErrorType(List<FitError> fitErrors, ERROR errorType) {
		List<FitError> list = new ArrayList<>();
		for (FitError error : fitErrors) {
			if (error.getErrorType() == errorType) {
				list.add(error);
			}
		}
		return list;
	}

	/**
	 * Check if there is multiple error type on a {@link List} of {@link FitError}.
	 *
	 * @param fitErrors The errors.
	 * @return if there is multipe error type.
	 */
	private static boolean haveMultipleErrorType(List<FitError> fitErrors) {
		ERROR firstError = fitErrors.get(0).getErrorType();
		for (FitError error : fitErrors) {
			if (error.getErrorType() != firstError) {
				return true;
			}
		}
		return false;
	}
}
