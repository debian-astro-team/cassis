/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram.fit;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

/**
 * View for the RotationalFit.
 *
 * @author M. Boiziot
 */
public class RotationalFitView extends JPanel {

	private static final long serialVersionUID = -1415394433117557919L;
	private RotationalFitControl control;
	private JButton resetLastButton;
	private JButton resetAllButton;
	private JButton closeSelectionButton;
	private JButton displayFitButton;
	private JButton removeAllFitButton;
	private JButton hideAllFitButton;
	private JCheckBox displaySelectionsCheckBox;
	private JCheckBox selectionAnnotationCheckBox;


	/**
	 * Constructor.
	 *
	 * @param model The model.
	 */
	public RotationalFitView(RotationalFitModel model) {
		super(new BorderLayout());
		this.control = new RotationalFitControl(this, model);
		initComponents();
	}

	/**
	 * Create and place the components.
	 */
	private void initComponents() {
		JPanel selectionPanel = new JPanel();
		selectionPanel.setLayout(new GridBagLayout());
		selectionPanel.setBorder(new TitledBorder("Selection"));
		selectionPanel.setToolTipText("Select the datapoints by clicking and dragging" +
				" the middle mouse button or the equivalent with the trackpad.");
		GridBagConstraints gbConstraint = new GridBagConstraints();
		gbConstraint.gridy = 0;
		gbConstraint.fill = GridBagConstraints.BOTH;
		gbConstraint.insets = new Insets(2, 2, 2, 2);
		selectionPanel.add(getResetLastButton(), gbConstraint);

		gbConstraint.gridy = 1;
		selectionPanel.add(getResetAllButton(), gbConstraint);

		gbConstraint.gridy = 2;
		selectionPanel.add(getCloseSelectionButton(), gbConstraint);

		gbConstraint.gridy = 3;
		selectionPanel.add(getDisplaySelectionsCheckBox(), gbConstraint);

		gbConstraint.gridy = 4;
		selectionPanel.add(getSelectionAnnotionCheckBox(), gbConstraint);

		JPanel centerPanel = new JPanel(new GridBagLayout());
		gbConstraint.gridy = 0;
		centerPanel.add(getHideAllFitButton(), gbConstraint);
		gbConstraint.gridy = 1;
		centerPanel.add(getRemoveAllFitButton(), gbConstraint);

		this.add(selectionPanel, BorderLayout.NORTH);
		this.add(centerPanel, BorderLayout.CENTER);
		this.add(getDisplayFitButton(), BorderLayout.SOUTH);
	}

	/**
	 * Create if needed then return the "Reset last" {@link JButton}.
	 *
	 * @return the "Reset last" button.
	 */
	public JButton getResetLastButton() {
		if (resetLastButton == null) {
			resetLastButton = new JButton("Reset last");
			resetLastButton.setEnabled(false);
			resetLastButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (control.getModel().getAllSelectionsSize() < 1) {
						resetLastButton.setEnabled(false);
						getDisplayFitButton().setEnabled(true);
					}
					control.getModel().resetLastSelection();
				}
			});
		}
		return resetLastButton;
	}

	/**
	 * Create if needed then return the "Reset all" {@link JButton}.
	 *
	 * @return the "Reset all" button.
	 */
	public JButton getResetAllButton() {
		if (resetAllButton == null) {
			resetAllButton = new JButton("Reset all");
			resetAllButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.getModel().resetAllSelections();
					getCloseSelectionButton().setEnabled(false);
					getResetLastButton().setEnabled(false);
				}
			});
		}
		return resetAllButton;
	}

	/**
	 * Create if needed then return the "Close selection" {@link JButton}.
	 *
	 * @return the "Close selection" button.
	 */
	public JButton getCloseSelectionButton() {
		if (closeSelectionButton == null) {
			closeSelectionButton = new JButton("Close selection");
			closeSelectionButton.setEnabled(false);
			closeSelectionButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.getModel().closeSelection();
					closeSelectionButton.setEnabled(false);
					getDisplayFitButton().setEnabled(true);
				}
			});
		}
		return closeSelectionButton;
	}

	/**
	 * Create if needed then return the "Display Fit" {@link JButton}.
	 *
	 * @return the "Display Fit" button.
	 */
	public JButton getDisplayFitButton() {
		if (displayFitButton == null) {
			displayFitButton = new JButton("Display Fit");
			displayFitButton.setPreferredSize(new Dimension(185, 74));
			displayFitButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.getModel().displayFitPlot();
				}
			});
		}
		return displayFitButton;
	}

	/**
	 * Begin a selection.
	 */
	public void mouseBeginRectangleSelection() {
		getResetLastButton().setEnabled(false);
		if (!control.getModel().isSelectionVisible()) {
			control.getModel().setSelectionVisible(true);
		}

		getDisplayFitButton().setEnabled(false);
	}

	/**
	 * End a selection.
	 *
	 * @param xLeft The X left value of the selection.
	 * @param yTop The Y top value of the selection.
	 * @param xRight The X right value of the selection.
	 * @param yBottom The Y bottom value of the selection.
	 */
	public void mouseEndRectangleFitReleased(double xLeft, double yTop, double xRight,
			double yBottom) {
		getCloseSelectionButton().setEnabled(true);
		getResetLastButton().setEnabled(true);
		control.getModel().displayRectangle(xLeft, yTop, xRight, yBottom);
		control.getModel().setRectTrace(true);
	}

	/**
	 * Create if needed then return the "Display selections" {@link JCheckBox}.
	 *
	 * @return the "Display selections" checkbox.
	 */
	public JCheckBox getDisplaySelectionsCheckBox() {
		if (displaySelectionsCheckBox == null) {
			displaySelectionsCheckBox = new JCheckBox("Display selections");
			displaySelectionsCheckBox.setSelected(control.getModel().isSelectionVisible());
			displaySelectionsCheckBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					boolean selected = displaySelectionsCheckBox.isSelected();
					control.getModel().setSelectionVisible(selected);
					if (!selected) {
						control.getModel().setSelectionAnnotationVisible(false);
					}
				}
			});
		}
		return displaySelectionsCheckBox;
	}

	/**
	 * Display a fit error.
	 *
	 * @param errorMsg The error message.
	 */
	public void displayFitError(String errorMsg) {
		JOptionPane.showMessageDialog(this, errorMsg, "Fit error", JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * Create if needed then return the "Hide all" {@link JButton}.
	 *
	 * @return the "Hide all" button.
	 */
	public JButton getHideAllFitButton() {
		if (hideAllFitButton == null) {
			hideAllFitButton = new JButton("Hide all");
			hideAllFitButton.setPreferredSize(new Dimension(110, 25));
			hideAllFitButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					control.getModel().hideSeries();
				}
			});
		}
		return hideAllFitButton;
	}

	/**
	 * Create if needed then return the "Remove all" {@link JButton}.
	 *
	 * @return the "Remove all" button.
	 */
	public JButton getRemoveAllFitButton() {
		if (removeAllFitButton == null) {
			removeAllFitButton = new JButton("Remove all");
			removeAllFitButton.setPreferredSize(new Dimension(110, 25));
			removeAllFitButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					control.getModel().removeAllFitSeries();
				}
			});
		}
		return removeAllFitButton;
	}

	/**
	 * Create if needed then return the "Display selections annotations" {@link JCheckBox}.
	 *
	 * @return the "Display selections annotations" {@link JCheckBox}.
	 */
	public JCheckBox getSelectionAnnotionCheckBox() {
		if (selectionAnnotationCheckBox == null) {
			selectionAnnotationCheckBox = new JCheckBox("Display selections annotations");
			selectionAnnotationCheckBox.setSelected(control.getModel().isSelectionAnnotationVisible());
			selectionAnnotationCheckBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					boolean visible = selectionAnnotationCheckBox.isSelected();
					control.getModel().setSelectionAnnotationVisible(visible);
					if (visible && !control.getModel().isSelectionVisible()) {
						control.getModel().setSelectionVisible(true);
					}
				}
			});
		}
		return selectionAnnotationCheckBox;
	}

	/**
	 * Set the view component with the selection disabled.
	 */
	public void disableSelection() {
		resetLastButton.setEnabled(false);
		resetAllButton.setEnabled(false);
		closeSelectionButton.setEnabled(false);
		displaySelectionsCheckBox.setEnabled(true);
		selectionAnnotationCheckBox.setEnabled(true);
		hideAllFitButton.setEnabled(true);
		removeAllFitButton.setEnabled(true);
		displayFitButton.setEnabled(true);
	}

	/**
	 * Restore the view component.
	 */
	public void restore() {
		resetLastButton.setEnabled(false);
		resetAllButton.setEnabled(true);
		closeSelectionButton.setEnabled(false);
		displaySelectionsCheckBox.setEnabled(true);
		selectionAnnotationCheckBox.setEnabled(true);
		hideAllFitButton.setEnabled(true);
		removeAllFitButton.setEnabled(true);
		displayFitButton.setEnabled(true);
	}

	public void showMessageNoMultiSelectionAllowed() {
		JOptionPane.showMessageDialog(this,
				"Line selection is not possible with multiple components displayed, "
				+ "\n please display only one of your components first.");

	}
}
