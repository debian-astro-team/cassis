/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram.fit;

import java.util.ArrayList;
import java.util.List;

import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.Point;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalTypeCurve;

/**
 * Describes a fit rotational component.
 *
 * @author M. Boiziot
 */
public class RotationalFitComponent {

	private int tag;
	private int compNumber;
	private RotationalTypeCurve generalType;
	private List<Point> listPoints;


	/**
	 * Constructor.
	 *
	 * @param tag The tag.
	 * @param compNumber The component number.
	 * @param generalType The general (top) type of the component.
	 */
	public RotationalFitComponent(int tag, int compNumber, RotationalTypeCurve generalType) {
		this.tag = tag;
		this.compNumber = compNumber;
		this.generalType = generalType;
		this.listPoints = new ArrayList<>();
	}

	/**
	 * Return the tag.
	 *
	 * @return the tag.
	 */
	public int getTag() {
		return tag;
	}

	/**
	 * Return the component number.
	 *
	 * @return the component number.
	 */
	public int getCompNumber() {
		return compNumber;
	}

	/**
	 * Return the general (top) {@link RotationalTypeCurve}.
	 *
	 * @return the general {@link RotationalTypeCurve}.
	 */
	public RotationalTypeCurve getGeneralType() {
		return generalType;
	}

	/**
	 * Add a point to the component.
	 *
	 * @param x The x value of the point.
	 * @param y The y value of the point.
	 * @param sigma The sigma value of the point.
	 */
	public void addPoint(double x, double y, double sigma) {
		listPoints.add(new Point(x, y, sigma));
	}

	/**
	 * Return the {@link List} of the {@link Point}.
	 *
	 * @return the {@link List} of the {@link Point}.
	 */
	public List<Point> getListPoints() {
		return listPoints;
	}

	/**
	 * Return the minimum X value of the points.
	 *
	 * @return the minimum X value of the points.
	 */
	public double getXMin() {
		double min = Double.MAX_VALUE;
		for (Point p : listPoints) {
			if (!Double.isNaN(p.getX()) && p.getX() < min) {
				min = p.getX();
			}
		}
		return min;
	}

	/**
	 * Return the maximum X value of the points.
	 *
	 * @return the maximum X value of the points.
	 */
	public double getXMax() {
		double max = Double.MIN_VALUE;
		for (Point p : listPoints) {
			if (!Double.isNaN(p.getX()) && p.getX() > max) {
				max = p.getX();
			}
		}
		return max;
	}
}
