/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram.fit;

import java.awt.Color;


/**
 * Represent a fit selection rectangle.
 *
 * @author M. Boiziot
 */
public class FitRotationalRectangle {

	private double up;
	private double down;
	private double left;
	private double right;
	private Color color;

	private static double upBig = Double.NEGATIVE_INFINITY;
	private static double downBig = Double.POSITIVE_INFINITY;
	private static double leftBig = Double.POSITIVE_INFINITY;
	private static double rightBig = Double.NEGATIVE_INFINITY;


	/**
	 * Constructor.
	 *
	 * @param left The X left value.
	 * @param up The Y up value.
	 * @param right The X right value.
	 * @param down The Y down value.
	 * @param selectionColor The color.
	 */
	public FitRotationalRectangle(double left, double up, double right, double down, Color selectionColor) {
		super();
		this.up = up;
		this.down = down;
		this.left = left;
		this.right = right;
		this.color = selectionColor;
	}

	/**
	 * Return the Y down value.
	 *
	 * @return the Y down value.
	 */
	public final double getDown() {
		return down;
	}

	/**
	 * Return the X left value.
	 *
	 * @return the X left value
	 */
	public final double getLeft() {
		return left;
	}

	/**
	 * Return the X right value.
	 *
	 * @return the X right value
	 */
	public final double getRight() {
		return right;
	}

	/**
	 * Return the Y up value.
	 *
	 * @return the Y up value.
	 */
	public final double getUp() {
		return up;
	}

	/**
	 * Return the color.
	 *
	 * @return the color.
	 */
	public final Color getColor() {
		return color;
	}

	/**
	 * Return the lower Y value.
	 *
	 * @return the Y downer value.
	 */
	public static double getDownBig() {
		return downBig;
	}

	/**
	 * Return the lowest X value.
	 *
	 * @return the lowest X value.
	 */
	public static double getLeftBig() {
		return leftBig;
	}

	/**
	 * Return the biggest X value.
	 *
	 * @return the biggest X value.
	 */
	public static double getRightBig() {
		return rightBig;
	}

	/**
	 * Return the biggest Y value.
	 *
	 * @return the biggest Y value.
	 */
	public static double getUpBig() {
		return upBig;
	}

	/**
	 * Change the lowest Y value.
	 *
	 * @param downBig the new lowest Y value.
	 */
	public static void setDownBig(double downBig) {
		if (!Double.isNaN(downBig)) {
			FitRotationalRectangle.downBig = downBig;
		}
	}

	/**
	 * Change the lowest X value.
	 *
	 * @param leftBig the new lowest X value.
	 */
	public static void setLeftBig(double leftBig) {
		if (!Double.isNaN(leftBig)) {
			FitRotationalRectangle.leftBig = leftBig;
		}
	}

	/**
	 * Change the biggest X value.
	 *
	 * @param rightBig the new biggest X value.
	 */
	public static void setRightBig(double rightBig) {
		if (!Double.isNaN(rightBig)) {
			FitRotationalRectangle.rightBig = rightBig;
		}
	}

	/**
	 * Change the biggest Y value.
	 *
	 * @param upBig The new biggest Y value.
	 */
	public static void setUpBig(double upBig) {
		if (!Double.isNaN(upBig)) {
			FitRotationalRectangle.upBig = upBig;
		}
	}

	/**
	 * Reset the lowest/biggest X/Y values.
	 */
	public static void reset() {
		upBig = Double.NEGATIVE_INFINITY;
		downBig = Double.POSITIVE_INFINITY;
		leftBig = Double.POSITIVE_INFINITY;
		rightBig = Double.NEGATIVE_INFINITY;
	}
}
