/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram.fit;

import java.util.List;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;

/**
 * Control for the RotationalFit.
 *
 * @author M. Boiziot
 */
public class RotationalFitControl implements ModelListener {

	private RotationalFitModel model;
	private RotationalFitView view;


	/**
	 * Constructor.
	 *
	 * @param view The view.
	 * @param model The model.
	 */
	public RotationalFitControl(RotationalFitView view, RotationalFitModel model) {
		this.model = model;
		this.view = view;
		this.model.addModelListener(this);
	}

	/**
	 * Return the model.
	 *
	 * @return the model.
	 */
	public RotationalFitModel getModel() {
		return model;
	}

	/**
	 * Return the view.
	 *
	 * @return the view
	 */
	public RotationalFitView getView() {
		return view;
	}

	/**
	 * @see eu.omp.irap.cassis.common.events.ModelListener#dataChanged(eu.omp.irap.cassis.common.events.ModelChangedEvent)
	 */
	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (RotationalFitModel.CLOSE_SELECTION_BUTTON_EVENT.equals(event.getSource())) {
			boolean enabled = ((Boolean) event.getValue()).booleanValue();
			view.getCloseSelectionButton().setEnabled(enabled);
		} else if (RotationalFitModel.RESET_SELECTION_BUTTON_EVENT.equals(event.getSource())) {
			boolean enabled = ((Boolean) event.getValue()).booleanValue();
			view.getResetLastButton().setEnabled(enabled);
		} else if (RotationalFitModel.DO_DISPLAY_BUTTON_CLICK_EVENT.equals(event.getSource())) {
			view.getDisplayFitButton().doClick();
		} else if (RotationalFitModel.ERROR_FIT_EVENT.equals(event.getSource())) {
			@SuppressWarnings("unchecked")
			List<FitError> fitErrors = (List<FitError>) event.getValue();
			String errorMsg = FitErrorMessageUtility.constructFitErrorMsg(fitErrors);
			view.displayFitError(errorMsg);
		} else if (RotationalFitModel.DISPLAY_SELECTION_EVENT.equals(event.getSource())) {
			view.getDisplaySelectionsCheckBox().setSelected(model.isSelectionVisible());
		} else if (RotationalFitModel.DISPLAY_BUTTON_EVENT.equals(event.getSource())) {
			boolean enabled = ((Boolean) event.getValue()).booleanValue();
			view.getDisplayFitButton().setEnabled(enabled);
		} else if (RotationalFitModel.DISPLAY_SELECTION_ANNOTATION_EVENT.equals(event.getSource())) {
			view.getSelectionAnnotionCheckBox().setSelected(
					model.isSelectionAnnotationVisible());
		} else if (RotationalFitModel.MULTI_SELECTION_ALLOWED_EVENT.equals(event.getSource())) {
			boolean enabled = ((Boolean) event.getValue()).booleanValue();
			if (enabled) {
				view.restore();
			} else {
				view.disableSelection();
			}
		}
	}

}
