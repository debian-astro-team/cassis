/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram.fit;

import java.awt.Color;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

/**
 * This class represent a line and contains all information on this line.
 *
 * @author Granier Vincent, Jean-Michel Glorian
 * @since 19 mai 2005
 */
public class LineInformation {

	private double temperature;
	private double temperatureDev;
	private double density;
	private double densityDev;
	private double a;
	private double b;
	private double xMinimum;
	private double xMaximum;
	private Color color;
	private int position = -1;
	private boolean visible = true;
	private double reducedChiSquared;
	private double chiSquared;
	private double chiProbability;


	/**
	 * Constructor makes a new standart LineInformation.
	 */
	public LineInformation() {
		this.temperature = 0.0;
		this.density = 0.0;
	}

	/**
	 * @return the visible
	 */
	public final boolean isVisible() {
		return visible;
	}

	/**
	 * @param visible
	 *            the visible to set
	 */
	public final void setVisible(boolean visible) {
		this.visible = visible;
	}

	/**
	 * Change the temperature.
	 *
	 * @param t
	 *            new temperature
	 */
	public void setTemperature(double t) {
		temperature = t;
	}

	/**
	 * Change the temperature dev.
	 *
	 * @param t
	 *            new temperature
	 */
	public void setTemperatureDev(double t) {
		temperatureDev = t;
	}

	/**
	 * Change density.
	 *
	 * @param d
	 *            new density
	 */
	public void setDensity(double d) {
		density = d;
	}

	/**
	 * Change the density dev.
	 *
	 * @param d
	 *            new density
	 */
	public void setDensityDev(double d) {
		densityDev = d;
	}

	/**
	 * Change the color.
	 *
	 * @param color
	 *            new color
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	/**
	 * Return the color.
	 *
	 * @return color
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Change a.
	 *
	 * @param a
	 *            new a
	 */
	public void setA(double a) {
		this.a = a;
	}

	/**
	 * Change b.
	 *
	 * @param b
	 *            new b
	 */
	public void setB(double b) {
		this.b = b;
	}

	/**
	 * Change X minimum.
	 *
	 * @param min
	 *            new X min
	 */
	public void setXMinimum(double min) {
		xMinimum = min;
	}

	/**
	 * Change X maximum.
	 *
	 * @param max
	 *            new X max
	 */
	public void setXMaximum(double max) {
		xMaximum = max;
	}

	/**
	 * Return a.
	 *
	 * @return a
	 */
	public double getA() {
		return a;
	}

	/**
	 * Return b.
	 *
	 * @return b
	 */
	public double getB() {
		return b;
	}

	/**
	 * Return X maximum.
	 *
	 * @return X max
	 */
	public double getXMaximum() {
		return xMaximum;
	}

	/**
	 * Return X minimum.
	 *
	 * @return X min
	 */
	public double getXMinimum() {
		return xMinimum;
	}

	/**
	 * Build a JPanel to display line informations in rotational diagram panel.
	 *
	 * @return JPanel containing line information
	 */
	public String getInfoPanel() {
		final double densityLog = Math.floor(Math.log(density) / Math.log(10));
		final double densityPower = Math.pow(10, densityLog);

		final DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
		final DecimalFormat df = new DecimalFormat("0.0");
		decimalFormatSymbols.setDecimalSeparator('.');
		df.setDecimalFormatSymbols(decimalFormatSymbols);

		StringBuilder sb = new StringBuilder(70);
		sb.append("Tex =  ").append(df.format(temperature));
		if (temperatureDev != 0) {
			sb.append(" (+/- ").append(df.format(temperatureDev)).append(')');
		}
		sb.append(" K\n");

		sb.append("N = ").append(df.format(density / densityPower)).append(' ');
		if (densityDev != 0) {
			sb.append("(+/- ").append(df.format(densityDev / densityPower)).append(')');
		}
		sb.append('E').append(new DecimalFormat("0").format(densityLog)).append(" /cm\u00B2");

		if (reducedChiSquared != Double.POSITIVE_INFINITY) {
			sb.append('\n');
			final DecimalFormat dfChi = new DecimalFormat("0.00");
			dfChi.setDecimalFormatSymbols(decimalFormatSymbols);
			sb.append(" \u03A7\u00B2 reduced = ");
			sb.append(dfChi.format(reducedChiSquared));
			sb.append('\n');

			sb.append(" P(\u03A7\u00B2\u2265 ");
			sb.append(dfChi.format(chiSquared));
			sb.append(") = ");
			sb.append(dfChi.format(chiProbability));
			sb.append('%');
		}
		return sb.toString();
	}

	/**
	 * Return a String representation of the line.
	 *
	 * @return string
	 */
	@Override
	public String toString() {
		StringBuilder returnString = new StringBuilder();

		returnString.append("\nTemperature (current, min, max)\n");
		returnString.append(temperature);
		returnString.append("\t");
		returnString.append(temperatureDev);
		returnString.append("\t");
		returnString.append("\nDensity (current, min, max)\n");
		returnString.append(density);
		returnString.append("\t");
		returnString.append(densityDev);
		returnString.append("\t");
		returnString.append("\nPlot (a, b, xMin, xMax)\n");
		returnString.append(a);
		returnString.append("\t");
		returnString.append(b);
		returnString.append("\t");
		returnString.append(xMinimum);
		returnString.append("\t");
		returnString.append(xMinimum);

		return returnString.toString();
	}

	public void setPosition(int newPosition) {
		position = newPosition;
	}

	/**
	 * @return the position
	 */
	public final int getPosition() {
		return position;
	}

	/**
	 * Return the temperature.
	 *
	 * @return the temperature.
	 */
	public double getTemperature() {
		return temperature;
	}

	/**
	 * Return the density.
	 *
	 * @return the density.
	 */
	public double getDensity() {
		return density;
	}

	/**
	 * Return the temperature dev formatted : if the value is 0, then
	 * {@link Double#NaN} is returned.
	 *
	 * @return the temperature dev.
	 */
	public double getFormatedTemperatureDev() {
		return temperatureDev == 0 ? Double.NaN : temperatureDev;
	}

	/**
	 * Return the density dev formatted : if the value is 0, then {@link Double#NaN}
	 * is returned.
	 *
	 * @return the density dev.
	 */
	public double getFormatedDensityDev() {
		return densityDev == 0 ? Double.NaN : densityDev;
	}

	public void setReducedChiSquared(double reducedChiSquared) {
		this.reducedChiSquared = reducedChiSquared;

	}

	public double getReducedChiSquared() {
		return reducedChiSquared;
	}

	public void setChiSquared(double chiSquared) {
		this.chiSquared = chiSquared;

	}

	public double getChiSquared() {
		return chiSquared;
	}

	public void setChiProbability(double chiProbability) {
		this.chiProbability = chiProbability;

	}

	public double getChiProbability() {
		return chiProbability;
	}
}
