/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram.fit;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jfree.data.xy.XYIntervalSeriesCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.cassisd.Server;
import eu.omp.irap.cassis.cassisd.ServerImpl;
import eu.omp.irap.cassis.common.events.ListenerManager;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.gui.plot.curve.config.ColorsCurve;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.Point;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalTextAnnotation;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalTypeCurve;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.XYRotationalIntervalSeries;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.XYSeriesFitArea;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.XYSeriesFitBorder;
import eu.omp.irap.cassis.gui.plot.rotdiagram.math.RotMath;
import herschel.ia.numeric.Double1d;
import herschel.ia.numeric.toolbox.fit.Fitter;
import herschel.ia.numeric.toolbox.fit.PolynomialModel;


/**
 * Model for the RotationalFit.
 *
 * @author M. Boiziot
 */
public class RotationalFitModel extends ListenerManager {

	private static final Logger LOGGER = LoggerFactory.getLogger(RotationalFitModel.class);

	public static final String CLOSE_SELECTION_BUTTON_EVENT = "closeSelectionButtonEvent";
	public static final String RESET_SELECTION_BUTTON_EVENT = "resetSelectionButtonEvent";
	public static final String DISPLAY_BUTTON_EVENT = "displayButtonEvent";
	public static final String DO_DISPLAY_BUTTON_CLICK_EVENT = "doDisplayButtonClickEvent";
	public static final String ERROR_FIT_EVENT = "errorFitEvent";
	public static final String DISPLAY_SELECTION_EVENT = "displaySelectionEvent";
	public static final String DISPLAY_SELECTION_ANNOTATION_EVENT = "displaySelectionAnnotationEvent";
	public static final String MULTI_SELECTION_ALLOWED_EVENT = "multiSelectionAllowedEvent";
	private FitInterface fitInterface;

	private int nbSelection; // number of selection
	private int numSubSelection; // number of rectangle in current selection
	private List<List<FitRotationalRectangle>> allSelections;	// list of all the selection
	private List<FitRotationalRectangle> currentSelection;	// the current Selection
	private List<SelectionFitRotational> selectionsFit;
	private List<AreaInformation> listOfAreas;	// List of selection
	private boolean rectTrace;// true if a rectangle is traced // was in view.
	private boolean closeSelected; // true if the button "close selection" is clicked // was in view
	private boolean lastFit; // true if the fit is traced // was in view
	private boolean isFirstFitTrace; // true if the first fit is traced // was in view
	private boolean stopBuildFit; // value true if the built of the line fit is cancelled // was in topModel
	private Color currentColorRectangle;
	private boolean selectionVisible;
	private List<FitError> lastFitErrors;
	private boolean displayError;
	private Server server;
	private boolean closed;
	private boolean selectionAnnotationVisible;
	private boolean multiSelectionAllowed;


	/**
	 * Constructor.
	 *
	 * @param fitInterface The {@link FitInterface}.
	 */
	public RotationalFitModel(FitInterface fitInterface) {
		this.fitInterface = fitInterface;
		this.nbSelection = 0;
		this.numSubSelection = 0;
		this.currentSelection = new ArrayList<>();
		this.allSelections = new ArrayList<>();
		this.selectionsFit = new ArrayList<>();
		this.listOfAreas = new ArrayList<>();
		this.rectTrace = false;
		this.closeSelected = false;
		this.lastFit = false;
		this.isFirstFitTrace = false;
		this.stopBuildFit = false;
		this.currentColorRectangle = ColorsCurve.getNewColorRotationalFit();
		this.selectionVisible = true;
		this.displayError = true;
		this.closed = false;
		this.selectionAnnotationVisible = true;
		this.server = new ServerImpl();
	}

	/**
	 * Reset the model.
	 */
	public void reset() {
		FitRotationalRectangle.reset();
		clearAllSelections();
		this.rectTrace = false;
		this.closeSelected = false;
		this.lastFit = false;
		this.isFirstFitTrace = false;
		fitInterface.removeAllFitSeries();
		fitInterface.removeAllFitRectangleSeries();
		this.setSelectionAnnotationVisible(true);
		this.setSelectionVisible(true);
	}

	/**
	 * Clear all selections.
	 */
	public void clearAllSelections() {
		nbSelection = 0;
		numSubSelection = 0;
		closed = false;
		allSelections.clear();
		listOfAreas.clear();
		selectionsFit.clear();
		currentSelection.clear();
		fitInterface.removeBlockAnnotations();
	}

	/**
	 * Close the current selection.
	 */
	public void closeSelection() {
		this.closed = true;
		this.closeSelected = true;
		this.lastFit = false;

		AreaInformation area = new AreaInformation(currentSelection);
		listOfAreas.add(area);

		nbSelection++;
		numSubSelection = 0;

		currentColorRectangle = ColorsCurve.getNewColorRotationalFit();
		allSelections.add(currentSelection);
		currentSelection = new ArrayList<>();
		this.rectTrace = false;
	}

	/**
	 * Clear the current selection.
	 *
	 * @return the series removed by the operation.
	 */
	public List<XYSeriesFitBorder> clearCurrentSelection() {
		List<XYSeriesFitBorder> l = new ArrayList<>(2);

		numSubSelection = 0;

		if (currentSelection.isEmpty() && nbSelection >= 1)  {
			nbSelection--;
			allSelections.remove(allSelections.size() - 1);
		}
		currentSelection = new ArrayList<>();
		List<SelectionFitRotational> d = new ArrayList<>(2);
		for (SelectionFitRotational sfr : selectionsFit) {
			if (sfr.getNumSelection() == nbSelection) {
				l.addAll(sfr.getRectangleBorders());
				d.add(sfr);
			}
		}
		for (SelectionFitRotational sfr : d) {
			selectionsFit.remove(sfr);
		}
		fitInterface.removeBlockAnnotations(nbSelection + 1);

		if (currentSelection.isEmpty() && allSelections.isEmpty()) {
			fireDataChanged(new ModelChangedEvent(RESET_SELECTION_BUTTON_EVENT, Boolean.FALSE));
		}
		return l;
	}

	/**
	 * Return the number of selection.
	 *
	 * @return the number of selection.
	 */
	public int getAllSelectionsSize() {
		return allSelections.size();
	}

	/**
	 * Return the number of selection.
	 *
	 * @return the number of selection.
	 */
	public int getNbSelection() {
		return nbSelection;
	}

	/**
	 * Remove the last selection.
	 */
	public void removeLastSelection() {
		allSelections.remove(allSelections.size() - 1);
	}

	/**
	 * Return the number of "Rectangle" on the current selection.
	 *
	 * @return the number of rectangle on the current selection.
	 */
	public int getNbCurrentSelection() {
		return currentSelection.size();
	}

	/**
	 * Return the list of rectangles of the current selection.
	 *
	 * @return the list of rectangles of the current selection.
	 */
	public List<FitRotationalRectangle> getCurrentSelection() {
		return currentSelection;
	}

	/**
	 * Add a fit rectangle selection to the current selection.
	 *
	 * @param rec The rectangle to add.
	 */
	public void addFitRectangle(FitRotationalRectangle rec) {
		currentSelection.add(rec);
	}

	/**
	 * Create the {@link XYSeriesFitBorder} associated with a selection.
	 *
	 * @param selectionColor The color of the selection.
	 * @param numSelection Number of the selection.
	 * @param i Number of the current selection.
	 * @return The list of {@link XYSeriesFitBorder} associated with the selection.
	 */
	public List<XYSeriesFitBorder> createSerieRectangle(Color selectionColor,
			int numSelection, int i) {
		// recovery values of clicks of the mouse
		double x1 = currentSelection.get(i).getLeft();
		double y1 = currentSelection.get(i).getUp();
		double x2 = currentSelection.get(i).getRight();
		double y2 = currentSelection.get(i).getDown();

		// initialization of series of data
		XYSeriesFitBorder pointsRectangleUp = new XYSeriesFitBorder(
				"FitUp_" + numSelection + '_' + numSubSelection,
				selectionColor, RotationalTypeCurve.FIT_COMPO);
		XYSeriesFitBorder pointsRectangleDown = new XYSeriesFitBorder(
				"FitDown_" + numSelection + '_' + numSubSelection,
				selectionColor, RotationalTypeCurve.FIT_COMPO);
		XYSeriesFitBorder pointsRectangleLeft = new XYSeriesFitBorder(
				"FitLeft_" + numSelection + '_' + numSubSelection,
				selectionColor, RotationalTypeCurve.FIT_COMPO);
		XYSeriesFitBorder pointsRectangleRight = new XYSeriesFitBorder(
				"FitRight_" + numSelection + '_' + numSubSelection,
				selectionColor, RotationalTypeCurve.FIT_COMPO);

		numSubSelection++;

		// create the lines of the rectangle
		pointsRectangleUp.add(x1, y1);
		pointsRectangleUp.add(x2, y1);

		pointsRectangleRight.add(x2, y1);
		pointsRectangleRight.add(x2, y2);

		pointsRectangleDown.add(x2, y2);
		pointsRectangleDown.add(x1, y2);

		pointsRectangleLeft.add(x1, y2);
		pointsRectangleLeft.add(x1, y1);

		List<XYSeriesFitBorder> listSeries = new ArrayList<>(4);
		listSeries.add(pointsRectangleUp);
		listSeries.add(pointsRectangleDown);
		listSeries.add(pointsRectangleLeft);
		listSeries.add(pointsRectangleRight);

		return listSeries;
	}

	/**
	 * Display a rectangle.
	 *
	 * @param xLeft The X left value.
	 * @param yBottom The Y bottom value.
	 * @param xRight The X right value.
	 * @param yTop The Y top value.
	 */
	public void displayRectangle(double xLeft, double yBottom, double xRight,
			double yTop) {
		addFitRectangle(new FitRotationalRectangle(xLeft, yTop, xRight, yBottom,
				currentColorRectangle));

		List<XYSeriesFitBorder> listSeries = createSerieRectangle(
				currentColorRectangle, nbSelection, getNbCurrentSelection() - 1);
		for (XYSeriesFitBorder series : listSeries) {
			fitInterface.addFitRectangleSerie(series);
		}
		if (selectionAnnotationVisible) {
			fitInterface.addBlockAnnotation(new RotationalTextAnnotation(xLeft,
					yTop, (nbSelection + 1), currentColorRectangle));
		}
		SelectionFitRotational selec = new SelectionFitRotational(listSeries,
				nbSelection, currentColorRectangle);
		selectionsFit.add(selec);
		this.closed = false;
	}

	/**
	 * Reset the last selection.
	 */
	public void resetLastSelection() {
		if (!listOfAreas.isEmpty() && closed) {
			listOfAreas.remove(listOfAreas.size() - 1);
			List<XYSeriesFitBorder> listSeries = clearCurrentSelection();

			for (XYSeriesFitBorder series : listSeries) {
				fitInterface.removeFitRectangleSerie(series);
			}

			fireDataChanged(new ModelChangedEvent(CLOSE_SELECTION_BUTTON_EVENT, Boolean.FALSE));

			if (!closeSelected && lastFit) {
				if (nbSelection > 0) {
					fireDataChanged(new ModelChangedEvent(DO_DISPLAY_BUTTON_CLICK_EVENT));
				} else {
					closeSelected = false;
					rectTrace = false;
				}
			}
			this.closed = true;
		} else { // the selection isn't closed
			List<XYSeriesFitBorder> listSeries = clearCurrentSelection();

			for (XYSeriesFitBorder series : listSeries) {
				fitInterface.removeFitRectangleSerie(series);
			}

			fireDataChanged(new ModelChangedEvent(CLOSE_SELECTION_BUTTON_EVENT, Boolean.FALSE));
			fireDataChanged(new ModelChangedEvent(DISPLAY_BUTTON_EVENT, Boolean.TRUE));
			rectTrace = false;
			this.closed = true;
		}
	}

	/**
	 * Reset all selections.
	 */
	public void resetAllSelections() {
		// Reset all selections
		fitInterface.removeAllFitRectangleSeries();
		clearAllSelections();

		// Clear informations of fits
		if (isFirstFitTrace) {
			fitInterface.removeAllFitSeries();
		}
		isFirstFitTrace = false;
		lastFit = false;
	}

	/**
	 * Display selections.
	 */
	public void displayFitPlot() {
		if (fitInterface.getDataSeries().getSeriesCount() == 0) {
			return;
		}
		// trace the fit of selection(s)
		lastFit = true;
		fitInterface.removeAllFitSeries();
		if (nbSelection == 0) {
			fitInterface.computeSelection();
			double xval = (FitRotationalRectangle.getRightBig() - FitRotationalRectangle.getLeftBig()) / 10;
			double yval = (FitRotationalRectangle.getUpBig() - FitRotationalRectangle.getDownBig()) / 10;

			if (isMultiSelectionAllowed()) {
				fireDataChanged(new ModelChangedEvent(CLOSE_SELECTION_BUTTON_EVENT, Boolean.TRUE));
				fireDataChanged(new ModelChangedEvent(RESET_SELECTION_BUTTON_EVENT, Boolean.TRUE));
			}
			displayRectangle(FitRotationalRectangle.getLeftBig() - xval,
					FitRotationalRectangle.getDownBig() - yval,
					FitRotationalRectangle.getRightBig() + xval,
					FitRotationalRectangle.getUpBig() + yval);
			this.rectTrace = true;
			closeSelection();
			displayFitPlot();
		} else {
			// build lines fit
			List<XYSeriesFitArea> fitLines = buildAreaFit(listOfAreas);
			// if the fit cannot build (temperature < 0), delete of the
			// rectangle
			if (stopBuildFit) {
				// delete the area of the rectangle
				listOfAreas.remove(listOfAreas.size() - 1);
				fitInterface.removeAllFitSeries();

				// trace other fit
				if (!listOfAreas.isEmpty()) {
					fireDataChanged(new ModelChangedEvent(DO_DISPLAY_BUTTON_CLICK_EVENT));
				}
			} else {
				fitInterface.addFitSeries(fitLines);
			}
			closeSelected = false;
			rectTrace = false;
		}
	}

	/**
	 * Do the fit.
	 *
	 * @param areaList The list of fit area.
	 * @return the list of fit series.
	 */
	private List<XYSeriesFitArea> buildAreaFit(List<AreaInformation> areaList) {
		List<XYSeriesFitArea> l = new ArrayList<>();
		this.stopBuildFit = false;
		int j;
		List<FitError> errors = new ArrayList<>();
		for (int i = 0; i < areaList.size(); i++) {
			AreaInformation currentArea = listOfAreas.get(i);
			j = 0;
			for (RotationalFitComponent rfc : getListFitComponent(currentArea)) {
				XYSeriesFitArea coordinateSerie = new XYSeriesFitArea(
						"Fit_" + i + '_' + j, RotationalTypeCurve.FIT,
						rfc, (i+1));
				double[] aAndB = null;
				try {
					aAndB = RotationalFitModel.fitData(rfc);
				} catch (Exception e) {
					LOGGER.error("An error occured during the file of the data", e);
					errors.add(new FitError(rfc.getTag(), rfc.getCompNumber(), (i+1), FitError.ERROR.UNKNOW));
					continue;
				}

				// Store displayed datas
				LineInformation lineInfo = new LineInformation();
				// add A and B to create the line y=Ax+B
				lineInfo.setA(aAndB[0]);
				lineInfo.setB(aAndB[1]);
				lineInfo.setReducedChiSquared(aAndB[4]);
				lineInfo.setChiSquared(aAndB[5]);
				lineInfo.setChiProbability(aAndB[6]);
				lineInfo.setXMinimum(rfc.getXMin());
				lineInfo.setXMaximum(rfc.getXMax());
				// Compute temperature T
				double t = calcTemperature(aAndB);
				lineInfo.setTemperatureDev(calcTemperatureDev(aAndB));

				// Compute Density N
				lineInfo.setTemperature(t);
				if (t < 0 && !this.stopBuildFit) {
					errors.add(new FitError(rfc.getTag(), rfc.getCompNumber(),
							(i+1), FitError.ERROR.NEGATIVE_TEMPERATURE));
					continue;
				} else if (!this.stopBuildFit) {
					// Connection to the server in order to calculate the value of qt
					// connect to the server, and exchange of values
					double qt = server.getTemperatureRDCommand(t, rfc.getTag());
					if (Double.isNaN(qt)) {
						this.stopBuildFit = true;
						errors.add(new FitError(rfc.getTag(), rfc.getCompNumber(),
								(i+1), FitError.ERROR.UNKNOW_MOL_IN_DB));
						continue;
					}
					lineInfo.setDensity(calcDensity(aAndB, qt));
					lineInfo.setDensityDev(calcDensityDev(aAndB, qt));
					coordinateSerie.setLineInformation(lineInfo);

					// add in the series the values of the point max and the point
					// min of the fit
					coordinateSerie.add(lineInfo.getXMinimum(),
							lineInfo.getA() * lineInfo.getXMinimum() + lineInfo.getB());
					coordinateSerie.add(lineInfo.getXMaximum(),
							lineInfo.getA() * lineInfo.getXMaximum() + lineInfo.getB());

					// Color of the selection on multiselection, else take the
					//  points Color (in RotationalModel).
					if (multiSelectionAllowed) {
						coordinateSerie.getConfigCurve().setColor(currentArea.getColor());
						lineInfo.setColor(currentArea.getColor());
					}
					l.add(coordinateSerie);
				}
				j++;
			}
		}
		if (!errors.isEmpty()) {
			this.lastFitErrors = errors;
			if (displayError) {
				fireDataChanged(new ModelChangedEvent(ERROR_FIT_EVENT, errors));
			}
		}
		return l;
	}

	/**
	 * Create if needed (=not in the provided list) then return a
	 *  {@link RotationalFitComponent}.
	 *
	 * @param list List of current {@link RotationalFitComponent}.
	 * @param tag Tag of the component.
	 * @param numCompo Number of the component.
	 * @param type {@link RotationalTypeCurve} of the component.
	 * @return The {@link RotationalFitComponent} with the given parameters.
	 */
	private RotationalFitComponent getRotationalFitComponent(
			List<RotationalFitComponent> list, int tag, int numCompo,
			RotationalTypeCurve type) {
		for (RotationalFitComponent rfc : list) {
			if (rfc.getTag() == tag &&
					rfc.getCompNumber() == numCompo &&
					RotationalTypeCurve.isCompatible(rfc.getGeneralType(), type)) {
				return rfc;
			}
		}
		RotationalFitComponent rfc = new RotationalFitComponent(tag, numCompo,
				RotationalTypeCurve.getTopType(type));
		list.add(rfc);
		return rfc;
	}

	/**
	 * Return the {@link List} of {@link RotationalFitComponent} with at least
	 * point two points on the given area.
	 *
	 * @param aera The area.
	 * @return the list of {@link RotationalFitComponent} with point
	 */
	private List<RotationalFitComponent> getListFitComponent(AreaInformation aera) {
		aera.computeGoodPoints(fitInterface.getPointList());
		List<RotationalFitComponent> list = new ArrayList<>();
		XYIntervalSeriesCollection dataSeries = fitInterface.getDataSeries();
		for (int i = 0; i < dataSeries.getSeriesCount(); i++) {
			XYRotationalIntervalSeries series = (XYRotationalIntervalSeries) dataSeries.getSeries(i);
			RotationalFitComponent rfc = getRotationalFitComponent(list,
					series.getTag(), series.getNumComponent(), series.getType());
			for (Point p : series.getPointSigmaList()) {
				if (aera.contains(p.getX(), p.getY())) {
					rfc.addPoint(p.getX(), p.getY(), p.getSigma());
				}
			}
		}

		for (Iterator<RotationalFitComponent> iterator = list.iterator(); iterator.hasNext();) {
			RotationalFitComponent rfc = iterator.next();
			if (rfc.getListPoints().size() < 2) {
				iterator.remove();
			}
		}
		return list;
	}

	/**
	 * Compute the temperature.
	 *
	 * @param aAndB
	 *            a and b
	 * @return temperature
	 */
	private double calcTemperature(double[] aAndB) {
		return -1 / aAndB[0];
	}

	/**
	 * Compute the temperature.
	 *
	 * @param aAndB
	 *            a and b
	 * @return temperature
	 */
	private double calcTemperatureDev(double[] aAndB) {
		return aAndB[2] / Math.pow(aAndB[0], 2.);
	}

	/**
	 * Compute the density.
	 *
	 * @param aAndB
	 *            a and b
	 * @param qt
	 *            qt
	 * @return density
	 */
	private double calcDensity(double[] aAndB, double qt) {
		return qt * Math.exp(aAndB[1]);
	}

	/**
	 * Compute the density dev.
	 *
	 * @param aAndB
	 *            a and b
	 * @param qt
	 *            qt
	 * @return density
	 */
	private double calcDensityDev(double[] aAndB, double qt) {
		return qt * Math.exp(aAndB[1]) * aAndB[3];
	}

	/**
	 * Compute the fit.
	 *
	 * @param rfc The {@link RotationalFitComponent} for the fit.
	 * @return Data of fit.
	 * @throws Exception In case of fitting error.
	 */
	public static double[] fitData(RotationalFitComponent rfc) throws Exception {
		double[] res = new double[7];
		int size = rfc.getListPoints().size();
		double[] xdata = new double[size];
		double[] ydata = new double[size];
		double[] yerrorData = new double[size];

		for (int i = 0; i < size; i++) {
			xdata[i] = rfc.getListPoints().get(i).getX();
			ydata[i] = rfc.getListPoints().get(i).getY();
			yerrorData[i] = 1.0 / Math.pow(rfc.getListPoints().get(i).getSigma(), 2) ;
		}

		PolynomialModel poly = new PolynomialModel(1);

		Fitter fitter = new Fitter(new Double1d(xdata), poly);
		Double1d lineFit = fitter.fit(new Double1d(ydata), new Double1d(yerrorData));
		fitter.getStandardDeviation();

		res[0] = lineFit.get(1);
		res[1] = lineFit.get(0);

		if (size == 2) {
			res[2] = 0.0;
			res[3] = 0.0;
			res[4] = Double.POSITIVE_INFINITY;
			res[5] = Double.POSITIVE_INFINITY;
			res[6] = Double.POSITIVE_INFINITY;
		} else {
			Double1d stdev = fitter.getStandardDeviation();
			res[2] = stdev.get(1);
			res[3] = stdev.get(0);
			final int deg = 2 ;//degree of freedom
			res[4]= fitter.getChiSquared()/(size -deg);
			res[5]= fitter.getChiSquared();
			res[6]= (1-RotMath.chiSquareCDF(fitter.getChiSquared(), size -deg)) *100;
		}

		return res;
	}

	/**
	 * Change the value of rectTrace.
	 *
	 * @param newRectTrace The new rect trace value.
	 */
	public void setRectTrace(boolean newRectTrace) {
		this.rectTrace = newRectTrace;
	}

	/**
	 * Return if the selection is visible.
	 *
	 * @return if the selection is visible.
	 */
	public boolean isSelectionVisible() {
		return selectionVisible;
	}

	/**
	 * Change the visibility of the selection.
	 *
	 * @param visible The new visibility of the selection.
	 */
	public void setSelectionVisible(boolean visible) {
		this.selectionVisible = visible;
		this.fitInterface.setFitSelectionVisible(visible);
		fireDataChanged(new ModelChangedEvent(DISPLAY_SELECTION_EVENT));
	}

	/**
	 * Return if a rectangle is traced.
	 *
	 * @return true if a rectangle is traced.
	 */
	public boolean isRectTrace() {
		return rectTrace;
	}

	/**
	 * Return if close is selected.
	 *
	 * @return true if close is selected.
	 */
	public boolean isCloseSelected() {
		return closeSelected;
	}

	/**
	 * Return if the fit is traced.
	 *
	 * @return true if the fit is traced.
	 */
	public boolean isLastFit() {
		return lastFit;
	}

	/**
	 * Return if the first fit is traced.
	 *
	 * @return true if the first fit is traced.
	 */
	public boolean isFirstFitTrace() {
		return isFirstFitTrace;
	}

	/**
	 * Return the number of rectangle in current selection.
	 *
	 * @return the number of rectangle in current selection.
	 */
	public int getNumSubSelection() {
		return numSubSelection;
	}

	/**
	 * Create then return an error message for the last fit.
	 * Return an empty String is there was no error.
	 * Do no remove: used on script.
	 *
	 * @return an error message for the last fit or an empty String.
	 */
	public String getLastFitErrorsMessage() {
		if (this.lastFitErrors == null || this.lastFitErrors.isEmpty()) {
			return "";
		}
		return FitErrorMessageUtility.constructFitErrorMsg(this.lastFitErrors);
	}

	/**
	 * Set if a fit error will trigger an event (in order to be displayed on GUI).
	 *
	 * @param display if a fit error will trigger an event.
	 */
	public void setDisplayError(boolean display) {
		this.displayError = display;
	}

	/**
	 * Hide all fit "data" series (not the rectangle of the selection).
	 */
	public void hideSeries() {
		fitInterface.hideAllFitSeries();
	}

	/**
	 * Removes all fit series.
	 */
	public void removeAllFitSeries() {
		fitInterface.removeAllFitSeries();
	}

	/**
	 * Return if the selection annotation is visible.
	 *
	 * @return if the selection annotation is visible.
	 */
	public boolean isSelectionAnnotationVisible() {
		return selectionAnnotationVisible;
	}

	/**
	 * Change the visibility of the selection annotation.
	 *
	 * @param visible The new visibility of the selection annotation.
	 */
	public void setSelectionAnnotationVisible(boolean visible) {
		this.selectionAnnotationVisible = visible;
		fireDataChanged(new ModelChangedEvent(DISPLAY_SELECTION_ANNOTATION_EVENT));
		fitInterface.removeBlockAnnotations();
		if (visible) {
			for (RotationalTextAnnotation annotation : regenerateSelectionAnnotations()) {
				fitInterface.addBlockAnnotation(annotation);
			}
		}
	}

	/**
	 * Recreate all selections annotations then return them.
	 *
	 * @return the selections annotations.
	 */
	private List<RotationalTextAnnotation> regenerateSelectionAnnotations() {
		List<RotationalTextAnnotation> l = new ArrayList<>();

		int i = 0;
		for (; i < allSelections.size(); i++) {
			List<FitRotationalRectangle> recList = allSelections.get(i);
			for (FitRotationalRectangle rec : recList) {
				l.add(new RotationalTextAnnotation(rec.getLeft(), rec.getUp(),
						(i + 1), rec.getColor()));
			}
		}
		for (FitRotationalRectangle rec : currentSelection) {
			l.add(new RotationalTextAnnotation(rec.getLeft(), rec.getUp(),
					(i + 1), rec.getColor()));
		}
		return l;
	}

	/**
	 * Set the new allowed state of the multi selection.
	 *
	 * @param val the new allowed state of the multi selection.
	 */
	public void setMultiSelectionAllowed(boolean val) {
		this.multiSelectionAllowed = val;
		fireDataChanged(new ModelChangedEvent(MULTI_SELECTION_ALLOWED_EVENT, val));
	}

	/**
	 * Return if the multi selection is allowed.
	 *
	 * @return true if the multi selection is allowed, false otherwise.
	 */
	public boolean isMultiSelectionAllowed() {
		return this.multiSelectionAllowed;
	}
}
