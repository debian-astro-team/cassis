/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram.fit;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.Point;

/**
 * An area.
 */
public class AreaInformation {

	/**
	 * Together lines of the zone.
	 */
	private List<List<Double>> listPointXY;
	private List<FitRotationalRectangle> listRectangle;


	/**
	 * Constructor.
	 *
	 * @param listRectangle The list of rectangle/selection.
	 */
	public AreaInformation(List<FitRotationalRectangle> listRectangle) {
		this.listPointXY = new ArrayList<>();
		this.listRectangle = listRectangle;
	}

	/**
	 * Add a point.
	 *
	 * @param x The x value of the point.
	 * @param y The y value of the point.
	 */
	public void add(double x, double y) {
		if (!contains(x, y)) {
			List<Double> el = new ArrayList<>(2);
			el.add(x);
			el.add(y);
			listPointXY.add(el);
		}
	}

	/**
	 * Check if the area contain the point with the given parameters.
	 *
	 * @param x The x value of the point to check.
	 * @param y The y value of the point to check.
	 * @return if the area contain the point with the given parameters.
	 */
	public boolean contains(double x, double y) {
		for (List<Double> l : listPointXY) {
			if (l.get(0).doubleValue() == x && l.get(1).doubleValue() == y) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Clear old points then compute the good points

	 * @param points The points to compute.
	 */
	public void computeGoodPoints(List<Point> points) {
		listPointXY.clear();
		double xMin;
		double xMax;
		double yMin;
		double yMax;
		double xItem;
		double yItem;
		for (Point p : points) {
			// recover the coordinates of the i point of the diagram
			xItem = p.getX();
			yItem = p.getY();
			for (int j = 0; j < listRectangle.size(); j++) {
				xMin = listRectangle.get(j).getLeft();
				yMax = listRectangle.get(j).getUp();
				xMax = listRectangle.get(j).getRight();
				yMin = listRectangle.get(j).getDown();

				if (xItem > xMin && xItem < xMax && yItem > yMin && yItem < yMax) {
					this.add(xItem, yItem);
				}
			}
		}
	}

	/**
	 * Return the color of the selection.
	 *
	 * @return the color of the selection.
	 */
	public Color getColor() {
		return listRectangle.get(0).getColor();
	}
}
