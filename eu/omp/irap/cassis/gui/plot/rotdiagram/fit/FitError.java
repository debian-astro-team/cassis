/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram.fit;


/**
 * Describe a fit error.
 *
 * @author M. Boiziot
 */
public class FitError {

	public enum ERROR { NEGATIVE_TEMPERATURE, UNKNOW_MOL_IN_DB, UNKNOW }

	private final int tag;
	private final int component;
	private final int block;
	private final ERROR errorType;


	/**
	 * Constructor.
	 *
	 * @param theTag The tag.
	 * @param componentNumber The component number.
	 * @param blockNumber The block number.
	 * @param theErrorType The type of error.
	 */
	public FitError(int theTag, int componentNumber, int blockNumber, ERROR theErrorType) {
		this.tag = theTag;
		this.component = componentNumber;
		this.block = blockNumber;
		this.errorType = theErrorType;
	}

	/**
	 * Return the tag.
	 *
	 * @return the tag.
	 */
	public int getTag() {
		return this.tag;
	}

	/**
	 * Return the component number.
	 *
	 * @return the component number.
	 */
	public int getComponent() {
		return this.component;
	}

	/**
	 * Return the number of the block/selection.
	 *
	 * @return the number of the block/selection.
	 */
	public int getBlock() {
		return this.block;
	}

	/**
	 * Return the type of error.
	 *
	 * @return the type of error.
	 */
	public ERROR getErrorType() {
		return this.errorType;
	}

}
