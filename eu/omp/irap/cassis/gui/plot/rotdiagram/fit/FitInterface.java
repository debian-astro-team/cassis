/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.rotdiagram.fit;

import java.util.List;

import org.jfree.data.xy.XYIntervalSeriesCollection;

import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.Point;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalTextAnnotation;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.XYSeriesFitArea;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.XYSeriesFitBorder;

/**
 * Interface needed for the Rotational Diagram Fit functionality.
 *
 * @author M. Boiziot
 */
public interface FitInterface {

	/**
	 * Add a {@link List} of {@link XYSeriesFitArea} to the plot.
	 *
	 * @param fitSeries The series to add.
	 */
	void addFitSeries(List<XYSeriesFitArea> fitSeries);

	/**
	 * Add a selection/rectangle fit series to the plot.
	 *
	 * @param series The series to add.
	 */
	void addFitRectangleSerie(XYSeriesFitBorder series);

	/**
	 * Return the data series. It <b>MUST</b> be used only as readonly.
	 *
	 * @return the data series.
	 */
	XYIntervalSeriesCollection getDataSeries();

	/**
	 * Return the list of visible {@link Point}.
	 *
	 * @return the list of visible {@link Point}.
	 */
	List<Point> getPointList();

	/**
	 * Removes all fit selections series.
	 */
	void removeAllFitRectangleSeries();

	/**
	 * Removes all fit series.
	 */
	void removeAllFitSeries();

	/**
	 * Removes a specific selection series.
	 *
	 * @param series The series to removes.
	 */
	void removeFitRectangleSerie(XYSeriesFitBorder series);

	/**
	 * Change the fit selection visibility.
	 *
	 * @param visible if the fit selection should be visible.
	 */
	void setFitSelectionVisible(boolean visible);

	/**
	 * Hide all fit "data" series (not the rectangle of the selection).
	 */
	void hideAllFitSeries();

	/**
	 * Add a selection annotation to the plot.
	 *
	 * @param annotation The annotation to add.
	 */
	void addBlockAnnotation(RotationalTextAnnotation annotation);

	/**
	 * Remove the selections annotations with the given block number.
	 *
	 * @param blockNumber The block number of annotation to remove.
	 */
	void removeBlockAnnotations(int blockNumber);

	/**
	 * Remove all selections annotations.
	 */
	void removeBlockAnnotations();

	/**
	 * Compute the global selection of the fit for the visible points.
	 */
	void computeSelection();
}
