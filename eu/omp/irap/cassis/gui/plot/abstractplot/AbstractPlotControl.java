/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.abstractplot;

import java.io.File;
import java.util.EventObject;
import java.util.List;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.file.FileReaderCassis;
import eu.omp.irap.cassis.gui.fit.FitConstant;
import eu.omp.irap.cassis.gui.fit.FitPanelControl;
import eu.omp.irap.cassis.gui.fit.advanced.interfaces.FitLineEstimable;
import eu.omp.irap.cassis.gui.model.spectrumanalysis.CassisSpectrumListener;
import eu.omp.irap.cassis.gui.otherspecies.SpeciesListener;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import eu.omp.irap.cassis.gui.plot.infopanel.InfoPanelListener;
import eu.omp.irap.cassis.gui.plot.simple.ChangeRenderingInterface;
import eu.omp.irap.cassis.gui.plot.util.MarkerView;
import eu.omp.irap.cassis.gui.plot.util.SpectrumFitPanelInterface;
import eu.omp.irap.cassis.gui.util.DropCassisSpectrumListener;

public abstract class AbstractPlotControl implements MarkerView, FitLineEstimable, CassisSpectrumListener,  InfoPanelListener,
DropCassisSpectrumListener, SpeciesListener, SpectrumFitPanelInterface,
ModelListener, ChangeRenderingInterface {

	public AbstractPlotControl() {
	}

	public void doGaussianDefaultParametersAction(double startValue, double endValue, FitModelInterface model,
			FitPanelControl fitControl) {
		double maxAmplitude = 0;
		double x0 = 0;
		XYSpectrumSeries seriesToFit = model.getSeriesToFit();
		if (seriesToFit != null) {
			XAxisCassis xAxisCassisDisplay = model.getXAxisCassis();
			YAxisCassis yAxisCassisDisplay = model.getyAxis();
			CommentedSpectrum spectrum = seriesToFit.getSpectrum();
			List<CassisMetadata> cassisMetadataList = spectrum.getCassisMetadataList();
			YAxisCassis yAxisData = spectrum.getyAxis();
			double[] intensitys = spectrum.getIntensities(xAxisCassisDisplay);
			double[] xValues = spectrum.getXData(xAxisCassisDisplay);

			for (int i = 0; i < xValues.length; i++) {
				double xValue = xValues[i];
				if (xValue > startValue && xValue < endValue) {
					double yValue = intensitys[i];
					if (yValue > maxAmplitude) {
						maxAmplitude = yValue;
						x0 = xValue;
					}
				}
			}
			maxAmplitude = YAxisCassis.convert(maxAmplitude, yAxisData, yAxisCassisDisplay,
					xAxisCassisDisplay.convertToMHzFreq(x0), cassisMetadataList);

			fitControl.doGaussianDefaultParametersAction(maxAmplitude, x0,
					FitConstant.getFWHM_DEFAULT(xAxisCassisDisplay, model.getTelescope()));
		}

	}

	/**
	 * Open overlay. Used on script. Do not use outside script, user should use
	 *  the Spectrum Manager.
	 *
	 * @param file The data file to open.
	 */
	public void openOverlayDataFile(File file) {
		if (file == null || !file.exists()) {
			return;
		}
		CassisSpectrum cs = FileReaderCassis.createCassisSpectrumFromFile(file);
		cassisSpectrumEvent(new EventObject(cs));
	}

}

