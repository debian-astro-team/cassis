/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.save;

import java.io.File;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.file.FileManagerAsciiCassis;
import eu.omp.irap.cassis.file.FileManagerSimpleData;


/**
 * Save class for Full Spectrum.
 */
public class SaveFullSpectrum extends SaveAbstractPlot {

	/**
	 * Construct a SaveFullSpectrum.
	 *
	 * @param savePlotInterface The save plot interface.
	 */
	public SaveFullSpectrum(SavePlotInterface savePlotInterface) {
		super(savePlotInterface);
	}

	@Override
	protected void addSpecificExtension(JFileChooser fc) {
		fc.addChoosableFileFilter(new FileNameExtensionFilter("ASCII Spectrum file (*.spec)", "spec"));
		fc.addChoosableFileFilter(new FileNameExtensionFilter("Full Spectrum file (*.fus)","fus"));
		fc.addChoosableFileFilter(new FileNameExtensionFilter("Votable file (*.votable)", "votable"));
		fc.addChoosableFileFilter(new FileNameExtensionFilter("Fits file (*.fits)","fits"));
	}

	@Override
	public boolean savePlotAsciiCassis(File file) {
		if (saveInterface.isOnGallery()) {
			return false;
		}
		else {
			List<CommentedSpectrum> spectrums = saveInterface.getSpectrumPlot().getSpectrumsVisibleAndSavable();
			if (spectrums.isEmpty()) {
				return false;
			}

			FileManagerAsciiCassis fileManagerAsciiCassis = new FileManagerAsciiCassis(file);
			fileManagerAsciiCassis.save(file, spectrums.get(0));

			for (int i = 1; i < spectrums.size(); i++) {
				String path = (file != null) ? file.getParent() : System.getProperty("user.home");
				file = askNewFile(path, "fus", spectrums.get(i).getTitle());
				if (file != null) {
					fileManagerAsciiCassis = new FileManagerAsciiCassis(file);
					fileManagerAsciiCassis.save(file, spectrums.get(i));
				}
			}
			return true;
		}
	}

	public static boolean saveOnePlotAscii(File file,CommentedSpectrum spectrum) {
		FileManagerAsciiCassis fileManagerAsciiCassis = new FileManagerAsciiCassis(file);
		return fileManagerAsciiCassis.save(file, spectrum);
	}


	public static boolean saveOnePlotAscii(File file,CassisSpectrum spectrum) {
		FileManagerAsciiCassis fileManagerAsciiCassis = new FileManagerAsciiCassis(file);
		return fileManagerAsciiCassis.save(file, spectrum);

	}

	@Override
	public boolean savePlotSpec(File file) {
		if (saveInterface.isOnGallery()) {
			return false;
		}
		else {
			List<CommentedSpectrum> spectrums = saveInterface.getSpectrumPlot().getSpectrumsVisibleAndSavable();
			if (spectrums.isEmpty()) {
				return false;
			}

			FileManagerSimpleData fileManagerSim = new FileManagerSimpleData(file);
			fileManagerSim.save(file, spectrums.get(0));

			for (int i = 1; i < spectrums.size(); i++) {
				String path = (file != null) ? file.getParent() : System.getProperty("user.home");
				file = askNewFile(path, "spec", spectrums.get(i).getTitle());
				if (file != null) {
					fileManagerSim = new FileManagerSimpleData(file);
					fileManagerSim.save(file, spectrums.get(i));
				}
			}
			return true;
		}
	}
}
