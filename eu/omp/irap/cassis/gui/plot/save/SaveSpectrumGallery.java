/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.save;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.DefaultFontMapper;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;

import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.file.FileManagerAsciiCassis;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import eu.omp.irap.cassis.gui.plot.gallery.GallerySortPane;
import eu.omp.irap.cassis.gui.plot.simple.SpectrumPlot;
import eu.omp.irap.cassis.gui.plot.simple.util.SpectrumPlotSaveUtils;

public class SaveSpectrumGallery {

	private static final Logger LOGGER = LoggerFactory.getLogger(SaveSpectrumGallery.class);


	public boolean saveGallery(GallerySortPane gsp, File file, String typeFile) {
		if (!("png".equals(typeFile) || "pdf".equals(typeFile)))
			return false;

		BufferedImage bi = saveGallery(gsp);

		if ("png".equals(typeFile)) {
			try {
				return ImageIO.write(bi, "png", file);
			} catch (IOException e) {
				LOGGER.error("Error while saving the png image of the gallery", e);
				return false;
			}
		} else {
			try {
				return saveGalleryAsPDF(bi, file);
			} catch (Exception e) {
				LOGGER.error("Error while saving the pdf of the gallery", e);
				return false;
			}
		}
	}

	public BufferedImage saveGallery(GallerySortPane gsp) {
		// Change color
		Paint oldColor = null;
		Color oldColorPlot = null;
		Color colorGallery = null;

		colorGallery = gsp.getBackground();
		gsp.setBackground(Color.WHITE);
		gsp.getGallery().setBackground(Color.WHITE);

		List<SpectrumPlot> plots = gsp.getListSpectrumPlots();
		if (!plots.isEmpty()) {
			oldColor = plots.get(0).getJFreeChart().getBackgroundPaint();
			oldColorPlot = plots.get(0).getBackground();
			for (SpectrumPlot plot : plots) {
				plot.getJFreeChart().setBackgroundPaint(Color.WHITE);
				plot.setBackground(Color.WHITE);
			}
		}
		gsp.setSave(true);
		gsp.repaint();

		BufferedImage bi = new BufferedImage(gsp.getWidth(), gsp.getHeight(), BufferedImage.TYPE_INT_RGB);
		Graphics g = bi.createGraphics();
		gsp.paint(g);
		g.dispose();

		// Restor color
		gsp.setBackground(colorGallery);
		gsp.getGallery().setBackground(colorGallery);
		for (SpectrumPlot plot : plots) {
			plot.getJFreeChart().setBackgroundPaint(oldColor);
			plot.setBackground(oldColorPlot);
		}

		gsp.setSave(false);
		gsp.repaint();

		return bi;
	}

	private boolean saveGalleryAsPDF(BufferedImage bufferedImage, File file) throws Exception {
		try (BufferedOutputStream out = new BufferedOutputStream(
				new FileOutputStream(file.getCanonicalPath()))) {
			int width = 1000;
			int height = 750;
			Rectangle pagesize = new Rectangle(width, height);
			Document document = new Document();
			document.setPageSize(pagesize);
			try {
				PdfWriter writer = PdfWriter.getInstance(document, out);
				document.open();

				PdfContentByte cb = writer.getDirectContent();
				PdfTemplate tp = cb.createTemplate(width, height);
				Graphics2D g2 = tp.createGraphics(width, height,
						new DefaultFontMapper());
				g2.drawImage(bufferedImage, 0, 0, width, height, null);
				g2.dispose();
				cb.addTemplate(tp, 0, 0);
			} finally {
				document.close();
			}
		}
		return true;
	}

	public void saveGalleryToPdf(File file, SavePlotInterface saveInterface) {
		int nPlot = saveInterface.getGallerySortPane().getFilter().size();
		try (BufferedOutputStream out = new BufferedOutputStream(
				new FileOutputStream(file.getCanonicalPath()))) {
			int width = 1000;
			int height = 750;
			Rectangle pagesize = new Rectangle(width, height);
			Document document = new Document();
			document.setPageSize(pagesize);
			try {
				PdfWriter writer = PdfWriter.getInstance(document, out);
				document.open();

				saveInterface.switchTo(0, 0);
				for (int i = 0; i < nPlot; i++) {
					PdfContentByte cb = writer.getDirectContent();
					PdfTemplate tp = cb.createTemplate(1000, 750);
					Graphics2D g2 = cb.createGraphics(width, height);
					SpectrumPlotSaveUtils.saveToGraphics(
							saveInterface.getSpectrumPlot(), saveInterface,
							saveInterface.getMessageControl(), g2);
					cb.addTemplate(tp, 0, 0);
					document.newPage();
					saveInterface.moveRight();
				}
				saveInterface.switchBack();
			} catch (DocumentException e) {
				LOGGER.error("Error while saving the pdf of the gallery", e);
			} finally {
				document.close();
				saveInterface.restorePanel(saveInterface.getMessageControl().getLayeredPane());
			}
		} catch (IOException e) {
			LOGGER.error("Error while saving the pdf of the gallery", e);
		}
	}

	/**
	 * Search all the visible and savable spectra with the given name, then save them.
	 *
	 * @param file The file to save.
	 * @param saveInterface The save interface.
	 * @param nameSpectrum The name of the spectrum to save.
	 * @return true if the operation succeed, false otherwise.
	 */
	public boolean saveAsciiCassisOnGallery(File file, SavePlotInterface saveInterface, String nameSpectrum) {
		FileManagerAsciiCassis fmac = new FileManagerAsciiCassis(null);
		List<CommentedSpectrum> spectrums = new ArrayList<>(
				saveInterface.getGallerySortPane().getFilter().size());
		for (int num : saveInterface.getGallerySortPane().getFilter()) {
			XYSpectrumSeries s = saveInterface.getListCassisPlots().get(num).getSavableSeries(nameSpectrum);
			if (s != null) {
				spectrums.add(s.getSpectrum());
			}
		}
		return fmac.saveOnGallery(file, spectrums);
	}

}
