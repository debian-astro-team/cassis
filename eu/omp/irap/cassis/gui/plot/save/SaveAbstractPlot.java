/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.save;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.file.FileManagerAsciiCassis;
import eu.omp.irap.cassis.file.FileManagerFits;
import eu.omp.irap.cassis.file.FileManagerVOTable;
import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassisCollection;
import eu.omp.irap.cassis.gui.plot.simple.util.SpectrumPlotSaveUtils;
import eu.omp.irap.cassis.gui.util.FileUtils;
import eu.omp.irap.cassis.properties.Software;

/**
 * Save class for AbstractPlot derivates class.
 */
public class SaveAbstractPlot extends CassisSaveAbstract {

	private static final Logger LOGGER = LoggerFactory.getLogger(SaveAbstractPlot.class);
	protected SavePlotInterface saveInterface;
	protected SaveSpectrumGallery saveSpectrumGallery;


	public SaveAbstractPlot(SavePlotInterface savePlotInterface) {
		super();
		this.saveInterface = savePlotInterface;
		saveSpectrumGallery = new SaveSpectrumGallery();
	}

	@Override
	public JFileChooser getfc() {
		if ((saveInterface.getListCassisPlots() != null && saveInterface.getListCassisPlots().isEmpty()) ||
			(!saveInterface.isOnGallery() && saveInterface.getSpectrumPlot().getNbSeriesVisibleAndSavable() == 0)) {
			return null;
		}
		JFileChooser fc = super.getfc();
		addSpecificExtension(fc);

		String filename;
		if (saveInterface.isOnGallery()) {
			if (!saveInterface.getVisibleSavableSpectra().isEmpty()) {
				filename = saveInterface.getVisibleSavableSpectra().get(0);
			} else {
				filename = "";
			}
		} else {
			if (saveInterface.getSpectrumPlot().getSeriesVisibleAndSavable().getSeriesCount() >= 1) {
				filename = saveInterface.getSpectrumPlot().getSeriesVisibleAndSavable().getSeries(0).getKey().toString();
			} else {
				filename = "";
			}
		}

		fc.setSelectedFile(new File(filename));
		return fc;
	}

	protected void addSpecificExtension(JFileChooser fc) {
		fc.addChoosableFileFilter(new FileNameExtensionFilter("Line Spectrum file (*.lis)","lis"));
		if (!saveInterface.isOnGallery()) {
			fc.addChoosableFileFilter(new FileNameExtensionFilter("Votable file (*.votable)", "votable"));
			fc.addChoosableFileFilter(new FileNameExtensionFilter("Fits file (*.fits)","fits"));
		}
	}

	@Override
	public boolean savePlotPdf(File file) {
		if (saveInterface.isOnGallery()) {
			try {
				saveSpectrumGallery.saveGalleryToPdf(file,saveInterface);
			} catch (Exception e) {
				LOGGER.error("Error while saving the pdf.", e);
				return false;
			}
		} else {
			SpectrumPlotSaveUtils.saveToPdf(saveInterface.getSpectrumPlot(),
					saveInterface, saveInterface.getMessageControl(), file);
		}
		return true;
	}

	@Override
	public boolean savePlotPng(File file) {
		if (saveInterface.isOnGallery()) {
			return saveSpectrumGallery.saveGallery(
					saveInterface.getGallerySortPane(), file, "png");
		} else {
			return SpectrumPlotSaveUtils.saveToPng(saveInterface.getSpectrumPlot(),
					file, saveInterface.getMessageControl());
		}
	}

	@Override
	public boolean savePlotAsciiCassis(File file) {
		if (saveInterface.isOnGallery()) {
			String pathFolder = file.getParent();
			List<String> listSavable = saveInterface.getVisibleSavableSpectra();
			if (listSavable.isEmpty()) {
				return false;
			}
			saveSpectrumGallery.saveAsciiCassisOnGallery(file, saveInterface, listSavable.get(0));
			listSavable.remove(0);
			for (String spec : listSavable) {
				file = askNewFile(pathFolder, "lis", spec);
				if (file != null) {
					if (!file.getPath().endsWith(".lis")) {
						file = new File(file.getPath() + ".lis");
					}
					saveSpectrumGallery.saveAsciiCassisOnGallery(file, saveInterface, spec);
				}
			}
			return true;
		}
		else {
			List<CommentedSpectrum> spectrums = saveInterface.getSpectrumPlot().getSpectrumsVisibleAndSavable();
			if (spectrums.isEmpty()) {
				return false;
			}

			FileManagerAsciiCassis fileManagerAsciiCassis = new FileManagerAsciiCassis(file);
			return fileManagerAsciiCassis.save(file, spectrums);
		}
	}

	@Override
	public boolean savePlotVOTable(File file) {
		if (saveInterface.isOnGallery()) {
			return false;
		}
		else {
			List<CommentedSpectrum> spectrums = saveInterface.getSpectrumPlot().getSpectrumsVisibleAndSavable();
			if (spectrums.isEmpty()) {
				return false;
			}

			FileManagerVOTable fileManagerVOTable = new FileManagerVOTable(file);
			fileManagerVOTable.save(file, spectrums.get(0));

			for (int i = 1; i < spectrums.size(); i++) {
				String path = (file != null) ? file.getParent() : System.getProperty("user.home");
				file = askNewFile(path, "votable", spectrums.get(i).getTitle());
				if (file != null) {
					fileManagerVOTable = new FileManagerVOTable(file);
					fileManagerVOTable.save(file, spectrums.get(i));
				}
			}
			return true;
		}
	}

	@Override
	public boolean savePlotFits(File file) {
		if (saveInterface.isOnGallery()) {
			return false;
		}
		else {
			List<CommentedSpectrum> spectrums = saveInterface.getSpectrumPlot().getSpectrumsVisibleAndSavable();
			if (spectrums.isEmpty()) {
				return false;
			}

			FileManagerFits fileManagerFits = new FileManagerFits(file);
			fileManagerFits.save(file, spectrums.get(0));

			for (int i = 1; i < spectrums.size(); i++) {
				String path = (file != null) ? file.getParent() : System.getProperty("user.home");
				file = askNewFile(path, "fits", spectrums.get(i).getTitle());
				if (file != null) {
					fileManagerFits = new FileManagerFits(file);
					fileManagerFits.save(file, spectrums.get(i));
				}
			}
			return true;
		}
	}

	@Override
	public boolean savePlotSpec(File file) {
		return false;
	}

	@Override
	public boolean savePlotAscii(File file) {
		return false;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.save.CassisSaveViewInterface#saveForSamp(java.io.File)
	 */
	@Override
	public int saveForSamp(File file) {
		XYSeriesCassisCollection seriesVisibleAndSavable = saveInterface.getSpectrumPlot().getSeriesVisibleAndSavable();
		if (seriesVisibleAndSavable == null || file == null)
			return SaveGraphic.ERROR;
		else
			return seriesVisibleAndSavable.saveForSamp(file);
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.save.CassisSaveViewInterface#isSampSendPossible()
	 */
	@Override
	public boolean isSampSendPossible() {
		return true;
	}

	/**
	 * Allow to choose a new file when saving many.
	 *
	 * @param path The path of the folder.
	 * @param filter format ex : votable, lis
	 * @param filename The name of the file.
	 * @return The new selected file.
	 */
	public File askNewFile(String path, String filter, String filename) {
		JFileChooser fc = new CassisJFileChooser(Software.getDataPath(), Software.getLastFolder("data"));
		fc.setSelectedFile(new File(filename));
		fc.resetChoosableFileFilters();
		fc.addChoosableFileFilter(new FileNameExtensionFilter("*"+filter, filter));
		fc.setDialogType(JFileChooser.SAVE_DIALOG);

		int returnVal = fc.showSaveDialog(null);
		String ext = fc.getFileFilter().getDescription().substring(2);
		ext = ext.substring(1, ext.length() - 1);

		File file = null;
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			Software.setLastFolder("data", fc.getCurrentDirectory().getAbsolutePath());
			File temp = fc.getSelectedFile();
			String fileName = temp.getPath();
			if (!FileUtils.getFileExtension(fileName).equals(ext))
				fileName = fileName + "." + filter;

			file = new File(fileName);

			// replace the existing file or not
			if (file.exists()) {
				String message = "This file " + file.toString() + " already exists.\n" + "Do you want to replace it?";
				// Modal dialog with yes/no button
				int result = JOptionPane.showConfirmDialog(null, message, "Replace existing file?",
						JOptionPane.YES_NO_OPTION);
				if (result == JOptionPane.NO_OPTION) {
					file = null;
				}
			}
		}
		return file;
	}

	@Override
	public BufferedImage getImageToPrint() {
		if (saveInterface.isOnGallery()) {
			return saveSpectrumGallery.saveGallery(saveInterface.getGallerySortPane());
		} else {
			return SpectrumPlotSaveUtils.getImageToSave(
					saveInterface.getSpectrumPlot(), saveInterface.getMessageControl());
		}
	}
}
