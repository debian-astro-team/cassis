/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.save;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import javax.swing.JComponent;

import org.jfree.chart.plot.XYPlot;

import com.lowagie.text.Document;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.DefaultFontMapper;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Class which allows to save the plot as pdf or png document.
 *
 * @author Rose Nerriere
 * @since 09/07/2007
 */
public class SaveGraphic {

	public static final String FAMILY = "Lucida Bright";
	public static final int SUCCESS = 0;
	public static final int ERROR = 1;
	public static final int CANCELED = 2;
	public static final int NOTHING_TO_SAVE = 3;

	private SaveGraphic() {
	}

	/**
	 * Change the shape of ticks.
	 *
	 * @param plot The {@link XYPlot}.
	 * @param length The length of the tick to set.
	 * @param width The width of the tick to set.
	 */
	public static void changeTicks(XYPlot plot, float length, float width) {
		plot.getDomainAxis().setTickMarkInsideLength(length);
		plot.getDomainAxis().setTickMarkStroke(new BasicStroke(width));

		plot.getRangeAxis().setTickMarkInsideLength(length);
		plot.getRangeAxis().setTickMarkStroke(new BasicStroke(width));

		plot.getDomainAxis(1).setTickMarkInsideLength(length);
		plot.getDomainAxis(1).setTickMarkStroke(new BasicStroke(width));

		plot.getRangeAxis(1).setTickMarkInsideLength(length);
		plot.getRangeAxis(1).setTickMarkStroke(new BasicStroke(width));
	}

	/**
	 * Save a JComponent to a pdf file.
	 *
	 * @param jp The JComponent to save.
	 * @param file The output file.
	 * @throws Exception In case of error.
	 */
	public static void saveAsPDF(JComponent jp, File file) throws Exception {
		try (BufferedOutputStream out = new BufferedOutputStream(
				new FileOutputStream(file.getCanonicalPath()))) {
			int width = 1000;
			int height = 750;
			Rectangle pagesize = new Rectangle(width, height);
			Document document = new Document();
			document.setPageSize(pagesize);
			try {
				PdfWriter writer = PdfWriter.getInstance(document, out);
				document.open();

				BufferedImage bufferedImage = new BufferedImage(jp.getWidth(),
						jp.getHeight(), BufferedImage.TYPE_INT_RGB);

				Graphics2D g2d = bufferedImage.createGraphics();
				jp.paint(g2d);

				PdfContentByte cb = writer.getDirectContent();
				PdfTemplate tp = cb.createTemplate(width, height);
				Graphics2D g2 = tp.createGraphics(width, height,
						new DefaultFontMapper());
				g2.drawImage(bufferedImage, 0, 0, width, height, null);
				g2.dispose();
				cb.addTemplate(tp, 0, 0);
			} finally {
				document.close();
			}
		}
	}

}
