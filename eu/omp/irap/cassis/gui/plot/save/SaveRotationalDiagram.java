/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.save;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Stroke;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.chart.renderer.xy.XYErrorRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.AbstractIntervalXYDataset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.database.access.AccessDataBase;
import eu.omp.irap.cassis.database.access.DataBaseConnection;
import eu.omp.irap.cassis.file.rot.ReadNewNewFile;
import eu.omp.irap.cassis.gui.model.parameter.rotationaltuning.TYPE_TEMPERATURE;
import eu.omp.irap.cassis.gui.plot.popup.MessageControl;
import eu.omp.irap.cassis.gui.plot.popup.MessagePanel;
import eu.omp.irap.cassis.gui.plot.rotdiagram.RotationalModel;
import eu.omp.irap.cassis.gui.plot.rotdiagram.RotationalView;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.RotationalTextAnnotation;
import eu.omp.irap.cassis.gui.plot.rotdiagram.curve.XYRotationalIntervalSeries;
import eu.omp.irap.cassis.parameters.PointInformation;
import eu.omp.irap.cassis.parameters.RotationalDiagramComponentResult;
import eu.omp.irap.cassis.parameters.RotationalDiagramMoleculeResult;
import eu.omp.irap.cassis.properties.Software;

/**
 * Class to do the save of RotationalDiagram.
 *
 * @author M. Boiziot
 */
public class SaveRotationalDiagram extends CassisSaveAbstract {

	private static final Logger LOGGER = LoggerFactory.getLogger(SaveRotationalDiagram.class);
	private RotationalView view;


	/**
	 * Constructor.
	 *
	 * @param rotationalView The rotational view.
	 */
	public SaveRotationalDiagram(RotationalView rotationalView) {
		super();
		this.view = rotationalView;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.save.CassisSaveAbstract#getfc()
	 */
	@Override
	public JFileChooser getfc() {
		JFileChooser fc = super.getfc();
		fc.addChoosableFileFilter(new FileNameExtensionFilter("Rotational Diagram file (*.rotd)", "rotd"));
		return fc;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.save.CassisSaveViewInterface#savePlotPng(java.io.File)
	 */
	@Override
	public boolean savePlotPng(File file) {
		BufferedImage image = getImageForPngSave();

		try {
			ImageIO.write(image, "png", file);
		} catch (IOException e) {
			LOGGER.error("Error while saving the png image", e);
			return false;
		}
		return true;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.save.CassisSaveViewInterface#savePlotPdf(java.io.File)
	 */
	@Override
	public boolean savePlotPdf(File file) {
		XYPlot plot = view.getPlot();
		JFreeChart chart = view.getChart();
		JComponent west = view.getWestPanel();
		JComponent comp = view.getLayeredPane();
		ChartPanel chartPanel = view.getChartPanel();
		view.getChartMouseCassisListener().getMessageControl().setAllowNewImage(false);
		west.remove(comp);

		// Backup
		Paint bgInsideColor = plot.getBackgroundPaint();
		Paint bgOutsideColor = chart.getBackgroundPaint();
		Paint domainTickLabelPaint = plot.getDomainAxis(1).getTickLabelPaint();
		Paint rangeTickLabelPaint = plot.getRangeAxis(1).getTickLabelPaint();
		Font tickLabelFont = plot.getRangeAxis().getTickLabelFont();
		Font labelFont = plot.getRangeAxis().getLabelFont();
		Dimension oldDim = comp.getSize();
		Dimension oldDimChartPanel = chartPanel.getSize();
		Stroke oldOutlineStroke = plot.getOutlineStroke();

		// New value
		Font newTickLabelFont = new Font(SaveGraphic.FAMILY, Font.PLAIN, 36);
		Font newLabelFont = new Font(SaveGraphic.FAMILY, Font.PLAIN, 48);
		Dimension newSize = new Dimension(1000, 750);

		// Conf
		comp.setSize(newSize);
		comp.setPreferredSize(newSize);
		chartPanel.setSize(newSize);
		chartPanel.setPreferredSize(newSize);
		comp.revalidate();
		chartPanel.revalidate();
		plot.setRangeCrosshairVisible(false);
		plot.setDomainCrosshairVisible(false);
		plot.setBackgroundPaint(Color.WHITE);
		chart.setBackgroundPaint(Color.WHITE);
		plot.getDomainAxis(1).setTickLabelPaint(Color.WHITE);
		plot.getRangeAxis(1).setTickLabelPaint(Color.WHITE);
		plot.getDomainAxis().setTickLabelFont(newTickLabelFont);
		plot.getRangeAxis().setTickLabelFont(newTickLabelFont);
		plot.getRangeAxis().setLabelFont(newLabelFont);
		plot.getDomainAxis().setLabelFont(newLabelFont);
		plot.setOutlineStroke(new BasicStroke(3));
		updateSeriesStrokes(true);
		SaveGraphic.changeTicks(plot, 20, 1.5F);

		MessageControl mc = view.getChartMouseCassisListener().getMessageControl();
		List<Point> oldPos = new ArrayList<>(mc.getImages().size());
		for (MessagePanel mp : mc.getImages()) {
			int fontSize =   mp.getModel().getFontSize() * 2;
			oldPos.add(mp.getLocation());
			mp.getModel().setFontSize(fontSize);
			mp.updatePopup();
			mp.getModel().setVisiblePersitent(true);
			mp.getModel().setAllowEdit(false);
		}
		for (RotationalTextAnnotation annotation : view.getControl().getModel().getBlockAnnotations()) {
			annotation.setSaveFont();
		}

		int imageNb = view.getChartMouseCassisListener().getMessageControl().getImages().size();
		if (imageNb == 0) {
			JFrame frame = new JFrame();
			JPanel panel = new JPanel();
			panel.add(comp);
			frame.setContentPane(panel);
			frame.setVisible(true);
			frame.pack();

			try {
				SaveGraphic.saveAsPDF(comp, file);
			} catch (Exception e) {
				LOGGER.error("Error while saving the pdf.", e);
			}

			frame.dispose();
		} else {
			int returnValue = JOptionPane.showConfirmDialog(view, comp,
					"Please place the popups correctly",   JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

			if (returnValue == JOptionPane.OK_OPTION) {
				try {
					SaveGraphic.saveAsPDF(comp, file);
				} catch (Exception e) {
					LOGGER.error("Error while saving the pdf.", e);
				}
			}
		}

		// Restore
		comp.setSize(oldDim);
		comp.setPreferredSize(oldDim);
		chartPanel.setSize(oldDimChartPanel);
		chartPanel.setPreferredSize(oldDimChartPanel);
		plot.setRangeCrosshairVisible(true);
		plot.setDomainCrosshairVisible(true);
		plot.setBackgroundPaint(bgInsideColor);
		chart.setBackgroundPaint(bgOutsideColor);
		plot.getDomainAxis(1).setTickLabelPaint(domainTickLabelPaint);
		plot.getRangeAxis(1).setTickLabelPaint(rangeTickLabelPaint);
		plot.getDomainAxis().setTickLabelFont(tickLabelFont);
		plot.getRangeAxis().setTickLabelFont(tickLabelFont);
		plot.getRangeAxis().setLabelFont(labelFont);
		plot.getDomainAxis().setLabelFont(labelFont);
		plot.setOutlineStroke(oldOutlineStroke);
		updateSeriesStrokes(false);
		SaveGraphic.changeTicks(plot, 8, 1);
		view.restorePanel(comp);
		int i = 0;
		for (MessagePanel mp : mc.getImages()) {
			int fontSize = mp.getModel().getFontSize() / 2;
			mp.getModel().setFontSize(fontSize);
			mp.updatePopup();
			mp.getModel().setVisiblePersitent(true);
			mp.getModel().setAllowEdit(false);
			mp.setLocation(oldPos.get(i));
			i++;
		}
		view.getChartMouseCassisListener().getMessageControl().setAllowNewImage(true);
		for (RotationalTextAnnotation annotation : view.getControl().getModel().getBlockAnnotations()) {
			annotation.setNormalFont();
		}
		return true;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.save.CassisSaveViewInterface#savePlotAsciiCassis(java.io.File)
	 */
	@Override
	public boolean savePlotAsciiCassis(File file) {
		try (BufferedWriter fileAscii = new BufferedWriter(new FileWriter(file))) {
			// Headers
			fileAscii.write("version=2");
			fileAscii.newLine();

			// Tmb
			boolean tmb = view.getControl().getModel().getCurrentResult()
					.getTypeTemperature() == TYPE_TEMPERATURE.TMB;
			fileAscii.write('#' + ReadNewNewFile.TMB_SAVE + '\t' + String.valueOf(tmb));
			fileAscii.newLine();

			// Beam
			for (RotationalDiagramMoleculeResult molRes : view.getControl().getModel().getCurrentResult().getResult()) {
				for (RotationalDiagramComponentResult compRes : molRes.getComponentResultsList()) {
					fileAscii.write('#' + ReadNewNewFile.BEAM_DILUTION_SAVE + '\t'
							+ molRes.getTag() + '\t' + compRes.getComponentNumber()
							+ '\t' + compRes.isBeamCorrection());
					fileAscii.newLine();
				}
			}

			fileAscii.write("id\tNumCompo\tSpecies\tQuantumNumbers\tFrequency"
					+ "\tEup\tGup\tAij\tFitFreq\tDeltaFitFreq\tVo\tdeltaVo"
					+ "\tFWHM_G\tdeltaFWHM_G\tFWHM_L\tdeltaFWHM_L\tIntensity"
					+ "\tdeltaIntensity\tFitFlux\tdeltaFitFlux\tFreq.IntensityMax"
					+ "\tV.IntensityMax\tFWHM\tIntensityMax\tFlux1stMom"
					+ "\tdeltaFlux1stMom\trms\tdeltaV\tCal\tSize"
					+ "\tTelescopePath\tTelescopeName");
			fileAscii.newLine();

			// Units
			fileAscii.append("None\tNone\tNone\tNone\tMHz\tK\tNone\ts-1\tMHz\tMHz\tkm/s"
					+ "\tkm/s\tkm/s\tkm/s\tkm/s\tkm/s\tK\tK\tK.km/s\tK.km/s\tMHz\tkm/s"
					+ "\tkm/s\tK\tK.km/s\tK.km/s\tmK\tkm/s\t%\tarcsec\tNone\tNone");
			fileAscii.newLine();

			// Data
			for (XYRotationalIntervalSeries s : view.getControl().getModel().getInfoToSave()) {
				for (PointInformation pi : s.getPoints()) {
					fileAscii.append(createDataStringLine(pi, s.getTag(), s.getNumComponent()));
					fileAscii.newLine();
				}
			}
			fileAscii.flush();
		} catch (IOException e) {
			LOGGER.error("Error while saving the file.", e);
			return false;
		}
		return true;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.save.CassisSaveViewInterface#savePlotAscii(java.io.File)
	 */
	@Override
	public boolean savePlotAscii(File file) {
		return false;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.save.CassisSaveViewInterface#savePlotVOTable(java.io.File)
	 */
	@Override
	public boolean savePlotVOTable(File file) {
		return false;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.save.CassisSaveViewInterface#savePlotSpec(java.io.File)
	 */
	@Override
	public boolean savePlotSpec(File file) {
		return false;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.save.CassisSaveViewInterface#saveForSamp(java.io.File)
	 */
	@Override
	public int saveForSamp(File file) {
		return SaveGraphic.ERROR;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.save.CassisSaveViewInterface#isSampSendPossible()
	 */
	@Override
	public boolean isSampSendPossible() {
		return false;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.save.CassisSaveViewInterface#savePlotFits(java.io.File)
	 */
	@Override
	public boolean savePlotFits(File file) {
		return false;
	}

	/**
	 * Configure, create the {@link BufferedImage} for the png save then restore
	 * the plot to it initial state.
	 *
	 * @return The image of the plot for the png save.
	 */
	private BufferedImage getImageForPngSave() {
		XYPlot plot = view.getPlot();
		JFreeChart chart = view.getChart();
		JComponent comp = view.getLayeredPane();

		// Backup
		Paint bgInsideColor = plot.getBackgroundPaint();
		Paint bgOutsideColor = chart.getBackgroundPaint();
		Paint domainTickLabelPaint = plot.getDomainAxis(1).getTickLabelPaint();
		Paint rangeTickLabelPaint = plot.getRangeAxis(1).getTickLabelPaint();

		// Conf
		plot.setRangeCrosshairVisible(false);
		plot.setDomainCrosshairVisible(false);
		plot.setBackgroundPaint(Color.WHITE);
		chart.setBackgroundPaint(Color.WHITE);
		plot.getDomainAxis(1).setTickLabelPaint(Color.WHITE);
		plot.getRangeAxis(1).setTickLabelPaint(Color.WHITE);

		// Create Image
		BufferedImage image = new BufferedImage(comp.getWidth(), comp.getHeight(), BufferedImage.TYPE_INT_RGB);
		Graphics graphics = image.createGraphics();
		comp.paint(graphics);
		graphics.dispose();

		// Restore
		plot.setRangeCrosshairVisible(true);
		plot.setDomainCrosshairVisible(true);
		plot.setBackgroundPaint(bgInsideColor);
		chart.setBackgroundPaint(bgOutsideColor);
		plot.getDomainAxis(1).setTickLabelPaint(domainTickLabelPaint);
		plot.getRangeAxis(1).setTickLabelPaint(rangeTickLabelPaint);

		return image;
	}

	/**
	 * Update the strokes of the series.
	 *
	 * @param setBigger true to set the stroke bigger, false to set it smaller.
	 */
	private void updateSeriesStrokes(boolean setBigger) {
		RotationalModel model = view.getControl().getModel();
		changeDots(model.getDataRenderer(), setBigger);
		changeCap(model.getErrorDataRenderer(), setBigger);
		updateDatasetStroke(model.getErrorDataSeries(), model.getErrorDataRenderer(), setBigger);
		updateDatasetStroke(model.getDataStepPoint(), model.getStepRenderer(), setBigger);
		updateDatasetStroke(model.getFitRectangleDataset(), model.getFitRectangleRenderer(), setBigger);
		updateDatasetStroke(model.getFitDataset(), model.getFitRenderer(), setBigger);
	}

	/**
	 * Update the Stroke of a dataset.
	 *
	 * @param collection The collection to update.
	 * @param renderer The renderer associated with the collection.
	 * @param setBigger true to set the stroke bigger, false to set it smaller.
	 */
	private void updateDatasetStroke(AbstractIntervalXYDataset collection, XYItemRenderer renderer, boolean setBigger) {
		int val = setBigger ? 3 : 1;
		for (int i = 0; i < collection.getSeriesCount(); i++) {
			Stroke stroke = renderer.getSeriesStroke(i);
			if (stroke instanceof BasicStroke) {
				BasicStroke nbs = new BasicStroke(val);
				renderer.setSeriesStroke(i, nbs);
			}
		}
	}

	/**
	 * Change the size of the dots.
	 *
	 * @param renderer The renderer for which we want to update the dots size.
	 * @param setBigger true to set the stroke bigger, false to set it smaller.
	 */
	public void changeDots(XYDotRenderer renderer, boolean setBigger) {
		final double coeff = setBigger ? 2 : 0.5;
		renderer.setDotHeight((int) (renderer.getDotHeight() * coeff));
		renderer.setDotWidth((int) (renderer.getDotWidth() * coeff));
	}

	/**
	 * Change the length of the caps.
	 *
	 * @param renderer The renderer for which we want to update the caps length.
	 * @param setBigger true to set the caps longer, false to set them shorter.
	 */
	public void changeCap(XYErrorRenderer renderer, boolean setBigger) {
		final double coeff = !setBigger ? 2 : 0.5;
		renderer.setCapLength(renderer.getCapLength() * coeff);
	}

	/**
	 * Create a formated String to append to the saving file with the data for a
	 *  {@link PointInformation}.
	 *
	 * @param point The PointInformation
	 * @param tag The tag of the point
	 * @param numcompo The component number of the point.
	 * @return a formated String for a PointInformation
	 */
	public String createDataStringLine(PointInformation point, int tag, int numcompo) {
		StringBuilder sb = new StringBuilder(460);
		char tab = '\t';
		sb.append(tag).append(tab);
		sb.append(numcompo).append(tab);

		String moleName;
		String qn;
		if (point.getMoleNameQuantique() == null) {
			moleName = AccessDataBase.getDataBaseConnection().getMolName(tag);
			if (DataBaseConnection.NOT_IN_DATABASE.equals(moleName)) {
				moleName = "???";
			}
			qn = "(???)";
		} else if (point.getMoleNameQuantique().contains("(")) {
			int index = point.getMoleNameQuantique().indexOf('(');
			moleName = point.getMoleNameQuantique().substring(0, index);
			qn = point.getMoleNameQuantique().substring(index);
		} else {
			moleName = point.getMoleNameQuantique();
			qn = "(???)";
		}

		sb.append(moleName).append(tab);
		sb.append(qn).append(tab);
		sb.append(point.getNu()).append(tab);
		sb.append(point.getEup()).append(tab);
		sb.append((int)point.getGup()).append(tab);
		sb.append(point.getAij()).append(tab);
		sb.append(point.getNuFit()).append(tab);
		sb.append(point.getDeltaNuFit()).append(tab);
		sb.append(point.getVelocity()).append(tab);
		sb.append(point.getDeltaVelocity()).append(tab);
		sb.append(point.getFwhmGaussianFit()).append(tab);
		sb.append(point.getDeltaFwhmGaussianFit()).append(tab);
		sb.append(point.getFwhmLorentzianFit()).append(tab);
		sb.append(point.getDeltaFwhmLorentzianFit()).append(tab);
		sb.append(point.getIntensityFit()).append(tab);
		sb.append(point.getDeltaIntensityFit()).append(tab);
		sb.append(point.getWFit()).append(tab);
		sb.append(point.getDeltaWFit()).append(tab);
		sb.append(point.getNuOfIntensityMax()).append(tab);
		sb.append(point.getVelocityOfIntensityMax()).append(tab);
		sb.append(point.getFwhmFirstMoment()).append(tab);
		sb.append(point.getIntensityMax()).append(tab);
		sb.append(point.getWFirstMoment()).append(tab);
		sb.append(point.getDeltaWFirstMoment()).append(tab);
		sb.append(point.getRms()).append(tab);
		sb.append(point.getDeltaV()).append(tab);
		sb.append(point.getCalibration()).append(tab);
		sb.append(point.getSizeSource()).append(tab);

		String pathFolderTelescope;
		String telescope;
		if (point.getTelescope() == null || point.getTelescope().endsWith("???")) {
			pathFolderTelescope = Software.getTelescopePath() + File.separatorChar;
			telescope = "???";
		} else if (!point.getTelescope().contains("/")) {
			pathFolderTelescope = Software.getTelescopePath() + File.separatorChar;
			telescope = point.getTelescope();
		} else {
			File tmpfile = new File(point.getTelescope());
			pathFolderTelescope = tmpfile.getParentFile().getAbsolutePath() + File.separatorChar;
			telescope = tmpfile.getName();
		}
		sb.append(pathFolderTelescope).append(tab);
		sb.append(telescope);

		return sb.toString();
	}

	@Override
	public BufferedImage getImageToPrint() {
		return getImageForPngSave();
	}

}
