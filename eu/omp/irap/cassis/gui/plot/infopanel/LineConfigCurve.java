/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.infopanel;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.TypeFrequency;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.gui.plot.curve.CurveCassisListener;
import eu.omp.irap.cassis.gui.plot.curve.CurveCassisMosaicListener;
import eu.omp.irap.cassis.gui.plot.curve.CurvePanelModel;
import eu.omp.irap.cassis.gui.plot.curve.CurvePanelMosaicControl;
import eu.omp.irap.cassis.gui.plot.curve.CurvePanelView;
import eu.omp.irap.cassis.gui.plot.curve.JPanelCurve;
import eu.omp.irap.cassis.gui.plot.curve.TypeCurve;
import eu.omp.irap.cassis.gui.plot.curve.XYErrorSeries;
import eu.omp.irap.cassis.gui.plot.curve.XYLineSeriesCassis;
import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassis;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import eu.omp.irap.cassis.gui.plot.curve.config.ConfigCurve;

public class LineConfigCurve {

	private Map<String, ConfigCurve> map;
	private final CurveCassisListener curveCassisListener;
	private final CurveCassisMosaicListener curveCassisMosaicListener;


	public LineConfigCurve(CurveCassisMosaicListener curveCassisMosaicListener,
			CurveCassisListener curveCassisListener) {
		this.curveCassisListener = curveCassisListener;
		this.curveCassisMosaicListener = curveCassisMosaicListener;
		map = new HashMap<>();
	}

	public boolean isThatCurveExist(String aName) {
		return map.containsKey(aName);
	}

	public void setElement(String nameCurve, ConfigCurve configCurve) {
		map.put(nameCurve, configCurve);
	}

	public void removeElement(String nameCurve) {
		map.remove(nameCurve);
	}

	public void clear() {
		map.clear();
	}

	public ConfigCurve getConfigCurve(String aName) {
		return map.get(aName);
	}

	public List<String> getListOfElement() {
		List<String> result = new ArrayList<>();
		for (Entry<String, ConfigCurve> entry : map.entrySet()) {
			result.add(entry.getKey());
		}
		return result;
	}

	public static Map<String, Boolean> getListOfKnownKey() {
		Map<String, Boolean> listOfKnownKey = new LinkedHashMap<>();
		// Line Spectrum
		listOfKnownKey.put(InfoPanelConstants.LINE_SPECTRUM_TITLE, false);
		listOfKnownKey.put(InfoPanelConstants.LTE_RADEX_TITLE, false);
		// Loomis Spectrum
		listOfKnownKey.put(InfoPanelConstants.LOOMIS_SPECTRUM_TITLE, false);
		listOfKnownKey.put(InfoPanelConstants.LOOMIS_LINES_TITLE, false);
		// Both
		listOfKnownKey.put(InfoPanelConstants.OTHER_SPECIES_TITLE, true);
		listOfKnownKey.put(InfoPanelConstants.FIT_CURVES_TITLE, true);
		listOfKnownKey.put(InfoPanelConstants.DATA_FILE_OVERLAYS_TITLE, true);
		listOfKnownKey.put(InfoPanelConstants.LINELIST_OVERLAYS_TITLE, true);
		listOfKnownKey.put(InfoPanelConstants.TOOLS_RESULTS_TITLE, true);
		listOfKnownKey.put(InfoPanelConstants.MARKERS_TITLE, true);
		return listOfKnownKey;
	}

	public Map<String, InfoMosaic> getListOfAllJPanelCurvePossible(List<InfoModel> listInfoModel) {
		Map<String, InfoMosaic> listOfJPanelCurveAndCurve = new HashMap<>();
		for (InfoModel infoModel : listInfoModel) {
			for (JPanelCurve jPanelCurve : infoModel.getListOfJPanelCurves()) {
				if (!listOfJPanelCurveAndCurve.containsKey(jPanelCurve.getName())) {
					listOfJPanelCurveAndCurve.put(jPanelCurve.getName(), new InfoMosaic());
				}
				for (CurvePanelView curvePanelView : jPanelCurve.getListCurvePanelView()) {
					if (!listOfJPanelCurveAndCurve.get(jPanelCurve.getName()).containsKey(curvePanelView.getNameFromModel())) {
						Double vlsr = Double.NaN;
						String name;
						TypeCurve typeCurve;
						name = curvePanelView.getNameFromModel();
						typeCurve = curvePanelView.getModel().getCassisModel().getTypeCurve();
						TypeFrequency typeFrequency = TypeFrequency.REST;
						if (!isThatCurveExist(name))
							setElement(name, curvePanelView.getModel().getCassisModel().getConfigCurve().copy());

						if (typeCurve == TypeCurve.DATA || typeCurve == TypeCurve.SYNTHETIC || typeCurve.isFit() || typeCurve == TypeCurve.OVERLAY_DATA || typeCurve == TypeCurve.RESULT) {
							final CommentedSpectrum spectrum = ((XYSpectrumSeries) curvePanelView.getModel().getCassisModel()).getSpectrum();
							vlsr = spectrum.getVlsrModel();
							typeFrequency = spectrum.getTypeFreq();
						} else if (TypeCurve.OTHER_SPECIES_IMAGE.equals(typeCurve)|| TypeCurve.OTHER_SPECIES_SIGNAL.equals(typeCurve)) {
							final List<LineDescription> listOfLines = ((XYLineSeriesCassis) curvePanelView.getModel().getCassisModel()).getListOfLines();
							if (listOfLines != null && !listOfLines.isEmpty())
								vlsr = listOfLines.get(0).getVlsr();
						}

						listOfJPanelCurveAndCurve.get(jPanelCurve.getName()).addElement(name, typeCurve, vlsr, typeFrequency);
					}

				}
			}
		}
		return listOfJPanelCurveAndCurve;
	}

	public InfoModel generateMosaicInfoModel(
			final List<InfoModel> listInfoModels,
			final boolean visiblePlotNumber,
			final Color plotNumberColor) {
		InfoModel infoModel = new InfoModel();

		Map<String, InfoMosaic> listOfJPanelCurveAndCurve =
				getListOfAllJPanelCurvePossible(listInfoModels);
		Map<String, Boolean> listOfKnownKey = getListOfKnownKey();
		for (Entry<String, Boolean> entry : listOfKnownKey.entrySet()) {
			String nameJPanelCurve = entry.getKey();
			boolean value = entry.getValue();
			if (listOfJPanelCurveAndCurve.containsKey(nameJPanelCurve)) {
				JPanelCurve panelCurve = new JPanelCurve(nameJPanelCurve, value);

				InfoMosaic infoMosaic = listOfJPanelCurveAndCurve.get(nameJPanelCurve);
				for (int i = 0; i < infoMosaic.getSize(); i++) {
					String name = infoMosaic.getName(i);
					TypeCurve typeCurve = infoMosaic.getTypeCurve(i);

					XYSeriesCassis serie = null;

					if (typeCurve == TypeCurve.DATA || typeCurve == TypeCurve.SYNTHETIC ||
							typeCurve.isFit() || typeCurve == TypeCurve.OVERLAY_DATA ||
							typeCurve == TypeCurve.RESULT) {
						CommentedSpectrum commentedSpectrum = new CommentedSpectrum();
						if (!Double.isNaN(infoMosaic.getVlsr(i)))
							commentedSpectrum.setVlsrModel(infoMosaic.getVlsr(i));
						commentedSpectrum.setTypeFreq(infoMosaic.getTypeFrequency(i));
						serie = new XYSpectrumSeries(name, XAxisCassis.getXAxisUnknown(), infoMosaic.getTypeCurve(i), commentedSpectrum);
					}
					else if (typeCurve == TypeCurve.LINE || typeCurve == TypeCurve.LINE_NEARBY ||
							typeCurve == TypeCurve.OTHER_SPECIES_IMAGE ||
							typeCurve == TypeCurve.OTHER_SPECIES_SIGNAL ||
							typeCurve == TypeCurve.OVERLAY_LINELIST ||
							typeCurve == TypeCurve.LOOMIS_LINE) {
						List<LineDescription> lines = new ArrayList<>();
						final LineDescription line = new LineDescription();
						if (!Double.isNaN(infoMosaic.getVlsr(i)))
							line.setVlsr(infoMosaic.getVlsr(i));
						lines.add(line);
						serie = new XYLineSeriesCassis(name, typeCurve, lines, Integer.MIN_VALUE, XAxisCassis.getXAxisUnknown());
					}
					else if (typeCurve == TypeCurve.LINE_ERROR) {
						serie = new XYErrorSeries(name, typeCurve, null, XAxisCassis.getXAxisUnknown());
					}
					else if (typeCurve == TypeCurve.MARKERS) {
						serie = new XYSpectrumSeries(name,
								XAxisCassis.getXAxisUnknown(),
								TypeCurve.MARKERS, new CommentedSpectrum());
					}

					CurvePanelView cpv = typeCurve == TypeCurve.MARKERS ?
							new CurvePanelView(new CurvePanelModel(serie), false, true, false)
							: new CurvePanelView(new CurvePanelModel(serie), true, true, false);
					cpv.getButtonColor().setBackground(getConfigCurve(name).getColor());
					cpv.getCheckBox().setSelected(getConfigCurve(name).isVisible());
					cpv.getModel().getCassisModel().setConfigCurve(getConfigCurve(name));
					((CurvePanelMosaicControl) cpv.getControl()).addCurveCassisListener(curveCassisListener);
					((CurvePanelMosaicControl) cpv.getControl()).addMosaicListener(curveCassisMosaicListener);
					cpv.getButtonColor().setEnabled(typeCurve != TypeCurve.MARKERS);
					panelCurve.addCurvePane(cpv);
				}
				infoModel.addPanelCurve(panelCurve);
				listOfJPanelCurveAndCurve.remove(nameJPanelCurve);
			}
 		}

		addPlotNumber(infoModel, visiblePlotNumber, plotNumberColor);
		return infoModel;
	}

	private void addPlotNumber(InfoModel infoModel, boolean visible, Color color) {
		JPanelCurve plotNumberPanelCurve = new JPanelCurve(InfoPanelConstants.PLOT_NUMBER_TITLE, false);
		CommentedSpectrum commentedSpectrum = new CommentedSpectrum();
		XYSeriesCassis serie = new XYSpectrumSeries(InfoPanelConstants.PLOT_NUMBER_TITLE,
				XAxisCassis.getXAxisUnknown(), TypeCurve.PLOT_NUMBER, commentedSpectrum);
		CurvePanelView cpv = new CurvePanelView(new CurvePanelModel(serie), false, true, false);
		cpv.getCheckBox().setSelected(visible);
		cpv.getModel().getCassisModel().getConfigCurve().setColor(color);
		cpv.getButtonColor().setBackground(color);
		((CurvePanelMosaicControl) cpv.getControl()).addCurveCassisListener(curveCassisListener);
		((CurvePanelMosaicControl) cpv.getControl()).addMosaicListener(curveCassisMosaicListener);
		plotNumberPanelCurve.addCurvePane(cpv);
		infoModel.addPanelCurve(plotNumberPanelCurve);
	}

	public void setAllVisible() {
		Iterator<String> it = map.keySet().iterator();
		while (it.hasNext()) {
			ConfigCurve valeur = map.get(it.next());
			valeur.setVisible(true);
		}
	}
}
