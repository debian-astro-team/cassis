/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.infopanel;

import java.util.ArrayList;
import java.util.List;

import eu.omp.irap.cassis.gui.plot.curve.JPanelCurve;

/**
 * Class: InfoModel Model of InfoPanel
 *
 * @author thachtn
 *
 */
public class InfoModel {

	/* List of PanelCurves. */
	private List<JPanelCurve> listOfJPanelCurves;

	public InfoModel() {
		listOfJPanelCurves = new ArrayList<>();
	}

	/**
	 * @return the listOfJPanelCurves
	 */
	public List<JPanelCurve> getListOfJPanelCurves() {
		return listOfJPanelCurves;
	}

	public void addPanelCurve(JPanelCurve panelCurve) {
		listOfJPanelCurves.add(panelCurve);
	}

	public void removePanelCurveByName(String name) {
		ArrayList<JPanelCurve> jPanelCurveToRemove = new ArrayList<>();
		for (JPanelCurve jpc : listOfJPanelCurves) {
			if (jpc.getName().equals(name)) {
				jPanelCurveToRemove.add(jpc);
			}
		}
		for (JPanelCurve jpc2 : jPanelCurveToRemove) {
			listOfJPanelCurves.remove(jpc2);
		}
	}
	/**
	 * Search the {@link JPanelCurve} with the given name then return it. If it
	 *  doesn't exist, create a new one with the given name and add it.
	 *
	 * @param name The name of the {@link JPanelCurve}.
	 * @return the JPanelCurve.
	 */
	public JPanelCurve getPanelCurveByName(String name) {
		for (JPanelCurve jpc : listOfJPanelCurves) {
			if (jpc.getName().equals(name)) {
				return jpc;
			}
		}

		final JPanelCurve jPanelCurve = new JPanelCurve(name);
		addPanelCurve(jPanelCurve);
		return jPanelCurve;
	}

	/**
	 * Return if the {@link JPanelCurve} with name given in parameter exist.
	 *
	 * @param name The name of the searched {@link JPanelCurve}.
	 * @return If the {@link JPanelCurve} with name exist.
	 */
	public boolean isJPanelCurveExist(String name) {
		for (JPanelCurve jpc : listOfJPanelCurves) {
			if (jpc.getName().equals(name)) {
				return true;
			}
		}
		return false;
	}
}
