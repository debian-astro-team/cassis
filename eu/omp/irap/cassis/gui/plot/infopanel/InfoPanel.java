/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.infopanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.JPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.gui.plot.curve.CurvePanelView;
import eu.omp.irap.cassis.gui.plot.curve.JPanelCurve;
import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassis;

@SuppressWarnings("serial")
public class InfoPanel extends JPanel {

	private static final Logger LOGGER = LoggerFactory.getLogger(InfoPanel.class);

	private InfoControl control;
	private InfoModel model;
	private Box box;


	public InfoPanel() {
		super();
		model = new InfoModel();
		this.control = new InfoControl(model, this);
		box = Box.createVerticalBox();
		add(box);
	}

	public void addJPanelCurve(final JPanelCurve panel) {
		addJPanelCurve(panel, true);
	}

	public void addJPanelCurve(final JPanelCurve jPanelCurve, final boolean haveDeleteButton) {
		add(jPanelCurve, haveDeleteButton);
		model.getListOfJPanelCurves().add(jPanelCurve);
	}

	public void setModel(InfoModel infoModel) {
		box.removeAll();
		model = infoModel;
		for (JPanelCurve jPanelCurve : model.getListOfJPanelCurves()) {
			add(jPanelCurve);
		}
		control.setModel(model);
	}

	private void add(final JPanelCurve jPanelCurve) {
		add(jPanelCurve, true);
	}

	private void add(final JPanelCurve jPanelCurve, final boolean haveDeleteButton) {
		final JPanel borderPanel = new JPanel();
		jPanelCurve.setSuperPanel(borderPanel);
		if (!haveDeleteButton)
			jPanelCurve.removeDeleteButton();
		else {
			if (jPanelCurve.getDeleteButton() != null) {
				jPanelCurve.getDeleteButton().addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						control.fireInfoPanelListener(jPanelCurve);
					}
				});
			}
		}

		box.add(jPanelCurve);
	}

	public InfoControl getControl() {
		return control;
	}

	public JPanelCurve removePanelInfo(XYSeriesCassis cassisModel) {
		JPanelCurve result = null;
		for (JPanelCurve panelCurve : model.getListOfJPanelCurves()) {
			if (panelCurve.getListCassisModel().contains(cassisModel)) {
				result = panelCurve;
				removePanelCurve(panelCurve);
				break;
			}
		}
		return result;
	}

	public JPanelCurve removePanelInfo(String title) {
		JPanelCurve result = null;
		for (JPanelCurve panelCurve : model.getListOfJPanelCurves()) {
			if (panelCurve.getListCassisModel().get(0).getKey().equals(title)) {
				result = panelCurve;
				removePanelCurve(panelCurve);
				break;
			}
		}
		return result;
	}

	public List<XYSeriesCassis> removeAllPanelCurve() {
		List<XYSeriesCassis> xySeriesCassis = new ArrayList<>();
		List<JPanelCurve> listPanelCurves = model.getListOfJPanelCurves();
		while (!listPanelCurves.isEmpty()) {
			JPanelCurve panelCurve = listPanelCurves.get(0);
			xySeriesCassis.addAll(panelCurve.getListCassisModel());
			removePanelCurve(panelCurve);
		}
		return xySeriesCassis;
	}

	public void removePanelCurve(JPanelCurve panelCurve) {
		try {
			List<CurvePanelView> listCurvePanelView = panelCurve.getListCurvePanelView();
			for (int cpt = 0; cpt < listCurvePanelView.size(); cpt++) {
				CurvePanelView cpv = listCurvePanelView.get(cpt);
				cpv.removeCurveCassisListener(
						control.getListeners().getListeners(InfoPanelListener.class)[0]);
				cpv.getModel().removeModelListener(cpv.getControl());
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			LOGGER.trace("Can not remove listener", e);
			// Do nothing, there is not listener.
		}
		model.getListOfJPanelCurves().remove(panelCurve);
		box.remove(panelCurve);
	}

	/**
	 * @return the model
	 */
	public final InfoModel getModel() {
		return model;
	}

	public JPanelCurve getJPanelCurveByName(String name) {
		for (JPanelCurve jpc : model.getListOfJPanelCurves()) {
			if (jpc.getName().equals(name)) {
				return jpc;
			}
		}
		return null;
	}

}
