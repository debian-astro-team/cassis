/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.infopanel;

/**
 * Simple class to store constants related to InfoPanel.
 *
 * @author M. Boiziot
 */
public class InfoPanelConstants {

	// JPanelCurve titles:
	public static final String LINE_SPECTRUM_TITLE = "Line Spectrum";
	public static final String LTE_RADEX_TITLE = "LTE + RADEX";
	public static final String LOOMIS_SPECTRUM_TITLE = "Loomis Spectrum";
	public static final String LOOMIS_LINES_TITLE = "Loomis Lines";
	public static final String OTHER_SPECIES_TITLE = "Other Species";
	public static final String FIT_CURVES_TITLE = "Fit Curves";
	public static final String DATA_FILE_OVERLAYS_TITLE = "Data file overlays";
	public static final String LINELIST_OVERLAYS_TITLE = "LineList overlays";
	public static final String TOOLS_RESULTS_TITLE = "Tools Results";
	public static final String MARKERS_TITLE = "Markers";
	public static final String PLOT_NUMBER_TITLE = "Plot number";

	// CurvePanel titles:
	public static final String SIGNAL_TITLE = "Signal";
	public static final String IMAGE_TITLE = "Image";
	public static final String FIT_TITLE = "Fit";
	public static final String FIT_RESIDUAL_TITLE = "Fit residual";
	public static final String LINE_TITLE = "Line";
	public static final String ERROR_TITLE = "Error";
	public static final String NEARBY_LINE_TITLE = "Nearby Line";
}
