/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.gallery;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.gui.plot.util.SORTING_PLOT;
import eu.omp.irap.cassis.gui.util.TextFieldFormatFilter;

/**
 * Class allowing to navigate in the gallery and to modify it.
 *
 * @author M. Boiziot
 */
@SuppressWarnings("serial")
public class GalleryNavigation extends JPanel implements ModelListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(GalleryNavigation.class);

	private GalleryHandler handler;
	private GalleryNavigationModel model;
	private JComboBox<SORTING_PLOT> sortingComboBox;
	private JComboBox<GalleryOption> optionCombobox;
	private JButton moveLeftButton;
	private JButton moveRightButton;
	private JTextField currentGalleryTextField;
	private JTextField listPlotTextField;
	private JLabel plotNumberLabel;
	private JLabel galleriesNumberLabel;
	private JTextField rowsTextField;
	private JTextField colsTextField;
	private String lastPlotListString;


	/**
	 * Constructor.
	 *
	 * @param handler The GalleryHandler.
	 * @param model The model.
	 */
	public GalleryNavigation(GalleryHandler handler, GalleryNavigationModel model) {
		this.handler = handler;
		this.model = model;
		this.model.addModelListener(this);
		this.setLayout(new BorderLayout());
		initComponents();
	}

	/**
	 * Place graphicals components.
	 */
	private void initComponents() {
		JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));

		// Left
		JPanel leftPanel = new JPanel(new GridLayout(2, 1));
		JPanel leftTopPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		leftTopPanel.add(new JLabel("Line sorting: "));
		leftTopPanel.add(getSortingComboBox());

		JPanel leftBottomPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		leftBottomPanel.add(new JLabel("Show by: "));
		leftBottomPanel.add(getOptionCombobox());

		leftPanel.add(leftTopPanel);
		leftPanel.add(leftBottomPanel);

		// Center
		JPanel centerPanel = new JPanel(new GridLayout(2, 1));
		JPanel centerBottomPanel = new JPanel();
		centerBottomPanel.add(getMoveLeftButton());
		centerBottomPanel.add(getCurrentGalleryTextField());
		centerBottomPanel.add(new JLabel("/"));
		centerBottomPanel.add(getGalleriesNumberLabel());
		centerBottomPanel.add(getMoveRightButton());

		centerPanel.add(new JLabel("      Gallery number: "));
		centerPanel.add(centerBottomPanel);

		// Right
		JPanel rightPanel = new JPanel(new GridLayout(2, 1));
		JPanel rightTopPanel = new JPanel();
		rightTopPanel.add(new JLabel("List of plots: "));
		rightTopPanel.add(getListPlotTextField());
		rightTopPanel.add(getPlotNumberLabel());

		JPanel rightBottomPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		rightBottomPanel.add(new JLabel("Rows x Cols: "));
		rightBottomPanel.add(getRowsTextField());
		rightBottomPanel.add(new JLabel(" x "));
		rightBottomPanel.add(getColsTextField());

		rightPanel.add(rightTopPanel);
		rightPanel.add(rightBottomPanel);

		panel.add(leftPanel);
		panel.add(centerPanel);
		panel.add(rightPanel);

		add(panel, BorderLayout.CENTER);
	}

	/**
	 * Create if needed then return the sorting JCombobox.
	 *
	 * @return the sorting combobox.
	 */
	public JComboBox<SORTING_PLOT> getSortingComboBox() {
		if (sortingComboBox == null) {
			sortingComboBox = new JComboBox<>(SORTING_PLOT.values());
			sortingComboBox.setEnabled(model.isSortingComboboxEnabled());
			sortingComboBox.setSelectedItem(
					handler.getStackMosaicPanel().getSortingComboBox().getSelectedItem());
			sortingComboBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					refreshSorting();
				}
			});
		}
		return sortingComboBox;
	}

	public void refreshSorting() {
		handler.getStackMosaicPanel().getSortingComboBox().setSelectedItem(
				getSortingComboBox().getSelectedItem());
	}

	/**
	 * Create if needed then return the move left button ([<]).
	 *
	 * @return the move left button ([<]).
	 */
	private JButton getMoveLeftButton() {
		if (moveLeftButton == null) {
			moveLeftButton = new JButton("<");
			moveLeftButton.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					int curGal = model.getCurrentGallery();
					int nbGal = model.getNumberOfGalleries();
					int newCurrentGallery =
							curGal > 1 ? (curGal > nbGal ? nbGal : curGal - 1) : nbGal;
					model.setCurrentGallery(newCurrentGallery);
					refresh();
				}
			});
		}
		return moveLeftButton;
	}

	/**
	 * Create if needed then return the move right button ([>]).
	 *
	 * @return the move right button ([>]).
	 */
	public JButton getMoveRightButton() {
		if (moveRightButton == null) {
			moveRightButton = new JButton(">");
			moveRightButton.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					int curGal = model.getCurrentGallery();
					int nbGal = model.getNumberOfGalleries();
					int newCurrentGallery =
							curGal < nbGal ? (curGal < 1 ? 1 : curGal + 1) : 1;
					model.setCurrentGallery(newCurrentGallery);
					refresh();
				}
			});
		}
		return moveRightButton;
	}


	/**
	 * Create if needed then return the option JComboBox.
	 *
	 * @return the option JComboBox.
	 */
	private JComboBox<GalleryOption> getOptionCombobox() {
		if (optionCombobox == null) {
			optionCombobox = new JComboBox<>(model.getOptionsAllowed());
			optionCombobox.setSelectedItem(model.getSelectedOption());
			optionCombobox.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					GalleryOption currentOption =
							(GalleryOption) optionCombobox.getSelectedItem();
					model.setSelectedOption(currentOption);

					refresh();
				}
			});
		}
		return optionCombobox;
	}

	/**
	 * Update the number of the current gallery in the model if needed.
	 *
	 * @return if the view value is correct.
	 */
	private boolean updateCurrentGalleryModel() {
		int currentGallery;
		try {
			currentGallery = Integer.parseInt(getCurrentGalleryTextField().getText());
		} catch (NumberFormatException nfe) {
			LOGGER.warn("Current gallery value must be an integer", nfe);
			return false;
		}
		if (currentGallery >= 1 && currentGallery <= model.getNumberOfGalleries()) {
			if (currentGallery != model.getCurrentGallery()) {
					model.setCurrentGallery(currentGallery);
			}
			return true;
		}
		return false;
	}

	/**
	 * Create if needed then return the current gallery JTextField.
	 *
	 * @return the current gallery JTextField.
	 */
	public JTextField getCurrentGalleryTextField() {
		if (currentGalleryTextField == null) {
			currentGalleryTextField = new JTextField(
					String.valueOf(model.getCurrentGallery()), 2);
			final TextFieldFormatFilter galleryFilter = new TextFieldFormatFilter(
					TextFieldFormatFilter.INTEGER, "1", TextFieldFormatFilter.POSITIVE);
			currentGalleryTextField.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					galleryFilter.keyPressed(e);
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						if (!updateCurrentGalleryModel()) {
							currentGalleryTextField.setText(
									String.valueOf(model.getCurrentGallery()));
						}
						refresh();
					}
				}
			});
			currentGalleryTextField.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					if (!updateCurrentGalleryModel()) {
						currentGalleryTextField.setText(
								String.valueOf(model.getCurrentGallery()));
					}
				}
			});
		}
		return currentGalleryTextField;
	}

	/**
	 * Display error message and restore the Filter from the model in the view.
	 */
	private void handleListPlotBadInput() {
		JOptionPane.showMessageDialog(this,
				"The format of the provided list is wrong, correct formats are : 1,3,8,… or 1-3,5-9,...\n" +
				"Back to the previous list.",
				"Error",
				JOptionPane.ERROR_MESSAGE);
		String val = GalleryNavigationUtils.getStringOfSpectrumList(model.getFilter());
		getListPlotTextField().setText(val);
		lastPlotListString = val;
	}

	/**
	 * Update the list of plot in the model if the view value is OK.
	 */
	private void updateListPlotModel() {
		try {
			List<Integer> filterList =
					GalleryNavigationUtils.getListOfSpectrums(
							getListPlotTextField().getText(),
							model.getPlotNumber());
			if (filterList.isEmpty()) {
				handleListPlotBadInput();
			} else {
				model.setFilter(filterList, true);
			}
		} catch (IllegalArgumentException iae) {
			LOGGER.error("The format of the provided list is wrong", iae);
			handleListPlotBadInput();
		}
	}

	/**
	 * Create if needed then return the list of plots JTextField.
	 *
	 * @return the list of plots JTextField.
	 */
	public JTextField getListPlotTextField() {
		if (listPlotTextField == null) {
			int plotNumber = model.getPlotNumber();
			String text = (plotNumber > 1) ? "1-" + plotNumber : "1";
			listPlotTextField = new JTextField(text, 4);
			listPlotTextField.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER &&
							listPlotTextField.getText().trim().length() > 0) {
						lastPlotListString = listPlotTextField.getText().trim();
						updateListPlotModel();
						handler.getStackMosaicPanel().getModel().setFilterText(listPlotTextField.getText());
						handler.getStackMosaicPanel().getModel().setFilter(model.getFilter());
					}
				}
			});
			listPlotTextField.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					if (!listPlotTextField.getText().trim().equals(lastPlotListString)) {
						updateListPlotModel();
						handler.getStackMosaicPanel().getModel().setFilterText(listPlotTextField.getText());
						handler.getStackMosaicPanel().getModel().setFilter(model.getFilter());
					}
				}
			});
		}
		return listPlotTextField;
	}

	/**
	 * Update the text in the plot number label.
	 *
	 * @param number The number of plots.
	 */
	private void updatePlotNumberLabel(int number) {
		String plot = (number > 1) ? " plots)" : " plot)";
		getPlotNumberLabel().setText("(" + number + plot);
	}

	/**
	 * Create if needed then return the plot number JLabel.
	 *
	 * @return the plot number label.
	 */
	private JLabel getPlotNumberLabel() {
		if (plotNumberLabel == null) {
			plotNumberLabel = new JLabel();
			updatePlotNumberLabel(model.getPlotNumber());
		}
		return plotNumberLabel;
	}

	/**
	 * Create if needed then return the Galleries number JLabel.
	 *
	 * @return gallery number JLabel.
	 */
	private JLabel getGalleriesNumberLabel() {
		if (galleriesNumberLabel == null) {
			galleriesNumberLabel = new JLabel(
					String.valueOf(model.getNumberOfGalleries()));
		}
		return galleriesNumberLabel;
	}

	/**
	 * Update the rows model if the view value is OK.
	 *
	 * @return if the view value is OK.
	 */
	private boolean updateRowsModel() {
		int value = 0;
		try {
			value = Integer.parseInt(getRowsTextField().getText());
		} catch (NumberFormatException nfe) {
			LOGGER.error("The row number must be an integer", nfe);
			return false;
		}
		if (value >= 1) {
			if (model.getRows() != value) {
				model.setRows(value);
			}
			return true;
		}
		return false;
	}

	/**
	 * Create if needed then return the rows JTextField.
	 *
	 * @return the rows JTextField.
	 */
	public JTextField getRowsTextField() {
		if (rowsTextField == null) {
			rowsTextField = new JTextField(String.valueOf(model.getRows()), 3);
			final TextFieldFormatFilter rowsFilter = new TextFieldFormatFilter(
					TextFieldFormatFilter.INTEGER,
					String.valueOf(model.getRows()),
					TextFieldFormatFilter.POSITIVE);

			rowsTextField.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					rowsFilter.keyReleased(e);
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						if (!updateRowsModel()) {
							rowsTextField.setText(String.valueOf(model.getRows()));
						}
						refresh();
					}
				}
			});

			rowsTextField.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					if (!updateRowsModel()) {
						rowsTextField.setText(String.valueOf(model.getRows()));
					}
				}
			});
		}
		return rowsTextField;
	}

	/**
	 * Update the cols model if the view value is OK.
	 *
	 * @return if the view value is OK.
	 */
	private boolean updateColsModel() {
		int value = 0;
		try {
			value = Integer.parseInt(getColsTextField().getText());
		} catch (NumberFormatException nfe) {
			LOGGER.error("The column number must an integer", nfe);
			return false;
		}
		if (value >= 1) {
			if (model.getCols() != value) {
				model.setCols(value);
			}
			return true;
		}
		return false;
	}

	/**
	 * Create if needed then return the cols JTextField.
	 *
	 * @return the cols JTextField.
	 */
	public JTextField getColsTextField() {
		if (colsTextField == null) {
			colsTextField = new JTextField(String.valueOf(model.getCols()), 3);
			final TextFieldFormatFilter colsFilter = new TextFieldFormatFilter(
					TextFieldFormatFilter.INTEGER,
					String.valueOf(model.getCols()),
					TextFieldFormatFilter.POSITIVE);

			colsTextField.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					colsFilter.keyReleased(e);
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						if (!updateColsModel()) {
							colsTextField.setText(String.valueOf(model.getCols()));
						}
						refresh();
					}
				}
			});
			colsTextField.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					if (!updateColsModel()) {
						colsTextField.setText(String.valueOf(model.getCols()));
					}
				}
			});
		}
		return colsTextField;
	}

	/**
	 * Refresh the gallery.
	 */
	private void refresh() {
		handler.showBy((GalleryOption) getOptionCombobox().getSelectedItem(),
				model.getFilter(), model.getCurrentGallery() - 1);
	}

	/**
	 * @see eu.omp.irap.cassis.common.events.ModelListener#dataChanged(eu.omp.irap.cassis.common.events.ModelChangedEvent)
	 */
	@Override
	public void dataChanged(ModelChangedEvent event) {
		String eventName = (String) event.getSource();

		if (GalleryNavigationModel.CURRENT_GALLERY_UPDATE_EVENT.equals(eventName)) {
			getCurrentGalleryTextField().setText(String.valueOf(model.getCurrentGallery()));
		} else if (GalleryNavigationModel.GALLERY_NUMBER_UPDATE_EVENT.equals(eventName)) {
			getGalleriesNumberLabel().setText(String.valueOf(model.getNumberOfGalleries()));
		} else if (GalleryNavigationModel.ROWS_UPDATE_EVENT.equals(eventName)) {
			getRowsTextField().setText(String.valueOf(model.getRows()));
		} else if (GalleryNavigationModel.COLS_UPDATE_EVENT.equals(eventName)) {
			getColsTextField().setText(String.valueOf(model.getCols()));
		} else if (GalleryNavigationModel.PLOT_NUMBER_EVENT.equals(eventName)) {
			updatePlotNumberLabel(model.getPlotNumber());
		} else if (GalleryNavigationModel.FILTER_UPDATE_EVENT.equals(eventName)) {
			String val = GalleryNavigationUtils.getStringOfSpectrumList(model.getFilter());
			getListPlotTextField().setText(val);
			lastPlotListString = val;
		} else if (GalleryNavigationModel.SELECTED_GALLERY_OPTION_EVENT.equals(eventName)) {
			if ((GalleryOption)getOptionCombobox().getSelectedItem() != model.getSelectedOption()) {
				getOptionCombobox().setSelectedItem(model.getSelectedOption());
			}
		} else if (GalleryNavigationModel.TRIGGER_REFRESH_EVENT.equals(eventName)) {
			refresh();
		}
	}

}
