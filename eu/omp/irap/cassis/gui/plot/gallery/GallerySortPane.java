/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.gallery;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.gui.plot.curve.Rendering;
import eu.omp.irap.cassis.gui.plot.curve.XYLineSeriesCassis;
import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassisCollection;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import eu.omp.irap.cassis.gui.plot.line.CassisPlot;
import eu.omp.irap.cassis.gui.plot.simple.ChangeRenderingInterface;
import eu.omp.irap.cassis.gui.plot.simple.SpectrumPlot;
import eu.omp.irap.cassis.gui.plot.util.InterValMarkerCassis;

@SuppressWarnings("serial")
public abstract class GallerySortPane extends GalleryPane {

	private static final Logger LOGGER = LoggerFactory.getLogger(GallerySortPane.class);

	protected ArrayList<XYSeriesCassisCollection>[] collections;
	protected ArrayList<Integer> numPlots;

	public static final int CENTER_INDEX = 0;
	public static final int TOP_LINE_INDEX = 1;
	public static final int TOP_LINE_ERROR_INDEX = 2;
	public static final int BOTTOM_SIGNAL_INDEX = 3;
	public static final int BOTTOM_IMAGE_INDEX = 4;

	protected int glOffset;
	protected String filename;
	protected int rows;
	protected int cols;
	protected List<CassisPlot> cassisPlots;
	private int numberOfSpectrums;
	protected boolean plotNumberVisible;
	protected Color plotNumberColor;
	protected boolean yAxisLog;
	protected boolean xAxisLog;
	protected Rendering rendering;
	protected ChangeRenderingInterface crInterface;
	protected List<ArrayList<InterValMarkerCassis>> markerListByPlot;


	public GallerySortPane(int rows, int cols, List<CassisPlot> cassisPlots,
			List<Integer> filter, int glOffset, boolean plotNumberVisible,
			Color plotNumberColor, boolean yAxisLog, boolean xAxisLog, Rendering rendering,
			ChangeRenderingInterface crInterface,
			List<ArrayList<InterValMarkerCassis>> markerListByPlot) {
		setNumberOfRowsCols(rows, cols);
		this.cassisPlots = cassisPlots;
		this.filter = filter;
		this.glOffset = glOffset;
		this.plotNumberVisible = plotNumberVisible;
		this.plotNumberColor = plotNumberColor;
		this.yAxisLog = yAxisLog;
		this.xAxisLog = xAxisLog;
		this.rendering = rendering;
		this.crInterface = crInterface;
		this.markerListByPlot = markerListByPlot;
		initComponents();
	}

	private void initFilter() {
		filter = new ArrayList<>(getNumberOfSpectrums());
		for (int i = 0; i < getNumberOfSpectrums(); i++) {
			filter.add(i);
		}
	}

	private void initComponents() {
		Dimension size = new Dimension(WIDTH, HEIGHT);
		setSize(size);
		setPreferredSize(size);
		setLayout(new BorderLayout());
		try {
			createGallery();
		} catch (CloneNotSupportedException | IOException e) {
			LOGGER.error("An error occured while trying to create the gallery", e);
		}
	}

	protected void createGallery() throws CloneNotSupportedException, IOException {
		if (cassisPlots != null) {
			collections = extractCharts(cassisPlots);
		}
		if (filter == null) {
			initFilter();
		}
	}

	public void setNumberOfRowsCols(int rows, int cols) {
		this.rows = rows;
		this.cols = cols;
	}

	protected ArrayList<XYSeriesCassisCollection>[] extractCharts(List<CassisPlot> cassisPlots) {
		@SuppressWarnings("unchecked")
		ArrayList<XYSeriesCassisCollection>[] tmpCollections = new ArrayList[5];
		numberOfSpectrums = cassisPlots.size();
		tmpCollections[TOP_LINE_INDEX] = new ArrayList<>(numberOfSpectrums);
		tmpCollections[TOP_LINE_ERROR_INDEX] = new ArrayList<>(numberOfSpectrums);
		tmpCollections[CENTER_INDEX] = new ArrayList<>(numberOfSpectrums);
		tmpCollections[BOTTOM_SIGNAL_INDEX] = new ArrayList<>(numberOfSpectrums);
		tmpCollections[BOTTOM_IMAGE_INDEX] = new ArrayList<>(numberOfSpectrums);
		numPlots = new ArrayList<>(numberOfSpectrums);

		for (int cpt = 0; cpt < numberOfSpectrums; cpt++) {
			CassisPlot cassisPlot = cassisPlots.get(cpt);
			tmpCollections[CENTER_INDEX].add(getCenterDataset(cassisPlot));

			tmpCollections[TOP_LINE_INDEX].add(getTopLineDataset(cassisPlot));
			tmpCollections[TOP_LINE_ERROR_INDEX].add(
					getTopLineErrorDataset(cassisPlot));
			tmpCollections[BOTTOM_SIGNAL_INDEX].add(
					getOtherSpeciesSignalDataset(cassisPlot));
			tmpCollections[BOTTOM_IMAGE_INDEX].add(
					getOtherSpeciesImageDataset(cassisPlot));

			numPlots.add(cpt+1);
		}
		return tmpCollections;
	}

	/**
	 * Create then return the center dataset for the given {@link CassisPlot}.
	 *
	 * @param cassisPlot The CassisPlot.
	 * @return the center dataset for the given CassisPlot.
	 */
	private XYSeriesCassisCollection getCenterDataset(CassisPlot cassisPlot) {
		XYSpectrumSeries fileSpectrumSeries = cassisPlot.getFileSpectrumSeries();
		XYSpectrumSeries theoreticalSpectrumSeries = cassisPlot.getSyntheticSeries();

		XYSeriesCassisCollection series = new XYSeriesCassisCollection(fileSpectrumSeries);
		series.setRendering(rendering);
		if (theoreticalSpectrumSeries != null) {
			series.addSeries(theoreticalSpectrumSeries);
		}
		XYSeriesCassisCollection loomisLine = cassisPlot.getLoomisLine();
		if (loomisLine != null) {
			for (int i = 0; i < loomisLine.getSeriesCount(); i++) {
				series.addSeries(loomisLine.getSeries(i));
			}
		}

		XYSeriesCassisCollection fitSeries = cassisPlot.getFitDataset();
		if (fitSeries != null) {
			for (int i = 0; i < fitSeries.getSeriesCount(); i++) {
				series.addSeries(fitSeries.getSeries(i));
			}
		}

		XYSeriesCassisCollection overlayDataSeries = cassisPlot.getOverlaySpectrumSeries();
		if (overlayDataSeries != null) {
			for (int i = 0; i < overlayDataSeries.getSeriesCount(); i++) {
				series.addSeries(overlayDataSeries.getSeries(i));
			}
		}

		XYSeriesCassisCollection resultSeries = cassisPlot.getResultSeries();
		if (resultSeries != null) {
			for (int i = 0; i < resultSeries.getSeriesCount(); i++) {
				series.addSeries(resultSeries.getSeries(i));
			}
		}
		return series;
	}

	/**
	 * Create then return the top line dataset for the given {@link CassisPlot}.
	 *
	 * @param cassisPlot The CassisPlot.
	 * @return the top line dataset for the given CassisPlot.
	 */
	private XYSeriesCassisCollection getTopLineDataset(CassisPlot cassisPlot) {
		XYSeriesCassisCollection topSeriesCollection = new XYSeriesCassisCollection();
		topSeriesCollection.setRendering(rendering);
		if (cassisPlot.getFileLineSeriesCassis() != null)
			topSeriesCollection.addSeries(cassisPlot.getFileLineSeriesCassis());
		if (cassisPlot.getSurroundSeries() != null)
			topSeriesCollection.addSeries(cassisPlot.getSurroundSeries());
		XYSeriesCassisCollection overlayLineSeries = cassisPlot.getOverlayLineSpectrumDataset();
		if (overlayLineSeries != null) {
			for (int i = 0; i < overlayLineSeries.getSeriesCount(); i++) {
				topSeriesCollection.addSeries(overlayLineSeries.getSeries(i));
			}
		}
		return topSeriesCollection;
	}

	/**
	 * Create then return the other species image dataset for the given {@link CassisPlot}.
	 *
	 * @param cassisPlot The CassisPlot.
	 * @return the other species image dataset for the given CassisPlot.
	 */
	private XYSeriesCassisCollection getOtherSpeciesImageDataset(CassisPlot cassisPlot) {
		XYSeriesCassisCollection bottomImageSeriesCollection = new XYSeriesCassisCollection();
		bottomImageSeriesCollection.setRendering(rendering);
		if (cassisPlot.getOtherSpeciesSeriesImage() != null) {
			bottomImageSeriesCollection.addSeries(cassisPlot.getOtherSpeciesSeriesImage());
		}
		return bottomImageSeriesCollection;
	}

	/**
	 * Create then return the top line error dataset for the given {@link CassisPlot}.
	 *
	 * @param cassisPlot The CassisPlot.
	 * @return the top line dataset error for the given CassisPlot.
	 */
	private XYSeriesCassisCollection getTopLineErrorDataset(CassisPlot cassisPlot) {
		XYSeriesCassisCollection topErrorSeriesCollection = new XYSeriesCassisCollection();
		topErrorSeriesCollection.setRendering(rendering);
		if (cassisPlot.getErrorFileSeries() != null)
			topErrorSeriesCollection.addSeries(cassisPlot.getErrorFileSeries());
		return topErrorSeriesCollection;
	}

	/**
	 * Create then return the other species signal dataset for the given {@link CassisPlot}.
	 *
	 * @param cassisPlot The CassisPlot.
	 * @return the other species signal dataset for the given CassisPlot.
	 */
	private XYSeriesCassisCollection getOtherSpeciesSignalDataset(CassisPlot cassisPlot) {
		XYSeriesCassisCollection bottomSignalSeriesCollection = new XYSeriesCassisCollection();
		bottomSignalSeriesCollection.setRendering(rendering);
		if (cassisPlot.getOtherSpeciesSeriesSignal() != null) {
			bottomSignalSeriesCollection.addSeries(cassisPlot.getOtherSpeciesSeriesSignal());
		}
		return bottomSignalSeriesCollection;
	}

	public int getNumberOfSpectrums() {
		return numberOfSpectrums;
	}

	public JPanel getGallery() {
		return this;
	}

	public static GallerySortPane getGallerySortPane(GalleryOption type, int rows, int cols,
			List<CassisPlot> cassisPlots, List<Integer> filter, int currentGallery,
			boolean plotNumberVisible, Color plotNumberColor, boolean yAxisLog,
			boolean xAxisLog, Rendering rendering, ChangeRenderingInterface crInterface,
			List<ArrayList<InterValMarkerCassis>> markerListByPlot) {
		GallerySortPane gallerySortPane = null;
		if (GalleryOption.NONE.equals(type)) {
			gallerySortPane = new GalleryNonePane(rows, cols, cassisPlots, filter,
					currentGallery, plotNumberVisible, plotNumberColor, yAxisLog,
					xAxisLog, rendering, crInterface, markerListByPlot);
		}
		else if (GalleryOption.ROW.equals(type)) {
			gallerySortPane = new GalleryRowsPane(rows, cols, cassisPlots, filter,
					currentGallery, plotNumberVisible, plotNumberColor, yAxisLog,
					xAxisLog, rendering, crInterface, markerListByPlot);
		}
		else if (GalleryOption.COLUMN.equals(type)) {
			gallerySortPane = new GalleryColumnsPane(rows, cols, cassisPlots, filter,
					currentGallery, plotNumberVisible, plotNumberColor, yAxisLog,
					xAxisLog, rendering, crInterface, markerListByPlot);
		}
		else if (GalleryOption.BOTH.equals(type)) {
			gallerySortPane = new GalleryBothPane(rows, cols, cassisPlots, filter,
					currentGallery, plotNumberVisible, plotNumberColor, yAxisLog,
					xAxisLog, rendering, crInterface, markerListByPlot);
		}
		return gallerySortPane;
	}

	/**
	 * Do specific operation for each gallery type needed for the save.
	 *
	 * @param saveMode if we try to save (true) or restore (false)
	 */
	public void setSave(boolean saveMode) {
	}

	/**
	 * Remove all series for each SpectrumPlot. Needed as it avoid a memory leak.
	 */
	public void clean() {
		for (SpectrumPlot sp : listSpectrumPlots) {
			sp.removeAllSeries();
		}
	}

	/**
	 * Remove other species for given plot number.
	 *
	 * @param plotNumber The plot number.
	 */
	public void removeOtherSpecies(int plotNumber) {
		if (collections[BOTTOM_SIGNAL_INDEX].size() > plotNumber) {
			collections[BOTTOM_SIGNAL_INDEX].get(plotNumber).removeAllSeries();
		}
		if (collections[BOTTOM_IMAGE_INDEX].size() > plotNumber) {
			collections[BOTTOM_IMAGE_INDEX].get(plotNumber).removeAllSeries();
		}
	}

	/**
	 * Add other species signal lines.
	 *
	 * @param plotNumber The number of the plot to add the lines.
	 * @param series The series.
	 */
	public void addOtherSpeciesSignal(int plotNumber, XYLineSeriesCassis series) {
		if (collections[BOTTOM_SIGNAL_INDEX].size() > plotNumber) {
			collections[BOTTOM_SIGNAL_INDEX].get(plotNumber).addSeries(series);
		}
	}

	/**
	 * Add other species image lines.
	 *
	 * @param plotNumber The number of the plot to add the lines.
	 * @param series The series.
	 */
	public void addOtherSpeciesImage(int plotNumber, XYLineSeriesCassis series) {
		if (collections[BOTTOM_IMAGE_INDEX].size() > plotNumber) {
			collections[BOTTOM_IMAGE_INDEX].get(plotNumber).addSeries(series);
		}
	}

	/**
	 * Hide (remove the markers).
	 */
	public void hideMarkers() {
		for (SpectrumPlot sp : listSpectrumPlots) {
			sp.getPlot().clearDomainMarkers();
		}
	}

}
