/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.gallery;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import org.jfree.chart.annotations.XYTextAnnotation;
import org.jfree.chart.plot.XYPlot;

import eu.omp.irap.cassis.gui.plot.simple.SpectrumPlot;
import eu.omp.irap.cassis.gui.plot.simple.util.XYPlotCassisUtil;

@SuppressWarnings("serial")
public abstract class GalleryPane extends JPanel {

	static final int WIDTH = 1024;
	static final int HEIGHT = 700;
	protected List<SpectrumPlot> listSpectrumPlots;
	private int[] heights = new int[] { 21, 15, 10, 8, 6, 5, 4 };
	protected List<Integer> filter;
	private static final Font annotationFont = new Font("Monospace", Font.PLAIN, 20);


	public GalleryPane() {
		listSpectrumPlots = new ArrayList<>();
	}

	public List<SpectrumPlot> getListSpectrumPlots() {
		return listSpectrumPlots;
	}

	/**
	 *
	 * @param rows : must be between 1 and ...
	 * @return
	 */
	int getPlotHeight(int rows) {
		int i = (rows - 1 >= 6) ? 6 : rows - 1;
		return heights[i];
	}

	public void setDefaultSettingsForGallery(SpectrumPlot spectrumPlot) {
		spectrumPlot.setRangeAxisLabelVisible(true);
		spectrumPlot.getChartPanel().setMouseWheelEnabled(false);
		spectrumPlot.getChartPanel().setMouseZoomable(false);
	}

	public void refreshPlotPaneFromConfigCurve() {
		for (SpectrumPlot spectrumPlot : listSpectrumPlots) {
			XYPlotCassisUtil.configureRenderer(spectrumPlot);
		}
	}

	public List<Integer> getFilter() {
		return filter;
	}

	/**
	 * Add an annotation with the plot number to the given plot.
	 *
	 * @param plot The plot.
	 * @param color The color of the annotation.
	 * @param number The number.
	 */
	public void addNumberToPlot(XYPlot plot, Color color, int number) {
		if (plot == null) {
			return;
		}
		String stringNumber = String.valueOf(number);
		XYTextAnnotation annotation = new PlotNumberAnnotation(stringNumber);
		annotation.setFont(annotationFont);
		annotation.setPaint(color);
		plot.addAnnotation(annotation);
	}
}
