/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.gallery;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Paint;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.axis.AxisSpace;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.gui.plot.curve.Rendering;
import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassisCollection;
import eu.omp.irap.cassis.gui.plot.simple.ChangeRenderingInterface;
import eu.omp.irap.cassis.gui.plot.simple.SpectrumPlot;
import eu.omp.irap.cassis.gui.plot.simple.util.ChartUtilities;
import eu.omp.irap.cassis.gui.plot.simple.util.XYPlotCassisUtil;
import eu.omp.irap.cassis.gui.plot.simple.util.ChartUtilities.POSITION;
import eu.omp.irap.cassis.gui.plot.util.InterValMarkerCassis;

@SuppressWarnings("serial")
public class GalleryColumnPane extends GalleryPane {

	private static final Logger LOGGER = LoggerFactory.getLogger(GalleryColumnPane.class);

	private int rows;
	private ArrayList<XYSeriesCassisCollection>[] collection;

	private ValueAxis colRef;
	private int offset;
	private int cols;
	private int colId;
	private boolean flag = true;
	private List<Integer> numPlots;
	private boolean plotNumberVisible;
	private Color plotNumberColor;
	private SpectrumPlot axisPane;
	private JPanel gallery;
	private JPanel p;
	private JPanel galleryAxis;
	private Color oldColor;
	private Paint oldColorAxisPane;
	private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("0.##");
	private boolean yAxisLog;
	private boolean xAxisLog;
	private Rendering rendering;
	private ChangeRenderingInterface crInterface;
	private List<ArrayList<InterValMarkerCassis>> markerListByPlot;


	public GalleryColumnPane(int rows, int offset, int cols, int colId,
			ArrayList<XYSeriesCassisCollection>[] collection,
			List<Integer> filter, List<Integer> numPlots,
			boolean plotNumberVisible, Color plotNumberColor, boolean yAxisLog,
			boolean xAxisLog, Rendering rendering, ChangeRenderingInterface crInterface,
			List<ArrayList<InterValMarkerCassis>> markerListByPlot) {
		super();
		setNumberOfRows(rows);
		this.offset = offset;
		this.cols = cols;
		this.collection = collection;
		this.filter = filter;
		this.colId = colId;
		this.numPlots = numPlots;
		this.plotNumberVisible = plotNumberVisible;
		this.plotNumberColor = plotNumberColor;
		this.yAxisLog = yAxisLog;
		this.xAxisLog = xAxisLog;
		this.rendering = rendering;
		this.markerListByPlot = markerListByPlot;
		this.crInterface = crInterface;
		initComponents();
	}

	private void initComponents() {
		Dimension size = new Dimension(WIDTH, HEIGHT);
		setSize(size);
		setPreferredSize(size);
		setLayout(new BorderLayout());
		try {
			createGallery(colId);
		} catch (CloneNotSupportedException e) {
			LOGGER.error("An error occured while trying to create the gallery", e);
		}
	}

	public void setNumberOfRows(int rows) {
		this.rows = rows;
	}

	private void createGallery(int j) throws CloneNotSupportedException {
		gallery = new JPanel(new GridLayout(rows, 0));
		int count = offset;
		colRef = null;
		List<SpectrumPlot> arr = new ArrayList<>();
		int rr = 0;
		for (int i = 0; i < rows; i++) {
			if (count >= filter.size()) {
				flag = false;
				return;
			}

			SpectrumPlot spectrumPlot = new SpectrumPlot(
					collection[GallerySortPane.CENTER_INDEX].get(filter.get(count)),
					collection[GallerySortPane.TOP_LINE_INDEX].get(filter.get(count)),
					collection[GallerySortPane.TOP_LINE_ERROR_INDEX].get(filter.get(count)),
					collection[GallerySortPane.BOTTOM_SIGNAL_INDEX].get(filter.get(count)),
					collection[GallerySortPane.BOTTOM_IMAGE_INDEX].get(filter.get(count)),
					new int[] { 60, 2, 1, i == rows - 1 || count + cols >= filter.size() ? 1 : 0 },
					true);
			spectrumPlot.updateRendering(rendering);
			spectrumPlot.setExternalChangeRenderingInterface(crInterface);
			spectrumPlot.addMarkers(markerListByPlot.get(filter.get(count)), false);

			if (yAxisLog) {
				spectrumPlot.setYAxisToLog();
			}
			if (xAxisLog) {
				spectrumPlot.setXAxisToLog();
			}
			if (plotNumberVisible) {
				addNumberToPlot(spectrumPlot.getPlot(), plotNumberColor, numPlots.get(filter.get(count)));
			}

			setDefaultSettingsForGallery(spectrumPlot);
			getListSpectrumPlots().add(spectrumPlot);
			spectrumPlot.setId(count / cols, j);
			gallery.add(spectrumPlot);
			arr.add(spectrumPlot);
			count += cols;

			spectrumPlot.getPlot().getDomainAxis(0).setTickMarkInsideLength(16);
			spectrumPlot.getPlot().getDomainAxis(1).setTickMarkInsideLength(15);

			if (spectrumPlot.getPlot().getDomainAxis(0).getClass() == NumberAxis.class) {
				((NumberAxis)spectrumPlot.getPlot().getDomainAxis(0)).setNumberFormatOverride(DECIMAL_FORMAT);
			}

			// Find reference
			colRef = SpectrumPlot.setMaxValueAxis(colRef, spectrumPlot.getPlot().getDomainAxis());
			if (count >= filter.size()) {
				flag = true;
				rr = i + 1;
				break;
			}
		}

		axisPane = createBottomAxisCell();
		arr.add(axisPane);
		for (SpectrumPlot sp : arr) {
			sp.initDomainsRangeWith(colRef);
			XYPlotCassisUtil.configureRenderer(sp);
		}

		galleryAxis = new JPanel(new BorderLayout());
		galleryAxis.add(gallery, BorderLayout.CENTER);
		if (rr > 0 && rr < rows) {
			p = new JPanel(new BorderLayout());
			p.add(axisPane, BorderLayout.NORTH);
			p.add(new JLabel(), BorderLayout.CENTER);
			gallery.add(p);

			JLabel dummy = new JLabel();
			dummy.setSize(1, 40);
			dummy.setPreferredSize(dummy.getSize());
			galleryAxis.add(dummy, BorderLayout.SOUTH);
		} else {
			galleryAxis.add(axisPane, BorderLayout.SOUTH);
		}
		this.setLayout(new BorderLayout());
		this.add(gallery, BorderLayout.CENTER);
		this.add(galleryAxis, BorderLayout.SOUTH);
	}

	private SpectrumPlot createBottomAxisCell() {
		SpectrumPlot axis = new SpectrumPlot(
				ChartUtilities.createEmptyXYStepChartAxis(POSITION.BOTTOM),
				new int[] { 60, 2, 0, 0 }, true, false);
		XYPlot plot = axis.getPlot();
		ChartPanel chartPanel = axis.getChartPanel();

		chartPanel.setMouseZoomable(false);
		chartPanel.setMouseWheelEnabled(false);
		AxisSpace domainSpace = new AxisSpace();
		domainSpace.setTop(0);
		domainSpace.setBottom(40);
		plot.setFixedDomainAxisSpace(domainSpace);

		chartPanel.setMinimumDrawHeight(0);

		AxisSpace rangeSpace = new AxisSpace();
		rangeSpace.setLeft(60);
		rangeSpace.setRight(2);
		plot.setFixedRangeAxisSpace(rangeSpace);

		NumberAxis yleft = new NumberAxis(null);
		yleft.setAxisLineVisible(false);
		yleft.setTickMarksVisible(false);
		yleft.setTickLabelsVisible(false);
		plot.setRangeAxis(0, yleft);

		String axe;
		if (!collection[1].isEmpty() && !filter.isEmpty()
				&& collection[1].get(filter.get(0)).getSeriesCount() >= 1) {
			axe = collection[1].get(filter.get(0)).getSeries(0).getXAxis()
					.getTitleLabel();
		} else {
			axe = SpectrumPlot.BOTTOM_TITLE;
		}

		NumberAxis xbottom = new NumberAxis(axe);
		xbottom.setAxisLineVisible(false);
		xbottom.setTickMarksVisible(false);
		Font labelFont = new Font("Monospace", Font.BOLD, 11);
		Font tickFont = new Font("Monospace", Font.PLAIN, 9);
		xbottom.setLabelFont(labelFont);
		xbottom.setTickLabelFont(tickFont);
		xbottom.setTickLabelsVisible(true);
		xbottom.setNumberFormatOverride(DECIMAL_FORMAT);
		plot.setDomainAxis(0, xbottom);

		plot.setNoDataMessage("");
		axis.setBorder(null);

		axis.setMinimumSize(new Dimension(40, 36));
		axis.setPreferredSize(new Dimension(40, 36));

		return axis;
	}

	public boolean getFlag() {
		return flag;
	}

	public void setSave(boolean saveMode) {
		if (axisPane == null || gallery == null || galleryAxis == null) {
			return;
		}
		if (saveMode) {
			oldColor = gallery.getBackground();
			oldColorAxisPane = axisPane.getJFreeChart().getBackgroundPaint();
			axisPane.getJFreeChart().setBackgroundPaint(Color.WHITE);
			gallery.setBackground(Color.WHITE);
			galleryAxis.setBackground(Color.WHITE);
			if (p != null) {
				p.setBackground(Color.WHITE);
			}
		} else {
			axisPane.getJFreeChart().setBackgroundPaint(oldColorAxisPane);
			gallery.setBackground(oldColor);
			galleryAxis.setBackground(oldColor);
			if (p != null) {
				p.setBackground(oldColor);
			}
		}
	}
}
