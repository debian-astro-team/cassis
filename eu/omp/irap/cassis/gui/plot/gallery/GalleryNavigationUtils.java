/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.gallery;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility class for the GalleryNavigation.
 *
 * @author M. Boiziot
 */
public final class GalleryNavigationUtils {

	/**
	 * Disabled constructor as it is an utility class.
	 */
	private GalleryNavigationUtils() {
	}

	/**
	 * Parse a list of plot number and build an optimized String
	 * representation for the GalleryNavigation list of plots.
	 *
	 * @param listPlot The list of plots number.
	 * @return The optimized String representation.
	 */
	public static String getStringOfSpectrumList(List<Integer> listPlot) {
		if (listPlot == null || listPlot.isEmpty()) {
			return "";
		} else if (listPlot.size() == 1) {
			return String.valueOf(listPlot.get(0) + 1);
		} else if (listPlot.size() >= 3 &&
				listPlot.get(listPlot.size()-1) == (listPlot.size() - 1) &&
				isSortedList(listPlot)) {
			return (listPlot.get(0) + 1) + "-" + listPlot.size();
		} else {
			List<Integer> workList = new ArrayList<>(listPlot);
			int i;
			StringBuilder sb = new StringBuilder();
			while (!workList.isEmpty()) {
				i = 0;
				sb.append(workList.get(i) + 1);
				while (i < workList.size() - 1 && workList.get(i+1) == workList.get(i) + 1) {
					i++;
				}

				if (i == 0) {
					workList.remove(0);
				} else if (i == 1) {
					sb.append(",").append(workList.get(i)+1);
					workList.remove(0);
					workList.remove(0);
				} else {
					sb.append("-").append(workList.get(i) +1);
					for (; i >= 0; i--) {
						workList.remove(0);
					}
				}
				if (!workList.isEmpty()) {
					sb.append(",");
				}
			}
			return sb.toString();
		}
	}

	/**
	 * Return if a Integer list is sorted by number.
	 *
	 * @param list The list to check.
	 * @return if the list is sorted by number.
	 */
	private static boolean isSortedList(List<Integer> list) {
		int i = 0;
		int elements = list.size();
		while (i < elements - 1) {
			if (list.get(i) >= list.get(i+1)) {
				return false;
			}
			i++;
		}
		return true;
	}

	/**
	 * Return a list of plot number from a String.
	 *
	 * @param text The string.
	 * @param plotNumber The number of plot (number highter than this one will be ignored).
	 * @return The list of plot number.
	 *
	 * @throws IllegalArgumentException If the text is null, empty or can not be parsed.
	 */
	public static List<Integer> getListOfSpectrums(String text, int plotNumber) throws IllegalArgumentException {
		if (text == null || text.trim().isEmpty()) {
			throw new IllegalArgumentException("The provided text input is empty.");
		}

		if ("*".equals(text)) {
			return getAllPlots(plotNumber);
		}

		Pattern pattern = Pattern.compile("[^0-9\\-, ]");
		Matcher matcher = pattern.matcher(text);
		if (matcher.find() || text.trim().isEmpty()) {
			throw new IllegalArgumentException("The input string \"" + text + "\" can not be parsed.");
		}

		Pattern patternComma = Pattern.compile("[,]");
		String[] results = patternComma.split(text.trim());

		List<Integer> filter = new ArrayList<>();
		for (int i = 0; i < results.length; i++) {
			GalleryNavigationUtils.splitTrait(results[i], filter, plotNumber);
		}

		return filter;
	}

	/**
	 * Create a list of all plots (from 0 to plotNumber - 1).
	 *
	 * @param plotNumber The number of plot (number highter than this one will be ignored).
	 * @return list of all plots (from 0 to plotNumber - 1)
	 */
	private static List<Integer> getAllPlots(int plotNumber) {
		List<Integer> listFilter = new ArrayList<>(plotNumber);
		for (int i = 0; i < plotNumber; i++) {
			listFilter.add(i);
		}
		return listFilter;
	}

	/**
	 * Parse the text and add the found plots numbers in the filter if not already in.
	 *
	 * @param text The text to parse.
	 * @param filter The list of plots numbers
	 * @param plotNumber The plot number than it will be ignored.
	 */
	private static void splitTrait(String text, List<Integer> filter, int plotNumber) {
		if (text.trim().startsWith("-") || text.trim().endsWith("-")) {
			return;
		}

		Pattern pattern = Pattern.compile("[\\-]");
		String[] results = pattern.split(text);

		if (results.length == 1) {
			String sub = results[0].trim();
			if (!"".equals(sub)) {
				int val = Integer.parseInt(sub);
				if (val >= 1 && val <= plotNumber && !filter.contains(val - 1)) {
					filter.add(val - 1);
				}
			}
		} else if (results.length == 2) {
			String sub1 = results[0].trim();
			String sub2 = results[1].trim();
			if (!"".equals(sub1) && !"".equals(sub2)) {
				int left = Integer.parseInt(sub1);
				int right = Integer.parseInt(sub2);
				for (int i = left; i <= right; i++) {
					if (i <= plotNumber) {
						if (i >= 1 && !filter.contains(i - 1)) {
							filter.add(i - 1);
						}
					}
					else {
						break;
					}
				}
			}
		}
	}
}
