/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.gallery;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Paint;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import org.jfree.chart.axis.ValueAxis;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.gui.plot.curve.Rendering;
import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassisCollection;
import eu.omp.irap.cassis.gui.plot.line.CassisPlot;
import eu.omp.irap.cassis.gui.plot.simple.ChangeRenderingInterface;
import eu.omp.irap.cassis.gui.plot.simple.SpectrumPlot;
import eu.omp.irap.cassis.gui.plot.simple.util.XYPlotCassisUtil;
import eu.omp.irap.cassis.gui.plot.util.InterValMarkerCassis;

@SuppressWarnings("serial")
public class GalleryRowPane extends GalleryPane {

	private static final Logger LOGGER = LoggerFactory.getLogger(GalleryRowPane.class);

	private int cols;
	private ArrayList<XYSeriesCassisCollection>[] collection;
	private ValueAxis rowRef;
	private int offset;
	private boolean flag = true;
	private List<Integer> numPlots;
	private boolean plotNumberVisible;
	private Color plotNumberColor;
	private CassisPlot cassisPlot;
	private SpectrumPlot axisPane;
	private JPanel gallery;
	private Color oldColorGallery;
	private Paint oldColorAxisPane;
	private boolean yAxisLog;
	private boolean xAxisLog;
	private Rendering rendering;
	private ChangeRenderingInterface crInterface;
	private List<ArrayList<InterValMarkerCassis>> markerListByPlot;


	public GalleryRowPane(int cols, int offset,
			ArrayList<XYSeriesCassisCollection>[] collection, List<Integer> filter,
			List<Integer> numPlots, boolean plotNumberVisible, Color plotNumberColor,
			CassisPlot aCassisPlot, boolean yAxisLog, boolean xAxisLog, Rendering rendering,
			ChangeRenderingInterface crInterface,
			List<ArrayList<InterValMarkerCassis>> markerListByPlot) {
		super();
		setNumberOfCols(cols);
		this.offset = offset;
		this.collection = collection;
		this.filter = filter;
		this.numPlots = numPlots;
		this.plotNumberVisible = plotNumberVisible;
		this.plotNumberColor = plotNumberColor;
		this.cassisPlot = aCassisPlot;
		this.yAxisLog = yAxisLog;
		this.xAxisLog = xAxisLog;
		this.rendering = rendering;
		this.crInterface = crInterface;
		this.markerListByPlot = markerListByPlot;
		initComponents();
	}

	private void initComponents() {
		Dimension size = new Dimension(WIDTH, HEIGHT);
		setSize(size);
		setPreferredSize(size);
		setLayout(new BorderLayout());
		try {
			createGallery();
		} catch (CloneNotSupportedException e) {
			LOGGER.error("An error occured while trying to create the gallery", e);
		}
	}

	public void setNumberOfCols(int cols) {
		this.cols = cols;
	}

	@Override
	int getPlotHeight(int rows) {
		return super.getPlotHeight(rows) + 30;
	}

	private void createGallery() throws CloneNotSupportedException {
		gallery = new JPanel(new GridLayout(0, cols));
		rowRef = null;
		List<SpectrumPlot> arr = new ArrayList<>();
		for (int j = 0; j < cols; j++) {
			SpectrumPlot sp = new SpectrumPlot(
					collection[GallerySortPane.CENTER_INDEX].get(filter.get(offset)),
					collection[GallerySortPane.TOP_LINE_INDEX].get(filter.get(offset)),
					collection[GallerySortPane.TOP_LINE_ERROR_INDEX].get(filter.get(offset)),
					collection[GallerySortPane.BOTTOM_SIGNAL_INDEX].get(filter.get(offset)),
					collection[GallerySortPane.BOTTOM_IMAGE_INDEX].get(filter.get(offset)),
					new int[] { 1, j == cols - 1 ? 1 : 0, 30, 30 },
					true);
			sp.setBottomTitle(cassisPlot.getxAxisCassis().getTitleLabel());
			sp.updateRendering(rendering);
			sp.setExternalChangeRenderingInterface(crInterface);
			sp.addMarkers(markerListByPlot.get(filter.get(offset)), false);

			if (yAxisLog) {
				sp.setYAxisToLog();
			}
			if (xAxisLog) {
				sp.setXAxisToLog();
			}
			if (plotNumberVisible) {
				addNumberToPlot(sp.getPlot(), plotNumberColor, numPlots.get(filter.get(offset)));
			}

			setDefaultSettingsForGallery(sp);
			sp.setTopAxisDomainSynchro(false);

			sp.setId(offset / cols, j);
			listSpectrumPlots.add(sp);
			gallery.add(sp);
			arr.add(sp);
			offset++;
			// Find reference
			rowRef = SpectrumPlot.setMaxValueAxis(rowRef, sp.getPlot().getRangeAxis());

			if (offset == filter.size()) {
				flag = false;
				break;
			}
		}

		axisPane = GalleryUtils.createLeftAxisCell(cassisPlot.getYAxisCassis().toString(),
				listSpectrumPlots.get(0), yAxisLog, new int[] { 0, 0, 30, 30 });
		arr.add(axisPane);

		for (SpectrumPlot sp : arr) {
			sp.synchronizeRangesWith(rowRef);
			XYPlotCassisUtil.configureRenderer(sp);
		}

		add(gallery, BorderLayout.CENTER);
		add(axisPane, BorderLayout.WEST);
	}

	public boolean getFlag() {
		return flag;
	}

	public void setSave(boolean saveMode) {
		if (saveMode) {
			oldColorGallery = gallery.getBackground();
			oldColorAxisPane = axisPane.getJFreeChart().getBackgroundPaint();
			axisPane.getJFreeChart().setBackgroundPaint(Color.WHITE);
			gallery.setBackground(Color.WHITE);
		} else {
			axisPane.getJFreeChart().setBackgroundPaint(oldColorAxisPane);
			gallery.setBackground(oldColorGallery);
		}
	}
}
