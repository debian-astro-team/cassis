/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.gallery;

import java.awt.Dimension;
import java.awt.Font;
import java.text.DecimalFormat;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.axis.AxisSpace;
import org.jfree.chart.axis.LogAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;

import eu.omp.irap.cassis.gui.plot.simple.SpectrumPlot;
import eu.omp.irap.cassis.gui.plot.simple.util.ChartUtilities;
import eu.omp.irap.cassis.gui.plot.simple.util.ChartUtilities.POSITION;

/**
 * Utility class for gallery.
 *
 * @author M. Boiziot
 */
public class GalleryUtils {

	/**
	 * Constructor. Utility class, must not be instanced.
	 */
	private GalleryUtils() {
		// Empty, must not be used.
	}

	/**
	 * Create a SpectrumPlot for which we only see the left axis.
	 *
	 * @param yLabel The label for the left axis.
	 * @param plotRef The plot used as reference.
	 * @param logAxis true if it is a log axis, false otherwise.
	 * @param insets The insets to use.
	 * @return The created SpectrumPlot.
	 */
	public static SpectrumPlot createLeftAxisCell(String yLabel,
			SpectrumPlot plotRef, boolean logAxis, int[] insets) {
		SpectrumPlot axis = new SpectrumPlot(
				ChartUtilities.createEmptyXYStepChartAxis(POSITION.LEFT), insets, true);
		XYPlot plot = axis.getPlot();
		ChartPanel chartPanel = axis.getChartPanel();

		chartPanel.setMouseZoomable(false);
		chartPanel.setMouseWheelEnabled(false);

		chartPanel.setMinimumDrawWidth(0);

		AxisSpace rangeSpace = new AxisSpace();
		rangeSpace.setLeft(60);
		rangeSpace.setRight(0);
		plot.setFixedRangeAxisSpace(rangeSpace);

		ValueAxis yleft = logAxis ? getLogAxis(yLabel) : getNumberAxis(yLabel);
		plot.setRangeAxis(0, yleft);
		axis.setBorder(null);
		plot.getRangeAxis(1).setTickMarksVisible(false);
		plot.getDomainAxis(1).setTickLabelsVisible(false);
		plot.getDomainAxis(0).setTickLabelsVisible(false);
		axis.setMinimumSize(new Dimension(60, 40));
		axis.setPreferredSize(new Dimension(60, 40));

		axis.copyRangeMargins(plotRef.getPlot());

		return axis;
	}

	/**
	 * Create, configure and return a LogAxis with the given label.
	 *
	 * @param label The label.
	 * @return The created LogAxis.
	 */
	private static LogAxis getLogAxis(String yAxisString) {
		LogAxis axis = SpectrumPlot.getLogAxis(yAxisString, true);
		axis.setAxisLineVisible(false);
		axis.setTickMarksVisible(false);
		Font labelFont = new Font("Monospace", Font.BOLD, 11);
		Font tickFont = new Font("Monospace", Font.PLAIN, 9);
		axis.setLabelFont(labelFont);
		axis.setTickLabelFont(tickFont);
		axis.setTickLabelsVisible(true);
		return axis;
	}

	/**
	 * Create, configure and return a NumberAxis with the given label.
	 *
	 * @param label The label.
	 * @return The created NumberAxis.
	 */
	private static NumberAxis getNumberAxis(String label) {
		NumberAxis axis = new NumberAxis(label);
		axis.setAxisLineVisible(false);
		axis.setTickMarksVisible(false);
		Font labelFont = new Font("Monospace", Font.BOLD, 11);
		Font tickFont = new Font("Monospace", Font.PLAIN, 9);
		axis.setLabelFont(labelFont);
		axis.setTickLabelFont(tickFont);
		axis.setTickLabelsVisible(true);
		axis.setNumberFormatOverride(new DecimalFormat("0.##"));
		return axis;
	}

}
