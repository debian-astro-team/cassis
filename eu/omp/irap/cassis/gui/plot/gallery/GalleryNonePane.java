/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.gallery;

import java.awt.Color;
import java.awt.GridLayout;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import eu.omp.irap.cassis.gui.plot.curve.Rendering;
import eu.omp.irap.cassis.gui.plot.line.CassisPlot;
import eu.omp.irap.cassis.gui.plot.simple.ChangeRenderingInterface;
import eu.omp.irap.cassis.gui.plot.simple.SpectrumPlot;
import eu.omp.irap.cassis.gui.plot.simple.util.XYPlotCassisUtil;
import eu.omp.irap.cassis.gui.plot.util.InterValMarkerCassis;

@SuppressWarnings("serial")
public class GalleryNonePane extends GallerySortPane {


	public GalleryNonePane(int rows, int cols, List<CassisPlot> listCassisPlot,
			List<Integer> filter, int glOffset, boolean plotNumberVisible,
			Color plotNumberColor, boolean yAxisLog, boolean xAxisLog, Rendering rendering,
			ChangeRenderingInterface crInterface,
			List<ArrayList<InterValMarkerCassis>> markerListByPlot) {
		super(rows, cols, listCassisPlot, filter, glOffset, plotNumberVisible,
				plotNumberColor, yAxisLog, xAxisLog, rendering, crInterface,
				markerListByPlot);
	}

	@Override
	int getPlotHeight(int rows) {
		return super.getPlotHeight(rows) + 30;
	}

	@Override
	protected void createGallery() throws CloneNotSupportedException, IOException {
		super.createGallery();
		setBorder(BorderFactory.createEmptyBorder(10, 10, 0, 10));

		JPanel gallery = this;
		GridLayout gl = new GridLayout(0, cols);
		gl.setHgap(10);
		gl.setVgap(10);
		gallery.setLayout(gl);
		int count = glOffset * rows * cols;

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				if (count == filter.size()) {
					gallery.add(new JLabel());
					continue;
				}

				SpectrumPlot sp = new SpectrumPlot(
						collections[CENTER_INDEX].get(filter.get(count)),
						collections[TOP_LINE_INDEX].get(filter.get(count)),
						collections[TOP_LINE_ERROR_INDEX].get(filter.get(count)),
						collections[BOTTOM_SIGNAL_INDEX].get(filter.get(count)),
						collections[BOTTOM_IMAGE_INDEX].get(filter.get(count)),
						new int[] { 50, 2, 30, 30 }, true);
				sp.updateRendering(rendering);
				sp.setExternalChangeRenderingInterface(crInterface);
				sp.addMarkers(markerListByPlot.get(filter.get(count)), false);

				if (yAxisLog) {
					sp.setYAxisToLog();
				}
				if (xAxisLog) {
					sp.setXAxisToLog();
				}
				if (plotNumberVisible) {
					addNumberToPlot(sp.getPlot(), plotNumberColor, numPlots.get(filter.get(count)));
				}

				setDefaultSettingsForGallery(sp);
				sp.setTopAxisDomainSynchro(false);
				sp.setId(count / cols, j);
				listSpectrumPlots.add(sp);
				gallery.add(sp);
				count++;
			}
		}

		final String bottomAxisName = cassisPlots.get(0).getxAxisCassis().getTitleLabel();

		for (SpectrumPlot sp : listSpectrumPlots) {
			XYPlotCassisUtil.configureRenderer(sp);
			sp.setBottomTitle(bottomAxisName);
		}
	}

}
