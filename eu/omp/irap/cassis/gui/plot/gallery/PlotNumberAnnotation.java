/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.gallery;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;

import org.jfree.chart.annotations.XYTextAnnotation;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.XYPlot;
import org.jfree.text.TextUtilities;
import org.jfree.ui.TextAnchor;

/**
 * Simple text annotation specific for the plot number in Gallery.
 * Theses annotations does not take a (x/y) place in the domain/range of the
 *  plot but a place specific to the data area of the plot, which is static.
 *
 * @author M. Boiziot
 */
public class PlotNumberAnnotation extends XYTextAnnotation {

	private static final long serialVersionUID = -4853401877088927052L;


	/**
	 * Constructor.
	 *
	 * @param text The text to display (the number of the plot as a String).
	 */
	public PlotNumberAnnotation(String text) {
		super(text, Double.NaN, Double.NaN);
	}

	/**
	 * Draws the annotation.
	 *
	 * @param g2  the graphics device.
	 * @param plot  the plot.
	 * @param dataArea  the data area.
	 * @param domainAxis  the domain axis.
	 * @param rangeAxis  the range axis.
	 * @param rendererIndex  the renderer index.
	 * @param info  an optional info object that will be populated with
	 *			  entity information.
	 */
	@Override
	public void draw(Graphics2D g2, XYPlot plot, Rectangle2D dataArea,
					 ValueAxis domainAxis, ValueAxis rangeAxis,
					 int rendererIndex, PlotRenderingInfo info) {
		double minX = dataArea.getMinX();
		double maxX = dataArea.getMaxX();

		double minY = dataArea.getMinY();
		double maxY = dataArea.getMaxY();

		float anchorX = (float) (minX + (maxX - minX) * 0.95);
		float anchorY = (float) (minY + (maxY - minY) * 0.05);

		g2.setFont(getFont());
		Shape hotspot = TextUtilities.calculateRotatedStringBounds(
				getText(), g2, anchorX, anchorY, getTextAnchor(),
				getRotationAngle(), getRotationAnchor());
		if (this.getBackgroundPaint() != null) {
			g2.setPaint(this.getBackgroundPaint());
			g2.fill(hotspot);
		}
		g2.setPaint(getPaint());
		TextUtilities.drawRotatedString(getText(), g2, anchorX, anchorY,
				TextAnchor.TOP_RIGHT, getRotationAngle(), getRotationAnchor());

		if (this.isOutlineVisible()) {
			g2.setStroke(this.getOutlineStroke());
			g2.setPaint(this.getOutlinePaint());
			g2.draw(hotspot);
		}

		String toolTip = getToolTipText();
		String url = getURL();
		if (toolTip != null || url != null) {
			addEntity(info, hotspot, rendererIndex, toolTip, url);
		}
	}
}
