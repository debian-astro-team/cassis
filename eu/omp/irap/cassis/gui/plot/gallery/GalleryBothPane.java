/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.gallery;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.axis.AxisSpace;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;

import eu.omp.irap.cassis.gui.plot.curve.Rendering;
import eu.omp.irap.cassis.gui.plot.line.CassisPlot;
import eu.omp.irap.cassis.gui.plot.simple.ChangeRenderingInterface;
import eu.omp.irap.cassis.gui.plot.simple.SpectrumPlot;
import eu.omp.irap.cassis.gui.plot.simple.util.ChartUtilities;
import eu.omp.irap.cassis.gui.plot.simple.util.XYPlotCassisUtil;
import eu.omp.irap.cassis.gui.plot.simple.util.ChartUtilities.POSITION;
import eu.omp.irap.cassis.gui.plot.util.InterValMarkerCassis;

@SuppressWarnings("serial")
public class GalleryBothPane extends GallerySortPane {

	private JPanel bottomAxisTitle;
	private JPanel bottom;
	private JPanel topLabel;
	private JPanel gallery0;
	private JPanel leftAxis;
	private JPanel bottomAxis;
	private Color oldColor;
	private List<JPanel> listJPanel;
	private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("0.##");


	public GalleryBothPane(int rows, int cols, List<CassisPlot> listCassisPlot,
			List<Integer> filter, int glOffset, boolean plotNumberVisible,
			Color plotNumberColor, boolean yAxisLog, boolean xAxisLog, Rendering rendering,
			ChangeRenderingInterface crInterface,
			List<ArrayList<InterValMarkerCassis>> markerListByPlot) {
		super(rows, cols, listCassisPlot, filter, glOffset, plotNumberVisible,
				plotNumberColor, yAxisLog, xAxisLog, rendering, crInterface,
				markerListByPlot);
	}

	@Override
	protected void createGallery() throws CloneNotSupportedException, IOException {
		super.createGallery();
		setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));

		gallery0 = new JPanel(new GridLayout(0, cols));
		int count = glOffset * rows * cols;

		ValueAxis colRef = null;
		ValueAxis rowRef = null;
		boolean flag = true;
		int rowLastPlot = 0;
		int columnLastPlot = 0;

		for (int i = 0; i < rows && flag; i++) {
			for (int j = 0; j < cols && flag; j++) {
				SpectrumPlot p = new SpectrumPlot(
						collections[CENTER_INDEX].get(filter.get(count)),
						collections[TOP_LINE_INDEX].get(filter.get(count)),
						collections[TOP_LINE_ERROR_INDEX].get(filter.get(count)),
						collections[BOTTOM_SIGNAL_INDEX].get(filter.get(count)),
						collections[BOTTOM_IMAGE_INDEX].get(filter.get(count)),
						new int[] { 1, j == cols - 1 || count + 1 == filter.size() ? 1 : 0, 1,
								i == rows - 1 || count + cols >= filter.size() ? 1 : 0 }, true);
				p.updateRendering(rendering);
				p.setExternalChangeRenderingInterface(crInterface);
				p.addMarkers(markerListByPlot.get(filter.get(count)), false);

				if (yAxisLog) {
					p.setYAxisToLog();
				}
				if (xAxisLog) {
					p.setXAxisToLog();
				}
				if (plotNumberVisible) {
					addNumberToPlot(p.getPlot(), plotNumberColor, numPlots.get(filter.get(count)));
				}

				setDefaultSettingsForGallery(p);
				p.setId(count / cols, j);
				gallery0.add(p);
				listSpectrumPlots.add(p);
				count++;
				// Find references
				colRef = SpectrumPlot.setMaxValueAxis(colRef, p.getPlot().getDomainAxis());
				rowRef = SpectrumPlot.setMaxValueAxis(rowRef, p.getPlot().getRangeAxis());

				p.getPlot().getDomainAxis(0).setTickMarkInsideLength(16);
				p.getPlot().getDomainAxis(1).setTickMarkInsideLength(15);

				if (p.getPlot().getDomainAxis(0).getClass() == NumberAxis.class) {
					((NumberAxis)p.getPlot().getDomainAxis(0)).setNumberFormatOverride(DECIMAL_FORMAT);
				}

				rowLastPlot = i;
				columnLastPlot = j;
				if (count == filter.size()) {
					flag = false;
					break;
				}
			}
		}

		int nbComponentInGallery = gallery0.getComponentCount();
		for (int k = nbComponentInGallery; k < rows * cols; k++) {
			gallery0.add(new JLabel()); // add empty "plot"
		}


		leftAxis = new JPanel(new GridLayout(rows, 1));
		for (int i = 0; i < rows; i++) {
			if (i > rowLastPlot) {
				leftAxis.add(new JLabel()); // add empty leftAxis
				continue;
			}

			SpectrumPlot axis = GalleryUtils.createLeftAxisCell(
					cassisPlots.get(0).getYAxisCassis().toString(),
					listSpectrumPlots.get(0), yAxisLog, new int[] { 0, 0, 0, 0 }); // TODO check insets
			leftAxis.add(axis);
			listSpectrumPlots.add(axis);
		}

		bottomAxis = new JPanel(new GridLayout(1, cols));
		bottomAxis.setBackground(SpectrumPlot.BACKGROUND);
		bottomAxis.setBorder(BorderFactory.createEmptyBorder(0, 58, 0, 0));

		for (int j = 0; j < cols; j++) {
			boolean isLastLine = false;
			boolean	isFinalLine = false;
			boolean havePlot = false;

			if (j <= columnLastPlot) {
				isLastLine = true;
				havePlot = true;
				if (rowLastPlot == rows-1) {
					isFinalLine = true;
				}
			} else {
				isFinalLine = false;
				havePlot = nbComponentInGallery - columnLastPlot - cols+j-1 >=0;
			}

			if (havePlot) {
				SpectrumPlot bottomAxisPane = createBottomAxisCell();
				listSpectrumPlots.add(bottomAxisPane);
				if (isFinalLine) {
					bottomAxis.add(bottomAxisPane);
				} else {
					int index;
					if (isLastLine) {
						index = (rowLastPlot + 1) * cols + j;
					} else {
						index = nbComponentInGallery + j - 1 - columnLastPlot;
					}

					if (index <= gallery0.getComponentCount()) {
						JPanel panel = createAxisPanel(bottomAxisPane);
						getListJPanel().add(panel);
						gallery0.remove(index);
						gallery0.add(panel, index);
					}
				}
			}
		}

		while (bottomAxis.getComponentCount() < cols) {
			bottomAxis.add(new JLabel());
		}

		JPanel gallery = this;
		gallery.setLayout(new BorderLayout());
		topLabel = new JPanel();
		topLabel.add(new JLabel());
		gallery.add(topLabel, BorderLayout.NORTH);
		gallery.add(leftAxis, BorderLayout.WEST);
		gallery.add(gallery0, BorderLayout.CENTER);
		gallery.add(getBottomAxisTitle(), BorderLayout.SOUTH);

		for (SpectrumPlot sp : listSpectrumPlots) {
			sp.initDomainsRangeWith(colRef);
			sp.synchronizeRangesWith(rowRef);
			XYPlotCassisUtil.configureRenderer(sp);
		}
	}

	private JPanel getBottomAxisTitle() {
		JLabel dummy = new JLabel();
		dummy.setSize(2, 1);
		dummy.setPreferredSize(new Dimension(2, 1));

		bottomAxisTitle = new JPanel(new BorderLayout());
		bottomAxisTitle.add(dummy, BorderLayout.WEST);
		bottomAxisTitle.add(bottomAxis, BorderLayout.CENTER);

		bottom = new JPanel();
		bottom.add(new JLabel(cassisPlots.get(0).getxAxisCassis().getTitleLabel()));
		bottomAxisTitle.add(bottom, BorderLayout.SOUTH);
		return bottomAxisTitle;
	}

	private static SpectrumPlot createBottomAxisCell() {
		SpectrumPlot axis = new SpectrumPlot(ChartUtilities.createEmptyXYStepChartAxis(POSITION.BOTTOM),
				new int[] { 1, 1, 0, 0 }, true, false);

		XYPlot plot = axis.getPlot();
		ChartPanel chartPanel = axis.getChartPanel();

		chartPanel.setMouseZoomable(false);
		chartPanel.setMouseWheelEnabled(false);

		AxisSpace domainSpace = new AxisSpace();
		domainSpace.setTop(0);
		domainSpace.setBottom(20);
		plot.setFixedDomainAxisSpace(domainSpace);

		chartPanel.setMinimumDrawHeight(0);

		AxisSpace rangeSpace = new AxisSpace();
		rangeSpace.setLeft(0);
		rangeSpace.setRight(0);
		plot.setFixedRangeAxisSpace(rangeSpace);

		NumberAxis yleft = new NumberAxis(null);
		yleft.setAxisLineVisible(false);
		yleft.setTickMarksVisible(false);
		yleft.setTickLabelsVisible(false);
		plot.setRangeAxis(0, yleft);

		NumberAxis xbottom = new NumberAxis(null);
		xbottom.setAxisLineVisible(false);
		xbottom.setTickMarksVisible(false);
		Font labelFont = new Font("Monospace", Font.BOLD, 11);
		Font tickFont = new Font("Monospace", Font.PLAIN, 9);
		xbottom.setLabelFont(labelFont);
		xbottom.setTickLabelFont(tickFont);
		xbottom.setTickLabelsVisible(true);
		xbottom.setNumberFormatOverride(DECIMAL_FORMAT);
		plot.setDomainAxis(0, xbottom);

		plot.setNoDataMessage("");

		axis.setBorder(null);

		axis.setMinimumSize(new Dimension(60, 20));
		axis.setPreferredSize(new Dimension(60, 20));

		return axis;
	}

	private JPanel createAxisPanel(SpectrumPlot axis) {
		JPanel p = new JPanel(new BorderLayout());
		JLabel dummy = new JLabel();
		p.add(dummy, BorderLayout.CENTER);
		p.add(axis, BorderLayout.NORTH);
		return p;
	}

	private List<JPanel> getListJPanel() {
		if (listJPanel == null) {
			listJPanel = new ArrayList<>();
		}
		return listJPanel;
	}

	@Override
	public void setSave(boolean saveMode) {
		if (saveMode) {
			oldColor = gallery0.getBackground();
			bottomAxisTitle.setBackground(Color.WHITE);
			bottom.setBackground(Color.WHITE);
			topLabel.setBackground(Color.WHITE);
			gallery0.setBackground(Color.WHITE);
			leftAxis.setBackground(Color.WHITE);
			bottomAxis.setBackground(Color.WHITE);
			List<JPanel> list = getListJPanel();
			for (JPanel panel : list) {
				panel.setBackground(Color.WHITE);
			}
		} else {
			bottomAxisTitle.setBackground(oldColor);
			bottom.setBackground(oldColor);
			topLabel.setBackground(oldColor);
			gallery0.setBackground(oldColor);
			leftAxis.setBackground(oldColor);
			bottomAxis.setBackground(SpectrumPlot.BACKGROUND);
			List<JPanel> list = getListJPanel();
			for (JPanel panel : list) {
				panel.setBackground(oldColor);
			}
		}
	}
}
