/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.gallery;


import java.util.ArrayList;
import java.util.List;

import org.python.modules.math;

import eu.omp.irap.cassis.common.events.ListenerManager;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;

/**
 * Model for the GalleryNavigation.
 *
 * @author M. Boiziot
 */
public class GalleryNavigationModel extends ListenerManager {

	public static final String CURRENT_GALLERY_UPDATE_EVENT = "currentGalleryUpdate";
	public static final String GALLERY_NUMBER_UPDATE_EVENT = "galleryNumberUpdate";
	public static final String ROWS_UPDATE_EVENT = "rowsUpdate";
	public static final String COLS_UPDATE_EVENT = "colsUpdate";
	public static final String PLOT_NUMBER_EVENT = "plotNumberUpdate";
	public static final String FILTER_UPDATE_EVENT = "filterUpdate";
	public static final String SELECTED_GALLERY_OPTION_EVENT = "galleryOptionUpdate";
	public static final String TRIGGER_REFRESH_EVENT = "refreshTrigger";

	private int currentGallery;
	private int numberOfGalleries;
	private int rows;
	private int cols;
	private int plotNumber;
	private final GalleryOption[] optionsAllowed;
	private List<Integer> filter;
	private final boolean sortingComboboxEnabled;
	private GalleryOption selectedOption;


	/**
	 * Constructor with defaults parameters.
	 */
	public GalleryNavigationModel() {
		this(GalleryOption.values(), true);
	}

	/**
	 * Constructor.
	 *
	 * @param optionsAllowed The {@link GalleryOption}s allowed.
	 * @param sortingComboboxEnabled If the sorting combobox is enabled.
	 */
	public GalleryNavigationModel(GalleryOption[] optionsAllowed,
			boolean sortingComboboxEnabled) {
		this.optionsAllowed = optionsAllowed;
		this.selectedOption = GalleryOption.NONE;
		this.sortingComboboxEnabled = sortingComboboxEnabled;
		this.rows = 3;
		this.cols = 3;
		this.currentGallery = 1;
		this.numberOfGalleries = 1;
		this.filter = new ArrayList<>(1);
		this.filter.add(1);
		this.plotNumber = 1;
	}

	/**
	 * Return the number of the current gallery.
	 *
	 * @return the number of the current gallery.
	 */
	public int getCurrentGallery() {
		return currentGallery;
	}

	/**
	 * Change the number of the current gallery.
	 *
	 * @param currentGallery The new number of the current gallery.
	 */
	public void setCurrentGallery(int currentGallery) {
		this.currentGallery = currentGallery;
		fireDataChanged(new ModelChangedEvent(CURRENT_GALLERY_UPDATE_EVENT, currentGallery));
	}

	/**
	 * Return the number of galleries.
	 *
	 * @return the number of galleries.
	 */
	public int getNumberOfGalleries() {
		return numberOfGalleries;
	}

	/**
	 * Change the number of galleries.
	 *
	 * @param numberOfGalleries The new number of galleries.
	 */
	private void setNumberOfGalleries(int numberOfGalleries) {
		this.numberOfGalleries = numberOfGalleries;
		fireDataChanged(new ModelChangedEvent(GALLERY_NUMBER_UPDATE_EVENT, numberOfGalleries));
		if (currentGallery > numberOfGalleries || currentGallery < 1) {
			setCurrentGallery(1);
		}
	}

	/**
	 * Return the number of rows in the gallery.
	 *
	 * @return the number of rows in the gallery.
	 */
	public int getRows() {
		return rows;
	}

	/**
	 * Change the number of rows in the gallery.
	 *
	 * @param rows The new number of rows.
	 */
	public void setRows(int rows) {
		if (rows < 1) {
			throw new IllegalArgumentException("Rows value must be >= 1");
		}
		this.rows = rows;
		fireDataChanged(new ModelChangedEvent(ROWS_UPDATE_EVENT, rows));
		updateGalleryNumber();
		triggerRefresh();
	}

	/**
	 * Return the number of columns in the gallery.
	 *
	 * @return the number of columns.
	 */
	public int getCols() {
		return cols;
	}

	/**
	 * Change the number of columns in the gallery.
	 *
	 * @param cols The new number of columns.
	 */
	public void setCols(int cols) {
		if (cols < 1) {
			throw new IllegalArgumentException("Cols value must be >= 1");
		}
		this.cols = cols;
		fireDataChanged(new ModelChangedEvent(COLS_UPDATE_EVENT, cols));
		updateGalleryNumber();
		triggerRefresh();
	}

	/**
	 * Return the number of plot without filter.
	 *
	 * @return the number of plot without filter.
	 */
	public int getPlotNumber() {
		return plotNumber;
	}

	/**
	 * Change the number of plot without filter.
	 *
	 * @param number The new number of plot.
	 */
	public void setPlotNumber(int number) {
		this.plotNumber = number;
		fireDataChanged(new ModelChangedEvent(PLOT_NUMBER_EVENT, number));
		updateGalleryNumber();
	}

	/**
	 * Return the list of allowed GalleryOption.
	 *
	 * @return the list of allowed GalleryOption.
	 */
	public GalleryOption[] getOptionsAllowed() {
		return optionsAllowed;
	}

	/**
	 * Return the list of numbers of the plots displayed (number starting by 0; one less than the view).
	 *
	 * @return the numbers of displayed plots.
	 */
	public List<Integer> getFilter() {
		return filter;
	}

	/**
	 * Change the list of plots displayed.
	 *
	 * @param filter The numbers of displayed plots (numbers starting by 0; one less than the view).
	 * @param updateGallery If it is needed to update the gallery.
	 */
	public void setFilter(List<Integer> filter, boolean updateGallery) {
		this.filter = filter;
		fireDataChanged(new ModelChangedEvent(FILTER_UPDATE_EVENT));
		updateGalleryNumber();
		if (updateGallery) {
			triggerRefresh();
		}
	}

	/**
	 * Return if sorting combobox is enabled.
	 *
	 * @return if sorting combobox is enabled.
	 */
	public boolean isSortingComboboxEnabled() {
		return sortingComboboxEnabled;
	}

	/**
	 * Return the current selected GalleryOption.
	 *
	 * @return the current selected GalleryOption.
	 */
	public GalleryOption getSelectedOption() {
		return selectedOption;
	}

	/**
	 * Change the selected GalleryOption.
	 *
	 * @param selectedOption The new GalleryOption selected.
	 */
	public void setSelectedOption(GalleryOption selectedOption) {
		this.selectedOption = selectedOption;
		fireDataChanged(new ModelChangedEvent(SELECTED_GALLERY_OPTION_EVENT, selectedOption));
	}

	/**
	 * Update the gallery number.
	 */
	private void updateGalleryNumber() {
		int val = (int) math.ceil(filter.size() / (double)(rows * cols));
		setNumberOfGalleries(val);
	}

	/**
	 * Send a event for refreshing the view if there is one.
	 */
	public void triggerRefresh() {
		fireDataChanged(new ModelChangedEvent(TRIGGER_REFRESH_EVENT));
	}
}
