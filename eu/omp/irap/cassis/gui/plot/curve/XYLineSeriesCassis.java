/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.curve;

import java.util.List;

import eu.omp.irap.cassis.common.Formula;
import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.TypeFrequency;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.YAxisCassis;

/**
 * Class for top and bottom lines.
 *
 * @author glorian
 * @author M. Boiziot
 */
public class XYLineSeriesCassis extends XYSeriesCassis {

	private static final long serialVersionUID = 8255942737622206658L;

	public static final int TOP = 1;
	public static final int BOTTOM = 2;

	protected List<LineDescription> lines;


	/**
	 * Constructor.
	 *
	 * @param seriesName The name of the series.
	 * @param typeCurve The TypeCurve of the series.
	 * @param lines the lines.
	 * @param type The type of lines {@link XYLineSeriesCassis#TOP} or
	 *  {@link XYLineSeriesCassis#BOTTOM}.
	 * @param xAxis The x axis.
	 * @param addLines true to add the lines, false to do not.
	 */
	public XYLineSeriesCassis(String seriesName, TypeCurve typeCurve,
			List<LineDescription> lines, int type, XAxisCassis xAxis,
			boolean addLines) {
		super(seriesName, typeCurve);
		if (addLines) {
			addLines(lines, type, xAxis);
		}
	}
	/**
	 * Constructor.
	 *
	 * @param seriesName The name of the series.
	 * @param typeCurve The TypeCurve of the series.
	 * @param lines the lines.
	 * @param type The type of lines {@link XYLineSeriesCassis#TOP} or
	 *  {@link XYLineSeriesCassis#BOTTOM}.
	 * @param xAxis The x axis.
	 */
	public XYLineSeriesCassis(String seriesName, TypeCurve typeCurve,
			List<LineDescription> lines, int type, XAxisCassis xAxis) {
		this(seriesName, typeCurve, lines, type, xAxis, true);
	}

	/**
	 * Add the given lines.
	 *
	 * @param lines The lines to add.
	 * @param type The type of lines {@link XYLineSeriesCassis#TOP} or
	 *  {@link XYLineSeriesCassis#BOTTOM}.
	 * @param xAxis The x axis.
	 */
	public void addLines(List<LineDescription> lines, int type, XAxisCassis xAxis) {
		this.lines = lines;

		if (lines == null || lines.isEmpty())
			return;
		setXAxis(xAxis);
	}

	/**
	 * Return the list of lines.
	 *
	 * @return the list of lines.
	 * @see eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassis#getListOfLines()
	 */
	@Override
	public List<LineDescription> getListOfLines() {
		return lines;
	}

	@Override
	protected void buildXYSeries(XAxisCassis xaxis, YAxisCassis yaxis) {
		this.clear();
		double xValue;

		for (LineDescription line : lines) {
			double freq = line.getFreqCompute();

			if (line.isDoubleSideBand())
				xValue = Formula.convertFreqWithLoFreq(freq, xaxis.getLoFreq());
			else
				xValue = freq;

			double vlsr;
			if (TypeFrequency.SKY.equals(xAxis.getTypeFrequency()))
				vlsr = 0;
			else
				vlsr = line.getVlsrData();
			xaxis.setVlsr(vlsr);

			xValue = xaxis.convertFromMhzFreq(xValue);

			add(xValue, Double.NaN, false);
		}
		fireSeriesChanged();
	}

	/**
	 * Return if the series use the rendering value to plot/add the points.
	 * This series type always return false in that case.
	 *
	 * @return false
	 * @see eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassis#useRenderingToPlotPoints()
	 */
	@Override
	public boolean useRenderingToPlotPoints() {
		return false;
	}
}
