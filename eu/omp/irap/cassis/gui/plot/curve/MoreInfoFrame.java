/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.curve;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.file.FileManagerVOTable;
import eu.omp.irap.cassis.file.gui.datalink.DatalinkPanel;
import eu.omp.irap.cassis.file.gui.datalink.ResourcePanel;
import eu.omp.irap.cassis.file.gui.datalink.RessourceDatalinkAction;
import eu.omp.irap.cassis.file.gui.fileinfo.FileInfoFrame;
import eu.omp.irap.cassis.file.gui.medatada.CASSIS_METADATA;
import eu.omp.irap.cassis.file.gui.medatada.MetadataPanel;

/**
 * Display information about a {@link CommentedSpectrum}.
 *
 * @author Lea Foissac
 */
public class MoreInfoFrame extends JFrame {

	private static final long serialVersionUID = -4980750460370253854L;

	private CommentedSpectrum commentedSpectrum;
	private MetadataPanel metadataPanel;
	private JButton leftSpectrumInfoButton;
	private JButton rightSpectrumInfoButton;
	private List<CassisMetadata> cassisMetadataList;
	private List<CassisMetadata> originalMetadataList;

	private List<ResourcePanel> resourcePanelList;


	/**
	 * Create a {@link MoreInfoFrame} using a {@link CommentedSpectrum}.
	 *
	 * @param commentedSpectrum The {@link CommentedSpectrum}
	 */
	public MoreInfoFrame(CommentedSpectrum commentedSpectrum) {
		super("More Info");
		this.commentedSpectrum = commentedSpectrum;
		this.cassisMetadataList = commentedSpectrum.getCassisMetadataList();
		this.originalMetadataList = commentedSpectrum.getOriginalMetadataList();
		initialize();
	}

	/**
	 * Frame initialization.
	 */
	private void initialize() {
		setContentPane(getMetadataPanel());
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setSize(600, 500);
	}

	/**
	 * Create a {@link MetadataPanel}. It contains 3 tabs to display:
	 * - the cassis metadata
	 * - the original metadata
	 * - the datalink if it exists
	 *
	 * @return The {@link MetadataPanel} containing the 3 tabs
	 */
	public MetadataPanel getMetadataPanel() {
		if (metadataPanel == null) {
			metadataPanel = new MetadataPanel();
			metadataPanel.getCassisMetadataPanel().remove(metadataPanel.getCassisMetadataPanel().getButtonPanel());
			metadataPanel.getCassisMetadataPanel().add(getLeftSpectrumInfoButton(), BorderLayout.SOUTH);
			metadataPanel.getOriginalMetadataPanel().add(getRightSpectrumInfoButton(), BorderLayout.SOUTH);
			metadataPanel.getCassisMetadataControl().getModel().getTableModel().setCellEditable(false);
			metadataPanel.getCassisMetadataControl().getModel().getTableModel().addMetadataList(cassisMetadataList);
			metadataPanel.getOriginalMetadataControl().getModel().getTableModel().addMetadataList(originalMetadataList);
			addDatalink();
		}
		return metadataPanel;
	}

	/**
	 * Add on the "Datalink" tab, datalink information if it exists.
	 */
	private void addDatalink() {
		String path = "";
		int index = 0;
		if (cassisMetadataList != null) {
			for (CassisMetadata cassisMetadata : cassisMetadataList) {
				if (cassisMetadata.getName().equals(CASSIS_METADATA.SPECTRUM_INDEX.getName())) {
					index = Integer.valueOf(cassisMetadata.getValue());
				}
				if (cassisMetadata.getName().equals(CASSIS_METADATA.RESOURCE_INDEX.getName())) {
					index = Integer.valueOf(cassisMetadata.getValue());
				}
				if (cassisMetadata.getName().equals(CASSIS_METADATA.VOTABLE_RESOURCE.getName())) {
					path = cassisMetadata.getValue();
					break;
				} else if (cassisMetadata.getName().equals(CASSIS_METADATA.FROM.getName())) {
					path = cassisMetadata.getValue();
				}
			}
			if (FileManagerVOTable.containDatalink(path)) {
				DatalinkPanel datalinkPanel = new DatalinkPanel(path, index);
				metadataPanel.getDatalinkPanel().add(datalinkPanel);
				resourcePanelList = datalinkPanel.getResourcePanelList();
				metadataPanel.getTabbedPane().setEnabledAt(2, true);
			}
		}
	}

	/**
	 * Create a Spectrum info {@link JButton} for the right tab.
	 *
	 * @return The Spectrum info {@link JButton}
	 */
	public JButton getRightSpectrumInfoButton() {
		if (rightSpectrumInfoButton == null) {
			rightSpectrumInfoButton = new JButton("Spectrum info");
			rightSpectrumInfoButton.setEnabled(true);
			rightSpectrumInfoButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					JFrame fif = new FileInfoFrame(commentedSpectrum);
					fif.setVisible(true);
				}
			});
		}
		return rightSpectrumInfoButton;
	}

	/**
	 * Create a Spectrum info {@link JButton} for the left tab.
	 *
	 * @return The Spectrum info {@link JButton}
	 */
	public JButton getLeftSpectrumInfoButton() {
		if (leftSpectrumInfoButton == null) {
			leftSpectrumInfoButton = new JButton("Spectrum info");
			leftSpectrumInfoButton.setEnabled(true);
			leftSpectrumInfoButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					JFrame fif = new FileInfoFrame(commentedSpectrum);
					fif.setVisible(true);
				}
			});
		}
		return leftSpectrumInfoButton;
	}

	public void removeDataLinkAction(String name) {
		if (resourcePanelList != null && !resourcePanelList.isEmpty()) {
			ResourcePanel resourcePanel;
			for (Iterator<ResourcePanel> iterator = resourcePanelList.iterator(); iterator.hasNext();) {
				resourcePanel = iterator.next();
				resourcePanel.getControl().removeRessourceDatalinkAction(name);
			}
		}
	}

	public List<ResourcePanel> getResourcePanelList() {
		return resourcePanelList;
	}

	public void addDataLinkAction(RessourceDatalinkAction ressourceDatalinkAction) {
		if (resourcePanelList != null && !resourcePanelList.isEmpty()) {
			ResourcePanel resourcePanel;
			for (Iterator<ResourcePanel> iterator = resourcePanelList.iterator(); iterator.hasNext();) {
				resourcePanel = iterator.next();
				resourcePanel.getControl().addRessourceDatalinkAction(ressourceDatalinkAction);
			}
		}

	}
}
