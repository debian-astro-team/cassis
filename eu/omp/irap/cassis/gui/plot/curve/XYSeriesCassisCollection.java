/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.curve;

import java.awt.Dimension;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.jfree.data.UnknownKeyException;
import org.jfree.data.general.DatasetChangeEvent;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.file.FileManagerVOTable;
import eu.omp.irap.cassis.gui.plot.save.SaveGraphic;
import eu.omp.irap.cassis.gui.util.ExtentedJComboBox;

/**
 * XYSeriesCollection with additional functions to handle XYSeriesCassis.
 *
 * @author glorian
 * @author M. Boiziot
 */
public class XYSeriesCassisCollection extends XYSeriesCollection {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(XYSeriesCassisCollection.class);
	private Rendering rendering;


	/**
	 * Constructor. Create an empty collection.
	 */
	public XYSeriesCassisCollection() {
		super();
	}

	/**
	 * Constructor. Create a collection with the given series.
	 *
	 * @param series The series.
	 */
	public XYSeriesCassisCollection(XYSeriesCassis series) {
		super(series);
	}

	/**
	 * Set the given x axis on all series in the collection.
	 *
	 * @param xAxisCassis The x axis.
	 */
	public void setXAxis(XAxisCassis xAxisCassis) {
		for (int cpt = 0; cpt < this.getSeriesCount(); cpt++) {
			this.getSeries(cpt).setXAxis(xAxisCassis);
		}
	}

	@Override
	public XYSeriesCassis getSeries(@SuppressWarnings("rawtypes") Comparable key) {
		XYSeriesCassis xySeriesCassis = null;
		try {
			xySeriesCassis=  (XYSeriesCassis) super.getSeries(key);
		} catch (UnknownKeyException e) {
			LOGGER.debug("The series with key {} is unknown", key, e);
		}
		return xySeriesCassis;
	}

	/**
	 * Save a series in a VOTable format and show a selecting menu if there are more than one series.
	 *
	 * @param file The file used for the save.
	 * @return The state of the operation:
	 * <ul>
	 * <li>SaveUtil.SUCCESS for success.</li>
	 * <li>SaveUtil.ERROR for error.</li>
	 * <li>SaveUtil.CANCELED if the operation was canceled by the user.</li>
	 * <li>SaveUtil.NOTHING_TO_SAVE if there is no data to send through samp.</li>
	 * </ul>
	 */
	public  int saveForSamp(File file) {
		if (getSeriesCount() == 0) {
			return SaveGraphic.NOTHING_TO_SAVE;
		} else if (getSeriesCount() == 1) {
			FileManagerVOTable fileManagerVOTable = new FileManagerVOTable(file);
			fileManagerVOTable.save(file, ((XYSpectrumSeries)getSeries(0)).getSpectrum());
			return SaveGraphic.SUCCESS;
		} else {
			return saveMultiSpectrumForSamp(file);
		}
	}

	/**
	 * Open a {@link JOptionPane} for selecting a spectrum to save for samp
	 *  then save it.
	 *
	 * @param file The file to save.
	 * @return The state of the operation:
	 * <ul>
	 * <li>SaveUtil.SUCCESS for success.</li>
	 * <li>SaveUtil.ERROR for error.</li>
	 * <li>SaveUtil.CANCELED if the operation was canceled by the user.</li>
	 * </ul>
	 */
	private int saveMultiSpectrumForSamp(File file) {
		int seriesCount = getSeriesCount();
		String[] seriesName = new String[seriesCount];
		for (int i = 0; i < seriesCount; i++) {
			seriesName[i] = (String) getSeries(i).getKey();
		}
		JComboBox<String> jComboBox = new ExtentedJComboBox<>(seriesName);
		JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(350, 70));
		panel.add(jComboBox);
		jComboBox.setPreferredSize(new Dimension(200, 24));
		int reply = JOptionPane.showConfirmDialog(null, panel,
				"Please select the spectrum to send through samp.",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);

		if (reply != JOptionPane.OK_OPTION) {
			return SaveGraphic.CANCELED;
		} else {
			String selectedCurveName = jComboBox.getSelectedItem().toString();
			for (XYSeriesCassis serie : getSeries()) {
				if (serie.getKey().equals(selectedCurveName)) {
					FileManagerVOTable fileManagerVOTable = new FileManagerVOTable(file);
					fileManagerVOTable.save(file, ((XYSpectrumSeries)serie).getSpectrum());
					return SaveGraphic.SUCCESS;
				}
			}
		}
		return SaveGraphic.ERROR;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<XYSeriesCassis> getSeries() {
		return super.getSeries();
	}

	@Override
	public XYSeriesCassis getSeries(int series) {
		return (XYSeriesCassis) super.getSeries(series);
	}

	/**
	 * Compute then return the range (Y) lowest positive value.
	 *
	 * @return the range (Y) lowest positive value, or Double.NaN if there is no value..
	 */
	public double getLowestPositiveValueRange() {
		double min = Double.MAX_VALUE;
		for (XYSeriesCassis series : getSeries()) {
			if (TypeCurve.isSpectrum(series.getTypeCurve())) {
				for (int i = 0; i < series.getItemCount(); i++) {
					double d = series.getY(i).doubleValue();
					if (d > 0 && d < min) {
						min = d;
					}
				}
			}
		}
		return min == Double.MAX_VALUE ? Double.NaN : min;
	}

	/**
	 * Compute then return the domain (X) lowest positive value.
	 *
	 * @return the domain (X) lowest positive value, or Double.NaN if there is no value.
	 */
	public double getLowestPositiveValueDomain() {
		double min = Double.MAX_VALUE;
		for (XYSeriesCassis series : getSeries()) {
			if (!series.getConfigCurve().isVisible()) {
				continue;
			}
			for (int i = 0; i < series.getItemCount(); i++) {
				double d = series.getX(i).doubleValue();
				if (d > 0 && d < min) {
					min = d;
					break;
				}
			}
		}
		return min == Double.MAX_VALUE ? Double.NaN : min;
	}

	/**
	 * Change the rendering of the collections and series inside the collection.
	 *
	 * @param newRendering The new Rendering to set.
	 */
	public void setRendering(Rendering newRendering) {
		boolean change = this.rendering != newRendering;
		if (change) {
			this.rendering = newRendering;
			for (XYSeriesCassis series : getSeries()) {
				series.setRendering(rendering);
			}
			fireDatasetChanged();
		}
	}

	/**
	 * Set the proper rendering type to the series then adds a series to the
	 *  collection and sends a {@link DatasetChangeEvent} toall registered listeners.
	 *
	 * @param series the series (<code>null</code> not permitted).
	 * @throws IllegalArgumentException if the key for the series is null or not
	 *  unique within the dataset.
	 */
	@Override
	public void addSeries(XYSeries series) {
		if (series != null && series instanceof XYSeriesCassis) {
			XYSeriesCassis s = (XYSeriesCassis) series;
			s.setRendering(rendering);
		}
		super.addSeries(series);
	}

	/**
	 * Check and return if the collection contains a series with the given name.
	 *
	 * @param name The name to search
	 * @return if the collection contains the series, false otherwise
	 */
	public boolean containsSeries(String name) {
		for (XYSeriesCassis series : getSeries()) {
			if (series.getKey().equals(name)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Add a list of series.
	 *
	 * @param seriesList The list of series to add.
	 */
	public void addSeriesList(List<XYSeriesCassis> seriesList) {
		for (XYSeries series : seriesList) {
			addSeries(series);
		}
	}

	/**
	 * Remove all series.
	 * Override to remove listeners on the series because when JFreeChart do it,
	 *  it is not working and create a memory leak.
	 *
	 * @see org.jfree.data.xy.XYSeriesCollection#removeAllSeries()
	 */
	@Override
	public void removeAllSeries() {
		List<XYSeriesCassis> seriesList = new ArrayList<>(getSeries());
		super.removeAllSeries();
		for (XYSeriesCassis series : seriesList) {
			series.removeChangeListener(this);
			series.removeVetoableChangeListener(this);
		}
	}
}
