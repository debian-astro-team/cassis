/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.curve;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import eu.omp.irap.cassis.gui.plot.curve.config.ConfigCurve;

@SuppressWarnings("serial")
public class CurvePanelView extends JPanel {

	private static final int MAX_CURVE_NAME = 28;

	private JButton buttonColor;
	private JCheckBox checkBox;
	private boolean displayCurveOptions;
	private CurvePanelControl control;
	private CurvePanelModel model;
	private JButton moreInfoButton;


	public CurvePanelView(CurvePanelModel model) {
		this(model, true);
	}

	public CurvePanelView(CurvePanelModel model, boolean displayCurveOptions) {
		this(model, displayCurveOptions, false, true);
	}

	/**
	 * Constructor.
	 *
	 * @param model The model
	 * @param displayCurveOptions true to display curve options, false otherwise.
	 * @param mosaicControl true if displayed in mosaic, false otherwise
	 * @param displayMoreInfoButton true to display MoreInfoButton, false otherwise.
	 */
	public CurvePanelView(CurvePanelModel model, boolean displayCurveOptions,
			boolean mosaicControl, boolean displayMoreInfoButton) {
		super(new FlowLayout(FlowLayout.LEFT));
		if (mosaicControl)
			this.control = new CurvePanelMosaicControl(model, this);
		else
			this.control = new CurvePanelControl(model, this);
		this.displayCurveOptions = displayCurveOptions;
		this.model = model;
		add(getCheckBox());
		add(getButtonColor(this));

		if (getNameFromModel().length() > MAX_CURVE_NAME) {
			String tmpName = getNameFromModel();
			String[] split = tmpName.split(Pattern.quote(File.separator));
			tmpName = split[split.length-1];
			String name = tmpName;
			if (tmpName.length() > MAX_CURVE_NAME) {
				name = tmpName.substring(0, MAX_CURVE_NAME - 4) + "..." ;
			}
			JLabel jLabel = new JLabel(name);
			jLabel.setToolTipText(getNameFromModel());
			add(jLabel);
		} else {
			JLabel jLabel = new JLabel(getNameFromModel());
			jLabel.setToolTipText(getNameFromModel());
			add(jLabel);
		}
		if (displayMoreInfoButton) {
			add(getMoreInfoButton());
		}
	}

	public boolean isDisplayCurveOptions() {
		return displayCurveOptions;
	}

	/**
	 * @return the control
	 */
	public final CurvePanelControl getControl() {
		return control;
	}

	public JCheckBox getCheckBox() {
		if (checkBox == null) {
			checkBox = new JCheckBox("", model.getCassisModel().getConfigCurve().isVisible());
			checkBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.handleCheckBoxAction();
				}
			});
		}
		return checkBox;
	}

	protected JButton getButtonColor(final JPanel panel) {
		if (buttonColor == null) {
			buttonColor = new JButton();
			buttonColor.setBackground(control.getModel().getCassisModel().getConfigCurve().getColor());

			buttonColor.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					XYSeriesCassis seriesCassis = control.getModel().getCassisModel();
					JOptionPaneCurve optionPane = displayOptionPane(panel, seriesCassis);

					Object val = optionPane.getValue();
					if ("OK".equals(val)) {
						useNewColor(seriesCassis, optionPane);
					}
				}
			});
		}
		return buttonColor;
	}

	/**
	 * Use new color if possible, validate changes and fire an event.
	 *
	 * @param seriesCassis The series who change color.
	 * @param optionPane The option pane to validate and get color from.
	 */
	private void useNewColor(XYSeriesCassis seriesCassis,
			JOptionPaneCurve optionPane) {
		Color newColor = optionPane.getColor();
		if (newColor != null) {
			seriesCassis.getConfigCurve().setColor(newColor);
			buttonColor.setBackground(newColor);
		}

		optionPane.validateParameters();
		control.fireCurveCassisChanged(control.getModel());
	}

	/**
	 * Display the option pane.
	 *
	 * @param panel The panel.
	 * @param seriesCassis The series for which we display the option pane.
	 * @return The option pane.
	 */
	private JOptionPaneCurve displayOptionPane(final JPanel panel,
			XYSeriesCassis seriesCassis) {
		JOptionPaneCurve optionPane = new JOptionPaneCurve(
				seriesCassis.getConfigCurve(), isDisplayCurveOptions(), true);
		optionPane.setColor(buttonColor.getBackground());
		JDialog dialog = optionPane.createDialog(panel, "Change the curve parameters ");
		dialog.setVisible(true);
		return optionPane;
	}

	public JButton getButtonColor() {
		return buttonColor;
	}

	/**
	 * @param control
	 *            the control to set
	 */
	public final void setControl(CurvePanelControl control) {
		this.control = control;
		refresh();
	}

	public void refresh() {
		ConfigCurve configCurve = control.getModel().getCassisModel().getConfigCurve();
		getCheckBox().setSelected(configCurve.isVisible());
		getButtonColor(this).setBackground(configCurve.getColor());
	}

	public void addCurveCassisListener(CurveCassisListener curveCassisListener) {
		control.addCurveCassisListener(curveCassisListener);
	}

	public void removeCurveCassisListener(CurveCassisListener curveCassisListener) {
		control.removeCurveCassisListener(curveCassisListener);
	}

	/**
	 * @return the model
	 */
	public final CurvePanelModel getModel() {
		return model;
	}

	public String getNameFromModel() {
		return (String) control.getModel().getCassisModel().getKey();
	}

	/**
	 * Getter for the More Info button
	 *
	 * @return The button named "..."
	 */
	public JButton getMoreInfoButton() {
		if (moreInfoButton == null) {
			moreInfoButton = new JButton(" ... ");
			moreInfoButton.setOpaque(true);
			moreInfoButton.setBorder(BorderFactory.createLineBorder(Color.black, 1));
			moreInfoButton.setVisible(false);
		}
		return moreInfoButton;
	}

	/**
	 * Return if the More Info button exist.
	 *
	 * @return true if the More Info button exist, false otherwise.
	 */
	public boolean haveMoreInfoButton() {
		return moreInfoButton != null;
	}
}
