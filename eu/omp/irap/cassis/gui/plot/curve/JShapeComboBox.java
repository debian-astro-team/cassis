/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.curve;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import eu.omp.irap.cassis.gui.plot.curve.config.ShapeCassis;

/**
 * @author glorian
 *
 */
@SuppressWarnings("serial")
public class JShapeComboBox extends JComboBox<Integer> {

	private ShapeCassis shape = ShapeCassis.NONE;


	/**
	 * Constructor.
	 */
	public JShapeComboBox() {
		super(new Integer[] { ShapeCassis.NONE.ordinal(), ShapeCassis.CROSS.ordinal(), ShapeCassis.ROUND.ordinal(),
				ShapeCassis.SQUARE.ordinal(), ShapeCassis.TRIANGLE.ordinal() });
		ItemStyleComboBoxRenderer<Integer> renderer = new ItemStyleComboBoxRenderer<>();
		renderer.setPreferredSize(new Dimension(35, 20));

		this.setRenderer(renderer);
	}

	class ItemStyleComboBoxRenderer<E> extends JLabel implements ListCellRenderer<E> {

		private static final long serialVersionUID = 1L;

		public ItemStyleComboBoxRenderer() {
			setOpaque(true);
			setHorizontalAlignment(CENTER);
			setVerticalAlignment(CENTER);
		}

		/**
		 * This method finds the image and text corresponding to the selected
		 * value and returns the label, set up to display the text and image.
		 */
		@Override
		public Component getListCellRendererComponent(JList<? extends E> list, E value, int index, boolean isSelected,
				boolean cellHasFocus) {
			// Get the selected index. (The index param isn't
			// always valid, so just use the value.)
			int selectedIndex = ((Integer) value).intValue();

			if (isSelected) {
				setBackground(list.getSelectionBackground());
				setForeground(list.getSelectionForeground());
			}
			else {
				setBackground(list.getBackground());
				setForeground(list.getForeground());
			}
			Shape cShape = getShape(selectedIndex);

			BufferedImage image = new BufferedImage(35, 15, BufferedImage.TYPE_3BYTE_BGR);
			Graphics2D g2 = image.createGraphics();
			g2.setColor(Color.WHITE);
			g2.fillRect(0, 0, 35, 15);
			g2.setColor(Color.BLACK);
			if (cShape == null) {
				g2.setFont(new Font("Dialog", Font.PLAIN, 10));
				g2.drawString(" NONE", 1, 10);
			}
			else
				g2.draw(cShape);
			image.flush();
			ImageIcon icon = new ImageIcon(image);
			this.setIcon(icon);

			return this;
		}
	}

	public Shape getShape(int selectedIndex) {
		if (selectedIndex == ShapeCassis.CROSS.ordinal()) {
			this.shape = ShapeCassis.CROSS;
		}
		else if (selectedIndex == ShapeCassis.ROUND.ordinal()) {
			this.shape = ShapeCassis.ROUND;
		}
		else if (selectedIndex == ShapeCassis.SQUARE.ordinal()) {
			this.shape = ShapeCassis.SQUARE;
		}
		else {
			this.shape = ShapeCassis.NONE;
		}
		return this.shape.getIcon();
	}

	public ShapeCassis getShapeCassis() {
		return shape;
	}

	public void setShape(ShapeCassis shape) {
		this.shape = shape;
		this.setSelectedItem(shape.ordinal());
	}
}
