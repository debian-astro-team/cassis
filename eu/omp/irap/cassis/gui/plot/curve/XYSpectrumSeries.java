/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.curve;

import java.util.List;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.TypeFrequency;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.X_AXIS;
import eu.omp.irap.cassis.common.axes.YAxisCassis;

@SuppressWarnings("serial")
public class XYSpectrumSeries extends XYSeriesCassis {

	protected CommentedSpectrum spectrum;


	public XYSpectrumSeries(String title, XAxisCassis xAxis, TypeCurve typeCurve, CommentedSpectrum commentedSpectrum) {
		super(title, typeCurve);
		this.spectrum = commentedSpectrum;
		setXAxis(xAxis);
		if (TypeCurve.DATA.equals(typeCurve)) {
			setToFit(true);
		}
	}

	public XYSpectrumSeries(String title, XAxisCassis xAxis, YAxisCassis yAxis, TypeCurve typeCurve,
			CommentedSpectrum commentedSpectrum) {
		super(title, typeCurve);
		this.spectrum = commentedSpectrum;
		setAxis(xAxis, yAxis);
		if (TypeCurve.DATA.equals(typeCurve)) {
			setToFit(true);
		}
	}

	@Override
	public List<LineDescription> getListOfLines() {
		return this.spectrum.getListOfLines();
	}

	@Override
	protected void buildXYSeries(XAxisCassis xaxis, YAxisCassis yaxis) {
		this.clear();
		if (spectrum == null || spectrum.getSize() == 0 ||(X_AXIS.UNKNOWN.equals(xaxis.getAxis())))
			return;

		if (TypeFrequency.SKY.equals(spectrum.getTypeFreq())) {
			xaxis.setVlsr(0.);
		} else {
			xaxis.setVlsr(spectrum.getVlsr());
		}


		if (spectrum.getSize() == 1) {
			double freqMhz = spectrum.getFrequenciesSignal()[0];
			this.add(xaxis.convertFromMhzFreq(freqMhz).doubleValue(),
					YAxisCassis.convert(spectrum.getIntensities()[0], spectrum.getyAxis(), yaxis, freqMhz,
							spectrum.getCassisMetadataList()));
		} else if (rendering == Rendering.HISTOGRAM) {
				addHistogramValues(xaxis, yaxis, spectrum.getCassisMetadataList());
		} else if (rendering == Rendering.DOT || rendering == Rendering.LINE) {
				addDotValues(xaxis, yaxis, spectrum.getCassisMetadataList());
		}
	}

	/**
	 * Create the points for histogram rendering. It <b>MUST</b> have more than 1 point.
	 *
	 * @param xaxis The X Axis.
	 * @param yaxis THe Y Axis
	 * @param metas the list of metadatas
	 * @param list
	 */
	private void addHistogramValues(XAxisCassis xaxis, YAxisCassis yaxis, List<CassisMetadata> metas) {
		double xVal = 0;
		double deltaVal = 0;
		double intensity = 0;
		double[] intensities = spectrum.getIntensities();
		double[] frequencies = spectrum.getFrequenciesSignal();
		int size = spectrum.getSize();

		if (xaxis.isInverted()) {
			xVal = xaxis.convertFromMhzFreq(frequencies[size-1]);
			deltaVal = xaxis.convertDeltaFromMhzFreq(frequencies[size-1], frequencies[size-2]);
			intensity = intensities[size-1];
			intensity = YAxisCassis.convert(intensity, spectrum.getyAxis(), yaxis, frequencies[size-1], metas);
			this.add(xVal - deltaVal, intensity, false);

			for (int i = size-1; i > 0; i--) {
				xVal = xaxis.convertFromMhzFreq(frequencies[i]);
				deltaVal = xaxis.convertDeltaFromMhzFreq(frequencies[i], frequencies[i-1]);
				intensity = intensities[i-1];
				intensity = YAxisCassis.convert(intensity, spectrum.getyAxis(), yaxis, frequencies[i], metas);
				this.add(xVal + deltaVal, intensity, false);
			}

			xVal = xaxis.convertFromMhzFreq(frequencies[0]);
			deltaVal = xaxis.convertDeltaFromMhzFreq(frequencies[1], frequencies[0]);
			intensity = intensities[0];
			intensity = YAxisCassis.convert(intensity, spectrum.getyAxis(), yaxis, frequencies[0], metas);
			this.add(xVal + deltaVal, intensity);
		} else {
			xVal = xaxis.convertFromMhzFreq(frequencies[0]);
			deltaVal = xaxis.convertDeltaFromMhzFreq(frequencies[1], frequencies[0]);
			intensity = intensities[0];
			intensity = YAxisCassis.convert(intensity, spectrum.getyAxis(), yaxis, frequencies[0], metas);
			this.add(xVal - deltaVal, intensity, false);

			for (int i = 0; i< size-1; i++) {
				xVal = xaxis.convertFromMhzFreq(frequencies[i]);
				deltaVal = xaxis.convertDeltaFromMhzFreq(frequencies[i], frequencies[i+1]);
				intensity = intensities[i+1];
				intensity = YAxisCassis.convert(intensity, spectrum.getyAxis(), yaxis, frequencies[i], metas);
				this.add(xVal + deltaVal, intensity, false);
			}

			xVal = xaxis.convertFromMhzFreq(frequencies[size-1]);
			deltaVal = xaxis.convertDeltaFromMhzFreq(frequencies[size-1], frequencies[size-2]);
			intensity = intensities[size-1];
			intensity = YAxisCassis.convert(intensity, spectrum.getyAxis(), yaxis, frequencies[size-1], metas);
			this.add(xVal + deltaVal, intensity);
		}
	}

	/**
	 * Create the points for dot/line rendering.
	 *
	 * @param xaxis The X Axis.
	 * @param yaxis The Y Axis
	 * @param list
	 */
	private void addDotValues(XAxisCassis xaxis, YAxisCassis yaxis, List<CassisMetadata> metas) {
		double xVal = 0;
		double intensity = 0;
		double[] intensities = spectrum.getIntensities();
		double[] frequencies = spectrum.getFrequenciesSignal();
		int size = spectrum.getSize();
		if (xaxis.isInverted()) {
			for (int i = size-1; i > 0; i--) {
				xVal = xaxis.convertFromMhzFreq(frequencies[i]);
				intensity = intensities[i];
				intensity = YAxisCassis.convert(intensity, spectrum.getyAxis(), yaxis, frequencies[i], metas);
				this.add(xVal, intensity, false);
			}
			xVal = xaxis.convertFromMhzFreq(frequencies[0]);
			intensity = intensities[0];
			intensity = YAxisCassis.convert(intensity, spectrum.getyAxis(), yaxis, frequencies[0], metas);
			this.add(xVal, intensity);
		} else {
			for (int i = 0; i < size-1; i++) {

				xVal = xaxis.convertFromMhzFreq(frequencies[i]);
				intensity = intensities[i];
				intensity = YAxisCassis.convert(intensity, spectrum.getyAxis(), yaxis, frequencies[i], metas);
				this.add(xVal, intensity, false);
			}
			xVal = xaxis.convertFromMhzFreq(frequencies[size - 1]);
			intensity = intensities[size-1];
			intensity = YAxisCassis.convert(intensity, spectrum.getyAxis(), yaxis, frequencies[size - 1], metas);
			this.add(xVal, intensity);
		}
	}

	/**
	 * @return the spectrum
	 */
	public final CommentedSpectrum getSpectrum() {
		return spectrum;
	}

	/**
	 * Return if the series use the rendering value to plot/add the points.
	 * This series type always return true in that case.
	 *
	 * @return true
	 * @see eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassis#useRenderingToPlotPoints()
	 */
	@Override
	public boolean useRenderingToPlotPoints() {
		return true;
	}
}
