/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.curve;

import org.jfree.data.xy.XYSeries;

/**
 * @author glorian
 *
 */
public enum TypeCurve {

	DATA, LINE, LINE_ERROR, LINE_NEARBY, FIT, FIT_RESIDUAL, FIT_COMPO,
	OTHER_SPECIES_SIGNAL, OTHER_SPECIES_IMAGE, SYNTHETIC, OVERLAY_DATA,
	OVERLAY_LINELIST, RESULT, PLOT_NUMBER, LOOMIS_LINE, MARKERS, UNKNOWN;


	public boolean isFit() {
		return TypeCurve.FIT.equals(this) || TypeCurve.FIT_RESIDUAL.equals(this)
				|| TypeCurve.FIT_COMPO.equals(this);
	}

	public static boolean sameType(TypeCurve typeCurveSeries, TypeCurve typeCurveToCompare) {
		if (typeCurveSeries == typeCurveToCompare) {
			return true;
		}
		else if (typeCurveToCompare == TypeCurve.FIT) {
			return typeCurveSeries == TypeCurve.FIT_COMPO;
		}
		return false;
	}

	public static boolean isSavable(XYSeriesCassis seriesCassis) {
		TypeCurve typeCurve = seriesCassis.getTypeCurve();
		return typeCurve == TypeCurve.DATA || typeCurve == TypeCurve.FIT ||
				typeCurve == TypeCurve.FIT_RESIDUAL ||
				typeCurve == TypeCurve.SYNTHETIC || typeCurve == TypeCurve.RESULT;
	}

	public static boolean isLineCurve(TypeCurve typeCurveSeries) {
		return typeCurveSeries == TypeCurve.OTHER_SPECIES_IMAGE ||
				typeCurveSeries == TypeCurve.OTHER_SPECIES_SIGNAL
				|| typeCurveSeries == TypeCurve.LINE ||
				typeCurveSeries == TypeCurve.LINE_ERROR ||
				typeCurveSeries == TypeCurve.LINE_NEARBY;
	}

	public static boolean isSavable(XYSeries series) {
		if (series instanceof XYSpectrumSeries) {
			XYSpectrumSeries serie = (XYSpectrumSeries) series;
			return TypeCurve.isSavable(serie);
		}
		return false;
	}

	/**
	 * Return if a TypeCurve is a spectrum (not line).
	 *
	 * @param tc The TypeCurve
	 * @return true if the TypeCurve is a spectrum, false otherwise.
	 */
	public static boolean isSpectrum(TypeCurve tc) {
		return tc == TypeCurve.DATA || tc == TypeCurve.FIT || tc == TypeCurve.FIT_RESIDUAL
				|| tc == TypeCurve.FIT_COMPO || tc == TypeCurve.SYNTHETIC
				|| tc == TypeCurve.OVERLAY_DATA || tc == TypeCurve.RESULT;
	}

	/**
	 * Return if this is an utility TypeCurve. (PLOT_NUMBER or MARKERS).
	 *
	 * @return true if this is an utility TypeCurve. (PLOT_NUMBER or MARKERS),
	 *  false otherwise.
	 */
	public boolean isUtility() {
		return this == TypeCurve.PLOT_NUMBER || this == TypeCurve.MARKERS;
	}
}
