/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.curve;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.X_AXIS;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.gui.plot.curve.config.ConfigCurve;

public abstract class XYSeriesCassis extends XYSeriesCassisCommon {

	private static final long serialVersionUID = -121780697514229319L;
	private static final Logger LOGGER = LoggerFactory.getLogger(XYSeriesCassis.class);

	protected XAxisCassis xAxis = XAxisCassis.getXAxisCassis(X_AXIS.UNKNOWN, UNIT.UNKNOWN);
	protected YAxisCassis yAxis = YAxisCassis.getYAxisCassis(UNIT.UNKNOWN.toString());
	private ConfigCurve configCurve = new ConfigCurve();
	private int id = -1;
	private boolean toFit = false;
	protected Rendering rendering;
	private String forcedPanelCurveTitle;


	protected XYSeriesCassis(String title, TypeCurve typeCurve) {
		super(title, typeCurve);
	}

	public abstract List<LineDescription> getListOfLines();

	protected abstract void buildXYSeries(XAxisCassis xaxis, YAxisCassis yaxis);

	/**
	 * Return if the series use the rendering value to plot/add the points.
	 *
	 * @return true if the series use the rendering value to plot/add the points.
	 */
	public abstract boolean useRenderingToPlotPoints();

	/**
	 * @return the xAxis
	 */
	public final XAxisCassis getXAxis() {
		return xAxis;
	}

	/**
	 * @param xaxis
	 *            the xAxis to set
	 */
	public final void setXAxis(XAxisCassis xaxis) {
		if (xaxis == null) {
			xaxis = XAxisCassis.getXAxisUnknown();
		} else if  (!xaxis.getAxis().equals(XAxisCassis.getXAxisUnknown().getAxis()) &&
				    !xAxis.equals(xaxis)) {
			try {
				xAxis = xaxis.clone();
				buildXYSeries(xaxis, getyAxis());
			} catch (CloneNotSupportedException e) {
				LOGGER.warn("Error while setting the new XAxis caused by a clone error.", e);
			}
		}
	}

	/**
	 * @param xaxis
	 * 			the xAxis to set
	 * @param yaxis
	 * 			the yAxis to set
	 */
	public final void setAxis(XAxisCassis xaxis, YAxisCassis yaxis) {
		if (!xAxis.equals(xaxis) || !yAxis.equals(yaxis)) {
			try {
				xAxis = xaxis.clone();
				yAxis = yaxis.clone();
				buildXYSeries(xaxis, yaxis);
			} catch (CloneNotSupportedException e) {
				LOGGER.warn("Error while setting the news X and Y Axis caused by a clone error.", e);
			}
		}
	}

	public final void setXAxis(XAxisCassis axis, boolean refresh) {
		try {
			xAxis = axis.clone();
			if (refresh) {
				buildXYSeries(axis, getyAxis());
			}
		} catch (CloneNotSupportedException e) {
			LOGGER.warn("Error while setting the new XAxis caused by a clone error.", e);
		}
	}

	/**
	 * @param yAxis
	 *            the yAxis to set
	 */
	public void setyAxis(YAxisCassis axis) {
		try {
			yAxis = axis.clone();
//			if (refresh)
			{
				buildXYSeries(getXAxis(), axis);
			}
		} catch (CloneNotSupportedException e) {
			LOGGER.warn("Error while setting the new XAxis caused by a clone error.", e);
		}
	}

	/**
	 * @return the yAxis
	 */
	public YAxisCassis getyAxis() {
		return yAxis;
	}

	/**
	 * @param configCurve
	 *            the configCurve to set
	 */
	public void setConfigCurve(ConfigCurve configCurve) {
		this.configCurve = configCurve;
	}

	/**
	 * @return the configCurve
	 */
	public ConfigCurve getConfigCurve() {
		return configCurve;
	}

	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public final void setId(int id) {
		this.id = id;
	}

	/**
	 * @param serieToFit : to know if the serie can be fitted
	 */
	public void setToFit(boolean serieToFit) {
		this.toFit = serieToFit;
	}

	/**
	 * @return the toFit
	 */
	public boolean isToFit() {
		return toFit;
	}

	public void rebuild() {
		this.buildXYSeries(xAxis, yAxis);
	}

	/**
	 * Change the rendering of the series and rebuild the series if needed.
	 *
	 * @param newRendering The new Rendering to set.
	 */
	public void setRendering(Rendering newRendering) {
		boolean change = rendering != newRendering;
		rendering = newRendering;
		if (change && useRenderingToPlotPoints()) {
			rebuild();
		}
	}

	/**
	 * Return the forced panel curve title. Should be only needed on FullSpectrum
	 *  for DATA series.
	 *
	 * @return the forced panel curve title
	 */
	public String getForcedPanelCurveTitle() {
		return forcedPanelCurveTitle;
	}

	/**
	 * Change the forced panel curve title.
	 *
	 * @param newForcedPanelCurveTitle The new forced panel curve title to set
	 */
	public void setForcedPanelCurveTitle(String newForcedPanelCurveTitle) {
		this.forcedPanelCurveTitle = newForcedPanelCurveTitle;
	}

}
