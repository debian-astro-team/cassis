/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.curve;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.lowagie.text.pdf.TextField;

import eu.omp.irap.cassis.gui.plot.curve.config.ConfigCurve;
import eu.omp.irap.cassis.gui.plot.curve.config.ShapeCassis;
import eu.omp.irap.cassis.gui.plot.curve.config.StrokeCassis;
import eu.omp.irap.cassis.gui.util.JColorChooserDebugged;

/**
 * @author glorian
 *
 */
@SuppressWarnings("serial")
public class JOptionPaneCurve extends JOptionPane {

	protected boolean displayCurveSettings = false;
	private boolean displayDotSettings = false;
	boolean isDisplayHeight = false;
	private JColorChooser colorChooser = new JColorChooserDebugged();
	protected JPanel panelCurveParameters;

	private JShapeComboBox comboStyleBox;
	private JStrokeComboBox comboLineStyleBox;

	private JPanel lineSylePanel;
	private JPanel itemSylePanel;
	private JPanel lineWidthPanel;

	private JPanel dotWidthPanel;
	private JPanel dotHeightPanel;

	private JFormattedTextField lineWidthTextField;
	private JFormattedTextField dotWidthTextField;
	private JFormattedTextField dotHeightTextField;

	private JPanel curveParamPanel = new JPanel(new BorderLayout());

	private ConfigCurve configCurve;

	public JOptionPaneCurve(ConfigCurve configCurve, boolean displayCurveSettings,
			boolean displayDotSettings) {
		super(null, JOptionPane.PLAIN_MESSAGE, JOptionPane.YES_NO_OPTION, null,
				new Object[] { "OK", "CANCEL" }, "OK");
		this.displayCurveSettings = displayCurveSettings;
		this.displayDotSettings = displayDotSettings;
		this.configCurve = configCurve;
		colorChooser.setColor(configCurve.getColor());
		colorChooser.setPreviewPanel(new JPanel());
		curveParamPanel.add(colorChooser, BorderLayout.CENTER);
		curveParamPanel.setBorder(BorderFactory.createTitledBorder("Choose your color"));
		setMessage(new Object[] { curveParamPanel, buildCurveParameters() });
	}

	private Component getLineWidthPanel() {
		if (lineWidthPanel == null) {
			lineWidthPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
			lineWidthPanel.add(new JLabel("Line Width :"));
			lineWidthPanel.add(getLineWidthTextField());
		}
		return lineWidthPanel;
	}

	private JComponent buildCurveParameters() {
		if (panelCurveParameters == null) {
			panelCurveParameters = new JPanel();
			panelCurveParameters.setBorder(BorderFactory.createTitledBorder("Other parameters"));
			panelCurveParameters.setLayout(new GridLayout(0, 2));
			panelCurveParameters.add(getLineStylePanel());
			panelCurveParameters.add(getLineWidthPanel());
			panelCurveParameters.add(getDotWidthPanel());
			panelCurveParameters.add(getDotHeightPanel());
			getItemStylePanel().setVisible(false);
		}
		return panelCurveParameters;
	}

	private Component getItemStylePanel() {
		if (itemSylePanel == null) {
			itemSylePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
			itemSylePanel.add(new JLabel("Item Style : "));
			comboStyleBox = new JShapeComboBox();
			itemSylePanel.add(comboStyleBox);
		}
		return itemSylePanel;
	}

	private Component getLineStylePanel() {
		if (lineSylePanel == null) {
			lineSylePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
			lineSylePanel.add(new JLabel("Line Style : "));
			comboLineStyleBox = new JStrokeComboBox();
			comboLineStyleBox.setStroke(configCurve.getStrokeCassis());
			lineSylePanel.add(comboLineStyleBox);
			comboLineStyleBox.setEnabled(displayCurveSettings);

		}
		return lineSylePanel;
	}

	public float getWidthCurve() {
		return configCurve.getSize();
	}

	public void setWidthCurve(float width) {
		configCurve.setSize(width);
		lineWidthTextField.setText(String.valueOf(width));
	}

	public void setColor(Color background) {
		colorChooser.setColor(background);
	}

	/**
	 * @return the color chosen
	 */
	public Color getColor() {
		return colorChooser.getColor();
	}

	public void setShape(ShapeCassis shape) {
		comboStyleBox.setShape(shape);
	}

	public void setStroke(StrokeCassis stroke) {
		comboLineStyleBox.setStroke(stroke);
	}

	public ShapeCassis getShapeCassis() {
		return comboStyleBox.getShapeCassis();
	}

	public StrokeCassis getStrokeCassis() {
		return comboLineStyleBox.getStrokeCassis();
	}

	public void validateWidth() {
		configCurve.setSize(Float.valueOf(lineWidthTextField.getText()));
	}

	/**
	 * Create if needed then return the Line Width {@link TextField}.
	 *
	 * @return the Line Width {@link TextField}.
	 */
	private JFormattedTextField getLineWidthTextField() {
		if (lineWidthTextField == null) {
			lineWidthTextField = new JFormattedTextField(new Double(1));
			lineWidthTextField.setColumns(5);
			lineWidthTextField.setText(Double.toString(configCurve.getSize()));
			lineWidthTextField.setEnabled(displayCurveSettings);
		}
		return lineWidthTextField;
	}

	/**
	 * Create if needed then return the Dot Width {@link TextField}.
	 *
	 * @return the Dot Width {@link TextField}.
	 */
	private JFormattedTextField getDotWidthTextField() {
		if (dotWidthTextField == null) {
			dotWidthTextField = new JFormattedTextField(configCurve.getDotWidthSize());
			dotWidthTextField.setColumns(5);
			dotWidthTextField.setEnabled(displayDotSettings);
		}
		return dotWidthTextField;
	}

	/**
	 * Create if needed then return the Dot Height {@link TextField}.
	 *
	 * @return the Dot Height {@link TextField}.
	 */
	private JFormattedTextField getDotHeightTextField() {
		if (dotHeightTextField == null) {
			dotHeightTextField = new JFormattedTextField(configCurve.getDotHeightSize());
			dotHeightTextField.setColumns(5);
			dotHeightTextField.setEnabled(displayDotSettings);
		}
		return dotHeightTextField;
	}

	/**
	 * Create if needed then return the Dot Width JPanel.
	 *
	 * @return the Dot Width JPanel.
	 */
	private Component getDotWidthPanel() {
		if (dotWidthPanel == null) {
			dotWidthPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
			dotWidthPanel.add(new JLabel("Dot Width :"));
			dotWidthPanel.add(getDotWidthTextField());
		}
		return dotWidthPanel;
	}

	/**
	 * Create if needed then return the Dot Height JPanel.
	 *
	 * @return the Dot Height JPanel.
	 */
	private Component getDotHeightPanel() {
		if (dotHeightPanel == null) {
			dotHeightPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
			dotHeightPanel.add(new JLabel("Dot Height :"));
			dotHeightPanel.add(getDotHeightTextField());
		}
		return dotHeightPanel;
	}

	/**
	 * Return the dot height size.
	 *
	 * @return the dot height size.
	 */
	public int getDotHeightSize() {
		return (int) getDotHeightTextField().getValue();
	}

	/**
	 * Return the dot width size.
	 *
	 * @return the dot width size.
	 */
	public int getDotWidthSize() {
		return (int) getDotWidthTextField().getValue();
	}

	/**
	 * Validate the parameters from the Current panel to the ConfigCurve.
	 */
	public void validateParameters() {
		configCurve.setSize(Float.valueOf(lineWidthTextField.getText()));
		configCurve.setShapeCassis(getShapeCassis());
		configCurve.setStrokeCassis(getStrokeCassis());
		configCurve.setDotHeightSize(getDotHeightSize());
		configCurve.setDotWidthSize(getDotWidthSize());
	}
}
