/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.curve;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import eu.omp.irap.cassis.common.CassisDecimalFormat;
import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.TypeFrequency;
import eu.omp.irap.cassis.file.gui.datalink.DisplayBrowserAction;
import eu.omp.irap.cassis.file.gui.datalink.ResourcePanel;
import eu.omp.irap.cassis.file.gui.datalink.RessourceDatalinkAction;
import eu.omp.irap.cassis.gui.menu.action.OpenAction;
import eu.omp.irap.cassis.gui.util.FileUtils;

@SuppressWarnings("serial")
public class JPanelCurve extends JPanel {

	private List<XYSeriesCassis> listCassisModel = new ArrayList<>();
	private List<CurvePanelView> listCurvePanelView = new ArrayList<>();
	private Map<String, JPanel> map = new HashMap<>(2);
	private JPanel noDataPanel;
	private JLabel title;
	private JButton deleteButton;
	private JPanel superPanel;
	private JPanel jPanelTitle;
	private JPanel jPanelButton;
	private boolean autoDestroy = false;


	public JPanelCurve(String title) {
		this(title, true);
	}

	public JPanelCurve(String title, boolean haveDeleteButton) {
		noDataPanel = new JPanel();
		noDataPanel.setLayout(new BoxLayout(noDataPanel, BoxLayout.Y_AXIS));
		setLayout(new BorderLayout());

		setTitle(title);

		jPanelTitle = new JPanel();
		if (haveDeleteButton) {
			deleteButton = new JButton("   X   ");
			deleteButton.setOpaque(true);
			deleteButton.setBorder(BorderFactory.createLineBorder(Color.black, 1));
			jPanelButton = new JPanel();
			jPanelTitle.setLayout(new BorderLayout());
			jPanelButton.add(deleteButton);
			jPanelTitle.add(this.title, BorderLayout.LINE_START);
			jPanelTitle.add(jPanelButton, BorderLayout.LINE_END);
		} else {
			jPanelTitle.setLayout(new BorderLayout());
			jPanelTitle.add(this.title, BorderLayout.LINE_START);
		}
		Box box = Box.createVerticalBox();

		box.add(jPanelTitle);
		box.add(noDataPanel);
		add(box, BorderLayout.NORTH);

		Border bordure = BorderFactory.createEtchedBorder();
		setBorder(bordure);
	}

	private void setTitle(String title) {
		if (this.title == null)
			this.title = new JLabel("<html><u>" + title + "</u></html>");
		else
			this.title.setText("<html><u>" + title + "</u></html>");
	}

	public void removeDeleteButton() {
		if (deleteButton != null) {
			jPanelButton.remove(deleteButton);
			deleteButton = null;
		}
	}

	public JButton getDeleteButton() {
		return deleteButton;
	}

	public void addCurvePane(CurvePanelView curvePanelView) {
		final XYSeriesCassis model = curvePanelView.getModel().getCassisModel();
		listCassisModel.add(model);
		listCurvePanelView.add(curvePanelView);

		JPanel panelTmp = new JPanel(new BorderLayout());

		panelTmp.add(curvePanelView, BorderLayout.WEST);

		if (model instanceof XYSpectrumSeries) {
			CommentedSpectrum cs = ((XYSpectrumSeries) model).getSpectrum();
			JPanel south = new JPanel(new BorderLayout());
			panelTmp.add(south, BorderLayout.SOUTH);
			if (!model.getTypeCurve().isFit() && !model.getTypeCurve().isUtility() && cs != null) {
				Double vlsr = cs.getVlsrModel();
				if (vlsr != null) {
					south.add(getVlsrPanel(vlsr), BorderLayout.EAST);
				}
				JPanel info = new JPanel();
				info.add(getTypeFrequencyPanel(cs.getTypeFreq()));
				info.add(getLoFrequencyPanel(cs.getLoFreq()));
				south.add(info, BorderLayout.CENTER);
			}
			if (curvePanelView.haveMoreInfoButton()) {
				boolean visible = !model.getTypeCurve().isUtility();
				if (visible) {
					addListenerForMoreInfoButton(curvePanelView, cs);
				}
				curvePanelView.getMoreInfoButton().setVisible(visible);
			}
		} else if (model instanceof XYLineSeriesCassis) {
			final XYLineSeriesCassis xyLineSeriesCassis = (XYLineSeriesCassis) model;
			if ((TypeCurve.OTHER_SPECIES_IMAGE.equals(xyLineSeriesCassis.getTypeCurve()) ||
					TypeCurve.OTHER_SPECIES_SIGNAL.equals(xyLineSeriesCassis.getTypeCurve()))
					&& (xyLineSeriesCassis.getListOfLines() != null
					&& !xyLineSeriesCassis.getListOfLines().isEmpty())) {
				double vlsr = xyLineSeriesCassis.getListOfLines().get(0).getVlsr();
				if (!Double.isNaN(vlsr)) {
					panelTmp.add(getVlsrPanel(vlsr), BorderLayout.EAST);
				}
			}
		}

		map.put(curvePanelView.getNameFromModel(), panelTmp);
		noDataPanel.add(panelTmp);
	}

	private Component getLoFrequencyPanel(double lo) {
		String loString = "unknow";
		if (!Double.isNaN(lo))
			loString = CassisDecimalFormat.FREQUENCY_GHZ_FORMAT.format(lo);
		return new JLabel("Lo [MHz] : " + loString);
	}

	private JComponent getTypeFrequencyPanel(TypeFrequency typeFreq) {
		return new JLabel("in : " + typeFreq.name());
	}

	private Component getVlsrPanel(Double vlsr) {
		return new JLabel("vlsr : " + CassisDecimalFormat.VLSR_FORMAT.format(vlsr) + "    ");
	}

	public void refresh() {
		for (CurvePanelView curvePanelView : listCurvePanelView) {
			curvePanelView.refresh();
		}
	}

	/**
	 * @return the listCassisModel
	 */
	public List<XYSeriesCassis> getListCassisModel() {
		return listCassisModel;
	}

	/**
	 * @return the superPanel
	 */
	public JPanel getSuperPanel() {
		return superPanel;
	}

	/**
	 * @param superPanel
	 *            the superPanel to set
	 */
	public void setSuperPanel(JPanel superPanel) {
		this.superPanel = superPanel;
	}

	public boolean getAutoDestroy() {
		return autoDestroy;
	}

	/**
	 * @param autoDestroy
	 *            the autoDestroy to set
	 */
	public final void setAutoDestroy(boolean autoDestroy) {
		this.autoDestroy = autoDestroy;
	}

	/**
	 * @return the listCurvePanelView
	 */
	public final List<CurvePanelView> getListCurvePanelView() {
		return listCurvePanelView;
	}

	@Override
	public String getName() {
		return title.getText().replaceAll("<html><u>", "").replaceAll("</u></html>", "");
	}

	/**
	 * Search for a CurvePanelView and return it if it exist.
	 *
	 * @param name The name of CurvePanelView to search.
	 * @return The CurvePanelView or null if it doesn't exist.
	 */
	private CurvePanelView getCurvePanelView(String name) {
		for (CurvePanelView cpv : listCurvePanelView) {
			if (cpv.getNameFromModel().equals(name)) {
				return cpv;
			}
		}
		return null;
	}

	/**
	 * Remove a CurvePanelView from the JPanelCurve if it exist. Warning: it
	 *  doesn't remove the associate listeners.
	 *
	 * @param cpvName The name of the CurvePanelView to remove.
	 * @return The CurvePanelView removed or null if it doesn't exit.
	 */
	public CurvePanelView removeCurvePanelView(String cpvName) {
		CurvePanelView cpv = getCurvePanelView(cpvName);
		if (cpv != null) {
			listCurvePanelView.remove(cpv);
			listCassisModel.remove(cpv.getModel().getCassisModel());
			JPanel panel = map.remove(cpvName);
			if (panel != null) {
				noDataPanel.remove(panel);
				noDataPanel.repaint();
			}
		}
		return cpv;
	}

	/**
	 * Add a listener for more info button. Open a {@link JFrame} to display all
	 *  information about {@link CommentedSpectrum}.
	 *
	 * @param cpv The {@link CurvePanelView} where the More info Button is created
	 * @param cs The {@link CommentedSpectrum}
	 */
	public void addListenerForMoreInfoButton(CurvePanelView cpv, final CommentedSpectrum cs) {
		cpv.getMoreInfoButton().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MoreInfoFrame moreInfoFrame = new MoreInfoFrame(cs);
				moreInfoFrame.removeDataLinkAction(DisplayBrowserAction.NAME);
				moreInfoFrame.addDataLinkAction(new RessourceDatalinkAction() {

					@Override
					public void run(ResourcePanel panel, String accessUrl) {
						System.out.println("try to open accessURL");
						File file = FileUtils.downloadAndRenameIfNeeded(accessUrl, true);
						if (file != null) {
							try {
								OpenAction.openCassisFileWithException(file, true);
							} catch (IOException e) {
								new DisplayBrowserAction().run(panel, accessUrl);
							}
						}
					}

					@Override
					public String getName() {
						return "DisplayFileOrBrowse";
					}
				});
				moreInfoFrame.setLocationRelativeTo(JPanelCurve.this);
				moreInfoFrame.setVisible(true);
			}
		});
	}

	/**
	 * Return the series with the given title.
	 *
	 * @param title The title of the series to search.
	 * @return The series with the given title or null if not found.
	 */
	private XYSeriesCassis getSeries(String title) {
		for (XYSeriesCassis s : listCassisModel) {
			if (s.getKey().equals(title)) {
				return s;
			}
		}
		return null;
	}

	/**
	 * Replace the series with the same name than the one given.
	 *
	 * @param series The series to set, to replace the one with the same name.
	 */
	public void replaceSeries(XYSpectrumSeries series) {
		CurvePanelView cpv = getCurvePanelView((String)series.getKey());
		XYSeriesCassis seriesToRemove = getSeries((String)series.getKey());
		int idx = -1;
		if (seriesToRemove != null) {
			 idx = listCassisModel.indexOf(seriesToRemove);
		}
		if (cpv != null && idx != -1) {
			cpv.getModel().setCassisModel(series);
			listCassisModel.remove(idx);
			listCassisModel.add(idx, series);
		}
	}
}
