/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.curve;

import java.util.ArrayList;
import java.util.List;

import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.YAxisCassis;

/**
 * Series for error line.
 */
public class XYErrorSeries extends XYSeriesCassis {

	private static final long serialVersionUID = 7488249448182130025L;

	private LineDescription mainLine;


	/**
	 * Constructor.
	 *
	 * @param seriesName The name of the series.
	 * @param typeCurve The TypeCurve.
	 * @param mainLine The main line associated to this error.
	 * @param xAxis The xAxis.
	 */
	public XYErrorSeries(String seriesName, TypeCurve typeCurve,
			LineDescription mainLine, XAxisCassis xAxis) {
		super(seriesName, typeCurve);
		this.mainLine = mainLine;
		setXAxis(xAxis);
	}

	@Override
	public List<LineDescription> getListOfLines() {
		ArrayList<LineDescription> arrayList = new ArrayList<>(1);
		if (mainLine != null)
			arrayList.add(mainLine);
		return arrayList;
	}

	@Override
	protected void buildXYSeries(XAxisCassis xaxis, YAxisCassis yaxis) {
		this.clear();
		if (mainLine != null) {
			xaxis.setVlsr(mainLine.getVlsr());
			double xValue = xaxis.convertFromMhzFreq(mainLine.getObsFrequency());
			double lineError = Math.abs(xValue - xaxis.convertFromMhzFreq(mainLine.getObsFrequency() +  mainLine.getDObsFrequency()));
			this.add(xValue - lineError, Double.NaN, false);
			this.add(xValue + lineError, Double.NaN);
		}
	}

	/**
	 * Return if this series type use the rendering value to plot/add the points.
	 * This series type always return false in that case.
	 *
	 * @return false
	 * @see eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassis#useRenderingToPlotPoints()
	 */
	@Override
	public boolean useRenderingToPlotPoints() {
		return false;
	}
}
