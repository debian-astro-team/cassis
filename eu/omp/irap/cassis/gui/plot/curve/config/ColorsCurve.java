/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.curve.config;

import java.awt.Color;
import java.util.Arrays;

public class ColorsCurve {

	private int indexColor = 0;
	private Color[] colors;
	private static int indexColorData = 0;
	private static int indexColorLine = 0;
	private static int indexColorResult = 0;
	private static int indexColorRotationalData = 0;
	private static int indexColorRotationalFit = 0;
	private static int indexColorLineList = 0;
	private static Color[] colorCurveData = { Color.BLACK, Color.BLUE, Color.CYAN,
		Color.GREEN, Color.ORANGE, Color.RED, Color.MAGENTA, Color.GRAY };

	private static Color[] colorCurveLine = { new Color(238, 130, 238), Color.GREEN,
		new Color(255, 0, 102), Color.BLACK };

	private static Color[] colorResult = { new Color(0, 204, 0), new Color(0, 0, 204),
		new Color(204, 0, 0), new Color(0, 204, 204)};

	private static Color[] colorRotationalData = {
		new Color(50, 50, 50), new Color(50, 50, 255), new Color(110, 40, 110),
		new Color(35, 100, 35), new Color(110, 40, 40), new Color(255, 50, 35) };

	private static Color[] colorRotationalFit = { Color.RED, Color.BLUE,
		new Color(0, 128, 0), Color.BLACK };

	private static Color[] colorLineList = {Color.ORANGE, Color.MAGENTA,
		new Color(0, 204, 51), Color.RED, Color.DARK_GRAY};

	public ColorsCurve(final Color[] colors) {
		this.colors = Arrays.copyOf(colors, colors.length);
	}

	public Color getNewColor() {
		Color color;
		color = colors[indexColor];
		indexColor = (indexColor + 1) % colors.length;
		return color;
	}

	public static ColorsCurve getColorsFullSpectrum() {
		return new ColorsCurve(
				new Color[] { Color.BLACK, new Color(238, 130, 238), Color.green, new Color(255, 102, 0) });
	}

	public static ColorsCurve getColorsLineSpectrum() {
		return new ColorsCurve(
				new Color[] { new Color(238, 130, 238), Color.green, new Color(255, 0, 102), Color.BLACK });
	}

	public static Color getNewColorData() {
		Color color;
		if (indexColorData == 0) {
			color = colorCurveData[indexColorData];
			indexColorData++;
		}
		else {
			color = colorCurveData[indexColorData];
			indexColorData = (indexColorData + 1) % (colorCurveData.length - 1) + 1;
		}
		return color;
	}

	public static void resetNewColorData() {
		indexColorData = 0;
	}

	public static Color getNewColorLine() {
		Color color;
		color = colorCurveLine[indexColorLine];
		indexColorLine = (indexColorLine + 1) % colorCurveLine.length;
		return color;
	}

	public static Color getNewColorResult() {
		Color color;
		color = colorResult[indexColorResult];
		indexColorResult = (indexColorResult + 1) % colorResult.length;
		return color;
	}

	public static Color getNewColorRotationalData() {
		Color color;
		color = colorRotationalData[indexColorRotationalData];
		indexColorRotationalData = (indexColorRotationalData + 1) % colorRotationalData.length;
		return color;
	}

	public static Color getNewColorRotationalFit() {
		Color color;
		color = colorRotationalFit[indexColorRotationalFit];
		indexColorRotationalFit = (indexColorRotationalFit + 1) % colorRotationalFit.length;
		return color;
	}

	public static Color getNewColorLineList() {
		Color color;
		color = colorLineList[indexColorLineList];
		indexColorLineList = (indexColorLineList + 1) % colorLineList.length;
		return color;
	}
}
