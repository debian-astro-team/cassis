/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.curve.config;

import java.awt.BasicStroke;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.GeneralPath;

/**
 * @author glorian
 *
 */
public enum StrokeCassis {
	CONTINU(getContinueIcon()), POINTILLE(getPointilleIcon()), TRAIT_POINT(getTraitPointIcon());

	private Shape icon;

	private StrokeCassis(Shape icon) {
		this.icon = icon;
	}

	private static Shape getPointilleIcon() {
		int[] x2Points = { 2, 8, 10, 16, 18, 24 };
		int[] y2Points = { 7, 7, 7, 7, 7, 7 };
		GeneralPath polyline = new GeneralPath(GeneralPath.WIND_EVEN_ODD, x2Points.length);

		polyline.moveTo(x2Points[0], y2Points[0]);
		polyline.lineTo(x2Points[1], y2Points[1]);
		polyline.moveTo(x2Points[2], y2Points[2]);
		polyline.lineTo(x2Points[3], y2Points[3]);
		polyline.moveTo(x2Points[4], y2Points[4]);
		polyline.lineTo(x2Points[5], y2Points[5]);
		polyline.closePath();
		return polyline;
	}

	private static Shape getContinueIcon() {
		int[] x2Points = { 2, 26 };
		int[] y2Points = { 7, 7 };
		GeneralPath polyline = new GeneralPath(GeneralPath.WIND_EVEN_ODD, x2Points.length);

		polyline.moveTo(x2Points[0], y2Points[0]);
		polyline.lineTo(x2Points[1], y2Points[1]);
		polyline.closePath();
		return polyline;
	}

	private static Shape getTraitPointIcon() {
		int[] x2Points = { 2, 18, 21, 26 };
		int[] y2Points = { 7, 7, 7, 7 };
		GeneralPath polyline = new GeneralPath(GeneralPath.WIND_EVEN_ODD, x2Points.length);

		polyline.moveTo(x2Points[0], y2Points[0]);
		polyline.lineTo(x2Points[1], y2Points[1]);
		polyline.moveTo(x2Points[2], y2Points[2]);
		polyline.lineTo(x2Points[3], y2Points[3]);
		polyline.closePath();
		return polyline;
	}

	private static Stroke getContinue(float width) {
		return new BasicStroke(width);
	}

	private static Stroke getPointille(float width) {
		return new BasicStroke(width, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 1.0f, new float[] { 4.0f, 2.0f },
				0f);
	}

	private static Stroke getTraitPoint(float width) {
		return new BasicStroke(width, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 1.0f, new float[] { 4.0f, 2.0f,
				1.0f }, 0f);
	}

	public Stroke getStroke(float width) {
		Stroke stroke = null;

		StrokeCassis strokeCassis = this;

		if (StrokeCassis.POINTILLE.equals(strokeCassis)) {
			stroke = getPointille(width);

		}
		else if (StrokeCassis.TRAIT_POINT.equals(strokeCassis)) {
			stroke = getTraitPoint(width);
		}
		else {
			stroke = getContinue(width);
		}
		return stroke;
	}

	/**
	 * @return the icon
	 */
	public final Shape getIcon() {
		return icon;
	}

}
