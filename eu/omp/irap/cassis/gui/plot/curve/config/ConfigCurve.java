/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.curve.config;

import java.awt.Color;
import java.awt.Shape;
import java.awt.Stroke;

public class ConfigCurve {

	private Color color = Color.BLACK;
	private boolean visible = true;
	private ShapeCassis shapeCassis = ShapeCassis.NONE;
	private StrokeCassis strokeCassis = StrokeCassis.CONTINU;
	private float size = 1;

	private int dotWidthSize = 2;
	private int dotHeightSize = 2;


	public ConfigCurve(Color color, boolean visible,
			ShapeCassis shapeCassis, StrokeCassis strokeCassis, int size) {
		this();
		this.color = color;
		this.visible = visible;
		this.shapeCassis = shapeCassis;
		this.strokeCassis = strokeCassis;
		this.size = size;
	}

	public ConfigCurve() {
		super();
	}

	/**
	 * @return the shapeCassis
	 */
	public final ShapeCassis getShapeCassis() {
		return shapeCassis;
	}

	/**
	 * @return the shape
	 */
	public final Shape getShape() {
		return shapeCassis.getShape();
	}

	/**
	 * @return the strokeCassis
	 */
	public final StrokeCassis getStrokeCassis() {
		return strokeCassis;
	}

	/**
	 * @return the size
	 */
	public final float getSize() {
		return size;
	}

	/**
	 * @param color
	 *            the color to set
	 */
	public final void setColor(Color color) {
		this.color = color;
	}

	/**
	 * @param visible
	 *            the visible to set
	 */
	public final void setVisible(boolean visible) {
		this.visible = visible;
	}

	/**
	 * @param shapeCassis
	 *            the shapeCassis to set
	 */
	public final void setShapeCassis(ShapeCassis shapeCassis) {
		this.shapeCassis = shapeCassis;
	}

	/**
	 * @param strokeCassis
	 *            the strokeCassis to set
	 */
	public final void setStrokeCassis(StrokeCassis strokeCassis) {
		this.strokeCassis = strokeCassis;
	}

	/**
	 * @param size
	 *            the size to set
	 */
	public final void setSize(float size) {
		this.size = size;
	}

	public boolean isVisible() {
		return visible;
	}

	public boolean isShapeVisible() {
		return shapeCassis.isShapeVisible();
	}

	public Color getColor() {
		return color;
	}

	public Stroke getStroke() {
		return getStrokeCassis().getStroke(getSize());
	}

	/**
	 * Return the dot width size.
	 *
	 * @return the dot width size.
	 */
	public int getDotWidthSize() {
		return dotWidthSize;
	}

	/**
	 * Return the dot height size.
	 *
	 * @return the dot height size.
	 */
	public int getDotHeightSize() {
		return dotHeightSize;
	}

	/**
	 * Change the dot width size.
	 *
	 * @param size The new width size of the dot to set.
	 */
	public void setDotWidthSize(int size) {
		dotWidthSize = size;
	}

	/**
	 * Change the dot height size.
	 *
	 * @param size The new height size of the dot to set.
	 */
	public void setDotHeightSize(int size) {
		dotHeightSize = size;
	}

	public ConfigCurve copy() {
		ConfigCurve configCurve = new ConfigCurve();
		configCurve.setColor(color);
		configCurve.setVisible(visible);
		configCurve.setShapeCassis(shapeCassis);
		configCurve.setStrokeCassis(strokeCassis);
		configCurve.setSize(size);
		configCurve.setDotWidthSize(dotWidthSize);
		configCurve.setDotHeightSize(dotHeightSize);
		return configCurve;
	}

}
