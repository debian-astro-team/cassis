/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.curve.config;

import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;

/**
 * @author glorian
 *
 */
public enum ShapeCassis {
	NONE(getNone(), getNone()), CROSS(getCross(), getCrossIcon()), ROUND(getRound(), getRoundIcon()), SQUARE(
			getSquare(), getSquareIcon()), TRIANGLE(getNone(), getNone());

	private Shape shape;
	private Shape icon;

	private ShapeCassis(Shape shape, Shape icon) {
		this.shape = shape;
		this.icon = icon;
	}

	public static Shape getCross() {
		int[] x2Points = { -5, 5, 0, 0 };
		int[] y2Points = { 0, 0, 5, -5 };
		GeneralPath polyline = new GeneralPath(GeneralPath.WIND_EVEN_ODD, x2Points.length);
		polyline.moveTo(x2Points[0], y2Points[0]);
		polyline.lineTo(x2Points[1], y2Points[1]);
		polyline.moveTo(x2Points[2], y2Points[2]);
		polyline.lineTo(x2Points[3], y2Points[3]);

		polyline.closePath();

		return polyline;
	}

	private static Shape getNone() {
		return null;
	}

	public static Shape getRound() {
		return new Ellipse2D.Double(-5.0, -5.0, 10.0, 10.0);
	}

	public static Shape getSquare() {
		return new Rectangle2D.Double(-5.0, -5.0, 10.0, 10.0);
	}

	public Shape getShape() {
		return shape;
	}

	private static Shape getCrossIcon() {
		int[] x2Points = { 7, 21, 14, 14 };
		int[] y2Points = { 7, 7, 0, 14 };
		GeneralPath polyline = new GeneralPath(GeneralPath.WIND_EVEN_ODD, x2Points.length);

		polyline.moveTo(x2Points[0], y2Points[0]);
		polyline.lineTo(x2Points[1], y2Points[1]);
		polyline.moveTo(x2Points[2], y2Points[2]);
		polyline.lineTo(x2Points[3], y2Points[3]);
		polyline.closePath();
		return polyline;
	}

	private static Shape getSquareIcon() {
		return new Rectangle2D.Double(0, 0, 15, 15);
	}

	private static Shape getRoundIcon() {
		return new Ellipse2D.Double(0, 0, 15, 15);
	}

	public Shape getIcon() {
		return icon;
	}

	public boolean isShapeVisible() {
		return shape != null;
	}
}
