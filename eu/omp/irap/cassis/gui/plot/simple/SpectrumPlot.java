/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.simple;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisSpace;
import org.jfree.chart.axis.LogAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.TickUnitSource;
import org.jfree.chart.axis.TickUnits;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.event.PlotChangeEvent;
import org.jfree.chart.event.PlotChangeListener;
import org.jfree.chart.event.RendererChangeEvent;
import org.jfree.chart.event.RendererChangeListener;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.AbstractXYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.renderer.xy.XYStepRenderer;
import org.jfree.data.Range;
import org.jfree.data.general.DatasetChangeEvent;
import org.jfree.data.general.DatasetChangeListener;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.Layer;
import org.jfree.ui.RectangleInsets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.TypeFrequency;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.common.axes.Y_AXIS;
import eu.omp.irap.cassis.gui.fit.advanced.interfaces.FitLineEstimable;
import eu.omp.irap.cassis.gui.plot.curve.Rendering;
import eu.omp.irap.cassis.gui.plot.curve.TypeCurve;
import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassis;
import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassisCollection;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import eu.omp.irap.cassis.gui.plot.curve.config.ConfigCurve;
import eu.omp.irap.cassis.gui.plot.curve.config.CurveParameters;
import eu.omp.irap.cassis.gui.plot.popup.MessageControl;
import eu.omp.irap.cassis.gui.plot.simple.listener.CassisChartMouseListener;
import eu.omp.irap.cassis.gui.plot.simple.renderer.XYDotBySeriesRenderer;
import eu.omp.irap.cassis.gui.plot.simple.util.ChartPanelCassis;
import eu.omp.irap.cassis.gui.plot.simple.util.ChartUtilities;
import eu.omp.irap.cassis.gui.plot.simple.util.PlotUtil;
import eu.omp.irap.cassis.gui.plot.simple.util.XYPlotCassisUtil;
import eu.omp.irap.cassis.gui.plot.util.InterValMarkerCassis;
import eu.omp.irap.cassis.gui.plot.util.JyBeamToKAxisDialog;
import eu.omp.irap.cassis.gui.plot.util.JyToKAxisDialog;
import eu.omp.irap.cassis.gui.plot.util.MarkerManager;
import eu.omp.irap.cassis.gui.plot.util.NearestLineInfo;
import eu.omp.irap.cassis.gui.plot.util.SpectrumFitPanelInterface;
import eu.omp.irap.cassis.gui.util.GraphicParameter;

/**
 * Class to represent a plot of spectra in CASSIS.
 *
 * @author M. Boiziot
 */
public class SpectrumPlot extends JPanel implements ChangeRenderingInterface {

	private static final long serialVersionUID = 5802609384074688763L;

	private static final Logger LOGGER = LoggerFactory.getLogger(SpectrumPlot.class);

	public static final Color BACKGROUND = new Color(240, 240, 240);
	private static final int RANGE_SPACE_LEFT = 90;
	private static final int RANGE_SPACE_RIGHT = 30;
	private static final int DOMAIN_SPACE_TOP = 40;
	private static final int DOMAIN_SPACE_BOTTOM = 40;

	private static final int DRAW_HEIGHT = 30;

	private static final int SPACE_LEFT_INDEX = 0;
	private static final int SPACE_RIGHT_INDEX = 1;
	private static final int SPACE_TOP_INDEX = 2;
	private static final int SPACE_BOTTOM_INDEX = 3;

	public static final String TOP_TITLE = "Frequency [GHz]";
	public static final String LEFT_TITLE = "T [K]";
	public static final String BOTTOM_TITLE = "Velocity [km/s]";

	private ChartPanelCassis chartPanel;
	private XYPlot plot;

	private XYSeriesCassisCollection topLineSeriesCollection;
	private XYSeriesCassisCollection topLineErrorSeriesCollection;
	private XYSeriesCassisCollection centerSeriesCollection;
	private XYSeriesCassisCollection bottomLineSignalSeriesCollection;
	private XYSeriesCassisCollection bottomLineImageSeriesCollection;

	private String topTitle;
	private String leftTitle;
	private String bottomTitle;
	private int rowId;
	private int colId;
	private int[] insets;
	private XAxisCassis xAxisCassisTop;
	private ChangeRenderingInterface externalRenderingListener;
	private Rendering rendering;
	private boolean gallery;
	private CassisChartMouseListener chartMouseListener;
	private boolean topAxisDomainSynchro;
	private boolean initDomainAxisSpace;


	/**
	 * Constructor.
	 */
	public SpectrumPlot() {
		this(ChartUtilities.createXYStepChart(LEFT_TITLE, "X", "Y",
				new XYSeriesCassisCollection(), new XYSeriesCassisCollection(),
				new XYSeriesCassisCollection(), new XYSeriesCassisCollection(),
				new XYSeriesCassisCollection(), true),
				new int[] { RANGE_SPACE_LEFT, RANGE_SPACE_RIGHT,
						DOMAIN_SPACE_TOP, DOMAIN_SPACE_BOTTOM },
				false);
	}

	/**
	 * Constructor.
	 *
	 * @param chartPanel The {@link ChartPanelCassis} to use.
	 * @param insets The insets to use.
	 * @param gallery true if the plot is for gallery, false if not.
	 */
	public SpectrumPlot(ChartPanelCassis chartPanel, int[] insets, boolean gallery) {
		this(chartPanel, insets, gallery, true);
	}

	/**
	 * Constructor.
	 *
	 * @param centerXySeries The center dataset to use.
	 * @param topLineXySeries The top line dataset to use.
	 * @param topLineErrorXySeries The top line error dataset to use.
	 * @param bottomLineSignalXySeries The bottom line signal dataset to use.
	 * @param bottomLineImageXySeries The bottom line image dataset to use.
	 * @param insets The insets to use.
	 * @param gallery true if the plot is for gallery, false if not.
	 */
	public SpectrumPlot(XYSeriesCassisCollection centerXySeries,
			XYSeriesCassisCollection topLineXySeries,
			XYSeriesCassisCollection topLineErrorXySeries,
			XYSeriesCassisCollection bottomLineSignalXySeries,
			XYSeriesCassisCollection bottomLineImageXySeries, int[] insets, boolean gallery) {
		this(ChartUtilities.createXYStepChart(LEFT_TITLE, "X", "Y",
				centerXySeries, topLineXySeries, topLineErrorXySeries,
				bottomLineSignalXySeries, bottomLineImageXySeries, true),
				insets, gallery);
	}

	/**
	 * Constructor.
	 *
	 * @param chartPanel The {@link ChartPanelCassis} to use.
	 * @param insets The insets to use.
	 * @param gallery true if the plot is for gallery, false if not.
	 * @param initDomainAxisSpace true to initialize the domain axis space,
	 *  false to do not.
	 */
	public SpectrumPlot(ChartPanelCassis chartPanel, int[] insets, boolean gallery, boolean initDomainAxisSpace) {
		super(new BorderLayout());
		this.insets = Arrays.copyOf(insets, insets.length);
		this.chartPanel = chartPanel;
		this.gallery = gallery;
		this.rendering = Rendering.HISTOGRAM;
		this.xAxisCassisTop = XAxisCassis.getXAxisFrequency(UNIT.GHZ);
		this.initDomainAxisSpace = initDomainAxisSpace;
		this.add(chartPanel, BorderLayout.CENTER);

		initValues();
		initAxis();
		addRendererListener();

		if (!gallery) {
			chartPanel.getChart().setAntiAlias(false);
			chartPanel.getChart().setTextAntiAlias(true);
			setBorder(BorderFactory.createEmptyBorder(4, 10, 0, 10));

			changeFont();
			changeRangeMargins();
			plot.setDomainCrosshairVisible(true);
			plot.setDomainCrosshairLockedOnData(true);
		} else {
			setBorder(null);
		}
	}

	/**
	 * Set the marker manager.
	 *
	 * @param markerManager the marker manager to set.
	 */
	public void setMarkerManager(MarkerManager markerManager) {
		if (chartMouseListener != null) {
			chartMouseListener.setMarkerManager(markerManager);
		}
	}

	/**
	 * Refresh to pop-ups.
	 *
	 * @param currentSpectrum The index of the spectrum.
	 */
	public void refreshPopups(int currentSpectrum) {
		if (chartMouseListener != null) {
			chartMouseListener.refreshPopups(currentSpectrum);
		}
	}

	/**
	 * Return the top line {@link XYSeriesCassisCollection}.
	 *
	 * @return the top line {@link XYSeriesCassisCollection}.
	 */
	public XYSeriesCassisCollection getTopLineSeriesCollection() {
		return topLineSeriesCollection;
	}

	/**
	 * Return the top line error {@link XYSeriesCassisCollection}.
	 *
	 * @return the top line error {@link XYSeriesCassisCollection}.
	 */
	public XYSeriesCassisCollection getTopLineErrorSeriesCollection() {
		return topLineErrorSeriesCollection;
	}

	/**
	 * Return the center {@link XYSeriesCassisCollection}.
	 *
	 * @return the center {@link XYSeriesCassisCollection}.
	 */
	public XYSeriesCassisCollection getCenterXYSeriesCollection() {
		return centerSeriesCollection;
	}

	/**
	 * Return the bottom line signal {@link XYSeriesCassisCollection}.
	 *
	 * @return the bottom line signal {@link XYSeriesCassisCollection}.
	 */
	public XYSeriesCassisCollection getBottomLineSignalSeriesCollection() {
		return bottomLineSignalSeriesCollection;
	}

	/**
	 * Return the bottom line image {@link XYSeriesCassisCollection}.
	 *
	 * @return the bottom line image {@link XYSeriesCassisCollection}.
	 */
	public XYSeriesCassisCollection getBottomLineImageSeriesCollection() {
		return bottomLineImageSeriesCollection;
	}

	/**
	 * Change the XAxis.
	 *
	 * @param xAxisCassis The new XAxis to set.
	 */
	public void changeXAxis(XAxisCassis xAxisCassis) {
		changeXAxis(topLineSeriesCollection, xAxisCassis);
		changeXAxis(topLineErrorSeriesCollection, xAxisCassis);
		changeXAxis(centerSeriesCollection, xAxisCassis);
		changeXAxis(bottomLineSignalSeriesCollection, xAxisCassis);
		changeXAxis(bottomLineImageSeriesCollection, xAxisCassis);
	}

	/**
	 *
	 * @param previousAxis
	 * @param selectedAxis
	 */
	public boolean updateYAxisParameters(YAxisCassis previousAxis, YAxisCassis selectedAxis) {

		boolean res = true;

		Y_AXIS previousAxisValue = previousAxis.getAxis();
		Y_AXIS selectedAxisValue = selectedAxis.getAxis();


		for (int cpt = 0; res && cpt < centerSeriesCollection.getSeries().size(); cpt++) {
			XYSeriesCassis serie = centerSeriesCollection.getSeries(cpt);
			boolean isSpectrum = TypeCurve.DATA.equals(serie.getTypeCurve());
			if (isSpectrum && serie instanceof XYSpectrumSeries) {
				CommentedSpectrum spectrum  = ((XYSpectrumSeries) serie).getSpectrum();
				res = askYParameters(previousAxisValue, selectedAxisValue, spectrum, null);
			}
		}


		for (int cpt = 0; res && cpt < centerSeriesCollection.getSeries().size(); cpt++) {
			XYSeriesCassis serie = centerSeriesCollection.getSeries(cpt);
			boolean isSpectrum = TypeCurve.DATA.equals(serie.getTypeCurve());
			if (!isSpectrum && serie instanceof XYSpectrumSeries) {
				CommentedSpectrum spectrum  = ((XYSpectrumSeries) serie).getSpectrum();

				CassisMetadata originMetadata = spectrum.getCassisMetadata(CassisMetadata.ORIGIN_DATA);
				List<CassisMetadata> otherMetadata = null;
				if (originMetadata != null) {
					XYSeriesCassis series = centerSeriesCollection.getSeries(originMetadata.getValue());
					if (series != null && series instanceof XYSpectrumSeries) {
						otherMetadata =  ((XYSpectrumSeries) series).getSpectrum().getCassisMetadataList();

					}
				}
				res = askYParameters(previousAxisValue, selectedAxisValue, spectrum, otherMetadata);
			}
		}
		return res;

	}

	public static boolean askYParameters(YAxisCassis previousAxisValue, YAxisCassis selectedAxisValue,
			CommentedSpectrum spectrum, List<CassisMetadata> otherMetadata) {
		return askYParameters(previousAxisValue.getAxis(), selectedAxisValue.getAxis(),spectrum, otherMetadata);
	}

	public static boolean askYParameters(Y_AXIS previousAxisValue, Y_AXIS selectedAxisValue,
			CommentedSpectrum spectrum, List<CassisMetadata> otherMetadata) {
		boolean res = true;
		boolean doNothing = false;
		Y_AXIS spectrumAxis = spectrum.getyAxis().getAxis();

		if (previousAxisValue.equals(selectedAxisValue)&&
			selectedAxisValue.equals(spectrumAxis)) {
			doNothing = true;
		}

		if (!doNothing && (previousAxisValue.equals(Y_AXIS.JANSKY_BY_BEAM) || selectedAxisValue.equals(Y_AXIS.JANSKY_BY_BEAM) ||
				spectrumAxis.equals(Y_AXIS.JANSKY_BY_BEAM))) {

			CassisMetadata metadataThetaMin = CassisMetadata.getMetadata(spectrum.getCassisMetadataList(), otherMetadata, CassisMetadata.THETA_MIN_BEAM);
			CassisMetadata metadataThetaMax = CassisMetadata.getMetadata(spectrum.getCassisMetadataList(), otherMetadata, CassisMetadata.THETA_MAX_BEAM);
			if (metadataThetaMin == null || metadataThetaMax  == null) {
				JyBeamToKAxisDialog dialog = new JyBeamToKAxisDialog(null, 1, 1);
				dialog.setTitle(spectrum.getTitle() + "-" +dialog.getTitle());
				dialog.setVisible(true);

				int value = ((Integer)dialog.getOptionPane().getValue()).intValue();
				if (value == JOptionPane.YES_OPTION) {
					metadataThetaMin = new CassisMetadata(CassisMetadata.THETA_MIN_BEAM, String.valueOf(dialog.getBeamMinorValue()), "", "arcsec");
					metadataThetaMax = new CassisMetadata(CassisMetadata.THETA_MAX_BEAM, String.valueOf(dialog.getBeamMajorValue()), "", "arcsec");
				} else {
					res = false;
				}
			}
			if (res) {
				CassisMetadata.replace(CassisMetadata.THETA_MIN_BEAM, metadataThetaMin, spectrum.getCassisMetadataList());
				CassisMetadata.replace(CassisMetadata.THETA_MAX_BEAM, metadataThetaMax, spectrum.getCassisMetadataList());
			}
		}

		if (!doNothing && res && (previousAxisValue.equals(Y_AXIS.JANSKY) || selectedAxisValue.equals(Y_AXIS.JANSKY) ||
				spectrumAxis.equals(Y_AXIS.JANSKY))) {
			CassisMetadata metadataThetaMin = CassisMetadata.getMetadata(spectrum.getCassisMetadataList(), otherMetadata, CassisMetadata.THETA_MIN);
			CassisMetadata metadataThetaMax = CassisMetadata.getMetadata(spectrum.getCassisMetadataList(), otherMetadata, CassisMetadata.THETA_MAX);
			CassisMetadata metadataTypeRegion = CassisMetadata.getMetadata(spectrum.getCassisMetadataList(), otherMetadata, CassisMetadata.TYPE_REGION);
			if (metadataThetaMin == null || metadataThetaMax  == null || metadataTypeRegion == null) {
				JyToKAxisDialog dialog = new JyToKAxisDialog(null, 1, 1);
				dialog.setTitle(spectrum.getTitle() + "-" +dialog.getTitle());
				dialog.setVisible(true);

				int value = ((Integer)dialog.getOptionPane().getValue()).intValue();
				if (value == JOptionPane.YES_OPTION) {
					metadataThetaMin = new CassisMetadata(CassisMetadata.THETA_MIN, String.valueOf(dialog.getBeamMinorValue()), "", "arcsec");
					metadataThetaMax = new CassisMetadata(CassisMetadata.THETA_MAX,  String.valueOf(dialog.getBeamMajorValue()), "", "arcsec");
					metadataTypeRegion = new CassisMetadata(CassisMetadata.TYPE_REGION,  dialog.getTypeRegion(),"", "");

				} else {
					res = false;
				}
			}
			if (res) {
				CassisMetadata.replace(CassisMetadata.THETA_MIN, metadataThetaMin, spectrum.getCassisMetadataList());
				CassisMetadata.replace(CassisMetadata.THETA_MAX, metadataThetaMax, spectrum.getCassisMetadataList());
				CassisMetadata.replace(CassisMetadata.TYPE_REGION, metadataTypeRegion, spectrum.getCassisMetadataList());
			}
		}
		return res;
	}


	public void changeYAxis(YAxisCassis yAxis) {
		for (int cpt = 0; cpt < centerSeriesCollection.getSeries().size(); cpt++) {
			centerSeriesCollection.getSeries(cpt).setyAxis(yAxis);
		}
	}

	/**
	 * Change the XAxis for a given collection.
	 *
	 * @param collection The collection.
	 * @param xAxisCassis The XAxis to set.
	 */
	private void changeXAxis(XYSeriesCassisCollection collection, XAxisCassis xAxisCassis) {
		for (int cpt = 0; cpt < collection.getSeries().size(); cpt++) {
			collection.getSeries(cpt).setXAxis(xAxisCassis);
		}
	}

	/**
	 * Compute then return the maximum frequency value of the series in the plot.
	 *
	 * @return the maximum frequency value of the series in the plot.
	 */
	public double getMaxFrequencyPlot() {
		double res = Double.NEGATIVE_INFINITY;
		XYSeriesCollection series = getCenterXYSeriesCollection();

		for (int cpt = 0; cpt < series.getSeriesCount(); cpt++) {
			if (series.getSeries(cpt) instanceof XYSpectrumSeries) {
				XYSpectrumSeries seriesCassis = (XYSpectrumSeries) series.getSeries(cpt);
				res = Math.max(res, seriesCassis.getSpectrum().getFrequencySignalMax());
			}
		}
		if (res == Double.NEGATIVE_INFINITY) {
			res = 0.;
		}
		return res;
	}

	/**
	 * Compute then return the minimum frequency value of the series in the plot.
	 *
	 * @return the minimum frequency value of the series in the plot.
	 */
	public double getMinFrequencyPlot() {
		double res = Double.POSITIVE_INFINITY;
		XYSeriesCollection series = getCenterXYSeriesCollection();

		for (int cpt = 0; cpt < series.getSeriesCount(); cpt++) {
			if (series.getSeries(cpt) instanceof XYSpectrumSeries) {
				XYSpectrumSeries seriesCassis = (XYSpectrumSeries) series.getSeries(cpt);
				res = Math.min(res, seriesCassis.getSpectrum().getFrequencySignalMin());
			}
		}
		if (res == Double.POSITIVE_INFINITY) {
			res = 0.;
		}
		return res;
	}

	/**
	 * Set the ChartMouseListener.
	 *
	 * @param model The {@link NearestLineInfo}.
	 * @param markerManager The marker manager.
	 * @param infoMessageControl The message control.
	 * @param estimable The fit interface to estimate lines.
	 * @param fitInterface The fit interface.
	 */
	public void setChartMouseListener(NearestLineInfo model,
			MarkerManager markerManager, MessageControl infoMessageControl,
			FitLineEstimable estimable, SpectrumFitPanelInterface fitInterface) {
		chartMouseListener = new CassisChartMouseListener(this, model,
				markerManager, infoMessageControl, estimable, fitInterface);
		chartPanel.addMouseListener(chartMouseListener);
		chartPanel.addChartMouseListener(chartMouseListener);
		chartPanel.getChart().addProgressListener(chartMouseListener);
	}

	/**
	 * Change the range margins.
	 */
	private void changeRangeMargins() {
		plot.getRangeAxis().setLowerMargin(0.14);
		plot.getRangeAxis().setUpperMargin(0.14);
		plot.getRangeAxis(1).setLowerMargin(0.14);
		plot.getRangeAxis(1).setUpperMargin(0.14);
	}

	/**
	 * Configure the fonts and ticks for the four axis.
	 */
	public void changeFont() {
		initFontAndTick(plot.getDomainAxis());
		initFontAndTick(plot.getDomainAxis(1));
		initFontAndTick(plot.getRangeAxis());
		initFontAndTick(plot.getRangeAxis(1));
	}

	/**
	 * Configure the the font and tick for the given axis.
	 *
	 * @param axis The axis to configure.
	 */
	private void initFontAndTick(ValueAxis axis) {
		GraphicParameter.initializeFontLabel(axis);
		GraphicParameter.initializeTicks(axis);
	}

	private void initValues() {
		chartPanel.setChangeRenderingInterface(this);
		plot = chartPanel.getChart().getXYPlot();
		topLineSeriesCollection = (XYSeriesCassisCollection)
				plot.getDataset(SpectrumPlotConstants.TOP_LINE_INDEX);
		topLineErrorSeriesCollection = (XYSeriesCassisCollection)
				plot.getDataset(SpectrumPlotConstants.TOP_LINE_ERROR_INDEX);
		centerSeriesCollection = (XYSeriesCassisCollection)
				plot.getDataset(SpectrumPlotConstants.CENTER_INDEX);
		bottomLineSignalSeriesCollection = (XYSeriesCassisCollection)
				plot.getDataset(SpectrumPlotConstants.BOTTOM_LINE_SIGNAL_INDEX);
		bottomLineImageSeriesCollection = (XYSeriesCassisCollection)
				plot.getDataset(SpectrumPlotConstants.BOTTOM_LINE_IMAGE_INDEX);
		centerSeriesCollection.addChangeListener(new DatasetChangeListener() {
			@Override
			public void datasetChanged(DatasetChangeEvent event) {
				handleLogChange();
			}
		});

		initTitle();
		initZoom();
		initChart();
		initSize();
		initPlot();
	}

	private void initTitle() {
		topTitle = TOP_TITLE;
		bottomTitle = BOTTOM_TITLE;
		leftTitle = LEFT_TITLE;
	}
	private void initZoom() {
		chartPanel.setMouseWheelEnabled(true);
		chartPanel.setMouseZoomable(true);
		chartPanel.setFillZoomRectangle(false);
	}

	private void initChart() {
		JFreeChart chart = chartPanel.getChart();
		chart.setBackgroundPaint(BACKGROUND);
		chart.setTitle((String) null); // no space for the title
	}

	private void initSize() {
		Dimension size = new Dimension(WIDTH, HEIGHT);
		setSize(size);
		setPreferredSize(size);
		chartPanel.setMinimumDrawHeight(DRAW_HEIGHT);
		chartPanel.setMinimumSize(new Dimension(size.width, DRAW_HEIGHT + 20));
		chartPanel.setPreferredSize(new Dimension(size.width, DRAW_HEIGHT + 20));
	}

	private void initPlot() {
		plot.setBackgroundPaint(Color.WHITE);
		plot.setOutlineVisible(false); // no Outline, use AxisLine to form the Outline
		plot.setDomainGridlinesVisible(false); // Gridlines = lines on the data area
		plot.setRangeGridlinesVisible(false);
		plot.setInsets(new RectangleInsets(0.0, 0.0, 0.0, 0.0), false);
		plot.setAxisOffset(new RectangleInsets(0, 0, 0, 0)); // no gap between the data area and the axes
		plot.setNoDataMessage("No Data");
		AxisSpace rangeAxisSpace = new AxisSpace();
		rangeAxisSpace.setLeft(insets[SPACE_LEFT_INDEX]);
		rangeAxisSpace.setRight(insets[SPACE_RIGHT_INDEX]);
		plot.setFixedRangeAxisSpace(rangeAxisSpace); // share the same range space
	}

	private void initAxis() {
		if (initDomainAxisSpace) {
			initDomainAxisSpace(insets[SPACE_BOTTOM_INDEX], insets[SPACE_TOP_INDEX]);
		}

		initFontAxis();
		try {
			initLeftAxis(plot.getRangeAxis());

			cloneFontTopandRightAxisCenter();
			cloneAxisFromCenterToTopAndBottom();
			setTitle(topTitle, leftTitle, bottomTitle);

			initAxisNotVisible();
			plot.getRangeAxis(1).setTickLabelsVisible(false);
		}
		catch (Exception e) {
			LOGGER.error("Error while cloning axis", e);
		}
	}

	/**
	 * Initialize the space reserved at the top and bottom of the plot.
	 *
	 * @param bottomSpace The space reserved at the bottom of the plot.
	 * @param topSpace The space reserved at the top of the plot.
	 */
	public void initDomainAxisSpace(int bottomSpace, int topSpace) {
		AxisSpace domainSpace = new AxisSpace();
		domainSpace.setTop(topSpace);
		domainSpace.setBottom(bottomSpace);
		plot.setFixedDomainAxisSpace(domainSpace, true);
	}

	private void cloneAxisFromCenterToTopAndBottom() {
		plot.addChangeListener(new PlotChangeListener() {

			@Override
			public void plotChanged(PlotChangeEvent event) {
				ValueAxis domainAxis = plot.getDomainAxis(0);

				double lowerBound = domainAxis.getRange().getLowerBound();
				double upperBound = domainAxis.getRange().getUpperBound();

				XAxisCassis bottomxAxis = getXAxisCassis();
				XAxisCassis topxAxis = getxAxisCassisTop();

				double lengthAxis = upperBound - lowerBound;
				double lowerMargin = lengthAxis * domainAxis.getLowerMargin() / (1 + domainAxis.getLowerMargin() + domainAxis.getUpperMargin());
				lowerBound = lowerBound + lowerMargin;

				double upperMargin = lengthAxis * domainAxis.getUpperMargin() / (1 + domainAxis.getLowerMargin() + domainAxis.getUpperMargin());
				upperBound = upperBound - upperMargin;

				boolean isInverted = topxAxis.isInverted() != bottomxAxis.isInverted();

				double min = XAxisCassis.convert(lowerBound, bottomxAxis, topxAxis, getTypeFrequency());
				double max = XAxisCassis.convert(upperBound, bottomxAxis, topxAxis, getTypeFrequency());

				double temp = min;
				min = Math.min(temp, max);
				max = Math.max(temp, max);

				lengthAxis = max - min;

				min = min - domainAxis.getLowerMargin() * lengthAxis;
				max = max + domainAxis.getUpperMargin() * lengthAxis;

				changeTopAxisRange(min, max, isInverted);
			}
		});
	}

	private void cloneFontTopandRightAxisCenter() throws CloneNotSupportedException {
		plot.setDomainAxis(1, (ValueAxis) plot.getDomainAxis().clone());
		plot.setRangeAxis(1, (ValueAxis) plot.getRangeAxis().clone());
		List<Integer> list = new ArrayList<>(2);
		list.add(0);
		list.add(1);
		plot.mapDatasetToRangeAxes(SpectrumPlotConstants.CENTER_INDEX, list);
	}

	private void initAxisNotVisible() {
		PlotUtil.configureXAxis(plot.getDomainAxis());
		PlotUtil.configureXAxis(plot.getDomainAxis(1));
	}

	private void setTitle(String topTitle, String leftTitle, String bottomTitle) {
		plot.getDomainAxis().setLabel(bottomTitle);
		plot.getRangeAxis().setLabel(leftTitle);
		plot.getDomainAxis(1).setLabel(topTitle);
		plot.getRangeAxis(1).setLabel(null);
	}

	private void initLeftAxis(ValueAxis yleft) {
		if (yleft instanceof NumberAxis) {
			PlotUtil.configureYAxis((NumberAxis) yleft);
		}
		yleft.setTickLabelsVisible(true);
		yleft.setTickMarksVisible(true);
		yleft.setTickMarkInsideLength(7);
		yleft.setTickMarkOutsideLength(0);
	}

	private void initFontAxis() {
		ValueAxis domainAxis = plot.getDomainAxis();
		ValueAxis rangeAxis = plot.getRangeAxis();
		Font labelFont = new Font("Monospace", Font.BOLD, 11);
		Font tickFont = new Font("Monospace", Font.PLAIN, 9);
		domainAxis.setLabelFont(labelFont);
		domainAxis.setTickLabelFont(tickFont);
		if (domainAxis instanceof NumberAxis) {
			((NumberAxis) domainAxis).setAutoRangeIncludesZero(false);
			((NumberAxis) domainAxis).setAutoRangeStickyZero(false);
		}

		rangeAxis.setLabelFont(labelFont);
		rangeAxis.setTickLabelFont(tickFont);

		if (rangeAxis instanceof NumberAxis) {
			((NumberAxis) rangeAxis).setAutoRangeIncludesZero(false);
			((NumberAxis) rangeAxis).setAutoRangeStickyZero(false);
		}
	}

	/**
	 * Set a logarithmic YAxis and reconfigure font/margin if needed.
	 */
	public void setYAxisToLog() {
		plot.setRangeAxis(getLogAxis(plot.getRangeAxis().getLabel(), true));
		setLogRangeSmallestValue(getPlot());
		initAxis();
		if (!gallery) {
			changeFont();
			changeRangeMargins();
		}
	}

	/**
	 * Set a non-logarithmic YAxis and reconfigure font/margin if needed.
	 */
	public void setYAxisNormal() {
		plot.setRangeAxis(new NumberAxis(plot.getRangeAxis().getLabel()));
		initAxis();
		if (!gallery) {
			changeFont();
			changeRangeMargins();
		}
	}

	/**
	 * Create TickUnits for Y logarithmic axis.
	 *
	 * @return TickUnits for Y logarithmic axis.
	 */
	public static TickUnitSource getLogTickUnit() {
		TickUnits units = new TickUnits();
		NumberFormat decimalFormat = new NumberFormat() {
			private static final long serialVersionUID = -1368761276970438942L;

			@Override
			public Number parse(String source, ParsePosition parsePosition) {
				return new DecimalFormat().parse(source, parsePosition);
			}

			@Override
			public StringBuffer format(long number, StringBuffer toAppendTo,
					FieldPosition pos) {
				if (Math.abs(number) >= 0.001 && Math.abs(number) <= 1000.0) {
					return new StringBuffer(new DecimalFormat("0.###").format(number));
				} else {
					return new StringBuffer(new DecimalFormat("0E00").format(number));
				}
			}

			@Override
			public StringBuffer format(double number, StringBuffer toAppendTo,
					FieldPosition pos) {
				if (Math.abs(number) >= 0.001 && Math.abs(number) <= 1000.0) {
					return new StringBuffer(new DecimalFormat("0.###").format(number));
				} else {
					return new StringBuffer(new DecimalFormat("0E00").format(number));
				}
			}
		};
		double d = 1e-11;
		for (int i = 0; i < 50; i++) {
			d *= 10.;
			units.add(new NumberTickUnit(d, decimalFormat));
		}
		return units;
	}

	/**
	 * Create a LogAxis.
	 *
	 * @param label The label to set.
	 * @param rangeAxis true if a range axis, false otherwise.
	 * @return the created LogAxis.
	 */
	public static LogAxis getLogAxis(String label, boolean rangeAxis) {
		LogAxis logAxis = new LogAxisCassis(label);
		logAxis.setBase(10);
		if (rangeAxis) {
			logAxis.setStandardTickUnits(getLogTickUnit());
		}
		logAxis.setSmallestValue(1E-5);
		logAxis.setAutoRange(true);
		return logAxis;
	}

	/**
	 *  Set a logarithmic XAxis  and reconfigure font/margin if needed.
	 */
	public void setXAxisToLog() {
		plot.setDomainAxis(getLogAxis(plot.getDomainAxis().getLabel(), false));
		setLogDomainSmallestValue(getPlot());
		initAxis();
		if (!gallery) {
			changeFont();
			changeRangeMargins();
		}
	}

	/**
	 * Set a non-logarithmic XAxis  and reconfigure font/margin if needed.
	 */
	public void setXAxisNormal() {
		plot.setDomainAxis(new NumberAxis(plot.getDomainAxis().getLabel()));
		initAxis();
		if (!gallery) {
			changeFont();
			changeRangeMargins();
		}
	}

	/**
	 * Return the size of the tick unit who is equals or lower than the provided value.
	 *
	 * @param value The value.
	 * @return the size of the tick unit who is equals or lower than the provided value.
	 */
	private double getLogSmallestValue(double value) {
		return getLogTickUnit().getCeilingTickUnit(value / 10).getSize();
	}

	/**
	 * Compute and set the optimal smallest range (Y) value for the given plot.
	 *
	 * @param plot The plot.
	 */
	private void setLogRangeSmallestValue(XYPlot plot) {
		double lowestPositiveValue = getLogSmallestValue(
				centerSeriesCollection.getLowestPositiveValueRange());
		if (!Double.isNaN(lowestPositiveValue)) {
			LogAxis logAxis = (LogAxis) plot.getRangeAxis();
			logAxis.setSmallestValue(lowestPositiveValue);
		}
	}

	/**
	 * Compute and set the optimal smallest domain (X) value for the given plot.
	 *
	 * @param p The plot.
	 */
	private void setLogDomainSmallestValue(XYPlot p) {
		double lowestPositiveValue = getLogSmallestValue(
				centerSeriesCollection.getLowestPositiveValueDomain());
		if (!Double.isNaN(lowestPositiveValue)) {
			LogAxis logAxis = (LogAxis) p.getDomainAxis();
			logAxis.setSmallestValue(lowestPositiveValue);
		}
	}

	/**
	 * Handle a change and do put the correct smallest value if the axis are LogAxis.
	 */
	private void handleLogChange() {
		if (isLogXAxis()) {
			setLogDomainSmallestValue(getPlot());
		}
		if (isLogYAxis()) {
			setLogRangeSmallestValue(getPlot());
		}
	}

	/**
	 * Return if the X Axis a logarithmic axis.
	 *
	 * @return true if the X Axis is a logarithmic axis, false otherwise.
	 */
	public boolean isLogXAxis() {
		return getPlot().getDomainAxis() instanceof LogAxis;
	}

	/**
	 * Return if the Y Axis a logarithmic axis.
	 *
	 * @return true if the Y Axis is a logarithmic axis, false otherwise.
	 */
	public boolean isLogYAxis() {
		return getPlot().getRangeAxis() instanceof LogAxis;
	}

	/**
	 * Add a change listener on the center renderer.
	 */
	private void addRendererListener() {
		getCenterRenderer().addChangeListener(new RendererChangeListener() {
			@Override
			public void rendererChanged(RendererChangeEvent event) {
				if (event.getSeriesVisibilityChanged()) {
					handleLogChange();
				}
			}
		});
	}

	/**
	 * Plot the values with an histogram rendering.
	 *
	 * @see eu.omp.irap.cassis.gui.plot.simple.ChangeRenderingInterface#setHistogramRendering()
	 */
	@Override
	public void setHistogramRendering() {
		if (rendering == Rendering.HISTOGRAM) {
			return;
		}
		rendering = Rendering.HISTOGRAM;
		XYStepRenderer newRenderer = new XYStepRenderer();
		XYPlotCassisUtil.configureRenderer(getCenterXYSeriesCollection(), newRenderer);
		plot.setRenderer(SpectrumPlotConstants.CENTER_INDEX, newRenderer);
		chartPanel.getChart().setAntiAlias(false);
		if (externalRenderingListener != null) {
			externalRenderingListener.setHistogramRendering();
		}
	}

	/**
	 * Plot the values with a dot rendering.
	 *
	 * @see eu.omp.irap.cassis.gui.plot.simple.ChangeRenderingInterface#setDotRendering()
	 */
	@Override
	public void setDotRendering() {
		if (rendering == Rendering.DOT) {
			return;
		}
		rendering = Rendering.DOT;
		XYDotBySeriesRenderer newRenderer = new XYDotBySeriesRenderer();
		XYPlotCassisUtil.configureRenderer(getCenterXYSeriesCollection(), newRenderer);
		plot.setRenderer(SpectrumPlotConstants.CENTER_INDEX, newRenderer);
		chartPanel.getChart().setAntiAlias(true);
		if (externalRenderingListener != null) {
			externalRenderingListener.setDotRendering();
		}
	}

	/**
	 * Plot the values with a line rendering.
	 *
	 * @see eu.omp.irap.cassis.gui.plot.simple.ChangeRenderingInterface#setLineRendering()
	 */
	@Override
	public void setLineRendering() {
		if (rendering == Rendering.LINE) {
			return;
		}
		rendering = Rendering.LINE;
		XYLineAndShapeRenderer newRenderer = new XYLineAndShapeRenderer();
		XYPlotCassisUtil.configureRenderer(getCenterXYSeriesCollection(), newRenderer);
		plot.setRenderer(SpectrumPlotConstants.CENTER_INDEX, newRenderer);
		chartPanel.getChart().setAntiAlias(true);
		if (externalRenderingListener != null) {
			externalRenderingListener.setLineRendering();
		}
	}

	/**
	 * Update the rendering for the given rendering.
	 *
	 * @param rendering The rendering to use.
	 */
	public void updateRendering(Rendering rendering) {
		switch (rendering) {
		case HISTOGRAM:
			setHistogramRendering();
			break;
		case DOT:
			setDotRendering();
			break;
		case LINE:
			setLineRendering();
			break;
		default:
			throw new IllegalArgumentException("Unknown Rendering type");
		}
	}

	/**
	 * Add an external {@link ChangeRenderingInterface}.
	 *
	 * @param externalRenderingListener The {@link ChangeRenderingInterface} to set.
	 */
	public void setExternalChangeRenderingInterface(ChangeRenderingInterface externalRenderingListener) {
		this.externalRenderingListener = externalRenderingListener;
	}

	/**
	 * Return the XYPlot.
	 *
	 * @return the XYPlot.
	 */
	public XYPlot getPlot() {
		return plot;
	}

	/**
	 * Return the top line renderer.
	 *
	 * @return the top line renderer.
	 */
	public final AbstractXYItemRenderer getTopLineRenderer() {
		return (AbstractXYItemRenderer) plot.getRenderer(SpectrumPlotConstants.TOP_LINE_INDEX);
	}

	/**
	 * Return the top line error renderer.
	 *
	 * @return the top line error renderer.
	 */
	public final AbstractXYItemRenderer getTopLineErrorRenderer() {
		return (AbstractXYItemRenderer) plot.getRenderer(SpectrumPlotConstants.TOP_LINE_ERROR_INDEX);
	}

	/**
	 * Return the center renderer.
	 *
	 * @return the center renderer.
	 */
	public final AbstractXYItemRenderer getCenterRenderer() {
		return (AbstractXYItemRenderer) plot.getRenderer(SpectrumPlotConstants.CENTER_INDEX);
	}

	/**
	 * Return the bottom line signal renderer.
	 *
	 * @return the bottom line signal renderer.
	 */
	public final AbstractXYItemRenderer getBottomLineSignalRenderer() {
		return (AbstractXYItemRenderer) plot.getRenderer(SpectrumPlotConstants.BOTTOM_LINE_SIGNAL_INDEX);
	}

	/**
	 * Return the bottom line image renderer.
	 *
	 * @return the bottom line image renderer.
	 */
	public final AbstractXYItemRenderer getBottomLineImageRenderer() {
		return (AbstractXYItemRenderer) plot.getRenderer(SpectrumPlotConstants.BOTTOM_LINE_IMAGE_INDEX);
	}

	/**
	 * Change the left title.
	 *
	 * @param title The title to set.
	 */
	public void setLeftTitle(String title) {
		plot.getRangeAxis().setLabel(title);
	}

	/**
	 * Change the bottom title.
	 *
	 * @param title The title to set.
	 */
	public void setBottomTitle(String title) {
		plot.getDomainAxis().setLabel(title);
	}

	/**
	 * Change the id.
	 *
	 * @param rowId The row id.
	 * @param colId The column id.
	 */
	public void setId(int rowId, int colId) {
		this.rowId = rowId;
		this.colId = colId;
	}

	/**
	 * Return the row id.
	 *
	 * @return the row id.
	 */
	public int getRowId() {
		return rowId;
	}

	/**
	 * Return the column id.
	 *
	 * @return the column id.
	 */
	public int getColId() {
		return colId;
	}

	/**
	 * Return the ChartPanel.
	 *
	 * @return the ChartPanel.
	 */
	public ChartPanel getChartPanel() {
		return chartPanel;
	}

	/**
	 * Return the JFreeChart.
	 *
	 * @return the JFreeChart.
	 */
	public JFreeChart getJFreeChart() {
		return chartPanel.getChart();
	}

	/**
	 * Search visible series that can be saved then return a
	 *  {@link XYSeriesCassisCollection} of them.
	 *
	 * @return A {@link XYSeriesCassisCollection} with the visible series that
	 *  can be saved.
	 */
	public XYSeriesCassisCollection getSeriesVisibleAndSavable() {
		XYSeriesCassisCollection series = new XYSeriesCassisCollection();
		series.setRendering(rendering);
		for (int i = 0; i < centerSeriesCollection.getSeriesCount(); i++) {
			XYSeriesCassis serie = centerSeriesCollection.getSeries(i);
			if (serie.getConfigCurve().isVisible() && TypeCurve.isSavable(serie)) {
				series.addSeries(serie);
			}
		}
		return series;
	}

	/**
	 * Return the number of series visible and savable.
	 *
	 * @return The number of series visible and savable.
	 */
	public int getNbSeriesVisibleAndSavable() {
	 	return getSeriesVisibleAndSavable().getSeriesCount();
	}

	/**
	 * Return the {@link CommentedSpectrum} of the series who are visible and savable.
	 *
	 * @return the {@link CommentedSpectrum} of the series who are visible and savable.
	 */
	public List<CommentedSpectrum> getSpectrumsVisibleAndSavable() {
		XYSeriesCassisCollection series = getSeriesVisibleAndSavable();
		List<CommentedSpectrum> spectrums = new ArrayList<>(series.getSeriesCount());
		for (int i = 0; i < series.getSeriesCount(); i++) {
			XYSeriesCassis seriesCassis = series.getSeries(i);
			if (seriesCassis instanceof XYSpectrumSeries) {
				spectrums.add(((XYSpectrumSeries) seriesCassis).getSpectrum());
			}
		}
		return spectrums;
	}

	/**
	 * Change the parameter of a given series name on the top with the given
	 *  {@link ConfigCurve}.
	 *
	 * @param seriesName The name of the series.
	 * @param configCurve The ConfigCurve.
	 */
	public void modifyTop(String seriesName, ConfigCurve configCurve) {
		int index = indexOf(seriesName, topLineSeriesCollection);
		if (index != -1) {
			XYPlotCassisUtil.setRenderSeriesParameters(getTopLineRenderer(),
					index, configCurve);
		} else {
			index = indexOf(seriesName, topLineErrorSeriesCollection);
			if (index != -1) {
				XYPlotCassisUtil.setRenderSeriesParameters(
						getTopLineErrorRenderer(), index, configCurve);
			}
		}
	}

	/**
	 * Change the parameter of a given series name on the center with the given
	 *  {@link ConfigCurve}.
	 *
	 * @param seriesName The name of the series.
	 * @param configCurve The ConfigCurve.
	 */
	public void modifyCenter(String seriesName, ConfigCurve configCurve) {
		int index = indexOf(seriesName, centerSeriesCollection);
		if (index != -1) {
			XYPlotCassisUtil.setRenderSeriesParameters(getCenterRenderer(),
					index, configCurve);
		}
	}

	/**
	 * Change the parameter of a given series name on the bottom with the given
	 *  {@link ConfigCurve}.
	 *
	 * @param seriesName The name of the series.
	 * @param configCurve The ConfigCurve.
	 */
	public void modifyBottom(String seriesName, ConfigCurve configCurve) {
		int index = indexOf(seriesName, bottomLineSignalSeriesCollection);
		if (index != -1) {
			XYPlotCassisUtil.setRenderSeriesParameters(
					getBottomLineSignalRenderer(), index, configCurve);
		} else {
			index = indexOf(seriesName, bottomLineImageSeriesCollection);
			if (index != -1) {
				XYPlotCassisUtil.setRenderSeriesParameters(
						getBottomLineImageRenderer(), index, configCurve);
			}
		}
	}

	/**
	 * Search then return the index of a given series name in a given collection.
	 *
	 * @param seriesName The name of the series.
	 * @param collection The collection.
	 * @return the index or -1 if not found.
	 */
	private int indexOf(String seriesName, XYSeriesCollection collection) {
		for (int i = 0; i < collection.getSeriesCount(); i++) {
			if (collection.getSeries(i).getKey().equals(seriesName)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Return the top x axis.
	 *
	 * @return the top x axis.
	 */
	public XAxisCassis getxAxisCassisTop() {
		return xAxisCassisTop;
	}

	/**
	 * Change the top x axis.
	 *
	 * @param axis The axis to set.
	 */
	public void setXAxisCassisTop(XAxisCassis axis) {
		this.xAxisCassisTop = axis;
		plot.getDomainAxis(1).setLabel(axis.getTitleLabel());
	}

	public void changeTopAxisRange(double minRange, double maxRange, boolean inverted) {
		if (Double.isInfinite(minRange) || Double.isInfinite(maxRange)) {
			return;
		}
		ValueAxis getAxisTop = plot.getDomainAxis(1);
		Range frequencyRange = new Range(minRange, maxRange);

		if (!getAxisTop.getRange().equals(frequencyRange)) {
			getAxisTop.setRange(frequencyRange);
		}
		//During the zoom, the values of axis don't change
		if (!getAxisTop.getDefaultAutoRange().equals(frequencyRange)) {
			getAxisTop.setDefaultAutoRange(frequencyRange);
		}

		if (getAxisTop.getFixedAutoRange() != maxRange - minRange)
			getAxisTop.setFixedAutoRange(maxRange - minRange);

		if (getAxisTop.isInverted() != inverted) {
			getAxisTop.setInverted(inverted);
		}
	}

	/**
	 * Update the renderer for the given series with the given parameters.
	 *
	 * @param series The series to update.
	 * @param curveParameters The parameters to use.
	 */
	public void updateRender(XYSeriesCassis series, CurveParameters curveParameters) {
		XYSeriesCassisCollection collection = getCollectionOf(series);
		if (collection != null) {
			int index = collection.indexOf(series);
			XYPlotCassisUtil.setRenderSeriesParameters((AbstractXYItemRenderer)
					plot.getRendererForDataset(collection), index,
					series.getConfigCurve(), curveParameters);
		}
	}

	private XYSeriesCassisCollection getCollectionOf(XYSeriesCassis series) {
		List<XYSeriesCassisCollection> collections = getCollections();
		for (XYSeriesCassisCollection collection : collections) {
			if (collection.indexOf(series) != -1) {
				return collection;
			}
		}
		return null;
	}

	/**
	 * Return the {@link TypeFrequency}.
	 *
	 * @return the {@link TypeFrequency}.
	 */
	private TypeFrequency getTypeFrequency() {
		if (centerSeriesCollection.getSeriesCount() > 0) {
			return ((XYSpectrumSeries)centerSeriesCollection.getSeries(0)).getSpectrum().getTypeFreq();
		} else {
			return TypeFrequency.REST;
		}
	}

	/**
	 * Return the XAxisCassis.
	 *
	 * @return the XAxisCassis.
	 */
	private XAxisCassis getXAxisCassis() {
		if (centerSeriesCollection.getSeriesCount() > 0) {
			return centerSeriesCollection.getSeries(0).getXAxis();
		} else {
			return XAxisCassis.getXAxisFrequency(UNIT.GHZ);
		}
	}

	/**
	 * Return the collections of the plot.
	 *
	 * @return the collections of the plot.
	 */
	public List<XYSeriesCassisCollection> getCollections() {
		List<XYSeriesCassisCollection> listCollection = new ArrayList<>(5);
		listCollection.add(getCenterXYSeriesCollection());
		listCollection.add(getTopLineSeriesCollection());
		listCollection.add(getTopLineErrorSeriesCollection());
		listCollection.add(getBottomLineSignalSeriesCollection());
		listCollection.add(getBottomLineImageSeriesCollection());
		return listCollection;
	}

	/**
	 * Change the visibility of the tick label and label of the range axis.
	 *
	 * @param visible true to set them visible, false to hide them.
	 */
	public void setRangeAxisLabelVisible(boolean visible) {
		ValueAxis axis = getPlot().getRangeAxis();
		axis.setTickLabelsVisible(visible);
		axis.setLabel(visible ? leftTitle : null);
	}

	/**
	 * Synchronize domain axis with a given domain axis. The top domain axis is
	 *  only synchronized if enabled. See {@link #setTopAxisDomainSynchro(boolean)}
	 *  for that.
	 *
	 * @param ref The axis to use as reference.
	 */
	public void initDomainsRangeWith(ValueAxis ref) {
		initDomainsRangeWith(0, ref);
		if (topAxisDomainSynchro) {
			initDomainsRangeWith(1, ref);
		}
	}

	/**
	 * Initialize the domain range with the given index with an axis of reference.
	 *
	 * @param index The index of the domain axis to change.
	 * @param ref The axis to use as reference.
	 */
	private void initDomainsRangeWith(int index, ValueAxis ref) {
		ValueAxis axis = getPlot().getDomainAxis(index);
		if (axis != ref) {
			axis.setRange(ref.getRange());
			axis.setFixedDimension(ref.getFixedDimension());
		}
	}

	/**
	 * Synchronize both range axis with a given reference axis.
	 *
	 * @param ref The axis to use as reference.
	 */
	public void synchronizeRangesWith(ValueAxis ref) {
		synchronizeRangesWith(0, ref);
		synchronizeRangesWith(1, ref);
	}

	/**
	 * Synchronize both range axis.
	 */
	public void synchronizeRanges() {
		synchronizeRangesWith(getPlot().getRangeAxis());
	}

	/**
	 * Synchronize the range of range axis with the given index with a given
	 *  axis of reference.
	 *
	 * @param index The index of the range axis to change.
	 * @param ref The reference axis to use.
	 */
	private void synchronizeRangesWith(int index, ValueAxis ref) {
		ValueAxis axis = getPlot().getRangeAxis(index);
		if (axis != ref) {
			axis.setRange(ref.getRange());
		}
	}

	/**
	 * Change the value of the synchronization of the top axis.
	 *
	 * @param synchro The value to set.
	 */
	public void setTopAxisDomainSynchro(boolean synchro) {
		topAxisDomainSynchro = synchro;
	}

	/**
	 * Use a reference plot to copy it range margin to this plot.
	 *
	 * @param refPlot The plot used as reference.
	 */
	public void copyRangeMargins(XYPlot refPlot) {
		plot.getRangeAxis().setLowerMargin(refPlot.getRangeAxis().getLowerMargin());
		plot.getRangeAxis().setUpperMargin(refPlot.getRangeAxis().getUpperMargin());
		plot.getRangeAxis(1).setLowerMargin(refPlot.getRangeAxis(1).getLowerMargin());
		plot.getRangeAxis(1).setUpperMargin(refPlot.getRangeAxis(1).getUpperMargin());
	}

	/**
	 * Set the max value for the range between two axis.
	 *
	 * @param axisOne The first axis.
	 * @param axisTwo The second axis.
	 * @return The axis whit the lower/upper range set.
	 * @throws CloneNotSupportedException In case of clone exception.
	 */
	public static ValueAxis setMaxValueAxis(ValueAxis axisOne, ValueAxis axisTwo)
			throws CloneNotSupportedException {
		if (axisOne == null) {
			return (ValueAxis) axisTwo.clone();
		}
		double lowerOne = axisOne.getRange().getLowerBound();
		double upperOne = axisOne.getRange().getUpperBound();
		double lowedTwo = axisTwo.getRange().getLowerBound();
		double upperTwo = axisTwo.getRange().getUpperBound();
		axisOne.setRange(Math.min(lowerOne, lowedTwo), Math.max(upperOne, upperTwo));
		return axisOne;
	}

	/**
	 * Remove all series.
	 */
	public void removeAllSeries() {
		topLineSeriesCollection.removeAllSeries();;
		topLineErrorSeriesCollection.removeAllSeries();
		centerSeriesCollection.removeAllSeries();
		bottomLineSignalSeriesCollection.removeAllSeries();
		bottomLineImageSeriesCollection.removeAllSeries();
	}

	/**
	 * Add given markers to the plot.
	 *
	 * @param markers The markers to add.
	 * @param displayLabel True to display the label (integral), false to hide it.
	 */
	public void addMarkers(List<InterValMarkerCassis> markers, boolean displayLabel) {
		if (markers == null || markers.isEmpty()) {
			return;
		}
		for (InterValMarkerCassis marker : markers) {
			if (displayLabel) {
				plot.addDomainMarker(marker, Layer.BACKGROUND);
			} else {
				InterValMarkerCassis iMarker = new InterValMarkerCassis(marker.getStartValue(),
						marker.getEndValue(), marker.getPaint());
				plot.addDomainMarker(iMarker, Layer.BACKGROUND);
			}
		}
	}

}
