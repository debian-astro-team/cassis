/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.simple;

import org.jfree.chart.axis.LogAxis;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.ValueAxisPlot;
import org.jfree.data.Range;

/**
 * Simple LogAxis class with change to autoAdjustRange to fix a bug.
 *
 * @author M. Boiziot
 */
public class LogAxisCassis extends LogAxis {

	private static final long serialVersionUID = -5160216147990973099L;


	/**
	 * Creates a new <code>LogAxisB</code> with the given label.
	 *
	 * @param label  the axis label (<code>null</code> permitted).
	 */
	public LogAxisCassis(String label) {
		super(label);
	}

	/**
	 * Same as LogAxis, but set the Range only if max &gt; min.
	 *
	 * Adjusts the axis range to match the data range that the axis is
	 * required to display.
	 *
	 */
	@Override
	protected void autoAdjustRange() {
		Plot plot = getPlot();
		if (plot == null) {
			return;  // no plot, no data
		}

		if (plot instanceof ValueAxisPlot) {
			ValueAxisPlot vap = (ValueAxisPlot) plot;

			Range r = vap.getDataRange(this);
			if (r == null) {
				r = getDefaultAutoRange();
			}

			double upper = r.getUpperBound();
			double lower = Math.max(r.getLowerBound(), getSmallestValue());
			double range = upper - lower;

			// if fixed auto range, then derive lower bound...
			double fixedAutoRange = getFixedAutoRange();
			if (fixedAutoRange > 0.0) {
				lower = Math.max(upper - fixedAutoRange, getSmallestValue());
			}
			else {
				// ensure the autorange is at least <minRange> in size...
				double minRange = getAutoRangeMinimumSize();
				if (range < minRange) {
					double expand = (minRange - range) / 2;
					upper = upper + expand;
					lower = lower - expand;
				}

				// apply the margins - these should apply to the exponent range
				double logUpper = calculateLog(upper);
				double logLower = calculateLog(lower);
				double logRange = logUpper - logLower;
				logUpper = logUpper + getUpperMargin() * logRange;
				logLower = logLower - getLowerMargin() * logRange;
				upper = calculateValue(logUpper);
				lower = calculateValue(logLower);
			}

			if (upper > lower) {
				setRange(new Range(lower, upper), false, false);
			}
		}
	}

}
