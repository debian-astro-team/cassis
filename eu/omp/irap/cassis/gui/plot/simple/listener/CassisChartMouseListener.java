/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.simple.listener;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.swing.SwingUtilities;

import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.event.ChartProgressEvent;
import org.jfree.chart.event.ChartProgressListener;
import org.jfree.chart.plot.Marker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.Layer;

import eu.omp.irap.cassis.gui.fit.advanced.interfaces.FitLineEstimable;
import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassisCollection;
import eu.omp.irap.cassis.gui.plot.line.LineSpectrumModel;
import eu.omp.irap.cassis.gui.plot.popup.MessageControl;
import eu.omp.irap.cassis.gui.plot.simple.SpectrumPlot;
import eu.omp.irap.cassis.gui.plot.util.InterValMarkerCassis;
import eu.omp.irap.cassis.gui.plot.util.LineInfoPopup;
import eu.omp.irap.cassis.gui.plot.util.MarkerManager;
import eu.omp.irap.cassis.gui.plot.util.NearestLineInfo;
import eu.omp.irap.cassis.gui.plot.util.SpectrumFitPanelInterface;

/**
 * Listener for a event (mainly mouse related) on CASSIS Plot.
 *
 * @author M. Boiziot
 */
public class CassisChartMouseListener extends MouseAdapter implements ChartMouseListener, ChartProgressListener {

	public static final double TOP_SIZE = 0.1;
	public static final double BOTTOM_SIZE = 0.1;

	private enum POSITION { TOP, CENTER, BOTTOM, OUTSIDE };
	private SpectrumPlot spectrumPlot;
	protected NearestLineInfo nearestLineInfo;
	protected MessageControl mc;
	private LineInfoPopup popup;
	private MarkerManager markerManager;
	private FitLineEstimable estimable;
	private SpectrumFitPanelInterface fitInterface;
	protected double domainCrosshairValue;
	private double mousePressX;


	/**
	 * Constructor.
	 *
	 * @param spectrumPlot The plot on which the event will be added latter.
	 * @param nearestLineInfo The interface allowing to get informations on lines.
	 * @param markerManager The marker manager.
	 * @param mc The message control.
	 * @param estimable The fit interface to estimate lines.
	 * @param fitInterface The fit interface.
	 */
	public CassisChartMouseListener(SpectrumPlot spectrumPlot, NearestLineInfo nearestLineInfo,
			MarkerManager markerManager, MessageControl mc, FitLineEstimable estimable,
			SpectrumFitPanelInterface fitInterface) {
		super();
		this.spectrumPlot = spectrumPlot;
		this.nearestLineInfo = nearestLineInfo;
		this.markerManager = markerManager;
		this.mc = mc;
		this.estimable = estimable;
		this.fitInterface = fitInterface;
		this.popup = new LineInfoPopup();
		domainCrosshairValue = Double.NaN;
	}

	/**
	 * Change the marker manager.
	 *
	 * @param markerManager the marker manager to use.
	 */
	public final void setMarkerManager(MarkerManager markerManager) {
		this.markerManager = markerManager;
	}

	/**
	 * Check then return if the given {@link ChartMouseEvent} is a left click.
	 *
	 * @param event The event.
	 * @return true if this is a left click, false otherwise.
	 */
	private boolean isLeftClick(ChartMouseEvent event) {
		return SwingUtilities.isLeftMouseButton(event.getTrigger());
	}

	/**
	 * Return if the Alt modifier is down on the given event.
	 *
	 * @param event The event.
	 * @return true if the Alt modifier is down on the event, false otherwise.
	 */
	private boolean isAltDown(ChartMouseEvent event) {
		return event.getTrigger().isAltDown();
	}

	/**
	 * Return if the Shift modifier is down on the given event.
	 *
	 * @param event The event.
	 * @return true if the Shift modifier is down on the event, false otherwise.
	 */
	private boolean isShiftDown(MouseEvent event) {
		return event.isShiftDown();
	}

	/**
	 * Check then return if the given event is on the top of the plot (but
	 *  inside the data area).
	 *
	 * @param event The event.
	 * @return true if the event is in the top of the plot (but inside the data
	 *  area), false otherwise.
	 */
	private boolean isEventOnTop(MouseEvent event) {
		Rectangle2D area = spectrumPlot.getChartPanel().getScreenDataArea();
		int yScreen = event.getY();
		return isEventInside(event) &&
				yScreen < area.getY() + area.getHeight() * TOP_SIZE;
	}

	/**
	 * Check then return if the given event is on the bottom of the plot (but
	 *  inside the data area).
	 *
	 * @param event The event.
	 * @return true if the event is in the top of the plot (but inside the data
	 *  area), false otherwise.
	 */
	private boolean isEventOnBottom(MouseEvent event) {
		Rectangle2D area = spectrumPlot.getChartPanel().getScreenDataArea();
		int yScreen = event.getY();
		return isEventInside(event) &&
				yScreen > area.getY() + area.getHeight() * (1 - BOTTOM_SIZE);
	}

	/**
	 * Check then return if the given event is on the center of the plot (inside
	 *  the data area and more precisely, the spectrum area).
	 *
	 * @param event The event.
	 * @return true if the event is in the spectrum area of the plot (the center),
	 *  false otherwise.
	 */
	private boolean isEventOnCenter(MouseEvent event) {
		return isEventInside(event) && !isEventOnBottom(event) && !isEventOnTop(event);
	}

	/**
	 * Check then return if the given event is inside the data area (top, center
	 *  or bottom) part of the plot.
	 *
	 * @param event The event.
	 * @return true if the given event is inside the data area (top, center
	 *  or bottom) part of the plot, false otherwise.
	 */
	private boolean isEventInside(ChartMouseEvent event) {
		return isEventInside(event.getTrigger());
	}

	/**
	 * Check then return if the given event is inside the data area (top, center
	 *  or bottom) part of the plot.
	 *
	 * @param event The event.
	 * @return true if the given event is inside the data area (top, center
	 *  or bottom) part of the plot, false otherwise.
	 */
	private boolean isEventInside(MouseEvent event) {
		double xValue = getXDataValue(event);
		double yValue = getYDataValue(event);
		XYPlot plot = spectrumPlot.getPlot();
		return xValue >= plot.getDomainAxis().getLowerBound()
				&& xValue <= plot.getDomainAxis().getUpperBound()
				&& yValue >= plot.getRangeAxis().getLowerBound()
				&& yValue <= plot.getRangeAxis().getUpperBound();
	}

	/**
	 * Search then return the position of the given event.
	 *
	 * @param event The event.
	 * @return The position.
	 */
	private POSITION getPosition(ChartMouseEvent event) {
		return getPosition(event.getTrigger());
	}

	/**
	 * Search then return the position of the given event.
	 *
	 * @param event The event.
	 * @return The position.
	 */
	private POSITION getPosition(MouseEvent event) {
		if (isEventOnTop(event)) {
			return POSITION.TOP;
		} else if (isEventOnBottom(event)) {
			return POSITION.BOTTOM;
		} else if (isEventOnCenter(event)) {
			return POSITION.CENTER;
		} else {
			return POSITION.OUTSIDE;
		}
	}

	/**
	 * Display line information for a given event and on a given dataset.
	 *
	 * @param mouseEvent The event.
	 * @param dataset The dataset.
	 */
	private void displayLineInformation(MouseEvent mouseEvent, XYSeriesCollection dataset) {
		int xScreen = mouseEvent.getX();
		int yScreen = mouseEvent.getY();
		double xValue = getXDataValue(mouseEvent);
		double yValue = getYDataValue(mouseEvent);
		Double[] xValueReturn = { Double.NaN };
		String info = nearestLineInfo.getNearestLineInfo(xValue, yValue, xValueReturn, dataset);
		if (!Double.isNaN(xValueReturn[0])) {
			displayPopup(mouseEvent, xScreen, yScreen, xValueReturn, info);
		} else {
			setDomainCrosshairValue(xValue);
		}
	}

	/**
	 * Display information on a top line.
	 *
	 * @param mouseEvent The event.
	 */
	private void displayTopLineInformation(MouseEvent mouseEvent) {
		displayLineInformation(mouseEvent, spectrumPlot.getTopLineSeriesCollection());
	}

	/**
	 * Create a new collection from two given dataset.
	 *
	 * @param collectionOne The first dataset.
	 * @param collectionTwo The second dataset.
	 * @return the new collection.
	 */
	private XYSeriesCassisCollection mergeDataset(
			XYSeriesCassisCollection datasetOne,
			XYSeriesCassisCollection datasetTwo) {
		XYSeriesCassisCollection newCollection = new XYSeriesCassisCollection();
		newCollection.addSeriesList(datasetOne.getSeries());
		newCollection.addSeriesList(datasetTwo.getSeries());
		return newCollection;
	}

	/**
	 * Display an information pop-up.
	 *
	 * @param mouseEvent The event.
	 * @param xScreen The x value on screen.
	 * @param yScreen The y value on screen.
	 * @param xValueReturn The return value.
	 * @param info The information to display.
	 */
	private void displayPopup(MouseEvent mouseEvent, int xScreen, int yScreen,
			Double[] xValueReturn, String info) {
		domainCrosshairValue = Double.NaN;
		if (!Double.isNaN(xValueReturn[0]) && !"null".equals(info)
				&& !"No synthetic line found".equals(info)) {
			if (mc != null) {
				mc.addMessagePanel(info, false, xScreen + 10, yScreen + 16,
						getCurrentSpectrumNumber());
			} else {
				LineInfoPopup linePopup = new LineInfoPopup();
				linePopup.display(mouseEvent, info);
			}
			domainCrosshairValue = xValueReturn[0];
			setDomainCrosshairValue(domainCrosshairValue);
		}
	}

	/**
	 * Display information on a bottom line.
	 *
	 * @param mouseEvent The event.
	 */
	private void displayBottomLineInformation(MouseEvent mouseEvent) {
		XYSeriesCassisCollection bottomDataset = mergeDataset(
				spectrumPlot.getBottomLineSignalSeriesCollection(),
				spectrumPlot.getBottomLineImageSeriesCollection());
		displayLineInformation(mouseEvent, bottomDataset);
	}

	/**
	 * Display information on a center line.
	 *
	 * @param mouseEvent The event.
	 */
	private void displayCenterLineInformation(MouseEvent mouseEvent) {
		displayLineInformation(mouseEvent, spectrumPlot.getCenterXYSeriesCollection());
	}

	/**
	 * Display quick information on a top line.
	 *
	 * @param me The event.
	 */
	private void displayQuickTopLineInformation(MouseEvent me) {
		displayQuickLineInformation(spectrumPlot.getTopLineSeriesCollection(), me);
	}

	/**
	 * Display quick information on a bottom line.
	 *
	 * @param me The event.
	 */
	private void displayQuickBottomLineInformation(MouseEvent me) {
		XYSeriesCassisCollection bottomDataset = mergeDataset(
				spectrumPlot.getBottomLineSignalSeriesCollection(),
				spectrumPlot.getBottomLineImageSeriesCollection());
		displayQuickLineInformation(bottomDataset, me);
	}

	/**
	 * Display a quick line information for a given collection and an event.
	 *
	 * @param ds The collection.
	 * @param me The event.
	 */
	private void displayQuickLineInformation(XYSeriesCassisCollection ds, MouseEvent me) {
		if (ds.getSeriesCount() >= 1) {
			Double[] xValueReturn = { Double.NaN };
			double xValue = getXDataValue(me);
			double yValue = getYDataValue(me);

			String res = nearestLineInfo.getNearestLineInfo(xValue, yValue, xValueReturn, ds);

			if (!Double.isNaN(xValueReturn[0])) {
				popup.display(me, res);
			}
		}
	}

	/**
	 * Handle a center mouse click.
	 *
	 * @param event The event.
	 */
	private void handleCenterMouseClick(ChartMouseEvent event) {
		if (isAltDown(event)) {
			configureZoom(false, false);
		} else {
			configureZoom(true, false);
			displayCenterLineInformation(event.getTrigger());
		}
	}

	/**
	 * Configure the zoom.
	 *
	 * @param mouseZoomable The value to set for MouseZoomable.
	 * @param fillZoomRectangle The value to set for FillZoomRectangle.
	 */
	private void configureZoom(boolean mouseZoomable, boolean fillZoomRectangle) {
		spectrumPlot.getChartPanel().setMouseZoomable(mouseZoomable);
		spectrumPlot.getChartPanel().setFillZoomRectangle(fillZoomRectangle);
	}

	/**
	 * Refresh the pop-ups.
	 *
	 * @param cat The index.
	 */
	public void refreshPopups(int cat) {
		if (mc != null) {
			mc.refreshPopups(cat);
		}
	}

	/**
	 * Display the coordinate for the given event.
	 *
	 * @param me The event.
	 */
	private void displayCoordinate(MouseEvent me) {
		double mouseMoveXBottom = getXDataValue(me);
		double mouseMoveXTop = getXDataValue(me, 1);
		double mouseMoveY = getYDataValue(me);

		List<String> coord = new ArrayList<>();
		coord.add(nearestLineInfo.getBottomAxis() + " = " + mouseMoveXBottom);
		coord.add(nearestLineInfo.getTopAxis() + " = " + mouseMoveXTop);
		coord.add(nearestLineInfo.getLeftAxis() + " = " +  mouseMoveY);

		LineInfoPopup info = new LineInfoPopup();
		info.display(me, coord);
	}

	/**
	 * Handle a click on the chart.
	 *
	 * @param event The event.
	 * @see org.jfree.chart.ChartMouseListener#chartMouseClicked(org.jfree.chart.ChartMouseEvent)
	 */
	@Override
	public void chartMouseClicked(ChartMouseEvent event) {
		if (isLeftClick(event)) {
			switch (getPosition(event)) {
			case TOP:
				// Do nothing
				break;
			case BOTTOM:
				// Do nothing
				break;
			case CENTER:
				handleCenterMouseClick(event);
				break;
			case OUTSIDE:
			default:
				refreshPopups();
				break;
			}
		}
	}

	/**
	 * Handle a movement of the mouse on the chart.
	 *
	 * @param event The event.
	 * @see org.jfree.chart.ChartMouseListener#chartMouseMoved(org.jfree.chart.ChartMouseEvent)
	 */
	@Override
	public void chartMouseMoved(ChartMouseEvent event) {
		popup.hide();
		if (isEventInside(event)) {
			if (isShiftDown(event.getTrigger()) ) {
				displayCoordinate(event.getTrigger());
			} else {
				switch (getPosition(event)) {
				case TOP:
					displayQuickTopLineInformation(event.getTrigger());
					break;
				case BOTTOM:
					displayQuickBottomLineInformation(event.getTrigger());
					break;
				default:
					// Do nothing.
					break;
				}
			}
		}
	}

	/**
	 * Handle notification of a chart progress event.
	 *
	 * @param event The event.
	 * @see org.jfree.chart.event.ChartProgressListener#chartProgress(org.jfree.chart.event.ChartProgressEvent)
	 */
	@Override
	public void chartProgress(ChartProgressEvent event) {
		if (event.getType() != ChartProgressEvent.DRAWING_FINISHED) {
			return;
		}

		if (!Double.isNaN(domainCrosshairValue) && spectrumPlot.getPlot().getDomainCrosshairValue() != domainCrosshairValue) {
			setDomainCrosshairValue(domainCrosshairValue);
			return;
		}
		repaintCat();
	}

	/**
	 * Repaint the layeredPane.
	 */
	private void repaintCat() {
		if (mc != null) {
			mc.repaintCat(getCurrentSpectrumNumber());
		}
	}

	/**
	 * Return the current spectrum number.
	 *
	 * @return the current spectrum number.
	 */
	private int getCurrentSpectrumNumber() {
		int currentSpec = 0;
		if (nearestLineInfo instanceof LineSpectrumModel) {
			currentSpec = ((LineSpectrumModel) nearestLineInfo).getStackMosaicModel().getCurrentSpectrum();
		}
		return currentSpec;
	}

	/**
	 * Refresh the pop-ups.
	 */
	private void refreshPopups() {
		refreshPopups(getCurrentSpectrumNumber());
	}

	/**
	 * Handle a mouse click on the chart.
	 *
	 * @event The event.
	 * @see java.awt.event.MouseAdapter#mouseClicked(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseClicked(MouseEvent event) {
		if (SwingUtilities.isLeftMouseButton(event)) {
			switch (getPosition(event)) {
			case TOP:
				// Do nothing
				break;
			case BOTTOM:
				// Do nothing.
				break;
			case CENTER:
				if (event.getClickCount() == 2 && !event.isAltDown()) {
					double mouseX = getXDataValue(event);
					handleMarkersRemoval(mouseX);
				}
				break;
			case OUTSIDE:
				// Do default action
			default:
				refreshPopups();
				break;
			}
		}
	}

	/**
	 * Handles the removal of the markers that contain
	 * the given x value
	 *
	 * @param mouseX The x value
	 */
	private void handleMarkersRemoval(double mouseX) {
		XYPlot plot = spectrumPlot.getPlot();

		LinkedList<InterValMarkerCassis> listmarkToremeove = new LinkedList<>();
		@SuppressWarnings("unchecked")
		Collection<Marker> markers = plot.getDomainMarkers(Layer.BACKGROUND);
		if (markerManager != null) {
			markerManager.removeMarkerClicked(plot, mouseX, listmarkToremeove, markers);
			if (fitInterface != null) {
				fitInterface.intervalMarkerRemoved();
			}
		}
		if (estimable != null) {
			estimable.removeEstimationRange(mouseX);
		}
	}

	/**
	 * Handle a mouse press on the chart.
	 *
	 * @param e The event.
	 * @see java.awt.event.MouseAdapter#mousePressed(java.awt.event.MouseEvent)
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		switch (getPosition(e)) {
		case TOP:
			// Same action as BOTTOM
		case BOTTOM:
			repaintCat();
			break;
		case CENTER:
			if ((SwingUtilities.isMiddleMouseButton(e) && !e.isShiftDown())
					|| (SwingUtilities.isLeftMouseButton(e) && e.isAltDown())) {
				spectrumPlot.getChartPanel().setMouseZoomable(false);
				mousePressX = getXDataValue(e);
			}
			refreshPopups();
			break;
		case OUTSIDE:
		default:
			// Do nothing.
			break;
		}
	}

	/**
	 * Check if the given event is a simple left click (IE: one click without modifiers).
	 *
	 * @param e The event.
	 * @return true if the event is a simple left click, false otherwise.
	 */
	private boolean isSimpleLeftClick(MouseEvent e) {
		return SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 1
				&& !e.isShiftDown() && !e.isControlDown() && !e.isMetaDown()
				&& !e.isAltDown() &&  !e.isAltGraphDown();
	}

	/**
	 * Handle a mouse release on the chart.
	 *
	 * @param e The event.
	 * @see java.awt.event.MouseAdapter#mouseReleased(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
		switch (getPosition(e)) {
		case TOP:
			if (isSimpleLeftClick(e)) {
				displayTopLineInformation(e);
			}
			handleZoomMarker(e);
			break;
		case BOTTOM:
			if (isSimpleLeftClick(e)) {
				displayBottomLineInformation(e);
			}
			handleZoomMarker(e);
			break;
		case CENTER:
			handleZoomMarker(e);
			break;
		case OUTSIDE:
		default:
			// Do nothing.
			break;
		}
	}

	/**
	 * Handle zoom and marker if the given event is correct.
	 *
	 * @param e The event.
	 */
	private void handleZoomMarker(MouseEvent e) {
		if ((SwingUtilities.isMiddleMouseButton(e) && !e.isShiftDown())
				|| (SwingUtilities.isLeftMouseButton(e) && e.isAltDown())) {
			configureZoom(false, false);
			double mouseReleaseX = getXDataValue(e);

			if (mouseReleaseX < mousePressX) {
				double tmpX = mouseReleaseX;
				mouseReleaseX = mousePressX;
				mousePressX = tmpX;
			}

			if (fitInterface != null) {
				fitInterface.addIntervalMarker(new InterValMarkerCassis(mousePressX, mouseReleaseX));
			}
			configureZoom(true, false);
		}
	}

	/**
	 * Handle the exit of the mouse from the chart.
	 *
	 * @param e The event.
	 * @see java.awt.event.MouseAdapter#mouseExited(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseExited(MouseEvent e) {
		popup.hide();
	}

	/**
	 * Compute then return the x data value for a given event for the first axis.
	 *
	 * @param me The event.
	 * @return the x data value.
	 */
	private double getXDataValue(MouseEvent me) {
		return getXDataValue(me, 0);
	}

	/**
	 * Compute then return the x data value for a given event on a given axis.
	 *
	 * @param me The event.
	 * @param axisNumber The axis number for which we want the value.
	 * @return The x data value.
	 */
	private double getXDataValue(MouseEvent me, int axisNumber) {
		XYPlot plot = spectrumPlot.getPlot();
		return plot.getDomainAxis(axisNumber).java2DToValue(me.getX(),
				spectrumPlot.getChartPanel().getScreenDataArea(),
				plot.getDomainAxisEdge(axisNumber));
	}

	/**
	 * Compute then return the y data value for a given event.
	 *
	 * @param me The event.
	 * @return the y data value.
	 */
	private double getYDataValue(MouseEvent me) {
		XYPlot plot = spectrumPlot.getPlot();
		return plot.getRangeAxis().java2DToValue(me.getY(),
				spectrumPlot.getChartPanel().getScreenDataArea(),
				plot.getRangeAxisEdge());
	}

	/**
	 * Change the domainCrosshairValue and set it on the plot.
	 *
	 * @param value The new value to set.
	 */
	private void setDomainCrosshairValue(double value) {
		domainCrosshairValue = value;
		spectrumPlot.getPlot().setDomainCrosshairValue(value);
	}
}
