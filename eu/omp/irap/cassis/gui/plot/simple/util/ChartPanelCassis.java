/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.simple.util;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;

import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.gui.fit.advanced.gui.AdvancedFitFrame;
import eu.omp.irap.cassis.gui.fit.advanced.interfaces.FitLineEstimable;
import eu.omp.irap.cassis.gui.plot.simple.ChangeRenderingInterface;

/**
 * ChartPanel class who add a popup menu allowing to change the rendering type.
 *
 * @author M. Boiziot
 */
public class ChartPanelCassis extends ChartPanel {

	private static final long serialVersionUID = 5848988755066976909L;
	private ChangeRenderingInterface crInterface;
	private FitLineEstimable lineEstimable;

	private JPopupMenu defaultPopupMenu;


	public ChartPanelCassis(JFreeChart chart, int width, int height,
			int minimumDrawWidth, int minimumDrawHeight, int maximumDrawWidth,
			int maximumDrawHeight, boolean useBuffer, boolean properties, boolean copy,
			boolean save, boolean print, boolean zoom, boolean tooltips) {
		super(chart, width, height, minimumDrawWidth, minimumDrawHeight,
				maximumDrawWidth, maximumDrawHeight, useBuffer, properties, copy, save,
				print, zoom, tooltips);
	}

	/**
	 * Change the ChangeRenderingInterface.
	 *
	 * @param newCrInterface the new ChangeRenderingInterface.
	 */
	public void setChangeRenderingInterface(ChangeRenderingInterface newCrInterface) {
		this.crInterface = newCrInterface;
	}

	/**
	 * Change the FitLineEstimable
	 *
	 * @param lineEstimable the new FitLineEstimable
	 */
	public void setLineEstimable(FitLineEstimable lineEstimable) {
		this.lineEstimable = lineEstimable;
	}

	/**
	 * Do the same as the super class but also add a rendering menu.
	 *
	 * @see org.jfree.chart.ChartPanel#createPopupMenu(boolean, boolean, boolean, boolean, boolean)
	 */
	@Override
	protected JPopupMenu createPopupMenu(boolean properties,
			boolean copy, boolean save, boolean print, boolean zoom) {
		JPopupMenu jpm = super.createPopupMenu(properties, copy, save, print, zoom);
		JMenu renderingMenu = new JMenu("Rendering");

		renderingMenu.add(createHistogramMenuItem());
		renderingMenu.add(createDotMenuItem());
		renderingMenu.add(createLineMenuItem());
		jpm.addSeparator();
		jpm.add(renderingMenu);
		this.defaultPopupMenu = jpm;
		return jpm;
	}

	/**
	 * Create and return a Line JMenuItem with action to change to a line rendering.
	 *
	 * @return a Line JMenuItem with action to change to a line rendering.
	 */
	private JMenuItem createLineMenuItem() {
		JMenuItem lineMenuItem = new JMenuItem("Line");
		lineMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (crInterface != null) {
					crInterface.setLineRendering();
				}
			}
		});
		return lineMenuItem;
	}

	/**
	 * Create and return a Line JMenuItem with action to change to a dot rendering.
	 *
	 * @return a Line JMenuItem with action to change to a dot rendering.
	 */
	private JMenuItem createDotMenuItem() {
		JMenuItem dotMenuItem = new JMenuItem("Dot");
		dotMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (crInterface != null) {
					crInterface.setDotRendering();
				}
			}
		});
		return dotMenuItem;
	}

	/**
	 * Create and return a Line JMenuItem with action to change to a histogram rendering.
	 *
	 * @return a Line JMenuItem with action to change to a histogram rendering.
	 */
	private JMenuItem createHistogramMenuItem() {
		JMenuItem histogramMenuItem = new JMenuItem("Histogram");
		histogramMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (crInterface != null) {
					crInterface.setHistogramRendering();
				}
			}
		});
		return histogramMenuItem;
	}

	/**
	 * The idea is to modify the zooming options depending on the type of chart
	 * being displayed by the panel.
	 *
	 * @param x horizontal position of the popup.
	 * @param y vertical position of the popup.
	 * @see org.jfree.chart.ChartPanel#displayPopupMenu(int, int)
	 */
	@Override
	protected void displayPopupMenu(int x, int y) {
		JPopupMenu menu = defaultPopupMenu;
		if (isOnBottom(x, y) && lineEstimable != null && lineEstimable.isFittable()) {
			menu = createBottomPopupMenu();
		} else if (isOnTop(x, y) && lineEstimable != null && lineEstimable.isFittable()) {
			menu = createTopPopupMenu();
		}
		if (menu != null) {
			setPopupMenu(menu);
			super.displayPopupMenu(x, y);
		}
	}

	/**
	 * Creates and return the JPopupMenu associated with the top lines
	 *
	 * @return The JPopupMenu
	 */
	private JPopupMenu createTopPopupMenu() {
		List<LineDescription> lines = lineEstimable.getFitSource().getTopPossibleLines();
		if (!lines.isEmpty()) {
			final Map<Integer, List<LineDescription>> sortedLines = sortBySpecies(lines);
			if (sortedLines.keySet().size() <= 10) {
				JPopupMenu menu = new JPopupMenu();
				if (sortedLines.keySet().size() > 1) {
					menu.add(getFitAllSpeciesInOneMenuItem(lines));
					menu.add(getFitAllSpeciesMenuItem(sortedLines.values()));
					menu.addSeparator();
				}
				for (List<LineDescription> speciesLines : sortedLines.values()) {
					menu.add(getMenuItemForSpecies(speciesLines));
				}
				return menu;
			}
		}
		return null;
	}

	/**
	 * Creates and return the JPopupMenu associated with the bottom lines
	 *
	 * @return The JPopupMenu
	 */
	private JPopupMenu createBottomPopupMenu() {
		List<LineDescription> lines = lineEstimable.getFitSource().getBottomPossibleLines();
		if (!lines.isEmpty()) {
			final Map<Integer, List<LineDescription>> sortedLines = sortBySpecies(lines);
			if (sortedLines.keySet().size() <= 10) {
				JPopupMenu menu = new JPopupMenu();
				if (sortedLines.keySet().size() > 1) {
					menu.add(getFitAllSpeciesInOneMenuItem(lines));
					menu.add(getFitAllSpeciesMenuItem(sortedLines.values()));
					menu.addSeparator();
				}
				for (List<LineDescription> speciesLines : sortedLines.values()) {
					menu.add(getMenuItemForSpecies(speciesLines));
				}
				return menu;
			}
		}
		return null;
	}

	private JMenuItem getFitAllSpeciesInOneMenuItem(final List<LineDescription> lines) {
		JMenuItem item = new JMenuItem("Fit all species in one");
		item.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				lineEstimable.estimateLinesForCurrentModel(lines);
				AdvancedFitFrame.getFrame().openFrame(true);
			}
		});
		return item;
	}

	/**
	 * Creates then return a JMenuItem that estimates a component for each
	 * element of the given collection, using the corresponding list.
	 *
	 * @param lines A collections of list of lines
	 * @return A JMenuItem that starts the estimation of every list of lines in different
	 * components
	 */
	private JMenuItem getFitAllSpeciesMenuItem(final Collection<List<LineDescription>> lines) {
		JMenuItem item = new JMenuItem("Fit all species separately");
		item.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				for (List<LineDescription> speciesLine : lines) {
					List<LineDescription> lines = new ArrayList<>(speciesLine);
					lineEstimable.estimateLinesForCurrentModel(lines);
				}
				AdvancedFitFrame.getFrame().openFrame(true);
			}
		});
		return item;
	}

	/**
	 * Sorts all the lines in the given list in several lists, each
	 * corresponding to all the lines belonging to the same species
	 *
	 * @param lines The full lines
	 * @return The lines, sorted by species
	 */
	private Map<Integer, List<LineDescription>> sortBySpecies(List<LineDescription> lines) {
		Map<Integer, List<LineDescription>> sortedLines = new HashMap<>();
		for (LineDescription line : lines) {
			if (!sortedLines.containsKey(line.getMolTag())) {
				sortedLines.put(line.getMolTag(), new ArrayList<>());
			}
			sortedLines.get(line.getMolTag()).add(line);
		}
		return sortedLines;
	}

	/**
	 * Creates and return a JMenuItem that let the user fit one specific species
	 *
	 * @param speciesLines The lines of the species to display
	 * @return The created JMenuItem
	 */
	private JMenuItem getMenuItemForSpecies(final List<LineDescription> speciesLines) {
		JMenuItem menuItem = new JMenuItem("Fit " + speciesLines.get(0).getMolName());
		menuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				lineEstimable.estimateLinesForCurrentModel(speciesLines);
			}
		});
		return menuItem;
	}

	/**
	 * Check then return if the given position are on the bottom of the plot
	 *  (inside the plot area, not in the top neither in the center).
	 *
	 * @param x The x position.
	 * @param y The y position.
	 * @return true if the given position is on bottom, false otherwise.
	 */
	private boolean isOnBottom(int x, int y) {
		Rectangle2D area = getScreenDataArea();
		return x >= area.getMinX() && x <= area.getMaxX()
				&& y > area.getY() + area.getHeight() * 0.9;
	}

	/**
	 * Check then return if the given position are on the top of the plot
	 *  (inside the plot area, not in the bottom neither in the center).
	 *
	 * @param x The x position.
	 * @param y The y position.
	 * @return true if the given position is on top, false otherwise.
	 */
	private boolean isOnTop(int x, int y) {
		Rectangle2D area = getScreenDataArea();
		return x >= area.getMinX() && x <= area.getMaxX()
				&& y < area.getY() + area.getHeight() * 0.1;
	}

}
