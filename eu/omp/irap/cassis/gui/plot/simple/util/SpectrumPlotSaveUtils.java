/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.simple.util;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Stroke;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.jfree.chart.axis.AxisSpace;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.renderer.xy.XYStepRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassis;
import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassisCollection;
import eu.omp.irap.cassis.gui.plot.popup.MessageControl;
import eu.omp.irap.cassis.gui.plot.popup.MessageModel;
import eu.omp.irap.cassis.gui.plot.popup.MessagePanel;
import eu.omp.irap.cassis.gui.plot.save.RestorationInterface;
import eu.omp.irap.cassis.gui.plot.save.SaveGraphic;
import eu.omp.irap.cassis.gui.plot.simple.SpectrumPlot;
import eu.omp.irap.cassis.gui.plot.simple.renderer.XYDotBySeriesRenderer;

/**
 * Utility class used to save the SpectrumPlot.
 *
 * @author M. Boiziot
 */
public class SpectrumPlotSaveUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpectrumPlotSaveUtils.class);


	/**
	 * Save the given plot in a PDF.
	 *
	 * @param spectrumPlot The plot.
	 * @param restoreInterface The restoration interface.
	 * @param messageControl The message control.
	 * @param file The file used to save the plot.
	 */
	public static void saveToPdf(SpectrumPlot spectrumPlot,
			RestorationInterface restoreInterface,
			MessageControl messageControl,File file) {
		saveToPdfOrGraphics(spectrumPlot, restoreInterface,
				messageControl, true, file);
	}

	/**
	 * Save the given plot in a Graphics2D.
	 *
	 * @param spectrumPlot The plot.
	 * @param restoreInterface The restoration interface.
	 * @param messageControl The message control.
	 * @param graphics The Graphics2D used to save the plot.
	 */
	public static void saveToGraphics(SpectrumPlot spectrumPlot,
			RestorationInterface restoreInterface, MessageControl messageControl,
			Graphics2D graphics) {
		saveToPdfOrGraphics(spectrumPlot, restoreInterface, messageControl,
				false, graphics);
	}

	/**
	 * Save the given SpectrumPlot as a PNG file.
	 *
	 * @param spectrumPlot The plot to save.
	 * @param file The file used for the save.
	 * @param messageControl The message control.
	 * @return true if the save succeeded, false otherwise.
	 */
	public static boolean saveToPng(SpectrumPlot spectrumPlot, File file,
			MessageControl messageControl) {
		BufferedImage bi = getImageToSave(spectrumPlot, messageControl);
		try {
			ImageIO.write(bi, "png", file);
		} catch (IOException e) {
			LOGGER.error("Error while saving the png image", e);
			return false;
		}
		return true;
	}

	/**
	 * Configure the plot for save, create a BufferedImage, restore the plot then
	 *  return the BufferedImage.
	 *
	 * @param spectrumPlot The plot to save.
	 * @param messageControl The message control.
	 * @return the BufferedImage.
	 */
	public static BufferedImage getImageToSave(SpectrumPlot spectrumPlot,
			MessageControl messageControl) {
		XYPlot plot = spectrumPlot.getPlot();

		// Change some settings from original view.
		plot.setDomainCrosshairVisible(false);
		plot.setDomainCrosshairLockedOnData(false);
		Paint oldColor = spectrumPlot.getJFreeChart().getBackgroundPaint();
		Color oldColorTriplePlot = spectrumPlot.getBackground();
		changeBackgroundColor(spectrumPlot, Color.WHITE, Color.WHITE);

		BufferedImage bi = new BufferedImage(spectrumPlot.getWidth(),
				spectrumPlot.getHeight(), BufferedImage.TYPE_INT_RGB);
		Graphics g = bi.createGraphics();

		messageControl.getLayeredPane().paint(g);
		g.dispose();

		// Restore the originals settings.
		plot.setDomainCrosshairVisible(true);
		plot.setDomainCrosshairLockedOnData(true);
		changeBackgroundColor(spectrumPlot, oldColor, oldColorTriplePlot);

		return bi;
	}

	/**
	 * Save the given plot to PDF or graphics.
	 *
	 * @param spectrumPlot The plot to save.
	 * @param restoreInterface The restoration interface.
	 * @param messageControl The message control.
	 * @param isPdf true to save as PDF, false to save it as a Graphics2D.
	 * @param obj The object, who must be a file for a PDF save or a Graphics2D
	 *  for a Graphics2D save.
	 */
	private static void saveToPdfOrGraphics(SpectrumPlot spectrumPlot,
			RestorationInterface restoreInterface, MessageControl messageControl,
			boolean isPdf, Object obj) {
		File file = null;
		Graphics2D g2d = null;
		if (isPdf) {
			file = (File) obj;
		} else {
			g2d = (Graphics2D) obj;
		}
		JLayeredPane layeredPane = messageControl.getLayeredPane();

		//Config
		Stroke stroke = new BasicStroke(3);
		Font font = new Font("Serif", Font.PLAIN, 36);
		Font labelFont = new Font("Serif", Font.PLAIN, 48);

		// Get the plots
		XYPlot plot = spectrumPlot.getPlot();
		List<XYSeriesCassisCollection> collections = spectrumPlot.getCollections();

		////////////////////////////////////////
		// backup settings
		Dimension dimBackup = spectrumPlot.getSize();
		Paint chartColorBackup = spectrumPlot.getJFreeChart().getBackgroundPaint();
		Color triplePlotColorBackup = spectrumPlot.getBackground();

		double topDasBackup = plot.getFixedDomainAxisSpace().getTop();
		double bottomDasBackup = plot.getFixedDomainAxisSpace().getBottom();

		double leftRasBackup = plot.getFixedRangeAxisSpace().getLeft();
		double rightRasBackup = plot.getFixedRangeAxisSpace().getRight();

		Paint linePaintBackup = plot.getDomainAxis().getAxisLinePaint();
		Stroke lineStrokeBackup = plot.getDomainAxis().getAxisLineStroke();

		Stroke tickStrokeBackup = plot.getDomainAxis().getTickMarkStroke();
		float tickMarkInsideLenghtBackup = plot.getDomainAxis().getTickMarkInsideLength();

		List<Map<String, Stroke>> backUpStroke = getStroke(collections);
		List<Map<String, List<Integer>>> backupDotSize = getDotSize(collections);

		int imageNb = messageControl.getImages().size();
		List<Point> points = new ArrayList<>(imageNb);
		List<Integer> fontSize = new ArrayList<>(imageNb);
		List<Boolean> persistant = new ArrayList<>(imageNb);
		if (imageNb > 0) {
			MessageModel mm = null;

			for (MessagePanel mp : messageControl.getImages()) {
				mm = mp.getModel();
				points.add(mp.getLocation());
				fontSize.add(mm.getFontSize());
				persistant.add(mm.isVisiblePersitent());
				mm.setFontSize(mm.getFontSize()*2);
				mm.setVisiblePersitent(true);
				mp.updatePopup();
				mm.setAllowEdit(false);
			}
		}

		///////////////////////////////////////
		///////////////////////////////////////
		Dimension dim = new Dimension(1000, 750);
		spectrumPlot.setSize(dim);
		spectrumPlot.setPreferredSize(dim);
		spectrumPlot.repaint();
		layeredPane.setSize(dim);
		layeredPane.setPreferredSize(dim);
		layeredPane.revalidate();
		layeredPane.repaint();

		changeBackgroundColor(spectrumPlot, Color.WHITE, Color.WHITE);

		configureAxis(plot, font, labelFont);

		changeAxisLinePaint(plot, Color.BLACK);
		changeAxisLineStroke(plot, stroke);
		changeTickMarkInsideLenght(plot, 20);
		changeTickMarkStroke(plot, new BasicStroke(2));

		updateSeriesForSave(plot, collections);

		plot.setDomainCrosshairVisible(false);

		spectrumPlot.initDomainAxisSpace(110, 110);

		initRangeAxisSpace(spectrumPlot.getPlot(), 220, 100);

		if (isPdf) {
			savePdf(spectrumPlot, file, layeredPane, imageNb);
		} else {
			saveGraphics(spectrumPlot, g2d, layeredPane, imageNb);
		}

		/////////////////////////////////////
		/////////////////////////////////////
		// Restore triplePlot
		restoreInterface.restorePanel(layeredPane);
		spectrumPlot.setSize(dimBackup);
		layeredPane.setSize(dimBackup);

		changeBackgroundColor(spectrumPlot, chartColorBackup, triplePlotColorBackup);
		spectrumPlot.changeFont();
		changeAxisLinePaint(plot, linePaintBackup);
		changeAxisLineStroke(plot, lineStrokeBackup);
		changeTickMarkInsideLenght(plot, tickMarkInsideLenghtBackup);
		changeTickMarkStroke(plot, tickStrokeBackup);

		restoreStroke(plot, backUpStroke, collections);
		restoreDotSize(plot, backupDotSize, collections);

		plot.setDomainCrosshairVisible(true);

		spectrumPlot.initDomainAxisSpace((int)bottomDasBackup, (int)topDasBackup);

		initRangeAxisSpace(spectrumPlot.getPlot(), leftRasBackup, rightRasBackup);

		if (imageNb > 0) {
			int i = 0;
			MessageModel mm = null;
			for (MessagePanel mp : messageControl.getImages()) {
				mm = mp.getModel();
				mp.setLocation(points.get(i));
				mm.setFontSize(fontSize.get(i));
				mm.setVisiblePersitent(persistant.get(i));
				mp.updatePopup();
				mm.setAllowEdit(true);
				i++;
			}
			layeredPane.repaint();
		}
		spectrumPlot.repaint();
	}

	/**
	 * Save the plot in a graphics.
	 *
	 * @param spectrumPlot The plot to save.
	 * @param g2d The graphics used to save the plot.
	 * @param layeredPane The layered pane.
	 * @param imageNb The number of images (pop-up).
	 */
	private static void saveGraphics(SpectrumPlot spectrumPlot, Graphics2D g2d,
			JLayeredPane layeredPane, int imageNb) {
		if (imageNb == 0) {
			layeredPane.paintComponents(g2d);
			g2d.dispose();
		} else {
			int returnValue = JOptionPane.showConfirmDialog(spectrumPlot, layeredPane,
					"Please place the popups correctly",
					JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

			if (returnValue == JOptionPane.OK_OPTION) {
				layeredPane.paintComponents(g2d);
				g2d.dispose();
			}
		}
	}

	/**
	 * Save the given plot to PDF.
	 *
	 * @param spectrumPlot The spectrum plot.
	 * @param file The file to save the PDF.
	 * @param layeredPane The layered pane.
	 * @param imageNb The number of images (pop-up).
	 */
	private static void savePdf(SpectrumPlot spectrumPlot, File file,
			JLayeredPane layeredPane, int imageNb) {
		if (imageNb == 0) {
			savePdfWithoutImage(file, layeredPane);
		} else {
			savePdfWithImage(spectrumPlot, file, layeredPane);
		}
	}

	/**
	 * Save the given the plot with image(s) in as PDF file.
	 *
	 * @param spectrumPlot The spectrum plot.
	 * @param file The file.
	 * @param layeredPane The layered pane.
	 */
	private static void savePdfWithImage(SpectrumPlot spectrumPlot, File file,
			JLayeredPane layeredPane) {
		int returnValue = JOptionPane.showConfirmDialog(spectrumPlot, layeredPane,
				"Please place the popups correctly",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

		if (returnValue == JOptionPane.OK_OPTION) {
			try {
				SaveGraphic.saveAsPDF(layeredPane, file);
			} catch (Exception e) {
				LOGGER.error("Error while saving the pdf.", e);
			}
		}
	}

	/**
	 * Save the given the plot without image in as PDF file.
	 *
	 * @param file The file to save.
	 * @param layeredPane The layered pane (who contains the plot).
	 */
	private static void savePdfWithoutImage(File file, JLayeredPane layeredPane) {
		JFrame frame = new JFrame();
		JPanel panel = new JPanel();
		panel.add(layeredPane);
		frame.setContentPane(panel);
		frame.setVisible(true);
		frame.pack();

		try {
			SaveGraphic.saveAsPDF(layeredPane, file);
		} catch (Exception e) {
			LOGGER.error("Error while saving the pdf.", e);
		}
		frame.dispose();
	}

	/**
	 * Configure the axis for the save.
	 *
	 * @param plot The XYPlot.
	 * @param font The font to use for the tick labels
	 * @param labelFont The font to use for the labels.
	 */
	private static void configureAxis(XYPlot plot, Font font, Font labelFont) {
		for (int i = 0; i <= 1; i++) {
			plot.getRangeAxis(i).setTickLabelFont(font);
			plot.getRangeAxis(i).setTickLabelPaint(Color.BLACK);
			plot.getRangeAxis(i).setTickLabelsVisible(true);
			plot.getRangeAxis(i).setLabelFont(labelFont);
			plot.getRangeAxis(i).setLabelPaint(Color.BLACK);

			plot.getDomainAxis(i).setTickLabelFont(font);
			plot.getDomainAxis(i).setTickLabelPaint(Color.BLACK);
			plot.getDomainAxis(i).setTickLabelsVisible(true);
			plot.getDomainAxis(i).setLabelFont(labelFont);
			plot.getDomainAxis(i).setLabelPaint(Color.BLACK);
		}
		plot.getRangeAxis(1).setTickLabelsVisible(false);
	}

	/**
	 * Create a backup of strokes values for the given collections.
	 *
	 * @param collections The collections.
	 * @return the created backup of strokes values.
	 */
	private static List<Map<String, Stroke>> getStroke(
			List<XYSeriesCassisCollection> collections) {
		List<Map<String, Stroke>> list = new ArrayList<>();
		for (XYSeriesCassisCollection collection : collections) {
			Map<String, Stroke> mapStrokeLineBackup =
					new HashMap<>(collection.getSeriesCount());
			for (XYSeriesCassis series : collection.getSeries()) {
				mapStrokeLineBackup.put(series.getKey().toString(),
						series.getConfigCurve().getStroke());
			}
			list.add(mapStrokeLineBackup);
		}
		return list;
	}

	/**
	 * Create a backup of the dot size (width, height) for each series for each collections.
	 *
	 * @param collections The list of collections.
	 * @return a backup of the dot size (width, height) for each series for each collections.
	 */
	private static List<Map<String, List<Integer>>> getDotSize(
			List<XYSeriesCassisCollection> collections) {
		List<Map<String, List<Integer>>> list = new ArrayList<>();
		for (XYSeriesCassisCollection collection : collections) {
			Map<String, List<Integer>> mapDotSizeBackup =
					new HashMap<>(collection.getSeriesCount());
			for (XYSeriesCassis series : collection.getSeries()) {
				List<Integer> listDotSize = new ArrayList<>(2);
				listDotSize.add(series.getConfigCurve().getDotWidthSize());
				listDotSize.add(series.getConfigCurve().getDotHeightSize());
				mapDotSizeBackup.put(series.getKey().toString(), listDotSize);
			}
			list.add(mapDotSizeBackup);
		}
		return list;
	}

	/**
	 * Change the background color.
	 *
	 * @param spectrumPlot The SpectrumPlot.
	 * @param plotColor The Paint to set for the plot inside elements.
	 * @param swingColor The color to set for the Swing part.
	 */
	private static void changeBackgroundColor(SpectrumPlot spectrumPlot,
			Paint plotColor, Color swingColor) {
		spectrumPlot.getJFreeChart().setBackgroundPaint(plotColor);
		spectrumPlot.setBackground(swingColor);
	}

	/**
	 * Change the TickMarkStroke on needed axis of top, bottom and center plot.
	 *
	 * @param plot The XYPlot.
	 * @param tickStroke The Stroke to set.
	 */
	private static void changeTickMarkStroke(XYPlot plot, Stroke tickStroke) {
		plot.getDomainAxis(0).setTickMarkStroke(tickStroke);
		plot.getDomainAxis(1).setTickMarkStroke(tickStroke);
		plot.getRangeAxis(0).setTickMarkStroke(tickStroke);
		plot.getRangeAxis(1).setTickMarkStroke(tickStroke);
	}

	/**
	 * Change the TickMarkInsideLenght.
	 *
	 * @param plot The XYPlot.
	 * @param tickMarkInsideLenght The TickMarkInsideLenght to set.
	 */
	private static void changeTickMarkInsideLenght(XYPlot plot, float tickMarkInsideLenght) {
		plot.getDomainAxis(0).setTickMarkInsideLength(tickMarkInsideLenght);
		plot.getDomainAxis(1).setTickMarkInsideLength(tickMarkInsideLenght);
		plot.getRangeAxis(0).setTickMarkInsideLength(tickMarkInsideLenght);
		plot.getRangeAxis(1).setTickMarkInsideLength(tickMarkInsideLenght);
	}

	/**
	 * Change the AxisLineStroke.
	 *
	 * @param plot The XYPlot.
	 * @param lineStroke The Stroke to set.
	 */
	private static void changeAxisLineStroke(XYPlot plot, Stroke lineStroke) {
		plot.getDomainAxis(0).setAxisLineStroke(lineStroke);
		plot.getDomainAxis(1).setAxisLineStroke(lineStroke);
		plot.getRangeAxis(0).setAxisLineStroke(lineStroke);
		plot.getRangeAxis(1).setAxisLineStroke(lineStroke);
	}

	/**
	 * Change the AxisLinePaint on needed axis of top, bottom and center plot.
	 *
	 * @param plot The XYPlot.
	 * @param linePaint The paint to set.
	 */
	private static void changeAxisLinePaint(XYPlot plot, Paint linePaint) {
		plot.getDomainAxis(0).setAxisLinePaint(linePaint);
		plot.getDomainAxis(1).setAxisLinePaint(linePaint);
		plot.getRangeAxis(0).setAxisLinePaint(linePaint);
		plot.getRangeAxis(1).setAxisLinePaint(linePaint);
	}

	/**
	 * Update the series for a PDF save.
	 *
	 * @param plot The XYPlot.
	 * @param collections The list of collections.
	 */
	private static void updateSeriesForSave(XYPlot plot, List<XYSeriesCassisCollection> collections) {
		for (XYSeriesCassisCollection collection : collections) {
			XYItemRenderer renderer = plot.getRendererForDataset(collection);
			if (renderer instanceof XYStepRenderer ||
					renderer instanceof XYLineAndShapeRenderer) {
				updateSeriesForSaveHistogramLineRendering(collection, renderer);
			} else if (renderer instanceof XYDotBySeriesRenderer) {
				updateSeriesForSaveDotRendering(collection, (XYDotBySeriesRenderer) renderer);
			} else {
				throw new IllegalArgumentException("Renderer is unknown");
			}
		}
	}

	/**
	 * Update the series of a collection for the save with a line or histogram rendering.
	 *
	 * @param collection The collection.
	 * @param renderer The renderer of the collection.
	 */
	private static void updateSeriesForSaveHistogramLineRendering(
			XYSeriesCassisCollection collection, XYItemRenderer renderer) {
		for (int i = 0; i < collection.getSeriesCount(); i++) {
			renderer.setSeriesStroke(i,
					new BasicStroke(((BasicStroke) renderer.getSeriesStroke(i))
							.getLineWidth() * 3));
		}
	}

	/**
	 * Update the series of a collection for the save with a dot rendering.
	 *
	 * @param collection The collection.
	 * @param renderer The renderer of the collection.
	 */
	private static void updateSeriesForSaveDotRendering(
			XYSeriesCassisCollection collection, XYDotBySeriesRenderer renderer) {
		for (int i = 0; i < collection.getSeriesCount(); i++) {
			renderer.setSeriesDotWidth(i, renderer.getSeriesDotWidth(i) * 2);
			renderer.setSeriesDotHeight(i, renderer.getSeriesDotHeight(i) * 2);
		}
	}

	/**
	 * Restore the Strokes.
	 *
	 * @param plot The center plot.
	 * @param backUpStroke The backup of the strokes values
	 * @param collections The list of collections.
	 */
	private static void restoreStroke(XYPlot plot,
			List<Map<String, Stroke>> backUpStroke,
			List<XYSeriesCassisCollection> collections) {
		for (int i = 0; i < collections.size(); i++) {
			restoreStroke(plot, backUpStroke.get(i), collections.get(i));
		}
	}

	/**
	 * Restore the Strokes for a collection.
	 *
	 * @param plot The plot.
	 * @param parameters The strokes values related to the collection.
	 * @param collection The collection.
	 */
	private static void restoreStroke(XYPlot plot, Map<String, Stroke> parameters,
			XYSeriesCassisCollection collection) {
		XYItemRenderer renderer = plot.getRendererForDataset(collection);
		if (renderer instanceof XYStepRenderer ||
				renderer instanceof XYLineAndShapeRenderer) {
			for (String key : parameters.keySet()) {
				int i = collection.indexOf(key);
				if (i != -1) {
					renderer.setSeriesStroke(i, parameters.get(key));
				}
			}
		}
	}

	/**
	 * Restore the dots sizes.
	 *
	 * @param plot The center plot.
	 * @param backUpDotSize The backup of the dot size
	 * @param collections The list of collections.
	 */
	private static void restoreDotSize(XYPlot plot,
			List<Map<String, List<Integer>>> backUpDotSize,
			List<XYSeriesCassisCollection> collections) {
		for (int i = 0; i < collections.size(); i++) {
			restoreDotSize(plot, backUpDotSize.get(i), collections.get(i));
		}
	}

	/**
	 * Restore the dots sizes for a collection.
	 *
	 * @param plot The plot.
	 * @param parameterMap The dots sizes values related to the collection.
	 * @param collection The collection.
	 */
	private static void restoreDotSize(XYPlot plot,
			Map<String, List<Integer>> parameterMap, XYSeriesCassisCollection collection) {
		XYItemRenderer renderer = plot.getRendererForDataset(collection);
		if (renderer instanceof XYDotBySeriesRenderer) {
			XYDotBySeriesRenderer dotSeriesRenderer = (XYDotBySeriesRenderer) renderer;
			for (String key : parameterMap.keySet()) {
				int i = collection.indexOf(key);
				if (i != -1) {
					dotSeriesRenderer.setSeriesDotWidth(i, parameterMap.get(key).get(0));
					dotSeriesRenderer.setSeriesDotHeight(i, parameterMap.get(key).get(1));
				}
			}
		}
	}

	/**
	 * Configure the fixed range axis space.
	 *
	 * @param plot The plot.
	 * @param spaceLeft The space to set on left.
	 * @param spaceRight The space to set on right.
	 */
	private static void initRangeAxisSpace(XYPlot plot, double spaceLeft, double spaceRight) {
		AxisSpace rangeAxisSpaceCenter = plot.getFixedRangeAxisSpace();
		rangeAxisSpaceCenter.setLeft(spaceLeft);
		rangeAxisSpaceCenter.setRight(spaceRight);
		plot.setFixedRangeAxisSpace(rangeAxisSpaceCenter);
	}

}
