/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.simple.util;

import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParsePosition;

import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.TickUnits;
import org.jfree.chart.axis.ValueAxis;

/**
 * @author glorian
 *
 */
public class PlotUtil {

	private static TickUnits xUnits = null;
	private static TickUnits yUnits = null;
	private final static String Y_SCIENTIFIC_PATTERN = "0.00E0";
	private final static String Y_PATTERN = "0.###";

	public static void configureYAxis(NumberAxis intensityAxis) {
		intensityAxis.setAutoRangeStickyZero(false);
		intensityAxis.setAutoRangeIncludesZero(false);
		intensityAxis.setAutoRangeMinimumSize(1E-70, true);
		intensityAxis.setStandardTickUnits(getTickUnitsY());
	}

	private static TickUnits getTickUnitsY() {
		if (yUnits == null) {
			yUnits = new TickUnits();
			TickUnits tickUnits = yUnits;
			double[] size = new double[] { -1.0, -5.0, -2.5, 1.0, 5.0, 2.5 };
			int[] valMinorTick = new int[] { 2, 5, 5, 2, 5, 5 };

			@SuppressWarnings("serial")
			NumberFormat decimalFormat = new NumberFormat() {

				@Override
				public Number parse(String source, ParsePosition parsePosition) {
					return new DecimalFormat().parse(source, parsePosition);
				}

				@Override
				public StringBuffer format(long number, StringBuffer toAppendTo,
						FieldPosition pos) {
					if ((Math.abs(number) > 999.99 || Math.abs(number) < 0.001)&& number !=0) {

						return new StringBuffer(new DecimalFormat(Y_SCIENTIFIC_PATTERN).format(number));
					} else {

						return new StringBuffer(new DecimalFormat(Y_PATTERN).format(number));
					}
				}

				@Override
				public StringBuffer format(double number, StringBuffer toAppendTo,
						FieldPosition pos) {
					if ((Math.abs(number) > 999.99 || Math.abs(number) < 0.001) && number !=0)
						return new StringBuffer(new DecimalFormat(Y_SCIENTIFIC_PATTERN).format(number));
					else
						return new StringBuffer(new DecimalFormat(Y_PATTERN).format(number));
				}
			};
			DecimalFormat scientifiqueFormat = new DecimalFormat(Y_SCIENTIFIC_PATTERN);
			NumberFormat format = null;
			for (int i = -30; i < 30; i++) {
				for (int cpt = 0; cpt < size.length; cpt++) {
					double coeff = size[cpt] * Math.pow(10, i);
					double val = Math.abs(coeff);
					if (val == 0) {
						format = decimalFormat;
					} else if  (val < 0.0001) {
						format = scientifiqueFormat;
					} else {
						format = decimalFormat;
					}
					tickUnits.add(new NumberTickUnit(coeff, format, valMinorTick[cpt]));
				}
			}
		}
		return yUnits;
	}

	public static void configureXAxis(ValueAxis intensityAxis) {
		if (intensityAxis instanceof NumberAxis){
			((NumberAxis) intensityAxis).setAutoRangeStickyZero(false);
			((NumberAxis) intensityAxis).setAutoRangeIncludesZero(false);
		}
		intensityAxis.setAutoRangeMinimumSize(1E-70, true);
		intensityAxis.setStandardTickUnits(getTickUnitsX());
	}

	private static TickUnits getTickUnitsX() {
		if (xUnits == null) {
			xUnits = new TickUnits();
			TickUnits tickUnits = xUnits;
			double[] size = new double[] { -1.0, -5.0, -2.5, 1.0, 5.0, 2.5 };
			int[] valMinorTick = new int[] { 2, 5, 5, 2, 5, 5 };

			@SuppressWarnings("serial")
			NumberFormat decimalFormat = new NumberFormat() {

				@Override
				public Number parse(String source, ParsePosition parsePosition) {
					return new DecimalFormat().parse(source, parsePosition);
				}

				@Override
				public StringBuffer format(long number, StringBuffer toAppendTo,
						FieldPosition pos) {
					if ((Math.abs(number) > 999999.99 || Math.abs(number) < 0.001)&& number !=0)
						return new StringBuffer(new DecimalFormat("0.000E0").format(number));
					else
						return new StringBuffer(new DecimalFormat("0.###").format(number));
				}

				@Override
				public StringBuffer format(double number, StringBuffer toAppendTo,
						FieldPosition pos) {
					if ((Math.abs(number) > 999999.99 || Math.abs(number) < 0.001) && number !=0)
						return new StringBuffer(new DecimalFormat("0.000E0").format(number));
					else
						return new StringBuffer(new DecimalFormat("0.###").format(number));
				}
			};
			DecimalFormat scientifiqueFormat = new DecimalFormat("0.00##E0");
			NumberFormat format = null;
			for (int i = -30; i < 30; i++) {
				for (int cpt = 0; cpt < size.length; cpt++) {
					double coeff = size[cpt] * Math.pow(10, i);
					double val = Math.abs(coeff);
					if (val == 0) {
						format = decimalFormat;
					} else if  (val < 0.0001) {
						format = scientifiqueFormat;
					} else {
						format = decimalFormat;
					}
					tickUnits.add(new NumberTickUnit(coeff, format, valMinorTick[cpt]));
				}
			}
		}
		return xUnits;
	}
}
