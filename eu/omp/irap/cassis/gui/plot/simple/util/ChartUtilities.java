/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.simple.util;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.renderer.xy.XYStepRenderer;

import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassisCollection;
import eu.omp.irap.cassis.gui.plot.simple.SpectrumPlotConstants;
import eu.omp.irap.cassis.gui.plot.simple.renderer.CassisLineErrorRenderer;
import eu.omp.irap.cassis.gui.plot.simple.renderer.CassisLineRenderer;

/**
 * Utilities class to create {@link ChartPanelCassis}.
 *
 * @author M. Boiziot
 */
public class ChartUtilities {

	public static enum POSITION { BOTTOM, LEFT }

	/**
	 * Create a {@link ChartPanelCassis} with the given parameters.
	 *
	 * @param title The title.
	 * @param xAxisLabel The x axis label.
	 * @param yAxisLabel The y axis label.
	 * @param centerDataset The center dataset.
	 * @param topLineDataset The top dataset.
	 * @param topLineErrorDataset The top dataset.
	 * @param bottomLineSignalDataset The bottom line signal dataset.
	 * @param bottomLineImageDataset The bottom line signal dataset.
	 * @param displayPopup true to display pop-up, false to do not.
	 * @return the created ChartPanelCassis.
	 */
	public static ChartPanelCassis createXYStepChart(String title, String xAxisLabel,
			String yAxisLabel, XYSeriesCassisCollection centerDataset,
			XYSeriesCassisCollection topLineDataset,
			XYSeriesCassisCollection topLineErrorDataset,
			XYSeriesCassisCollection bottomLineSignalDataset,
			XYSeriesCassisCollection bottomLineImageDataset,
			boolean displayPopup) {
		JFreeChart jfc = ChartFactory.createXYLineChart(title, xAxisLabel,
				yAxisLabel, centerDataset, PlotOrientation.VERTICAL, false, false, false);
		ChartPanelCassis chartPanel = new ChartPanelCassis(
				jfc,
				ChartPanel.DEFAULT_WIDTH,
				ChartPanel.DEFAULT_HEIGHT,
				ChartPanel.DEFAULT_MINIMUM_DRAW_WIDTH,
				ChartPanel.DEFAULT_MINIMUM_DRAW_HEIGHT,
				ChartPanel.DEFAULT_MAXIMUM_DRAW_WIDTH,
				ChartPanel.DEFAULT_MAXIMUM_DRAW_HEIGHT,
				ChartPanel.DEFAULT_BUFFER_USED,
				false, // properties
				false, // copy
				false, // save
				false, // print
				displayPopup, // zoom
				true);
		XYPlot plot = chartPanel.getChart().getXYPlot();
		ChartUtilities.addDefaultTopBottomDataset(plot, topLineDataset,
				topLineErrorDataset, bottomLineSignalDataset, bottomLineImageDataset);
		ChartUtilities.setDefaultRenderer(plot);
		return chartPanel;
	}

	/**
	 * Add empty collection for the top and bottom for the given plot.
	 *
	 * @param plot The plot.
	 */
	private static void addDefaultTopBottomDataset(XYPlot plot) {
		addDefaultTopBottomDataset(plot, new XYSeriesCassisCollection(),
				new XYSeriesCassisCollection(), new XYSeriesCassisCollection(),
				new XYSeriesCassisCollection());
	}

	/**
	 * Add given top and bottom dataset for the given plot.
	 *
	 * @param plot The plot.
	 * @param topLineDataset The top dataset.
	 * @param topLineErrorDataset The top dataset.
	 * @param bottomLineSignalDataset The bottom line signal dataset.
	 * @param bottomLineImageDataset The bottom line signal dataset.
	 */
	private static void addDefaultTopBottomDataset(XYPlot plot,
			XYSeriesCassisCollection topLineDataset,
			XYSeriesCassisCollection topLineErrorDataset,
			XYSeriesCassisCollection bottomLineSignalDataset,
			XYSeriesCassisCollection bottomLineImageDataset) {
		plot.setDataset(SpectrumPlotConstants.TOP_LINE_INDEX, topLineDataset);
		plot.setDataset(SpectrumPlotConstants.TOP_LINE_ERROR_INDEX,
				topLineErrorDataset);
		plot.setDataset(SpectrumPlotConstants.BOTTOM_LINE_SIGNAL_INDEX,
				bottomLineSignalDataset);
		plot.setDataset(SpectrumPlotConstants.BOTTOM_LINE_IMAGE_INDEX,
				bottomLineImageDataset);
	}

	/**
	 * Set the default top, center and bottom renderer for the given plot.
	 *
	 * @param plot The plot.
	 */
	private static void setDefaultRenderer(XYPlot plot) {
		plot.setRenderer(SpectrumPlotConstants.CENTER_INDEX, new XYStepRenderer());
		plot.setRenderer(SpectrumPlotConstants.TOP_LINE_INDEX,
				new CassisLineRenderer(CassisLineRenderer.TOP_RENDERER, 7, 0));
		plot.setRenderer(SpectrumPlotConstants.TOP_LINE_ERROR_INDEX,
				new CassisLineErrorRenderer(CassisLineRenderer.TOP_RENDERER, 0, 3.5));
		plot.setRenderer(SpectrumPlotConstants.BOTTOM_LINE_SIGNAL_INDEX,
				new CassisLineRenderer(CassisLineRenderer.BOTTOM_RENDERER, 5, 1.5));
		plot.setRenderer(SpectrumPlotConstants.BOTTOM_LINE_IMAGE_INDEX,
				new CassisLineRenderer(CassisLineRenderer.BOTTOM_RENDERER, 5, 0));
	}

	/**
	 * Configure the given axis.
	 *
	 * @param axis the axis to configure.
	 */
	private static void confAxis(NumberAxis axis) {
		axis.setAxisLineVisible(false);
		axis.setMinorTickMarksVisible(false);
		axis.setTickMarksVisible(false);
	}

	/**
	 * Create a JFreeChart with only one axis, left or bottom.
	 *
	 * @param pos The position to use, LEFT or BOTTOM.
	 * @return the created JFreeChart.
	 */
	public static JFreeChart createXYLineChart(POSITION pos) {
		NumberAxis xAxis = new NumberAxis();
		xAxis.setAutoRangeIncludesZero(false);
		NumberAxis yAxis = new NumberAxis();

		XYItemRenderer renderer = new XYLineAndShapeRenderer(true, false);
		boolean hideRange = false;
		boolean hideDomain = false;
		if (pos == POSITION.LEFT) {
			hideDomain = true;
			confAxis(yAxis);
		} else {
			hideRange = true;
			confAxis(xAxis);
		}
		XYPlot plot = new XYPlotHidden(new XYSeriesCassisCollection(), xAxis,
				yAxis, renderer, hideRange, hideDomain);
		plot.setOrientation(PlotOrientation.VERTICAL);
		JFreeChart chart = new JFreeChart("", JFreeChart.DEFAULT_TITLE_FONT, plot, false);
		ChartFactory.getChartTheme().apply(chart);
		return chart;
	}

	/**
	 * Create then return a {@link ChartPanelCassis} with only one axis, left or bottom.
	 *
	 * @param pos The position to use, LEFT or BOTTOM.
	 * @return The chart panel.
	 */
	public static ChartPanelCassis createEmptyXYStepChartAxis(POSITION pos) {
		JFreeChart jfc = ChartUtilities.createXYLineChart(pos);
		ChartPanelCassis chartPanel = new ChartPanelCassis(
				jfc,
				ChartPanel.DEFAULT_WIDTH,
				ChartPanel.DEFAULT_HEIGHT,
				pos == POSITION.LEFT ? 0 : ChartPanel.DEFAULT_MINIMUM_DRAW_WIDTH,
				pos == POSITION.BOTTOM ? 0 : ChartPanel.DEFAULT_MINIMUM_DRAW_HEIGHT,
				ChartPanel.DEFAULT_MAXIMUM_DRAW_WIDTH,
				ChartPanel.DEFAULT_MAXIMUM_DRAW_HEIGHT,
				ChartPanel.DEFAULT_BUFFER_USED,
				false, // properties
				false, // copy
				false, // save
				false, // print
				false, // zoom
				true);
		XYPlot plot = chartPanel.getChart().getXYPlot();
		addDefaultTopBottomDataset(plot);
		setDefaultRenderer(plot);
		return chartPanel;
	}

}
