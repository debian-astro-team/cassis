/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.simple.util;

import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jfree.chart.axis.AxisCollection;
import org.jfree.chart.axis.AxisSpace;
import org.jfree.chart.axis.AxisState;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.PlotState;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.Layer;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.RectangleInsets;

/**
 * Partially hidden XYPlot where keep only on axis. Used for gallery.
 *
 * @author M. Boiziot
 */
public class XYPlotHidden extends XYPlot {

	private static final long serialVersionUID = 1621016393941707703L;

	private boolean hideRange;
	private boolean hideDomain;


	/**
	 * Constructor.
	 *
	 * @param dataset The dataset.
	 * @param domainAxis The domain axis.
	 * @param rangeAxis The range axis.
	 * @param renderer The renderer.
	 * @param hideRange true to hide range, false otherwise.
	 * @param hideDomain true to hide range, false otherwise.
	 */
	public XYPlotHidden(XYDataset dataset, ValueAxis domainAxis, ValueAxis rangeAxis,
			XYItemRenderer renderer, boolean hideRange, boolean hideDomain) {
		super(dataset, domainAxis, rangeAxis, renderer);
		this.hideRange = hideRange;
		this.hideDomain = hideDomain;
	}

	/**
	 * Do not draw annotations.
	 *
	 * @param g2  the graphics device.
	 * @param dataArea  the data area.
	 * @param info  the chart rendering info.
	 */
	@Override
	public void drawAnnotations(Graphics2D g2, Rectangle2D dataArea,
			PlotRenderingInfo info) {
		return;
	}

	/**
	 * Draws a domain crosshair if the domain is not hidden.
	 *
	 * @param g2  the graphics target.
	 * @param dataArea  the data area.
	 * @param orientation  the plot orientation.
	 * @param value  the crosshair value.
	 * @param axis  the axis against which the value is measured.
	 * @param stroke  the stroke used to draw the crosshair line.
	 * @param paint  the paint used to draw the crosshair line.
	 *
	 * @since 1.0.4
	 */
	@Override
	protected void drawDomainCrosshair(Graphics2D g2, Rectangle2D dataArea,
			PlotOrientation orientation, double value, ValueAxis axis, Stroke stroke,
			Paint paint) {
		if (hideDomain) {
			return;
		}
		super.drawDomainCrosshair(g2, dataArea, orientation, value, axis, stroke, paint);
	}

	/**
	 * Draws the gridlines for the plot, if they are visible and the domain is not hidden.
	 *
	 * @param g2  the graphics device.
	 * @param dataArea  the data area.
	 * @param ticks  the ticks.
	 *
	 * @see #drawRangeGridlines(Graphics2D, Rectangle2D, List)
	 */
	@Override
	@SuppressWarnings("rawtypes")
	protected void drawDomainGridlines(Graphics2D g2, Rectangle2D dataArea, List ticks) {
		if (hideDomain) {
			return;
		}
		super.drawDomainGridlines(g2, dataArea, ticks);
	}

	/**
	 * Draws the domain markers (if any) for an axis and layer if the domain is not hidden.
	 *  This method is typically called from within the draw() method.
	 *
	 * @param g2  the graphics device.
	 * @param dataArea  the data area.
	 * @param index  the renderer index.
	 * @param layer  the layer (foreground or background).
	 */
	@Override
	protected void drawDomainMarkers(Graphics2D g2, Rectangle2D dataArea, int index,
			Layer layer) {
		if (hideDomain) {
			return;
		}
		super.drawDomainMarkers(g2, dataArea, index, layer);
	}

	/**
	 * Draws the domain tick bands, if any and the domain is not hidden.
	 *
	 * @param g2  the graphics device.
	 * @param dataArea  the data area.
	 * @param ticks  the ticks.
	 *
	 * @see #setDomainTickBandPaint(Paint)
	 */
	@Override
	@SuppressWarnings("rawtypes")
	public void drawDomainTickBands(Graphics2D g2, Rectangle2D dataArea, List ticks) {
		if (hideDomain) {
			return;
		}
		super.drawDomainTickBands(g2, dataArea, ticks);
	}

	/**
	 * Draws a range crosshair if the range is not hidden.
	 *
	 * @param g2  the graphics target.
	 * @param dataArea  the data area.
	 * @param orientation  the plot orientation.
	 * @param value  the crosshair value.
	 * @param axis  the axis against which the value is measured.
	 * @param stroke  the stroke used to draw the crosshair line.
	 * @param paint  the paint used to draw the crosshair line.
	 */
	@Override
	protected void drawRangeCrosshair(Graphics2D g2, Rectangle2D dataArea,
			PlotOrientation orientation, double value, ValueAxis axis, Stroke stroke,
			Paint paint) {
		if (hideRange) {
			return;
		}
		super.drawDomainCrosshair(g2, dataArea, orientation, value, axis, stroke, paint);
	}

	/**
	 * Draws the gridlines for the plot's primary range axis, if they are
	 * visible. and the range is not hidden.
	 *
	 * @param g2  the graphics device.
	 * @param area  the data area.
	 * @param ticks  the ticks.
	 *
	 * @see #drawDomainGridlines(Graphics2D, Rectangle2D, List)
	 */
	@Override
	@SuppressWarnings("rawtypes")
	protected void drawRangeGridlines(Graphics2D g2, Rectangle2D area, List ticks) {
		if (hideRange) {
			return;
		}
		super.drawRangeGridlines(g2, area, ticks);
	}

	/**
	 * Draws the range markers (if any) for a renderer and layer if the range is not hidden.
	 *  This method is typically called from within the draw() method.
	 *
	 * @param g2  the graphics device.
	 * @param dataArea  the data area.
	 * @param index  the renderer index.
	 * @param layer  the layer (foreground or background).
	 */
	@Override
	protected void drawRangeMarkers(Graphics2D g2, Rectangle2D dataArea, int index,
			Layer layer) {
		if (hideRange) {
			return;
		}
		super.drawRangeMarkers(g2, dataArea, index, layer);
	}

	/**
	 * Draws the range tick bands, if any and the range is not hidden.
	 *
	 * @param g2  the graphics device.
	 * @param dataArea  the data area.
	 * @param ticks  the ticks.
	 *
	 * @see #setRangeTickBandPaint(Paint)
	 */
	@Override
	@SuppressWarnings("rawtypes")
	public void drawRangeTickBands(Graphics2D g2, Rectangle2D dataArea, List ticks) {
		if (hideRange) {
			return;
		}
		super.drawRangeTickBands(g2, dataArea, ticks);
	}

	/**
	 * Trims a rectangle to integer coordinates.
	 *
	 * @param rect  the incoming rectangle.
	 *
	 * @return A rectangle with integer coordinates.
	 */
	private Rectangle integerise(Rectangle2D rect) {
		int x0 = (int) Math.ceil(rect.getMinX());
		int y0 = (int) Math.ceil(rect.getMinY());
		int x1 = (int) Math.floor(rect.getMaxX());
		int y1 = (int) Math.floor(rect.getMaxY());
		return new Rectangle(x0, y0, (x1 - x0), (y1 - y0));
	}

	/**
	 * A utility method for drawing the axes.
	 *
	 * @param g2
	 *	 	   the graphics device (<code>null</code> not permitted).
	 * @param plotArea
	 *	 	   the plot area (<code>null</code> not permitted).
	 * @param dataArea
	 *	 	   the data area (<code>null</code> not permitted).
	 * @param plotState
	 *	 	   collects information about the plot (<code>null</code>
	 *	 	   permitted).
	 *
	 * @return A map containing the state for each axis drawn.
	 */
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected Map drawAxes(Graphics2D g2, Rectangle2D plotArea, Rectangle2D dataArea,
			PlotRenderingInfo plotState) {

		AxisCollection axisCollection = new AxisCollection();

		if (!hideDomain) {
			// add domain axes to lists...
			for (int index = 0; index < this.getDomainAxisCount(); index++) {
				ValueAxis axis = (ValueAxis) this.getDomainAxis(index);
				if (axis != null) {
					axisCollection.add(axis, getDomainAxisEdge(index));
				}
			}
		}

		if (!hideRange) {
			// add range axes to lists...
			for (int index = 0; index < this.getRangeAxisCount(); index++) {
				ValueAxis yAxis = (ValueAxis) this.getRangeAxis(index);
				if (yAxis != null) {
					axisCollection.add(yAxis, getRangeAxisEdge(index));
				}
			}
		}

		Map axisStateMap = new HashMap();
		double cursor;
		Iterator iterator;

		if (!hideDomain) {
			// draw the bottom axes
			cursor = dataArea.getMaxY()
					+ this.getAxisOffset().calculateBottomOutset(dataArea.getHeight());
			iterator = axisCollection.getAxesAtBottom().iterator();
			while (iterator.hasNext()) {
				ValueAxis axis = (ValueAxis) iterator.next();
				AxisState info = axis.draw(g2, cursor, plotArea, dataArea,
						RectangleEdge.BOTTOM, plotState);
				cursor = info.getCursor();
				axisStateMap.put(axis, info);
			}
		}

		if (!hideRange) {
			// draw the left axes
			cursor = dataArea.getMinX()
					- this.getAxisOffset().calculateLeftOutset(dataArea.getWidth());
			iterator = axisCollection.getAxesAtLeft().iterator();
			while (iterator.hasNext()) {
				ValueAxis axis = (ValueAxis) iterator.next();
				AxisState info = axis.draw(g2, cursor, plotArea, dataArea,
						RectangleEdge.LEFT, plotState);
				cursor = info.getCursor();
				axisStateMap.put(axis, info);
			}
		}

		return axisStateMap;
	}

	/**
	 * Draws the plot within the specified area on a graphics device.
	 *
	 * @param g2  the graphics device.
	 * @param area  the plot area (in Java2D space).
	 * @param anchor  an anchor point in Java2D space (<code>null</code>
	 *	 	 	  permitted).
	 * @param parentState  the state from the parent plot, if there is one
	 *	 	 	 	  (<code>null</code> permitted).
	 * @param info  collects chart drawing information (<code>null</code>
	 *	 		 permitted).
	 */
	@Override
	public void draw(Graphics2D g2, Rectangle2D area, Point2D anchor,
			PlotState parentState, PlotRenderingInfo info) {
		// if the plot area is too small, just return...
		boolean b1 = (area.getWidth() <= MINIMUM_WIDTH_TO_DRAW);
		boolean b2 = (area.getHeight() <= MINIMUM_HEIGHT_TO_DRAW);
		if (b1 || b2) {
			return;
		}

		// record the plot area...
		if (info != null) {
			info.setPlotArea(area);
		}

		// // adjust the drawing area for the plot insets (if any)...
		RectangleInsets insets = getInsets();
		insets.trim(area);

		AxisSpace space = calculateAxisSpace(g2, area);
		Rectangle2D dataArea = space.shrink(area, null);
		this.getAxisOffset().trim(dataArea);

		dataArea = integerise(dataArea);

		createAndAddEntity((Rectangle2D) dataArea.clone(), info, null, null);
		if (info != null) {
			info.setDataArea(dataArea);
		}

		drawAxes(g2, area, dataArea, info);
	}
}
