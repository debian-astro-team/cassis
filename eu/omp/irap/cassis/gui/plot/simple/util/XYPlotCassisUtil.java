/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.simple.util;

import org.jfree.chart.renderer.xy.AbstractXYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.Range;
import org.jfree.data.xy.XYSeriesCollection;

import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassis;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import eu.omp.irap.cassis.gui.plot.curve.config.ConfigCurve;
import eu.omp.irap.cassis.gui.plot.curve.config.CurveParameters;
import eu.omp.irap.cassis.gui.plot.simple.SpectrumPlot;
import eu.omp.irap.cassis.gui.plot.simple.renderer.XYDotBySeriesRenderer;

public class XYPlotCassisUtil {

	public static void setRenderSeriesParameters(AbstractXYItemRenderer renderer, int i,
			ConfigCurve configCurve, CurveParameters curveParameters) {
		if (renderer instanceof XYLineAndShapeRenderer) {
			XYLineAndShapeRenderer lineAndShapeRenderer = (XYLineAndShapeRenderer) renderer;
			setRenderSeriesParameters(lineAndShapeRenderer, i, configCurve, curveParameters);
		} else if (renderer instanceof XYDotBySeriesRenderer) {
			XYDotBySeriesRenderer seriesDotRenderer = (XYDotBySeriesRenderer) renderer;
			setRenderSeriesParameters(seriesDotRenderer, i, configCurve, curveParameters);
		} else {
			throw new IllegalArgumentException("Renderer not supported.");
		}
	}

	public static void setRenderSeriesParameters(AbstractXYItemRenderer renderer, int i, ConfigCurve configCurve) {
		setRenderSeriesParameters(renderer, i, configCurve, CurveParameters.ALL);
	}

	/**
	 * Configure the renderer: associate a color for each series.
	 * @param displayDataset The dataset.
	 * @param renderer The rendered.
	 */
	public static void configureRenderer(XYSeriesCollection displayDataset, AbstractXYItemRenderer renderer) {
		for (int i = 0; i < displayDataset.getSeriesCount(); ++i) {
			if (displayDataset.getSeries(i) instanceof XYSeriesCassis) {
				XYSeriesCassis serieCassis = (XYSeriesCassis) displayDataset.getSeries(i);
				int y = displayDataset.indexOf(serieCassis.getKey());
				XYPlotCassisUtil.setRenderSeriesParameters(renderer, y, serieCassis.getConfigCurve());
			}
		}
	}

	/**
	 * Return the range of visible {@link XYSpectrumSeries}.
	 *
	 * @param dataset The dataset.
	 * @return the X range of a dataset
	 */
	public static Range getXRangeWithoutSpecies(XYSeriesCollection dataset) {
		double xminimum = Double.POSITIVE_INFINITY;
		double xmaximum = Double.NEGATIVE_INFINITY;

		int seriesCount = dataset.getSeriesCount();
		for (int series = 0; series < seriesCount; series++) {
			if (dataset.getSeries(series) instanceof XYSpectrumSeries) {
				XYSpectrumSeries serieCassis = (XYSpectrumSeries) dataset.getSeries(series);
				if (serieCassis.getConfigCurve().isVisible()) {
					xminimum = Math.min(xminimum, serieCassis.getSpectrum().getFrequencySignalMin());
					xmaximum = Math.max(xmaximum, serieCassis.getSpectrum().getFrequencySignalMax());
				}
			}
		}

		return new Range(xminimum, xmaximum);
	}

	/**
	 * Change the generals parameters (color, shape, visibility) if needed.
	 *
	 * @param renderer The renderer.
	 * @param i The index of the series.
	 * @param configCurve The ConfigCurve.
	 * @param curveParameters The CurveParameters.
	 */
	private static void setRenderSeriesGeneralParameters(AbstractXYItemRenderer renderer,
			int i, ConfigCurve configCurve, CurveParameters curveParameters) {
		if ((curveParameters.equals(CurveParameters.ALL) ||
				curveParameters.equals(CurveParameters.COLOR))
				&& (renderer.getSeriesPaint(i) == null ||
				!renderer.getSeriesPaint(i).equals(configCurve.getColor())))
			renderer.setSeriesPaint(i, configCurve.getColor(), true);

		if ((curveParameters.equals(CurveParameters.ALL) ||
				curveParameters.equals(CurveParameters.SHAPE))
				&& (renderer.getSeriesShape(i) == null ||
				!renderer.getSeriesShape(i).equals(configCurve.getShape())))
			renderer.setSeriesShape(i, configCurve.getShape(), true);

		if ((curveParameters.equals(CurveParameters.ALL) ||
				curveParameters.equals(CurveParameters.VISIBILITY))
				&& (renderer.getSeriesVisible(i) == null ||
				!renderer.getSeriesVisible(i).equals(configCurve.isVisible())))
			renderer.setSeriesVisible(i, configCurve.isVisible(), true);
	}

	/**
	 * Set rendering parameters for renderer of type {@link XYLineAndShapeRenderer} (line and histogram).
	 *
	 * @param renderer The renderer.
	 * @param i The index of the series.
	 * @param configCurve The ConfigCurve.
	 * @param curveParameters The CurveParameters.
	 */
	private static void setRenderSeriesParameters(XYLineAndShapeRenderer renderer, int i,
			ConfigCurve configCurve, CurveParameters curveParameters) {
		setRenderSeriesGeneralParameters(renderer, i, configCurve, curveParameters);
		if ((curveParameters.equals(CurveParameters.ALL) ||
				curveParameters.equals(CurveParameters.STROKE))
				&& (renderer.getSeriesStroke(i) == null ||
				!renderer.getSeriesStroke(i).equals(configCurve.getStroke())))
			renderer.setSeriesStroke(i, configCurve.getStroke(), true);

		if ((curveParameters.equals(CurveParameters.ALL) ||
				curveParameters.equals(CurveParameters.SHAPE_VISIBILITY))
				&& (renderer.getSeriesShapesVisible(i) == null ||
				!renderer.getSeriesShapesVisible(i).equals(configCurve.isShapeVisible())))
			renderer.setSeriesShapesVisible(i, configCurve.isShapeVisible());
	}

	/**
	 * Set rendering parameters for renderer of type {@link XYDotBySeriesRenderer} (dot).
	 *
	 * @param renderer The renderer.
	 * @param i The index of the series.
	 * @param configCurve The ConfigCurve.
	 * @param curveParameters The CurveParameters.
	 */
	private static void setRenderSeriesParameters(XYDotBySeriesRenderer renderer, int i,
			ConfigCurve configCurve, CurveParameters curveParameters) {
		setRenderSeriesGeneralParameters(renderer, i, configCurve, curveParameters);

		if ((curveParameters.equals(CurveParameters.ALL) ||
				curveParameters.equals(CurveParameters.DOT_HEIGHT))
				&& (renderer.getSeriesDotHeight(i) == null ||
				!renderer.getSeriesDotHeight(i).equals(configCurve.getDotHeightSize())))
			renderer.setSeriesDotHeight(i, configCurve.getDotHeightSize());

		if ((curveParameters.equals(CurveParameters.ALL) ||
				curveParameters.equals(CurveParameters.DOT_WIDTH))
				&& (renderer.getSeriesDotWidth(i) == null ||
				!renderer.getSeriesDotWidth(i).equals(configCurve.getDotWidthSize())))
			renderer.setSeriesDotWidth(i, configCurve.getDotWidthSize());
	}

	/**
	 * Configure the renderer for the given SpectrumPlot.
	 *
	 * @param sp The SpectrumPlot.
	 */
	public static void configureRenderer(SpectrumPlot sp) {
		XYPlotCassisUtil.configureRenderer(sp.getCenterXYSeriesCollection(),
				sp.getCenterRenderer());
		XYPlotCassisUtil.configureRenderer(sp.getTopLineSeriesCollection(),
				sp.getTopLineRenderer());
		XYPlotCassisUtil.configureRenderer(sp.getTopLineErrorSeriesCollection(),
				sp.getTopLineErrorRenderer());
		XYPlotCassisUtil.configureRenderer(sp.getBottomLineSignalSeriesCollection(),
				sp.getBottomLineSignalRenderer());
		XYPlotCassisUtil.configureRenderer(sp.getBottomLineImageSeriesCollection(),
				sp.getBottomLineImageRenderer());
	}

}
