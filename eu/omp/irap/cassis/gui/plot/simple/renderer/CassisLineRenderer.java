/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.simple.renderer;

import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Rectangle2D;

import org.jfree.chart.LegendItem;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CrosshairState;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRendererState;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.util.LineUtilities;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RectangleEdge;

/**
 * Line renderer for top and bottom lines in CASSIS.
 *
 * @author M. Boiziot
 */
public class CassisLineRenderer extends XYLineAndShapeRenderer {

	private static final long serialVersionUID = 8091185498627252832L;

	public static final int TOP_RENDERER = 0;
	public static final int BOTTOM_RENDERER = 1;

	private int position;
	private double size;
	private double offset;


	/**
	 * Constructor.
	 *
	 * @param position The position of the renderer. It must be
	 *  {@link #TOP_RENDERER} or {@link #BOTTOM_RENDERER}.
	 * @param size The size of the line (percentage margin, from 0 to 100).
	 * @param offset The percentage offset from border (from 0 to 100).
	 */
	public CassisLineRenderer(int position, double size, double offset) {
		super(true, true);
		this.position = position;
		if (!isPositionValid(position)) {
			throw new IllegalArgumentException("Position is not valid.");
		}
		this.size = size;
		this.offset = offset;
	}

	/**
	 * Return the size (percentage margin).
	 *
	 * @return the size
	 */
	public double getSize() {
		return size;
	}

	/**
	 * Return the offset (percentage offset from border).
	 *
	 * @return the offset.
	 */
	public double getOffset() {
		return offset;
	}

	/**
	 * Return if the given position is valid.
	 *
	 * @param position The position.
	 * @return true if the position is valid, false otherwise.
	 */
	private boolean isPositionValid(int position) {
		return position == TOP_RENDERER || position == BOTTOM_RENDERER;
	}

	/**
	 * Return the position.
	 *
	 * @return the position.
	 */
	public int getPosition() {
		return position;
	}

	/**
	 * Return false.
	 *
	 * @param series  the series index (zero-based).
	 * @param item  the item index (zero-based).
	 * @return false
	 */
	@Override
	public boolean getItemShapeVisible(int series, int item) {
		return false;
	}

	/**
	 * Return false.
	 *
	 * @param series  the series index (zero-based).
	 * @return false.
	 * @see #setSeriesShapesVisible(int, Boolean)
	 */
	@Override
	public Boolean getSeriesShapesVisible(int series) {
		return false;
	}

	/**
	 * Do nothing.
	 *
	 * @param series  the series index (zero-based).
	 * @param visible  the flag.
	 * @see #getSeriesShapesVisible(int)
	 */
	@Override
	public void setSeriesShapesVisible(int series, boolean visible) {
		// Do nothing.
	}

	/**
	 * Do nothing.
	 *
	 * @param series  the series index (zero-based).
	 * @param flag  the flag.
	 * @see #getSeriesShapesVisible(int)
	 */
	@Override
	public void setSeriesShapesVisible(int series, Boolean flag) {
		// Do nothing.
	}

	/**
	 * Return one, to only draw the line.
	 *
	 * @return 1.
	 * @see org.jfree.chart.renderer.xy.XYLineAndShapeRenderer#getPassCount()
	 */
	@Override
	public int getPassCount() {
		return 1;
	}

	/**
	 * Draws the visual representation of a single data item.
	 *
	 * @param g2  the graphics device.
	 * @param state  the renderer state.
	 * @param dataArea  the area within which the data is being drawn.
	 * @param info  collects information about the drawing.
	 * @param plot  the plot (can be used to obtain standard color
	 *			  information etc).
	 * @param domainAxis  the domain axis.
	 * @param rangeAxis  the range axis.
	 * @param dataset  the dataset.
	 * @param series  the series index (zero-based).
	 * @param item  the item index (zero-based).
	 * @param crosshairState  crosshair information for the plot
	 *						(<code>null</code> permitted).
	 * @param pass  the pass index.
	 */
	@Override
	public void drawItem(Graphics2D g2, XYItemRendererState state, Rectangle2D dataArea,
			PlotRenderingInfo info, XYPlot plot, ValueAxis domainAxis,
			ValueAxis rangeAxis, XYDataset dataset, int series, int item,
			CrosshairState crosshairState, int pass) {
		// do nothing if item is not visible
		if (!getItemVisible(series, item)) {
			return;
		}

		// first pass draws the background (lines, for instance)
		if (isLinePass(pass) && getItemLineVisible(series, item)) {
			drawPrimaryLine(state, g2, plot, dataset, pass, series, item,
					domainAxis, rangeAxis, dataArea);
		}
	}

	/**
	 * Draws the item (first pass). This method draws the lines
	 * connecting the items.
	 *
	 * @param g2  the graphics device.
	 * @param state  the renderer state.
	 * @param dataArea  the area within which the data is being drawn.
	 * @param plot  the plot (can be used to obtain standard color
	 *			  information etc).
	 * @param domainAxis  the domain axis.
	 * @param rangeAxis  the range axis.
	 * @param dataset  the dataset.
	 * @param pass  the pass.
	 * @param series  the series index (zero-based).
	 * @param item  the item index (zero-based).
	 */
	protected void drawPrimaryLine(XYItemRendererState state, Graphics2D g2, XYPlot plot,
			XYDataset dataset, int pass, int series, int item, ValueAxis domainAxis,
			ValueAxis rangeAxis, Rectangle2D dataArea) {
		double x = dataset.getXValue(series, item);
		if (Double.isNaN(x)) {
			return;
		}
		RectangleEdge xAxisLocation = plot.getDomainAxisEdge();

		double aPercentHeight = dataArea.getHeight() / 100;
		double sizeToUse = aPercentHeight * size;
		double offsetToUse = aPercentHeight * offset;

		double transY0;
		double transY1;
		if (this.position == TOP_RENDERER) {
			transY0 = dataArea.getMinY() + offsetToUse;
			transY1 = transY0 + sizeToUse;
		} else if (this.position == BOTTOM_RENDERER) {
			transY1 = dataArea.getMinY() + dataArea.getHeight() - offsetToUse;
			transY0 = transY1 - sizeToUse;
		} else {
			throw new IllegalArgumentException("Invalid position value");
		}
		double transX = domainAxis.valueToJava2D(x, dataArea, xAxisLocation);
		if (Double.isNaN(transX) || Double.isNaN(transY0) || Double.isNaN(transY1)) {
			return;
		}
		PlotOrientation orientation = plot.getOrientation();
		boolean visible;
		if (orientation == PlotOrientation.HORIZONTAL) {
			state.workingLine.setLine(transY0, transX, transY1, transX);
		} else if (orientation == PlotOrientation.VERTICAL) {
			state.workingLine.setLine(transX, transY0, transX, transY1);
		}
		visible = LineUtilities.clipLine(state.workingLine, dataArea);
		if (visible) {
			drawFirstPassShape(g2, pass, series, item, state.workingLine);
		}
	}

	/**
	 * Draws the first pass shape.
	 *
	 * @param g2  the graphics device.
	 * @param pass  the pass.
	 * @param series  the series index.
	 * @param item  the item index.
	 * @param shape  the shape.
	 */
	protected void drawFirstPassShape(Graphics2D g2, int pass, int series, int item,
			Shape shape) {
		g2.setStroke(getItemStroke(series, item));
		g2.setPaint(getItemPaint(series, item));
		g2.draw(shape);
	}

	/**
	 * Returns a legend item for the specified series.
	 *
	 * @param datasetIndex  the dataset index (zero-based).
	 * @param series  the series index (zero-based).
	 *
	 * @return A legend item for the series (possibly <code>null</code).
	 */
	@Override
	public LegendItem getLegendItem(int datasetIndex, int series) {
		XYPlot plot = getPlot();
		if (plot == null) {
			return null;
		}

		XYDataset dataset = plot.getDataset(datasetIndex);
		if (dataset == null) {
			return null;
		}

		if (!getItemVisible(series, 0)) {
			return null;
		}
		String label = getLegendItemLabelGenerator().generateLabel(dataset, series);
		String description = label;
		String toolTipText = null;
		if (getLegendItemToolTipGenerator() != null) {
			toolTipText = getLegendItemToolTipGenerator().generateLabel(dataset, series);
		}
		String urlText = null;
		if (getLegendItemURLGenerator() != null) {
			urlText = getLegendItemURLGenerator().generateLabel(dataset, series);
		}
		boolean shapeIsVisible = getItemShapeVisible(series, 0);
		Shape shape = lookupLegendShape(series);
		boolean shapeIsFilled = getItemShapeFilled(series, 0);
		Paint fillPaint = (this.getUseFillPaint() ? lookupSeriesFillPaint(series)
				: lookupSeriesPaint(series));
		boolean shapeOutlineVisible = this.getDrawOutlines();
		Paint outlinePaint = (this.getUseOutlinePaint() ? lookupSeriesOutlinePaint(series)
				: lookupSeriesPaint(series));
		Stroke outlineStroke = lookupSeriesOutlineStroke(series);
		boolean lineVisible = getItemLineVisible(series, 0);
		Stroke lineStroke = lookupSeriesStroke(series);
		Paint linePaint = lookupSeriesPaint(series);
		LegendItem result = new LegendItem(label, description, toolTipText, urlText,
				shapeIsVisible, shape, shapeIsFilled, fillPaint, shapeOutlineVisible,
				outlinePaint, outlineStroke, lineVisible, this.getLegendLine(),
				lineStroke, linePaint);
		result.setLabelFont(lookupLegendTextFont(series));
		Paint labelPaint = lookupLegendTextPaint(series);
		if (labelPaint != null) {
			result.setLabelPaint(labelPaint);
		}
		result.setSeriesKey(dataset.getSeriesKey(series));
		result.setSeriesIndex(series);
		result.setDataset(dataset);
		result.setDatasetIndex(datasetIndex);

		return result;
	}

}
