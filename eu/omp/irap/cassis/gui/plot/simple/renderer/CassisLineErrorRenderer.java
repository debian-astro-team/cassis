/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.simple.renderer;

import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRendererState;
import org.jfree.chart.util.LineUtilities;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RectangleEdge;

/**
 * Line renderer for errors.
 *
 * @author M. Boiziot
 */
public class CassisLineErrorRenderer extends CassisLineRenderer {

	private static final long serialVersionUID = -373752589583892141L;

	public static final int TOP_RENDERER = 0;
	public static final int BOTTOM_RENDERER = 1;


	/**
	 * Constructor.
	 *
	 * @param position The position of the renderer. It must be
	 *  {@link #TOP_RENDERER} or {@link #BOTTOM_RENDERER}.
	 * @param size The size of the line (percentage margin, from 0 to 100).
	 * @param offset The percentage offset from border (from 0 to 100).
	 */
	public CassisLineErrorRenderer(int position, double size, double offset) {
		super(position, size, offset);
	}

	/**
	 * Draws the item (first pass). This method draws the lines
	 * connecting the items.
	 *
	 * @param g2  the graphics device.
	 * @param state  the renderer state.
	 * @param dataArea  the area within which the data is being drawn.
	 * @param plot  the plot (can be used to obtain standard color
	 *			  information etc).
	 * @param domainAxis  the domain axis.
	 * @param rangeAxis  the range axis.
	 * @param dataset  the dataset.
	 * @param pass  the pass.
	 * @param series  the series index (zero-based).
	 * @param item  the item index (zero-based).
	 */
	protected void drawPrimaryLine(XYItemRendererState state, Graphics2D g2, XYPlot plot,
			XYDataset dataset, int pass, int series, int item, ValueAxis domainAxis,
			ValueAxis rangeAxis, Rectangle2D dataArea) {
		if (item == 0) {
			return;
		}

		// get the data point...
		double xOne = dataset.getXValue(series, item);
		double xZero = dataset.getXValue(series, item - 1);

		if (Double.isNaN(xOne) || Double.isNaN(xZero)) {
			return;
		}

		double aPercentHeight = dataArea.getHeight() / 100;
		double offsetToUse = aPercentHeight * getOffset();

		double transY;
		if (this.getPosition() == CassisLineRenderer.TOP_RENDERER) {
			transY = dataArea.getMinY() + offsetToUse;
		} else if (this.getPosition() == CassisLineRenderer.BOTTOM_RENDERER) {
			transY = dataArea.getMinY() + dataArea.getHeight() - offsetToUse;
		} else {
			throw new IllegalArgumentException("Invalid position value");
		}

		RectangleEdge xAxisLocation = plot.getDomainAxisEdge();

		double transX0 = domainAxis.valueToJava2D(xZero, dataArea, xAxisLocation);
		double transX1 = domainAxis.valueToJava2D(xOne, dataArea, xAxisLocation);

		// only draw if we have good values
		if (Double.isNaN(transX0) || Double.isNaN(transX1) || Double.isNaN(transY)) {
			return;
		}

		PlotOrientation orientation = plot.getOrientation();
		boolean visible;
		if (orientation == PlotOrientation.HORIZONTAL) {
			state.workingLine.setLine(transY, transX0, transY, transX1);
		}
		else if (orientation == PlotOrientation.VERTICAL) {
			state.workingLine.setLine(transX0, transY, transX1, transY);
		}
		visible = LineUtilities.clipLine(state.workingLine, dataArea);
		if (visible) {
			drawFirstPassShape(g2, pass, series, item, state.workingLine);
		}
	}

}
