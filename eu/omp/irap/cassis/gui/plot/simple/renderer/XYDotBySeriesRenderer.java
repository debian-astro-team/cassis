/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.simple.renderer;

import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CrosshairState;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.AbstractXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRendererState;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RectangleEdge;
import org.jfree.util.PublicCloneable;

/**
 * Renderer allowing to plot with dot and changing the size of the points by series.
 *
 * @author M. Boiziot
 */
public class XYDotBySeriesRenderer extends AbstractXYItemRenderer implements XYItemRenderer, PublicCloneable {

	private static final long serialVersionUID = 1644353906262512190L;
	private IntegerList seriesDotHeight;
	private IntegerList seriesDotWidth;


	/**
	 * Constructor.
	 */
	public XYDotBySeriesRenderer() {
		super();
		this.seriesDotHeight = new IntegerList();
		this.seriesDotWidth = new IntegerList();
	}

	/**
	 * Draws the visual representation of a single data item.
	 *
	 * @param g2 the graphics device.
	 * @param state the renderer state.
	 * @param dataArea the area within which the data is being drawn.
	 * @param info collects information about the drawing.
	 * @param plot the plot (can be used to obtain standard color information etc).
	 * @param domainAxis the domain (horizontal) axis.
	 * @param rangeAxis the range (vertical) axis.
	 * @param dataset the dataset.
	 * @param series the series index (zero-based).
	 * @param item the item index (zero-based).
	 * @param crosshairState crosshair information for the plot (<code>null</code> permitted).
	 * @param pass the pass index.
	 */
	@Override
	public void drawItem(Graphics2D g2, XYItemRendererState state, Rectangle2D dataArea,
			PlotRenderingInfo info, XYPlot plot, ValueAxis domainAxis,
			ValueAxis rangeAxis, XYDataset dataset, int series, int item,
			CrosshairState crosshairState, int pass) {

		// do nothing if item is not visible
		if (!getItemVisible(series, item)) {
			return;
		}

		// get the data point...
		double x = dataset.getXValue(series, item);
		double y = dataset.getYValue(series, item);
		int currentSeriesDotWidth = seriesDotWidth.getInteger(series);
		int currentSeriesDotHeight = seriesDotHeight.getInteger(series);
		double adjx = (currentSeriesDotWidth - 1) / 2.0;
		double adjy = (currentSeriesDotHeight - 1) / 2.0;
		if (!Double.isNaN(y)) {
			RectangleEdge xAxisLocation = plot.getDomainAxisEdge();
			RectangleEdge yAxisLocation = plot.getRangeAxisEdge();
			double transX = domainAxis.valueToJava2D(x, dataArea, xAxisLocation) - adjx;
			double transY = rangeAxis.valueToJava2D(y, dataArea, yAxisLocation) - adjy;

			g2.setPaint(getItemPaint(series, item));
			PlotOrientation orientation = plot.getOrientation();
			if (orientation == PlotOrientation.HORIZONTAL) {
				g2.fillRect((int) transY, (int) transX, currentSeriesDotHeight,
						currentSeriesDotWidth);
			} else if (orientation == PlotOrientation.VERTICAL) {
				g2.fillRect((int) transX, (int) transY, currentSeriesDotWidth,
						currentSeriesDotHeight);
			}

			int domainAxisIndex = plot.getDomainAxisIndex(domainAxis);
			int rangeAxisIndex = plot.getRangeAxisIndex(rangeAxis);
			updateCrosshairValues(crosshairState, x, y, domainAxisIndex, rangeAxisIndex,
					transX, transY, orientation);
		}
	}

	/**
	 * Tests this renderer for equality with an arbitrary object. This method
	 * returns <code>true</code> if and only if:
	 *
	 * <ul>
	 * <li><code>obj</code> is not <code>null</code>;</li>
	 * <li><code>obj</code> is an instance of <code>XYDotRenderer</code>;</li>
	 * <li>both renderers have the same attribute values.
	 * </ul>
	 *
	 * @param obj the object (<code>null</code> permitted).
	 *
	 * @return A boolean.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof XYDotBySeriesRenderer)) {
			return false;
		}
		XYDotBySeriesRenderer that = (XYDotBySeriesRenderer) obj;
		if (!this.seriesDotWidth.equals(that.seriesDotWidth)) {
			return false;
		}
		if (!this.seriesDotHeight.equals(that.seriesDotHeight)) {
			return false;
		}
		return super.equals(obj);
	}

	/**
	 * Returns a clone of the renderer.
	 *
	 * @return A clone.
	 *
	 * @throws CloneNotSupportedException if the renderer cannot be cloned.
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		XYDotBySeriesRenderer s = (XYDotBySeriesRenderer) super.clone();
		s.seriesDotHeight = (IntegerList) seriesDotHeight.clone();
		s.seriesDotWidth = (IntegerList) seriesDotWidth.clone();
		return s;
	}

	/**
	 * Change the height of dot.
	 *
	 * @param series The series for which we want to change the size of the dot.
	 * @param dotHeight The dot height to set.
	 */
	public void setSeriesDotHeight(int series, int dotHeight) {
		setSeriesDotHeight(series, Integer.valueOf(dotHeight));
	}

	/**
	 * Change the height of dot.
	 *
	 * @param series The series for which we want to change the size of the dot.
	 * @param dotHeight The dot height to set.
	 */
	public void setSeriesDotHeight(int series, Integer dotHeight) {
		this.seriesDotHeight.setInteger(series, dotHeight);
		fireChangeEvent();
	}

	/**
	 * Change the width of dot.
	 *
	 * @param series The series for which we want to change the size of the dot.
	 * @param dotWidth The dot width to set.
	 */
	public void setSeriesDotWidth(int series, int dotWidth) {
		setSeriesDotWidth(series, Integer.valueOf(dotWidth));
	}

	/**
	 * Change the height of dot.
	 *
	 * @param series The series for which we want to change the size of the dot.
	 * @param dotWidth The dot width to set.
	 */
	public void setSeriesDotWidth(int series, Integer dotWidth) {
		this.seriesDotWidth.setInteger(series, dotWidth);
		fireChangeEvent();
	}

	/**
	 * Return the dot height for the given series index.
	 *
	 * @param series The index of the series.
	 * @return The dot height.
	 */
	public Integer getSeriesDotHeight(int series) {
		return this.seriesDotHeight.getInteger(series);
	}

	/**
	 * Return the dot width for the given series index.
	 *
	 * @param series The index of the series.
	 * @return The dot width.
	 */
	public Integer getSeriesDotWidth(int series) {
		return this.seriesDotWidth.getInteger(series);
	}

	/**
	 * Create a Hash Code for the object.
	 *
	 * @return a hash code for the object.
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((seriesDotHeight == null) ? 0 : seriesDotHeight.hashCode());
		result = prime * result
				+ ((seriesDotWidth == null) ? 0 : seriesDotWidth.hashCode());
		return result;
	}

}
