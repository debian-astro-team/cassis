/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.abstractmozaicplot;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.io.File;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.XAxisCassisUtil;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.file.EXTENSION;
import eu.omp.irap.cassis.gui.fit.FitPanel;
import eu.omp.irap.cassis.gui.fit.FitPanelAdapteur;
import eu.omp.irap.cassis.gui.fit.advanced.gui.AdvancedFitCreationPanel;
import eu.omp.irap.cassis.gui.fit.advanced.gui.AdvancedFitFrame;
import eu.omp.irap.cassis.gui.model.CassisResult;
import eu.omp.irap.cassis.gui.otherspecies.SpeciesTab;
import eu.omp.irap.cassis.gui.plot.abstractplot.AbstractPlotView;
import eu.omp.irap.cassis.gui.plot.curve.CurvePanelView;
import eu.omp.irap.cassis.gui.plot.curve.JPanelCurve;
import eu.omp.irap.cassis.gui.plot.curve.TypeCurve;
import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassis;
import eu.omp.irap.cassis.gui.plot.curve.config.ConfigCurve;
import eu.omp.irap.cassis.gui.plot.gallery.GalleryHandler;
import eu.omp.irap.cassis.gui.plot.gallery.GalleryNavigation;
import eu.omp.irap.cassis.gui.plot.gallery.GalleryNavigationModel;
import eu.omp.irap.cassis.gui.plot.gallery.GalleryOption;
import eu.omp.irap.cassis.gui.plot.gallery.GallerySortPane;
import eu.omp.irap.cassis.gui.plot.infopanel.InfoModel;
import eu.omp.irap.cassis.gui.plot.infopanel.InfoPanel;
import eu.omp.irap.cassis.gui.plot.infopanel.InfoPanelConstants;
import eu.omp.irap.cassis.gui.plot.infopanel.LineConfigCurve;
import eu.omp.irap.cassis.gui.plot.line.CassisPlot;
import eu.omp.irap.cassis.gui.plot.popup.MessageControl;
import eu.omp.irap.cassis.gui.plot.save.CassisSaveViewInterface;
import eu.omp.irap.cassis.gui.plot.save.RestorationInterface;
import eu.omp.irap.cassis.gui.plot.save.SaveAbstractPlot;
import eu.omp.irap.cassis.gui.plot.simple.SpectrumPlot;
import eu.omp.irap.cassis.gui.plot.simple.util.ChartPanelCassis;
import eu.omp.irap.cassis.gui.plot.simple.util.XYPlotCassisUtil;
import eu.omp.irap.cassis.gui.plot.tools.ToolsView;
import eu.omp.irap.cassis.gui.plot.util.ChartMovePanel;
import eu.omp.irap.cassis.gui.plot.util.InterValMarkerCassis;
import eu.omp.irap.cassis.gui.plot.util.MarkerManager;
import eu.omp.irap.cassis.gui.plot.util.PrintUtil;
import eu.omp.irap.cassis.gui.plot.util.StackMosaicPanel;
import eu.omp.irap.cassis.gui.plot.util.ToolsState;
import eu.omp.irap.cassis.gui.util.CassisRessource;
import eu.omp.irap.cassis.gui.util.MyLayeredPane;

@SuppressWarnings("serial")
public abstract class AbstractMozaicPlotView extends AbstractPlotView implements
RestorationInterface, GalleryHandler{

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractMozaicPlotView.class);

	protected AbstractMozaicPlotModel model;
	protected AbstractMozaicPlotControl control;

	private SpeciesTab otherSpeciesPanel;

	private JPanel overlayPanel;
	private JButton dataFileButton;
	private ToolsState toolsState = ToolsState.STACK;
	protected JTabbedPane tabManager;

	private List<FitPanel> fitPanelList;
	private FitPanel lastFitPanel = null;

	protected StackMosaicPanel stackMosaicPanel;
	private JPanel westPanel;
	private JTabbedPane toolsChartTabbedPanel;
	private SaveAbstractPlot save;

	private JButton lineListButton;
	private MyLayeredPane layeredPane = new MyLayeredPane();
	protected MessageControl messageControl = new MessageControl(layeredPane);
	private SpectrumPlot spectrumPlot;
	private JPanel axisYToolsPanel;
	private JPanel axisXToolsPanel;
	private ChartMovePanel chartYMovePanel;
	private JPanel yAxisPanel;
	protected JComboBox<YAxisCassis> yAxisLeftComboBox;
	protected JComboBox<YAxisCassis> yAxisRigthComboBox;
	private JPanel xAxisPanel;
	private ChartMovePanel chartXMovePanel;
	private JPanel xAxisBottomPanel;
	protected JComboBox<String> comboBottomAxisType;
	protected JComboBox<XAxisCassis> comboBottomtAxis;
	private JPanel xAxisTopPanel;
	protected JComboBox<String> comboTopAxisType;
	protected JComboBox<XAxisCassis> comboTopAxis;
	private JScrollPane scrollPane;
	private JPanel infoPanelTab;

	private InfoPanel infoPanel;

	private JPanel galleryPane;
	protected GallerySortPane gallerySortPane;
	protected GalleryNavigation galleryNavi;

	private JPanel toolsPanel;
	private ToolsView toolsView;
	private JPanel toolbar;
	private JCheckBox backButton;
	private JLabel freqRef;

	private LineConfigCurve lineConfigCurve;
	private JCheckBox yLogCheckBox;
	private JCheckBox xLogCheckBox;

	private AdvancedFitCreationPanel creationPanel;

	private boolean replacingToolsTab;


	protected AbstractMozaicPlotView(AbstractMozaicPlotControl control) {
		super(new BorderLayout());
		setControl(control);
		model = control.getModel();

		add(getTabManager(), BorderLayout.EAST);
		add(getWestPanel(), BorderLayout.CENTER);
	}

	public abstract AbstractMozaicPlotControl getControl();
	public abstract JComboBox<String> getComboBottomAxisType();
	public abstract JComboBox<XAxisCassis> getComboBottomtAxis();

	@Override
	public abstract String getHelpUrl();

	@Override
	public abstract String getTitle();

	public abstract void displayResult(CassisResult result, boolean newDisplay);

	protected abstract JPanelCurve getFilePanelCurve(CassisPlot cassisPlot);

	public void setControl(AbstractMozaicPlotControl control) {
		this.control = control;
	}

	private JPanel getWestPanel() {
		if (westPanel == null) {
			westPanel = new JPanel(new BorderLayout());
			toolsPanel = new JPanel(new BorderLayout());
			toolsPanel.add(getToolsChartTabbedPanel(), BorderLayout.SOUTH);
			getToolsChartTabbedPanel().setSelectedIndex(2);
			westPanel.add(toolsPanel, BorderLayout.SOUTH);
			getSpectrumPlot().setLocation(layeredPane.getLocation());
			layeredPane.add(getSpectrumPlot(), -1);
			layeredPane.addComponentListener(new ComponentAdapter() {
				@Override
				public void componentResized(ComponentEvent e) {
					super.componentResized(e);
					getSpectrumPlot().setBounds(layeredPane.getBounds());
					revalidate();
				}
			});

			westPanel.addComponentListener(new ComponentAdapter() {
				@Override
				public void componentResized(ComponentEvent e) {
					getSpectrumPlot().validate();
					layeredPane.validate();
				}
			});
			westPanel.add(layeredPane, BorderLayout.CENTER);
		}
		return westPanel;
	}

	private JPanel buildYAxisComboPanel(String label, JComboBox<YAxisCassis> comboBox) {
		JLabel topLabel = new JLabel(label);
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		topPanel.add(topLabel);
		topPanel.add(comboBox);

		comboBox.setSize(180, 24);
		comboBox.setPreferredSize(new Dimension(180, 24));

		return topPanel;
	}

	private JPanel getXAxisTopPanel() {
		if (xAxisTopPanel == null) {
			xAxisTopPanel = new JPanel(new BorderLayout());

			xAxisTopPanel.add(
					buildXAxisComboPanel("<HTML><p>X<sub>top</sub></p></HTML>",
							getComboTopAxis(), getComboTopAxisType()), BorderLayout.CENTER);
		}
		return xAxisTopPanel;
	}

	public void displayResult(CassisResult result) {
		displayResult(result, true);
	}

	public SpeciesTab getOtherSpeciesPanel() {
		if (otherSpeciesPanel == null) {
			otherSpeciesPanel = control.getSpeciesControl().getView();
			control.getSpeciesControl().addSpeciesListener(control);
		}
		return otherSpeciesPanel;
	}

	/**
	 * @return the dataFileButton
	 */
	public JButton getDataFileButton() {
		if (dataFileButton == null) {
			dataFileButton = new JButton("Select");
			dataFileButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.onOverlayPanelDataFileButtonClicked();
				}
			});
		}
		return dataFileButton;
	}

	public JTabbedPane getTabManager() {
		if (tabManager == null) {
			tabManager = new JTabbedPane();
			tabManager.setPreferredSize(new Dimension(353, 0));
			tabManager.addChangeListener(new ChangeListener() {
				@Override
				public void stateChanged(ChangeEvent e) {
					if (tabManager.getSelectedIndex() == getIndiceTabByName("Tools")) {
						getToolsView().refresh();
					} else if (tabManager.getSelectedIndex() == getIndiceTabByName("Fit")) {
						if (fitPanelList != null &&
								getLineConfigCurve().isThatCurveExist(InfoPanelConstants.FIT_RESIDUAL_TITLE)) {
							getCurrentFitPanel().setSelectedResidual(
									getLineConfigCurve().getConfigCurve(
											InfoPanelConstants.FIT_RESIDUAL_TITLE).isVisible());
						}
					} else if (tabManager.getSelectedIndex() == getIndiceTabByName("Advanced Fit")
							&& !(control.isReplacingFitPanel() || replacingToolsTab)) {
						AdvancedFitFrame.getFrame().openFrame(getSpectrumPlot().getNbSeriesVisibleAndSavable()!= 0);
					}
				}
			});
			tabManager.addTab("InfoPanel", getInfoPanelTab());
			tabManager.addTab("Overlays", getOverlayPanel());
			tabManager.addTab("Species", getOtherSpeciesPanel());
			FitPanel fitPanel = new FitPanel(null, false, control.getFitControl());
			control.getFitControl().setFitPanel(fitPanel);
			tabManager.addTab("Fit", fitPanel);
			tabManager.addTab("Advanced Fit", getCreationPanel());
		}
		return tabManager;
	}

	/**
	 * @return The panel used to compute fit models
	 */
	public AdvancedFitCreationPanel getCreationPanel() {
		if (creationPanel == null) {
			creationPanel = new AdvancedFitCreationPanel(model.getFitModelManager());
		}
		return creationPanel;
	}

	private JPanel getInfoPanelTab() {
		if (infoPanelTab == null) {
			infoPanelTab = new JPanel(new BorderLayout());
			infoPanelTab.setBorder(BorderFactory.createTitledBorder(
					BorderFactory.createEtchedBorder(), "Plot Info"));
			infoPanelTab.add(getInfoPanelScrollPane(), BorderLayout.CENTER);

			JButton removeAllButton = new JButton("Remove All");
			removeAllButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.removeAllButtonClicked();
				}
			});
			JPanel temp = new JPanel();
			temp.setBorder(BorderFactory.createLineBorder(Color.GRAY));
			temp.add(removeAllButton);
			infoPanelTab.add(temp, BorderLayout.SOUTH);
		}
		return infoPanelTab;
	}

	public JScrollPane getInfoPanelScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane(getInfoPanel(),
					ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
					ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			scrollPane.setPreferredSize(new Dimension(200, 400));
			scrollPane.getVerticalScrollBar().setUnitIncrement(10);
		}
		return scrollPane;
	}

	/**
	 * Create the SpectrumPlot if needed then return it.
	 *
	 * @return the SpectrumPlot.
	 * @see eu.omp.irap.cassis.gui.plot.save.SavePlotInterface#getSpectrumPlot()
	 */
	@Override
	public SpectrumPlot getSpectrumPlot() {
		if (spectrumPlot == null) {
			spectrumPlot = new SpectrumPlot();

			spectrumPlot.setBottomTitle(model.getXAxisCassis().getTitleLabel());
			spectrumPlot.setLeftTitle(model.getYAxisCassis().toString());
			spectrumPlot.setXAxisCassisTop(model.getXAxisCassisTop());

			((ChartPanelCassis) spectrumPlot.getChartPanel()).setLineEstimable(control);

			model.setCenterDataset(spectrumPlot.getCenterXYSeriesCollection());
			model.setTopLineDataset(spectrumPlot.getTopLineSeriesCollection());
			model.setTopLineErrorDataset(spectrumPlot.getTopLineErrorSeriesCollection());
			model.setBottomLineSignalDataset(spectrumPlot.getBottomLineSignalSeriesCollection());
			model.setBottomLineImageDataset(spectrumPlot.getBottomLineImageSeriesCollection());

			spectrumPlot.setChartMouseListener(model, null, messageControl, control, control);
			spectrumPlot.setExternalChangeRenderingInterface(control);
			updateRendering(false);
		}
		return spectrumPlot;
	}

	/**
	 * Creates the {@link InfoPanel} if necessary then return it.
	 *
	 * @return the {@link InfoPanel}.
	 */
	public InfoPanel getInfoPanel() {
		if (infoPanel == null) {
			infoPanel = new InfoPanel();
			infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.Y_AXIS));
			infoPanel.getControl().addInfoPanelListener(control);
		}
		return infoPanel;
	}

	/**
	 * Build the north overlay panel.
	 *
	 * @return the north overlay panel.
	 */
	private JPanel getOverlayPanel() {
		if (overlayPanel == null) {
			JLabel dataFile = new JLabel("Data file");
			JLabel lineList = new JLabel("LineList");

			overlayPanel = new JPanel();
			overlayPanel.setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Overlays"),
					BorderFactory.createEmptyBorder(2, 2, 2, 2)));

			overlayPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
			JPanel temp = new JPanel(new GridLayout(2, 2, 2, 2));
			temp.add(dataFile);
			temp.add(getDataFileButton());
			temp.add(lineList);
			temp.add(getLineListButton());
			overlayPanel.add(temp);
		}
		return overlayPanel;
	}

	/**
	 * @return the lineListButton
	 */
	public JButton getLineListButton() {
		if (lineListButton == null) {
			lineListButton = new JButton("Select");
			lineListButton.setEnabled(true);
			lineListButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.onLineListButtonClicked();
				}
			});
		}
		return lineListButton;
	}

	/**
	 * Build the south panel composed of the move button, the zoom, the search
	 * and the range parameter.
	 *
	 * @return JPanel
	 */
	private JTabbedPane getToolsChartTabbedPanel() {
		if (toolsChartTabbedPanel == null) {
			toolsChartTabbedPanel = new JTabbedPane();
			toolsChartTabbedPanel.addTab("X Tools", getAxisXToolsPanel());
			toolsChartTabbedPanel.addTab("Y Tools", getAxisYToolsPanel());
			toolsChartTabbedPanel.addTab("Stack/Mosaic", getStackMosaicPanel());

			toolsChartTabbedPanel.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					changeState(toolsChartTabbedPanel.getSelectedComponent().getName());
				}
			});
		}
		return toolsChartTabbedPanel;
	}

	private JPanel getAxisYToolsPanel() {
		if (axisYToolsPanel == null) {
			axisYToolsPanel = new JPanel(new BorderLayout());
			axisYToolsPanel.setName(ToolsState.Y.name());
			axisYToolsPanel.add(getChartYMovePanel(), BorderLayout.CENTER);
			axisYToolsPanel.add(getYAxisPanel(), BorderLayout.EAST);
		}
		return axisYToolsPanel;
	}

	private JPanel getYAxisPanel() {
		if (yAxisPanel == null) {
			yAxisPanel = new JPanel(new FlowLayout());
			JPanel tmpPanel = new JPanel(new GridLayout(2, 1));
			JPanel yAxisLeftPanel = new JPanel(new BorderLayout());

			yAxisLeftPanel.add(buildYAxisComboPanel("<HTML><p>Y<sub>lef</sub></p></HTML>", getYAxisLeftComboBox()), BorderLayout.CENTER);
			tmpPanel.add(yAxisLeftPanel);
			JPanel yAxisRightPanel = new JPanel(new BorderLayout());
			yAxisRightPanel.add(buildYAxisComboPanel("<HTML><p>Y<sub>rig</sub></p></HTML>", getYAxisRightComboBox()), BorderLayout.CENTER);
			tmpPanel.add(yAxisRightPanel);
			yAxisPanel.add(tmpPanel);
			yAxisPanel.add(getYLogCheckBox());
		}
		return yAxisPanel;
	}

	private JComboBox<YAxisCassis> getYAxisRightComboBox() {
		if (yAxisRigthComboBox == null) {
			yAxisRigthComboBox = new JComboBox<>(YAxisCassis.getAllYAxisCassis());
			yAxisRigthComboBox.setSelectedItem(model.getYAxisCassis());
			yAxisRigthComboBox.setEnabled(false);
		}

		return yAxisRigthComboBox;
	}

	public JComboBox<YAxisCassis> getYAxisLeftComboBox() {
		if (yAxisLeftComboBox == null) {
			yAxisLeftComboBox = new JComboBox<>(YAxisCassis.getAllYAxisCassis());
			yAxisLeftComboBox.setSelectedItem(model.getYAxisCassis());
			yAxisLeftComboBox.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					control.yAxisLeftChanged();
				}
			});
		}

		return yAxisLeftComboBox;
	}

	private JPanel buildXAxisComboPanel(String label, JComboBox<XAxisCassis> comboBox, JComboBox<String> comboBoxType) {
		JLabel topLabel = new JLabel(label);
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		topPanel.add(topLabel);
		topPanel.add(comboBoxType);
		topPanel.add(comboBox);

		comboBox.setSize(100, 24);
		comboBox.setPreferredSize(new Dimension(100, 24));

		return topPanel;
	}

	private ChartMovePanel getChartYMovePanel() {
		if (chartYMovePanel == null) {
			chartYMovePanel = new ChartMovePanel(getSpectrumPlot().getChartPanel(),
					getSpectrumPlot().getPlot().getRangeAxis(), ToolsState.Y);
		}
		return chartYMovePanel;
	}

	private JPanel getAxisXToolsPanel() {
		if (axisXToolsPanel == null) {
			axisXToolsPanel = new JPanel(new BorderLayout());
			axisXToolsPanel.setName(ToolsState.X.name());
			axisXToolsPanel.add(getChartXMovePanel(), BorderLayout.CENTER);
			axisXToolsPanel.add(getXAxisPanel(), BorderLayout.EAST);
		}
		return axisXToolsPanel;
	}

	private JPanel getXAxisPanel() {
		if (xAxisPanel == null) {
			xAxisPanel = new JPanel(new FlowLayout());
			JPanel tmpPanel = new JPanel(new GridLayout(2, 1));
			tmpPanel.add(getXAxisTopPanel());
			tmpPanel.add(getXAxisBottomPanel());
			xAxisPanel.add(tmpPanel);
			xAxisPanel.add(getXLogCheckBox());
		}
		return xAxisPanel;
	}

	private JPanel getXAxisBottomPanel() {
		if (xAxisBottomPanel == null) {
			xAxisBottomPanel = new JPanel(new BorderLayout());
			xAxisBottomPanel.add(
					buildXAxisComboPanel("<HTML><p>X<sub>bot</sub></p></HTML>",
							getComboBottomtAxis(), getComboBottomAxisType()), BorderLayout.CENTER);
		}
		return xAxisBottomPanel;
	}

	public JComboBox<String> getComboTopAxisType() {
		if (comboTopAxisType == null) {
			comboTopAxisType = new JComboBox<>(XAxisCassis.getAllXAxisCassisType());
			comboTopAxisType.setSelectedItem(XAxisCassisUtil.getType(model.getXAxisCassisTop()));
			comboTopAxisType.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					XAxisCassisUtil.changeXAxisAccordingToType(comboTopAxisType.getSelectedItem().toString(), getComboTopAxis());
				}
			});
		}
		return comboTopAxisType;
	}

	public JComboBox<XAxisCassis> getComboTopAxis() {
		if (comboTopAxis == null) {
			comboTopAxis = new JComboBox<>(XAxisCassis.getAllXAxisForType(getComboTopAxisType().getSelectedItem().toString()));
			comboTopAxis.setSelectedItem(model.getXAxisCassisTop());
			comboTopAxis.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.comboTopAxisChanged();
					XAxisCassisUtil.changeType(comboTopAxis, getComboTopAxisType());
				}
			});
		}
		return comboTopAxis;
	}

	private ChartMovePanel getChartXMovePanel() {
		if (chartXMovePanel == null) {
			chartXMovePanel = new ChartMovePanel(getSpectrumPlot().getChartPanel(),
					getSpectrumPlot().getPlot().getDomainAxis(), ToolsState.X);
		}
		return chartXMovePanel;
	}

	/**
	 * Build the Navigation panel.
	 */
	@Override
	public StackMosaicPanel getStackMosaicPanel() {
		if (stackMosaicPanel == null) {
			stackMosaicPanel = new StackMosaicPanel(model.getStackMosaicModel());
			stackMosaicPanel.setName(ToolsState.STACK.name());
			stackMosaicPanel.setCurrentSpectrumEditable(true);
			stackMosaicPanel.addKeyCurrentSpectrumListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					control.currentSpectrumKeyPressed(e);
				}
			});

			stackMosaicPanel.addMoveRightListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.moveRightClicked();
				}
			});

			stackMosaicPanel.addMoveLeftListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.moveLeftClicked();
				}
			});

			stackMosaicPanel.addStepMoveRightListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.stepMoveRightCliked();
				}
			});

			stackMosaicPanel.addStepMoveLeftListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.stepMoveLeftCliked();
				}
			});
		}

		return stackMosaicPanel;
	}

	@Override
	public CassisSaveViewInterface getSave() {
		if (save == null) {
			save = new SaveAbstractPlot(this);
		}
		return save;
	}

	public void changeState(String state) {
		if (state == null)
			return;
		if (state.equals(ToolsState.X.name())) {
			this.toolsState = ToolsState.X;
			double range = chartXMovePanel.getUpperBoundDomain()
					- chartXMovePanel.getLowerBoundDomain();
			double search = chartXMovePanel.getLowerBoundDomain() + range;

			chartXMovePanel.setRange(ChartMovePanel.arrondi(range, 2));
			chartXMovePanel.setSearch(ChartMovePanel.arrondi(search, 2));
		} else if (state.equals(ToolsState.Y.name())) {
			this.toolsState = ToolsState.Y;
			double range = chartYMovePanel.getUpperBoundRange()
					- chartYMovePanel.getLowerBoundRange();
			double search = chartYMovePanel.getLowerBoundDomain() + range;
			chartYMovePanel.setRange(ChartMovePanel.arrondi(range, 2));
			chartYMovePanel.setSearch(ChartMovePanel.arrondi(search, 2));
		} else if (state.equals(ToolsState.STACK.name())) {
			this.toolsState = ToolsState.STACK;
		}
	}

	@Override
	public Component getComponent() {
		return this;
	}

	@Override
	public List<LineDescription> getSyntheticLineVisible() {
		return model.getAllSyntheticLines();
	}

	@Override
	public List<LineDescription> getOtherSpeciesLineVisible() {
		return model.getAllOtherSpeciesLines();
	}

	@Override
	public int print(Graphics graphics, PageFormat pageFormat, int pageIndex)
			throws PrinterException {
		return PrintUtil.print(graphics, pageFormat, pageIndex,
				getSave().getImageToPrint());
	}

	@Override
	public void restorePanel(JComponent panel) {
		westPanel.add(panel, BorderLayout.CENTER);
		westPanel.revalidate();
	}

	@Override
	public boolean checkIfSaveIsPossible(File file, EXTENSION ext) {
		if (file.exists()) {
			String message = "This file " + file.toString() +
					" already exists.\nDo you want to replace it?";

			int result = JOptionPane.showConfirmDialog(null, message,
					"Replace existing file?", JOptionPane.YES_NO_OPTION);
			if (result == JOptionPane.NO_OPTION) {
				return false;
			}
		}
		return true;
	}

	public ToolsState getToolsState() {
		return toolsState;
	}

	/**
	 * @return the lastFitPanel
	 */
	public final FitPanel getLastFitPanel() {
		return lastFitPanel;
	}

	public FitPanel getCurrentFitPanel() {
		return fitPanelList.get(model.getCurrentNumTriplePlot());
	}

	public ToolsView getToolsView() {
		if (toolsView == null) {
			if (model.isOnGallery()) {
				toolsView = new ToolsView(model.getToolsMosaicModel());
			} else {
				if (!model.getListToolsModel().isEmpty()) {
					toolsView = new ToolsView(model.getCurrentToolsModel());
				}
			}
		}
		return toolsView;
	}

	public void replaceToolsPanel() {
		int indiceTab = getIndiceTabByName("Tools");
		replacingToolsTab = true;
		if (indiceTab != -1) {
			tabManager.remove(indiceTab);
			toolsView.getControl().removeListeners();
			toolsView = null;
			tabManager.insertTab("Tools", null, getToolsView(), null, indiceTab);
		} else {
			tabManager.addTab("Tools", getToolsView());
		}
		replacingToolsTab = false;
	}

	public int getIndiceTabByName(String aName) {
		if (tabManager == null)
			return -1;

		int nbTab = tabManager.getTabCount();
		for (int i = 0; i < nbTab; i++) {
			if (tabManager.getTitleAt(i).equals(aName)) {
				return i;
			}
		}
		return -1;
	}

	public void changeFitParameters(FitPanel fitPanel) {
		FitPanel last = getLastFitPanel();
		if (last != null) {
			fitPanel.getModel().setFitStyle(last.getModel().getFitStyle());
			fitPanel.getModel().setNbIterations(last.getModel().getNbIterations());
			fitPanel.getModel().setOversamplingFit(last.getModel().getOversamplingFit());
			fitPanel.getModel().setTolerance(last.getModel().getTolerance());
		}
	}

	private FitPanel createFitPanel(CassisPlot cassisPlot) {
		final FitPanel fitPanel = new FitPanel(new MarkerManager(control), false,
				control.getFitControl());
		fitPanel.setXAxis(cassisPlot.getxAxisCassis());

		fitPanel.initHistory(cassisPlot.getFileSpectrumSeries());
		fitPanel.addFitPanelListener(control.getSpectrumFitPanelListener());
		fitPanel.addFitPanelListener(new FitPanelAdapteur() {
			@Override
			public void fitAllClicked(EventObject ev) {
				control.fitAllClicked(fitPanel);
			}

			@Override
			public void substractFitClicked(EventObject ev) {
				if (control.isOnGallery()) {
					control.fitSubtractAll();
				}
			}

			@Override
			public void divideByFitClicked(EventObject object) {
				if (control.isOnGallery()) {
					control.fitDivideAll();
				}
			}
		});
		return fitPanel;
	}

	private JPanel getToolbar() {
		if (toolbar == null) {
			toolbar = new JPanel(new FlowLayout(FlowLayout.RIGHT));
			toolbar.add(getFreqRefLabel());
			toolbar.add(getBackButton());
			getSpectrumPlot().add(toolbar, BorderLayout.SOUTH);
			toolbar.setVisible(true);
			getBackButton().addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					switchBack();
				}
			});
		}
		return toolbar;
	}

	public JLabel getFreqRefLabel() {
		if (freqRef == null) {
			freqRef = new JLabel("Reference frequency:");
		}
		return freqRef;
	}

	public JCheckBox getBackButton() {
		if (backButton == null) {
			backButton = new JCheckBox("", CassisRessource.createImageIcon("refresh24.png"));
			backButton.setToolTipText("Return to the gallery");
		}
		return backButton;
	}

	public void displayDataset() {
		displayDataset(model.getCurrentNumTriplePlot());
	}

	public void displayDataset(int numSpectrum) {
		stackMosaicPanel.move(numSpectrum);
		stackMosaicPanel.setMoveCurrentSpectrum(true);
		chartXMovePanel.setChartAxis(getSpectrumPlot().getPlot().getDomainAxis());
		changeState(ToolsState.X.name());

		infoPanel.setModel(model.getListInfoModels().get(numSpectrum));
		infoPanel.repaint();

		model.removeAllSeriesDisplay();
		model.addSeriesToDisplay();

		control.replaceFitPanel(getCurrentFitPanel());
		control.yAxisLeftChanged();
		control.comboBottomAxisChanged();
		spectrumPlot.getChartPanel().repaint();

		XYPlotCassisUtil.configureRenderer(spectrumPlot);
		checkColorFromLineConfigCurveToInfoPanel();
	}

	private JPanel getGalleryPane() {
		if (galleryPane == null) {
			galleryPane = new JPanel(new BorderLayout());
		}
		return galleryPane;
	}

	public GalleryNavigation getGalleryNavigation() {
		if (galleryNavi == null) {
			galleryNavi = new GalleryNavigation(this, model.getGalleryNavigationModel());
		}
		return galleryNavi;
	}

	protected void initStackMosaicPanel() {
		stackMosaicPanel.setValue(0, model.getNbCassisPlots());
		double stepValeuDefaut = (model.getListCassisPlots().get(0).getDataSpectrum().getFrequencySignalMax() - model
				.getListCassisPlots().get(0).getDataSpectrum().getFrequencySignalMin()) / 1000;
		DecimalFormat decimalFormat;
		DecimalFormatSymbols ds = new DecimalFormatSymbols();
		ds.setDecimalSeparator('.');
		decimalFormat = new DecimalFormat("0.##", ds);
		getStackMosaicPanel().getStepValueField().setText(decimalFormat.format(stepValeuDefaut * 1E3));
	}

	@Override
	public void showBy(GalleryOption type, List<Integer> filter, int currentGallery) {
		if (gallerySortPane != null) {
			getGalleryPane().remove(gallerySortPane);
			gallerySortPane.clean();
		}

		gallerySortPane = GallerySortPane.getGallerySortPane(type,
				model.getGalleryNavigationModel().getRows(),
				model.getGalleryNavigationModel().getCols(),
				model.getListCassisPlots(), filter, currentGallery,
				model.isPlotNumberVisible(), model.getPlotNumberColor(),
				getYLogCheckBox().isSelected(), getXLogCheckBox().isSelected(),
				model.getRendering(), control, getMarkersList());

		getGalleryPane().add(gallerySortPane, BorderLayout.CENTER);

		for (final SpectrumPlot spectrumPlot : gallerySortPane.getListSpectrumPlots()) {
			spectrumPlot.getChartPanel().addChartMouseListener(new ChartMouseListener() {
				@Override
				public void chartMouseClicked(ChartMouseEvent event) {
					control.galleryPlotPanelClicked(event.getTrigger(),
							spectrumPlot.getRowId(), spectrumPlot.getColId());
				}
				@Override
				public void chartMouseMoved(ChartMouseEvent event) {
					// Can not be moved here, not used.
				}
			});
		}
		control.comboTopAxisChanged();
		control.yAxisLeftChanged();
		validate();
	}

	@Override
	public void switchTo(int i, int j) {
		model.setOnGallery(false);
		getStackMosaicPanel().getModel().setFilterText(getGalleryNavigation().getListPlotTextField().getText());
		getStackMosaicPanel().getModel().setFilter(model.getGalleryNavigationModel().getFilter());

		westPanel.remove(getGalleryPane());
		toolsPanel.add(getToolbar(), BorderLayout.CENTER);
		GalleryNavigationModel galNavModel = model.getGalleryNavigationModel();
		displayDataset(galNavModel.getFilter().get(i * galNavModel.getCols() + j));
		westPanel.add(layeredPane, BorderLayout.CENTER);
		int selectedIndex = getToolsChartTabbedPanel().getSelectedIndex();
		getToolsChartTabbedPanel().remove(2);
		getToolsChartTabbedPanel().addTab("Stack", getStackMosaicPanel());
		getToolsChartTabbedPanel().setSelectedIndex(selectedIndex);

		checkColorFromLineConfigCurveToInfoPanel();
		replaceToolsPanel();

		refreshRefFreq();

		spectrumPlot.setSize(layeredPane.getSize());
		westPanel.validate();
		westPanel.repaint();
		updateUI();
	}

	@Override
	public void switchBack() {
		if (model.getListCassisPlots().isEmpty())
			return;
		model.setOnGallery(true);
		westPanel.remove(layeredPane);
		toolsPanel.remove(getToolbar());
		westPanel.add(getGalleryPane(), BorderLayout.CENTER);

		getToolsChartTabbedPanel().remove(2);
		getToolsChartTabbedPanel().addTab("Mosaic", getGalleryNavigation());

		if (gallerySortPane != null)
			gallerySortPane.refreshPlotPaneFromConfigCurve();

		replaceToolsPanel();
		regenerateMosaicInfoPanel();
		selectMosaicPanel();

		westPanel.validate();
		updateUI();
	}

	protected void selectMosaicPanel() {
		if (toolsChartTabbedPanel != null && toolsChartTabbedPanel.getTabCount() == 3)
			toolsChartTabbedPanel.setSelectedIndex(2);
	}

	protected void createFitPanelList() {
		fitPanelList = new ArrayList<>(model.getNbCassisPlots());
		for (int cpt = 0; cpt <  model.getNbCassisPlots(); cpt++) {
			final FitPanel fitPanel = createFitPanel(model.getListCassisPlots().get(cpt));
			fitPanelList.add(fitPanel);
		}
		if (!fitPanelList.isEmpty()) {
			FitPanel fp = fitPanelList.get(fitPanelList.size() - 1);
			control.replaceFitPanel(fp);
		}
		control.updateNumberOfPlotInFitListener();
	}

	protected void createListInfoModels() {
		model.getListInfoModels().clear();

		for (int cpt = 0; cpt < model.getNbCassisPlots(); cpt++) {
			CassisPlot cassisPlot = model.getListCassisPlots().get(cpt);
			InfoModel infoModel = createInfoModel(cassisPlot);
			model.getListInfoModels().add(infoModel);
		}
	}

	protected void initOtherSpeciesPanel() {
		control.getSpeciesControl().initValue(model.getListCassisPlots().get(0).getVlsrData(),
				model.getTypeFrequency());

		otherSpeciesPanel.setShowSpeciesImageEnabled(!Double.isNaN(model.getLoFrequency()));
	}

	protected InfoModel createInfoModel(CassisPlot cassisPlot) {
		InfoModel infoModel = new InfoModel();

		JPanelCurve filePanelCurve = getFilePanelCurve(cassisPlot);
		infoModel.addPanelCurve(filePanelCurve);
		return infoModel;
	}

	/**
	 * @param lastFitPanel
	 *            the lastFitPanel to set
	 */
	public final void setLastFitPanel(FitPanel lastFitPanel) {
		this.lastFitPanel = lastFitPanel;
	}

	@Override
	public GallerySortPane getGallerySortPane() {
		return gallerySortPane;
	}

	public void checkLineConfigCurveFor(CurvePanelView curvePanelView) {
		if (getLineConfigCurve().isThatCurveExist(curvePanelView.getNameFromModel())) {
			ConfigCurve configCurve = getLineConfigCurve().getConfigCurve(curvePanelView.getNameFromModel());
			curvePanelView.getButtonColor().setBackground(configCurve.getColor());
			curvePanelView.getCheckBox().setSelected(configCurve.isVisible());
			curvePanelView.getModel().getCassisModel().setConfigCurve(configCurve);
		} else {
			getLineConfigCurve().setElement(
					curvePanelView.getNameFromModel(),
					curvePanelView.getModel().getCassisModel().getConfigCurve().copy());
		}
	}

	public void checkColorFromLineConfigCurveToInfoPanel() {
		for (JPanelCurve jPanelCurve : getInfoPanel().getModel().getListOfJPanelCurves()) {
			for (CurvePanelView curvePanelView : jPanelCurve.getListCurvePanelView()) {
				checkLineConfigCurveFor(curvePanelView);
			}
		}
	}

	public void regenerateMosaicInfoPanel() {
		getInfoPanel().setModel(new InfoModel());
		recreatePlotsMosaic();
		final List<InfoModel> listInfoModels = model.getListInfoModels();
		InfoModel infoModel = getLineConfigCurve().generateMosaicInfoModel(listInfoModels,
				model.isPlotNumberVisible(), model.getPlotNumberColor());

		getInfoPanel().setModel(infoModel);
	}

	public void recreatePlotsMosaic() {
		try {
			GalleryNavigationModel galNavModel = model.getGalleryNavigationModel();
			showBy(galNavModel.getSelectedOption(), galNavModel.getFilter(), galNavModel.getCurrentGallery() - 1);
		} catch (NullPointerException e) {
			LOGGER.trace("Unable to recreate gallery, no data to display?", e);
			// Ignore, do nothing in this case as we don't have data to display.
		}
	}

	public LineConfigCurve getLineConfigCurve() {
		if (lineConfigCurve == null)
			lineConfigCurve = new LineConfigCurve(control, control);
		return lineConfigCurve;
	}

	@Override
	public void refreshCurrentTriplePlot() {
		final int currentSpectrum = model.getStackMosaicModel().getCurrentSpectrum();
		displayDataset(currentSpectrum);
		getSpectrumPlot().refreshPopups(currentSpectrum);
		refreshRefFreq();
		replaceToolsPanel();
	}

	public void setXAxisOfFitPanel() {
		final ArrayList<CassisPlot> listCassisPlots = model.getListCassisPlots();
		for (int i = 0; i < listCassisPlots.size(); i++) {
			fitPanelList.get(i).setXAxis(listCassisPlots.get(i).getxAxisCassis());
		}
	}

	public void refreshRefFreq() {
		DecimalFormatSymbols formatSymbol = new DecimalFormatSymbols();
		formatSymbol.setDecimalSeparator('.');
		DecimalFormat twoDecimalFormat = new DecimalFormat("0.00", formatSymbol);

		String refFreq = twoDecimalFormat.format(model.getCurrentCassisPlot().getFreqRef());
		getFreqRefLabel().setText("Reference frequency: " + refFreq);
	}

	@Override
	public void comboBottomAxisChanged() {
		getControl().comboBottomAxisChanged();
	}

	@Override
	public boolean isOnGallery() {
		return model.isOnGallery();
	}

	@Override
	public List<CassisPlot> getListCassisPlots() {
		return model.getListCassisPlots();
	}

	@Override
	public MessageControl getMessageControl() {
		return messageControl;
	}

	private JCheckBox getXLogCheckBox() {
		if (xLogCheckBox == null) {
			xLogCheckBox = new JCheckBox("Log");
			xLogCheckBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					boolean log = xLogCheckBox.isSelected();
					control.changeXAxisType(log);
				}
			});
		}
		return xLogCheckBox;
	}

	private JCheckBox getYLogCheckBox() {
		if (yLogCheckBox == null) {
			yLogCheckBox = new JCheckBox("Log");
			yLogCheckBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					boolean log = yLogCheckBox.isSelected();
					control.changeYAxisType(log);
				}
			});
		}
		return yLogCheckBox;
	}

	public List<FitPanel> getFitPanelList() {
		return fitPanelList;
	}

	@Override
	public void moveLeft() {
		control.moveLeftClicked();
	}

	@Override
	public void moveRight() {
		control.moveRightClicked();
	}

	/**
	 * Return the names of the visible and savable spectra on all the plot.
	 *
	 * @see eu.omp.irap.cassis.gui.plot.save.SavePlotInterface#getVisibleSavableSpectra()
	 */
	@Override
	public List<String> getVisibleSavableSpectra() {
		List<JPanelCurve> listJpc = infoPanel.getModel().getListOfJPanelCurves();
		List<String> listCurveVisible = new ArrayList<>();
		for (JPanelCurve jpc : listJpc) {
			for (CurvePanelView cpv : jpc.getListCurvePanelView()) {
				XYSeriesCassis series = cpv.getModel().getCassisModel();
				if (cpv.getCheckBox().isSelected() && TypeCurve.isSavable(series) &&
						!listCurveVisible.contains(series.getKey().toString())) {
					listCurveVisible.add(series.getKey().toString());
				}
			}
		}
		return listCurveVisible;
	}

	/**
	 * Update the labels of the plot.
	 */
	public void updatePlotLabels() {
		getSpectrumPlot().setBottomTitle(model.getXAxisCassis().getTitleLabel());
		getSpectrumPlot().setLeftTitle(model.getYAxisCassis().toString());
		getSpectrumPlot().setXAxisCassisTop(model.getXAxisCassis());
	}

	/**
	 * Update the rendering type of the TriplePlot and eventually gallery.
	 *
	 * @param updateGallery true to update the rendering of the gallery too,
	 *  false to not update it.
	 */
	public void updateRendering(boolean updateGallery) {
		switch (model.getRendering()) {
		case LINE:
			getSpectrumPlot().setLineRendering();
			break;

		case DOT:
			getSpectrumPlot().setDotRendering();
			break;

		case HISTOGRAM:
			getSpectrumPlot().setHistogramRendering();
			break;

		default:
			throw new IllegalArgumentException("Unknown rendering type");
		}
		if (updateGallery) {
			recreatePlotsMosaic();
		}
	}

	/**
	 * Compute then return the list of markers by plot.
	 *
	 * @return the list of markers by plot.
	 */
	public List<ArrayList<InterValMarkerCassis>> getMarkersList() {
		List<ArrayList<InterValMarkerCassis>> listMarkersByPlot = new ArrayList<>(
				control.getModel().getNbCassisPlots());
		for (FitPanel fitPanel : fitPanelList) {
			listMarkersByPlot.add((ArrayList<InterValMarkerCassis>)
					fitPanel.getMarkerManager().getListMarker());
		}
		return listMarkersByPlot;
	}
}
