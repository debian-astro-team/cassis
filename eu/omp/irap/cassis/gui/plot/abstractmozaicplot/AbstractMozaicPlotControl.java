/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.abstractmozaicplot;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.UnknownKeyException;
import org.jfree.data.xy.XYSeriesCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.CassisException;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.Formula;
import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.TypeFrequency;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.file.linelist.RotationalLineListFileReader;
import eu.omp.irap.cassis.file.linelist.SpectralLineListFileReader;
import eu.omp.irap.cassis.fit.FitParametersModel;
import eu.omp.irap.cassis.fit.components.FitAbstractComponent;
import eu.omp.irap.cassis.fit.computing.FitEstimator;
import eu.omp.irap.cassis.fit.util.Category;
import eu.omp.irap.cassis.fit.util.FitException;
import eu.omp.irap.cassis.fit.util.enums.FitType;
import eu.omp.irap.cassis.gui.PanelFrame;
import eu.omp.irap.cassis.gui.fit.FitOperation;
import eu.omp.irap.cassis.gui.fit.FitPanel;
import eu.omp.irap.cassis.gui.fit.FitPanelControl;
import eu.omp.irap.cassis.gui.fit.FitPanelListener;
import eu.omp.irap.cassis.gui.fit.FitStyleEnum;
import eu.omp.irap.cassis.gui.fit.FittingItem;
import eu.omp.irap.cassis.gui.fit.SpectrumFitPanelListener;
import eu.omp.irap.cassis.gui.fit.advanced.AdvancedFitModel;
import eu.omp.irap.cassis.gui.fit.advanced.AdvancedFitResult;
import eu.omp.irap.cassis.gui.fit.advanced.FitResultInterface;
import eu.omp.irap.cassis.gui.fit.advanced.ModelFitManager;
import eu.omp.irap.cassis.gui.fit.advanced.MolCategory;
import eu.omp.irap.cassis.gui.fit.advanced.gui.AdvancedFitFrame;
import eu.omp.irap.cassis.gui.fit.advanced.history.FitSubstractCommand;
import eu.omp.irap.cassis.gui.fit.advanced.interfaces.FitActionsInterface;
import eu.omp.irap.cassis.gui.fit.advanced.interfaces.FitSourceInterface;
import eu.omp.irap.cassis.gui.fit.save.SaveFitInterface;
import eu.omp.irap.cassis.gui.menu.action.SpectrumManagerAction;
import eu.omp.irap.cassis.gui.model.spectrumanalysis.SpectrumAnalysisModel;
import eu.omp.irap.cassis.gui.otherspecies.SpeciesColorChangedEvent;
import eu.omp.irap.cassis.gui.otherspecies.SpeciesControl;
import eu.omp.irap.cassis.gui.otherspecies.SpeciesEnableEvent;
import eu.omp.irap.cassis.gui.otherspecies.SpeciesModel;
import eu.omp.irap.cassis.gui.plot.abstractplot.AbstractPlotControl;
import eu.omp.irap.cassis.gui.plot.curve.CurveCassisMosaicListener;
import eu.omp.irap.cassis.gui.plot.curve.CurvePanelModel;
import eu.omp.irap.cassis.gui.plot.curve.CurvePanelMosaicControl;
import eu.omp.irap.cassis.gui.plot.curve.CurvePanelView;
import eu.omp.irap.cassis.gui.plot.curve.JPanelCurve;
import eu.omp.irap.cassis.gui.plot.curve.Rendering;
import eu.omp.irap.cassis.gui.plot.curve.TypeCurve;
import eu.omp.irap.cassis.gui.plot.curve.XYLineSeriesCassis;
import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassis;
import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassisCollection;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import eu.omp.irap.cassis.gui.plot.curve.config.ColorsCurve;
import eu.omp.irap.cassis.gui.plot.curve.config.CurveParameters;
import eu.omp.irap.cassis.gui.plot.gallery.GalleryNavigationModel;
import eu.omp.irap.cassis.gui.plot.gallery.GallerySortPane;
import eu.omp.irap.cassis.gui.plot.infopanel.InfoModel;
import eu.omp.irap.cassis.gui.plot.infopanel.InfoPanelConstants;
import eu.omp.irap.cassis.gui.plot.line.CassisPlot;
import eu.omp.irap.cassis.gui.plot.simple.SpectrumPlot;
import eu.omp.irap.cassis.gui.plot.simple.util.XYPlotCassisUtil;
import eu.omp.irap.cassis.gui.plot.util.InterValMarkerCassis;
import eu.omp.irap.cassis.gui.plot.util.MarkerManager;
import eu.omp.irap.cassis.gui.plot.util.SORTING_PLOT;
import eu.omp.irap.cassis.gui.plot.util.StackMosaicModel;
import eu.omp.irap.cassis.parameters.LineIdentificationUtils;
import eu.omp.irap.cassis.properties.Software;

public abstract class AbstractMozaicPlotControl extends AbstractPlotControl implements CurveCassisMosaicListener{

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractMozaicPlotControl.class);

	protected AbstractMozaicPlotModel model;
	protected AbstractMozaicPlotView view;
	private SpeciesControl speciesControl;
	private FitPanelControl fitControl;

	protected SaveFitInterface saveFit;
	protected FitPanelListener spectrumFitPanelListener;

	private boolean replacingFitPanel;


	protected AbstractMozaicPlotControl(AbstractMozaicPlotModel model) {
		this.model = model;
		this.model.getFitModelManager().addModelListener(this);
		speciesControl = new SpeciesControl(model.getSpeciesModel());
		fitControl = new FitPanelControl();
		model.getStackMosaicModel().addModelListener(this);
		model.addModelListener(this);
	}

	protected abstract void changeLineSorting(SORTING_PLOT sortingLine);

	public abstract AbstractMozaicPlotView getView();

	public abstract AbstractMozaicPlotModel getModel();

	/**
	 * @see eu.omp.irap.cassis.gui.plot.util.SpectrumFitPanelInterface#saveAllLineAnalysisInCurrentFile()
	 */
	@Override
	public abstract void saveAllLineAnalysisInCurrentFile();

	/**
	 * Action to do on overlay data button clicked.
	 */
	public abstract void onOverlayPanelDataFileButtonClicked();

	public void comboBottomAxisChanged() {
		if (view.getComboBottomtAxis().getSelectedItem() == null) {
			return;
		}

		XAxisCassis previousXAxis = model.getXAxisCassis();

		XAxisCassis xAxisCassis = (XAxisCassis) view.getComboBottomtAxis().getSelectedItem();
		xAxisCassis.setTypeFrequency(model.getTypeFrequency());
		model.setXAxisCassis(xAxisCassis);
		view.getSpectrumPlot().setBottomTitle(xAxisCassis.getTitleLabel());
		view.setXAxisOfFitPanel();
		if (previousXAxis.getUnit() != xAxisCassis.getUnit()) {
			view.recreatePlotsMosaic();
		}
		changeLeftLabel();
		comboTopAxisChanged();
		view.getToolsView().getControl().refreshInfo();

		model.getFitModelManager().refreshInputs();
		model.getFitModelManager().convertConfiguration(previousXAxis);
	}

	public void setView(AbstractMozaicPlotView view) {
		this.view = view;
	}

	public void onLineListButtonClicked() {
		JFileChooser chooser = new CassisJFileChooser(null, Software.getLastFolder("overlay-linelist"));
		chooser.addChoosableFileFilter(new FileNameExtensionFilter("Line List CASSIS (*.txt)","txt"));

		int returnVal = chooser.showOpenDialog(view);
		if (chooser.getSelectedFile() != null && returnVal == JFileChooser.APPROVE_OPTION) {
			Software.setLastFolder("overlay-linelist", chooser.getCurrentDirectory().getAbsolutePath());

			String filePath = chooser.getSelectedFile().getPath();
			displayLineList(filePath);
		}
	}

	public void displayLineList(String file) {
		List<LineDescription> lines;
		boolean recomputeFreqCompute = false;
		try {
			lines = extracteLineList(file);
		} catch (Exception e) {
			LOGGER.trace("Error while reading linelist file {} at first try.\n"
					+ " Probably another type of linelist file.", file, e);
			lines = SpectralLineListFileReader.getLines(file, Double.NaN);
			if (lines == null) {
				lines = RotationalLineListFileReader.getLines(file, Double.NaN);
			}
			if (lines == null) {
				JOptionPane.showMessageDialog(view, "Error while opening file.",
						null, JOptionPane.ERROR_MESSAGE);
			} else {
				recomputeFreqCompute = true;
			}
		}

		displayLineList(file, lines, recomputeFreqCompute);
	}

	public void displayLineList(String file, List<LineDescription> lines,
			boolean recomputeFreqCompute) {
		XYLineSeriesCassis overlayLineSeries;
		Color c = ColorsCurve.getNewColorLineList();

		String fileName = new File(file).getName();
		String seriesName;
		if (fileName == null || fileName.isEmpty()) {
			seriesName = getUniqueLineListSeriesName("LineList");
		} else {
			seriesName = getUniqueLineListSeriesName(fileName);
		}

		if (lines != null && !lines.isEmpty()) {
			for (int cpt = 0; cpt < model.getListCassisPlots().size(); cpt++) {
				CassisPlot cassisPlot = model.getListCassisPlots().get(cpt);
				overlayLineSeries = cassisPlot.createOverlayLineSeries(seriesName, lines, recomputeFreqCompute);
				if (overlayLineSeries != null) {
					overlayLineSeries.getConfigCurve().setColor(c);
					addSeriesinInfoModel(overlayLineSeries,
							InfoPanelConstants.LINELIST_OVERLAYS_TITLE,
							model.getListInfoModels().get(cpt));
				}
			}
		}

		if (model.isOnGallery())
			view.regenerateMosaicInfoPanel();
		else {
			view.refreshCurrentTriplePlot();
		}
		view.getInfoPanel().revalidate();
		view.getSpectrumPlot().revalidate();
	}

	/**
	 * Create an unique series name for LineList using a base.
	 *
	 * @param base The base for the series name (usually the file name or "LineList").
	 * @return the unique name to use.
	 */
	private String getUniqueLineListSeriesName(String base) {
		String id = base;
		int i = 2;
		while (!model.isSeriesNameAvailable(id, true)) {
			id = base + '_' + i;
			i++;
		}
		return id;
	}

	private List<LineDescription> extracteLineList(final String file)
			throws FileNotFoundException, IOException {
		List<LineDescription> lines = new ArrayList<>();
		String line = null;
		// read the file
		try (BufferedReader bufferReader = new BufferedReader(new FileReader(file))) {
			while ((line = bufferReader.readLine()) != null) {
				if (line.startsWith("#") || line.startsWith("Transition"))
					continue;
				String[] val = Pattern.compile("[\t]+").split(line);
				if (val.length == 2)
					continue;
				LineDescription lineId = new LineDescription();
				if (val.length == 8) {
					double freq = Double.parseDouble(val[2]);
					lineId.setObsFrequency(freq);
					lineId.setEUpK(Double.parseDouble(val[3]));
					lineId.setAij(Double.parseDouble(val[4]));
					lineId.setIdentification(
							LineIdentificationUtils.createHeightColumnsFileIdentification(
									val[0], freq, lineId.getEUpK(), lineId.getAij()));
				} else {
					double freq = Double.parseDouble(val[2]);
					double vlsr = Double.parseDouble(val[7]);
					lineId.setObsFrequency(Formula.calcFreqWithVlsr(freq, vlsr, freq));
					lineId.setEUpK(Double.parseDouble(val[3]));
					lineId.setAij(Double.parseDouble(val[4]));
					lineId.setVlsr(vlsr);
					lineId.setDObsFrequency(freq - Double.parseDouble(val[5]));
					lineId.setIdentification(
							LineIdentificationUtils.createOldFitFileIdentification(val[1],
									freq, lineId.getEUpK(), lineId.getAij(),
									lineId.getDObsFrequency(), lineId.getVlsr()));
				}
				lines.add(lineId);
			}
		}

		return lines;
	}

	/**
	 * Add load binding for overlay data.
	 *
	 * @param title The title to set for the Spectrum Manager window.
	 */
	public void addOverlayDataLoadBinding(String title) {
		Runnable r = new Runnable() {
			@Override
			public void run() {
				CassisSpectrum cs = SpectrumManagerAction.getInstance().getSelectedSpectrum();
				if (cs == null) {
					JOptionPane.showMessageDialog(null,
							"Please select a spectrum first.",
							"No spectrum selected!",
							JOptionPane.WARNING_MESSAGE);
				} else {
					cassisSpectrumEvent(new EventObject(cs));
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							PanelFrame.getInstance().setVisible(true);
							PanelFrame.getInstance().toFront();
							PanelFrame.getInstance().repaint();
						}
					});
				}
			}
		};
		SpectrumManagerAction.getInstance().setLoadBinding(r, title);
	}

	public boolean computeOtherSpecies() throws CassisException {
		LOGGER.info("Begin computing other species");
		boolean find = false;
		find = computeOtherSpeciesSignal();
		if (model.haveImage()) {
			find = computeOtherSpeciesImage()||find;
		}
		LOGGER.info("End computing other species");

		return find;
	}

	private boolean computeOtherSpeciesImage() {
		boolean find = false;
		for (CassisPlot cassisPlot : model.getListCassisPlots()) {
			if (!Double.isNaN(cassisPlot.getDataSpectrum().getLoFreq())) {
				List<LineDescription> otherLines = speciesControl.computeOtherSpecies(
						cassisPlot.getDataSpectrum().getFrequencySignalMax() +
						2 * (cassisPlot.getDataSpectrum().getLoFreq()-
								cassisPlot.getDataSpectrum().getFrequencySignalMax()),
						cassisPlot.getDataSpectrum().getFrequencySignalMin() +
						2 * (cassisPlot.getDataSpectrum().getLoFreq() -
							 cassisPlot.getDataSpectrum().getFrequencySignalMin()),
						false,
						"(image)");
				if (!otherLines.isEmpty()) {
					find = true;
				}

				cassisPlot.setOtherSpeciesImage(otherLines);
			}
		}
		return find;
	}

	private boolean computeOtherSpeciesSignal() {
		boolean find = false;
		for (CassisPlot cassisPlot : model.getListCassisPlots()) {
			List<LineDescription> otherLines =  speciesControl.computeOtherSpecies(
					cassisPlot.getDataSpectrum().getFrequencySignalMin(),
					cassisPlot.getDataSpectrum().getFrequencySignalMax(),
					true,
					"(signal)");

			if (!otherLines.isEmpty()) {
				find = true;
			}
			cassisPlot.setOtherSpeciesSignal(otherLines);
		}
		return find;
	}

	public SpeciesControl getSpeciesControl() {
		return speciesControl;
	}

	/**
	 * Ask user what he want to do about the wrong Y-Axis.
	 *
	 * @return if the user want to stop the operation.
	 */
	private boolean askWrongYAxis() {
		String message = "The data do not have the same Y-axis unit as the current plotted ones.\n"
				+ "It is advised to press \"Cancel\" and to choose data with the same Y-axis\n"
				+ "or to remove the current plot(s) before displaying again. If you know what\n"
				+ "you are doing, you can press \"Continue\", but be aware that the Y-axis\n"
				+ "scale and unit will likely be meaningless.";
		String[] choices = { "Continue", "Cancel" };
		int r = JOptionPane.showOptionDialog(view, message, "Warning : different Y-axis",
				JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null,
				choices, choices[1]);
		return r != 0;
	}

	@Override
	public void cassisSpectrumEvent(EventObject e) {
		CassisSpectrum cassisSpectrum = (CassisSpectrum) e.getSource();

		if (!model.getListCassisPlots().isEmpty() &&
				!cassisSpectrum.getYAxis().equals(model.getYAxisCassis()) &&
				askWrongYAxis()) {
			return;
		}

		int numberOverlaySpectrumSearched = 0;
		String seriesName = model.getUniqueSeriesName(cassisSpectrum.getTitle(), true);
		Color newColor = ColorsCurve.getNewColorLine();

		for (int cpt = 0; cpt < model.getListCassisPlots().size(); cpt++) {
			CassisPlot cassisPlot = model.getListCassisPlots().get(cpt);
			final double frequencyMin = cassisPlot.getDataSpectrum().getFrequencySignalMin();
			final double frequencyMax = cassisPlot.getDataSpectrum().getFrequencySignalMax();
			CommentedSpectrum[] spectrums = computeSpectrumAnalysis(
					cassisSpectrum, frequencyMin, frequencyMax);

			if (spectrums != null) {
				numberOverlaySpectrumSearched++;

				CommentedSpectrum commentedSpectrum = spectrums[0];
				commentedSpectrum.setTitle(seriesName);
				commentedSpectrum.setFreqRef(cassisPlot.getFreqRef());
				if (commentedSpectrum.getVlsr() == 0.0) {
					commentedSpectrum.setTypeFreq(TypeFrequency.SKY);
				} else {
					commentedSpectrum.setTypeFreq(TypeFrequency.REST);
				}

				XYSpectrumSeries serieCassis = cassisPlot.addOverlaySpectrum(commentedSpectrum);
				serieCassis.getConfigCurve().setColor(newColor);
				addSeriesinInfoModel(serieCassis,
						InfoPanelConstants.DATA_FILE_OVERLAYS_TITLE,
						model.getListInfoModels().get(cpt));
			}
		}

		if (numberOverlaySpectrumSearched == 0) {
			JOptionPane.showMessageDialog(view, "No data to overlay", "Alert", JOptionPane.ERROR_MESSAGE);
		} else {
			view.displayDataset();
			if (model.isOnGallery())
				view.regenerateMosaicInfoPanel();
		}
	}

	private void addSeriesinInfoModel(XYSeriesCassis serieCassis,
			final String name, final InfoModel infoModel) {
		CurvePanelView curvePanelView = new CurvePanelView(new CurvePanelModel(serieCassis));
		curvePanelView.addCurveCassisListener(this);
		JPanelCurve jPanelCurve = infoModel.getPanelCurveByName(name);
		jPanelCurve.addCurvePane(curvePanelView);
	}

	private CommentedSpectrum[] computeSpectrumAnalysis(
			CassisSpectrum cassisSpectrum, final double frequencyMin,
			final double frequencyMax) {
		SpectrumAnalysisModel specModel = new SpectrumAnalysisModel();
		XAxisCassis xAxisCassis = XAxisCassis.getXAxisCassis(UNIT.GHZ);
		specModel.setBandUnit(xAxisCassis.getUnit());
		cassisSpectrum.setxAxisOrigin(xAxisCassis);

		specModel.setValMin(xAxisCassis.convertFromMhzFreq(frequencyMin));

		specModel.setValMax(xAxisCassis.convertFromMhzFreq(frequencyMax));
		specModel.setCassisSpectrum(cassisSpectrum);
		CommentedSpectrum[] spectrums;
		try {
			spectrums = specModel.readFile();
		} catch (CassisException ex) {
			LOGGER.info("Can't compute the full spectrum overlay in range: [{}, {}]",
					frequencyMin, frequencyMax, ex);
			spectrums = null;
		}
		return spectrums;
	}

	public FitPanelControl getFitControl() {
		return fitControl;
	}

	/**
	 * Remove only fit, OverlayData, OverlayLine, otherSpecies.
	 */
	@Override
	public void deleteButtonInfoPanelClicked(JPanelCurve panelCurve) {
		LOGGER.debug("Suppression du JSeriesPanel");
		// Gallery / Mosaic.
		if (model.isOnGallery()) {
			view.getInfoPanel().removePanelCurve(panelCurve);
			removeSeriesofPanelCurveFromGallery(panelCurve);
			view.recreatePlotsMosaic();
			if (InfoPanelConstants.MARKERS_TITLE.equals(panelCurve.getName())) {
				resetSelectionAllPlot();
			}
		}
		// "On a plot"
		else {
			view.getInfoPanel().removePanelCurve(panelCurve);
			removeSeriesOfPlot(panelCurve.getListCurvePanelView().get(0).getModel().getCassisModel());
			// For other species we act like on gallery, we remove it for all plots
			if (InfoPanelConstants.OTHER_SPECIES_TITLE.equals(panelCurve.getName())) {
				removeSeriesofPanelCurveFromGallery(panelCurve);
			} else if (InfoPanelConstants.MARKERS_TITLE.equals(panelCurve.getName())) {
				resetSelectionCurrentPlot();
			} else {
				removeSeriesofPanelCurve(panelCurve.getName(), model.getCurrentNumTriplePlot());
			}
			view.refreshCurrentTriplePlot();
		}

		view.getInfoPanel().revalidate();
	}

	private void removeSeriesOfPlot(XYSeriesCassis serie) {
		TypeCurve typeCurve = serie.getTypeCurve();
		if (TypeCurve.OTHER_SPECIES_SIGNAL.equals(typeCurve)) {
			model.getBottomLineSignalDataset().removeAllSeries();
		} else if (TypeCurve.OTHER_SPECIES_IMAGE.equals(typeCurve)) {
			model.getBottomLineImageDataset().removeAllSeries();
		} else if (TypeCurve.OVERLAY_DATA.equals(typeCurve) || TypeCurve.OVERLAY_LINELIST.equals(typeCurve)) {
			model.removeOverlayDataset(serie);
		} else if (typeCurve.isFit()) {
			model.removeFitAndResiudalCurve();
		} else if (TypeCurve.RESULT.equals(typeCurve)) {
			model.removeResultFromCurrentPlot();
		}
	}

	private void removeSeriesofPanelCurveFromGallery(JPanelCurve panelCurve) {
		String nameJPanelCurve = panelCurve.getName();
		for (int i = 0; i < model.getListCassisPlots().size(); i++) {
			removeSeriesofPanelCurve(nameJPanelCurve, i);
		}
	}

	private void removeSeriesofPanelCurve(String nameJPanelCurve, int i) {
		if (model.getListCassisPlots().isEmpty() || model.getListInfoModels().isEmpty())
			return;

		CassisPlot cassisPlot = model.getListCassisPlots().get(i);
		InfoModel infoModel = model.getListInfoModels().get(i);
		infoModel.removePanelCurveByName(nameJPanelCurve);
		if (InfoPanelConstants.OTHER_SPECIES_TITLE.equals(nameJPanelCurve)) {
			if (cassisPlot.getOtherSpeciesSeriesImage() != null &&
					cassisPlot.getOtherSpeciesSeriesImage().getListOfLines() != null) {
				cassisPlot.getOtherSpeciesSeriesImage().clear();
				cassisPlot.getOtherSpeciesSeriesImage().getListOfLines().clear();
			}
			if (cassisPlot.getOtherSpeciesSeriesSignal() != null &&
					cassisPlot.getOtherSpeciesSeriesSignal().getListOfLines() != null) {
				cassisPlot.getOtherSpeciesSeriesSignal().clear();
				cassisPlot.getOtherSpeciesSeriesSignal().getListOfLines().clear();
			}
			view.getOtherSpeciesPanel().setOtherDisplayButtonEnabled(true);
		}
		else if (InfoPanelConstants.FIT_CURVES_TITLE.equals(nameJPanelCurve)) {
			cassisPlot.getFitDataset().removeAllSeries();
		}
		else if (InfoPanelConstants.DATA_FILE_OVERLAYS_TITLE.equals(nameJPanelCurve)) {
			cassisPlot.getOverlaySpectrumSeries().removeAllSeries();
		}
		else if (InfoPanelConstants.LINELIST_OVERLAYS_TITLE.equals(nameJPanelCurve)) {
			cassisPlot.getOverlayLineSpectrumDataset().removeAllSeries();
		}
		else if (InfoPanelConstants.TOOLS_RESULTS_TITLE.equals(nameJPanelCurve)) {
			cassisPlot.getResultSeries().removeAllSeries();
		}
	}

	@Override
	public void setCassisSpectrum(CassisSpectrum cassisSpectrum) {
		cassisSpectrumEvent(new EventObject(cassisSpectrum));
	}

	@Override
	public void setNameData(String titre) {
	}

	@Override
	public void dataChanged(ModelChangedEvent e) {
		if (StackMosaicModel.LINE_SORTING_EVENT.equals(e.getSource())) {
			onLineSorting(e);
		} else if (AbstractMozaicPlotModel.NEW_RESULT_EVENT.equals(e.getSource())) {
			onNewResult(e);
		} else if (AbstractMozaicPlotModel.NEW_RESULT_NO_REFRESH_EVENT.equals(e.getSource())) {
			onNewResultNoRefresh(e);
		} else if (AbstractMozaicPlotModel.REFRESH_EVENT.equals(e.getSource())) {
			onRefresh();
		} else if (AbstractMozaicPlotModel.CHANGE_RENDERING_EVENT.equals(e.getSource())) {
			onUpdateRendering();
		} else if (e.getSource().equals(FitActionsInterface.FIT_PERFORMED_EVENT)) {
			onFitComputed((AdvancedFitResult) e.getValue());
		} else if (e.getSource().equals(FitActionsInterface.FIT_ALL_PERFORMED_EVENT)) {
			onFitAllComputed(e);
		} else if (e.getSource().equals(FitActionsInterface.FIT_GLOBAL_PERFORMED_EVENT)) {
			onFitGlobalPerformed(e);
		} else if (e.getSource().equals(FitActionsInterface.FIT_SUBSTRACTED_EVENT)) {
			onFitSubstracted();
		} else if (e.getSource().equals(FitActionsInterface.LOG_FILE_SELECTED_EVENT)) {
			onInitLogParameters();
		} else if (e.getSource().equals(FitActionsInterface.FIT_SAVED_EVENT)) {
			onSaveCurrentFit();
		} else if (e.getSource().equals(FitActionsInterface.FIT_FAILED_EVENT)) {
			onFitError(e);
		} else if (e.getSource().equals(FitActionsInterface.OVERLAY_FIT_EVENT)) {
			onOverlayFit(e);
		} else if (e.getSource().equals(FitActionsInterface.OVERLAY_RESIDUAL_EVENT)) {
			onOverlayResidual(e);
		} else if (e.getSource().equals(FitActionsInterface.ORIGINAL_RESTORED_EVENT)) {
			onOriginalRestored();
		} else if (e.getSource().equals(FitActionsInterface.COMMAND_ADDED_EVENT)) {
			onCommandAdded();
		} if (e.getSource().equals(StackMosaicModel.CURRENT_SPECTRUM_EVENT)) {
			onCurrentSpectrumChange();
		} else if (e.getSource().equals(ModelFitManager.FIT_MODEL_ADDED_EVENT)) {
			onFitModelAdded(e);
		} else if (e.getSource().equals(AbstractMozaicPlotModel.ON_GALLERY_EVENT)) {
			onGallery();
		}
	}

	private void onLineSorting(ModelChangedEvent e) {
		changeLineSorting((SORTING_PLOT) e.getValue());
	}

	private void onNewResult(ModelChangedEvent e) {
		int currentSpectrum = (Integer) e.getOrigin();
		XYSpectrumSeries serie = (XYSpectrumSeries) e.getValue();
		addSeriesinInfoModel(serie, InfoPanelConstants.TOOLS_RESULTS_TITLE,
				model.getListInfoModels().get(currentSpectrum));
		serie.rebuild();
		view.displayDataset();
	}

	private void onNewResultNoRefresh(ModelChangedEvent e) {
		int currentSpectrum = (Integer) e.getOrigin();
		addSeriesinInfoModel((XYSpectrumSeries) e.getValue(),
				InfoPanelConstants.TOOLS_RESULTS_TITLE,
				model.getListInfoModels().get(currentSpectrum));
	}

	private void onRefresh() {
		view.regenerateMosaicInfoPanel();
	}

	private void onUpdateRendering() {
		view.updateRendering(true);
	}

	private void onFitComputed(FitResultInterface result) {
		if (result == null) {
			removeFitSeries();
			return;
		}
		view.getCreationPanel().getActions().getSubstractFit().setEnabled(true);
		view.getCreationPanel().getActions().getOverlayFits().setEnabled(true);
		view.getCreationPanel().getActions().getOverlayResidual().setEnabled(true);
		view.getCreationPanel().getActions().getOverlayFits().setSelected(true);
		view.getCreationPanel().getActions().getOverlayResidual().setSelected(true);
		displayFitAndResidual(result.getFitSeries(), result.getResidualSeries(), result.getCompoSeries());
	}

	private void onFitAllComputed(ModelChangedEvent e) {
		@SuppressWarnings("unchecked")
		List<FitResultInterface> results = (List<FitResultInterface>) e.getValue();
		displayAll(results);
		if (managerContainsMolCategory()) {
			saveAllFits();
		}
	}

	private boolean managerContainsMolCategory() {
		for (Category c : model.getFitModelManager().getCurrentModel().getParametersModel().getManager()
				.getCategories()) {
			if (c instanceof MolCategory) {
				return true;
			}
		}
		return false;
	}

	private void onFitGlobalPerformed(ModelChangedEvent e) {
		@SuppressWarnings("unchecked")
		List<FitResultInterface> results = (List<FitResultInterface>) e.getValue();
		displayAll(results);
		onGallery();
	}

	private void displayAll(List<FitResultInterface> results) {
		int old = getNumCurrentSpectrum();
		for (int i : model.getGalleryNavigationModel().getFilter()) {
			view.getStackMosaicPanel().move(i);
			model.getStackMosaicModel().setCurrentSpectrum(i);
			view.refreshCurrentTriplePlot();
			onFitComputed(results.get(i));
		}
		view.getStackMosaicPanel().move(old);
		view.refreshCurrentTriplePlot();
	}

	private void onFitSubstracted() {
		XYSpectrumSeries source = model.getCurrentDataCurve();
		AdvancedFitResult lastResult = model.getFitModelManager().getLastResult();
		FitSubstractCommand cmd = new FitSubstractCommand(model.getCurrentFitModel().getParametersModel(),
				this, source, lastResult.getResidualSeries());
		view.getCreationPanel().getActions().getOverlayFits().setSelected(false);
		view.getCreationPanel().getActions().getOverlayResidual().setSelected(false);
		model.getFitModelManager().addCommand(cmd);
		removeFitSeries();
	}

	private void onInitLogParameters() {
		model.getFitModelManager().initLogParameters(view, model);
	}

	private void onSaveCurrentFit() {
		if (!model.getFitModelManager().isSavingInitialized()) {
			model.getFitModelManager().initLogParameters(view, model);
		}
		try {
			model.getFitModelManager().saveCurrentFitModel(model.getTelescope());
		} catch (IOException | IllegalArgumentException e) {
			displayError("Error", "<html>Impossible to save the fit: <br>" + e.getMessage() + "</html>");
		}
	}

	private void onFitError(ModelChangedEvent e) {
		FitException exc = (FitException) e.getValue();
		String text = "<html>Error during Fit.<br> " + exc.getMessageError() + "<br>" + exc.getProbableSolution() + "</html>";
		displayError(exc.getNameError(), text);
	}

	private void onOverlayFit(ModelChangedEvent e) {
		setSeriesVisible((boolean) e.getValue(), TypeCurve.FIT);
		setSeriesVisible((boolean) e.getValue(), TypeCurve.FIT_COMPO);
		view.getCreationPanel().getActions().getOverlayFits().setSelected((boolean) e.getValue());
	}

	private void onOverlayResidual(ModelChangedEvent e) {
		setSeriesVisible((boolean) e.getValue(), TypeCurve.FIT_RESIDUAL);
		view.getCreationPanel().getActions().getOverlayResidual().setSelected((boolean) e.getValue());
	}

	private void onOriginalRestored() {
		model.getCurrentFitModel().getParametersModel().restoreOriginal();
		removeFitSeries();
	}

	private void onCommandAdded() {
		boolean undoEmpty = model.getCurrentFitModel().getParametersModel().isUndoEmpty();
		view.getCreationPanel().getActions().getUndo().setEnabled(!undoEmpty);
		boolean redoEmpty = model.getCurrentFitModel().getParametersModel().isRedoEmpty();
		view.getCreationPanel().getActions().getRedo().setEnabled(!redoEmpty);
	}

	private void onCurrentSpectrumChange() {
		ModelFitManager manager = this.model.getFitModelManager();
		model.getCurrentFitModel().removeModelListener(this);
		manager.setCurrentIndex(getNumCurrentSpectrum());
		if (getNumCurrentSpectrum() < manager.getNbModels()) {
			AdvancedFitModel fitModel = model.getCurrentFitModel();
			AdvancedFitFrame.getFrame().displayModel(fitModel);
			view.getCreationPanel().refreshRms();
		}
	}

	private void onFitModelAdded(ModelChangedEvent e) {
		AdvancedFitModel model = (AdvancedFitModel) e.getValue();
		if (Software.getUserConfiguration().isFitAutoEstimate()) {
			estimateLines(model);
		}
	}

	private void onGallery() {
		this.model.getFitModelManager().setOnGallery(isOnGallery());
		this.view.getCreationPanel().setGalleryMode(isOnGallery());
		AdvancedFitFrame.getFrame().openFrame(isFittable());
		AdvancedFitFrame.getFrame().displayModel(this.model.getCurrentFitModel());
	}

	private void saveAllFits() {
		if (!model.getFitModelManager().isSavingInitialized()) {
			onInitLogParameters();
		}
		int old = getNumCurrentSpectrum();
		List<List<LineDescription>> lines = new ArrayList<>();
		for (int i : model.getGalleryNavigationModel().getFilter()) {
			view.getStackMosaicPanel().move(i);
			view.refreshCurrentTriplePlot();
			lines.add(model.getCurrentCassisPlot().getAllLines());
		}
		try {
			model.getFitModelManager().saveAllFitModels(lines, model.getTelescope(), model.getGalleryNavigationModel().getFilter());
		} catch (IOException | IllegalArgumentException e) {
			displayError("Error", "<html>Impossible to save the fit: <br>" + e.getMessage() + "</html>");
		}
		view.getStackMosaicPanel().move(old);
		view.refreshCurrentTriplePlot();
	}

	@Override
	public void estimateLinesForCurrentModel(List<LineDescription> lines) {
		FitType defComp = Software.getUserConfiguration().getFitDefaultType();
		model.getCurrentFitModel().estimateListOfLines(lines, defComp);
	}

	@Override
	public boolean isFittable() {
		return view.getTabManager().getSelectedIndex() == 4;
	}

	@Override
	public void removeEstimationRange(double x) {
		model.getCurrentFitModel().getParametersModel().removeRanges(x);
	}

	@Override
	public FitSourceInterface getFitSource() {
		return model.getCurrentCassisPlot();
	}

	private void estimateLines(AdvancedFitModel model) {
		FitSourceInterface current = model.getDataSource();
		if (current.getCurrentSeries() != null && !current.getCurrentSeries().getListOfLines().isEmpty()) {
			Map<Integer, List<LineDescription>> separatedSpecies = new HashMap<>();
			for (LineDescription line : current.getCurrentSeries().getListOfLines()) {
				if (!separatedSpecies.containsKey(line.getMolTag())) {
					separatedSpecies.put(line.getMolTag(), new ArrayList<>());
				}
				separatedSpecies.get(line.getMolTag()).add(line);
			}
			FitType defComp = Software.getUserConfiguration().getFitDefaultType();
			for (List<LineDescription> lines : separatedSpecies.values()) {
				model.estimateListOfLines(lines, defComp);
			}
		}
	}

	private void displayError(String title, String text) {
		JOptionPane.showMessageDialog(view, text, title, JOptionPane.ERROR_MESSAGE);
	}

	public void seriesCassisChanged(XYSeriesCassis series, CurveParameters curveParameters) {
		try {
			if (TypeCurve.PLOT_NUMBER.equals(series.getTypeCurve())) {
				model.setPlotNumberVisible(series.getConfigCurve().isVisible());
				model.setPlotNumberColor(series.getConfigCurve().getColor());
				view.recreatePlotsMosaic();
				return;
			}
			if (TypeCurve.MARKERS.equals(series.getTypeCurve())) {
				displaySelection(series.getConfigCurve().isVisible());
				if (view.isOnGallery()) {
					if (series.getConfigCurve().isVisible()) {
						view.recreatePlotsMosaic();
					} else {
						view.getGallerySortPane().hideMarkers();
					}
				}
			}

			view.getSpectrumPlot().updateRender(series, curveParameters);

			updateCassisPlotConfig(series);

			if (TypeCurve.OTHER_SPECIES_SIGNAL.equals(series.getTypeCurve())) {
				model.getSpeciesModel().setColorSpeciesSignal(series.getConfigCurve().getColor());
				model.getSpeciesModel().setSpeciesSignalVisible(series.getConfigCurve().isVisible());
				view.getOtherSpeciesPanel().getButtonColor().setBackground(series.getConfigCurve().getColor());
			}
			else if (TypeCurve.OTHER_SPECIES_IMAGE.equals(series.getTypeCurve())) {
				model.getSpeciesModel().setColorSpeciesImage(series.getConfigCurve().getColor());
				model.getSpeciesModel().setSpeciesImageVisible(series.getConfigCurve().isVisible());
				view.getOtherSpeciesPanel().getButtonColorImage().setBackground(series.getConfigCurve().getColor());
			}

			for (SpectrumPlot spectrumPlot : view.getGallerySortPane().getListSpectrumPlots()) {
				XYPlotCassisUtil.configureRenderer(spectrumPlot);
			}

		} catch (UnknownKeyException e) {
			LOGGER.error("The series {} you have modified is not in the display dataset",
					series.getKey(), e);
		}
	}

	private void updateCassisPlotConfig(XYSeriesCassis series) {
		final ArrayList<CassisPlot> listCassisPlots = model
				.getListCassisPlots();
		for (CassisPlot cassisPlot : listCassisPlots) {
			cassisPlot.updateConfigSeries(series);
		}
	}

	@Override
	public void curveCassisChange(CurvePanelModel curvePanelModel) {
		view.getLineConfigCurve().setElement(curvePanelModel.getCassisModel().getKey().toString(),
				curvePanelModel.getCassisModel().getConfigCurve());
		seriesCassisChanged(curvePanelModel.getCassisModel(), CurveParameters.ALL);
	}

	@Override
	public void curveCassisMosaicChange() {
		view.getGallerySortPane().refreshPlotPaneFromConfigCurve();
	}

	public void changeLeftLabel() {
		view.getSpectrumPlot().setLeftTitle(model.getYAxisCassis().toString());
		for (SpectrumPlot sp : view.getGallerySortPane().getListSpectrumPlots()) {
			sp.setLeftTitle(model.getYAxisCassis().toString());
			sp.revalidate();
			sp.repaint();
		}
	}

	public void comboTopAxisChanged() {
		if (view.getComboTopAxis().getSelectedItem() == null) {
			return;
		}
		XAxisCassis xAxisCassis = (XAxisCassis) view.getComboTopAxis().getSelectedItem();
		final CassisPlot currentCassisPlot = model.getCurrentCassisPlot();
		final SpectrumPlot spectrumPlot = view.getSpectrumPlot();

		changeTopAxis(xAxisCassis, currentCassisPlot, spectrumPlot);

		List<SpectrumPlot> listSpectrumPlot = new ArrayList<>();
		for (SpectrumPlot plotMosaic : view.getGallerySortPane().getListSpectrumPlots()) {
			listSpectrumPlot.add(plotMosaic);
		}

		GalleryNavigationModel galNavModel = model.getGalleryNavigationModel();
		for (SpectrumPlot plotPaneMosaic : listSpectrumPlot) {
			CassisPlot cassisPlotMosaic =
					model.getListCassisPlots().get(galNavModel.getFilter().get(plotPaneMosaic.getRowId() *
							galNavModel.getCols() + plotPaneMosaic.getColId()));
			changeTopAxis(xAxisCassis, cassisPlotMosaic, plotPaneMosaic);
		}

		model.setXAxisCassisTop(xAxisCassis);
		model.getFitModelManager().convertGlobalConfiguration(xAxisCassis);
	}

	private XAxisCassis changeTopAxis(XAxisCassis xAxisCassisTop,
			final CassisPlot currentCassisPlot, final SpectrumPlot spectrumPlot) {
		if (UNIT.KM_SEC_MOINS_1.equals(xAxisCassisTop.getUnit())) {
			xAxisCassisTop = currentCassisPlot.getXAxisVelocity();
		}

		ValueAxis domainAxis = spectrumPlot.getPlot().getDomainAxis();

		double lowerBound = domainAxis.getRange().getLowerBound();
		double upperBound = domainAxis.getRange().getUpperBound();
		double lengthAxis = upperBound-lowerBound;
		double lowerMargin = lengthAxis * domainAxis.getLowerMargin() / (1 + domainAxis.getLowerMargin() + domainAxis.getUpperMargin());
		lowerBound = lowerBound + lowerMargin;

		double upperMargin = lengthAxis * domainAxis.getUpperMargin() / (1 + domainAxis.getLowerMargin() + domainAxis.getUpperMargin());
		upperBound = upperBound - upperMargin;

		XAxisCassis xAxisCassisBottom = model.getXAxisCassis();
		boolean isInverted = xAxisCassisTop.isInverted() != xAxisCassisBottom.isInverted();

		double min = XAxisCassis.convert(lowerBound, xAxisCassisBottom,
				xAxisCassisTop, model.getSpeciesModel().getTypeFrequency());
		double max = XAxisCassis.convert(upperBound, xAxisCassisBottom,
				xAxisCassisTop, model.getSpeciesModel().getTypeFrequency());

		double temp = min;
		min = Math.min(temp, max);
		max = Math.max(temp, max);

		lengthAxis = max - min;

		min = min - domainAxis.getLowerMargin() * lengthAxis;
		max = max + domainAxis.getUpperMargin() * lengthAxis;

		model.setXAxisCassisTop(xAxisCassisTop);

		spectrumPlot.setXAxisCassisTop(xAxisCassisTop);
		spectrumPlot.changeTopAxisRange(min, max, isInverted);

		spectrumPlot.repaint();
		return xAxisCassisTop;
	}

	@Override
	public XYSpectrumSeries getSeriesToFit() {
		return model.getSeriesToFit();
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.util.SpectrumFitPanelInterface#getNumCurrentSpectrum()
	 */
	@Override
	public int getNumCurrentSpectrum() {
		return model.getCurrentNumTriplePlot();
	}

	@Override
	public List<InterValMarkerCassis> getListMarker() {
		return view.getCurrentFitPanel().getMarkerManager().getListMarkerDisjoint();
	}

	@Override
	public void setLogParameters(File selectedFile, boolean append) {
		saveFit.setLogParameters(selectedFile, append);
	}

	@Override
	public void displaySelection(boolean selected) {
		view.getCurrentFitPanel().getMarkerManager().displaySelection(
				view.getSpectrumPlot().getPlot(), selected);
	}

	@Override
	public void setDataSeries(XYSpectrumSeries substractSeries) {
		model.setDataSeries(substractSeries);
		view.displayDataset();
	}

	@Override
	public void resetLastSelection() {
		view.getCurrentFitPanel().getMarkerManager().resetLastSelection(
				view.getSpectrumPlot().getPlot());
		model.getFitModelManager().resetLastStudiedRange();
	}

	@Override
	public void resetAllSelection() {
		view.getCurrentFitPanel().getMarkerManager().resetAllSelection(
				view.getSpectrumPlot().getPlot());
		model.getFitModelManager().resetStudyRanges();
		removeMarkersPanelCurve();
	}

	@Override
	public void displayFitAndResidual(XYSpectrumSeries serie, XYSpectrumSeries residualSerie,
			List<XYSpectrumSeries> serieFitCompo) {
		removeFitSeries();
		serie.getConfigCurve().setColor(this.model.getColorFitCurve());

		List<XYSpectrumSeries> allFitCurves = new ArrayList<>(serieFitCompo.size()+2);
		allFitCurves.add(serie);
		allFitCurves.add(residualSerie);
		allFitCurves.addAll(serieFitCompo);

		JPanelCurve seriesFitPanel = new JPanelCurve(InfoPanelConstants.FIT_CURVES_TITLE);

		for (XYSpectrumSeries xyFitSpectrumSeries : allFitCurves) {
			model.addFitCurve(xyFitSpectrumSeries);
			CurvePanelView curvePanelView = new CurvePanelView(new CurvePanelModel(xyFitSpectrumSeries));
			curvePanelView.addCurveCassisListener(this);
			seriesFitPanel.addCurvePane(curvePanelView);
			view.checkLineConfigCurveFor(curvePanelView);
		}

		view.getCurrentFitPanel().setSelectedFit(true);
		view.getCurrentFitPanel().setSelectedResidual(true);
		XYPlotCassisUtil.configureRenderer(view.getSpectrumPlot());

		if (view.isOnGallery()) {
			view.recreatePlotsMosaic();
			model.getListInfoModels().get(model.getCurrentNumTriplePlot()).addPanelCurve(seriesFitPanel);
			view.regenerateMosaicInfoPanel();
		} else {
			view.getInfoPanel().addJPanelCurve(seriesFitPanel);
		}
	}

	private void removeFitSeries() {
		model.removeFitAndResiudalCurve();
		model.getCurrentCassisPlot().getFitDataset().removeAllSeries();

		JPanelCurve oldPanelCurve = view.getInfoPanel().getJPanelCurveByName(
				InfoPanelConstants.FIT_CURVES_TITLE);
		if (oldPanelCurve != null)
			view.getInfoPanel().removePanelCurve(oldPanelCurve);
	}

	@Override
	public void speciesSignalVisibleChanged(SpeciesEnableEvent event) {
		setSeriesVisible(event.isEnabled(), TypeCurve.OTHER_SPECIES_SIGNAL,
				model.getBottomLineSignalDataset());
		if (view.getLineConfigCurve().isThatCurveExist(InfoPanelConstants.SIGNAL_TITLE)) {
			view.getLineConfigCurve().getConfigCurve(
					InfoPanelConstants.SIGNAL_TITLE).setVisible(event.isEnabled());
		}
		JPanelCurve panelCurveByName = view.getInfoPanel().getModel().getPanelCurveByName(
				InfoPanelConstants.OTHER_SPECIES_TITLE);
		for (CurvePanelView cpv : panelCurveByName.getListCurvePanelView()) {
			if (InfoPanelConstants.SIGNAL_TITLE.equals(cpv.getNameFromModel())
					&& cpv.getButtonColor() != null) {
				cpv.getCheckBox().setSelected(event.isEnabled());
			}
		}
	}

	@Override
	public void speciesImageVisibleChanged(SpeciesEnableEvent event) {
		setSeriesVisible(event.isEnabled(), TypeCurve.OTHER_SPECIES_IMAGE,
				model.getBottomLineImageDataset());
		if (view.getLineConfigCurve().isThatCurveExist(InfoPanelConstants.IMAGE_TITLE)) {
			view.getLineConfigCurve().getConfigCurve(
					InfoPanelConstants.IMAGE_TITLE).setVisible(event.isEnabled());
		}
		JPanelCurve panelCurveByName = view.getInfoPanel().getModel().getPanelCurveByName(
				InfoPanelConstants.OTHER_SPECIES_TITLE);
		for (CurvePanelView cpv : panelCurveByName.getListCurvePanelView()) {
			if (InfoPanelConstants.IMAGE_TITLE.equals(cpv.getNameFromModel())
					&& cpv.getButtonColor() != null) {
				cpv.getCheckBox().setSelected(event.isEnabled());
			}
		}
	}

	@Override
	public void setSeriesVisible(boolean visible, TypeCurve speciesType) {
		XYSeriesCassisCollection series = model.getCenterDataset();
		for (int cpt = 0; cpt < series.getSeriesCount(); cpt++) {
			XYSeriesCassis seriesCassis = series.getSeries(cpt);
			if (TypeCurve.sameType(seriesCassis.getTypeCurve(), speciesType)
					&& seriesCassis.getConfigCurve().isVisible() != visible) {
				seriesCassis.getConfigCurve().setVisible(visible);
				seriesCassisChanged(seriesCassis, CurveParameters.VISIBILITY);
				if (TypeCurve.FIT_RESIDUAL.equals(speciesType))
					view.getCurrentFitPanel().changeResidualButton(visible);
			}

		}

		if (view.getInfoPanel().getModel().isJPanelCurveExist(InfoPanelConstants.FIT_CURVES_TITLE)) {
			JPanelCurve panelCurveByName = view.getInfoPanel().getModel().getPanelCurveByName(
					InfoPanelConstants.FIT_CURVES_TITLE);
			for (CurvePanelView cpv : panelCurveByName.getListCurvePanelView()) {
				XYSeriesCassis cassisModel = cpv.getModel().getCassisModel();
				if (TypeCurve.sameType(cassisModel.getTypeCurve(), speciesType)) {
					cpv.getCheckBox().setSelected(visible);
					cpv.getControl().handleCheckBoxAction();
				}
			}
		}
	}

	private void setSeriesVisible(boolean visible, TypeCurve speciesType,
			XYSeriesCollection series) {
		for (int cpt = 0; cpt < series.getSeriesCount(); cpt++) {
			if (series.getSeries(cpt) instanceof XYSeriesCassis) {
				XYSeriesCassis fullSeries = (XYSeriesCassis) series.getSeries(cpt);
				if (TypeCurve.sameType(fullSeries.getTypeCurve(), speciesType)) {
					fullSeries.getConfigCurve().setVisible(visible);
					seriesCassisChanged(fullSeries, CurveParameters.VISIBILITY);
				}
			}
		}
	}

	@Override
	public XAxisCassis getXAxisCassis() {
		return model.getXAxisCassis();
	}

	@Override
	public YAxisCassis getYAxisCassis() {
		return model.getYAxisCassis();
	}

	@Override
	public double computeIntegral(List<InterValMarkerCassis> markerList) {
		return FitOperation.computeIntegral(model.getSeriesToFit(), markerList);
	}

	@Override
	public void speciesDisplayClicked(EventObject event) {
		if (model.getLineResult() == null)
			return;

		try {
			removeOtherSpecies();
			if (computeOtherSpecies()) {
				displayOtherSpecies();
			} else if (!Software.getUserConfiguration().isAutoOtherSpeciesDisplayed() ||
					   !speciesControl.getModel().getCassisTableMoleculeModel().getSelectedMolecules().isEmpty()){
				JOptionPane.showMessageDialog(view,
						"No transition found. Check your thresholds and/or template if necessary.",
						"Other Species", JOptionPane.WARNING_MESSAGE);
				view.displayDataset();
			}
		} catch (CassisException e) {
			LOGGER.error("Error while computing other species", e);
			JOptionPane.showMessageDialog(view, e.getMessage(), "Alert", JOptionPane.WARNING_MESSAGE);
		}
	}

	private void removeOtherSpecies() {
		for (int cpt = 0; cpt < model.getListCassisPlots().size(); cpt++) {
			removeSeriesofPanelCurve(InfoPanelConstants.OTHER_SPECIES_TITLE, cpt);
		}
	}

	@Override
	public void speciesColorSignalChanged(SpeciesColorChangedEvent event) {
		final Color newColor = event.getNewColor();
		XYLineSeriesCassis otherSpeciesSeriesSignal = model.getListCassisPlots().get(0).getOtherSpeciesSeriesSignal();
		if (otherSpeciesSeriesSignal == null) {
			return;
		}
		otherSpeciesSeriesSignal.getConfigCurve().setColor(newColor);
		view.getLineConfigCurve().setElement((String)otherSpeciesSeriesSignal.getKey(),
				otherSpeciesSeriesSignal.getConfigCurve());
		JPanelCurve panelCurveByName = view.getInfoPanel().getModel().getPanelCurveByName(
				InfoPanelConstants.OTHER_SPECIES_TITLE);
		for (CurvePanelView cpv : panelCurveByName.getListCurvePanelView()) {
			if (InfoPanelConstants.SIGNAL_TITLE.equals(cpv.getNameFromModel())
					&& cpv.getButtonColor() != null) {
				cpv.getButtonColor().setBackground(event.getNewColor());
			}
		}
		seriesCassisChanged(otherSpeciesSeriesSignal, CurveParameters.COLOR);
	}

	@Override
	public void speciesColorImageChanged(SpeciesColorChangedEvent event) {
		final Color newColor = event.getNewColor();
		XYLineSeriesCassis otherSpeciesSeriesImage = model.getListCassisPlots().get(0).getOtherSpeciesSeriesImage();
		if (otherSpeciesSeriesImage == null) {
			return;
		}
		otherSpeciesSeriesImage.getConfigCurve().setColor(newColor);
		view.getLineConfigCurve().setElement((String)otherSpeciesSeriesImage.getKey(),
				otherSpeciesSeriesImage.getConfigCurve());
		JPanelCurve panelCurveByName = view.getInfoPanel().getModel().getPanelCurveByName(
				InfoPanelConstants.OTHER_SPECIES_TITLE);
		for (CurvePanelView cpv : panelCurveByName.getListCurvePanelView()) {
			if (InfoPanelConstants.IMAGE_TITLE.equals(cpv.getNameFromModel())
					&& cpv.getButtonColor() != null) {
				cpv.getButtonColor().setBackground(event.getNewColor());
			}
		}
		seriesCassisChanged(otherSpeciesSeriesImage, CurveParameters.COLOR);
	}

	private void displayOtherSpecies() {
		SpeciesModel speciesModel = model.getSpeciesModel();
		for (int cpt = 0; cpt < model.getListCassisPlots().size(); cpt++) {
			CassisPlot cassisPlot = model.getListCassisPlots().get(cpt);
			JPanelCurve panelCurveSpecies = new JPanelCurve(
					InfoPanelConstants.OTHER_SPECIES_TITLE, true);

			XYLineSeriesCassis otherSpeciesSeriesSignal = cassisPlot.getOtherSpeciesSeriesSignal();
			otherSpeciesSeriesSignal.setConfigCurve(speciesModel.getConfigCurveSignal().copy());
			CurvePanelModel curveModelSpeciesSignal = new CurvePanelModel(otherSpeciesSeriesSignal);
			CurvePanelView curvePanelSpeciesSignal = new CurvePanelView(curveModelSpeciesSignal);
			curvePanelSpeciesSignal.addCurveCassisListener(this);
			panelCurveSpecies.addCurvePane(curvePanelSpeciesSignal);
			this.curveCassisChange(curveModelSpeciesSignal);

			if (speciesModel.isHaveImage() && cassisPlot.getOtherSpeciesSeriesImage() != null) {
				XYLineSeriesCassis otherSpeciesSeriesImage = cassisPlot.getOtherSpeciesSeriesImage();
				otherSpeciesSeriesImage.setConfigCurve(speciesModel.getConfigCurveImage().copy());
				CurvePanelModel curveModelSpeciesImage = new CurvePanelModel(otherSpeciesSeriesImage);
				CurvePanelView curvePanelSpeciesImage = new CurvePanelView(curveModelSpeciesImage);
				curvePanelSpeciesImage.addCurveCassisListener(this);
				panelCurveSpecies.addCurvePane(curvePanelSpeciesImage);
				this.curveCassisChange(curveModelSpeciesImage);
			}

			model.getListInfoModels().get(cpt).addPanelCurve(panelCurveSpecies);

			GallerySortPane gsp = view.getGallerySortPane();
			gsp.removeOtherSpecies(cpt);
			if (view.getOtherSpeciesPanel().getModel().isSpeciesImageVisible()
					&& cassisPlot.getOtherSpeciesSeriesImage() != null) {
				gsp.addOtherSpeciesImage(cpt, cassisPlot.getOtherSpeciesSeriesImage());
			}
			if (view.getOtherSpeciesPanel().getModel().isSpeciesSignalVisible()
					&& cassisPlot.getOtherSpeciesSeriesSignal() != null) {
				gsp.addOtherSpeciesSignal(cpt, cassisPlot.getOtherSpeciesSeriesSignal());
			}
		}
		model.displayOtherSpecies();

		CassisPlot cassisPlot = model.getListCassisPlots().get(model.getCurrentNumTriplePlot());
		if (cassisPlot.getOtherSpeciesSeriesSignal() != null) {
			this.curveCassisChange(new CurvePanelModel(cassisPlot.getOtherSpeciesSeriesSignal()));
		}
		if (speciesModel.isHaveImage() && cassisPlot.getOtherSpeciesSeriesImage() != null) {
			this.curveCassisChange(new CurvePanelModel(cassisPlot.getOtherSpeciesSeriesImage()));
		}

		view.displayDataset();
	}

	//TODO Refactoring : duplicate code with AbstractMozaicPlotControl to put in AbstractPlotModel
	@Override
	public void doGaussianDefaultParametersAction(double startValue, double endValue) {
		super.doGaussianDefaultParametersAction(startValue, endValue, model, fitControl);

	}

	public void fitAllClicked(FitPanel fitPanel) {
		List<InterValMarkerCassis> listMarker = getListMarker();

		FitStyleEnum fitStyle = fitPanel.getModel().getFitStyle();
		int nbIter = fitPanel.getModel().getNbIterations();
		int nbOversampl = fitPanel.getModel().getOversamplingFit();
		double tolerance =  fitPanel.getModel().getTolerance();
		int numCurrentSpectrum = model.getCurrentNumTriplePlot();

		LinkedList<FittingItem> list = fitPanel.getActiveLinkedListFittingItem();

		List<double[]> parametersValuesArray = new ArrayList<>();
		List<boolean[]> parametersBlockedArray = new ArrayList<>();
		List<Boolean> parametersActiveArray = new ArrayList<>();

		for (FittingItem fittingItem : list) {
			parametersValuesArray.add(fittingItem.getParametersValues());
			parametersBlockedArray.add(fittingItem.getParametersBlocked());
			parametersActiveArray.add(fittingItem.isActive());
		}

		if (!spectrumFitPanelListener.selectFileClicked(null))
			return;

		boolean isSave = saveFit.getLogFile() != null;

		int numSpectrum;
		do {
			FitPanel current = view.getCurrentFitPanel();
			fitCurrentPlot(listMarker, fitStyle, nbIter, nbOversampl, tolerance, list,
					parametersValuesArray, parametersBlockedArray,
					parametersActiveArray, current);

			spectrumFitPanelListener.fitCurrentClicked(null);
			if (isSave) {
				spectrumFitPanelListener.saveFitClicked(null);
			}
			view.getStackMosaicPanel().moveRight();
			numSpectrum = model.getCurrentNumTriplePlot();
			view.displayDataset(numSpectrum);
		} while (numSpectrum != numCurrentSpectrum);

		if (view.isOnGallery()) {
			view.recreatePlotsMosaic();
			view.regenerateMosaicInfoPanel();
		}
	}

	private void fitCurrentPlot(List<InterValMarkerCassis> listMarker,
			FitStyleEnum fitStyle, int nbIter, int nbOversampl, double tolerance,
			LinkedList<FittingItem> list,
			List<double[]> parametersValuesArray,
			List<boolean[]> parametersBlockedArray,
			List<Boolean> parametersActiveArray, FitPanel current) {
		current.getModel().setFitStyle(fitStyle);
		current.getModel().setNbIterations(nbIter);
		current.getModel().setOversamplingFit(nbOversampl);
		current.getModel().setTolerance(tolerance);
		current.getMarkerManager().resetAllSelection(view.getSpectrumPlot().getPlot());
		if (!listMarker.isEmpty()) {
			for (InterValMarkerCassis marker : listMarker) {
				current.getMarkerManager().addMarker(
						view.getSpectrumPlot().getPlot(), marker.getStartValue(),
						marker.getEndValue());
			}
		}

		current.clearFittings();

		for (int cpt = 0; cpt < list.size(); cpt++) {
			FittingItem addFittings = current.addFittings(list.get(cpt));
			addFittings.setParametersValues(parametersValuesArray.get(cpt));
			addFittings.setParametersBlocked(parametersBlockedArray.get(cpt));
			addFittings.setButtonOff(!parametersActiveArray.get(cpt));
			addFittings.revalidate();
		}
	}

	public FitPanelListener getSpectrumFitPanelListener() {
		return spectrumFitPanelListener;
	}

	/**
	 * @param spectrumFitPanelListener
	 *            the spectrumFitPanelListener to set
	 */
	public void setSpectrumFitPanelListener(FitPanelListener spectrumFitPanelListener) {
		this.spectrumFitPanelListener = spectrumFitPanelListener;
	}

	public void replaceFitPanel(final FitPanel fitPanel) {
		int indiceFitTab = view.getIndiceTabByName("Fit");
		int indice = view.getTabManager().getSelectedIndex();

		replacingFitPanel = true;
		if (indiceFitTab != -1) {
			view.getTabManager().remove(indiceFitTab);
			view.getTabManager().insertTab("Fit", null, fitPanel, null, indiceFitTab);
		} else {
			view.getTabManager().addTab("Fit", fitPanel);
		}
		replacingFitPanel = false;

		view.changeFitParameters(fitPanel);
		view.setLastFitPanel(fitPanel);
		view.getTabManager().setSelectedIndex(indice);
		view.getSpectrumPlot().setMarkerManager(fitPanel.getMarkerManager());
		((SpectrumFitPanelListener)spectrumFitPanelListener).setFitPanel(fitPanel);
		getFitControl().setFitPanel(fitPanel);
		fitPanel.getMarkerManager().displaySelection(
				view.getSpectrumPlot().getPlot(),true);
	}

	public boolean isReplacingFitPanel() {
		return replacingFitPanel;
	}

	public void removeAllButtonClicked() {
		// We can only delete some JPanelCurve, get the list of those who have a delete button
		List<JPanelCurve> listJPanelCurveDeletable = new ArrayList<>();
		for (JPanelCurve jPanelCurve : view.getInfoPanel().getModel().getListOfJPanelCurves()) {
			if (jPanelCurve.getDeleteButton() != null)
				listJPanelCurveDeletable.add(jPanelCurve);
		}

		if (!listJPanelCurveDeletable.isEmpty()) {
			int ret = JOptionPane.showConfirmDialog(view, "Do you want to remove all the plots who can be?",
					"Plot Info", JOptionPane.YES_NO_OPTION);
			if (ret == JOptionPane.YES_OPTION) {
				for (JPanelCurve panelCurve : listJPanelCurveDeletable) {
					deleteButtonInfoPanelClicked(panelCurve);
				}
			}
		}
	}

	public void currentSpectrumKeyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			int numSpectrum = view.getStackMosaicPanel().getCurrentItem() - 1;
			if (model.getStackMosaicModel().isMovePossible(numSpectrum)) {
				model.getStackMosaicModel().setCurrentSpectrum(numSpectrum);
				view.replaceToolsPanel();
				view.displayDataset();
				view.getSpectrumPlot().refreshPopups(model.getCurrentNumTriplePlot());
			} else { // Error, reset the field pos to the current one
				view.getStackMosaicPanel().setCurrentItem(
				model.getStackMosaicModel().getCurrentSpectrumDisplay());
			}
		}
	}

	public void moveRightClicked() {
		view.getStackMosaicPanel().moveRight();
		view.refreshCurrentTriplePlot();
	}

	public void moveLeftClicked() {
		view.getStackMosaicPanel().moveLeft();
		view.refreshCurrentTriplePlot();
	}

	public void stepMoveRightCliked() {
		Double stepValue = Double.parseDouble(view.getStackMosaicPanel().getStepValueField().getText().trim()) / 1E3;
		int spectrumNearest = model.searchNearestSpectrum(model.getCurrentNumTriplePlot(), stepValue, 1);
		view.getStackMosaicPanel().move(spectrumNearest);
	}

	public void stepMoveLeftCliked() {
		Double stepValue = Double.parseDouble(view.getStackMosaicPanel().getStepValueField().getText().trim()) / 1E3;
		int spectrumNearest = model.searchNearestSpectrum(model.getCurrentNumTriplePlot(), stepValue, 0);
		view.getStackMosaicPanel().move(spectrumNearest);
	}

	public void galleryPlotPanelClicked(MouseEvent me, final int rowId,
			final int colId) {
		if (me.getButton() == MouseEvent.BUTTON1 && me.getClickCount() == 2) { // double click
			view.switchTo(rowId, colId);
		}
	}

	public void updateNumberOfPlotInFitListener() {
		if (spectrumFitPanelListener instanceof SpectrumFitPanelListener) {
			SpectrumFitPanelListener sfpl = (SpectrumFitPanelListener) spectrumFitPanelListener;
			sfpl.setNumberOfPlot(model.getNbCassisPlots());
		}
	}

	@Override
	public boolean isOnGallery() {
		return model.isOnGallery();
	}

	public void fitSubtractAll() {
		int numCurrentSpectrum = model.getCurrentNumTriplePlot();
		int numSpectrum;
		do {
			spectrumFitPanelListener.subtractFit();
			view.getStackMosaicPanel().moveRight();
			numSpectrum = model.getCurrentNumTriplePlot();
			view.displayDataset(numSpectrum);
		} while (numSpectrum != numCurrentSpectrum);
	}

	public void fitDivideAll() {
		int numCurrentSpectrum = model.getCurrentNumTriplePlot();
		int numSpectrum;
		do {
			spectrumFitPanelListener.divideByFit();
			view.getStackMosaicPanel().moveRight();
			numSpectrum = model.getCurrentNumTriplePlot();
			view.displayDataset(numSpectrum);
		} while (numSpectrum != numCurrentSpectrum);
	}

	@Override
	public void addIntervalMarker(InterValMarkerCassis marker) {
		XYPlot plot = view.getSpectrumPlot().getPlot();
		view.getCurrentFitPanel().getMarkerManager().addMarker(plot,
				marker.getStartValue(), marker.getEndValue());
		forwardMarkerToFitModule(marker);
		addMarkersPanelCurve();
	}

	/**
	 * Forwards the given marker to the fit module of the current model
	 *
	 * @param marker Marker to forward
	 */
	private void forwardMarkerToFitModule(InterValMarkerCassis marker) {
		FitParametersModel paramModel = model.getCurrentFitModel().getParametersModel();
		double start = marker.getStartValue();
		double end = marker.getEndValue();
		FitEstimator estimator = new FitEstimator(start, end, paramModel.getSourceCurve());
		FitAbstractComponent estim = paramModel.getManager().getEstimableComponent();
		if (estim == null || (estim != null && estim.isEstimated())) {
			paramModel.studyRange(estimator);
		} else {
			paramModel.addRange(estimator.getRangeAsCurve());
		}
	}

	/**
	 * Change the YAxis type (log or not).
	 *
	 * @param log true to set a logarithmic axis, false otherwise.
	 */
	public void changeYAxisType(boolean log) {
		if (log) {
			view.getSpectrumPlot().setYAxisToLog();
		} else {
			view.getSpectrumPlot().setYAxisNormal();
		}
		view.recreatePlotsMosaic();
		view.updatePlotLabels();
	}

	/**
	 * Change the XAxis type (log or not).
	 *
	 * @param log true to set a logarithmic axis, false otherwise.
	 */
	public void changeXAxisType(boolean log) {
		if (log) {
			view.getSpectrumPlot().setXAxisToLog();
		} else {
			view.getSpectrumPlot().setXAxisNormal();
		}
		view.recreatePlotsMosaic();
		view.updatePlotLabels();
	}

	/**
	 * Set a histogram rendering.
	 *
	 * @see eu.omp.irap.cassis.gui.plot.simple.ChangeRenderingInterface#setHistogramRendering()
	 */
	@Override
	public void setHistogramRendering() {
		model.setRendering(Rendering.HISTOGRAM);
	}

	/**
	 * Set a dot rendering.
	 *
	 * @see eu.omp.irap.cassis.gui.plot.simple.ChangeRenderingInterface#setDotRendering()
	 */
	@Override
	public void setDotRendering() {
		model.setRendering(Rendering.DOT);
	}

	/**
	 * Set a line rendering.
	 *
	 * @see eu.omp.irap.cassis.gui.plot.simple.ChangeRenderingInterface#setLineRendering()
	 */
	@Override
	public void setLineRendering() {
		model.setRendering(Rendering.LINE);
	}

	/**
	 * Add a "Markers" JPanelCurve if there is none yet.
	 */
	public void addMarkersPanelCurve() {
		if (view.getInfoPanel().getModel().isJPanelCurveExist(InfoPanelConstants.MARKERS_TITLE)) {
			return;
		}
		JPanelCurve markersCurve = new JPanelCurve(InfoPanelConstants.MARKERS_TITLE, true);
		XYSeriesCassis serie = new XYSpectrumSeries(InfoPanelConstants.MARKERS_TITLE,
				XAxisCassis.getXAxisUnknown(), TypeCurve.MARKERS, new CommentedSpectrum());
		CurvePanelView cpv = new CurvePanelView(new CurvePanelModel(serie), false, true, false);
		cpv.getCheckBox().setSelected(true);
		cpv.getModel().getCassisModel().getConfigCurve().setColor(MarkerManager.COLOR_SELECTION);
		cpv.getButtonColor().setBackground(MarkerManager.COLOR_SELECTION);
		((CurvePanelMosaicControl) cpv.getControl()).addCurveCassisListener(this);
		cpv.getButtonColor().setEnabled(false);
		markersCurve.addCurvePane(cpv);
		view.getInfoPanel().addJPanelCurve(markersCurve);
		view.getInfoPanel().revalidate();
		view.getInfoPanel().repaint();
	}

	/**
	 * Remove the "Markers" panel curve if there is one.
	 */
	public void removeMarkersPanelCurve() {
		JPanelCurve markersPc = view.getInfoPanel().getJPanelCurveByName(
				InfoPanelConstants.MARKERS_TITLE);
		if (markersPc != null) {
			view.getInfoPanel().removePanelCurve(markersPc);
			view.getInfoPanel().revalidate();
			view.getInfoPanel().repaint();
		}
	}

	/**
	 * Remove markers on current plot.
	 */
	public void resetSelectionCurrentPlot() {
		resetAllSelection();
	}

	/**
	 * Remove markers on all plot.
	 */
	public void resetSelectionAllPlot() {
		List<FitPanel> fitPanelList = view.getFitPanelList();
		for (FitPanel fitPanel : fitPanelList) {
			fitPanel.getMarkerManager().clearListMarker();
		}
		List<AdvancedFitModel> models = model.getFitModelManager().getModels();
		for (AdvancedFitModel advancedFitModel : models) {
			advancedFitModel.getParametersModel().resetStudyRange();
		}
		view.getSpectrumPlot().getPlot().clearDomainMarkers();
		view.getGallerySortPane().hideMarkers();
	}

	/**
	 * Handle the removal of a marker.
	 * This remove the "Markers" panel curve if there is no markers left.
	 *
	 * @see eu.omp.irap.cassis.gui.plot.util.SpectrumFitPanelInterface#intervalMarkerRemoved()
	 */
	@Override
	public void intervalMarkerRemoved() {
		if (!view.getCurrentFitPanel().getMarkerManager().haveMarker()) {
			removeMarkersPanelCurve();
		}
	}

	public void yAxisLeftChanged() {

		if (view.getYAxisLeftComboBox().getSelectedItem() == null) {
			return;
		}
		YAxisCassis previousAxis = model.getYAxisCassis();
		YAxisCassis selectedAxis = (YAxisCassis) view.getYAxisLeftComboBox().getSelectedItem();

		model.setYAxisCassis(selectedAxis);

		boolean res = view.getSpectrumPlot().updateYAxisParameters(previousAxis, selectedAxis);
		if (res) {
			view.getSpectrumPlot().changeYAxis(model.getYAxisCassis());
		}

		view.setXAxisOfFitPanel();


//		comboTopAxisChanged();

//		model.getFitModelManager().convertConfiguration(previousXAxis);

		if ( view.getGallerySortPane() != null) {
			for (final SpectrumPlot spectrumPlot : view.getGallerySortPane().getListSpectrumPlots()) {
				res = spectrumPlot.updateYAxisParameters(previousAxis, selectedAxis);
				if (res) {
					spectrumPlot.changeYAxis(model.getYAxisCassis());
				}
			}
		}
		if (previousAxis.getUnit() != selectedAxis.getUnit()) {
			view.recreatePlotsMosaic();

		}
		changeLeftLabel();
		view.getToolsView().getControl().refreshInfo();
		model.getFitModelManager().refreshInputs();
	}
}
