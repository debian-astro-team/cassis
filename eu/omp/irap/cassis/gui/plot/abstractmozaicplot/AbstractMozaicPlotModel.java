/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.abstractmozaicplot;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import org.jfree.data.xy.XYSeriesCollection;

import eu.omp.irap.cassis.common.Formula;
import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.TypeFrequency;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.XAxisVelocity;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.gui.fit.advanced.AdvancedFitModel;
import eu.omp.irap.cassis.gui.fit.advanced.ModelFitManager;
import eu.omp.irap.cassis.gui.model.CassisResult;
import eu.omp.irap.cassis.gui.otherspecies.SpeciesModel;
import eu.omp.irap.cassis.gui.plot.abstractplot.AbstractPlotModel;
import eu.omp.irap.cassis.gui.plot.curve.Rendering;
import eu.omp.irap.cassis.gui.plot.curve.TypeCurve;
import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassis;
import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassisCollection;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import eu.omp.irap.cassis.gui.plot.gallery.GalleryNavigationModel;
import eu.omp.irap.cassis.gui.plot.gallery.GalleryNavigationUtils;
import eu.omp.irap.cassis.gui.plot.infopanel.InfoModel;
import eu.omp.irap.cassis.gui.plot.infopanel.InfoPanelConstants;
import eu.omp.irap.cassis.gui.plot.infopanel.LineConfigCurve;
import eu.omp.irap.cassis.gui.plot.line.CassisPlot;
import eu.omp.irap.cassis.gui.plot.tools.ToolsModel;
import eu.omp.irap.cassis.gui.plot.tools.ToolsMosaicInterface;
import eu.omp.irap.cassis.gui.plot.tools.ToolsMosaicModel;
import eu.omp.irap.cassis.gui.plot.util.StackMosaicModel;
import eu.omp.irap.cassis.properties.Software;

public abstract class AbstractMozaicPlotModel extends AbstractPlotModel implements ToolsMosaicInterface{

	public static final String CHANGE_RENDERING_EVENT = "changeRendering";
	public static final String NEW_RESULT_EVENT = "newResult";
	public static final String NEW_RESULT_NO_REFRESH_EVENT = "newResultNoRefresh";
	public static final String REFRESH_EVENT = "refresh";
	public static final String ON_GALLERY_EVENT = "onGalleryEvent";

	protected ArrayList<CassisPlot> listCassisPlots;

	/* display dataset */
	protected XYSeriesCassisCollection centerDataset = new XYSeriesCassisCollection();
	private XYSeriesCassisCollection topLineDataset = new XYSeriesCassisCollection();
	private XYSeriesCassisCollection topLineErrorDataset = new XYSeriesCassisCollection();
	private XYSeriesCassisCollection bottomLineSignalDataset = new XYSeriesCassisCollection();
	private XYSeriesCassisCollection bottomLineImageDataset = new XYSeriesCassisCollection();

	private List<InfoModel> listInfoModels;
	protected List<ToolsModel> listToolsModels;
	protected ToolsMosaicModel toolsMosaicModel;
	private XAxisCassis xAxisCassisTop = XAxisCassis.getXAxisFrequency(UNIT.GHZ);

	private boolean onGallery = false;
	private boolean plotNumberVisible;
	private Color plotNumberColor = new Color(255, 102, 51);

	protected GalleryNavigationModel galleryNavigationModel;

	/**
	 * Molecule model constructor: Initialize Gaussian parameters, and set chart
	 * renderer, axis and plot
	 */
	protected AbstractMozaicPlotModel() {
		speciesModel = new SpeciesModel(false);
		this.fitModelManager = new ModelFitManager();
		stackMosaicModel = new StackMosaicModel();
		listCassisPlots = new ArrayList<>();
		final CassisPlot cassisPlot = new CassisPlot();
		cassisPlot.setXAxisVelocity(XAxisCassis.getXAxisVelocity());
		listCassisPlots.add(cassisPlot);
		listInfoModels = new ArrayList<>();
		listToolsModels = new ArrayList<>();
		toolsMosaicModel = new ToolsMosaicModel(this);
		rendering = Rendering.valueOf(Software.getUserConfiguration().getRendering());
		initializeParameters();
	}

	public abstract void setCassisPlots(CassisResult result, LineConfigCurve lineConfigCurve);

	public ModelFitManager getFitModelManager() {
		return fitModelManager;
	}

	public void clearFitModels() {
		fitModelManager.clearModels();
	}

	public AdvancedFitModel getModel(int index) {
		return fitModelManager.getModel(index);
	}

	public AdvancedFitModel getCurrentFitModel() {
		return fitModelManager.getCurrentModel();
	}

	public void addFitModel(CassisPlot plot) {
		fitModelManager.addModel(plot);
	}

	public abstract CassisResult getLineResult();

	public void addSeriesToDisplay() {
		CassisPlot cassisPlot = getCurrentCassisPlot();
		addSeries(cassisPlot.getFileSpectrumSeries(), centerDataset);
		addSeries(cassisPlot.getSyntheticSeries(), centerDataset);
		addSeries(cassisPlot.getFitDataset(),centerDataset);
		addSeries(cassisPlot.getOverlaySpectrumSeries(),centerDataset);
		addSeries(cassisPlot.getResultSeries(),centerDataset);

		addSeries(cassisPlot.getFileLineSeriesCassis(), topLineDataset);
		addSeries(cassisPlot.getSurroundSeries(), topLineDataset);
		addSeries(cassisPlot.getOverlayLineSpectrumDataset(),topLineDataset);

		addSeries(cassisPlot.getErrorFileSeries(), topLineErrorDataset);

		addSeries(cassisPlot.getOtherSpeciesSeriesSignal(), bottomLineSignalDataset);
		addSeries(cassisPlot.getOtherSpeciesSeriesImage(), bottomLineImageDataset);
		fitModelManager.refreshInputs();
	}

	/**
	 * Initialize the parameters.
	 */
	private void initializeParameters() {
		if (speciesModel != null) {
			speciesModel.setSpeciesSignalVisible(false);
			speciesModel.setSpeciesImageVisible(false);
		}
	}

	protected void addSeries(XYSeriesCassisCollection series,
			XYSeriesCassisCollection xySeriesCassisCollection) {
		if (series != null) {
			for (int i = 0; i < series.getSeriesCount(); i++) {
				addSeries(series.getSeries(i), xySeriesCassisCollection);
			}
		}
	}

	private void addSeries(XYSeriesCassis series,
			XYSeriesCassisCollection xySeriesCassisCollection) {
		if (series != null) {
			xySeriesCassisCollection.addSeries(series);
		}
	}

	public void removeAllSeriesDisplay() {
		centerDataset.removeAllSeries();
		topLineDataset.removeAllSeries();
		topLineErrorDataset.removeAllSeries();
		bottomLineSignalDataset.removeAllSeries();
		bottomLineImageDataset.removeAllSeries();
	}

	/**
	 * Return info on the nearest line of the given parameter.
	 *
	 * @param xValue The X value.
	 * @param yValue The Y value.
	 * @param valueReturn Array to return parameter.
	 * @param series The series contening the lines.
	 * @return The message with informations on the nearest line.
	 * @see eu.omp.irap.cassis.gui.plot.util.NearestLineInfo#getNearestLineInfo(double, double, java.lang.Double[], org.jfree.data.xy.XYSeriesCollection)
	 */
	@Override
	public String getNearestLineInfo(final double xValue, double yValue, Double[] valueReturn, XYSeriesCollection series) {
		List<LineDescription> listOfLines = new ArrayList<>();
		for (int cpt = 0; cpt < series.getSeriesCount(); cpt++) {
			XYSeriesCassis spectrumSeries = (XYSeriesCassis) series.getSeries(cpt);
			if (spectrumSeries.getConfigCurve().isVisible()&&
				!TypeCurve.LINE_ERROR.equals(spectrumSeries.getTypeCurve())&&
				!TypeCurve.DATA.equals(spectrumSeries.getTypeCurve())) {
				List<LineDescription> listOfSpectrumLines = spectrumSeries.getListOfLines();
				if (listOfSpectrumLines != null)
					listOfLines.addAll(listOfSpectrumLines);
			}
		}
		String res = getNearestLine(xValue, listOfLines, valueReturn, getXAxisCassis(),
				speciesModel.getTypeFrequency());
		if (res.isEmpty())
			res = "No line found";

		return res;
	}

	public String getNearestLine(final double xLine,
			final List<LineDescription> listOfLines, Double[] valReturn, XAxisCassis scale,
			TypeFrequency typeFrequency) {
		StringBuilder lineInfo = new StringBuilder();
		double valueToCompare = Double.MAX_VALUE - 1;
		double tmpDifference;
		int nearestLineIndex = -1;
		ArrayList<Integer> indicesNearestLines = new ArrayList<>();
		double valueFind = 0;
		if (listOfLines.isEmpty())
			return "";
		if (scale instanceof XAxisVelocity && TypeFrequency.SKY.equals(typeFrequency)) {
			scale.setVlsr(0);
		}

		final Double convertToMHzFreq = scale.convertToMHzFreq(xLine);

		for (int i = 0; listOfLines != null && i < listOfLines.size(); i++) {
			LineDescription line = listOfLines.get(i);
			double value = line.getFreqCompute();
			if (line.isDoubleSideBand())
				value = Formula.convertFreqWithLoFreq(value, listCassisPlots.get(stackMosaicModel.getCurrentSpectrum()).getDataSpectrum().getLoFreq());
			tmpDifference = Math.abs(value - convertToMHzFreq);

			if (tmpDifference <= valueToCompare) {
				if (tmpDifference < valueToCompare) {
					nearestLineIndex = i;
					valueToCompare = tmpDifference;
					valueFind = value;
					indicesNearestLines.clear();
				}
				indicesNearestLines.add(i);
			}
		}

		if (nearestLineIndex > -1) {
			double vlsr = 0.;
			for (Integer indice : indicesNearestLines) {
				LineDescription line = listOfLines.get(indice);
				lineInfo.append(line.getIdentification());
				if (TypeFrequency.SKY.equals(typeFrequency))
					vlsr = 0;
				else
					vlsr = line.getVlsrData();
				lineInfo.append("\n\n");
			}
			scale.setVlsr(vlsr);
			lineInfo.deleteCharAt(lineInfo.length() - 1);
			lineInfo.deleteCharAt(lineInfo.length() - 1);
			valReturn[0] = scale.convertFromMhzFreq(valueFind);
		}
		else {
			lineInfo.append("");
			valReturn[0] = Double.NaN;
		}

		return lineInfo.toString();
	}

	public void addFitCurve(XYSeriesCassis serie) {
		centerDataset.addSeries(serie);
		int currentSpectrum = stackMosaicModel.getCurrentSpectrum();
		XYSeriesCollection fitDataset = listCassisPlots.get(currentSpectrum).getFitDataset();
		fitDataset.addSeries(serie);
	}

	public void removeFitAndResiudalCurve() {
		XYSeriesCassisCollection fitDataset = listCassisPlots.get(
				stackMosaicModel.getCurrentSpectrum()).getFitDataset();
		for (int cpt = 0; cpt < fitDataset.getSeriesCount(); cpt++) {
			XYSeriesCassis series = fitDataset.getSeries(cpt);
			centerDataset.removeSeries(series);
		}
	}

	public void removeOverlayDataset(XYSeriesCassis serie) {
		if (TypeCurve.OVERLAY_DATA.equals(serie.getTypeCurve()))
			centerDataset.removeSeries(serie);
		else if (TypeCurve.OVERLAY_LINELIST.equals(serie.getTypeCurve()))
			topLineDataset.removeSeries(serie);
	}

	public XYSpectrumSeries getFitCurve(int cpt) {
		return (XYSpectrumSeries) centerDataset.getSeries("Fit" + (cpt + 1));
	}

	/**
	 * Return the fit series.
	 *
	 * @return the fit series.
	 */
	public XYSpectrumSeries getFit() {
		return (XYSpectrumSeries) centerDataset.getSeries("Fit");
	}

	public Color getColorFitCurve() {
		Color color = Color.RED;
		XYSeriesCassis series = centerDataset.getSeries(InfoPanelConstants.FIT_TITLE);
		if (series != null)
			color = series.getConfigCurve().getColor();
		return color;
	}

	public XYSpectrumSeries getSeriesToFit() {
		for (int cpt = 0; cpt < centerDataset.getSeries().size(); cpt++) {
			XYSpectrumSeries series = (XYSpectrumSeries) centerDataset.getSeries(cpt);
			if (series.getConfigCurve().isVisible() && series.isToFit()) {
				return series;
			}
		}
		return null;
	}

	public List<LineDescription> getAllSyntheticLines() {
		ArrayList<LineDescription> lines = new ArrayList<>();
		for (CassisPlot cassisPlot : listCassisPlots) {
			lines.addAll(cassisPlot.getSyntheticLine());
		}
		return lines;
	}

	public List<LineDescription> getAllOtherSpeciesLines() {
		ArrayList<LineDescription> lines = new ArrayList<>();
		for (CassisPlot cassisPlot : listCassisPlots) {
			lines.addAll(cassisPlot.getOtherListSignal());
			lines.addAll(cassisPlot.getOtherListImage());
		}
		return lines;
	}

	public XYSpectrumSeries getCurrentDataCurve() {
		return getCurrentDataCurve(stackMosaicModel.getCurrentSpectrum());
	}

	public XYSpectrumSeries getCurrentDataCurve(int num) {
		return listCassisPlots.get(num).getFileSpectrumSeries();
	}

	public void setDataSeries(XYSpectrumSeries substractSeries) {
		int currentSpectrum = stackMosaicModel.getCurrentSpectrum();
		listCassisPlots.get(currentSpectrum).setFileSpectrumSeries(substractSeries);
	}

	@Override
	public String getTelescope() {
		return this.telescope;
	}

	public StackMosaicModel getStackMosaicModel() {
		return stackMosaicModel;
	}

	public SpeciesModel getSpeciesModel() {
		return speciesModel;
	}

	/**
	 * @return the listCassisPlot
	 */
	public final ArrayList<CassisPlot> getListCassisPlots() {
		return listCassisPlots;
	}

	/**
	 * Return the dataset who contains the lines on the top.
	 *
	 * @return the top line dataset.
	 */
	public XYSeriesCassisCollection getTopLineDataset() {
		return topLineDataset;
	}

	/**
	 * Return the dataset who contains the lines error on the top.
	 *
	 * @return the top lines error dataset.
	 */
	public XYSeriesCassisCollection getTopLineErrorDataset() {
		return topLineErrorDataset;
	}

	/**
	 * Return the dataset who contains the other species signal lines.
	 *
	 * @return the other species signal dataset.
	 */
	public XYSeriesCassisCollection getBottomLineSignalDataset() {
		return bottomLineSignalDataset;
	}

	/**
	 * Return the dataset who contains the other species image lines.
	 *
	 * @return the other species image dataset.
	 */
	public XYSeriesCassisCollection getBottomLineImageDataset() {
		return bottomLineImageDataset;
	}

	/**
	 * Return the center dataset.
	 *
	 * @return the center dataset.
	 */
	public XYSeriesCassisCollection getCenterDataset() {
		return centerDataset;
	}

	public List<InfoModel> getListInfoModels() {
		return listInfoModels;
	}

	public void displayOtherSpecies() {
		bottomLineSignalDataset.removeAllSeries();
		bottomLineImageDataset.removeAllSeries();
		CassisPlot cassisPlot = getCurrentCassisPlot();
		if (cassisPlot.getOtherSpeciesSeriesSignal() != null) {
			bottomLineSignalDataset.addSeries(cassisPlot.getOtherSpeciesSeriesSignal());
		}
		if (speciesModel.isHaveImage() && cassisPlot.getOtherSpeciesSeriesImage() != null) {
			bottomLineImageDataset.addSeries(cassisPlot.getOtherSpeciesSeriesImage());
		}
	}

	protected CassisPlot getCurrentCassisPlot() {
		return getListCassisPlots().get(getStackMosaicModel().getCurrentSpectrum());
	}

	/**
	 * Set the given dataset as center dataset.
	 *
	 * @param dataset The dataset to set.
	 */
	public void setCenterDataset(XYSeriesCassisCollection dataset) {
		dataset.setRendering(rendering);
		centerDataset = dataset;
	}

	/**
	 * Set the given dataset as top line dataset.
	 *
	 * @param dataset The dataset to set.
	 */
	public void setTopLineDataset(XYSeriesCassisCollection dataset) {
		dataset.setRendering(rendering);
		topLineDataset = dataset;
	}

	/**
	 * Set the given dataset as top line error dataset.
	 *
	 * @param dataset The dataset to set.
	 */
	public void setTopLineErrorDataset(XYSeriesCassisCollection dataset) {
		dataset.setRendering(rendering);
		topLineErrorDataset = dataset;
	}

	/**
	 * Set the given dataset as bottom line signal dataset.
	 *
	 * @param dataset The dataset to set.
	 */
	public void setBottomLineSignalDataset(XYSeriesCassisCollection dataset) {
		dataset.setRendering(rendering);
		bottomLineSignalDataset = dataset;
	}

	/**
	 * Set the given dataset as bottom line image dataset.
	 *
	 * @param dataset The dataset to set.
	 */
	public void setBottomLineImageDataset(XYSeriesCassisCollection dataset) {
		dataset.setRendering(rendering);
		bottomLineImageDataset = dataset;
	}

	@Override
	public XAxisCassis getXAxisCassis() {
		return getCurrentCassisPlot().getxAxisCassis();
	}

	public XAxisCassis getXAxisCassisVelocity() {
		return getCurrentCassisPlot().getXAxisVelocity();
	}

	public YAxisCassis getYAxisCassis() {
		return yAxisCassis;
	}

	public YAxisCassis getyAxis() {
		return yAxisCassis;
	}

	public void setYAxisCassis(YAxisCassis newYAxisCassis) {
		yAxisCassis = newYAxisCassis;
		for (int cpt = 0; cpt < listCassisPlots.size(); cpt++) {
			listCassisPlots.get(cpt).setYAxisCassis(newYAxisCassis);
		}
	}

	@Override
	public List<XYSpectrumSeries> getDataForToolsSpectrumOne(int numPlot) {
		CassisPlot currentCassisPlot = listCassisPlots.get(numPlot);
		List<XYSpectrumSeries> listSeries = new ArrayList<>();

		// DATA
		if (currentCassisPlot.getFileSpectrumSeries() != null)
			listSeries.add(currentCassisPlot.getFileSpectrumSeries());

		// OVERLAY_DATA
		XYSeriesCassisCollection series = currentCassisPlot.getOverlaySpectrumSeries();
		if (series != null) {
			for (int i = 0; i < series.getSeriesCount(); i++) {
				listSeries.add((XYSpectrumSeries) series.getSeries(i));
			}
		}
		// RESULT
		series = currentCassisPlot.getResultSeries();
		if (series != null) {
			for (int i = 0; i < series.getSeriesCount(); i++) {
				listSeries.add((XYSpectrumSeries) series.getSeries(i));
			}
		}

		return listSeries;
	}

	@Override
	public List<XYSpectrumSeries> getDataForToolsSpectrumTwo(int numPlot) {
		CassisPlot currentCassisPlot = listCassisPlots.get(numPlot);
		List<XYSpectrumSeries> listSeries = new ArrayList<>();

		// SYNTHETIC
		if (currentCassisPlot.getSyntheticSeries() != null)
			listSeries.add(currentCassisPlot.getSyntheticSeries());

		// OVERLAY_DATA
		XYSeriesCassisCollection series = currentCassisPlot.getOverlaySpectrumSeries();
		if (series != null) {
			for (int i = 0; i < series.getSeriesCount(); i++) {
				listSeries.add((XYSpectrumSeries) series.getSeries(i));
			}
		}

		// FIT
		series = currentCassisPlot.getFitDataset();
		if (series != null) {
			for (int i = 0; i < series.getSeriesCount(); i++) {
				XYSeriesCassis serie = series.getSeries(i);
				if (serie.getTypeCurve() == TypeCurve.FIT) {
					listSeries.add((XYSpectrumSeries) serie);
				}
			}
		}

		// RESULT
		series = currentCassisPlot.getResultSeries();
		if (series != null) {
			for (int i = 0; i < series.getSeriesCount(); i++) {
				listSeries.add((XYSpectrumSeries) series.getSeries(i));
			}
		}

		return listSeries;
	}

	@Override
	public void addResult(XYSpectrumSeries serie, int numPlot, boolean refresh) {
		serie.setToFit(true);
		listCassisPlots.get(numPlot).getResultSeries().addSeries(serie);
		if (refresh) {
			fireDataChanged(new ModelChangedEvent(NEW_RESULT_EVENT, serie, numPlot));
		} else {
			fireDataChanged(new ModelChangedEvent(NEW_RESULT_NO_REFRESH_EVENT, serie, numPlot));
		}
	}

	@Override
	public void refreshInfoPanelGallery() {
		fireDataChanged(new ModelChangedEvent(REFRESH_EVENT));
	}

	@Override
	public List<ToolsModel> getListToolsModel() {
		return listToolsModels;
	}

	public ToolsModel getCurrentToolsModel() {
		return listToolsModels.get(stackMosaicModel.getCurrentSpectrum());
	}

	public ToolsMosaicModel getToolsMosaicModel() {
		return toolsMosaicModel;
	}

	public void removeResultFromCurrentPlot() {
		XYSeriesCassisCollection resultDataset = listCassisPlots.get(
				stackMosaicModel.getCurrentSpectrum()).getResultSeries();
		for (int cpt = 0; cpt < resultDataset.getSeriesCount(); cpt++) {
			XYSeriesCassis series = resultDataset.getSeries(cpt);
			centerDataset.removeSeries(series);
		}
	}

	@Override
	public String getBottomAxis() {
		return getXAxisCassis().toString();
	}

	@Override
	public String getTopAxis() {
		return xAxisCassisTop.toString();
	}

	@Override
	public String getLeftAxis() {
		return yAxisCassis.toString();
	}


	public void setXAxisCassisTop(XAxisCassis xAxisCassis) {
		this.xAxisCassisTop = xAxisCassis;
	}

	public XAxisCassis getXAxisCassisTop() {
		return this.xAxisCassisTop;
	}

	public void setXAxisCassis(XAxisCassis xAxisCassis) {
		for (int cpt = 0; cpt < listCassisPlots.size(); cpt++) {
			listCassisPlots.get(cpt).setXAxisCassis(xAxisCassis);
		}
	}

	public int getCurrentNumTriplePlot() {
		return getStackMosaicModel().getCurrentSpectrum();
	}

	public int getNbCassisPlots() {
		return getListCassisPlots().size();
	}

	public double getLoFrequency() {
		return getListCassisPlots().get(0).getxAxisCassis().getLoFreq();
	}

	public boolean isOtherSpeciesSignalVisible() {
		boolean res = false;
		CassisPlot casssisPlot = getListCassisPlots().get(0);
		if (casssisPlot.getOtherSpeciesSeriesSignal() != null) {
			res = casssisPlot.getOtherSpeciesSeriesSignal().getConfigCurve().isVisible();
		}
		return res;
	}

	public boolean isOtherSpeciesImageVisible() {
		boolean res = false;
		CassisPlot casssisPlot = getListCassisPlots().get(0);
		if (casssisPlot.getOtherSpeciesSeriesImage() != null) {
			res = casssisPlot.getOtherSpeciesSeriesImage().getConfigCurve().isVisible();
		}
		return res;
	}

	public boolean isSignalOrImageSpeciesVisible() {
		CassisPlot cp = getListCassisPlots().get(0);
		if (cp.getOtherSpeciesSeriesSignal() != null
				&& cp.getOtherSpeciesSeriesSignal().getConfigCurve().isVisible()) {
			return true;
		}
		if (cp.getOtherSpeciesSeriesImage() != null
				&& cp.getOtherSpeciesSeriesImage().getConfigCurve().isVisible()) {
			return true;
		}
		return false;
	}

	@Override
	public void setTelescope(String telescope) {
		this.telescope = telescope;
	}

	public boolean haveImage() {
		return getSpeciesModel().isHaveImage();
	}

	public boolean isOnGallery() {
		return onGallery;
	}

	public void setOnGallery(boolean b) {
		onGallery = b;
		fireDataChanged(new ModelChangedEvent(ON_GALLERY_EVENT));
	}

	/**
	 * Search the nearest spectrum when navigation.
	 *
	 * @param spectrumCurrentId The spectrum id.
	 * @param stepValue The step value.
	 * @param move Move direction: 1 to move right, 0 to move left.
	 * @return indice of the the nearest spectrum.
	 */
	public int searchNearestSpectrum(int spectrumCurrentId, Double stepValue, int move) {
		stepValue = stepValue * 1000; // En Mhz

		int spectrumNearest = spectrumCurrentId;
		CassisPlot cassisPlot = getListCassisPlots().get(spectrumCurrentId);

		double frequencyCurrent = cassisPlot.getFreqRef();
		double frequency;
		if (move == 1) {
			// step move right
			frequency = frequencyCurrent + stepValue;
		} else {
			// step move left
			frequency = frequencyCurrent - stepValue;
		}

		// Search a nearest spectrum
		Double min = Double.MAX_VALUE;
		if (move == 1) {
			for (int i = 0; i < getListCassisPlots().size(); i++) {
				cassisPlot = getListCassisPlots().get(i);
				if (frequencyCurrent < cassisPlot.getFreqRef()
						&& min > Math.abs(frequency - cassisPlot.getFreqRef())) {
					min = Math.abs(frequency - cassisPlot.getFreqRef());
					spectrumNearest = i;
				}
			}
		}
		else {
			for (int i = 0; i < getListCassisPlots().size(); i++) {
				cassisPlot = getListCassisPlots().get(i);
				if (frequencyCurrent > cassisPlot.getFreqRef()
						&& min > Math.abs(frequency - cassisPlot.getFreqRef())) {
					min = Math.abs(frequency - cassisPlot.getFreqRef());
					spectrumNearest = i;
				}
			}
		}

		return spectrumNearest;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsInterface#getDataForToolsSpectrumAll(int)
	 */
	@Override
	public List<XYSpectrumSeries> getDataForToolsSpectrumAll(int numPlot) {
		CassisPlot currentCassisPlot = listCassisPlots.get(numPlot);
		List<XYSpectrumSeries> listSeries = new ArrayList<>();

		// DATA
		if (currentCassisPlot.getFileSpectrumSeries() != null) {
			listSeries.add(currentCassisPlot.getFileSpectrumSeries());
		}
		// SYNTHETIC
		if (currentCassisPlot.getSyntheticSeries() != null) {
			listSeries.add(currentCassisPlot.getSyntheticSeries());
		}
		// OVERLAY_DATA
		XYSeriesCassisCollection series = currentCassisPlot.getOverlaySpectrumSeries();
		if (series != null) {
			for (int i = 0; i < series.getSeriesCount(); i++) {
				listSeries.add((XYSpectrumSeries) series.getSeries(i));
			}
		}

		// FIT
		series = currentCassisPlot.getFitDataset();
		if (series != null) {
			for (int i = 0; i < series.getSeriesCount(); i++) {
				XYSeriesCassis serie = series.getSeries(i);
				if (serie.getTypeCurve().isFit()) {
					listSeries.add((XYSpectrumSeries) serie);
				}
			}
		}

		// RESULT
		series = currentCassisPlot.getResultSeries();
		if (series != null) {
			for (int i = 0; i < series.getSeriesCount(); i++) {
				listSeries.add((XYSpectrumSeries) series.getSeries(i));
			}
		}

		return listSeries;
	}

	public TypeFrequency getTypeFrequency() {
		return typeFrequency;
	}

	protected void setTypeFrequency(TypeFrequency typeFreq) {
		this.typeFrequency = typeFreq;
	}

	public void setPlotNumberVisible(boolean value) {
		plotNumberVisible = value;
	}

	/**
	 * @return the plotNumberVisible
	 */
	public boolean isPlotNumberVisible() {
		return plotNumberVisible;
	}

	public void setPlotNumberColor(Color theNewColor) {
		plotNumberColor = theNewColor;
	}

	/**
	 * @return the plotNumberColor
	 */
	public Color getPlotNumberColor() {
		return plotNumberColor;
	}

	public GalleryNavigationModel getGalleryNavigationModel() {
		return galleryNavigationModel;
	}

	public void initGalleryNavigation() {
		int nbPlot = getNbCassisPlots();
		galleryNavigationModel.setPlotNumber(nbPlot);
		galleryNavigationModel.setFilter(
				GalleryNavigationUtils.getListOfSpectrums("1-" + nbPlot, nbPlot), false);
	}

	/**
	 * Return the {@link Rendering}.
	 *
	 * @return the {@link Rendering}.
	 */
	public Rendering getRendering() {
		return rendering;
	}

	/**
	 * Change the rendering.
	 *
	 * @param newRendering The new rendering to set.
	 */
	public void setRendering(Rendering newRendering) {
		boolean change = rendering != newRendering;
		rendering = newRendering;
		updateRendering();
		if (change) {
			fireDataChanged(new ModelChangedEvent(CHANGE_RENDERING_EVENT));
		}
	}

	/**
	 * Update the rendering of the CassisPlots and of the currents collections.
	 */
	public void updateRendering() {
		for (CassisPlot cassisPlot : listCassisPlots) {
			cassisPlot.setRendering(rendering);
		}
		centerDataset.setRendering(rendering);
		topLineDataset.setRendering(rendering);
		topLineErrorDataset.setRendering(rendering);
		bottomLineSignalDataset.setRendering(rendering);
		bottomLineImageDataset.setRendering(rendering);
	}

	/**
	 * Check and return if the series with the given name is available.
	 *
	 * @param name The name to search
	 * @param checkAllCassisPlot true to check in all the CassisPlot, false check
	 *  only the datasets of the current TriplePlot
	 * @return true if the name is available, false otherwise
	 */
	public boolean isSeriesNameAvailable(String name, boolean checkAllCassisPlot) {
		boolean current = !centerDataset.containsSeries(name)
				&& !topLineDataset.containsSeries(name)
				&& !topLineErrorDataset.containsSeries(name)
				&& !bottomLineSignalDataset.containsSeries(name)
				&& !bottomLineImageDataset.containsSeries(name);

		if (current && checkAllCassisPlot) {
			for (CassisPlot plot : listCassisPlots) {
				if (plot.containsSeries(name)) {
					current = false;
					break;
				}
			}
		}
		return current;
	}

	/**
	 * Check and return if the series name is allowed: Fit, Fit residual, Signal,
	 *  Image, Error, Line and Nearby Line are not allowed.
	 *
	 * @param name The name to check
	 * @return true if the name is allowed, false otherwise
	 */
	public boolean isSeriesNameAllowed(String name) {
		boolean allowed;
		switch (name) {
		case InfoPanelConstants.FIT_TITLE:
		case InfoPanelConstants.FIT_RESIDUAL_TITLE:
		case InfoPanelConstants.SIGNAL_TITLE:
		case InfoPanelConstants.IMAGE_TITLE:
		case InfoPanelConstants.ERROR_TITLE:
		case InfoPanelConstants.LINE_TITLE:
		case InfoPanelConstants.NEARBY_LINE_TITLE:
			allowed = false;
			break;
		default:
			allowed = true;
			break;
		}
		return allowed;
	}

	/**
	 * Check and return if the given name is allowed and available.
	 *
	 * @param name The name to check
	 * @param checkAllCassisPlot true to check in all the CassisPlot, false check
	 *  only the datasets of the current TriplePlot
	 * @return true if the name is allowed and available, false otherwise.
	 */
	public boolean isSeriesNameAllowedAndAvailable(String name, boolean checkAllCassisPlot) {
		return isSeriesNameAllowed(name) && isSeriesNameAvailable(name, checkAllCassisPlot);
	}

	/**
	 * Check if the given series name is allowed and available then return a new
	 *  created name who is unique if necessary.
	 *
	 * @param seriesName The base series name
	 * @param checkAllCassisPlot true to check in all the CassisPlot, false check
	 *  only the datasets of the current TriplePlot
	 * @return an unique name for a series
	 */
	public String getUniqueSeriesName(String seriesName, boolean checkAllCassisPlot) {
		if (isSeriesNameAllowedAndAvailable(seriesName, checkAllCassisPlot)) {
			return seriesName;
		}
		String newName;
		int i = 0;
		do {
			i++;
			newName = seriesName + '_' + i;
		} while (!isSeriesNameAllowedAndAvailable(newName, checkAllCassisPlot));
		return newName;
	}

	/**
	 * Return if there is at least one series with TypeCurve.FIT_COMPO.
	 *
	 * @return the number of series with the TypeCurve.FIT_COMPO.
	 */
	public boolean hasFitCompo() {
		for (XYSeriesCassis series : centerDataset.getSeries()) {
			if (series.getTypeCurve() == TypeCurve.FIT_COMPO) {
				return true;
			}
		}
		return false;
	}

}
