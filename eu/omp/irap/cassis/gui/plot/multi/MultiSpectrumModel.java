/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.multi;

import java.awt.Color;
import java.util.List;

import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.XAxisVelocity;
import eu.omp.irap.cassis.gui.fit.advanced.ModelFitManager;
import eu.omp.irap.cassis.gui.model.CassisResult;
import eu.omp.irap.cassis.gui.model.loomisanalysis.LoomisWoodResult;
import eu.omp.irap.cassis.gui.plot.abstractmozaicplot.AbstractMozaicPlotModel;
import eu.omp.irap.cassis.gui.plot.curve.JPanelCurve;
import eu.omp.irap.cassis.gui.plot.curve.TypeCurve;
import eu.omp.irap.cassis.gui.plot.curve.XYLineSeriesCassis;
import eu.omp.irap.cassis.gui.plot.curve.XYLoomisLineSeriesCassis;
import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassisCollection;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import eu.omp.irap.cassis.gui.plot.curve.config.ColorsCurve;
import eu.omp.irap.cassis.gui.plot.gallery.GalleryNavigationModel;
import eu.omp.irap.cassis.gui.plot.gallery.GalleryOption;
import eu.omp.irap.cassis.gui.plot.infopanel.InfoModel;
import eu.omp.irap.cassis.gui.plot.infopanel.InfoPanelConstants;
import eu.omp.irap.cassis.gui.plot.infopanel.LineConfigCurve;
import eu.omp.irap.cassis.gui.plot.line.CassisPlot;
import eu.omp.irap.cassis.gui.plot.tools.ToolsModel;
import eu.omp.irap.cassis.gui.plot.tools.ViewType;


public class MultiSpectrumModel extends AbstractMozaicPlotModel {

	private LoomisWoodResult lineResult;


	public MultiSpectrumModel() {
		super();
		GalleryOption[] options = { GalleryOption.NONE, GalleryOption.ROW };
		galleryNavigationModel = new GalleryNavigationModel(options, false);
		fitModelManager = new ModelFitManager(galleryNavigationModel);
	}

	@Override
	public void setCassisPlots(CassisResult result, LineConfigCurve lineConfigCurve) {
		if (result instanceof LoomisWoodResult) {
			LoomisWoodResult loomisWoodResult = (LoomisWoodResult) result;
			listCassisPlots.clear();
			listToolsModels.clear();
			toolsMosaicModel.resetId();
			clearFitModels();
			lineResult = loomisWoodResult;
			this.telescope = loomisWoodResult.getTelescope();

			int nbPlot = loomisWoodResult.getFileSpectrumList().size();
			boolean haveTheoritical = loomisWoodResult.getSumSpectrumList() != null;
			for (int cpt = 0; cpt < nbPlot; cpt++) {
				CommentedSpectrum spectrumTheoritical = null;
				if (haveTheoritical) {
					spectrumTheoritical = loomisWoodResult.getSumSpectrumList().get(cpt);
				}

				CommentedSpectrum fileCommentedSpectrum = loomisWoodResult.getFileSpectrumList().get(cpt);
				XAxisCassis xAxis = fileCommentedSpectrum.getxAxisOrigin();

				xAxis.setLoFreq(fileCommentedSpectrum.getLoFreq());
				CassisPlot cassisPlot = new CassisPlot(fileCommentedSpectrum, spectrumTheoritical, xAxis, lineConfigCurve);
				cassisPlot.setVlsrData(fileCommentedSpectrum.getVlsr());
				cassisPlot.setFreqRef(fileCommentedSpectrum.getFreqRef());
				XAxisVelocity xAxisVelocity = XAxisCassis.getXAxisVelocity();
				xAxisVelocity.setFreqRef(fileCommentedSpectrum.getFreqRef());
				xAxisVelocity.setLoFreq(fileCommentedSpectrum.getLoFreq());
				cassisPlot.setXAxisVelocity(xAxisVelocity);

				ColorsCurve colors = new ColorsCurve(new Color[] { Color.BLUE, Color.RED, Color.GREEN,
						new Color(242, 121, 16), new Color(215, 54, 205) });
				final CommentedSpectrum[][] lineSpectrums = loomisWoodResult.getLineSpectrums();
				XYSeriesCassisCollection loomisWoodLine = new XYSeriesCassisCollection();
				for (int cpt2 = 0; cpt2 < lineSpectrums.length; cpt2++) {
					List<LineDescription> lines = lineSpectrums[cpt2][cpt].getListOfLines();
					Color color = colors.getNewColor();
					XYLineSeriesCassis lineSerie = null;
					if (!lines.isEmpty()) {
						int direction = XYLineSeriesCassis.BOTTOM;
						if ("Absorption".equals(loomisWoodResult.getMode()))
							direction = XYLineSeriesCassis.TOP;

						String name = lines.get(0).getMolName();
						lineSerie = new XYLoomisLineSeriesCassis(name,
								TypeCurve.LOOMIS_LINE, lines, direction, xAxis,
								yAxisCassis, loomisWoodResult.getContinuum());

						if (lineConfigCurve.isThatCurveExist(name)) {
							lineSerie.setConfigCurve(lineConfigCurve.getConfigCurve(name));
						} else {
							lineSerie.getConfigCurve().setColor(color);
							lineSerie.getConfigCurve().setSize((float) 2.0);
						}
						loomisWoodLine.addSeries(lineSerie);
					}
				}
				cassisPlot.setLoomisLine(loomisWoodLine);
				getListCassisPlots().add(cassisPlot);
				addFitModel(cassisPlot);
				getListToolsModel().add(new ToolsModel(this, cpt));
			}
			if (nbPlot >= 1) {
				setYAxisCassis(loomisWoodResult.getFileSpectrumList().get(0).getyAxis());
			}
			setTypeFrequency(getListCassisPlots().get(0).getDataSpectrum().getTypeFreq());
			updateRendering();
		}
	}

	@Override
	public LoomisWoodResult getLineResult() {
		return lineResult;
	}

	@Override
	public void addSeriesToDisplay() {
		super.addSeriesToDisplay();
		CassisPlot cassisPlot = getCurrentCassisPlot();
		addSeries(cassisPlot.getLoomisLine(), centerDataset);
	}

	@Override
	public ViewType getViewType() {
		return ViewType.LOOMIS_SPECTRUM;
	}

	@Override
	public void setDataSeries(XYSpectrumSeries substractSeries) {
		super.setDataSeries(substractSeries);
		int numPlot = getCurrentNumTriplePlot();
		if (numPlot < getListInfoModels().size()) {
			InfoModel im = getListInfoModels().get(numPlot);
			if (im.isJPanelCurveExist(InfoPanelConstants.LOOMIS_SPECTRUM_TITLE)) {
				JPanelCurve jpc = im.getPanelCurveByName(InfoPanelConstants.LOOMIS_SPECTRUM_TITLE);
				jpc.replaceSeries(substractSeries);
			}
		}
	}
}
