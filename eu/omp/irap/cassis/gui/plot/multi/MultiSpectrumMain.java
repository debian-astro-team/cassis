/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.multi;

import javax.swing.JFrame;

import eu.omp.irap.cassis.gui.PanelAppSA;
import eu.omp.irap.cassis.gui.model.loomisanalysis.LoomisWoodListener;
import eu.omp.irap.cassis.gui.model.loomisanalysis.LoomisWoodPanel;
import eu.omp.irap.cassis.gui.model.loomisanalysis.LoomisWoodResult;

/**
 * @author glorian
 *
 */
public class MultiSpectrumMain {

	private JFrame frameModel;
	private LoomisWoodPanel loomisWoodPanel;
	private MultiSpectrumView mutiSpectrumView;
	private JFrame frameView;

	private MultiSpectrumMain() {
		frameModel = new JFrame("Loomis Wood model");
		loomisWoodPanel = new LoomisWoodPanel();
		frameModel.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frameModel.setContentPane(loomisWoodPanel);
		frameModel.pack();

		loomisWoodPanel.getControl().addLoomisListener(new LoomisWoodListener() {

			@Override
			public void setLoomisModelResult(LoomisWoodResult loomisWoodResult) {
				if (frameView == null)
					createFrameView();
				mutiSpectrumView.getControl().setLoomisModelResult(loomisWoodResult);
				frameView.setVisible(true);
			}
		});

	}

	private void createFrameView() {
		frameView = new JFrame("Multiple Spectrum View");
		mutiSpectrumView = new MultiSpectrumView();
		frameView.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frameView.setContentPane(mutiSpectrumView);
		frameView.pack();
	}

	private void setVisible() {
		frameModel.setVisible(true);
	}

	public static void main(String[] args) {
		PanelAppSA.initLookAndFeel();

		MultiSpectrumMain multiSpectrumMain = new MultiSpectrumMain();
		multiSpectrumMain.setVisible();
	}

}
