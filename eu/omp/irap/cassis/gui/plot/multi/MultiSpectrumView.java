/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.multi;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.XAxisCassisUtil;
import eu.omp.irap.cassis.gui.model.CassisResult;
import eu.omp.irap.cassis.gui.model.loomisanalysis.LoomisWoodResult;
import eu.omp.irap.cassis.gui.plot.abstractmozaicplot.AbstractMozaicPlotView;
import eu.omp.irap.cassis.gui.plot.curve.CurvePanelModel;
import eu.omp.irap.cassis.gui.plot.curve.CurvePanelView;
import eu.omp.irap.cassis.gui.plot.curve.JPanelCurve;
import eu.omp.irap.cassis.gui.plot.curve.XYErrorSeries;
import eu.omp.irap.cassis.gui.plot.curve.XYLineSeriesCassis;
import eu.omp.irap.cassis.gui.plot.gallery.GalleryNavigationModel;
import eu.omp.irap.cassis.gui.plot.gallery.GalleryOption;
import eu.omp.irap.cassis.gui.plot.infopanel.InfoModel;
import eu.omp.irap.cassis.gui.plot.infopanel.InfoPanelConstants;
import eu.omp.irap.cassis.gui.plot.line.CassisPlot;
import eu.omp.irap.cassis.gui.plot.simple.SpectrumPlot;
import eu.omp.irap.cassis.gui.plot.util.StackMosaicPanel;


@SuppressWarnings("serial")
public class MultiSpectrumView extends AbstractMozaicPlotView {

	public MultiSpectrumView() {
		this(new MultiSpectrumControl(new MultiSpectrumModel()));
	}

	public MultiSpectrumView(MultiSpectrumControl control) {
		super(control);
		control.setView(this);
	}

	@Override
	public JComboBox<String> getComboBottomAxisType() {
		if (comboBottomAxisType == null) {
			comboBottomAxisType = new JComboBox<>(XAxisCassis.getAllXAxisCassisType());
			comboBottomAxisType.setSelectedItem(XAxisCassisUtil.getType(XAxisCassis.getXAxisFrequency(UNIT.GHZ)));
			comboBottomAxisType.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					XAxisCassisUtil.changeXAxisAccordingToType(comboBottomAxisType.getSelectedItem().toString(), getComboBottomtAxis());
				}
			});
		}
		return comboBottomAxisType;
	}

	@Override
	public JComboBox<XAxisCassis> getComboBottomtAxis() {
		if (comboBottomtAxis == null) {
			comboBottomtAxis = new JComboBox<>(XAxisCassis.getAllXAxisForType(getComboBottomAxisType().getSelectedItem().toString()));
			comboBottomtAxis.setSelectedItem(XAxisCassis.getXAxisFrequency(UNIT.GHZ));
			comboBottomtAxis.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.comboBottomAxisChanged();
					XAxisCassisUtil.changeType(comboBottomtAxis, getComboBottomAxisType());
				}
			});
		}
		return comboBottomtAxis;
	}

	@Override
	protected JPanelCurve getFilePanelCurve(CassisPlot cassisPlot) {
		CurvePanelView curvePanelData = new CurvePanelView(new CurvePanelModel(cassisPlot.getFileSpectrumSeries()));
		JPanelCurve jPanelCurve = new JPanelCurve(InfoPanelConstants.LOOMIS_SPECTRUM_TITLE, false);
		curvePanelData.addCurveCassisListener(control);
		jPanelCurve.addCurvePane(curvePanelData);

		final XYErrorSeries errorFileSeries = cassisPlot.getErrorFileSeries();
		if (errorFileSeries != null) {
			CurvePanelView curvePanelError = new CurvePanelView(new CurvePanelModel(errorFileSeries));
			curvePanelError.addCurveCassisListener(control);
			jPanelCurve.addCurvePane(curvePanelError);
		}

		final XYLineSeriesCassis fileLineSeriesCassis = cassisPlot.getFileLineSeriesCassis();
		if (fileLineSeriesCassis != null) {
			CurvePanelView curvePanelLineData = new CurvePanelView(
					new CurvePanelModel(fileLineSeriesCassis));
			curvePanelLineData.addCurveCassisListener(control);
			jPanelCurve.addCurvePane(curvePanelLineData);
		}

		final XYLineSeriesCassis surroundSeries = cassisPlot.getSurroundSeries();
		if (surroundSeries != null) {
			CurvePanelView curvePanelNearbyLine = new CurvePanelView(new CurvePanelModel(surroundSeries));
			curvePanelNearbyLine.addCurveCassisListener(control);
			jPanelCurve.addCurvePane(curvePanelNearbyLine);
		}

		return jPanelCurve;
	}

	@Override
	public void displayResult(CassisResult result, boolean newDisplay) {
		if (result instanceof LoomisWoodResult) {
			LoomisWoodResult lineResult = (LoomisWoodResult) result;
			XAxisCassis topAxis = lineResult.getAxisData();
			if (newDisplay) {
				getLineConfigCurve().clear();
			}
			messageControl.removeAllMessages();

			model.setCassisPlots(lineResult, getLineConfigCurve());

			createListInfoModels();
			tabManager.setSelectedIndex(0);
			createFitPanelList();
			initStackMosaicPanel();
			initOtherSpeciesPanel();


			if (lineResult.getModelIdentifiedInterface() != null)
				control.getSpeciesControl().initThresholdModel(
						lineResult.getModelIdentifiedInterface().getThresholdModel());

			model.initGalleryNavigation();

			selectMosaicPanel();

			GalleryNavigationModel galNavModel = model.getGalleryNavigationModel();
			showBy(GalleryOption.NONE, galNavModel.getFilter(), 0);

			switchBack();
			galNavModel.setCurrentGallery(1);

			// Switch to the triple plot if just one plot
			if (model.getListCassisPlots().size() == 1) {
				SpectrumPlot sp = gallerySortPane.getListSpectrumPlots().get(0);
				switchTo(sp.getRowId(), sp.getColId());
			}

			getLineConfigCurve().clear();
			checkColorFromLineConfigCurveToInfoPanel();

			getGalleryNavigation().refreshSorting();

			if (newDisplay) {
				model.setXAxisCassisTop(topAxis);
				yAxisRigthComboBox.setSelectedItem(model.getYAxisCassis());
				yAxisLeftComboBox.setSelectedItem(model.getYAxisCassis());

				getComboTopAxisType().setSelectedItem(XAxisCassisUtil.getType(topAxis));
				getComboTopAxis().setSelectedItem(topAxis);
			}

			control.yAxisLeftChanged();
			control.comboBottomAxisChanged();
			control.comboTopAxisChanged();
		}
	}

	@Override
	public MultiSpectrumControl getControl() {
		return (MultiSpectrumControl) control;
	}

	@Override
	public String getHelpUrl() {
		// TODO, return something?
		return "";
	}

	@Override
	public String getTitle() {
		return "Loomis Spectrum";
	}

	@Override
	protected InfoModel createInfoModel(CassisPlot cassisPlot) {
		InfoModel im = super.createInfoModel(cassisPlot);
		if (cassisPlot.getLoomisLine() != null && cassisPlot.getLoomisLine().getSeriesCount() > 0) {
			JPanelCurve loomisLinesPanelCurve = new JPanelCurve(
					InfoPanelConstants.LOOMIS_LINES_TITLE, false);
			int nbLoomisLine = cassisPlot.getLoomisLine().getSeriesCount();
			for (int i = 0; i < nbLoomisLine; i++) {
				CurvePanelView cpv = new CurvePanelView(new CurvePanelModel(
						cassisPlot.getLoomisLine().getSeries(i)));
				cpv.addCurveCassisListener(control);
				loomisLinesPanelCurve.addCurvePane(cpv);
			}
			im.addPanelCurve(loomisLinesPanelCurve);
		}
		return im;
	}

	@Override
	public StackMosaicPanel getStackMosaicPanel() {
		if (stackMosaicPanel == null) {
			stackMosaicPanel = super.getStackMosaicPanel();
			stackMosaicPanel.getSortingComboBox().setEnabled(false);
		}

		return stackMosaicPanel;
	}
}
