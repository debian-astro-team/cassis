/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.tools;

import java.awt.Color;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.UtilArrayList;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;

public class ToolsYScale {

	private static final Logger LOGGER = LoggerFactory.getLogger(ToolsYScale.class);


	/**
	 * Create a new {@link XYSpectrumSeries} by doing a change Tmb/Ta change.
	 *
	 * @param serie The {@link XYSpectrumSeries} used a base for the new series after the operation.
	 * @param operation The Operation to do, must be {@link Operation#TA_TO_TMB} or {@link Operation#TMB_TO_TA}.
	 * @param name The name of the new {@link XYSpectrumSeries}.
	 * @param color The color of the new series.
	 * @param efficiencyList The efficiency list of the telescope.
	 * @param frequencyList The frequency list of the telescope.
	 * @return The created {@link XYSpectrumSeries} or null if there was an error.
	 */
	public static XYSpectrumSeries doTmbTaChange(XYSpectrumSeries serie,
			Operation operation, String name, Color color, List<Double> efficiencyList,
			List<Double> frequencyList) {
		if (!(operation == Operation.TA_TO_TMB || operation == Operation.TMB_TO_TA)
				|| serie == null || efficiencyList == null || efficiencyList.isEmpty()
				|| frequencyList == null || frequencyList.isEmpty()) {
			return null;
		}
		return ToolsUtil.createNewSerie(serie, doTmbTaChange(serie.getSpectrum(), operation,
				efficiencyList, frequencyList), name, color);
	}

	/**
	 * Do a ta to tmb operation.
	 *
	 * @param efficiencyList The efficiency list of the telescope.
	 * @param frequencyList The frequency list of the telescope.
	 * @param frequencies The frequencies.
	 * @param intensities The intensities.
	 * @return the array of the new intensities after the ta to tmb operation.
	 */
	private static double[] doTaToTmb(List<Double> efficiencyList,
			List<Double> frequencyList, double[] frequencies, double[] intensities) {
		double[] newIntensities = new double[intensities.length];
		double efficiency;
		for (int i = 0; i < intensities.length; i++) {
			efficiency = UtilArrayList.getArrayValueAt(frequencies[i], efficiencyList,
					frequencyList);
			newIntensities[i] = intensities[i] / efficiency;
		}
		return newIntensities;
	}

	/**
	 * Do a tmb to ta operation.
	 *
	 * @param efficiencyList The efficiency list of the telescope.
	 * @param frequencyList The frequency list of the telescope.
	 * @param frequencies The frequencies.
	 * @param intensities The intensities.
	 * @return the array of the new intensities after the tmb to ta operation.
	 */
	private static double[] doTmbToTa(List<Double> efficiencyList,
			List<Double> frequencyList, double[] frequencies, double[] intensities) {
		double[] newIntensities = new double[intensities.length];
		double efficiency;
		for (int i = 0; i < intensities.length; i++) {
			efficiency = UtilArrayList.getArrayValueAt(frequencies[i], efficiencyList,
					frequencyList);
			newIntensities[i] = intensities[i] * efficiency;
		}
		return newIntensities;
	}

	/**
	 * Create a new {@link CommentedSpectrum} by doing a change Tmb/Ta change.
	 *
	 * @param cs The {@link CommentedSpectrum} used a base for the new series after the operation.
	 * @param operation The Operation to do, must be {@link Operation#TA_TO_TMB} or {@link Operation#TMB_TO_TA}.
	 * @param efficiencyList The efficiency list of the telescope.
	 * @param frequencyList The frequency list of the telescope.
	 * @return The created {@link CommentedSpectrum} or null if there was an error.
	 */
	public static CommentedSpectrum doTmbTaChange(CommentedSpectrum cs,
			Operation operation, List<Double> efficiencyList,
			List<Double> frequencyList) {
		if (!(operation == Operation.TA_TO_TMB || operation == Operation.TMB_TO_TA)
				|| cs == null || efficiencyList == null || efficiencyList.isEmpty()
				|| frequencyList == null || frequencyList.isEmpty()) {
			return null;
		}

		double[] frequencies = cs.getFrequenciesSignal();
		double[] newFrequencies = new double[frequencies.length];
		System.arraycopy(frequencies, 0, newFrequencies, 0, frequencies.length);
		double[] intensities = cs.getIntensities();
		double[] newIntensities;

		try {
			if (operation == Operation.TMB_TO_TA) {
				newIntensities = doTmbToTa(efficiencyList, frequencyList, frequencies,
						intensities);
			} else {
				newIntensities = doTaToTmb(efficiencyList, frequencyList, frequencies,
						intensities);
			}
		} catch (IndexOutOfBoundsException ioobe) {
			LOGGER.error("Index error during doTmbTaChange", ioobe);
			return null;
		}
		return ToolsUtil.createNewCommentedSpectrum(cs, newFrequencies, newIntensities);
	}

}
