/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.tools;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import eu.omp.irap.cassis.common.TypeFrequency;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.events.ListenerManager;
import eu.omp.irap.cassis.gui.model.parameter.telescope.TelescopeModel;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import eu.omp.irap.cassis.gui.plot.curve.config.ColorsCurve;

/**
 * Model class for the Tools functionality when used on mosaic view.
 *
 * @author M. Boiziot
 */
public class ToolsMosaicModel extends ListenerManager implements ToolsModelsInterface {

	private ToolsMosaicInterface theInterface;
	private Operation actionSelected;
	private int mosaicResultSubtractId = 0;
	private int mosaicResultAddId = 0;
	private int mosaicResultDivideId = 0;
	private int mosaicResultAddConstantId = 0;
	private int mosaicResultMultiplyConstantId = 0;
	private int mosaicResultSmoothHenningId = 0;
	private int mosaicResultSmoothBoxId = 0;
	private int mosaicResultAverageId = 0;
	private int mosaicResultResampleAutomaticId = 0;
	private int mosaicResultResampleAdvancedId = 0;
	private int mosaicResultResampleGridId = 0;
	private int mosaicResultRestToSkyId = 0;
	private int mosaicResultSkyToRestId = 0;
	private int mosaicResultTmbId = 0;
	private int mosaicResultTaId = 0;
	private int mosaicResultSmoothGaussId = 0;
	private int mosaicResultVlsrId = 0;
	private int mosaicResultAirToVacuumId = 0;
	private int mosaicResultVacuumToAirId = 0;
	private int mosaicResultRedshiftId = 0;
	private static final boolean ALLOW_ADVANCED_PARAMETERS_FOR_TWO_SPECTRUM = false;
	private static final boolean ALLOW_ADVANCED_SMOOTH_HANNING = true;
	private boolean advancedSmoothHanning = false;
	private String selectedSpectrumOne;
	private String selectedSpectrumTwo;
	private String selectedSpectrumAll;
	private String selectedSpectrumAllSecond;
	private String selectedSpectrumRest;
	private String selectedSpectrumSky;
	private TelescopeModel telescopeModel;



	/**
	 * Constructor of the ToolsMosaicModel.
	 *
	 * @param anInterface The class who implements the {@link ToolsMosaicInterface}.
	 */
	public ToolsMosaicModel(ToolsMosaicInterface anInterface) {
		theInterface = anInterface;
		actionSelected = Operation.SUBTRACT;
		telescopeModel = new TelescopeModel();
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#getCurveOneName()
	 */
	@Override
	public List<String> getCurveOneName() {
		List<String> listReturn = new ArrayList<>();
		boolean toAdd;
		for (ToolsModel toolsModel : theInterface.getListToolsModel()) {
			List<String> toolsModelListCurveOne = toolsModel.getCurveOneName();
			for (String nameLocal : toolsModelListCurveOne) {
				toAdd = true;
				for (String nameOfListReturn : listReturn) {
					if (nameOfListReturn.equals(nameLocal)) {
						toAdd = false;
						break;
					}
				}
				if (toAdd)
					listReturn.add(nameLocal);
			}
		}
		if (selectedSpectrumOne == null && !listReturn.isEmpty()) {
			selectedSpectrumOne = listReturn.get(0);
		}
		return listReturn;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#getCurveTwoName()
	 */
	@Override
	public List<String> getCurveTwoName() {
		List<String> listReturn = new ArrayList<>();
		boolean toAdd;
		for (ToolsModel toolsModel : theInterface.getListToolsModel()) {
			List<String> toolsModelListCurveOne = toolsModel.getCurveTwoName();
			for (String nameLocal : toolsModelListCurveOne) {
				toAdd = true;
				for (String nameOfListReturn : listReturn) {
					if (nameOfListReturn.equals(nameLocal)) {
						toAdd = false;
						break;
					}
				}
				if (toAdd)
					listReturn.add(nameLocal);
			}
		}
		if (selectedSpectrumTwo == null && !listReturn.isEmpty()) {
			selectedSpectrumTwo = listReturn.get(0);
		}
		return listReturn;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#getCurveAllName()
	 */
	@Override
	public List<String> getCurveAllName() {
		List<String> listReturn = new ArrayList<>();
		boolean toAdd;
		for (ToolsModel toolsModel : theInterface.getListToolsModel()) {
			List<String> toolsModelListCurveOne = toolsModel.getCurveAllName();
			for (String nameLocal : toolsModelListCurveOne) {
				toAdd = true;
				for (String nameOfListReturn : listReturn) {
					if (nameOfListReturn.equals(nameLocal)) {
						toAdd = false;
						break;
					}
				}
				if (toAdd)
					listReturn.add(nameLocal);
			}
		}
		if (selectedSpectrumAll == null && !listReturn.isEmpty()) {
			selectedSpectrumAll = listReturn.get(0);
		}
		return listReturn;
	}

	/**
	 * Create a new serie by doing an operation between two series (ADD, SUBTRACT or DIVIDE).
	 *
	 * @param serieName The name of the first serie. (This is the serie whose data will be subtracted in case of SUBTRACT operation.)
	 * @param serieName2 The name of the second serie. (This is the serie used to subtract in case of SUBTRACT operation.)
	 * @param operation The operation to do. It <b>must</b> be {@link Operation#ADD}, {@link Operation#SUBTRACT} or {@link Operation#DIVIDE}.
	 * @param avoidResample Avoid resampling if possible.
	 * @return if the operation is successful.
	 * @throws ToolsException In case of error for the operation.
	 */
	@Override
	public boolean doOperation(String serieName, String serieName2, Operation operation, boolean avoidResample) throws ToolsException {
		String name = null;
		int id;
		if (operation == Operation.SUBTRACT) {
			mosaicResultSubtractId++;
			id = mosaicResultSubtractId;
			name = "Mosaic result subtract ";
		} else if (operation == Operation.ADD) {
			mosaicResultAddId++;
			id = mosaicResultAddId;
			name = "Mosaic result add ";
		} else if (operation == Operation.DIVIDE) {
			mosaicResultDivideId++;
			id = mosaicResultDivideId;
			name = "Mosaic result divide ";
		} else {
			return false;
		}

		boolean error = false;
		List<Color> colors = new ArrayList<>(3);
		colors.add(ColorsCurve.getNewColorResult());
		colors.add(ColorsCurve.getNewColorResult());
		colors.add(ColorsCurve.getNewColorResult());
		for (ToolsModel toolsModel : theInterface.getListToolsModel()) {
			if (!toolsModel.doOperation(serieName, serieName2, name, id, colors, operation, avoidResample))
				error = true;
		}
		theInterface.refreshInfoPanelGallery();

		return !error;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#getActionSelected()
	 */
	@Override
	public Operation getActionSelected() {
		return actionSelected;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#setActionSelected(eu.omp.irap.cassis.gui.plot.tools.Operation)
	 */
	@Override
	public void setActionSelected(Operation actionSelected) {
		this.actionSelected = actionSelected;
	}

	/**
	 * Reset the mosaicResultId to 0.
	 */
	public void resetId() {
		mosaicResultAddConstantId = 0;
		mosaicResultMultiplyConstantId = 0;
		mosaicResultSubtractId = 0;
		mosaicResultAddId = 0;
		mosaicResultSmoothHenningId = 0;
		mosaicResultSmoothBoxId = 0;
		mosaicResultAverageId = 0;
		mosaicResultResampleAutomaticId = 0;
		mosaicResultResampleAdvancedId = 0;
		mosaicResultResampleGridId = 0;
		mosaicResultRestToSkyId = 0;
		mosaicResultSkyToRestId = 0;
		mosaicResultDivideId = 0;
		mosaicResultTmbId = 0;
		mosaicResultTaId = 0;
		mosaicResultSmoothGaussId = 0;
		mosaicResultVlsrId = 0;
		mosaicResultAirToVacuumId = 0;
		mosaicResultVacuumToAirId = 0;
	}

	/**
	 * Create a new serie by doing an operation between a serie and a double value.
	 * (ADD_CONSTANT, MULTIPLY_CONSTANT, RESAMPLE_AUTOMATIC, VLSR or SMOOTH_GAUSS)
	 *
	 * @param serieName The name of the serie.
	 * @param value The double value.
	 * @param operation The operation to do. It <b>must</b> be {@link Operation#ADD_CONSTANT},
	 * {@link Operation#MULTIPLY_CONSTANT}, {@link Operation#RESAMPLE_AUTOMATIC}
	 * {@link Operation#VLSR}, {@link Operation#SKY_TO_REST} or {@link Operation#SMOOTH_GAUSS}.
	 * @return if the Operation of is successful.
	 * @throws ToolsException In case of error for the operation.
	 */
	@Override
	public boolean doOperationWithAValue(String serieName, double value,
			Operation operation) throws ToolsException {
		String name = null;
		if (operation == Operation.ADD_CONSTANT) {
			mosaicResultAddConstantId++;
			name = "Mosaic result addConstant " + mosaicResultAddConstantId;
		} else if (operation == Operation.MULTIPLY_CONSTANT) {
			mosaicResultMultiplyConstantId++;
			name = "Mosaic result multiplyConstant " + mosaicResultMultiplyConstantId;
		} else if (operation == Operation.RESAMPLE_AUTOMATIC) {
			mosaicResultResampleAutomaticId++;
			name = "Mosaic result automatic resample " + mosaicResultResampleAutomaticId;
		} else if (operation == Operation.SKY_TO_REST) {
			mosaicResultSkyToRestId++;
			name = "Mosaic result StR " + mosaicResultSkyToRestId;
		} else if (operation == Operation.SMOOTH_GAUSS) {
			mosaicResultSmoothGaussId++;
			name = "Mosaic result smooth gauss " + mosaicResultSmoothGaussId;
		} else if (operation == Operation.VLSR) {
			mosaicResultVlsrId++;
			name = "Mosaic result vlsr " + mosaicResultVlsrId;
		} else if (operation == Operation.REDSHIFT) {
			mosaicResultRedshiftId++;
			name = "Mosaic result redshift " + mosaicResultRedshiftId;
		}else {
			return false;
		}

		boolean error = false;
		Color color = ColorsCurve.getNewColorResult();
		for (ToolsModel toolsModel : theInterface.getListToolsModel()) {
			if (!toolsModel.doOperationWithAValue(serieName, value, name, color, operation))
				error = true;
		}
		theInterface.refreshInfoPanelGallery();

		return !error;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#smoothHanning(java.lang.String, int)
	 */
	@Override
	public boolean smoothHanning(String serieName, int iterationNumber) {
		mosaicResultSmoothHenningId++;
		boolean error = false;

		Color color = ColorsCurve.getNewColorResult();
		for (ToolsModel toolsModel : theInterface.getListToolsModel()) {
			if (!toolsModel.smoothHanning(serieName, "Mosaic result smooth hanning ", mosaicResultSmoothHenningId, iterationNumber, color))
				error = true;
		}
		theInterface.refreshInfoPanelGallery();

		return !error;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#isAdvancedParametersTwoSpectrumAllowed()
	 */
	@Override
	public boolean isAdvancedParametersTwoSpectrumAllowed() {
		return ALLOW_ADVANCED_PARAMETERS_FOR_TWO_SPECTRUM;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#isAdvancedSmoothHanningAllowed()
	 */
	@Override
	public boolean isAdvancedSmoothHanningAllowed() {
		return ALLOW_ADVANCED_SMOOTH_HANNING;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#getAdvancedSubtractState()
	 */
	@Override
	public boolean getAdvancedSubtractState() {
		return false;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#getAdvancedSmoothHanningState()
	 */
	@Override
	public boolean getAdvancedSmoothHanningState() {
		return advancedSmoothHanning;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#setAdvancedSmoothHanningState(boolean)
	 */
	@Override
	public void setAdvancedSmoothHanningState(boolean newState) {
		advancedSmoothHanning = newState;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#setAdvancedSubtractState(boolean)
	 */
	@Override
	public void setAdvancedSubtractState(boolean newState) {
		// Do nothing as this method should not be called.
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#doOperation(java.lang.String, java.lang.String, double, double, double, eu.omp.irap.cassis.gui.plot.tools.Operation, boolean)
	 */
	@Override
	public boolean doOperation(String serieName, String serie2Name, double xMin,
			double xMax, double sampling, Operation operation, boolean avoidResample) {
		return false;
	}

	/**
	 * Create a new serie by doing an operation with two series.
	 *
	 * @param serieName The name of the first serie.
	 * @param serie2Name The name of the second serie.
	 * @param operation The operation to do. It must be {@link Operation#AVERAGE_SPECTRA}
	 *  or {@link Operation#RESAMPLE_GRID} for now.
	 * @param avoidResample Avoid resampling if possible.
	 * @return if the operation is successful.
	 * @throws ToolsException In case of error for the operation.
	 */
	@Override
	public boolean doOperationWithTwoSpectra(String serieName, String serie2Name,
			Operation operation, boolean avoidResample) throws ToolsException {
		boolean error = false;
		Color color = ColorsCurve.getNewColorResult();
		String name;
		if (operation == Operation.AVERAGE_SPECTRA) {
			mosaicResultAverageId++;
			name = "Mosaic result average " + mosaicResultAverageId;
		} else if (operation == Operation.RESAMPLE_GRID) {
			mosaicResultResampleGridId++;
			name = "Mosaic result grid resample " + mosaicResultResampleGridId;
		} else {
			return false;
		}

		for (ToolsModel toolsModel : theInterface.getListToolsModel()) {
			if (!toolsModel.doOperationWithTwoSpectra(serieName, serie2Name,
					operation, name, color, avoidResample))
				error = true;
		}
		theInterface.refreshInfoPanelGallery();

		return !error;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#doOperationWithTwoSpectra(java.lang.String, java.lang.String, eu.omp.irap.cassis.gui.plot.tools.Operation, double, double, double, boolean)
	 */
	@Override
	public boolean doOperationWithTwoSpectra(String serieName, String serie2Name, Operation operation, double xMin, double xMax, double sampling, boolean avoidResample) {
		return false;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#getAllCurveOfType(eu.omp.irap.cassis.common.TypeFrequency)
	 */
	@Override
	public List<String> getAllCurveOfType(TypeFrequency type) {
		List<String> listReturn = new ArrayList<>();
		boolean toAdd;
		for (ToolsModel toolsModel : theInterface.getListToolsModel()) {
			List<String> toolsModelListCurveRest = toolsModel.getAllCurveOfType(type);
			for (String nameLocal : toolsModelListCurveRest) {
				toAdd = true;
				for (String nameOfListReturn : listReturn) {
					if (nameOfListReturn.equals(nameLocal)) {
						toAdd = false;
						break;
					}
				}
				if (toAdd)
					listReturn.add(nameLocal);
			}
		}
		if (type == TypeFrequency.REST && selectedSpectrumRest == null
				&& !listReturn.isEmpty()) {
			selectedSpectrumRest = listReturn.get(0);
		} else if (type == TypeFrequency.SKY && selectedSpectrumSky == null
				&& !listReturn.isEmpty()) {
			selectedSpectrumSky = listReturn.get(0);
		}
		return listReturn;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#doOperationWithOneSpectrum(java.lang.String, eu.omp.irap.cassis.gui.plot.tools.Operation)
	 */
	@Override
	public boolean doOperationWithOneSpectrum(String serieName, Operation operation) {
		String name = null;
		if (operation == Operation.REST_TO_SKY) {
			mosaicResultRestToSkyId++;
			name = "Mosaic result RtS " + mosaicResultRestToSkyId;
		} else if (operation == Operation.AIR_TO_VACUUM) {
			mosaicResultAirToVacuumId++;
			name = "Mosaic result AtV " + mosaicResultAirToVacuumId;
		} else if (operation == Operation.VACUUM_TO_AIR) {
			mosaicResultVacuumToAirId++;
			name = "Mosaic result VtA " + mosaicResultVacuumToAirId;
		} else {
			return false;
		}

		boolean error = false;
		Color color = ColorsCurve.getNewColorResult();
		for (ToolsModel toolsModel : theInterface.getListToolsModel()) {
			if (!toolsModel.doOperationWithOneSpectrum(serieName, name, color, operation))
				error = true;
		}
		theInterface.refreshInfoPanelGallery();

		return !error;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#getSelectedSpectrumOne()
	 */
	@Override
	public String getSelectedSpectrumOne() {
		return selectedSpectrumOne;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#setSelectedSpectrumOne(java.lang.String)
	 */
	@Override
	public void setSelectedSpectrumOne(String selectedSpectrumOne) {
		this.selectedSpectrumOne = selectedSpectrumOne;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#getSelectedSpectrumTwo()
	 */
	@Override
	public String getSelectedSpectrumTwo() {
		return selectedSpectrumTwo;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#setSelectedSpectrumTwo(java.lang.String)
	 */
	@Override
	public void setSelectedSpectrumTwo(String selectedSpectrumTwo) {
		this.selectedSpectrumTwo = selectedSpectrumTwo;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#getSelectedSpectrumAll()
	 */
	@Override
	public String getSelectedSpectrumAll() {
		return selectedSpectrumAll;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#setSelectedSpectrumAll(java.lang.String)
	 */
	@Override
	public void setSelectedSpectrumAll(String selectedSpectrumAll) {
		this.selectedSpectrumAll = selectedSpectrumAll;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#getSelectedSpectrumRest()
	 */
	@Override
	public String getSelectedSpectrumRest() {
		return selectedSpectrumRest;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#setSelectedSpectrumRest(java.lang.String)
	 */
	@Override
	public void setSelectedSpectrumRest(String selectedSpectrumRest) {
		this.selectedSpectrumRest = selectedSpectrumRest;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#getSelectedSpectrumSky()
	 */
	@Override
	public String getSelectedSpectrumSky() {
		return selectedSpectrumSky;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#setSelectedSpectrumSky(java.lang.String)
	 */
	@Override
	public void setSelectedSpectrumSky(String selectedSpectrumSky) {
		this.selectedSpectrumSky = selectedSpectrumSky;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#getTelescopeModel()
	 */
	@Override
	public TelescopeModel getTelescopeModel() {
		return telescopeModel;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#doTmbTaChange(java.lang.String, eu.omp.irap.cassis.gui.plot.tools.Operation, java.util.List, java.util.List)
	 */
	@Override
	public boolean doTmbTaChange(String serieName, Operation operation, List<Double> efficiencyList,
			List<Double> frequencyList) {
		String name;
		if (operation == Operation.TA_TO_TMB) {
			mosaicResultTaId++;
			name = "Mosaic result TA " + mosaicResultTaId;
		} else if (operation == Operation.TMB_TO_TA) {
			mosaicResultTmbId++;
			name = "Mosaic result TMB " + mosaicResultTmbId;
		} else {
			return false;
		}

		boolean error = false;
		Color color = ColorsCurve.getNewColorResult();
		for (ToolsModel toolsModel : theInterface.getListToolsModel()) {
			if (!toolsModel.doTmbTaChange(serieName, name, color, operation, efficiencyList, frequencyList)) {
				error = true;
			}
		}
		theInterface.refreshInfoPanelGallery();

		return !error;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#doSmoothBox(java.lang.String, int)
	 */
	@Override
	public boolean doSmoothBox(String serieName, int boxSize) {
		mosaicResultSmoothBoxId++;
		boolean error = false;

		String name = "Mosaic result smooth box " + mosaicResultSmoothBoxId;
		Color color = ColorsCurve.getNewColorResult();
		for (ToolsModel toolsModel : theInterface.getListToolsModel()) {
			if (!toolsModel.doSmoothBox(serieName, name, boxSize, color))
				error = true;
		}
		theInterface.refreshInfoPanelGallery();

		return !error;
	}

	@Override
	public ViewType getViewType() {
		return theInterface.getListToolsModel().get(0).getViewType();
	}

	@Override
	public XAxisCassis getXAxisCassis() {
		return theInterface.getListToolsModel().get(0).getXAxisCassis();
	}

	@Override
	public String getSelectedSpectrumAllSecond() {
		return selectedSpectrumAllSecond;
	}

	@Override
	public void setSelectedSpectrumAllSecond(String selectedSpectrumAllSecond) {
		this.selectedSpectrumAllSecond = selectedSpectrumAllSecond;
	}

	/**
	 * Create a new serie by doing an advanced resampling.
	 *
	 * @param serieName The name of the serie used for the operation.
	 * @param sampling The sampling used for the created {@link XYSpectrumSeries}
	 *  in the xAxis unit of the serie.
	 * @param initialSample The initial sample in the xAxis unit of the serie.
	 * @param finalSample The final sample in the xAxis unit of the serie.
	 * @return if the operation is successful.
	 * @throws ToolsException In case of error for the operation.
	 */
	@Override
	public boolean doResampleAdvanced(String serieName, double sampling,
			double initialSample, double finalSample) throws ToolsException {
		mosaicResultResampleAdvancedId++;
		boolean error = false;

		String name = "Mosaic result resample advanced " + mosaicResultResampleAdvancedId;
		Color color = ColorsCurve.getNewColorResult();
		for (ToolsModel toolsModel : theInterface.getListToolsModel()) {
			if (!toolsModel.doResampleAdvanced(serieName, sampling, initialSample, finalSample, name, color))
				error = true;
		}
		theInterface.refreshInfoPanelGallery();

		return !error;
	}
}
