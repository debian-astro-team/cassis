/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.tools;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.TypeFrequency;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.events.ListenerManager;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.gui.model.parameter.telescope.TelescopeModel;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import eu.omp.irap.cassis.gui.plot.curve.config.ColorsCurve;

/**
 * Model class for the Tools functionality when used for only one plot (= not on mosaic).
 *
 * @author M. Boiziot
 */
public class ToolsModel extends ListenerManager implements ToolsModelsInterface {

	public static final String REFRESH_TOOLS_EVENT = "refreshTools";
	private static final Logger LOGGER = LoggerFactory.getLogger(ToolsModel.class);
	private static final boolean ALLOW_ADVANCED_PARAMETERS_FOR_TWO_SPECTRUM = true;
	private static final boolean ALLOW_ADVANCED_SMOOTH_HANNING = true;
	private ToolsInterface theInterface;
	private Operation actionSelected;
	private int resultSubtractId = 0;
	private int resultAddId = 0;
	private int resultDivideId = 0;
	private int resultAddConstantId = 0;
	private int resultMultiplyConstantId = 0;
	private int resultSmoothHenningId = 0;
	private int resultSmoothBoxId = 0;
	private int resultSmoothGaussId = 0;
	private int resultAverageId = 0;
	private int resultResampleAutomaticId = 0;
	private int resultResampleGridId = 0;
	private int resultResampleAdvanced = 0;
	private int resultRestToSkyId = 0;
	private int resultSkyToRestId = 0;
	private int resultTmbId = 0;
	private int resultTaId = 0;
	private int resultVlsrId = 0;
	private int resultAirToVacuumId = 0;
	private int resultVacuumToAirId = 0;
	private int resultRedshiftId = 0;
	private int numPlot;
	private boolean advancedSmoothHanning = false;
	private boolean advancedSubtract = false;
	private String selectedSpectrumOne;
	private String selectedSpectrumTwo;
	private String selectedSpectrumAll;
	private String selectedSpectrumAllSecond;
	private String selectedSpectrumRest;
	private String selectedSpectrumSky;
	private TelescopeModel telescopeModel;


	/**
	 * Constructor of the ToolsModel.
	 * @param anInterface The class who implements the ToolsInterface.
	 * @param aNumPlot The number of the plot in the Mosaic.
	 */
	public ToolsModel(ToolsInterface anInterface, int aNumPlot) {
		theInterface = anInterface;
		numPlot = aNumPlot;
		actionSelected = Operation.SUBTRACT;
		telescopeModel = new TelescopeModel();
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#getCurveOneName()
	 */
	@Override
	public List<String> getCurveOneName() {
		List<String> listReturn = new ArrayList<>();
		List<XYSpectrumSeries> listSeries = theInterface.getDataForToolsSpectrumOne(numPlot);
		for (int i = 0; i < listSeries.size(); i++) {
			XYSpectrumSeries serie = listSeries.get(i);
			listReturn.add((String) serie.getKey());
		}
		if (selectedSpectrumOne == null && !listReturn.isEmpty()) {
			selectedSpectrumOne = listReturn.get(0);
		}
		return listReturn;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#getCurveTwoName()
	 */
	@Override
	public List<String> getCurveTwoName() {
		List<String> listReturn = new ArrayList<>();
		List<XYSpectrumSeries> listSeries = theInterface.getDataForToolsSpectrumTwo(numPlot);
		for (int i = 0; i < listSeries.size(); i++) {
			XYSpectrumSeries serie = listSeries.get(i);
			listReturn.add((String) serie.getKey());
		}
		if (selectedSpectrumTwo == null && !listReturn.isEmpty()) {
			selectedSpectrumTwo = listReturn.get(0);
		}
		return listReturn;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#getCurveAllName()
	 */
	@Override
	public List<String> getCurveAllName() {
		List<String> listReturn = new ArrayList<>();
		List<XYSpectrumSeries> listSeries = theInterface.getDataForToolsSpectrumAll(numPlot);
		for (int i = 0; i < listSeries.size(); i++) {
			XYSpectrumSeries serie = listSeries.get(i);
			listReturn.add((String) serie.getKey());
		}
		if (selectedSpectrumAll == null && !listReturn.isEmpty()) {
			selectedSpectrumAll = listReturn.get(0);
		}
		if (selectedSpectrumAllSecond == null && !listReturn.isEmpty()) {
			selectedSpectrumAllSecond = listReturn.get(0);
		}
		return listReturn;
	}

	/**
	 * Create a new {@link XYSpectrumSeries} by doing an operation between two
	 * {@link XYSpectrumSeries}, also add the two resampled series used.
	 *
	 * @param serie The first {@link XYSpectrumSeries} (whose data will be subtracted in case of SUBTRACT operation).
	 * @param serie2 The second {@link XYSpectrumSeries} (used to subtract in case of SUBTRACT operation).
	 * @param name The generic name of the created {@link XYSpectrumSeries}.
	 * @param number The number used for the news series.
	 * @param colors The Colors used for the created {@link XYSpectrumSeries}.
	 * @param xMin The value of the minimum x point in the xAxis unit of the series.
	 * @param xMax The value of the maximum x point in the xAxis unit of the series.
	 * @param sampling The sampling used for the created {@link XYSpectrumSeries} in the xAxis unit of the series.
	 * @param operation The operation to do. It <b>must</b> be {@link Operation#ADD}, {@link Operation#SUBTRACT} or {@link Operation.DIVIDE}.
	 * @param avoidResample Avoid resampling if possible.
	 * @return If the operation has been successfully completed.
	 * @throws ToolsException In case of error for the operation.
	 */
	private boolean doOperation(XYSpectrumSeries serie, XYSpectrumSeries serie2,
			String name, int number, List<Color> colors, double xMin, double xMax,
			double sampling, Operation operation, boolean avoidResample) throws ToolsException {
		List<XYSpectrumSeries> listNewsSeries = ToolsUtil.doOperation(serie, serie2,
				name, number, colors, xMin, xMax, sampling, operation, avoidResample);
		if (listNewsSeries == null || listNewsSeries.size() != 3) {
			return false;
		}

		// Do not display the two originals resampled series.
		listNewsSeries.get(0).getConfigCurve().setVisible(false);
		listNewsSeries.get(1).getConfigCurve().setVisible(false);

		theInterface.addResult(listNewsSeries.get(0), numPlot, false);
		theInterface.addResult(listNewsSeries.get(1), numPlot, false);
		theInterface.addResult(listNewsSeries.get(2), numPlot, true);
		fireDataChanged(new ModelChangedEvent(REFRESH_TOOLS_EVENT));

		return true;
	}

	/**
	 * Compute the xMin, xMax and lowest sampling from the given {@link XYSpectrumSeries} then try to
	 * create a new {@link XYSpectrumSeries} by doing an operation (SUBTRACT, ADD or DIVIDE).
	 *
	 * @param serie The first {@link XYSpectrumSeries} (whose data will be subtracted in case of SUBTRACT operation).
	 * @param serie2 The second {@link XYSpectrumSeries} (used to subtract in case of SUBTRACT operation).
	 * @param name The generic name of the created {@link XYSpectrumSeries}.
	 * @param number The number used for the news series.
	 * @param colors The Colors used for the three new {@link XYSpectrumSeries}.
	 * @param operation The operation to do. It <b>must</b> be  {@link Operation#ADD}, {@link Operation#SUBTRACT} or {@link Operation.DIVIDE}.
	 * @param avoidResample Avoid resampling if possible.
	 * @return If the operation has been successfully completed.
	 * @throws ToolsException In case of error for the operation.
	 */
	private boolean doOperation(XYSpectrumSeries serie, XYSpectrumSeries serie2,
			String name, int number, List<Color> colors, Operation operation,
			boolean avoidResample) throws ToolsException {
		// Get xMin/xMax
		double xMin = ToolsUtil.getXMin(serie, serie2);
		double xMax = ToolsUtil.getXMax(serie, serie2);

		// Get the sampling = the lowest sampling
		double sampling = ToolsUtil.getLowestSampling(serie, serie2);

		if (Double.MAX_VALUE == sampling) {
			sampling = Double.NaN;
		}
		return doOperation(serie, serie2, name, number, colors, xMin, xMax,
				sampling, operation, avoidResample);
	}

	/**
	 * Create a new serie by doing an operation between two series (ADD, SUBTRACT or DIVIDE).
	 *
	 * @param serieName The name of the first serie. (This is the serie whose data will be subtracted in case of SUBTRACT operation.)
	 * @param serieName2 The name of the second serie. (This is the serie used to subtract in case of SUBTRACT operation.)
	 * @param operation The operation to do. It <b>must</b> be {@link Operation#ADD}, {@link Operation#SUBTRACT} or {@link Operation#DIVIDE}.
	 * @param avoidResample Avoid resampling if possible.
	 * @return if the operation is successful.
	 * @throws ToolsException In case of error for the operation.
	 */
	@Override
	public boolean doOperation(String serieName, String serie2Name,
			Operation operation, boolean avoidResample) throws ToolsException {
		String name;
		int id;
		if (operation == Operation.SUBTRACT) {
			resultSubtractId++;
			id = resultSubtractId;
			name = "Result subtract ";
		} else if (operation == Operation.ADD) {
			resultAddId++;
			id = resultAddId;
			name = "Result add ";
		} else if (operation == Operation.DIVIDE) {
			resultDivideId++;
			id = resultDivideId;
			name = "Result divide ";
		} else {
			return false;
		}

		return doOperation(serieName, serie2Name, name, id, null, operation, avoidResample);
	}

	/**
	 * Retrieves {@link XYSpectrumSeries} corresponding to series names given then call
	 * {@link ToolsModel#doOperation(XYSpectrumSeries, XYSpectrumSeries, String, int, List, Operation)}.
	 *
	 * @param serieName The name of the first {@link XYSpectrumSeries} (whose data will be subtracted in case of SUBTRACT operation).
	 * @param serie2Name The name of the second {@link XYSpectrumSeries} (used to subtract in case of SUBTRACT operation).
	 * @param nameNewCurve The generic name of the created {@link XYSpectrumSeries}.
	 * @param number The number used for the news series.
	 * @param newColors The Colors used for the three new {@link XYSpectrumSeries}.
	 * @param operation The operation to do. It <b>must</b> be {@link Operation#ADD}, {@link Operation#SUBTRACT} or {@link Operation#DIVIDE}.
	 * @param avoidResample Avoid resampling if possible.
	 * @return If the operation has been successfully completed.
	 * @throws ToolsException In case of error for the operation.
	 */
	public boolean doOperation(String serieName, String serie2Name,
			String nameNewCurve, int number, List<Color> newColors,
			Operation operation, boolean avoidResample) throws ToolsException {
		XYSpectrumSeries serie = getCurveByName(3, serieName);
		XYSpectrumSeries serie2 = getCurveByName(3, serie2Name);
		if (serie != null && serie2 != null) {
			return doOperation(serie, serie2, nameNewCurve, number, newColors, operation, avoidResample);
		}
		return false;
	}

	/**
	 * Retrieves {@link XYSpectrumSeries} corresponding to series names given then call
	 * {@link ToolsModel#doOperation(XYSpectrumSeries, XYSpectrumSeries, String, int, List, double, double, double, Operation)}.
	 *
	 * @param serieName The name of the first {@link XYSpectrumSeries}.
	 * @param serie2Name The name of the second {@link XYSpectrumSeries}.
	 * @param nameNewCurve The generic name of the created {@link XYSpectrumSeries}.
	 * @param number The number used for the news series.
	 * @param colors The Colors used for the three new {@link XYSpectrumSeries}.
	 * @param xMin The value of the minimum x point in the xAxis unit of the series.
	 * @param xMax The value of the maximum x point in the xAxis unit of the series.
	 * @param sampling The sampling used for the operation and the new serie in the xAxis unit of the series.
	 * @param operation The operation to do. It <b>must</b> be {@link Operation#ADD}, {@link Operation#SUBTRACT} or {@link Operation.DIVIDE}.
	 * @param avoidResample Avoid resampling if possible.
	 * @return If the operation has been successfully completed.
	 * @throws ToolsException In case of error for the operation.
	 */
	private boolean doOperation(String serieName, String serie2Name,
			String nameNewCurve, int number, List<Color> colors, double xMin,
			double xMax, double sampling, Operation operation, boolean avoidResample) throws ToolsException {
		XYSpectrumSeries serie = getCurveByName(1, serieName);
		XYSpectrumSeries serie2 = getCurveByName(2, serie2Name);
		if (serie != null && serie2 != null) {
			return doOperation(serie, serie2, nameNewCurve, number, colors,
					xMin, xMax, sampling, operation, avoidResample);
		}
		return false;
	}

	/**
	 * Get an {@link XYSpectrumSeries} by his name.
	 *
	 * @param numList The number of the method where we have to look for the {@link XYSpectrumSeries}.
	 * @param name The name of the {@link XYSpectrumSeries}.
	 * @return The {@link XYSpectrumSeries} found or null.
	 */
	public XYSpectrumSeries getCurveByName(int numList, String name) {
		List<XYSpectrumSeries> listSeries;
		switch (numList) {
		case 1:
			listSeries = theInterface.getDataForToolsSpectrumOne(numPlot);
			break;

		case 2:
			listSeries = theInterface.getDataForToolsSpectrumTwo(numPlot);
			break;

		case 3:
			listSeries = theInterface.getDataForToolsSpectrumAll(numPlot);
			break;

		default:
			return null;
		}

		XYSpectrumSeries serie;
		for (int i = 0; i < listSeries.size(); i++) {
			serie = listSeries.get(i);

			if (serie.getKey().equals(name)) {
				return serie;
			}
		}
		return null;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#getActionSelected()
	 */
	@Override
	public Operation getActionSelected() {
		return actionSelected;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#setActionSelected(eu.omp.irap.cassis.gui.plot.tools.Operation)
	 */
	@Override
	public void setActionSelected(Operation actionSelected) {
		this.actionSelected = actionSelected;
	}

	/**
	 * Create a new serie by doing an operation between a serie and a double value.
	 * (ADD_CONSTANT, MULTIPLY_CONSTANT, RESAMPLE_AUTOMATIC, VLSR or SMOOTH_GAUSS)
	 *
	 * @param serieName The name of the serie.
	 * @param value The double value.
	 * @param operation The operation to do. It <b>must</b> be {@link Operation#ADD_CONSTANT},
	 * {@link Operation#MULTIPLY_CONSTANT}, {@link Operation#RESAMPLE_AUTOMATIC}
	 * {@link Operation#VLSR}, {@link Operation#SKY_TO_REST} or {@link Operation#SMOOTH_GAUSS}.
	 * @return if the Operation of is successful.
	 * @throws ToolsException In case of error for the operation.
	 */
	@Override
	public boolean doOperationWithAValue(String serieName, double value, Operation operation) throws ToolsException {
		String name = null;
		if (operation == Operation.ADD_CONSTANT) {
			resultAddConstantId++;
			name = "Result addConstant " + resultAddConstantId;
		} else if (operation == Operation.MULTIPLY_CONSTANT) {
			resultMultiplyConstantId++;
			name = "Result multiplyConstant " + resultMultiplyConstantId;
		} else if (operation == Operation.RESAMPLE_AUTOMATIC) {
			resultResampleAutomaticId++;
			name = "Result automatic resample " + resultResampleAutomaticId;
		} else if (operation == Operation.SKY_TO_REST) {
			resultSkyToRestId++;
			name = "Result StR " + resultSkyToRestId;
		} else if (operation == Operation.VLSR) {
			resultVlsrId++;
			name = "Result vlsr " + resultVlsrId;
		} else if (operation == Operation.SMOOTH_GAUSS) {
			resultSmoothGaussId++;
			name = "Result smooth gauss " + resultSmoothGaussId;
		} else if (operation == Operation.REDSHIFT) {
			resultRedshiftId++;
			name = "Result redshift " + resultRedshiftId;
		} else {
			return false;
		}

		return doOperationWithAValue(serieName, value, name, null, operation);
	}

	/**
	 * Retrieves {@link XYSpectrumSeries} corresponding to serie names given then call
	 * {@link ToolsModel#doOperationWithAValue(XYSpectrumSeries, double, String, Color, Operation)}}
	 *
	 * @param serieName The name of the {@link XYSpectrumSeries}.
	 * @param value The double used for the operation.
	 * @param nameNewCurve The name of the created {@link XYSpectrumSeries}.
	 * @param newColor The Color used for the new {@link XYSpectrumSeries}.
	 * @param operation The operation to do. It <b>must</b> be {@link Operation#ADD_CONSTANT},
	 * {@link Operation#MULTIPLY_CONSTANT}, {@link Operation#RESAMPLE},
	 * {@link Operation#VLSR} or {@link Operation#SKY_TO_REST}.
	 * @return If the operation has been successfully completed.
	 * @throws ToolsException In case of error for the operation.
	 */
	public boolean doOperationWithAValue(String serieName, double value, String nameNewCurve, Color newColor, Operation operation) throws ToolsException {
		XYSpectrumSeries serie = getCurveByName(3, serieName);
		if (serie != null) {
			return doOperationWithAValue(serie, value, nameNewCurve, newColor, operation);
		}
		return false;
	}

	/**
	 * Add a new {@link XYSpectrumSeries} by doing an operation between a {@link XYSpectrumSeries} and a double.
	 *
	 * @param serie The {@link XYSpectrumSeries}.
	 * @param value The double used for the operation.
	 * @param name The name of the created {@link XYSpectrumSeries}.
	 * @param colorSpectrum The Color used for the created {@link XYSpectrumSeries}.
	 * @param operation The operation to do. It <b>must</b> be
	 *  {@link Operation#ADD_CONSTANT}, {@link Operation#MULTIPLY_CONSTANT},
	 *  {@link Operation#RESAMPLE}, {@link Operation#VLSR},
	 *  {@link Operation#SKY_TO_REST} or {@link Operation#REDSHIFT}.
	 * @return If the operation has been successfully completed.
	 * @throws ToolsException In case of error for the operation.
	 */
	private boolean doOperationWithAValue(XYSpectrumSeries serie, double value, String name, Color colorSpectrum, Operation operation) throws ToolsException {
		XYSpectrumSeries newSerie = null;
		if (operation == Operation.ADD_CONSTANT || operation == Operation.MULTIPLY_CONSTANT) {
			newSerie = ToolsUtil.doOperationWithConstant(serie, value, name, colorSpectrum, operation);
		} else if (operation == Operation.RESAMPLE_AUTOMATIC) {
			newSerie = ToolsUtil.doResample(serie, value, name, colorSpectrum);
		} else if (operation == Operation.SKY_TO_REST) {
			newSerie = ToolsUtil.doSkyToRest(serie, value, name, colorSpectrum);
		} else if (operation == Operation.VLSR) {
			newSerie = ToolsUtil.doVlsrChange(serie, name, colorSpectrum, value);
		} else if (operation == Operation.SMOOTH_GAUSS) {
			newSerie = ToolsUtil.doSmoothGauss(serie, name, colorSpectrum, value);
		} else if (operation == Operation.REDSHIFT) {
			newSerie = ToolsUtil.doRedshift(serie, name, colorSpectrum, value);
		}

		if (newSerie == null) {
			return false;
		}

		theInterface.addResult(newSerie, numPlot, true);
		fireDataChanged(new ModelChangedEvent(REFRESH_TOOLS_EVENT));
		return true;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#smoothHanning(java.lang.String, int)
	 */
	@Override
	public boolean smoothHanning(String serieName, int iterationNumber) {
		if (serieName == null)
			return false;

		resultSmoothHenningId++;

		return smoothHanning(serieName, "Result smooth hanning ", resultSmoothHenningId, iterationNumber, null);
	}

	/**
	 * Retrieves {@link XYSpectrumSeries} corresponding to serie name given then call
	 * {@link ToolsModel#smoothHanning(XYSpectrumSeries, String, int, int, Color, boolean)}.
	 *
	 * @param serieName The name of the {@link XYSpectrumSeries} who will be smoothed.
	 * @param genericName The generic name for the created serie (which is the name without the number).
	 * @param actualNumber The current id of Smooth Hanning.
	 * @param iterationNumber The number of iterations.
	 * @param color The color used for the serie.
	 * @return If the operation has been successfully completed.
	 */
	public boolean smoothHanning(String serieName, String genericName, int actualNumber, int iterationNumber, Color color) {
		XYSpectrumSeries serie = getCurveByName(3, serieName);
		if (serie != null) {
			return smoothHanning(serie, genericName, actualNumber, iterationNumber, color, true);
		}
		return false;
	}

	/**
	 * Add a new {@link XYSpectrumSeries} by doing a Smooth Hanning on a given {@link XYSpectrumSeries}.
	 *
	 * @param serie The {@link XYSpectrumSeries} used for doing the first Smooth Hanning.
	 * @param genericName The generic name for the created serie (which is the name without the number).
	 * @param actualNumber The current id of Smooth Hanning.
	 * @param iterationNumber The number of iterations.
	 * @param color The color used for the serie.
	 * @param refresh If it needed to perform a refresh.
	 * @return If the operation has been successfully completed.
	 */
	private boolean smoothHanning(XYSpectrumSeries serie, String genericName, int actualNumber, int iterationNumber, Color color, boolean refresh) {
		int nbItemInSerie = serie.getSpectrum().getFrequenciesSignal().length;
		if (nbItemInSerie <= 6) {
			LOGGER.error("Not enought point to do a smoothHanning. Exiting.");
			return false;
		}

		iterationNumber -= 1;
		String name = genericName + actualNumber;

		XYSpectrumSeries newSerie = ToolsUtil.doSmoothHanning(serie, name, color);
		if (newSerie == null) {
			return false;
		}

		if (iterationNumber == 0) {
			theInterface.addResult(newSerie, numPlot, true);
		}

		boolean returnValue = true;
		if (iterationNumber >= 1) {
			returnValue = smoothHanning(newSerie, genericName, actualNumber, iterationNumber, color, false);
		}

		if (refresh) {
			fireDataChanged(new ModelChangedEvent(REFRESH_TOOLS_EVENT));
		}
		return returnValue;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#isAdvancedParametersTwoSpectrumAllowed()
	 */
	@Override
	public boolean isAdvancedParametersTwoSpectrumAllowed() {
		return ALLOW_ADVANCED_PARAMETERS_FOR_TWO_SPECTRUM;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#isAdvancedSmoothHanningAllowed()
	 */
	@Override
	public boolean isAdvancedSmoothHanningAllowed() {
		return ALLOW_ADVANCED_SMOOTH_HANNING;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#getAdvancedSubtractState()
	 */
	@Override
	public boolean getAdvancedSubtractState() {
		return advancedSubtract;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#getAdvancedSmoothHanningState()
	 */
	@Override
	public boolean getAdvancedSmoothHanningState() {
		return advancedSmoothHanning;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#setAdvancedSmoothHanningState(boolean)
	 */
	@Override
	public void setAdvancedSmoothHanningState(boolean newState) {
		advancedSmoothHanning = newState;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#setAdvancedSubtractState(boolean)
	 */
	@Override
	public void setAdvancedSubtractState(boolean newState) {
		advancedSubtract = newState;
	}

	/**
	 * Create a new serie by doing an operation between two series (ADD, SUBTRACT or DIVIDE).
	 *
	 * @param serieName The name of the first serie. (This is the serie whose data will be subtracted in case of SUBTRACT operation.)
	 * @param serie2Name The name of the second serie. (This is the serie used to subtract in case of SUBTRACT operation.)
	 * @param xMin The value of the minimum x point in the xAxis unit of the series.
	 * @param xMax The value of the maximum x point in the xAxis unit of the series.
	 * @param sampling The sampling used for the created {@link XYSpectrumSeries} in the xAxis unit of the series.
	 * @param operation The operation to do. It <b>must</b> be {@link Operation#ADD}, {@link Operation#SUBTRACT} or {@link Operation#DIVIDE}.
	 * @param avoidResample Avoid resampling if possible.
	 * @return if the operation is successful.
	 * @throws ToolsException In case of error for the operation.
	 */
	@Override
	public boolean doOperation(String serieName, String serie2Name, double xMin,
			double xMax, double sampling, Operation operation, boolean avoidResample) throws ToolsException {
		String name;
		int id;
		if (operation == Operation.SUBTRACT) {
			resultSubtractId++;
			id = resultSubtractId;
			name = "Result subtract ";
		} else if (operation == Operation.ADD) {
			resultAddId++;
			id = resultAddId;
			name = "Result add ";
		} else if (operation == Operation.DIVIDE) {
			resultDivideId++;
			id = resultDivideId;
			name = "Result divide ";
		} else {
			return false;
		}
		return doOperation(serieName, serie2Name, name, id, null, xMin, xMax, sampling, operation, avoidResample);
	}

	/**
	 * Create a new serie by doing an operation with two series.
	 *
	 * @param serieName The name of the first serie.
	 * @param serie2Name The name of the second serie.
	 * @param operation The operation to do. It must be {@link Operation#AVERAGE_SPECTRA}
	 *  or {@link Operation#RESAMPLE_GRID} for now.
	 * @param avoidResample Avoid resampling if possible.
	 * @return if the operation is successful.
	 * @throws ToolsException In case of error for the operation.
	 */
	@Override
	public boolean doOperationWithTwoSpectra(String serieName, String serie2Name, Operation operation, boolean avoidResample) throws ToolsException {
		String name = null;
		if (operation == Operation.AVERAGE_SPECTRA) {
			resultAverageId++;
			name = "Result average " + resultAverageId;
		} else if (operation == Operation.RESAMPLE_GRID) {
			resultResampleGridId++;
			name = "Result resample grid " + resultResampleGridId;
		} else {
			return false;
		}
		return doOperationWithTwoSpectra(serieName, serie2Name, operation, name, null, avoidResample);
	}

	/**
	 * Do an operation with two spectra.
	 *
	 * @param serieName The name of the first serie.
	 * @param serie2Name The name of the second serie.
	 * @param operation The operation to do.
	 * @param nameNewCurve The name of the created {@link XYSpectrumSeries}.
	 * @param color The Color used for the new {@link XYSpectrumSeries}.
	 * @param avoidResample Avoid resampling if possible.
	 * @return If the operation has been successfully completed.
	 * @throws ToolsException In case of error for the operation.
	 */
	public boolean doOperationWithTwoSpectra(String serieName, String serie2Name,
			Operation operation, String nameNewCurve, Color color, boolean avoidResample) throws ToolsException {
		XYSpectrumSeries serie = getCurveByName(3, serieName);
		XYSpectrumSeries serie2;
		if (operation == Operation.AVERAGE_SPECTRA) {
			serie2 = getCurveByName(2, serie2Name);
		} else if (operation == Operation.RESAMPLE_GRID) {
			serie2 = getCurveByName(3, serie2Name);
		} else {
			return false;
		}

		if (serie == null || serie2 == null) {
			return false;
		}
		if (operation == Operation.AVERAGE_SPECTRA) {
			return doAverage(serie, serie2, nameNewCurve, color, avoidResample);
		} else if (operation == Operation.RESAMPLE_GRID) {
			return doResampleGrid(serie, serie2, nameNewCurve, color);
		}
		return false;
	}

	/**
	 * Compute the xMin, xMax and lowest sampling from the given {@link XYSpectrumSeries} then try to
	 * create a new {@link XYSpectrumSeries} by adding the values of two serie then dividing them by 2.
	 *
	 * @param serie The first {@link XYSpectrumSeries}.
	 * @param serie2 The second {@link XYSpectrumSeries}.
	 * @param nameNewCurve The generic name of the created {@link XYSpectrumSeries}.
	 * @param number The number used for the new serie.
	 * @param color The Color used for the new {@link XYSpectrumSeries}.
	 * @param avoidResample Avoid resampling if possible.
	 * @return If the operation has been successfully completed.
	 * @throws ToolsException In case of error for the operation.
	 */
	private boolean doAverage(XYSpectrumSeries serie, XYSpectrumSeries serie2,
			String nameNewCurve, Color color, boolean avoidResample) throws ToolsException {
		// Get xMin/xMax
		double xMin = ToolsUtil.getXMin(serie, serie2);
		double xMax = ToolsUtil.getXMax(serie, serie2);

		// Get the lowest sampling
		double sampling = ToolsUtil.getLowestSampling(serie, serie2);

		return doAverage(serie, serie2, nameNewCurve, color, xMin, xMax, sampling, avoidResample);
	}

	/**
	 * Create a new {@link XYSpectrumSeries} by adding the value of two {@link XYSpectrumSeries}
	 * then dividing them by 2.
	 *
	 * @param serie The first {@link XYSpectrumSeries}.
	 * @param serie2 The second {@link XYSpectrumSeries}.
	 * @param nameNewCurve The generic name of the created {@link XYSpectrumSeries}.
	 * @param number The number used for the new serie.
	 * @param color The Color used for the new {@link XYSpectrumSeries}.
	 * @param xMin The value of the minimum x point in the xAxis unit of the series.
	 * @param xMax The value of the maximum x point in the xAxis unit of the series.
	 * @param sampling The sampling used for the created {@link XYSpectrumSeries} in the xAxis unit of the series.
	 * @param avoidResample Avoid resampling if possible.
	 * @return If the operation has been successfully completed.
	 * @throws ToolsException In case of error for the operation.
	 */
	private boolean doAverage(XYSpectrumSeries serie, XYSpectrumSeries serie2,
			String nameNewCurve, Color color, double xMin, double xMax,
			double sampling, boolean avoidResample) throws ToolsException {
		XYSpectrumSeries newSerie = ToolsUtil.doAverage(serie, serie2,
				nameNewCurve, color, xMin, xMax, sampling, avoidResample);
		if (newSerie == null) {
			return false;
		}
		theInterface.addResult(newSerie, numPlot, true);
		fireDataChanged(new ModelChangedEvent(REFRESH_TOOLS_EVENT));

		return true;
	}

	/**
	 * Create a new serie by doing an operation with two series.
	 *
	 * @param serieName The name of the first serie.
	 * @param serie2Name The name of the second serie.
	 * @param operation The operation to do. It must be {@link Operation#AVERAGE_SPECTRA}
	 *  or {@link Operation#RESAMPLE_GRID} for now.
	 * @param xMin The value of the minimum x point in the xAxis unit of the series.
	 * @param xMax The value of the maximum x point in the xAxis unit of the series.
	 * @param sampling The sampling used for the created {@link XYSpectrumSeries}
	 *  in the xAxis unit of the series.
	 * @param avoidResample Avoid resampling if possible.
	 * @return if the operation is successful.
	 * @throws ToolsException In case of error for the operation.
	 */
	@Override
	public boolean doOperationWithTwoSpectra(String serieName, String serie2Name,
			Operation operation, double xMin, double xMax, double sampling,
			boolean avoidResample) throws ToolsException {
		String name;
		if (operation == Operation.AVERAGE_SPECTRA) {
			resultAverageId++;
			name = "Result average " + resultAverageId;
		} else if (operation == Operation.RESAMPLE_GRID) {
			resultResampleGridId++;
			name = "Result resample grid " + resultResampleGridId;
		} else {
			return false;
		}

		return doAverage(serieName, serie2Name, name, null, xMin, xMax, sampling, avoidResample);
	}

	/**
	 * Retrieves {@link XYSpectrumSeries} corresponding to series names given then call
	 * {@link ToolsModel#doAverage(XYSpectrumSeries, XYSpectrumSeries, String, int, Color, double, double, double)}
	 *
	 * @param serieName The name of the first serie.
	 * @param serie2Name The name of the second serie.
	 * @param nameNewCurve The generic name of the created {@link XYSpectrumSeries}.
	 * @param number The number used for the new serie.
	 * @param color The Color used for the new {@link XYSpectrumSeries}.
	 * @param xMin The value of the minimum x point in the xAxis unit of the series.
	 * @param xMax The value of the maximum x point in the xAxis unit of the series.
	 * @param sampling The sampling used for the operation and the new serie in the xAxis unit of the series.
	 * @param avoidResample Avoid resampling if possible.
	 * @return If the operation has been successfully completed.
	 * @throws ToolsException In case of error for the operation.
	 */
	private boolean doAverage(String serieName, String serie2Name, String nameNewCurve, Color color,
			double xMin, double xMax, double sampling, boolean avoidResample) throws ToolsException {
		XYSpectrumSeries serie = getCurveByName(1, serieName);
		XYSpectrumSeries serie2 = getCurveByName(2, serie2Name);
		if (serie != null && serie2 != null) {
			return doAverage(serie, serie2, nameNewCurve, color, xMin, xMax, sampling, avoidResample);
		}
		return false;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#getAllCurveOfType(eu.omp.irap.cassis.common.TypeFrequency)
	 */
	@Override
	public List<String> getAllCurveOfType(TypeFrequency type) {
		List<String> listReturn = new ArrayList<>();
		for (int i = 0; i < theInterface.getDataForToolsSpectrumAll(numPlot).size(); i++) {
			XYSpectrumSeries serie = theInterface.getDataForToolsSpectrumAll(numPlot).get(i);
			if (serie.getSpectrum().getTypeFreq() == type) {
				listReturn.add((String) serie.getKey());
			}
		}
		if (type == TypeFrequency.REST && selectedSpectrumRest == null
				&& !listReturn.isEmpty()) {
			selectedSpectrumRest = listReturn.get(0);
		} else if (type == TypeFrequency.SKY && selectedSpectrumSky == null
				&& !listReturn.isEmpty()) {
			selectedSpectrumSky = listReturn.get(0);
		}
		return listReturn;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#doOperationWithOneSpectrum(java.lang.String, eu.omp.irap.cassis.gui.plot.tools.Operation)
	 */
	@Override
	public boolean doOperationWithOneSpectrum(String serieName, Operation operation) {
		String name = null;
		if (operation == Operation.REST_TO_SKY) {
			resultRestToSkyId++;
			name = "Result RtS " + resultRestToSkyId;
		} else if (operation == Operation.AIR_TO_VACUUM) {
			resultAirToVacuumId++;
			name = "Result AtV " + resultAirToVacuumId;
		} else if (operation == Operation.VACUUM_TO_AIR) {
			resultVacuumToAirId++;
			name = "Result VtA " + resultVacuumToAirId;
		} else {
			return false;
		}

		return doOperationWithOneSpectrum(serieName, name, null, operation);
	}

	/**
	 * Retrieves {@link XYSpectrumSeries} corresponding to the series name given then call
	 * {@link ToolsModel#doOperationWithOneSpectrum(XYSpectrumSeries, String, Color, Operation)}
	 *
	 * @param serieName The name of the serie.
	 * @param nameNewCurve The generic name of the created {@link XYSpectrumSeries}.
	 * @param color The Color used for the new {@link XYSpectrumSeries}.
	 * @param operation The operation to do. It <b>must</b> be {@link Operation#REST_TO_SKY},
	 * {@link Operation#AIR_TO_VACUUM} or {@link Operation#VACUUM_TO_AIR}.
	 * @return If the operation has been successfully completed.
	 */
	public boolean doOperationWithOneSpectrum(String serieName, String nameNewCurve, Color color, Operation operation) {
		XYSpectrumSeries serie = getCurveByName(3, serieName);

		if (serie != null) {
			return doOperationWithOneSpectrum(serie, nameNewCurve, color, operation);
		}
		return false;
	}

	/**
	 * Add a new {@link XYSpectrumSeries} by doing an operation with a {@link XYSpectrumSeries}.
	 *
	 * @param serie The {@link XYSpectrumSeries}.
	 * @param nameNewCurve The generic name of the created {@link XYSpectrumSeries}.
	 * @param color The Color used for the new {@link XYSpectrumSeries}.
	 * @param operation The operation to do. It <b>must</b> be {@link Operation#REST_TO_SKY},
	 * {@link Operation#AIR_TO_VACUUM} or {@link Operation#VACUUM_TO_AIR}.
	 * @return If the operation has been successfully completed.
	 */
	private boolean doOperationWithOneSpectrum(XYSpectrumSeries serie, String nameNewCurve, Color color,
			Operation operation) {

		XYSpectrumSeries newSerie;
		switch (operation) {
			case REST_TO_SKY:
				newSerie = ToolsUtil.doRestToSky(serie, nameNewCurve, color);
				break;
			case AIR_TO_VACUUM:
				newSerie = ToolsUtil.doAirToVacuum(serie, nameNewCurve, color);
				break;
			case VACUUM_TO_AIR:
				newSerie = ToolsUtil.doVacuumToAir(serie, nameNewCurve, color);
				break;
			default:
				return false;
		}

		if (newSerie == null) {
			return false;
		}

		theInterface.addResult(newSerie, numPlot, true);
		return true;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#getSelectedSpectrumOne()
	 */
	@Override
	public String getSelectedSpectrumOne() {
		return selectedSpectrumOne;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#setSelectedSpectrumOne(java.lang.String)
	 */
	@Override
	public void setSelectedSpectrumOne(String selectedSpectrumOne) {
		this.selectedSpectrumOne = selectedSpectrumOne;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#getSelectedSpectrumTwo()
	 */
	@Override
	public String getSelectedSpectrumTwo() {
		return selectedSpectrumTwo;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#setSelectedSpectrumTwo(java.lang.String)
	 */
	@Override
	public void setSelectedSpectrumTwo(String selectedSpectrumTwo) {
		this.selectedSpectrumTwo = selectedSpectrumTwo;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#getSelectedSpectrumAll()
	 */
	@Override
	public String getSelectedSpectrumAll() {
		return selectedSpectrumAll;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#setSelectedSpectrumAll(java.lang.String)
	 */
	@Override
	public void setSelectedSpectrumAll(String selectedSpectrumAll) {
		this.selectedSpectrumAll = selectedSpectrumAll;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#getSelectedSpectrumRest()
	 */
	@Override
	public String getSelectedSpectrumRest() {
		return selectedSpectrumRest;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#setSelectedSpectrumRest(java.lang.String)
	 */
	@Override
	public void setSelectedSpectrumRest(String selectedSpectrumRest) {
		this.selectedSpectrumRest = selectedSpectrumRest;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#getSelectedSpectrumSky()
	 */
	@Override
	public String getSelectedSpectrumSky() {
		return selectedSpectrumSky;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#setSelectedSpectrumSky(java.lang.String)
	 */
	@Override
	public void setSelectedSpectrumSky(String selectedSpectrumSky) {
		this.selectedSpectrumSky = selectedSpectrumSky;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#getTelescopeModel()
	 */
	@Override
	public TelescopeModel getTelescopeModel() {
		return telescopeModel;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#doTmbTaChange(java.lang.String, eu.omp.irap.cassis.gui.plot.tools.Operation, java.util.List, java.util.List)
	 */
	@Override
	public boolean doTmbTaChange(String serieName, Operation operation,
			List<Double> efficiencyList, List<Double> frequencyList) {
		String name;
		if (operation == Operation.TMB_TO_TA) {
			resultTmbId++;
			name = "Result Tmb " + resultTmbId;
		} else if (operation == Operation.TA_TO_TMB) {
			resultTaId++;
			name = "Result Ta " + resultTaId;
		} else {
			return false;
		}
		return doTmbTaChange(serieName, name, null, operation, efficiencyList, frequencyList);
	}

	/**
	 * Retrieves {@link XYSpectrumSeries} corresponding to serie name given then call
	 * {@link ToolsModel#doTmbToTaChange(XYSpectrumSeries, String, Color, List, List, Operation)}.
	 *
	 * @param serieName The name of the {@link XYSpectrumSeries} who will be used for the Tmb/Ta change.
	 * @param nameNewCurve The name of the new serie.
	 * @param color The color used for the serie.
	 * @param operation The operation todo. It <b>must</b> be {@link Operation#TA_TO_TMB} or {@link Operation#TMB_TO_TA}.
	 * @param efficiencyList The efficiency list of the telescope.
	 * @param frequencyList The frequency list of the telescope.
	 * @return If the operation has been successfully completed.
	 */
	public boolean doTmbTaChange(String serieName, String nameNewCurve, Color color,
			Operation operation, List<Double> efficiencyList, List<Double> frequencyList) {
		XYSpectrumSeries serie = getCurveByName(3, serieName);
		if (serie != null) {
			return doTmbToTaChange(serie, nameNewCurve, color, efficiencyList, frequencyList, operation);
		}
		return false;
	}

	/**
	 * Add a new {@link XYSpectrumSeries} by doing a tmb/ta change on a given {@link XYSpectrumSeries}.
	 *
	 * @param serie The {@link XYSpectrumSeries} used for the operation.
	 * @param nameNewCurve The name of the new serie.
	 * @param color The color used for the serie.
	 * @param efficiencyList The efficiency list of the telescope.
	 * @param frequencyList The frequency list of the telescope.
	 * @param operation The operation todo. It <b>must</b> be {@link Operation#TA_TO_TMB} or {@link Operation#TMB_TO_TA}.
	 * @return If the operation has been successfully completed.
	 */
	private boolean doTmbToTaChange(XYSpectrumSeries serie, String nameNewCurve,
			Color color, List<Double> efficiencyList, List<Double> frequencyList,
			Operation operation) {
		XYSpectrumSeries newSerie = ToolsUtil.doTmbTaChange(serie, operation, nameNewCurve, color, efficiencyList, frequencyList);
		if (newSerie == null) {
			return false;
		}
		theInterface.addResult(newSerie, numPlot, true);
		fireDataChanged(new ModelChangedEvent(REFRESH_TOOLS_EVENT));

		return true;
	}

	/** (non-Javadoc)
	 * @see eu.omp.irap.cassis.gui.plot.tools.ToolsModelsInterface#doSmoothBox(java.lang.String, int)
	 */
	@Override
	public boolean doSmoothBox(String serieName, int boxSize) {
		if (serieName == null) {
			return false;
		}

		resultSmoothBoxId++;
		String name = "Result smooth box " + resultSmoothBoxId;

		return doSmoothBox(serieName, name, boxSize, null);
	}

	/**
	 * Retrieves {@link XYSpectrumSeries} corresponding to serie name given then call
	 * {@link ToolsModel#doSmoothBox(XYSpectrumSeries, String, int, Color)}.
	 *
	 * @param serieName The name of the {@link XYSpectrumSeries} who will be smoothed.
	 * @param nameNewCurve The name of the new serie.
	 * @param boxSize The box size used for the operation.
	 * @param color The color used for the serie.
	 * @return If the operation has been successfully completed.
	 */
	public boolean doSmoothBox(String serieName, String nameNewCurve, int boxSize, Color color) {
		XYSpectrumSeries serie = getCurveByName(3, serieName);
		if (serie != null) {
			return doSmoothBox(serie, nameNewCurve, boxSize, color);
		}
		return false;
	}

	/**
	 * Add a new {@link XYSpectrumSeries} by doing a Smooth Box on a given {@link XYSpectrumSeries}.
	 *
	 * @param serie The {@link XYSpectrumSeries} used for doing the Smooth Box.
	 * @param nameNewCurve The name of the new serie.
	 * @param boxSize The box size used for the operation.
	 * @param color The color used for the serie.
	 * @return If the operation has been successfully completed.
	 */
	private boolean doSmoothBox(XYSpectrumSeries serie, String nameNewCurve, int boxSize, Color color) {
		XYSpectrumSeries newSerie = ToolsUtil.doSmoothBox(serie, nameNewCurve, color, boxSize);
		if (newSerie == null) {
			return false;
		}

		theInterface.addResult(newSerie, numPlot, true);
		fireDataChanged(new ModelChangedEvent(REFRESH_TOOLS_EVENT));

		return true;
	}

	@Override
	public ViewType getViewType() {
		return theInterface.getViewType();
	}

	@Override
	public XAxisCassis getXAxisCassis() {
		return theInterface.getXAxisCassis();
	}

	@Override
	public String getSelectedSpectrumAllSecond() {
		return selectedSpectrumAllSecond;
	}

	@Override
	public void setSelectedSpectrumAllSecond(String selectedSpectrumAllSecond) {
		this.selectedSpectrumAllSecond = selectedSpectrumAllSecond;
	}

	/**
	 * Add a new {@link XYSpectrumSeries} by doing a grid resample.
	 *
	 * @param serie The {@link XYSpectrumSeries} used for doing the resample.
	 * @param serie2 The {@link XYSpectrumSeries} used for the regriding.
	 * @param nameNewCurve The name of the new serie.
	 * @param color The color used for the serie.
	 * @return If the operation has been successfully completed.
	 */
	private boolean doResampleGrid(XYSpectrumSeries serie, XYSpectrumSeries serie2, String nameNewCurve, Color color) {
		XYSpectrumSeries newSerie = ToolsUtil.doResampleGrid(serie, serie2, nameNewCurve, color);
		if (newSerie == null) {
			return false;
		}
		theInterface.addResult(newSerie, numPlot, true);
		fireDataChanged(new ModelChangedEvent(REFRESH_TOOLS_EVENT));

		return true;
	}

	/**
	 * Create a new serie by doing an advanced resampling.
	 *
	 * @param serieName The name of the serie used for the operation.
	 * @param sampling The sampling used for the created {@link XYSpectrumSeries}
	 *  in the xAxis unit of the serie.
	 * @param initialSample The initial sample in the xAxis unit of the serie.
	 * @param finalSample The final sample in the xAxis unit of the serie.
	 * @return if the operation is successful.
	 * @throws ToolsException In case of error for the operation.
	 */
	@Override
	public boolean doResampleAdvanced(String serieName, double sampling,
			double initialSample, double finalSample) throws ToolsException {
		resultResampleAdvanced++;
		String name = "Result resample advanced " + resultResampleAdvanced;
		Color color = ColorsCurve.getNewColorResult();
		return doResampleAdvanced(serieName, sampling, initialSample, finalSample, name, color);
	}

	/**
	 * Add a new {@link XYSpectrumSeries} by doing a grid resample.
	 *
	 * @param serieName The name of the serie used for the operation.
	 * @param sampling The sampling used for the created {@link XYSpectrumSeries}
	 *  in the xAxis unit of the serie.
	 * @param initialSample The initial sample in the xAxis unit of the serie.
	 * @param finalSample The final sample in the xAxis unit of the serie.
	 * @param nameNewCurve The name of the new serie.
	 * @param color The color used for the serie.
	 * @return If the operation has been successfully completed.
	 * @throws ToolsException In case of error for the operation.
	 */
	public boolean doResampleAdvanced(String serieName, double sampling,
			double initialSample, double finalSample, String nameNewCurve,
			Color color) throws ToolsException {
		XYSpectrumSeries serie = getCurveByName(3, serieName);
		if (serie == null) {
			return false;
		}

		XYSpectrumSeries newSerie = ToolsUtil.doResample(serie, sampling, initialSample, finalSample, nameNewCurve, color);
		if (newSerie == null) {
			return false;
		}
		theInterface.addResult(newSerie, numPlot, true);
		fireDataChanged(new ModelChangedEvent(REFRESH_TOOLS_EVENT));

		return true;
	}
}
