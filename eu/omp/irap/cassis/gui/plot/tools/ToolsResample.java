/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.tools;

import java.awt.Color;

import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.XAxisFrequency;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import herschel.ia.numeric.Double1d;
import herschel.ia.toolbox.spectrum.operations.segments.resampling.EulerNNResampler;

public class ToolsResample {

	/**
	 * Resample the points from a series with the given array of X points.
	 *
	 * @param xDest The array used for the news X points.
	 * @param xSerieToResample The array of X points of the series to resample.
	 * @param ySerieToResample The array of Y points of the series to resample.
	 * @return the new array of Y points or null if there was an error.
	 */
	public static double[] resample(double[] xDest, double[] xSerieToResample,
			double[] ySerieToResample) {
		if (xDest == null || xSerieToResample == null || ySerieToResample == null
				|| xSerieToResample.length != ySerieToResample.length) {
			return null;
		}

		EulerNNResampler eulerNNResampler = new EulerNNResampler();
		Double1d res = new Double1d(xDest.length);
		eulerNNResampler.resample(new Double1d(xSerieToResample),
				new Double1d(ySerieToResample), new Double1d(xDest), res);

		double[] resArray = res.toArray();
		if (resArray.length >= 1 && Double.isNaN(resArray[0])
				&& xDest[0] == xSerieToResample[0]) {
			resArray[0] = ySerieToResample[0];
		}
		return resArray;
	}

	/**
	 * Create a new {@link CommentedSpectrum} by doing a resampling to a double
	 * array of X points. Also check and replace the points if the first and
	 * last one are wrong (mathematical error...).
	 *
	 * @param cs The {@link CommentedSpectrum} on which is based the resampling.
	 * @param xFinal The X points used to resample.
	 * @return The created {@link CommentedSpectrum}
	 */
	private static CommentedSpectrum resample(CommentedSpectrum cs, double[] xFinal) {
		if (xFinal[0] < cs.getFrequenciesSignal()[0]) {
			xFinal[0] = cs.getFrequenciesSignal()[0];
		}
		if (xFinal[xFinal.length - 1] > cs.getFrequenciesSignal()
				[cs.getFrequenciesSignal().length - 1]) {
			xFinal[xFinal.length - 1] = cs
					.getFrequenciesSignal()[cs.getFrequenciesSignal().length - 1];
		}

		double[] yFinal = resample(xFinal, cs.getFrequenciesSignal(),
				cs.getIntensities());

		return ToolsUtil.createNewCommentedSpectrum(cs, xFinal, yFinal);
	}

	/**
	 * Get the lowest sampling from two series.
	 *
	 * @param serie The first series.
	 * @param serie2 The second series.
	 * @return The lowest sampling, in the unit of the xAxis.
	 */
	public static double getLowestSampling(XYSpectrumSeries serie, XYSpectrumSeries serie2) {
		double[] x = serie.getSpectrum().getFrequenciesSignal();
		double[] x2 = serie2.getSpectrum().getFrequenciesSignal();
		XAxisCassis axis = serie.getXAxis();

		if (axis instanceof XAxisFrequency) {
			return getLowestSamplingFrequency(x, x2, axis);
		} else {
			return getLowestSampling(x, x2, axis);
		}
	}

	/**
	 * Return the lowest sampling from two x arrays, if the arrays values are in
	 * Frequency, use
	 * {@link ToolsUtil#getLowestSamplingFrequency(double[], double[], XAxisCassis)}
	 * which is faster.
	 *
	 * @param x An array of x values.
	 * @param x2 Another array of x values.
	 * @param axis The X axis of the series.
	 * @return the lowest sampling in the unit of x axis.
	 */
	private static double getLowestSampling(double[] x, double[] x2, XAxisCassis axis) {
		double lowestSampling = Double.MAX_VALUE;
		double value;
		double valueI1;
		double valueI2;

		for (int i = 1; i < x.length; i++) {
			valueI1 = axis.convertFromMhzFreq(x[i - 1]);
			valueI2 = axis.convertFromMhzFreq(x[i]);
			value = Math.abs(valueI1 - valueI2);
			if (value < lowestSampling) {
				lowestSampling = value;
			}
		}
		for (int i = 1; i < x2.length; i++) {
			valueI1 = axis.convertFromMhzFreq(x2[i - 1]);
			valueI2 = axis.convertFromMhzFreq(x2[i]);
			value = Math.abs(valueI1 - valueI2);
			if (value < lowestSampling) {
				lowestSampling = value;
			}
		}
		return lowestSampling;
	}

	/**
	 * Return the lowest sampling from two x arrays.
	 *
	 * @param x An array of x values.
	 * @param x2 Another array of x values.
	 * @param axis The X axis of the series.
	 * @return the lowest sampling in the unit of x axis.
	 */
	private static double getLowestSamplingFrequency(double[] x,
			double[] x2, XAxisCassis axis) {
		double lowestSampling = Double.MAX_VALUE;
		double value;

		for (int i = 1; i < x.length; i++) {
			value = x[i] - x[i-1];
			if (value < lowestSampling && value > 0.001) {
				lowestSampling = value;
			}
		}
		for (int i = 1; i < x2.length; i++) {
			value = x2[i] - x2[i-1];
			if (value < lowestSampling && value > 0.001) {
				lowestSampling = value;
			}
		}
		return axis.convertFromMhzFreq(lowestSampling);
	}

	/**
	 * Get the lowest sampling from two {@link CommentedSpectrum}.
	 *
	 * @param cs The first {@link CommentedSpectrum}.
	 * @param cs2 The second {@link CommentedSpectrum}.
	 * @param axis The {@link XAxisCassis} of the spectra.
	 * @return The lowest sampling, in the unit of the xAxis.
	 */
	public static double getLowestSampling(CommentedSpectrum cs, CommentedSpectrum cs2,
			XAxisCassis axis) {
		double[] x = cs.getFrequenciesSignal();
		double[] x2 = cs2.getFrequenciesSignal();
		if (axis instanceof XAxisFrequency) {
			return getLowestSamplingFrequency(x, x2, axis);
		} else {
			return getLowestSampling(x, x2, axis);
		}
	}


	/**
	 * Check if we two series has joint points based by theirs xMin and xMax
	 *
	 * @param xMin The xMin value.
	 * @param xMax The XMax value.
	 * @return If the ressample is possible.
	 */
	public static boolean isRessamplePossible(double xMin, double xMax) {
		return xMin < xMax;
	}


	/**
	 * Create a new {@link XYSpectrumSeries} by doing a resampling.
	 *
	 * @param serie The {@link XYSpectrumSeries} on which is based the resampling.
	 * @param sampling The sampling used for the created {@link XYSpectrumSeries} in the xAxis unit of the series.
	 * @param name The name of the new serie.
	 * @param color The color of the new serie.
	 * @return The created {@link XYSpectrumSeries} or null if there was an error.
	 * @throws ToolsException In case of error for the operation.
	 */
	public static XYSpectrumSeries doResample(XYSpectrumSeries serie, double sampling,
			String name, Color color) throws ToolsException {
		if (serie == null) {
			return null;
		}
		return ToolsUtil.createNewSerie(serie,
				doResample(serie.getSpectrum(), serie.getXAxis(), sampling), name, color);
	}

	/**
	 * Create a new {@link XYSpectrumSeries} by doing a resampling using a base point.
	 *
	 * @param serie The {@link XYSpectrumSeries} on which is based the resampling.
	 * @param sampling The sampling used for the created {@link XYSpectrumSeries} in the xAxis unit of the series.
	 * @param initialSample The initial point used in the xAxis unit of the series.
	 * @param finalSample The final point used in the xAxis unit of the series.
	 * @param name The name of the new series.
	 * @param color The color of the new series.
	 * @return The created {@link XYSpectrumSeries} or null if there was an error.
	 * @throws ToolsException In case of error for the operation.
	 */
	public static XYSpectrumSeries doResample(XYSpectrumSeries serie, double sampling,
			double initialSample, double finalSample, String name, Color color) throws ToolsException {
		if (serie == null) {
			return null;
		}
		return ToolsUtil.createNewSerie(serie, doResample(serie.getSpectrum(), serie.getXAxis(),
				sampling, initialSample, finalSample), name, color);
	}

	/**
	 * Create a new {@link XYSpectrumSeries} by doing a resampling a serie to another to the same sampling
	 * from another serie.
	 *
	 * @param serie The {@link XYSpectrumSeries} on which is based the resampling (points x/y).
	 * @param serie2 The {@link XYSpectrumSeries} on which is based the resampling (points x used).
	 * @param name The name of the new serie.
	 * @param color The color of the new serie.
	 * @return The created {@link XYSpectrumSeries} or null if there was an error.
	 */
	public static XYSpectrumSeries doResampleGrid(XYSpectrumSeries serie,
			XYSpectrumSeries serie2, String name, Color color) {
		if (serie == null || serie2 == null) {
			return null;
		}
		return ToolsUtil.createNewSerie(serie,
				doResampleGrid(serie.getSpectrum(), serie2.getSpectrum()), name, color);
	}

	/**
	 * Create a new {@link CommentedSpectrum} by doing a resampling.
	 *
	 * @param cs The {@link CommentedSpectrum} on which is based the resampling.
	 * @param axis The {@link XAxisCassis} of the spectrum.
	 * @param sampling The sampling used for the created {@link CommentedSpectrum} in the xAxis unit of the series.
	 * @return The created {@link CommentedSpectrum} or null if there was an error.
	 * @throws ToolsException In case of error for the operation.
	 */
	public static CommentedSpectrum doResample(CommentedSpectrum cs, XAxisCassis axis,
			double sampling) throws ToolsException {
		if (cs == null) {
			return null;
		}

		double xMin;
		double xMax;
		if (axis.isInverted()) {
			xMin = axis.convertFromMhzFreq(cs.getFrequencySignalMax());
			xMax = axis.convertFromMhzFreq(cs.getFrequencySignalMin());
		} else {
			xMin = axis.convertFromMhzFreq(cs.getFrequencySignalMin());
			xMax = axis.convertFromMhzFreq(cs.getFrequencySignalMax());
		}

		if (!ToolsUtil.checkParameters(xMin, xMax, sampling)) {
			return null;
		}

		double[] xFinal = ToolsUtil.getXFinal(axis, xMin, xMax, sampling);
		return resample(cs, xFinal);
	}

	/**
	 * Create a new {@link CommentedSpectrum} by doing a resampling using a base point.
	 *
	 * @param cs The {@link CommentedSpectrum} on which is based the resampling.
	 * @param axis The {@link XAxisCassis} of the spectrum.
	 * @param sampling The sampling used for the created {@link XYSpectrumSeries} in the xAxis unit of the series.
	 * @param initialSample The initial point used in the xAxis unit of the series.
	 * @param finalSample The final point used in the xAxis unit of the series.
	 * @return The created {@link CommentedSpectrum} or null if there was an error.
	 * @throws ToolsException In case of error for the operation.
	 */
	public static CommentedSpectrum doResample(CommentedSpectrum cs, XAxisCassis axis,
			double sampling, double initialSample, double finalSample) throws ToolsException {
		if (cs == null) {
			return null;
		}

		double xMin;
		double xMax;
		if (axis.isInverted()) {
			xMin = axis.convertFromMhzFreq(cs.getFrequencySignalMax());
			xMax = axis.convertFromMhzFreq(cs.getFrequencySignalMin());
		} else {
			xMin = axis.convertFromMhzFreq(cs.getFrequencySignalMin());
			xMax = axis.convertFromMhzFreq(cs.getFrequencySignalMax());
		}

		if (!ToolsUtil.checkParameters(xMin, xMax, sampling)) {
			return null;
		}

		double[] xFinal = ToolsUtil.getXFinal(axis, xMin, xMax, sampling, initialSample,
				finalSample);
		return resample(cs, xFinal);
	}

	/**
	 * Create a new {@link CommentedSpectrum} by doing a resampling a spectrum to another to the same sampling
	 * from another series.
	 *
	 * @param cs The {@link CommentedSpectrum} on which is based the resampling (points x/y).
	 * @param cs2 The {@link CommentedSpectrum} on which is based the resampling (points x used).
	 * @return The created {@link CommentedSpectrum} or null if there was an error.
	 */
	public static CommentedSpectrum doResampleGrid(CommentedSpectrum cs, CommentedSpectrum cs2) {
		if (cs == null || cs2 == null) {
			return null;
		}

		double[] xFinal = ToolsUtil.getXFinal(cs2.getFrequenciesSignal(),
				cs.getFrequencySignalMin(), cs.getFrequencySignalMax());
		if (xFinal.length < 3) {
			return null;
		}
		return resample(cs, xFinal);
	}





}
