/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.tools;

import java.awt.Color;
import java.util.Arrays;

import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import eu.omp.irap.cassis.gui.plot.curve.config.ColorsCurve;

public class ToolsSimpleOperations {

	/**
	 * Divide the value of the arrayOne by the value of the arrayTwo like:
	 *  arrayResult[i] = arrayOne[i] / arrayTwo[i]. In case of zero value in
	 *  the arrayTwo, the value Double.NaN is set to arrayResult.
	 *
	 * @param arrayOne The first array.
	 * @param arrayTwo The array used to divide.
	 * @param size The size of the new array.
	 * @return The array resulting of the divide of arrayOne by arrayTwo.
	 */
	public static double[] divide(double[] arrayOne, double[] arrayTwo, int size) {
		double[] arrayResult = new double[size];
		for (int i = 0; i < size; i++) {
			if (arrayTwo[i] != 0) {
				arrayResult[i] = arrayOne[i] / arrayTwo[i];
			} else {
				arrayResult[i] = Double.NaN;
			}
		}
		return arrayResult;
	}

	/**
	 * Addition the values of two arrays.
	 *
	 * @param arrayOne The first array.
	 * @param arrayTwo The second array.
	 * @param size The size of the final array.
	 * @return The array resulting of the addition of the two given arrays.
	 */
	public static double[] add(double[] arrayOne, double[] arrayTwo, int size) {
		double[] arrayResult = new double[size];
		for (int i = 0; i < size; i++) {
			arrayResult[i] = arrayOne[i] + arrayTwo[i];
		}
		return arrayResult;
	}

	/**
	 * Subtract an array (arrayOne) by another one (arrayTwo).
	 *
	 * @param arrayOne The array to subtract.
	 * @param arrayTwo The array used to subtract the first array.
	 * @param size The size of the final array.
	 */
	public static double[] subtract(double[] arrayOne, double[] arrayTwo, int size) {
		double[] arrayResult = new double[size];
		for (int i = 0; i < size; i++) {
			arrayResult[i] = arrayOne[i] - arrayTwo[i];
		}
		return arrayResult;
	}

	/**
	 * Create a new {@link CommentedSpectrum} by adding the values of two {@link CommentedSpectrum} then dividing them by 2.
	 *
	 * @param cs The first {@link CommentedSpectrum}.
	 * @param cs2 The second {@link CommentedSpectrum}.
	 * @param axis The {@link XAxisCassis} of the spectra.
	 * @param xMin The value of the minimum x point in the xAxis unit of the CommentedSpectrum.
	 * @param xMax The value of the maximum x point in the xAxis unit of the CommentedSpectrum.
	 * @param sampling The sampling used for the created {@link CommentedSpectrum} in the xAxis unit of the series.
	 * @return The created {@link CommentedSpectrum} or null if there was an error.
	 * @throws ToolsException In case of error for the operation.
	 */
	public static CommentedSpectrum doAverage(CommentedSpectrum cs, CommentedSpectrum cs2,
			XAxisCassis axis, double xMin, double xMax, double sampling) throws ToolsException {
		if (!ToolsUtil.checkParameters(xMin, xMax, sampling)) {
			return null;
		}

		double[] xFinal = ToolsUtil.getXFinal(axis, xMin, xMax, sampling);
		double[] yFinalSerie1;
		double[] yFinalSerie2;

		double[] freqs = cs.getFrequenciesSignal();
		double[] freqs2 = cs2.getFrequenciesSignal();

		if (xFinal[0] < Math.max(freqs[0], freqs2[0])) {
			xFinal[0] = Math.max(freqs[0], freqs2[0]);
		}
		if (xFinal[xFinal.length - 1] > Math.min(freqs[freqs.length - 1],
				freqs2[freqs2.length - 1])) {
			xFinal[xFinal.length - 1] = Math.min(freqs[freqs.length - 1],
					freqs2[freqs2.length - 1]);
		}

		yFinalSerie1 = ToolsResample.resample(xFinal, freqs, cs.getIntensities());
		yFinalSerie2 = ToolsResample.resample(xFinal, freqs2, cs2.getIntensities());

		double[] yFinalNewSerie = new double[xFinal.length];

		for (int i = 0; i < xFinal.length; i++) {
			yFinalNewSerie[i] = (yFinalSerie1[i] + yFinalSerie2[i]) / 2.0;
		}

		return ToolsUtil.createNewCommentedSpectrum(cs, xFinal, yFinalNewSerie);
	}

	/**
	 * Create a new {@link XYSpectrumSeries} by adding the values of two series then dividing them by 2.
	 *
	 * @param serie The first {@link XYSpectrumSeries}.
	 * @param serie2 The second {@link XYSpectrumSeries}.
	 * @param nameSerie The name of the created {@link XYSpectrumSeries}.
	 * @param color  The Color used for the new {@link XYSpectrumSeries}.
	 * @param xMin The value of the minimum x point in the xAxis unit of the series.
	 * @param xMax The value of the maximum x point in the xAxis unit of the series.
	 * @param sampling The sampling used for the created {@link XYSpectrumSeries} in the xAxis unit of the series.
	 * @param avoidResample Avoid resampling if possible.
	 * @return The created {@link XYSpectrumSeries} or null if there was an error.
	 * @throws ToolsException In case of error for the operation.
	 */
	public static XYSpectrumSeries doAverage(XYSpectrumSeries serie,
			XYSpectrumSeries serie2, String nameSerie, Color color, double xMin,
			double xMax, double sampling, boolean avoidResample) throws ToolsException {
		CommentedSpectrum cs = null;
		if (avoidResample && Arrays.equals(serie.getSpectrum().getFrequenciesSignal(),
				serie2.getSpectrum().getFrequenciesSignal())) {
			cs = doAverageWithoutResampling(serie.getSpectrum(), serie2.getSpectrum());
		} else {
			cs = doAverage(serie.getSpectrum(), serie2.getSpectrum(),
					serie.getXAxis(), xMin, xMax, sampling);
		}

		if (cs != null && color == null) {
			color = ColorsCurve.getNewColorResult();
		}
		return ToolsUtil.createNewSerie(serie, cs, nameSerie, color);
	}

	/**
	 * Average two CommentedSpectrum without doing a resampling. You must check first that
	 *  the x points are the same!
	 *
	 * @param cs The first CommentedSpectrum.
	 * @param cs2 The second CommentedSpectrum.
	 * @return The result CommentedSpectrum.
	 */
	private static CommentedSpectrum doAverageWithoutResampling(
			CommentedSpectrum cs, CommentedSpectrum cs2) {
		int size = cs.getFrequenciesSignal().length;
		double[] xFinal = new double[size];
		double[] yFinal = new double[size];

		System.arraycopy(cs.getFrequenciesSignal(), 0, xFinal, 0, size);

		double[] intensitiesSerie = cs.getIntensities();
		double[] intensitiesSerie2 = cs2.getIntensities();

		for (int i = 0; i < size; i++) {
			yFinal[i] = (intensitiesSerie[i] + intensitiesSerie2[i]) / 2.0;
		}
		return ToolsUtil.createNewCommentedSpectrum(cs, xFinal, yFinal);
	}
}
