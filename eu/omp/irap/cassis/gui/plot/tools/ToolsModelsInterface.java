/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.tools;

import java.util.List;

import eu.omp.irap.cassis.common.TypeFrequency;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.gui.model.parameter.telescope.TelescopeModel;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;


/**
 * Interface for the Tools Models classes.
 * Must be implemented for each models of the Tools (Mosaic, or for only one plot).
 *
 * @author M. Boiziot
 */
public interface ToolsModelsInterface {

	/**
	 * @return The name of the spectra for the first list.
	 */
	List<String> getCurveOneName();

	/**
	 * @return The name of the spectra for the second list.
	 */
	List<String> getCurveTwoName();

	/**
	 * @return The name of all spectra in the center plot.
	 */
	List<String> getCurveAllName();

	/**
	 * @param type The Type of the curve.
	 * @return The name of all curve with the given TypeFrequency.
	 */
	List<String> getAllCurveOfType(TypeFrequency type);

	/**
	 * Create a new serie by doing an operation between two series (ADD, SUBTRACT or DIVIDE).
	 *
	 * @param serieName The name of the first serie. (This is the serie whose data will be subtracted in case of SUBTRACT operation.)
	 * @param serieName2 The name of the second serie. (This is the serie used to subtract in case of SUBTRACT operation.)
	 * @param operation The operation to do. It <b>must</b> be {@link Operation#ADD}, {@link Operation#SUBTRACT} or {@link Operation#DIVIDE}.
	 * @param avoidResample Avoid resampling if possible.
	 * @return if the operation is successful.
	 * @throws ToolsException In case of error for the operation.
	 */
	boolean doOperation(String serieName, String serieName2, Operation operation, boolean avoidResample) throws ToolsException;

	/**
	 * Create a new serie by doing an operation between a serie and a double value.
	 * (ADD_CONSTANT, MULTIPLY_CONSTANT, RESAMPLE_AUTOMATIC, VLSR or SMOOTH_GAUSS)
	 *
	 * @param serieName The name of the serie.
	 * @param value The double value.
	 * @param operation The operation to do. It <b>must</b> be {@link Operation#ADD_CONSTANT},
	 * {@link Operation#MULTIPLY_CONSTANT}, {@link Operation#RESAMPLE_AUTOMATIC}
	 * {@link Operation#VLSR}, {@link Operation#SKY_TO_REST} or {@link Operation#SMOOTH_GAUSS}.
	 * @return if the Operation of is successful.
	 * @throws ToolsException In case of error for the operation.
	 */
	boolean doOperationWithAValue(String serieName, double value, Operation operation) throws ToolsException;

	/**
	 * @return The selected action.
	 */
	Operation getActionSelected();

	/**
	 * Change the selected action.
	 *
	 * @param actionSelected The new selected action.
	 */
	void setActionSelected(Operation actionSelected);

	/**
	 * Do a Smooth Hanning on a serie.
	 *
	 * @param serieName The name of the serie on which we want to do a Smooth Haning.
	 * @param iterationNumber The number of iterations.
	 * @return If the operation has been successfully completed.
	 */
	boolean smoothHanning(String serieName, int iterationNumber);

	/**
	 * @return If the advanced panel is allowed for the two spectrum view.
	 */
	boolean isAdvancedParametersTwoSpectrumAllowed();

	/**
	 * @return If the advanced Smooth Hanning panel is allowed.
	 */
	boolean isAdvancedSmoothHanningAllowed();

	/**
	 * @return The state of the advanced subtract panel.
	 */
	boolean getAdvancedSubtractState();

	/**
	 * @return The state of the advanced Smooth Hanning panel.
	 */
	boolean getAdvancedSmoothHanningState();

	/**
	 * Change the state of the advanced Smooth Hanning panel.
	 *
	 * @param newState The new state.
	 */
	void setAdvancedSmoothHanningState(boolean newState);

	/**
	 * Change the state of the advanced subtract panel.
	 *
	 * @param newState The new state.
	 */
	void setAdvancedSubtractState(boolean newState);

	/**
	 * Create a new serie by doing an operation between two series (ADD, SUBTRACT or DIVIDE).
	 *
	 * @param serieName The name of the first serie. (This is the serie whose data will be subtracted in case of SUBTRACT operation.)
	 * @param serie2Name The name of the second serie. (This is the serie used to subtract in case of SUBTRACT operation.)
	 * @param xMin The value of the minimum x point in the xAxis unit of the series.
	 * @param xMax The value of the maximum x point in the xAxis unit of the series.
	 * @param sampling The sampling used for the created {@link XYSpectrumSeries} in the xAxis unit of the series.
	 * @param operation The operation to do. It <b>must</b> be {@link Operation#ADD}, {@link Operation#SUBTRACT} or {@link Operation#DIVIDE}.
	 * @param avoidResample Avoid resampling if possible.
	 * @return if the operation is successful.
	 * @throws ToolsException In case of error for the operation.
	 */
	boolean doOperation(String serieName, String serie2Name, double xMin,
			double xMax, double sampling, Operation operation,
			boolean avoidResample) throws ToolsException;

	/**
	 * Create a new serie by doing an operation with two series.
	 *
	 * @param serieName The name of the first serie.
	 * @param serie2Name The name of the second serie.
	 * @param operation The operation to do. It must be {@link Operation#AVERAGE_SPECTRA}
	 *  or {@link Operation#RESAMPLE_GRID} for now.
	 * @param avoidResample Avoid resampling if possible.
	 * @return if the operation is successful.
	 * @throws ToolsException In case of error for the operation.
	 */
	boolean doOperationWithTwoSpectra(String serieName, String serie2Name,
			Operation operation, boolean avoidResample) throws ToolsException;

	/**
	 * Create a new serie by doing an operation with two series.
	 *
	 * @param serieName The name of the first serie.
	 * @param serie2Name The name of the second serie.
	 * @param operation The operation to do. It must be {@link Operation#AVERAGE_SPECTRA}
	 *  or {@link Operation#RESAMPLE_GRID} for now.
	 * @param xMin The value of the minimum x point in the xAxis unit of the series.
	 * @param xMax The value of the maximum x point in the xAxis unit of the series.
	 * @param sampling The sampling used for the created {@link XYSpectrumSeries}
	 *  in the xAxis unit of the series.
	 * @param avoidResample Avoid resampling if possible.
	 * @return if the operation is successful.
	 * @throws ToolsException In case of error for the operation.
	 */
	boolean doOperationWithTwoSpectra(String serieName, String serie2Name,
			Operation operation, double xMin, double xMax, double sampling,
			boolean avoidResample) throws ToolsException;

	/**
	 * Create a new serie by doing an operation with only a serie.
	 *
	 * @param serieName The name of the serie.
	 * @param operation The operation to do. It must be {@link Operation#REST_TO_SKY},
	 * {@link Operation#AIR_TO_VACUUM} or {@link Operation#VACUUM_TO_AIR}.
	 * @return if the operation is successful.
	 */
	boolean doOperationWithOneSpectrum(String serieName, Operation operation);

	/**
	 * @return The name of the selected spectrum one.
	 */
	String getSelectedSpectrumOne();

	/**
	 * Change only on model the selected spectrum one.
	 *
	 * @param selectedSpectrumOne The name of the selected spectrum one.
	 */
	void setSelectedSpectrumOne(String selectedSpectrumOne);

	/**
	 * @return The name of the selected spectrum two.
	 */
	String getSelectedSpectrumTwo();

	/**
	 * Change only on model the selected spectrum two.
	 *
	 * @param selectedSpectrumTwo The name of the selected spectrum two.
	 */
	void setSelectedSpectrumTwo(String selectedSpectrumTwo);

	/**
	 * @return The name of the selected spectrum "all".
	 */
	String getSelectedSpectrumAll();

	/**
	 * Change only on model the selected spectrum "all".
	 *
	 * @param selectedSpectrumAll The name of the selected spectrum "all.
	 */
	void setSelectedSpectrumAll(String selectedSpectrumAll);


	/**
	 * @return The name of the second selected spectrum "all".
	 */
	String getSelectedSpectrumAllSecond();

	/**
	 * Change only on model the second selected spectrum "all".
	 *
	 * @param selectedSpectrumAllSecond The name of the second selected spectrum "all.
	 */
	void setSelectedSpectrumAllSecond(String selectedSpectrumAllSecond);

	/**
	 * @return The name of the rest selected spectrum;
	 */
	String getSelectedSpectrumRest();

	/**
	 * Change only on model the selected spectrum "rest"
	 *
	 * @param selectedSpectrumRest The name of the rest selected spectrum.
	 */
	void setSelectedSpectrumRest(String selectedSpectrumRest);

	/**
	 * @return The name of the sky selected spectrum.
	 */
	String getSelectedSpectrumSky();

	/**
	 * Change only on model the selected spectrum "sky".
	 *
	 * @param selectedSpectrumSky The name of the sky selected spectrum.
	 */
	void setSelectedSpectrumSky(String selectedSpectrumSky);

	/**
	 * @return The model of the telescope.
	 */
	TelescopeModel getTelescopeModel();

	/**
	 * Create a new serie by doing a change with tmb/ta.
	 *
	 * @param serieName The name of the serie used for the operation.
	 * @param operation The operation to do. It <b>must</b> be {@link Operation#TA_TO_TMB} or {@link Operation#TMB_TO_TA}.
	 * @param efficiencyList The efficiency list of the telescope.
	 * @param frequencyList The frequency list of the telescope.
	 * @return if the operation is successful.
	 */
	boolean doTmbTaChange(String serieName, Operation operation,
			List<Double> efficiencyList, List<Double> frequencyList);

	/**
	 * Create a new serie by doing a Smooth Box operation.
	 *
	 * @param serieName The name of the serie used for the operation.
	 * @param boxSize The box size used for the operation.
	 * @return if the operation is successful.
	 */
	boolean doSmoothBox(String serieName, int boxSize);

	/**
	 * Return the ViewType.
	 *
	 * @return the ViewType.
	 */
	ViewType getViewType();

	/**
	 * Return the current XAxisCassis.
	 *
	 * @return the current XAxisCassis.
	 */
	XAxisCassis getXAxisCassis();

	/**
	 * Create a new serie by doing an advanced resampling.
	 *
	 * @param serieName The name of the serie used for the operation.
	 * @param sampling The sampling used for the created {@link XYSpectrumSeries}
	 *  in the xAxis unit of the serie.
	 * @param initialSample The initial sample in the xAxis unit of the serie.
	 * @param finalSample The final sample in the xAxis unit of the serie.
	 * @return if the operation is successful.
	 * @throws ToolsException In case of error for the operation.
	 */
	boolean doResampleAdvanced(String serieName, double sampling,
			double initialSample, double finalSample) throws ToolsException;

	/**
	 * Add a listener to the model.
	 *
	 * @param listener The listener.
	 */
	void addModelListener(ModelListener listener);

	/**
	 * Remove a listener.
	 *
	 * @param listener The listener to remove.
	 */
	void removeModelListener(ModelListener listener);

}
