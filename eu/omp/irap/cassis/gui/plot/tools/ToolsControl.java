/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.tools;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.axes.XAxisVelocity;
import eu.omp.irap.cassis.common.axes.XAxisWaveLength;
import eu.omp.irap.cassis.common.axes.X_AXIS;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import eu.omp.irap.cassis.gui.plot.util.VelocityAxisDialog;

/**
 * Control class for the Tools functionality.
 *
 * @author M. Boiziot
 */
public class ToolsControl implements ModelListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(ToolsControl.class);
	private ToolsView view;
	private ToolsModelsInterface model;


	/**
	 * Constructor of the ToolsControl.
	 *
	 * @param theView The view.
	 * @param theModel The model who implements ToolsModelsInterface.
	 */
	public ToolsControl(ToolsView theView, ToolsModelsInterface theModel) {
		view = theView;
		model = theModel;
		model.addModelListener(this);
		model.getTelescopeModel().addModelListener(this);
	}

	/**
	 * @return The view.
	 */
	public ToolsView getView() {
		return view;
	}

	/**
	 * @return The model who implements ToolsModelsInterface.
	 */
	public ToolsModelsInterface getModel() {
		return model;
	}

	/**
	 * @see eu.omp.irap.cassis.common.events.ModelListener#dataChanged(eu.omp.irap.cassis.common.events.ModelChangedEvent)
	 */
	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (ToolsModel.REFRESH_TOOLS_EVENT.equals(event.getSource())) {
			view.refresh();
		} else if (ToolsView.TOOLS_TELESCOPE_CHANGE_EVENT.equals(event.getSource())) {
			view.refreshTelescopeButton();
		}
	}

	/**
	 * Method executed when the display button is clicked. Get the selected Operation,
	 * then check if all parameters are OK for this operation. If so, call the model
	 * to run the operation.
	 */
	public void displayClicked() {
		Operation operation = model.getActionSelected();

		if (operation == Operation.SUBTRACT || operation == Operation.ADD ||
				operation == Operation.AVERAGE_SPECTRA || operation == Operation.DIVIDE) {
			checkAndDisplayTwoSpectrumOperation(operation);
		} else if (operation == Operation.ADD_CONSTANT || operation == Operation.MULTIPLY_CONSTANT ||
				operation == Operation.SKY_TO_REST || operation == Operation.VLSR ||
				operation == Operation.REDSHIFT) {
			checkAndDisplaySpectrumValueOperation(operation);
		} else if (operation == Operation.REST_TO_SKY) {
			checkAndDisplayRestToSkyOperation();
		} else if (operation == Operation.TA_TO_TMB || operation == Operation.TMB_TO_TA) {
			checkAndDisplayTmbTaOperation(operation);
		} else if (operation == Operation.SMOOTH) {
			checkAndDisplaySmoothOperation();
		} else if (operation == Operation.RESAMPLE) {
			checkAndDisplayResampleOperation();
		} else if (operation == Operation.AIR_TO_VACUUM || operation == Operation.VACUUM_TO_AIR) {
			checkAndDisplayAirVacuumOperation(operation);
		} else {
			LOGGER.error("ToolsControl Error. This operation is unknow.");
		}
	}

	/**
	 * Refresh the advanced panel if needed and possible.
	 */
	public void refreshInfo() {
		Operation op = model.getActionSelected();
		boolean twoSpectrumOp = op == Operation.SUBTRACT || op == Operation.ADD || op == Operation.AVERAGE_SPECTRA || op == Operation.DIVIDE;
		if (twoSpectrumOp &&
				model.isAdvancedParametersTwoSpectrumAllowed() &&
				model.getAdvancedSubtractState() &&
				view.getJComboBoxDataFirst().getSelectedItem() != null &&
				view.getJComboBoxDataSecond().getSelectedItem() != null &&
				model instanceof ToolsModel) {

			ToolsModel theToolsModel = (ToolsModel) model;
			XYSpectrumSeries serieOne = theToolsModel.getCurveByName(1, view.getJComboBoxDataFirst().getSelectedItem().toString());
			XYSpectrumSeries serieTwo = theToolsModel.getCurveByName(2, view.getJComboBoxDataSecond().getSelectedItem().toString());

			if (serieOne != null && serieTwo != null) {
				view.getXMinTextField().setText(String.valueOf(ToolsUtil.getXMin(serieOne, serieTwo)));
				view.getXMaxTextField().setText(String.valueOf(ToolsUtil.getXMax(serieOne, serieTwo)));
				view.getSamplingTextField().setText(String.valueOf(ToolsUtil.getLowestSampling(serieOne, serieTwo)));
			}
		}
	}

	/**
	 * Check the parameters then display a REST_TO_SKY operation.
	 */
	private void checkAndDisplayRestToSkyOperation() {
		// We only need a curve here.
		if (view.getJComboBoxRest().getSelectedItem() == null) {
			LOGGER.error("ToolsControl Error : the spectrum can not be null for the operation. Exiting.");
			return;
		}
		String spectrumName = (String) view.getJComboBoxRest().getSelectedItem();

		if (!model.doOperationWithOneSpectrum(spectrumName, Operation.REST_TO_SKY)) {
			displayGenericOperationError();
		}
	}

	/**
	 * Check the parameters then display an operation with a spectrum and a double.
	 *
	 * @param operation The operation to do.
	 */
	private void checkAndDisplaySpectrumValueOperation(Operation operation) {
		if ((operation == Operation.SKY_TO_REST && view.getJComboBoxSky() == null)
				|| view.getJComboBoxAllCenter().getSelectedItem() == null
				|| ((operation == Operation.ADD_CONSTANT
						|| operation == Operation.MULTIPLY_CONSTANT)
						&& (!ToolsView.isContentANumber(view.getConstantCassisTextField())
								|| Double.parseDouble(view.getConstantCassisTextField()
										.getText()) == 0))
				|| ((operation == Operation.SKY_TO_REST || operation == Operation.VLSR)
						&& "".equals(view.getVlsrTextField().getText()))) {
			LOGGER.error("Error with the given parametters. Abording.");
			return;
		}

		double value = 0.0;
		if (operation == Operation.ADD_CONSTANT) {
			value = Double.parseDouble(view.getConstantCassisTextField().getText());
			if (value == 0.0) {
				LOGGER.warn("No new spectrum created, added contant is 0.0");
				return;
			}
		} else if (operation == Operation.MULTIPLY_CONSTANT) {
			value = Double.parseDouble(view.getConstantCassisTextField().getText());
			if (value == 1.0) {
				LOGGER.warn("No new spectrum created, the multiplier is 1.");
				return;
			}

		} else if (operation == Operation.REDSHIFT) {
			value = Double.parseDouble(view.getConstantCassisTextField().getText());
			if (value == 0.0) {
				LOGGER.warn("No new spectrum created, the redshift is 0.");
				return;
			}
		} else if (operation == Operation.SKY_TO_REST ||
					operation == Operation.VLSR) {
			try {
				value = Double.parseDouble(view.getVlsrTextField().getText());
			} catch (NumberFormatException nfe) {
				LOGGER.error("The vlsr must be a number", nfe);
				return;
			}
			if (value == 0.0 && operation == Operation.SKY_TO_REST) {
				LOGGER.warn("No new spectrum created as the vlsr value is 0.");
				return;
			}
		}

		String spectrumName = null;
		if (operation == Operation.SKY_TO_REST || operation == Operation.REDSHIFT) {
			spectrumName = (String) view.getJComboBoxSky().getSelectedItem();
		} else {
			spectrumName = (String) view.getJComboBoxAllCenter().getSelectedItem();
		}

		try {
			if (!model.doOperationWithAValue(spectrumName, value, operation)) {
				displayGenericOperationError();
			}
		} catch (ToolsException te) {
			displayToolsExceptionError(te.getMessage());
		}
	}

	/**
	 * Check parameters then display a Tmb/Ta operation.
	 *
	 * @param operation The operation to do.
	 */
	private void checkAndDisplayTmbTaOperation(Operation operation) {
		if (view.getJComboBoxAllCenter().getSelectedItem() == null) {
			LOGGER.error("ToolsControl Error : the spectrum can not be null for the operation. Exiting");
			return;
		}
		String serieName = (String) view.getJComboBoxAllCenter().getSelectedItem();
		if (!model.doTmbTaChange(serieName, operation,
				model.getTelescopeModel().getTelescope().getEffValueList(),
				model.getTelescopeModel().getTelescope().getFrequencyList())) {
			JOptionPane.showMessageDialog(view, "Error during the operation.\n" +
					"Check if the spectrum is in the range of the telescope.",
					"Cassis Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Check the parameters then display an operation with two spectrum.
	 *
	 * @param operation The operation to do.
	 */
	private void checkAndDisplayTwoSpectrumOperation(Operation operation) {
		if (view.getJComboBoxDataFirst().getSelectedItem() == null || view.getJComboBoxDataSecond().getSelectedItem() == null) {
			LOGGER.error("ToolsControl Error. At least one of two item is null. Exiting.");
			return;
		}
		String firstSpectrumName = (String) view.getJComboBoxDataFirst().getSelectedItem();
		String secondSpectrumName = (String) view.getJComboBoxDataSecond().getSelectedItem();

		boolean advancedAllowed = model.isAdvancedParametersTwoSpectrumAllowed();
		boolean advanced = model.getAdvancedSubtractState();

		if (firstSpectrumName.equals(secondSpectrumName)) {
			JOptionPane.showMessageDialog(view,
					"There is an error with parameters. You can not cannot do that operation with two times the same spectrum.",
					"Parameters error", JOptionPane.ERROR_MESSAGE);
			return;
		}

		boolean avoidResample = view.getAvoidResamplingCheckBox().isSelected();
		// Advanced or not ?
		if (advancedAllowed && advanced) {
			// This is an advanced operation.
			// Check if every parameters are filled.
			checkAndDisplayTwoSpectrumAdvanced(operation, firstSpectrumName,
					secondSpectrumName, avoidResample);
		} else {
			checkAndDisplayTwoSpectrumOperationNotAdvanced(operation,
					firstSpectrumName, secondSpectrumName, advancedAllowed, true);
		}
	}

	private void checkAndDisplayTwoSpectrumOperationNotAdvanced(Operation operation,
			String firstSpectrumName, String secondSpectrumName,
			boolean advancedAllowed, boolean avoidResample) {
		boolean result = false;
		try {
			if (operation == Operation.ADD || operation == Operation.SUBTRACT || operation == Operation.DIVIDE) {
				result = model.doOperation(firstSpectrumName, secondSpectrumName, operation, avoidResample);
			} else if (operation == Operation.AVERAGE_SPECTRA) {
				result = model.doOperationWithTwoSpectra(firstSpectrumName, secondSpectrumName, operation, avoidResample);
			}

			if (!result) {
				if (advancedAllowed) {
					JOptionPane.showMessageDialog(view, "Defaults parameters do"
							+ " not allow to do the operation, please use the"
							+ " advanced interface instead.",
							"Subtract error", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(view, "There was a least one error"
							+ " during the operation with defaults parameters." +
							" Please use the advanced interface one each plot for doing"
							+ " the operation with propers parameters.",
							"Operation error", JOptionPane.ERROR_MESSAGE);
				}
			}
		} catch (ToolsException te) {
			displayToolsExceptionError(te.getMessage());
		}
	}

	/**
	 * Check the parameters for an operation with two spectra then do the operation.
	 *
	 * @param operation The operation to do.
	 * @param firstSpectrumName The name of the first spectrum used for the operation.
	 * @param secondSpectrumNameThe name of the second spectrum used for the operation.
	 */
	private void checkAndDisplayTwoSpectrumAdvanced(Operation operation,
			String firstSpectrumName, String secondSpectrumName, boolean avoidResample) {
		if (view.getXMinTextField().getText().isEmpty() ||
				view.getXMaxTextField().getText().isEmpty() ||
				view.getSamplingTextField().getText().isEmpty()) {
			JOptionPane.showMessageDialog(view,
					"There is an error with the parameters. Please fully fill the parameters.",
					"Operation parameters error", JOptionPane.ERROR_MESSAGE);
			return;
		}

		double xMin = 0.0;
		double xMax = 0.0;
		double sampling = 0.0;
		try {
			xMin = Double.valueOf(view.getXMinTextField().getText());
			xMax = Double.valueOf(view.getXMaxTextField().getText());
			sampling = Double.valueOf(view.getSamplingTextField().getText());
		} catch (NumberFormatException nfe) {
			LOGGER.error("The xMin, xMax and the sampling must be numbers", nfe);
			return;
		}

		if (xMin > xMax) {
			JOptionPane.showMessageDialog(view, "The operation can not be performed because xMin > xMax.",
					"Operation parameters error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		if (sampling > xMax - xMin) {
			JOptionPane.showMessageDialog(view, "The selected sampling is too large.",
					"Operation parameters error", JOptionPane.ERROR_MESSAGE);
			return;
		}

		boolean result = false;
		try {
			if (operation == Operation.ADD || operation == Operation.SUBTRACT || operation == Operation.DIVIDE) {
				result = model.doOperation(firstSpectrumName, secondSpectrumName, xMin, xMax, sampling, operation, avoidResample);
			} else if (operation == Operation.AVERAGE_SPECTRA) {
				result = model.doOperationWithTwoSpectra(firstSpectrumName, secondSpectrumName, operation, xMin, xMax, sampling, avoidResample);
			}

			if (!result) {
				JOptionPane.showMessageDialog(view, "The given parameters do not allow to do the operation.",
						"Operation parameters error", JOptionPane.ERROR_MESSAGE);
			}
		} catch (ToolsException te) {
			displayToolsExceptionError(te.getMessage());
		}
	}

	/**
	 * Check the parameters for Smooth then do the needed operation.
	 */
	private void checkAndDisplaySmoothOperation() {
		Operation smoothOperation = (Operation) view.getSmoothOperationComboBox().getSelectedItem();

		// In all cases, we need a Spectrum.
		if (view.getJComboBoxAllCenter().getSelectedItem() == null) {
			LOGGER.error("ToolsControl Error : the spectrum can not be null for the operation. Exiting.");
			return;
		}
		String spectrumName = (String) view.getJComboBoxAllCenter().getSelectedItem();

		if (smoothOperation == Operation.SMOOTH_BOX) {
			int boxSize = Integer.parseInt(view.getIntegerTextField().getText());

			if (!model.doSmoothBox(spectrumName, boxSize)) {
				displayGenericOperationError();
			}
		} else if (smoothOperation == Operation.SMOOTH_GAUSS) {
			checkAndDisplaySmoothGauss(spectrumName);
		} else if (smoothOperation == Operation.SMOOTH_HANNING) {
			checkAndDisplaySmoothHanning(spectrumName);
		}
	}

	/**
	 * Check parameters for a {@link Operation#SMOOTH_GAUSS} then do the operation.
	 *
	 * @param spectrumName The name of the spectrum used for the operation.
	 */
	private void checkAndDisplaySmoothGauss(String spectrumName) {
		if (!ToolsView.isContentANumber(view.getConstantCassisTextField())) {
			JOptionPane.showMessageDialog(view,
					"The given parameter must be a number.",
					"Parameter error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		double fwhm = Double.parseDouble(view.getConstantCassisTextField().getText());
		if (fwhm == 0.0) {
			JOptionPane.showMessageDialog(view,
					"The fwhm parameter can not be equals to 0.",
					"Parameter error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		if ((model.getViewType() == ViewType.FULL_SPECTRUM || model.getViewType() == ViewType.LOOMIS_SPECTRUM) &&
				(model.getXAxisCassis() instanceof XAxisVelocity || model.getXAxisCassis() instanceof XAxisWaveLength)) {
			JOptionPane.showMessageDialog(view,
					"Warning, result could be incorrect on spectrum edges for broad spectra.",
					"Cassis Warning", JOptionPane.WARNING_MESSAGE);
			if (model.getViewType() == ViewType.FULL_SPECTRUM) {
				ToolsModel mod = (ToolsModel) model;
				XYSpectrumSeries serie = mod.getCurveByName(3, spectrumName);
				if (serie.getXAxis().getAxis() == X_AXIS.WAVE_LENGTH) {
					XAxisWaveLength axisWL = (XAxisWaveLength) serie.getXAxis();
					double oldFreqRef = Double.isNaN(axisWL.getFreqRef()) ? 0.0 : axisWL.getFreqRef();
					VelocityAxisDialog vad = new VelocityAxisDialog(null, oldFreqRef);
					vad.setVisible(true);
					axisWL.setFreqRef(vad.getFreqRef());
				}
			}
		}
		try {
			if (!model.doOperationWithAValue(spectrumName, fwhm, Operation.SMOOTH_GAUSS)) {
				displayGenericOperationError();
			}
		} catch (ToolsException te) {
			displayToolsExceptionError(te.getMessage());
		}
	}

	/**
	 * Check parameters for a {@link Operation#SMOOTH_HANNING} then do the operation.
	 *
	 * @param spectrumName The name of the spectrum used for the operation.
	 */
	private void checkAndDisplaySmoothHanning(String spectrumName) {
		boolean advancedAllowed = model.isAdvancedSmoothHanningAllowed();
		boolean advanced = model.getAdvancedSmoothHanningState();
		int iterationNumber;
		if (advancedAllowed && advanced) {
			iterationNumber = Integer.parseInt(view.getSHITextField().getText());

			if (iterationNumber < 1) {
				JOptionPane.showMessageDialog(view,
						"The iteration number must be an integer greater or equal to 1.",
						"Smooth Hanning parameters error", JOptionPane.ERROR_MESSAGE);
				return;
			}
		} else {
			iterationNumber = 1;
		}

		if (!model.smoothHanning(spectrumName, iterationNumber)) {
			displayGenericOperationError();
		}
	}

	/**
	 * Check the parameters for resample then do the needed operation.
	 */
	private void checkAndDisplayResampleOperation() {
		Operation resampleOperation = (Operation) view.getResampleOperationComboBox().getSelectedItem();

		// In all cases, we need a Spectrum.
		if (view.getJComboBoxAllCenter().getSelectedItem() == null) {
			LOGGER.error("ToolsControl Error : the spectrum can not be null for the operation. Exiting.");
			return;
		}
		String spectrumName = (String) view.getJComboBoxAllCenter().getSelectedItem();

		if (resampleOperation == Operation.RESAMPLE_AUTOMATIC) {
			checkAndDisplayResampleAutomatic(resampleOperation, spectrumName);
		} else if (resampleOperation == Operation.RESAMPLE_ADVANCED) {
			checkAndDisplayResampleAdvanced(spectrumName);
		} else if (resampleOperation == Operation.RESAMPLE_GRID) {
			checkAndDisplayResampleGrid(spectrumName);
		}
	}

	/**
	 * Check parameters for a {@link Operation#RESAMPLE_AUTOMATIC} then do the operation.
	 *
	 * @param spectrumName The name of the spectrum used for the resampling.
	 */
	private void checkAndDisplayResampleAutomatic(Operation resampleOperation,
			String spectrumName) {
		double value;
		try {
			value = Double.parseDouble(view.getSamplingTextField().getText());
		} catch (NumberFormatException nfe) {
			LOGGER.error("The sampling must be a number", nfe);
			JOptionPane.showMessageDialog(view, "The sampling must be a number!",
					"Operation parameters error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		try {
			if (!model.doOperationWithAValue(spectrumName, value, resampleOperation)) {
				displayGenericOperationError();
			}
		} catch (ToolsException te) {
			displayToolsExceptionError(te.getMessage());
		}
	}

	/**
	 * Check parameters for a {@link Operation#RESAMPLE_ADVANCED} then do the operation.
	 *
	 * @param spectrumName The name of the spectrum used for the resampling.
	 */
	private void checkAndDisplayResampleAdvanced(String spectrumName) {
		double value;
		double initialSample;
		double finalSample;
		try {
			value = Double.parseDouble(view.getSamplingTextField().getText());
			initialSample = Double.parseDouble(view.getInitialSampleTextField().getText());
			finalSample = Double.parseDouble(view.getFinalSampleTextField().getText());
		} catch (NumberFormatException nfe) {
			LOGGER.error("The sampling, initial sample and final sample must be numbers", nfe);
			JOptionPane.showMessageDialog(view,
					"The sampling, initial sample and final sample must be numbers!",
					"Operation parameters error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		try {
			if (!model.doResampleAdvanced(spectrumName, value, initialSample, finalSample)) {
				displayGenericOperationError();
			}
		} catch (ToolsException te) {
			displayToolsExceptionError(te.getMessage());
		}
	}

	/**
	 * Check parameters for a {@link Operation#RESAMPLE_GRID} then do the operation.
	 *
	 * @param spectrumName The name of the spectrum used for the resampling.
	 */
	private void checkAndDisplayResampleGrid(String spectrumName) {
		String regridingSpectrumName = (String) view.getJComboBoxAllCenterSecond().getSelectedItem();
		if (regridingSpectrumName == null) {
			JOptionPane.showMessageDialog(view, "The regriding spectrum can not be null for the operation.",
					"Operation parameters error", JOptionPane.ERROR_MESSAGE);
			return;
		} else if (regridingSpectrumName.equals(spectrumName)) {
			JOptionPane.showMessageDialog(view, "The regriding spectrum can not be the same as the initial spectrum.",
					"Operation parameters error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		try {
			if (!model.doOperationWithTwoSpectra(spectrumName, regridingSpectrumName,
					Operation.RESAMPLE_GRID, false)) {
				displayGenericOperationError();
			}
		} catch (ToolsException te) {
			displayToolsExceptionError(te.getMessage());
		}
	}

	/**
	 * Check parameters then display a Air to Vacuum or Vacuum to Air operation.
	 *
	 * @param operation The operation to do.
	 */
	private void checkAndDisplayAirVacuumOperation(Operation operation) {
		// We only need a curve here.
		if (view.getJComboBoxAllCenter().getSelectedItem() == null) {
			LOGGER.error("ToolsControl Error : the spectrum can not be null for the operation. Exiting.");
			return;
		}
		String spectrumName = (String) view.getJComboBoxAllCenter().getSelectedItem();

		if (!model.doOperationWithOneSpectrum(spectrumName, operation)) {
			displayGenericOperationError();
		}
	}

	/**
	 * Display a generic operation error message.
	 */
	private void displayGenericOperationError() {
		JOptionPane.showMessageDialog(view, "Error during the operation.",
				"Operation Error", JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * Display an error message message.
	 *
	 * @param error The error to display.
	 */
	private void displayToolsExceptionError(String error) {
		JOptionPane.showMessageDialog(view, error,
				"Operation Error", JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * Remove listeners (model and telescope).
	 */
	public void removeListeners() {
		model.removeModelListener(this);
		model.getTelescopeModel().removeModelListener(this);
	}
}
