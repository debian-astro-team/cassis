/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.tools;

import java.awt.Color;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.gui.fit.FitConstant;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import herschel.ia.numeric.Double1d;
import herschel.ia.numeric.toolbox.filter.Convolution;
import herschel.ia.numeric.toolbox.fit.GaussModel;

public class ToolsSmooth {

	private static final Logger LOGGER = LoggerFactory.getLogger(ToolsSmooth.class);
	/**
	 * Create a new {@link CommentedSpectrum} by doing a Smooth Box.
	 *
	 * @param cs The {@link CommentedSpectrum} used a base for the new spectrum after the operation.
	 * @param boxSize The size of the box.
	 * @return The created {@link CommentedSpectrum} or null if there was an error.
	 */
	public static CommentedSpectrum doSmoothBox(CommentedSpectrum cs, int boxSize) {
		double[] frequencies = cs.getFrequenciesSignal();
		double[] intensities = cs.getIntensities();

		int n;
		boolean pair;

		if ((boxSize % 2) == 0) {
			n = boxSize / 2;
			pair = true;
		} else {
			n = (boxSize - 1) / 2;
			pair = false;
		}

		int nbPoints = frequencies.length - 2 * n;
		double[] newFrequencies = Arrays.copyOfRange(frequencies, n, frequencies.length - n);
		double[] newIntensities = new double[nbPoints];

		int m;
		if (pair) {
			for (int j = 0; j < nbPoints; j++) {
				m = j + n;
				newIntensities[j] = doSmoothBoxPairOperation(m, n, intensities);
			}
		} else {
			for (int j = 0; j < nbPoints; j++) {
				m = j + n;
				newIntensities[j] = doSmoothBoxImpairOperation(m, n, intensities);
			}
		}

		return ToolsUtil.createNewCommentedSpectrum(cs, newFrequencies, newIntensities);
	}

	/**
	 * Create a new {@link CommentedSpectrum} by doing a Smooth Gauss.
	 *
	 * @param cs The {@link CommentedSpectrum} used a base for the new spectrum after the operation.
	 * @param axis The {@link XAxisCassis} of the {@link CommentedSpectrum}.
	 * @param fwhm The FWHM used for the operation.
	 * @return The created {@link CommentedSpectrum} or null if there was an error.
	 */
	public static CommentedSpectrum doSmoothGauss(CommentedSpectrum cs,XAxisCassis axis,
			double fwhm) {
		double fwhmInMhz = ToolsUtil.convDeltaToFreq(axis, fwhm);

		double[] x = cs.getFrequenciesSignal();
		double[] y = cs.getIntensities();
		double deltaFreq;
		double[] xNew;
		double[] yNew;

		if (cs.isSamplingRegular()) {
			deltaFreq = cs.getDeltaF()[0];

			xNew = new double[x.length];
			yNew = new double[y.length];
			System.arraycopy(x, 0, xNew, 0, x.length);
			System.arraycopy(y, 0, yNew, 0, y.length);
		} else {
			deltaFreq = ToolsUtil.getLowestSampling(cs, cs, axis);
			xNew = ToolsUtil.getXFinal(XAxisCassis.getXAxisFrequency(),
					cs.getFrequencySignalMin(), cs.getFrequencySignalMax(), deltaFreq);
			// Correction for java mathematical errors.
			if (xNew[0] < x[0]) {
				xNew[0] = x[0];
			}
			if (xNew[xNew.length - 1] > x[x.length - 1]) {
				xNew[xNew.length - 1] = x[x.length - 1];
			}
			yNew = ToolsUtil.resample(xNew, x, y);
		}

		Double1d wave = new Double1d(xNew);
		Double1d flux = new Double1d(yNew);
		double xo = xNew[0] + (xNew[xNew.length - 1] - xNew[0]) / 2.;
		final double fwhmGauss = fwhmInMhz / FitConstant.getFWHM();
		double io = deltaFreq / Math.sqrt(2.0 * Math.PI) / fwhmGauss;
		GaussModel gaussModel = new GaussModel();
		double[] params = { io, xo, fwhmGauss };
		gaussModel.setParameters(new Double1d(params));
		Double1d gaussVal = gaussModel.result(wave);

		Convolution f = new Convolution(gaussVal, true, Convolution.REPEAT);
		Double1d convol = f.mutate(flux);

		yNew = convol.toArray();
		System.out.println("fwhm=" + fwhm);
		System.out.println("fwhmInMhz=" + fwhmInMhz);
		System.out.println("deltaFreq=" + deltaFreq);

		System.out.println("xo=" + xo);
		System.out.println("io=" + io);
		System.out.println("fwhmGauss=" + fwhmGauss);
		System.out.println("xNew =" + Arrays.toString(yNew));
		System.out.println("yNew =" + Arrays.toString(yNew));

		return ToolsUtil.createNewCommentedSpectrum(cs, xNew, yNew);
	}

	/**
	 * Create a new {@link XYSpectrumSeries} by doing a Smooth Box.
	 *
	 * @param serie The {@link XYSpectrumSeries} used a base for the new series after the operation.
	 * @param name The name of the new {@link XYSpectrumSeries}.
	 * @param color The color of the new series.
	 * @param boxSize The size of the box.
	 * @return The created {@link XYSpectrumSeries} or null if there was an error.
	 */
	public static XYSpectrumSeries doSmoothBox(XYSpectrumSeries serie, String name,
			Color color, int boxSize) {
		if (serie == null) {
			return null;
		}
		return ToolsUtil.createNewSerie(serie, doSmoothBox(serie.getSpectrum(), boxSize), name,
				color);
	}

	/**
	 * Do the operation needed for SmoothBox for a point when an impair box size is used.
	 *
	 * @param m The index of the point in the y array.
	 * @param n The n value.
	 * @param y The array of old points.
	 * @return The value of the point.
	 */
	private static double doSmoothBoxImpairOperation(int m, int n, double[] y) {
		double val = 0.0;
		for (int i = m - n; i <= m + n; i++) {
			val += y[i];
		}
		return val / (2 * n + 1);
	}

	/**
	 * Do the operation needed for SmoothBox for a point when a pair box size is used.
	 *
	 * @param m The index of the point in the y array.
	 * @param n The n value.
	 * @param y The array of old points.
	 * @return The value of the point.
	 */
	private static double doSmoothBoxPairOperation(int m, int n, double[] y) {
		double val = y[m - n] / 2;
		val += y[m + n] / 2;
		for (int i = m - n + 1; i <= m + n - 1; i++) {
			val += y[i];
		}
		return val / (2 * n);
	}

	/**
	 * Create a new {@link XYSpectrumSeries} by doing a Smooth Gauss.
	 *
	 * @param serie The {@link XYSpectrumSeries} used a base for the new series after the operation.
	 * @param name The name of the new {@link XYSpectrumSeries}.
	 * @param color The color of the new series.
	 * @param fwhm The FWHM used for the operation.
	 * @return The created {@link XYSpectrumSeries} or null if there was an error.
	 */
	public static XYSpectrumSeries doSmoothGauss(XYSpectrumSeries serie, String name,
			Color color, double fwhm) {
		if (serie == null) {
			return null;
		}
		return ToolsUtil.createNewSerie(serie,
				doSmoothGauss(serie.getSpectrum(), serie.getXAxis(), fwhm), name, color);
	}

	/**
	 * Create a new {@link XYSpectrumSeries} by doing a Smooth Hanning on a {@link XYSpectrumSeries}.
	 * This function is deprecated, please use {@link #doSmoothHanning(XYSpectrumSeries, String, Color)}
	 * instead.
	 *
	 * @param serie The {@link XYSpectrumSeries} used for doing the first Smooth Hanning.
	 * @param name The generic name for the created series (which is the name without the number).
	 * @param color The list of colors used for the series.
	 * @return The smoothed series, or null if there was an error.
	 */
	@Deprecated
	public static XYSpectrumSeries smoothHanning(XYSpectrumSeries serie, String name,
			Color color) {
		LOGGER.warn("ToolsUtil.smoothHanning(XYSpectrumSeries, String, Color) is "
				+ "deprecated. Use ToolsUtils.doSmoothHanning(XYSpectrumSeries,"
				+ " String, Color) instead.");
		return ToolsUtil.createNewSerie(serie, doSmoothHanning(serie.getSpectrum()), name, color);
	}

	/**
	 * Create a new {@link XYSpectrumSeries} by doing a Smooth Hanning on a {@link XYSpectrumSeries}.
	 *
	 * @param serie The {@link XYSpectrumSeries} used for doing the first Smooth Hanning.
	 * @param name The generic name for the created series (which is the name without the number).
	 * @param color The list of colors used for the series.
	 * @return The smoothed series, or null if there was an error.
	 */
	public static XYSpectrumSeries doSmoothHanning(XYSpectrumSeries serie, String name,
			Color color) {
		return ToolsUtil.createNewSerie(serie, ToolsSmooth.doSmoothHanning(serie.getSpectrum()), name, color);
	}


	/**
	 * Create a new {@link CommentedSpectrum} by doing a Smooth Hanning on a {@link CommentedSpectrum}.
	 *
	 * @param cs The {@link CommentedSpectrum} used for doing the first Smooth Hanning.
	 * @return The smoothed {@link CommentedSpectrum}, or null if there was an error.
	 */
	public static CommentedSpectrum doSmoothHanning(CommentedSpectrum cs) {
		int nbItemInSerie = cs.getFrequenciesSignal().length;
		if (nbItemInSerie <= 6) {
			LOGGER.error("Not enough points to do a smoothHanning. Exiting.");
			return null;
		}

		int nbNewItem = -1;
		if (nbItemInSerie % 2 == 0) {
			nbNewItem = nbItemInSerie / 2 - 1;
		} else {
			nbNewItem = nbItemInSerie / 2;
		}
		double[] x = new double[nbNewItem];
		double[] y = new double[nbNewItem];

		int j = 0;
		for (int i = 1; i < nbItemInSerie - 1; i += 2) {
			x[j] = cs.getFrequenciesSignal()[i];
			y[j] = cs.getIntensities()[i - 1] / 4. + cs.getIntensities()[i] / 2.
					+ cs.getIntensities()[i + 1] / 4.;
			j++;
		}

		return ToolsUtil.createNewCommentedSpectrum(cs, x, y);
	}
}
