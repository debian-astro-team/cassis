/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.tools;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.InputVerifier;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.TypeFrequency;
import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.gui.util.ExtentedJComboBox;
import eu.omp.irap.cassis.gui.util.GbcUtil;
import eu.omp.irap.cassis.gui.util.MyTelescopeButton;
import eu.omp.irap.cassis.gui.util.TextFieldFormatFilter;
import eu.omp.irap.cassis.properties.Software;

/**
 * View class for the tools functionality.
 *
 * @author M. Boiziot
 */
public class ToolsView extends JPanel {

	private static final long serialVersionUID = 546895662237075168L;
	private static final Logger LOGGER = LoggerFactory.getLogger(ToolsView.class);
	public static final String TOOLS_TELESCOPE_CHANGE_EVENT = "toolsTelescopeChange";

	private ToolsControl control;

	private JComboBox<String> jComboBoxDataFirst;
	private JComboBox<String> jComboBoxDataSecond;
	private JComboBox<String> jComboBoxAllCenter;
	private JComboBox<String> jComboBoxAllCenterSecond;
	private JComboBox<String> jComboBoxRest;
	private JComboBox<String> jComboBoxSky;

	private JTextField constantCassisTextField;

	private JComboBox<Operation> operationComboBox;
	private JComboBox<Operation> smoothOperationComboBox;
	private JComboBox<Operation> resampleOperationComboBox;

	private JPanel paramPanel;
	private JButton displayButton;

	private JCheckBox advancedSubtractCheckBox;
	private JCheckBox advancedSmoothHanningCheckBox;
	private JCheckBox avoidResamplingCheckBox;

	private JTextField xMinTextField;
	private JTextField xMaxTextField;
	private JTextField samplingTextField;
	private JTextField initialSampleTextField;
	private JTextField finalSampleTextField;
	private JTextField vlsrTextField;

	private JTextField smoothHanningTextField;

	private JButton telescopeButton;

	private JTextField integerTextField;


	/**
	 * Constructor of ToolsView.
	 *
	 * @param theModel The model who implements ToolsModelsInterface.
	 */
	public ToolsView(ToolsModelsInterface theModel) {
		control = new ToolsControl(this, theModel);
		createPanel();
	}

	/**
	 * @return The controller
	 */
	public ToolsControl getControl() {
		return control;
	}

	/**
	 * Create the global panel.
	 */
	private void createPanel() {
		setLayout(new BorderLayout());
		add(getActionPanel(), BorderLayout.NORTH);
		add(getParamPanel(), BorderLayout.CENTER);
		add(getDisplayButton(), BorderLayout.SOUTH);
	}

	/**
	 * @return The parameters panel.
	 */
	private JPanel getParamPanel() {
		if (paramPanel == null) {
			paramPanel = new JPanel(new BorderLayout());
			paramPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Parameters"));
		}
		return paramPanel;
	}

	/**
	 * @return The telescope button.
	 */
	public JButton getTelescopeButton() {
		if (telescopeButton == null) {
			telescopeButton = new MyTelescopeButton(
					control.getModel().getTelescopeModel().getSelectedTelescope());
			telescopeButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					JFileChooser loadFile = new CassisJFileChooser(Software.getTelescopePath(),
							Software.getLastFolder("tools-telescope"));

					loadFile.setDialogTitle("Select a telescope file");
					int ret = loadFile.showOpenDialog(ToolsView.this);
					if (ret == JFileChooser.APPROVE_OPTION) {
						control.getModel().getTelescopeModel().setSelectedTelescope(
								loadFile.getSelectedFile().getAbsolutePath(),
								TOOLS_TELESCOPE_CHANGE_EVENT);
						Software.setLastFolder("tools-telescope", loadFile.getCurrentDirectory().getAbsolutePath());
					}
				}
			});
		}
		return telescopeButton;
	}

	/**
	 * @return the parameters panel for ta/tmb change.
	 */
	private JComponent getTelescopeParametersPanel() {
		JPanel panel = new JPanel(new BorderLayout());

		JPanel centerPanel = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		GbcUtil.configureFillGrids(gbc, GridBagConstraints.CENTER, 0, 0);
		centerPanel.add(new JLabel("Spectrum:   "), gbc);
		GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 0);
		centerPanel.add(getJComboBoxAllCenter(), gbc);
		GbcUtil.configureFillGrids(gbc, GridBagConstraints.CENTER, 0, 1);
		centerPanel.add(new JLabel("Telescope:   "), gbc);
		GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 1);
		centerPanel.add(getTelescopeButton(), gbc);

		panel.add(centerPanel, BorderLayout.CENTER);

		return panel;
	}

	/**
	 * @return The parameters panel for two spectrum operation.
	 */
	private JComponent getTwoSpectrumParametersPanel(Operation operation) {
		JPanel panel = new JPanel(new BorderLayout());

		JPanel centerPanel = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		GbcUtil.configureFillGrids(gbc, GridBagConstraints.CENTER, 0, 0);
		centerPanel.add(new JLabel("Spectrum 1:   "), gbc);
		GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 0);
		centerPanel.add(getJComboBoxDataFirst(), gbc);
		GbcUtil.configureFillGrids(gbc, GridBagConstraints.CENTER, 0, 1);
		centerPanel.add(new JLabel("Spectrum 2:   "), gbc);
		GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 1);
		centerPanel.add(getJComboBoxDataSecond(), gbc);

		JLabel label = null;
		if (operation == Operation.SUBTRACT) {
			label = new JLabel("              Result = Spectrum 1 - Spectrum 2");
		} else if (operation == Operation.ADD) {
			label = new JLabel("              Result = Spectrum 1 + Spectrum 2");
		} else if (operation == Operation.AVERAGE_SPECTRA) {
			label = new JLabel("              Result = (Spectrum 1 + Spectrum 2) / 2");
		} else if (operation == Operation.DIVIDE) {
			label = new JLabel("              Result = Spectrum 1 / Spectrum 2");
		}

		panel.add(label, BorderLayout.NORTH);
		panel.add(centerPanel, BorderLayout.CENTER);
		if (control.getModel().isAdvancedParametersTwoSpectrumAllowed()) {
			JPanel aPane = new JPanel(new BorderLayout());
			aPane.add(getAdvancedSubtractCheckBox(), BorderLayout.EAST);
			GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 2);
			centerPanel.add(aPane, gbc);
			if (control.getModel().getAdvancedSubtractState()) {
				GbcUtil.configureFillGrids(gbc, GridBagConstraints.CENTER, 1, 3);
				centerPanel.add(getAvoidResamplingCheckBox(), gbc);
				GbcUtil.configureFillGrids(gbc, GridBagConstraints.CENTER, 0, 4);
				centerPanel.add(new JLabel("X Min:   "), gbc);
				GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 4);
				centerPanel.add(getXMinTextField(), gbc);
				GbcUtil.configureFillGrids(gbc, GridBagConstraints.CENTER, 0, 5);
				centerPanel.add(new JLabel("X Max:   "), gbc);
				GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 5);
				centerPanel.add(getXMaxTextField(), gbc);
				GbcUtil.configureFillGrids(gbc, GridBagConstraints.CENTER, 0, 6);
				centerPanel.add(new JLabel("Sampling Min:   "), gbc);
				GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 6);
				centerPanel.add(getSamplingTextField(), gbc);
				GbcUtil.configureGrids(gbc, 0, 7, 2);
				centerPanel.add(new JLabel("        Units are those of X-bottom axis"), gbc);
			}
		}

		return panel;
	}

	/**
	 * @return The first JComboBox.
	 */
	public JComboBox<String> getJComboBoxDataFirst() {
		if (jComboBoxDataFirst == null) {
			List<String> list = control.getModel().getCurveOneName();
			String[] items = list.toArray(new String[list.size()]);
			jComboBoxDataFirst = new ExtentedJComboBox<>(items);
			jComboBoxDataFirst.setPrototypeDisplayValue("XXXXXXXXXXXXXXXXX");
			jComboBoxDataFirst.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					if (jComboBoxDataFirst.getSelectedItem() != null) {
						control.getModel().setSelectedSpectrumOne(
								(String)jComboBoxDataFirst.getSelectedItem());
					}
					control.refreshInfo();
				}
			});
		}
		return jComboBoxDataFirst;
	}

	/**
	 * @return The second JComboBox.
	 */
	public JComboBox<String> getJComboBoxDataSecond() {
		if (jComboBoxDataSecond == null) {
			List<String> list = control.getModel().getCurveTwoName();
			String[] items = list.toArray(new String[list.size()]);
			jComboBoxDataSecond = new ExtentedJComboBox<>(items);
			jComboBoxDataSecond.setPrototypeDisplayValue("XXXXXXXXXXXXXXXXX");
			jComboBoxDataSecond.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					if (jComboBoxDataSecond.getSelectedItem() != null) {
						control.getModel().setSelectedSpectrumTwo(
								(String)jComboBoxDataSecond.getSelectedItem());
					}
					control.refreshInfo();
				}
			});
		}
		return jComboBoxDataSecond;
	}

	/**
	 * @return The JComboBox with all spectra name from centerPlot
	 */
	public JComboBox<String> getJComboBoxAllCenter() {
		if (jComboBoxAllCenter == null) {
			List<String> list = control.getModel().getCurveAllName();
			String[] items = list.toArray(new String[list.size()]);
			jComboBoxAllCenter = new ExtentedJComboBox<>(items);
			jComboBoxAllCenter.setPrototypeDisplayValue("XXXXXXXXXXXXXXXXX");
			jComboBoxAllCenter.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					if (jComboBoxAllCenter.getSelectedItem() != null) {
						control.getModel().setSelectedSpectrumAll(
								(String)jComboBoxAllCenter.getSelectedItem());
					}
				}
			});
		}
		return jComboBoxAllCenter;
	}

	/**
	 * Refresh the first JComboBox with the data from the model.
	 */
	private void refreshJComboBoxDataFirst() {
		getJComboBoxDataFirst().removeAllItems();
		List<String> list = control.getModel().getCurveOneName();
		String[] items = list.toArray(new String[list.size()]);

		getJComboBoxDataFirst().setModel(new DefaultComboBoxModel<>(items));
		if (list.contains(control.getModel().getSelectedSpectrumOne())) {
			getJComboBoxDataFirst().setSelectedItem(
					control.getModel().getSelectedSpectrumOne());
		}
	}

	/**
	 * Refresh the second JComboBox with the data from the model.
	 */
	private void refreshJComboBoxDataSecond() {
		getJComboBoxDataSecond().removeAllItems();
		List<String> list = control.getModel().getCurveTwoName();
		String[] items = list.toArray(new String[list.size()]);

		getJComboBoxDataSecond().setModel(new DefaultComboBoxModel<>(items));
		if (list.contains(control.getModel().getSelectedSpectrumTwo())) {
			getJComboBoxDataSecond().setSelectedItem(
					control.getModel().getSelectedSpectrumTwo());
		}
	}

	/**
	 * Return the action panel.
	 * @return The action panel.
	 */
	private JComponent getActionPanel() {
		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Action"));
		panel.setLayout(new BorderLayout());
		panel.add(getOperationComboBox(), BorderLayout.CENTER);
		panel.add(new JLabel(" "), BorderLayout.SOUTH);

		return panel;
	}

	/**
	 * Create if needed a JComboBox used for selecting the operation then return it..
	 *
	 * @return operationComboBox the combobox used to change the Operation.
	 */
	private JComboBox<Operation> getOperationComboBox() {
		if (operationComboBox == null) {
			operationComboBox = new JComboBox<>(Operation.getMenuOperations());
			operationComboBox.setMaximumRowCount(10);
			operationComboBox.setSelectedItem(control.getModel().getActionSelected());
			operationComboBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					Operation selectedOperation = (Operation) operationComboBox.getSelectedItem();
					control.getModel().setActionSelected(selectedOperation);
					refresh();
				}
			});
		}
		return operationComboBox;
	}

	/**
	 * Create if needed then return a JComboBox used for selecting the smooth operation.
	 *
	 * @return smoothOperationComboBox the combobox used to change the smooth Operation.
	 */
	public JComboBox<Operation> getSmoothOperationComboBox() {
		if (smoothOperationComboBox == null) {
			smoothOperationComboBox = new JComboBox<>(Operation.getSmoothOperations());
			smoothOperationComboBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					refresh();
				}
			});
		}
		return smoothOperationComboBox;
	}

	/**
	 * Change the parameter panel according to the Operation given.
	 *
	 * @param Operation The Operation.
	 */
	private void changeParameterPanel(Operation operation) {
		getParamPanel().removeAll();
		if (operation == Operation.SUBTRACT || operation == Operation.ADD ||
				operation == Operation.AVERAGE_SPECTRA || operation == Operation.DIVIDE) {
			getParamPanel().add(getTwoSpectrumParametersPanel(operation), BorderLayout.CENTER);
		} else if (operation == Operation.ADD_CONSTANT || operation == Operation.MULTIPLY_CONSTANT ||
				operation == Operation.SKY_TO_REST || operation == Operation.VLSR ||
				operation == Operation.REDSHIFT){
			getParamPanel().add(getSpectrumDoubleParametersPanel(operation), BorderLayout.CENTER);
		} else if (operation == Operation.REST_TO_SKY || operation == Operation.AIR_TO_VACUUM ||
				operation == Operation.VACUUM_TO_AIR) {
			getParamPanel().add(getSimpleSpectrumParametersPanel(), BorderLayout.CENTER);
		} else if (operation == Operation.TA_TO_TMB || operation == Operation.TMB_TO_TA) {
			getParamPanel().add(getTelescopeParametersPanel(), BorderLayout.CENTER);
		} else if (operation == Operation.SMOOTH) {
			getParamPanel().add(getSmoothParametersPanel(), BorderLayout.CENTER);
		} else if (operation == Operation.RESAMPLE) {
			getParamPanel().add(getResampleParametersPanel(), BorderLayout.CENTER);
		}
		revalidate();
	}

	/**
	 * @return The parameters panel for smooth operations.
	 */
	private JComponent getSmoothParametersPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		JPanel centerPanel = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		GbcUtil.configureFillGrids(gbc, GridBagConstraints.CENTER, 0, 0);
		centerPanel.add(new JLabel("Smooth type :   "), gbc);
		GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 0);
		centerPanel.add(getSmoothOperationComboBox(), gbc);
		GbcUtil.configureFillGrids(gbc, GridBagConstraints.CENTER, 0, 1);
		centerPanel.add(new JLabel("Spectrum:   "), gbc);
		GbcUtil.configureFillGrids(gbc, GridBagConstraints.CENTER, 1, 1);
		centerPanel.add(getJComboBoxAllCenter(), gbc);

		Operation smoothOperation = (Operation) getSmoothOperationComboBox().getSelectedItem();
		if (smoothOperation == Operation.SMOOTH_BOX) {
			GbcUtil.configureFillGrids(gbc, GridBagConstraints.CENTER, 0, 2);
			centerPanel.add(new JLabel("Box size:   "), gbc);
			GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 2);
			centerPanel.add(getIntegerTextField(), gbc);
			GbcUtil.configureGrids(gbc, 0, 3, 2);
			centerPanel.add(new JLabel("                  In number of channels"), gbc);
		} else if (smoothOperation == Operation.SMOOTH_GAUSS) {
			GbcUtil.configureFillGrids(gbc, GridBagConstraints.CENTER, 0, 2);
			centerPanel.add(new JLabel("FWHM:   "), gbc);
			GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 2);
			centerPanel.add(getConstantCassisTextField(), gbc);
			GbcUtil.configureGrids(gbc, 0, 3, 2);
			centerPanel.add(new JLabel("         Units are those of X-bottom axis"), gbc);
		} else if (smoothOperation == Operation.SMOOTH_HANNING) {
			if (control.getModel().isAdvancedSmoothHanningAllowed()) {
				JPanel aPane = new JPanel(new BorderLayout());
				aPane.add(getAdvancedSmoothHanningCheckBox(), BorderLayout.EAST);
				GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 2);
				centerPanel.add(aPane, gbc);
				if (control.getModel().getAdvancedSmoothHanningState()) {
					GbcUtil.configureFillGrids(gbc, GridBagConstraints.CENTER, 0, 3);
					centerPanel.add(new JLabel("Iteration: "), gbc);
					GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 3);
					centerPanel.add(getSHITextField(), gbc);
				}
			}
		}

		panel.add(centerPanel, BorderLayout.CENTER);

		return panel;
	}

	/**
	 * @return The parameters panel for one spectrum operation.
	 */
	private JComponent getSimpleSpectrumParametersPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		JPanel centerPanel = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		GbcUtil.configureFillGrids(gbc, GridBagConstraints.CENTER, 0, 0);
		centerPanel.add(new JLabel("Spectrum :   "), gbc);
		GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 0);
		Operation operation = (Operation) getOperationComboBox().getSelectedItem();
		if (operation == Operation.REST_TO_SKY) {
			centerPanel.add(getJComboBoxRest(), gbc);
			JLabel label = new JLabel("              Change REST Spectrum to SKY.");
			panel.add(label, BorderLayout.NORTH);
		} else { // Operation.AIR_TO_VACUUM or Operation.VACUUM_TO_AIR
			centerPanel.add(getJComboBoxAllCenter(), gbc);
		}

		panel.add(centerPanel, BorderLayout.CENTER);

		return panel;
	}

	/**
	 * Create a panel for the operation between a spectrum and a double value
	 * for a given {@link Operation}.
	 *
	 * @param operation The Operation.
	 * @return The parameters panel for mathematical operation on a spectrum and a double value.
	 */
	private JComponent getSpectrumDoubleParametersPanel(Operation operation) {
		JPanel panel = new JPanel(new BorderLayout());

		JPanel centerPanel = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		GbcUtil.configureFillGrids(gbc, GridBagConstraints.CENTER, 0, 0);
		centerPanel.add(new JLabel("Spectrum:   "), gbc);
		GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 0);
		if (operation == Operation.SKY_TO_REST || operation == Operation.REDSHIFT) {
			centerPanel.add(getJComboBoxSky(), gbc);
		} else {
			centerPanel.add(getJComboBoxAllCenter(), gbc);
		}
		GbcUtil.configureFillGrids(gbc, GridBagConstraints.CENTER, 0, 2);
		if (operation == Operation.ADD_CONSTANT || operation == Operation.MULTIPLY_CONSTANT ||
				operation == Operation.REDSHIFT) {
		centerPanel.add(new JLabel("Constant:   "), gbc);
		GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 2);
		centerPanel.add(getConstantCassisTextField(), gbc);
		} else if (operation == Operation.SKY_TO_REST || operation == Operation.VLSR) {
			centerPanel.add(new JLabel("Vlsr:   "), gbc);
			GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 2);
			centerPanel.add(getVlsrTextField(), gbc);
		}

		JLabel label = null;
		if (operation == Operation.ADD_CONSTANT) {
			label = new JLabel("              Result = Spectrum + Constant");
		} else if (operation == Operation.MULTIPLY_CONSTANT) {
			label = new JLabel("              Result = Spectrum * Constant");
		} else if (operation == Operation.SKY_TO_REST) {
			label = new JLabel("              Change SKY Spectrum to REST.");
		} else if (operation == Operation.VLSR) {
			label = new JLabel("                           Change Vlsr.");
		} else if (operation == Operation.REDSHIFT) {
			label = new JLabel("                        Apply redshift.");
		} else {
			label = new JLabel("");
		}
		panel.add(label, BorderLayout.NORTH);
		panel.add(centerPanel, BorderLayout.CENTER);

		return panel;
	}

	/**
	 * Refresh the view.
	 */
	public void refresh() {
		Operation op = control.getModel().getActionSelected();
		getOperationComboBox().setSelectedItem(op);
		changeParameterPanel(op);
		if (op == Operation.SUBTRACT || op == Operation.ADD ||
				op == Operation.AVERAGE_SPECTRA || op == Operation.DIVIDE) {
			refreshJComboBoxDataFirst();
			refreshJComboBoxDataSecond();
		} else if (op == Operation.ADD_CONSTANT || op == Operation.MULTIPLY_CONSTANT ||
				op == Operation.SMOOTH || op == Operation.VLSR ||
				op == Operation.AIR_TO_VACUUM || op == Operation.VACUUM_TO_AIR) {
			refreshJComboBoxAllCenter();
		} else if (op == Operation.REST_TO_SKY) {
			refreshJComboBoxRest();
		} else if (op == Operation.SKY_TO_REST || op == Operation.REDSHIFT) {
			refreshJComboBoxSky();
		} else if (op == Operation.TA_TO_TMB || op == Operation.TMB_TO_TA) {
			refreshJComboBoxAllCenter();
			refreshTelescopeButton();
		} else if (op == Operation.RESAMPLE) {
			refreshJComboBoxAllCenter();
			refreshJComboBoxAllCenterSecond();
		}
	}

	/**
	 * Refresh the AllCenter JComboBox with the data from the model.
	 */
	private void refreshJComboBoxAllCenter() {
		getJComboBoxAllCenter().removeAllItems();
		List<String> list = control.getModel().getCurveAllName();
		String[] items = list.toArray(new String[list.size()]);

		getJComboBoxAllCenter().setModel(new DefaultComboBoxModel<>(items));
		if (list.contains(control.getModel().getSelectedSpectrumAll())) {
			getJComboBoxAllCenter().setSelectedItem(
					control.getModel().getSelectedSpectrumAll());
		}
	}

	/**
	 * Refresh the Rest JComboBox with the data from the model.
	 */
	private void refreshJComboBoxRest() {
		getJComboBoxRest().removeAllItems();
		List<String> list = control.getModel().getAllCurveOfType(TypeFrequency.REST);
		String[] items = list.toArray(new String[list.size()]);

		getJComboBoxRest().setModel(new DefaultComboBoxModel<>(items));
		if (list.contains(control.getModel().getSelectedSpectrumRest())) {
			getJComboBoxRest().setSelectedItem(
					control.getModel().getSelectedSpectrumRest());
		}
	}

	/**
	 * Refresh the Sky JComboBox with the data from the model.
	 */
	private void refreshJComboBoxSky() {
		getJComboBoxSky().removeAllItems();
		List<String> list = control.getModel().getAllCurveOfType(TypeFrequency.SKY);
		String[] items = list.toArray(new String[list.size()]);

		getJComboBoxSky().setModel(new DefaultComboBoxModel<>(items));
		if (list.contains(control.getModel().getSelectedSpectrumSky())) {
			getJComboBoxSky().setSelectedItem(
					control.getModel().getSelectedSpectrumSky());
		}
	}

	/**
	 * @return The display button.
	 */
	public JButton getDisplayButton() {
		if (displayButton == null) {
			displayButton = new JButton("Display");
			displayButton.setMinimumSize(new Dimension(100, 60));
			displayButton.setPreferredSize(new Dimension(100, 60));
			displayButton.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
			displayButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.displayClicked();
				}
			});
		}
		return displayButton;
	}

	/**
	 * @return The constant text field.
	 */
	public JTextField getConstantCassisTextField() {
		if (constantCassisTextField == null) {
			constantCassisTextField = new JTextField("0.0");
			configureDoubleTextField(constantCassisTextField);
			constantCassisTextField.setColumns(7);
		}
		return constantCassisTextField;
	}

	/**
	 * @return The JTextField for the XMIN value.
	 */
	public JTextField getXMinTextField() {
		if (xMinTextField == null) {
			xMinTextField = new JTextField();
			configureDoubleTextField(xMinTextField);
		}
		return xMinTextField;
	}

	/**
	 * @return The JTextField for the XMIN value.
	 */
	public JTextField getXMaxTextField() {
		if (xMaxTextField == null) {
			xMaxTextField = new JTextField();
			configureDoubleTextField(xMaxTextField);
		}
		return xMaxTextField;
	}

	/**
	 * @return The JTextField of the sampling value.
	 */
	public JTextField getSamplingTextField() {
		if (samplingTextField == null) {
			samplingTextField = new JTextField();
			configureDoubleTextField(samplingTextField);
		}
		return samplingTextField;
	}

	/**
	 * @return The JTextField of the iteration number on Advanced SmoothHanning.
	 */
	public JTextField getSHITextField() {
		if (smoothHanningTextField == null) {
			smoothHanningTextField = new JTextField(10);
			smoothHanningTextField.setText("1");
			final TextFieldFormatFilter galleryFilter = new TextFieldFormatFilter(
					TextFieldFormatFilter.INTEGER, "1", TextFieldFormatFilter.POSITIVE);
			smoothHanningTextField.addKeyListener(galleryFilter);
		}
		return smoothHanningTextField;
	}

	/**
	 * @return The JCheckBox for the advanced subtract.
	 */
	public JCheckBox getAdvancedSubtractCheckBox() {
		if (advancedSubtractCheckBox == null) {
			advancedSubtractCheckBox = new JCheckBox("Advanced");
			advancedSubtractCheckBox.setSelected(control.getModel().getAdvancedSubtractState());
			advancedSubtractCheckBox.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					control.getModel().setAdvancedSubtractState(advancedSubtractCheckBox.isSelected());
					refresh();
					control.refreshInfo();
				}
			});
		}
		return advancedSubtractCheckBox;
	}

	/**
	 * @return The JCheckBox for the advanced smooth hanning.
	 */
	public JCheckBox getAdvancedSmoothHanningCheckBox() {
		if (advancedSmoothHanningCheckBox == null) {
			advancedSmoothHanningCheckBox = new JCheckBox("Advanced");
			advancedSmoothHanningCheckBox.setSelected(control.getModel().getAdvancedSmoothHanningState());
			advancedSmoothHanningCheckBox.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					control.getModel().setAdvancedSmoothHanningState(advancedSmoothHanningCheckBox.isSelected());
					refresh();
				}
			});
		}
		return advancedSmoothHanningCheckBox;
	}

	/**
	 * @return The JTextField of the vlsr value.
	 */
	public JTextField getVlsrTextField() {
		if (vlsrTextField == null) {
			vlsrTextField = new JTextField();
			configureDoubleTextField(vlsrTextField);
		}
		return vlsrTextField;
	}

	/**
	 * @return The JComboBox with the REST curve.
	 */
	public JComboBox<String> getJComboBoxRest() {
		if (jComboBoxRest == null) {
			List<String> list = control.getModel().getAllCurveOfType(TypeFrequency.REST);
			String[] items = list.toArray(new String[list.size()]);
			jComboBoxRest = new ExtentedJComboBox<>(items);
			jComboBoxRest.setPrototypeDisplayValue("XXXXXXXXXXXXXXXXX");
			jComboBoxRest.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (jComboBoxRest.getSelectedItem() != null) {
						control.getModel().setSelectedSpectrumRest(
								(String)jComboBoxRest.getSelectedItem());
					}
				}
			});
		}
		return jComboBoxRest;
	}

	/**
	 * @return The first JComboBox with the SKY curve.
	 */
	public JComboBox<String> getJComboBoxSky() {
		if (jComboBoxSky == null) {
			List<String> list = control.getModel().getAllCurveOfType(TypeFrequency.SKY);
			String[] items = list.toArray(new String[list.size()]);
			jComboBoxSky = new ExtentedJComboBox<>(items);
			jComboBoxSky.setPrototypeDisplayValue("XXXXXXXXXXXXXXXXX");
			jComboBoxSky.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (jComboBoxSky.getSelectedItem() != null) {
						control.getModel().setSelectedSpectrumSky(
								(String)jComboBoxSky.getSelectedItem());
					}
				}
			});
		}
		return jComboBoxSky;
	}

	/**
	 * Configure a {@link JTextField} where we need a Double value.
	 *
	 * @param aJTextField The {@link JTextField} to configure.
	 */
	private void configureDoubleTextField(JTextField aJTextField) {
		aJTextField.setHorizontalAlignment(JTextField.RIGHT);
		aJTextField.setInputVerifier(new InputVerifier() {
			@Override
			public boolean verify(JComponent comp) {
				JTextField textField = (JTextField) comp;
				if (isContentANumber(textField)) {
					textField.setBackground(Color.WHITE);
					return true;
				} else {
					textField.setBackground(Color.RED);
					return false;
				}
			}
		});
	}

	/**
	 * Refresh the Telescope button from de model.
	 */
	public void refreshTelescopeButton() {
		getTelescopeButton().setText(control.getModel().getTelescopeModel().getSelectedTelescope());
	}

	/**
	 * @return The JTextField of an integer value.
	 */
	public JTextField getIntegerTextField() {
		if (integerTextField == null) {
			integerTextField = new JTextField("1");
			final TextFieldFormatFilter integerFilter = new TextFieldFormatFilter(
					TextFieldFormatFilter.INTEGER, "1", TextFieldFormatFilter.POSITIVE);
			integerTextField.addKeyListener(integerFilter);
		}
		return integerTextField;
	}

	/**
	 * Create if needed then return a JComboBox used for selecting the resample operation.
	 *
	 * @return resampleOperationComboBox the combobox used to change the resample Operation.
	 */
	public JComboBox<Operation> getResampleOperationComboBox() {
		if (resampleOperationComboBox == null) {
			resampleOperationComboBox = new JComboBox<>(Operation.getResampleOperations());
			resampleOperationComboBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					refresh();
				}
			});
		}
		return resampleOperationComboBox;
	}

	/**
	 * @return The JTextField of the initial sample value.
	 */
	public JTextField getInitialSampleTextField() {
		if (initialSampleTextField == null) {
			initialSampleTextField = new JTextField();
			configureDoubleTextField(initialSampleTextField);
		}
		return initialSampleTextField;
	}

	/**
	 * @return The JTextField of the final sample value.
	 */
	public JTextField getFinalSampleTextField() {
		if (finalSampleTextField == null) {
			finalSampleTextField = new JTextField();
			configureDoubleTextField(finalSampleTextField);
		}
		return finalSampleTextField;
	}

	/**
	 * @return The JComboBox with all spectra name from centerPlot
	 */
	public JComboBox<String> getJComboBoxAllCenterSecond() {
		if (jComboBoxAllCenterSecond == null) {
			List<String> list = control.getModel().getCurveAllName();
			String[] items = list.toArray(new String[list.size()]);
			jComboBoxAllCenterSecond = new ExtentedJComboBox<>(items);
			jComboBoxAllCenterSecond.setPrototypeDisplayValue("XXXXXXXXXXXXXXXXX");
			jComboBoxAllCenterSecond.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					if (jComboBoxAllCenterSecond.getSelectedItem() != null) {
						control.getModel().setSelectedSpectrumAllSecond(
								(String)jComboBoxAllCenterSecond.getSelectedItem());
					}
				}
			});
		}
		return jComboBoxAllCenterSecond;
	}

	/**
	 * Refresh the AllCenterSecond JComboBox with the data from the model.
	 */
	private void refreshJComboBoxAllCenterSecond() {
		getJComboBoxAllCenterSecond().removeAllItems();
		List<String> list = control.getModel().getCurveAllName();
		String[] items = list.toArray(new String[list.size()]);

		getJComboBoxAllCenterSecond().setModel(new DefaultComboBoxModel<>(items));
		if (list.contains(control.getModel().getSelectedSpectrumAllSecond())) {
			getJComboBoxAllCenterSecond().setSelectedItem(
					control.getModel().getSelectedSpectrumAllSecond());
		}
	}

	/**
	 * @return The parameters panel for resample operations.
	 */
	private JComponent getResampleParametersPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		JPanel centerPanel = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		GbcUtil.configureFillGrids(gbc, GridBagConstraints.CENTER, 0, 0);
		centerPanel.add(new JLabel("Resample type:   "), gbc);
		GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 0);
		centerPanel.add(getResampleOperationComboBox(), gbc);
		GbcUtil.configureFillGrids(gbc, GridBagConstraints.CENTER, 0, 1);
		centerPanel.add(new JLabel("Spectrum:   "), gbc);
		GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 1);
		centerPanel.add(getJComboBoxAllCenter(), gbc);
		GbcUtil.configureFillGrids(gbc, GridBagConstraints.CENTER, 0, 2);
		Operation resampleOperation = (Operation) getResampleOperationComboBox().getSelectedItem();
		if (resampleOperation == Operation.RESAMPLE_AUTOMATIC) {
			centerPanel.add(new JLabel("Sampling:   "), gbc);
			GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 2);
			centerPanel.add(getSamplingTextField(), gbc);
			GbcUtil.configureGrids(gbc, 0, 3, 2);
			centerPanel.add(new JLabel("       Units are those of X-bottom axis"), gbc);
		} else if (resampleOperation == Operation.RESAMPLE_ADVANCED) {
			centerPanel.add(new JLabel("Sampling:   "), gbc);
			GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 2);
			centerPanel.add(getSamplingTextField(), gbc);
			GbcUtil.configureFillGrids(gbc, GridBagConstraints.CENTER, 0, 3);
			centerPanel.add(new JLabel("Initial sample: "), gbc);
			GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 3);
			centerPanel.add(getInitialSampleTextField(), gbc);
			GbcUtil.configureFillGrids(gbc, GridBagConstraints.CENTER, 0, 4);
			centerPanel.add(new JLabel("Final sample: "), gbc);
			GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 4);
			centerPanel.add(getFinalSampleTextField(), gbc);
			GbcUtil.configureGrids(gbc, 0, 5, 2);
			centerPanel.add(new JLabel("       Units are those of X-bottom axis"), gbc);
		} else if (resampleOperation == Operation.RESAMPLE_GRID) {
			centerPanel.add(new JLabel("Regriding spectrum: "), gbc);
			GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 2);
			centerPanel.add(getJComboBoxAllCenterSecond(), gbc);
		}

		panel.add(centerPanel, BorderLayout.CENTER);

		return panel;
	}

	/**
	 * Check if the {@link JTextField} content is a number.
	 *
	 * @param textField The {@link JTextField} to check.
	 * @return true if the content is a number, false otherwise.
	 */
	public static boolean isContentANumber(JTextField textField) {
		try {
			Double.parseDouble(textField.getText());
			return true;
		} catch (NumberFormatException nfe) {
			LOGGER.warn("The content must be a number", nfe);
			return false;
		}
	}

	/**
	 * Create if needed then return the "avoid resampling" CheckBox.
	 *
	 * @return the "avoid resampling" CheckBox.
	 */
	public JCheckBox getAvoidResamplingCheckBox() {
		if (avoidResamplingCheckBox == null) {
			avoidResamplingCheckBox = new JCheckBox("Avoid resampling", true);
			avoidResamplingCheckBox.setToolTipText("Avoid resampling if it is not needed");
		}
		return avoidResamplingCheckBox;
	}

}
