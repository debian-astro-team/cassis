/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.tools;


/**
 * Enumeration of the operations who can be used on tools.
 *
 * @author M. Boiziot
 */
public enum Operation {

	SUBTRACT("Subtract"),
	ADD("Add spectra"),
	DIVIDE("Divide spectra"),
	ADD_CONSTANT("Add constant"),
	MULTIPLY_CONSTANT("Multiply constant"),
	SMOOTH("Smooth"), // Not really an operation, but used for menu.
	SMOOTH_BOX("Smooth Box"),
	SMOOTH_GAUSS("Smooth Gauss"),
	SMOOTH_HANNING("Smooth Hanning"),
	AVERAGE_SPECTRA("Average spectra"),
	RESAMPLE("Resample"), // Not really an operation, but used for menu.
	RESAMPLE_AUTOMATIC("Automatic resample"),
	RESAMPLE_ADVANCED("Advanced resample"),
	RESAMPLE_GRID("Resample to a grid"),
	VLSR("Change Vlsr"),
	TA_TO_TMB("TA to TMB"),
	TMB_TO_TA("TMB to TA"),
	REST_TO_SKY("Change REST to SKY"),
	SKY_TO_REST("Change SKY to REST"),
	AIR_TO_VACUUM("Wavelength conversion air to vacuum"),
	VACUUM_TO_AIR("Wavelength conversion vacuum to air"),
	REDSHIFT("Apply RedShift");

	private String label;


	/**
	 * Creates an Operation.
	 *
	 * @param label The name of the operation
	 */
	private Operation(String label) {
		this.label = label;
	}

	/**
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return label;
	}

	/**
	 * Return the operations used for the menu (exclude the subOperations).
	 *
	 * @return the operations used for the menu.
	 */
	public static Operation[] getMenuOperations() {
		return new Operation[]{
				SUBTRACT,
				ADD,
				DIVIDE,
				ADD_CONSTANT,
				MULTIPLY_CONSTANT,
				SMOOTH,
				AVERAGE_SPECTRA,
				RESAMPLE,
				VLSR,
				TA_TO_TMB,
				TMB_TO_TA,
				REST_TO_SKY,
				SKY_TO_REST,
				AIR_TO_VACUUM,
				VACUUM_TO_AIR,
				REDSHIFT};
	}

	/**
	 * Return the Smooth {@link Operation}s (sub-operations).
	 *
	 * @return the Smooth {@link Operation}s.
	 */
	public static Operation[] getSmoothOperations() {
		Operation[] ops = new Operation[3];
		ops[0] = SMOOTH_BOX;
		ops[1] = SMOOTH_GAUSS;
		ops[2] = SMOOTH_HANNING;
		return ops;
	}

	/**
	 * Return the Resample {@link Operation}s (sub-operations)
	 *
	 * @return the Resample {@link Operation}s.
	 */
	public static Operation[] getResampleOperations() {
		Operation[] ops = new Operation[3];
		ops[0] = RESAMPLE_AUTOMATIC;
		ops[1] = RESAMPLE_ADVANCED;
		ops[2] = RESAMPLE_GRID;
		return ops;
	}
}
