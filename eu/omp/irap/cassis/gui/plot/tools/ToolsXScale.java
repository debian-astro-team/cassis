/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.tools;

import java.awt.Color;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.Formula;
import eu.omp.irap.cassis.common.TypeFrequency;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;

public class ToolsXScale {


	/**
	 * Create a new {@link XYSpectrumSeries} by doing a redshift on the wave
	 *
	 * @param serie The {@link XYSpectrumSeries} with wave values on sky.
	 * @param name The name of the new series.
	 * @param color The color of the new series.
	 * @return The created {@link XYSpectrumSeries} with the wave values on rest
	 * according to the redshift.
	 */
	public static XYSpectrumSeries doRedshift(XYSpectrumSeries serie, String name,
			Color color, double value) {
		if (serie == null) {
			return null;
		}
		return ToolsUtil.createNewSerie(serie, doRedshift(serie.getSpectrum(), value), name,
				color);
	}

	/**
	 * Create a new {@link CommentedSpectrum} by doing a redshift on the wave
	 *
	 * @param cs The {@link CommentedSpectrum} on which is based the resampling.
	 * @param value The value of the redshift.
	 * @return The created {@link CommentedSpectrum}
	 */
	public static CommentedSpectrum doRedshift(CommentedSpectrum cs, double value) {
		double[] x = cs.getFrequenciesSignal();
		double[] y = cs.getIntensities();
		double[] xNew = new double[x.length];
		double[] yNew = new double[y.length];
		System.arraycopy(y, 0, yNew, 0, y.length);
		CommentedSpectrum newCommentedSpectrum = null;

		if (cs.getTypeFreq() != TypeFrequency.REST) {
			for (int i = 0; i < x.length; i++) {
				xNew[i] = x[i]* (1 + value);
			}
			newCommentedSpectrum = ToolsUtil.createNewCommentedSpectrum(cs, xNew, yNew);
			newCommentedSpectrum.getCassisMetadataList().add(
					new CassisMetadata("redshift", String.valueOf(value), "Compute with CASSIS", "factor"));
		}

		return newCommentedSpectrum;
	}

	/**
	 * Create a new {@link CommentedSpectrum} by doing a wavelength air to vacuum conversion.
	 * In order to do that, the formula used is :
	 * s = 10000 / λair
	 * n = 1 + 0.00008336624212083 + 0.02408926869968 / (130.1065924522 - s²) +
	 *  0.0001599740894897 / (38.92568793293 - s²)
	 * Then the conversion is λvac = λair * n.
	 *
	 * @param cs The {@link CommentedSpectrum} with values on Air.
	 * @return The created {@link CommentedSpectrum} with the values on Vacuum.
	 */
	public static CommentedSpectrum doAirToVacuum(CommentedSpectrum cs) {
		double[] angstrom = convertArrayMhzToAngstroms(cs.getFrequenciesSignal());
		double s;
		double n;
		for (int i = 0; i < angstrom.length; i++) {
			s = 10000 / angstrom[i];
			n = 1 + 0.00008336624212083 + 0.02408926869968 /
					(130.1065924522 - Math.pow(s, 2)) + 0.0001599740894897 /
					(38.92568793293 - Math.pow(s, 2));
			angstrom[i] = angstrom[i] * n;
		}

		double[] freqMhz = convertArrayAngstromsToMhz(angstrom);
		double[] intensities = new double[freqMhz.length];
		System.arraycopy(cs.getIntensities(), 0, intensities, 0, freqMhz.length);

		return ToolsUtil.createNewCommentedSpectrum(cs, freqMhz, intensities);
	}

	/**
	 * Create a new {@link CommentedSpectrum} by doing a wavelength vacuum to air conversion.
	 * In order to do that, the formula used is :
	 * s = 10000 / λvac
	 * n = 1 + 0.0000834254 + 0.02406147 / (130 - s²) + 0.00015998 / (38.9 - s²)
	 * The conversion is then: λair = λvac / n.
	 *
	 * @param cs The {@link CommentedSpectrum} with values on vacuum.
	 * @return The created {@link CommentedSpectrum} with the values on air.
	 */
	public static CommentedSpectrum doVacuumToAir(CommentedSpectrum cs) {
		double[] angstrom = convertArrayMhzToAngstroms(cs.getFrequenciesSignal());
		double s;
		double n;
		for (int i = 0; i < angstrom.length; i++) {
			s = 10000 / angstrom[i];
			n = 1 + 0.0000834254 + 0.02406147 / (130 - Math.pow(s, 2)) + 0.00015998 / (38.9 - Math.pow(s, 2));
			angstrom[i] = angstrom[i] / n;
		}

		double[] freqMhz = convertArrayAngstromsToMhz(angstrom);
		double[] intensities = new double[freqMhz.length];
		System.arraycopy(cs.getIntensities(), 0, intensities, 0, freqMhz.length);

		return ToolsUtil.createNewCommentedSpectrum(cs, freqMhz, intensities);
	}

	/**
	 * Convert an array of values in MHz to Angstroms.
	 *
	 * @param freqMhz The array of values in MHz.
	 * @return The array of values in Angstroms.
	 */
	private static double[] convertArrayMhzToAngstroms(double[] freqMhz) {
		double[] angstrom = new double[freqMhz.length];
		XAxisCassis angstromAxis = XAxisCassis.getXAxisWaveLength(UNIT.ANGSTROM);
		for (int i = 0; i < freqMhz.length; i++) {
			angstrom[i] = angstromAxis.convertFromMhzFreq(freqMhz[i]).doubleValue();
		}
		return angstrom;
	}

	/**
	 * Convert an array of values in Angstroms to MHz.
	 *
	 * @param angstroms The array of values in Angstroms.
	 * @return The array of values in MHz.
	 */
	private static double[] convertArrayAngstromsToMhz(double[] angstroms) {
		double[] freqMhz = new double[angstroms.length];
		XAxisCassis angstromAxis = XAxisCassis.getXAxisWaveLength(UNIT.ANGSTROM);
		for (int i = 0; i < freqMhz.length; i++) {
			freqMhz[i] = angstromAxis.convertToMHzFreq(angstroms[i]);
		}
		return freqMhz;
	}

	/**
	 * Create a new {@link XYSpectrumSeries} by doing a wavelength air to vacuum conversion.
	 * In order to do that, the formula used is :
	 * s = 10000 / λair
	 * n = 1 + 0.00008336624212083 + 0.02408926869968 / (130.1065924522 - s²) +
	 *  0.0001599740894897 / (38.92568793293 - s²)
	 * Then the conversion is λvac = λair * n.
	 *
	 * @param serie The {@link XYSpectrumSeries} with values on Air.
	 * @param name The name of the new series.
	 * @param color The color of the new series.
	 * @return The created {@link XYSpectrumSeries} with the values on Vacuum.
	 */
	public static XYSpectrumSeries doAirToVacuum(XYSpectrumSeries serie,
			String name, Color color) {
		return ToolsUtil.createNewSerie(serie, doAirToVacuum(serie.getSpectrum()), name, color);
	}

	/**
	 * Create a new {@link XYSpectrumSeries} by doing a wavelength vacuum to air conversion.
	 * In order to do that, the formula used is :
	 * s = 10000 / λvac
	 * n = 1 + 0.0000834254 + 0.02406147 / (130 - s²) + 0.00015998 / (38.9 - s²)
	 * The conversion is then: λair = λvac / n.
	 *
	 * @param serie The {@link XYSpectrumSeries} with values on vacuum.
	 * @param name The name of the new series.
	 * @param color The color of the new series.
	 * @return The created {@link XYSpectrumSeries} with the values on air.
	 */
	public static XYSpectrumSeries doVacuumToAir(XYSpectrumSeries serie,
			String name, Color color) {
		return ToolsUtil.createNewSerie(serie, doVacuumToAir(serie.getSpectrum()), name, color);
	}

	/**
	 * Create a new {@link CommentedSpectrum} by changing the TypeFrequency to REST from
	 * a series with a SKY TypeFrequency.
	 *
	 * @param cs The {@link XYSpectrumSeries} with the SKY TypeFrequency.
	 * @param vlsr The value of the new VLSR.
	 * @return The created {@link CommentedSpectrum} or null if there was an error.
	 */
	public static CommentedSpectrum doSkyToRest(CommentedSpectrum cs, double vlsr) {
		if (cs.getTypeFreq() != TypeFrequency.SKY) {
			return null;
		}
		double[] oldFrequency = cs.getFrequenciesSignal();
		double[] newFrequency = new double[oldFrequency.length];

		for (int i = 0; i < oldFrequency.length; i++) {
			newFrequency[i] = Formula.calcFreqWithVlsr(oldFrequency[i], -vlsr,
					oldFrequency[i]);
		}

		double[] newIntensities = new double[cs.getIntensities().length];
		System.arraycopy(cs.getIntensities(), 0, newIntensities, 0,
				cs.getIntensities().length);

		CommentedSpectrum csf = ToolsUtil.createNewCommentedSpectrum(cs, newFrequency,
				cs.getIntensities());
		csf.setTypeFreq(TypeFrequency.REST);
		csf.setVlsr(vlsr);
		if (!Double.isNaN(cs.getLoFreq())) {
			csf.setLoFreq(
					Formula.calcFreqWithVlsr(cs.getLoFreq(), -vlsr, cs.getLoFreq()));
		}

		return csf;
	}

	/**
	 * Create a new {@link CommentedSpectrum} by changing the TypeFrequency to SKY from
	 * a series with a REST TypeFrequency.
	 *
	 * @param cs The {@link CommentedSpectrum} with the REST TypeFrequency.
	 * @return The created {@link CommentedSpectrum} or null if there was an error.
	 */
	public static CommentedSpectrum doRestToSky(CommentedSpectrum cs) {
		if (cs.getTypeFreq() != TypeFrequency.REST) {
			return null;
		}
		double[] oldFrequency = cs.getFrequenciesSignal();
		double vlsrRec = cs.getVlsr();
		double[] newFrequency = new double[oldFrequency.length];

		for (int i = 0; i < oldFrequency.length; i++) {
			newFrequency[i] = Formula.calcFreqWithVlsr(oldFrequency[i], vlsrRec,
					oldFrequency[i]);
		}

		double[] newIntensities = new double[cs.getIntensities().length];
		System.arraycopy(cs.getIntensities(), 0, newIntensities, 0,
				cs.getIntensities().length);

		CommentedSpectrum spectrum = ToolsUtil.createNewCommentedSpectrum(cs, newFrequency,
				newIntensities);
		spectrum.setTypeFreq(TypeFrequency.SKY);
		spectrum.setVlsr(0.0);

		return spectrum;
	}


	/**
	 * Create a new {@link XYSpectrumSeries} by changing the TypeFrequency to SKY from
	 * a serie with a REST TypeFrequency.
	 *
	 * @param serie The {@link XYSpectrumSeries} with the REST TypeFrequency.
	 * @param nameNewCurve The name of the new {@link XYSpectrumSeries}
	 * @param color The color of the new serie.
	 * @return The created {@link XYSpectrumSeries} or null if there was an error.
	 */
	public static XYSpectrumSeries doRestToSky(XYSpectrumSeries serie,
			String nameNewCurve, Color color) {
		return ToolsUtil.createNewSerie(serie, doRestToSky(serie.getSpectrum()), nameNewCurve,
				color);
	}

	/**
	 * Create a new {@link XYSpectrumSeries} by changing the TypeFrequency to REST from
	 * a series with a SKY TypeFrequency.
	 *
	 * @param serie The {@link XYSpectrumSeries} with the SKY TypeFrequency.
	 * @param vlsr The value of the new VLSR.
	 * @param name The name of the new {@link XYSpectrumSeries}
	 * @param color The color of the new serie.
	 * @return The created {@link XYSpectrumSeries} or null if there was an error.
	 */
	public static XYSpectrumSeries doSkyToRest(XYSpectrumSeries serie, double vlsr,
			String name, Color color) {
		return ToolsUtil.createNewSerie(serie, doSkyToRest(serie.getSpectrum(), vlsr), name, color);
	}





	/**
	 * Create a new {@link CommentedSpectrum} by changing the VLSR.
	 *
	 * @param cs The {@link CommentedSpectrum} used a base for the new spectrum after the operation.
	 * @param vlsr The new VLSR.
	 * @return The created {@link CommentedSpectrum} or null if there was an error.
	 */
	public static CommentedSpectrum doVlsrChange(CommentedSpectrum cs, double vlsr) {
//		double[] x = cs.getFrequenciesSignal();
//		double[] y = cs.getIntensities();
//		double[] xNew = new double[x.length];
//		double[] yNew = new double[y.length];
//		System.arraycopy(y, 0, yNew, 0, y.length);
		CommentedSpectrum newCommentedSpectrum;

		if (cs.getTypeFreq() == TypeFrequency.REST) {
			newCommentedSpectrum = doVlsrChange(ToolsUtil.doRestToSky(cs), vlsr);
		} else {
			newCommentedSpectrum = doSkyToRest(cs, vlsr);
//			double vlsrDiff = vlsr - cs.getVlsr();
//			for (int i = 0; i < x.length; i++) {
//				xNew[i] = Formula.calcFreqWithVlsr(x[i], -vlsrDiff, x[i]);
//			}
//			newCommentedSpectrum = createNewCommentedSpectrum(cs, xNew, yNew);
//			newCommentedSpectrum.setVlsr(vlsr);
		}

		return newCommentedSpectrum;
	}

	/**
	 * Create a new {@link XYSpectrumSeries} by changing the VLSR.
	 *
	 * @param serie The {@link XYSpectrumSeries} used a base for the new series after the operation.
	 * @param name The name of the new {@link XYSpectrumSeries}.
	 * @param color The color of the new series.
	 * @param vlsr The new VLSR.
	 * @return The created {@link XYSpectrumSeries} or null if there was an error.
	 */
	public static XYSpectrumSeries doVlsrChange(XYSpectrumSeries serie, String name,
			Color color, double vlsr) {
		if (serie == null) {
			return null;
		}
		return ToolsUtil.createNewSerie(serie, doVlsrChange(serie.getSpectrum(), vlsr), name,
				color);
	}
}
