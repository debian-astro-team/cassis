/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.tools;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.XAxisVelocity;
import eu.omp.irap.cassis.common.axes.XAxisWaveLength;
import eu.omp.irap.cassis.common.axes.X_AXIS;
import eu.omp.irap.cassis.gui.plot.curve.TypeCurve;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import eu.omp.irap.cassis.gui.plot.curve.config.ColorsCurve;


/**
 * Utility class for the Tools functionality.
 * Contain the core function used for computing the operations.
 *
 * @author M. Boiziot
 */
public final class ToolsUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(ToolsUtil.class);
	public static final int NB_MAX_POINTS_DEFAULT = 500000;
	private static int nbMaxPoints = NB_MAX_POINTS_DEFAULT;

	/**
	 * Utility class, should not be instantiated.
	 */
	private ToolsUtil() {
	}

	/**
	 * Set maximum number of points allowed for the operations.
	 *
	 * @param nbPoints The number of points allowed for an operation.
	 */
	public static void setNbMaxPoints(int nbPoints) {
		nbMaxPoints = nbPoints;
	}

	/**
	 * Resample the points from a series with the given array of X points.
	 *
	 * @param xDest The array used for the news X points.
	 * @param xSerieToResample The array of X points of the series to resample.
	 * @param ySerieToResample The array of Y points of the series to resample.
	 * @return the new array of Y points or null if there was an error.
	 */
	public static double[] resample(double[] xDest, double[] xSerieToResample,
			double[] ySerieToResample) {
		return ToolsResample.resample(xDest, xSerieToResample, ySerieToResample);
	}



	/**
	 * Get the lowest sampling from two series.
	 *
	 * @param serie The first series.
	 * @param serie2 The second series.
	 * @return The lowest sampling, in the unit of the xAxis.
	 */
	public static double getLowestSampling(XYSpectrumSeries serie, XYSpectrumSeries serie2) {
		return ToolsResample.getLowestSampling(serie, serie2);
	}

	/**
	 * Get the lowest sampling from two {@link CommentedSpectrum}.
	 *
	 * @param cs The first {@link CommentedSpectrum}.
	 * @param cs2 The second {@link CommentedSpectrum}.
	 * @param axis The {@link XAxisCassis} of the spectra.
	 * @return The lowest sampling, in the unit of the xAxis.
	 */
	public static double getLowestSampling(CommentedSpectrum cs, CommentedSpectrum cs2,
			XAxisCassis axis) {
		return ToolsResample.getLowestSampling(cs, cs2, axis);
	}


	/**
	 * Check if we two series has joint points based by theirs xMin and xMax
	 *
	 * @param xMin The xMin value.
	 * @param xMax The XMax value.
	 * @return If the ressample is possible.
	 */
	public static boolean isRessamplePossible(double xMin, double xMax) {
		return ToolsResample.isRessamplePossible(xMin, xMax);
	}


	/**
	 * Create a new {@link XYSpectrumSeries} by doing a resampling.
	 *
	 * @param serie The {@link XYSpectrumSeries} on which is based the resampling.
	 * @param sampling The sampling used for the created {@link XYSpectrumSeries} in the xAxis unit of the series.
	 * @param name The name of the new serie.
	 * @param color The color of the new serie.
	 * @return The created {@link XYSpectrumSeries} or null if there was an error.
	 * @throws ToolsException In case of error for the operation.
	 */
	public static XYSpectrumSeries doResample(XYSpectrumSeries serie, double sampling,
			String name, Color color) throws ToolsException {
		return ToolsResample.doResample(serie, sampling, name, color);
	}

	/**
	 * Create a new {@link XYSpectrumSeries} by doing a resampling using a base point.
	 *
	 * @param serie The {@link XYSpectrumSeries} on which is based the resampling.
	 * @param sampling The sampling used for the created {@link XYSpectrumSeries} in the xAxis unit of the series.
	 * @param initialSample The initial point used in the xAxis unit of the series.
	 * @param finalSample The final point used in the xAxis unit of the series.
	 * @param name The name of the new series.
	 * @param color The color of the new series.
	 * @return The created {@link XYSpectrumSeries} or null if there was an error.
	 * @throws ToolsException In case of error for the operation.
	 */
	public static XYSpectrumSeries doResample(XYSpectrumSeries serie, double sampling,
			double initialSample, double finalSample, String name, Color color) throws ToolsException {
		return ToolsResample.doResample(serie, sampling, initialSample, finalSample, name, color);
	}

	/**
	 * Create a new {@link XYSpectrumSeries} by doing a resampling a serie to another to the same sampling
	 * from another serie.
	 *
	 * @param serie The {@link XYSpectrumSeries} on which is based the resampling (points x/y).
	 * @param serie2 The {@link XYSpectrumSeries} on which is based the resampling (points x used).
	 * @param name The name of the new serie.
	 * @param color The color of the new serie.
	 * @return The created {@link XYSpectrumSeries} or null if there was an error.
	 */
	public static XYSpectrumSeries doResampleGrid(XYSpectrumSeries serie,
			XYSpectrumSeries serie2, String name, Color color) {
		return ToolsResample.doResampleGrid(serie, serie2, name, color);
	}

	/**
	 * Create a new {@link CommentedSpectrum} by doing a resampling.
	 *
	 * @param cs The {@link CommentedSpectrum} on which is based the resampling.
	 * @param axis The {@link XAxisCassis} of the spectrum.
	 * @param sampling The sampling used for the created {@link CommentedSpectrum} in the xAxis unit of the series.
	 * @return The created {@link CommentedSpectrum} or null if there was an error.
	 * @throws ToolsException In case of error for the operation.
	 */
	public static CommentedSpectrum doResample(CommentedSpectrum cs, XAxisCassis axis,
			double sampling) throws ToolsException {
		return ToolsResample.doResample(cs, axis, sampling);
	}

	/**
	 * Create a new {@link CommentedSpectrum} by doing a resampling using a base point.
	 *
	 * @param cs The {@link CommentedSpectrum} on which is based the resampling.
	 * @param axis The {@link XAxisCassis} of the spectrum.
	 * @param sampling The sampling used for the created {@link XYSpectrumSeries} in the xAxis unit of the series.
	 * @param initialSample The initial point used in the xAxis unit of the series.
	 * @param finalSample The final point used in the xAxis unit of the series.
	 * @return The created {@link CommentedSpectrum} or null if there was an error.
	 * @throws ToolsException In case of error for the operation.
	 */
	public static CommentedSpectrum doResample(CommentedSpectrum cs, XAxisCassis axis,
			double sampling, double initialSample, double finalSample) throws ToolsException {
		return ToolsResample.doResample(cs, axis, sampling, initialSample, finalSample);
	}

	/**
	 * Create a new {@link CommentedSpectrum} by doing a resampling a spectrum to another to the same sampling
	 * from another series.
	 *
	 * @param cs The {@link CommentedSpectrum} on which is based the resampling (points x/y).
	 * @param cs2 The {@link CommentedSpectrum} on which is based the resampling (points x used).
	 * @return The created {@link CommentedSpectrum} or null if there was an error.
	 */
	public static CommentedSpectrum doResampleGrid(CommentedSpectrum cs, CommentedSpectrum cs2) {
		return ToolsResample.doResampleGrid(cs, cs2);
	}

	/**
	 * Check some parameters : if ressample is possible, the sampling is ok and the
	 * number of points to be created is not too big.
	 *
	 * @param xMin The xMin.
	 * @param xMax The xMax.
	 * @param sampling The sampling.
	 * @return if the parameters are OK.
	 * @throws ToolsException In case of error for the operation.
	 */
	public static boolean checkParameters(double xMin, double xMax, double sampling) throws ToolsException {
		if (!isRessamplePossible(xMin, xMax)) {
			LOGGER.info("Operation is not possible. min = " + xMin + "; max = " + xMax);
			throw new ToolsException("Operation is not possible. The value of"
					+ " xMin must be inferior of xMax.");
		}
		if (sampling == 0.0 || sampling == Double.MAX_VALUE) {
			LOGGER.error("Operation is not possible with the current sampling: "
					+ sampling);
			throw new ToolsException("Operation is not possible with the current"
					+ " sampling (" + sampling + ")");
		}

		long nbPoint = (long) Math.floor((xMax - xMin) / sampling);

		if (nbPoint > nbMaxPoints) {
			LOGGER.error("Too many points: " + nbPoint);
			throw new ToolsException("The given parameters yield to create a"
					+ " spectrum with a larger number of points (" + nbPoint
					+ ") than defined in your preferences (" + nbMaxPoints
					+ ").\nPlease change your parameters or set the maximum"
					+ " number of points to a larger value in your preferences.");
		}

		return true;
	}

	/**
	 * Compute the X min point between two series.
	 *
	 * @param serie The first series.
	 * @param serie2 The second series.
	 * @return The X Min point in the unit of the xAxis.
	 */
	public static double getXMin(XYSpectrumSeries serie, XYSpectrumSeries serie2) {
		double xMinInMhz;
		if (serie.getXAxis().isInverted()) {
			xMinInMhz = Math.min(serie.getSpectrum().getFrequencySignalMax(),
					serie2.getSpectrum().getFrequencySignalMax());
		} else {
			xMinInMhz = Math.max(serie.getSpectrum().getFrequencySignalMin(),
				serie2.getSpectrum().getFrequencySignalMin());
		}

		return serie.getXAxis().convertFromMhzFreq(xMinInMhz);
	}

	/**
	 * Compute the X max point between two series.
	 *
	 * @param serie The first series.
	 * @param serie2 The second series.
	 * @return The X Max point in the unit of the xAxis.
	 */
	public static double getXMax(XYSpectrumSeries serie, XYSpectrumSeries serie2) {
		double xMaxInMhz;
		if (serie.getXAxis().isInverted()) {
			xMaxInMhz = Math.max(serie.getSpectrum().getFrequencySignalMin(),
					serie2.getSpectrum().getFrequencySignalMin());
		} else {
			xMaxInMhz = Math.min(serie.getSpectrum().getFrequencySignalMax(),
					serie2.getSpectrum().getFrequencySignalMax());
		}

		return serie.getXAxis().convertFromMhzFreq(xMaxInMhz);
	}

	/**
	 * Compute the values of the x points in MHz between xMin and xMax with a sampling.
	 *
	 * @param axis The XAXisCassis of the xMin/xMax/sampling.
	 * @param xMin The xMin value.
	 * @param xMax The xMax value.
	 * @param sampling The sampling to use.
	 * @return The array of the x value in MHz.
	 */
	public static double[] getXFinal(XAxisCassis axis, double xMin, double xMax,
			double sampling) {
		// The coeff is needed due to the mathematical error of java.
		final double coeff = 1E8;
		xMin *= coeff;
		xMax *= coeff;
		sampling *= coeff;
		double lastValueUnit = xMin;
		int i = 0;
		while (lastValueUnit <= xMax) {
			lastValueUnit += sampling;
			i++;
		}
		double[] xFinal = new double[i];
		double xMinMhz = axis.convertToMHzFreq(xMin / coeff);
		double lastValueMhz = xMinMhz;
		lastValueUnit = xMin;

		for (int j = 0; j < i; j++) {
			xFinal[j] = lastValueMhz;
			lastValueUnit += sampling;
			lastValueMhz = axis.convertToMHzFreq(lastValueUnit / coeff);
		}
		Arrays.sort(xFinal);

		return xFinal;
	}

	/**
	 * Create a new {@link XYSpectrumSeries} by doing an operation two {@link XYSpectrumSeries}.
	 *
	 * @param serie The first {@link XYSpectrumSeries} (whose data will be subtracted in case of SUBTRACT operation).
	 * @param serie2 The second {@link XYSpectrumSeries} (used to subtract in case of SUBTRACT operation).
	 * @param name The generic name of the created {@link XYSpectrumSeries}.
	 * @param number The number used for the news series.
	 * @param colors  The Colors used for the three new {@link XYSpectrumSeries}.
	 * @param xMin The value of the minimum x point in the xAxis unit of the series.
	 * @param xMax The value of the maximum x point in the xAxis unit of the series.
	 * @param sampling The sampling used for the created {@link XYSpectrumSeries} in the xAxis unit of the series.
	 * @param operation The operation to do. It <b>must</b> be {@link Operation#ADD}, {@link Operation#SUBTRACT} or {@link Operation#DIVIDE}.
	 * @param avoidResample Avoid resampling if possible.
	 * @return The list of created {@link XYSpectrumSeries} or null if there was an error..
	 * @throws ToolsException In case of error for the operation.
	 */
	public static List<XYSpectrumSeries> doOperation(XYSpectrumSeries serie,
			XYSpectrumSeries serie2, String name, int number, List<Color> colors,
			double xMin, double xMax, double sampling, Operation operation,
			boolean avoidResample) throws ToolsException {
		List<CommentedSpectrum> listCs = doOperation(serie.getSpectrum(),
				serie2.getSpectrum(), serie.getXAxis(), xMin, xMax, sampling, operation, avoidResample);
		if (listCs == null) {
			return null;
		}

		if (colors == null || colors.size() != 3) {
			colors = new ArrayList<>(3);
			colors.add(ColorsCurve.getNewColorResult());
			colors.add(ColorsCurve.getNewColorResult());
			colors.add(ColorsCurve.getNewColorResult());
		}

		List<XYSpectrumSeries> listSeries = new ArrayList<>(3);
		String tempName = "";
		if (name.startsWith("Mosaic")) {
			tempName = "Mosaic ";
		}
		String nameSerie1 = tempName + serie.getKey() + " resampled " + number;
		String nameSerie2 = tempName + serie2.getKey() + " resampled " + number;

		listSeries.add(createNewSerie(serie, listCs.get(0), nameSerie1, colors.get(0)));
		listSeries.add(createNewSerie(serie2, listCs.get(1), nameSerie2, colors.get(1)));
		listSeries.add(createNewSerie(serie, listCs.get(2), name + number, colors.get(2)));

		return listSeries;
	}



	/**
	 * Create a new {@link XYSpectrumSeries} by doing an operation ({@link Operation#ADD_CONSTANT}
	 * or {@link Operation#MULTIPLY_CONSTANT}) with a {@link XYSpectrumSeries} and a constant.
	 *
	 * @param serie The {@link XYSpectrumSeries}.
	 * @param constant The constant.
	 * @param name The name of the created {@link XYSpectrumSeries}.
	 * @param colorSpectrum The Color used for the created {@link XYSpectrumSeries}.
	 * @param operation The operation to do. It <b>must</b> be {@link Operation#ADD_CONSTANT} or {@link Operation#MULTIPLY_CONSTANT}.
	 * @return The created {@link XYSpectrumSeries} or null if there was an error.
	 */
	public static XYSpectrumSeries doOperationWithConstant(XYSpectrumSeries serie,
			double constant, String name, Color colorSpectrum, Operation operation) {
		return createNewSerie(serie,
				doOperationWithConstant(serie.getSpectrum(), constant, operation), name,
				colorSpectrum);
	}

	/**
	 * Create a new {@link CommentedSpectrum} by doing an operation on two {@link CommentedSpectrum}.
	 * This function will use the already existing wave points who must be the same on the two spectra.
	 *
	 * @param cs The first {@link CommentedSpectrum} (whose data will be subtracted in case of SUBTRACT operation).
	 * @param cs2 The second {@link CommentedSpectrum} (used to subtract in case of SUBTRACT operation).
	 * @param axis The {@link XAxisCassis} of the spectra.
	 * @param xMin The value of the minimum x point in the xAxis unit of the series.
	 * @param xMax The value of the maximum x point in the xAxis unit of the series.
	 * @param operation The operation to do. It <b>must</b> be {@link Operation#ADD}, {@link Operation#SUBTRACT} or {@link Operation#DIVIDE}.
	 * @return The list of created {@link CommentedSpectrum} or null if there was an error.
	 */
	private static List<CommentedSpectrum> doOperationWithoutResampling(CommentedSpectrum cs,
			CommentedSpectrum cs2, XAxisCassis axis, double xMin, double xMax,
			Operation operation) {
		double[] freq = cs.getFrequenciesSignal();
		int tooLittle = getTooLittle(freq, axis.convertToMHzFreq(xMin));
		int tooBig = getTooBig(freq, axis.convertToMHzFreq(xMax));
		double[] xFinal = Arrays.copyOfRange(freq, tooLittle, freq.length - tooBig);
		double[] yFinalSerie1 = Arrays.copyOfRange(cs.getIntensities(), tooLittle, cs.getIntensities().length - tooBig);
		double[] yFinalSerie2 = Arrays.copyOfRange(cs2.getIntensities(), tooLittle, cs2.getIntensities().length - tooBig);
		double[] yFinalNewSerie = new double[xFinal.length];

		if (operation == Operation.SUBTRACT) {
			yFinalNewSerie = ToolsSimpleOperations.subtract(yFinalSerie1, yFinalSerie2, xFinal.length);
		} else if (operation == Operation.ADD) {
			yFinalNewSerie = ToolsSimpleOperations.add(yFinalSerie1, yFinalSerie2, xFinal.length);
		} else if (operation == Operation.DIVIDE) {
			yFinalNewSerie = ToolsSimpleOperations.divide(yFinalSerie1, yFinalSerie2, xFinal.length);
		}
		List<CommentedSpectrum> listCs = new ArrayList<>(3);
		listCs.add(createNewCommentedSpectrum(cs, Arrays.copyOf(xFinal, xFinal.length), yFinalSerie1));
		listCs.add(createNewCommentedSpectrum(cs2, Arrays.copyOf(xFinal, xFinal.length), yFinalSerie2));
		listCs.add(createNewCommentedSpectrum(cs, xFinal, yFinalNewSerie));

		return listCs;
	}

	/**
	 * Create a new {@link CommentedSpectrum} by doing an operation on two {@link CommentedSpectrum}.
	 * This function will do a resampling to apply the operation.
	 *
	 * @param cs The first {@link CommentedSpectrum} (whose data will be subtracted in case of SUBTRACT operation).
	 * @param cs2 The second {@link CommentedSpectrum} (used to subtract in case of SUBTRACT operation).
	 * @param axis The {@link XAxisCassis} of the spectra.
	 * @param xMin The value of the minimum x point in the xAxis unit of the series.
	 * @param xMax The value of the maximum x point in the xAxis unit of the series.
	 * @param sampling The sampling used for the created {@link CommentedSpectrum} in the xAxis unit of the series.
	 * @param operation The operation to do. It <b>must</b> be {@link Operation#ADD}, {@link Operation#SUBTRACT} or {@link Operation#DIVIDE}.
	 * @return The list of created {@link CommentedSpectrum} or null if there was an error.
	 * @throws ToolsException In case of error for the operation.
	 */
	private static List<CommentedSpectrum> doOperationWithResampling(CommentedSpectrum cs,
			CommentedSpectrum cs2, XAxisCassis axis, double xMin, double xMax,
			double sampling, Operation operation) throws ToolsException {
		checkParameters(xMin, xMax, sampling);
		double[] xFinal = getXFinal(axis, xMin, xMax, sampling);
		double[] xFinalSerie1 = new double[xFinal.length];
		double[] xFinalSerie2 = new double[xFinal.length];
		System.arraycopy(xFinal, 0, xFinalSerie1, 0, xFinal.length);
		System.arraycopy(xFinal, 0, xFinalSerie2, 0, xFinal.length);
		double[] yFinalSerie1;
		double[] yFinalSerie2;

		double[] freqs = cs.getFrequenciesSignal();
		double[] freqs2 = cs2.getFrequenciesSignal();

		if (xFinal[0] < Math.max(freqs[0], freqs2[0])) {
			xFinal[0] = Math.max(freqs[0], freqs2[0]);
		}
		if (xFinal[xFinal.length - 1] > Math.min(freqs[freqs.length - 1],
				freqs2[freqs2.length - 1])) {
			xFinal[xFinal.length - 1] = Math.min(freqs[freqs.length - 1],
					freqs2[freqs2.length - 1]);
		}

		yFinalSerie1 = resample(xFinal, freqs, cs.getIntensities());
		yFinalSerie2 = resample(xFinal, freqs2, cs2.getIntensities());

		double[] yFinalNewSerie = new double[xFinal.length];

		if (operation == Operation.SUBTRACT) {
			yFinalNewSerie = ToolsSimpleOperations.subtract(yFinalSerie1, yFinalSerie2, xFinal.length);
		} else if (operation == Operation.ADD) {
			yFinalNewSerie = ToolsSimpleOperations.add(yFinalSerie1, yFinalSerie2, xFinal.length);
		} else if (operation == Operation.DIVIDE) {
			yFinalNewSerie = ToolsSimpleOperations.divide(yFinalSerie1, yFinalSerie2, xFinal.length);
		}

		List<CommentedSpectrum> listCs = new ArrayList<>(3);

		listCs.add(createNewCommentedSpectrum(cs, xFinalSerie1, yFinalSerie1));
		listCs.add(createNewCommentedSpectrum(cs2, xFinalSerie2, yFinalSerie2));
		listCs.add(createNewCommentedSpectrum(cs, xFinal, yFinalNewSerie));

		return listCs;
	}



	/**
	 * Create a new {@link CommentedSpectrum} by doing an operation ({@link Operation#ADD_CONSTANT}
	 * or {@link Operation#MULTIPLY_CONSTANT}) with a {@link CommentedSpectrum} and a constant.
	 *
	 * @param cs The {@link CommentedSpectrum}.
	 * @param constant The constant.
	 * @param operation The operation to do. It <b>must</b> be {@link Operation#ADD_CONSTANT} or {@link Operation#MULTIPLY_CONSTANT}.
	 * @return The created {@link CommentedSpectrum} or null if there was an error.
	 */
	public static CommentedSpectrum doOperationWithConstant(CommentedSpectrum cs,
			double constant, Operation operation) {
		if (!(operation == Operation.ADD_CONSTANT
				|| operation == Operation.MULTIPLY_CONSTANT)) {
			LOGGER.error("The specified operation can not be performed here.");
			return null;
		}

		int nbItemInSerie = cs.getFrequenciesSignal().length;

		double[] x = new double[nbItemInSerie];
		double[] y = new double[nbItemInSerie];

		if (operation == Operation.ADD_CONSTANT) {
			for (int i = 0; i < nbItemInSerie; i++) {
				x[i] = cs.getFrequenciesSignal()[i];
				y[i] = cs.getIntensities()[i] + constant;
			}
		} else if (operation == Operation.MULTIPLY_CONSTANT) {
			for (int i = 0; i < nbItemInSerie; i++) {
				x[i] = cs.getFrequenciesSignal()[i];
				y[i] = cs.getIntensities()[i] * constant;
			}
		}
		return createNewCommentedSpectrum(cs, x, y);
	}

	/**
	 * Create a new {@link CommentedSpectrum} by adding the values of two {@link CommentedSpectrum} then dividing them by 2.
	 *
	 * @param cs The first {@link CommentedSpectrum}.
	 * @param cs2 The second {@link CommentedSpectrum}.
	 * @param axis The {@link XAxisCassis} of the spectra.
	 * @param xMin The value of the minimum x point in the xAxis unit of the CommentedSpectrum.
	 * @param xMax The value of the maximum x point in the xAxis unit of the CommentedSpectrum.
	 * @param sampling The sampling used for the created {@link CommentedSpectrum} in the xAxis unit of the series.
	 * @return The created {@link CommentedSpectrum} or null if there was an error.
	 * @throws ToolsException In case of error for the operation.
	 */
	public static CommentedSpectrum doAverage(CommentedSpectrum cs, CommentedSpectrum cs2,
			XAxisCassis axis, double xMin, double xMax, double sampling) throws ToolsException {
		return ToolsSimpleOperations.doAverage(cs, cs2, axis, xMin, xMax, sampling);
	}

	/**
	 * Create a new {@link XYSpectrumSeries} by adding the values of two series then dividing them by 2.
	 *
	 * @param serie The first {@link XYSpectrumSeries}.
	 * @param serie2 The second {@link XYSpectrumSeries}.
	 * @param nameSerie The name of the created {@link XYSpectrumSeries}.
	 * @param color  The Color used for the new {@link XYSpectrumSeries}.
	 * @param xMin The value of the minimum x point in the xAxis unit of the series.
	 * @param xMax The value of the maximum x point in the xAxis unit of the series.
	 * @param sampling The sampling used for the created {@link XYSpectrumSeries} in the xAxis unit of the series.
	 * @param avoidResample Avoid resampling if possible.
	 * @return The created {@link XYSpectrumSeries} or null if there was an error.
	 * @throws ToolsException In case of error for the operation.
	 */
	public static XYSpectrumSeries doAverage(XYSpectrumSeries serie,
			XYSpectrumSeries serie2, String nameSerie, Color color, double xMin,
			double xMax, double sampling, boolean avoidResample) throws ToolsException {
		return ToolsSimpleOperations.doAverage(serie, serie2, nameSerie, color,
				xMin, xMax, sampling, avoidResample);
	}

	/**
	 * Create a new {@link XYSpectrumSeries} by doing a change Tmb/Ta change.
	 *
	 * @param serie The {@link XYSpectrumSeries} used a base for the new series after the operation.
	 * @param operation The Operation to do, must be {@link Operation#TA_TO_TMB} or {@link Operation#TMB_TO_TA}.
	 * @param name The name of the new {@link XYSpectrumSeries}.
	 * @param color The color of the new series.
	 * @param efficiencyList The efficiency list of the telescope.
	 * @param frequencyList The frequency list of the telescope.
	 * @return The created {@link XYSpectrumSeries} or null if there was an error.
	 */
	public static XYSpectrumSeries doTmbTaChange(XYSpectrumSeries serie,
			Operation operation, String name, Color color, List<Double> efficiencyList,
			List<Double> frequencyList) {
		return ToolsYScale.doTmbTaChange(serie, operation, name, color, efficiencyList, frequencyList);
	}



	/**
	 * Create a new {@link CommentedSpectrum} by doing a change Tmb/Ta change.
	 *
	 * @param cs The {@link CommentedSpectrum} used a base for the new series after the operation.
	 * @param operation The Operation to do, must be {@link Operation#TA_TO_TMB} or {@link Operation#TMB_TO_TA}.
	 * @param efficiencyList The efficiency list of the telescope.
	 * @param frequencyList The frequency list of the telescope.
	 * @return The created {@link CommentedSpectrum} or null if there was an error.
	 */
	public static CommentedSpectrum doTmbTaChange(CommentedSpectrum cs,
			Operation operation, List<Double> efficiencyList,
			List<Double> frequencyList) {
		return ToolsYScale.doTmbTaChange(cs, operation, efficiencyList, frequencyList);
	}

	/**
	 * Create a new {@link CommentedSpectrum} by doing a Smooth Hanning on a {@link CommentedSpectrum}.
	 *
	 * @param cs The {@link CommentedSpectrum} used for doing the first Smooth Hanning.
	 * @return The smoothed {@link CommentedSpectrum}, or null if there was an error.
	 */
	public static CommentedSpectrum doSmoothHanning(CommentedSpectrum cs) {
		return ToolsSmooth.doSmoothHanning(cs);
	}

	/**
	 * Create a new {@link XYSpectrumSeries} by doing a Smooth Hanning on a {@link XYSpectrumSeries}.
	 * This function is deprecated, please use {@link #doSmoothHanning(XYSpectrumSeries, String, Color)}
	 * instead.
	 *
	 * @param serie The {@link XYSpectrumSeries} used for doing the first Smooth Hanning.
	 * @param name The generic name for the created series (which is the name without the number).
	 * @param color The list of colors used for the series.
	 * @return The smoothed series, or null if there was an error.
	 */
	@Deprecated
	public static XYSpectrumSeries smoothHanning(XYSpectrumSeries serie, String name,
			Color color) {
		return ToolsSmooth.smoothHanning(serie, name, color);
	}

	/**
	 * Create a new {@link XYSpectrumSeries} by doing a Smooth Hanning on a {@link XYSpectrumSeries}.
	 *
	 * @param serie The {@link XYSpectrumSeries} used for doing the first Smooth Hanning.
	 * @param name The generic name for the created series (which is the name without the number).
	 * @param color The list of colors used for the series.
	 * @return The smoothed series, or null if there was an error.
	 */
	public static XYSpectrumSeries doSmoothHanning(XYSpectrumSeries serie, String name,
			Color color) {
		return ToolsSmooth.doSmoothHanning(serie, name, color);
	}

	/**
	 * Create a new {@link XYSpectrumSeries} by doing a Smooth Box.
	 *
	 * @param serie The {@link XYSpectrumSeries} used a base for the new series after the operation.
	 * @param name The name of the new {@link XYSpectrumSeries}.
	 * @param color The color of the new series.
	 * @param boxSize The size of the box.
	 * @return The created {@link XYSpectrumSeries} or null if there was an error.
	 */
	public static XYSpectrumSeries doSmoothBox(XYSpectrumSeries serie, String name,
			Color color, int boxSize) {
		return ToolsSmooth.doSmoothBox(serie, name, color, boxSize);
	}

	/**
	 * Create a new {@link XYSpectrumSeries} by doing a Smooth Gauss.
	 *
	 * @param serie The {@link XYSpectrumSeries} used a base for the new series after the operation.
	 * @param name The name of the new {@link XYSpectrumSeries}.
	 * @param color The color of the new series.
	 * @param fwhm The FWHM used for the operation.
	 * @return The created {@link XYSpectrumSeries} or null if there was an error.
	 */
	public static XYSpectrumSeries doSmoothGauss(XYSpectrumSeries serie, String name,
			Color color, double fwhm) {
		return ToolsSmooth.doSmoothGauss(serie, name, color, fwhm);
	}

	/**
	 * Create a new {@link CommentedSpectrum} by doing a Smooth Box.
	 *
	 * @param cs The {@link CommentedSpectrum} used a base for the new spectrum after the operation.
	 * @param boxSize The size of the box.
	 * @return The created {@link CommentedSpectrum} or null if there was an error.
	 */
	public static CommentedSpectrum doSmoothBox(CommentedSpectrum cs, int boxSize) {
		return ToolsSmooth.doSmoothBox(cs, boxSize);
	}

	/**
	 * Create a new {@link CommentedSpectrum} by doing a Smooth Gauss.
	 *
	 * @param cs The {@link CommentedSpectrum} used a base for the new spectrum after the operation.
	 * @param axis The {@link XAxisCassis} of the {@link CommentedSpectrum}.
	 * @param fwhm The FWHM used for the operation.
	 * @return The created {@link CommentedSpectrum} or null if there was an error.
	 */
	public static CommentedSpectrum doSmoothGauss(CommentedSpectrum cs,XAxisCassis axis,
			double fwhm) {
		return ToolsSmooth.doSmoothGauss(cs, axis, fwhm);
	}

	/**
	 * Convert a value of a delta with an UNIT from an axis to a MHz delta.
	 *
	 * @param axis The axis with the unit of the delta.
	 * @param val The value of the delta.
	 * @return The delta in MHz.
	 */
	public static double convDeltaToFreq(XAxisCassis axis, double val) {
		double finalValue;
		if (axis.getAxis() == X_AXIS.VELOCITY_SIGNAL) {
			finalValue = axis.convertDeltaToMhz(val, ((XAxisVelocity) axis).getFreqRef());
		} else if (axis.getAxis() == X_AXIS.WAVE_LENGTH) {
			finalValue = axis.convertDeltaToMhz(val,
					((XAxisWaveLength) axis).getFreqRef());
		} else {
			finalValue = axis.convertDeltaToMhz(val, Double.NaN);
		}
		return finalValue;
	}

	/**
	 * Compute the values of the x points in MHz between xMin and xMax by
	 * using an initial and final value and a sampling.
	 *
	 * @param axis The XAXisCassis of the xMin/xMax/sampling/initialValue/finalValue.
	 * @param xMin The xMin value.
	 * @param xMax The xMax value.
	 * @param sampling The sampling to use.
	 * @param initialValue The initial value.
	 * @param finalValue The final value.
	 * @return The array of the x value in MHz.
	 */
	public static double[] getXFinal(XAxisCassis axis, double xMin, double xMax,
			double sampling, double initialValue, double finalValue) {
		// The coeff is needed due to the mathematical error of java.
		final double coeff = 1E8;
		xMin *= coeff;
		xMax *= coeff;
		sampling *= coeff;
		initialValue *= coeff;
		finalValue *= coeff;
		double lastValueUnit = initialValue;
		int i = 0;
		while (lastValueUnit <= xMax && lastValueUnit <= finalValue) {
			lastValueUnit += sampling;
			i++;
		}
		double[] xFinal = new double[i];
		double xMinMhz = axis.convertToMHzFreq(initialValue / coeff);
		double lastValueMhz = xMinMhz;
		lastValueUnit = initialValue;

		for (int j = 0; j < i; j++) {
			xFinal[j] = lastValueMhz;
			lastValueUnit += sampling;
			lastValueMhz = axis.convertToMHzFreq(lastValueUnit / coeff);
		}
		Arrays.sort(xFinal);
		return xFinal;
	}

	/**
	 * Compute the values of the x points in MHz between xMin and xMax from
	 * a provided array.
	 *
	 * @param points The base array.
	 * @param xMin The minimum value.
	 * @param xMax The maximum value.
	 * @return the new array.
	 */
	public static double[] getXFinal(double[] points, double xMin, double xMax) {
		double[] newPoints = Arrays.copyOf(points, points.length);
		Arrays.sort(newPoints);
		int tooLittle = getTooLittle(newPoints, xMin);
		int tooBig = getTooBig(newPoints, xMax);

		return Arrays.copyOfRange(newPoints, tooLittle, newPoints.length - tooBig);
	}

	/**
	 * Compute the X max point between two {@link CommentedSpectrum}.
	 *
	 * @param cs The first {@link CommentedSpectrum}.
	 * @param cs2 The second {@link CommentedSpectrum}.
	 * @param axis The {@link XAxisCassis} of the spectra.
	 * @return The X Max point in the unit of the xAxis.
	 */
	public static double getXMax(CommentedSpectrum cs, CommentedSpectrum cs2,
			XAxisCassis axis) {
		double xMaxInMhz;
		if (axis.isInverted()) {
			xMaxInMhz = Math.max(cs.getFrequencySignalMin(), cs2.getFrequencySignalMin());
		} else {
			xMaxInMhz = Math.min(cs.getFrequencySignalMax(), cs2.getFrequencySignalMax());
		}

		return axis.convertFromMhzFreq(xMaxInMhz);
	}

	/**
	 * Create a new {@link CommentedSpectrum} by doing an operation on two {@link CommentedSpectrum}.
	 *
	 * @param cs The first {@link CommentedSpectrum} (whose data will be subtracted in case of SUBTRACT operation).
	 * @param cs2 The second {@link CommentedSpectrum} (used to subtract in case of SUBTRACT operation).
	 * @param axis The {@link XAxisCassis} of the spectra.
	 * @param xMin The value of the minimum x point in the xAxis unit of the series.
	 * @param xMax The value of the maximum x point in the xAxis unit of the series.
	 * @param sampling The sampling used for the created {@link CommentedSpectrum} in the xAxis unit of the series.
	 *  This value can be a double, or NaN. If the value is NaN and the wave values in the spectra are the same,
	 *  the operation will be done without resampling.
	 * @param operation The operation to do. It <b>must</b> be {@link Operation#ADD}, {@link Operation#SUBTRACT} or {@link Operation#DIVIDE}.
	 * @param avoidResample Avoid resampling if possible.
	 * @return The list of created {@link CommentedSpectrum} or null if there was an error.
	 * @throws ToolsException In case of error for the operation.
	 */
	public static List<CommentedSpectrum> doOperation(CommentedSpectrum cs,
			CommentedSpectrum cs2, XAxisCassis axis, double xMin, double xMax,
			double sampling, Operation operation, boolean avoidResample) throws ToolsException {
		if (!(operation == Operation.ADD || operation == Operation.SUBTRACT
				|| operation == Operation.DIVIDE)) {
			LOGGER.error("The specified operation can not be performed here.");
			return null;
		}

		if ((Double.isNaN(sampling) || avoidResample) && Arrays.equals(cs.getFrequenciesSignal(), cs2.getFrequenciesSignal())) {
			return doOperationWithoutResampling(cs, cs2, axis, xMin, xMax, operation);
		}
		return doOperationWithResampling(cs, cs2, axis, xMin, xMax, sampling, operation);
	}

	/**
	 * Compute the number of points who are smaller than the given value in the given sorted array.
	 * @param sortedPoints The sorted array.
	 * @param min The minimum value.
	 * @return the number of points who are smaller than the given value.
	 */
	private static int getTooLittle(double[] sortedPoints, double min) {
		int tooLittle = 0;
		for (int i = 0; i < sortedPoints.length && sortedPoints[i] < min; i++) {
			tooLittle++;
		}
		return tooLittle;
	}

	/**
	 * Compute the number of points who are bigger than the given value in the given sorted array.
	 * @param sortedPoints The sorted array.
	 * @param max The maximum value.
	 * @return the number of points who are bigger than the given value.
	 */
	private static int getTooBig(double[] sortedPoints, double max) {
		int tooBig = 0;
		for (int i = sortedPoints.length - 1; i >= 0 && sortedPoints[i] > max; i--) {
			tooBig++;
		}
		return tooBig;
	}



	/**
	 * Create a new {@link CommentedSpectrum}.
	 *
	 * @param cs The {@link CommentedSpectrum} used to copy some parameters from.
	 * @param x The X points of the new {@link CommentedSpectrum} in MHz.
	 * @param y The Y point of the new {@link CommentedSpectrum}.
	 * @return The created {@link CommentedSpectrum}.
	 */
	public static CommentedSpectrum createNewCommentedSpectrum(CommentedSpectrum cs,
			double[] x, double[] y) {
		CommentedSpectrum newCommentedSpectrum = new CommentedSpectrum(null, x, y, null);
		newCommentedSpectrum.setVlsr(cs.getVlsr());
		newCommentedSpectrum.setFreqRef(cs.getFreqRef());
		newCommentedSpectrum.setLoFreq(cs.getLoFreq());
		newCommentedSpectrum.setTypeFreq(cs.getTypeFreq());
		newCommentedSpectrum.setCassisMetadataList(cs.getCassisMetadataList());
		newCommentedSpectrum.setOriginalMetadataList(cs.getOriginalMetadataList());
		newCommentedSpectrum.setTitle(cs.getTitle());
		return newCommentedSpectrum;
	}

	/**
	 * Create a new {@link XYSpectrumSeries}.
	 *
	 * @param serie The {@link XYSpectrumSeries} used to copy some parameters from.
	 * @param cs The {@link CommentedSpectrum} used as base for the new {@link XYSpectrumSeries}.
	 * @param name The name of the new {@link XYSpectrumSeries}.
	 * @param color The color of the new {@link XYSpectrumSeries}, if null a new color is generated.
	 * @return The created {@link XYSpectrumSeries}.
	 */
	public static XYSpectrumSeries createNewSerie(XYSpectrumSeries serie,
			CommentedSpectrum cs, String name, Color color) {
		if (serie == null || cs == null) {
			return null;
		}
		XYSpectrumSeries newSerie = new XYSpectrumSeries(name, serie.getXAxis(),
				serie.getyAxis(), TypeCurve.RESULT, cs);
		if (color == null) {
			color = ColorsCurve.getNewColorResult();
		}
		newSerie.getConfigCurve().setColor(color);

		return newSerie;
	}

	/**
	 * Create a new {@link XYSpectrumSeries} by doing a redshift on the wave
	 *
	 * @param serie The {@link XYSpectrumSeries} with wave values on sky.
	 * @param name The name of the new series.
	 * @param color The color of the new series.
	 * @return The created {@link XYSpectrumSeries} with the wave values on rest
	 * according to the redshift.
	 */
	public static XYSpectrumSeries doRedshift(XYSpectrumSeries serie, String name,
			Color color, double value) {
		return ToolsXScale.doRedshift(serie, name, color, value);
	}

	/**
	 * Create a new {@link CommentedSpectrum} by doing a redshift on the wave
	 *
	 * @param cs The {@link CommentedSpectrum} on which is based the resampling.
	 * @param value The value of the redshift.
	 * @return The created {@link CommentedSpectrum}
	 */
	public static CommentedSpectrum doRedshift(CommentedSpectrum cs, double value) {
		return ToolsXScale.doRedshift(cs, value);
	}

	/**
	 * Create a new {@link CommentedSpectrum} by doing a wavelength air to vacuum conversion.
	 * In order to do that, the formula used is :
	 * s = 10000 / λair
	 * n = 1 + 0.00008336624212083 + 0.02408926869968 / (130.1065924522 - s²) +
	 *  0.0001599740894897 / (38.92568793293 - s²)
	 * Then the conversion is λvac = λair * n.
	 *
	 * @param cs The {@link CommentedSpectrum} with values on Air.
	 * @return The created {@link CommentedSpectrum} with the values on Vacuum.
	 */
	public static CommentedSpectrum doAirToVacuum(CommentedSpectrum cs) {
		return ToolsXScale.doAirToVacuum(cs);
	}

	/**
	 * Create a new {@link CommentedSpectrum} by doing a wavelength vacuum to air conversion.
	 * In order to do that, the formula used is :
	 * s = 10000 / λvac
	 * n = 1 + 0.0000834254 + 0.02406147 / (130 - s²) + 0.00015998 / (38.9 - s²)
	 * The conversion is then: λair = λvac / n.
	 *
	 * @param cs The {@link CommentedSpectrum} with values on vacuum.
	 * @return The created {@link CommentedSpectrum} with the values on air.
	 */
	public static CommentedSpectrum doVacuumToAir(CommentedSpectrum cs) {
		return ToolsXScale.doVacuumToAir(cs);
	}


	/**
	 * Create a new {@link XYSpectrumSeries} by doing a wavelength air to vacuum conversion.
	 * In order to do that, the formula used is :
	 * s = 10000 / λair
	 * n = 1 + 0.00008336624212083 + 0.02408926869968 / (130.1065924522 - s²) +
	 *  0.0001599740894897 / (38.92568793293 - s²)
	 * Then the conversion is λvac = λair * n.
	 *
	 * @param serie The {@link XYSpectrumSeries} with values on Air.
	 * @param name The name of the new series.
	 * @param color The color of the new series.
	 * @return The created {@link XYSpectrumSeries} with the values on Vacuum.
	 */
	public static XYSpectrumSeries doAirToVacuum(XYSpectrumSeries serie,
			String name, Color color) {
		return ToolsXScale.doAirToVacuum(serie, name, color);
	}

	/**
	 * Create a new {@link XYSpectrumSeries} by doing a wavelength vacuum to air conversion.
	 * In order to do that, the formula used is :
	 * s = 10000 / λvac
	 * n = 1 + 0.0000834254 + 0.02406147 / (130 - s²) + 0.00015998 / (38.9 - s²)
	 * The conversion is then: λair = λvac / n.
	 *
	 * @param serie The {@link XYSpectrumSeries} with values on vacuum.
	 * @param name The name of the new series.
	 * @param color The color of the new series.
	 * @return The created {@link XYSpectrumSeries} with the values on air.
	 */
	public static XYSpectrumSeries doVacuumToAir(XYSpectrumSeries serie,
			String name, Color color) {
		return ToolsXScale.doVacuumToAir(serie, name, color);
	}

	/**
	 * Create a new {@link CommentedSpectrum} by changing the TypeFrequency to REST from
	 * a series with a SKY TypeFrequency.
	 *
	 * @param cs The {@link XYSpectrumSeries} with the SKY TypeFrequency.
	 * @param vlsr The value of the new VLSR.
	 * @return The created {@link CommentedSpectrum} or null if there was an error.
	 */
	public static CommentedSpectrum doSkyToRest(CommentedSpectrum cs, double vlsr) {
		return ToolsXScale.doSkyToRest(cs, vlsr);
	}

	/**
	 * Create a new {@link CommentedSpectrum} by changing the TypeFrequency to SKY from
	 * a series with a REST TypeFrequency.
	 *
	 * @param cs The {@link CommentedSpectrum} with the REST TypeFrequency.
	 * @return The created {@link CommentedSpectrum} or null if there was an error.
	 */
	public static CommentedSpectrum doRestToSky(CommentedSpectrum cs) {
		return ToolsXScale.doRestToSky(cs);
	}


	/**
	 * Create a new {@link XYSpectrumSeries} by changing the TypeFrequency to SKY from
	 * a serie with a REST TypeFrequency.
	 *
	 * @param serie The {@link XYSpectrumSeries} with the REST TypeFrequency.
	 * @param nameNewCurve The name of the new {@link XYSpectrumSeries}
	 * @param color The color of the new serie.
	 * @return The created {@link XYSpectrumSeries} or null if there was an error.
	 */
	public static XYSpectrumSeries doRestToSky(XYSpectrumSeries serie,
			String nameNewCurve, Color color) {
		return ToolsXScale.doRestToSky(serie, nameNewCurve, color);
	}

	/**
	 * Create a new {@link XYSpectrumSeries} by changing the TypeFrequency to REST from
	 * a series with a SKY TypeFrequency.
	 *
	 * @param serie The {@link XYSpectrumSeries} with the SKY TypeFrequency.
	 * @param vlsr The value of the new VLSR.
	 * @param name The name of the new {@link XYSpectrumSeries}
	 * @param color The color of the new serie.
	 * @return The created {@link XYSpectrumSeries} or null if there was an error.
	 */
	public static XYSpectrumSeries doSkyToRest(XYSpectrumSeries serie, double vlsr,
			String name, Color color) {
		return ToolsXScale.doSkyToRest(serie, vlsr, name, color);
	}

	/**
	 * Create a new {@link CommentedSpectrum} by changing the VLSR.
	 *
	 * @param cs The {@link CommentedSpectrum} used a base for the new spectrum after the operation.
	 * @param vlsr The new VLSR.
	 * @return The created {@link CommentedSpectrum} or null if there was an error.
	 */
	public static CommentedSpectrum doVlsrChange(CommentedSpectrum cs, double vlsr) {
		return ToolsXScale.doVlsrChange(cs, vlsr);
	}

	/**
	 * Create a new {@link XYSpectrumSeries} by changing the VLSR.
	 *
	 * @param serie The {@link XYSpectrumSeries} used a base for the new series after the operation.
	 * @param name The name of the new {@link XYSpectrumSeries}.
	 * @param color The color of the new series.
	 * @param vlsr The new VLSR.
	 * @return The created {@link XYSpectrumSeries} or null if there was an error.
	 */
	public static XYSpectrumSeries doVlsrChange(XYSpectrumSeries serie, String name,
			Color color, double vlsr) {
		return ToolsXScale.doVlsrChange(serie, name, color, vlsr);
	}
}
