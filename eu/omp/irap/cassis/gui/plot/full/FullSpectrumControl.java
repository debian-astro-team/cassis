/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.full;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.event.EventListenerList;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.Range;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.cassisd.ServerImpl;
import eu.omp.irap.cassis.common.CassisException;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.TypeFrequency;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.XAxisVelocity;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.common.axes.YAxisGeneric;
import eu.omp.irap.cassis.common.axes.Y_AXIS;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.file.linelist.RotationalLineListFileReader;
import eu.omp.irap.cassis.file.linelist.SpectralLineListFileReader;
import eu.omp.irap.cassis.fit.FitParametersModel;
import eu.omp.irap.cassis.fit.components.FitAbstractComponent;
import eu.omp.irap.cassis.fit.computing.FitEstimator;
import eu.omp.irap.cassis.fit.util.FitException;
import eu.omp.irap.cassis.fit.util.enums.FitType;
import eu.omp.irap.cassis.gui.PanelFrame;
import eu.omp.irap.cassis.gui.fit.FitOperation;
import eu.omp.irap.cassis.gui.fit.FitPanel;
import eu.omp.irap.cassis.gui.fit.FitPanelControl;
import eu.omp.irap.cassis.gui.fit.FitPanelListener;
import eu.omp.irap.cassis.gui.fit.SpectrumFitPanelListener;
import eu.omp.irap.cassis.gui.fit.advanced.AdvancedFitModel;
import eu.omp.irap.cassis.gui.fit.advanced.AdvancedFitResult;
import eu.omp.irap.cassis.gui.fit.advanced.ModelFitManager;
import eu.omp.irap.cassis.gui.fit.advanced.gui.AdvancedFitFrame;
import eu.omp.irap.cassis.gui.fit.advanced.history.FitSubstractCommand;
import eu.omp.irap.cassis.gui.fit.advanced.interfaces.FitActionsInterface;
import eu.omp.irap.cassis.gui.fit.advanced.interfaces.FitSourceInterface;
import eu.omp.irap.cassis.gui.fit.save.SaveFit;
import eu.omp.irap.cassis.gui.menu.action.SpectrumManagerAction;
import eu.omp.irap.cassis.gui.model.CassisModel;
import eu.omp.irap.cassis.gui.model.spectrumanalysis.SpectrumAnalysisModel;
import eu.omp.irap.cassis.gui.model.table.ModelIdentifiedInterface;
import eu.omp.irap.cassis.gui.otherspecies.SpeciesColorChangedEvent;
import eu.omp.irap.cassis.gui.otherspecies.SpeciesControl;
import eu.omp.irap.cassis.gui.otherspecies.SpeciesEnableEvent;
import eu.omp.irap.cassis.gui.otherspecies.SpeciesListener;
import eu.omp.irap.cassis.gui.plot.abstractplot.AbstractPlotControl;
import eu.omp.irap.cassis.gui.plot.curve.CurvePanelModel;
import eu.omp.irap.cassis.gui.plot.curve.CurvePanelView;
import eu.omp.irap.cassis.gui.plot.curve.JPanelCurve;
import eu.omp.irap.cassis.gui.plot.curve.Rendering;
import eu.omp.irap.cassis.gui.plot.curve.TypeCurve;
import eu.omp.irap.cassis.gui.plot.curve.XYLineSeriesCassis;
import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassis;
import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassisCollection;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import eu.omp.irap.cassis.gui.plot.curve.config.ColorsCurve;
import eu.omp.irap.cassis.gui.plot.infopanel.InfoControl;
import eu.omp.irap.cassis.gui.plot.infopanel.InfoPanel;
import eu.omp.irap.cassis.gui.plot.infopanel.InfoPanelConstants;
import eu.omp.irap.cassis.gui.plot.simple.SpectrumPlot;
import eu.omp.irap.cassis.gui.plot.simple.util.XYPlotCassisUtil;
import eu.omp.irap.cassis.gui.plot.util.InterValMarkerCassis;
import eu.omp.irap.cassis.gui.plot.util.MarkerManager;
import eu.omp.irap.cassis.gui.plot.util.MarkerView;
import eu.omp.irap.cassis.gui.plot.util.VelocityAxisDialog;
import eu.omp.irap.cassis.properties.Software;

public class FullSpectrumControl extends AbstractPlotControl {

	private static final Logger LOGGER = LoggerFactory.getLogger(FullSpectrumControl.class);

	private FullSpectrumModel model;
	private SpeciesControl speciesControl;
	private FullSpectrumView view;
	private EventListenerList listeners;
	private FitPanelControl fitControl;

	private SaveFit saveFit;
	private InfoControl infoPanelControl;
	private FitPanelListener spectrumFitPanelListener;

	private VelocityAxisDialog dialog;


	public FullSpectrumControl(FullSpectrumModel model) {
		this.model = model;
		model.addModelListener(this);
		this.model.getFitModelManager().addModelListener(this);
		speciesControl = new SpeciesControl(model.getSpeciesModel());

		fitControl = new FitPanelControl();
		FitPanel fitPanel = new FitPanel(new MarkerManager(this), false, fitControl);
		fitPanel.setXAxis(model.getXAxisCassis());
		fitControl.setFitPanel(fitPanel);
		this.spectrumFitPanelListener = new SpectrumFitPanelListener(this, fitPanel, null, 1, false);
		fitPanel.addFitPanelListener(spectrumFitPanelListener);

		view = new FullSpectrumView(this);
		infoPanelControl = view.getInfoPanel().getControl();
		infoPanelControl.addInfoPanelListener(this);
		listeners = new EventListenerList();
		saveFit = new SaveFit();
	}

	public void setFitPanelListener(FitPanelListener fitPanelListener) {
		view.getFitPanel().removeFitPanelListener(this.spectrumFitPanelListener);
		this.spectrumFitPanelListener = fitPanelListener;
		view.getFitPanel().addFitPanelListener(spectrumFitPanelListener);
	}

	public void onLineListButtonClicked() {
		JFileChooser chooser = new CassisJFileChooser(null, Software.getLastFolder("overlay-linelist"));
		chooser.addChoosableFileFilter(new FileNameExtensionFilter("Line list CASSIS format (*.txt)","txt"));

		int returnVal = chooser.showOpenDialog(view);
		if (chooser.getSelectedFile() != null && returnVal == JFileChooser.APPROVE_OPTION) {
			Software.setLastFolder("overlay-linelist", chooser.getCurrentDirectory().getAbsolutePath());

			String filePath = chooser.getSelectedFile().getPath();
			displayLineList(filePath);
		}
	}

	/**
	 * Open the SpectrumManagerAction to select a spectrum to overlay on the current data.
	 */
	public void onOverlayPanelDataFileButtonClicked() {
		Runnable r = new Runnable() {
			@Override
			public void run() {
				CassisSpectrum cs = SpectrumManagerAction.getInstance().getSelectedSpectrum();
				if (cs == null) {
					JOptionPane.showMessageDialog(null,
							"Please select a spectrum first.",
							"No spectrum selected!",
							JOptionPane.WARNING_MESSAGE);
				} else {
					try {
						openOverlay(cs);
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								PanelFrame.getInstance().setVisible(true);
								PanelFrame.getInstance().toFront();
								PanelFrame.getInstance().repaint();
							}
						});
					} catch (CassisException ce) {
						if ("Frequency limits out of data file frequency range.".equals(ce.getMessage())) {
							LOGGER.error("Frequency limits out of data file frequency range", ce);
							JOptionPane.showMessageDialog(view, ce.getMessage(),
									"Alert", JOptionPane.ERROR_MESSAGE);
						}
					}
				}
			}
		};
		SpectrumManagerAction.getInstance().setLoadBinding(r, "Full Spectrum - Overlay");
	}

	public void openOverlay(CassisSpectrum cassisSpectrum) throws CassisException {
		SpectrumAnalysisModel tmpSamModel = new SpectrumAnalysisModel();
		tmpSamModel.setCassisSpectrum(cassisSpectrum);
		tmpSamModel.setValMin(getMinFrequencyPlot());
		tmpSamModel.setValMin(getMaxFrequencyPlot());
		tmpSamModel.setValUnit(UNIT.MHZ);
		if (cassisSpectrum.getVlsr() == 0) {
			tmpSamModel.getLoadDataModel().setTypeFrequency(TypeFrequency.SKY);
		} else {
			tmpSamModel.getLoadDataModel().setTypeFrequency(TypeFrequency.REST);
		}

		CommentedSpectrum[] result1 = new ServerImpl().readFile(cassisSpectrum,
				getMinFrequencyPlot(),getMaxFrequencyPlot());
		displayCommentedSpectrum(result1, tmpSamModel);
	}

	@Override
	public void cassisSpectrumEvent(EventObject e) {
		CassisSpectrum cassisSpectrum = (CassisSpectrum) e.getSource();
		try {
			SpectrumAnalysisModel spectrumAnalysisModel = new SpectrumAnalysisModel();
			spectrumAnalysisModel.setValMin(getMinFrequencyPlot());
			spectrumAnalysisModel.setValMax(getMaxFrequencyPlot());
			spectrumAnalysisModel.setCassisSpectrum(cassisSpectrum);

			CommentedSpectrum[] spectrums = spectrumAnalysisModel.readFile();

			displayCommentedSpectrum(spectrums, spectrumAnalysisModel);
		} catch (CassisException ce) {
			LOGGER.info("Can't compute the full spectrum overlay in range: [{}, {}]",
					getMinFrequencyPlot(), getMaxFrequencyPlot(), ce);
		}
	}

	private void displayCommentedSpectrum(CommentedSpectrum[] spectrums,
			ModelIdentifiedInterface modelIdentifiedInterface) {
		if (spectrums != null && spectrums[0] != null) {
			if (!spectrums[0].getyAxis().equals(model.getYAxisCassis()) && askWrongYAxis()) {
				return;
			}
			CommentedSpectrum commentedSpectrum = spectrums[0];

			CassisModel fileModel = new CassisModel(commentedSpectrum, modelIdentifiedInterface);
			//TODO A voir
			fileModel.setSpectrumName(commentedSpectrum.getTitle());
			fileModel.setConfigName(commentedSpectrum.getTitle());

			model.addOverlayFullSpectrumDataset(fileModel);
			model.setYAxisCassis(commentedSpectrum.getyAxis());

			displayDataset();
		} else {
			JOptionPane.showMessageDialog(view, "Full spectrum overlay not exist",
					"Alert", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Ask user what he want to do about the wrong Y-Axis.
	 *
	 * @return if the user want to stop the operation.
	 */
	private boolean askWrongYAxis() {
		String message = "The data do not have the same Y-axis unit as the current plotted ones.\n"
				+ "It is advised to press \"Cancel\" and to choose data with the same Y-axis\n"
				+ "or to remove the current plot(s) before displaying again. If you know what\n"
				+ "you are doing, you can press \"Continue\", but be aware that the Y-axis\n"
				+ "scale and unit will likely be meaningless.";
		String[] choices = { "Continue", "Cancel" };
		int r = JOptionPane.showOptionDialog(view, message, "Warning : different Y-axis",
				JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null,
				choices, choices[1]);
		return r != 0;
	}

	/**
	 * Create the plot of spectrum analysis : File model
	 *
	 * @param fileModel
	 *            the model to displayseriesCount >
	 */
	public void createFileplot(CassisModel fileModel) {

		int seriesCount = model.getCenterDataset().getSeriesCount();
		boolean yAxisCompatibility = true;
		YAxisCassis currentYAxisCassis = model.getYAxisCassis();
//		Y_AXIS currentYAxis = currentYAxisCassis.getAxis();
		CommentedSpectrum spectrum = fileModel.getSpectrumSignal();
		YAxisCassis newYAxisCassis = spectrum.getyAxis();
//		Y_AXIS newYAxis = newYAxisCassis.getAxis();

		if (seriesCount == 0) {
			view.getComboBottomAxisType().setSelectedItem(findAxis(fileModel.getModel().getXaxisAskToDisplay()));
			view.getComboBottomtAxis().setSelectedItem(findtAxis(fileModel.getModel().getXaxisAskToDisplay()));

			try {
				currentYAxisCassis = newYAxisCassis.clone();
			} catch (CloneNotSupportedException e) {
				LOGGER.error("Clone error, unable to clone the axis", e);
			}
			changeYAxis(currentYAxisCassis);
			yAxisLeftChanged();
		} else {
			yAxisCompatibility = newYAxisCassis.equals(currentYAxisCassis) ||
					YAxisCassis.isCompatible(newYAxisCassis,currentYAxisCassis);

			if (! yAxisCompatibility) {
				yAxisCompatibility = ! askWrongYAxis();
			}
		}

		if (yAxisCompatibility) {


			boolean res = true;
			if (seriesCount > 0) {
				res = SpectrumPlot.askYParameters(currentYAxisCassis, newYAxisCassis, spectrum, null);
			}
			if (res) {
				int uniqueID = fileModel.getModelId();
				XYSeriesCassis serieRef = model.removeSeries(uniqueID, model.getCenterDataset());
				LOGGER.debug("****** Id = " + uniqueID + " " + "serie =" + serieRef);
				XYSpectrumSeries newSerie = model.createSerie(fileModel, serieRef);
				view.getCreationPanel().enableAll(true);

				model.setParametersForNewDataRef(spectrum);
				speciesControl.initThresholdModel(fileModel.getModel().getThresholdModel());
				model.removeBottomDataset();


				// TODO a revoir
				fitControl.getCurrentFitPanel().initHistory(newSerie);
				comboBottomAxisChanged(false);
				yAxisLeftChanged();
				displayDataset();

				view.getMessageControl().removeAllMessages();
				view.getToolsView().refresh();
			}
		}
	}

	private void changeYAxis(YAxisCassis newYAxis) {
		YAxisCassis item = newYAxis;
		if (newYAxis.getAxis() == Y_AXIS.UNKNOW) {
			final JComboBox<YAxisCassis> comboLeftAxis = view.getYAxisLeftComboBox();
			int nb = comboLeftAxis.getModel().getSize();
			for (int i = 0; i< nb; i++) {
				final YAxisCassis itemAt = comboLeftAxis.getItemAt(i);
				if (itemAt.getAxis() == Y_AXIS.UNKNOW) {
					YAxisGeneric yAxisGeneric = (YAxisGeneric)itemAt;
					item = itemAt;
					yAxisGeneric.setInformationName(newYAxis.getInformationName());
					yAxisGeneric.setUnitGeneric(newYAxis.getUnitString());
				}
			}
		}

		view.getYAxisLeftComboBox().setSelectedItem(item);
		view.getYAxisRightComboBox().setSelectedItem(item);
		model.setYAxisCassis(newYAxis);

	}

	private XAxisCassis findtAxis(XAxisCassis getxAxisOrigin) {
		final JComboBox<XAxisCassis> comboBottomtAxis = view.getComboBottomtAxis();
		int nb = comboBottomtAxis.getModel().getSize();
		for (int i = 0; i< nb; i++) {
			final XAxisCassis itemAt = comboBottomtAxis.getItemAt(i);
			if (itemAt.equals(getxAxisOrigin))
				return itemAt;
		}

		return null;
	}

	private String findAxis(XAxisCassis getxAxisOrigin) {
		final JComboBox<String> comboBottomtAxis = view.getComboBottomAxisType();
		int nb = comboBottomtAxis.getModel().getSize();
		for (int i = 0; i< nb; i++) {
			final String itemAt = comboBottomtAxis.getItemAt(i);
			if (itemAt.equals(getxAxisOrigin.getAxis().toString()))
				return itemAt;
		}

		return null;
	}

	/**
	 * @see MarkerView
	 */
	@Override
	public double computeIntegral(List<InterValMarkerCassis> markerList) {
		return FitOperation.computeIntegral(model.getSeriesToFit(), markerList);
	}

	/**
	 * @see MarkerView
	 */
	// TODO Refactoring : duplicate code with AbstractMozaicPlotControl to put in
	// AbstractPlotModel
	@Override
	public void doGaussianDefaultParametersAction(double startValue, double endValue) {
		super.doGaussianDefaultParametersAction(startValue, endValue, model, fitControl);
	}

	public FullSpectrumModel getModel() {
		return model;
	}

	public SpeciesControl getSpeciesControl() {
		return speciesControl;
	}

	public FullSpectrumView getView() {
		return view;
	}

	public void addFullSpectrumListener(FullSpectrumListener listener) {
		listeners.add(FullSpectrumListener.class, listener);
	}

	public void removeFullSpectrumListener(FullSpectrumListener l) {
		listeners.remove(FullSpectrumListener.class, l);
	}

	public void fireFullSpectrumXYPlotRemoved(XYSeriesCassis serie) {
		FullSpectrumListener[] listenerList = listeners.getListeners(FullSpectrumListener.class);

		for (FullSpectrumListener listener : listenerList) {
			listener.fullSpectrumXYPlotRemoved(new EventObject(serie));
		}
		if (TypeCurve.DATA.equals(serie.getTypeCurve()) ||
			TypeCurve.OVERLAY_DATA.equals(serie.getTypeCurve())) {
			if (model.getCurrentDataCurve() != null) {
				model.setParametersForNewDataRef(model.getCurrentDataCurve().getSpectrum());
			} else {
				model.setParametersForNewDataRef(null);
				model.removeBottomDataset();
			}
		}
	}

	public void fireFullSpectrumXYPlotLeftMouseCliked(XYSeriesCassis serie) {
		FullSpectrumListener[] listenerList = listeners.getListeners(FullSpectrumListener.class);

		for (FullSpectrumListener listener : listenerList) {
			listener.fullSpectrumXYPlotLeftMouseCliked(new EventObject(serie));
		}
	}

	public FitPanelControl getFitPanelControl() {
		return fitControl;
	}

	public void setView(FullSpectrumView fullSpectrumView) {
		this.view = fullSpectrumView;
	}

	@Override
	public void setCassisSpectrum(CassisSpectrum cassisSpectrum) {
		CommentedSpectrum[] result1;
		try {
			SpectrumAnalysisModel tmpModel = new SpectrumAnalysisModel();
			tmpModel.setCassisSpectrum(cassisSpectrum);
			tmpModel.setValMin(getMinFrequencyPlot());
			tmpModel.setValMin(getMaxFrequencyPlot());
			tmpModel.setValUnit(UNIT.MHZ);

			result1 = new ServerImpl().readFile(cassisSpectrum, getMinFrequencyPlot(), getMaxFrequencyPlot());
			displayCommentedSpectrum(result1, tmpModel);
		} catch (CassisException ce) {
			LOGGER.error("Error while displaying CommentedSpectrum", ce);
		}
	}

	@Override
	public void setNameData(String titre) {
		// Not used.
	}

	@Override
	public void curveCassisChange(CurvePanelModel curvePanelModel) {
		XYSeriesCassis serie = curvePanelModel.getCassisModel();

		if (serie.getTypeCurve() == TypeCurve.MARKERS) {
			displaySelection(curvePanelModel.getCassisModel().getConfigCurve().isVisible());
			return;
		}

		int i = model.getCenterDataset().indexOf(serie.getKey());
		if (i != -1) {
			handleCenterCurveChange(serie, i);
			return;
		}
		i = view.getSpectrumPlot().getBottomLineSignalSeriesCollection().indexOf(serie.getKey());
		if (i != -1) {
			handleCurveBottomLineSignalChange(curvePanelModel, serie, i);
			return;
		}
		i = view.getSpectrumPlot().getBottomLineImageSeriesCollection().indexOf(serie.getKey());
		if (i != -1) {
			handleCurveBottomLineImageChange(curvePanelModel, serie, i);
			return;
		}
		i = view.getSpectrumPlot().getTopLineSeriesCollection().indexOf(serie.getKey());
		if (i != -1) {
			XYPlotCassisUtil.setRenderSeriesParameters(view.getSpectrumPlot().getTopLineRenderer(), i,
					serie.getConfigCurve());
			return;
		}
		i = view.getSpectrumPlot().getTopLineErrorSeriesCollection().indexOf(serie.getKey());
		if (i != -1) {
			XYPlotCassisUtil.setRenderSeriesParameters(view.getSpectrumPlot().getTopLineErrorRenderer(), i,
					serie.getConfigCurve());
			return;
		}
	}

	/**
	 * Handle the a change on an other species signal.
	 *
	 * @param curvePanelModel The CurvePanelModel.
	 * @param serie The series.
	 * @param index The index of the series inside its dataset.
	 */
	private void handleCurveBottomLineSignalChange(CurvePanelModel curvePanelModel,
			XYSeriesCassis serie, int index) {
		XYPlotCassisUtil.setRenderSeriesParameters(view.getSpectrumPlot().getBottomLineSignalRenderer(), index,
				serie.getConfigCurve());
		model.getSpeciesModel().setColorSpeciesSignal(
				curvePanelModel.getCassisModel().getConfigCurve().getColor());
		model.getSpeciesModel().setSpeciesSignalVisible(
				curvePanelModel.getCassisModel().getConfigCurve().isVisible());
	}

	/**
	 * Handle the a change on an other species image.
	 *
	 * @param curvePanelModel The CurvePanelModel.
	 * @param serie The series.
	 * @param index The index of the series inside its dataset.
	 */
	private void handleCurveBottomLineImageChange(CurvePanelModel curvePanelModel,
			XYSeriesCassis serie, int i) {
		XYPlotCassisUtil.setRenderSeriesParameters(view.getSpectrumPlot().getBottomLineImageRenderer(), i,
				serie.getConfigCurve());
		model.getSpeciesModel().setColorSpeciesImage(
				curvePanelModel.getCassisModel().getConfigCurve().getColor());
		model.getSpeciesModel().setSpeciesImageVisible(
				curvePanelModel.getCassisModel().getConfigCurve().isVisible());
	}

	/**
	 * Handle the a change on a center series.
	 *
	 * @param serie The series.
	 * @param index The index of the series inside its dataset.
	 */
	private void handleCenterCurveChange(XYSeriesCassis serie, int index) {
		boolean visibilityChanged = false;
		if (TypeCurve.DATA.equals(serie.getTypeCurve()) ||
			TypeCurve.OVERLAY_DATA.equals(serie.getTypeCurve())) {
			visibilityChanged =serie.getConfigCurve().isVisible() !=
				view.getSpectrumPlot().getCenterRenderer().getSeriesVisible(index);
		}

		XYPlotCassisUtil.setRenderSeriesParameters(view.getSpectrumPlot().getCenterRenderer(), index, serie.getConfigCurve());
		if (visibilityChanged) {
			if (model.getCurrentDataCurve() != null)
				model.setParametersForNewDataRef(model.getCurrentDataCurve().getSpectrum());
			else
				model.setParametersForNewDataRef(null);
		}
	}

	@Override
	public void deleteButtonInfoPanelClicked(JPanelCurve panelCurve) {
		LOGGER.debug("Suppression du JSeriesPanel");
		int answer = JOptionPane.YES_OPTION;

		for (CurvePanelView curvePanelView : panelCurve.getListCurvePanelView()) {
			XYSeriesCassis serie = curvePanelView.getModel().getCassisModel();
			TypeCurve typeCurve = serie.getTypeCurve();
			if (TypeCurve.DATA.equals(typeCurve)) {
				int id = serie.getId();
				fireFullSpectrumXYPlotLeftMouseCliked(serie);
				String message = "Do you want to close this frame and delete the plot ?";
				answer = panelCurve.getAutoDestroy() ? JOptionPane.YES_OPTION : JOptionPane.showConfirmDialog(
						null, message, "Exit", JOptionPane.YES_NO_OPTION);

				if (answer == JOptionPane.YES_OPTION) {
					model.removeSeries(id, model.getCenterDataset());
					fireFullSpectrumXYPlotRemoved(serie);
				}
			}
			else if (TypeCurve.OTHER_SPECIES_SIGNAL.equals(typeCurve)
					|| TypeCurve.OTHER_SPECIES_IMAGE.equals(typeCurve)) {
				model.removeBottomDataset();
				curvePanelView.removeCurveCassisListener(this);
				view.getOtherSpeciesPanel().setOtherDisplayButtonEnabled(true);
			}
			else if (TypeCurve.OVERLAY_DATA.equals(typeCurve) || TypeCurve.OVERLAY_LINELIST.equals(typeCurve)) {
				model.removeOverlayDataset(serie);
				curvePanelView.removeCurveCassisListener(this);
			}
			else if (typeCurve.isFit()) {
				model.removeFitAndResiudalCurve();
				curvePanelView.removeCurveCassisListener(this);
			}
			else if (TypeCurve.RESULT.equals(typeCurve)) {
				model.removeResults();
				curvePanelView.removeCurveCassisListener(this);
			}
			else if (TypeCurve.MARKERS.equals(typeCurve)) {
				view.getFitPanel().getMarkerManager().resetAllSelection(
						view.getSpectrumPlot().getPlot());
			}
		}
		if (answer == JOptionPane.YES_OPTION) {
			view.getInfoPanel().removePanelCurve(panelCurve);
			displayDataset();
		}
	}

	public void comboBottomAxisChanged() {
		comboBottomAxisChanged(true);
	}

	public void comboTopAxisChanged() {
		comboTopAxisChanged(true);
	}

	public void comboBottomAxisChanged(boolean askParameters) {
		if (view.getComboBottomtAxis().getSelectedItem() == null) {
			return;
		}

		XAxisCassis previousAxis = model.getxAxis();
		XAxisCassis xAxisCassis = (XAxisCassis) view.getComboBottomtAxis().getSelectedItem();

		if (xAxisCassis instanceof XAxisVelocity) {
			XAxisVelocity axisVelocity = (XAxisVelocity) xAxisCassis;
			axisVelocity.setFreqRef(model.getFreqRef());

			if (askParameters) {
				VelocityAxisDialog dialog = new VelocityAxisDialog(
						null, XAxisCassis.getXAxisCassis(UNIT.MHZ).convertFromMhzFreq(axisVelocity.getFreqRef()));
				dialog.setVisible(true);
				axisVelocity.setFreqRef(dialog.getFreqRef());
			}

			model.setFreqRef(axisVelocity.getFreqRef());
		}
		xAxisCassis.setLoFreq(model.getXAxisCassis().getLoFreq());

		model.setXAxisCassis(xAxisCassis);
		view.getSpectrumPlot().changeXAxis(model.getXAxisCassis());
		view.getSpectrumPlot().setBottomTitle(xAxisCassis.getTitleLabel());
		view.getFitPanel().setXAxis(xAxisCassis);
		comboTopAxisChanged(false);
		view.getToolsView().getControl().refreshInfo();

		model.getFitModelManager().refreshInputs();
		model.getFitModelManager().convertConfiguration(previousAxis);
	}

	public void comboTopAxisChanged(boolean askParameters) {
		if (view.getComboTopAxis().getSelectedItem() == null) {
			return;
		}
		XAxisCassis xAxisCassisTop = (XAxisCassis) view.getComboTopAxis().getSelectedItem();

		if (xAxisCassisTop instanceof XAxisVelocity) {
			XAxisVelocity axisVelocity = (XAxisVelocity) xAxisCassisTop;
			axisVelocity.setVlsr(model.getVlsrData());
			axisVelocity.setFreqRef(model.getFreqRef());
			if (askParameters) {
				dialog = new VelocityAxisDialog(
						null, XAxisCassis.getXAxisCassis(UNIT.MHZ).convertFromMhzFreq(axisVelocity.getFreqRef()));
				dialog.setVisible(true);
				axisVelocity.setFreqRef(dialog.getFreqRef());
			}
			model.setFreqRef(axisVelocity.getFreqRef());
		}
		ValueAxis domainAxis = view.getSpectrumPlot().getPlot().getDomainAxis();
		ValueAxis rangeAxis = view.getSpectrumPlot().getPlot().getRangeAxis();

		domainAxis.setAutoRange(true);
		rangeAxis.setAutoRange(true);

		double lowerBound = domainAxis.getRange().getLowerBound();
		double upperBound = domainAxis.getRange().getUpperBound();

		double lengthAxis = upperBound-lowerBound;
		double lowerMargin = lengthAxis * domainAxis.getLowerMargin() / (1+ domainAxis.getLowerMargin() + domainAxis.getUpperMargin());
		lowerBound = lowerBound + lowerMargin;

		double upperMargin = lengthAxis * domainAxis.getUpperMargin() / (1+ domainAxis.getLowerMargin() + domainAxis.getUpperMargin());
		upperBound = upperBound - upperMargin;


		XAxisCassis xAxisCassisBottom = model.getXAxisCassis();
		boolean isInverted = xAxisCassisTop.isInverted() != xAxisCassisBottom.isInverted();

		double min = XAxisCassis.convert(lowerBound, xAxisCassisBottom,
				xAxisCassisTop, model.getSpeciesModel().getTypeFrequency());
		double max = XAxisCassis.convert(upperBound, xAxisCassisBottom,
				xAxisCassisTop, model.getSpeciesModel().getTypeFrequency());

		double temp = min;
		min = Math.min(temp, max);
		max = Math.max(temp, max);

		lengthAxis = max-min;

		min = min - domainAxis.getLowerMargin() * lengthAxis;
		max = max + domainAxis.getUpperMargin() * lengthAxis;
		model.setXAxisCassisTop(xAxisCassisTop);
		view.getSpectrumPlot().setXAxisCassisTop(xAxisCassisTop);
		view.getSpectrumPlot().changeTopAxisRange(min, max, isInverted);
		view.getSpectrumPlot().repaint();

	}

	public void removeAllButtonClicked() {
		InfoPanel infoPanel = view.getInfoPanel();
		if (!infoPanel.getModel().getListOfJPanelCurves().isEmpty()) {
			int ret = JOptionPane.showConfirmDialog(view, "Do you want to remove all the plots?",
					"Plot Info", JOptionPane.YES_NO_OPTION);
			if (ret == JOptionPane.YES_OPTION) {
				List<XYSeriesCassis> list = infoPanel.removeAllPanelCurve();
				for (XYSeriesCassis serie : list) {
					fireFullSpectrumXYPlotRemoved(serie);
				}
				ColorsCurve.resetNewColorData();
				model.removeAllData();
				view.getMessageControl().removeAllMessages();
				view.getInfoPanelScrollPane().validate();
				model.getFitModelManager().clearModels();
				AdvancedFitFrame.getFrame().openFrame(false);
			}
		}
	}

	public void displayDataset() {
		LOGGER.debug("DisplayDataset fullSpectrum");
		view.getInfoPanel().removeAllPanelCurve();

		XYPlotCassisUtil.configureRenderer(view.getSpectrumPlot());

		ArrayList<XYSeriesCassis> overlayDataList = new ArrayList<>();
		ArrayList<XYSeriesCassis> fitSerieList = new ArrayList<>();
		ArrayList<XYSeriesCassis> resultSerieList = new ArrayList<>();
		XYSeriesCassisCollection series = model.getCenterDataset();

		//TODO getSeries from model ...
		for (int i = 0; i < series.getSeriesCount(); i++) {
			XYSeriesCassis serieCassis = series.getSeries(i);
			if (serieCassis.getTypeCurve().isFit()) {
				fitSerieList.add(serieCassis);
			} else if (serieCassis.getTypeCurve() == TypeCurve.OVERLAY_DATA) {
				overlayDataList.add(serieCassis);
			} else if (serieCassis.getTypeCurve() == TypeCurve.RESULT) {
				resultSerieList.add(serieCassis);
			} else {
				createCurvePanel(serieCassis);
			}
		}

		createPanelCurveOverlayData(overlayDataList);
		createPanelCurveFit(fitSerieList);
		createPanelCurveLineList();
		createPanelCurveResult(resultSerieList);
		createOtherSpeciesPanelCurve(model.getBottomLineSignalDataset(),
				model.getBottomLineImageDataset());
		if (view.getFitPanel().getMarkerManager().haveMarker()) {
			addMarkersPanelCurve();
		}
		view.getStackMosaicPanel().setMoveCurrentSpectrum(true);
		view.changeState(view.getToolsState().name());
		view.repaint();
	}

	private void createPanelCurveResult(List<XYSeriesCassis> resultSerieList) {
		if (!resultSerieList.isEmpty()) {
			JPanelCurve jpcToolsResults = new JPanelCurve(
					InfoPanelConstants.TOOLS_RESULTS_TITLE, true);
			for (XYSeriesCassis serieCassis : resultSerieList) {
				CurvePanelView curvePanelView = new CurvePanelView(new CurvePanelModel(serieCassis));
				curvePanelView.addCurveCassisListener(this);
				jpcToolsResults.addCurvePane(curvePanelView);
			}
			view.getInfoPanel().addJPanelCurve(jpcToolsResults, true);
		}
	}

	private void createPanelCurveLineList() {
		XYSeriesCassisCollection series;
		CurvePanelView curvePanelViewLineList;
		series = model.getTopLineDataset();
		if (series.getSeriesCount() > 0) {
			JPanelCurve jpcLineList = new JPanelCurve(
					InfoPanelConstants.LINELIST_OVERLAYS_TITLE);

			for (int i = 0; i < series.getSeriesCount(); i++) {
				XYSeriesCassis serieCassis = series.getSeries(i);
				curvePanelViewLineList = new CurvePanelView(new CurvePanelModel(serieCassis));
				curvePanelViewLineList.addCurveCassisListener(this);
				jpcLineList.addCurvePane(curvePanelViewLineList);
			}
			view.getInfoPanel().addJPanelCurve(jpcLineList);
		}
	}

	private void createPanelCurveFit(ArrayList<XYSeriesCassis> fitSerieList) {
		if (!fitSerieList.isEmpty()) {
			JPanelCurve jpcFit = new JPanelCurve(InfoPanelConstants.FIT_CURVES_TITLE, true);

			for (XYSeriesCassis serieCassis : fitSerieList) {
				CurvePanelView curvePanelView = new CurvePanelView(new CurvePanelModel(serieCassis));
				curvePanelView.addCurveCassisListener(this);
				jpcFit.addCurvePane(curvePanelView);
			}
			view.getInfoPanel().addJPanelCurve(jpcFit, true);
		}
	}

	private void createPanelCurveOverlayData(ArrayList<XYSeriesCassis> overlayDataList) {
		if (!overlayDataList.isEmpty()) {
			JPanelCurve jpcOverlayData = new JPanelCurve(
					InfoPanelConstants.DATA_FILE_OVERLAYS_TITLE);

			for (XYSeriesCassis serieCassis : overlayDataList) {
				CurvePanelView curvePanelViewOverlayData = new CurvePanelView(new CurvePanelModel(serieCassis));
				curvePanelViewOverlayData.addCurveCassisListener(this);
				jpcOverlayData.addCurvePane(curvePanelViewOverlayData);
			}
			view.getInfoPanel().addJPanelCurve(jpcOverlayData);
		}
	}

	public void createOtherSpeciesPanelCurve(XYSeriesCassisCollection seriesSignal,
			XYSeriesCassisCollection seriesImage) {
		if (seriesSignal.getSeriesCount() + seriesImage.getSeriesCount() > 0) {
			JPanelCurve jpc = new JPanelCurve(InfoPanelConstants.OTHER_SPECIES_TITLE);
			if (seriesSignal.getSeriesCount() >= 1) {
				XYLineSeriesCassis serieOne = (XYLineSeriesCassis) seriesSignal.getSeries(0);
				CurvePanelView curvePanelViewSignal = new CurvePanelView(new CurvePanelModel(serieOne));
				curvePanelViewSignal.addCurveCassisListener(this);
				jpc.addCurvePane(curvePanelViewSignal);
			}
			if (seriesImage.getSeriesCount() >= 1) {
				XYLineSeriesCassis serieTwo = (XYLineSeriesCassis) seriesImage.getSeries(0);
				CurvePanelView curvePanelViewImage = new CurvePanelView(new CurvePanelModel(serieTwo));
				curvePanelViewImage.addCurveCassisListener(this);
				jpc.addCurvePane(curvePanelViewImage);
			}
			view.getInfoPanel().addJPanelCurve(jpc, true);
		}
	}

	private void createCurvePanel(XYSeriesCassis serieCassis) {
		String jPanelCurveTitle = serieCassis.getForcedPanelCurveTitle() == null ?
				(String) serieCassis.getKey() : serieCassis.getForcedPanelCurveTitle();
		JPanelCurve jPanelCurve = new JPanelCurve(jPanelCurveTitle);
		CurvePanelView curvePanelView = new CurvePanelView(new CurvePanelModel(serieCassis));
		curvePanelView.addCurveCassisListener(this);
		jPanelCurve.addCurvePane(curvePanelView);
		view.getInfoPanel().addJPanelCurve(jPanelCurve);
	}

	/**
	 * @see SpeciesListener#speciesColorSignalChanged
	 */
	@Override
	public void speciesColorSignalChanged(SpeciesColorChangedEvent event) {
		XYSeriesCassisCollection series = model.getBottomLineSignalDataset();

		for (int cpt = 0; cpt < series.getSeriesCount(); cpt++) {
			XYSeriesCassis fullSerie = series.getSeries(cpt);
			fullSerie.getConfigCurve().setColor(event.getNewColor());
			view.getSpectrumPlot().modifyBottom((String) fullSerie.getKey(), fullSerie.getConfigCurve());
			JPanelCurve jpc = view.getInfoPanel().getJPanelCurveByName(
					InfoPanelConstants.OTHER_SPECIES_TITLE);
			for (CurvePanelView cpv : jpc.getListCurvePanelView()) {
				if (InfoPanelConstants.SIGNAL_TITLE.equals(cpv.getNameFromModel())
						&& cpv.getButtonColor() != null) {
					cpv.getButtonColor().setBackground(event.getNewColor());
				}
			}
		}
	}

	/**
	 * @see SpeciesListener
	 */
	@Override
	public void speciesColorImageChanged(SpeciesColorChangedEvent event) {
		final XYSeriesCassisCollection series = model.getBottomLineImageDataset();

		for (int cpt = 0; cpt < series.getSeriesCount(); cpt++) {
			XYSeriesCassis fullSerie = series.getSeries(cpt);
			fullSerie.getConfigCurve().setColor(event.getNewColor());

			view.getSpectrumPlot().modifyBottom((String) fullSerie.getKey(), fullSerie.getConfigCurve());
			JPanelCurve jpc = view.getInfoPanel().getJPanelCurveByName(
					InfoPanelConstants.OTHER_SPECIES_TITLE);
			for (CurvePanelView cpv : jpc.getListCurvePanelView()) {
				if (InfoPanelConstants.IMAGE_TITLE.equals(cpv.getNameFromModel())
						&& cpv.getButtonColor() != null) {
					cpv.getButtonColor().setBackground(event.getNewColor());
				}
			}
		}
	}

	@Override
	public void speciesDisplayClicked(EventObject event) {
		if (model.getCenterDataset() == null ||
				model.getCenterDataset().getSeriesCount() == 0 ||
				model.getCenterSeriesVisibleCount() == 0) {
			JOptionPane.showMessageDialog(view,
					"As there is no visible spectrum, the other species can not be displayed.",
					"Unable to display Other Species",
					JOptionPane.WARNING_MESSAGE);
			return;
		}

		model.removeBottomDataset();

		Range range;
		if (speciesControl.getModel().isLimitVisibleData() && model.getCenterDataset().getSeriesCount() >= 1) {
			range = getPlotCurrentDomainRange();
		} else {
			range = XYPlotCassisUtil.getXRangeWithoutSpecies(model.getCenterDataset());
		}
		double fMin = range.getLowerBound();
		double fMax = range.getUpperBound();

		if (fMin == Double.POSITIVE_INFINITY || fMax == Double.NEGATIVE_INFINITY) {
			JOptionPane
					.showMessageDialog(view, "No Min or Max frequency defined", "Alert", JOptionPane.WARNING_MESSAGE);
		} else {
			double loFreq = model.getXAxisCassis().getLoFreq();
			List<LineDescription> speciesLines =
					speciesControl.computeOtherSpecies(model.getXAxisCassis().convertFromMhzFreq(fMin)
							, model.getXAxisCassis().convertFromMhzFreq(fMax), model.getXAxisCassis(), true, "");
			if (!Double.isNaN(loFreq)) {
				List<LineDescription> speciesLinesImage = speciesControl.computeOtherSpecies(
						fMax + 2 * (loFreq - fMax), fMin + 2 * (loFreq - fMin),
						false, "");
				if (speciesLines.size() + speciesLinesImage.size() >= 1) {
					displayOtherSpecies(speciesLines, speciesLinesImage);
				} else if (!Software.getUserConfiguration().isAutoOtherSpeciesDisplayed() ||
						   !speciesControl.getModel().getCassisTableMoleculeModel().getSelectedMolecules().isEmpty()) {
					JOptionPane.showMessageDialog(view,
							"No transition found. Check your thresholds and/or template if necessary.",
							"Other Species", JOptionPane.WARNING_MESSAGE);
				}
			} else {
				if (view.getOtherSpeciesPanel().getOtherSpeciesBox().isSelected() && !speciesLines.isEmpty()) {
					displayOtherSpecies(speciesLines, null);
				} else if (! speciesControl.getModel().
						getCassisTableMoleculeModel().getSelectedMolecules().isEmpty()){
					JOptionPane.showMessageDialog(view,
							"No transition found. Check your thresholds and/or template if necessary.",
							"Other Species", JOptionPane.WARNING_MESSAGE);
				}
			}
			displayDataset();
		}
	}

	/**
	 * Get currently displayed range value for Domain axis converted in MHz.
	 *
	 * @return currently displayed range value for Domain axis converted in MHz.
	 */
	private Range getPlotCurrentDomainRange() {
		Range r = view.getSpectrumPlot().getPlot().getDomainAxis().getRange();
		double xLower = model.getXAxisCassis().convertToMHzFreq(r.getLowerBound());
		double xUppper = model.getXAxisCassis().convertToMHzFreq(r.getUpperBound());
		return new Range(Math.min(xLower, xUppper), Math.max(xLower, xUppper));
	}

	@Override
	public void speciesSignalVisibleChanged(SpeciesEnableEvent event) {
		final XYSeriesCassisCollection series = model.getBottomLineSignalDataset();

		for (int cpt = 0; cpt < series.getSeriesCount(); cpt++) {
			XYSeriesCassis fullSerie = series.getSeries(cpt);
			fullSerie.getConfigCurve().setVisible(event.isEnabled());
			view.getSpectrumPlot().modifyBottom((String) fullSerie.getKey(), fullSerie.getConfigCurve());
		}
		if (view.getInfoPanel().getModel().isJPanelCurveExist(
				InfoPanelConstants.OTHER_SPECIES_TITLE)) {
			JPanelCurve pc = view.getInfoPanel().getModel().getPanelCurveByName(
					InfoPanelConstants.OTHER_SPECIES_TITLE);
			for (CurvePanelView cpv : pc.getListCurvePanelView()) {
				if (InfoPanelConstants.SIGNAL_TITLE.equals(cpv.getNameFromModel())
						&& cpv.getButtonColor() != null) {
					cpv.getCheckBox().setSelected(event.isEnabled());
				}
			}
		}
	}

	@Override
	public void speciesImageVisibleChanged(SpeciesEnableEvent event) {
		final XYSeriesCassisCollection series = model.getBottomLineImageDataset();

		for (int cpt = 0; cpt < series.getSeriesCount(); cpt++) {
			XYSeriesCassis fullSerie = series.getSeries(cpt);
			fullSerie.getConfigCurve().setVisible(event.isEnabled());
			view.getSpectrumPlot().modifyBottom((String) fullSerie.getKey(), fullSerie.getConfigCurve());
		}
		if (view.getInfoPanel().getModel().isJPanelCurveExist(
				InfoPanelConstants.OTHER_SPECIES_TITLE)) {
			JPanelCurve pc = view.getInfoPanel().getModel().getPanelCurveByName(
					InfoPanelConstants.OTHER_SPECIES_TITLE);
			for (CurvePanelView cpv : pc.getListCurvePanelView()) {
				if (InfoPanelConstants.IMAGE_TITLE.equals(cpv.getNameFromModel())
						&& cpv.getButtonColor() != null) {
					cpv.getCheckBox().setSelected(event.isEnabled());
				}
			}
		}
	}

	@Override
	public void setSeriesVisible(boolean visible, TypeCurve speciesType) {
		XYSeriesCassisCollection series = model.getCenterDataset();

		for (int cpt = 0; cpt < series.getSeriesCount(); cpt++) {
			XYSeriesCassis fullSerie = series.getSeries(cpt);
			if (TypeCurve.sameType(fullSerie.getTypeCurve(), speciesType)) {
				fullSerie.getConfigCurve().setVisible(visible);
				view.getSpectrumPlot().modifyCenter((String) fullSerie.getKey(), fullSerie.getConfigCurve());
			}
		}

		if (view.getInfoPanel().getModel().isJPanelCurveExist(
				InfoPanelConstants.FIT_CURVES_TITLE)) {
			JPanelCurve panelCurveByName = view.getInfoPanel().getModel().getPanelCurveByName(
					InfoPanelConstants.FIT_CURVES_TITLE);
			for (CurvePanelView cpv : panelCurveByName.getListCurvePanelView()) {
				XYSeriesCassis cassisModel = cpv.getModel().getCassisModel();
				if (TypeCurve.sameType(cassisModel.getTypeCurve(), speciesType)) {
					cpv.getCheckBox().setSelected(visible);
				}
			}
		}
	}

	@Override
	public XYSpectrumSeries getSeriesToFit() {
		return model.getSeriesToFit();
	}

	@Override
	public List<InterValMarkerCassis> getListMarker() {
		return view.getFitPanel().getMarkerManager().getListMarkerDisjoint();
	}

	@Override
	public void setLogParameters(File selectedFile, boolean append) {
		saveFit.setLogParameters(selectedFile, append);
	}

	@Override
	public void displaySelection(boolean selected) {
		view.getFitPanel().getMarkerManager().displaySelection(view.getSpectrumPlot().getPlot(), selected);
	}

	@Override
	public void setDataSeries(XYSpectrumSeries newDataSerie) {
		model.replaceDataSerie(newDataSerie);
		view.getFitPanel().changeResidualButton(true);
		this.model.removeFitAndResiudalCurve();
		displayDataset();
	}

	@Override
	public void saveAllLineAnalysisInCurrentFile() {
		if (saveFit.getLogFile() == null) {
			JOptionPane.showMessageDialog(view,
					"To save the fit, please click on Select file button to select your save file", "Warning",
					JOptionPane.WARNING_MESSAGE);
			return;
		}
		saveFit.saveAllLineAnalysisInCurrentFile(view.getFitPanel().getActiveLinkedListFittingItem());
	}

	@Override
	public void resetLastSelection() {
		view.getFitPanel().getMarkerManager().resetLastSelection(view.getSpectrumPlot().getPlot());
		model.getFitModelManager().resetLastStudiedRange();
	}

	@Override
	public void resetAllSelection() {
		view.getFitPanel().getMarkerManager().resetAllSelection(view.getSpectrumPlot().getPlot());
		model.getFitModelManager().resetStudyRanges();
	}


	@Override
	public void displayFitAndResidual(XYSpectrumSeries fitSerie, XYSpectrumSeries residualSerie,
			List<XYSpectrumSeries> fitCompoSerie) {
		fitSerie.getConfigCurve().setColor(this.model.getColorFitCurve());

		model.removeFitAndResiudalCurve();
		model.addFitAndResidualCurve(fitSerie, residualSerie);
		for (int cpt = 0; cpt < fitCompoSerie.size(); cpt++) {
			model.addFitCurve(fitCompoSerie.get(cpt));
		}

		view.getFitPanel().setSelectedFit(true);
		view.getFitPanel().setSelectedResidual(true);

		displayDataset();
	}

	public void displayOtherSpecies(List<LineDescription> speciesLinesSignal, List<LineDescription> speciesLinesImage) {
		model.removeBottomDataset();
		JPanelCurve jpc = new JPanelCurve(InfoPanelConstants.OTHER_SPECIES_TITLE);

		if (view.getOtherSpeciesPanel().getOtherSpeciesBox().isSelected()) {
			XYLineSeriesCassis otherSerieSignal = new XYLineSeriesCassis(
					InfoPanelConstants.SIGNAL_TITLE,
					TypeCurve.OTHER_SPECIES_SIGNAL, speciesLinesSignal,
					XYLineSeriesCassis.BOTTOM, model.getXAxisCassis());
			otherSerieSignal.getConfigCurve().setColor(model.getSpeciesModel().getColorSpeciesSignal());
			model.getBottomLineSignalDataset().addSeries(otherSerieSignal);
			XYPlotCassisUtil.setRenderSeriesParameters(view.getSpectrumPlot().getBottomLineSignalRenderer(),
					model.getBottomLineSignalDataset().indexOf(
							InfoPanelConstants.SIGNAL_TITLE), otherSerieSignal.getConfigCurve());

			CurvePanelView curvePanelViewSignal = new CurvePanelView(new CurvePanelModel(otherSerieSignal));
			curvePanelViewSignal.addCurveCassisListener(this);
			jpc.addCurvePane(curvePanelViewSignal);
		}

		if (speciesLinesImage != null &&
				view .getOtherSpeciesPanel().isSpeciesImageEnabled() &&
				view.getOtherSpeciesPanel().getOtherSpeciesImageBox().isSelected()) {
			XYLineSeriesCassis otherSerieImage = new XYLineSeriesCassis(
					InfoPanelConstants.IMAGE_TITLE,
					TypeCurve.OTHER_SPECIES_IMAGE, speciesLinesImage,
					XYLineSeriesCassis.BOTTOM, model.getXAxisCassis());
			otherSerieImage.getConfigCurve().setColor(model.getSpeciesModel().getColorSpeciesImage());
			model.getBottomLineImageDataset().addSeries(otherSerieImage);
			XYPlotCassisUtil.setRenderSeriesParameters(view.getSpectrumPlot().getBottomLineImageRenderer(),
					model.getBottomLineImageDataset().indexOf(
							InfoPanelConstants.IMAGE_TITLE), otherSerieImage.getConfigCurve());
			CurvePanelView curvePanelViewImage = new CurvePanelView(new CurvePanelModel(otherSerieImage));
			curvePanelViewImage.addCurveCassisListener(this);
			jpc.addCurvePane(curvePanelViewImage);
		}
		view.getInfoPanel().addJPanelCurve(jpc);
	}

	public void displayLineList(String file) {
		List<LineDescription> lines = null;
		try {
			lines = model.getOverlayLine(file);
		} catch (Exception e) {
			LOGGER.trace("Error while reading linelist file {} at first try.\n"
					+ " Probably another type of linelist file.", file, e);
			lines = SpectralLineListFileReader.getLines(file, model.getDataMinFreq(),
					model.getDataMaxFreq(), model.getVlsrData());
			if (lines == null) {
				lines = RotationalLineListFileReader.getLines(file,
						model.getDataMinFreq(), model.getDataMaxFreq(),
						model.getVlsrData());
			}
			if (lines == null) {
				JOptionPane.showMessageDialog(view, "Error while opening file.",
						null, JOptionPane.ERROR_MESSAGE);
				return;
			}
		}

		displayLineList(file, lines);
	}

	public void displayLineList(String file, List<LineDescription> lines) {
		if (lines == null || lines.isEmpty())
			return;

		String fileName = new File(file).getName();
		String seriesName;
		if (fileName == null || fileName.isEmpty()) {
			seriesName = getUniqueLineListSeriesName("LineList");
		} else {
			seriesName = getUniqueLineListSeriesName(fileName);
		}

		XYLineSeriesCassis lineSerie = new XYLineSeriesCassis(seriesName,
				TypeCurve.OVERLAY_LINELIST, lines, XYLineSeriesCassis.TOP,
				model.getXAxisCassis());
		lineSerie.getConfigCurve().setColor(ColorsCurve.getNewColorLineList());
		lineSerie.setToFit(false);
		model.getTopLineDataset().addSeries(lineSerie);
		displayDataset();
	}

	/**
	 * Create an unique series name for LineList using a base.
	 *
	 * @param base The base for the series name (usually the file name or "LineList").
	 * @return the unique name to use.
	 */
	private String getUniqueLineListSeriesName(String base) {
		String id = base;
		XYSeriesCassisCollection ds = model.getTopLineDataset();
		int i = 2;
		while (ds.containsSeries(id)) {
			id = base + '_' + i;
			i++;
		}
		return id;
	}

	public double getMaxFrequencyPlot() {
		return view.getSpectrumPlot().getMaxFrequencyPlot();
	}

	public Double getMinFrequencyPlot() {
		return view.getSpectrumPlot().getMinFrequencyPlot();
	}

	@Override
	public XAxisCassis getXAxisCassis() {
		return model.getXAxisCassis();
	}

	@Override
	public YAxisCassis getYAxisCassis() {
		return model.getYAxisCassis();
	}

	@Override
	public void dataChanged(ModelChangedEvent e) {
		if (FullSpectrumModel.FREQ_REF_EVENT.equals(e.getSource())) {
			onFreqRefChanged();
		} else if (FullSpectrumModel.ADD_RESULT_EVENT.equals(e.getSource())) {
			onNewResult(e);
		} else if (FullSpectrumModel.VLSR_DATA_EVENT.equals(e.getSource()) ||
				FullSpectrumModel.TYPE_FREQUENCY_EVENT.equals(e.getSource())) {
			onVlsrDataEvent();
		} else if (FullSpectrumModel.LOFREQ_EVENT.equals(e.getSource())) {
			onLoFreqChanged();
		} else if (FullSpectrumModel.CENTER_DATASET_CHANGE_EVENT.equals(e.getSource())) {
			onCenterDatasetChanged(e);
		} else if (FullSpectrumModel.CHANGE_RENDERING_EVENT.equals(e.getSource())) {
			onChangeRendering();
		} else if (e.getSource().equals(ModelFitManager.FIT_MODEL_ADDED_EVENT)) {
			onFitModelAdded(e);
		}if (e.getSource().equals(FitActionsInterface.FIT_PERFORMED_EVENT)) {
			onFitComputed((AdvancedFitResult) e.getValue());
		} else if (e.getSource().equals(FitActionsInterface.FIT_SUBSTRACTED_EVENT)) {
			onSubstractFit();
		} else if (e.getSource().equals(FitActionsInterface.FIT_FAILED_EVENT)) {
			onFitError(e);
		} else if (e.getSource().equals(FitActionsInterface.OVERLAY_FIT_EVENT)) {
			onOverlayFit(e);
		} else if (e.getSource().equals(FitActionsInterface.OVERLAY_RESIDUAL_EVENT)) {
			onOverlayResidual(e);
		} else if (e.getSource().equals(FitActionsInterface.ORIGINAL_RESTORED_EVENT)) {
			onOriginalRestored();
		} else if (e.getSource().equals(FitActionsInterface.LOG_FILE_SELECTED_EVENT)) {
			onInitLogParams();
		} else if (e.getSource().equals(FitActionsInterface.FIT_SAVED_EVENT)) {
			onSaveCurrentFit();
		}
	}

	private void onFreqRefChanged() {
		double value = model.getFreqRef();

		if (Double.isNaN(value)) {
			view.getFreqRefLabel().setText("Reference frequency: ");
		} else {
			DecimalFormatSymbols formatSymbol = new DecimalFormatSymbols();
			formatSymbol.setDecimalSeparator('.');
			DecimalFormat twoDecimalFormat = new DecimalFormat("0.00", formatSymbol);
			String refFreq = twoDecimalFormat.format(value);
			view.getFreqRefLabel().setText("Reference frequency: " + refFreq);
		}
	}

	private void onNewResult(ModelChangedEvent e) {
		displayDataset();
		XYSpectrumSeries serie = (XYSpectrumSeries) e.getValue();
		serie.rebuild();
	}

	private void onVlsrDataEvent() {
		speciesControl.initValue(model.getVlsrData(), model.getTypeFrequency());
	}

	private void onLoFreqChanged() {
		if (model.getCenterDataset().getSeriesCount() > 1) {
			removeOtherSpeciesImage();
		} else {
			checkOsState();
		}
	}

	private void onCenterDatasetChanged(ModelChangedEvent e) {
		int nbSeries = (Integer)e.getValue();
		if (nbSeries > 1) {
			removeOtherSpeciesImage();
		} else {
			checkOsState();
		}
		if (nbSeries >= 1) {
			AdvancedFitFrame.getFrame().displayModel(model.getCurrentFitModel());
			view.getCreationPanel().enableAll(nbSeries >= 1);
		}
	}

	private void onChangeRendering() {
		view.updateRendering();
	}

	private void onFitModelAdded(ModelChangedEvent e) {
		AdvancedFitModel fitModel = (AdvancedFitModel) e.getValue();
		if (Software.getUserConfiguration().isFitAutoEstimate()) {
			List<LineDescription> lines = new ArrayList<>(model.getSyntheticLineVisible());
			lines.addAll(model.getOtherSpeciesLineVisible());
			if (!lines.isEmpty()) {
				FitType defComp = Software.getUserConfiguration().getFitDefaultType();
				fitModel.estimateListOfLines(lines, defComp);
			}
		}
	}

	private void onFitComputed(AdvancedFitResult result) {
		view.getCreationPanel().getActions().getSubstractFit().setEnabled(true);
		view.getCreationPanel().getActions().getOverlayFits().setEnabled(true);
		view.getCreationPanel().getActions().getOverlayResidual().setEnabled(true);
		view.getCreationPanel().getActions().getOverlayFits().setSelected(true);
		view.getCreationPanel().getActions().getOverlayResidual().setSelected(true);
		displayFitAndResidual(result.getFitSeries(), result.getResidualSeries(),
				result.getCompoSeries());
	}

	private void onSubstractFit() {
		XYSpectrumSeries source = model.getCurrentDataCurve();
		AdvancedFitResult lastResult = model.getFitModelManager().getLastResult();
		FitSubstractCommand cmd = new FitSubstractCommand(model.getCurrentFitModel().getParametersModel(),
				this, source, lastResult.getResidualSeries());
		view.getCreationPanel().getActions().getOverlayFits().setSelected(false);
		view.getCreationPanel().getActions().getOverlayResidual().setSelected(false);
		model.getFitModelManager().addCommand(cmd);
		model.removeFitAndResiudalCurve();
	}

	private void onFitError(ModelChangedEvent e) {
		FitException exc = (FitException) e.getValue();
		String text = "<html>Error during Fit.<br> " + exc.getMessageError() + "<br>" + exc.getProbableSolution() + "</html>";
		displayError(exc.getNameError(), text);
	}

	private void onOverlayFit(ModelChangedEvent e) {
		setSeriesVisible((boolean) e.getValue(), TypeCurve.FIT);
		setSeriesVisible((boolean) e.getValue(), TypeCurve.FIT_COMPO);
		view.getCreationPanel().getActions().getOverlayFits().setSelected((boolean) e.getValue());
	}

	private void onOverlayResidual(ModelChangedEvent e) {
		setSeriesVisible((boolean) e.getValue(), TypeCurve.FIT_RESIDUAL);
		view.getCreationPanel().getActions().getOverlayResidual().setSelected((boolean) e.getValue());
	}

	private void onOriginalRestored() {
		model.getCurrentFitModel().getParametersModel().restoreOriginal();
		model.removeFitAndResiudalCurve();
	}

	private void onInitLogParams() {
		model.getFitModelManager().initLogParameters(view, model);
	}

	private void onSaveCurrentFit() {
		if (!model.getFitModelManager().isSavingInitialized()) {
			model.getFitModelManager().initLogParameters(view, model);
		}
		try {
			model.getFitModelManager().saveCurrentFitModel(model.getTelescope());
		} catch (IOException | IllegalArgumentException e) {
			displayError("Error", "<html>Impossible to save the fit: <br>" + e.getMessage() + "</html>");
		}
	}

	private void displayError(String title, String text) {
		JOptionPane.showMessageDialog(view, text, title, JOptionPane.ERROR_MESSAGE);
	}

	public VelocityAxisDialog getDialog() {
		return dialog;
	}

	public void yAxisLeftChanged() {
		if (view.getYAxisLeftComboBox().getSelectedItem() == null) {
			return;
		}

		YAxisCassis previousAxis = model.getyAxis();
		YAxisCassis selectedAxis = (YAxisCassis) view.getYAxisLeftComboBox().getSelectedItem();

		model.setYAxisCassis(selectedAxis);
		boolean res = view.getSpectrumPlot().updateYAxisParameters(previousAxis, selectedAxis);
		if (res) {
			view.getSpectrumPlot().changeYAxis(model.getYAxisCassis());
		}
//		view.getSpectrumPlot().setBottomTitle(xAxisCassis.getTitleLabel());
//		view.getFitPanel().setXAxis(xAxisCassis);
//		comboTopAxisChanged(false);
//		view.getToolsView().getControl().refreshInfo();

//		model.getFitModelManager().refreshInputs();
//		model.getFitModelManager().convertConfiguration(previousAxis);

		view.getSpectrumPlot().setLeftTitle(model.getYAxisCassis().toString());
	}

	@Override
	public int getNumCurrentSpectrum() {
		return 0;
	}

	private void checkOsState() {
		if (model.getCenterDataset().getSeriesCount() == 1 &&
				!Double.isNaN(((XYSpectrumSeries)model.getCenterDataset().getSeries(0)).getSpectrum().getLoFreq())) {
			view.getOtherSpeciesPanel().setShowSpeciesImageEnabled(true);
		}
	}

	private void removeOtherSpeciesImage() {
		view.getOtherSpeciesPanel().setShowSpeciesImageEnabled(false);
		model.removeOtherSpeciesImage();
		JPanelCurve infopanelOs = view.getInfoPanel().getJPanelCurveByName(
				InfoPanelConstants.OTHER_SPECIES_TITLE);
		if (infopanelOs != null) {
			CurvePanelView cpv = infopanelOs.removeCurvePanelView(
					InfoPanelConstants.IMAGE_TITLE);
			if (cpv != null) {
				cpv.removeCurveCassisListener(this);
			}
			if (infopanelOs.getListCurvePanelView().isEmpty()) {
				view.getInfoPanel().removePanelCurve(infopanelOs);
			}
		}
	}

	@Override
	public boolean isOnGallery() {
		return false;
	}

	/**
	 * Remove a JPanelCurve if it exist in the InfoPanel.
	 *
	 * @param title The title of the JPanelCurve to remove.
	 */
	public void removeJPanelCurve(String title) {
		JPanelCurve jpc = view.getInfoPanel().getJPanelCurveByName(title);
		if (jpc != null) {
			jpc.setAutoDestroy(true);
			deleteButtonInfoPanelClicked(jpc);
		}
	}

	@Override
	public void addIntervalMarker(InterValMarkerCassis marker) {
		XYPlot plot = view.getSpectrumPlot().getPlot();
		view.getFitPanel().getMarkerManager().addMarker(plot,
				marker.getStartValue(), marker.getEndValue());
		forwardMarkerToFitModule(marker);
		addMarkersPanelCurve();
	}

	/**
	 * Forwards the given marker to the fit module of the current model
	 *
	 * @param marker Marker to forward
	 */
	private void forwardMarkerToFitModule(InterValMarkerCassis marker) {
		FitParametersModel paramModel = model.getCurrentFitModel().getParametersModel();
		double start = marker.getStartValue();
		double end = marker.getEndValue();
		FitEstimator estimator = new FitEstimator(start, end, paramModel.getSourceCurve());
		FitAbstractComponent estim = paramModel.getManager().getEstimableComponent();
		if (estim == null || (estim != null && estim.isEstimated())) {
			paramModel.studyRange(estimator);
		} else {
			paramModel.addRange(estimator.getRangeAsCurve());
		}
	}

	/**
	 * Change the YAxis type (log or not).
	 *
	 * @param log true to set a logarithmic axis, false otherwise.
	 */
	public void changeYAxisType(boolean log) {
		if (log) {
			view.getSpectrumPlot().setYAxisToLog();
		} else {
			view.getSpectrumPlot().setYAxisNormal();
		}
		view.updatePlotLabels();
	}

	/**
	 * Change the XAxis type (log or not).
	 *
	 * @param log true to set a logarithmic axis, false otherwise.
	 */
	public void changeXAxisType(boolean log) {
		if (log) {
			view.getSpectrumPlot().setXAxisToLog();
		} else {
			view.getSpectrumPlot().setXAxisNormal();
		}
		view.updatePlotLabels();
	}

	/**
	 * Set a histogram rendering.
	 *
	 * @see eu.omp.irap.cassis.gui.plot.simple.ChangeRenderingInterface#setHistogramRendering()
	 */
	@Override
	public void setHistogramRendering() {
		model.setRendering(Rendering.HISTOGRAM);
	}

	/**
	 * Set a dot rendering.
	 *
	 * @see eu.omp.irap.cassis.gui.plot.simple.ChangeRenderingInterface#setDotRendering()
	 */
	@Override
	public void setDotRendering() {
		model.setRendering(Rendering.DOT);
	}

	/**
	 * Set a line rendering.
	 *
	 * @see eu.omp.irap.cassis.gui.plot.simple.ChangeRenderingInterface#setLineRendering()
	 */
	@Override
	public void setLineRendering() {
		model.setRendering(Rendering.LINE);
	}

	@Override
	public void estimateLinesForCurrentModel(List<LineDescription> lines) {
		FitType defComp = Software.getUserConfiguration().getFitDefaultType();
		model.getCurrentFitModel().estimateListOfLines(lines, defComp);
	}

	@Override
	public boolean isFittable() {
		return view.getTabManager().getSelectedIndex() == 5;
	}

	@Override
	public void removeEstimationRange(double x) {
		model.getCurrentFitModel().getParametersModel().removeRanges(x);
	}

	@Override
	public FitSourceInterface getFitSource() {
		return model;
	}

	/**
	 * Add a "Markers" JPanelCurve if there is none yet.
	 */
	public void addMarkersPanelCurve() {
		if (view.getInfoPanel().getModel().isJPanelCurveExist(
				InfoPanelConstants.MARKERS_TITLE)) {
			return;
		}
		JPanelCurve markersCurve = new JPanelCurve(
				InfoPanelConstants.MARKERS_TITLE, true);
		XYSeriesCassis serie = new XYSpectrumSeries(InfoPanelConstants.MARKERS_TITLE,
				XAxisCassis.getXAxisUnknown(), TypeCurve.MARKERS, new CommentedSpectrum());
		CurvePanelView cpv = new CurvePanelView(new CurvePanelModel(serie), false, false, false);
		cpv.getCheckBox().setSelected(true);
		cpv.getModel().getCassisModel().getConfigCurve().setColor(MarkerManager.COLOR_SELECTION);
		cpv.getButtonColor().setBackground(MarkerManager.COLOR_SELECTION);
		cpv.getControl().addCurveCassisListener(this);
		cpv.getButtonColor().setEnabled(false);
		markersCurve.addCurvePane(cpv);
		view.getInfoPanel().addJPanelCurve(markersCurve);
		view.getInfoPanel().revalidate();
		view.getInfoPanel().repaint();
	}

	/**
	 * Remove the "Markers" panel curve if there is one.
	 */
	public void removeMarkersPanelCurve() {
		JPanelCurve markersPc = view.getInfoPanel().getJPanelCurveByName(
				InfoPanelConstants.MARKERS_TITLE);
		if (markersPc != null) {
			view.getInfoPanel().removePanelCurve(markersPc);
			view.getInfoPanel().revalidate();
			view.getInfoPanel().repaint();
		}
	}

	/**
	 * Handle the removal of a marker.
	 * This remove the "Markers" panel curve if there is no markers left.
	 *
	 * @see eu.omp.irap.cassis.gui.plot.util.SpectrumFitPanelInterface#intervalMarkerRemoved()
	 */
	@Override
	public void intervalMarkerRemoved() {
		if (!view.getFitPanel().getMarkerManager().haveMarker()) {
			removeMarkersPanelCurve();
		}
	}

}
