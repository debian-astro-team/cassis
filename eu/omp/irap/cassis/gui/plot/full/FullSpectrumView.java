/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.full;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.io.File;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.XAxisCassisUtil;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.file.EXTENSION;
import eu.omp.irap.cassis.gui.fit.FitPanel;
import eu.omp.irap.cassis.gui.fit.FitStyleEnum;
import eu.omp.irap.cassis.gui.fit.advanced.gui.AdvancedFitCreationPanel;
import eu.omp.irap.cassis.gui.fit.advanced.gui.AdvancedFitFrame;
import eu.omp.irap.cassis.gui.otherspecies.SpeciesTab;
import eu.omp.irap.cassis.gui.plot.abstractplot.AbstractPlotView;
import eu.omp.irap.cassis.gui.plot.curve.CurvePanelView;
import eu.omp.irap.cassis.gui.plot.gallery.GallerySortPane;
import eu.omp.irap.cassis.gui.plot.infopanel.InfoPanel;
import eu.omp.irap.cassis.gui.plot.infopanel.InfoPanelConstants;
import eu.omp.irap.cassis.gui.plot.line.CassisPlot;
import eu.omp.irap.cassis.gui.plot.popup.MessageControl;
import eu.omp.irap.cassis.gui.plot.save.CassisSaveViewInterface;
import eu.omp.irap.cassis.gui.plot.save.SaveFullSpectrum;
import eu.omp.irap.cassis.gui.plot.simple.SpectrumPlot;
import eu.omp.irap.cassis.gui.plot.simple.util.ChartPanelCassis;
import eu.omp.irap.cassis.gui.plot.tools.ToolsView;
import eu.omp.irap.cassis.gui.plot.util.ChartMovePanel;
import eu.omp.irap.cassis.gui.plot.util.PrintUtil;
import eu.omp.irap.cassis.gui.plot.util.StackMosaicPanel;
import eu.omp.irap.cassis.gui.plot.util.ToolsState;
import eu.omp.irap.cassis.gui.util.MyLayeredPane;

public class FullSpectrumView extends AbstractPlotView {
	private static final long serialVersionUID = 1923560975477705127L;
	private InfoPanel infoPanel;

	private FullSpectrumModel model;
	private FullSpectrumControl control;

	private SpeciesTab otherSpeciesPanel;

	private JPanel overlayPanel;
	private JButton dataFileButton;
	private ToolsState toolsState = ToolsState.X;
	private JTabbedPane tabManager;

	private FitPanel fitPanel;

	private StackMosaicPanel stackMosaicPanel;
	private JPanel westPanel;
	private JTabbedPane toolsChartTabbedPanel;
	private SaveFullSpectrum save;

	private JButton lineListButton;
	private MyLayeredPane layeredPane = new MyLayeredPane();
	private MessageControl messageControl = new MessageControl(layeredPane);
	private SpectrumPlot spectrumPlot;

	private JPanel axisYToolsPanel;
	private JPanel axisXToolsPanel;
	private ChartMovePanel chartYMovePanel;
	private JPanel yAxisPanel;
	private JPanel yAxisLeftPanel;
	private JComboBox<YAxisCassis> yAxisLeftComboBox;
	private JPanel yAxisRightPanel;
	private JComboBox<YAxisCassis> comboRightAxis;
	private JPanel xAxisPanel;
	private ChartMovePanel chartXMovePanel;
	private JPanel xAxisBottomPanel;
	private JComboBox<String> comboBottomAxisType;
	private JComboBox<XAxisCassis> comboBottomtAxis;
	private JPanel xAxisTopPanel;
	private JComboBox<String> comboTopAxisType;
	private JComboBox<XAxisCassis> comboTopAxis;
	private JScrollPane scrollPane;
	private JPanel infoPanelTab;
	private JLabel freqRef;
	private JPanel toolbar;
	private ToolsView toolsView;
	private JCheckBox yLogCheckBox;
	private JCheckBox xLogCheckBox;
	private AdvancedFitCreationPanel creationPanel;


	public FullSpectrumView() {
		this(new FullSpectrumControl(new FullSpectrumModel()));
	}

	/**
	 * Constructor.
	 *
	 * @param control The controller.
	 */
	public FullSpectrumView(FullSpectrumControl control) {
		super(new BorderLayout());
		setControl(control);
		model = control.getModel();

		add(getTabManager(), BorderLayout.EAST);
		add(getWestPanel(), BorderLayout.CENTER);
	}

	private void setControl(FullSpectrumControl control) {
		this.control = control;
		control.setView(this);
	}

	/**
	 * @return the otherSpeciesPanel
	 */
	public SpeciesTab getOtherSpeciesPanel() {
		if (otherSpeciesPanel == null) {
			this.otherSpeciesPanel = control.getSpeciesControl().getView();
			otherSpeciesPanel.setShowSpeciesImageEnabled(true);
			otherSpeciesPanel.getControl().addSpeciesListener(control);
		}
		return otherSpeciesPanel;
	}

	/**
	 * @return the dataFileButton
	 */
	public JButton getDataFileButton() {
		if (dataFileButton == null) {
			dataFileButton = new JButton("Select");
			dataFileButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.onOverlayPanelDataFileButtonClicked();
				}
			});
		}
		return dataFileButton;
	}

	public FullSpectrumControl getControl() {
		return control;
	}

	public JTabbedPane getTabManager() {
		if (tabManager == null) {
			tabManager = new JTabbedPane();
			tabManager.setPreferredSize(new Dimension(353, 0));
			tabManager.addChangeListener(new ChangeListener() {
				@Override
				public void stateChanged(ChangeEvent e) {
					if (tabManager.getSelectedIndex() == 3) {
						if (control.getFitPanelControl().getCurrentFitPanel() != null &&
								getInfoPanel().getModel().isJPanelCurveExist(InfoPanelConstants.FIT_CURVES_TITLE)) {
							CurvePanelView aCurvePanelView = null;
							for (CurvePanelView cpv : getInfoPanel().getModel().getPanelCurveByName(
									InfoPanelConstants.FIT_CURVES_TITLE).getListCurvePanelView()) {
								if ("Fit residual".equals(cpv.getNameFromModel())) {
									aCurvePanelView = cpv;
									break;
								}
							}
							if (aCurvePanelView != null)
								getFitPanel().setSelectedResidual(aCurvePanelView.getModel().getCassisModel().getConfigCurve().isVisible());
						}
					} else if (tabManager.getSelectedIndex() == 4) {
						getToolsView().refresh();
					} else if (tabManager.getSelectedIndex() == 5) {
						AdvancedFitFrame.getFrame().openFrame(model.getAllSeries().size() >= 1);
					}
				}
			});
			tabManager.addTab("InfoPanel", getInfoPanelTab());
			tabManager.addTab("Overlays", getOverlayPanel());
			tabManager.addTab("Species", getOtherSpeciesPanel());

			fitPanel = control.getFitPanelControl().getCurrentFitPanel();
			fitPanel.getModel().setFitStyle(FitStyleEnum.LEVENBERG);
			tabManager.addTab("Fit", fitPanel);
			tabManager.addTab("Tools", getToolsView());
			tabManager.addTab("Advanced Fit", getCreationPanel());
		}
		return tabManager;
	}

	public JPanel getInfoPanelTab() {
		if (infoPanelTab == null) {
			infoPanelTab = new JPanel();
			infoPanelTab.setLayout(new BorderLayout());
			infoPanelTab.setBorder(BorderFactory.createTitledBorder(
					BorderFactory.createEtchedBorder(), "Plot Info"));
			infoPanelTab.add(getInfoPanelScrollPane(), BorderLayout.CENTER);

			JButton removeAllButton = new JButton("Remove All");
			removeAllButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.removeAllButtonClicked();
				}
			});
			JPanel temp = new JPanel();
			temp.setBorder(BorderFactory.createLineBorder(Color.GRAY));
			temp.add(removeAllButton);
			infoPanelTab.add(temp, BorderLayout.SOUTH);
		}
		return infoPanelTab;
	}

	public JScrollPane getInfoPanelScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane(getInfoPanel(),
					ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
					ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			scrollPane.setPreferredSize(new Dimension(200, 400));
			scrollPane.getVerticalScrollBar().setUnitIncrement(10);
		}
		return scrollPane;
	}

	/**
	 * @return The panel used to compute fit models
	 */
	public AdvancedFitCreationPanel getCreationPanel() {
		if (creationPanel == null) {
			creationPanel = new AdvancedFitCreationPanel(model.getFitModelManager());
			creationPanel.getActions().getFitAll().setEnabled(false);
		}
		return creationPanel;
	}

	/**
	 * Build the west panel composed of the chart, north parameters and south
	 * buttons.
	 *
	 * @param movePanel
	 * @param topControlPanel
	 * @return JPanel
	 */
	private JPanel getWestPanel() {
		if (westPanel == null) {
			westPanel = new JPanel(new BorderLayout());
			westPanel.setMinimumSize(new Dimension(820, 590));
			westPanel.setPreferredSize(new Dimension(820, 590));
			westPanel.setMaximumSize(new Dimension(Short.MAX_VALUE,
					Short.MAX_VALUE));

			JPanel tempPanel = new JPanel();
			tempPanel.setLayout(new BorderLayout());
			tempPanel.add(getToolbar(), BorderLayout.NORTH);
			tempPanel.add(getToolsChartTabbedPanel(), BorderLayout.SOUTH);
			westPanel.add(tempPanel, BorderLayout.SOUTH);
			getSpectrumPlot().setLocation(layeredPane.getLocation());
			layeredPane.add(getSpectrumPlot(), -1);

			layeredPane.addComponentListener(new ComponentAdapter() {
				@Override
				public void componentResized(ComponentEvent e) {
					super.componentResized(e);
					getSpectrumPlot().setBounds(layeredPane.getBounds());
					FullSpectrumView.this.revalidate();
				}
			});

			westPanel.addComponentListener(new ComponentAdapter() {
				@Override
				public void componentResized(ComponentEvent e) {
					getSpectrumPlot().validate();
					layeredPane.validate();
				}
			});

			westPanel.add(layeredPane, BorderLayout.CENTER);
		}

		return westPanel;
	}

	/**
	 * Create if needed then return the SpectrumPlot.
	 *
	 * @return the SpectrumPlot.
	 * @see eu.omp.irap.cassis.gui.plot.save.SavePlotInterface#getSpectrumPlot()
	 */
	@Override
	public SpectrumPlot getSpectrumPlot() {
		if (spectrumPlot == null) {
			spectrumPlot = new SpectrumPlot();

			spectrumPlot.setBottomTitle(model.getXAxisCassis().getTitleLabel());
			spectrumPlot.setLeftTitle(model.getYAxisCassis().toString());
			spectrumPlot.setXAxisCassisTop(model.getXAxisCassis());

			((ChartPanelCassis) spectrumPlot.getChartPanel()).setLineEstimable(control);

			model.setCenterDataset(spectrumPlot.getCenterXYSeriesCollection());
			model.setTopLineDataset(spectrumPlot.getTopLineSeriesCollection());
			model.setTopLineErrorDataset(spectrumPlot.getTopLineErrorSeriesCollection());
			model.setBottomLineSignalDataset(spectrumPlot.getBottomLineSignalSeriesCollection());
			model.setBottomLineImageDataset(spectrumPlot.getBottomLineImageSeriesCollection());

			spectrumPlot.setChartMouseListener(model,
					fitPanel.getMarkerManager(), getMessageControl(), control, control);
			spectrumPlot.setExternalChangeRenderingInterface(control);
			updateRendering();
		}
		return spectrumPlot;
	}

	/**
	 * Create if necessary the {@link InfoPanel} then return it.
	 *
	 * @return the {@link InfoPanel}.
	 */
	public InfoPanel getInfoPanel() {
		if (infoPanel == null) {
			infoPanel = new InfoPanel();
			infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.Y_AXIS));
			infoPanel.setBorder(BorderFactory.createEmptyBorder());
		}
		return infoPanel;
	}

	/**
	 * Build the north overlay panel.
	 *
	 * @return the north overlay panel.
	 */
	private JPanel getOverlayPanel() {
		if (overlayPanel == null) {
			JLabel dataFile = new JLabel("Data file");
			JLabel lineListDisplay = new JLabel("LineList");

			overlayPanel = new JPanel();
			overlayPanel.setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createTitledBorder(
							BorderFactory.createEtchedBorder(), "Overlays"),
					BorderFactory.createEmptyBorder(2, 2, 2, 2)));

			overlayPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
			JPanel temp = new JPanel(new GridLayout(2, 2, 2, 2));
			temp.add(dataFile);
			temp.add(getDataFileButton());
			temp.add(lineListDisplay);
			temp.add(getLineListButton());
			overlayPanel.add(temp);
		}
		return overlayPanel;
	}

	/**
	 * @return the lineListButton
	 */
	public JButton getLineListButton() {
		if (lineListButton == null) {
			lineListButton = new JButton("Select");
			lineListButton.setEnabled(true);
			lineListButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.onLineListButtonClicked();
				}
			});
		}
		return lineListButton;
	}

	/**
	 * Build the south panel composed of the move button, the zoom, the search
	 * and the range parameter.
	 *
	 * @return JPanel
	 */
	public JTabbedPane getToolsChartTabbedPanel() {
		if (toolsChartTabbedPanel == null) {
			toolsChartTabbedPanel = new JTabbedPane();
			toolsChartTabbedPanel.addTab("X Tools", getAxisXToolsPanel());
			toolsChartTabbedPanel.addTab("Y Tools", getAxisYToolsPanel());
			toolsChartTabbedPanel.addTab("Stack/Mosaic", getStackMosaicPanel());

			toolsChartTabbedPanel.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					changeState(toolsChartTabbedPanel.getSelectedComponent().getName());
				}
			});
		}
		return toolsChartTabbedPanel;
	}

	private JPanel getAxisYToolsPanel() {
		if (axisYToolsPanel == null) {
			axisYToolsPanel = new JPanel(new BorderLayout());
			axisYToolsPanel.setName(ToolsState.Y.name());
			axisYToolsPanel.add(getChartYMovePanel(), BorderLayout.CENTER);
			axisYToolsPanel.add(getYAxisPanel(), BorderLayout.EAST);
		}
		return axisYToolsPanel;
	}

	private JPanel getYAxisPanel() {
		if (yAxisPanel == null) {
			yAxisPanel = new JPanel(new FlowLayout());
			JPanel tmpPanel = new JPanel(new GridLayout(2, 1));
			tmpPanel.add(getYAxisLeftPanel(
					"<HTML><p>Y<sub>lef</sub></p></HTML>"));
			tmpPanel.add(getYAxisRightPanel(
					"<HTML><p>Y<sub>rig</sub></p></HTML>"));
			yAxisPanel.add(tmpPanel);
			yAxisPanel.add(getYLogCheckBox());
		}
		return yAxisPanel;
	}

	private JPanel getYAxisRightPanel(String label) {
		if (yAxisRightPanel == null) {
			yAxisRightPanel = new JPanel(new BorderLayout());
			yAxisRightPanel.add(buildYAxisComboPanel(label, getYAxisRightComboBox()),
					BorderLayout.CENTER);
		}
		return yAxisRightPanel;
	}

	public JComboBox<YAxisCassis> getYAxisRightComboBox() {
		if (comboRightAxis == null) {
			YAxisCassis[] yAxisCassis = new YAxisCassis[] {YAxisCassis.getYAxisKelvin(), YAxisCassis.getYAxisJansky()};
			comboRightAxis = new JComboBox<>(yAxisCassis);
			comboRightAxis.setEnabled(false);
		}
		return comboRightAxis;
	}

	private JPanel getYAxisLeftPanel(String label) {
		if (yAxisLeftPanel == null) {
			yAxisLeftPanel = new JPanel(new BorderLayout());
			yAxisLeftPanel.add(buildYAxisComboPanel(label, getYAxisLeftComboBox()),
					BorderLayout.CENTER);
		}
		return yAxisLeftPanel;
	}

	public JComboBox<YAxisCassis> getYAxisLeftComboBox() {
		if (yAxisLeftComboBox == null) {
			YAxisCassis[] yAxisCassis = YAxisCassis.getAllYAxisCassis();
			yAxisLeftComboBox = new JComboBox<>(yAxisCassis);
			yAxisLeftComboBox.setSelectedItem(model.getYAxisCassis());
			yAxisLeftComboBox.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					control.yAxisLeftChanged();
				}
			});
		}
		return yAxisLeftComboBox;
	}

	public JCheckBox getXLogCheckBox() {
		if (xLogCheckBox == null) {
			xLogCheckBox = new JCheckBox("Log");
			xLogCheckBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					boolean log = xLogCheckBox.isSelected();
					control.changeXAxisType(log);
				}
			});
		}
		return xLogCheckBox;
	}

	public JCheckBox getYLogCheckBox() {
		if (yLogCheckBox == null) {
			yLogCheckBox = new JCheckBox("Log");
			yLogCheckBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					boolean log = yLogCheckBox.isSelected();
					control.changeYAxisType(log);
				}
			});
		}
		return yLogCheckBox;
	}

	private JPanel buildYAxisComboPanel(String label, JComboBox<YAxisCassis> comboBox) {
		JLabel topLabel = new JLabel(label);
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		topPanel.add(topLabel);
		topPanel.add(comboBox);

		comboBox.setSize(180, 24);
		comboBox.setPreferredSize(new Dimension(180, 24));

		return topPanel;
	}

	private JPanel buildXAxisComboPanel(String label, JComboBox<XAxisCassis> comboBox, JComboBox<String> comboBoxType) {
		JLabel topLabel = new JLabel(label);
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		topPanel.add(topLabel);
		topPanel.add(comboBoxType);
		topPanel.add(comboBox);

		comboBox.setSize(100, 24);
		comboBox.setPreferredSize(new Dimension(100, 24));

		return topPanel;
	}

	private ChartMovePanel getChartYMovePanel() {
		if (chartYMovePanel == null) {
			chartYMovePanel = new ChartMovePanel(getSpectrumPlot().getChartPanel(),
					getSpectrumPlot().getPlot().getRangeAxis(), ToolsState.Y);
		}
		return chartYMovePanel;
	}

	private JPanel getAxisXToolsPanel() {
		if (axisXToolsPanel == null) {
			axisXToolsPanel = new JPanel(new BorderLayout());
			axisXToolsPanel.setName(ToolsState.X.name());
			axisXToolsPanel.add(getChartXMovePanel(), BorderLayout.CENTER);
			axisXToolsPanel.add(getXAxisPanel(), BorderLayout.EAST);
		}
		return axisXToolsPanel;
	}

	private JPanel getXAxisPanel() {
		if (xAxisPanel == null) {
			xAxisPanel = new JPanel(new FlowLayout());
			JPanel tmpPanel = new JPanel(new GridLayout(2, 1));
			tmpPanel.add(getXAxisTopPanel());
			tmpPanel.add(getXAxisBottomPanel());
			xAxisPanel.add(tmpPanel);
			xAxisPanel.add(getXLogCheckBox());
		}
		return xAxisPanel;
	}

	private JPanel getXAxisBottomPanel() {
		if (xAxisBottomPanel == null) {
			xAxisBottomPanel = new JPanel(new BorderLayout());
			xAxisBottomPanel.add(
					buildXAxisComboPanel("<HTML><p>X<sub>bot</sub></p></HTML>",
							getComboBottomtAxis(), getComboBottomAxisType()), BorderLayout.CENTER);
		}
		return xAxisBottomPanel;
	}

	public JComboBox<String> getComboBottomAxisType() {
		if (comboBottomAxisType == null) {
			comboBottomAxisType = new JComboBox<>(XAxisCassis.getAllXAxisCassisType());
			comboBottomAxisType.setSelectedItem(XAxisCassisUtil.getType(model.getXAxisCassisTop()));
			comboBottomAxisType.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					XAxisCassisUtil.changeXAxisAccordingToType(comboBottomAxisType.getSelectedItem().toString(), getComboBottomtAxis());
				}
			});
		}
		return comboBottomAxisType;
	}

	public JComboBox<XAxisCassis> getComboBottomtAxis() {
		if (comboBottomtAxis == null) {
			comboBottomtAxis = new JComboBox<>(XAxisCassis.getAllXAxisForType(getComboBottomAxisType().getSelectedItem().toString()));
			comboBottomtAxis.setSelectedItem(model.getXAxisCassis());
			comboBottomtAxis.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.comboBottomAxisChanged();
					XAxisCassisUtil.changeType(comboBottomtAxis, getComboBottomAxisType());
				}
			});
		}
		return comboBottomtAxis;
	}

	private JPanel getXAxisTopPanel() {
		if (xAxisTopPanel == null) {
			xAxisTopPanel = new JPanel(new BorderLayout());
			xAxisTopPanel.add(
					buildXAxisComboPanel("<HTML><p>X<sub>top</sub></p></HTML>",
							getComboTopAxis(), getComboTopAxisType()), BorderLayout.CENTER);
		}
		return xAxisTopPanel;
	}

	public JComboBox<String> getComboTopAxisType() {
		if (comboTopAxisType == null) {
			comboTopAxisType = new JComboBox<>(XAxisCassis.getAllXAxisCassisType());
			comboTopAxisType.setSelectedItem(XAxisCassisUtil.getType(model.getXAxisCassisTop()));
			comboTopAxisType.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					XAxisCassisUtil.changeXAxisAccordingToType(comboTopAxisType.getSelectedItem().toString(), getComboTopAxis());
				}
			});
		}
		return comboTopAxisType;
	}

	public JComboBox<XAxisCassis> getComboTopAxis() {
		if (comboTopAxis == null) {
			comboTopAxis = new JComboBox<>(XAxisCassis.getAllXAxisForType(getComboTopAxisType().getSelectedItem().toString()));
			comboTopAxis.setSelectedItem(model.getXAxisCassisTop());
			comboTopAxis.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.comboTopAxisChanged();
					XAxisCassisUtil.changeType(comboTopAxis, getComboTopAxisType());
				}
			});
		}
		return comboTopAxis;
	}

	private ChartMovePanel getChartXMovePanel() {
		if (chartXMovePanel == null) {
			chartXMovePanel = new ChartMovePanel(getSpectrumPlot().getChartPanel(),
					getSpectrumPlot().getPlot().getDomainAxis(), ToolsState.X);
		}
		return chartXMovePanel;
	}

	public StackMosaicPanel getStackMosaicPanel() {
		if (stackMosaicPanel == null) {
			stackMosaicPanel = new StackMosaicPanel(model.getStackMosaicModel());
			stackMosaicPanel.setName(ToolsState.STACK.name());
		}
		return stackMosaicPanel;
	}

	public FullSpectrumModel getModel() {
		return model;
	}

	@Override
	public CassisSaveViewInterface getSave() {
		if (save == null)
			save = new SaveFullSpectrum(this);
		return save;
	}

	public void changeState(String state) {
		if (state.equals(ToolsState.X.name())) {
			this.toolsState = ToolsState.X;
			double range = chartXMovePanel.getUpperBoundDomain()
					- chartXMovePanel.getLowerBoundDomain();
			double search = chartXMovePanel.getLowerBoundDomain() + range;

			chartXMovePanel.setRange(ChartMovePanel.arrondi(range, 2));
			chartXMovePanel.setSearch(ChartMovePanel.arrondi(search, 2));
		} else if (state.equals(ToolsState.Y.name())) {
			this.toolsState = ToolsState.Y;
			double range = chartYMovePanel.getUpperBoundRange()
					- chartYMovePanel.getLowerBoundRange();
			double search = chartYMovePanel.getLowerBoundDomain() + range;
			chartYMovePanel.setRange(ChartMovePanel.arrondi(range, 2));
			chartYMovePanel.setSearch(ChartMovePanel.arrondi(search, 2));
		} else if (state.equals(ToolsState.STACK.name())) {
			this.toolsState = ToolsState.STACK;
		}
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.CassisViewInterface#getHelpUrl()
	 */
	@Override
	public String getHelpUrl() {
		return "FullSpectrum.html";
	}


	/**
	 * @see eu.omp.irap.cassis.gui.plot.CassisViewInterface#getTitle()
	 */
	@Override
	public String getTitle() {
		return "Full Spectrum";
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.CassisViewInterface#getComponent()
	 */
	@Override
	public Component getComponent() {
		return this;
	}

	@Override
	public List<LineDescription> getSyntheticLineVisible() {
		return getModel().getSyntheticLineVisible();
	}

	@Override
	public List<LineDescription> getOtherSpeciesLineVisible() {
		return getModel().getOtherSpeciesLineVisible();
	}

	@Override
	public int print(Graphics graphics, PageFormat pageFormat, int pageIndex)
			throws PrinterException {
		return PrintUtil.print(graphics, pageFormat, pageIndex,
				getSave().getImageToPrint());
	}

	@Override
	public void restorePanel(JComponent panel) {
		westPanel.add(panel, BorderLayout.CENTER);
		westPanel.revalidate();
	}

	@Override
	public boolean checkIfSaveIsPossible(File file, EXTENSION ext) {
		if (file.exists()) {
			String message = "This file " + file.toString()
					+ " already exists.\n" + "Do you want to replace it?";
			// Modal dialog with yes/no button
			int result = JOptionPane.showConfirmDialog(null, message,
					"Replace existing file?", JOptionPane.YES_NO_OPTION);
			if (result == JOptionPane.NO_OPTION)
				return false;
		}
		return true;
	}

	public ToolsState getToolsState() {
		return toolsState;
	}

	public FitPanel getFitPanel() {
		return fitPanel;
	}

	private JPanel getToolbar() {
		if (toolbar == null) {
			toolbar = new JPanel(new FlowLayout(FlowLayout.RIGHT));
			toolbar.add(getFreqRefLabel());
			toolbar.setVisible(true);
		}
		return toolbar;
	}

	public JLabel getFreqRefLabel() {
		if (freqRef == null) {
			freqRef = new JLabel("Reference frequency:");
			freqRef.setVisible(false);
		}
		return freqRef;
	}

	@Override
	public MessageControl getMessageControl() {
		return messageControl;
	}

	public ToolsView getToolsView() {
		if (toolsView == null) {
			toolsView = new ToolsView(model.getToolsModel());
		}
		return toolsView;
	}

	@Override
	public boolean isOnGallery() {
		return false;
	}

	@Override
	public GallerySortPane getGallerySortPane() {
		return null;
	}

	@Override
	public List<CassisPlot> getListCassisPlots() {
		return null;
	}

	@Override
	public void moveLeft() {
		// Not used, only one plot on Full Spectrum.
	}

	@Override
	public void moveRight() {
		// Not used, only one plot on Full Spectrum.
	}

	@Override
	public void switchTo(int i, int j) {
		// Not used, only one plot on Full Spectrum.
	}

	@Override
	public void switchBack() {
		// Not used, only one plot on Full Spectrum.
	}

	@Override
	public void refreshCurrentTriplePlot() {
		// Not used, only one plot on Full Spectrum.
	}

	/**
	 * <b>Should not be used. Not implemented/used here.</b>
	 *
	 * @see eu.omp.irap.cassis.gui.plot.save.SavePlotInterface#getVisibleSavableSpectra()
	 */
	@Override
	public List<String> getVisibleSavableSpectra() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Update the labels of the TriplePlot.
	 */
	public void updatePlotLabels() {
		getSpectrumPlot().setBottomTitle(model.getXAxisCassis().getTitleLabel());
		getSpectrumPlot().setLeftTitle(model.getYAxisCassis().toString());
		getSpectrumPlot().setXAxisCassisTop(model.getXAxisCassis());
	}

	/**
	 * Update the rendering type of the TriplePlot.
	 */
	public void updateRendering() {
		switch (model.getRendering()) {
		case LINE:
			getSpectrumPlot().setLineRendering();
			break;

		case DOT:
			getSpectrumPlot().setDotRendering();
			break;

		case HISTOGRAM:
			getSpectrumPlot().setHistogramRendering();
			break;

		default:
			throw new IllegalArgumentException("Unknown rendering type");
		}
	}

}
