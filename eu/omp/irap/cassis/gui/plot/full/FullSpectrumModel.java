/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.full;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.Formula;
import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.TypeFrequency;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.XAxisVelocity;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.gui.fit.advanced.AdvancedFitModel;
import eu.omp.irap.cassis.gui.fit.advanced.ModelFitManager;
import eu.omp.irap.cassis.gui.fit.advanced.interfaces.FitSourceInterface;
import eu.omp.irap.cassis.gui.model.CassisModel;
import eu.omp.irap.cassis.gui.otherspecies.SpeciesModel;
import eu.omp.irap.cassis.gui.plot.abstractplot.AbstractPlotModel;
import eu.omp.irap.cassis.gui.plot.abstractplot.FitModelInterface;
import eu.omp.irap.cassis.gui.plot.curve.Rendering;
import eu.omp.irap.cassis.gui.plot.curve.TypeCurve;
import eu.omp.irap.cassis.gui.plot.curve.XYLineSeriesCassis;
import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassis;
import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassisCollection;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import eu.omp.irap.cassis.gui.plot.curve.config.ColorsCurve;
import eu.omp.irap.cassis.gui.plot.curve.config.ShapeCassis;
import eu.omp.irap.cassis.gui.plot.curve.config.StrokeCassis;
import eu.omp.irap.cassis.gui.plot.infopanel.InfoPanelConstants;
import eu.omp.irap.cassis.gui.plot.tools.ToolsModel;
import eu.omp.irap.cassis.gui.plot.tools.ViewType;
import eu.omp.irap.cassis.gui.plot.util.StackMosaicModel;
import eu.omp.irap.cassis.parameters.LineIdentificationUtils;
import eu.omp.irap.cassis.properties.Software;

public class FullSpectrumModel extends AbstractPlotModel implements FitSourceInterface, FitModelInterface {

	public static final String ADD_RESULT_EVENT = "addResult";
	public static final String CENTER_DATASET_CHANGE_EVENT = "centerDatasetChange";
	public static final String CHANGE_RENDERING_EVENT = "changeRendering";
	public static final String FREQ_REF_EVENT = "freqRef";
	public static final String LOFREQ_EVENT = "loFreq";
	public static final String TYPE_FREQUENCY_EVENT = "typeFrequency";
	public static final String VLSR_DATA_EVENT = "vlsrData";

	private XYSeriesCassisCollection centerDataset;
	private XYSeriesCassisCollection topLineDataset;
	private XYSeriesCassisCollection topLineErrorDataset;
	private XYSeriesCassisCollection bottomLineSignalDataset;
	private XYSeriesCassisCollection bottomLineImageDataset;

	private double vlsrData;
	private double freqRef = Double.NaN;
	private XAxisCassis xAxisCassis;
	private XAxisCassis xAxisCassisTop;

	private ToolsModel toolsModel;

	/**
	 * Creates a new FullSpectrumModel.
	 */
	public FullSpectrumModel() {
		this.createCollections();
		this.fitModelManager = new ModelFitManager();
		this.speciesModel = new SpeciesModel(true);
		this.stackMosaicModel = new StackMosaicModel();
		xAxisCassis = XAxisCassis.getXAxisFrequency(UNIT.GHZ);
		xAxisCassisTop = XAxisCassis.getXAxisFrequency(UNIT.GHZ);
		toolsModel = new ToolsModel(this, 0);
		rendering = Rendering.valueOf(Software.getUserConfiguration().getRendering());
	}

	public ModelFitManager getFitModelManager() {
		return fitModelManager;
	}

	public AdvancedFitModel getCurrentFitModel() {
		return fitModelManager.getModel(0);
	}

	/**
	 * Remove all dataset.
	 */
	public void removeAllData() {
		if (centerDataset != null) {
			centerDataset.removeAllSeries();
			triggerCenterDatasetChange();
		}
		removeTopDataset();
		removeBottomDataset();
		vlsrData = 0.0;
		freqRef = Double.NaN;
	}

	public void removeTopDataset() {
		if (topLineDataset != null) {
			topLineDataset.removeAllSeries();
		}
		if (topLineErrorDataset != null) {
			topLineErrorDataset.removeAllSeries();
		}
	}

	public void removeBottomDataset() {
		if (bottomLineSignalDataset != null) {
			bottomLineSignalDataset.removeAllSeries();
		}
		if (bottomLineImageDataset != null) {
			bottomLineImageDataset.removeAllSeries();
		}
	}

	public int getCenterSeriesVisibleCount() {
		int nb = 0;
		List<XYSeriesCassis> series = centerDataset.getSeries();
		for (XYSeriesCassis serie : series) {
			if (serie.getConfigCurve().isVisible()) {
				nb++;
			}
		}
		return nb;
	}

	@Override
	public XAxisCassis getXAxisCassis() {
		return xAxisCassis;
	}

	public XAxisCassis getXAxisCassisTop() {
		return xAxisCassisTop;
	}

	@Override
	public String getNearestLineInfo(double xValue, double yValue, Double[] valueReturn, XYSeriesCollection series) {
		List<LineDescription> listOfLines = new ArrayList<>();
		for (int i = 0; i < series.getSeriesCount(); i++) {
			XYSeriesCassis serieCassis = (XYSeriesCassis) series.getSeries(i);
			if (serieCassis.getConfigCurve().isVisible()
					&& serieCassis.getListOfLines() != null) {
				listOfLines.addAll(serieCassis.getListOfLines());
			}
		}
		if (getXAxisCassis() instanceof XAxisVelocity) {
			((XAxisVelocity)getXAxisCassis()).setFreqRef(freqRef);
			((XAxisVelocity)getXAxisCassis()).setVlsr(vlsrData);
		}
		String res = getNearestLine(xValue, listOfLines, valueReturn, getXAxisCassis(),
				speciesModel.getTypeFrequency());
		if (res.isEmpty())
			res = "No line found";

		return res;
	}

	/**
	 * @param xLine : the X cliked
	 * @param listOfLines : list of Lines Description
	 * @param valReturn : the x value return in scale Axis
	 * @param xAxisCassis : the current xAxis
	 * @param typeFrequency : the type of the frequency
	 * @return the line information
	 */
	public static String getNearestLine(final double xLine,
			final List<LineDescription> listOfLines, Double[] valReturn,
			XAxisCassis xAxisCassis, TypeFrequency typeFrequency) {
		if (listOfLines.isEmpty())
			return "";

		double valueToCompare = Double.MAX_VALUE - 1;
		double tmpDifference;
		int nearestLineIndex = -1;
		ArrayList<Integer> indicesNearestLines = new ArrayList<>();
		double valueFind = 0;

		if (xAxisCassis instanceof XAxisVelocity && TypeFrequency.SKY.equals(typeFrequency)) {
			xAxisCassis.setVlsr(0);
		}

		final Double convertToMHzFreq = xAxisCassis.convertToMHzFreq(xLine);

		for (int i = 0; i < listOfLines.size(); i++) {
			LineDescription line = listOfLines.get(i);
			double value = line.getFreqCompute();
			if (line.isDoubleSideBand())
				value = Formula.convertFreqWithLoFreq(value, xAxisCassis.getLoFreq());

			tmpDifference = Math.abs(value - convertToMHzFreq);

			if (tmpDifference <= valueToCompare) {
				if (tmpDifference < valueToCompare) {
					nearestLineIndex = i;
					valueToCompare = tmpDifference;
					valueFind = value;
					indicesNearestLines.clear();
				}
				indicesNearestLines.add(i);
			}
		}

		final StringBuilder lineInfo = new StringBuilder();
		if (nearestLineIndex > -1) {
			double vlsr = 0.;
			for (Integer indice : indicesNearestLines) {
				LineDescription line = listOfLines.get(indice);
				lineInfo.append(line.getIdentification());
				if (TypeFrequency.SKY.equals(typeFrequency))
					vlsr = 0;
				else
					vlsr = line.getVlsrData();
				lineInfo.append("\n\n");
			}
			xAxisCassis.setVlsr(vlsr);
			lineInfo.deleteCharAt(lineInfo.length() - 1);
			lineInfo.deleteCharAt(lineInfo.length() - 1);
			valReturn[0] = xAxisCassis.convertFromMhzFreq(valueFind);
		} else {
			lineInfo.append("");
			valReturn[0] = Double.NaN;
		}

		return lineInfo.toString();
	}

	public void addFitAndResidualCurve(XYSeriesCassis serie, XYSeriesCassis residualSerie) {
		centerDataset.addSeries(serie);
		centerDataset.addSeries(residualSerie);
		triggerCenterDatasetChange();
	}

	public void addFitCurve(XYSeriesCassis serie) {
		centerDataset.addSeries(serie);
		triggerCenterDatasetChange();
	}

	public void removeFitAndResiudalCurve() {
		ArrayList<XYSpectrumSeries> listToRemove = new ArrayList<>();
		for (int cpt = 0; cpt < centerDataset.getSeriesCount(); cpt++) {
			XYSeries serie = centerDataset.getSeries(cpt);
			if (serie instanceof XYSpectrumSeries) {
				XYSpectrumSeries seriesCassis = (XYSpectrumSeries) serie;
				if (seriesCassis.getTypeCurve().isFit()) {
					listToRemove.add(seriesCassis);
				}
			}
		}
		for (XYSpectrumSeries serie : listToRemove) {
			centerDataset.removeSeries(serie);
		}
		if (!listToRemove.isEmpty()) {
			triggerCenterDatasetChange();
		}
	}

	public void removeOverlayDataset(XYSeriesCassis serie) {
		if (TypeCurve.OVERLAY_DATA.equals(serie.getTypeCurve())) {
			getCenterDataset().removeSeries(serie);
			triggerCenterDatasetChange();
		}
		else if (TypeCurve.OVERLAY_LINELIST.equals(serie.getTypeCurve())) {
			topLineDataset.removeSeries(serie);
		}
	}

	public Color getColorFitCurve() {
		Color color = Color.RED;
		int seriesIndex = centerDataset.getSeriesIndex(InfoPanelConstants.FIT_TITLE);
		if (seriesIndex != -1){
			XYSeriesCassis serie = centerDataset.getSeries(InfoPanelConstants.FIT_TITLE);
			color = serie.getConfigCurve().getColor();
		}
		return color;
	}

	public XYSpectrumSeries getSeriesToFit() {
		for (int cpt = 0; cpt < centerDataset.getSeries().size(); cpt++) {
			if (centerDataset.getSeries(cpt) instanceof XYSpectrumSeries) {
				XYSpectrumSeries serie = (XYSpectrumSeries) centerDataset.getSeries(cpt);
				if (serie.getConfigCurve().isVisible() && serie.isToFit())
					return serie;
			}
		}
		return null;
	}

	/**
	 * @return the line from visible curve
	 *
	 */
	public List<LineDescription> getSyntheticLineVisible() {
		XYSeriesCassisCollection series = getCenterDataset();
		List<LineDescription> lines = new ArrayList<>();
		for (int cpt = 0; cpt < series.getSeriesCount(); cpt++) {
			XYSeriesCassis serie = series.getSeries(cpt);
			if (serie.getConfigCurve().isVisible()
					&& (TypeCurve.DATA.equals(serie.getTypeCurve()) || TypeCurve.SYNTHETIC.equals(serie.getTypeCurve()))
					&& serie.getListOfLines() != null) {
				lines.addAll(serie.getListOfLines());
			}
		}
		return lines;
	}

	/**
	 * Return the visible Other Species lines.
	 *
	 * @param collection The collection.
	 * @return the list of visible Other Species lines.
	 */
	public List<LineDescription> getOtherSpeciesLineVisible() {
		List<LineDescription> lines = getVisibleLines(getBottomLineSignalDataset());
		lines.addAll(getVisibleLines(getBottomLineImageDataset()));
		return lines;
	}

	/**
	 * Return the visible lines in the given collection.
	 *
	 * @param collection The collection.
	 * @return the list of visible lines in the given collection.
	 */
	public List<LineDescription> getVisibleLines(XYSeriesCassisCollection collection) {
		List<LineDescription> lines = new ArrayList<>();
		for (XYSeriesCassis series : collection.getSeries()) {
			if (series.getConfigCurve().isVisible() && series.getListOfLines() != null) {
				lines.addAll(series.getListOfLines());
			}
		}
		return lines;
	}

	public XYSpectrumSeries getCurrentDataCurve() {
		return getSeriesToFit();
	}

	public XYSeriesCassis removeSeries(int uniqueID, XYSeriesCassisCollection dataset) {
		for (int cpt = 0; cpt < dataset.getSeriesCount(); cpt++) {
			XYSeriesCassis serie = dataset.getSeries(cpt);
			if (serie.getId() == uniqueID) {
				dataset.removeSeries(serie);
				triggerCenterDatasetChange();
				return serie;
			}
		}
		return null;
	}

	public XYSpectrumSeries createSerie(CassisModel cassisModel, XYSeriesCassis serieRef) {
		final CommentedSpectrum commentedSpectrum = cassisModel.getSpectrumSignal();
		Color color = ColorsCurve.getNewColorData();
		ShapeCassis shape = ShapeCassis.NONE;
		StrokeCassis stroke = StrokeCassis.CONTINU;
		if (serieRef != null) {
			color = serieRef.getConfigCurve().getColor();
			stroke = serieRef.getConfigCurve().getStrokeCassis();
			shape = serieRef.getConfigCurve().getShapeCassis();
		}

		String seriesName = cassisModel.getSpectrumName() == null ||
				cassisModel.getSpectrumName().isEmpty() ?
				cassisModel.getConfigName() : cassisModel.getSpectrumName();
		seriesName = getUniqueSeriesName(seriesName);
		final XYSpectrumSeries serie = new XYSpectrumSeries(seriesName,
				cassisModel.getModel().getXaxisAskToDisplay(), commentedSpectrum.getyAxis(),TypeCurve.DATA, commentedSpectrum);
		serie.setForcedPanelCurveTitle(cassisModel.getConfigName());
		serie.setId(cassisModel.getModelId());
		serie.getConfigCurve().setColor(color);
		serie.getConfigCurve().setShapeCassis(shape);
		serie.getConfigCurve().setStrokeCassis(stroke);
		serie.setToFit(true);
		centerDataset.addSeries(serie);
		if (fitModelManager.getNbModels() == 0) {
			fitModelManager.addModel(this);
		}
		triggerCenterDatasetChange();
		return serie;
	}

	public void setParametersForNewDataRef(final CommentedSpectrum commentedSpectrum) {
		if (commentedSpectrum != null) {
			setLoFrequency(commentedSpectrum.getLoFreq());
			setVlsrData(commentedSpectrum.getVlsr());
			setFreqRef(commentedSpectrum.getFreqRef());
			setTypeFrequency(commentedSpectrum.getTypeFreq());
		} else {
			setLoFrequency(Double.NaN);
			setVlsrData(0.);
			setFreqRef(Double.NaN);
			setTypeFrequency(TypeFrequency.REST);
		}
	}

	private void setVlsrData(final double vlsrData) {
		this.vlsrData = vlsrData;
		fireDataChanged(new ModelChangedEvent(VLSR_DATA_EVENT, vlsrData));
	}

	private void setLoFrequency(final double loFreq) {
		boolean haveDiffLo = false;
		if (!Double.isNaN(loFreq)) {
			for (int i = 0; i < getCenterDataset().getSeriesCount(); i++) {
				final XYSeriesCassis series = getCenterDataset().getSeries(i);
				if (series.getConfigCurve().isVisible()) {
					double loFreqOtherSerie = ((XYSpectrumSeries) series).getSpectrum().getLoFreq();
					if (Double.isNaN(loFreqOtherSerie) || Math.abs(loFreqOtherSerie - loFreq) > 1e-4) {
						haveDiffLo = true;
						break;
					}
				}
			}
		}
		if (haveDiffLo) {
			xAxisCassis.setLoFreq(Double.NaN);
			fireDataChanged(new ModelChangedEvent(LOFREQ_EVENT));
		} else {
			xAxisCassis.setLoFreq(loFreq);
			fireDataChanged(new ModelChangedEvent(LOFREQ_EVENT, loFreq));
		}
	}

	public void replaceDataSerie(XYSeriesCassis newDataSerie) {
		for (int cpt = 0; cpt < centerDataset.getSeriesCount(); cpt++) {
			XYSeries serie = centerDataset.getSeries(cpt);
			if (serie instanceof XYSpectrumSeries) {
				XYSpectrumSeries seriesCassis = (XYSpectrumSeries) serie;
				if (seriesCassis.getKey().equals(newDataSerie.getKey())) {
					centerDataset.removeSeries(seriesCassis);
					break;
				}
			}
		}
		centerDataset.addSeries(newDataSerie);
		triggerCenterDatasetChange();
	}

	/**
	 * @return the vlsr0
	 */
	public final double getVlsrData() {
		return vlsrData;
	}

	/**
	 * Return the reference frequency.
	 *
	 * @return the reference frequency.
	 */
	@Override
	public double getFreqRef() {
		return freqRef;
	}

	/**
	 * Change the reference frequency.
	 *
	 * @param freqRef The new reference frequency to set.
	 */
	public void setFreqRef(double freqRef) {
		this.freqRef =  freqRef;
		fireDataChanged(new ModelChangedEvent(FREQ_REF_EVENT, freqRef));
	}

	public SpeciesModel getSpeciesModel() {
		return speciesModel;
	}

	/**
	 * AddOverlayFullSpectrum Dataset.
	 *
	 * @param fileModel The CassisModel to add.
	 * @return the serie added in the overlayDataSet
	 */
	public XYSpectrumSeries addOverlayFullSpectrumDataset(CassisModel fileModel) {
		Color color = ColorsCurve.getNewColorData();
		ShapeCassis shape = ShapeCassis.NONE;
		StrokeCassis stroke = StrokeCassis.CONTINU;

		String seriesName = getUniqueSeriesName(fileModel.getSpectrumName());
		XYSpectrumSeries serie = new XYSpectrumSeries(seriesName, xAxisCassis, TypeCurve.OVERLAY_DATA,
					fileModel.getSpectrumSignal());
		serie.getConfigCurve().setColor(color);
		serie.getConfigCurve().setShapeCassis(shape);
		serie.getConfigCurve().setStrokeCassis(stroke);
		setVlsrData(serie.getSpectrum().getVlsr());
		centerDataset.addSeries(serie);

		serie.setToFit(true);
		triggerCenterDatasetChange();
		return serie;
	}

	public List<LineDescription> getOverlayLine(String file) throws NumberFormatException, IOException {
		List<LineDescription> lines = new ArrayList<>();
		String line = null;
		// read the file
		try (BufferedReader bufferReader = new BufferedReader(new FileReader(file))) {
			Double minFreq = getDataMinFreq();
			Double maxFreq = getDataMaxFreq();
			if (minFreq == null || maxFreq == null) {
				return null;
			}
			while ((line = bufferReader.readLine()) != null) {
				if (line.startsWith("#") || line.startsWith("Transition"))
					continue;
				String[] val = Pattern.compile("[\t]+").split(line);
				if (val.length == 2)
					continue;
				if (val.length == 8) {
					double freq = Double.parseDouble(val[2]);
					if (freq < maxFreq.doubleValue() && freq > minFreq.doubleValue()) {
						LineDescription lineId = new LineDescription();
						lineId.setObsFrequency(freq);
						lineId.setVlsrData(getVlsrData());
						lineId.setEUpK(Double.parseDouble(val[3]));
						lineId.setAij(Double.parseDouble(val[4]));
						lineId.setIdentification(LineIdentificationUtils
								.createHeightColumnsFileIdentification(val[0], freq,
										lineId.getEUpK(), lineId.getAij()));
						lines.add(lineId);
					}
				}
				else {
					double freq = Double.parseDouble(val[2]);
					if (freq < maxFreq.doubleValue() && freq > minFreq.doubleValue()) {
						double vlsr = Double.parseDouble(val[7]);
						LineDescription lineId = new LineDescription();
						lineId.setObsFrequency(Formula.calcFreqWithVlsr(freq, vlsr, freq));

						lineId.setVlsrData(getVlsrData());
						lineId.setEUpK(Double.parseDouble(val[3]));
						lineId.setAij(Double.parseDouble(val[4]));
						lineId.setVlsr(vlsr);
						lineId.setDObsFrequency(freq - Double.parseDouble(val[5]));
						lineId.setIdentification(
								LineIdentificationUtils.createOldFitFileIdentification(
										val[1], freq, lineId.getEUpK(), lineId.getAij(),
										lineId.getDObsFrequency(), lineId.getVlsr()));
						lines.add(lineId);
					}
				}
			}
		}

		return lines;
	}

	public StackMosaicModel getStackMosaicModel() {
		return stackMosaicModel;
	}

	@Override
	public String getTelescope() {
		return telescope;
	}

	@Override
	public void setTelescope(String telescope) {
		this.telescope = telescope;
	}

	public void setXAxisCassis(XAxisCassis xAxisCassis) {
		this.xAxisCassis = xAxisCassis;
	}

	public void setXAxisCassisTop(XAxisCassis xAxisCassis) {
		this.xAxisCassisTop = xAxisCassis;
	}

	//TODO remove duplicate functionalities with getyAxis()
	public YAxisCassis getYAxisCassis() {
		return yAxisCassis;
	}

	@Override
	public YAxisCassis getyAxis() {
		return yAxisCassis;
	}

	public void setYAxisCassis(YAxisCassis newYAxis) {
		yAxisCassis = newYAxis;
	}

	@Override
	public String getBottomAxis() {
		return getXAxisCassis().toString();
	}

	@Override
	public String getTopAxis() {
		return xAxisCassisTop.toString();
	}

	@Override
	public String getLeftAxis() {
		return yAxisCassis.toString();
	}

	public TypeFrequency getTypeFrequency() {
		return typeFrequency;
	}

	private void setTypeFrequency(TypeFrequency typeFreq) {
		this.typeFrequency = typeFreq;
		fireDataChanged(new ModelChangedEvent(TYPE_FREQUENCY_EVENT, typeFreq));
	}

	public Double getDataMinFreq() {
		Double minFreq = Double.MAX_VALUE;
		for (XYSeriesCassis serie : getCenterDataset().getSeries()) {
			if (serie.getTypeCurve() == TypeCurve.DATA && serie instanceof XYSpectrumSeries
					&& ((XYSpectrumSeries) serie).getSpectrum().getFrequencySignalMin() < minFreq) {
				minFreq = ((XYSpectrumSeries) serie).getSpectrum().getFrequencySignalMin();
			}
		}
		if (minFreq == Double.MAX_VALUE)
			return null;
		return minFreq;
	}

	public Double getDataMaxFreq() {
		Double maxFreq = Double.MIN_VALUE;
		for (XYSeriesCassis serie : getCenterDataset().getSeries()) {
			if (serie.getTypeCurve() == TypeCurve.DATA && serie instanceof XYSpectrumSeries
					&& ((XYSpectrumSeries) serie).getSpectrum().getFrequencySignalMax() > maxFreq) {
				maxFreq = ((XYSpectrumSeries) serie).getSpectrum().getFrequencySignalMax();
			}
		}
		if (maxFreq == Double.MIN_VALUE)
			return null;
		return maxFreq;
	}

	@Override
	public List<XYSpectrumSeries> getDataForToolsSpectrumOne(int numPlot) {
		List<XYSpectrumSeries> listSeries = new ArrayList<>();
		for (XYSeriesCassis serie : centerDataset.getSeries()) {
			TypeCurve tc = serie.getTypeCurve();
			if (TypeCurve.DATA.equals(tc) || TypeCurve.OVERLAY_DATA.equals(tc) || TypeCurve.RESULT.equals(tc)) {
				listSeries.add((XYSpectrumSeries) serie);
			}
		}
		return listSeries;
	}

	@Override
	public List<XYSpectrumSeries> getDataForToolsSpectrumTwo(int numPlot) {
		List<XYSpectrumSeries> listSeries = new ArrayList<>();
		for (XYSeriesCassis serie : centerDataset.getSeries()) {
			TypeCurve tc = serie.getTypeCurve();
			if (TypeCurve.DATA.equals(tc) || TypeCurve.OVERLAY_DATA.equals(tc) || TypeCurve.RESULT.equals(tc) || TypeCurve.FIT.equals(tc) || TypeCurve.SYNTHETIC.equals(tc)) {
				listSeries.add((XYSpectrumSeries) serie);
			}
		}
		return listSeries;
	}

	@Override
	public void addResult(XYSpectrumSeries serie, int numPlot, boolean refresh) {
		serie.setToFit(true);
		centerDataset.addSeries(serie);
		if (refresh) {
			fireDataChanged(new ModelChangedEvent(ADD_RESULT_EVENT, serie));
		}
		triggerCenterDatasetChange();
	}

	@Override
	public List<XYSpectrumSeries> getDataForToolsSpectrumAll(int numPlot) {
		List<XYSpectrumSeries> listSeries = new ArrayList<>();
		for (XYSeriesCassis serie : centerDataset.getSeries()) {
			listSeries.add((XYSpectrumSeries) serie);
		}
		return listSeries;
	}

	public void removeResults() {
		ArrayList<XYSeriesCassis> listToRemove = new ArrayList<>();
		for (XYSeriesCassis serie : centerDataset.getSeries()) {
			if (TypeCurve.RESULT.equals(serie.getTypeCurve())) {
				listToRemove.add(serie);
			}
		}
		for (XYSeriesCassis serie : listToRemove) {
			centerDataset.removeSeries(serie);
		}
		triggerCenterDatasetChange();
	}

	public ToolsModel getToolsModel() {
		return toolsModel;
	}

	@Override
	public ViewType getViewType() {
		return ViewType.FULL_SPECTRUM;
	}

	public void removeOtherSpeciesImage() {
		if (bottomLineImageDataset == null) {
			return;
		}
		bottomLineImageDataset.removeAllSeries();
	}

	private void triggerCenterDatasetChange() {
		refreshFitManager();
		fireDataChanged(new ModelChangedEvent(CENTER_DATASET_CHANGE_EVENT, centerDataset.getSeriesCount()));
	}

	private void refreshFitManager() {
		int fittable = 0;
		for (int i = 0 ; i < centerDataset.getSeriesCount() ; i++) {
			if (centerDataset.getSeries(i) instanceof XYSpectrumSeries
					&& ((XYSpectrumSeries) centerDataset.getSeries(i)).isToFit()) {
				fittable++;
			}
		}
		if (fittable == 0) {
			fitModelManager.clearModels();
		} else {
			fitModelManager.refreshInputs();
		}
	}

	/**
	 * Return the {@link Rendering}.
	 *
	 * @return the {@link Rendering}.
	 */
	public Rendering getRendering() {
		return rendering;
	}

	/**
	 * Change the rendering.
	 *
	 * @param newRendering The new rendering to set.
	 */
	public void setRendering(Rendering newRendering) {
		boolean change = rendering != newRendering;
		rendering = newRendering;
		updateRendering();
		if (change) {
			fireDataChanged(new ModelChangedEvent(CHANGE_RENDERING_EVENT));
		}
	}

	/**
	 * Create the collections and update the rendering for them.
	 */
	private void createCollections() {
		centerDataset = new XYSeriesCassisCollection();
		topLineDataset = new XYSeriesCassisCollection();
		topLineErrorDataset = new XYSeriesCassisCollection();
		bottomLineSignalDataset = new XYSeriesCassisCollection();
		bottomLineImageDataset = new XYSeriesCassisCollection();
		updateRendering();
	}

	/**
	 * Update the rendering of the currents collections.
	 */
	public void updateRendering() {
		centerDataset.setRendering(rendering);
		topLineDataset.setRendering(rendering);
		topLineErrorDataset.setRendering(rendering);
		bottomLineSignalDataset.setRendering(rendering);
		bottomLineImageDataset.setRendering(rendering);
	}

	/**
	 * Check and return if the series with the given name is available.
	 *
	 * @param name The name to search
	 * @return true if the name is available, false otherwise
	 */
	public boolean isSeriesNameAvalaible(String name) {
		return !centerDataset.containsSeries(name)
				&& !topLineDataset.containsSeries(name)
				&& !topLineErrorDataset.containsSeries(name)
				&& !bottomLineSignalDataset.containsSeries(name)
				&& !bottomLineImageDataset.containsSeries(name);
	}

	/**
	 * Check and return if the series name is allowed: Fit, Fit residual, Signal
	 *  and Image are not allowed.
	 *
	 * @param name The name to check
	 * @return true if the name is allowed, false otherwise
	 */
	public boolean isSeriesNameAllowed(String name) {
		boolean allowed;
		switch (name) {
		case InfoPanelConstants.FIT_TITLE:
		case InfoPanelConstants.FIT_RESIDUAL_TITLE:
		case InfoPanelConstants.SIGNAL_TITLE:
		case InfoPanelConstants.IMAGE_TITLE:
			allowed = false;
			break;
		default:
			allowed = true;
			break;
		}
		return allowed;
	}

	/**
	 * Check and return if the given name is allowed and available.
	 *
	 * @param name The name to check
	 * @return true if the name is allowed and available, false otherwise.
	 */
	public boolean isSeriesNameAllowedAndAvailable(String name) {
		return isSeriesNameAllowed(name) && isSeriesNameAvalaible(name);
	}

	/**
	 * Check if the given series name is allowed and available then return a new
	 *  created name who is unique if necessary.
	 *
	 * @param seriesName The base series name
	 * @return an unique name for a series
	 */
	public String getUniqueSeriesName(String seriesName) {
		if (isSeriesNameAllowedAndAvailable(seriesName)) {
			return seriesName;
		}
		String newName;
		int i = 0;
		do {
			i++;
			newName = seriesName + '_' + i;
		} while (!isSeriesNameAllowedAndAvailable(newName));
		return newName;
	}

	/**
	 * Return the dataset who contains the lines on the top.
	 *
	 * @return the top line dataset.
	 */
	public XYSeriesCassisCollection getTopLineDataset() {
		return topLineDataset;
	}

	/**
	 * Return the dataset who contains the lines error on the top.
	 *
	 * @return the top lines error dataset.
	 */
	public XYSeriesCassisCollection getTopLineErrorDataset() {
		return topLineErrorDataset;
	}

	/**
	 * Return the dataset who contains the other species signal lines.
	 *
	 * @return the other species signal dataset.
	 */
	public XYSeriesCassisCollection getBottomLineSignalDataset() {
		return bottomLineSignalDataset;
	}

	/**
	 * Return the dataset who contains the other species image lines.
	 *
	 * @return the other species image dataset.
	 */
	public XYSeriesCassisCollection getBottomLineImageDataset() {
		return bottomLineImageDataset;
	}

	/**
	 * Return the center dataset.
	 *
	 * @return the center dataset.
	 */
	public XYSeriesCassisCollection getCenterDataset() {
		return centerDataset;
	}

	/**
	 * Set the given dataset as center dataset.
	 *
	 * @param dataset The dataset to set.
	 */
	public void setCenterDataset(XYSeriesCassisCollection dataset) {
		dataset.setRendering(rendering);
		centerDataset = dataset;
	}

	/**
	 * Set the given dataset as top line dataset.
	 *
	 * @param dataset The dataset to set.
	 */
	public void setTopLineDataset(XYSeriesCassisCollection dataset) {
		dataset.setRendering(rendering);
		topLineDataset = dataset;
	}

	/**
	 * Set the given dataset as top line error dataset.
	 *
	 * @param dataset The dataset to set.
	 */
	public void setTopLineErrorDataset(XYSeriesCassisCollection dataset) {
		dataset.setRendering(rendering);
		topLineErrorDataset = dataset;
	}

	/**
	 * Set the given dataset as bottom line signal dataset.
	 *
	 * @param dataset The dataset to set.
	 */
	public void setBottomLineSignalDataset(XYSeriesCassisCollection dataset) {
		dataset.setRendering(rendering);
		bottomLineSignalDataset = dataset;
	}

	/**
	 * Set the given dataset as bottom line image dataset.
	 *
	 * @param dataset The dataset to set.
	 */
	public void setBottomLineImageDataset(XYSeriesCassisCollection dataset) {
		dataset.setRendering(rendering);
		bottomLineImageDataset = dataset;
	}

	@Override
	public XAxisCassis getxAxis() {
		return xAxisCassis;
	}

	@Override
	public double getVlsr() {
		return vlsrData;
	}

	@Override
	public XYSpectrumSeries getCurrentSeries() {
		XYSpectrumSeries invisibleSeries = null;
		for (int cpt = 0; cpt < centerDataset.getSeries().size(); cpt++) {
			if (centerDataset.getSeries(cpt) instanceof XYSpectrumSeries) {
				XYSpectrumSeries serie = (XYSpectrumSeries) centerDataset.getSeries(cpt);
				if (!serie.getConfigCurve().isVisible() && serie.isToFit()) {
					invisibleSeries = serie;
				} else if (serie.isToFit()) {
					return serie;
				}
			}
		}
		return invisibleSeries;
	}

	@Override
	public List<XYSpectrumSeries> getAllSeries() {
		List<XYSpectrumSeries> series = new ArrayList<>();
		for (XYSeriesCassis cassisSeries : centerDataset.getSeries()) {
			if (cassisSeries instanceof XYSpectrumSeries) {
				XYSpectrumSeries spectrumSeries = (XYSpectrumSeries) cassisSeries;
				if (!isFitCurve(spectrumSeries.getTypeCurve())) {
					series.add(spectrumSeries);
				}
			}
		}
		return series;
	}

	@Override
	public List<LineDescription> getBottomPossibleLines() {
		List<LineDescription> lines = new ArrayList<>();
		for (XYSeriesCassis seriesCassis : bottomLineImageDataset.getSeries()) {
			if (seriesCassis instanceof XYLineSeriesCassis) {
				lines.addAll(((XYLineSeriesCassis) seriesCassis).getListOfLines());
			}
		}
		for (XYSeriesCassis seriesCassis : bottomLineSignalDataset.getSeries()) {
			if (seriesCassis instanceof XYLineSeriesCassis) {
				lines.addAll(((XYLineSeriesCassis) seriesCassis).getListOfLines());
			}
		}
		return lines;
	}

	@Override
	public List<LineDescription> getTopPossibleLines() {
		List<LineDescription> lines = new ArrayList<>();
		for (XYSeriesCassis seriesCassis : topLineDataset.getSeries()) {
			if (seriesCassis instanceof XYLineSeriesCassis) {
				lines.addAll(((XYLineSeriesCassis) seriesCassis).getListOfLines());
			}
		}
		return lines;
	}

	@Override
	public List<LineDescription> getAllLines() {
		List<LineDescription> lines = new ArrayList<>(getBottomPossibleLines());
		lines.addAll(getTopPossibleLines());
		return lines;
	}

	private boolean isFitCurve(TypeCurve type) {
		return type.equals(TypeCurve.FIT) || type.equals(TypeCurve.FIT_COMPO) || type.equals(TypeCurve.FIT_RESIDUAL);
	}

}
