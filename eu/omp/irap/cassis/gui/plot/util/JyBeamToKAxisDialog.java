/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.util;

import java.awt.Frame;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import eu.omp.irap.cassis.gui.util.JDoubleCassisTextField;

public class JyBeamToKAxisDialog extends JDialog implements
		PropertyChangeListener {

	private static final long serialVersionUID = 1L;
	private double beamMajorValue;
	private double beamMinorValue;

	private JDoubleCassisTextField beamMajorValueTextField;
	private JDoubleCassisTextField beamMinorValueTextField;

	private JOptionPane optionPane;


	/**
	 * Creates the JyBeamToK Axis dialog.
	 *
	 * @param aFrame The owner frame.
	 * @param beamMinor The beamMinor.
	 * @param beamMajor The beamMajor.
	 */
	public JyBeamToKAxisDialog(Frame aFrame, double beamMinor, double beamMajor) {
		super(aFrame, true);
		this.beamMinorValue = beamMinor;
		this.beamMajorValue = beamMajor;
		setTitle(" Flux [Jy/beam] <-> T [K]");
		setContentPane(getOptionPane());
		setModalityType(DEFAULT_MODALITY_TYPE);

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent we) {
				optionPane.setValue(JOptionPane.CLOSED_OPTION);
			}
		});

		// Ensure the text field always gets the first focus.
		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentShown(ComponentEvent ce) {
				beamMajorValueTextField.requestFocusInWindow();
			}
		});

		// Register an event handler that reacts to option pane state changes.
		optionPane.addPropertyChangeListener(this);
		setSize(420, 250);
	}

	public JOptionPane getOptionPane() {
		if (optionPane == null) {
			JPanel conversionParametersPanel = new JPanel();
			JPanel minParamPanel = new JPanel();
			minParamPanel.add(new JLabel("Beam major: "));
			minParamPanel.add(getBeamMajorTextField());
			minParamPanel.add(new JLabel("arcsec"));

			JPanel maxParamPanel = new JPanel();
			maxParamPanel.add(new JLabel("Beam minor: "));
			maxParamPanel.add(getBeamMinorTextField());
			maxParamPanel.add(new JLabel("arcsec"));

			conversionParametersPanel.add(minParamPanel);
			conversionParametersPanel.add(maxParamPanel);

			Object[] array = { "Please enter the beam major and minor axes\n" +
					"Note that the calculation will be performed \n using the Rayleigh-Jeans approximation. :", conversionParametersPanel };
			optionPane = new JOptionPane(array, JOptionPane.QUESTION_MESSAGE,
					JOptionPane.OK_CANCEL_OPTION);
		}
		return optionPane;
	}


	public JDoubleCassisTextField getBeamMajorTextField() {
		if(beamMajorValueTextField == null) {
			beamMajorValueTextField = new JDoubleCassisTextField(8, beamMajorValue);
		}
		return beamMajorValueTextField;
	}


	public JDoubleCassisTextField getBeamMinorTextField() {
		if(beamMinorValueTextField == null) {
			beamMinorValueTextField = new JDoubleCassisTextField(8, beamMinorValue);
		}
		return beamMinorValueTextField;
	}


	/** This method reacts to state changes in the option pane. */
	@Override
	public void propertyChange(PropertyChangeEvent e) {
		String prop = e.getPropertyName();

		if (isVisible()
				&& e.getSource() == optionPane
				&& (JOptionPane.VALUE_PROPERTY.equals(prop) || JOptionPane.INPUT_VALUE_PROPERTY
						.equals(prop))) {
			int value = (Integer)optionPane.getValue();

			if (JOptionPane.OK_OPTION == value) {
				beamMajorValue = ((Number) beamMajorValueTextField.getValue()).doubleValue();
				beamMinorValue = ((Number) beamMinorValueTextField.getValue()).doubleValue();
			}
			dispose();
		}
	}

	public double getBeamMajorValue() {
		return beamMajorValue;
	}

	public double getBeamMinorValue() {
		return beamMinorValue;
	}

	public static void main(String[] args) {
		JyBeamToKAxisDialog jyBeamToKAxisDialog = new JyBeamToKAxisDialog(null, 0,0);
		jyBeamToKAxisDialog.setVisible(true);
		System.out.println("Minor Beam = " + jyBeamToKAxisDialog.getBeamMinorValue());
		System.out.println("Major Beam = " + jyBeamToKAxisDialog.getBeamMajorValue());
	}
}
