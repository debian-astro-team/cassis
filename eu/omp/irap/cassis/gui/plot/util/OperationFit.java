/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.util;

import java.awt.Color;
import java.util.List;

import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.gui.plot.curve.TypeCurve;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import herschel.ia.numeric.Double1d;

/**
 * @author glorian
 *
 */
public class OperationFit {

	public static final int FIT_RESOLUTION = 10;

	private Double1d intensityToFit;
	private Double1d xValueToFit;
	private Double1d xValueData;
	private XAxisCassis xAxis = XAxisCassis.getXAxisFrequency();
	private YAxisCassis yAxis = YAxisCassis.getYAxisKelvin();
	private CommentedSpectrum spectrum = null;

	private String key;
	private Color color;
	private TypeCurve typeCurve;


	public OperationFit(XYSpectrumSeries dataToFit) {
//		this.dataToFit = dataToFit;
		xValueData = new Double1d();
		xAxis = dataToFit.getXAxis();
		yAxis = dataToFit.getyAxis();
		spectrum  = dataToFit.getSpectrum();
		key = (String) dataToFit.getKey();
		color = dataToFit.getConfigCurve().getColor();
		typeCurve = dataToFit.getTypeCurve();

	}

	public int setDatastoFit(List<InterValMarkerCassis> domains) {
		int res = 0;
		if (spectrum != null) {
			xValueData = new Double1d(spectrum.getXData(xAxis));
			xValueToFit = new Double1d();
			intensityToFit = new Double1d();

			// No marker
			if (domains == null || domains.isEmpty()) {
				xValueToFit = xValueData;
				intensityToFit = new Double1d(spectrum.getIntensities(yAxis, xAxis.isInverted()));

			}// If marker exists
			else {
				fillXValueAndIntensity(domains);
			}
		} else
			res = -1;

		return res;
	}

	private void fillXValueAndIntensity(List<InterValMarkerCassis> domains) {
		InterValMarkerCassis tmpInterval;
		double minAbsolute;
		double maxAbsolute;
		double xValue;
		double minXValue;
		double maxXValue;
		int minSelectionIndex;
		int maxSelectionIndex;

		for (int i = 0; i < domains.size(); i++) {
			minAbsolute = Double.MAX_VALUE;
			maxAbsolute = Double.NEGATIVE_INFINITY;
			minSelectionIndex = 0;
			maxSelectionIndex = 0;
			tmpInterval = (InterValMarkerCassis) domains.toArray()[i];

			minXValue = tmpInterval.getStartValueCassis();
			maxXValue = tmpInterval.getEndValueCassis();

			// Navigate throught fileSpectrum to get selection indexes
			for (int j = 0; j < xValueData.getSize(); j++) {
				xValue = xValueData.get(j);
				if (xValue >= minXValue && xValue <= maxXValue) {
					if (xValue <= minAbsolute) {
						minSelectionIndex = j;
						minAbsolute = xValue;
					}
					if (xValue >= maxAbsolute) {
						maxSelectionIndex = j;
						maxAbsolute = xValue;
					}
				}
			}

			double[] intensities = spectrum.getIntensities(yAxis, xAxis.isInverted());
			for (int j = minSelectionIndex; j <= maxSelectionIndex; j++) {
				xValueToFit.append(xValueData.get(j));
				intensityToFit.append(intensities[j]);
			}
		}
	}

	public Double1d getIntensityArray() {
		return intensityToFit;
	}

	public Double1d getXValueToFit() {
		return xValueToFit;
	}

	public Double1d getXValueData() {
		return xValueData;
	}

	/**
	 * @return the xAxis
	 */
	public final XAxisCassis getXAxis() {
		return xAxis;
	}

	/**
	 * @return the yAxis
	 */
	public final YAxisCassis getYAxis() {
		return yAxis;
	}

	/**
	 *
	 * @return the spectrum
	 */
	public CommentedSpectrum getSpectrum() {
		return spectrum;
	}

	/**
	 *
	 * @return the name of the curve of spectrum
	 */
	public String getKey() {
		return key;
	}

	/**
	 *
	 * @return the color of the curve fitted, use to get the same color
	 * for the substract curve
	 */
	public Color getColor() {
		return color;
	}

	/**
	 *
	 * @return typeCurve of the curve fitted, use to get the same typecurve
	 * for the substract curve
	 */
	public TypeCurve getTypeCurve() {
		return typeCurve;
	}


}
