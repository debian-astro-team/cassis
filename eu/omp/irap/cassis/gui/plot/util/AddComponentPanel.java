/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.util;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Class defining a panel with a Check box title.<BR>
 * This class is managing 2 panels: the outer panel and the inner panel. The
 * inner panel acts as a "content pane", so every method for adding/removing
 * components or laying out the contents must be called on the inner panel. The
 * method <code>getSubPanel()</code> is made for this purpose.
 */
public class AddComponentPanel extends JPanel {
	// Comment for serialVersionUID
	private static final long serialVersionUID = 1L;

	private JButton buttonManageComponents; // The Button clear
	private final JComponent labelTitle; // The title field
	private final JPanel subPanel; // The sub-panel (content pane)


	public AddComponentPanel(String title) {
		this(title, true);
	}

	/**
	 * Constructs a new titled panel.
	 *
	 * @param title The title of the panel.
	 * @param withCheckbox true if adding checkbox, false otherwise.
	 */
	public AddComponentPanel(String title, boolean withCheckbox) {
		// The outer panel is set with no layout
		super();
		setLayout(new BorderLayout());

		if (withCheckbox) {
			labelTitle = new JCheckBox(title);
			labelTitle.setOpaque(true);
			labelTitle.setSize(labelTitle.getPreferredSize());
			labelTitle.setBorder(null);
		} else {
			labelTitle = new JLabel(title);
			labelTitle.setOpaque(true);
			labelTitle.setSize(labelTitle.getPreferredSize());
			labelTitle.setBorder(null);
		}

		// Create the sub-panel
		subPanel = new JPanel();
		subPanel.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
		// Define the outer panel with titled border (on which
		// will be over-painted the check box)
		setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(18, 0, 0, 0),
				BorderFactory.createTitledBorder("")));

		// Add the 2 components: check box and sub-panel
		add(getManageComponentsButton());
		add(labelTitle);
		add(subPanel, BorderLayout.CENTER);

		// Add the listener for panel resizing
		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				Dimension size = getSize();
				Insets insets = getInsets();

				// Relocate and resize the sub-panel
				subPanel.setLocation(insets.left, insets.top);
				subPanel.setSize(size.width - insets.left - insets.right, size.height - insets.top - insets.bottom);
				subPanel.doLayout();

				// Relocate and resize the check box
				labelTitle.setLocation(insets.left + 6 + buttonManageComponents.getSize().width, 5);
				labelTitle.setSize(labelTitle.getSize().width, insets.top);
				buttonManageComponents.setLocation(insets.left + 2, 5);
				buttonManageComponents.setSize(buttonManageComponents.getSize().width, insets.top);
			}
		});
	}

	public JButton getManageComponentsButton() {
		if (buttonManageComponents == null) {
			buttonManageComponents = new JButton();
			buttonManageComponents.setOpaque(true);
			buttonManageComponents.setText("Manage Components");
			buttonManageComponents.setSize(buttonManageComponents.getPreferredSize());
		}

		return buttonManageComponents;
	}

	/**
	 * Add a listener to the component that will be fired when user click on the
	 * button.
	 *
	 * @param e
	 *            actionListener
	 */
	public void addButtonAddListener(ActionListener e) {
		buttonManageComponents.addActionListener(e);
	}

	/**
	 * Returns the sub-panel (content pane).
	 *
	 * @return panel the sub-panel
	 */
	public JPanel getSubPanel() {
		return subPanel;
	}

	public void addSubPanel(Component comp) {
		subPanel.add(comp);
	}

	public void addActionListener(ActionListener l) {
		if (labelTitle.getClass().equals(JCheckBox.class)) {
			((JCheckBox) labelTitle).addActionListener(l);
			((JCheckBox) labelTitle).setActionCommand("checkAll");
		}
	}

	public boolean isChecked() {
		if (labelTitle.getClass().equals(JCheckBox.class)) {
			return ((JCheckBox) labelTitle).isSelected();
		}
		return false;
	}
}
