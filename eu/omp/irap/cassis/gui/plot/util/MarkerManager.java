/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.util;

import java.awt.Color;
import java.awt.Font;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.jfree.chart.plot.IntervalMarker;
import org.jfree.chart.plot.Marker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.ui.Layer;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.TextAnchor;

import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.YAxisGeneric;

/**
 * @author glorian
 *
 */
public class MarkerManager {

	/** Gaussian fit domain markers. */
	private InterValMarkerCassis interValMarkerCassis;
	public static final Color COLOR_SELECTION = new Color(200, 200, 255);
	private List<InterValMarkerCassis> listMarker = new ArrayList<>();

	/** Use for display the gaussiant parameter. */
	/** Use for display the integral in the marker. */
	private MarkerView markerView;


	public MarkerManager(MarkerView markerView) {
		this.markerView = markerView;
	}

	public void removeMarkerClicked(XYPlot plot, double mouseX,
			List<InterValMarkerCassis> listmarkToremeove,
			Collection<Marker> markers) {
		if (markers != null) {
			for (Marker marker : markers) {
				if (marker.getClass().equals(InterValMarkerCassis.class)) {
					InterValMarkerCassis intervalMarker = (InterValMarkerCassis) marker;

					if (mouseX >= intervalMarker.getStartValue() && mouseX <= intervalMarker.getEndValue())
						listmarkToremeove.add(intervalMarker);
				}
			}
		}
		for (InterValMarkerCassis marker : listmarkToremeove) {
			plot.removeDomainMarker(marker, Layer.BACKGROUND);
			listMarker.remove(marker);
		}
		InterValMarkerCassis marker = getLastSelection();
		if (marker != null) {
			removeAllOtherLabel(marker);
			marker.setLabel(computeIntegral());
		}
	}

	/**
	 * Add a marker.
	 *
	 * @param plot The {@link XYPlot}.
	 * @param mousePressX The x value where the mouse was pressed.
	 * @param mouseReleaseX The x value where the mouse was released.
	 */
	public void addMarker(XYPlot plot, double mousePressX, double mouseReleaseX) {
		interValMarkerCassis = new InterValMarkerCassis(mousePressX, mouseReleaseX);
		interValMarkerCassis.setPaint(COLOR_SELECTION);
		interValMarkerCassis.setLabelFont(new Font("SansSerif", Font.ITALIC, 11));
		interValMarkerCassis.setLabelAnchor(RectangleAnchor.BOTTOM);
		interValMarkerCassis.setLabelTextAnchor(TextAnchor.BOTTOM_CENTER);
		plot.addDomainMarker(interValMarkerCassis, Layer.BACKGROUND);
		listMarker.add(interValMarkerCassis);

		if (markerView != null) {
			removeAllOtherLabel(interValMarkerCassis);
			interValMarkerCassis.setLabel(computeIntegral());

			double startValue = interValMarkerCassis.getStartValueCassis();
			double endValue = interValMarkerCassis.getEndValueCassis();
			markerView.doGaussianDefaultParametersAction(startValue, endValue);
		}
	}

	private String computeIntegral() {
		if (markerView == null)
			return "";

		List<InterValMarkerCassis> list = getListMarkerDisjoint();

		String returnString = "";
		if (list != null && !list.isEmpty()) {
			StringBuilder sb = new StringBuilder(" ");
			if (markerView.getYAxisCassis().getUnit() == UNIT.UNKNOWN &&
					markerView.getYAxisCassis() instanceof YAxisGeneric) {
				sb.append(((YAxisGeneric)markerView.getYAxisCassis()).getUnitGeneric());
			} else {
				sb.append(markerView.getYAxisCassis().getUnit().getValString());
			}
			sb.append(".");
			sb.append(markerView.getXAxisCassis().getUnit().getValString());
			sb.append(".");

			DecimalFormatSymbols formatSymbol = new DecimalFormatSymbols();
			formatSymbol.setDecimalSeparator('.');
			DecimalFormat integralFormat;
			double integral = markerView.computeIntegral(list);
			double absIntegral = Math.abs(integral);

			if ((absIntegral > 999.999 || absIntegral < 0.001) && integral != 0) {
				integralFormat = new DecimalFormat("0.000E0", formatSymbol);
			} else {
				integralFormat = new DecimalFormat("0.00#", formatSymbol);
			}
			returnString = integralFormat.format(integral) + sb.toString();
		}
		return returnString;
	}

	private void removeAllOtherLabel(IntervalMarker marker) {
		for (Marker markerList : listMarker) {
			if (markerList != marker) {
				markerList.setLabel("");
			}
		}
	}

	/**
	 * @return the list of interval marker disjoint
	 */
	public List<InterValMarkerCassis> getListMarkerDisjoint() {
		List<InterValMarkerCassis> list = new ArrayList<>();

		for (int cpt = 0; cpt < listMarker.size(); cpt++) {
			list.add(new InterValMarkerCassis(listMarker.get(cpt).getStartValue(), listMarker.get(cpt).getEndValue()));
		}

		return makeDisjointMakerlist(list);
	}

	/**
	 * @param list
	 * @return the list of interval maker computed
	 */
	private List<InterValMarkerCassis> makeDisjointMakerlist(List<InterValMarkerCassis> list) {
		InterValMarkerCassis markerNext = null;
		InterValMarkerCassis markerNew = null;
		boolean changed = false;
		if (list.size() < 2)
			return list;
		double min = list.get(0).getStartValueCassis();
		double max = list.get(0).getEndValue();
		InterValMarkerCassis marker = list.get(0);
		for (int cpt = 1; cpt < list.size(); cpt++) {
			markerNext = list.get(cpt);
			double minNext = markerNext.getStartValue();
			double maxNext = markerNext.getEndValue();
			if ((minNext <= min && min <= maxNext) || (minNext <= max && max <= maxNext)
					|| (minNext >= min && maxNext <= max)) {
				markerNew = new InterValMarkerCassis(Math.min(min, minNext), Math.max(max, maxNext));
				changed = true;
				break;
			}
		}

		if (!changed) {
			ArrayList<InterValMarkerCassis> res = new ArrayList<>();
			res.add(marker);
			list.remove(marker);
			res.addAll(makeDisjointMakerlist(list));
			return res;
		}
		else {

			list.remove(marker);
			list.remove(markerNext);
			list.add(markerNew);
			return makeDisjointMakerlist(list);
		}
	}

	/**
	 * Get the last selection done on the plot.
	 *
	 * @return the last selection done on the plot.
	 */
	public InterValMarkerCassis getLastSelection() {
		if (listMarker.isEmpty())
			return null;
		else
			return listMarker.get(listMarker.size() - 1);
	}

	/**
	 * Reset the last selection from the plot.
	 *
	 * @param plot The plot.
	 */
	public void resetLastSelection(XYPlot plot) {
		if (!listMarker.isEmpty()) {
			InterValMarkerCassis marker = listMarker.remove(listMarker.size() - 1);

			plot.removeDomainMarker(marker, Layer.BACKGROUND);
			listMarker.remove(marker);

			if (!listMarker.isEmpty()) {
				marker = getLastSelection();
				removeAllOtherLabel(marker);
				marker.setLabel(computeIntegral());
			}
		}
	}

	/**
	 * Return an InterValMarkerCassis corresponding with user selection on line
	 * fitting mode.
	 *
	 * @return an InterValMarkerCassis
	 */
	public InterValMarkerCassis getGaussianDomain() {
		return interValMarkerCassis;
	}

	/**
	 * Reselt all the selection of the plot.
	 *
	 * @param plot The plot.
	 */
	public void resetAllSelection(XYPlot plot) {
		listMarker.clear();
		plot.clearDomainMarkers();
	}

	/***
	 * Display or not the selection of the plot.
	 *
	 * @param plot The plot.
	 * @param display If the selection should be displayed.
	 */
	public void displaySelection(XYPlot plot, boolean display) {
		plot.clearDomainMarkers();

		if (display) {
			for (Marker marker : listMarker) {
				plot.addDomainMarker(marker, Layer.BACKGROUND);
			}
		}
	}

	/**
	 * Return the list of markers. This is the original list. Some can cover others.
	 * Do not use this list for computation, only for plot.
	 *
	 * @return the list of markers.
	 */
	public List<InterValMarkerCassis> getListMarker() {
		return listMarker;
	}

	/**
	 * Clear the list of markers.
	 */
	public void clearListMarker() {
		listMarker.clear();
	}

	/**
	 * Return if there is marker.
	 *
	 * @return true is there is marker, false otherwise.
	 */
	public boolean haveMarker() {
		if (listMarker != null && !listMarker.isEmpty()) {
			return true;
		}
		return false;
	}
}
