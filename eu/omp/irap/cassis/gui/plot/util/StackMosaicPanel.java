/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.util;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.gui.util.TextFieldFormatFilter;

public class StackMosaicPanel extends JPanel implements ModelListener {

	private static final long serialVersionUID = 1L;
	private JTextField currentSpectrumField;
	private JLabel totalSpectrumLabel;
	private JButton moveLeft;
	private JButton moveRight;
	private StackMosaicModel model;
	private JButton stepMoveLeft;
	private JButton stepMoveRight;
	private JTextField stepValueField;
	private JComboBox<SORTING_PLOT> sortingComboBox;
	private JLabel filterTextLabel;


	public StackMosaicPanel() {
		this(new StackMosaicModel());
	}

	public StackMosaicPanel(StackMosaicModel model) {
		super();
		this.model = model;
		model.addModelListener(this);
		JLabel navigateLabel = new JLabel("Plot number: ");
		getCurrentSpectrumField();
		filterTextLabel = new JLabel("");
		filterTextLabel.setVisible(false);
		JLabel spectrumLabel = new JLabel("/");
		totalSpectrumLabel = new JLabel("1");
		moveLeft = new JButton("<");
		moveRight = new JButton(">");
		setMoveCurrentSpectrum(false);

		JPanel navigatePanel1 = new JPanel();
		navigatePanel1.setLayout(new FlowLayout(FlowLayout.LEFT));
		// add 1st line
		navigatePanel1.add(navigateLabel);
		navigatePanel1.add(moveLeft);
		navigatePanel1.add(currentSpectrumField);
		navigatePanel1.add(filterTextLabel);
		navigatePanel1.add(spectrumLabel);
		navigatePanel1.add(totalSpectrumLabel);
		navigatePanel1.add(moveRight);

		JPanel navigatePanel2 = new JPanel();
		navigatePanel2.setLayout(new FlowLayout(FlowLayout.LEFT));
		// add 2nd line
		JLabel stepLabel = new JLabel("Step: ");
		stepLabel.setPreferredSize(navigateLabel.getPreferredSize());
		stepMoveLeft = new JButton("<");
		stepMoveRight = new JButton(">");
		stepValueField = new JTextField(6);
		stepValueField.addKeyListener(new TextFieldFormatFilter(TextFieldFormatFilter.DECIMAL, "10"));
		stepValueField.setText("2000");
		setCurrentSpectrumEditable(false);
		JLabel stepUnitLabel = new JLabel("MHz");

		navigatePanel2.add(stepLabel);
		navigatePanel2.add(stepMoveLeft);
		navigatePanel2.add(stepValueField);
		navigatePanel2.add(stepUnitLabel);
		navigatePanel2.add(stepMoveRight);

		JPanel tempNavigatePanel = new JPanel(new GridLayout(2, 1));
		tempNavigatePanel.add(navigatePanel1);
		tempNavigatePanel.add(navigatePanel2);

		setLayout(new BorderLayout());
		JPanel panelTemp = new JPanel();
		panelTemp.setBorder(new TitledBorder("Line sorting"));
		panelTemp.add(getSortingComboBox());
		add(panelTemp, BorderLayout.WEST);
		add(tempNavigatePanel, BorderLayout.CENTER);
	}

	public JTextField getCurrentSpectrumField() {
		if (currentSpectrumField == null) {
			currentSpectrumField = new JTextField(4);
			currentSpectrumField.setText("1");
			currentSpectrumField.addKeyListener(new TextFieldFormatFilter(TextFieldFormatFilter.INTEGER, "1"));

		}
		return currentSpectrumField;

	}

	public JComboBox<SORTING_PLOT> getSortingComboBox() {
		if (sortingComboBox == null) {
			sortingComboBox = new JComboBox<>(SORTING_PLOT.values());
			sortingComboBox.setEnabled(true);
			sortingComboBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					StackMosaicPanel.this.model.setLineSorting((SORTING_PLOT) sortingComboBox.getSelectedItem());
				}
			});
		}
		return sortingComboBox;
	}

	public void setMoveCurrentSpectrum(boolean b) {
		moveLeft.setEnabled(b);
		moveRight.setEnabled(b);
		currentSpectrumField.setEditable(b);
	}

	/**
	 * @return the stepValueField
	 */
	public JTextField getStepValueField() {
		return stepValueField;
	}

	public void setCurrentSpectrumEditable(boolean val) {
		stepValueField.setEnabled(val);
		stepMoveLeft.setEnabled(val);
		stepMoveRight.setEnabled(val);
	}

	public int getCurrentItem() {
		return Integer.parseInt(currentSpectrumField.getText());
	}

	public void setCurrentItem(int val) {
		currentSpectrumField.setText(String.valueOf(val));
	}

	public int getNbItems() {
		return Integer.parseInt(totalSpectrumLabel.getText());
	}

	public void setNbItems(int val) {
		totalSpectrumLabel.setText(String.valueOf(val));
	}

	private String getStringWithoutBadChar(String value, boolean reverse) {
		String finalString;
		int nb;
		if (!reverse) {
			nb = 10;
			while (nb >= 1 && (value.charAt(nb-1) == ',' || value.charAt(nb-1) == '-' || value.charAt(nb-1) == ' ')) {
				nb--;
			}
			finalString = new String(value.substring(0, nb));
		} else {
			nb = 0;
			while (nb < value.length() && (value.charAt(nb) == ',' || value.charAt(nb) == '-' || value.charAt(nb) == ' ')) {
				nb++;
			}
			finalString = new String(value.substring(nb, 10));
		}
		return finalString;
	}

	public void setFilterText(String value) {
		String valueAff;
		if (value.length() > 24) {
			//Start
			String valueAffStart = new String(value.substring(0, 10));
			if (valueAffStart.charAt(9) == ',' || valueAffStart.charAt(9) == '-' || valueAffStart.charAt(9) == ' ') {
				valueAffStart = getStringWithoutBadChar(valueAffStart, false);
			} else {
				int i = 10;
				while (i < value.length() && !(value.charAt(i) == ',' || value.charAt(i) == '-' || value.charAt(i) == ' ')) {
					valueAffStart += value.charAt(i);
					i++;
				}
 			}
			//End
			String valueAffEnd = new String(value.substring(value.length() - 10, value.length()));
			if (valueAffEnd.charAt(0) == ',' || valueAffEnd.charAt(0) == '-' || valueAffEnd.charAt(0) == ' ') {
				valueAffEnd = getStringWithoutBadChar(valueAffEnd, true);
			} else {
				int j = value.length() - 11;
				while (j >= 0 && !(value.charAt(j) == ',' || value.charAt(j) == '-' || value.charAt(j) == ' ')) {
					valueAffEnd = value.charAt(j) + valueAffEnd;
					j--;
				}
			}
			valueAff = valueAffStart + " ... " + valueAffEnd;
		} else {
			valueAff = value;
		}

		filterTextLabel.setText(valueAff);
		filterTextLabel.setToolTipText(value);
	}

	public void addMoveRightListener(ActionListener l) {
		moveRight.addActionListener(l);
	}

	public void addMoveLeftListener(ActionListener l) {
		moveLeft.addActionListener(l);
	}

	public void addStepMoveLeftListener(ActionListener l) {
		stepMoveLeft.addActionListener(l);
	}

	public void addStepMoveRightListener(ActionListener l) {
		stepMoveRight.addActionListener(l);
	}

	public void setMoveRightEnabled(boolean val) {
		moveRight.setEnabled(val);
	}

	public void setMoveLeftEnabled(boolean val) {
		moveLeft.setEnabled(val);
	}

	public StackMosaicModel getModel() {
		return model;
	}

	public void refresh() {
		setNbItems(model.getTotalSpectrum());
		setCurrentItem(model.getCurrentSpectrumDisplay());
	}

	public void moveLeft() {
		int pos = model.getLeftPos();
		if (!model.isMovePossible(pos)) {
			return;
		}

		model.setCurrentSpectrum(pos);

		refresh();
	}

	public void move(int pos) {
		if (!model.isMovePossible(pos)) {
			return;
		}
		model.setCurrentSpectrum(pos);
		refresh();
	}

	public void moveRight() {
		int pos = model.getRightPos();
		if (!model.isMovePossible(pos)) {
			return;
		}

		model.setCurrentSpectrum(pos);
		refresh();
	}

	public void setValue(int indiceCurrentSpectrum, int nbTotalSpectrum) {
		model.setCurrentSpectrum(indiceCurrentSpectrum);
		model.setTotalSpectrum(nbTotalSpectrum);

		refresh();
	}

	public void addKeyCurrentSpectrumListener(KeyListener l) {
		currentSpectrumField.addKeyListener(l);
	}

	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (StackMosaicModel.FILTER_TEXT_EVENT.equals(event.getSource())) {
			setFilterText((String) event.getValue());
			if (!"".equals(event.getValue()))
				filterTextLabel.setVisible(true);
			else
				filterTextLabel.setVisible(false);
		}
	}

}
