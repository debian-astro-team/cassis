/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.util;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;

import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.gui.util.JDoubleCassisTextField;

public class VelocityAxisDialog extends JDialog implements
		PropertyChangeListener {

	private static final long serialVersionUID = 1L;
	private double freqRef = 0;

	private JDoubleCassisTextField freqRefTextField;
	private JLabel freqRefOriginTextField;
	private UNIT freqUnit = UNIT.MHZ;
	private JOptionPane optionPane;

	private static final String ENTER_STRING = "Enter";


	/**
	 * Creates the Velocity Axis dialog.
	 *
	 * @param aFrame The owner frame.
	 * @param freqRef The default reference frequency.
	 */
	public VelocityAxisDialog(Frame aFrame, double freqRef) {
		super(aFrame, true);
		this.freqRef = freqRef;
		setTitle("Velocity Axis");
		setContentPane(getOptionPane());
		setModalityType(DEFAULT_MODALITY_TYPE);

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent we) {
				optionPane.setValue(JOptionPane.CLOSED_OPTION);
			}
		});

		// Ensure the text field always gets the first focus.
		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentShown(ComponentEvent ce) {
				freqRefTextField.requestFocusInWindow();
			}
		});

		// Register an event handler that reacts to option pane state changes.
		optionPane.addPropertyChangeListener(this);
		setSize(420, 200);
	}

	public JOptionPane getOptionPane() {
		if (optionPane == null) {
			JPanel panelFreqRef = new JPanel();
			panelFreqRef.add(new JLabel("Freq Ref : "));
			panelFreqRef.add(getFreqUnitComboBox());
			panelFreqRef.add(getFreqRefTextField());
			panelFreqRef.add(getFreqOriginTextField());

			Object[] array = { "Enter the velocity parameters :", panelFreqRef };
			Object[] options = { ENTER_STRING, "Cancel" };
			optionPane = new JOptionPane(array, JOptionPane.QUESTION_MESSAGE,
					JOptionPane.YES_NO_OPTION, null, options, options[0]);
		}
		return optionPane;
	}

	public JLabel getFreqOriginTextField() {
		if(freqRefOriginTextField == null) {
			freqRefOriginTextField = new JLabel(new DecimalFormat("#####0.0##").format(freqRef));

		}
		return freqRefOriginTextField;
	}

	public JDoubleCassisTextField getFreqRefTextField() {
		if(freqRefTextField == null) {
			freqRefTextField = new JDoubleCassisTextField(8, freqRef);
			freqRefTextField.setValMax(1E6);
		}
		return freqRefTextField;
	}

	public JComboBox<UNIT> getFreqUnitComboBox() {
		final JComboBox<UNIT> freqUnitComboBox = new JComboBox<>(new UNIT[] { UNIT.GHZ,
				UNIT.MHZ });
		freqUnitComboBox.setSelectedItem(freqUnit);
		freqUnitComboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				freqUnit = (UNIT) freqUnitComboBox.getSelectedItem();
			}
		});
		return freqUnitComboBox;
	}

	/** This method reacts to state changes in the option pane. */
	@Override
	public void propertyChange(PropertyChangeEvent e) {
		String prop = e.getPropertyName();

		if (isVisible()
				&& e.getSource() == optionPane
				&& (JOptionPane.VALUE_PROPERTY.equals(prop) || JOptionPane.INPUT_VALUE_PROPERTY
						.equals(prop))) {
			Object value = optionPane.getValue();

			if (ENTER_STRING.equals(value)) {
				freqRef = ((Number) freqRefTextField.getValue()).doubleValue();
			}
			dispose();
		}
	}

	/**
	 * Return the double value of the string as the user entered it.
	 *  If the typed string was invalid return null.
	 *
	 * @return the double value of the string as the user entered it.
	 *  If the typed string was invalid return null.
	 */
	public double getFreqRef() {
		return XAxisCassis.getXAxisCassis(freqUnit).convertToMHzFreq(freqRef);
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setSize(100, 100);
		VelocityAxisDialog velocityAxisDialog = new VelocityAxisDialog(frame,
				115000.345);

		velocityAxisDialog.setSize(420, 200);
		velocityAxisDialog.setVisible(true);
	}
}
