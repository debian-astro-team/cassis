/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.util;

import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.ButtonGroup;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.gui.util.JDoubleCassisTextField;

public class JyToKAxisDialog extends JDialog implements
		PropertyChangeListener {

	private static final long serialVersionUID = 1L;
	private double beamMajorValue;
	private double beamMinorValue;
	private JDoubleCassisTextField beamMajorValueTextField;
	private JDoubleCassisTextField beamMinorValueTextField;
	private JOptionPane optionPane;
	private JRadioButton ellipticalRegionRadioButton;
	private JRadioButton gaussianRegionRadioButton;
	private String typeRegion = CassisMetadata.GAUSSIAN_REGION;


	/**
	 * Creates the JyToK Axis dialog.
	 *
	 * @param aFrame The owner frame.
	 * @param beamMinor The beamMinor.
	 * @param beamMajor The beamMajor.
	 */
	public JyToKAxisDialog(Frame aFrame, double beamMinor, double beamMajor) {
		super(aFrame, true);
		this.beamMinorValue = beamMinor;
		this.beamMajorValue = beamMajor;
		setTitle("Flux density [Jy] <-> T [K]");
		setContentPane(getOptionPane());
		setModalityType(DEFAULT_MODALITY_TYPE);

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent we) {
				optionPane.setValue(JOptionPane.CLOSED_OPTION);
			}
		});

		// Ensure the text field always gets the first focus.
		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentShown(ComponentEvent ce) {
				beamMajorValueTextField.requestFocusInWindow();
			}
		});

		// Register an event handler that reacts to option pane state changes.
		optionPane.addPropertyChangeListener(this);
		setSize(520, 300);
	}

	public JOptionPane getOptionPane() {
		if (optionPane == null) {
			JPanel conversionParametersPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
			JPanel minParamPanel = new JPanel();
			minParamPanel.add(new JLabel("Beam major: "));
			minParamPanel.add(getBeamMinorValueTextField());
			minParamPanel.add(new JLabel("arcsec"));

			JPanel maxParamPanel = new JPanel();
			maxParamPanel.add(new JLabel("Beam minor: "));
			maxParamPanel.add(getBeamMajorValueTextField());
			maxParamPanel.add(new JLabel("arcsec"));

			JPanel regionTypePanel = new JPanel(new GridLayout(2, 1));
			ButtonGroup buttonGroup = new ButtonGroup();
			buttonGroup.add(getGaussianRegionRadioButton());
			buttonGroup.add(getEllipticalRegionRadioButton());
			regionTypePanel.add(getGaussianRegionRadioButton());
			regionTypePanel.add(getEllipticalRegionRadioButton());
			conversionParametersPanel.add(minParamPanel);
			conversionParametersPanel.add(maxParamPanel);
			conversionParametersPanel.add(regionTypePanel);

			Object[] array = { "Please enter the major and minor axes of the region \nover which the flux density was obtained, \n "
					+ "and choose the appropriate type:", conversionParametersPanel };
			optionPane = new JOptionPane(array, JOptionPane.QUESTION_MESSAGE,
					JOptionPane.OK_CANCEL_OPTION);
		}
		return optionPane;
	}

	private JRadioButton getEllipticalRegionRadioButton() {
		if (ellipticalRegionRadioButton == null) {
			ellipticalRegionRadioButton = new JRadioButton("elliptical/circular region (Ω =  π * (Θ_maj/2) * (Θ_min/2) )");
		}
		return ellipticalRegionRadioButton;
	}

	private JRadioButton getGaussianRegionRadioButton() {
		if (gaussianRegionRadioButton == null) {
			gaussianRegionRadioButton = new JRadioButton("gaussian region (Ω = π * Θ_maj * Θ_min / (4*ln 2)");
			gaussianRegionRadioButton.setSelected(true);
		}
		return gaussianRegionRadioButton;
	}

	public JDoubleCassisTextField getBeamMajorValueTextField() {
		if(beamMajorValueTextField == null) {
			beamMajorValueTextField = new JDoubleCassisTextField(8, beamMajorValue);
		}
		return beamMajorValueTextField;
	}

	public JDoubleCassisTextField getBeamMinorValueTextField() {
		if(beamMinorValueTextField == null) {
			beamMinorValueTextField = new JDoubleCassisTextField(8, beamMinorValue);
		}
		return beamMinorValueTextField;
	}


	public double getBeamMajorValue() {
		return beamMajorValue;
	}

	public double getBeamMinorValue() {
		return beamMinorValue;
	}

	public String getTypeRegion() {
		return typeRegion;
	}

	/** This method reacts to state changes in the option pane. */
	@Override
	public void propertyChange(PropertyChangeEvent e) {
		String prop = e.getPropertyName();

		if (isVisible()
				&& e.getSource() == optionPane
				&& (JOptionPane.VALUE_PROPERTY.equals(prop) || JOptionPane.INPUT_VALUE_PROPERTY
						.equals(prop))) {
			int value = (Integer)optionPane.getValue();

			if (JOptionPane.OK_OPTION == value) {
				beamMajorValue = ((Number) beamMajorValueTextField.getValue()).doubleValue();
				beamMinorValue = ((Number) beamMinorValueTextField.getValue()).doubleValue();
				if (getGaussianRegionRadioButton().isSelected()) {
					typeRegion = CassisMetadata.GAUSSIAN_REGION;
				} else {
					typeRegion = CassisMetadata.ELLIPTICAL_REGION;
				}

			}
			dispose();
		}
	}

	public static void main(String[] args) {
		JyToKAxisDialog velocityAxisDialog = new JyToKAxisDialog(null, 0,0);
		velocityAxisDialog.setSize(520, 300);
		velocityAxisDialog.setVisible(true);
	}
}
