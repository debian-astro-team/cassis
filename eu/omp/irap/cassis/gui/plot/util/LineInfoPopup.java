/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.util.Iterator;
import java.util.List;

import javax.swing.JPopupMenu;
import javax.swing.JTextArea;

/**
 * That class display a line of information on the chart.
 *
 * @author girard
 */
public class LineInfoPopup {

	private final JPopupMenu popup;


	/**
	 * Constructor makes a new LineInfoPopup invisible.
	 */
	public LineInfoPopup() {
		super();
		popup = new JPopupMenu();
	}

	/**
	 * Display the LineInfoPopup when MouseEvent occurs.
	 *
	 * @param event
	 *            MouseEvent
	 * @param menuList
	 *            Menu to display
	 */
	public void display(final MouseEvent event, final List<String> menuList) {
		String menuLabel;

		final Iterator<String> menuIterator = menuList.iterator();
		popup.removeAll();

		if (!menuList.isEmpty() && menuList.get(0) != null) {
			// add information of the menu in the line
			while (menuIterator.hasNext()) {
				menuLabel = menuIterator.next().toString();
				if (!"".equals(menuLabel)) {
					addMenuItem(menuLabel);
				}
			}

			// check if there is some line to display
			if (popup.getComponentCount() > 0) {
				// display the line where we have clicked.
				popup.show(event.getComponent(), event.getX() + 5, event.getY());
				popup.setVisible(false);
				popup.setVisible(true);
				popup.repaint();
			}
		}
	}

	/**
	 * Display the LineInfoPopup when MouseEvent occurs.
	 *
	 * @param event MouseEvent
	 * @param msg The message to display
	 */
	public void display(final MouseEvent event, String msg) {
		popup.removeAll();

		JTextArea comp = addMenuItem(msg);
		comp.setMargin(new Insets(2, 3, 2, 5));
		// display the line where we have clicked.
		popup.show(event.getComponent(), event.getX() + 5, event.getY());
		popup.repaint();
	}

	/**
	 * Add information in the line.
	 *
	 * @param menuLabel info
	 * @return the added item.
	 */
	private JTextArea addMenuItem(final String menuLabel) {
		JTextArea item = new JTextArea();
		item.setText(menuLabel);
		item.setBackground(new Color(220, 220, 220));

		item.setFont(new Font("Dialog", Font.BOLD, 12));
		popup.add(item);
		return item;
	}

	/**
	 * Hide the popup.
	 */
	public void hide() {
		popup.removeAll();
		popup.setVisible(false);
	}
}
