/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.util;

import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.gui.util.TextFieldFormatFilter;

/**
 * @author glorian
 *
 */
public class ChartMovePanel extends JPanel {

	private static final long serialVersionUID = -717482094315669291L;
	private static final Logger LOGGER = LoggerFactory.getLogger(ChartMovePanel.class);
	private static final long TIME_DELAY = 100;

	private JButton moveLeft;
	private JButton moveRight;
	private JButton zoomPlusButton;
	private JButton zoomMinusButton;
	private JTextField goFreqField;
	private JButton goButton;
	private JTextField rangeField;
	private JButton rangeButton;
	private ChartPanel chartPanel;
	private ValueAxis chartAxis;
	private ToolsState toolsState;
	private boolean isLeftReleased = false;
	private boolean isRightReleased = false;


	public ChartMovePanel(ChartPanel chartPanel, ValueAxis chartAxis, ToolsState toolState) {
		this.chartPanel = chartPanel;
		this.chartAxis = chartAxis;
		this.toolsState = toolState;
		JPanel mvPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		mvPanel.add(getShiftPanel());
		mvPanel.add(getZoomPanel());
		mvPanel.add(getSearchPanel());
		mvPanel.add(getRangePanel());

		setLayout(new FlowLayout(FlowLayout.LEFT));
		add(mvPanel);
	}

	private JPanel getShiftPanel() {
		// Create move left and tight button
		JPanel shiftPanel = new JPanel();
		shiftPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
		shiftPanel.setBorder(createBorder("Shift"));
		shiftPanel.add(getMoveLeftButton());
		shiftPanel.add(getMoveRightButton());
		return shiftPanel;
	}

	/**
	 * Create if necessary then return the move right {@link JButton}.
	 *
	 * @return the move right {@link JButton}.
	 */
	private JButton getMoveRightButton() {
		if (moveRight == null) {
			moveRight = new JButton(">>");
			Insets insetsButton = new Insets(0, 1, 0, 1);
			moveRight.setMargin(insetsButton);

			moveRight.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent e) {
					isRightReleased = true;
				}

				@Override
				public void mousePressed(MouseEvent e) {
					moveRightPressed();
				}
			});
		}
		return moveRight;
	}

	/**
	 * Create if necessary then return the move left {@link JButton}.
	 *
	 * @return the move left {@link JButton}.
	 */
	private JButton getMoveLeftButton() {
		if (moveLeft == null) {
			moveLeft = new JButton("<<");
			Insets insetsButton = new Insets(0, 1, 0, 1);
			moveLeft.setMargin(insetsButton);

			moveLeft.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent e) {
					isLeftReleased = true;
				}

				@Override
				public void mousePressed(MouseEvent e) {
					moveLeftPressed();
				}
			});
		}
		return moveLeft;
	}

	/**
	 * Handle the action of move left pressed.
	 */
	private void moveLeftPressed() {
		isLeftReleased = false;
		new Thread(new Runnable() {
			@Override
			public void run() {
				do {
					onMoveLeftButton();
					try {
						Thread.sleep(TIME_DELAY);
					} catch (InterruptedException ie) {
						LOGGER.warn("Error during sleep", ie);
					}
				} while (!isLeftReleased);
			}
		}).start();
	}

	/**
	 * Handle the action of move right pressed.
	 */
	private void moveRightPressed() {
		isRightReleased = false;
		new Thread(new Runnable() {
			@Override
			public void run() {
				do {
					onMoveRightButton();
					try {
						Thread.sleep(TIME_DELAY);
					} catch (InterruptedException ie) {
						LOGGER.warn("Error during sleep", ie);
					}
				} while (!isRightReleased);
			}
		}).start();
	}

	private JPanel getZoomPanel() {
		// Create zoom in and out button
		JPanel zoomPanel = new JPanel();
		zoomPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
		zoomPanel.setBorder(createBorder("Zoom"));
		zoomPlusButton = new JButton("+");
		Insets insetsButton = new Insets(0, 6, 0, 6);
		zoomPlusButton.setMargin(insetsButton);
		zoomPlusButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				onZoomPlusButton();
				changeRange();
			}
		});
		zoomMinusButton = new JButton("-");
		Insets insets2Button = new Insets(0, 8, 0, 8);
		zoomMinusButton.setMargin(insets2Button);
		zoomMinusButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				onZoomMinusButton();
				changeRange();
			}
		});

		zoomPanel.add(zoomPlusButton);
		zoomPanel.add(zoomMinusButton);
		return zoomPanel;
	}

	private JPanel getSearchPanel() {
		// Create search Frequency
		JPanel searchPanel = new JPanel();
		searchPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
		searchPanel.setBorder(createBorder("Search"));
		goFreqField = new JTextField("0", 4);
		goFreqField.setHorizontalAlignment(JTextField.CENTER);
		goFreqField.addKeyListener(
				new TextFieldFormatFilter(TextFieldFormatFilter.DECIMAL, TextFieldFormatFilter.ALL));
		goFreqField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent key) {
				if (key.getKeyCode() == KeyEvent.VK_ENTER)
					onGoButton();
			}
		});
		goButton = new JButton("Go");
		Insets insetsButton = new Insets(0, 1, 0, 1);
		goButton.setMargin(insetsButton);
		goButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				onGoButton();
			}
		});
		searchPanel.add(goFreqField);
		searchPanel.add(goButton);
		return searchPanel;
	}

	private JPanel getRangePanel() {
		// Create Range label
		JPanel rangePanel = new JPanel();
		rangePanel.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
		rangePanel.setBorder(createBorder("Range"));

		// create range field
		rangeField = new JTextField(4);
		rangeField.setHorizontalAlignment(JTextField.CENTER);
		rangeField.setText("1.5");

		rangeField.addKeyListener(new TextFieldFormatFilter(TextFieldFormatFilter.DECIMAL, "1"));
		rangeField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent key) {
				if (key.getKeyCode() == KeyEvent.VK_ENTER)
					onRangeButton();
			}
		});
		rangeButton = new JButton("Set");
		Insets insetsButton = new Insets(0, 1, 0, 1);
		rangeButton.setMargin(insetsButton);
		rangeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				onRangeButton();
			}
		});
		rangePanel.add(rangeField);
		rangePanel.add(rangeButton);
		return rangePanel;
	}

	private Border createBorder(String name) {
		return BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), name),
				BorderFactory.createEmptyBorder());
	}

	public void setRange(double val) {
		rangeField.setText(Double.toString(val));
	}

	public void setSearch(double val) {
		goFreqField.setText(Double.toString(val));
	}

	/**
	 * Go to the value entered in the search label.
	 */
	private void onGoButton() {
		if (chartAxis == null) {
			return;
		}
		// User wants to go to the specified value
		final double freqToGo = Double.parseDouble(goFreqField.getText());

		if (ToolsState.X.equals(toolsState)) {
			final ValueAxis zoomAxis = chartAxis;
			final double rangeLength = zoomAxis.getRange().getLength();
			// Zoom on specified value
			zoomAxis.setRangeAboutValue(freqToGo, rangeLength);
			NumberAxis xAxis2 = (NumberAxis)
					chartPanel.getChart().getXYPlot().getDomainAxis(1);
			xAxis2.setRangeAboutValue(freqToGo, rangeLength);
			// place cursor on specified value
			chartPanel.getChart().getXYPlot().setDomainCrosshairValue(freqToGo);
		}
		else if (ToolsState.Y.equals(toolsState)) {
			final ValueAxis zoomAxis = chartAxis;
			final double rangeLength = zoomAxis.getRange().getLength();
			// Zoom on specified value
			zoomAxis.setRangeAboutValue(freqToGo, rangeLength);
			NumberAxis xAxis2= (NumberAxis)
					chartPanel.getChart().getXYPlot().getRangeAxis(1);
			xAxis2.setRangeAboutValue(freqToGo, rangeLength);
			// place cursor on specified value
			chartPanel.getChart().getXYPlot().setRangeCrosshairValue(freqToGo);
		}
	}

	/**
	 * Move the plot to the right.
	 */
	private void onMoveRightButton() {
		if (chartAxis == null)
			return;

		// User wants to move on right
		final double range = chartAxis.getRange().getLength();
		if (ToolsState.X.equals(toolsState)) {
			chartAxis.setRange(chartAxis.getLowerBound() + range / 3, chartAxis.getUpperBound() + range / 3);

			NumberAxis xAxis2 = (NumberAxis) chartPanel.getChart().getXYPlot().getDomainAxis(1);
			final double range2 = xAxis2.getRange().getLength();
			xAxis2.setRange(xAxis2.getLowerBound() + range2 / 3, xAxis2.getUpperBound() + range2 / 3);
		}
		else {
			chartAxis.setRange(chartAxis.getLowerBound() + range / 3, chartAxis.getUpperBound() + range / 3);

			NumberAxis xAxis2 = (NumberAxis) chartPanel.getChart().getXYPlot().getRangeAxis(1);
			final double range2 = xAxis2.getRange().getLength();
			xAxis2.setRange(xAxis2.getLowerBound() + range2 / 3, xAxis2.getUpperBound() + range2 / 3);
		}
	}

	/**
	 * Move the plot to the left.
	 */
	private void onMoveLeftButton() {
		if (chartAxis == null)
			return;

		// User wants to move on left
		final double range = chartAxis.getRange().getLength();
		if (ToolsState.X.equals(toolsState)) {
			chartAxis.setRange(chartAxis.getLowerBound() - range / 3, chartAxis.getUpperBound() - range / 3);
			NumberAxis xAxis2 = (NumberAxis) chartPanel.getChart().getXYPlot().getDomainAxis(1);
			final double range2 = xAxis2.getRange().getLength();
			xAxis2.setRange(xAxis2.getLowerBound() - range2 / 3, xAxis2.getUpperBound() - range2 / 3);
		}
		else {
			chartAxis.setRange(chartAxis.getLowerBound() - range / 3, chartAxis.getUpperBound() - range / 3);
			NumberAxis xAxis2 = (NumberAxis) chartPanel.getChart().getXYPlot().getRangeAxis(1);
			final double range2 = xAxis2.getRange().getLength();
			xAxis2.setRange(xAxis2.getLowerBound() - range2 / 3, xAxis2.getUpperBound() - range2 / 3);
		}
	}

	/**
	 * Zoom minus on the plot.
	 */
	private void onZoomMinusButton() {
		if (chartAxis == null)
			return;

		// User wants to zoom out
		if (ToolsState.X.equals(toolsState)) {
			chartAxis.resizeRange(1.5);
//			chartPanel.getChart().getXYPlot().setDomainAxis(chartAxis);
			NumberAxis xAxis2 = (NumberAxis) chartPanel.getChart().getXYPlot().getDomainAxis(1);
			xAxis2.resizeRange(1.5);
//			chartPanel.getChart().getXYPlot().setDomainAxis(1, xAxis2);
		}
		else {
			chartAxis.resizeRange(1.5);
//			chartPanel.getChart().getXYPlot().setRangeAxis(chartAxis);
			NumberAxis xAxis2 = (NumberAxis) chartPanel.getChart().getXYPlot().getRangeAxis(1);
			xAxis2.resizeRange(1.5);
//			chartPanel.getChart().getXYPlot().setRangeAxis(1, xAxis2);
		}
	}

	/**
	 * Zoom plus on the plot.
	 */
	private void onZoomPlusButton() {
		if (chartAxis == null)
			return;

		// User wants to zoom in
		if (ToolsState.X.equals(toolsState)) {
			chartAxis.resizeRange(0.5);
			NumberAxis xAxis2 = (NumberAxis) chartPanel.getChart().getXYPlot().getDomainAxis(1);
			xAxis2.resizeRange(0.5);
		}
		else {
			chartAxis.resizeRange(0.5);
			NumberAxis xAxis2 = (NumberAxis) chartPanel.getChart().getXYPlot().getRangeAxis(1);
			xAxis2.resizeRange(0.5);
		}
	}

	/**
	 * Change the range value of the plot.
	 */
	private void onRangeButton() {
		if (chartAxis == null)
			return;

		if (ToolsState.X.equals(toolsState)) {
			// User wants a 500Mhz range
			final double centralValue = chartAxis.getRange().getCentralValue();
			final double rangeValue = Double.parseDouble(rangeField.getText());
			final double rangeLength = rangeValue + rangeValue / 10.;
			chartAxis.setRangeAboutValue(centralValue, rangeLength);
			NumberAxis xAxis2 = (NumberAxis) chartPanel.getChart().getXYPlot().getDomainAxis(1);
			final double centralValue2 = xAxis2.getRange().getCentralValue();
			final double rangeValue2 = Double.parseDouble(rangeField.getText());
			final double rangeLength2 = rangeValue2 + rangeValue2 / 10.;
			xAxis2.setRangeAboutValue(centralValue2, rangeLength2);
		}
		else {
			// User wants a 500Mhz range
			final double centralValue = chartAxis.getRange().getCentralValue();
			final double rangeValue = Double.parseDouble(rangeField.getText());
			final double rangeLength = rangeValue + rangeValue / 10.;
			chartAxis.setRangeAboutValue(centralValue, rangeLength);
			NumberAxis xAxis2 = (NumberAxis) chartPanel.getChart().getXYPlot().getRangeAxis(1);
			final double centralValue2 = xAxis2.getRange().getCentralValue();
			final double rangeValue2 = Double.parseDouble(rangeField.getText());
			final double rangeLength2 = rangeValue2 + rangeValue2 / 10.;
			xAxis2.setRangeAboutValue(centralValue2, rangeLength2);
		}
	}

	public void setChartAxis(ValueAxis chartAxis) {
		this.chartAxis = chartAxis;
	}

	public void setChartPanel(ChartPanel chartPanel) {
		this.chartPanel = chartPanel;
	}

	public double getUpperBoundDomain() {
		return this.chartPanel.getChart().getXYPlot().getDomainAxis().getUpperBound();
	}

	public double getLowerBoundDomain() {
		return this.chartPanel.getChart().getXYPlot().getDomainAxis().getLowerBound();
	}

	public double getUpperBoundRange() {
		return this.chartPanel.getChart().getXYPlot().getRangeAxis().getUpperBound();
	}

	public double getLowerBoundRange() {
		return this.chartPanel.getChart().getXYPlot().getRangeAxis().getLowerBound();
	}

	private void changeRange() {
		if (chartAxis == null)
			return;

		if (ToolsState.X.equals(toolsState)) {
			double range = this.getUpperBoundDomain() - this.getLowerBoundDomain();
			this.setRange(arrondi(range, 2));
		}
		else {
			double range = this.getUpperBoundRange() - this.getLowerBoundRange();
			this.setRange(arrondi(range, 2));
		}
	}

	public static double arrondi(Double val, int decimale) {
		DecimalFormatSymbols ds = new DecimalFormatSymbols();
		ds.setDecimalSeparator('.');
		DecimalFormat format;
		switch (decimale) {
			case 1:
				format = new DecimalFormat("###0.0", ds);
				break;
			case 2:
				format = new DecimalFormat("###0.00", ds);
				break;
			case 3:
				format = new DecimalFormat("###0.000", ds);
				break;
			default:
				format = new DecimalFormat("###0.000", ds);
				break;
		}
		return Double.valueOf(format.format(val));
	}
}
