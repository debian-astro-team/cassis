/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.util;

import java.util.ArrayList;
import java.util.List;

import eu.omp.irap.cassis.common.events.ListenerManager;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;

public class StackMosaicModel extends ListenerManager {

	public static final String CURRENT_SPECTRUM_EVENT = "currentSpectrum";
	public static final String FILTER_TEXT_EVENT = "filterText";
	public static final String LINE_SORTING_EVENT = "lineSorting";

	private int currentSpectrum = 0;
	private int totalSpectrum = 1;
	private Double stepValue = 2.;
	private SORTING_PLOT lineSorting;
	private List<Integer> filter = new ArrayList<>();
	private String filterText = "";


	public StackMosaicModel() {
		super();
		filter.add(0);
	}

	/**
	 * @return the stepValue
	 */
	public Double getStepValue() {
		return stepValue;
	}

	/**
	 * @param stepValue
	 *            the stepValue to set
	 */
	public void setStepValue(Double stepValue) {
		this.stepValue = stepValue;
	}

	/**
	 * Gets the current spectrum value.
	 *
	 * @return The current spectrum value
	 */
	public int getCurrentSpectrum() {
		return currentSpectrum;
	}

	/**
	 * Gets the total spectrum value.
	 *
	 * @return The total spectrum value
	 */
	public int getTotalSpectrum() {
		return totalSpectrum;
	}

	/**
	 * Gets the current spectrum index.
	 *
	 * @return The current spectrum index
	 */
	public int getCurrentSpectrumDisplay() {
		return currentSpectrum + 1;
	}

	/**
	 * Sets the total spectrum index.
	 *
	 * @param inTotaltSpectrum
	 *            The total spectrum index
	 */
	public void setTotalSpectrum(int inTotaltSpectrum) {
		totalSpectrum = inTotaltSpectrum;
	}

	public void setCurrentSpectrum(int currentSpectrum) {
		this.currentSpectrum = currentSpectrum;
		fireDataChanged(new ModelChangedEvent(CURRENT_SPECTRUM_EVENT, this.currentSpectrum));
	}

	public void setLineSorting(SORTING_PLOT selectedItem) {
		this.lineSorting = selectedItem;
		fireDataChanged(new ModelChangedEvent(LINE_SORTING_EVENT, selectedItem));
	}

	public SORTING_PLOT getLineSorting() {
		return lineSorting;
	}

	public List<Integer> getFilter() {
		return filter;
	}

	public void setFilter(List<Integer> filter) {
		this.filter = filter;
	}

	public String getFilterText() {
		return filterText;
	}

	public void setFilterText(String filterText) {
		String string = "1-" + getTotalSpectrum();
		// All plots, so don't display the range.
		if (string.equals(filterText) || "*".equals(filterText))
			this.filterText = "";
		else
			this.filterText = "[" + filterText + "]";
		fireDataChanged(new ModelChangedEvent(FILTER_TEXT_EVENT, this.filterText));
	}

	public Boolean isMovePossible(int nb) {
		return filter.contains(nb);
	}

	public int getRightPos() {
		int pos = filter.indexOf(currentSpectrum);
		// Error.
		if (pos == -1)
			return -1;
		// Last one, go to the first.
		if (pos == filter.size() - 1)
			return filter.get(0);

		return filter.get(pos + 1);
	}

	public int getLeftPos() {
		int pos = filter.indexOf(currentSpectrum);
		// Error.
		if (pos == -1)
			return -1;
		// First, go to the last one.
		if (pos == 0)
			return filter.get(filter.size() - 1);

		return filter.get(pos - 1);
	}
}
