/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.util;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;

/**
 * This class is a simple utility class for printing.
 *
 * @author M. Boiziot
 */
public abstract class PrintUtil {

	/**
	 * Utility class... Must not be instancied.
	 */
	private PrintUtil() {
	}

	/**
	 * Print a BufferedImage.
	 *
     * @param graphics the context into which the page is drawn
     * @param pageFormat the size and orientation of the page being drawn
     * @param pageIndex the zero based index of the page to be drawn
     * @param image The BufferedImage to print.
     * @return Printable.PAGE_EXISTS if the page is rendered successfully
     *         or Printable.NO_SUCH_PAGE if <code>pageIndex</code> specifies a
     *         non-existent page.
	 */
	public static int print(Graphics graphics, PageFormat pageFormat, int pageIndex,
			BufferedImage image) {
		if (pageIndex != 0) {
			return Printable.NO_SUCH_PAGE;
		}

		double pageHeight = pageFormat.getImageableHeight();
		double pageWidth = pageFormat.getImageableWidth();
		Graphics2D g2d = (Graphics2D) graphics;
		g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
		if (pageHeight < image.getHeight() || pageWidth < image.getWidth()) {
			g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
					RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			int textSize = (int) (pageHeight - image.getHeight() * pageWidth / image.getWidth());
			g2d.drawImage(image, 0, 0, (int) pageWidth, (int) pageHeight - textSize, null);
		} else {
			g2d.drawImage(image, 0, 0, null);
		}
		g2d.dispose();

		return Printable.PAGE_EXISTS;
	}
}
