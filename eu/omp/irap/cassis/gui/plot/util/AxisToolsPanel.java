/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.util;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.axis.ValueAxis;

public class AxisToolsPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private ChartPanel chartPanel;
	private ValueAxis chartAxis;
	private ChartMovePanel chartMovePanel;
	private JComboBox<Object> comboBottomAxis;
	private JComboBox<Object> comboTopAxis;
	private JCheckBox logCheckBox;


	public AxisToolsPanel(ChartPanel chartPanel, ValueAxis chartAxis, Object[] listValAxis, String labelTop,
			String labelBottom, ToolsState toolState) {
		super();
		buildAxisToolsPanel(chartPanel, chartAxis, listValAxis, labelTop, labelBottom, toolState);
	}

	private void buildAxisToolsPanel(ChartPanel chartPanel, ValueAxis chartAxis, Object[] listValAxis, String labelTop,
			String labelBottom, ToolsState toolState) {
		this.chartPanel = chartPanel;
		this.chartAxis = chartAxis;
		chartMovePanel = new ChartMovePanel(chartPanel, chartAxis, toolState);
		GridBagLayout gblayout = new GridBagLayout();
		setLayout(gblayout);
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.CENTER;
		c.gridwidth = 4;
		gblayout.setConstraints(chartMovePanel, c);
		add(chartMovePanel);
		c.gridwidth = 1;
		JPanel temp = new JPanel();
		temp.setLayout(new GridLayout(2, 1));
		temp.add(buildxTopPanel(labelTop, listValAxis));
		temp.add(buildxBottomPanel(labelBottom, listValAxis));
		gblayout.setConstraints(temp, c);
		add(temp);

		JPanel temp2 = new JPanel();
		temp2.setLayout(new BoxLayout(temp2, BoxLayout.Y_AXIS));
		logCheckBox = new JCheckBox("");
		logCheckBox.setEnabled(false);
		temp2.add(new JLabel(" Log"));
		temp2.add(logCheckBox);
		gblayout.setConstraints(temp2, c);
		add(temp2);
	}

	/**
	 * Build the north xTopPanel.
	 *
	 * @return the north xTopPanel.
	 */
	private JPanel buildxTopPanel(String label, Object[] listXAxis) {
		comboTopAxis = new JComboBox<>(listXAxis);
		return buildAxisPanel(label, comboTopAxis);
	}

	/**
	 * Build the north xBottomPanel.
	 *
	 * @return the north xBottomPanel.
	 */
	private JPanel buildxBottomPanel(String label, Object[] listXAxis) {
		comboBottomAxis = new JComboBox<>(listXAxis);
		return buildAxisPanel(label, comboBottomAxis);
	}

	private JPanel buildAxisPanel(String label, JComboBox<Object> comboBox) {
		JLabel topLabel = new JLabel(label);

		JPanel topPanel = new JPanel();
		topPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

		topPanel.add(topLabel);
		topPanel.add(comboBox);
		topPanel.setEnabled(false);
		comboBox.setEnabled(false);
		comboBox.setSize(150, 24);
		comboBox.setPreferredSize(new Dimension(150, 24));

		return topPanel;
	}

	public ValueAxis getChartAxis() {
		return chartAxis;
	}

	public void setChartAxis(ValueAxis chartAxis) {
		this.chartAxis = chartAxis;
	}

	public ChartMovePanel getChartMovePanel() {
		return chartMovePanel;
	}

	public ChartPanel getChartPanel() {
		return chartPanel;
	}

	public void setChartPanel(ChartPanel chartPanel) {
		this.chartPanel = chartPanel;
	}

	public JComboBox<Object> getComboBottomAxis() {
		return comboBottomAxis;
	}

	public JComboBox<Object> getComboTopAxis() {
		return comboTopAxis;
	}

	public JCheckBox getLogCheckBox() {
		return logCheckBox;
	}
}
