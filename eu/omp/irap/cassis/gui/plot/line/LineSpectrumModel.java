/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.line;

import java.util.ArrayList;
import java.util.List;

import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.XAxisVelocity;
import eu.omp.irap.cassis.gui.fit.advanced.ModelFitManager;
import eu.omp.irap.cassis.gui.model.CassisResult;
import eu.omp.irap.cassis.gui.model.parameter.UtilDatabase;
import eu.omp.irap.cassis.gui.plot.abstractmozaicplot.AbstractMozaicPlotModel;
import eu.omp.irap.cassis.gui.plot.curve.JPanelCurve;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import eu.omp.irap.cassis.gui.plot.gallery.GalleryNavigationModel;
import eu.omp.irap.cassis.gui.plot.infopanel.InfoModel;
import eu.omp.irap.cassis.gui.plot.infopanel.InfoPanelConstants;
import eu.omp.irap.cassis.gui.plot.infopanel.LineConfigCurve;
import eu.omp.irap.cassis.gui.plot.tools.ToolsModel;
import eu.omp.irap.cassis.gui.plot.tools.ViewType;
import eu.omp.irap.cassis.gui.plot.util.SORTING_PLOT;
import eu.omp.irap.cassis.lineanalysis.LineAnalysisResult;

public class LineSpectrumModel extends AbstractMozaicPlotModel {

	private LineAnalysisResult lineResult;


	public LineSpectrumModel() {
		super();
		galleryNavigationModel = new GalleryNavigationModel();
		fitModelManager = new ModelFitManager(galleryNavigationModel);
	}

	@Override
	public void setCassisPlots(CassisResult result, LineConfigCurve lineConfigCurve) {
		if (result instanceof LineAnalysisResult) {
			LineAnalysisResult lineAnalysisResult = (LineAnalysisResult) result;
			setLineAnalysisResult(lineAnalysisResult, lineConfigCurve);
		}
	}

	public void setLineAnalysisResult(LineAnalysisResult lineAnalysisResult, LineConfigCurve lineConfigCurve) {
		listCassisPlots.clear();
		listToolsModels.clear();
		toolsMosaicModel.resetId();
		clearFitModels();
		lineResult = lineAnalysisResult;
		this.telescope = lineAnalysisResult.getTelescope();

		int nbPlot = lineAnalysisResult.getNbPlots();

		boolean haveTheoritical = lineAnalysisResult.haveTheoriticalSpectra();

		for (int cpt = 0; cpt < nbPlot; cpt++) {
			CommentedSpectrum spectrumTheoritical = null;
			if (haveTheoritical) {
				spectrumTheoritical = lineAnalysisResult.getSumSpectrumList().get(cpt);
			}
			XAxisVelocity axisVelocity = XAxisCassis.getXAxisVelocity();
			CommentedSpectrum fileCommentedSpectrum = lineAnalysisResult.getFileSpectrumList().get(cpt);
			axisVelocity.setFreqRef(fileCommentedSpectrum.getListOfLines().get(0).getObsFrequency());
			axisVelocity.setVlsr(fileCommentedSpectrum.getVlsr());
			axisVelocity.setLoFreq(fileCommentedSpectrum.getLoFreq());
			CassisPlot cassisPlot = new CassisPlot(fileCommentedSpectrum, spectrumTheoritical,
													axisVelocity, lineConfigCurve);
			cassisPlot.setXAxisVelocity(axisVelocity);
			addFitModel(cassisPlot);
			getListCassisPlots().add(cassisPlot);
			getListToolsModel().add(new ToolsModel(this, cpt));
		}
		if (nbPlot >= 1) {
			setYAxisCassis(lineAnalysisResult.getFileSpectrumList().get(0).getyAxis());
		}
		setTypeFrequency(getListCassisPlots().get(0).getDataSpectrum().getTypeFreq());
		updateRendering();
	}

	@Override
	public LineAnalysisResult getLineResult() {
		return lineResult;
	}

	public LineAnalysisResult changeResultWithSortLine(SORTING_PLOT sortingLine) {
		int[] index = getIndex(lineResult.getFileSpectrumList(), sortingLine);

		ArrayList<CommentedSpectrum> fileSpectrumList = new ArrayList<>();
		ArrayList<CommentedSpectrum> sumSpectrumList = null;
		boolean haveTheoriticalSpectra = lineResult.haveTheoriticalSpectra();
		if (haveTheoriticalSpectra) {
			sumSpectrumList = new ArrayList<>();
		}
		for (int cpt = 0; cpt < index.length; cpt++) {
			int val = index[cpt];
			fileSpectrumList.add(lineResult.getFileSpectrumList().get(val));
			if (haveTheoriticalSpectra) {
				sumSpectrumList.add(lineResult.getSumSpectrumList().get(val));
			}
		}
		LineAnalysisResult lineAnalysisResult = new LineAnalysisResult(fileSpectrumList, sumSpectrumList);
		lineAnalysisResult.setModelIdentifiedInterface(lineAnalysisResult.getModelIdentifiedInterface());

		lineAnalysisResult.setTelescope(lineResult.getTelescope());

		return lineAnalysisResult;
	}

	private int[] getIndex(List<CommentedSpectrum> fileSpectrumList, SORTING_PLOT sortingLine) {
		List<LineDescription> lines = new ArrayList<>(fileSpectrumList.size());
		for (CommentedSpectrum spectrum : fileSpectrumList) {
			lines.add(spectrum.getListOfLines().get(0));
		}
		return UtilDatabase.sortLines(lines, sortingLine);
	}

	@Override
	public ViewType getViewType() {
		return ViewType.LINE_SPECTRUM;
	}

	@Override
	public void setDataSeries(XYSpectrumSeries substractSeries) {
		super.setDataSeries(substractSeries);
		int numPlot = getCurrentNumTriplePlot();
		if (numPlot < getListInfoModels().size()) {
			InfoModel im = getListInfoModels().get(numPlot);
			if (im.isJPanelCurveExist(InfoPanelConstants.LINE_SPECTRUM_TITLE)) {
				JPanelCurve jpc = im.getPanelCurveByName(InfoPanelConstants.LINE_SPECTRUM_TITLE);
				jpc.replaceSeries(substractSeries);
			}
		}
	}
}
