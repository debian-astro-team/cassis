/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.line;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.XAxisCassisUtil;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.common.axes.YAxisGeneric;
import eu.omp.irap.cassis.common.axes.Y_AXIS;
import eu.omp.irap.cassis.gui.model.CassisResult;
import eu.omp.irap.cassis.gui.plot.abstractmozaicplot.AbstractMozaicPlotView;
import eu.omp.irap.cassis.gui.plot.curve.CurvePanelModel;
import eu.omp.irap.cassis.gui.plot.curve.CurvePanelView;
import eu.omp.irap.cassis.gui.plot.curve.JPanelCurve;
import eu.omp.irap.cassis.gui.plot.gallery.GalleryNavigationModel;
import eu.omp.irap.cassis.gui.plot.gallery.GalleryOption;
import eu.omp.irap.cassis.gui.plot.infopanel.InfoModel;
import eu.omp.irap.cassis.gui.plot.infopanel.InfoPanelConstants;
import eu.omp.irap.cassis.gui.plot.simple.SpectrumPlot;
import eu.omp.irap.cassis.lineanalysis.LineAnalysisResult;


@SuppressWarnings("serial")
public class LineSpectrumView extends AbstractMozaicPlotView {


	public LineSpectrumView() {
		this(new LineSpectrumControl(new LineSpectrumModel()));
	}

	public LineSpectrumView(LineSpectrumControl control) {
		super(control);
		control.setView(this);
	}

	@Override
	public JComboBox<String> getComboBottomAxisType() {
		if (comboBottomAxisType == null) {
			comboBottomAxisType = new JComboBox<>(XAxisCassis.getAllXAxisCassisType());
			comboBottomAxisType.setSelectedItem(XAxisCassisUtil.getType(XAxisCassis.getXAxisVelocity()));
			comboBottomAxisType.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					XAxisCassisUtil.changeXAxisAccordingToType(comboBottomAxisType.getSelectedItem().toString(), getComboBottomtAxis());
				}
			});
		}
		return comboBottomAxisType;
	}

	@Override
	public JComboBox<XAxisCassis> getComboBottomtAxis() {
		if (comboBottomtAxis == null) {
			comboBottomtAxis = new JComboBox<>(XAxisCassis.getAllXAxisForType(getComboBottomAxisType().getSelectedItem().toString()));
			comboBottomtAxis.setSelectedItem(XAxisCassis.getXAxisVelocity());
			comboBottomtAxis.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.comboBottomAxisChanged();
					XAxisCassisUtil.changeType(comboBottomtAxis, getComboBottomAxisType());
				}
			});
		}
		return comboBottomtAxis;
	}

	@Override
	protected JPanelCurve getFilePanelCurve(CassisPlot cassisPlot) {
		CurvePanelView curvePanelData = new CurvePanelView(new CurvePanelModel(cassisPlot.getFileSpectrumSeries()));
		JPanelCurve jPanelCurve = new JPanelCurve(InfoPanelConstants.LINE_SPECTRUM_TITLE, false);
		curvePanelData.addCurveCassisListener(control);
		CurvePanelView curvePanelError = new CurvePanelView(new CurvePanelModel(cassisPlot.getErrorFileSeries()));
		curvePanelError.addCurveCassisListener(control);
		CurvePanelView curvePanelLineData = new CurvePanelView(
				new CurvePanelModel(cassisPlot.getFileLineSeriesCassis()));
		curvePanelLineData.addCurveCassisListener(control);
		CurvePanelView curvePanelNearbyLine = new CurvePanelView(new CurvePanelModel(cassisPlot.getSurroundSeries()));
		curvePanelNearbyLine.addCurveCassisListener(control);

		jPanelCurve.addCurvePane(curvePanelData);
		jPanelCurve.addCurvePane(curvePanelError);
		jPanelCurve.addCurvePane(curvePanelLineData);
		jPanelCurve.addCurvePane(curvePanelNearbyLine);
		return jPanelCurve;
	}

	/**
	 * Display a line result product
	 */
	@Override
	public void displayResult(CassisResult result, boolean newDisplay) {
		if (result instanceof LineAnalysisResult) {
			displayResult((LineAnalysisResult)result, newDisplay);
		}

	}
	/**
	 *
	 * Display a line result product
	 */
	public void displayResult(LineAnalysisResult lineResult, boolean newDisplay) {
		XAxisCassis topAxis = lineResult.getAxisData();
		if (newDisplay) {
			getLineConfigCurve().clear();
		}
		messageControl.removeAllMessages();

		model.setCassisPlots(lineResult, getLineConfigCurve());

		createListInfoModels();
		tabManager.setSelectedIndex(0);
		createFitPanelList();
		initStackMosaicPanel();
		initOtherSpeciesPanel();

		if (lineResult.getModelIdentifiedInterface() != null)
			control.getSpeciesControl().initThresholdModel(
					lineResult.getModelIdentifiedInterface().getThresholdModel());

		model.initGalleryNavigation();

		selectMosaicPanel();
		YAxisCassis yAxisCassis = model.getYAxisCassis();
		GalleryNavigationModel galNavModel = model.getGalleryNavigationModel();
		showBy(GalleryOption.NONE, galNavModel.getFilter(), 0);

		switchBack();
		galNavModel.setCurrentGallery(1);

		// Switch to the triple plot if just one plot
		if (model.getListCassisPlots().size() == 1) {
			SpectrumPlot sp = gallerySortPane.getListSpectrumPlots().get(0);
			switchTo(sp.getRowId(), sp.getColId());
		}

		getLineConfigCurve().clear();
		checkColorFromLineConfigCurveToInfoPanel();

		getGalleryNavigation().refreshSorting();

		//to Change the xAxis and Y axis i nthe combo box we must create before the Mozaic
		if (newDisplay) {
			model.setXAxisCassisTop(topAxis);
//			yAxisRigthComboBox.setSelectedItem(model.getYAxisCassis());
//			yAxisLeftComboBox.setSelectedItem(model.getYAxisCassis());

			getComboTopAxisType().setSelectedItem(XAxisCassisUtil.getType(topAxis));
			getComboTopAxis().setSelectedItem(topAxis);
			model.setYAxisCassis(yAxisCassis);
			changeYAxis(yAxisCassis);
		}

		control.yAxisLeftChanged();
		control.comboBottomAxisChanged();
		control.comboTopAxisChanged();
	}

	private void changeYAxis(YAxisCassis newYAxis) {
		YAxisCassis item = newYAxis;
		if (newYAxis.getAxis() == Y_AXIS.UNKNOW) {
			final JComboBox<YAxisCassis> comboLeftAxis = getYAxisLeftComboBox();
			int nb = comboLeftAxis.getModel().getSize();
			for (int i = 0; i< nb; i++) {
				final YAxisCassis itemAt = comboLeftAxis.getItemAt(i);
				if (itemAt.getAxis() == Y_AXIS.UNKNOW) {
					YAxisGeneric yAxisGeneric = (YAxisGeneric)itemAt;
					item = itemAt;
					yAxisGeneric.setInformationName(newYAxis.getInformationName());
					yAxisGeneric.setUnitGeneric(newYAxis.getUnitString());
				}
			}
		}

		yAxisLeftComboBox.setSelectedItem(item);
		yAxisRigthComboBox.setSelectedItem(item);

	}

	@Override
	public LineSpectrumControl getControl() {
		return (LineSpectrumControl) control;
	}

	@Override
	public String getHelpUrl() {
		return "LineSpectrumView.html";
	}

	@Override
	public String getTitle() {
		return "Line Spectrum";
	}

	@Override
	protected InfoModel createInfoModel(CassisPlot cassisPlot) {
		InfoModel im = super.createInfoModel(cassisPlot);
		if (cassisPlot.getSyntheticSeries() != null) {
			JPanelCurve syntheticPanelCurve = getSyntheticPanelCurve(cassisPlot);
			im.addPanelCurve(syntheticPanelCurve);
		}
		return im;
	}

	private JPanelCurve getSyntheticPanelCurve(CassisPlot cassisPlot) {
		CurvePanelView curvePanelSynthetic = new CurvePanelView(new CurvePanelModel(cassisPlot.getSyntheticSeries()));
		String title = curvePanelSynthetic.getNameFromModel();
		JPanelCurve jPanelCurve = new JPanelCurve(title, false);
		curvePanelSynthetic.addCurveCassisListener(control);
		jPanelCurve.addCurvePane(curvePanelSynthetic);
		return jPanelCurve;
	}


}
