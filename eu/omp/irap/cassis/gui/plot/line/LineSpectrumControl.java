/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.line;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.gui.fit.FittingItem;
import eu.omp.irap.cassis.gui.fit.SpectrumFitPanelListener;
import eu.omp.irap.cassis.gui.fit.save.SaveFitLine;
import eu.omp.irap.cassis.gui.plot.abstractmozaicplot.AbstractMozaicPlotControl;
import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassisCollection;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import eu.omp.irap.cassis.gui.plot.util.InterValMarkerCassis;
import eu.omp.irap.cassis.gui.plot.util.SORTING_PLOT;
import eu.omp.irap.cassis.lineanalysis.LineAnalysisResult;


public class LineSpectrumControl extends AbstractMozaicPlotControl {

	private static final Logger LOGGER = LoggerFactory.getLogger(LineSpectrumControl.class);


	public LineSpectrumControl(LineSpectrumModel model) {
		super(model);
		spectrumFitPanelListener = new SpectrumFitPanelListener(this, null, model, model.getNbCassisPlots(), true);
		saveFit = new SaveFitLine();
	}

	@Override
	protected void changeLineSorting(SORTING_PLOT sortingLine) {
		if (view.getGalleryNavigation().getSortingComboBox().getSelectedItem() != sortingLine) {
			view.getGalleryNavigation().getSortingComboBox().setSelectedItem(sortingLine);
		}
		LineAnalysisResult newLineResult = ((LineSpectrumModel)model).changeResultWithSortLine(sortingLine);
		view.displayResult(newLineResult, false);
	}

	@Override
	public LineSpectrumView getView() {
		return (LineSpectrumView) view;
	}

	@Override
	public LineSpectrumModel getModel() {
		return (LineSpectrumModel) model;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.plot.abstractmozaicplot.AbstractMozaicPlotControl#saveAllLineAnalysisInCurrentFile()
	 */
	@Override
	public void saveAllLineAnalysisInCurrentFile() {
		if (saveFit.getLogFile() == null) {
			JOptionPane.showMessageDialog(view,
					"To save the fit, please click on Select file button to select your save file", "Warning",
					JOptionPane.WARNING_MESSAGE);
			return;
		}
		try {
			LinkedList<FittingItem> fittingItem = view.getCurrentFitPanel().getActiveLinkedListFittingItem();
			List<InterValMarkerCassis> listMarker = view.getCurrentFitPanel().getMarkerManager()
					.getListMarkerDisjoint();
			int currentSpectrum = model.getCurrentNumTriplePlot();
			CassisPlot cassisPlot = model.getListCassisPlots().get(currentSpectrum);
			String telescope = model.getTelescope();
			Double rms = view.getCurrentFitPanel().getModel().getRms();
			XYSpectrumSeries currentDataCurve = model.getCurrentDataCurve();

			int nbFitCompo = fittingItem.size();
			XYSeriesCassisCollection fitCurves = new XYSeriesCassisCollection();
			fitCurves.setRendering(model.getRendering());
			if (model.hasFitCompo()) {
				for (int cpt = 0; cpt < nbFitCompo; cpt++) {
					fitCurves.addSeries(model.getFitCurve(cpt));
				}
			} else {
				fitCurves.addSeries(model.getFit());
			}
			saveFit.saveAllLineAnalysisInCurrentFile(fittingItem, listMarker, cassisPlot,
					telescope, rms, currentDataCurve, fitCurves);
		} catch (IOException e) {
			LOGGER.error("Error while saving the file", e);
			JOptionPane.showMessageDialog(view, "Sorry, impossible to save the fit: " + e.getMessage(), "Error",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
	}

	/**
	 *  Action to do on overlay data button clicked.
	 *
	 * @see eu.omp.irap.cassis.gui.plot.abstractmozaicplot.AbstractMozaicPlotControl#onOverlayPanelDataFileButtonClicked()
	 */
	@Override
	public void onOverlayPanelDataFileButtonClicked() {
		addOverlayDataLoadBinding("Line Spectrum - Overlay");
	}

}
