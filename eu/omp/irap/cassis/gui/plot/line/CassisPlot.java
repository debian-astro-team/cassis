/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.plot.line;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.Formula;
import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.XAxisVelocity;
import eu.omp.irap.cassis.common.axes.XAxisWaveLength;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.gui.fit.advanced.interfaces.FitSourceInterface;
import eu.omp.irap.cassis.gui.otherspecies.SpeciesModel;
import eu.omp.irap.cassis.gui.plot.curve.Rendering;
import eu.omp.irap.cassis.gui.plot.curve.TypeCurve;
import eu.omp.irap.cassis.gui.plot.curve.XYErrorSeries;
import eu.omp.irap.cassis.gui.plot.curve.XYLineSeriesCassis;
import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassis;
import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassisCollection;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import eu.omp.irap.cassis.gui.plot.curve.config.ConfigCurve;
import eu.omp.irap.cassis.gui.plot.infopanel.InfoPanelConstants;
import eu.omp.irap.cassis.gui.plot.infopanel.LineConfigCurve;

public class CassisPlot implements FitSourceInterface {

	private static final Logger LOGGER = LoggerFactory.getLogger(CassisPlot.class);

	/* File spectrum dataset. */
	private XYSeriesCassisCollection dataSpectrumSeries;
	private XYLineSeriesCassis fileLineSeriesCassis;

	private XYSeriesCassisCollection errorFileSeries;

	private XYLineSeriesCassis surroundSeries;

	/* Overlay line spectrum dataset. */
	private XYSeriesCassisCollection overlayLineSpectrumDataset;
	private XYSeriesCassisCollection overlaySpectrumSeries;

	private XYSeriesCassisCollection syntheticSeries;
	/* Fit dataset. */
	private XYSeriesCassisCollection fitDataset;

	private XYSeriesCassisCollection resultSeries;

	private XYLineSeriesCassis otherSpeciesSeriesSignal;
	private XYLineSeriesCassis otherSpeciesSeriesImage;
	private XAxisCassis xAxisCassis = XAxisCassis.getXAxisUnknown();
	private XAxisVelocity xAxisCassisVelocity = XAxisCassis.getXAxisVelocity();
	private YAxisCassis yAxisCassis = YAxisCassis.getYAxisKelvin();
	private double vlsrData = 0.;
	private double freqRef = 0.;

	private XYSeriesCassisCollection loomisWoodLine;


	public CassisPlot() {
		overlaySpectrumSeries = new XYSeriesCassisCollection();
		this.dataSpectrumSeries = new XYSeriesCassisCollection();
		this.errorFileSeries = new XYSeriesCassisCollection();
		overlayLineSpectrumDataset = new XYSeriesCassisCollection();
		resultSeries = new XYSeriesCassisCollection();
		fitDataset = new XYSeriesCassisCollection();
		syntheticSeries = new XYSeriesCassisCollection();
	}

	public CassisPlot(CommentedSpectrum fileCommentedSpectrum,
			CommentedSpectrum syntheticCommentedSpectrum,
			XAxisCassis xAxisCassis, LineConfigCurve lineConfigCurve) {
		this();
		this.xAxisCassis = xAxisCassis;
		fileCommentedSpectrum.setxAxisOrigin(xAxisCassis);
		XYSpectrumSeries commentedSpectrumSeries = new XYSpectrumSeries(
				fileCommentedSpectrum.getTitle(), xAxisCassis, TypeCurve.DATA,
				fileCommentedSpectrum);
		commentedSpectrumSeries.setyAxis(fileCommentedSpectrum.getyAxis());
		if (lineConfigCurve.isThatCurveExist(fileCommentedSpectrum.getTitle()))
			commentedSpectrumSeries.setConfigCurve(lineConfigCurve.getConfigCurve(fileCommentedSpectrum.getTitle()));
		else
			commentedSpectrumSeries.getConfigCurve().setColor(Color.BLACK);

		this.dataSpectrumSeries.addSeries(commentedSpectrumSeries);


		if (getMainLine()!= null) {
			setVlsrData(getMainLine().getVlsr());
			setFreqRef(getMainLine().getObsFrequency());
			ArrayList<LineDescription> lines = new ArrayList<>(1);
			lines.add(getMainLine());
			fileLineSeriesCassis = new XYLineSeriesCassis(
					InfoPanelConstants.LINE_TITLE, TypeCurve.LINE,
					lines, XYLineSeriesCassis.TOP, xAxisCassis);
			if (lineConfigCurve.isThatCurveExist("Line"))
				fileLineSeriesCassis.setConfigCurve(lineConfigCurve.getConfigCurve(
						InfoPanelConstants.LINE_TITLE));
			else
				fileLineSeriesCassis.getConfigCurve().setColor(Color.BLUE);
			XYErrorSeries errorSeries = new XYErrorSeries(InfoPanelConstants.ERROR_TITLE,
					TypeCurve.LINE_ERROR, getMainLine(), xAxisCassis);

			if (lineConfigCurve.isThatCurveExist(InfoPanelConstants.ERROR_TITLE))
				errorSeries.setConfigCurve(lineConfigCurve.getConfigCurve(
						InfoPanelConstants.ERROR_TITLE));
			else
				errorSeries.getConfigCurve().setColor(Color.RED);

			this.errorFileSeries.addSeries(errorSeries);

			ArrayList<LineDescription> linesNearby = new ArrayList<>(
					fileCommentedSpectrum.getListOfLines().size() - 1);
			for (int cpt = 1; cpt < fileCommentedSpectrum.getListOfLines().size(); cpt++) {
				linesNearby.add(fileCommentedSpectrum.getListOfLines().get(cpt));
			}

			surroundSeries = new XYLineSeriesCassis(InfoPanelConstants.NEARBY_LINE_TITLE,
					TypeCurve.LINE_NEARBY, linesNearby, XYLineSeriesCassis.TOP,
					xAxisCassis);

			if (lineConfigCurve.isThatCurveExist(InfoPanelConstants.NEARBY_LINE_TITLE))
				surroundSeries.setConfigCurve(lineConfigCurve.getConfigCurve(
						InfoPanelConstants.NEARBY_LINE_TITLE));
			else
				surroundSeries.getConfigCurve().setColor(Color.CYAN);
		}

		XYSpectrumSeries syntheticSpectrumSeries = null;
		if (syntheticCommentedSpectrum != null) {
			syntheticSpectrumSeries = new XYSpectrumSeries(
					syntheticCommentedSpectrum.getTitle(), xAxisCassis,
					TypeCurve.SYNTHETIC, syntheticCommentedSpectrum);

			if (lineConfigCurve.isThatCurveExist(syntheticCommentedSpectrum.getTitle()))
				syntheticSpectrumSeries.setConfigCurve(lineConfigCurve.getConfigCurve(syntheticCommentedSpectrum.getTitle()));
			else {
				ConfigCurve configCurve = new ConfigCurve();
				configCurve.setColor(new Color(201, 33, 145));
				syntheticSpectrumSeries.setConfigCurve(configCurve);
			}

			LOGGER.debug("Title synthetic spectrum: {}", syntheticCommentedSpectrum.getTitle());
			this.syntheticSeries.addSeries(syntheticSpectrumSeries);
		}
	}

	public XYSpectrumSeries addOverlaySpectrum(
			CommentedSpectrum commentedSpectrum) {
		XYSpectrumSeries xySpectrumSeries = new XYSpectrumSeries(
				commentedSpectrum.getTitle(), xAxisCassis,
				TypeCurve.OVERLAY_DATA, commentedSpectrum);

		getOverlaySpectrumSeries().addSeries(xySpectrumSeries);

		return xySpectrumSeries;
	}

	/**
	 * @return the fileSpectrumSeries
	 */
	public final XYSpectrumSeries getFileSpectrumSeries() {
		return (XYSpectrumSeries) dataSpectrumSeries.getSeries(0);
	}

	/**
	 * @return the syntheticLine
	 */
	public final List<LineDescription> getSyntheticLine() {
		if (syntheticSeries == null || syntheticSeries.getSeriesCount() == 0)
			return new ArrayList<>();
		return syntheticSeries.getSeries(0).getListOfLines();
	}

	public final List<LineDescription> getOverlayLineListLines() {
		ArrayList<LineDescription> lines = new ArrayList<>();
		if (overlayLineSpectrumDataset == null)
			return lines;

		int nbSeries = overlayLineSpectrumDataset.getSeriesCount();
		for (int i = 0; i < nbSeries; i++) {
			XYLineSeriesCassis serie = (XYLineSeriesCassis) overlayLineSpectrumDataset
					.getSeries(i);
			lines.addAll(serie.getListOfLines());
		}
		return lines;
	}

	public LineDescription getMainLine() {
		List<LineDescription> listOfLines = getFileSpectrumSeries().getSpectrum().getListOfLines();
		if (listOfLines != null && ! listOfLines.isEmpty())
			return getFileSpectrumSeries().getSpectrum().getListOfLines().get(0);
		else
			return null;
	}

	public XYSeriesCassisCollection getFitDataset() {
		return fitDataset;
	}

	/**
	 * @return the syntheticSerie
	 */
	public XYSpectrumSeries getSyntheticSeries() {
		if (syntheticSeries.getSeriesCount() != 0)
			return (XYSpectrumSeries) syntheticSeries.getSeries(0);
		else
			return null;
	}

	/**
	 * @return the overlayLineSpectrumDataset
	 */
	public XYSeriesCassisCollection getOverlayLineSpectrumDataset() {
		return overlayLineSpectrumDataset;
	}

	/**
	 * @return the overlaySpectrumSeries
	 */
	public XYSeriesCassisCollection getOverlaySpectrumSeries() {
		return overlaySpectrumSeries;
	}

	public void setFileSpectrumSeries(XYSpectrumSeries dataSeries) {
		XYSeriesCassisCollection collection = getCollection(dataSeries);
		if (collection == null) {
			LOGGER.error("Series not found in any collection");
			return;
		}
		XYSeriesCassis series = collection.getSeries(dataSeries.getKey());
		collection.removeSeries(series);
		collection.addSeries(dataSeries);
	}

	public XYLineSeriesCassis getOtherSpeciesSeries(boolean isImage) {
		if (isImage) {
			return otherSpeciesSeriesImage;
		} else {
			return otherSpeciesSeriesSignal;
		}
	}

	/**
	 * @return the errorFileSeries
	 */
	public final XYErrorSeries getErrorFileSeries() {
		if (errorFileSeries.getSeriesCount() != 0)
			return (XYErrorSeries) errorFileSeries.getSeries(0);
		else
			return null;
	}

	/**
	 * @return the surroundSeries
	 */
	public final XYLineSeriesCassis getSurroundSeries() {
		return surroundSeries;
	}

	/**
	 * @return the otherListImage
	 */
	public final List<LineDescription> getOtherListImage() {
		if (otherSpeciesSeriesImage == null) {
			return Collections.emptyList();
		}
		return otherSpeciesSeriesImage.getListOfLines();
	}

	/**
	 * @return the otherListSignal
	 */
	public final List<LineDescription> getOtherListSignal() {
		if (otherSpeciesSeriesSignal == null) {
			return Collections.emptyList();
		}
		return otherSpeciesSeriesSignal.getListOfLines();
	}

	public void setOtherSpeciesSignal(List<LineDescription> lines) {
		Color color = SpeciesModel.DEFAULT_COLOR_SIGNAL;
		TypeCurve typeCurve = TypeCurve.OTHER_SPECIES_SIGNAL;

		otherSpeciesSeriesSignal = new XYLineSeriesCassis(
				InfoPanelConstants.SIGNAL_TITLE,
				typeCurve, lines, XYLineSeriesCassis.BOTTOM, xAxisCassis);

		otherSpeciesSeriesSignal.getConfigCurve().setColor(color);
	}

	public void setOtherSpeciesImage(List<LineDescription> lines) {
		Color color = SpeciesModel.DEFAULT_COLOR_IMAGE;

		TypeCurve typeCurve = TypeCurve.OTHER_SPECIES_IMAGE;

		otherSpeciesSeriesImage = new XYLineSeriesCassis(
				InfoPanelConstants.IMAGE_TITLE, typeCurve,
				lines, XYLineSeriesCassis.BOTTOM, xAxisCassis);

		otherSpeciesSeriesImage.getConfigCurve().setColor(color);
	}

	/**
	 * @return the fileLineSeriesCassis
	 */
	public final XYLineSeriesCassis getFileLineSeriesCassis() {
		return fileLineSeriesCassis;
	}

	/**
	 * @return the otherSpeciesSeriesSignal
	 */
	public final XYLineSeriesCassis getOtherSpeciesSeriesSignal() {
		return otherSpeciesSeriesSignal;
	}

	/**
	 * @return the otherSpeciesSeriesImage
	 */
	public final XYLineSeriesCassis getOtherSpeciesSeriesImage() {
		return otherSpeciesSeriesImage;
	}

	public List<LineDescription> getSurroundLine() {
		ArrayList<LineDescription> surroundLine = new ArrayList<>();
		List<LineDescription> listLine = getFileSpectrumSeries().getSpectrum()
				.getListOfLines();
		if (listLine != null) {
			for (int i = 1; i < listLine.size(); i++) {
				surroundLine.add(listLine.get(i));
			}
		}
		return surroundLine;
	}

	public CommentedSpectrum getDataSpectrum() {
		return getFileSpectrumSeries().getSpectrum();
	}

	public XYLineSeriesCassis createOverlayLineSeries(String file,
			List<LineDescription> lines, boolean recomputeFreqCompute) {
		List<LineDescription> overlayLines = new ArrayList<>();
		double vlsrData = getVlsrData();
		for (LineDescription line : lines) {
			if (recomputeFreqCompute) {
				line.setFreqCompute(Formula.calcFreqWithVlsr(line.getObsFrequency(),
						 line.getVlsr() - vlsrData, line.getObsFrequency()));
			}
			if (line.getFreqCompute() >= getDataSpectrum().getFrequencySignalMin()
					&& line.getFreqCompute() <= getDataSpectrum().getFrequencySignalMax()) {
				line.setVlsrData(vlsrData);

				file = new File(file).getName();
				if (file == null || file.isEmpty())
					file = "LineList";
				overlayLines.add(line);
			}
		}

		if (overlayLines.isEmpty())
			return null;

		XYLineSeriesCassis lineSerie = new XYLineSeriesCassis(file,
				TypeCurve.OVERLAY_LINELIST, overlayLines, XYLineSeriesCassis.TOP,
				xAxisCassis);

		overlayLineSpectrumDataset.addSeries(lineSerie);

		lineSerie.setToFit(false);

		return lineSerie;
	}

	public XYSeriesCassisCollection getResultSeries() {
		return resultSeries;
	}

	public XAxisCassis getxAxisCassis() {
		return xAxisCassis;
	}

	public XAxisVelocity getXAxisVelocity() {
		return xAxisCassisVelocity;
	}

	public void setXAxisVelocity(XAxisVelocity xAxisCassis) {
		xAxisCassisVelocity = xAxisCassis;
	}

	public void setXAxisCassis(XAxisCassis xAxisCassis) {
		if (xAxisCassis instanceof XAxisVelocity) {
			xAxisCassis = xAxisCassisVelocity;
		} else if (xAxisCassis instanceof XAxisWaveLength) {
			XAxisWaveLength axisWave = (XAxisWaveLength) xAxisCassis;
			if (Double.isNaN(axisWave.getFreqRef())) {
				axisWave.setFreqRef(xAxisCassisVelocity.getFreqRef());
			}
		}
		xAxisCassis
				.setLoFreq(getFileSpectrumSeries().getSpectrum().getLoFreq());

		this.xAxisCassis = xAxisCassis;
		this.dataSpectrumSeries.setXAxis(xAxisCassis);

		if (this.fileLineSeriesCassis != null)
			this.fileLineSeriesCassis.setXAxis(xAxisCassis);
		if (this.errorFileSeries != null)
			this.errorFileSeries.setXAxis(xAxisCassis);
		if (this.surroundSeries != null)
			this.surroundSeries.setXAxis(xAxisCassis);

		this.overlayLineSpectrumDataset.setXAxis(xAxisCassis);
		this.overlaySpectrumSeries.setXAxis(xAxisCassis);
		if (syntheticSeries != null) {
			this.syntheticSeries.setXAxis(xAxisCassis);
		}
		this.fitDataset.setXAxis(xAxisCassis);

		this.resultSeries.setXAxis(xAxisCassis);

		if (otherSpeciesSeriesSignal != null) {
			this.otherSpeciesSeriesSignal.setXAxis(xAxisCassis);
		}

		if (otherSpeciesSeriesImage != null) {
			this.otherSpeciesSeriesImage.setXAxis(xAxisCassis);
		}

		if (loomisWoodLine != null) {
			this.loomisWoodLine.setXAxis(xAxisCassis);
		}
	}

	public void updateConfigSeries(XYSeriesCassis series) {
		if (TypeCurve.DATA.equals(series.getTypeCurve())) {
			this.getFileSpectrumSeries().setConfigCurve(
					series.getConfigCurve().copy());
		} else if (TypeCurve.LINE.equals(series.getTypeCurve())) {
			this.getFileLineSeriesCassis().setConfigCurve(
					series.getConfigCurve().copy());
		} else if (TypeCurve.LINE_ERROR.equals(series.getTypeCurve())) {
			if (this.getErrorFileSeries() != null)
				this.getErrorFileSeries().setConfigCurve(
						series.getConfigCurve().copy());
		} else if (TypeCurve.LINE_NEARBY.equals(series.getTypeCurve())) {
			this.getSurroundSeries().setConfigCurve(
					series.getConfigCurve().copy());
		} else if (TypeCurve.SYNTHETIC.equals(series.getTypeCurve())) {
			if (this.getSyntheticSeries() != null)
				this.getSyntheticSeries().setConfigCurve(
						series.getConfigCurve().copy());
		} else if (TypeCurve.OTHER_SPECIES_SIGNAL.equals(series.getTypeCurve())) {
			if (this.getOtherSpeciesSeriesSignal() != null)
				this.getOtherSpeciesSeriesSignal().setConfigCurve(
						series.getConfigCurve().copy());
		} else if (TypeCurve.OTHER_SPECIES_IMAGE.equals(series.getTypeCurve())) {
			if (this.getOtherSpeciesSeriesImage() != null)
				this.getOtherSpeciesSeriesImage().setConfigCurve(
						series.getConfigCurve().copy());
		} else if (TypeCurve.OVERLAY_DATA.equals(series.getTypeCurve())) {
			for (int j = 0; j < this.getOverlaySpectrumSeries()
					.getSeriesCount(); j++) {
				if (this.getOverlaySpectrumSeries().getSeries(j).getKey()
						.equals(series.getKey())) {
					this.getOverlaySpectrumSeries().getSeries(j)
							.setConfigCurve(series.getConfigCurve().copy());
				}
			}
		} else if (TypeCurve.OVERLAY_LINELIST.equals(series.getTypeCurve())) {
			for (int j = 0; j < this.getOverlayLineSpectrumDataset()
					.getSeriesCount(); j++) {
				if (this.getOverlayLineSpectrumDataset().getSeries(j).getKey()
						.equals(series.getKey())) {
					this.getOverlayLineSpectrumDataset().getSeries(j)
							.setConfigCurve(series.getConfigCurve().copy());
				}
			}
		} else if (TypeCurve.RESULT.equals(series.getTypeCurve())) {
			for (int j = 0; j < this.getResultSeries().getSeriesCount(); j++) {
				if (this.getResultSeries().getSeries(j).getKey()
						.equals(series.getKey())) {
					this.getResultSeries().getSeries(j)
							.setConfigCurve(series.getConfigCurve().copy());
				}
			}
		} else if (TypeCurve.LOOMIS_LINE.equals(series.getTypeCurve())) {
			for (int j = 0; j < this.getLoomisLine().getSeriesCount(); j++) {
				if (this.getLoomisLine().getSeries(j).getKey()
						.equals(series.getKey())) {
					this.getLoomisLine().getSeries(j)
							.setConfigCurve(series.getConfigCurve().copy());
				}
			}
		}
	}

	public void setYAxisCassis(YAxisCassis newValue) {
		yAxisCassis = newValue;
	}

	public YAxisCassis getYAxisCassis() {
		return yAxisCassis;
	}

	@Override
	public double getFreqRef() {
		return this.freqRef;
	}

	public double getVlsrData() {
		return this.vlsrData;
	}

	public void setVlsrData(double vlsr) {
		this.vlsrData = vlsr;
	}

	public void setFreqRef(double freqRef) {
		this.freqRef = freqRef;
	}

	public void setLoomisLine(XYSeriesCassisCollection loomisWoodLine) {
		this.loomisWoodLine = loomisWoodLine;
	}

	public XYSeriesCassisCollection getLoomisLine() {
		return this.loomisWoodLine;
	}

	/**
	 * Search and return the series (who must be savable) with the given name.
	 *
	 * @param name The name of the series.
	 * @return The series or null if not found.
	 */
	public XYSpectrumSeries getSavableSeries(String name) {
		List<XYSeriesCassisCollection> listColl = new ArrayList<>(5);
		listColl.add(dataSpectrumSeries);
		listColl.add(syntheticSeries);
		listColl.add(fitDataset);
		listColl.add(overlaySpectrumSeries);
		listColl.add(resultSeries);
		for (XYSeriesCassisCollection coll : listColl) {
			XYSeriesCassis s = coll.getSeries(name);
			if (s instanceof XYSpectrumSeries) {
				return (XYSpectrumSeries) s;
			}
		}
		return null;
	}

	/**
	 * Change the rendering of all the collections.
	 *
	 * @param rendering The new rendering to set.
	 */
	public void setRendering(Rendering rendering) {
		updateCollectionRendering(dataSpectrumSeries, rendering);
		updateCollectionRendering(errorFileSeries, rendering);
		updateCollectionRendering(overlayLineSpectrumDataset, rendering);
		updateCollectionRendering(overlaySpectrumSeries, rendering);
		updateCollectionRendering(syntheticSeries, rendering);
		updateCollectionRendering(fitDataset, rendering);
		updateCollectionRendering(resultSeries, rendering);
		updateCollectionRendering(loomisWoodLine, rendering);
	}

	/**
	 * Update the rendering of a collection.
	 *
	 * @param col The collection to update.
	 * @param rendering The rendering to set.
	 */
	private void updateCollectionRendering(XYSeriesCassisCollection col, Rendering rendering) {
		if (col != null) {
			col.setRendering(rendering);
		}
	}

	/**
	 * Check if the CassisPlot contains a series with the given name.
	 *
	 * @param name The name to search
	 * @return true if there is a series with the given name, false otherwise
	 */
	public boolean containsSeries(String name) {
		return (isCollectionContainsSeries(dataSpectrumSeries, name)
				|| (fileLineSeriesCassis != null && fileLineSeriesCassis.getKey().equals(name))
				|| isCollectionContainsSeries(errorFileSeries, name)
				|| (surroundSeries != null && surroundSeries.getKey().equals(name))
				|| isCollectionContainsSeries(overlayLineSpectrumDataset, name)
				|| isCollectionContainsSeries(overlaySpectrumSeries, name)
				|| isCollectionContainsSeries(syntheticSeries, name)
				|| isCollectionContainsSeries(fitDataset, name)
				|| isCollectionContainsSeries(resultSeries, name)
				|| (otherSpeciesSeriesSignal != null && otherSpeciesSeriesSignal.getKey().equals(name))
				|| (otherSpeciesSeriesImage != null && otherSpeciesSeriesImage.getKey().equals(name))
				|| isCollectionContainsSeries(loomisWoodLine, name));
	}

	/**
	 * Check and return if the given collection contains a series with the given name.
	 *  This also check if the collection is not null first.
	 *
	 * @param collection The collection to check
	 * @param name The name to search
	 * @return true if the collection contains the series, false otherwise
	 */
	private boolean isCollectionContainsSeries(XYSeriesCassisCollection collection, String name) {
		return collection != null && collection.containsSeries(name);
	}

	@Override
	public XAxisCassis getxAxis() {
		return xAxisCassis;
	}

	@Override
	public YAxisCassis getyAxis() {
		return yAxisCassis;
	}

	@Override
	public double getVlsr() {
		return vlsrData;
	}

	@Override
	public XYSpectrumSeries getCurrentSeries() {
		return getFileSpectrumSeries();
	}

	@Override
	public List<XYSpectrumSeries> getAllSeries() {
		List<XYSpectrumSeries> series = new ArrayList<>();
		series.add(getFileSpectrumSeries());
		return series;
	}

	@Override
	public List<LineDescription> getTopPossibleLines() {
		List<LineDescription> lines = new ArrayList<>();
		lines.add(getMainLine());
		lines.addAll(getSurroundLine());
		return lines;
	}

	@Override
	public List<LineDescription> getBottomPossibleLines() {
		return new ArrayList<>();
	}

	@Override
	public List<LineDescription> getAllLines() {
		List<LineDescription> lines = new ArrayList<>(getBottomPossibleLines());
		lines.addAll(getTopPossibleLines());
		return lines;
	}

	/**
	 * Search then return the collection who contains the given spectrum series.
	 *
	 * @param spectrumSeries The given spectrum series.
	 * @return the collection who contains the given spectrum series or null if not found.
	 */
	public XYSeriesCassisCollection getCollection(XYSpectrumSeries spectrumSeries) {
		if (isCollectionContainsSeries(dataSpectrumSeries,
				(String) spectrumSeries.getKey())) {
			return dataSpectrumSeries;
		}
		if (isCollectionContainsSeries(overlaySpectrumSeries,
				(String) spectrumSeries.getKey())) {
			return overlaySpectrumSeries;
		}
		if (isCollectionContainsSeries(syntheticSeries,
				(String) spectrumSeries.getKey())) {
			return syntheticSeries;
		}
		if (isCollectionContainsSeries(fitDataset,
				(String) spectrumSeries.getKey())) {
			return fitDataset;
		}
		if (isCollectionContainsSeries(resultSeries,
				(String) spectrumSeries.getKey())) {
			return resultSeries;
		}
		return null;
	}

}
