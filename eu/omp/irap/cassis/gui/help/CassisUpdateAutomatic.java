/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.help;

import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.gui.help.UpdateView.UpdateRegular;
import eu.omp.irap.cassis.properties.Software;
import eu.omp.irap.cassis.properties.UserConfiguration;

public class CassisUpdateAutomatic {

	private static final Logger LOGGER = LoggerFactory.getLogger(CassisUpdateAutomatic.class);


	/**
	 * Check update cassis automatic.
	 *
	 * @param feedBackIfNoUpdate true if we want to notify the user that there
	 *  is no update, false otherwise.
	 */
	public static void checkUpdateAutomatic(boolean feedBackIfNoUpdate) {
		UserConfiguration userConfiguration = Software.getUserConfiguration();
		UpdateRegular updateRegular = userConfiguration.getUpdateRegular();

		if (updateRegular == null) {
			updateRegular = UpdateRegular.NONE;
			Software.getUserConfiguration().setUpdateRegular(updateRegular);
		}

		String lastUpdate = userConfiguration.getLastUpdate();

		if (lastUpdate == null || lastUpdate.isEmpty())
			lastUpdate = "00000000"; // YYYYMMDD

		if (updateRegular == UpdateRegular.EVERY_DAY) {
			checkUpdateEveryDay(lastUpdate, feedBackIfNoUpdate);
		}
		else if (updateRegular == UpdateRegular.EVERY_WEEK) {
			checkUpdateEveryWeek(lastUpdate, feedBackIfNoUpdate);
		}
		else if (updateRegular == UpdateRegular.EVERY_MONTH) {
			checkUpdateEveryMonth(lastUpdate, feedBackIfNoUpdate);
		}
	}

	/**
	 * Get String of now: YYYYMMDD
	 *
	 * @return {@link String}
	 */
	public static String getNowString() {
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		int month = now.get(Calendar.MONTH) + 1;
		int day = now.get(Calendar.DAY_OF_MONTH);

		String monthString = Integer.toString(month).length() < 2 ? "0" + month : Integer.toString(month);
		String dayString = Integer.toString(day).length() < 2 ? "0" + day : Integer.toString(day);

		return year + monthString + dayString;
	}

	/**
	 * Get week of year: YYYYWW
	 *
	 * @param date
	 *            {@link String}
	 * @return {@link String}
	 */
	private static String getWeekString(String date) {
		if ("00000000".equals(date))
			return "000000";

		int year = getYear(date);
		int month = getMonth(date);
		int day = getDay(date);

		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month - 1);
		calendar.set(Calendar.DAY_OF_MONTH, day);

		int week = calendar.get(Calendar.WEEK_OF_YEAR);
		String weekString = Integer.toString(week).length() < 2 ? "0" + week : "" + week;

		return year + weekString;
	}

	/**
	 * Check update. Mode every_day
	 * @param feedBackIfNoUpdate
	 *
	 * @param lastUpdate
	 *            : {@link String}
	 */
	private static void checkUpdateEveryDay(String lastUpdate, boolean feedBackIfNoUpdate) {
		String now = getNowString();
		if (lastUpdate.compareTo(now) < 0) {
			LOGGER.info("Check update cassis automatic mode EVERY-DAY");
			new UpdateControl(feedBackIfNoUpdate).updateCassis();
			Software.getUserConfiguration().setLastUpdate(now);
		}
	}

	/**
	 * Check update. Mode every_week
	 * @param feedBackIfNoUpdate
	 *
	 * @param lastUpdate
	 *            : {@link String}
	 */
	private static void checkUpdateEveryWeek(String lastUpdate, boolean feedBackIfNoUpdate) {
		String now = getNowString();

		String lastWeek = getWeekString(lastUpdate);
		String week = getWeekString(now);

		if (lastWeek.compareTo(week) < 0) {
			LOGGER.info("Check update cassis automatic mode EVERY-WEEK");
			new UpdateControl(feedBackIfNoUpdate).updateCassis();
			Software.getUserConfiguration().setLastUpdate(now);
		}
	}

	/**
	 * Check update. Mode every_month
	 *
	 * @param lastUpdate
	 *            : {@link String}
	 */
	private static void checkUpdateEveryMonth(String lastUpdate, boolean feedBackIfNoUpdate) {
		String now = getNowString();
		if (lastUpdate.substring(0, 6).compareTo(now.substring(0, 6)) < 0) {
			LOGGER.info("Check update cassis automatic mode EVERY-MONTH");
			new UpdateControl(feedBackIfNoUpdate).updateCassis();
			Software.getUserConfiguration().setLastUpdate(now);
		}
	}

	/**
	 * Get year of date string.
	 *
	 * @param date
	 *            {@link String}
	 * @return the year
	 */
	public static int getYear(String date) {
		if (date != null)
			return Integer.parseInt(date.substring(0, 4));
		return 0;
	}

	/**
	 * Get month of date string.
	 *
	 * @param date
	 *            {@link String}
	 * @return the month
	 */
	public static int getMonth(String date) {
		if (date != null)
			return Integer.parseInt(date.substring(4, 6));
		return 0;
	}

	/**
	 * Get day of date string.
	 *
	 * @param date
	 *            {@link String}
	 * @return the day
	 */
	public static int getDay(String date) {
		if (date != null)
			return Integer.parseInt(date.substring(6, 8));
		return 0;
	}

}
