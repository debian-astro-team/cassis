/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.help;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.gui.ScreenUtil;
import eu.omp.irap.cassis.gui.util.BrowserControl;
import eu.omp.irap.cassis.gui.util.CassisRessource;
import eu.omp.irap.cassis.properties.Software;

public class AboutDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(AboutDialog.class);

	private JLabel aboutLbl = null;
	private JList<String> authorsLst = null;
	private JPanel jContentPane = null;
	private static boolean instanced = false;
	private static AboutDialog about;


	public AboutDialog(JFrame owner) {
		super(owner);
		initialize();
	}

	/**
	 * This method initializes this.
	 *
	 * @return void
	 */
	private void initialize() {
		setTitle("Cassis About");
		setContentPane(getJContentPane());
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				exit();
			}
		});
		pack();
		ScreenUtil.center(getOwner(), this);
		setResizable(false);
		setModal(true);
		setAlwaysOnTop(true);
	}

	/**
	 * This method initializes jContentPane
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());

			JLabel logoCassisLbl = new JLabel(new ImageIcon(CassisRessource.getCassisLogo()));
			JLabel logoUPSLbl = new JLabel(new ImageIcon(CassisRessource.getUpsLogo()));
			JLabel logoIRAPLbl = new JLabel(new ImageIcon(CassisRessource.getIrapLogo()));
			JLabel logoCNRSLbl = new JLabel(new ImageIcon(CassisRessource.getCnrsLogo()));

			JButton okBtn = new JButton("OK");
			okBtn.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					exit();
				}
			});
			JPanel buttonPanel = new JPanel(new BorderLayout());
			buttonPanel.add(okBtn, BorderLayout.CENTER);

			GridLayout layout = new GridLayout();
			layout.setRows(3);
			JPanel panel = new JPanel(layout);
			JScrollPane listScroller = new JScrollPane(getAuthors());
			panel.add(logoCassisLbl);
			panel.add(getAboutLbl(), null);
			panel.add(listScroller, null);

			JPanel logoPanel = new JPanel();
			logoPanel.add(logoUPSLbl);
			logoPanel.add(logoIRAPLbl);
			logoPanel.add(logoCNRSLbl);

			jContentPane.add(logoPanel, BorderLayout.NORTH);
			jContentPane.add(panel, BorderLayout.CENTER);
			jContentPane.add(buttonPanel, BorderLayout.SOUTH);
		}
		return jContentPane;
	}

	void exit() {
		dispose();
		instanced = false;
	}

	/**
	 * Get About Label.
	 *
	 * @return {@link JLabel}
	 */
	private JLabel getAboutLbl() {
		if (aboutLbl == null) {
			String cassisVersionBuild = Software.getCassisConfiguration().getCassisVersion();
			String[] ss = cassisVersionBuild.split("-");
			String version = "???";
			String date = "???";
			String build = "???";

			if (ss.length == 4) {
				version = ss[1];
				date = ss[2];
				build = ss[3];
				date = date.replaceAll("beta", "");
				if (date.contains("SNAPSHOT")) {
					date = date.replaceAll("SNAPSHOT", "");
					version += "-SNAPSHOT";
				}
				date = date.substring(4, 6) + "/" + date.substring(2, 4) + "/20" + date.substring(0, 2);
				if (build.contains("beta")) {
					version += " beta";
				}
				build = build.replaceAll("Build", "").replaceAll("build", "").replaceAll("beta", "").trim();
			}

			String contentAbout = "<HTML><br><p> Cassis Software </p><br>Version: " + version
					+ "<br>Date: " + date + "<br>Build id: " + build + "<br><br>"
					+ "<p> (c) Copyright Open CNRS contributors and others 2005-2017. All rights reserved. <br>"
					+ "Visit <a href='http://cassis.irap.omp.eu/'> http://cassis.irap.omp.eu/ </a></p><br>Authors:"
					+ "<br></HTML>";

			aboutLbl = new JLabel(contentAbout);
			aboutLbl.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					BrowserControl.displayURL("http://cassis.irap.omp.eu/");
				}
			});

			aboutLbl.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
		}
		return aboutLbl;
	}

	/**
	 * Get Authors List.
	 *
	 * @return {@link JList}
	 */
	private JList<String> getAuthors() {
		if (authorsLst == null) {
			List<String> data = new ArrayList<>();
			try (BufferedReader in =  new BufferedReader(
					new InputStreamReader(CassisRessource.getAuthors(), "UTF-8"))) {
				String line = "";
				while ((line = in.readLine()) != null) {
					data.add(line);
				}
			} catch (IOException ioe) {
				LOGGER.error("Error while retrieving authors list", ioe);
			}

			authorsLst = new JList<>(data.toArray(new String[data.size()]));
		}

		return authorsLst;
	}

	public static void createInstance(final JFrame owner) {
		if (!instanced) {
			instanced = true;
			about = new AboutDialog(owner);
			about.setVisible(true);
		}
	}

}
