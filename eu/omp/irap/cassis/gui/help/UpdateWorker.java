/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.help;

import java.io.File;
import java.io.IOException;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.gui.util.FileUtils;
import eu.omp.irap.cassis.properties.Software;

/**
 * SwingWorker derived class to update CASSIS.
 *
 * @author M. Boiziot
 */
public class UpdateWorker extends SwingWorker<Void, Void> {

	private static final Logger LOGGER = LoggerFactory.getLogger(UpdateWorker.class);

	private static final int OK = 0;
	private static final int NO_UPDATE = 1;
	private static final int ERROR = 2;

	private boolean feedBackIfNoUpdate;
	private int state;


	/**
	 * SwingWorker for updating CASSIS.
	 *
	 * @param feedBackIfNoUpdate If a message must be displayed for informing
	 *  of the update status.
	 */
	public UpdateWorker(boolean feedBackIfNoUpdate) {
		this.feedBackIfNoUpdate = feedBackIfNoUpdate;
	}

	/**
	 * In Background thread: download the CASSIS updater .jar then try to run it.
	 *
	 * @return null
	 * @throws Exception In case of Exception.
	 */
	@Override
	protected Void doInBackground() throws Exception {
		String cassisHome = Software.getCassisPath();
		if (cassisHome != null && (new File(cassisHome)).exists()) {
			String stringUrl = Software.getUserConfiguration().getUpdatePath() + "cassis.setup.wizard.jar";
			File updateJar = FileUtils.download(stringUrl, true);
			if (updateJar == null) {
				state = NO_UPDATE;
			} else {
				Runtime rt = Runtime.getRuntime();
				try {
					rt.exec(new String[] { "java", "-jar", updateJar.getAbsolutePath(), cassisHome, String.valueOf(feedBackIfNoUpdate)});
				} catch (IOException ioe) {
					LOGGER.error("Error while executing update jar", ioe);
					state = ERROR;
				}
			}
		}
		return null;
	}

	/**
	 * In EDT thread: display message if needed after the operation.
	 */
	@Override
	protected void done() {
		if (feedBackIfNoUpdate) {
			switch (state) {
			case NO_UPDATE:
				JOptionPane.showMessageDialog(null, "There is no update available.",
						"Cassis update", JOptionPane.INFORMATION_MESSAGE);
				break;

			case ERROR:
				JOptionPane.showMessageDialog(null,
						"An error occurred while searching update.",
						"Cassis update", JOptionPane.ERROR_MESSAGE);
				break;

			case OK:
			default:
				// Nothing to do.
				break;
			}
		}
	}

}
