/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.help;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.WindowConstants;

import eu.omp.irap.cassis.common.gui.ScreenUtil;
import eu.omp.irap.cassis.properties.Software;

@SuppressWarnings("serial")
public class UpdateView extends JDialog {

	private JPanel jContentPane = null;
	private JButton checkNewBtn = null;
	private JCheckBox automaticCheckBox = null;
	private JRadioButton everyMonthRbtn = null;
	private JRadioButton everyWeekRbtn = null;
	private JRadioButton everyDayRbtn = null;
	private UpdateControl updateControl = null;
	private JButton closeBtn;
	private JButton applyBtn;
	private JPanel buttonPanel;
	private static boolean instanced = false;
	private static UpdateView uv;

	public enum UpdateRegular {
		EVERY_DAY, EVERY_WEEK, EVERY_MONTH, NONE;

		public static UpdateRegular getValue(String value) {
			UpdateRegular result = NONE;
			if (value != null) {
				if (value.equalsIgnoreCase(EVERY_DAY.toString()))
					result = EVERY_DAY;
				else if (value.equalsIgnoreCase(EVERY_WEEK.toString()))
					result = EVERY_WEEK;
				else if (value.equalsIgnoreCase(EVERY_MONTH.toString()))
					result = EVERY_MONTH;
			}
			return result;
		}
	}

	UpdateRegular updateRegular = UpdateRegular.NONE;

	public UpdateView(JFrame owner) {
		super(owner);
		initialize();
	}

	private void initialize() {
		updateControl = new UpdateControl(true);
		setSize(300, 200);
		setTitle("Cassis Update");
		setContentPane(getJContentPane());
		setLocationRelativeTo(getParent());
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				exit();
			}
		});
		ScreenUtil.center(getOwner(), this);
		setResizable(false);
		setModal(true);
		setVisible(true);
	}

	void exit() {
		instanced = false;
		dispose();
	}

	/**
	 * This method initializes jContentPane
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel(new BorderLayout());

			JPanel panel = new JPanel(new GridBagLayout());

			GridBagConstraints checkNewGbc = new GridBagConstraints();
			setGridBagParameters(checkNewGbc, 0, 0);
			checkNewGbc.insets = new Insets(0, 5, 0, 0);

			GridBagConstraints automaticGbc = new GridBagConstraints();
			setGridBagParameters(automaticGbc, 0, 1);

			GridBagConstraints everyDayGbc = new GridBagConstraints();
			setGridBagParameters(everyDayGbc, 0, 2);
			everyDayGbc.insets = new Insets(0, 30, 0, 0);

			GridBagConstraints everyWeekGbc = new GridBagConstraints();
			setGridBagParameters(everyWeekGbc, 0, 3);
			everyWeekGbc.insets = new Insets(0, 30, 0, 0);

			GridBagConstraints everyMonthGbc = new GridBagConstraints();
			setGridBagParameters(everyMonthGbc, 0, 4);
			everyMonthGbc.insets = new Insets(0, 30, 0, 0);

			panel.add(getCheckNewBtn(), checkNewGbc);
			panel.add(getAutomaticCheckBox(), automaticGbc);

			panel.add(getEveryDayRbtn(), everyDayGbc);
			panel.add(getEveryWeekRbtn(), everyWeekGbc);
			panel.add(getEveryMonthRbtn(), everyMonthGbc);

			ButtonGroup groupRadio = new ButtonGroup();
			groupRadio.add(getEveryDayRbtn());
			groupRadio.add(getEveryWeekRbtn());
			groupRadio.add(getEveryMonthRbtn());

			jContentPane.add(panel, BorderLayout.CENTER);
			jContentPane.add(getButtonPanel(), BorderLayout.SOUTH);
		}
		return jContentPane;
	}

	/**
	 * This method initializes checkNewBtn
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getCheckNewBtn() {
		if (checkNewBtn == null) {
			checkNewBtn = new JButton();
			checkNewBtn.setText("Check for Updates");
			checkNewBtn.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent ae) {
					updateControl.updateCassis();
				}
			});
		}
		return checkNewBtn;
	}

	/**
	 * This method initializes automaticCheckBox
	 *
	 * @return javax.swing.JCheckBox
	 */
	private JCheckBox getAutomaticCheckBox() {
		if (automaticCheckBox == null) {
			automaticCheckBox = new JCheckBox();
			automaticCheckBox.setText("Automatic");

			UpdateRegular ur = Software.getUserConfiguration().getUpdateRegular();
			if (ur != null && ur != UpdateRegular.NONE) {
				automaticCheckBox.setSelected(true);
				setEnableUpdateBtns(true);
			} else {
				automaticCheckBox.setSelected(false);
				setEnableUpdateBtns(false);
			}
			automaticCheckBox.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					int state = e.getStateChange();
					boolean enabled = false;
					if (state == ItemEvent.SELECTED) {
						enabled = true;
					}

					setEnableUpdateBtns(enabled);
				}
			});
		}
		return automaticCheckBox;
	}

	private void setEnableUpdateBtns(boolean enabled) {
		getEveryDayRbtn().setEnabled(enabled);
		getEveryMonthRbtn().setEnabled(enabled);
		getEveryWeekRbtn().setEnabled(enabled);
	}

	/**
	 * This method initializes buttonPanel
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getButtonPanel() {
		if (buttonPanel == null) {
			buttonPanel = new JPanel();
			buttonPanel.setLayout(new FlowLayout());
			buttonPanel.add(getApplyBtn());
			buttonPanel.add(getCloseBtn());
		}
		return buttonPanel;
	}

	/**
	 * This method initializes applyBtn
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getApplyBtn() {
		if (applyBtn == null) {
			applyBtn = new JButton();
			applyBtn.setText("Apply");
			applyBtn.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					boolean automaticCheck = UpdateView.this.getAutomaticCheckBox().isSelected();
					updateRegular = UpdateRegular.NONE;
					if (automaticCheck) {
						if (UpdateView.this.getEveryDayRbtn().isSelected())
							updateRegular = UpdateRegular.EVERY_DAY;
						else if (UpdateView.this.getEveryWeekRbtn().isSelected())
							updateRegular = UpdateRegular.EVERY_WEEK;
						else if (UpdateView.this.getEveryMonthRbtn().isSelected())
							updateRegular = UpdateRegular.EVERY_MONTH;
						else {
							JOptionPane
									.showMessageDialog(UpdateView.this,
											"Oupps, you did not select any option ('every day', 'every week' or 'every month')");
							return;
						}
					}
					Software.getUserConfiguration().setUpdateRegular(updateRegular);
					exit();
				}
			});
		}
		return applyBtn;
	}

	/**
	 * This method initializes cancelBtn
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getCloseBtn() {
		if (closeBtn == null) {
			closeBtn = new JButton();
			closeBtn.setText("Close");
			closeBtn.addActionListener(new java.awt.event.ActionListener() {
				@Override
				public void actionPerformed(java.awt.event.ActionEvent e) {
					exit();
				}
			});
		}
		return closeBtn;
	}

	/**
	 * This method initializes everyMonthRbtn
	 *
	 * @return javax.swing.JRadioButton
	 */
	private JRadioButton getEveryMonthRbtn() {
		if (everyMonthRbtn == null) {
			everyMonthRbtn = new JRadioButton();
			everyMonthRbtn.setText("Every month");
			updateRegular = Software.getUserConfiguration().getUpdateRegular();
			if (updateRegular == UpdateRegular.EVERY_MONTH)
				everyMonthRbtn.setSelected(true);
			else
				everyMonthRbtn.setSelected(false);
		}
		return everyMonthRbtn;
	}

	/**
	 * This method initializes everyWeekRbtn
	 *
	 * @return javax.swing.JRadioButton
	 */
	private JRadioButton getEveryWeekRbtn() {
		if (everyWeekRbtn == null) {
			everyWeekRbtn = new JRadioButton();
			everyWeekRbtn.setText("Every week");

			updateRegular = Software.getUserConfiguration().getUpdateRegular();
			if (updateRegular == UpdateRegular.EVERY_WEEK)
				everyWeekRbtn.setSelected(true);
			else
				everyWeekRbtn.setSelected(false);
		}
		return everyWeekRbtn;
	}

	/**
	 * This method initializes everyDayRbtn
	 *
	 * @return javax.swing.JRadioButton
	 */
	private JRadioButton getEveryDayRbtn() {
		if (everyDayRbtn == null) {
			everyDayRbtn = new JRadioButton();
			everyDayRbtn.setText("Every day");

			updateRegular = Software.getUserConfiguration().getUpdateRegular();
			if (updateRegular == UpdateRegular.EVERY_DAY) {
				everyDayRbtn.setSelected(true);
			}
			else {
				everyDayRbtn.setSelected(false);
			}
		}
		return everyDayRbtn;
	}

	public void setGridBagParameters(GridBagConstraints gb, int x, int y) {
		gb.gridx = x;
		gb.gridy = y;
		gb.gridwidth = 1;
		gb.insets = new Insets(0, 0, 0, 0);
		gb.anchor = GridBagConstraints.WEST;
	}

	public static void createInstance(JFrame owner) {
		if (!instanced) {
			instanced = true;
			uv = new UpdateView(owner);
		} else {
			uv.setVisible(true);
			uv.toFront();
			uv.repaint();
		}
	}

}
