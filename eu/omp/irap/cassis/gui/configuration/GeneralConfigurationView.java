/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.configuration;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;

import eu.omp.irap.cassis.gui.plot.curve.Rendering;
import eu.omp.irap.cassis.gui.util.TextFieldFormatFilter;
import eu.omp.irap.cassis.properties.Software;
import eu.omp.irap.cassis.properties.UserConfiguration;

/**
 * General tab for the configuration.
 *
 * @author M. Boiziot
 */
public class GeneralConfigurationView extends JPanel {

	private static final long serialVersionUID = -6981584288072703825L;

	private JTextField toolsMaxNbPointsTextField;
	private JComboBox<Rendering> renderingComboBox;
	private JComboBox<Boolean> forceOpenCombobox;
	private JComboBox<Boolean> autoStartSampCombobox;

	private JComboBox<Boolean> autoOtherSpeciesComboBox;


	/**
	 * Constructor.
	 */
	public GeneralConfigurationView() {
		super(new GridBagLayout());
		setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		createGeneralPanel();
	}

	/**
	 * Create the General panel.
	 */
	private void createGeneralPanel() {
		GridBagConstraints gbc = new GridBagConstraints();
		UserConfigurationUtils.createLine(this, gbc, "Autostart Samp:",
				null, getSampComboBox(), null, 0);
		UserConfigurationUtils.createLine(this, gbc, "Force direct opening:",
				"If set to true, the file will open directly in Spectrum Analysis.",
				getForceOpenCombobox(), null, 1);
		UserConfigurationUtils.createLine(this, gbc, "Tools max nb points:",
				"Change the maximum number of points allowed in a Tools operation.",
				getToolsMaxNbPointsTextField(), null, 2);
		UserConfigurationUtils.createLine(this, gbc, "Default rendering:",
				null, getRenderingComboBox(), null, 3);
		UserConfigurationUtils.createLine(this, gbc, "Automatic other species displayed",
				null, getAutoOtherSpeciesComboBox(), null, 4);
	}

	/**
	 * Create if needed then return the Samp autostart {@link JComboBox}.
	 *
	 * @return the Samp autostart {@link JComboBox}.
	 */
	private JComboBox<Boolean> getSampComboBox() {
		if (autoStartSampCombobox == null) {
			Boolean[] booleans = new Boolean[] {true, false};
			autoStartSampCombobox = new JComboBox<>(booleans);
			autoStartSampCombobox.setSelectedItem(Software.getUserConfiguration().isSampOnAutoStart());
		}
		return autoStartSampCombobox;
	}

	/**
	 * Create if needed then return the "Force direct opening" {@link JComboBox}.
	 *
	 * @return the "Force direct opening" {@link JComboBox}.
	 */
	private JComboBox<Boolean> getForceOpenCombobox() {
		if (forceOpenCombobox == null) {
			Boolean[] booleans = new Boolean[] {true, false};
			forceOpenCombobox = new JComboBox<>(booleans);
			forceOpenCombobox.setSelectedItem(Software.getUserConfiguration().isOpenFileDirectly());
		}
		return forceOpenCombobox;
	}

	/**
	 * Create if needed then return the tools max number of points {@link JTextField}.
	 *
	 * @return the tools max number of points {@link JTextField}.
	 */
	private JTextField getToolsMaxNbPointsTextField() {
		if (toolsMaxNbPointsTextField == null) {
			toolsMaxNbPointsTextField = new JTextField(
					String.valueOf(Software.getUserConfiguration().getToolsMaxNbPoints()));
			final TextFieldFormatFilter integerFilter = new TextFieldFormatFilter(
					TextFieldFormatFilter.INTEGER,
					String.valueOf(Software.getUserConfiguration().getToolsMaxNbPoints()),
					TextFieldFormatFilter.POSITIVE);
			toolsMaxNbPointsTextField.addKeyListener(integerFilter);
		}
		return toolsMaxNbPointsTextField;
	}

	/**
	 * Initializes the rendering {@link JComboBox}.
	 */
	private JComboBox<Rendering> getRenderingComboBox() {
		if (renderingComboBox == null) {
			renderingComboBox = new JComboBox<>(Rendering.values());
			String rendering = Software.getUserConfiguration().getRendering();
			renderingComboBox.setSelectedItem(Rendering.valueOf(rendering));
			renderingComboBox.setPreferredSize(new Dimension(300, 24));
			renderingComboBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					UserConfiguration userConf = Software.getUserConfiguration();
					userConf.setRendering((Rendering) renderingComboBox.getSelectedItem());
				}
			});
		}
		return renderingComboBox;
	}

	/**
	 * Save the configuration.
	 */
	public void saveConfiguration() {
		UserConfiguration conf = Software.getUserConfiguration();
		conf.setSampOnAutoStart(((Boolean)
				getSampComboBox().getSelectedItem()).booleanValue());
		conf.setOpenFileDirectly(((Boolean)
				getForceOpenCombobox().getSelectedItem()).booleanValue());
		conf.setToolsMaxNbPoints(Integer.parseInt(
				getToolsMaxNbPointsTextField().getText()));
		conf.setAutoOtherSpeciesDisplayed(((Boolean)
				getAutoOtherSpeciesComboBox().getSelectedItem()).booleanValue());
	}

	/**
	 * Create if needed then return the the auto other species displaying {@link JComboBox}.
	 *
	 * @return the auto OtherSpecies ComboBox {@link JComboBox}.
	 */
	private JComboBox<Boolean> getAutoOtherSpeciesComboBox() {
		if (autoOtherSpeciesComboBox == null) {
			Boolean[] booleans = new Boolean[] {true, false};
			autoOtherSpeciesComboBox = new JComboBox<>(booleans);
			autoOtherSpeciesComboBox.setSelectedItem(Software.getUserConfiguration().isAutoOtherSpeciesDisplayed());
		}
		return autoOtherSpeciesComboBox;
	}

}
