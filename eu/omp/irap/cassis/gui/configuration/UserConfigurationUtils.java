/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.configuration;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class UserConfigurationUtils {

	/**
	 * Create and configure a "line" of components with a {@link JLabel} and
	 * two others components.
	 *
	 * @param panel The panel where the line of components will be added.
	 * @param gbc The {@link GridBagConstraints} for the line.
	 * @param labelText The text to be set to the {@link JLabel}.
	 * @param toolTipText The tool tip text to be set to component 1 and label.
	 * @param comp1 The first component.
	 * @param comp2 The second component.
	 * @param line The indice of the line in the panel.
	 */
	public static void createLine(JPanel panel, GridBagConstraints gbc,
			String labelText, String toolTipText, JComponent comp1,
			JComponent comp2, int line) {
		setGridBagParameters(gbc, 0, line);
		gbc.weightx = 0.2;
		JLabel label = new JLabel(labelText);
		panel.add(label, gbc);

		setGridBagParameters(gbc, 1, line);
		gbc.weightx = 1.0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		panel.add(comp1, gbc);

		setGridBagParameters(gbc, 2, line);
		gbc.weightx = 0.1;
		gbc.fill = GridBagConstraints.NONE;
		if (comp2 == null) {
			panel.add(new JLabel(), gbc);
		} else {
			panel.add(comp2, gbc);
		}

		if (labelText != null && !labelText.isEmpty()) {
			label.setToolTipText(toolTipText);
			comp1.setToolTipText(toolTipText);
		}
	}




	/**
	 * Configure a {@link GridBagConstraints}.
	 *
	 * @param gbc The {@link GridBagConstraints} to configure.
	 * @param x The new gridx value.
	 * @param y The new gridy value.
	 */
	public static void setGridBagParameters(GridBagConstraints gbc, int x, int y) {
		gbc.gridx = x;
		gbc.gridy = y;
		gbc.gridwidth = 1;
		gbc.insets = new Insets(2, 2, 2, 2);
		gbc.anchor = GridBagConstraints.WEST;
	}

}
