/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.configuration.dbupdate;

import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import javax.swing.SwingWorker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.database.access.AccessDataBase;
import eu.omp.irap.cassis.database.access.DataBaseConnection;
import eu.omp.irap.cassis.database.access.InfoDataBase;
import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB;
import eu.omp.irap.cassis.database.access.TypeDataBase;

/**
 * SwingWorker to handle the change of a database.
 *
 * @author M. Boiziot
 */
public class DatabaseChangeWorker extends SwingWorker<Boolean, Void> {

	private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseChangeWorker.class);

	private String type;
	private String name;
	private boolean inMemory;


	/**
	 * Constructor.
	 *
	 * @param type The type of database to use (String version of {@link TypeDataBase}).
	 * @param name The name/path/URL of the dababase.
	 * @param inMemory If the database should be on memory (on SQLite only).
	 */
	public DatabaseChangeWorker(String type, String name, boolean inMemory) {
		super();
		this.type = type;
		this.name = name;
		this.inMemory = inMemory;
	}

	/**
	 * Background Thread, action:
	 * 	- Create a DatabaseConnecion and try to get all molecules.
	 *  - If the molecule list is not null and not empty (beside SLAP who have
	 *   no molecule list from start or NO database):
	 *    - set the database, update properties files, history and do refresh.
	 *
	 * @return true if the database is OK, false otherwise.
	 * @see javax.swing.SwingWorker#doInBackground()
	 */
	@Override
	protected Boolean doInBackground() throws Exception {
		if (!isDatabaseChangeNeeded()) {
			return true;
		}
		boolean dbOk = false;
		try {
			TypeDataBase typeDb = TypeDataBase.valueOf(type.toUpperCase());
			DataBaseConnection db = AccessDataBase.createDataBaseConnection(
					typeDb, name, inMemory);
			if (db != null) {
				List<SimpleMoleculeDescriptionDB> mols = db.getAllMoleculeDescriptionDB();
				if (mols != null && (!mols.isEmpty() ||
						(mols.isEmpty() && (typeDb == TypeDataBase.SLAP ||
						typeDb == TypeDataBase.NO)))) {
					InfoDataBase.getInstance().setDataBaseType(type, name, inMemory, db);
					dbOk = true;
				}
			}
		} catch (Exception e) {
			dbOk = false;
			LOGGER.warn("Error while trying to use the database", e);
		}
		return dbOk;
	}

	/**
	 * Return if a change of database is needed.
	 */
	private boolean isDatabaseChangeNeeded() {
		InfoDataBase current = InfoDataBase.getInstance();
		return current.getTypeDb() != TypeDataBase.valueOf(type.toUpperCase())
				|| !current.getDbname().equals(name) ||
				current.isInMemoryDatabase() != inMemory;
	}

	/**
	 * Return the result.
	 *
	 * @return true if the database is OK, false otherwise.
	 */
	public boolean getResult() {
		try {
			return get();
		} catch (CancellationException e) {
			LOGGER.debug("Worker canceled", e);
		} catch (InterruptedException e) {
			LOGGER.warn("InterruptionException", e);
		} catch (ExecutionException e) {
			LOGGER.warn("ExecutionException", e);
		}
		return false;
	}
}
