/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.configuration.dbupdate;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

import eu.omp.irap.cassis.database.access.TypeDataBase;

/**
 * Dialog to handle the change of the database.
 *
 * @author M. Boiziot
 */
public class DatabaseChangeDialog extends JDialog {

	private static final long serialVersionUID = -5722466729071772551L;

	private DatabaseChangeWorker worker;
	private JProgressBar progressBar;


	/**
	 * Constructor.
	 *
	 * @param window Parent window
	 * @param type The type of database to use (String version of {@link TypeDataBase}).
	 * @param name The name/path/URL of the dababase.
	 * @param inMemory If the database should be on memory (on SQLite only).
	 */
	public DatabaseChangeDialog(Window window, String type, String name, boolean inMemory) {
		super(window, "Database initialization...", ModalityType.APPLICATION_MODAL);
		createWorker(type, name, inMemory);
		createUi();
		setLocationRelativeTo(window);
		setSize(320, 60);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				if (!worker.isDone()) {
					worker.cancel(true);
				}
			}
		});
	}

	/**
	 * Create the graphical user interface of the dialog.
	 */
	private final void createUi() {
		Container container = this.getContentPane();
		container.setLayout(new BorderLayout());
		container.add(getProgressBar(), BorderLayout.CENTER);
	}

	/**
	 * Creates if needed then return the {@link JProgressBar}.
	 *
	 * @return the {@link JProgressBar}.
	 */
	public JProgressBar getProgressBar() {
		if (progressBar == null) {
			progressBar = new JProgressBar(0, 100);
			progressBar.setIndeterminate(true);

			progressBar.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
		}
		return progressBar;
	}

	/**
	 * Create the worker.
	 *
	 * @param type The type of database to use (String version of {@link TypeDataBase}).
	 * @param name The name/path/URL of the dababase.
	 * @param inMemory If the database should be on memory (on SQLite only).
	 */
	private final void createWorker(String type, String name, boolean inMemory) {
		worker = new DatabaseChangeWorker(type, name, inMemory);
		worker.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if ("state".equals(evt.getPropertyName()) &&
						SwingWorker.StateValue.DONE == evt.getNewValue()) {
					DatabaseChangeDialog.this.setVisible(false);
					DatabaseChangeDialog.this.dispose();
				}
			}
		});
	}

	/**
	 * Start the worker and set the dialog visible.
	 */
	public void start() {
		worker.execute();
		setVisible(true);
	}

	/**
	 * Return the result.
	 *
	 * @return true if the database is OK, false otherwise.
	 */
	public boolean getResult() {
		return worker.getResult();
	}
}
