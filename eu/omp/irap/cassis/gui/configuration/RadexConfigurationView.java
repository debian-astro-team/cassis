/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.configuration;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;

import eu.omp.irap.cassis.gui.util.TextFieldFormatFilter;
import eu.omp.irap.cassis.properties.Software;
import eu.omp.irap.cassis.properties.UserConfiguration;

/**
 * RADEX tab for the configuration.
 *
 * @author M. Boiziot
 */
public class RadexConfigurationView extends JPanel {

	private static final long serialVersionUID = -6686512565475842796L;

	private JTextField radexFortranPathTextField;
	private JButton radexFortranPathButton;
	private JTextField radexCollisionPathTextField;
	private JButton radexCollisionPathButton;
	private JTextField radexMaxPartTextField;
	private JTextField radexMaxTempTextField;
	private JTextField radexMaxLevTextField;
	private JTextField radexMaxLineTextField;
	private JTextField radexMaxCollTextField;


	/**
	 * Constructor.
	 */
	public RadexConfigurationView() {
		super(new GridBagLayout());
		setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		createRadexPanel();
	}

	/**
	 * Create the RADEX panel.
	 */
	private void createRadexPanel() {
		GridBagConstraints gbc = new GridBagConstraints();
		int line = 0;
		UserConfigurationUtils.createLine(this, gbc, "Radex Fortran path:",
				"The path to the Fortran folder",
				getRadexFortranPathTextField(),
				getRadexFortranPathButton(), line);
		line++;
		UserConfigurationUtils.createLine(this, gbc, "Radex Collisions path:",
				"The path to the Fortran folder",
				getRadexCollisionPathTextField(),
				getRadexCollisionPathButton(), line);
		line++;
		UserConfigurationUtils.createLine(this, gbc, "Radex Max Part:",
				"Maximum no. of collision partners",
				getRadexMaxPartTextField(), null, line);
		line++;
		UserConfigurationUtils.createLine(this, gbc, "Radex Max Temp:",
				"Maximum no. of collision temperatures",
				getRadexMaxTempTextField(), null, line);
		line++;
		UserConfigurationUtils.createLine(this, gbc, "Radex Max Lev:",
				"Maximum no. of energy levels",
				getRadexMaxLevTextField(), null, line);
		line++;
		UserConfigurationUtils.createLine(this, gbc, "Radex Max Line:",
				"Maximum no. of radiative transitions",
				getRadexMaxLineTextField(), null, line);
		line++;
		UserConfigurationUtils.createLine(this, gbc, "Radex Max Coll:",
				"Maximum no. of collisional transitions",
				getRadexMaxCollTextField(), null, line);
	}

	/**
	 * Create if needed then return the RADEX path {@link JTextField}.
	 *
	 * @return the RADEX path {@link JTextField}.
	 */
	private JTextField getRadexFortranPathTextField() {
		if (radexFortranPathTextField == null) {
			radexFortranPathTextField = new JTextField(
					Software.getUserConfiguration().getRadexFortranPath());
		}
		return radexFortranPathTextField;
	}

	/**
	 * Create if needed then return the RADEX path {@link JButton}.
	 *
	 * @return the RADEX path {@link JButton}.
	 */
	private JButton getRadexFortranPathButton() {
		if (radexFortranPathButton == null) {
			radexFortranPathButton = new JButton("...");
			radexFortranPathButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					String path = getRadexFortranPathTextField().getText();
					File folder = selectFolder(path, "Select Fortran folder");
					if (folder != null) {
						getRadexFortranPathTextField().setText(folder.getAbsolutePath());
					}
				}
			});
		}
		return radexFortranPathButton;
	}

	/**
	 * Create if needed then return the RADEX collision path {@link JButton}.
	 *
	 * @return the RADEX collsion path {@link JButton}.
	 */
	private JButton getRadexCollisionPathButton() {
		if (radexCollisionPathButton == null) {
			radexCollisionPathButton = new JButton("...");
			radexCollisionPathButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					String path = getRadexCollisionPathTextField().getText();
					File selectedFolder = selectFolder(path, "Select Collisions folder");
					if (selectedFolder != null) {
						getRadexCollisionPathTextField().setText(selectedFolder.getAbsolutePath());
					}
				}
			});
		}
		return radexCollisionPathButton;
	}

	/**
	 * Create if needed then return the RADEX collision path {@link JTextField}.
	 *
	 * @return the RADEX collision path {@link JTextField}.
	 */
	private JTextField getRadexCollisionPathTextField() {
		if (radexCollisionPathTextField == null) {
			radexCollisionPathTextField = new JTextField(
					Software.getUserConfiguration().getRadexCollisionPath());
		}
		return radexCollisionPathTextField;
	}

	/**
	 * Create if needed then return the RADEX max part {@link JTextField}.
	 *
	 * @return the RADEX max part {@link JTextField}.
	 */
	private JTextField getRadexMaxPartTextField() {
		if (radexMaxPartTextField == null) {
			radexMaxPartTextField = new JTextField(
					String.valueOf(Software.getUserConfiguration().getRadexDataMaxPart()));
			final TextFieldFormatFilter integerFilter = new TextFieldFormatFilter(
					TextFieldFormatFilter.INTEGER,
					String.valueOf(Software.getUserConfiguration().getRadexDataMaxPart()),
					TextFieldFormatFilter.POSITIVE);
			radexMaxPartTextField.addKeyListener(integerFilter);
		}
		return radexMaxPartTextField;
	}

	/**
	 * Create if needed then return the RADEX max temp {@link JTextField}.
	 *
	 * @return the RADEX max temp {@link JTextField}.
	 */
	private JTextField getRadexMaxTempTextField() {
		if (radexMaxTempTextField == null) {
			radexMaxTempTextField = new JTextField(
					String.valueOf(Software.getUserConfiguration().getRadexDataMaxTemp()));
			final TextFieldFormatFilter integerFilter = new TextFieldFormatFilter(
					TextFieldFormatFilter.INTEGER,
					String.valueOf(Software.getUserConfiguration().getRadexDataMaxTemp()),
					TextFieldFormatFilter.POSITIVE);
			radexMaxTempTextField.addKeyListener(integerFilter);
		}
		return radexMaxTempTextField;
	}

	/**
	 * Create if needed then return the RADEX max lev {@link JTextField}.
	 *
	 * @return the RADEX max lev {@link JTextField}.
	 */
	private JTextField getRadexMaxLevTextField() {
		if (radexMaxLevTextField == null) {
			radexMaxLevTextField = new JTextField(
					String.valueOf(Software.getUserConfiguration().getRadexDataMaxLev()));
			final TextFieldFormatFilter integerFilter = new TextFieldFormatFilter(
					TextFieldFormatFilter.INTEGER,
					String.valueOf(Software.getUserConfiguration().getRadexDataMaxLev()),
					TextFieldFormatFilter.POSITIVE);
			radexMaxLevTextField.addKeyListener(integerFilter);
		}
		return radexMaxLevTextField;
	}

	/**
	 * Create if needed then return the RADEX max line {@link JTextField}.
	 *
	 * @return the RADEX max line {@link JTextField}.
	 */
	private JTextField getRadexMaxLineTextField() {
		if (radexMaxLineTextField == null) {
			radexMaxLineTextField = new JTextField(
					String.valueOf(Software.getUserConfiguration().getRadexDataMaxLine()));
			final TextFieldFormatFilter integerFilter = new TextFieldFormatFilter(
					TextFieldFormatFilter.INTEGER,
					String.valueOf(Software.getUserConfiguration().getRadexDataMaxLine()),
					TextFieldFormatFilter.POSITIVE);
			radexMaxLineTextField.addKeyListener(integerFilter);
		}
		return radexMaxLineTextField;
	}

	/**
	 * Create if needed then return the RADEX max coll {@link JTextField}.
	 *
	 * @return the RADEX max coll {@link JTextField}.
	 */
	private JTextField getRadexMaxCollTextField() {
		if (radexMaxCollTextField == null) {
			radexMaxCollTextField = new JTextField(
					String.valueOf(Software.getUserConfiguration().getRadexDataMaxColl()));
			final TextFieldFormatFilter integerFilter = new TextFieldFormatFilter(
					TextFieldFormatFilter.INTEGER,
					String.valueOf(Software.getUserConfiguration().getRadexDataMaxColl()),
					TextFieldFormatFilter.POSITIVE);
			radexMaxCollTextField.addKeyListener(integerFilter);
		}
		return radexMaxCollTextField;
	}

	/**
	 * Save the configuration.
	 */
	public void saveConfiguration() {
		UserConfiguration userConf = Software.getUserConfiguration();
		String pathFortran = getRadexFortranPathTextField().getText();
		userConf.setRadexFortranPath(pathFortran);
		userConf.setRadexCollisionPath(getRadexCollisionPathTextField().getText());
		userConf.setRadexDataMaxPart(Integer.parseInt(getRadexMaxPartTextField().getText()));
		userConf.setRadexDataMaxTemp(Integer.parseInt(getRadexMaxTempTextField().getText()));
		userConf.setRadexDataMaxLev(Integer.parseInt(getRadexMaxLevTextField().getText()));
		userConf.setRadexDataMaxLine(Integer.parseInt(getRadexMaxLineTextField().getText()));
		userConf.setRadexDataMaxColl(Integer.parseInt(getRadexMaxCollTextField().getText()));
		userConf.updateRadexConfig();
	}

	/**
	 * Select a folder then return the selected File for the folder.
	 *
	 * @param path String base path open the file chooser.
	 * @param title The title to set.
	 * @return The selected File for the folder, or null if none selected.
	 */
	private File selectFolder(String path, String title) {
		if (path == null || path.isEmpty()) {
			path = System.getProperty("user.home");
		}
		JFileChooser fc = new JFileChooser(path);
		fc.setDialogTitle(title);
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		if (fc.showOpenDialog(RadexConfigurationView.this) == JFileChooser.APPROVE_OPTION) {
			return fc.getSelectedFile();
		}
		return null;
	}

}
