/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.configuration;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.database.access.DatabaseHistory;
import eu.omp.irap.cassis.database.access.DatabaseProperties;
import eu.omp.irap.cassis.database.access.InfoDataBase;
import eu.omp.irap.cassis.database.access.TypeDataBase;
import eu.omp.irap.cassis.database.creation.importation.gui.add.VamdcSelectionPanel;
import eu.omp.irap.cassis.database.creation.importation.options.vamdc.services.Provider;
import eu.omp.irap.cassis.gui.configuration.dbupdate.DatabaseChangeDialog;
import eu.omp.irap.cassis.gui.model.table.JCassisTableConfiguration;
import eu.omp.irap.cassis.gui.model.table.TableListenerList;
import eu.omp.irap.cassis.gui.template.JComboBoxTemplate;
import eu.omp.irap.cassis.gui.template.ListTemplateManager;
import eu.omp.irap.cassis.properties.Software;
import eu.omp.irap.cassis.template.SqlTemplateManager;

/**
 * Database tab for the configuration.
 *
 * @author M. Boiziot
 */
public class DatabaseConfigurationView extends JPanel {

	private static final long serialVersionUID = 4281200391000448134L;
	private static final Logger LOGGER =
			LoggerFactory.getLogger(DatabaseConfigurationView.class);

	private JComboBox<String> dbPathComboBox;
	private JButton dbFileButton;
	private JComboBox<TypeDataBase> typeDatabaseComboBox;
	private DatabaseHistory databaseHistory;
	private JCheckBox dbInMemoryCheckBox;
	private JComboBox<String> userDefaultTemplateComboBox;
	private Map<String, JButton> mapSourceColor;
	private JComboBox<Boolean> forceMiniDbCombobox;
	private boolean isSqlite;


	/**
	 * Constructor.
	 */
	public DatabaseConfigurationView() {
		super(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		isSqlite = TypeDataBase.SQLITE == getTypeDataBaseSelected();
		databaseHistory = DatabaseHistory.getInstance();
		mapSourceColor = new Hashtable<>();
		createContent();
	}

	/**
	 * Create the content of the panel.
	 */
	private void createContent() {
		add(createTopPanel(), BorderLayout.CENTER);
		add(createTablePanel(), BorderLayout.SOUTH);
	}

	/**
	 * Create the top panel.
	 */
	private JPanel createTopPanel() {
		JPanel panel = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		addComponent(panel, gbc, 0, "Database Type:   ", null,
				getTypeDatabaseComboBox(), null);
		addComponent(panel, gbc, 1, "Database Path:   ", null,
				getDbPathComboBox(), getDbFileButton());
		int y = 2;
		if (isSqlite) {
			String labelToolTip = "<html>For SQLite database only, copy the"
					+ " database in memory on start.<br/>This improves a lot the"
					+ " speed of queries but use a lot of RAM.</html>";
			addComponent(panel, gbc, y, "In-memory database: ", labelToolTip,
					getDbInMemoryCheckBox(), null);
			y++;
		}

		if (Software.isOnlineMode()) {
			String labelToolTip = "On online mode only: if set to true, always"
					+ " start CASSIS with a minimal database.";
			addComponent(panel, gbc, y, "Force minimal database:   ",
					labelToolTip, getForceMiniDbCombobox(), null);
			y++;
		}
		addComponent(panel, gbc, y, "Default template:",
				"Change the template to use by default.",
				getUserDefaultTemplateComboBox(), null);
		return panel;
	}

	/**
	 * Add a new component.
	 *
	 * @param panel The panel where to add the component.
	 * @param gbc The constraints.
	 * @param index The index of the component.
	 * @param label The label to set for the component.
	 * @param labelToolTip The tool tip to set for the label, null to do not set tool tip.
	 * @param centerComponent The center component.
	 * @param rightComponent The right component.
	 */
	private void addComponent(JPanel panel, GridBagConstraints gbc, int index,
			String label, String labelToolTip, JComponent centerComponent,
			JComponent rightComponent) {
		addLeftComponent(panel, gbc, index, label, labelToolTip);
		addCenterComponent(panel, gbc, index, centerComponent);
		if (rightComponent != null) {
			addRightComponent(panel, gbc, index, rightComponent);
		}
	}

	/**
	 * Add a left component (label).
	 *
	 * @param panel The panel where to add the component.
	 * @param gbc The constraints.
	 * @param index The index of the component.
	 * @param labelText The text to set to the label.
	 * @param labelToolTip The tool tip to set to the label.
	 */
	private void addLeftComponent(JPanel panel, GridBagConstraints gbc,
			int index, String labelText, String labelToolTip) {
		UserConfigurationUtils.setGridBagParameters(gbc, 0, index);
		gbc.weightx = 0.2;
		gbc.fill = GridBagConstraints.NONE;
		JLabel label = new JLabel(labelText);
		if (labelToolTip != null) {
			label.setToolTipText(labelToolTip);
		}
		panel.add(label, gbc);
	}

	/**
	 * Add a center component (main component).
	 *
	 * @param panel The panel where to add the component.
	 * @param gbc The constraints.
	 * @param index The index of the component.
	 * @param component The component to add.
	 */
	private void addCenterComponent(JPanel panel, GridBagConstraints gbc,
			int index, JComponent component) {
		UserConfigurationUtils.setGridBagParameters(gbc, 1, index);
		gbc.weightx = 1.0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		panel.add(component, gbc);
	}

	/**
	 * Add a right component.
	 *
	 * @param panel The panel where to add the component.
	 * @param gbc The constraints.
	 * @param index The index of the component.
	 * @param component The component to add.
	 */
	private void addRightComponent(JPanel panel, GridBagConstraints gbc,
			int index, JComponent component) {
		UserConfigurationUtils.setGridBagParameters(gbc, 2, index);
		gbc.weightx = 0.1;
		gbc.fill = GridBagConstraints.NONE;
		panel.add(component, gbc);
	}

	/**
	 * Create then return the panel with the boutons allowing to change the
	 *  color of a database.
	 *
	 * @return the table panel.
	 */
	private JPanel createTablePanel() {
		Set<String> listSource = SqlTemplateManager.getInstance().getAllSources();
		final JCassisTableConfiguration tableConf = JCassisTableConfiguration.getInstance();

		JPanel mainPanel = new JPanel(new BorderLayout());
		JPanel tablePanel = new JPanel(new GridLayout(3, listSource.size() / 3, 5, 5));
		mainPanel.add(new JLabel("Species sources colors: "), BorderLayout.WEST);
		mainPanel.add(tablePanel, BorderLayout.CENTER);

		for (final String source : listSource) {
			final JButton button = new JButton(source);
			button.setSize(new Dimension(100, 24));
			Color color = tableConf.getColorFor(source);
			button.setForeground(color);
			mapSourceColor.put(source, button);

			button.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					Color color = JColorChooser.showDialog(DatabaseConfigurationView.this,
							"Select the new color for " + source + ".",
							tableConf.getColorFor(source));
					if (color != null) {
						button.setForeground(color);
					}
				}
			});
			tablePanel.add(button);
		}
		return mainPanel;
	}

	/**
	 * This method initializes typeDatabaseComboBox.
	 */
	private JComboBox<TypeDataBase> getTypeDatabaseComboBox() {
		if (typeDatabaseComboBox == null) {
			typeDatabaseComboBox = new JComboBox<>(TypeDataBase.values());
			typeDatabaseComboBox.setSelectedItem(InfoDataBase.getInstance().getTypeDb());
			typeDatabaseComboBox.setPreferredSize(new Dimension(300, 24));
			typeDatabaseComboBox.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					typeDatabaseChange((TypeDataBase) typeDatabaseComboBox.getSelectedItem());
				}
			});
		}
		return typeDatabaseComboBox;
	}

	/**
	 * This method initializes dbPathComboBox.
	 */
	private JComboBox<String> getDbPathComboBox() {
		if (dbPathComboBox == null) {
			TypeDataBase typeDb = getTypeDataBaseSelected();
			List<String> list = databaseHistory.getDb(typeDb);
			String dbName = InfoDataBase.getInstance().getDbname();
			if (!list.contains(dbName)) {
				list.add(0, dbName);
			}
			dbPathComboBox = new JComboBox<>(list.toArray(new String[list.size()]));
			dbPathComboBox.setSelectedItem(dbName);
			dbPathComboBox.setPreferredSize(new Dimension(300, 24));
			dbPathComboBox.setEditable(true);
			if (typeDb == TypeDataBase.SLAP) {
				addSlapHardcodedServices(dbPathComboBox);
			}
		}
		return dbPathComboBox;
	}

	/**
	 * This method initializes dbFileButton.
	 */
	private JButton getDbFileButton() {
		if (dbFileButton == null) {
			dbFileButton = new JButton("...");
			dbFileButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (TypeDataBase.VAMDC == getTypeDataBaseSelected()) {
						displayVamdcSelectionDialog();
					} else if (TypeDataBase.FILE == getTypeDataBaseSelected()) {
						displayDatabaseFileSelection(true, "Select database folder");
					} else {
						displayDatabaseFileSelection(false, "Select database file");
					}
				}
			});
		}
		return dbFileButton;
	}

	/**
	 * Create if needed then return the "In memory" {@link JCheckBox}.
	 *
	 * @return the "In memory" {@link JCheckBox}.
	 */
	private JCheckBox getDbInMemoryCheckBox() {
		if (dbInMemoryCheckBox == null) {
			dbInMemoryCheckBox = new JCheckBox();
			dbInMemoryCheckBox.setSelected(InfoDataBase.getInstance().isInMemoryDatabase());
		}
		return dbInMemoryCheckBox;
	}

	/**
	 * Change the database type.
	 *
	 * @param newTypeDb the new database type.
	 */
	private void typeDatabaseChange(TypeDataBase newTypeDb) {
		boolean wasSqlite = isSqlite;
		isSqlite = newTypeDb == TypeDataBase.SQLITE;
		if (wasSqlite && newTypeDb != TypeDataBase.SQLITE) {
			recreateDatabasePanel();
		} else if (!wasSqlite && newTypeDb == TypeDataBase.SQLITE) {
			recreateDatabasePanel();
		}
		String itemToSelect = null;
		List<String> list = databaseHistory.getDb(newTypeDb);
		DefaultComboBoxModel<String> model =
				(DefaultComboBoxModel<String>) getDbPathComboBox().getModel();

		/* If the current path is not the current used and not exist in history
		we can thinks the user change the path before the type... So keep it */
		String dbName = InfoDataBase.getInstance().getDbname();
		if (!dbName.equals(getDbPathComboBox().getSelectedItem())
				&& !databaseHistory.exists((String)getDbPathComboBox().getSelectedItem())) {
			itemToSelect = (String) getDbPathComboBox().getSelectedItem();
		}

		getDbPathComboBox().removeAllItems();

		// Retore old path if necessary.
		if (itemToSelect != null) {
			model.addElement(itemToSelect);
		} else if (newTypeDb == InfoDataBase.getInstance().getTypeDb()) {
			model.addElement(InfoDataBase.getInstance().getDbname());
			itemToSelect = InfoDataBase.getInstance().getDbname();
		}

		// Add element from the history.
		for (String path : list) {
			if (!path.equals(itemToSelect)) {
				model.addElement(path);
			}
		}

		if (newTypeDb == TypeDataBase.SLAP) {
			addSlapHardcodedServices(getDbPathComboBox());
		}

		if (itemToSelect != null) {
			getDbPathComboBox().setSelectedItem(itemToSelect);
		}
	}

	/**
	 * Add some SLAP services to the JComboBox if they are not already there.
	 *
	 * @param combobox The JComboBox.
	 */
	private void addSlapHardcodedServices(JComboBox<String> combobox) {
		DefaultComboBoxModel<String> model =
				(DefaultComboBoxModel<String>) combobox.getModel();
		String[] elementsToAdd = { "https://find.nrao.edu/splata-slap/slap?",
				"http://physics.nist.gov/cgi-bin/ASD/slap.pl?" };
		for (String service : elementsToAdd) {
			if (model.getIndexOf(service) == -1) {
				model.addElement(service);
			}
		}
	}

	/**
	 * Return the {@link TypeDataBase} currently selected in the corresponding
	 *  {@link JComboBox}.
	 *
	 * @return the {@link TypeDataBase} currently selected in the corresponding
	 *  {@link JComboBox}.
	 */
	private TypeDataBase getTypeDataBaseSelected() {
		return (TypeDataBase) getTypeDatabaseComboBox().getSelectedItem();
	}

	/**
	 * Return the path of the current database folder if it exists or the
	 *  default database path.
	 *
	 * @return The path of the current database folder if it exists or the
	 *  default database path.
	 */
	private String getDatabaseFolderPath() {
		String nameDb = InfoDataBase.getInstance().getDbname();
		String path = new File(nameDb).getParent();
		if (path == null) {
			path = new File(new File(nameDb).getAbsolutePath()).getParent();
			if (path == null) {
				path = DatabaseProperties.getDefaultPath();
			}
		}
		return path;
	}

	/**
	 * Configure the given file chooser.
	 *
	 * @param fc The file chooser.
	 * @param allowFolder True to allow selection of folder, false to allow only file.
	 * @param title The title to set.
	 */
	private void configureFileChooser(JFileChooser fc, boolean allowFolder, String title) {
		fc.setDialogTitle(title);
		if (allowFolder) {
			fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		} else {
			fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		}
	}

	/**
	 * Display the FileChooser to select a database file.
	 */
	private void displayDatabaseFileSelection(boolean allowFolder, String title) {
		String path = getDatabaseFolderPath();
		JFileChooser fc = new CassisJFileChooser(Software.getDatabasePath(), path);
		configureFileChooser(fc, allowFolder, title);
		if (fc.showOpenDialog(DatabaseConfigurationView.this) == JFileChooser.APPROVE_OPTION) {
			File selectedFile = fc.getSelectedFile();
			if (selectedFile != null) {
				String absPath = selectedFile.getAbsolutePath();
				addDatabaseInComboBox(absPath);
			}
		}
	}

	/**
	 * Add an element in the database JComboBox if it is not already in it then
	 *  select it.
	 *
	 * @param element The element to add and select.
	 */
	private void addDatabaseInComboBox(String element) {
		DefaultComboBoxModel<String> mod =
				(DefaultComboBoxModel<String>) getDbPathComboBox().getModel();
		if (mod.getIndexOf(element) == -1) {
			mod.addElement(element);
		}
		getDbPathComboBox().setSelectedItem(element);
	}

	/**
	 * Create if needed then return the user default template {@link JComboBox}.
	 *
	 * @return the user default template {@link JComboBox}.
	 */
	private JComboBox<String> getUserDefaultTemplateComboBox() {
		if (userDefaultTemplateComboBox == null) {
			userDefaultTemplateComboBox = new JComboBoxTemplate(false);
			String template = Software.getUserConfiguration().getUserDefaultTemplate();
			DefaultComboBoxModel<String> templateComboBoxModel =
					(DefaultComboBoxModel<String>) userDefaultTemplateComboBox.getModel();
			if (templateComboBoxModel.getIndexOf(template) == -1) {
				userDefaultTemplateComboBox.addItem(template);
			}
			userDefaultTemplateComboBox.setSelectedItem(template);
		}
		return userDefaultTemplateComboBox;
	}

	/**
	 * Display the VAMDC selection dialog.
	 */
	private void displayVamdcSelectionDialog() {
		final VamdcSelectionPanel panel = new VamdcSelectionPanel();
		panel.getValidateButton().setText("Select");
		Window parentWindow = SwingUtilities.getWindowAncestor(DatabaseConfigurationView.this);
		final JDialog dialog = new JDialog(parentWindow, "VAMDC service selection");
		dialog.setContentPane(panel);
		panel.getValidateButton().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JList<Provider> jList = panel.getProviderList();
				if (jList.getSelectedIndex() != -1) {
					addDatabaseInComboBox(jList.getSelectedValue().getUrl());
				}
				dialog.dispose();
			}
		});

		dialog.setPreferredSize(new Dimension(500, 300));
		dialog.pack();
		dialog.setVisible(true);
	}

	/**
	 * Recreate a new database panel.
	 */
	private void recreateDatabasePanel() {
		this.removeAll();
		createContent();
		this.repaint();
	}

	/**
	 * Create if needed then return the "Force use of minimal database" {@link JComboBox}.
	 *
	 * @return the "Force use of minimal database" JComboBox.
	 */
	private JComboBox<Boolean> getForceMiniDbCombobox() {
		if (forceMiniDbCombobox == null) {
			Boolean[] booleans = new Boolean[] {true, false};
			forceMiniDbCombobox = new JComboBox<>(booleans);
			forceMiniDbCombobox.setSelectedItem(
					Software.getUserConfiguration().isForceMiniDbOnOnlineMode());
		}
		return forceMiniDbCombobox;
	}

	/**
	 * Save the configured color for the species.
	 */
	private void saveSpeciesColor() {
		// Species sources colors :
		JCassisTableConfiguration conf = JCassisTableConfiguration.getInstance();
		for (String string : mapSourceColor.keySet()) {
			conf.addElement(string, mapSourceColor.get(string).getForeground().getRGB());
		}
		conf.saveConfig();
		TableListenerList.getInstance().fireRefreshColor();
	}

	/**
	 * Save the configuration.
	 *
	 * @return true is the save is OK, false otherwise.
	 */
	public boolean saveConfiguration() {
		boolean res = saveDatabase();
		if (res) {
			saveTemplate();
			saveSpeciesColor();
		}
		return res;
	}

	/**
	 * Save template settings, displaying information message if needed.
	 */
	private void saveTemplate() {
		String template = (String)
				getUserDefaultTemplateComboBox().getSelectedItem();
		boolean change = !ListTemplateManager.getInstance()
				.getDefaultTemplateToUse().equals(template);
		if (change) {
			Software.getUserConfiguration().setUserDefaultTemplate(template);
			JOptionPane.showMessageDialog(this,
					"Please restart CASSIS for the change to take effect.",
					"Restart", JOptionPane.INFORMATION_MESSAGE);
		}
	}
	/**
	 * Save the database configuration.
	 *
	 * @return true is the save is OK, false otherwise.
	 */
	private boolean saveDatabase() {
		if (Software.isOnlineMode()) {
			boolean forceMiniDb = (Boolean)getForceMiniDbCombobox().getSelectedItem();
			Software.getUserConfiguration().setForceMiniDbOnOnlineMode(forceMiniDb);
		}

		boolean dbInMemory = getDbInMemoryCheckBox().isSelected();

		String dbname = null;
		if (getDbPathComboBox().getSelectedItem() != null) {
			dbname = ((String)getDbPathComboBox().getSelectedItem()).trim();
		} else {
			dbname = "";
		}
		TypeDataBase typeDatabase = getTypeDataBaseSelected();
		if (!"".equals(dbname) || typeDatabase == TypeDataBase.NO) {
			Window parent = SwingUtilities.getWindowAncestor(this);
			DatabaseChangeDialog dcd = new DatabaseChangeDialog(parent, typeDatabase.name(), dbname, dbInMemory);
			dcd.start();
			boolean res = dcd.getResult();
			if (!res) {
				JOptionPane.showMessageDialog(this,
						"Error with the configured database. Using the previous one.",
						"Database error", JOptionPane.ERROR_MESSAGE);
			}
			return res;
		} else {
			LOGGER.info("Selected database file is null");
			JOptionPane.showMessageDialog(DatabaseConfigurationView.this,
					"Select a database file before saving.",
					"Information", JOptionPane.INFORMATION_MESSAGE);
			return false;
		}
	}

	/**
	 * Clear the database history.
	 */
	public void clearHistory() {
		databaseHistory.clear();
	}
}
