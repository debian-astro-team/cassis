/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.configuration;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.WindowConstants;

import eu.omp.irap.cassis.common.gui.ScreenUtil;
import eu.omp.irap.cassis.gui.PanelFrame;
import eu.omp.irap.cassis.gui.template.ManageTemplateControl;

@SuppressWarnings("serial")
public class UserConfigurationView extends JDialog {

	public static final int TAB_GENERAL_INDEX = 0;
	public static final int TAB_DATABASE_INDEX = 1;
	public static final int TAB_RADEX_INDEX = 2;
	public static final int TAB_FIT_INDEX = 3;

	private static boolean instanced = false;
	private static UserConfigurationView usrConfigView;

	private JPanel contentPane;
	private JPanel south;
	private JButton saveButton;
	private JButton cancelButton;
	private JTabbedPane tabbedPane;
	private DatabaseConfigurationView databaseConfigurationView;
	private GeneralConfigurationView generalConfigurationView;
	private RadexConfigurationView radexConfigurationView;
	private FitConfigurationView fitConfigurationView;


	/**
	 * Constructor.
	 *
	 * @param owner The {@link JFrame}
	 */
	public UserConfigurationView(JFrame owner) {
		super(owner);
		createGui();
	}

	/**
	 * Create the GUI.
	 */
	private void createGui() {
		setTitle("Preferences");

		setContentPane(getJContentPane());
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				exit();
			}
		});
		setPreferredSize(new Dimension(560, 320));
		ScreenUtil.center(getOwner(), this);
		pack();
		setModal(true);
		setAlwaysOnTop(true);
	}

	/**
	 * Dispose the {@link UserConfigurationView}.
	 */
	private void exit() {
		getDatabaseConfigurationView().clearHistory();
		instanced = false;
		dispose();
	}

	/**
	 * This method initializes contentPane.
	 */
	private JPanel getJContentPane() {
		if (contentPane == null) {
			contentPane = new JPanel();
			contentPane.setLayout(new BorderLayout());
			contentPane.add(getTabbedPane(), BorderLayout.CENTER);
			contentPane.add(getSouthPanel(), BorderLayout.SOUTH);
		}
		return contentPane;
	}

	/**
	 * This method initializes southPanel.
	 */
	private JPanel getSouthPanel() {
		if (south == null) {
			south = new JPanel();
			south.setLayout(new FlowLayout(FlowLayout.RIGHT));
			south.add(getSaveButton());
			south.add(getCancelButton());
		}
		return south;
	}

	/**
	 * This method initializes saveButton.
	 */
	private JButton getSaveButton() {
		if (saveButton == null) {
			saveButton = new JButton();
			saveButton.setText("Save");
			saveButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					doSave();
				}
			});
		}
		return saveButton;
	}

	/**
	 * This method initializes cancelButton.
	 */
	private JButton getCancelButton() {
		if (cancelButton == null) {
			cancelButton = new JButton();
			cancelButton.setText("Cancel");
			cancelButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					exit();
				}
			});
		}
		return cancelButton;
	}

	/**
	 * Create an instance of {@link UserConfigurationView}.
	 *
	 * @param owner The owner.
	 * @param selectedTab The selected tab. Some constants are available :
	 *  TAB_GENERAL_INDEX, TAB_DATABASE_INDEX, TAB_RADEX_INDEX.
	 */
	public static void createInstance(JFrame owner, int selectedTab) {
		if (!instanced) {
			instanced = true;
			usrConfigView = new UserConfigurationView(owner);
			usrConfigView.selectTab(selectedTab);
			usrConfigView.setVisible(true);
		}
	}

	/**
	 * Create an instance of {@link UserConfigurationView}.
	 *
	 * @param owner The owner.
	 */
	public static void createInstance(JFrame owner) {
		createInstance(owner, TAB_GENERAL_INDEX);
	}

	/**
	 * Update the titles of the CASSIS frames.
	 */
	public static void doSaveUpdateTitle() {
		if (PanelFrame.getInstance() != null) {
			PanelFrame.getInstance().updateTitle();
		}
		if (ManageTemplateControl.isInstanceExists()) {
			ManageTemplateControl.getInstance().updateTitle();
		}
	}

	/**
	 * Save the parameters.
	 */
	private void doSave() {
		if (!getDatabaseConfigurationView().saveConfiguration()) {
			return;
		}
		doSaveUpdateTitle();
		getGeneralConfigurationView().saveConfiguration();
		getRadexConfigurationView().saveConfiguration();
		getFitConfigurationView().saveConfiguration();
		JOptionPane.showMessageDialog(UserConfigurationView.this,
				"Your preferences have been updated.",
				"Information", JOptionPane.INFORMATION_MESSAGE);
		exit();
	}

	/**
	 * Create if needed then return the database configuration view.
	 *
	 * @return the DatabaseConfigurationView.
	 */
	private DatabaseConfigurationView getDatabaseConfigurationView() {
		if (databaseConfigurationView == null) {
			databaseConfigurationView = new DatabaseConfigurationView();
		}
		return databaseConfigurationView;
	}

	/**
	 * Create if needed then return the general configuration view.
	 *
	 * @return the general configuration view.
	 */
	private GeneralConfigurationView getGeneralConfigurationView() {
		if (generalConfigurationView == null) {
			generalConfigurationView = new GeneralConfigurationView();
		}
		return generalConfigurationView;
	}

	/**
	 * Create if needed then return the RADEX configuration view.
	 *
	 * @return the RADEX configuration view.
	 */
	private RadexConfigurationView getRadexConfigurationView() {
		if (radexConfigurationView == null) {
			radexConfigurationView = new RadexConfigurationView();
		}
		return radexConfigurationView;
	}

	private FitConfigurationView getFitConfigurationView() {
		if (fitConfigurationView == null) {
			fitConfigurationView = new FitConfigurationView();
		}
		return fitConfigurationView;
	}

	/**
	 * Create if needed then return the tab pane.
	 *
	 * @return the tab pane.
	 */
	private JTabbedPane getTabbedPane() {
		if (tabbedPane == null) {
			tabbedPane = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.SCROLL_TAB_LAYOUT);
			tabbedPane.insertTab("General", null, getGeneralConfigurationView(),
					"General settings", TAB_GENERAL_INDEX);
			tabbedPane.insertTab("Database Settings", null, getDatabaseConfigurationView(),
					"Database settings", TAB_DATABASE_INDEX);
			tabbedPane.insertTab("Radex", null, getRadexConfigurationView(),
					"RADEX settings", TAB_RADEX_INDEX);
			tabbedPane.insertTab("Fit", null, getFitConfigurationView(), "Fit settings", TAB_FIT_INDEX);
		}
		return tabbedPane;
	}

	/**
	 * Change the selected tab.
	 *
	 * @param tabIndex The index of the tab to select. Some constants are available:
	 *  TAB_GENERAL_INDEX, TAB_DATABASE_INDEX, TAB_RADEX_INDEX.
	 */
	public void selectTab(int tabIndex) {
		getTabbedPane().setSelectedIndex(tabIndex);
	}

}
