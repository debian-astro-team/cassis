/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.configuration;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JPanel;

import eu.omp.irap.cassis.fit.util.enums.FitType;
import eu.omp.irap.cassis.properties.Software;

@SuppressWarnings("serial")
public class FitConfigurationView extends JPanel {

	private JCheckBox automateEstimation;
	private JCheckBox automateSaving;
	private JComboBox<FitType> defaultComponent;


	public FitConfigurationView() {
		super(new GridBagLayout());
		setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		createFitConfigurationPanel();
	}

	private void createFitConfigurationPanel() {
		GridBagConstraints gbc = new GridBagConstraints();
		UserConfigurationUtils.createLine(this, gbc, "Save automatically after Fit", null, getAutomateSaving(), null, 0);
		UserConfigurationUtils.createLine(this, gbc, "Auto-Estimate Configuration from lines: ",
				null, getAutomateEstimation(), null, 1);
		UserConfigurationUtils.createLine(this, gbc, "Default Component to use: ", null, getDefaultComponent(), null, 2);
	}

	/**
	 * @return A {@link JCheckBox} that let the user choose if he wants
	 * to automatically estimates the components from the computed lines
	 * when creating a model
	 */
	public JCheckBox getAutomateEstimation() {
		if (automateEstimation == null) {
			automateEstimation = new JCheckBox();
			automateEstimation.setSelected(Software.getUserConfiguration().isFitAutoEstimate());
		}
		return automateEstimation;
	}

	/**
	 * @return A {@link JCheckBox} that let the user choose if he wants
	 * to automatically save the result of a fit after its execution
	 */
	public JCheckBox getAutomateSaving() {
		if (automateSaving == null) {
			automateSaving = new JCheckBox();
			automateSaving.setSelected(Software.getUserConfiguration().isFitAutoSave());
		}
		return automateSaving;
	}

	/**
	 * @return A {@link JComboBox} that let the user choose which model to use
	 * by default when automatically estimating components
	 */
	public JComboBox<FitType> getDefaultComponent() {
		if (defaultComponent == null) {
			defaultComponent = new JComboBox<>(FitType.getLineComponents());
			defaultComponent.setRenderer(new DefaultListCellRenderer(){
				@Override
				public Component getListCellRendererComponent(JList<?> list, Object value, int index,
						boolean isSelected, boolean cellHasFocus) {
					String label = ((FitType) value).getLabel();
					return super.getListCellRendererComponent(list, label, index, isSelected, cellHasFocus);
				}
			});
			defaultComponent.setSelectedItem(Software.getUserConfiguration().getFitDefaultType());
		}
		return defaultComponent;
	}

	/**
	 * Save the configuration.
	 */
	public void saveConfiguration() {
		Software.getUserConfiguration().setFitDefaultType((FitType) getDefaultComponent().getSelectedItem());
		Software.getUserConfiguration().setFitAutoEstimate(getAutomateEstimation().isSelected());
		Software.getUserConfiguration().setFitAutoSave(getAutomateSaving().isSelected());
	}

}
