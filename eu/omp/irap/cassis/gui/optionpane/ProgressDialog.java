/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.optionpane;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.ISpectrumComputation;
import eu.omp.irap.cassis.common.ProgressDialogConstants;

@SuppressWarnings("serial")
public class ProgressDialog extends JDialog {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProgressDialog.class);
	private Component component;
	private ISpectrumComputation control;
	private ComputeWorker worker;
	private JProgressBar progress;
	private JButton cancel;


	public ProgressDialog(Component component, ISpectrumComputation control) {
		this.component = component;
		this.control = control;
		initComponents();
		createAndShowGUI();
	}

	private void initComponents() {
		Container container = getContentPane();

		// Center panel
		JPanel center = new JPanel(new BorderLayout());
		progress = new JProgressBar(0, 100);
		progress.setIndeterminate(true);
		progress.setPreferredSize(new Dimension(300, 24));

		cancel = new JButton("Cancel");
		cancel.setPreferredSize(new Dimension(100, 24));
		cancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				interrupt();
				exit();
			}
		});

		JPanel p = new JPanel(new FlowLayout(FlowLayout.CENTER));
		p.add(progress);
		p.add(cancel);

		center.add(p, BorderLayout.CENTER);

		container.add(center, BorderLayout.CENTER);
	}

	/**
	 * Create the GUI and show it. For thread safety, this method should be invoked from the event-dispatching thread.
	 */
	private void createAndShowGUI() {
		// Create and set up the window
		setTitle("Computing the spectrum...");
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setLocationRelativeTo(component);
		setResizable(false);
		setModal(true); // create a modal JFrame (extends JDialog)

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				interrupt();
				exit();
			}
		});

		// Start computing worker
		worker = new ComputeWorker();
		worker.execute();

		// Display the window
		setSize(new Dimension(400, 400));
		pack();
		setVisible(true);
	}

	private void interrupt() {
		if (worker != null && !worker.isDone()) {
			worker.interrupt();
		}
	}

	private void exit() {
		if (worker != null) {
			while (!worker.isDone());
		}
		dispose();
	}

	private class ComputeWorker extends SwingWorker<Void, Long> {
		private volatile boolean readyToStop = false;

		public ComputeWorker() {
			LOGGER.debug("ComputeWorker started");
			ProgressDialogConstants.workerInterrupted = false;
		}

		public void interrupt() {
			LOGGER.debug("ComputeWorker interrupted");
			readyToStop = true;
			ProgressDialogConstants.workerInterrupted = true;
			super.cancel(true);
		}

		@Override
		public Void doInBackground() {
			try {
				control.invokeSpectrumComputation();
			} catch (Exception e) {// Show an error message and exit
				LOGGER.error("An exception occured during the computation", e);
				JOptionPane.showMessageDialog(null, e.toString());
			}
			return null;
		}

		@Override
		protected void done() {
			try {
				LOGGER.debug("ComputeWorker done");
				if (!readyToStop) {
					// Update IHM
					progress.setIndeterminate(false);
					progress.setValue(100);
					cancel.setText("OK");
				}
				exit();
				if (!ComputeWorker.this.isCancelled()) {
					control.display();
				}
			} catch (Exception e) {
				LOGGER.error("An exception occured while handling the end of the computation", e);
			}
		}
	}

}
