/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.otherspecies;

import java.util.EventObject;
import java.util.List;

import javax.swing.event.EventListenerList;

import eu.omp.irap.cassis.cassisd.Server;
import eu.omp.irap.cassis.cassisd.ServerImpl;
import eu.omp.irap.cassis.cassisd.model.OtherSpeciesParameters;
import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.TypeFrequency;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.gui.model.parameter.threshold.ThresholdModel;

public class SpeciesControl  implements ModelListener {

	private SpeciesModel model;
	private SpeciesTab view;
	private EventListenerList listeners;


	public SpeciesControl(SpeciesModel model) {
		super();
		this.model = model;
		view = new SpeciesTab(this);
		model.addModelListener(this);
		listeners = new EventListenerList();
	}

	public List<LineDescription> computeOtherSpecies(double minFreq, double maxFreq,
			boolean modeSignal, String comment) {
		List<LineDescription> speciesLines = null;

		Server server = new ServerImpl();
		speciesLines = server.getOtherSpecies(
				model.getCassisTableMoleculeModel().getList(),
				minFreq, maxFreq, model.getEupMinSpecies(),
				model.getEupMaxSpecies(), model.getAijMinSpecies(), model.getAijMaxSpecies(),
				model.getVlsrPlot(), model.getVlsrData(), modeSignal, comment,
				model.getTypeFrequency());

		return speciesLines;
	}

	public List<LineDescription> computeOtherSpecies(double waveMin, double waveMax, XAxisCassis waveUnit,
			boolean modeSignal, String comment) {
		List<LineDescription> speciesLines = null;

		Server server = new ServerImpl();
		speciesLines = server.getOtherSpecies(
				model.getCassisTableMoleculeModel().getList(),
				waveMin, waveMax, waveUnit,new OtherSpeciesParameters(model.getEupMinSpecies(),
				model.getEupMaxSpecies(), model.getAijMinSpecies(), model.getAijMaxSpecies(),
				model.getVlsrPlot(), model.getVlsrData(), modeSignal, comment,
				model.getTypeFrequency()));

		return speciesLines;
	}

	public SpeciesTab getView() {
		return view;
	}

	public SpeciesModel getModel() {
		return model;
	}

	public void initValue(double vlsr, TypeFrequency typeFrequency) {
		model.setTypeFrequency(typeFrequency);
		view.setOtherDisplayButtonEnabled(true);
		initVelocityParameters(vlsr);
	}

	public void initVelocityParameters(double vlsr) {
		model.setVlsrData(vlsr);
		model.setVlsrPlot(vlsr);
	}

	public void addSpeciesListener(SpeciesListener listener) {
		listeners.add(SpeciesListener.class, listener);
	}

	public void removeSpeciesListener(SpeciesListener l) {
		listeners.remove(SpeciesListener.class, l);
	}

	public void fireSpeciesColorSignalChanged() {
		SpeciesListener[] listenerList = listeners.getListeners(SpeciesListener.class);

		for (SpeciesListener listener : listenerList) {
			listener.speciesColorSignalChanged(new SpeciesColorChangedEvent(this, model.getConfigCurveSignal().getColor()));
		}
	}

	public void fireSpeciesColorImageChanged() {
		SpeciesListener[] listenerList = listeners.getListeners(SpeciesListener.class);

		for (SpeciesListener listener : listenerList) {
			listener.speciesColorImageChanged(new SpeciesColorChangedEvent(this, model.getConfigCurveImage().getColor()));
		}
	}

	public void fireSpeciesDisplayClicked() {

		if (!model.isSpeciesSignalVisible() && !model.isSpeciesImageVisible()) {
			model.setSpeciesSignalVisible(true);
		}
		SpeciesListener[] listenerList = listeners.getListeners(SpeciesListener.class);

		for (SpeciesListener listener : listenerList) {
			listener.speciesDisplayClicked(new EventObject(this));
		}
	}

	public void fireSpeciesSignalVisibleChanged() {
		SpeciesListener[] listenerList = listeners.getListeners(SpeciesListener.class);

		for (SpeciesListener listener : listenerList) {
			listener.speciesSignalVisibleChanged(new SpeciesEnableEvent(this, model.isSpeciesSignalVisible()));
		}
	}

	public void fireSpeciesImageVisibleChanged() {
		SpeciesListener[] listenerList = listeners.getListeners(SpeciesListener.class);

		for (SpeciesListener listener : listenerList) {
			listener.speciesImageVisibleChanged(new SpeciesEnableEvent(this, model.isSpeciesImageVisible()));
		}
	}

	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (SpeciesModel.COLOR_SIGNAL_CHANGED_EVENT.equals(event.getSource())) {
			view.getButtonColor().setBackground(model.getColorSpeciesSignal());
			fireSpeciesColorSignalChanged();
		} else if (SpeciesModel.COLOR_IMAGE_CHANGED_EVENT.equals(event.getSource())) {
			view.getButtonColorImage().setBackground(model.getColorSpeciesImage());
			fireSpeciesColorImageChanged();
		} else if (SpeciesModel.VISIBLE_SIGNAL_CHANGED_EVENT.equals(event.getSource())) {
			view.getOtherSpeciesBox().setSelected(model.isSpeciesSignalVisible());
			fireSpeciesSignalVisibleChanged();
		} else if (SpeciesModel.VISIBLE_IMAGE_CHANGED_EVENT.equals(event.getSource())) {
			view.getOtherSpeciesImageBox().setSelected(model.isSpeciesImageVisible());
			fireSpeciesImageVisibleChanged();
		} else if (SpeciesModel.EUP_MIN_CHANGED_EVENT.equals(event.getSource())) {
			view.getMinEupField().setValue(model.getEupMinSpecies());
		} else if (SpeciesModel.EUP_MAX_CHANGED_EVENT.equals(event.getSource())) {
			view.getMaxEupField().setValue(model.getEupMaxSpecies());
		} else if (SpeciesModel.AIJ_MIN_CHANGED_EVENT.equals(event.getSource())) {
			view.getMinAijField().setValue(model.getAijMinSpecies());
		} else if (SpeciesModel.AIJ_MAX_CHANGED_EVENT.equals(event.getSource())) {
			view.getMaxAijField().setValue(model.getAijMaxSpecies());
		} else if (SpeciesModel.VLSR_PLOT_CHANGED_EVENT.equals(event.getSource())) {
			view.getVlsrPlotField().setValue(model.getVlsrPlot());
		} else if (SpeciesModel.VLSR_DATA_CHANGED_EVENT.equals(event.getSource())) {
			view.getVlsrDataField().setValue(model.getVlsrData());
		} else if (SpeciesModel.TEMPLATE_CHANGED_EVENT.equals(event.getSource())) {
			final String template = model.getTemplate();
			view.getComboBoxTemplate().setSelectedItem(template);
//			if (template != null && ! Template.FULL_TEMPLATE.equals(template)&&
//				! template.startsWith("Full")){
//				model.getCassisTableMoleculeModel().selectAll();
//			}
		} else if (SpeciesModel.FREQUENCY_CHANGED_EVENT.equals(event.getSource())) {
			view.getFrequencyComboBox().setSelectedItem(model.getTypeFrequency().forSave());
		}
	}

	public void setView(SpeciesTab speciesTab) {
		view = speciesTab;
	}

	public void initThresholdModel(ThresholdModel thresholdModel) {
		if (thresholdModel != null) {
			model.setEupMinSpecies(thresholdModel.getThresEupMin());
			model.setEupMaxSpecies(thresholdModel.getThresEupMax());
			model.setAijMinSpecies(thresholdModel.getThresAijMin());
			model.setAijMaxSpecies(thresholdModel.getThresAijMax());
		}
	}
}
