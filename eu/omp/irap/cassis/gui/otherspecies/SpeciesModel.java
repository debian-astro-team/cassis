/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.otherspecies;

import java.awt.Color;
import java.util.ArrayList;

import eu.omp.irap.cassis.common.TypeFrequency;
import eu.omp.irap.cassis.common.events.ListenerManager;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.gui.model.table.CassisTableMolecule;
import eu.omp.irap.cassis.gui.model.table.CassisTableMoleculeModel;
import eu.omp.irap.cassis.gui.plot.curve.config.ConfigCurve;
import eu.omp.irap.cassis.gui.template.ListTemplateManager;

public class SpeciesModel extends ListenerManager {

	public static final String AIJ_MAX_CHANGED_EVENT = "aijMaxChanged";
	public static final String AIJ_MIN_CHANGED_EVENT = "aijMinChanged";
	public static final String COLOR_IMAGE_CHANGED_EVENT = "colorImageChanged";
	public static final String COLOR_SIGNAL_CHANGED_EVENT = "colorSignalChanged";
	public static final String EUP_MAX_CHANGED_EVENT = "eupMaxChanged";
	public static final String EUP_MIN_CHANGED_EVENT = "eupMinChanged";
	public static final String FREQUENCY_CHANGED_EVENT = "frequencyChanged";
	public static final String LIMIT_VISIBLE_DATA_CHANGED_EVENT =
			"limitVisibleDataChanged";
	public static final String TEMPLATE_CHANGED_EVENT = "templateChanged";
	public static final String VISIBLE_IMAGE_CHANGED_EVENT = "visibleImageChanged";
	public static final String VISIBLE_SIGNAL_CHANGED_EVENT = "visibleSignalChanged";
	public static final String VLSR_DATA_CHANGED_EVENT = "vlsrDataChanged";
	public static final String VLSR_PLOT_CHANGED_EVENT = "vlsrPlotChanged";

	public static final Color DEFAULT_COLOR_SIGNAL = Color.GREEN;
	public static final Color DEFAULT_COLOR_IMAGE = new Color(102, 0, 102);

	private double eupMin = 0.;
	private double eupMax = 150.;

	private double aijMin = 0.;
	private double aijMax = Double.MAX_VALUE;

	private String template;

	private ConfigCurve configCurveSignal;
	private ConfigCurve configCurveImage;

	private double vlsrPlot;
	private double vlsrData;

	private boolean haveImage = true;

	private boolean limitVisibleData = true;
	private boolean displayVisibleDataOption;

	private CassisTableMoleculeModel cassisTableMoleculeModel;
	private TypeFrequency typeFrequency = TypeFrequency.REST;


	public SpeciesModel(boolean displayVisibleDataOption) {
		super();
		this.displayVisibleDataOption = displayVisibleDataOption;
		template = ListTemplateManager.getInstance().getDefaultTemplateToUse();
		configCurveSignal = new ConfigCurve();
		configCurveSignal.setColor(DEFAULT_COLOR_SIGNAL);

		configCurveImage = new ConfigCurve();
		configCurveImage.setColor(DEFAULT_COLOR_IMAGE);

		setSpeciesSignalVisible(false);
		setSpeciesImageVisible(false);
	}

	public Color getColorSpeciesSignal() {
		return configCurveSignal.getColor();
	}

	public Color getColorSpeciesImage() {
		return configCurveImage.getColor();
	}

	public void setColorSpeciesSignal(Color colorSpecies) {
		if (!colorSpecies.equals(getColorSpeciesSignal())) {
			configCurveSignal.setColor(colorSpecies);
			fireDataChanged(new ModelChangedEvent(COLOR_SIGNAL_CHANGED_EVENT));
		}
	}

	public void setColorSpeciesImage(Color colorSpecies) {
		if (!colorSpecies.equals(getColorSpeciesImage())) {
			configCurveImage.setColor(colorSpecies);
			fireDataChanged(new ModelChangedEvent(COLOR_IMAGE_CHANGED_EVENT));
		}
	}

	public void setSpeciesSignalVisible(boolean enabled) {
		if (enabled != isSpeciesSignalVisible()) {
			configCurveSignal.setVisible(enabled);
			fireDataChanged(new ModelChangedEvent(VISIBLE_SIGNAL_CHANGED_EVENT));
		}
	}

	public void setSpeciesImageVisible(boolean enabled) {
		if (enabled != isSpeciesImageVisible()) {
			configCurveImage.setVisible(enabled);
			fireDataChanged(new ModelChangedEvent(VISIBLE_IMAGE_CHANGED_EVENT));
		}
	}

	public boolean isSpeciesSignalVisible() {
		return configCurveSignal.isVisible();
	}

	public boolean isSpeciesImageVisible() {
		return configCurveImage.isVisible();
	}

	/**
	 * @param template
	 *            the template to set
	 */
	public final void setTemplate(String template) {
		this.template = template;
		fireDataChanged(new ModelChangedEvent(TEMPLATE_CHANGED_EVENT));
	}

	/**
	 * @return the template
	 */
	public final String getTemplate() {
		return template;
	}

	public double getEupMinSpecies() {
		return eupMin;
	}

	public void setEupMinSpecies(double eupMinSpecies) {
		this.eupMin = eupMinSpecies;
		fireDataChanged(new ModelChangedEvent(EUP_MIN_CHANGED_EVENT));
	}

	public double getEupMaxSpecies() {
		return eupMax;
	}

	public void setEupMaxSpecies(double eupMaxSpecies) {
		this.eupMax = eupMaxSpecies;
		fireDataChanged(new ModelChangedEvent(EUP_MAX_CHANGED_EVENT));
	}

	public double getAijMinSpecies() {
		return aijMin;
	}

	public void setAijMinSpecies(double aijMinSpecies) {
		this.aijMin = aijMinSpecies;
		fireDataChanged(new ModelChangedEvent(AIJ_MIN_CHANGED_EVENT));
	}

	public double getAijMaxSpecies() {
		return aijMax;
	}

	public void setAijMaxSpecies(double aijMaxSpecies) {
		this.aijMax = aijMaxSpecies;
		fireDataChanged(new ModelChangedEvent(AIJ_MAX_CHANGED_EVENT));
	}

	public double getVlsrPlot() {
		return vlsrPlot;
	}

	public void setVlsrPlot(double vlsrSpecies) {
		this.vlsrPlot = vlsrSpecies;
		fireDataChanged(new ModelChangedEvent(VLSR_PLOT_CHANGED_EVENT));
	}

	/**
	 * @param vlsrData
	 *            set thevlsr data just for information
	 */
	public void setVlsrData(double vlsrData) {
		this.vlsrData = vlsrData;
		fireDataChanged(new ModelChangedEvent(VLSR_DATA_CHANGED_EVENT));
	}

	/**
	 * @return the vlsrFile
	 */
	public double getVlsrData() {
		return vlsrData;
	}

	public void setHaveImage(boolean value) {
		this.haveImage = value;
	}

	/**
	 * @return the haveImage
	 */
	public final boolean isHaveImage() {
		return haveImage;
	}

	/**
	 * @return the configCurveSignal
	 */
	public final ConfigCurve getConfigCurveSignal() {
		return configCurveSignal;
	}

	/**
	 * @return the configCurveImage
	 */
	public final ConfigCurve getConfigCurveImage() {
		return configCurveImage;
	}

	/**
	 * @param configCurveSignal the configCurveSignal to set
	 */
	public final void setConfigCurveSignal(ConfigCurve configCurveSignal) {
		this.configCurveSignal = configCurveSignal;
	}

	/**
	 * @param configCurveImage the configCurveImage to set
	 */
	public final void setConfigCurveImage(ConfigCurve configCurveImage) {
		this.configCurveImage = configCurveImage;
	}

	/**
	 * @return the cassisTableMoleculeModel
	 */
	public CassisTableMoleculeModel getCassisTableMoleculeModel() {
		if (cassisTableMoleculeModel == null) {
			this.cassisTableMoleculeModel = new CassisTableMoleculeModel(new ArrayList<>(),
					new String[] {"Species", "Tag", "Database", "Sel." },
					new Integer[] { CassisTableMolecule.NAME_INDICE,
							CassisTableMolecule.TAG_INDICE,
							CassisTableMolecule.DATA_SOURCE_INDICE,
							CassisTableMolecule.COMPUTE_INDICE });
		}
		return cassisTableMoleculeModel;
	}

	public void setTypeFrequency(TypeFrequency value) {
		this.typeFrequency  = value;
		fireDataChanged(new ModelChangedEvent(FREQUENCY_CHANGED_EVENT));
	}

	public TypeFrequency getTypeFrequency() {
		return this.typeFrequency;
	}

	/**
	 * Return if the the research should be limited on visible data.
	 *
	 * @return true if the research should be limited to visible data, false otherwise.
	 */
	public boolean isLimitVisibleData() {
		return limitVisibleData;
	}

	/**
	 * Change the value of the limit on visible data.
	 *
	 * @param limitVisibleData true to limit the other species query on visible
	 *  data only, false to display the other species on all plot range.
	 */
	public void setLimitVisibleData(boolean limitVisibleData) {
		this.limitVisibleData = limitVisibleData;
		fireDataChanged(new ModelChangedEvent(LIMIT_VISIBLE_DATA_CHANGED_EVENT));
	}

	/**
	 * Return true to display "limit on visible data" option, false otherwise.
	 *
	 * @return true to display "limit on visible data" option, false otherwise.
	 */
	public boolean isDisplayVisibleDataOption() {
		return displayVisibleDataOption;
	}

}
