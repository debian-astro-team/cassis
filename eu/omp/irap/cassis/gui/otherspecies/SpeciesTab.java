/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.otherspecies;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.common.TypeFrequency;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.gui.model.table.JCassisTable;
import eu.omp.irap.cassis.gui.plot.selection.SelectionPanel;
import eu.omp.irap.cassis.gui.template.JComboBoxTemplate;
import eu.omp.irap.cassis.gui.util.JDoubleCassisTextField;
import eu.omp.irap.cassis.properties.Software;

/**
 * @author glorian
 *
 */
public class SpeciesTab extends JPanel implements MoleculeSelectionnable{

	private static final long serialVersionUID = 567387585486017805L;
	private JCheckBox speciesSignalBox;
	private JCheckBox speciesImageBox;
	private JLabel eupLabel = new JLabel("Eup ");
	private JComboBox<String> eupUnitComboBox = new JComboBox<>(new String[]{UNIT.KELVIN.toString()});
	private JLabel minEupLabel = new JLabel("min  :");
	private JLabel maxEupLabel = new JLabel("max  :");
	private JDoubleCassisTextField minEupField;
	private JDoubleCassisTextField maxEupField;

	private JLabel aijLabel = new JLabel("Aij   ");
	private JLabel minAijLabel = new JLabel("min  :");
	private JLabel maxAijLabel = new JLabel("max  :");
	private JDoubleCassisTextField minAijField;
	private JDoubleCassisTextField maxAijField;

	private JLabel vlsrLabel = new JLabel("Vlsr");
	private JComboBox<String> vlsrUnitCombobox = new JComboBox<>(new String[]{UNIT.KM_SEC_MOINS_1.toString()});
	private JLabel vlsrDataLabel = new JLabel("data :");
	private JLabel vlsrPlotLabel = new JLabel("plot :");
	private JDoubleCassisTextField vlsrDataField;
	private JDoubleCassisTextField vlsrPlotField;

	private JCassisTable<MoleculeDescription> moleculesTable;
	private JComboBoxTemplate comboBoxTemplate;
	private JButton displayButton;

	private JButton buttonColor;
	private JButton buttonColorImage;
	private SpeciesModel model;

	private JPanel thresholdPanel;
	private JPanel displayPanel;
	private JPanel speciesPanel;

	private JPanel eupPanel;
	private JPanel aijPanel;
	private JPanel vlsrPanel;
	private SpeciesControl control;
	private SelectionPanel selectionPanel;
	private JPanel frequencyPanel;
	private JComboBox<String> frequencyComboBox;
	private JCheckBox limitVisibleDataCheckBox;


	/**
	 * Constructor.
	 */
	public SpeciesTab() {
		this(new SpeciesControl(new SpeciesModel(true)));
	}

	public SpeciesTab(SpeciesControl control) {
		setControl(control);
		model = control.getModel();
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		add(getThresholdPanel());
		add(getSpeciesPanel());
		JPanel temp = new JPanel(new BorderLayout());
		temp.add(getSelectionPanel(), BorderLayout.NORTH);
		add(temp);
		add(getDisplayPanel());
		setSize(new Dimension(300, 500));
		getComboBoxTemplate().setSelectedItem(model.getTemplate());
//		model.getCassisTableMoleculeModel().selectAll();
	}

	private void setControl(SpeciesControl control) {
		this.control = control;
		control.setView(this);
	}

	private JPanel getSelectionPanel() {
		if (selectionPanel == null) {
			selectionPanel = new SelectionPanel();
			selectionPanel.setVisible(false);
		}
		return selectionPanel;
	}

	private JComponent getSpeciesPanel() {
		if (speciesPanel == null) {
			speciesPanel = new JPanel();
			speciesPanel.setLayout(new BorderLayout());
			speciesPanel.setBorder(new TitledBorder("Template"));

			speciesPanel.add(getComboBoxTemplate(), BorderLayout.NORTH);
			JScrollPane scrollPane = new JScrollPane(getMoleculesTable());
			speciesPanel.add(scrollPane, BorderLayout.CENTER);
			getMoleculesTable().setScrollPane(scrollPane);
		}
		return speciesPanel;
	}

	private JPanel getThresholdPanel() {
		if (thresholdPanel == null) {
			thresholdPanel = new JPanel();
			thresholdPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
			thresholdPanel.setBorder(new TitledBorder("Thresholds and Settings"));
			JPanel temp = new JPanel();
			temp.setLayout(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			c.fill = GridBagConstraints.HORIZONTAL;
			c.anchor = GridBagConstraints.LINE_START;
			c.gridx = 0;
			c.gridy = 0;

			temp.add(getEupPanel(), c);
			c.gridx = 0;
			c.gridy = 1;
			temp.add(getAijPanel(), c);
			c.gridx = 0;
			c.gridy = 2;
			temp.add(getVlsrPanel(), c);
			c.gridx = 0;
			c.gridy = 3;
			temp.add(getFrequencyPanel(), c);
			thresholdPanel.add(temp);
		}
		return thresholdPanel;
	}

	private JPanel getFrequencyPanel() {
		if (frequencyPanel == null) {
			frequencyPanel = new JPanel();
			frequencyPanel.setBorder(new TitledBorder("Frequency Scale"));

			frequencyPanel.add(getFrequencyComboBox());
			frequencyPanel.setVisible(false);

		}
		return frequencyPanel;
	}

	public JComboBox<String> getFrequencyComboBox() {
		if (frequencyComboBox == null) {
			frequencyComboBox = new JComboBox<>(new String[]{TypeFrequency.REST.forSave(),
												 TypeFrequency.SKY.forSave()});
			frequencyComboBox.setEnabled(false);
			frequencyComboBox.setSelectedItem(model.getTypeFrequency());
			frequencyComboBox.addActionListener(new ActionListener() {
					@Override
				public void actionPerformed(ActionEvent e) {
					TypeFrequency value = TypeFrequency.toFreq((String) ((JComboBox<?>)e.getSource()).getSelectedItem());
					model.setTypeFrequency(value);
				}
			});
		}
		return frequencyComboBox;
	}

	private JPanel getVlsrPanel() {
		if (vlsrPanel == null) {
			vlsrPanel = new JPanel();
			vlsrPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
			vlsrLabel.setLabelFor(getVlsrDataField());
			vlsrPlotLabel.setLabelFor(getVlsrPlotField());

			vlsrPanel.add(vlsrLabel);
			vlsrPanel.add(vlsrDataLabel);
			vlsrPanel.add(getVlsrDataField());
			vlsrPanel.add(vlsrPlotLabel);
			vlsrPanel.add(getVlsrPlotField());
			vlsrPanel.add(vlsrUnitCombobox);
		}
		return vlsrPanel;
	}

	public JDoubleCassisTextField getVlsrPlotField() {
		if (vlsrPlotField == null) {
			vlsrPlotField = new JDoubleCassisTextField(4, Double.valueOf(0.));

			vlsrPlotField.addPropertyChangeListener("value", new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					model.setVlsrPlot(Double.valueOf(evt.getNewValue().toString()));
				}
			});
		}
		return vlsrPlotField;
	}

	public JDoubleCassisTextField getVlsrDataField() {
		if (vlsrDataField == null) {
			vlsrDataField = new JDoubleCassisTextField(4, Double.valueOf(0.));
			vlsrDataField.setEditable(false);
			vlsrDataField.addPropertyChangeListener("value", new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					model.setVlsrData(Double.valueOf(evt.getNewValue().toString()));
				}
			});
		}
		return vlsrDataField;
	}

	private JPanel getAijPanel() {
		if (aijPanel == null) {
			aijPanel = new JPanel();
			aijPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

			minAijLabel.setLabelFor(getMinAijField());
			maxAijLabel.setLabelFor(getMaxAijField());

			aijPanel.add(aijLabel);
			aijPanel.add(minAijLabel);
			aijPanel.add(getMinAijField());
			aijPanel.add(maxAijLabel);
			aijPanel.add(getMaxAijField());
		}
		return aijPanel;
	}

	public JDoubleCassisTextField getMinAijField() {
		if (minAijField == null) {
			minAijField = new JDoubleCassisTextField(4, model.getAijMinSpecies());
			Map<String, Double> symbols = new HashMap<>();
			symbols.put("*", 0.0);
			minAijField.setSymbols(symbols);
			minAijField.addPropertyChangeListener("value", new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					model.setAijMinSpecies(Double.valueOf(evt.getNewValue().toString()));
				}
			});
		}
		return minAijField;
	}

	public JDoubleCassisTextField getMaxAijField() {
		if (maxAijField == null) {
			maxAijField = new JDoubleCassisTextField(4, model.getAijMaxSpecies());
			maxAijField.addPropertyChangeListener("value", new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					model.setAijMaxSpecies(Double.valueOf(evt.getNewValue().toString()));
				}
			});
		}
		return maxAijField;
	}

	private JPanel getEupPanel() {
		if (eupPanel == null) {
			eupPanel = new JPanel();
			eupPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

			minEupLabel.setLabelFor(getMinEupField());
			maxEupLabel.setLabelFor(getMaxEupField());

			eupPanel.add(eupLabel);
			eupPanel.add(minEupLabel);
			eupPanel.add(getMinEupField());
			eupPanel.add(maxEupLabel);
			eupPanel.add(getMaxEupField());
			eupPanel.add(eupUnitComboBox);
		}
		return eupPanel;
	}

	public JDoubleCassisTextField getMaxEupField() {
		if (maxEupField == null) {
			maxEupField = new JDoubleCassisTextField(4,model.getEupMaxSpecies());
			maxEupField.addPropertyChangeListener("value", new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					model.setEupMaxSpecies(Double.valueOf(evt.getNewValue().toString()));
				}
			});
		}
		return maxEupField;
	}

	public JDoubleCassisTextField getMinEupField() {
		if (minEupField == null) {
			minEupField = new JDoubleCassisTextField(4,model.getEupMinSpecies());
			Map<String, Double> symbols = new HashMap<>();
			symbols.put("*", 0.0);
			minEupField.setSymbols(symbols);
			minEupField.addPropertyChangeListener("value", new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					model.setEupMinSpecies(Double.valueOf(evt.getNewValue().toString()));
				}
			});
		}
		return minEupField;
	}

	private JPanel getDisplayPanel() {
		if (displayPanel == null) {
			displayPanel = new JPanel();
			JPanel temp;
			temp = new JPanel();
			temp.setLayout(new GridBagLayout());

			GridBagConstraints c = new GridBagConstraints();
			c.fill = GridBagConstraints.HORIZONTAL;
			c.anchor = GridBagConstraints.LINE_START;
			c.gridx = 0;
			c.gridy = 0;
			temp.add(getOtherSpeciesBox(), c);
			c.gridx = 1;
			c.gridy = 0;
			temp.add(getButtonColor(), c);
			c.gridx = 0;
			c.gridy = 1;
			temp.add(getOtherSpeciesImageBox(), c);
			c.gridx = 1;
			c.gridy = 1;
			temp.add(getButtonColorImage(), c);
			c.gridx = 0;
			c.gridy = 2;
			if (model.isDisplayVisibleDataOption()) {
				c.gridwidth = 3;
				temp.add(getLimitVisibleDataCheckBox(), c);
				c.gridwidth = 1;
				c.gridy = 3;
			}
			temp.add(getDisplayButton(), c);

			displayPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
			displayPanel.setBorder(new TitledBorder("Display"));
			displayPanel.add(temp);
		}
		return displayPanel;
	}

	public JCheckBox getOtherSpeciesImageBox() {
		if (speciesImageBox == null) {
			speciesImageBox = new JCheckBox("Show image");
			speciesImageBox.setSelected(false);
			speciesImageBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					model.setSpeciesImageVisible(speciesImageBox.isSelected());
				}
			});
		}
		return speciesImageBox;
	}

	public JCheckBox getOtherSpeciesBox() {
		if (speciesSignalBox == null) {
			speciesSignalBox = new JCheckBox("Show signal");
			speciesSignalBox.setSelected(false);
			speciesSignalBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					model.setSpeciesSignalVisible(speciesSignalBox.isSelected());
				}
			});
		}
		return speciesSignalBox;
	}

	public JButton getButtonColor() {
		if (buttonColor == null) {
			buttonColor = new JButton();
			buttonColor.setBackground(model.getColorSpeciesSignal());
			buttonColor.setSize(43, 50);
			buttonColor.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					Color newColor = JColorChooser.showDialog(SpeciesTab.this,
							"Choose Background Color",
							buttonColor.getBackground());
					if (newColor != null) {
						model.setColorSpeciesSignal(newColor);
					}
				}
			});
		}
		return buttonColor;
	}

	public JButton getButtonColorImage() {
		if (buttonColorImage == null) {
			buttonColorImage = new JButton();
			buttonColorImage.setBackground(model.getColorSpeciesImage());
			buttonColorImage.setEnabled(true);
			buttonColorImage.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					Color newColor = JColorChooser.showDialog(SpeciesTab.this,
							"Choose Background Color",
							buttonColorImage.getBackground());
					if (newColor != null) {
						model.setColorSpeciesImage(newColor);
					}
				}
			});
		}
		return buttonColorImage;
	}

	public JButton getDisplayButton() {
		if (displayButton == null) {
			displayButton = new JButton("Display");
			displayButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.fireSpeciesDisplayClicked();
				}
			});
		}
		return displayButton;
	}

	public void setShowSpeciesImageEnabled(boolean value) {
		if (!value) {
			getOtherSpeciesBox().setText("Show");
		}
		else {
			getOtherSpeciesBox().setText("Show signal");
		}
		getOtherSpeciesImageBox().setVisible(value);
		buttonColorImage.setVisible(value);
		model.setHaveImage(value);
	}

	public boolean isSpeciesImageEnabled() {
		return getOtherSpeciesImageBox().isVisible();
	}

	/**
	 * Set the selection of Other species.
	 *
	 * @param value The selection value.
	 */
	public void selectSpeciesSignal(boolean value) {
		getOtherSpeciesBox().setSelected(value);
	}

	public void selectSpeciesImage(boolean value) {
		getOtherSpeciesImageBox().setSelected(value);
	}

	public SpeciesModel getModel() {
		return model;
	}

	public void setSpeciesSignalVisible(boolean enabled) {
		model.setSpeciesSignalVisible(enabled);
	}

	public void setSpeciesImageVisible(boolean enabled) {
		model.setSpeciesImageVisible(enabled);
	}

	public void setOtherDisplayButtonEnabled(boolean val) {
		displayButton.setEnabled(val);
	}

	public void initVlsr(double d) {
		vlsrDataField.setValue(d);
		vlsrPlotField.setValue(d);
	}

	public SpeciesControl getControl() {
		return control;
	}

	public JComboBoxTemplate getComboBoxTemplate() {
		if (comboBoxTemplate == null) {
			comboBoxTemplate = new JComboBoxTemplate(false, true);
			comboBoxTemplate.setSelectedItem(model.getTemplate());
			comboBoxTemplate.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					String template = (String) comboBoxTemplate.getSelectedItem();
					if (template == null) {
						return;
					} else if (!JComboBoxTemplate.OPERATIONS_TEMPLATE.equals(template) && !JComboBoxTemplate.TEMPLATE.equals(template)) {
						model.setTemplate(template);
					}
				}
			});

		}
		return comboBoxTemplate;
	}

	public JCassisTable<MoleculeDescription> getMoleculesTable() {
		if (moleculesTable == null) {
			moleculesTable = new JOtherSpeciesTable<>(
					model.getCassisTableMoleculeModel(), false, true, this);
			moleculesTable.setTemplateCombo(getComboBoxTemplate());
			moleculesTable.getColumn("Tag").setMinWidth(52);
			moleculesTable.getColumn("Tag").setMaxWidth(52);
			moleculesTable.getColumn("Tag").setPreferredWidth(52);
			moleculesTable.getColumn("Database").setMinWidth(66);
			moleculesTable.getColumn("Database").setMaxWidth(66);
			moleculesTable.getColumn("Database").setPreferredWidth(66);
			moleculesTable.getColumn("Sel.").setMinWidth(25);
			moleculesTable.getColumn("Sel.").setMaxWidth(25);
			moleculesTable.getColumn("Sel.").setPreferredWidth(25);
			moleculesTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
				@Override
				public void valueChanged(ListSelectionEvent event) {
					moleculesTable.getSelectionModel().clearSelection();
				}
			});
			moleculesTable.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (Software.getUserConfiguration().isAutoOtherSpeciesDisplayed()) {
						getDisplayButton().doClick();
					}
				}
			});
		}
		return moleculesTable;
	}

	/**
	 * Create if needed then return the limit visible data JCheckBox.
	 *
	 * @return the limit visible data JCheckBox.
	 */
	public JCheckBox getLimitVisibleDataCheckBox() {
		if (limitVisibleDataCheckBox == null) {
			limitVisibleDataCheckBox = new JCheckBox("Limit research to visible data");
			limitVisibleDataCheckBox.setSelected(model.isLimitVisibleData());
			limitVisibleDataCheckBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					model.setLimitVisibleData(limitVisibleDataCheckBox.isSelected());
				}
			});
		}
		return limitVisibleDataCheckBox;
	}

	public static void main(String[] args) {
		JPanel panel = new SpeciesTab();
		JFrame frame = new JFrame("Test other species");
		frame.setContentPane(panel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.pack();
	}

	@Override
	public void moleculeSelectedbyList() {
		if (Software.getUserConfiguration().isAutoOtherSpeciesDisplayed()) {
			getDisplayButton().doClick();
		}

	}

	@Override
	public void headerAllComputeClicked() {
		if (Software.getUserConfiguration().isAutoOtherSpeciesDisplayed()) {
			getDisplayButton().doClick();
		}

	}

}
