/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.text.DecimalFormat;
import java.util.EventObject;
import java.util.Iterator;
import java.util.LinkedList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;
import javax.swing.event.EventListenerList;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.gui.fit.history.HistoryListener;
import eu.omp.irap.cassis.gui.fit.history.HistoryPanel;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import eu.omp.irap.cassis.gui.plot.util.AddComponentPanel;
import eu.omp.irap.cassis.gui.plot.util.InterValMarkerCassis;
import eu.omp.irap.cassis.gui.plot.util.MarkerManager;
import eu.omp.irap.cassis.gui.util.JDoubleCassisTextField;
import eu.omp.irap.cassis.properties.Software;
import herschel.ia.numeric.Double1d;
import herschel.ia.numeric.toolbox.fit.AbstractModel;

/**
 * Class to display the fit.
 *
 * @author glorian
 */
public class FitPanel extends JPanel implements HistoryListener, ModelListener {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(FitPanel.class);

	private static final String SHOW_FIT_STRING = "Overlay fits";
	private static final String HIDE_FIT_STRING = "Hide fits";
	private static final String SHOW_RESIDUAL_STRING = "Overlay residual";
	private static final String HIDE_RESIDUAL_STRING = "Hide residual";

	private JPopupMenu popupFitting;
	private JScrollPane scrollPanelFitting;
	private JPanel panelFitting;

	private JToggleButton toggleButtonOverlayFit;
	private JToggleButton toggleButtonPlotResidual;
	private JButton subtractFitButton;

	private EventListenerList listeners;
	private HistoryPanel<XYSpectrumSeries> historyPanel;
	private JDoubleCassisTextField nbIterations;
	private JDoubleCassisTextField oversamplingFit;
	private JDoubleCassisTextField tolerance;
	private FitResult lastFitResult;
	private JButton divideButton;

	private FittingItemList listFittingItem;
	private FitModel model;

	private MarkerManager markerManager;
	private JComboBox<String> fitterComboBox;
	private FitPanelControl control;

	private JPanel actionPanel;

	private JPanel operationAndHistory;

	private JButton selectButton;
	private AddComponentPanel manageComponentPanel;
	private JMenuItem menuBaseLinePoly;
	private JMenuItem menuBaseLineSin;
	private JMenuItem menuLineGaus;
	private JMenuItem menuLineLorentz;
	private JMenuItem menuLineVoight;
	private JMenuItem menuLineSinc;
	private JMenuItem menuClearAll;
	private JMenuItem menuSelectAll;
	private JMenuItem menuUnSelectAll;
	private JButton fitButton;
	private JButton fitAllbutton;
	private JButton saveButton;
	private JButton loadConfigButton;
	private JButton saveConfigButton;

	private static boolean verbose = false;

	/**
	 * Default constructor.
	 */
	private FitPanel() {
		super();
		listFittingItem = new FittingItemList(this);
		model = new FitModel();
		model.addModelListener(this);
		makePanel();
		listeners = new EventListenerList();
	}

	public FitPanel(MarkerManager markerManager, boolean historyDisplayed, FitPanelControl control) {
		this();
		historyPanel.setEnabled(historyDisplayed);
		this.markerManager = markerManager;
		this.control = control;
	}

	/**
	 * Build the main panel.
	 */
	private void makePanel() {
		setLayout(new BorderLayout());
		add(getFitParametersPanel(), BorderLayout.NORTH); // Menu Panel
		add(getManageComponentPanel(), BorderLayout.CENTER); // Fitting Panel
		add(getSouthFitPanel(), BorderLayout.SOUTH); // Fit Button
	}

	public AddComponentPanel getManageComponentPanel() {
		if (manageComponentPanel == null) {
			manageComponentPanel = new AddComponentPanel("", false);
			manageComponentPanel.getSubPanel().setLayout(new BoxLayout(manageComponentPanel.getSubPanel(), BoxLayout.Y_AXIS));
			manageComponentPanel.getSubPanel().add(getScrollPanelFitting());
			manageComponentPanel.addButtonAddListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					getPopupAddModel().show(getScrollPanelFitting(), 0, 0);
				}
			});
		}
		return manageComponentPanel;
	}

	public JScrollPane getScrollPanelFitting() {
		if (scrollPanelFitting == null) {
			JPanel panelf = new JPanel();
			panelf.add(getPanelFitting());
			scrollPanelFitting = new JScrollPane(panelf, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
					JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			scrollPanelFitting.setPreferredSize(new Dimension(50, 200));
		}
		return scrollPanelFitting;
	}

	public JPanel getPanelFitting() {
		if (panelFitting == null) {
			panelFitting = new JPanel();
			panelFitting.setLayout(new BoxLayout(panelFitting, BoxLayout.Y_AXIS));
		}
		return panelFitting;
	}

	private Component getFitParametersPanel() {
		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Parameters"));
		panel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.0;
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 0;
		panel.add(getFitterComboBox(), c);

		c.gridwidth = 1;
		c.gridy = 1;

		JPanel paramPanel = new JPanel();
		paramPanel.add(new JLabel("Iter. :"));
		paramPanel.add(getNbiterations());
		paramPanel.add(new JLabel("Sampl. :"));
		paramPanel.add(getOversamplingFit());
		paramPanel.add(new JLabel("Toler.:"));
		paramPanel.add(getTolerance());
		panel.add(paramPanel, c);

		return panel;
	}

	private JDoubleCassisTextField getTolerance() {
		if (tolerance == null) {
			tolerance = new JDoubleCassisTextField();
			tolerance.setColumns(5);
			model.setTolerance(0.001);
		}
		return tolerance;
	}

	private JDoubleCassisTextField getOversamplingFit() {
		if (oversamplingFit == null) {
			oversamplingFit = new JDoubleCassisTextField();
			oversamplingFit.setColumns(2);
			oversamplingFit.setDecimalFormat(new DecimalFormat("0"));
			oversamplingFit.setScientificFormat(new DecimalFormat("0E0"));

			oversamplingFit.addPropertyChangeListener("value", new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					int oversampling;
					try {
						oversampling = Integer.valueOf(oversamplingFit.getValue().toString());
					} catch (NumberFormatException nfe) {
						LOGGER.error("The oversampling value must be a number", nfe);
						oversampling = Integer.valueOf(String.valueOf(evt.getOldValue()));
					}
					model.setOversamplingFit(oversampling);
				}
			});

			model.setOversamplingFit(10);
		}
		return oversamplingFit;
	}

	private JDoubleCassisTextField getNbiterations() {
		if (nbIterations == null) {
			nbIterations = new JDoubleCassisTextField();
			nbIterations.setColumns(4);
			nbIterations.setDecimalFormat(new DecimalFormat("0"));

			nbIterations.addPropertyChangeListener("value", new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					int iterations;
					try {
						iterations = Integer.valueOf(nbIterations.getValue().toString());
					} catch (NumberFormatException nfe) {
						LOGGER.error("The iterations value must be a number", nfe);
						iterations = Integer.valueOf(String.valueOf(evt.getOldValue()));
					}
					model.setNbIterations(iterations);
				}
			});
			model.setNbIterations(1000);
		}
		return nbIterations;
	}

	public JComboBox<String> getFitterComboBox() {
		if (fitterComboBox == null) {
			final String[] fitt = { FitStyleEnum.AMOEBA.getLabel(), FitStyleEnum.LEVENBERG.getLabel() };
			fitterComboBox = new JComboBox<>(fitt);
			fitterComboBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (fitterComboBox.getSelectedItem().toString().equals(FitStyleEnum.LEVENBERG.getLabel())) {
						model.setFitStyle(FitStyleEnum.LEVENBERG);
					}
					else if (fitterComboBox.getSelectedItem().toString().equals(FitStyleEnum.AMOEBA.getLabel())) {
						model.setFitStyle(FitStyleEnum.AMOEBA);
					}
				}
			});
		}
		return fitterComboBox;
	}

	private JComponent getSouthFitPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.add(getActionPanel());
		panel.add(getOperationAndHistory());

		return panel;
	}

	private JPanel getOperationAndHistory() {
		if (operationAndHistory == null) {
			operationAndHistory = new JPanel();
			operationAndHistory.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
					"Operation"));
			BoxLayout opAndHistLayout = new BoxLayout(operationAndHistory, BoxLayout.Y_AXIS);
			operationAndHistory.setLayout(opAndHistLayout);
			JPanel operationFit = new JPanel(new GridLayout(2, 1, 4, 4));
			operationFit.add(getSubtractFitButton());
			operationFit.add(getDivideButton());
			operationFit.add(getToggleButtonOverlayFit());
			operationFit.add(getToggleButtonPlotResidual());

			operationAndHistory.add(operationFit);
			operationAndHistory.add(Box.createRigidArea(new Dimension(0, 4)));
			operationAndHistory.add(getHistoryPanel());
		}
		return operationAndHistory;
	}

	private HistoryPanel<XYSpectrumSeries> getHistoryPanel() {
		if (historyPanel == null) {
			historyPanel = new HistoryPanel<>();
			historyPanel.addHistoryListener(this);
		}
		return historyPanel;
	}

	public JToggleButton getToggleButtonPlotResidual() {
		if (toggleButtonPlotResidual == null) {
			// Button Plot Residual
			toggleButtonPlotResidual = new JToggleButton(SHOW_RESIDUAL_STRING);
			toggleButtonPlotResidual.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					setSelectedResidual(toggleButtonPlotResidual.isSelected());
				}
			});
		}
		return toggleButtonPlotResidual;
	}

	public JToggleButton getToggleButtonOverlayFit() {
		if (toggleButtonOverlayFit == null) {
			toggleButtonOverlayFit = new JToggleButton(SHOW_FIT_STRING);
			toggleButtonOverlayFit.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					setSelectedFit(toggleButtonOverlayFit.isSelected());
				}
			});
			toggleButtonOverlayFit.setPreferredSize(new Dimension(136, 25));
		}
		return toggleButtonOverlayFit;
	}

	public JButton getDivideButton() {
		if (divideButton == null) {
			divideButton = new JButton("Divide by fit");
			divideButton.setEnabled(false);
			divideButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					toggleButtonOverlayFit.setSelected(false);
					toggleButtonOverlayFit.setText(SHOW_FIT_STRING);
					divideButton.setEnabled(false);
					initPlotResidualButton();
					fireOverlayFitClicked();
					fireDivideByFitClicked();
				}
			});
		}
		return divideButton;
	}

	public JButton getSubtractFitButton() {
		if (subtractFitButton == null) {
			subtractFitButton = new JButton("Subtract fit");
			subtractFitButton.setEnabled(false);
			subtractFitButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					toggleButtonOverlayFit.setSelected(false);
					toggleButtonOverlayFit.setText(SHOW_FIT_STRING);
					subtractFitButton.setEnabled(false);
					initPlotResidualButton();
					fireOverlayFitClicked();
					fireSubstractFitClicked();
				}
			});
		}
		return subtractFitButton;
	}

	private JPanel getActionPanel() {
		if (actionPanel == null) {
			actionPanel = new JPanel(new GridLayout(3, 2, 5, 3));

			actionPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Actions"));

			actionPanel.add(getFitButton());
			actionPanel.add(getFitAllbutton());

			actionPanel.add(getSelectButton());
			actionPanel.add(getSaveButton());

			actionPanel.add(getLoadConfigButton());
			actionPanel.add(getSaveConfigButton());
		}

		return actionPanel;
	}

	public JButton getSaveConfigButton() {
		if (saveConfigButton == null) {
			saveConfigButton = new JButton("Save Config");
			saveConfigButton.setMargin(new Insets(0, 0, 0, 0));
			saveConfigButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					saveConfig();
				}
			});
		}
		return saveConfigButton;
	}

	public JButton getLoadConfigButton() {
		if (loadConfigButton == null) {
			loadConfigButton = new JButton("Load Config");
			loadConfigButton.setMargin(new Insets(0, 0, 0, 0));
			loadConfigButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					loadConfig();
				}
			});
		}
		return loadConfigButton;
	}

	public JButton getSaveButton() {
		if (saveButton == null) {
			saveButton = new JButton("Save");
			saveButton.setMargin(new Insets(0, 0, 0, 0));
			saveButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					fireSaveClicked();
				}
			});
		}
		return saveButton;
	}

	public JButton getFitAllbutton() {
		if (fitAllbutton == null) {
			fitAllbutton = new JButton("Fit all");
			fitAllbutton.setMargin(new Insets(0, 0, 0, 0));
			fitAllbutton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					LinkedList<FittingItem> fitComponents = getActiveLinkedListFittingItem();
					if (fitComponents == null || fitComponents.isEmpty()) {
						JOptionPane.showMessageDialog(FitPanel.this,
								"You must have at least one ACTIVE Component",
								"Error: Fitting", JOptionPane.ERROR_MESSAGE);
					} else {
						fireFitAllClicked();
					}
				}
			});
		}
		return fitAllbutton;
	}

	public JButton getFitButton() {
		if (fitButton == null) {
			fitButton = new JButton("Fit current");
			fitButton.setMargin(new Insets(0, 0, 0, 0));
			fitButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					fireFitCurrentClicked();
				}
			});
		}
		return fitButton;
	}

	public JPopupMenu getPopupAddModel() {
		if (popupFitting == null) {
			popupFitting = new JPopupMenu();

			JMenu menuBaseline = new JMenu("Add Baseline");
			JMenu menuLine = new JMenu("Add Line");

			menuBaseline.add(getMenuBaseLinePoly());
			menuBaseline.add(getMenuBaseLineSin());
			menuLine.add(getMenuLineGaus());
			menuLine.add(getMenuLineLorentz());
			menuLine.add(getMenuLineVoight());
			menuLine.add(getMenuLineSinc());

			popupFitting.add(menuBaseline);
			popupFitting.add(menuLine);
			popupFitting.addSeparator();
			popupFitting.add(getMenuClearAll());
			popupFitting.add(getMenuSelectAll());
			popupFitting.add(getMenuUnSelectAll());
		}
		return popupFitting;
	}

	public JMenuItem getMenuUnSelectAll() {
		if (menuUnSelectAll == null) {
			menuUnSelectAll = new JMenuItem("UnSelect All");
			menuUnSelectAll.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					selectAllFittings(false);
				}
			});
		}
		return menuUnSelectAll;
	}

	public JMenuItem getMenuSelectAll() {
		if (menuSelectAll == null) {
			menuSelectAll = new JMenuItem("Select All");
			menuSelectAll.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					selectAllFittings(true);
				}
			});
		}
		return menuSelectAll;
	}

	public JMenuItem getMenuClearAll() {
		if (menuClearAll == null) {
			menuClearAll = new JMenuItem("Clear All");
			menuClearAll.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					clearFittings();
				}
			});
		}
		return menuClearAll;
	}

	public JMenuItem getMenuLineSinc() {
		if (menuLineSinc == null) {
			menuLineSinc = new JMenuItem("Sinc Profile");
			menuLineSinc.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					addFitModel("Line :  Sinc Profile", FitModelEnum.SINC);
				}
			});
		}
		return menuLineSinc;
	}

	public JMenuItem getMenuLineVoight() {
		if (menuLineVoight == null) {
			menuLineVoight = new JMenuItem("Voigt Profile");
			menuLineVoight.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					addFitModel("Line :  Voigt Profile", FitModelEnum.VOIGT);
				}
			});
		}
		return menuLineVoight;
	}

	public JMenuItem getMenuLineLorentz() {
		if (menuLineLorentz == null) {
			menuLineLorentz = new JMenuItem("Lorentzian");
			menuLineLorentz.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					addFitModel("Line :  Lorentzian", FitModelEnum.LORENTZ);
				}
			});
		}
		return menuLineLorentz;
	}

	public JMenuItem getMenuLineGaus() {
		if (menuLineGaus == null) {
			menuLineGaus = new JMenuItem("Gaussian");
			menuLineGaus.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					addFitModel("Line :  Gaussian", FitModelEnum.GAUSS);
				}
			});
		}
		return menuLineGaus;
	}

	public JMenuItem getMenuBaseLineSin() {
		if (menuBaseLineSin == null) {
			menuBaseLineSin = new JMenuItem("Sinus");
			menuBaseLineSin.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					addFitModel("Baseline :  Sinus", FitModelEnum.SIN);
				}
			});
		}
		return menuBaseLineSin;
	}

	public JMenuItem getMenuBaseLinePoly() {
		if (menuBaseLinePoly == null) {
			menuBaseLinePoly = new JMenuItem("Polynomial");
			menuBaseLinePoly.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					addFitModel("Baseline :  Polynomial", FitModelEnum.POLY);
				}
			});
		}
		return menuBaseLinePoly;
	}

	private void selectAllFittings(boolean select) {
		boolean selectTemp = !select;
		for (FittingItem fittingItem : listFittingItem.geListFittingItem()) {
			fittingItem.setButtonOff(selectTemp);

		if (selectTemp)
			((JToggleButton) fittingItem.getButtonOnOff()).setText("OFF");
		else
			((JToggleButton) fittingItem.getButtonOnOff()).setText("ON");
		}
	}

	public void addFitModel(String title, FitModelEnum model) {
		FittingItem ft = this.listFittingItem.addFittingItem(title, model);
		panelFitting.add(ft);
		ft.revalidate();
		panelFitting.revalidate();
	}

	/**
	 * Copy the parameters of a given FittingItem.
	 *
	 * @param ftcopy The FittingItem.
	 * @return the new FittingItem.
	 */
	public FittingItem addFittings(FittingItem ftcopy) {
		FittingItem ft = this.listFittingItem.addFittingItem(ftcopy.getTitle(), ftcopy.getModel());
		ft.setParametersValues(ftcopy.getParametersValues());
		ft.setParametersBlocked(ftcopy.getParametersBlocked());
		ft.setButtonOff(!ftcopy.isActive());
		panelFitting.add(ft);
		ft.revalidate();
		panelFitting.revalidate();

		return ft;
	}

	public void initPlotResidualButton() {
		toggleButtonPlotResidual.setSelected(false);
		toggleButtonPlotResidual.setText(SHOW_RESIDUAL_STRING);
	}

	public boolean isSelectedFit() {
		return toggleButtonOverlayFit.isSelected();
	}

	public void setSelectedFit(boolean select) {
		changeOverlayButton(select);
		fireOverlayFitClicked();
	}

	public boolean isSelectedResidual() {
		return toggleButtonPlotResidual.isSelected();
	}

	public void setSelectedResidual(boolean select) {
		changeResidualButton(select);
		fireOverlayResidualFitClicked();
	}

	public void clearFittings() {
		listFittingItem.clearListFittingItem();

		// Remove from panel
		panelFitting.removeAll();
		panelFitting.revalidate();
		panelFitting.repaint();
	}

	public void displayFittingItem() {
		Iterator<FittingItem> it = listFittingItem.getListFittingItemIterator();

		while (it.hasNext()) {
			panelFitting.add(it.next());
		}
		panelFitting.revalidate();
	}

	public void removeResidual() {
		if (toggleButtonPlotResidual.isSelected())
			toggleButtonPlotResidual.doClick();
	}

	public void saveOriginalFit() {
		historyPanel.initPanelFittingButtons();
	}

	public void addFitPanelListener(FitPanelListener listener) {
		listeners.add(FitPanelListener.class, listener);
	}

	public void removeFitPanelListener(FitPanelListener l) {
		listeners.remove(FitPanelListener.class, l);
	}

	public void fireFitCurrentClicked() {
		FitPanelListener[] listenerList = listeners.getListeners(FitPanelListener.class);

		for (FitPanelListener listener : listenerList) {
			listener.fitCurrentClicked(new EventObject(this));
		}
	}

	public void fireAddIntervalMarker(InterValMarkerCassis marker) {
		FitPanelListener[] listenerList = listeners.getListeners(FitPanelListener.class);

		for (FitPanelListener listener : listenerList) {
			listener.addIntervalMarker(marker);
		}
	}

	private void fireSelectFileClicked() {
		FitPanelListener[] listenerList = listeners.getListeners(FitPanelListener.class);

		for (FitPanelListener listener : listenerList) {
			listener.selectFileClicked(new EventObject(this));
		}
	}

	private void fireSaveClicked() {
		FitPanelListener[] listenerList = listeners.getListeners(FitPanelListener.class);

		for (FitPanelListener listener : listenerList) {
			listener.saveFitClicked(new EventObject(this));
		}
	}

	public void fireFitAllClicked() {
		FitPanelListener[] listenerList = listeners.getListeners(FitPanelListener.class);

		for (FitPanelListener listener : listenerList) {
			listener.fitAllClicked(new EventObject(this));
		}
	}

	public void fireSubstractFitClicked() {
		FitPanelListener[] listenerList = listeners.getListeners(FitPanelListener.class);

		for (FitPanelListener listener : listenerList) {
			listener.substractFitClicked(new EventObject(this));
		}
	}

	public void fireOverlayFitClicked() {
		FitPanelListener[] listenerList = listeners.getListeners(FitPanelListener.class);

		for (FitPanelListener listener : listenerList) {
			listener.overlayFitClicked(new EventObject(this));
		}
	}

	public void fireOverlayResidualFitClicked() {
		FitPanelListener[] listenerList = listeners.getListeners(FitPanelListener.class);

		for (FitPanelListener listener : listenerList) {
			listener.overlayResidualClicked(new EventObject(this));
		}
	}

	public void fireHistoricFitClicked() {
		FitPanelListener[] listenerList = listeners.getListeners(FitPanelListener.class);

		for (FitPanelListener listener : listenerList) {
			listener.historicFitClicked(new EventObject(this));
		}
	}

	public void fireDivideByFitClicked() {
		FitPanelListener[] listenerList = listeners.getListeners(FitPanelListener.class);

		for (FitPanelListener listener : listenerList) {
			listener.divideByFitClicked(new EventObject(this));
		}
	}

	public void fireResetAllSelectionClicked() {
		FitPanelListener[] listenerList = listeners.getListeners(FitPanelListener.class);
		for (FitPanelListener listener : listenerList) {
			listener.resetAllSelectionClicked(new EventObject(this));
		}
		listFittingItem.resetAllDefaultValue();
	}

	public FitResult doFit(Double1d xdatasOrigin, Double1d ydatasOrigin) {
		AbstractModel abstractModel = null;
		LinkedList<AbstractModel> fitCompo = null;

		double offsetX = 0.;
		double multX = 0.;
		LinkedList<FittingItem> activeListFittingItem = listFittingItem.getActiveListFittingItem();
		if (listFittingItem.checkParameters(activeListFittingItem,
				model.getFitStyle(), this)) {
			if (verbose) {
				System.out.println("xdatasOrigin = " + "[ nbPoints : "+ xdatasOrigin.getSize()+ "; "+
						xdatasOrigin.get(0) + "; " + xdatasOrigin.get(xdatasOrigin.getSize()-1)+ "]");
			}
			offsetX = FitModel.computeXOffset(xdatasOrigin);
			Double1d xdatasTofit = xdatasOrigin.copy().add(offsetX);

			multX = FitModel.computeXMult(xdatasTofit);
			xdatasTofit = xdatasTofit.multiply(multX);
			if (verbose) {
				System.out.println("offsetX = " + offsetX + ", multX = " + multX);
				System.out.println("xdatasTofit = " + "[nbPoints = "+ xdatasTofit.getSize()+ "; "+
				xdatasTofit.get(0) + "; " + xdatasTofit.get(xdatasTofit.getSize()-1)+ "]");
			}
			abstractModel = model.buildAbstractModel(
					listFittingItem.convert(activeListFittingItem, offsetX, multX, model.getFitStyle()), model.getFitStyle());

			try {
				if (verbose) {
					System.out.println("abstractModel parameters = " + abstractModel.getParameters());
				}

				final double tolerance = Math.max(Math.abs(ydatasOrigin.get(0)),
						Math.abs(ydatasOrigin.get(ydatasOrigin.getSize()-1))/10000000.0);
				if (verbose) {
					System.out.println("tolerance before = " + tolerance +
							", tolerance after = " + model.getTolerance() +
							", nbIterations =" + model.getNbIterations() +
						",fitStyle = " + model.getFitStyle());
				}
				abstractModel = model.computeFit(xdatasTofit, ydatasOrigin, model.getFitStyle(),
						model.getNbIterations(),
						model.getTolerance(), abstractModel);
				if (verbose) {
					System.out.println("abstractModel parameters fitted= " + abstractModel.getParameters());
				}
				fitCompo = FitModel.extractCompo(activeListFittingItem, abstractModel);
				listFittingItem.refreshParameters(activeListFittingItem, abstractModel, offsetX, multX);
				Double1d ydatasFited = abstractModel.result(xdatasTofit);
				refreshRMS(xdatasTofit, ydatasOrigin.copy().subtract(ydatasFited));

				toggleButtonOverlayFit.setSelected(true);
				toggleButtonOverlayFit.setText(HIDE_FIT_STRING);
				subtractFitButton.setEnabled(true);
				initPlotResidualButton();
			} catch (Exception e) {
				LOGGER.error("Error during the fit", e);
				JOptionPane.showMessageDialog(this, "Error" + e.getMessage(), "Error during the fit",
						JOptionPane.OK_OPTION);
			}
		}

		lastFitResult = new FitResult(fitCompo, abstractModel, offsetX, multX);
		return lastFitResult;
	}

	private void refreshRMS(Double1d xdatasTofit, Double1d ydatasResidual) {
		Double1d param = new Double1d();

		double[] rms = model.computeRms(xdatasTofit, ydatasResidual, param, 1);
		listFittingItem.refreshRms(rms);
	}

	/**
	 * @return the lastFitResult
	 */
	public final FitResult getLastFitResult() {
		return lastFitResult;
	}

	public void doGaussianDefaultParametersAction(double maxAmplitude, double shift, double fwhm) {
		final InterValMarkerCassis lastSelection = markerManager.getLastSelection();
		if (lastSelection != null) {
			fwhm = Math.abs(lastSelection.getEndValueCassis()- lastSelection.getStartValueCassis())/2;
		}
		listFittingItem.doGaussianDefaultParametersAction(maxAmplitude, shift, fwhm);
	}

	/**
	 * @return the linkedListFittingItem
	 */
	public final LinkedList<FittingItem> getActiveLinkedListFittingItem() {
		return listFittingItem.getActiveListFittingItem();
	}

	public final LinkedList<FittingItem> getListFittingItem() {
		return listFittingItem.geListFittingItem();
	}

	public int getOversample() {
		return Integer.valueOf(oversamplingFit.getText());
	}

	public void changeOverlayButton(boolean serieVisible) {
		toggleButtonOverlayFit.setSelected(serieVisible);
		if (toggleButtonOverlayFit.isSelected()) {
			toggleButtonOverlayFit.setText(HIDE_FIT_STRING);

		}
		else {
			toggleButtonOverlayFit.setText(SHOW_FIT_STRING);
		}
	}

	public void changeResidualButton(boolean serieVisible) {
		toggleButtonPlotResidual.setSelected(serieVisible);
		if (toggleButtonPlotResidual.isSelected()) {
			toggleButtonPlotResidual.setText(HIDE_RESIDUAL_STRING);
		}
		else {
			toggleButtonPlotResidual.setText(SHOW_RESIDUAL_STRING);
		}
	}

	public void updateHistory(XYSpectrumSeries serie) {
		historyPanel.updateHistoryButtons(serie);
	}

	public void initHistory(XYSpectrumSeries serie) {
		historyPanel.initHistory(serie);
	}

	public void setHistoryOriginal() {
		historyPanel.getModel().getOriginal();
	}

	public XYSpectrumSeries getCurrentSerie() {
		return historyPanel.getCurrentSerie();
	}

	@Override
	public void nextHistoryAction(EventObject ev) {
		fireHistoricFitClicked();
	}

	@Override
	public void originalHistoryAction(EventObject ev) {
		fireHistoricFitClicked();
		if (toggleButtonOverlayFit.isSelected())
			toggleButtonOverlayFit.doClick();
		if (toggleButtonPlotResidual.isSelected())
			toggleButtonPlotResidual.doClick();
	}

	@Override
	public void previousHistoryAction(EventObject ev) {
		fireHistoricFitClicked();
	}

	/**
	 * @return the markerManager
	 */
	public final MarkerManager getMarkerManager() {
		return markerManager;
	}

	/**
	 * @return the selectButton
	 */
	public final JButton getSelectButton() {
		if (selectButton == null) {
			selectButton = new JButton("Select file");
			selectButton.setMargin(new Insets(0, 0, 0, 0));
			selectButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					fireSelectFileClicked();
				}
			});
		}
		return selectButton;
	}

	public XAxisCassis getXAxis() {
		return model.getXAxis();
	}

	public void setXAxis(XAxisCassis xAxisCassis) {
		model.setXAxis(xAxisCassis);
	}

	public FitModel getModel() {
		return model;
	}

	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (FitModel.NB_ITERATIONS_EVENT.equals(event.getSource())) {
			getNbiterations().setValue(Long.valueOf(model.getNbIterations()));
		} else if (FitModel.OVERSAMPLING_FIT_EVENT.equals(event.getSource())) {
			getOversamplingFit().setValue(Long.valueOf(model.getOversamplingFit()));
		} else if (FitModel.FIT_STYLE_EVENT.equals(event.getSource())) {
			if (model.getFitStyle() == FitStyleEnum.LEVENBERG) {
				fitterComboBox.setSelectedItem(FitStyleEnum.LEVENBERG.getLabel());
			}
			else if (model.getFitStyle() == FitStyleEnum.AMOEBA) {
				fitterComboBox.setSelectedItem(FitStyleEnum.AMOEBA.getLabel());
			}
		}else if (FitModel.TOLERANCE_FIT_EVENT.equals(event.getSource())) {
			getTolerance().setValue(Double.valueOf(model.getTolerance()));
		}
	}

	/**
	 * This open a {@link JFileChooser} to select a file, then save the configuration in it.
	 */
	private void saveConfig() {
		JFileChooser fc = new CassisJFileChooser(Software.getFitConfigPath(),
				Software.getLastFolder("fit-config"));
		fc.setDialogTitle("Save your fit configuration:");
		fc.setDialogType(JFileChooser.SAVE_DIALOG);
		fc.setFileFilter(new FileNameExtensionFilter("Fit configuration (*.fitc)","fitc"));
		if (fc.showSaveDialog(FitPanel.this) == JFileChooser.APPROVE_OPTION) {
			File selectedFile = fc.getSelectedFile();
			if (!selectedFile.getAbsolutePath().endsWith(".fitc")) {
				selectedFile = new File(selectedFile.getAbsolutePath() + ".fitc");
			}
			control.save(selectedFile);
			Software.setLastFolder("fit-config", fc.getSelectedFile().getParent());
		}
	}

	/**
	 * This open a {@link JFileChooser} to select a file, then load the configuration in it.
	 */
	private void loadConfig() {
		JFileChooser fc = new CassisJFileChooser(Software.getFitConfigPath(),
				Software.getLastFolder("fit-config"));

		fc.setDialogTitle("Load your fit configuration:");
		fc.setDialogType(JFileChooser.OPEN_DIALOG);
		fc.setFileFilter(new FileNameExtensionFilter("Fit configuration (*.fitc)","fitc"));
		if (fc.showOpenDialog(FitPanel.this) == JFileChooser.APPROVE_OPTION) {
			control.load(fc.getSelectedFile());
			Software.setLastFolder("fit-config", fc.getSelectedFile().getParent());
		}
	}

	public static void setVerbose(boolean verbose) {
		FitPanel.verbose = verbose;
	}
}
