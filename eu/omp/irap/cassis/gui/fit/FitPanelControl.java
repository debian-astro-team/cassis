/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit;

import java.io.File;
import java.util.List;

import javax.swing.JOptionPane;

import eu.omp.irap.cassis.gui.plot.util.InterValMarkerCassis;

public class FitPanelControl {

	private FitPanel fitPanel;
	private FitConfig config;


	public FitPanelControl() {
		super();
		config = new FitConfig();
	}

	public FitPanel getCurrentFitPanel() {
		return fitPanel;
	}

	public void setFitPanel(FitPanel fitPanel) {
		this.fitPanel = fitPanel;
	}

	public void load(File file) {
		if (file != null && config.loadFile(file)) {
			List<FittingItem> list = config.getListFittingItem();
			fitPanel.clearFittings();
			fitPanel.fireResetAllSelectionClicked();
			List<InterValMarkerCassis> listMarker = config.getListMarker();
			for (InterValMarkerCassis marker : listMarker) {
				fitPanel.fireAddIntervalMarker(marker);
			}
			for (FittingItem item : list) {
				fitPanel.addFittings(item);
			}
			fitPanel.getModel().setFitStyle(config.getFitStyle());
			fitPanel.getModel().setNbIterations(config.getNbIter());
			fitPanel.getModel().setOversamplingFit(config.getNbOversampl());
		}
	}

	public void save(File file) {
		if (file != null && file.exists()) {
			String message = "This file " + file.toString() + " already exists.\n" + "Do you want to replace it?";
			// Modal dialog with yes/no button
			int result = JOptionPane.showConfirmDialog(fitPanel, message, "Replace existing file?",
					JOptionPane.YES_NO_OPTION);
			if (result != JOptionPane.YES_OPTION) {
				file = null;
			}
		}
		if (file != null) {
			config.setListMarker(fitPanel.getMarkerManager().getListMarkerDisjoint());
			config.setxAxis(fitPanel.getXAxis());
			config.setFitStyle(fitPanel.getModel().getFitStyle());
			config.setNbIter(fitPanel.getModel().getNbIterations());
			config.setNbOversampl(fitPanel.getModel().getOversamplingFit());
			config.setTolerance(fitPanel.getModel().getTolerance());
			config.setListFittingItem(fitPanel.getListFittingItem());

			config.saveFile(file);
		}

	}

	public void doGaussianDefaultParametersAction(double maxAmplitude, double x0, Double fwhm_DEFAULT) {
		getCurrentFitPanel().doGaussianDefaultParametersAction(maxAmplitude, x0, fwhm_DEFAULT);

	}
}
