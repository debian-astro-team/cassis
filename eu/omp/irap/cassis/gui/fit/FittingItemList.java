/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit;

import herschel.ia.numeric.Double1d;
import herschel.ia.numeric.toolbox.fit.AbstractModel;

import java.awt.Component;
import java.text.DecimalFormatSymbols;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;

import javax.swing.JOptionPane;

import eu.omp.irap.cassis.fit.components.impl.FitPolynomialComponent;
import eu.omp.irap.cassis.fit.computing.FitNormalizationParameters;

/**
 * @author glorian
 *
 *
 */

public class FittingItemList {

	private LinkedList<FittingItem> linkedListFittingItem;
	private FitPanel panel;
	private Integer polyVal = null;
	private Double1d polyParam = null;


	/**
	 * Constructor.
	 *
	 * @param panel The {@link FitPanel}.
	 */
	public FittingItemList(FitPanel panel) {
		super();
		linkedListFittingItem = new LinkedList<>();
		this.panel = panel;
	}

	/**
	 * Add a {@link FittingItem}.
	 *
	 * @param title The title of the {@link FittingItem}.
	 * @param model The type of {@link FittingItem}.
	 * @return a new {@link FittingItem}.
	 */
	public FittingItem addFittingItem(String title, FitModelEnum model) {
		FittingItem ft = null;
		if (model == FitModelEnum.POLY)
			ft = new FittingItem(title, false, this, model);
		else
			ft = new FittingItem(title, this, model);
		if (FitModelEnum.isLine(model)) {
			ft.setChangeDefaultValue(true);
			for (FittingItem item : linkedListFittingItem) {
				item.setChangeDefaultValue(false);
			}
		}
		linkedListFittingItem.addLast(ft);
		return ft;
	}

	public void clearListFittingItem() {
		// Remove from data
		linkedListFittingItem.clear();
	}

	public void deleteFittingItem(FittingItem fittingIten) {
		fittingIten.revalidate();
		int index = linkedListFittingItem.indexOf(fittingIten);
		linkedListFittingItem.remove(index);
		LinkedList<FittingItem> copy = new LinkedList<>(linkedListFittingItem);
		panel.clearFittings();
		linkedListFittingItem = copy;
		panel.displayFittingItem();

		if (FitModelEnum.POLY.equals(fittingIten.getModelName())) {
			polyVal = null;
		}
	}

	public Iterator<FittingItem> getListFittingItemIterator() {
		return linkedListFittingItem.iterator();
	}

	public void refreshRms(double[] rms) {
		for (int i = 0; i < linkedListFittingItem.size(); i++) {
			FittingItem item = linkedListFittingItem.get(i);
			if (FitModelEnum.POLY.equals(item.getModelName())) {
				item.setStdDev(rms);
			}
		}
	}

	public Integer havePolynomialModel(Double1d param) {
		Integer val = polyVal;
		if (polyParam != null)
			param.append(polyParam.copy());
		return val;
	}

	public LinkedList<AbstractModel> refreshParameters(LinkedList<FittingItem> activeListFittingItem,
			AbstractModel abstractModel, double offsetX, double multX) {
		int offset = 0;

		LinkedList<AbstractModel> listModel = new LinkedList<>();

		polyVal = null;
		polyParam = null;

		for (int i = 0; i < activeListFittingItem.size(); i++) {
			FittingItem item = activeListFittingItem.get(i);
			Double1d param = new Double1d();
			Double1d paramDev = new Double1d();

			int nbValue = 0;
			if (FitModelEnum.POLY.equals(item.getModelName())) {
				nbValue = (int) item.getParametersValues()[0] + 1;
			}
			else
				nbValue = item.getParameters().size();

			for (int j = 0; j < nbValue; j++) {
				param.append(abstractModel.getParameters().get(j + offset));
				paramDev.append(abstractModel.getStandardDeviations().get(j + offset));
			}

			offset += nbValue;
			refreshParams(item, param, paramDev, offsetX, multX);

		}
		return listModel;
	}

	private void refreshParams(FittingItem item, Double1d param, Double1d paramDev, double offsetX, double multX) {
		final DecimalFormatSymbols scienceFormatSymbols = new DecimalFormatSymbols();
		scienceFormatSymbols.setDecimalSeparator('.');
		if (FitModelEnum.POLY.equals(item.getModelName())) {
			double[] val = new double[1];
			val[0] = item.getParametersValues()[0];

			polyVal = Integer.valueOf((int) val[0]);
			polyParam = new Double1d(param.toArray());
			item.setParametersValues(val);

			item.setToolTipText(FitPolynomialComponent.getEquationPolyParam(
			FitPolynomialComponent.changeParametersNormalization(new FitNormalizationParameters(offsetX, multX, 0.0, 1.0),
														param.toArray())));

		}
		else {
			double[] val = param.toArray();
			double[] valDev = paramDev.toArray();

			if (FitModelEnum.GAUSS.equals(item.getModelName())) {

				val[FitModelEnum.GAUSS_X0_INDEX] = val[FitModelEnum.GAUSS_X0_INDEX] / multX - offsetX;
				valDev[FitModelEnum.GAUSS_X0_INDEX] = valDev[FitModelEnum.GAUSS_X0_INDEX] / multX;
				//FWHM Gaussian = sigma * 2log..
				//also for stdDev
				val[FitModelEnum.GAUSS_FWHM_INDEX] = (val[FitModelEnum.GAUSS_FWHM_INDEX] * FitConstant.getFWHM())
						/ multX;
				valDev[FitModelEnum.GAUSS_FWHM_INDEX] = (valDev[FitModelEnum.GAUSS_FWHM_INDEX] * FitConstant.getFWHM())
						/ multX;
			}
			else if (FitModelEnum.LORENTZ.equals(item.getModelName())) {
				//Io Lorentzian = Io / sigma
				//also for stdDev
				//TODO to put only in hipe
				//					val[0] = val[0]/val[2];
				//					valDev[0] = valDev[0]/val[2];
				val[FitModelEnum.LORENTZ_X0_INDEX] = val[FitModelEnum.LORENTZ_X0_INDEX] / multX - offsetX;
				valDev[FitModelEnum.LORENTZ_X0_INDEX] = valDev[FitModelEnum.LORENTZ_X0_INDEX] / multX;
				val[FitModelEnum.LORENTZ_FWHM_INDEX] = (val[FitModelEnum.LORENTZ_FWHM_INDEX] * FitConstant.FWHM_LORENTZIAN)
						/ multX;
				valDev[FitModelEnum.LORENTZ_FWHM_INDEX] = (valDev[FitModelEnum.LORENTZ_FWHM_INDEX] * FitConstant.FWHM_LORENTZIAN)
						/ multX;
			}
			else if (FitModelEnum.SINC.equals(item.getModelName())) {
				val[FitModelEnum.SINC_P1_INDEX] = val[FitModelEnum.SINC_P1_INDEX] - offsetX;
				val[FitModelEnum.SINC_P2_INDEX] = val[FitModelEnum.SINC_P2_INDEX] * FitConstant.FWHM_SINC;
				valDev[FitModelEnum.SINC_P2_INDEX] = valDev[FitModelEnum.SINC_P2_INDEX] * FitConstant.FWHM_SINC;
			}
			else if (FitModelEnum.VOIGT.equals(item.getModelName())) {
				val[FitModelEnum.VOIGT_X0_INDEX] = val[FitModelEnum.VOIGT_X0_INDEX] / multX - offsetX;
				valDev[FitModelEnum.VOIGT_X0_INDEX] = valDev[FitModelEnum.VOIGT_X0_INDEX] / multX;
				val[FitModelEnum.VOIGT_FWHM_G_INDEX] = (val[FitModelEnum.VOIGT_FWHM_G_INDEX] * FitConstant.getFWHM())
						/ multX;
				valDev[FitModelEnum.VOIGT_FWHM_G_INDEX] = (valDev[FitModelEnum.VOIGT_FWHM_G_INDEX] * FitConstant
						.getFWHM()) / multX;
				val[FitModelEnum.VOIGT_FWHM_L_INDEX] = (val[FitModelEnum.VOIGT_FWHM_L_INDEX] * FitConstant.FWHM_LORENTZIAN)
						/ multX;
				valDev[FitModelEnum.VOIGT_FWHM_L_INDEX] = (valDev[FitModelEnum.VOIGT_FWHM_L_INDEX] * FitConstant.FWHM_LORENTZIAN)
						/ multX;
			}

			item.refreshValues(val);
			item.refreshStdDevValues(valDev);
		}
	}

	/**
	 * Set default value for line fit.
	 *
	 * @param maxAmplitude The max amplitude.
	 * @param shift The shift.
	 * @param fwhm The fwhm.
	 */
	public void doGaussianDefaultParametersAction(double maxAmplitude, double shift, double fwhm) {
		for (int i = 0; i < linkedListFittingItem.size(); i++) {
			FittingItem item = linkedListFittingItem.get(i);
			if (!item.isActive() || !item.isChangeDefaultValue())
				continue;

			FitModelEnum model = item.getModelName();
			if (FitModelEnum.isLine(model)) {
				double[] parameters = new double[3];

				ParamItem io = item.getParameters().get(0);
				ParamItem xo = item.getParameters().get(1);
				ParamItem fWHM1 = item.getParameters().get(2);

				if (io.isBlocked())
					parameters[0] = io.getValue();
				else
					parameters[0] = maxAmplitude;

				if (xo.isBlocked())
					parameters[1] = xo.getValue();
				else
					parameters[1] = shift;

				if (fWHM1.isBlocked())
					parameters[2] = fWHM1.getValue();
				else
					parameters[2] = fwhm;

				if (FitModelEnum.VOIGT.equals(model)) {
					double[] paramTemp = Arrays.copyOf(parameters, 4);
					parameters = paramTemp;
					ParamItem fWHM2 = item.getParameters().get(3);
					if (fWHM2.isBlocked())
						parameters[3] = fWHM2.getValue();
					else
						parameters[3] = fwhm;
				}

				item.setParametersValues(parameters);
				item.setChangeDefaultValue(false);
			}
		}
	}

	public LinkedList<FittingItem> getActiveListFittingItem() {
		LinkedList<FittingItem> list = new LinkedList<>();
		for (FittingItem item : linkedListFittingItem)
			if (item.isActive())
				list.add(item);
		return list;
	}

	public LinkedList<FittingItem> geListFittingItem() {
		return linkedListFittingItem;
	}

	public void resetAllDefaultValue() {
		for (FittingItem item : linkedListFittingItem)
			item.setChangeDefaultValue(false);
	}

	boolean checkParameters(LinkedList<FittingItem> activeListFittingItem, FitStyleEnum fitStyle, Component owner) {
		boolean res = true;
		if (activeListFittingItem.isEmpty()) {
			JOptionPane.showMessageDialog(owner, "You must have at least one ACTIVE Component", "Error: Fitting",
					JOptionPane.ERROR_MESSAGE);
			res = false;
		}
		else if (fitStyle == FitStyleEnum.LEVENBERG && haveAllValueBlocked(activeListFittingItem)) {
			JOptionPane.showMessageDialog(owner,
					"You must not block all parameters for a fit component in LEVENBERG-MARQUARDT Fitter",
					"Error: Fitting", JOptionPane.ERROR_MESSAGE);
			res = false;
		}
		return res;
	}

	/**
	 * Create a list of fittingModel with the correct of unit and the Xoffset.
	 *
	 * @param activeListFittingItem
	 * @param multX
	 * @param fitStyle
	 * @return
	 */
	LinkedList<FittingItemModel> convert(LinkedList<FittingItem> activeListFittingItem, double xOffset,
			double multX, FitStyleEnum fitStyle) {
		if (activeListFittingItem == null)
			return null;
		LinkedList<FittingItemModel> list = new LinkedList<>();
		for (FittingItem item : activeListFittingItem) {
			FittingItemModel fittingItemModel = new FittingItemModel(item.getModel(), item.getParametersValues(),
					item.getParametersBlocked());
			fittingItemModel.changeParameters(xOffset, multX, fitStyle);
			list.add(fittingItemModel);
		}
		return list;
	}

	private boolean haveAllValueBlocked(LinkedList<FittingItem> activeListFittingItem) {
		for (FittingItem item : activeListFittingItem) {
			boolean free = false;
			for (ParamItem param : item.getParameters()) {
				free = free || !param.isBlocked();
			}
			if (!free)
				return true;
		}
		return false;
	}
}
