/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit.advanced.interfaces;

import java.io.File;

import eu.omp.irap.cassis.fit.history.ICommand;
import eu.omp.irap.cassis.fit.util.FitException;

/**
 * Interface that represents a class which performs the actions
 * of the fit
 * @author bastienkovac
 */
public interface FitActionsInterface {

	/**
	 * Event when the fit of one data source has been performed.
	 * The associated event's value is the result obtained
	 */
	public static final String FIT_PERFORMED_EVENT = "fitPerformedEvent";
	/**
	 * Event when the fit of several data sources has been performed
	 * The associated event's value is the list of results obtained
	 */
	public static final String FIT_ALL_PERFORMED_EVENT = "fitAllPerformedEvent";
	/**
	 * Event when the fit of the global spectrum of a line analysis has been
	 * performed. The associated value is the list of the results linked to
	 * each individual configuration, with all parameters fixed so the configuration
	 * isn't computed again, but just the resulting curve is
	 */
	public static final String FIT_GLOBAL_PERFORMED_EVENT = "fitGlobalPerformedEvent";
	/**
	 * Event when an error happened during the fit.
	 * The associated event's value is the thrown {@link FitException}
	 */
	public static final String FIT_FAILED_EVENT = "fitFailedEvent";
	/**
	 * Event when the user clicked on the Select File button
	 */
	public static final String LOG_FILE_SELECTED_EVENT = "logFileSelectedEvent";
	/**
	 * Event when the user clicked on the Save Fit button
	 */
	public static final String FIT_SAVED_EVENT = "fitSavedEvent";
	/**
	 * Event when the user clicked on the Load Configuration button
	 */
	public static final String CONFIGURATION_LOADED_EVENT = "configurationLoadedEvent";
	/**
	 * Event when the user clicked on the Save Configuration button
	 */
	public static final String CONFIGURATION_SAVED_EVENT = "configurationSavedEvent";
	/**
	 * Event when the user clicked on the Substract button
	 */
	public static final String FIT_SUBSTRACTED_EVENT = "fitSubstractedEvent";
	/**
	 * Event when the user clicked on the Divide button
	 */
	public static final String FIT_DIVIDED_EVENT = "fitDividedEvent";
	/**
	 * Event when the user wants to display or not the fit curves.
	 * The associated event's value is a boolean, true if the curves must be displayed, false otherwise
	 */
	public static final String OVERLAY_FIT_EVENT = "overlayFitEvent";
	/**
	 * Event when the user wants to display or not the residual curve.
	 * The associated event's value is a boolean, true if the curve must be displayed, false otherwise
	 */
	public static final String OVERLAY_RESIDUAL_EVENT = "overlayResidualEvent";
	/**
	 * Event when the history has been restored to its original state
	 */
	public static final String ORIGINAL_RESTORED_EVENT = "originalRestoredEvent";
	/**
	 * Event when the last undoable action has been undone
	 */
	public static final String UNDO_EVENT = "undoEvent";
	/**
	 * Event when the last redoable action has been redone
	 */
	public static final String REDO_EVENT = "redoEvent";
	/**
	 * Event when an undoable command has been added to the history
	 */
	public static final String COMMAND_ADDED_EVENT = "commandAddedEvent";

	/**
	 * Performs the fit with the current configuration
	 */
	void performFit();

	/**
	 * Performs the fit on a set of source curves, with the same configuration.
	 */
	void performFitAll();

	/**
	 * Performs the fit on the reconstructed spectrum of the Line Analyse module
	 */
	void performGlobalFit();

	/**
	 * Compute the global configuration on the reconstructed spectrum of the Line Analysis
	 * module
	 */
	void computeGlobalConfiguration();

	/**
	 * Selects the file in which the rotational diagram configuration will be saved
	 */
	void selectLogFile();

	/**
	 * Save the current configuration in the log file
	 */
	void saveFit();

	/**
	 * Loads the fit configuration from the given file
	 *
	 * @param file File from which to load the configuration
	 */
	void loadConfiguration(File file);

	/**
	 * Saves the fit configuration in the given file
	 *
	 * @param file File in which to save the configuration
	 */
	void saveConfiguration(File file);

	/**
	 * Subtract the source curve by the fitted one.
	 */
	void substractFit();

	/**
	 * Divide the source curve by the fitted one
	 */
	void divideFit();

	/**
	 * Display or not the fit curves (general and components)
	 *
	 * @param display True if the curves must be displayed, false if not
	 */
	void overlayFit(boolean display);

	/**
	 * Display or not the residual curve
	 *
	 * @param display True if the curve must be displayed, false if not
	 */
	void overlayResidual(boolean display);

	/**
	 * Undoes all the actions that can be
	 */
	void restoreOriginal();

	/**
	 * Adds an undoable and redoable action to the fit module
	 *
	 * @param command The action to add
	 */
	void addCommand(ICommand command);

	/**
	 * Undoes the last action
	 */
	void undo();

	/**
	 * Redoes the last undone action
	 */
	void redo();

	/**
	 * Clears the current configuration
	 */
	void clearConfiguration();

}
