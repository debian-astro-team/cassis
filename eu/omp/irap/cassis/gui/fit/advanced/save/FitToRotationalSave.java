/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit.advanced.save;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

import eu.omp.irap.cassis.common.Telescope;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.database.access.MoleculeDescriptionDB;
import eu.omp.irap.cassis.gui.fit.TelescopeFitInterface;
import eu.omp.irap.cassis.gui.util.FileUtils;
import eu.omp.irap.cassis.properties.Software;

/**
 * Class used to save a fit result into a file readable by the Rotational Diagram module
 *
 * @author bastienkovac
 *
 */
public class FitToRotationalSave {

	private static final String NONE_UNIT = "None";
	private static final char SEPARATOR_CHAR = '\t';

	private DecimalFormat deltaFormat;
	private DecimalFormat veloFormat;
	private DecimalFormat freqFormat;
	private DecimalFormat intensityFormat;
	private DecimalFormat aijFormat;

	private static final String[] COLUMNS = {
			"id", "NumCompo", "Species", "QuantumNumbers", "Frequency", "Eup", "Gup",
			"Aij", "FitFreq", "DeltaFitFreq", "Vo", "deltaVo", "FWHM_G", "deltaFWHM_G",
			"FWHM_L", "deltaFWHM_L", "Intensity", "deltaIntensity", "FitFlux", "deltaFitFlux",
			"Freq.IntensityMax", "V.IntensityMax", "FWHM", "IntensityMax", "Flux1stMom",
			"deltaFlux1stMom", "rms", "deltaV", "Cal", "Size", "TelescopePath", "TelescopeName"
	};


	private File logFile;
	private boolean appendLog;

	private FitToRotationalComputations computations;
	private String telescopePath;
	private String telescopeName;


	/**
	 * Constructor
	 */
	public FitToRotationalSave() {
		DecimalFormatSymbols ds = new DecimalFormatSymbols();
		ds.setDecimalSeparator('.');

		deltaFormat = new DecimalFormat("###0.0000", ds);// 8c
		veloFormat = new DecimalFormat("#####0.000", ds);// 10c
		freqFormat = new DecimalFormat("######0.000", ds);// 11c
		intensityFormat = new DecimalFormat("##0.0000", ds);// 8c
		aijFormat = new DecimalFormat("0.##E0", ds);
	}

	/**
	 * Sets the parameters of the save
	 *
	 * @param logFile The file
	 * @param appendLog  True if we keep the current content of the file
	 */
	public void setLogParameters(File logFile, boolean appendLog) {
		this.logFile = logFile;
		this.appendLog = appendLog;
	}

	/**
	 * Sets the File in which the fit will be saved
	 *
	 * @param logFile The file
	 */
	public void setLogFile(File logFile) {
		this.logFile = logFile;
	}

	/**
	 * @return The file in which to log the fit
	 */
	public File getLogFile() {
		return logFile;
	}

	/**
	 * Sets if the saving will overwrite the current content
	 * of the log file or not
	 *
	 * @param appendLog True if we keep the current content of the file
	 */
	public void setAppendLog(boolean appendLog) {
		this.appendLog = appendLog;
	}

	/**
	 * @return True if the results are saved without ovewriting the current
	 * content of the log file
	 */
	public boolean isAppendLog() {
		return appendLog;
	}

	/**
	 * Returns if the file is properly initialized
	 *
	 * @return True if the log file is different from null
	 */
	public boolean isInitialized() {
		return this.logFile != null;
	}

	/**
	 * Initializes the parameters of the logging from scratch
	 *
	 * @param referenceView The JPanel used as reference to show the file choosers
	 * @param telescopeInterface The telescope interface
	 */
	public void initLogParameters(JPanel referenceView, TelescopeFitInterface telescopeInterface) {
		if (!initTelescope(referenceView, telescopeInterface)) {
			return;
		}
		initLogFile(referenceView);
	}

	/**
	 * Initializes the log file
	 *
	 * @param referenceView The JPanel used as reference to show the file choosers
	 */
	private void initLogFile(JPanel referenceView) {
		String[] choices = { "Create a new File", "Open an existing File", "Cancel" };
		int choice = JOptionPane.showOptionDialog(referenceView,
				"What do you want?", "Write the log in a file",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,
				null, choices, choices[0]);
		CassisJFileChooser fc = getFileChooser();
		if (choice >= 0) {
			if (choice == 1) { // open a file
				int returnVal = fc.showOpenDialog(referenceView);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					setLogParameters(fc.getSelectedFile(), true);
					Software.setLastFolder("fit", fc.getSelectedFile().getParent());
				}
			} else if (choice == 0) { // create a file
				createFile(referenceView, fc, "rotd");
			}
		}
	}

	/**
	 * Create a file if necessary
	 *
	 * @param referenceView The JPanel used as reference to show the file choosers
	 * @param fc The filechooser
	 * @param ext The allowed extension
	 */
	private void createFile(JPanel referenceView, JFileChooser fc, final String ext) {
		int returnVal = fc.showDialog(referenceView, "Create");
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File temp = fc.getSelectedFile();
			String fileName = temp.getPath();
			if (!FileUtils.getFileExtension(fileName).equals(ext))
				fileName = fileName + "." + ext;

			File file = new File(fileName);
			// replace the existing file or not
			if (file.exists()) {
				String message = "This file " + file.toString() +
						" already exists.\nDo you want to replace it?";
				// Modal dialog with yes/no button
				int result = JOptionPane.showConfirmDialog(referenceView, message,
						"Replace existing file?", JOptionPane.YES_NO_OPTION);
				if (result != JOptionPane.YES_OPTION) {
					file = null;
				}
			}
			if (file != null) {
				setLogParameters(file, false);
				Software.setLastFolder("fit", file.getParent());
			}
		}
	}

	/**
	 * @return A CassisFileChooser with *rotd files accepted
	 */
	private CassisJFileChooser getFileChooser() {
		CassisJFileChooser fc = new CassisJFileChooser(Software.getDataPath(), Software.getLastFolder("fit"));
		fc.resetChoosableFileFilters();
		String ext = null;
		FileNameExtensionFilter filter = null;
		ext = "rotd";
		filter = new FileNameExtensionFilter("Rotational Diagram file (*.rotd)", ext);
		fc.addChoosableFileFilter(filter);
		fc.setFileFilter(filter);
		return fc;
	}

	/**
	 * Initializes the telescope
	 *
	 * @param referenceView The JPanel used as reference to show the file choosers
	 * @param telescopeInterface The telescope interface
	 * @return True if the telescope has been initialized
	 */
	private boolean initTelescope(JPanel referenceView, TelescopeFitInterface telescopeInterface) {
		String tel = telescopeInterface.getTelescope();
		if (tel == null ||	"???".equals(tel) || "????".equals(tel)) {
			int retTel = JOptionPane.showConfirmDialog(referenceView,
					"You do not have selected a telescope. Do you want select one now?",
					"Telescope Error!", JOptionPane.YES_NO_CANCEL_OPTION);

			if (retTel == JOptionPane.YES_OPTION) {
				JFileChooser loadFile = new CassisJFileChooser(Software.getTelescopePath(),
						Software.getLastFolder("lte_radex-model-telescope"));
				loadFile.setDialogTitle("Select a telescope file");
				int ret = loadFile.showOpenDialog(null);
				if (ret == JFileChooser.APPROVE_OPTION) {
					telescopeInterface.setTelescope(loadFile.getSelectedFile().getAbsolutePath());
					Software.setLastFolder("lte_radex-model-telescope",
							loadFile.getSelectedFile().getParent());
				}
			} else if (retTel == JOptionPane.CANCEL_OPTION || retTel == JOptionPane.CLOSED_OPTION) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Saves the result of a fit represented by the computations object in a file usable by the rotational
	 * diagram module
	 *
	 * @param computations The object handling the computation of the fit
	 * @param telescope The telescope argument
	 * @param global True if the fit is a global one (no first moment)
	 *
	 * @throws IOException If an error occurred while writing in the file
	 * @throws IllegalArgumentException If one of the components of the fit isn't usable by the rotational diagram
	 */
	public void saveLinesInFile(FitToRotationalComputations computations, String telescope, boolean global) throws IOException, IllegalArgumentException {
		if (logFile == null) {
			return;
		}
		init(computations, telescope);
		String formatPath = logFile.getAbsolutePath() + ".txt";
		try (BufferedWriter outFull = new BufferedWriter(new FileWriter(logFile, appendLog));
			 BufferedWriter outFormat = new BufferedWriter(new FileWriter(formatPath, appendLog))) {
			if (!appendLog) {
				headerFile(computations, outFull, false);
				headerFile(computations, outFormat, true);
			}
			for (int i = 0 ; i < computations.getNbExpandedComponents() ; i++) {
				writeLine(i, outFull, false, global);
				writeLine(i, outFormat, true, global);
			}
		}
		if (!appendLog) {
			appendLog = true;
		}
	}

	/**
	 * Writes the data associated to the i-th line
	 *
	 * @param i The index of the line
	 * @param out The BufferedWriter
	 * @param format True if the data must be formatted
	 * @param global True if the fit is a global one (no first moment)
	 *
	 * @throws IOException If an error occurred while writing in the file
	 * @throws IllegalArgumentException If one of the components of the fit isn't usable by the rotational diagram
	 */
	private void writeLine(int i, BufferedWriter out, boolean format, boolean global) throws IOException, IllegalArgumentException {
		StringBuilder sb = new StringBuilder();

		// Line Info
		addData(sb, computations.getLineFromComponentIndex(i).getMolTag());
		addData(sb, computations.getReducedIndex(i) + 1);
		addData(sb, computations.getLineFromComponentIndex(i).getMolName());
		addData(sb, MoleculeDescriptionDB.constitueLine(computations.getLineFromComponentIndex(i).getQuanticN()));
		addData(sb, computations.getLineFromComponentIndex(i).getObsFrequency(), freqFormat, format);
		addData(sb, computations.getLineFromComponentIndex(i).getEUpK(), intensityFormat, format);
		addData(sb, computations.getLineFromComponentIndex(i).getGu());
		addData(sb, computations.getLineFromComponentIndex(i).getAij(), aijFormat, format);

		// Fit Component Info
		addData(sb, computations.getFitOriginFrequency(i), freqFormat, format);
		addData(sb, computations.getFitOriginFrequencyError(i), deltaFormat, format);
		addData(sb, computations.getFitOriginVelo(i), veloFormat, format);
		addData(sb, computations.getFitOriginVeloError(i), deltaFormat, format);
		addData(sb, computations.getFitFWHMGaussian(i), deltaFormat, format);
		addData(sb, computations.getFitFWHMGaussianError(i), deltaFormat, format);
		addData(sb, computations.getFitFWHMLorentzian(i), deltaFormat, format);
		addData(sb, computations.getFitFWHMLorentzianError(i), deltaFormat, format);
		addData(sb, computations.getFitIntensity(i), intensityFormat, format);
		addData(sb, computations.getFitIntensityError(i), intensityFormat, format);
		addData(sb, computations.getFitFlux(i), intensityFormat, format);
		addData(sb, computations.getFitFluxError(i), deltaFormat, format);

		// Fit Info
		addData(sb, computations.getDataFreqIntensityMax(), freqFormat, format);
		addData(sb, computations.getDataVelocityIntensityMax(i), veloFormat, format);
		addData(sb, computations.getDataSigmaFluxFirstMoment(i), deltaFormat, format);
		addData(sb, computations.getDataIntensityMax(), intensityFormat, format);
		if (global) {
			addData(sb, 0.0, intensityFormat, format);
		} else {
			addData(sb, computations.getDataFluxFirstMoment(i), intensityFormat, format);
		}
		addData(sb, computations.getDataDeltaFluxFirstMoment(), intensityFormat, format);
		addData(sb, computations.getFitComputedRms(), intensityFormat, format);
		addData(sb, computations.getDataDeltaVelocity(i), deltaFormat, format);

		// Misc
		addData(sb, 0.0, null, format);
		addData(sb, 0.0, null, format);
		addData(sb, telescopePath);
		addData(sb, telescopeName);

		out.write(sb.toString());
		out.newLine();
	}

	/**
	 * Adds data to the StringBuilder, followed by the separator character
	 *
	 * @param sb The StringBuilder
	 * @param data The data to add
	 * @param formatter The formatter to use
	 * @param format True if the data must be formatted
	 */
	private void addData(StringBuilder sb, double data, DecimalFormat formatter, boolean format) {
		sb.append(format(data, formatter, format)).append(SEPARATOR_CHAR);
	}

	/**
	 * Adds data to the StringBuilder, followed by the separator character
	 *
	 * @param sb The StringBuilder
	 * @param data The data to add
	 */
	private void addData(StringBuilder sb, int data) {
		sb.append(String.valueOf(data)).append(SEPARATOR_CHAR);
	}

	/**
	 * Adds data to the StringBuilder, followed by the separator character
	 *
	 * @param sb The StringBuilder
	 * @param data The data to add
	 */
	private void addData(StringBuilder sb, String data) {
		sb.append(data).append(SEPARATOR_CHAR);
	}

	/**
	 * Format the given data if necessary then returns its string representation,
	 * if the formatter is null or format is false, no format is used.
	 *
	 * @param data The data to format
	 * @param formatter The formatter to use
	 * @param format True if the data must be formatted
	 * @return The formatted data
	 */
	private String format(double data, DecimalFormat formatter, boolean format) {
		if (formatter != null && format) {
			return formatter.format(data);
		}
		return String.valueOf(data);
	}

	/**
	 * Writes the header for the RotD file
	 *
	 * @param computations The object handling the computation of the fit
	 * @param out The BufferedWriter
	 * @param format True if the data must be formatted
	 *
	 * @throws IOException If an error occurred while writing in the file
	 */
	private void headerFile(FitToRotationalComputations computations, BufferedWriter out, boolean format) throws IOException {
		out.write("version=3");
		out.newLine();
		writeArrayWithSeparator(out, COLUMNS, SEPARATOR_CHAR);
		out.newLine();
		writeArrayWithSeparator(out, getUnits(computations), SEPARATOR_CHAR);
		out.newLine();
		if (format) {
			out.newLine();
		}
	}

	/**
	 * Returns an array with the units of the different columns of the file
	 *
	 * @param computations The object handling the computation of the fit
	 * @return An array with the units of the different columns of the file
	 */
	private String[] getUnits(FitToRotationalComputations computations) {
		String unitX = XAxisCassis.getXAxisFrequency(UNIT.MHZ).getUnit().getValString();
		String unitVo = computations.getVelocityAxis().getUnit().getValString();
		String unitIntensity = computations.getSourceYAxis().getUnitString();
		String unitFitFlux = unitIntensity + "." + unitVo;
		String unitRms = "K".equals(unitIntensity) ? "mK" : unitIntensity;
 		return new String[] {
				NONE_UNIT, NONE_UNIT, NONE_UNIT, NONE_UNIT,
				unitX,
				"K",
				NONE_UNIT,
				"s-1",
				unitX, unitX,
				unitVo, unitVo, unitVo, unitVo, unitVo, unitVo,
				unitIntensity, unitIntensity,
				unitFitFlux, unitFitFlux,
				unitX,
				unitVo, unitVo,
				unitIntensity,
				unitFitFlux, unitFitFlux,
				unitRms,
				unitVo,
				"%",
				"arcsec",
				NONE_UNIT,
				NONE_UNIT
		};
	}

	/**
	 * Writes the content of the given array as one line of text in the given
	 * BufferedWriter, with each element separated with the given character.
	 *
	 * @param out The BufferedWriter to write in
	 * @param array The array to print
	 * @param separator The separator used
	 *
	 * @throws IOException If an error occurred while writing in the file
	 */
	private void writeArrayWithSeparator(BufferedWriter out, String[] array, char separator) throws IOException {
		for (int i = 0 ; i < array.length ; i++) {
			out.write(array[i]);
			if (i != array.length - 1) {
				out.write(separator);
			}
		}
	}

	/**
	 * Initializes the local variables
	 *
	 * @param computations The object handling the computation of the fit
	 * @param telescope The telescope argument
	 */
	private void init(FitToRotationalComputations computations, String telescope) {
		this.computations = computations;
		this.telescopeName = Telescope.getNameStatic(telescope);
		if (this.telescopeName.length() == telescope.length()) {
			this.telescopePath = new File(Software.getTelescopePath()).getPath();
		} else {
			this.telescopePath = telescope.substring(0, telescope.length() - this.telescopeName.length());
		}
		if (!this.telescopePath.endsWith(File.separator)) {
			this.telescopePath += File.separator;
		}
	}

}
