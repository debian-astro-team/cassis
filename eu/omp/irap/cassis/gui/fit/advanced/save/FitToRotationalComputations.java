/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit.advanced.save;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.XAxisVelocity;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.fit.FitComponentManager;
import eu.omp.irap.cassis.fit.FitParametersModel;
import eu.omp.irap.cassis.fit.components.FitAbstractComponent;
import eu.omp.irap.cassis.fit.components.FitComponent;
import eu.omp.irap.cassis.fit.components.FitMultiComponent;
import eu.omp.irap.cassis.fit.util.Category;
import eu.omp.irap.cassis.fit.util.FitCurve;
import eu.omp.irap.cassis.fit.util.enums.FitParameterType;
import eu.omp.irap.cassis.fit.util.enums.FitType;
import eu.omp.irap.cassis.gui.fit.advanced.AdvancedFitModel;
import eu.omp.irap.cassis.gui.fit.advanced.MolCategory;
import eu.omp.irap.cassis.gui.fit.advanced.interfaces.FitSourceInterface;
import eu.omp.irap.cassis.gui.plot.curve.TypeCurve;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;

/**
 * Class used to make all the computations from the fit module to the
 * rotational diagram one
 *
 * @author bastienkovac
 *
 */
public class FitToRotationalComputations {

	private FitParametersModel fitModel;
	private FitSourceInterface source;
	private List<LineDescription> sourceLines;

	private List<FitComponent> components;
	private List<Integer> expandedIndexes;
	private List<LineDescription> expandedLines;


	/**
	 * Constructor
	 *
	 * @param model The model to compute
	 */
	public FitToRotationalComputations(AdvancedFitModel model, List<LineDescription> lines) {
		this.components = new ArrayList<>();
		this.fitModel = model.getParametersModel();
		this.source = model.getDataSource();
		this.sourceLines = lines;
		this.expandedLines = new ArrayList<>();
		this.expandedIndexes = new ArrayList<>();
		extractComponentsFromManager(fitModel.getManager());
	}

	/**
	 * @return The X axis of the data source
	 */
	private XAxisCassis getSourceXAxis() {
		return source.getxAxis();
	}

	/**
	 * @return The X velocity axis of the data source. If the input data
	 * wasn't in a velocity unit, a new axis in km/s is returned. The freqRef
	 * is the one of the first line of the source
	 */
	public XAxisVelocity getVelocityAxis() {
		return getVelocityAxis(0);
	}

	/**
	 * @return The Y axis of the source
	 */
	public YAxisCassis getSourceYAxis() {
		return source.getyAxis();
	}

	/**
	 * @return The X velocity axis of the data source. If the input data
	 * wasn't in a velocity unit, a new axis in km/s is returned. The freqRef
	 * is given by the line at the given index
	 *
	 * @param i The index of the considered line
	 */
	private XAxisVelocity getVelocityAxis(int i) {
		if (UNIT.isVelocity(source.getxAxis().getUnit())) {
			return (XAxisVelocity) source.getxAxis();
		} else {
			XAxisVelocity axis = XAxisCassis.getXAxisVelocity();
			axis.setVlsr(source.getVlsr());
			if (!expandedIndexes.isEmpty()) {
				axis.setFreqRef(getLineFromComponentIndex(i).getObsFrequency());
			} else {
				axis.setFreqRef(source.getFreqRef());
			}
			return axis;
		}
	}

	/**
	 * Returns the x0 frequency of the i-th component, in frequency unit
	 *
	 * @param i Index of the component
	 * @return Its x0 value
	 * @throws IllegalArgumentException If the component can't be used in the rotational
	 * diagram
	 */
	public double getFitOriginFrequency(int i) throws IllegalArgumentException {
		double value = getComponent(i).getParameter(FitParameterType.X0.getIndex()).getValue();
		return getSourceXAxis().convertToMHzFreq(value);
	}

	/**
	 * Returns the error on the x0 frequency of the i-th component, in frequency unit
	 *
	 * @param i Index of the component
	 * @return Its x0 error
	 * @throws IllegalArgumentException If the component can't be used in the rotational
	 * diagram
	 */
	public double getFitOriginFrequencyError(int i) throws IllegalArgumentException {
		double error = getComponent(i).getParameter(FitParameterType.X0.getIndex()).getError();
		return getSourceXAxis().convertDeltaToMhz(error, getLineFromComponentIndex(i).getObsFrequency());
	}

	/**
	 * Returns the x0 frequency of the i-th component, in velocity unit
	 *
	 * @param i Index of the component
	 * @return Its x0 value
	 * @throws IllegalArgumentException If the component can't be used in the rotational
	 * diagram
	 */
	public double getFitOriginVelo(int i) throws IllegalArgumentException {
		double value = getComponent(i).getParameter(FitParameterType.X0.getIndex()).getValue();
		return XAxisCassis.convert(value, getSourceXAxis(), getVelocityAxis(i));
	}

	/**
	 * Returns the error on the x0 frequency of the i-th component, in velocity unit
	 *
	 * @param i Index of the component
	 * @return Its x0 error
	 * @throws IllegalArgumentException If the component can't be used in the rotational
	 * diagram
	 */
	public double getFitOriginVeloError(int i) throws IllegalArgumentException {
		double error = getComponent(i).getParameter(FitParameterType.X0.getIndex()).getError();
		return XAxisCassis.convertDelta(getSourceXAxis(), getVelocityAxis(i), getLineFromComponentIndex(i).getObsFrequency(), error);
	}

	/**
	 * Returns the value of the FWHM for a Gaussian component (or twice the sigma
	 * gaussian for a voigt one). Returns 0 if the component is neither gaussian or
	 * voigt. This value is in velocity unit.
	 *
	 * @param i Index of the component
	 * @return Its fwhm value
	 * @throws IllegalArgumentException If the component can't be used in the rotational
	 * diagram
	 */
	public double getFitFWHMGaussian(int i) throws IllegalArgumentException {
		FitComponent component = getComponent(i);
		double fwhm = 0.;
		if (isGaussian(component)) {
			fwhm = component.getParameter(FitParameterType.GAUSSIAN_FWHM.getIndex()).getValue();
		} else if (isVoigt(component)) {
			fwhm = component.getParameter(FitParameterType.SIGMA_G.getIndex()).getValue() * 2;
		}
		return XAxisCassis.convertDelta(getSourceXAxis(), getVelocityAxis(i), getLineFromComponentIndex(i).getObsFrequency(), fwhm);
	}

	/**
	 * Returns the error on the FWHM for a Gaussian component (or on the sigma gaussian
	 * for a voigt one) Returns 0 if the component is neither gaussian or
	 * voigt. This value is in velocity unit.
	 *
	 * @param i Index of the component
	 * @return Its fwhm error
	 * @throws IllegalArgumentException If the component can't be used in the rotational
	 * diagram
	 */
	public double getFitFWHMGaussianError(int i) throws IllegalArgumentException {
		FitComponent component = getComponent(i);
		double delta = 0.;
		if (isGaussian(component)) {
			delta = component.getParameter(FitParameterType.GAUSSIAN_FWHM.getIndex()).getError();
		} else if (isVoigt(component)) {
			delta = component.getParameter(FitParameterType.SIGMA_G.getIndex()).getError();
		}
		return XAxisCassis.convertDelta(getSourceXAxis(), getVelocityAxis(i), getLineFromComponentIndex(i).getObsFrequency(), delta);
	}

	/**
	 * Returns the value of the FWHM for a Lorentzian component (or twice the sigma
	 * lorentzian for a voigt one) Returns 0 if the component is neither lorentzian or
	 * voigt. This value is in velocity unit.
	 *
	 * @param i Index of the component
	 * @return Its fwhm value
	 * @throws IllegalArgumentException If the component can't be used in the rotational
	 * diagram
	 */
	public double getFitFWHMLorentzian(int i) throws IllegalArgumentException {
		FitComponent component = getComponent(i);
		double fwhm = 0.;
		if (isLorentzian(component)) {
			fwhm = component.getParameter(FitParameterType.LORENTZIAN_FWHM.getIndex()).getValue();
		} else if (isVoigt(component)) {
			fwhm = component.getParameter(FitParameterType.SIGMA_L.getIndex()).getValue() * 2;
		}
		return XAxisCassis.convertDelta(getSourceXAxis(), getVelocityAxis(i), getLineFromComponentIndex(i).getObsFrequency(), fwhm);
	}

	/**
	 * Returns the error on the FWHM for a Lorentzian component (or on the sigma lorentzian
	 * for a voigt one) Returns 0 if the component is neither lorentzian or
	 * voigt. This value is in velocity unit.
	 *
	 * @param i Index of the component
	 * @return Its fwhm error
	 * @throws IllegalArgumentException If the component can't be used in the rotational
	 * diagram
	 */
	public double getFitFWHMLorentzianError(int i) throws IllegalArgumentException {
		FitComponent component = getComponent(i);
		double delta = 0.;
		if (isLorentzian(component)) {
			delta = component.getParameter(FitParameterType.LORENTZIAN_FWHM.getIndex()).getError();
		} else if (isVoigt(component)) {
			delta = component.getParameter(FitParameterType.SIGMA_L.getIndex()).getError();
		}
		return XAxisCassis.convertDelta(getSourceXAxis(), getVelocityAxis(i), getLineFromComponentIndex(i).getObsFrequency(), delta);
	}

	/**
	 * Returns the intensity of the i-th component
	 *
	 * @param i Index of the component
	 * @return Its i0 value
	 * @throws IllegalArgumentException If the component can't be used in the rotational
	 * diagram
	 */
	public double getFitIntensity(int i) throws IllegalArgumentException {
		return getComponent(i).getParameter(FitParameterType.I0.getIndex()).getValue();
	}

	/**
	 * Returns the error on the intensity of the i-th component
	 *
	 * @param i Index of the component
	 * @return Its i0 error
	 * @throws IllegalArgumentException If the component can't be used in the rotational
	 * diagram
	 */
	public double getFitIntensityError(int i) throws IllegalArgumentException {
		return getComponent(i).getParameter(FitParameterType.I0.getIndex()).getError();
	}

	/**
	 * Returns the integral of the i-th component
	 *
	 * @param i Index of the component
	 * @return Its integral
	 * @throws IllegalArgumentException If the component can't be used in the rotational
	 * diagram
	 */
	public double getFitFlux(int i) throws IllegalArgumentException {
		return getFitFWHMGaussian(i) * getFitIntensity(i);
	}

	/**
	 * Returns the error on the integral for the i-th component of the fit.
	 * Currently returns 0 in all cases.
	 *
	 * @param i Index of the component
	 * @return The flux error
	 */
	public double getFitFluxError(int i) {
		return 0.;
	}

	/**
	 * Returns the x-value corresponding to the maximum intensity of the studied curve, in frequency
	 * unit
	 *
	 * @return The frequency corresponding to the maximum intensity of the studied curve
	 */
	public double getDataFreqIntensityMax() {
		double xIMax = fitModel.getStudiedCurve().getX()[fitModel.getStudiedCurve().getMaxYIndex()];
		return getSourceXAxis().convertToMHzFreq(xIMax);
	}

	/**
	 * Returns the x-value corresponding to the maximum intensity of the studied curve, in velocity
	 * unit
	 *
	 * @param i Index of the component
	 * @return The velocity corresponding to the maximum intensity of the studied curve
	 */
	public double getDataVelocityIntensityMax(int i) {
		double xIMax = fitModel.getStudiedCurve().getX()[fitModel.getStudiedCurve().getMaxYIndex()];
		return XAxisCassis.convert(xIMax, getSourceXAxis(), getVelocityAxis());
	}

	/**
	 * Returns the maximal intensity value of the studied curve
	 *
	 * @return The maximal intensity of the input data
	 */
	public double getDataIntensityMax() {
		return fitModel.getStudiedCurve().getMaxY();
	}

	/**
	 * Returns the integral of the studied curve
	 *
	 * @param i Index of the component
	 * @return The integral of the studied curve
	 */
	public double getDataFluxFirstMoment(int i) {
		return computeIntegral(fitModel.getStudiedCurve(), i);
	}

	/**
	 * Returns the error on the integral for the studied curve.
	 * Currently returns 0 in all cases.
	 *
	 * @return The flux error
	 */
	public double getDataDeltaFluxFirstMoment() {
		return 0.;
	}

	/**
	 * Compute the sigma of the first moment's flux

	 * @param i Index of the component
	 * @return The sigma
	 */
	public double getDataSigmaFluxFirstMoment(int i) {
		return getDataFluxFirstMoment(i) / fitModel.getStudiedCurve().getMaxY();
	}

	/**
	 * Returns the rms computed by the fit. The RMS is computing when
	 * the data is fitted with only one polynomial.
	 *
	 * @return The rms
	 */
	public double getFitComputedRms() {
		return fitModel.getRms() * 1000;
	}

	/**
	 * Returns the delta between two x-values of the studied curve (in velocity).
	 * Currently computes the delta between the first two values and assumes
	 * it's constant.
	 * The delta is the distance between two points, divided by 2
	 *
	 * @param i Index of the component
	 *
	 * @return The delta between the x-values
	 */
	public double getDataDeltaVelocity(int i) {

		final double[] xValues = fitModel.getStudiedCurve().getX();
		double xOne = xValues[0];
		double xTwo = xValues[1];
		double delta = Math.sqrt(Math.pow((xTwo - xOne), 2)) / 2;
		return XAxisCassis.convertDelta(getSourceXAxis(), getVelocityAxis(i), getLineFromComponentIndex(i).getObsFrequency(), delta);
	}

	/**
	 * Return the line associated with a expanded component
	 * index
	 *
	 * @param i The expanded component index
	 * @return The correct LineDescription
	 */
	public LineDescription getLineFromComponentIndex(int i) {
		return expandedLines.get(i);
	}

	/**
	 * Returns a reduced index given an expanded one
	 *
	 * @param i The expanded index
	 * @return The reduced one
	 */
	public int getReducedIndex(int i) {
		return expandedIndexes.get(i);
	}

	/**
	 * @return The number of expanded components;
	 */
	public int getNbExpandedComponents() {
		return components.size();
	}

	/**
	 * Compute the integral for a fitCurve
	 *
	 * @param curve Curve to integrate
	 * @param index Index of the component
	 * @return The area under the curve
	 */
	private double computeIntegral(FitCurve curve, int index) {
		try {
			double[] x = curve.getX();
			for (int i = 0 ; i < x.length ; i++) {
				x[i] = source.getxAxis().convertToMHzFreq(x[i]);
			}
			CommentedSpectrum spectrum = new CommentedSpectrum(null, x, curve.getY(), "tmpSpectrum");
			spectrum.setxAxisOrigin(source.getxAxis().clone());
			spectrum.setVlsr(source.getVlsr());
			spectrum.setFreqRef(getLineFromComponentIndex(index).getObsFrequency());
			XYSpectrumSeries tmpSeries = new XYSpectrumSeries("tmpSpectrum", source.getxAxis().clone(), TypeCurve.DATA, spectrum);
			return computeIntegral(tmpSeries, 0, tmpSeries.getSpectrum().getSize() - 1, index);
		} catch (CloneNotSupportedException e) {
			return 0.;
		}
	}

	/**
	 * Computes the integral of a curve between two indexes
	 *
	 * @param serie The curve
	 * @param minSelectionIndex The min index
	 * @param maxSelectionIndex The max index
	 * @param i Index of the component
	 * @return Area under the curve between those two indexes
	 */
	private double computeIntegral(XYSpectrumSeries serie, int minSelectionIndex, int maxSelectionIndex, int i) {
		double delta;
		double intensity;
		double moment = 0;
		CommentedSpectrum spectrum = serie.getSpectrum();
		double[] intensityTab = spectrum.getIntensities(getVelocityAxis(i));
		double[] deltaTab = spectrum.getDelta(getVelocityAxis(i));
		int length = maxSelectionIndex - minSelectionIndex + 1;
		if (serie.getXAxis().isInverted()) {
			reverse(deltaTab);
		}
		for (int cpt = 0; cpt < length; cpt++) {
			intensity = intensityTab[minSelectionIndex + cpt];
			delta = Math.abs(deltaTab[minSelectionIndex + cpt]);
			moment += intensity * delta;
		}
		return moment;
	}

	/**
	 * Reverse the given array, in time <code>O(n/2)</code>
	 *
	 * @param array Array to reverse
	 */
	private void reverse(double[] array) {
		double tmp;
		int i = 0;
		int j = array.length - 1;
		while (i < j) {
			tmp = array[j];
			array[j] = array[i];
			array[i] = tmp;
			j--;
			i++;
		}
	}

	/**
	 * Returns the i-th component of the extracted list
	 *
	 * @param i Index
	 * @return The component
	 * @throws IllegalArgumentException If the component can't be used in the rotational diagram
	 */
	private FitComponent getComponent(int i) throws IllegalArgumentException {
		if (!isComponentTypeValid(components.get(i))) {
			throw new IllegalArgumentException("The " + components.get(i).getComponentType().getLabel() +
					" model cannot be used in the rotational diagram");
		}
		return components.get(i);
	}

	/**
	 * Extracts all the components of the manager, ignoring unselected components
	 * and baseline ones, and transforming any multiple in simple components by
	 * separating the siblings
	 *
	 * @param manager Manager to interpret
	 */
	private void extractComponentsFromManager(FitComponentManager manager) {
		for (Category category : manager.getCategories()) {
			if (category.getId() != Integer.MIN_VALUE
					&& category.getId() != Integer.MAX_VALUE
					&& category instanceof MolCategory) {
				MolCategory molCategory = (MolCategory) category;
				extractComponentsFromList(manager.getComponents(category), getSubLineList(molCategory.getTags()));
			}
		}
	}

	/**
	 * Returns a list of LineDescription with only the given molTag
	 *
	 * @param molTag The molTag
	 * @return The lines with this tag
	 */
	private List<LineDescription> getSubLineList(Set<Integer> molTags) {
		List<LineDescription> subList = new ArrayList<>();
		for (LineDescription line : sourceLines) {
			if (molTags.contains(line.getMolTag())) {
				subList.add(line);
			}
		}
		return subList;
	}

	/**
	 * Extracts all the components from a list, ignoring the unselected ones
	 * and transforming any multiple in simple components by separating the siblings
	 *
	 * @param list List of components to extract
	 * @param lines List of the associated lines
	 */
	private void extractComponentsFromList(List<FitAbstractComponent> list, List<LineDescription> lines) {
		int currentComponent = 0;
		for (FitAbstractComponent component : list) {
			int currentLine = 0;
			if (component.isSelected() && !component.isMultiple()) {
				components.add((FitComponent) FitAbstractComponent.cloneComponent(component));
				expandedIndexes.add(currentComponent);
				expandedLines.add(getLine(lines, currentLine));
				currentLine++;
			} else if (component.isSelected()) {
				for (FitComponent comp : getSeparateComponents((FitMultiComponent) component)) {
					components.add(comp);
					expandedIndexes.add(currentComponent);
					expandedLines.add(getLine(lines, currentLine));
					currentLine++;
				}
			}
			currentComponent++;
		}
	}

	/**
	 * Returns the Line at the i-th index of the list if the size matches, or the last one
	 * if it doesn't
	 *
	 * @param list The list of lines
	 * @param i The index
	 * @return A line
	 */
	private LineDescription getLine(List<LineDescription> list, int i) {
		if (i < list.size()) {
			return list.get(i);
		}
		return list.get(list.size() - 1);
	}

	/**
	 * Extracts a list of simple components from a multiple one
	 *
	 * @param multiModel The multi model to separate
	 * @return A list of simple components
	 */
	private List<FitComponent> getSeparateComponents(FitMultiComponent multiModel) {
		List<FitComponent> compos = new ArrayList<>();
		FitComponent parent = cloneComponent(multiModel.getParent());
		compos.add(parent);
		for (int i = 0 ; i < multiModel.getNbChildren() ; i++) {
			FitComponent child = cloneComponent(multiModel.getChild(i));
			copyDeltas(parent, child);
			compos.add(child);
		}
		return compos;
	}

	/**
	 * Returns a copied version of the given component
	 *
	 * @param component The component to copy
	 * @return The copy
	 */
	private FitComponent cloneComponent(FitComponent component) {
		return (FitComponent) FitAbstractComponent.cloneComponent(component);
	}

	/**
	 * Copy the deltas of the parent component to the child one, on every constrained
	 * parameter
	 *
	 * @param parent The parent component
	 * @param child The child component
	 */
	private void copyDeltas(FitComponent parent, FitComponent child) {
		for (int i = 0 ; i < parent.getNbParameters() ; i++) {
			if (child.getParameter(i).isConstrained()) {
				child.getParameter(i).setError(parent.getParameter(i).getError());
			}
		}
	}

	/**
	 * Returns if the given component can be used in the rotational diagram
	 *
	 * @param component The component to evaluate
	 * @return True if the component can be used in the rotational diagram
	 */
	private boolean isComponentTypeValid(FitAbstractComponent component) {
		return isGaussian(component) || isLorentzian(component) || isVoigt(component);
	}

	/**
	 * Returns if the component is of gaussian type
	 *
	 * @param component The component
	 * @return True if the component is Gaussian
	 */
	private boolean isGaussian(FitAbstractComponent component) {
		return component.getComponentType().equals(FitType.GAUSS);
	}

	/**
	 * Returns if the component is of lorentzian type
	 *
	 * @param component The component
	 * @return True if the component is Lorentzian
	 */
	private boolean isLorentzian(FitAbstractComponent component) {
		return component.getComponentType().equals(FitType.LORENTZ);
	}

	/**
	 * Returns if the component is of voigt type
	 *
	 * @param component The component
	 * @return True if the component is Voigt
	 */
	private boolean isVoigt(FitAbstractComponent component) {
		return component.getComponentType().equals(FitType.VOIGT);
	}

}
