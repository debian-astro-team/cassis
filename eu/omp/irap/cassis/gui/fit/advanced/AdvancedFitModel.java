/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit.advanced;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.Formula;
import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.XAxisVelocity;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.common.events.ListenerManager;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.fit.FitComponentManager;
import eu.omp.irap.cassis.fit.FitParametersModel;
import eu.omp.irap.cassis.fit.FitResult;
import eu.omp.irap.cassis.fit.components.FitAbstractComponent;
import eu.omp.irap.cassis.fit.components.FitComponent;
import eu.omp.irap.cassis.fit.components.FitConstraint.FitConstraintType;
import eu.omp.irap.cassis.fit.components.impl.FitPolynomialComponent;
import eu.omp.irap.cassis.fit.computing.FitNormalizationParameters;
import eu.omp.irap.cassis.fit.components.FitMultiComponent;
import eu.omp.irap.cassis.fit.components.FitParameter;
import eu.omp.irap.cassis.fit.util.Category;
import eu.omp.irap.cassis.fit.util.FitCurve;
import eu.omp.irap.cassis.fit.util.FitException;
import eu.omp.irap.cassis.fit.util.enums.FitParameterType;
import eu.omp.irap.cassis.fit.util.enums.FitType;
import eu.omp.irap.cassis.gui.fit.advanced.gui.CategorizedComponent;
import eu.omp.irap.cassis.gui.fit.advanced.interfaces.FitSourceInterface;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import eu.omp.irap.cassis.properties.Software;

/**
 * Model that link a data-source (implementing {@link FitSourceInterface}) with
 * the fitting module.
 * @author bastienkovac
 */
public class AdvancedFitModel extends ListenerManager implements ModelListener {

	public static final String CATEGORIZED_COMPONENTS_EVENT = "categorizedComponentsEvent";
	public static final String DUPLICATED_LINES_EVENT = "duplicatedLinesEvent";
	public static final String VLSR_EVENT = "vlsrEvent";

	private FitParametersModel parametersModel;
	private FitSourceInterface dataSource;

	private List<CategorizedComponent> categorizedComponents;
	private static boolean haveToconvertPolynomial = true;


	/**
	 * Constructor
	 *
	 * @param dataSource The data-source to use
	 */
	public AdvancedFitModel(FitSourceInterface dataSource) {
		this.parametersModel = new FitParametersModel();
		this.parametersModel.getManager().addModelListener(this);
		this.dataSource = dataSource;
		this.categorizedComponents = new ArrayList<>();
		refreshInput();
	}

	/**
	 * @return The intern {@link FitParametersModel}
	 */
	public FitParametersModel getParametersModel() {
		return parametersModel;
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.cassis.common.events.ModelListener#dataChanged(eu.omp.irap.cassis.common.events.ModelChangedEvent)
	 *
	 * Dispatches all event from the underlying Fit Model
	 *
	 */
	@Override
	public void dataChanged(ModelChangedEvent event) {
		fireDataChanged(event);
		if (event.getSource().equals(FitComponentManager.COMPONENT_MANAGER_CATEGORY_REMOVED_EVENT)) {
			removeCategory((Category) event.getValue());
		} else if (event.getSource().equals(FitComponentManager.COMPONENT_MANAGER_CLEARED_EVENT)) {
			clearCategorizedComponents();
		} else if (event.getSource().equals(FitComponentManager.COMPONENT_MANAGER_COMPONENT_ADDED_EVENT)) {
			addCategorizedComponents(event);
			setAsListener((FitAbstractComponent) event.getValue());
		} else if (event.getSource().equals(FitComponentManager.COMPONENT_MANAGER_COMPONENT_REMOVED_EVENT)) {
			removeComponent((String) event.getValue());
		} else if (event.getSource().equals(FitComponentManager.COMPONENT_MANAGER_CLONED_COMPONENT)) {
			copyCategorizedComponent((String) event.getValue(), (String) event.getOrigin());
		} else if (event.getSource().equals(FitComponent.PARAMETER_VALUE_EVENT)) {
			updateVlsr((String) event.getOrigin(), (FitParameter) event.getValue());
		}
	}

	private void updateVlsr(String id, FitParameter p) {
		if (!p.getParameterType().equals(FitParameterType.X0)) {
			return;
		}
		for (CategorizedComponent cc : this.categorizedComponents) {
			FitAbstractComponent comp = cc.getComponent();
			FitComponent listenedComp = comp.isMultiple() ? ((FitMultiComponent) comp).getParent()
					: (FitComponent) comp;
			if (listenedComp.getId().equals(id)) {
				cc.updateVlsr(p.getValue(), dataSource.getxAxis());
				break;
			}
		}
		fireDataChanged(new ModelChangedEvent(VLSR_EVENT));
	}

	private void setAsListener(FitAbstractComponent component) {
		if (component.isMultiple()) {
			((FitMultiComponent) component).getParent().addModelListener(this);
		} else {
			((FitComponent) component).addModelListener(this);
		}
	}

	private void removeCategory(Category cat) {
		Iterator<CategorizedComponent> it = this.categorizedComponents.iterator();
		while (it.hasNext()) {
			CategorizedComponent cc = it.next();
			if (cc.getCategory().equals(cat)) {
				it.remove();
			}
		}
		fireDataChanged(new ModelChangedEvent(CATEGORIZED_COMPONENTS_EVENT));
	}

	private void clearCategorizedComponents() {
		this.categorizedComponents.clear();
		fireDataChanged(new ModelChangedEvent(CATEGORIZED_COMPONENTS_EVENT));
	}

	private void addCategorizedComponents(ModelChangedEvent event) {
		Category category = (Category) event.getOrigin();
		FitAbstractComponent component = (FitAbstractComponent) event.getValue();
		if (!component.getComponentType().isBaseline()) {
			double vlsr = dataSource.getAllLines().isEmpty() ? dataSource.getVlsr() : dataSource.getAllLines().get(0).getVlsr();
			CategorizedComponent cc = new CategorizedComponent(category, component, dataSource.getxAxis(), vlsr);
			this.categorizedComponents.add(cc);
			fireDataChanged(new ModelChangedEvent(CATEGORIZED_COMPONENTS_EVENT));
		}
	}

	private void removeComponent(String id) {
		Iterator<CategorizedComponent> it = this.categorizedComponents.iterator();
		while (it.hasNext()) {
			CategorizedComponent cc = it.next();
			if (cc.getComponent().getId().equals(id)) {
				it.remove();
			}
		}
		fireDataChanged(new ModelChangedEvent(CATEGORIZED_COMPONENTS_EVENT));
	}

	private void copyCategorizedComponent(String idClone, String idSource) {
		CategorizedComponent src = null;
		for (CategorizedComponent cc : categorizedComponents) {
			if (cc.getComponent().getId().equals(idSource)) {
				src = cc;
				break;
			}
		}
		if (src == null) {
			return;
		}
		for (CategorizedComponent cc : categorizedComponents) {
			if (cc.getComponent().getId().equals(idClone)) {
				cc.copy(src);
			}
		}
		fireDataChanged(new ModelChangedEvent(CATEGORIZED_COMPONENTS_EVENT));
	}

	public List<CategorizedComponent> getCategorizedComponents() {
		return categorizedComponents;
	}

	/**
	 * Initiate the fit
	 *
	 * @return The result of the fit
	 * @throws FitException If an error occurred during the fit
	 */
	public AdvancedFitResult performFit() throws FitException {
		return performAdvancedFit(parametersModel.performFit());
	}

	/**
	 *
	 * @param moduleResult
	 * @param source
	 * @param vlsr
	 * @param freqRef
	 * @param xAxis
	 * @param metaDatas
	 * @param yAxisCassis
	 * @param yAxisData
	 * @param nameCurveFitted
	 * @return
	 * @throws FitException
	 */
	private AdvancedFitResult performAdvancedFit(FitResult moduleResult, FitCurve source,
			double vlsr, double freqRef, XAxisCassis xAxis, YAxisCassis yAxisCassis,
			List<CassisMetadata> metaDatas, YAxisCassis yAxisData,
			String nameCurveFitted)  throws FitException  {
		source.setVlsr(vlsr);
		source.setFreqRef(freqRef);
		int oversampling = parametersModel.getOversampling();

		AdvancedFitResult advResult = new AdvancedFitResult(moduleResult, source, oversampling, xAxis,
				yAxisCassis, metaDatas, yAxisData, nameCurveFitted);
		return advResult;
	}


	/**
	 *
	 * @param moduleResult
	 * @return
	 * @throws FitException
	 */
	private AdvancedFitResult performAdvancedFit(FitResult moduleResult) throws FitException{
		return performAdvancedFit(moduleResult, parametersModel.getSourceCurve(),
				dataSource.getVlsr(), dataSource.getFreqRef(),dataSource.getxAxis(),
				dataSource.getyAxis(), dataSource.getCurrentSeries().getSpectrum().getCassisMetadataList(),
				dataSource.getCurrentSeries().getSpectrum().getyAxis(),
				(String)dataSource.getCurrentSeries().getKey());
	}

	@Override
	public void removeAllListener() {
		super.removeAllListener();
		parametersModel.removeAllListener();
	}

	@Override
	public void removeModelListener(ModelListener listener) {
		super.removeModelListener(listener);
		parametersModel.removeModelListener(listener);
	}

	/**
	 * @return The data-source of this model
	 */
	public FitSourceInterface getDataSource() {
		return dataSource;
	}

	/**
	 * Estimates a set of lines several times.
	 * See the {@link #estimateListOfLines(List, FitType)} method for the documentation
	 *
	 * @param lines The read lines
	 * @param type The default component model
	 * @param nb The number of times the estimation must be repeated
	 */
	public void estimateListOfLines(List<LineDescription> lines, FitType type, int nb) {
		for (int i = 0 ; i < nb ; i++) {
			estimateListOfLines(lines, type);
		}
	}

	/**
	 * Reestimates the components of the fit model for the given species to refresh the constraints
	 * factors
	 *
	 * @param tag Tag of the species to reestimate;
	 */
	public void reestimateComponents(int tag) {
		FitType type = Software.getUserConfiguration().getFitDefaultType();
		List<LineDescription> allLines = dataSource.getAllLines();
		Iterator<LineDescription> it = allLines.iterator();
		while (it.hasNext()) {
			LineDescription next = it.next();
			if (next.getMolTag() != tag) {
				it.remove();
			}
		}
		estimateListOfLines(allLines, type, false);
	}

	protected static List<LineDescription> removeLinesWithCopy(List<LineDescription> allLines) {
		List<LineDescription> res = new ArrayList<>();
		HashMap<String, LineDescription> map = new HashMap<>();
		for (LineDescription lineDescription : allLines) {
			String freqString = String.valueOf(lineDescription.getObsFrequency());
			if ( !map.containsKey(freqString)) {
				res.add(lineDescription);
				map.put(freqString, lineDescription);
			}
		}
		return res;
	}

	protected static List<LineDescription> getSameLines(List<LineDescription> allLines) {
		List<LineDescription> res = new ArrayList<>();
		HashMap<String, LineDescription> map = new HashMap<>();
		for (LineDescription lineDescription : allLines) {
			String freqString = String.valueOf(lineDescription.getObsFrequency());
			if (map.containsKey(freqString)) {
				res.add(lineDescription);
			} else {
				map.put(freqString, lineDescription);
			}
		}
		return res;
	}

	/**
	 * Estimates a configuration for a given list of lines and default model.
	 * If there is only one line, an element of the given type is created and estimated
	 * through the x0 value of the line (converted in the correct unit).
	 * <br>
	 * If there are multiple lines, a multi-component is created, with <code>size() - 1</code>
	 * children. A shift constraint is put on the x0 values of the children, then the factors
	 * for this constraint are computed between the main line (the first one in the list) and
	 * the other ones, using the formula <code>factor_i = x_child_i - x_parent</code>
	 * <br>
	 * The parent component is assumed to be the first one in the list and is thus removed
	 * from it. In consequence, only pass copies of lists to this method.
	 * <br>
	 * If the component that is selected for estimation in the manager matches the estimated structure,
	 * the estimated values are inserted. If not, a new component is created and added to the
	 * manager.
	 *
	 * @param lines The read lines
	 * @param type The default component model
	 * @param ignoreParent True if the estimation must ignore the parent model of every multi-model
	 */
	public void estimateListOfLines(List<LineDescription> lines, FitType type, boolean ignoreParent) {
		List<LineDescription> linesToEstimate = new ArrayList<>(lines);
		if (linesToEstimate.isEmpty()) {
			return;
		}

		List<LineDescription> duplicateLines = getSameLines(linesToEstimate);
		if (! duplicateLines.isEmpty()) {
			fireDataChanged(new ModelChangedEvent(DUPLICATED_LINES_EVENT,duplicateLines));
			linesToEstimate = removeLinesWithCopy(linesToEstimate);
		}

		if (linesToEstimate.size() == 1) {
			estimateSimpleComponent(linesToEstimate.get(0), type);
		} else {
			LineDescription parentLine = linesToEstimate.get(0);
			linesToEstimate.remove(0);
			estimateMultiComponent(parentLine, linesToEstimate, type, ignoreParent);
		}
	}

	/**
	 * Estimates a configuration for a given list of lines and default model.
	 * If there is only one line, an element of the given type is created and estimated
	 * through the x0 value of the line (converted in the correct unit).
	 * <br>
	 * If there are multiple lines, a multi-component is created, with <code>size() - 1</code>
	 * children. A shift constraint is put on the x0 values of the children, then the factors
	 * for this constraint are computed between the main line (the first one in the list) and
	 * the other ones, using the formula <code>factor_i = x_child_i - x_parent</code>
	 * <br>
	 * The parent component is assumed to be the first one in the list and is thus removed
	 * from it. In consequence, only pass copies of lists to this method.
	 * <br>
	 * If the component that is selected for estimation in the manager matches the estimated structure,
	 * the estimated values are inserted. If not, a new component is created and added to the
	 * manager.
	 *
	 * @param lines The read lines
	 * @param type The default component model
	 */
	public void estimateListOfLines(List<LineDescription> lines, FitType type) {
		estimateListOfLines(lines, type, false);
	}

	/**
	 * Adds a component to the manager, in a category that matches the different lines
	 * given
	 *
	 * @param component Component to add
	 * @param lineRef The reference lines
	 */
	private void addComponent(FitAbstractComponent component, List<LineDescription> linesRef) {
		if (!linesRef.isEmpty()) {
			MolCategory category = new MolCategory(linesRef.get(0));
			for (LineDescription line : linesRef) {
				category.addTag(line.getMolTag());
			}
			parametersModel.getManager().addAbstractComponent(category, component);
		}
	}

	/**
	 * Creates and returns a list with only one element
	 *
	 * @param line The line
	 * @return A list only containing the line
	 */
	private List<LineDescription> makeList(LineDescription line) {
		List<LineDescription> lines = new ArrayList<>();
		lines.add(line);
		return lines;
	}

	/**
	 * Creates and returns a list containing the parent line, and the surrounding ones
	 *
	 * @param parentLine The parent line
	 * @param lines The surrounding lines
	 * @return A list containing the parent line, then the surrounding ones
	 */
	private List<LineDescription> makeList(LineDescription parentLine, List<LineDescription> lines) {
		List<LineDescription> molList = new ArrayList<>();
		molList.add(parentLine);
		molList.addAll(lines);
		return molList;
	}

	/**
	 * Estimates a simple component of the given type. Checks if the current estimated
	 * component matches the asked configuration. If it does, the current estimated component
	 * is estimated. If not, a new component is created then estimated
	 *
	 * @param line The line
	 * @param type The type of component
	 */
	private void estimateSimpleComponent(LineDescription line, FitType type) {
		FitAbstractComponent estimModel = parametersModel.getManager().getEstimableComponent();
		if (match(estimModel, 1, type)) {
			estimateSimpleComponent((FitComponent) estimModel, line, false);
		} else {
			estimateSimpleComponent(type.buildIndividualComponent(), line, true);
		}
	}

	/**
	 * Compute the estimation of a simple component from a line
	 *
	 * @param model The model on which to compute the estimation
	 * @param line The line from which to estimate
	 * @param toAdd True if the component must be added to the manager after estimation
	 */
	private void estimateSimpleComponent(FitComponent model, LineDescription line, boolean toAdd) {
		for (int i = 0 ; i < model.getNbParameters() ; i++) {
			FitParameter parameter = model.getParameter(i);
			if (parameter.getParameterType().equals(FitParameterType.X0)) {
				model.setParameterValue(i, getX0ValueFromLine(line));
			} else if (FitParameterType.isWidthParameter(i)) {
				model.setParameterValue(i, getWidthValue());
			}
		}
		if (toAdd) {
			addComponent(model, makeList(line));
		}
	}

	/**
	 * Estimates a multi component of the given type. Checks if the current estimated
	 * component matches the asked configuration. If it does, the current estimated component
	 * is estimated. If not, a new component is created then estimated
	 *
	 * @param parentLine The main line
	 * @param lines The surrounding lines
	 * @param type The type of component
	 * @param ignoreParent True if the estimation must ignore the parent model of every multi-model
	 */
	private void estimateMultiComponent(LineDescription parentLine, List<LineDescription> lines, FitType type, boolean ignoreParent) {
		FitAbstractComponent estimModel = parametersModel.getManager().getEstimableComponent();
		if (match(estimModel, lines.size() + 1, type)) {
			estimateMultiComponent((FitMultiComponent) estimModel, parentLine, lines, false, ignoreParent);
		} else {
			estimateMultiComponent(type.buildMultiComponent(lines.size()), parentLine, lines, true, ignoreParent);
		}
	}

	/**
	 * Compute the estimation of a multi component
	 *
	 * @param model The model on which to compute the estimation
	 * @param parentLine The main line
	 * @param lines The surrounding lines
	 * @param toAdd True if the component must be added to the manager afterwards
	 * @param ignoreParent True if the estimation must ignore the parent model of every multi-model
	 */
	private void estimateMultiComponent(FitMultiComponent model, LineDescription parentLine, List<LineDescription> lines, boolean toAdd, boolean ignoreParent) {
		initializeConstraints(model);
		initShiftFactors(model, parentLine, lines);
		if (!ignoreParent) {
			estimateSimpleComponent(model.getParent(), parentLine, false);
		}
		removeTemporaryConstraints(model);
		if (toAdd) {
			addComponent(model, makeList(parentLine, lines));
		}
	}

	/**
	 * Initializes the shift factors from the difference between each surrounding line and the main one
	 *
	 * @param model The model on which to initialize the shift factors
	 * @param parentLine The main line
	 * @param lines The surrounding lines
	 */
	private void initShiftFactors(FitMultiComponent model, LineDescription parentLine, List<LineDescription> lines) {
		int x0Index = FitParameterType.X0.getIndex();
		for (int i = 0 ; i < lines.size() ; i++) {
			model.getChild(i).setParameterFactor(x0Index, getX0ValueFromLine(lines.get(i)) - getX0ValueFromLine(parentLine));
		}
	}

	/**
	 * Sets constraints on a multi-model. X0 parameters are constrained with a Shift,
	 * and every other one is constrained with an equal (so we only have to estimate the parent)
	 *
	 * @param model The model on which to initialize constraints
	 */
	private void initializeConstraints(FitMultiComponent model) {
		FitComponent firstChild = model.getFirstChild();
		for (int i = 0 ; i < firstChild.getNbParameters() ; i++) {
			FitParameter param = firstChild.getParameter(i);
			if (param.getParameterType().equals(FitParameterType.X0)) {
				firstChild.setParameterConstraint(i, FitConstraintType.SHIFT);
			} else {
				firstChild.setParameterConstraint(i, FitConstraintType.EQUAL);
				if (FitParameterType.isWidthParameter(i)) {
					model.getParent().setParameterValue(i, getWidthValue());
				}
			}
		}
	}

	/**
	 * Removes the constraints that were only temporary (that is every constraint that is
	 * not on X0 or on a width parameter)
	 *
	 * @param model The model on which to remove the temporary constraints
	 */
	private void removeTemporaryConstraints(FitMultiComponent model) {
		FitComponent firstChild = model.getFirstChild();
		for (int i = 0 ; i < firstChild.getNbParameters() ; i++) {
			FitParameter param = firstChild.getParameter(i);
			if (!param.getParameterType().equals(FitParameterType.X0) && !FitParameterType.isWidthParameter(i)) {
				firstChild.setParameterConstraint(i, FitConstraintType.NONE);
			}
		}
	}

	/**
	 * Returns the value for a x0 parameter, in the appropriate unit
	 *
	 * @param line The line from which to compute the x0 value
	 * @return The line's observed frequency, translated in the current unit of the axis
	 */
	private double getX0ValueFromLine(LineDescription line) {
		double x0 = dataSource.getxAxis().convertFromMhzFreq(line.getObsFrequency());
		double vlsrLine = line.getVlsr();
		double vlsrSource = dataSource.getVlsr();
		double vlsr = XAxisCassis.convertDelta(XAxisCassis.getXAxisVelocity(), dataSource.getxAxis(), dataSource.getFreqRef(), vlsrLine - vlsrSource);
		return x0 - vlsr;
	}

	/**
	 * @return The value for a width parameter, in the appropriate unit
	 */
	private double getWidthValue() {
		return XAxisCassis.convertDelta(XAxisCassis.getXAxisVelocity(), dataSource.getxAxis(), dataSource.getFreqRef(), 1.);
	}

	/**
	 * Returns if the given component matches a configuration. A component matches if
	 * <li>It is not null</li>
	 * <li>It's in a category different than Baseline or Other</li>
	 * <li>Its type is the same as the one given</li>
	 * <li>It is a simple component and the number of lines is one</li>
	 * <li>It is a multiple component and the number of lines is equal to the number of children plus one</li>
	 * <br>
	 * @param estim The component which would be estimated
	 * @param totalNbLines The total number of lines to estimate from
	 * @param type The type of component
	 * @return If the component matches the configuration.
	 */
	private boolean match(FitAbstractComponent estim, int totalNbLines, FitType type) {
		boolean match = false;
		if (estim != null && isInCorrectCategory(estim) && estim.getComponentType().equals(type)) {
			if (!estim.isMultiple() && totalNbLines == 1) {
				match = true;
			}
			if (estim.isMultiple() && totalNbLines == ((FitMultiComponent) estim).getNbChildren() + 1) {
				match = true;
			}
		}
		return match;
	}

	/**
	 * Returns if the model is in another category than the ones defined as default
	 *
	 * @param model The model
	 * @return True if the model is in another category than Baseline or Other
	 */
	private boolean isInCorrectCategory(FitAbstractComponent model) {
		Category category = parametersModel.getManager().getCategory(model);
		return !category.equals(FitComponentManager.BASELINE_CATEGORY) && !category.equals(FitComponentManager.DEFAULT_CATEGORY);
	}


	/**
	 * Refreshes the input data for this model
	 */
	public void refreshInput() {
		parametersModel.clearFitCurves();
		for (XYSpectrumSeries series : dataSource.getAllSeries()) {
			if (series.getSpectrum() != null) {
				FitCurve curve = translateSpectrum(series.getSpectrum());
				curve.setFreqRef(series.getSpectrum().getFreqRef());
				curve.setVlsr(series.getSpectrum().getVlsr());
				curve.setName((String) series.getKey());
				this.parametersModel.addFitCurve(curve);
			}
		}
	}

	/**
	 * Converts every component of the fit configuration to match
	 * the new axis of the data source
	 *
	 * @param xAxis The destination axis
	 */
	public void convertConfiguration(XAxisCassis xAxis) {
		if (!xAxis.getUnit().equals(dataSource.getxAxis().getUnit())) {
			for (FitAbstractComponent component : parametersModel.getManager().getComponents()) {
				convertComponent(component, xAxis, dataSource.getxAxis());
			}
		}
		for (CategorizedComponent cc : categorizedComponents) {
			cc.refreshOriginalArray(xAxis);
		}
	}

	/**
	 * Converts every component of the fit configuration to match
	 * the new axis of the data source
	 *
	 * @param source The source axis
	 * @param dst The destination axis
	 */
	public void convertConfiguration(XAxisCassis source, XAxisCassis dst) {
		if (!source.getUnit().equals(dst.getUnit())) {
			for (FitAbstractComponent component : parametersModel.getManager().getComponents()) {
				convertComponent(component, source, dst);
			}
		}
		for (CategorizedComponent cc : categorizedComponents) {
			cc.refreshOriginalArray(dst);
		}
	}

	/**
	 * Converts an abstract component to match the new axis
	 *
	 * @param component The component to convert
	 * @param source The source axis
	 * @param dest The destination axis
	 */
	private void convertComponent(FitAbstractComponent component, XAxisCassis source, XAxisCassis dest) {
		if (!component.isMultiple()) {
			convertComponent((FitComponent) component, source, dest);
		} else {
			FitMultiComponent mComp = (FitMultiComponent) component;
			for (int i = 0 ; i < mComp.getNbChildren() ; i++) {
				convertComponent(mComp.getChild(i), mComp.getParent(), source, dest);
			}
			convertComponent(mComp.getParent(), source, dest);
		}
	}

	/**
	 * Converts a component to match the new axis
	 *
	 * @param component The component to convert
	 * @param source The source axis
	 * @param dest The destination axis
	 */
	private void convertComponent(FitComponent component, XAxisCassis source, XAxisCassis dest) {
		convertComponent(component, null, source, dest);
	}

	/**
	 * Converts a component to match the new axis
	 *
	 * @param component The component to convert
	 * @param parent The parent component (not converted)
	 * @param source The source axis
	 * @param dest The destination axis
	 */
	private void convertComponent(FitComponent component, FitComponent parent, XAxisCassis source, XAxisCassis dest) {
		for (int i = 0 ; i < component.getNbParameters() ; i++) {
			FitParameter param = component.getParameter(i);
			if (!param.isConstrained() && !param.getParameterType().equals(FitParameterType.I0)) {
				double value = param.getValue();
				if (FitParameterType.isWidthParameter(i)) {
					value = XAxisCassis.convertDelta(source, dest, dataSource.getFreqRef(), value);
				} else {
					value = XAxisCassis.convert(value, source, dest);
				}
				component.setParameterValue(i, value);
			} else if (param.getConstraint().getConstraintType().equals(FitConstraintType.SHIFT) && parent != null) {
				double value = XAxisCassis.convert(param.getValue(), source, dest);
				double parentValue = XAxisCassis.convert(parent.getParameter(i).getValue(), source, dest);
				component.setParameterFactor(i, value - parentValue);
			}
		}
		if (component instanceof FitPolynomialComponent && haveToconvertPolynomial ) {
			convertPolynomialComponent(component, source, dest);
		}
	}

	/**
	 * @param component
	 * @param source
	 * @param dest
	 */
	public void convertPolynomialComponent(FitComponent component, XAxisCassis source, XAxisCassis dest) {
		FitPolynomialComponent polyComp = (FitPolynomialComponent)component;
		if (UNIT.isFrequency(source.getUnit()) &&
			UNIT.isVelocity(dest.getUnit())) {
			XAxisVelocity dest2 = (XAxisVelocity)dest;
			double fo = source.convertFromMhzFreq(dest2.getFreqRef());
			double offsetX = -Formula.CKm - dest2.getVlsr();
			double scaleX =  fo / -Formula.CKm;
			FitNormalizationParameters fitNorParam = new FitNormalizationParameters(offsetX, scaleX, 0, 1);
			double[] params = FitPolynomialComponent.changeParametersNormalization(fitNorParam, polyComp.getFactors());
			polyComp.setFactor(params);
		} else if (UNIT.isFrequency(source.getUnit()) &&
			UNIT.isFrequency(dest.getUnit())) {
			FitNormalizationParameters fitNorParam = new FitNormalizationParameters(0, dest.getUnit().getCoeff()/source.getUnit().getCoeff(), 0, 1);
			double[] params = FitPolynomialComponent.changeParametersNormalization(fitNorParam, polyComp.getFactors());
			polyComp.setFactor(params);
		}
	}

	/**
	 * Create a FitCurve that is equivalent to the given spectrum
	 *
	 * @param spectrum The spectrum to translate
	 * @return A FitCurve with the same data (without unit)
	 */
	private FitCurve translateSpectrum(CommentedSpectrum spectrum) {
		double[] x = spectrum.getXData(dataSource.getxAxis());
		double[] y = spectrum.getIntensities(dataSource.getxAxis());
		return new FitCurve(x, y);
	}



}
