/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit.advanced.gui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.fit.gui.util.FitGuiUtil;
import eu.omp.irap.cassis.fit.gui.util.JDoubleCassisTextField;
import eu.omp.irap.cassis.gui.fit.advanced.AdvancedFitModel;
import eu.omp.irap.cassis.gui.fit.advanced.ModelFitManager;
import eu.omp.irap.cassis.properties.Software;

/**
 * Panel used to execute every actions available on the fitter
 * @author bastienkovac
 */
@SuppressWarnings("serial")
public class AdvancedFitActionPanel extends JPanel implements ModelListener {

	private ModelFitManager modelManager;

	private JButton fitGlobal;
	private JButton computeGlobalConfiguration;
	private JButton fitCurrent;
	private JButton fitAll;
	private JButton selectFile;
	private JButton save;
	private JButton loadConfig;
	private JButton saveConfig;

	private JComboBox<CategorizedComponent> categorizedCompComboBox;
	private JDoubleCassisTextField vlsrText;
	private JButton applyVlsr;

	private JButton substractFit;
	private JButton divideFit;
	private JToggleButton overlayFits;
	private JToggleButton overlayResidual;
	private JButton original;
	private JButton undo;
	private JButton redo;

	private JPanel actions;
	private JPanel vlsrPanel;
	private JPanel operations;


	/**
	 * Constructor
	 *
	 * @param modelManager Class that handles the actions of the fit
	 */
	public AdvancedFitActionPanel(ModelFitManager modelManager) {
		this.modelManager = modelManager;
		this.modelManager.addModelListener(this);
		initLayout(false);
	}

	private void initLayout(boolean gallery) {
		removeAll();
		actions = null;
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(Box.createVerticalStrut(10));
		add(getActions(gallery));
		add(Box.createVerticalStrut(10));
		add(getVlsrPanel());
		add(Box.createVerticalStrut(10));
		add(getOperations());
	}

	public void setGalleryMode(boolean gallery) {
		initLayout(gallery);
		getSubstractFit().setEnabled(!gallery);
		getOriginal().setEnabled(!gallery);
		getUndo().setEnabled(!gallery);
		getRedo().setEnabled(!gallery);
	}

	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (event.getSource().equals(AdvancedFitModel.CATEGORIZED_COMPONENTS_EVENT)) {
			reinitComboBox();
		} else if (event.getSource().equals(ModelFitManager.CURRENT_MODEL_CHANGED_EVENT)) {
			reinitComboBox();
		} else if (event.getSource().equals(AdvancedFitModel.VLSR_EVENT)) {
			CategorizedComponent cc = categorizedCompComboBox.getItemAt(categorizedCompComboBox.getSelectedIndex());
			if (cc != null) {
				getVlsr().setValue(cc.getVlsr());
			}
		} else if (event.getSource().equals(AdvancedFitModel.DUPLICATED_LINES_EVENT)) {
			@SuppressWarnings("unchecked")
			List<LineDescription> lines = (List<LineDescription>) event.getValue();

			JOptionPane.showMessageDialog(this, lines.size() +
					" lines have been removed because \n"
					+ "they have the exact same frequencies as retained lines ", "Duplicates lines",
					JOptionPane.WARNING_MESSAGE);
		}
	}


	private void reinitComboBox() {
		getCategorizedCompComboBox().removeAllItems();
		final AdvancedFitModel currentModel = modelManager.getCurrentModel();
		if (currentModel != null) {
			for (CategorizedComponent cc : currentModel.getCategorizedComponents()) {
				getCategorizedCompComboBox().addItem(cc);
			}
		}
	}

	/**
	 * @return A {@link JButton} that initiate the fitting on the current curve
	 */
	public JButton getFitCurrent() {
		if (fitCurrent == null) {
			fitCurrent = new JButton("Fit Current");
			fitCurrent.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					modelManager.performFit();
				}
			});
		}
		return fitCurrent;
	}

	/**
	 * @return A {@link JButton} that initiate the fitting on every curve
	 */
	public JButton getFitAll() {
		if (fitAll == null) {
			fitAll = new JButton("Fit All");
			fitAll.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					modelManager.performFitAll();
				}
			});
		}
		return fitAll;
	}

	/**
	 * @return A {@link JButton} that let the user sets the file to use
	 * as log for the rotational diagram
	 */
	public JButton getSelectFile() {
		if (selectFile == null) {
			selectFile = new JButton("Select File");
			selectFile.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					modelManager.selectLogFile();
				}
			});
		}
		return selectFile;
	}

	/**
	 * @return A {@link JButton} that let the user save the current fit
	 * components in the log file
	 */
	public JButton getSave() {
		if (save == null) {
			save = new JButton("Save");
			save.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					modelManager.saveFit();
				}
			});
		}
		return save;
	}

	/**
	 * @return A {@link JButton} used to load a fit configuration from a file
	 */
	public JButton getLoadConfig() {
		if (loadConfig == null) {
			loadConfig = new JButton("Load Config.");
			loadConfig.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					File loadingFile = FitGuiUtil.openComponentsPropertiesFile(getTopLevelAncestor(), false,
							Software.getCassisPath(), Software.getLastFolder("fit-config"));
					if (loadingFile != null) {
						modelManager.loadConfiguration(loadingFile);
						Software.setLastFolder("fit-config", loadingFile.getParent());
					}
				}
			});
		}
		return loadConfig;
	}

	/**
	 * @return A {@link JButton} used to save the fit configuration in a file
	 */
	public JButton getSaveConfig() {
		if (saveConfig == null) {
			saveConfig = new JButton("Save Config.");
			saveConfig.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					File savingFile = FitGuiUtil.openComponentsPropertiesFile(getTopLevelAncestor(), true,
							Software.getCassisPath(), Software.getLastFolder("fit-config"));
					if (savingFile != null) {
						modelManager.saveConfiguration(savingFile);
						Software.setLastFolder("fit-config", savingFile.getParent());
					}
				}
			});
		}
		return saveConfig;
	}

	public JComboBox<CategorizedComponent> getCategorizedCompComboBox() {
		if (categorizedCompComboBox == null) {
			categorizedCompComboBox = new JComboBox<>();
			categorizedCompComboBox.addItemListener(new ItemListener() {

				@Override
				public void itemStateChanged(ItemEvent e) {
					CategorizedComponent cc = categorizedCompComboBox.getItemAt(categorizedCompComboBox.getSelectedIndex());
					if (cc != null) {
						getVlsr().setText(String.valueOf(cc.getVlsr()));
					} else {
						getVlsr().setText("0.0");
					}
				}
			});
			reinitComboBox();
		}
		return categorizedCompComboBox;
	}

	public JDoubleCassisTextField getVlsr() {
		if (vlsrText == null) {
			vlsrText = new JDoubleCassisTextField();
			vlsrText.setText("0.0");
		}
		return vlsrText;
	}

	public JButton getApplyVlsr() {
		if (applyVlsr == null) {
			applyVlsr = new JButton("Apply VLSR");
			applyVlsr.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					applyVlsr();
				}
			});
		}
		return applyVlsr;
	}

	/**
	 * @return A {@link JButton} that initiate the substract between the current curve
	 * and the fitted one
	 */
	public JButton getSubstractFit() {
		if (substractFit == null) {
			substractFit = new JButton("Substract Fit");
			substractFit.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					modelManager.substractFit();
				}
			});
			substractFit.setEnabled(false);
		}
		return substractFit;
	}

	/**
	 * Not implemented
	 * @return
	 */
	public JButton getDivideFit() {
		if (divideFit == null) {
			divideFit = new JButton("Divide by Fit");
			divideFit.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					modelManager.divideFit();
				}
			});
			divideFit.setEnabled(false);
		}
		return divideFit;
	}

	/**
	 * @return A {@link JToggleButton} that let the user display or hide the fit
	 * curves
	 */
	public JToggleButton getOverlayFits() {
		if (overlayFits == null) {
			overlayFits = new JToggleButton("Overlay Fits");
			overlayFits.addChangeListener(new ChangeListener() {

				@Override
				public void stateChanged(ChangeEvent e) {
					if (overlayFits.isSelected()) {
						overlayFits.setText("Hide Fits");
					} else {
						overlayFits.setText("Overlay Fits");
					}
				}
			});
			overlayFits.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					modelManager.overlayFit(overlayFits.isSelected());
				}
			});
			overlayFits.setEnabled(false);
		}
		return overlayFits;
	}

	/**
	 * @return A {@link JToggleButton} that let the user display or hide the residual
	 * curves
	 */
	public JToggleButton getOverlayResidual() {
		if (overlayResidual == null) {
			overlayResidual = new JToggleButton("Overlay Residual");
			overlayResidual.addChangeListener(new ChangeListener() {

				@Override
				public void stateChanged(ChangeEvent e) {
					if (overlayResidual.isSelected()) {
						overlayResidual.setText("Hide Residual");
					} else {
						overlayResidual.setText("Overlay Residual");
					}
				}
			});
			overlayResidual.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					modelManager.overlayResidual(overlayResidual.isSelected());
				}
			});
			overlayResidual.setEnabled(false);
		}
		return overlayResidual;
	}

	/**
	 * @return A {@link JButton} that restores the curve in its original state
	 */
	public JButton getOriginal() {
		if (original == null) {
			original = new JButton("Original");
			original.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					modelManager.restoreOriginal();
				}
			});
		}
		return original;
	}

	/**
	 * @return A {@link JButton} that undoes the last undoable action
	 */
	public JButton getUndo() {
		if (undo == null) {
			undo = new JButton("Undo");
			undo.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					modelManager.undo();
				}
			});
			undo.setEnabled(false);
		}
		return undo;
	}

	/**
	 * @return A {@link JButton} that redoes the next redoable action
	 */
	public JButton getRedo() {
		if (redo == null) {
			redo = new JButton("Redo");
			redo.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					modelManager.redo();
				}
			});
			redo.setEnabled(false);
		}
		return redo;
	}

	public JButton getFitGlobal() {
		if (fitGlobal == null) {
			fitGlobal = new JButton("Fit Current Global Spectrum");
			fitGlobal.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					modelManager.performGlobalFit();
				}
			});
		}
		return fitGlobal;
	}

	public JButton getComputeGlobalConfiguration() {
		if (computeGlobalConfiguration == null) {
			computeGlobalConfiguration = new JButton("Compute Global Configuration");
			computeGlobalConfiguration.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					modelManager.computeGlobalConfiguration();
					AdvancedFitFrame.getFrame().openFrame(true);
				}
			});
		}
		return computeGlobalConfiguration;
	}

	private JPanel getActions(boolean gallery) {
		if (actions == null) {
			actions = new JPanel();
			actions.setLayout(new BoxLayout(actions, BoxLayout.Y_AXIS));
			if (gallery) {
				JPanel tmp = new JPanel(new GridLayout(2, 1, 5, 5));
				tmp.add(getFitGlobal());
				tmp.add(getComputeGlobalConfiguration());
				JPanel tmp2 = new JPanel(new GridLayout(2, 2, 5, 5));
				tmp2.add(getSelectFile());
				tmp2.add(getSave());
				tmp2.add(getLoadConfig());
				tmp2.add(getSaveConfig());
				actions.add(tmp);
				actions.add(Box.createRigidArea(new Dimension(0, 4)));
				actions.add(tmp2);
			} else {
				JPanel tmp = new JPanel(new GridLayout(3, 2, 5, 5));
				tmp.add(getFitCurrent());
				tmp.add(getFitAll());
				tmp.add(getSelectFile());
				tmp.add(getSave());
				tmp.add(getLoadConfig());
				tmp.add(getSaveConfig());
				actions.add(tmp);
			}
			actions.setBorder(BorderFactory.createTitledBorder("Actions"));
		}
		return actions;
	}

	private JPanel getVlsrPanel() {
		if (vlsrPanel == null) {
			vlsrPanel = new JPanel();
			vlsrPanel.setBorder(BorderFactory.createTitledBorder("VLSR"));
			vlsrPanel.setLayout(new GridLayout(2, 1, 5, 5));
			vlsrPanel.add(getCategorizedCompComboBox());
			JPanel applyVlsrPanel = new JPanel(new GridLayout(1, 2, 5, 5));
			applyVlsrPanel.add(new JLabel("Value"));
			applyVlsrPanel.add(getVlsr());
			getVlsr().addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						applyVlsr();
					}
				}
			});
			vlsrPanel.add(applyVlsrPanel);
		}
		return vlsrPanel;
	}

	private JPanel getOperations() {
		if (operations == null) {
			operations = new JPanel();
			operations.setBorder(BorderFactory.createTitledBorder("Operation"));
			operations.setLayout(new BoxLayout(operations, BoxLayout.Y_AXIS));
			JPanel operationFit = new JPanel(new GridLayout(2, 1, 5, 5));
			operationFit.add(getSubstractFit());
			operationFit.add(getDivideFit());
			operationFit.add(getOverlayFits());
			operationFit.add(getOverlayResidual());

			operations.add(operationFit);
			operations.add(Box.createRigidArea(new Dimension(0, 4)));

			JPanel history = new JPanel(new GridLayout(1, 3, 5, 5));
			history.add(getOriginal());
			history.add(getUndo());
			history.add(getRedo());
			operations.add(history);
		}
		return operations;
	}

	private void applyVlsr() {
		CategorizedComponent selectedComp = (CategorizedComponent) getCategorizedCompComboBox().getSelectedItem();
		double val = selectedComp.getVlsr();
		try {
			val = Double.parseDouble(getVlsr().getText());
		} catch (NumberFormatException exc) {
			return;
		}
		if (selectedComp != null) {
			selectedComp.applyVlsr(val);
		}
	}

}
