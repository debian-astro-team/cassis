/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit.advanced.gui;

import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.XAxisVelocity;
import eu.omp.irap.cassis.fit.components.FitAbstractComponent;
import eu.omp.irap.cassis.fit.components.FitComponent;
import eu.omp.irap.cassis.fit.components.FitMultiComponent;
import eu.omp.irap.cassis.fit.util.Category;
import eu.omp.irap.cassis.fit.util.enums.FitParameterType;

public class CategorizedComponent {

	private static final int X0_INDEX = FitParameterType.X0.getIndex();

	private final Category category;
	private final FitAbstractComponent component;
	private double vlsrOrigin;

	private XAxisCassis axis;

	private double[] originalValues;
	private double vlsr;


	public CategorizedComponent(Category category, FitAbstractComponent component, XAxisCassis axis, double vlsrOrigin) {
		this.category = category;
		this.component = component;
		this.axis = axis;
		this.vlsrOrigin = vlsrOrigin;
		this.vlsr = this.vlsrOrigin;
		initOriginalArray();
	}

	public void refreshOriginalArray(XAxisCassis newAxis) {
		this.axis = newAxis;
		fillOriginalArray();
	}

	public void copy(CategorizedComponent other) {
		if (this.originalValues.length == other.originalValues.length) {
			this.vlsr = other.vlsr;
			for (int i = 0 ; i < this.originalValues.length ; i++) {
				this.originalValues[i] = other.originalValues[i];
			}
			this.axis = other.axis;
			this.vlsrOrigin = other.vlsrOrigin;
		}
	}

	private void initOriginalArray() {
		int size = 1;
		if (component.isMultiple()) {
			size = ((FitMultiComponent) component).getNbChildren() + 1;
		}
		originalValues = new double[size];
		fillOriginalArray();
	}

	private void fillOriginalArray() {
		if (component.isMultiple()) {
			FitMultiComponent mC = (FitMultiComponent) component;
			originalValues[0] = mC.getParent().getParameter(X0_INDEX).getValue();
			for (int i = 0 ; i < mC.getNbChildren() ; i++) {
				originalValues[i + 1] = mC.getChild(i).getParameter(X0_INDEX).getValue();
			}
		} else {
			originalValues[0] = ((FitComponent) component).getParameter(X0_INDEX).getValue();
		}
	}

	public void applyVlsr(double val) {
		this.vlsr = val;
		if (component.isMultiple()) {
			applyVlsr((FitMultiComponent) component, val - vlsrOrigin);
		} else {
			applyVlsr((FitComponent) component, val - vlsrOrigin);
		}
	}

	public void updateVlsr(double newX0Value, XAxisCassis paramAxis) {
		if (!this.axis.equals(paramAxis)) {
			this.vlsr = newX0Value;
			return;
		}
		XAxisVelocity vel = XAxisVelocity.getXAxisVelocity();
		vel.setFreqRef(this.axis.convertToMHzFreq(originalValues[0]));
		if (!this.axis.getUnit().equals(vel.getUnit())) {
			this.vlsr = XAxisCassis.convert(newX0Value, axis, vel) + vlsrOrigin;
		} else {
			this.vlsr = newX0Value;
		}
	}

	private void applyVlsr(FitMultiComponent comp, double vlsr) {
		double x0Parent = getNewX0Value(originalValues[0], vlsr);
		for (int i = 0 ; i < comp.getNbChildren() ; i++) {
			double x0I = getNewX0Value(originalValues[i + 1], vlsr);
			comp.getChild(i).setParameterFactor(X0_INDEX, x0I - x0Parent);
		}
		comp.getParent().setParameterValue(X0_INDEX, x0Parent);
	}

	private void applyVlsr(FitComponent comp, double vlsr) {
		comp.setParameterValue(X0_INDEX, getNewX0Value(originalValues[0], vlsr));
	}

	private double getNewX0Value(double old, double vlsr) {
		XAxisVelocity vel = XAxisVelocity.getXAxisVelocity();
		vel.setFreqRef(this.axis.convertToMHzFreq(old));
		return XAxisCassis.convert(vlsr, vel, this.axis);
	}

	public Category getCategory() {
		return category;
	}

	public FitAbstractComponent getComponent() {
		return component;
	}

	public double getVlsr() {
		return vlsr;
	}

	@Override
	public String toString() {
		return category.getName() + " - " + component.getName();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((category == null) ? 0 : category.hashCode());
		result = prime * result + ((component == null) ? 0 : component.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CategorizedComponent other = (CategorizedComponent) obj;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (component == null) {
			if (other.component != null)
				return false;
		} else if (!component.equals(other.component))
			return false;
		return true;
	}

}
