/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit.advanced.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.border.Border;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.fit.gui.util.FitGuiUtil;
import eu.omp.irap.cassis.gui.fit.advanced.ModelFitManager;

/**
 * Panel handling the creation of the different models, as well
 * as some general parameters linked with the {@link ModelFitManager}
 * @author bastienkovac
 */
@SuppressWarnings("serial")
public class AdvancedFitCreationPanel extends JPanel implements ModelListener {

	private ModelFitManager model;

	private JButton openFrame;
	private JButton clearConfiguration;
	private AdvancedFitActionPanel actions;

	private JLabel stdResidualError;
	private JLabel loggingFile;
	private JCheckBox appending;
	private JCheckBox autoSave;

	private Set<Component> toEnable;

	private JPanel miscPanel;


	/**
	 * Constructor
	 *
	 * @param model The ModelManager
	 */
	public AdvancedFitCreationPanel(ModelFitManager model) {
		super(new BorderLayout());
		this.model = model;
		this.model.addModelListener(this);
		toEnable = new HashSet<>();
		initLayout();
	}

	/**
	 * Sets the {@link AdvancedFitFrame} visible
	 *
	 * @return A {@link JButton}
	 */
	public JButton getOpenFrame() {
		if (openFrame == null) {
			openFrame = new JButton("Open");
			openFrame.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					AdvancedFitFrame.getFrame().openFrame(true);
				}
			});
		}
		return openFrame;
	}

	/**
	 * @return A {@link JButton} that let the user clear the current configuration
	 */
	public JButton getClearConfiguration() {
		if (clearConfiguration == null) {
			clearConfiguration = new JButton("Clear");
			clearConfiguration.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					model.clearConfiguration();
				}
			});
		}
		return clearConfiguration;
	}

	/**
	 * @return The {@link AdvancedFitActionPanel} associated with this panel
	 */
	public AdvancedFitActionPanel getActions() {
		if (actions == null) {
			actions = new AdvancedFitActionPanel(model);
		}
		return actions;
	}

	/**
	 * @return A JLabel displaying the standard residual error of the fit
	 */
	public JLabel getStdResidualError() {
		if (stdResidualError == null) {
			double rmsValue = 0.;
			if (model.getCurrentModel() != null) {
				rmsValue = model.getCurrentModel().getParametersModel().getStdResidualError();
			}
			stdResidualError = new JLabel("Residual Standard Deviation : " + FitGuiUtil.formatDouble(rmsValue));
		}
		return stdResidualError;
	}

	/**
	 * @return A JLabel displaying the current log file's name
	 */
	public JLabel getLoggingFile() {
		if (loggingFile == null) {
			loggingFile = new JLabel("Current log file: --- ");
		}
		return loggingFile;
	}

	/**
	 * @return A JCheckBox that let the user decide if the result of the fit
	 * must overwrite the previous content of the log file
	 */
	public JCheckBox getAppending() {
		if (appending == null) {
			appending = new JCheckBox("Overwriting log file", !model.isAppending());
			appending.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					model.setAppending(!appending.isSelected());
				}
			});
		}
		return appending;
	}

	/**
	 * @return A JCheckBox that let the user decide if the result of every fit must be saved
	 * automatically
	 */
	public JCheckBox getAutoSave() {
		if (autoSave == null) {
			autoSave = new JCheckBox("Save automatically after Fit", model.isAutoSave());
			autoSave.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					model.setAutoSave(autoSave.isSelected());
				}
			});
		}
		return autoSave;
	}

	/**
	 * Recursively sets every component of this panel enabled
	 *
	 * @param enabled
	 */
	public void enableAll(boolean enabled) {
		enableAll(this, enabled);
		stdResidualError.setText("Residual Standard Deviation : " + 0.);
	}

	public void setGalleryMode(boolean gallery) {
		getActions().setGalleryMode(gallery);
	}

	/**
	 * Refreshes the RMS label's text
	 */
	public void refreshRms() {
		dataChanged(new ModelChangedEvent(ModelFitManager.FIT_PERFORMED_EVENT));
	}

	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (event.getSource().equals(ModelFitManager.FIT_PERFORMED_EVENT)) {
			double stdResidual = 0.;
			if (model.getCurrentModel() != null) {
				stdResidual = model.getCurrentModel().getParametersModel().getStdResidualError();
			}
			stdResidualError.setText("Residual Standard Deviation : " + FitGuiUtil.formatDouble(stdResidual));
		} else if (event.getSource().equals(ModelFitManager.FIT_LOGGING_INITIALIZED_EVENT)) {
			getLoggingFile().setText("Current log file: " + model.getLogFileName());
			getAppending().setSelected(!model.isAppending());
		}
	}

	private void enableAll(Container container, boolean enabled) {
		for (Component c : container.getComponents()) {
			if (c.isEnabled()) {
				toEnable.add(c);
			}
			c.setEnabled(enabled && toEnable.contains(c));
			if (c instanceof Container) {
				enableAll((Container) c, enabled);
			}
		}
	}

	private void initLayout() {
		JPanel outPanel = new JPanel();
		outPanel.setLayout(new BoxLayout(outPanel, BoxLayout.Y_AXIS));

		JPanel tmpPanel = new JPanel();
		tmpPanel.setLayout(new BoxLayout(tmpPanel, BoxLayout.Y_AXIS));
		JPanel tmpGrid = new JPanel(new GridLayout(1, 2, 5, 5));
		Border out = BorderFactory.createTitledBorder("Configuration");
		Border in = BorderFactory.createEmptyBorder(5, 5, 5, 5);
		tmpGrid.setBorder(BorderFactory.createCompoundBorder(out, in));
		tmpGrid.add(getOpenFrame());
		tmpGrid.add(getClearConfiguration());
		tmpPanel.add(tmpGrid);
		tmpPanel.add(getActions());
		outPanel.add(tmpPanel);
		outPanel.add(Box.createVerticalStrut(10));
		outPanel.add(getMiscPanel());
		add(outPanel, BorderLayout.NORTH);
		setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
	}

	private JPanel getMiscPanel() {
		if (miscPanel == null) {
			miscPanel = new JPanel();
			miscPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
					"General information"));
			JSeparator sep = new JSeparator();

			GroupLayout layout = new GroupLayout(miscPanel);
			layout.setAutoCreateContainerGaps(true);
			layout.setAutoCreateGaps(true);

			layout.setHorizontalGroup(layout.createParallelGroup(Alignment.LEADING)
					.addComponent(getStdResidualError())
					.addComponent(sep)
					.addComponent(getLoggingFile())
					.addComponent(getAppending())
					.addComponent(getAutoSave())
					);

			layout.setVerticalGroup(layout.createSequentialGroup()
					.addComponent(getStdResidualError())
					.addComponent(sep)
					.addComponent(getLoggingFile())
					.addComponent(getAppending())
					.addComponent(getAutoSave())
					);

			miscPanel.setLayout(layout);
		}
		return miscPanel;
	}

}
