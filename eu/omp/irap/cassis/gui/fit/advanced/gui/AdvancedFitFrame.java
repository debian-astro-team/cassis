/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit.advanced.gui;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.fit.FitComponentManager;
import eu.omp.irap.cassis.fit.components.FitAbstractComponent;
import eu.omp.irap.cassis.gui.fit.advanced.AdvancedFitModel;

/**
 * Frame used to display the different {@link AdvancedFitPanel}, the frame acts
 * as a singleton so only its content will change when moving from one model to another.
 * @author bastienkovac
 */
@SuppressWarnings("serial")
public class AdvancedFitFrame extends JFrame {

	private static AdvancedFitFrame frame;
	private static AdvancedFitModel curModel;


	/**
	 * Constructor
	 */
	private AdvancedFitFrame() {
		super("Fit Configuration");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
	}

	/**
	 * Clean up the current instance of AdvancedFitFrame
	 */
	public static void cleanUp() {
		if (frame != null) {
			frame.dispose();
		}
		frame = null;
		curModel = null;
	}

	/**
	 * Creates if necessary, then returns the {@link AdvancedFitFrame} instance
	 *
	 * @return The frame instance
	 */
	public static AdvancedFitFrame getFrame() {
		if (frame == null) {
			frame = new AdvancedFitFrame();
			frame.addWindowListener(new WindowAdapter() {

				@Override
				public void windowDeactivated(WindowEvent e) {
					super.windowDeactivated(e);
					if (frame != null) {
						stopEditingJTable(frame.getContentPane());
					}
				}

				private void stopEditingJTable(Container contentPane) {
					for (Component compo : contentPane.getComponents()) {
						if (compo instanceof JTable){
							JTable table = (JTable)compo;
							if (table.isEditing()){
								table.getCellEditor().stopCellEditing();
							}
						}else if (compo instanceof Container){
							stopEditingJTable((Container)compo);
						}
					}
				}
			});
		}
		return frame;
	}

	/**
	 * Sets the frame visible and executes a refresh if necessary
	 *
	 * @param b True if the frame must be set visible
	 */
	public void openFrame(boolean b) {
		setVisible(b);
		toFront();
		repaint();
	}

	/**
	 * Creates a panel that displays the given model, then sets it
	 * as content pane of the frame
	 *
	 * @param model The model to display
	 */
	public void displayModel(final AdvancedFitModel model) {
		if (model != null && !model.equals(curModel)) {
			curModel = model;
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					setContentPane(new AdvancedFitPanel(model));
					pack();
					if (!isVisible()) {
						setLocationRelativeTo(null);
					}
					FitComponentManager manager = model.getParametersModel().getManager();
					ModelChangedEvent e = new ModelChangedEvent(FitComponentManager.COMPONENT_MANAGER_STYLE_SELECTED_EVENT,
							manager.getSelectedFitStyle());
					for (FitAbstractComponent component : manager.getComponents()) {
						component.fireDataChanged(e);
					}
				}
			});
		}
	}

}
