/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit.advanced;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.gui.fit.advanced.interfaces.FitSourceInterface;
import eu.omp.irap.cassis.gui.plot.curve.TypeCurve;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;

public class MultiGraphSource implements FitSourceInterface {

	private CommentedSpectrum combinedSpectrum;
	private List<FitSourceInterface> sources;


	public MultiGraphSource(List<FitSourceInterface> sources) {
		this.sources = sources;
		updateCombinedGraph(this.sources);
	}

	private void updateCombinedGraph(List<FitSourceInterface> sources) {
		List<CassisSpectrum> spectrums = new ArrayList<>();
		List<LineDescription> lines = new ArrayList<>();
		for (FitSourceInterface source : sources) {
			CommentedSpectrum commentedSpect = source.getCurrentSeries().getSpectrum();
			String title = commentedSpect.getTitle();
			double[] x = commentedSpect.getFrequenciesSignal();
			double[] y = commentedSpect.getIntensities();
			double vlsr = commentedSpect.getVlsr();
			double lo = commentedSpect.getLoFreq();
			XAxisCassis xAxis = commentedSpect.getxAxisOrigin();
			YAxisCassis yAxis = commentedSpect.getyAxis();
			CassisSpectrum cassisSpectrum = CassisSpectrum.generateCassisSpectrum(title, Double.NaN, x, y, lo, vlsr, xAxis, yAxis);
			spectrums.add(cassisSpectrum);
			lines.addAll(source.getTopPossibleLines());
		}
		CassisSpectrum spectrum = CassisSpectrum.mergeCassisSpectrumList(spectrums);
		if (spectrum != null) {
			this.combinedSpectrum = new CommentedSpectrum(lines, spectrum.getFrequencies(), spectrum.getIntensities(), spectrum.getTitle());
			this.combinedSpectrum.setVlsr(spectrum.getVlsr());
			this.combinedSpectrum.setLoFreq(spectrum.getLoFrequency());
		}
	}

	public List<FitSourceInterface> getSources() {
		return Collections.unmodifiableList(sources);
	}

	public double getFreqRefSource(int index) {
		return this.sources.get(index).getFreqRef();
	}

	@Override
	public XAxisCassis getxAxis() {
		return combinedSpectrum == null ? XAxisCassis.getXAxisVelocity() : combinedSpectrum.getxAxisOrigin();
	}

	@Override
	public YAxisCassis getyAxis() {
		return combinedSpectrum.getyAxis();
	}

	@Override
	public double getVlsr() {
		return combinedSpectrum.getVlsr();
	}

	@Override
	public double getFreqRef() {
		return sources.get(0).getFreqRef();
	}

	@Override
	public XYSpectrumSeries getCurrentSeries() {
		return new XYSpectrumSeries("Global Spectrum", getxAxis(), TypeCurve.DATA, combinedSpectrum);
	}

	@Override
	public List<XYSpectrumSeries> getAllSeries() {
		List<XYSpectrumSeries> series = new ArrayList<>();
		series.add(getCurrentSeries());
		return series;
	}

	@Override
	public List<LineDescription> getBottomPossibleLines() {
		return new ArrayList<>();
	}

	@Override
	public List<LineDescription> getTopPossibleLines() {
		return combinedSpectrum.getListOfLines();
	}

	@Override
	public List<LineDescription> getAllLines() {
		return combinedSpectrum.getListOfLines();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((combinedSpectrum == null) ? 0 : combinedSpectrum.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MultiGraphSource other = (MultiGraphSource) obj;
		if (sources.size() != other.sources.size())
			return false;
		for (int i = 0 ; i < sources.size() ; i++) {
			if (!sources.get(i).equals(other.sources.get(i))) {
				return false;
			}
		}
		if (combinedSpectrum == null) {
			if (other.combinedSpectrum != null)
				return false;
		} else if (!compareSpectrums(combinedSpectrum, other.combinedSpectrum))
			return false;
		return true;
	}

	private boolean compareSpectrums(CommentedSpectrum s1, CommentedSpectrum s2) {
		double[] x1 = s1.getFrequenciesSignal();
		double[] x2 = s2.getFrequenciesSignal();
		double[] y1 = s1.getIntensities();
		double[] y2 = s2.getIntensities();
		if (x1.length != x2.length || y1.length != y2.length || x1.length != y1.length || x2.length != y2.length) {
			return false;
		} else {
			for (int i = 0 ; i < x1.length ; i++) {
				if (x1[i] != x2[i] || y1[i] != y2[i]) {
					return false;
				}
			}
		}
		return true;
	}

}
