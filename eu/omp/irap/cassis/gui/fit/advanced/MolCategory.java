/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit.advanced;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.fit.util.Category;

/**
 * A Category used for the fit that can represent multiple molecules
 *
 * @author bastienkovac
 *
 */
public class MolCategory extends Category {

	private Set<Integer> molTags;


	/**
	 * Constructor
	 *
	 * @param mainLine The main line, whose tag will serve as ID of the category
	 */
	public MolCategory(LineDescription mainLine) {
		super(mainLine.getMolName(), mainLine.getMolTag());
		this.molTags = new HashSet<>();
		this.molTags.add(mainLine.getMolTag());
	}

	/**
	 * Constructor
	 *
	 * @param mainLine The main line, whose tag will serve as ID of the category
	 * @param extraLines The extra lines, whose tags will be stored
	 */
	public MolCategory(LineDescription mainLine, List<LineDescription> extraLines) {
		super("Multiple Species", mainLine.getMolTag());
		this.molTags = new HashSet<>();
		this.molTags.add(mainLine.getMolTag());
		for (LineDescription line : extraLines) {
			this.molTags.add(line.getMolTag());
		}
	}

	/**
	 * Adds a molecular tag to the category
	 *
	 * @param tag The tag to add
	 */
	public void addTag(int tag) {
		molTags.add(tag);
		if (molTags.size() > 1) {
			setName("Multiple Species");
		}
	}

	/**
	 * @return The tags contained in this category
	 */
	public Set<Integer> getTags() {
		return Collections.unmodifiableSet(molTags);
	}

}
