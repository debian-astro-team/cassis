/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit.advanced;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.UtilArrayList;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.XAxisVelocity;
import eu.omp.irap.cassis.common.events.ListenerManager;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.database.access.AccessDataBase;
import eu.omp.irap.cassis.fit.FitComponentManager;
import eu.omp.irap.cassis.fit.FitParametersModel;
import eu.omp.irap.cassis.fit.components.FitAbstractComponent;
import eu.omp.irap.cassis.fit.components.FitComponent;
import eu.omp.irap.cassis.fit.components.FitConstraint.FitConstraintType;
import eu.omp.irap.cassis.fit.components.FitMultiComponent;
import eu.omp.irap.cassis.fit.history.ICommand;
import eu.omp.irap.cassis.fit.util.Category;
import eu.omp.irap.cassis.fit.util.FitCurve;
import eu.omp.irap.cassis.fit.util.FitException;
import eu.omp.irap.cassis.fit.util.enums.FitParameterType;
import eu.omp.irap.cassis.gui.fit.TelescopeFitInterface;
import eu.omp.irap.cassis.gui.fit.advanced.gui.AdvancedFitFrame;
import eu.omp.irap.cassis.gui.fit.advanced.interfaces.FitActionsInterface;
import eu.omp.irap.cassis.gui.fit.advanced.interfaces.FitSourceInterface;
import eu.omp.irap.cassis.gui.fit.advanced.save.FitToRotationalComputations;
import eu.omp.irap.cassis.gui.fit.advanced.save.FitToRotationalSave;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import eu.omp.irap.cassis.gui.plot.gallery.GalleryNavigationModel;
import eu.omp.irap.cassis.properties.Software;

/**
 * This class is used to manage the different {@link AdvancedFitModel} created
 * (One per plot).
 *
 * @author bastienkovac
 */
public class ModelFitManager extends ListenerManager implements FitActionsInterface, ModelListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(ModelFitManager.class);

	public static final String FIT_MODEL_ADDED_EVENT = "fitModelAddedEvent";
	public static final String FIT_LOGGING_INITIALIZED_EVENT = "fitLoggingInitializedEvent";
	public static final String FIT_AUTO_SAVE_EVENT = "fitAutoSaveEvent";
	public static final String CURRENT_MODEL_CHANGED_EVENT = "currentModelChangedEvent";

	private List<AdvancedFitModel> models;
	private int currentIndex;

	private AdvancedFitResult lastResult;

	private GalleryNavigationModel galleryNavigationModel;
	private FitToRotationalSave saveFit;

	private MultiGraphModel combinedModel;

	private boolean autoSave;
	private boolean onGallery;


	/**
	 * Constructor
	 */
	public ModelFitManager() {
		this(null);
	}

	/**
	 * Constructor
	 *
	 * @param galleryNavigationModel
	 *            The gallery navigation model
	 */
	public ModelFitManager(GalleryNavigationModel galleryNavigationModel) {
		this.models = new ArrayList<>();
		this.galleryNavigationModel = galleryNavigationModel;
		if (this.galleryNavigationModel != null) {
			this.galleryNavigationModel.addModelListener(this);
			updateCombinedModel();
		}
		this.saveFit = new FitToRotationalSave();
		this.autoSave = Software.getUserConfiguration().isFitAutoSave();
	}

	/**
	 * Clears the created models and sets the current index to 0
	 */
	public void clearModels() {
		for (AdvancedFitModel model : models) {
			model.removeModelListener(this);
		}
		models.clear();
		this.currentIndex = 0;
		this.lastResult = null;
	}

	/**
	 * Adds a {@link AdvancedFitModel} computed from a given source to the
	 * manager
	 *
	 * @param source
	 *            The source used for the model
	 */
	public void addModel(FitSourceInterface source) {
		AdvancedFitModel model = new AdvancedFitModel(source);
		model.addModelListener(this);
		models.add(model);
		fireDataChanged(new ModelChangedEvent(ModelFitManager.FIT_MODEL_ADDED_EVENT, model));
	}

	/**
	 * Returns the model at the i-th index
	 *
	 * @param index
	 *            The index
	 * @return The corresponding model
	 */
	public AdvancedFitModel getModel(int index) {
		if (index < models.size()) {
			return models.get(index);
		}
		return null;
	}

	/**
	 * @return The models held by the manager
	 */
	public List<AdvancedFitModel> getModels() {
		return models;
	}

	public void copyModel(AdvancedFitModel modelSource) {
		FitComponentManager managerSource = modelSource.getParametersModel().getManager();
		List<Integer> molTags = getMolCategoryTags(managerSource);
		for (int i : galleryNavigationModel.getFilter()) {
			if (i != currentIndex) {
				AdvancedFitModel modelDest = models.get(i);
				copyModel(modelSource, modelDest);
				copyManager(managerSource, modelDest, molTags);
			}
		}
	}

	private void copyModel(AdvancedFitModel modelSource, AdvancedFitModel modelDest) {
		modelDest.getParametersModel().setNbIterations(modelSource.getParametersModel().getNbIterations());
		modelDest.getParametersModel().setOversampling(modelSource.getParametersModel().getOversampling());
		modelDest.getParametersModel().setTolerance(modelSource.getParametersModel().getTolerance());
	}

	private void copyManager(FitComponentManager managerSource, AdvancedFitModel modelDest, List<Integer> tags) {
		FitComponentManager managerDest = modelDest.getParametersModel().getManager();
		managerDest.clearManager();
		copyCategory(FitComponentManager.BASELINE_CATEGORY, managerSource, managerDest);
		copyCategory(FitComponentManager.DEFAULT_CATEGORY, managerSource, managerDest);
		for (int tag : tags) {
			Category c = getCategory(managerSource, tag);
			if (c != null) {
				for (int i = 0 ; i < managerSource.getComponents(c).size() ; i++) {
					modelDest.reestimateComponents(tag);
				}
			}
		}
	}

	private Category getCategory(FitComponentManager manager, int tag) {
		for (Category c : manager.getCategories()) {
			if (c.getId() == tag) {
				return c;
			}
		}
		return null;
	}

	private List<Integer> getMolCategoryTags(FitComponentManager managerSource) {
		List<Integer> tags = new ArrayList<>();
		for (Category category : managerSource.getCategories()) {
			int tag = category.getId();
			if (!AccessDataBase.getDataBaseConnection().getMolName(tag).equals("Not in Database")) {
				tags.add(tag);
			}
		}
		return tags;
	}

	private void copyCategory(Category cat, FitComponentManager src, FitComponentManager dst) {
		if (src.getCategories().contains(cat)) {
			for (FitAbstractComponent component : src.getComponents(cat)) {
				FitAbstractComponent clone = FitAbstractComponent.cloneComponent(component);
				dst.addAbstractComponent(cat, clone);
			}
		}
	}

	/**
	 * @return The model that is at the current index, or null if the index is
	 *         outside the bounds of the list. If the model manager is on gallery,
	 *         a general model that is the combination of all the other ones is returned
	 */
	public AdvancedFitModel getCurrentModel() {
		if (onGallery) {
			return combinedModel;
		}
		if (currentIndex < models.size()) {
			return models.get(currentIndex);
		} else {
			return null;
		}
	}

	/**
	 * Sets the onGallery value
	 *
	 * @param onGallery
	 */
	public void setOnGallery(boolean onGallery) {
		this.onGallery = onGallery;
		fireDataChanged(new ModelChangedEvent(CURRENT_MODEL_CHANGED_EVENT));
	}

	/**
	 * Sets the current index
	 *
	 * @param currentIndex
	 *            The current index to use
	 */
	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
		fireDataChanged(new ModelChangedEvent(CURRENT_MODEL_CHANGED_EVENT));
	}

	/**
	 * @return The last {@link AdvancedFitResult} that has been computed
	 */
	public AdvancedFitResult getLastResult() {
		return lastResult;
	}

	/**
	 * @return The number of models in the manager
	 */
	public int getNbModels() {
		return models.size();
	}

	/**
	 * Reset the ranges for the current model
	 */
	public void resetStudyRanges() {
		getCurrentModel().getParametersModel().resetStudyRange();
	}

	/**
	 * Reset the last studied range for the current model
	 */
	public void resetLastStudiedRange() {
		getCurrentModel().getParametersModel().resetLastStudiedRange();
	}

	/**
	 * Refreshes the source of every model in the manager.
	 */
	public void refreshInputs() {
		for (AdvancedFitModel model : models) {
			model.refreshInput();
		}
	}

	/**
	 * Converts the X parameters of every model to match the new axis of
	 * their data source
	 *
	 * @param source The source axis
	 */
	public void convertConfiguration(XAxisCassis source) {
		for (AdvancedFitModel model : models) {
			if (source instanceof XAxisVelocity){
				XAxisVelocity sourceClone;
				try {
					sourceClone = (XAxisVelocity) source.clone();
					sourceClone.setVlsr(model.getDataSource().getVlsr());
					sourceClone.setFreqRef(model.getDataSource().getFreqRef());
					model.convertConfiguration(sourceClone);
				} catch (CloneNotSupportedException e) {
					LOGGER.error("Clone error", e);
				}

			} else {
				model.convertConfiguration(source);
			}
		}
	}

	/**
	 * Converts the X parameters of the global model to match the new axis of
	 * their data source. Also change the bottom axis of the data source to
	 * match the destination unit.
	 *
	 * @param dst The destination axis
	 */
	public void convertGlobalConfiguration(XAxisCassis dst) {
		if (isOnGallery() && !UNIT.isVelocity(dst.getUnit())) {
			XAxisCassis source = combinedModel.getDataSource().getxAxis();
			combinedModel.getDataSource().getCurrentSeries().getSpectrum().setxAxisOrigin(dst);
			combinedModel.refreshInput();
			combinedModel.convertConfiguration(source, dst);
		}
	}

	public boolean isOnGallery() {
		return onGallery;
	}

	/**
	 * Initializes the logging parameters
	 *
	 * @param referenceView The JPanel used as reference to show the file choosers
	 * @param telescopeInterface The telescope interface
	 */
	public void initLogParameters(JPanel referenceView, TelescopeFitInterface telescopeInterface) {
		this.saveFit.initLogParameters(referenceView, telescopeInterface);
		fireDataChanged(new ModelChangedEvent(FIT_LOGGING_INITIALIZED_EVENT));
	}

	/**
	 * @return The name of the file in which to log the fit
	 */
	public String getLogFileName() {
		return this.saveFit.getLogFile() == null ? " --- " : this.saveFit.getLogFile().getName();
	}

	/**
	 * Sets if the saving will overwrite the current content
	 * of the log file or not
	 *
	 * @param appendLog True if we keep the current content of the file
	 */
	public void setAppending(boolean appending) {
		this.saveFit.setAppendLog(appending);
		fireDataChanged(new ModelChangedEvent(FIT_LOGGING_INITIALIZED_EVENT));
	}

	/**
	 * Sets if the saving will overwrite the current content
	 * of the log file or not
	 *
	 * @param appendLog True if we keep the current content of the file
	 */
	public boolean isAppending() {
		return this.saveFit.isAppendLog();
	}

	/**
	 * Sets if the manager must save automatically
	 * in the log file after fitting
	 *
	 * @param autoSave autoSave
	 */
	public void setAutoSave(boolean autoSave) {
		this.autoSave = autoSave;
		fireDataChanged(new ModelChangedEvent(FIT_AUTO_SAVE_EVENT));
	}

	/**
	 * @return True if the fit must be saved after each computation
	 */
	public boolean isAutoSave() {
		return autoSave;
	}

	/**
	 * @return True if the logging parameters have been initialized properly
	 */
	public boolean isSavingInitialized() {
		return this.saveFit.isInitialized();
	}

	/**
	 * Save the current fit model in the logging file
	 *
	 * @param lines The lines to save
	 * @param telescope The associated telescope
	 * @throws IOException If an error occurred while writing in the file
	 * @throws IllegalArgumentException If one of the components of the fit isn't usable by the rotational diagram
	 */
	public void saveCurrentFitModel(String telescope) throws IllegalArgumentException, IOException {
		List<LineDescription> lines = getCurrentModel().getDataSource().getAllLines();
		List<LineDescription> sameLines = AdvancedFitModel.getSameLines(lines);
		if (!sameLines.isEmpty()) {
			fireDataChanged(new ModelChangedEvent(AdvancedFitModel.DUPLICATED_LINES_EVENT, sameLines));
			lines = AdvancedFitModel.removeLinesWithCopy(lines);
		}
		FitToRotationalComputations computations = new FitToRotationalComputations(getCurrentModel(), lines);
		this.saveFit.saveLinesInFile(computations, telescope, isOnGallery());
		fireDataChanged(new ModelChangedEvent(FIT_LOGGING_INITIALIZED_EVENT));
	}

	/**
	 * Save the specified fit models in the logging file
	 *
	 * @param lines The lines to save, ordered in the same way as the models
	 * @param telescope The associated telescope
	 * @param indexes Indexes of the models to save
	 * @throws IOException If an error occurred while writing in the file
	 * @throws IllegalArgumentException If one of the components of the fit isn't usable by the rotational diagram, or if
	 * the number of lists of lines doesn't match the number of model to fit
	 */
	public void saveAllFitModels(List<List<LineDescription>> lines, String telescope, List<Integer> indexes) throws IllegalArgumentException, IOException {
		if (lines.size() != indexes.size()) {
			throw new IllegalArgumentException("The number of lines set doesn't match the number of models");
		}
		FitToRotationalComputations computations = new FitToRotationalComputations(models.get(indexes.get(0)), lines.get(0));
		this.saveFit.saveLinesInFile(computations, telescope, false);
		for (int i = 1 ; i < indexes.size() ; i++) {
			computations = new FitToRotationalComputations(models.get(indexes.get(i)), lines.get(i));
			this.saveFit.saveLinesInFile(computations, telescope, false);
		}
		fireDataChanged(new ModelChangedEvent(FIT_LOGGING_INITIALIZED_EVENT));
	}

	@Override
	public void performFit() {
		try {
			AdvancedFitResult result = getCurrentModel().performFit();
			this.lastResult = result;
			fireDataChanged(new ModelChangedEvent(FitActionsInterface.FIT_PERFORMED_EVENT, result));
		} catch (FitException e) {
			fireDataChanged(new ModelChangedEvent(FitActionsInterface.FIT_FAILED_EVENT, e));
		}
		if (autoSave) {
			saveFit();
		}
	}

	@Override
	public void performFitAll() {
		List<AdvancedFitResult> results = new ArrayList<>();
		AdvancedFitModel refModel = getCurrentModel();
		copyModel(refModel);
		try {
			for (int i = 0 ; i < models.size() ; i++) {
			 	results.add(null);
				if (galleryNavigationModel.getFilter().contains(i)) {
					AdvancedFitResult result = getModel(i).performFit();
					this.lastResult = result;
					results.set(i, result);
				}
			}
			fireDataChanged(new ModelChangedEvent(FitActionsInterface.FIT_ALL_PERFORMED_EVENT, results));
		} catch (FitException e) {
			fireDataChanged(new ModelChangedEvent(FitActionsInterface.FIT_FAILED_EVENT, e));
		}
		this.lastResult = results.get(currentIndex);
	}

	@Override
	public void performGlobalFit() {
		AdvancedFitResult advancedFitResult = null;
		try {

			XAxisCassis getxAxis = combinedModel.getDataSource().getxAxis();
			final FitParametersModel parametersModel = combinedModel.getParametersModel();

			parametersModel.resetStudyRange();
			for (int index = 0 ; index < models.size() ; index++) {
				if (galleryNavigationModel.getFilter().contains(index)) {
					AdvancedFitModel model = models.get(index);

					XAxisCassis getxAxis2 = model.getDataSource().getxAxis();
					final FitParametersModel parametersModel2 = model.getParametersModel();
					FitCurve studiedCurve = parametersModel2.getStudiedCurve();
					double [] x = studiedCurve.getX();
					double [] y = studiedCurve.getY();
					if (getxAxis2.isInverted() != getxAxis.isInverted()) {
						double [] tempY = new double [y.length];
						double [] tempX = new double [x.length];
						for (int i = 0; i < tempY.length; i++) {
							tempY[i] = y[tempY.length-i-1];
							tempX[i] = XAxisCassis.convert(x[tempY.length-i-1], getxAxis2, getxAxis);
						}
						y = tempY;
						x= tempX;
					}else {
						double [] tempX = new double [x.length];
						for (int i = 0; i < tempX.length; i++) {
							tempX[i] = XAxisCassis.convert(x[i], getxAxis2, getxAxis);
						}
						x= tempX;
					}


					FitCurve fitCurve = new FitCurve(x, y);
					parametersModel.addRange(fitCurve);
				}
			}


			advancedFitResult = combinedModel.performFit();
		} catch (FitException e) {
			fireDataChanged(new ModelChangedEvent(FitActionsInterface.FIT_FAILED_EVENT, e));
			return;
		}
		dispatchGlobalFitResults(advancedFitResult);
		if (autoSave) {
			saveFit();
		}
	}

	@Override
	public void computeGlobalConfiguration() {
		combinedModel.computeInitialGlobalConfiguration();
	}

	@Override
	public void selectLogFile() {
		fireDataChanged(new ModelChangedEvent(FitActionsInterface.LOG_FILE_SELECTED_EVENT));
	}

	@Override
	public void saveFit() {
		fireDataChanged(new ModelChangedEvent(FitActionsInterface.FIT_SAVED_EVENT));
	}

	@Override
	public void loadConfiguration(File file) {
		getCurrentModel().getParametersModel().loadConfiguration(file);
		fireDataChanged(new ModelChangedEvent(FitActionsInterface.CONFIGURATION_LOADED_EVENT));
	}

	@Override
	public void saveConfiguration(File file) {
		getCurrentModel().getParametersModel().saveConfiguration(file);
		fireDataChanged(new ModelChangedEvent(FitActionsInterface.CONFIGURATION_SAVED_EVENT));
	}

	@Override
	public void substractFit() {
		fireDataChanged(new ModelChangedEvent(FitActionsInterface.FIT_SUBSTRACTED_EVENT));
	}

	@Override
	public void divideFit() {
		fireDataChanged(new ModelChangedEvent(FitActionsInterface.FIT_DIVIDED_EVENT));
	}

	@Override
	public void overlayFit(boolean display) {
		fireDataChanged(new ModelChangedEvent(FitActionsInterface.OVERLAY_FIT_EVENT, display));
	}

	@Override
	public void overlayResidual(boolean display) {
		fireDataChanged(new ModelChangedEvent(FitActionsInterface.OVERLAY_RESIDUAL_EVENT, display));
	}

	@Override
	public void restoreOriginal() {
		getCurrentModel().getParametersModel().restoreOriginal();
		fireDataChanged(new ModelChangedEvent(FitActionsInterface.ORIGINAL_RESTORED_EVENT));
	}

	@Override
	public void undo() {
		getCurrentModel().getParametersModel().undo();
		fireDataChanged(new ModelChangedEvent(FitActionsInterface.UNDO_EVENT));
	}

	@Override
	public void redo() {
		getCurrentModel().getParametersModel().redo();
		fireDataChanged(new ModelChangedEvent(FitActionsInterface.REDO_EVENT));
	}

	@Override
	public void addCommand(ICommand command) {
		getCurrentModel().getParametersModel().addCommand(command);
		fireDataChanged(new ModelChangedEvent(FitActionsInterface.COMMAND_ADDED_EVENT));
	}

	@Override
	public void clearConfiguration() {
		getCurrentModel().getParametersModel().getManager().clearManager();
		if (isOnGallery()) {
			for (AdvancedFitModel model : models) {
				model.getParametersModel().getManager().clearManager();
			}
		}
	}

	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (event.getSource().equals(GalleryNavigationModel.GALLERY_NUMBER_UPDATE_EVENT)) {
			updateCombinedModel();
		} else if (event.getSource().equals(AdvancedFitModel.CATEGORIZED_COMPONENTS_EVENT)) {
			fireDataChanged(event);
		} else if (event.getSource().equals(AdvancedFitModel.VLSR_EVENT)) {
			fireDataChanged(event);
		}else if (event.getSource().equals(AdvancedFitModel.DUPLICATED_LINES_EVENT)) {
			fireDataChanged(event);
		}
	}

	public void updateCombinedModel() {
		if (models.size() > 0) {
			List<FitSourceInterface> sources = new ArrayList<>();
			for (int i : this.galleryNavigationModel.getFilter()) {
				if (i < models.size()) {
					sources.add(models.get(i).getDataSource());
				}
			}
			MultiGraphSource model = new MultiGraphSource(sources);
			if (combinedModel == null || (combinedModel != null && !model.equals(combinedModel.getDataSource()))) {
				if (this.combinedModel != null) {
					this.combinedModel.removeAllListener();
				}
				this.combinedModel = new MultiGraphModel(model);
				this.combinedModel.addModelListener(this);
				fireDataChanged(new ModelChangedEvent(CURRENT_MODEL_CHANGED_EVENT));
				AdvancedFitFrame.getFrame().displayModel(getCurrentModel());
			}
		}
	}

	private void dispatchGlobalFitResults(AdvancedFitResult advancedFitResult) {

		XAxisCassis xAxis = combinedModel.getDataSource().getxAxis();
		final FitComponent baseline = combinedModel.getBaseline();
		final FitParametersModel parametersModel = combinedModel.getParametersModel();
		final FitComponentManager globalManager = parametersModel.getManager();
		for (int i : galleryNavigationModel.getFilter()) {
			final FitParametersModel parametersModeli = models.get(i).getParametersModel();
			FitComponentManager manageri = parametersModeli.getManager();

			parametersModeli.setNbIterations(parametersModel.getNbIterations());
			parametersModeli.setOversampling(parametersModel.getOversampling());
			parametersModeli.setTolerance(parametersModel.getTolerance());

			manageri.selectFitStyle(globalManager.getSelectedFitStyle());
			manageri.clearManager();

			if (baseline != null) {
				manageri.addAbstractComponent(FitAbstractComponent.cloneComponent(baseline));
			}
			for (FitAbstractComponent comp : globalManager.getComponents()) {
				List<FitComponent> comps = extract(comp, models.get(i).getDataSource().getCurrentSeries().
									getSpectrum().getFrequenciesSignal());
				if (!comps.isEmpty()) {
					if (comps.size() == 1) {
						manageri.addAbstractComponent(globalManager.getCategory(comp), comps.get(0));
					} else {
						List<FitComponent> children = new ArrayList<>();
						for (int i1 = 1 ; i1 < comps.size() ; i1++) {
							children.add(comps.get(i1));
						}
						FitMultiComponent mult = new FitMultiComponent(comps.get(0), children);
						manageri.addAbstractComponent(globalManager.getCategory(comp), mult);
					}
				}
			}
			models.get(i).convertConfiguration(xAxis);
		}
		displayModels(advancedFitResult);
	}

	private List<FitComponent> extract(FitAbstractComponent comp, final double x[]) {
		List<FitComponent> components = extract(comp);
		Iterator<FitComponent> it = components.iterator();
		while (it.hasNext()) {
			FitComponent c = it.next();
			if (!isInSource(c, x)) {
				it.remove();
			}
		}
		return components;
	}

	private List<FitComponent> extract(FitAbstractComponent component) {
		List<FitComponent> components = new ArrayList<>();
		if (component.isMultiple()) {
			FitMultiComponent mult = (FitMultiComponent) component;
			components.add((FitComponent) FitAbstractComponent.cloneComponent(mult.getParent()));
			for (FitComponent c : mult.getChildren()) {
				FitComponent clone = (FitComponent) FitAbstractComponent.cloneComponent(c);
				for (int k = 0 ; k < c.getNbParameters() ; k++) {
					clone.setParameterConstraint(k, FitConstraintType.NONE);
				}
				components.add(clone);
			}
		} else if (!component.getComponentType().isBaseline()) {
			components.add((FitComponent) FitAbstractComponent.cloneComponent(component));
		}
		initFixedAndLimits(component, components);
		return components;
	}

	private void initFixedAndLimits(FitAbstractComponent component, List<FitComponent> components) {
		FitComponent ref = component.isMultiple() ? ((FitMultiComponent) component).getParent() : (FitComponent) component;
		for (FitComponent c : components) {
			for (int k = 0 ; k < c.getNbParameters() ; k++) {
				c.setParameterFixed(k, ref.getParameter(k).isFixed());
				c.setParameterLowLimit(k, ref.getParameter(k).getLowLimit());
				c.setParameterHighLimit(k, ref.getParameter(k).getHighLimit());
			}
		}
	}

	private boolean isInSource(FitComponent component, final double[] x) {
		double x0 = component.getParameter(FitParameterType.X0.getIndex()).getValue();
		x0 = combinedModel.getDataSource().getxAxis().convertToMHzFreq(x0);
		return x0 >= x[0] && x0 <= x[x.length - 1];
	}

	private void displayModels(final AdvancedFitResult advancedFitResult) {
		List<FitResultInterface> results = new ArrayList<>();
		for (int i = 0 ; i < models.size() ; i++) {
			results.add(null);
			FitComponentManager manager = models.get(i).getParametersModel().getManager();
			if (galleryNavigationModel.getFilter().contains(i) && !manager.getComponents().isEmpty()) {
				final FitParametersModel parametersModel = models.get(i).getParametersModel();
				FitSourceInterface dataSource = models.get(i).getDataSource();
				final FitCurve source = parametersModel.getSourceCurve();

				double[] waves = source.getX();
				int nbPoints = waves.length;

				final double[] newWaves = new double[nbPoints];
				final XAxisCassis xAxis = dataSource.getxAxis();
				XAxisCassis xAxisFrequency = XAxisCassis.getXAxisFrequency(UNIT.MHZ);
				if (xAxis.isInverted()!= xAxisFrequency.isInverted()) {
					for (int j = nbPoints-1; j >= 0; j--) {
						newWaves[nbPoints-j-1] = XAxisCassis.convert(waves[j], xAxis, xAxisFrequency);
					}
				} else {
					for (int j = 0; j < nbPoints; j++) {
						newWaves[j] = XAxisCassis.convert(waves[j], xAxis, xAxisFrequency);
					}
				}
				final double[] newWaveOversample = UtilArrayList.oversample(newWaves, parametersModel.getOversampling());

				FitResultInterface fitResultInterface = new FitResultInterface() {

					@Override
					public XYSpectrumSeries getResidualSeries() {
						final XYSpectrumSeries fitSeries = advancedFitResult.getResidualSeries();
						XYSpectrumSeries serie = computeFitSeriesWithNewX(fitSeries,
								newWaves, source.getVlsr(), source.getFreqRef(), xAxis);
						return serie;

					}

					@Override
					public XYSpectrumSeries getFitSeries() {
						final XYSpectrumSeries fitSeries = advancedFitResult.getFitSeries();
						XYSpectrumSeries serie = computeFitSeriesWithNewX(fitSeries,
								newWaveOversample, source.getVlsr(), source.getFreqRef(), xAxis);
						return serie;
					}

					@Override
					public List<XYSpectrumSeries> getCompoSeries() {
						final List<XYSpectrumSeries> compos = advancedFitResult.getCompoSeries();
						List<XYSpectrumSeries> res = new ArrayList<>();
						for (XYSpectrumSeries fitSeries : compos) {
							XYSpectrumSeries serie = computeFitSeriesWithNewX(fitSeries,
									newWaveOversample, source.getVlsr(), source.getFreqRef(), xAxis);

							res.add(serie);
						}

						return res;
					}
				};

				results.set(i, fitResultInterface);
			}
		}
		fireDataChanged(new ModelChangedEvent(FIT_GLOBAL_PERFORMED_EVENT, results));
	}
	/**
	 *
	 * @param fitSeries
	 * @param newWavesOverSampled
	 * @param vlsr
	 * @param freqRef
	 * @param xAxis
	 * @return
	 */
	private XYSpectrumSeries computeFitSeriesWithNewX(XYSpectrumSeries fitSeries,
			final double[] newWavesOverSampled, double vlsr, double freqRef, final XAxisCassis xAxis) {

		final CommentedSpectrum spectrum1 = fitSeries.getSpectrum();
		double y [] = new double[newWavesOverSampled.length];
		double[] intensities2 = spectrum1.getIntensities();
		double[] frequencies2 = spectrum1.getFrequenciesSignal();
		double newWavesMin = newWavesOverSampled[0];
		double newWavesMax = newWavesOverSampled[newWavesOverSampled.length-1];
		int cpt = 0;
		for (int j = 0; j < intensities2.length; j++) {
			if ((frequencies2[j]>= newWavesMin && frequencies2[j]<= newWavesMax) ||
				(Math.abs(frequencies2[j] - newWavesMin )<=1e-6 ) ||
				(Math.abs(frequencies2[j] - newWavesMax )<=1e-6 )){
				y[cpt] = intensities2[j];
				cpt++;
			}
		}
		String title = String.valueOf(fitSeries.getKey());
		CommentedSpectrum spectrum = new CommentedSpectrum(null, newWavesOverSampled, y, title);
		spectrum.setxAxisOrigin(xAxis);
		spectrum.setVlsr(vlsr);
		spectrum.setFreqRef(freqRef);
		XYSpectrumSeries serie = new XYSpectrumSeries(title, xAxis, fitSeries.getTypeCurve(), spectrum);
		serie.getConfigCurve().setColor(fitSeries.getConfigCurve().getColor());
		return serie;
	}

}
