/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit.advanced;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.fit.FitResult;
import eu.omp.irap.cassis.fit.util.FitCurve;
import eu.omp.irap.cassis.gui.plot.curve.TypeCurve;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import herschel.ia.numeric.Double1d;

/**
 * This class is used to make the link between the fit results from the module, and
 * the CASSIS objects used to display them
 * @author bastienkovac
 */
public class AdvancedFitResult implements FitResultInterface{

	private static final Logger LOGGER = LoggerFactory.getLogger(AdvancedFitResult.class);

	private FitResult result;
	private FitCurve source;
	private int oversampling;
	private XAxisCassis xAxis;

	private XYSpectrumSeries fitSeries;
	private XYSpectrumSeries residualSeries;
	private List<XYSpectrumSeries> compoSeries;
	private YAxisCassis yAxis;
	private YAxisCassis yAxisData;
	private List<CassisMetadata> metaDatas;
	private String nameOriginData;


	/**
	 * Constructor
	 *
	 * @param result The result given by the fit module
	 * @param source The source data, given by the fit module
	 * @param oversampling The oversampling factor
	 * @param xAxis The xAxis on which to display the curves
	 * @param metaDatas the metadata of the spectrum fitted
	 * @param yAxisCassis The yAxis on which to display the curves
	 * @param yAxisData The yAxis of the data fitted
	 * @param nameOriginData the name of the fitted curve
	 */
	public AdvancedFitResult(FitResult result, FitCurve source, int oversampling, XAxisCassis xAxis,
			YAxisCassis yAxisCassis, List<CassisMetadata> metaDatas, YAxisCassis yAxisData,
			String nameOriginData) {
		this.result = result;
		this.source = source;
		this.oversampling = oversampling;
		this.xAxis = xAxis;
		this.yAxis = yAxisCassis;
		this.metaDatas = metaDatas;
		this.yAxisData = yAxisData;
		this.nameOriginData = nameOriginData;
		computeResults();
	}

	/**
	 * @return The compound result as a series that can be displayed by CASSIS
	 */
	public XYSpectrumSeries getFitSeries() {
		return fitSeries;
	}

	/**
	 * @return The residual of the fit as a series that can be displayed by CASSIS
	 */
	public XYSpectrumSeries getResidualSeries() {
		return residualSeries;
	}

	/**
	 * @return The components results as a list of series that can be displayed by CASSIS
	 */
	public List<XYSpectrumSeries> getCompoSeries() {
		return compoSeries;
	}

	/**
	 * Computes the results obtained by the module
	 */
	private void computeResults() {
		FitCurve oversampledResult = result.getGeneralFitCurve(source, oversampling);
		FitCurve standardResult = result.getGeneralFitCurve(source, 1);
		List<FitCurve> oversampleCompos = result.getExtractedFitCurves(source, oversampling);
		computeArrays(standardResult, oversampledResult, oversampleCompos);
	}

	/**
	 * Create and compute the arrays that will be used as data for the created
	 * spectrums
	 *
	 * @param standardResult General fit result, without any oversampling
	 * @param oversampledResult General fit result, with the oversmapling set by the user
	 * @param oversampleCompos List of every individual components of the fit, with the oversampling
	 * set by the user
	 */
	private void computeArrays(FitCurve standardResult, FitCurve oversampledResult, List<FitCurve> oversampleCompos) {
		double[] xDataResampledMhz = oversampledResult.getX();
		double[] yDataResampled = oversampledResult.getY();
		if (xAxis.isInverted()) {
			computeInvertedArrays(xDataResampledMhz, yDataResampled, oversampleCompos, standardResult);
		} else {
			computeRegularArrays(xDataResampledMhz, yDataResampled, oversampleCompos, standardResult);
		}
	}

	/**
	 * Reverse the arrays to match the unit of the data source's x axis, and convert
	 * the x values to MHz from the right unit
	 *
	 * @param xResampled The x values, oversampled
	 * @param yResampled The y values, oversampled
	 * @param compos List of every individual components of the fit, with the oversampling
	 * set by the user
	 * @param standard General fit result, without any oversampling
	 */
	private void computeInvertedArrays(double[] xResampled, double[] yResampled, List<FitCurve> compos, FitCurve standard) {
		int nbCompos = compos.size();

		double[][] yCompo = new double[nbCompos][];
		Double1d xDataResampledMhz = new Double1d();

		for (int i = xResampled.length - 1 ; i >= 0 ; i--) {
			xDataResampledMhz.append(xAxis.convertToMHzFreq(xResampled[i]));
		}
		double[] x = Arrays.copyOf(xDataResampledMhz.getArray(), xDataResampledMhz.length());
		for (int j = 0 ; j < nbCompos ; j++) {
			double[] y = compos.get(j).getY();
			reverse(y);
			yCompo[j] = y;
		}
		reverse(yResampled);
		computeSeries(x, yResampled, yCompo, standard);
	}

	/**
	 * Reverse the given array, in time <code>O(n/2)</code>
	 *
	 * @param array Array to reverse
	 */
	private void reverse(double[] array) {
		double tmp;
		int i = 0;
		int j = array.length - 1;
		while (i < j) {
			tmp = array[j];
			array[j] = array[i];
			array[i] = tmp;
			j--;
			i++;
		}
	}

	/**
	 * Convert the x values to MHz from the x axis' unit, and
	 * compute the arrays.
	 *
	 * @param xResampled The x values, oversampled
	 * @param yResampled The y values, oversampled
	 * @param compos List of every individual components of the fit, with the oversampling
	 * set by the user
	 * @param standard General fit result, without any oversampling
	 */
	private void computeRegularArrays(double[] xResampled, double[] yResampled,  List<FitCurve> compos, FitCurve standard) {
		int nbCompos = compos.size();
		double[][] yCompo = new double[nbCompos][];

		for (int i = 0 ; i < xResampled.length ; i++) {
			xResampled[i] = xAxis.convertToMHzFreq(xResampled[i]);
		}
		for (int j = 0 ; j < nbCompos ; j++) {
			yCompo[j] = compos.get(j).getY();
		}
		computeSeries(xResampled, yResampled, yCompo, standard);
	}

	/**
	 * Compute all the series that constitute a fit from the arrays
	 *
	 * @param x The x values oversampled
	 * @param y The y values of the general result (oversampled)
	 * @param yCompo 2D Array with the y values oversampled of each component of the fit
	 * @param standard General fit result, without any oversampling
	 */
	private void computeSeries(double[] x, double[] y, double[][] yCompo, FitCurve standard) {
		try {
			this.fitSeries = computeFitSeries("Fit", x, y, TypeCurve.FIT);
			this.residualSeries = computeResidualSeries(standard);
		} catch (CloneNotSupportedException e) {
			LOGGER.error("Clone error, unable to compute series.", e);
		}
		this.compoSeries = computeCompoSeries(x, yCompo);
	}

	/**
	 * Create and returns a XYSpectrumSeries
	 *
	 * @param title Title to give to the series
	 * @param x x data of the series
	 * @param y y data of the series
	 * @param type Type of curve of the series
	 * @return The created series
	 * @throws CloneNotSupportedException If {@link XAxisCassis} doesn't implement clone()
	 */
	private XYSpectrumSeries computeFitSeries(String title, double[] x, double[] y, TypeCurve type) throws CloneNotSupportedException {
		CommentedSpectrum spectrum = new CommentedSpectrum(null, x, y, title);
		spectrum.getCassisMetadataList().addAll(this.metaDatas);
		spectrum.setxAxisOrigin(xAxis.clone());
		spectrum.setyAxis(this.yAxisData.clone());
		spectrum.setVlsr(source.getVlsr());
		spectrum.setFreqRef(source.getFreqRef());
		spectrum.getCassisMetadataList().add(new CassisMetadata(CassisMetadata.ORIGIN_DATA,
				this.nameOriginData, "name of the fitted curve", "none"));
		XYSpectrumSeries serie = new XYSpectrumSeries(title, xAxis.clone(), yAxis.clone(), type, spectrum);
		serie.getConfigCurve().setColor(Color.RED);
		return serie;
	}

	/**
	 * Create and returns a XYSpectrumSeries that is the residual of the fit
	 *
	 * @param standard General fit result, without any oversampling
	 * @return The residual series
	 * @throws CloneNotSupportedException If {@link XAxisCassis} doesn't implement clone()
	 */
	private XYSpectrumSeries computeResidualSeries(FitCurve standard) throws CloneNotSupportedException {
		double[] x = source.getX();
		double[] sourceY = source.getY();
		double[] fitY = standard.getY();
		double[] y = new double[sourceY.length];
		Double1d xDataMhz = new Double1d();

		if (xAxis.isInverted()) {
			for (int i = x.length - 1 ; i >= 0 ; i--) {
				xDataMhz.append(xAxis.convertToMHzFreq(x[i]));
			}

			double[] ySourceClone = source.getY();
			double[] yStandardSourceClone = standard.getY();
			for (int i = 0 ; i < x.length ; i++) {
				x[i] = xDataMhz.get(i);
				sourceY[i] = ySourceClone[y.length - i - 1];
				fitY[i] = yStandardSourceClone[fitY.length - i - 1];
			}
		} else {
			for (int i = 0 ; i < x.length ; i++) {
				xDataMhz.append(xAxis.convertToMHzFreq(x[i]));
			}
			for (int i = 0 ; i < x.length ; i++) {
				x[i] = xDataMhz.get(i);
			}
		}

		for (int i = 0 ; i < y.length ; i++) {
			y[i] = sourceY[i] - fitY[i];
		}
		XYSpectrumSeries series = computeFitSeries("Residual", x, y, TypeCurve.FIT_RESIDUAL);
		series.getConfigCurve().setColor(Color.ORANGE);
		return series;
	}

	/**
	 * Creates the series for each component of the fit
	 *
	 * @param x x values oversampled
	 * @param yCompo 2D Array with the y values oversampled of each component of the fit
	 * @return A list with series corresponding to every component of the fit
	 */
	private List<XYSpectrumSeries> computeCompoSeries(double[] x, double[][] yCompo) {
		List<XYSpectrumSeries> compoSeries = new ArrayList<>();
		Color[] colorCompo = new Color[] { Color.BLUE, Color.GREEN, Color.CYAN, Color.MAGENTA, Color.PINK, Color.YELLOW };
		Color color = null;
		for (int i = 0 ; i < yCompo.length ; i++) {
			color = colorCompo[i % colorCompo.length];
			CommentedSpectrum spectrum = new CommentedSpectrum(null, x, yCompo[i], "Fit " + (i + 1));
			spectrum.getCassisMetadataList().addAll(this.metaDatas);
			spectrum.setxAxisOrigin(xAxis);
			spectrum.setyAxis(this.yAxisData);
			spectrum.setFreqRef(source.getFreqRef());
			spectrum.setVlsr(source.getVlsr());
			spectrum.getCassisMetadataList().add(new CassisMetadata(CassisMetadata.ORIGIN_DATA,
					this.nameOriginData, "name of the fitted curve", "none"));

			XYSpectrumSeries serieCompo = new XYSpectrumSeries(result.getExtractedName(i), xAxis,
					yAxis, TypeCurve.FIT_COMPO, spectrum);
			serieCompo.setId(-1);
			serieCompo.getConfigCurve().setColor(color);
			compoSeries.add(serieCompo);
		}
		return compoSeries;
	}
}
