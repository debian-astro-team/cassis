/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit.advanced.history;

import java.util.Arrays;

import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.fit.FitParametersModel;
import eu.omp.irap.cassis.fit.history.ICommand;
import eu.omp.irap.cassis.fit.util.FitCurve;
import eu.omp.irap.cassis.gui.plot.curve.TypeCurve;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import eu.omp.irap.cassis.gui.plot.util.SpectrumFitPanelInterface;

/**
 * An ICommand used to compute the substract between a source curve and its fitted
 * one.
 * <br>
 * This action can be undone
 * @author bastienkovac
 */
public class FitSubstractCommand implements ICommand {

	private XYSpectrumSeries source;
	private XYSpectrumSeries residual;
	private SpectrumFitPanelInterface display;
	private FitParametersModel model;


	/**
	 * Constructor
	 *
	 * @param model The FitModel
	 * @param display The class that displays the result
	 * @param source The source curve
	 * @param residual The residual of the fit
	 */
	public FitSubstractCommand(FitParametersModel model, SpectrumFitPanelInterface display,
			XYSpectrumSeries source, XYSpectrumSeries residual) {
		this.model = model;
		this.display = display;
		this.source = source;
		this.residual = residual;
	}

	@Override
	public void executeCommand() {
		XYSpectrumSeries series = getNewResidualSeries();
		this.display.setDataSeries(series);
	}

	@Override
	public void unexecuteCommand() {
		this.display.setDataSeries(source);
		updateSourceCurve(source.getSpectrum().getFrequenciesSignal(), source.getSpectrum().getIntensities());
	}

	private XYSpectrumSeries getNewResidualSeries() {
		double[] x = residual.getSpectrum().getFrequenciesSignal();
		double[] y = residual.getSpectrum().getIntensities();
		updateSourceCurve(x, y);

		String title = (String) source.getKey();
		CommentedSpectrum substract = new CommentedSpectrum(null, x, y, title);
		substract.setVlsr(source.getSpectrum().getVlsr());
		substract.setxAxisOrigin(source.getSpectrum().getxAxisOrigin());
		substract.setFreqRef(source.getSpectrum().getFreqRef());
		substract.setLoFreq(source.getSpectrum().getLoFreq());
		substract.setListOfLines(source.getListOfLines());

		return new XYSpectrumSeries(title, source.getXAxis(), TypeCurve.DATA, substract);
	}

	private void updateSourceCurve(double[] x, double[] y) {
		FitCurve curve = model.getSourceCurve();
		double[] xConverted = Arrays.copyOf(x, x.length);
		for (int i = 0 ; i < xConverted.length ; i++) {
			xConverted[i] = source.getSpectrum().getxAxisOrigin().convertFromMhzFreq(x[i]);
		}
		curve.setX(xConverted);
		curve.setY(y);
	}

}
