/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit;

import herschel.ia.numeric.Double1d;
import herschel.ia.numeric.Int1d;
import herschel.ia.numeric.toolbox.basic.StdDev;
import herschel.ia.numeric.toolbox.fit.AbstractModel;
import herschel.ia.numeric.toolbox.fit.AmoebaFitter;
import herschel.ia.numeric.toolbox.fit.GaussModel;
import herschel.ia.numeric.toolbox.fit.IterativeFitter;
import herschel.ia.numeric.toolbox.fit.LevenbergMarquardtFitter;
import herschel.ia.numeric.toolbox.fit.LorentzModel;
import herschel.ia.numeric.toolbox.fit.PolynomialModel;
import herschel.ia.numeric.toolbox.fit.SincModel;
import herschel.ia.numeric.toolbox.fit.SineModel;
import herschel.ia.numeric.toolbox.fit.VoigtModel;

import java.util.Arrays;
import java.util.LinkedList;

import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.events.ListenerManager;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;

/**
 * @author glorian
 */
public class FitModel extends ListenerManager {

	public static final String FIT_STYLE_EVENT = "fitStyle";
	public static final String NB_ITERATIONS_EVENT = "nbIterations";
	public static final String OVERSAMPLING_FIT_EVENT = "oversamplingFit";
	public static final String TOLERANCE_FIT_EVENT= "tolerance";

	private static final double FIT_LIMIT_TOLERANCE = 1e-3;
	private Double rms = Double.NaN;
	private int nbIterations;
	private int oversamplingFit;
	private FitStyleEnum fitStyle = FitStyleEnum.LEVENBERG;
	private XAxisCassis xAxisCassis = XAxisCassis.getXAxisUnknown();
	private double tolerance;


	/**
	 * Compute the RMS.
	 *
	 * @param xdatas The X values.
	 * @param ydatasResidual The Y values.
	 * @param param The parameters.
	 * @param val The value of the polynomial model.
	 * @return the value of RMS
	 */
	public double[] computeRms(Double1d xdatas, Double1d ydatasResidual, Double1d param, int val) {
		AbstractModel model = new PolynomialModel(val);
		model = computeFit(xdatas, ydatasResidual, fitStyle, nbIterations,
				tolerance,model);

		Double1d yPoly = model.result(xdatas);
		Double1d y = new Double1d();
		for (int cpt = 0; cpt < yPoly.getSize(); cpt++) {
			y.append(ydatasResidual.get(cpt) - yPoly.get(cpt));
		}
		double[] computedRms = { StdDev.FUNCTION.calc(y) };
		this.rms = computedRms[0];
		return computedRms;
	}

	public AbstractModel computeFit(Double1d xdatas, Double1d ydatas,
			FitStyleEnum fitStyle, int nbIterations, double tolerance, AbstractModel abstractModel) {

		IterativeFitter fitter = getIterativeFitter(abstractModel, fitStyle, xdatas);
		fitter.setTolerance(tolerance);
		fitter.setMaxIterations(nbIterations);
		fitter.fit(ydatas);
		fitter.getStandardDeviation();
		return abstractModel;
	}

	public AbstractModel buildAbstractModel(LinkedList<FittingItemModel> linkedListFittingItem, FitStyleEnum fitStyle) {
		FittingItemModel current = linkedListFittingItem.get(0);
		Double1d lowLimits = new Double1d();
		Double1d highLimits = new Double1d();
		Int1d fixeIndiceParam = new Int1d();

		int offset = 0;
		AbstractModel abstractModel = getModel(current);
		if (FitStyleEnum.AMOEBA.equals(fitStyle)) {
			setAmoebaLimits(current, lowLimits, highLimits);
		}
		else if (FitStyleEnum.LEVENBERG.equals(fitStyle)) {
			setLevenbergLimits(current, fixeIndiceParam, offset);
		}

		offset = abstractModel.getParameters().length();
		for (int i = 1; i < linkedListFittingItem.size(); i++) {

			current = linkedListFittingItem.get(i);
			AbstractModel model = getModel(linkedListFittingItem.get(i));

			if (FitStyleEnum.AMOEBA.equals(fitStyle)) {
				setAmoebaLimits(current, lowLimits, highLimits);
			}
			else if (FitStyleEnum.LEVENBERG.equals(fitStyle)) {
				setLevenbergLimits(current, fixeIndiceParam, offset);
			}
			abstractModel.addModel(model);
			offset = abstractModel.getParameters().length();
		}

		if (fitStyle == FitStyleEnum.AMOEBA) {
			abstractModel.setLimits(lowLimits, highLimits);
		}
		else {
			abstractModel.keepFixed(fixeIndiceParam);
		}
		return abstractModel;
	}

	/**
	 * @param abstractModel
	 * @param fitStyle
	 * @param xdatas
	 * @return the IterativeFitter
	 */
	private IterativeFitter getIterativeFitter(AbstractModel abstractModel, FitStyleEnum fitStyle, Double1d xdatas) {
		return FitStyleEnum.AMOEBA.equals(fitStyle) ? new AmoebaFitter(xdatas, abstractModel)
				: new LevenbergMarquardtFitter(xdatas, abstractModel);
	}

	/**
	 * @param item
	 * @param style
	 *
	 * @return an AbstractModel
	 */
	private AbstractModel getModel(FittingItemModel item) {
		AbstractModel model = null;

		FitModelEnum modelType = item.getModelName();
		if (FitModelEnum.POLY.equals(modelType)) {
			int valpoly = (int) item.getParametersValues()[0];
			model = new PolynomialModel(valpoly);
		}
		else if (FitModelEnum.GAUSS.equals(modelType)) {
			model = new GaussModel();
			Double1d val = new Double1d(item.getParametersValues());
			model.setParameters(val);
		}
		else if (FitModelEnum.LORENTZ.equals(modelType)) {
			model = new LorentzModel();
			Double1d val = new Double1d(item.getParametersValues());
			model.setParameters(val);
		}
		else if (FitModelEnum.SIN.equals(modelType)) {
			model = new SineModel();
			((SineModel) model).setMixedModel(new Int1d(new int[] { 1, 2 }));
		}
		else if (FitModelEnum.VOIGT.equals(modelType)) {
			model = new VoigtModel();
			Double1d val = new Double1d(item.getParametersValues());
			model.setParameters(val);
		}
		else if (FitModelEnum.SINC.equals(modelType)) {
			model = new SincModel();
			Double1d val = new Double1d(item.getParametersValues());
			model.setParameters(val);
		}

		return model;
	}

	private void setLevenbergLimits(FittingItemModel item, Int1d fixeIndiceParam, int offset) {
		Int1d test = new Int1d();
		for (int i = 0; i < item.getParametersValues().length; i++) {
			if (item.getParametersBlocked()[i]) {
				fixeIndiceParam.append(offset + i);
				test.append(i);
			}
		}
	}

	private void setAmoebaLimits(FittingItemModel item, Double1d lowLimits, Double1d highLimits) {
		if (FitModelEnum.POLY.equals(item.getModelName())) {
			int valpoly = (int) item.getParametersValues()[0];
			for (int cpt = 0; cpt < valpoly + 1; cpt++) {
				lowLimits.append(0.);
				highLimits.append(0.);
			}
		} else {

			for (int i = 0; i < item.getParametersValues().length; i++) {
				if (item.getParametersBlocked()[i]) {
					double val = item.getParametersValues()[i];
					lowLimits.append(val - val * FIT_LIMIT_TOLERANCE);
					highLimits.append(val + val * FIT_LIMIT_TOLERANCE);
				} else {
					lowLimits.append(0.);
					highLimits.append(0.);
				}
			}
		}
	}

	/**
	 * Return the rms.
	 *
	 * @return the rms
	 */
	public final Double getRms() {
		return rms;
	}

	/**
	 * Compute the X offset.
	 *
	 * @param xdatas The X values.
	 * @return the offset computed
	 */
	public static double computeXOffset(Double1d xdatas) {
		double min = Math.min(xdatas.get(0), xdatas.get(xdatas.getSize() - 1));
		double max = Math.max(xdatas.get(0), xdatas.get(xdatas.getSize() - 1));
		double offset = 0.;
		if (!(min < 0 && max > 0))
			offset = -((max - min) / 2) - min;
		return offset;
	}

	public static LinkedList<AbstractModel> extractCompo(LinkedList<FittingItem> activeListFittingItem,
			AbstractModel abstractModel) {
		int offset = 0;

		LinkedList<AbstractModel> listModel = new LinkedList<>();

		for (int i = 0; i < activeListFittingItem.size(); i++) {
			FittingItem item = activeListFittingItem.get(i);
			Double1d param = new Double1d();
			Double1d paramDev = new Double1d();

			int nbValue = 0;
			if (FitModelEnum.POLY.equals(item.getModelName())) {
				nbValue = (int) item.getParametersValues()[0] + 1;
			}
			else
				nbValue = item.getParameters().size();
			for (int j = 0; j < nbValue; j++) {
				param.append(abstractModel.getParameters(j + offset));
				paramDev.append(abstractModel.getStandardDeviations().get(j + offset));
			}

			AbstractModel model = null;

			FitModelEnum modelType = item.getModelName();
			if (FitModelEnum.POLY.equals(modelType)) {
				double val = item.getParametersValues()[0];
				model = new PolynomialModel((int) val);
			}
			else if (FitModelEnum.GAUSS.equals(modelType)) {
				model = new GaussModel();
			}
			else if (FitModelEnum.LORENTZ.equals(modelType)) {
				model = new LorentzModel();
			}
			else if (FitModelEnum.SIN.equals(modelType)) {
				model = new SineModel();
				((SineModel) model).setMixedModel(new Int1d(new int[] { 1, 2 }));
			}
			else if (FitModelEnum.VOIGT.equals(modelType)) {
				model = new VoigtModel();
			}
			else if (FitModelEnum.SINC.equals(modelType)) {
				model = new SincModel();
			}
			else {// default
				model = new GaussModel();
			}

			model.setParameters(param);
			model.setStandardDeviations(paramDev);

			offset += nbValue;
			listModel.add(model);

		}
		return listModel;
	}

	public int getCoefMulti(double[] intensity) {
		double[] copyIntensity = Arrays.copyOf(intensity, intensity.length);
		Arrays.sort(copyIntensity);
		double valMedian = copyIntensity[intensity.length / 2];
		int result = (int) Math.log10(valMedian);
		if (result <= -2 || result >= 2) {
			return result;
		}
		else
			return 1;
	}

	public static double computeXMult(Double1d xdatas) {
		double min = Math.min(xdatas.get(0), xdatas.get(xdatas.getSize() - 1));
		double offset = 1.;
		if (Math.abs(min) < 1)
			offset = Math.abs(1 / min) * 10;
		return offset;
	}

	public XAxisCassis getXAxis() {
		return xAxisCassis;
	}

	public void setXAxis(XAxisCassis xAxis) {
		this.xAxisCassis = xAxis;
	}

	public int getNbIterations() {
		return nbIterations;
	}

	public void setNbIterations(int nbIterations) {
		this.nbIterations = nbIterations;
		fireDataChanged(new ModelChangedEvent(NB_ITERATIONS_EVENT, nbIterations));
	}

	/**
	 * @return the oversamplingFit
	 */
	public int getOversamplingFit() {
		return oversamplingFit;
	}

	/**
	 * @param oversamplingFit the oversamplingFit to set
	 */
	public void setOversamplingFit(int oversamplingFit) {
		this.oversamplingFit = oversamplingFit;
		fireDataChanged(new ModelChangedEvent(OVERSAMPLING_FIT_EVENT, oversamplingFit));
	}

	/**
	 * @return the fitStyle
	 */
	public FitStyleEnum getFitStyle() {
		return fitStyle;
	}

	/**
	 * @param fitStyle the fitStyle to set
	 */
	public void setFitStyle(FitStyleEnum fitStyle) {
		this.fitStyle = fitStyle;
		fireDataChanged(new ModelChangedEvent(FIT_STYLE_EVENT, fitStyle));
	}

	public void setTolerance(double tolerance) {
		this.tolerance = tolerance;
		fireDataChanged(new ModelChangedEvent(TOLERANCE_FIT_EVENT, tolerance));

	}

	/**
	 * @return the oversamplingFit
	 */
	public double getTolerance() {
		return tolerance;
	}


}
