/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit.save;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.gui.fit.FittingItem;
import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassisCollection;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import eu.omp.irap.cassis.gui.plot.line.CassisPlot;
import eu.omp.irap.cassis.gui.plot.util.InterValMarkerCassis;


public class SaveFit implements SaveFitInterface {

	private static final Logger LOGGER = LoggerFactory.getLogger(SaveFit.class);

	private File logFile;
	private boolean appendLog;


	public SaveFit() {
		this.appendLog = false;
	}

	@Override
	public void setLogParameters(File selectedFile, boolean append) {
		this.logFile = selectedFile;
		this.appendLog = append;
	}

	/**
	 * @return the logFile
	 */
	@Override
	public final File getLogFile() {
		return logFile;
	}

	@Override
	public void saveAllLineAnalysisInCurrentFile(
			LinkedList<FittingItem> fittingItem) {
		try (BufferedWriter fileResult = new BufferedWriter(new FileWriter(logFile, appendLog))) {
			// write in the end of the existing file
			if (!appendLog) {
				fileResult
						.write("Comp\tProfile\tDegree\tRms\tIo\tIoErr\tXo\tXoErr\tFWHM_G\tFWHM_GErr\tFWHM_L\tFWHM_LErr");
				fileResult.newLine();
				fileResult.flush();
			}

			int nbFitCompo = fittingItem.size();
			int numComp = 0;
			for (int cpt = 0; cpt < nbFitCompo; cpt++) {
				if (fittingItem.get(cpt).isActive()) {
					fileResult.write(String.valueOf(numComp + 1) + "\t"
							+ fittingItem.get(cpt).getModelName() + "\t");
					String param = "";
					switch (fittingItem.get(cpt).getModel()) {
					case POLY:
						param = String.valueOf(fittingItem.get(cpt)
								.getParameters().get(0).getValue()
								+ "\t");
						param += String.valueOf(fittingItem.get(cpt)
								.getParameters().get(0).getStdDev()
								+ "\t" + "\t" + "\t" + "\t");
						break;

					case SINC:
					case GAUSS:
						param += "\t\t";
						param += String.valueOf(fittingItem.get(cpt)
								.getParameters().get(0).getValue()
								+ "\t");
						param += String.valueOf(fittingItem.get(cpt)
								.getParameters().get(0).getStdDev()
								+ "\t");
						param += String.valueOf(fittingItem.get(cpt)
								.getParameters().get(1).getValue()
								+ "\t");
						param += String.valueOf(fittingItem.get(cpt)
								.getParameters().get(1).getStdDev()
								+ "\t");
						param += String.valueOf(fittingItem.get(cpt)
								.getParameters().get(2).getValue()
								+ "\t");
						param += String.valueOf(fittingItem.get(cpt)
								.getParameters().get(2).getStdDev()
								+ "\t");
						param += "\t\t";
						break;

					case LORENTZ:
						param += "\t\t";
						param += String.valueOf(fittingItem.get(cpt)
								.getParameters().get(0).getValue()
								+ "\t");
						param += String.valueOf(fittingItem.get(cpt)
								.getParameters().get(0).getStdDev()
								+ "\t");
						param += String.valueOf(fittingItem.get(cpt)
								.getParameters().get(1).getValue()
								+ "\t");
						param += String.valueOf(fittingItem.get(cpt)
								.getParameters().get(1).getStdDev()
								+ "\t");
						param += "\t\t";
						param += String.valueOf(fittingItem.get(cpt)
								.getParameters().get(2).getValue()
								+ "\t");
						param += String.valueOf(fittingItem.get(cpt)
								.getParameters().get(2).getStdDev()
								+ "\t");
						break;
					case VOIGT:
						param += "\t\t";
						param += String.valueOf(fittingItem.get(cpt)
								.getParameters().get(0).getValue()
								+ "\t");
						param += String.valueOf(fittingItem.get(cpt)
								.getParameters().get(0).getStdDev()
								+ "\t");
						param += String.valueOf(fittingItem.get(cpt)
								.getParameters().get(1).getValue()
								+ "\t");
						param += String.valueOf(fittingItem.get(cpt)
								.getParameters().get(1).getStdDev()
								+ "\t");
						param += String.valueOf(fittingItem.get(cpt)
								.getParameters().get(2).getValue()
								+ "\t");
						param += String.valueOf(fittingItem.get(cpt)
								.getParameters().get(2).getStdDev()
								+ "\t");
						param += String.valueOf(fittingItem.get(cpt)
								.getParameters().get(3).getValue()
								+ "\t");
						param += String.valueOf(fittingItem.get(cpt)
								.getParameters().get(3).getStdDev()
								+ "\t");
						break;
					default:
						break;
					}

					fileResult.write(param);

					fileResult.newLine();
					fileResult.flush();
				}
				numComp++;
			}
		} catch (IOException ioe) {
			LOGGER.error("Error while saving file", ioe);
		}
		if (!appendLog)
			appendLog = true;
	}

	@Override
	public void saveAllLineAnalysisInCurrentFile(LinkedList<FittingItem> fittingItem,
			List<InterValMarkerCassis> listMarker, CassisPlot cassisPlot, String telescope, Double rms,
			XYSpectrumSeries currentDataCurve, XYSeriesCassisCollection fitCurves) throws IOException {
		// Unused here.
	}

}
