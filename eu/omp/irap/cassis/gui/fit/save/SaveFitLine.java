/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit.save;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.LinkedList;
import java.util.List;

import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.Telescope;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.database.access.MoleculeDescriptionDB;
import eu.omp.irap.cassis.gui.fit.FitModelEnum;
import eu.omp.irap.cassis.gui.fit.FitOperation;
import eu.omp.irap.cassis.gui.fit.FittingItem;
import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassisCollection;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import eu.omp.irap.cassis.gui.plot.line.CassisPlot;
import eu.omp.irap.cassis.gui.plot.util.InterValMarkerCassis;
import eu.omp.irap.cassis.properties.Software;

public class SaveFitLine implements SaveFitInterface {

	private File logFile;
	private boolean appendLog;

	private static DecimalFormat deltaFormat;
	private static DecimalFormat veloFormat;
	private static DecimalFormat freqFormat;
	private static DecimalFormat intensityFormat;
	private static DecimalFormat aijFormat;
	static {
		DecimalFormatSymbols ds = new DecimalFormatSymbols();
		ds.setDecimalSeparator('.');

		deltaFormat = new DecimalFormat("###0.0000", ds);// 8c
		veloFormat = new DecimalFormat("#####0.000", ds);// 10c
		freqFormat = new DecimalFormat("######0.000", ds);// 11c
		intensityFormat = new DecimalFormat("##0.0000", ds);// 8c
		aijFormat = new DecimalFormat("0.##E0", ds);
	}

	public SaveFitLine() {
		this.appendLog = false;
	}

	@Override
	public void saveAllLineAnalysisInCurrentFile(LinkedList<FittingItem> fittingItem,
			List<InterValMarkerCassis> listMarker, CassisPlot cassisPlot,
			String telescope, Double rms, XYSpectrumSeries currentDataCurve,
			XYSeriesCassisCollection fitCurves) throws IOException {


		LineDescription currentLine = cassisPlot.getMainLine();
		String yUnit = cassisPlot.getYAxisCassis().getUnit().getValString();
		String rmsUnit = "K".equals(yUnit) ? "mK" : yUnit;

		String telescopePath;
		String telescopeName = Telescope.getNameStatic(telescope);

		if (telescope.length() == telescopeName.length()) {
			telescopePath = new File(Software.getTelescopePath()).getPath();
		} else {
			telescopePath = telescope.substring(0, telescope.length() - telescopeName.length());
		}

		try (BufferedWriter fileResult = new BufferedWriter(new FileWriter(logFile, appendLog));
			 BufferedWriter fileHumanResult = new BufferedWriter(new FileWriter(logFile.getAbsolutePath()+".txt", appendLog))) {
			// write in the end of the existing file
			if (!appendLog) {
				headerFile(cassisPlot, rmsUnit, fileResult);
				headerFile(cassisPlot, rmsUnit, fileHumanResult);
			}

			int nbFitCompo = fittingItem.size();
			int numComp = 0;
			double vlsrData = cassisPlot.getVlsrData();
			for (int cpt = 0; cpt < nbFitCompo; cpt++) {
				if (fittingItem.get(cpt).isActive() && FitModelEnum.isLine(fittingItem.get(cpt).getModelName())) {
					FittingItem crtFitItem = fittingItem.get(cpt);
					XYSpectrumSeries fitSerie = (XYSpectrumSeries)fitCurves.getSeries(cpt);
					writeData(listMarker, rms, currentDataCurve, currentLine, telescopeName,
							telescopePath, fileResult, numComp, vlsrData, crtFitItem, fitSerie);
					writeHumanData(listMarker, rms, currentDataCurve, currentLine, telescopeName,
							telescopePath, fileHumanResult, numComp, vlsrData, crtFitItem, fitSerie);
				}
				numComp++;
			}
		}

		if (!appendLog) {
			appendLog = true;
		}
	}

	private void writeData(List<InterValMarkerCassis> listMarker, Double rms,
			XYSpectrumSeries currentDataCurve, LineDescription currentLine, String telescopeName,
			String telescopePath, BufferedWriter fileResult, int numComp, double vlsrData,
			FittingItem crtFitItem, XYSpectrumSeries fitSerie) throws IOException {
		fileResult.write(getLine(listMarker, rms, currentDataCurve, currentLine,
				telescopeName, telescopePath, numComp, vlsrData, crtFitItem,
				fitSerie));
		fileResult.newLine();
		fileResult.flush();
	}

	private void writeHumanData(List<InterValMarkerCassis> listMarker, Double rms,
			XYSpectrumSeries currentDataCurve, LineDescription currentLine, String telescopeName,
			String telescopePath, BufferedWriter fileResult, int numComp, double vlsrData,
			FittingItem crtFitItem, XYSpectrumSeries fitSerie) throws IOException {
		fileResult.write(getHumanLine(listMarker, rms, currentDataCurve, currentLine,
				telescopeName, telescopePath, numComp, vlsrData, crtFitItem,
				fitSerie));
		fileResult.newLine();
		fileResult.flush();
	}

	private void headerFile(CassisPlot cassisPlot, String rmsUnit, BufferedWriter fileResult)
			throws IOException {
		fileResult.write("version=2");
		fileResult.newLine();
		fileResult.write("id\tNumCompo\tSpecies\tQuantumNumbers\tFrequency"
				+ "\tEup\tGup\tAij\tFitFreq\tDeltaFitFreq\tVo\tdeltaVo"
				+ "\tFWHM_G\tdeltaFWHM_G\tFWHM_L\tdeltaFWHM_L\tIntensity"
				+ "\tdeltaIntensity\tFitFlux\tdeltaFitFlux\tFreq.IntensityMax"
				+ "\tV.IntensityMax\tFWHM\tIntensityMax\tFlux1stMom"
				+ "\tdeltaFlux1stMom\trms\tdeltaV\tCal\tSize"
				+ "\tTelescopePath\tTelescopeName");
		fileResult.newLine();

		fileResult.write(getLineUnit(
//						cassisPlot.getxAxisCassis().getUnit().getValString(),
				XAxisCassis.getXAxisFrequency(UNIT.MHZ).getUnit().getValString(),
				cassisPlot.getXAxisVelocity().getUnit().getValString(),
				cassisPlot.getYAxisCassis().getUnit().getValString(),
				cassisPlot.getYAxisCassis().getUnit().getValString() + '.' + cassisPlot.getXAxisVelocity().getUnit().getValString(),
				rmsUnit));
		fileResult.newLine();
		fileResult.flush();
	}

	private String getLine(List<InterValMarkerCassis> listMarker, Double rms,
			XYSpectrumSeries currentDataCurve, LineDescription currentLine,
			String telescopeName, String telescopePath, int numComp, double vlsrData,
			FittingItem crtFitItem, XYSpectrumSeries fitSerie) {
		StringBuilder sb = new StringBuilder();
		sb.append(currentLine.getMolTag()).append('\t'); // id // TODO wrong avec multiTag ?
		sb.append(String.valueOf(numComp + 1)).append('\t'); // NumCompo
		sb.append(currentLine.getMolName()).append('\t'); // Species // TODO wrong avec multiSpecies ???
		sb.append(MoleculeDescriptionDB.constitueLine(currentLine.getQuanticN())).append('\t'); // QuantumNumbers
		sb.append(currentLine.getObsFrequency()).append('\t'); // Frequency // TODO  unit... ... wrong value ???
		sb.append(currentLine.getEUpK()).append('\t'); // eup
		sb.append(currentLine.getGu()).append('\t'); // Gup
		sb.append(currentLine.getAij()).append('\t'); // Aij

		 // FitFreq & deltaFitFreq // TODO unit..
		sb.append(FitOperation.getFitFreqAndDeltaFitFreq(
				crtFitItem,
				currentLine.getObsFrequency(),
				vlsrData)).append('\t');

		sb.append(FitOperation.getVelocity(crtFitItem)).append('\t'); // Vo // TODO unit
		sb.append(FitOperation.getDeltaVelocity(crtFitItem)).append('\t'); // deltaVo // TODO unit

		// FWHM_G, deltaFWHM_G, FWHM_L, deltaFWHM_L
		sb.append(FitOperation.getFwhmGAndL(crtFitItem)).append('\t');

		sb.append(FitOperation.getIntensity(crtFitItem)).append('\t'); // Intensity // TODO unit
		sb.append(FitOperation.getDeltaIntensity(crtFitItem)).append('\t'); // deltaIntensity // TODO unit

		// FitFlux && deltaFitFlux // TODO unit & deltaFitFlux tjs à 0 ????
		sb.append(FitOperation.getFitFluxAndDeltaFitFlux(
				fitSerie, currentLine.getObsFrequency(),
				vlsrData)).append('\t');

		// Freq.IntensityMax & V.IntensityMax & FWHM & Flux1stMom & deltaFlux1stMom
		sb.append(FitOperation.getFirstMomentLog(currentDataCurve,
				currentLine.getObsFrequency(), listMarker,
				vlsrData)).append('\t');

		sb.append(FitOperation.getRms(rms)).append('\t'); // rms // TODO unit?
		sb.append(FitOperation.getDeltaV(listMarker, currentDataCurve)).append('\t'); // deltaV
		sb.append(0.0).append('\t'); // Cal // TODO use real value instead of 0.
		sb.append(0.0).append('\t'); // Size // TODO use real value instead of 0.
		String correctedTelescopePath = telescopePath.endsWith(File.separator) ?
				telescopePath : telescopePath + File.separator;
		sb.append(correctedTelescopePath).append('\t');
		sb.append(telescopeName);
		return sb.toString();
	}


	private String getHumanLine(List<InterValMarkerCassis> listMarker, Double rms,
			XYSpectrumSeries currentDataCurve, LineDescription currentLine,
			String telescopeName, String telescopePath, int numComp, double vlsrData,
			FittingItem crtFitItem, XYSpectrumSeries fitSerie) {

		StringBuilder sb = new StringBuilder();
		sb.append(currentLine.getMolTag()).append('\t'); // id // TODO wrong avec multiTag ?
		sb.append(String.valueOf(numComp + 1)).append('\t'); // NumCompo
		sb.append(currentLine.getMolName()).append('\t'); // Species // TODO wrong avec multiSpecies ???
		sb.append(MoleculeDescriptionDB.constitueLine(currentLine.getQuanticN())).append('\t'); // QuantumNumbers
		sb.append(freqFormat.format(currentLine.getObsFrequency())).append('\t'); // Frequency // TODO  unit... ... wrong value ???
		sb.append(intensityFormat.format(currentLine.getEUpK())).append('\t'); // eup
		sb.append(currentLine.getGu()).append('\t'); // Gup
		sb.append(aijFormat.format(currentLine.getAij())).append('\t'); // Aij

		 // FitFreq & deltaFitFreq // TODO unit..
		sb.append(FitOperation.getFitFreqAndDeltaFitFreq(
				crtFitItem,
				currentLine.getObsFrequency(),
				vlsrData, true)).append('\t');

		sb.append(veloFormat.format(FitOperation.getVelocity(crtFitItem))).append('\t'); // Vo // TODO unit
		sb.append(deltaFormat.format(FitOperation.getDeltaVelocity(crtFitItem))).append('\t'); // deltaVo // TODO unit

		// FWHM_G, deltaFWHM_G, FWHM_L, deltaFWHM_L
		sb.append(FitOperation.getHumanFwhmGAndL(crtFitItem)).append('\t');

		sb.append(intensityFormat.format(FitOperation.getIntensity(crtFitItem))).append('\t'); // Intensity // TODO unit
		sb.append(intensityFormat.format(FitOperation.getDeltaIntensity(crtFitItem))).append('\t'); // deltaIntensity // TODO unit

		// FitFlux && deltaFitFlux // TODO unit & deltaFitFlux tjs à 0 ????
		sb.append(FitOperation.getFitFluxAndDeltaFitFlux(
				fitSerie, currentLine.getObsFrequency(),
				vlsrData, true)).append('\t');

		// Freq.IntensityMax & V.IntensityMax & FWHM & Flux1stMom & deltaFlux1stMom
		sb.append(FitOperation.getFirstMomentLog(currentDataCurve,
				currentLine.getObsFrequency(), listMarker,
				vlsrData, true)).append('\t');

		sb.append(intensityFormat.format(FitOperation.getRms(rms))).append('\t'); // rms // TODO unit?
		sb.append(deltaFormat.format(FitOperation.getDeltaV(listMarker, currentDataCurve))).append('\t'); // deltaV
		sb.append(0.0).append('\t'); // Cal // TODO use real value instead of 0.
		sb.append(0.0).append('\t'); // Size // TODO use real value instead of 0.
		String correctedTelescopePath = telescopePath.endsWith(File.separator) ?
				telescopePath : telescopePath + File.separator;
		sb.append(correctedTelescopePath).append('\t');
		sb.append(telescopeName);
		return sb.toString();
	}

	@Override
	public void setLogParameters(File selectedFile, boolean append) {
		this.logFile = selectedFile;
		this.appendLog = append;
	}

	/**
	 * @return the logFile
	 */
	@Override
	public final File getLogFile() {
		return logFile;
	}

	@Override
	public void saveAllLineAnalysisInCurrentFile(LinkedList<FittingItem> fittingItem) {
		// Not used here.
	}

	private static String getLineUnit(String unitX, String unitVo, String unitIntensity, String unitFitFlux, String unitRms) {
		StringBuilder sb = new StringBuilder();
		sb.append("None").append('\t'); //id
		sb.append("None").append('\t'); // NumCompo
		sb.append("None").append('\t'); // Species
		sb.append("None").append('\t'); // QuantumNumbers
		sb.append(unitX).append('\t'); // Frequency 				// TODO peut changer? => Unit X > Si Velo deux axes demander
		sb.append("K").append('\t'); // Eup
		sb.append("None").append('\t'); // Gup
		sb.append("s-1").append('\t'); // Aij
		sb.append(unitX).append('\t'); // FitFreq					//idem Freq
		sb.append(unitX).append('\t'); // DeltaFitFreq				//idem Freq

		sb.append(unitVo).append('\t'); // Vo						// Peut changer, toujours vit/redshift
		sb.append(unitVo).append('\t'); // deltaVo					// idem Vo
		sb.append(unitVo).append('\t'); // fwhm_g					// idem Vo
		sb.append(unitVo).append('\t'); //deltaFWHM_G				// idem Vo
		sb.append(unitVo).append('\t'); // FWHM_L					// idem Vo
		sb.append(unitVo).append('\t'); // deltaFWHM_L				// idem Vo

		sb.append(unitIntensity).append('\t'); // Int 				// TODO peut changer? (Jy... spire ?)
		sb.append(unitIntensity).append('\t'); // deltaInt			// TODO peut changer? (Jy... spire?) idem Int

		sb.append(unitFitFlux).append('\t'); // FitFlux 			// TODO peut changer? (Jy.km/s .. spire?)
		sb.append(unitFitFlux).append('\t'); // deltaFixFlux 		// TODO peut changer? (Jy.km/s .. spire?) idemFitFlux

		sb.append(unitX).append('\t'); // Freq.I_Max 				// TODO peut changer???? => axe X,
		sb.append(unitVo).append('\t'); // V.I_Max					// TODO peut changer, idem Vo
		sb.append(unitVo).append('\t'); // FWHM						// TODO peut changer, idem Vo

		sb.append(unitIntensity).append('\t'); // I_Max				// TODO peut changer ? idem Int

		sb.append(unitFitFlux).append('\t'); // Flux 				// TODO peut changer? idemFitFlux
		sb.append(unitFitFlux).append('\t'); // deltaFlux 			// TODO peut changer?idemFitFlux

		sb.append(unitRms).append('\t'); // RMS 					// TODO peut changer? ======== cf rmsUnit
		sb.append(unitVo).append('\t'); // deltaV					// TODO pc, idem Vo
		sb.append("%").append('\t'); // Cal
		sb.append("arcsec").append('\t'); // Size 						// TODO check
		sb.append("None").append('\t'); // TelescopePath
		sb.append("None"); // TelescopeName

		return sb.toString();
	}
}
