/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit;

import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.X_AXIS;

/**
 * This class is a singleton to provide constant fields.<br>
 * Constant.getInstance().getFWHL() return 2.3548<br>
 *
 * @author Cedric Chappert
 *
 */
public final class FitConstant {

	private static final double FWHM_VELOCITY_DEFAULT = 1;
	private static final double FWHM_FREQ_DEFAULT = 0.005;
	private static final double FWHM_CM_MOINS_1_DEFAULT = 0.07;
	private static final double FWHM_MICRON_METER_DEFAULT = 0.1; // PACS
	private static final double FWHM_SPIRE_VELOCITY = 100;

	public static final double FWHM_LORENTZIAN = 2;

	/**
	 * FWHM_COEFF = 2.3548
	 */
	public static final double FWHM_COEFF = calcFWHM_COEFF();
	public static final double FWHM_SINC = 3.75;

	/**
	 * Get Fwhl value.
	 *
	 * @return double 2.3548
	 */
	public static double getFWHM() {
		return FWHM_COEFF;
	}

	/**
	 * Calculate the FWHM_COEFF with :<br>
	 * 2 * Math.sqrt(2 * Math.ln(2))<br>
	 * 2 * &radic; (2 * ln(2))
	 *
	 * @return double FWHM
	 */
	private static double calcFWHM_COEFF() {
		return 2 * Math.sqrt(2 * Math.log(2));
	}

	public static Double getFWHM_DEFAULT(XAxisCassis axis, String telescope) {
		Double val = null;
		if (telescope != null &&
				("pacs".equalsIgnoreCase(telescope)||
				"spire".equalsIgnoreCase(telescope))) {
			if (X_AXIS.VELOCITY_SIGNAL.equals(axis.getAxis()))
				val = FWHM_SPIRE_VELOCITY;
		}
		else
			switch (axis.getAxis()) {
				case FREQUENCY_SIGNAL:
					val = FWHM_FREQ_DEFAULT;
					break;
				case VELOCITY_SIGNAL:
					val = FWHM_VELOCITY_DEFAULT;
					break;
				case UNKNOWN:
					val = 0.;
					break;
				case WAVE_LENGTH:
					val = FWHM_MICRON_METER_DEFAULT;
					break;
				case WAVE_NUMBER:
					val = FWHM_CM_MOINS_1_DEFAULT;
					break;
				default:
					break;
			}
		return val;
	}
}
