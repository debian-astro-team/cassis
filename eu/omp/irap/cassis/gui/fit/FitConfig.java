/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.BufferedWriterProperty;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.X_AXIS;
import eu.omp.irap.cassis.gui.plot.util.InterValMarkerCassis;

public class FitConfig {

	private static final Logger LOGGER = LoggerFactory.getLogger(FitConfig.class);
	private List<InterValMarkerCassis> listMarker;
	private List<FittingItem> listFittingItem;
	private FitStyleEnum fitStyle;
	private int nbOversampl;
	private int nbIter;
	private XAxisCassis xAxis;
	private double tolerance;


	// TODO Set activate and blockable parameters.
	public boolean loadFile(File file) {
		boolean val = true; //, isBlockable = true; //, isChecked = false, actived = true;
		List<ArrayList<Boolean>> blockArray = new ArrayList<>();
		List<ArrayList<Double>> valueArray = new ArrayList<>();
		List<Integer> nbParams = new ArrayList<>();
		FitModelEnum modelEnum;
		FittingItemList modelFitting;
		try (FileInputStream fis = new FileInputStream(file)) {
			Properties prop = new Properties();
			prop.load(fis);

			xAxis = XAxisCassis.getXAxisCassis(X_AXIS.valueOf(prop.getProperty("xAxis")),
					UNIT.valueOf(prop.getProperty("xAxisUnit")));
			fitStyle = FitStyleEnum.valueOf(prop.getProperty("fitter"));
			nbIter = Integer.valueOf(prop.getProperty("nbIterations"));
			nbOversampl = Integer.valueOf(prop.getProperty("oversampling"));
			tolerance = Double.valueOf(prop.getProperty("tolerance", "0.001"));
			loadListMarker(prop);

			int nbComposants = Integer.parseInt(prop.getProperty("nbComposants"));
			modelFitting = new FittingItemList(new FitPanel(null, false, null));
			for (int i = 1; i <= nbComposants; i++) {
				modelEnum = FitModelEnum.valueOf(prop.getProperty("composant" + i));

				String title = getTitle(modelEnum);

				modelFitting.addFittingItem(title, modelEnum);

				ArrayList<Boolean> blockArrayTemp = new ArrayList<>();
				ArrayList<Double> valueArrayTemp = new ArrayList<>();
				int nbparameters = Integer.parseInt(prop.getProperty("nbParamsComposant" + i));
				for (int j = 1; j <= nbparameters; j++) {
					blockArrayTemp.add(Boolean.valueOf(prop.getProperty("composant" + i + "Param" + j + "Blocked")));
					valueArrayTemp.add(Double.valueOf(prop.getProperty("composant" + i + "Param" + j + "Value")));
				}
				nbParams.add(nbparameters);
				blockArray.add(blockArrayTemp);
				valueArray.add(valueArrayTemp);
			}
			loadFittingItemParameters(blockArray, valueArray, modelFitting, prop);
		} catch (IOException e) {
			LOGGER.error("Error while reading the file {}", file.getAbsolutePath(), e);
			val = false;
		}

		return val;
	}

	private void loadListMarker(Properties prop) {
		InterValMarkerCassis markerCassis;
		double startMarker;
		double endMarker;
		listMarker = new ArrayList<>();
		int nbMarker = Integer.parseInt(prop.getProperty("nbMarker"));

		for (int cpt = 1; cpt <= nbMarker; cpt++) {
			startMarker = Double.valueOf(prop.getProperty("startMarker" + cpt));
			endMarker = Double.valueOf(prop.getProperty("endMarker" + cpt));
			markerCassis = new InterValMarkerCassis(startMarker, endMarker);
			listMarker.add(markerCassis);
		}
	}

	private String getTitle(FitModelEnum modelEnum) {
		String title = "";
		switch (modelEnum) {
			case POLY:
				title = "Baseline :  Polynomial";
				break;
			case SIN:
				title = "Baseline :  Sinus";
				break;
			case GAUSS:
				title = "Line :  Gaussian";
				break;
			case LORENTZ:
				title = "Line :  Lorentzian";
				break;
			case VOIGT:
				title = "Line :  Voigt Profile";
				break;
			case SINC:
				title = "Line :  Sinc Profile";
				break;
			default:
				title = "Unknow";
		}
		return title;
	}

	private void loadFittingItemParameters(List<ArrayList<Boolean>> blockArray,
			List<ArrayList<Double>> valueArray, FittingItemList modelFitting, Properties prop) {
		listFittingItem = modelFitting.geListFittingItem();
		for (int i = 0; i < listFittingItem.size(); i++) {
			FittingItem fittingItem = listFittingItem.get(i);
			ArrayList<Boolean> blockArrayTemp = blockArray.get(i);
			ArrayList<Double> valueArrayTemp = valueArray.get(i);
			boolean[] isBlocked = new boolean[blockArrayTemp.size()];
			double[] parameters = new double[blockArrayTemp.size()];
			for (int j = 0; j < blockArrayTemp.size(); j++) {
				isBlocked[j] = blockArrayTemp.get(j);
				parameters[j] = valueArrayTemp.get(j);
			}

			fittingItem.setParametersBlocked(isBlocked);
			fittingItem.refreshValues(parameters);
		}
	}

	public boolean saveFile(File file) {
		boolean val = true;
		InterValMarkerCassis markerCassis;
		ParamItem paramItem;
		FittingItem item;
		try (BufferedWriterProperty out = new BufferedWriterProperty(new FileWriter(file))) {
			out.writeTitle("Fit Parameters");
			out.writeProperty("xAxis", String.valueOf(xAxis.getAxis().name()));
			out.writeProperty("xAxisUnit", String.valueOf(xAxis.getUnit().name()));
			out.writeProperty("fitter", String.valueOf(fitStyle));
			out.writeProperty("nbIterations", String.valueOf(nbIter));
			out.writeProperty("oversampling", String.valueOf(nbOversampl));
			out.writeProperty("tolerance", String.valueOf(tolerance));
			out.writeProperty("nbMarker", String.valueOf(listMarker.size()));
			// save Marker
			for (int cpt = 1; cpt <= listMarker.size(); cpt++) {
				markerCassis = listMarker.get(cpt - 1);
				out.writeProperty("startMarker" + cpt, String.valueOf(markerCassis.getStartValueCassis()));
				out.writeProperty("endMarker" + cpt, String.valueOf(markerCassis.getEndValueCassis()));
			}
			out.writeProperty("nbComposants", String.valueOf(listFittingItem.size()));

			for (int i = 1; i <= listFittingItem.size(); i++) {
				item = listFittingItem.get(i - 1);
				FitModelEnum modelEnum = item.getModel();
				out.writeProperty("composant" + i, String.valueOf(modelEnum));
				if (!FitModelEnum.POLY.equals(modelEnum))
					out.writeProperty("composant" + i + "Checked", String.valueOf(item.isChecked()));
				out.writeProperty("composant" + i + "Actived", String.valueOf(item.isActive()));
				out.writeProperty("nbParamsComposant" + i, String.valueOf(item.getParameters().size()));
				for (int j = 1; j <= item.getParameters().size(); j++) {
					paramItem = item.getParameters().get(j - 1);
					out.writeProperty("composant" + i + "Param" + j + "Name", String.valueOf(paramItem.getTitle()));
					out.writeProperty("composant" + i + "Param" + j + "Blocked", String.valueOf(paramItem.isBlocked()));
					out.writeProperty("composant" + i + "Param" + j + "Value", String.valueOf(paramItem.getValue()));
				}
			}
			out.flush();
		} catch (IOException e) {
			LOGGER.error("Error while saving the file {}", file.getAbsolutePath(), e);
			val = false;
		}

		return val;
	}

	/**
	 * @return the xAxis
	 */
	public final XAxisCassis getxAxis() {
		return xAxis;
	}

	/**
	 * @param xAxis
	 *            the xAxis to set
	 */
	public final void setxAxis(XAxisCassis xAxis) {
		this.xAxis = xAxis;
	}

	/**
	 * @param listMarker
	 *            the listMarker to set
	 */
	public final void setListMarker(List<InterValMarkerCassis> listMarker) {
		this.listMarker = listMarker;
	}

	/**
	 * @param listFittingItem
	 *            the listFittingItem to set
	 */
	public final void setListFittingItem(List<FittingItem> listFittingItem) {
		this.listFittingItem = listFittingItem;
	}

	/**
	 * @param fitStyle
	 *            the fitStyle to set
	 */
	public final void setFitStyle(FitStyleEnum fitStyle) {
		this.fitStyle = fitStyle;
	}

	/**
	 * @param nbOversampl
	 *            the nbOversampl to set
	 */
	public final void setNbOversampl(int nbOversampl) {
		this.nbOversampl = nbOversampl;
	}

	/**
	 * @param nbIter
	 *            the nbIter to set
	 */
	public final void setNbIter(int nbIter) {
		this.nbIter = nbIter;
	}

	public FitStyleEnum getFitStyle() {
		return fitStyle;
	}

	/**
	 * @return the listMarker
	 */
	public final List<InterValMarkerCassis> getListMarker() {
		return listMarker;
	}

	/**
	 * @return the listFittingItem
	 */
	public final List<FittingItem> getListFittingItem() {
		return listFittingItem;
	}

	/**
	 * @return the nbOversampl
	 */
	public final int getNbOversampl() {
		return nbOversampl;
	}

	/**
	 * @return the nbIter
	 */
	public final int getNbIter() {
		return nbIter;
	}

	/**
	 *
	 * @param tolerance
	 */
	public void setTolerance(double tolerance) {
		this.tolerance = tolerance;

	}

	/**
	 * @return the nbOversampl
	 */
	public final double getTolerance() {
		return tolerance;
	}

}
