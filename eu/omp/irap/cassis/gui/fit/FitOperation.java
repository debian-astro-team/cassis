/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.Formula;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import eu.omp.irap.cassis.gui.plot.util.InterValMarkerCassis;

/**
 * @author glorian
 *
 */
public class FitOperation {

	private static DecimalFormat deltaFormat;
	private static DecimalFormat veloFormat;
	private static DecimalFormat freqFormat;
	private static DecimalFormat intensityFormat;

	static {
		DecimalFormatSymbols ds = new DecimalFormatSymbols();
		ds.setDecimalSeparator('.');

		deltaFormat = new DecimalFormat("###0.0000", ds);// 8c
		veloFormat = new DecimalFormat("#####0.000", ds);// 10c
		freqFormat = new DecimalFormat("######0.000", ds);// 11c
		intensityFormat = new DecimalFormat("##0.0000", ds);// 8c
	}

	public static double computeIntegral(XYSpectrumSeries serie, List<InterValMarkerCassis> markerList) {
		if (serie == null)
			return 0;

		double res = 0;

		if (markerList.isEmpty()) {
			double[] xValues = serie.getSpectrum().getXData(serie.getXAxis());
			double min = xValues[0];
			double max = xValues[serie.getSpectrum().getSize() - 1];
			res = res + computeIntegral(serie, min, max);
		} else {
			for (InterValMarkerCassis marker : markerList) {
				double min = marker.getStartValueCassis();
				double max = marker.getEndValueCassis();
				res = res + computeIntegral(serie, min, max);
			}
		}

		return res;
	}

	// TODO revoir pour multiaxes
	// min et max sont en unité de l'axe affiché
	private static double computeIntegral(XYSpectrumSeries dataCurve, double min, double max) {
		int minSelectionIndex = 0;
		int maxSelectionIndex = 0;
		double xValue;
		double minAbsolute = Double.MAX_VALUE;
		double maxAbsolute = Double.NEGATIVE_INFINITY;

		// Get selected displayed spectrum from model
		// Navigate throught displayedSpectrum to get selection indexes
		double[] xValueofCurrentAxis = dataCurve.getSpectrum().getXData(dataCurve.getXAxis());
		for (int i = 0; i < dataCurve.getSpectrum().getSize(); i++) {
			xValue = xValueofCurrentAxis[i];

			if (xValue >= min && xValue <= max) {
				if (xValue <= minAbsolute) {
					minSelectionIndex = i;
					minAbsolute = xValue;
				}
				if (xValue >= maxAbsolute) {
					maxSelectionIndex = i;
					maxAbsolute = xValue;
				}
			}
		}
		return computeIntegral(dataCurve, minSelectionIndex, maxSelectionIndex);
	}

	public static String getFirstMomentLog(XYSpectrumSeries serie, Double freqCentral,
			List<InterValMarkerCassis> listMarker, Double vlsrFile) {
		return getFirstMomentLog(serie, freqCentral, listMarker, vlsrFile, false) ;
	}

	public static String getFirstMomentLog(XYSpectrumSeries serie, Double freqCentral,
			List<InterValMarkerCassis> listMarker, Double vlsrFile, boolean humanFormat) {
		int indiceMax = -1;
		double iMax = Double.NEGATIVE_INFINITY;
		double[] xValues = serie.getSpectrum().getXData(serie.getXAxis());
		final double[] intensity = serie.getSpectrum().getIntensities(serie.getXAxis());
		if (listMarker.isEmpty()) {
			for (int cpt = 0; cpt < serie.getSpectrum().getSize(); cpt++) {
				if (intensity[cpt] > iMax) {
					iMax = intensity[cpt];
					indiceMax = cpt;
				}
			}
		} else {
			for (int indice = 0; indice < listMarker.size(); indice++) {
				double minMarker = listMarker.get(indice).getStartValueCassis();
				double maxMarker = listMarker.get(indice).getEndValueCassis();

				for (int cpt = 0; cpt < serie.getSpectrum().getSize(); cpt++) {
					if (intensity[cpt] > iMax
							&& xValues[cpt] >= minMarker
							&& xValues[cpt] <= maxMarker) {
						iMax = intensity[cpt];
						indiceMax = cpt;
					}
				}
			}
		}

		// FreqImax
		// TODO PROBLEME DOUBLE AXES
		double vIMax = xValues[indiceMax];
		double fIMax = freqCentral - (vIMax - vlsrFile) / Formula.CKm * freqCentral;
		// TODO voir si un listMarker est contenu dans un autre ...
		double flux = 0;
		flux = FitOperation.computeIntegral(serie, listMarker);

		double sigma = flux / iMax;
		double deltaFlux = 0.;
		StringBuilder sb = new StringBuilder();
		if(humanFormat){
			sb.append(freqFormat.format(fIMax)).append('\t');
			sb.append(veloFormat.format(vIMax)).append('\t');
			sb.append(deltaFormat.format(sigma)).append('\t');
			sb.append(intensityFormat.format(iMax)).append('\t');
			sb.append(intensityFormat.format(flux)).append('\t');
			sb.append(intensityFormat.format(deltaFlux));
		} else {

			sb.append(fIMax).append('\t');
			sb.append(vIMax).append('\t');
			sb.append(sigma).append('\t');
			sb.append(iMax).append('\t');
			sb.append(flux).append('\t');
			sb.append(deltaFlux);
		}

		return sb.toString();
	}

	public static String getFitFreqAndDeltaFitFreq(FittingItem item, double freqCentral, double vlsrFile) {
		return getFitFreqAndDeltaFitFreq(item, freqCentral, vlsrFile,false);
	}
	public static String getFitFreqAndDeltaFitFreq(FittingItem item, double freqCentral, double vlsrFile,
			boolean humanFormat) {
		double velocity = item.getParameters().get(FittingItem.INDICE_XO).getValue();
		double deltaVelocity = item.getParameters().get(FittingItem.INDICE_XO).getStdDev();
		double freq = freqCentral - (velocity - vlsrFile) / Formula.CKm * freqCentral;
		double deltaFreq = freq * deltaVelocity / Formula.CKm;
		StringBuilder sb = new StringBuilder();
		if (humanFormat){
			sb.append(freqFormat.format(freq)).append('\t').append(deltaFormat.format(deltaFreq));
		} else {
			sb.append(freq).append('\t').append(deltaFreq);
		}
		return sb.toString();
	}

	public static double getVelocity(FittingItem item) {
		return item.getParameters().get(FittingItem.INDICE_XO).getValue();
	}

	public static double getDeltaVelocity(FittingItem item) {
		return item.getParameters().get(FittingItem.INDICE_XO).getStdDev();
	}

	public static double getIntensity(FittingItem item) {
		return item.getParameters().get(FittingItem.INDICE_I0).getValue();
	}

	public static double getDeltaIntensity(FittingItem item) {
		return item.getParameters().get(FittingItem.INDICE_I0).getStdDev();
	}

	public static String getFitFluxAndDeltaFitFlux(XYSpectrumSeries serie, double freqCentral, double vlsrFile) {
		return  getFitFluxAndDeltaFitFlux(serie, freqCentral, vlsrFile, false);
	}
	// TODO deltaFitFlux ???
	public static String getFitFluxAndDeltaFitFlux(XYSpectrumSeries serie, double freqCentral,
			double vlsrFile, boolean humanFormat) {
		serie.getSpectrum().setFreqRef(freqCentral);
		serie.getSpectrum().setVlsr(vlsrFile);

		double fitFlux = computeIntegral(serie);
		double deltaFitFlux = 0.;
		StringBuilder sb = new StringBuilder();
		if (humanFormat){
			sb.append(intensityFormat.format(fitFlux)).append('\t').append(deltaFormat.format(deltaFitFlux));
		} else {
			sb.append(fitFlux).append('\t').append(deltaFitFlux);
		}

		return sb.toString();
	}

	public static String getFwhmGAndL(FittingItem item) {
		StringBuilder sb = new StringBuilder();
		ParamItem param = item.getParameters().get(FittingItem.INDICE_FWHM);
		FitModelEnum fitModel = item.getModelName();
		if (FitModelEnum.GAUSS.equals(fitModel) || FitModelEnum.SINC.equals(fitModel)) {
			sb.append(param.getValue()).append('\t'); // FWHM_G
			sb.append(param.getStdDev()).append('\t'); // deltaFWHM_G
			sb.append("0.0\t0.0"); // FWHM_L & deltaFWHM_L
		}
		else if (FitModelEnum.LORENTZ.equals(fitModel)) {
			sb.append("0.0\t0.0\t"); // FWHM_G & deltaFWHM_G
			sb.append(param.getValue()).append('\t'); // FWHM_L
			sb.append(param.getStdDev()); // deltaFWHM_L
		}
		else if (FitModelEnum.VOIGT.equals(fitModel)) {
			ParamItem param2 = item.getParameters().get(FittingItem.INDICE_FWHM2);
			sb.append(param.getValue()).append('\t'); // FWHM_G
			sb.append(param.getStdDev()).append('\t'); // deltaFWHM_G
			sb.append(param2.getValue()).append('\t'); // FWHM_L
			sb.append(param.getStdDev()); // deltaFWHM_L
		}
		else {
			// fallback : FWHM_G, deltaFWHM_G, FWHM_L, deltaFWHM_L
			sb.append("0.0\t0.0\t0.0\t0.0");
		}

		return sb.toString();
	}

	public static String getHumanFwhmGAndL(FittingItem item) {
		StringBuilder sb = new StringBuilder();
		ParamItem param = item.getParameters().get(FittingItem.INDICE_FWHM);
		FitModelEnum fitModel = item.getModelName();
		if (FitModelEnum.GAUSS.equals(fitModel) || FitModelEnum.SINC.equals(fitModel)) {
			sb.append(deltaFormat.format(param.getValue())).append('\t'); // FWHM_G
			sb.append(deltaFormat.format(param.getStdDev())).append('\t'); // deltaFWHM_G
			sb.append("0.0\t0.0"); // FWHM_L & deltaFWHM_L
		}
		else if (FitModelEnum.LORENTZ.equals(fitModel)) {
			sb.append("0.0\t0.0\t"); // FWHM_G & deltaFWHM_G
			sb.append(deltaFormat.format(param.getValue())).append('\t'); // FWHM_L
			sb.append(deltaFormat.format(param.getStdDev())); // deltaFWHM_L
		}
		else if (FitModelEnum.VOIGT.equals(fitModel)) {
			ParamItem param2 = item.getParameters().get(FittingItem.INDICE_FWHM2);
			sb.append(deltaFormat.format(param.getValue())).append('\t'); // FWHM_G
			sb.append(deltaFormat.format(param.getStdDev())).append('\t'); // deltaFWHM_G
			sb.append(deltaFormat.format(param2.getValue())).append('\t'); // FWHM_L
			sb.append(deltaFormat.format(param.getStdDev())); // deltaFWHM_L
		}
		else {
			// fallback : FWHM_G, deltaFWHM_G, FWHM_L, deltaFWHM_L
			sb.append("0.0\t0.0\t0.0\t0.0");
		}

		return sb.toString();
	}

	public static double computeIntegral(XYSpectrumSeries dataCurve) {
		return computeIntegral(dataCurve, 0, dataCurve.getSpectrum().getSize() - 1);
	}


	private static double computeIntegral(XYSpectrumSeries dataCurve, int minSelectionIndex, int maxSelectionIndex) {
		double delta;
		double intensity;
		double moment = 0;
		CommentedSpectrum spectrum = dataCurve.getSpectrum();
		double[] intensityTab = spectrum.getIntensities(dataCurve.getXAxis());

		double[] deltaTab = spectrum.getDelta(dataCurve.getXAxis());
		int length = maxSelectionIndex - minSelectionIndex + 1;
		if (dataCurve.getXAxis().isInverted()) {
			double[] deltaTabInv = new double[deltaTab.length];
			for (int i = 0; i < deltaTabInv.length; i++) {
				deltaTabInv[i] = deltaTab[deltaTab.length - 1 - i];
			}
			for (int i = 0; i < length; i++) {
				intensity = intensityTab[minSelectionIndex + i];
				delta = Math.abs(deltaTabInv[minSelectionIndex + i]);
				moment += intensity * delta;
			}
		} else {
			for (int i = 0; i < length; i++) {
				intensity = intensityTab[minSelectionIndex + i];
				delta = Math.abs(deltaTab[minSelectionIndex + i]);
				moment += intensity * delta;
			}
		}
		return moment;
	}

	public static double getRms(double rms) {
		if (Double.isNaN(rms))
			rms = 0.;
		return rms * 1000;
	}

	public static double getDeltaV(List<InterValMarkerCassis> listMarker, XYSpectrumSeries dataSerie) {
		return computeDvObservation(listMarker, dataSerie);
	}

	private static double computeDvObservation(List<InterValMarkerCassis> listMarker, XYSpectrumSeries serie) {
		double[] deltaTab = serie.getSpectrum().getDelta(serie.getXAxis());
		if (listMarker.isEmpty())
			return Math.abs(deltaTab[0]);

		// TODO a revoir essayer de prendre le marker contenant la frequen
		// centrale
		for (int indice = 0; indice < listMarker.size(); indice++) {
			double minMarker = listMarker.get(indice).getStartValueCassis();
			double maxMarker = listMarker.get(indice).getEndValueCassis();
			double[] xValues = serie.getSpectrum().getXData(serie.getXAxis());
			for (int cpt = 0; cpt < serie.getSpectrum().getSize(); cpt++) {

				if (xValues[cpt] >= minMarker
						&& xValues[cpt] <= maxMarker) {
					return Math.abs(deltaTab[cpt]);
				}
			}
		}
		return 0;
	}

}
