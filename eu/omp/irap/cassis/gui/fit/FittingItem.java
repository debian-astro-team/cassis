/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.util.LinkedList;

import javax.swing.JLabel;
import javax.swing.JPanel;

import eu.omp.irap.cassis.gui.util.ButtonTitledPanel;

public class FittingItem extends ButtonTitledPanel {

	private static final long serialVersionUID = 1L;
	public static final int INDICE_XO = 1;
	public static final int INDICE_FWHM = 2;
	public static final int INDICE_FWHM2 = 3;
	public static final int INDICE_I0 = 0;
	private LinkedList<ParamItem> parameters;
	private JPanel panelEast;
	private JPanel panelCenter;
	private JPanel panelWest;
	private boolean active = true;
	private boolean changeDefaultValue = false;
	private FitModelEnum model = FitModelEnum.GAUSS;


	public FittingItem(String mode, FittingItemList modelFitting, FitModelEnum model) {
		this(mode, true, modelFitting, model);
	}

	public FittingItem(String mode, boolean isBlockable, final FittingItemList modelFitting, FitModelEnum model) {
		super(mode, isBlockable);
		this.model = model;
		super.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if ("checkAll".equalsIgnoreCase(e.getActionCommand())) {
					for (ParamItem param : parameters)
						param.getChkBox().setSelected(FittingItem.this.isChecked());
				}
			}
		});

		final JPanel panelParam = new JPanel();
		panelParam.setLayout(new BorderLayout());
		panelWest = new JPanel();
		panelCenter = new JPanel();
		panelEast = new JPanel();

		parameters = new LinkedList<>();

		createPanelComponent(mode);
		panelParam.add(panelWest, BorderLayout.WEST);
		panelParam.add(panelCenter, BorderLayout.CENTER);
		panelParam.add(panelEast, BorderLayout.EAST);
		panelParam.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (FitModelEnum.isLine(FittingItem.this.model) && isActive()) {
					Color color = parameters.get(0).getTxtField().getBackground();
					if (color.equals(Color.WHITE))
						color = new Color(211, 211, 255);
					else
						color = Color.WHITE;
					for (ParamItem item : parameters) {
						item.getTxtField().setBackground(color);
					}
					for (FittingItem item : modelFitting.geListFittingItem()) {
						item.setChangeDefaultValue(false);
					}
					setChangeDefaultValue(color != Color.WHITE);
				}
			}
		});
		addSubPanel(panelParam);
		addButtonListener(modelFitting);
	}

	private void addButtonListener(final FittingItemList modelFitting) {
		addButtonClearListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				modelFitting.deleteFittingItem(FittingItem.this);
			}
		});

		addButtonONOFFListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				boolean selected = FittingItem.this.buttonOnOff.isSelected();
				setButtonOff(selected);
			}

		});
	}

	public void setButtonOff(boolean selected) {
		buttonOnOff.setSelected(selected);
		if (FitModelEnum.POLY.equals(FittingItem.this.model)) {
			for (ParamItem param : parameters) {
				param.getTxtField().setEnabled(!selected);
				param.getLabel().setEnabled(!selected);
			}
		}
		else
			for (ParamItem param : parameters) {
				param.getChkBox().setEnabled(!selected);
				param.getTxtField().setEnabled(!selected);
				param.getLabel().setEnabled(!selected);
			}
		active = !selected;
	}

	private void createPanelComponent(String mode) {
		if ("Baseline :  Polynomial".equalsIgnoreCase(mode)) {
			baselinePoly();
		}
		else if ("Baseline :  Sinus".equalsIgnoreCase(mode)) {
			baselineSin();
		}
		else if ("Line :  Gaussian".equalsIgnoreCase(mode)) {
			lineGauss();
		}
		else if ("Line :  Lorentzian".equalsIgnoreCase(mode)) {
			lineLaur();
		}
		else if ("Line :  Voigt Profile".equalsIgnoreCase(mode)) {
			lineVoight();
		}
		else if ("Line :  Sinc Profile".equalsIgnoreCase(mode)) {
			lineSinc();
		}
	}

	private void baselinePoly() {
		model = FitModelEnum.POLY;
		// Case Poly : we ask the degre of the polynom
		setLayouts(1);
		addParam("Degree : ", false);
		parameters.getLast().getTxtField().setDecimalFormat(new DecimalFormat("0"));
		parameters.getLast().getTxtField().setValue(Long.valueOf(1));
	}

	private void baselineSin() {
		model = FitModelEnum.SIN;
		setLayouts(3);
		// Io, omega, phi
		addParam("Io : ", true);
		// Omega : 969
		addParam((char) 969 + " : ", true);
		// Phi : 966
		addParam((char) 966 + " : ", true);
	}

	private void lineGauss() {
		setLayouts(3);
		// Io, xo, FWHM
		addParam("Io : ", true);
		addParam("xo : ", true);
		addParam("FWHM : ", true);
	}

	private void lineLaur() {
		lineGauss();
	}

	private void lineSinc() {
		lineGauss();
	}

	private void lineVoight() {
		setLayouts(4);
		addParam("Io : ", true);
		addParam("xo : ", true);
		addParam((char) 963 + "G : ", true);
		addParam((char) 963 + "L : ", true);
	}

	public FitModelEnum getModelName() {
		return model;
	}

	public LinkedList<ParamItem> getParameters() {
		return parameters;
	}

	public double[] getParametersValues() {
		double[] parametersValues = new double[parameters.size()];
		int i = 0;
		for (ParamItem param : parameters) {
			parametersValues[i] = param.getValue();
			i++;
		}
		return parametersValues;
	}

	public boolean[] getParametersBlocked() {
		boolean[] parametersBlockedStatus = new boolean[parameters.size()];
		int i = 0;
		for (ParamItem param : parameters) {
			parametersBlockedStatus[i] = param.isBlocked();
			i++;
		}
		return parametersBlockedStatus;
	}

	public void setParametersBlocked(boolean[] isBlocked) {
		int i = 0;
		for (ParamItem param : parameters) {
			if (param.getChkBox() != null)
				param.getChkBox().setSelected(isBlocked[i]);
			i++;
		}
	}

	public void refreshValues(double[] newParameters) {
		int i = 0;
		for (ParamItem param : parameters) {
			param.refreshValues(newParameters[i], i == 2
					&& (getModelName() == FitModelEnum.GAUSS || getModelName() == FitModelEnum.LORENTZ));
			i++;
		}
	}

	public void setParametersValues(double[] newParameters) {
		int i = 0;
		if (newParameters.length != 0)
			for (ParamItem param : parameters) {
				// if case of FWHM
				param.setValue(newParameters[i], i == 2
						&& (getModelName() == FitModelEnum.GAUSS || getModelName() == FitModelEnum.LORENTZ));
				i++;
			}
		else {

			for (ParamItem param : parameters) {
				param.setValue(0.0, i == 2
						&& (getModelName() == FitModelEnum.GAUSS || getModelName() == FitModelEnum.LORENTZ));
				i++;
			}
		}
	}

	public void setStdDev(double[] stdDevValues) {
		int i = 0;
		for (ParamItem param : parameters) {
			param.setStdDev(stdDevValues[i]);
			i++;
		}
	}

	public void refreshStdDevValues(double[] stdDevValues) {
		int i = 0;
		for (ParamItem param : parameters) {
			param.setStdDev(stdDevValues[i]);
			i++;
		}
	}

	private void addParam(String title, boolean isBlockable) {
		parameters.addLast(new ParamItem(isBlockable, title));
		panelWest.add(new JLabel(title));
		if (isBlockable)
			panelCenter.add(parameters.getLast().getChkBox());
		panelEast.add(parameters.getLast().getTxtField());
		panelEast.add(parameters.getLast().getLabel());
	}

	private void setLayouts(int rows) {
		panelWest.setLayout(new GridLayout(rows, 1));
		panelCenter.setLayout(new GridLayout(rows, 1));
		panelEast.setLayout(new GridLayout(rows, 2));
	}

	/**
	 * @return the active
	 */
	public final boolean isActive() {
		return active;
	}

	/**
	 * @return the model
	 */
	public final FitModelEnum getModel() {
		return model;
	}

	/**
	 * @return the changeDefaultValue
	 */
	public final boolean isChangeDefaultValue() {
		return changeDefaultValue;
	}

	/**
	 * @param changeDefaultValue
	 *            the changeDefaultValue to set
	 */
	public final void setChangeDefaultValue(boolean changeDefaultValue) {
		this.changeDefaultValue = changeDefaultValue;
		Color color = null;
		if (changeDefaultValue)
			color = new Color(211, 211, 255);
		else
			color = Color.WHITE;
		for (ParamItem item : parameters) {
			item.getTxtField().setBackground(color);
		}
	}
}
