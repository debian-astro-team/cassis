/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit;

import java.util.Arrays;

public class FittingItemModel {

	private FitModelEnum fitModel;
	private static final double MIN_VAL_FWHM_LEVENBERG = 1E-6;
	private double[] parametersValues;
	private boolean[] parametersBlocked;


	public FittingItemModel(FitModelEnum fitModel, double[] parametersValues, boolean[] parametersBlocked) {
		super();
		this.fitModel = fitModel;
		this.parametersValues = parametersValues;
		this.parametersBlocked = parametersBlocked;
	}

	public void changeParameters(double xOffset, double multX, FitStyleEnum fitStyle) {
		if (FitModelEnum.GAUSS.equals(fitModel)) {
			changeGaussParameters(parametersValues, parametersBlocked, xOffset, multX, fitStyle);
		}
		else if (FitModelEnum.LORENTZ.equals(fitModel)) {
			changeLorentzParameters(parametersValues, parametersBlocked, xOffset, multX, fitStyle);
		}
		else if (FitModelEnum.SINC.equals(fitModel)) {
			changeSincParameters(parametersValues, parametersBlocked, xOffset, fitStyle);
		}
		else if (FitModelEnum.VOIGT.equals(fitModel)) {
			changeVoigthParameters(parametersValues, parametersBlocked, xOffset, multX, fitStyle);
		}
	}

	private void changeVoigthParameters(double[] parametersValues, boolean[] parametersBlocked, double xOffset,
			double multX, FitStyleEnum fitStyle) {
		parametersValues[FitModelEnum.VOIGT_X0_INDEX] = (parametersValues[FitModelEnum.VOIGT_X0_INDEX] + xOffset)
				* multX;
		if (FitStyleEnum.LEVENBERG.equals(fitStyle) && parametersBlocked[FitModelEnum.GAUSS_FWHM_INDEX]
				&& parametersValues[FitModelEnum.GAUSS_FWHM_INDEX] == 0)
			parametersValues[FitModelEnum.VOIGT_FWHM_G_INDEX] = MIN_VAL_FWHM_LEVENBERG * multX;
		parametersValues[FitModelEnum.VOIGT_FWHM_G_INDEX] = parametersValues[FitModelEnum.VOIGT_FWHM_G_INDEX]
				/ FitConstant.getFWHM() * multX;
		if (FitStyleEnum.LEVENBERG.equals(fitStyle) && parametersBlocked[FitModelEnum.VOIGT_FWHM_L_INDEX]
				&& parametersValues[FitModelEnum.VOIGT_FWHM_L_INDEX] == 0)
			parametersValues[FitModelEnum.VOIGT_FWHM_L_INDEX] = MIN_VAL_FWHM_LEVENBERG * multX;
		parametersValues[FitModelEnum.VOIGT_FWHM_L_INDEX] = parametersValues[FitModelEnum.VOIGT_FWHM_L_INDEX]
				/ FitConstant.FWHM_LORENTZIAN * multX;
	}

	private void changeSincParameters(double[] parametersValues, boolean[] parametersBlocked, double xOffset,
			FitStyleEnum fitStyle) {
		parametersValues[FitModelEnum.SINC_P1_INDEX] = parametersValues[FitModelEnum.SINC_P1_INDEX] + xOffset;
		if (FitStyleEnum.LEVENBERG.equals(fitStyle) && parametersBlocked[FitModelEnum.SINC_P2_INDEX]
				&& parametersValues[FitModelEnum.SINC_P2_INDEX] == 0)
			parametersValues[FitModelEnum.SINC_P2_INDEX] = MIN_VAL_FWHM_LEVENBERG;
		parametersValues[FitModelEnum.SINC_P2_INDEX] = parametersValues[FitModelEnum.SINC_P2_INDEX]
				/ FitConstant.FWHM_SINC;
	}

	private void changeLorentzParameters(double[] parametersValues, boolean[] parametersBlocked, double xOffset,
			double multX, FitStyleEnum fitStyle) {
		parametersValues[FitModelEnum.LORENTZ_X0_INDEX] = (parametersValues[FitModelEnum.LORENTZ_X0_INDEX] + xOffset)
				* multX;
		if (FitStyleEnum.LEVENBERG.equals(fitStyle) && parametersBlocked[FitModelEnum.LORENTZ_FWHM_INDEX]
				&& parametersValues[FitModelEnum.LORENTZ_FWHM_INDEX] == 0)
			parametersValues[FitModelEnum.LORENTZ_FWHM_INDEX] = MIN_VAL_FWHM_LEVENBERG * multX;
		parametersValues[FitModelEnum.LORENTZ_FWHM_INDEX] = parametersValues[FitModelEnum.LORENTZ_FWHM_INDEX]
				/ FitConstant.FWHM_LORENTZIAN * multX;
	}

	private void changeGaussParameters(double[] parametersValues, boolean[] parametersBlocked, double xOffset,
			double multX, FitStyleEnum fitStyle) {
		parametersValues[FitModelEnum.GAUSS_X0_INDEX] = (parametersValues[FitModelEnum.GAUSS_X0_INDEX] + xOffset)
				* multX;
		if (FitStyleEnum.LEVENBERG.equals(fitStyle) && parametersBlocked[FitModelEnum.GAUSS_FWHM_INDEX]
				&& parametersValues[FitModelEnum.GAUSS_FWHM_INDEX] == 0)
			parametersValues[FitModelEnum.GAUSS_FWHM_INDEX] = MIN_VAL_FWHM_LEVENBERG * multX;
		parametersValues[FitModelEnum.GAUSS_FWHM_INDEX] = parametersValues[FitModelEnum.GAUSS_FWHM_INDEX]
				/ FitConstant.getFWHM() * multX;
	}

	public FitModelEnum getModelName() {
		return fitModel;
	}

	public double[] getParametersValues() {
		return parametersValues;
	}

	public boolean[] getParametersBlocked() {
		return parametersBlocked;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FittingItemModel [fitModel=" + fitModel + ", parametersBlocked=" + Arrays.toString(parametersBlocked)
				+ ", parametersValues=" + Arrays.toString(parametersValues) + "]";
	}

}
