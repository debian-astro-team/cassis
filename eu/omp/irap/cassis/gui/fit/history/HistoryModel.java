/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit.history;

import java.util.EventObject;
import java.util.LinkedList;

import javax.swing.event.EventListenerList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author glorian
 *
 */
public class HistoryModel<T> {

	private static final Logger LOGGER = LoggerFactory.getLogger(HistoryModel.class);

	private int currentCpt = -1;
	private LinkedList<T> list = new LinkedList<>();
	private EventListenerList listeners = new EventListenerList();


	public void clear() {
		list.clear();
		currentCpt = -1;
	}

	public void getOriginal() {
		currentCpt = 0;
		fireOriginalHistoryAction();
	}

	public void getNext() {
		if (currentCpt + 1 >= list.size())
			LOGGER.error("No next action");
		else {
			currentCpt++;
			fireNextHistoryAction();
		}
	}

	public void getPrevious() {
		if (currentCpt - 1 < 0)
			LOGGER.error("No previous action");
		else {
			currentCpt--;
			firePreviousHistoryAction();
		}
	}

	public void addHistoryListener(HistoryListener listener) {
		listeners.add(HistoryListener.class, listener);
	}

	public void removeHistoryListener(HistoryListener l) {
		listeners.remove(HistoryListener.class, l);
	}

	public void fireNextHistoryAction() {
		HistoryListener[] listenerList = listeners.getListeners(HistoryListener.class);

		for (HistoryListener listener : listenerList) {
			listener.nextHistoryAction(new EventObject(list.get(currentCpt)));
		}
	}

	public void firePreviousHistoryAction() {
		HistoryListener[] listenerList = listeners.getListeners(HistoryListener.class);

		for (HistoryListener listener : listenerList) {
			listener.previousHistoryAction(new EventObject(list.get(currentCpt)));
		}
	}

	public void fireOriginalHistoryAction() {
		HistoryListener[] listenerList = listeners.getListeners(HistoryListener.class);

		for (HistoryListener listener : listenerList) {
			listener.originalHistoryAction(new EventObject(list.get(currentCpt)));
		}
	}

	public void addAction(T serie) {
		if (isLast()) {
			list.add(serie);
			currentCpt++;
		}
		else {
			while (list.size() - 1 > currentCpt)
				list.removeLast();
			list.add(serie);
			currentCpt++;
		}
	}

	public boolean isLast() {
		return currentCpt == list.size() - 1;
	}

	/**
	 * @return the currentCpt
	 */
	public final int getCurrentCpt() {
		return currentCpt;
	}

	public T getCurrentSerie() {
		return list.get(currentCpt);
	}
}
