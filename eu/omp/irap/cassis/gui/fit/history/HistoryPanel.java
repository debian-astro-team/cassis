/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit.history;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventObject;

import javax.swing.JButton;
import javax.swing.JPanel;

public class HistoryPanel<T> extends JPanel implements HistoryListener {

	enum HistoryButton {
		ORIGINAL, PREVIOUS, NEXT
	}

	private static final long serialVersionUID = -9131674565313696368L;
	private JButton buttonNext;
	private JButton buttonPrevious;
	private JButton buttonOriginal;
	private HistoryModel<T> historyModel;


	public HistoryPanel() {
		super();
		historyModel = new HistoryModel<>();
		historyModel.addHistoryListener(this);
		setLayout(new GridLayout(1, 3, 4, 4));
		add(getButtonOriginal());
		add(getButtonPrevious());
		add(getButtonNext());
	}

	private JButton getButtonNext() {
		// Button Redo
		if (buttonNext == null) {
			buttonNext = new JButton("Redo");
			buttonNext.setPreferredSize(new Dimension(85, 25));
			buttonNext.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					historyModel.getNext();
				}
			});
		}
		return buttonNext;
	}

	private JButton getButtonPrevious() {
		if (buttonPrevious == null) {
			buttonPrevious = new JButton("Undo");
			buttonPrevious.setPreferredSize(new Dimension(85, 25));
			buttonPrevious.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					historyModel.getPrevious();
				}
			});
		}
		return buttonPrevious;
	}

	private JButton getButtonOriginal() {
		if (buttonOriginal == null) {
			buttonOriginal = new JButton("Original");
			buttonOriginal.setEnabled(false);
			buttonOriginal.setPreferredSize(new Dimension(85, 25));
			buttonOriginal.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					historyModel.getOriginal();
				}
			});
		}
		return buttonOriginal;
	}

	@Override
	public void setEnabled(boolean val) {
		buttonOriginal.setEnabled(val);
		buttonNext.setEnabled(val);
		buttonPrevious.setEnabled(val);
	}

	public void addHistoryListener(HistoryListener listener) {
		historyModel.addHistoryListener(listener);
	}

	public void removeHistoryListener(HistoryListener l) {
		historyModel.removeHistoryListener(l);
	}

	public void initPanelFittingButtons() {
		buttonOriginal.setEnabled(false);
		buttonPrevious.setEnabled(false);
		buttonNext.setEnabled(false);
	}

	@Override
	public void nextHistoryAction(EventObject ev) {
		if (historyModel.isLast()) {
			buttonNext.setEnabled(false);
		}
		buttonPrevious.setEnabled(true);
	}

	@Override
	public void originalHistoryAction(EventObject ev) {
		buttonPrevious.setEnabled(false);
	}

	@Override
	public void previousHistoryAction(EventObject ev) {
		if (historyModel.getCurrentCpt() == 0) {
			buttonPrevious.setEnabled(false);
		}
		buttonNext.setEnabled(true);
	}

	public void initHistory(T serie) {
		historyModel.clear();
		initPanelFittingButtons();
		buttonOriginal.setEnabled(true);
		historyModel.addAction(serie);
	}

	public void updateHistoryButtons(T serie) {
		if (historyModel.getCurrentCpt() >= 0) {
			historyModel.addAction(serie);
			buttonNext.setEnabled(false);
			buttonPrevious.setEnabled(true);
		}
	}

	public T getCurrentSerie() {
		return historyModel.getCurrentSerie();
	}

	public HistoryModel<T> getModel() {
		return historyModel;
	}
}
