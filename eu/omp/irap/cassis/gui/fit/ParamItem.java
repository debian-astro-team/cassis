/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit;

import java.text.DecimalFormat;

import javax.swing.JCheckBox;

import eu.omp.irap.cassis.gui.util.JDoubleCassisTextField;

/**
 * Internal class to pass parameters and say if they are blocked or not.
 *
 * @author cupissol
 *
 */
public class ParamItem {

	private static final int TEXTFIELDSIZE = 8;
	private String title;
	private JDoubleCassisTextField paramText;
	private JCheckBox paramBlock = null;
	private JDoubleCassisTextField paramStdDev;


	public ParamItem(boolean isBlockable, String title) {
		if (isBlockable) {
			paramBlock = new JCheckBox();
		}
		this.title = title;
		paramText = new JDoubleCassisTextField();
		paramText.setValue(0.0);
		paramText.setColumns(TEXTFIELDSIZE);
		paramText.setDecimalFormat(new DecimalFormat("###0.0000"));
		paramText.setScientificFormat(new DecimalFormat("0.######E0"));
		paramText.setValMax(9999.9999);
		paramStdDev = new JDoubleCassisTextField();
		paramStdDev.setEditable(false);
		paramStdDev.setColumns(TEXTFIELDSIZE);
		paramStdDev.setValue(0.0);
		paramStdDev.setDecimalFormat(new DecimalFormat("###0.0###"));
		paramStdDev.setScientificFormat(new DecimalFormat("0.######E0"));
		paramStdDev.setValMax(9999.9999);
	}

	public void setValue(double value, boolean isFWHM) {
		if (!isBlocked()) {
			paramText.setValue(isFWHM ? Math.abs(value) : value);
		}
	}

	public void refreshValues(double value, boolean isFWHM) {
		paramText.setValue(isFWHM ? Math.abs(value) : value);
		paramText.repaint();
	}

	public void setValue(double value) {
		setValue(value, false);
	}

	public double getValue() {
		if (paramText.getValue() instanceof Long) {
			return (Long) paramText.getValue();
		}
		return (Double) paramText.getValue();
	}

	public double getStdDev() {
		return (Double) paramStdDev.getValue();
	}

	public boolean isBlocked() {
		return paramBlock == null ? false : paramBlock.isSelected();
	}

	public JDoubleCassisTextField getTxtField() {
		return paramText;
	}

	public JCheckBox getChkBox() {
		return paramBlock;
	}

	public JDoubleCassisTextField getLabel() {
		return paramStdDev;
	}

	public void setStdDev(double value) {
		paramStdDev.setValue(Math.abs(value));
		paramStdDev.repaint();
	}

	public String getTitle() {
		return this.title;
	}
}
