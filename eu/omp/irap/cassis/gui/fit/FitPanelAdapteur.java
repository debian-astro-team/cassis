/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit;

import java.util.EventObject;

import eu.omp.irap.cassis.gui.plot.util.InterValMarkerCassis;

/**
 * Adpateur class to avoid to implements all functions each time.
 *
 * @author glorian
 */
public class FitPanelAdapteur implements FitPanelListener {

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#displaySelectionClicked(java.util.EventObject)
	 */
	@Override
	public void displaySelectionClicked(EventObject ev) {
		// Do nothing by default.
	}

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#divideByFitClicked(java.util.EventObject)
	 */
	@Override
	public void divideByFitClicked(EventObject object) {
		// Do nothing by default.
	}

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#fitAllClicked(java.util.EventObject)
	 */
	@Override
	public void fitAllClicked(EventObject ev) {
		// Do nothing by default.
	}

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#fitCurrentClicked(java.util.EventObject)
	 */
	@Override
	public void fitCurrentClicked(EventObject ev) {
		// Do nothing by default.
	}

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#historicFitClicked(java.util.EventObject)
	 */
	@Override
	public void historicFitClicked(EventObject ev) {
		// Do nothing by default.
	}

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#overlayFitClicked(java.util.EventObject)
	 */
	@Override
	public void overlayFitClicked(EventObject ev) {
		// Do nothing by default.
	}

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#overlayResidualClicked(java.util.EventObject)
	 */
	@Override
	public void overlayResidualClicked(EventObject ev) {
		// Do nothing by default.
	}

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#resetAllSelectionClicked(java.util.EventObject)
	 */
	@Override
	public void resetAllSelectionClicked(EventObject ev) {
		// Do nothing by default.
	}

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#resetLastSelectionClicked(java.util.EventObject)
	 */
	@Override
	public void resetLastSelectionClicked(EventObject ev) {
		// Do nothing by default.
	}

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#saveFitClicked(java.util.EventObject)
	 */
	@Override
	public void saveFitClicked(EventObject ev) {
		// Do nothing by default.
	}

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#selectFileClicked(java.util.EventObject)
	 */
	@Override
	public boolean selectFileClicked(EventObject ev) {
		return true;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#substractFitClicked(java.util.EventObject)
	 */
	@Override
	public void substractFitClicked(EventObject ev) {
		// Do nothing by default.
	}

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#subtractFit()
	 */
	@Override
	public void subtractFit() {
		// Do nothing by default.
	}

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#divideByFit()
	 */
	@Override
	public void divideByFit() {
		// Do nothing by default.
	}

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#addIntervalMarker(eu.omp.irap.cassis.gui.plot.util.InterValMarkerCassis)
	 */
	@Override
	public void addIntervalMarker(InterValMarkerCassis marker) {
		// Do nothing by default.
	}

}
