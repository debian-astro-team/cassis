/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.fit;

import java.awt.Color;
import java.awt.Component;
import java.io.File;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.ChannelDescription;
import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.UtilArrayList;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.gui.plot.curve.TypeCurve;
import eu.omp.irap.cassis.gui.plot.curve.XYSpectrumSeries;
import eu.omp.irap.cassis.gui.plot.infopanel.InfoPanelConstants;
import eu.omp.irap.cassis.gui.plot.util.InterValMarkerCassis;
import eu.omp.irap.cassis.gui.plot.util.OperationFit;
import eu.omp.irap.cassis.gui.plot.util.SpectrumFitPanelInterface;
import eu.omp.irap.cassis.gui.util.FileUtils;
import eu.omp.irap.cassis.properties.Software;
import herschel.ia.numeric.Double1d;
import herschel.ia.numeric.toolbox.fit.AbstractModel;

public class SpectrumFitPanelListener implements FitPanelListener {

	private Component compo = null;
	private FitPanel fitPanel;
	private Double1d[] ydatasFittedNotResampleInOriginArray;
	private OperationFit[] opFitArray;
	private boolean lineAnalysis;
	private TelescopeFitInterface interfaceTelescope;
	private SpectrumFitPanelInterface spectrumView;
	private File selectedFile;


	public SpectrumFitPanelListener(SpectrumFitPanelInterface spectrumView, FitPanel fitPanel,
			TelescopeFitInterface interfaceTelescope, int nbPlot, boolean lineAnalysis) {
		this.spectrumView = spectrumView;
		this.fitPanel = fitPanel;
		this.interfaceTelescope = interfaceTelescope;
		if (spectrumView instanceof Component) {
			compo = (Component) spectrumView;
		}
		setNumberOfPlot(nbPlot);
		this.lineAnalysis = lineAnalysis;
	}

	public void setNumberOfPlot(int numberOfPlot) {
		ydatasFittedNotResampleInOriginArray = new Double1d[numberOfPlot];
		opFitArray = new OperationFit[numberOfPlot];
	}

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#fitCurrentClicked(java.util.EventObject)
	 */
	@Override
	public void fitCurrentClicked(EventObject ev) {
		if (spectrumView.getSeriesToFit() == null)
			return;

		OperationFit opFit = new OperationFit(spectrumView.getSeriesToFit());
		int numPlot = spectrumView.getNumCurrentSpectrum();
		opFitArray[numPlot] = opFit;
		int res = opFit.setDatastoFit(spectrumView.getListMarker());
		if (res == -1) {
			JOptionPane.showMessageDialog(compo, "No data in your selection", "Alert", JOptionPane.WARNING_MESSAGE);
		}
		else {
			FitResult fitResult = fitPanel.doFit(opFit.getXValueToFit(), opFit.getIntensityArray());
			display(fitResult, opFit, numPlot);
		}
	}

	//TODO Refactor to have a method less big
	private void display(FitResult result, OperationFit opFit, int numPlot) {
		AbstractModel model = result.getSumModel();
		double offsetX = result.getOffsetX();
		double multiX = result.getMultX();
		Double1d xdatas = opFit.getXValueData();
		if (model != null) {

			int oversampling = fitPanel.getOversample();
			Double1d xDatasResample = new Double1d(UtilArrayList.oversample(xdatas.getArray(), oversampling));

			// Compute y for sum fit
			Double1d ydatasFittedNotResample = model.result(xdatas.copy().add(offsetX).multiply(multiX));
			Double1d ydatasFittedResample = model.result(xDatasResample.add(offsetX).multiply(multiX));


			Double1d xDatasResampleInMhz = new Double1d();
			if (opFit.getXAxis().isInverted()) {
				for (int cpt = xDatasResample.getSize() - 1; cpt >= 0; cpt--) {
					xDatasResampleInMhz.append(opFit.getXAxis().convertToMHzFreq(xDatasResample.get(cpt)
					/ multiX - offsetX));
				}
			}
			else {
				for (int cpt = 0; cpt < xDatasResample.getSize(); cpt++) {
					xDatasResampleInMhz.append(opFit.getXAxis().convertToMHzFreq(xDatasResample.get(cpt)
					/ multiX - offsetX));
				}
			}

			//Convert Y data in orginal unit
			double [] ydatasFittedResampleTemp = ydatasFittedResample.toArray();
			if (opFit.getXAxis().isInverted()) {
				ydatasFittedResampleTemp = UtilArrayList.reverse(ydatasFittedResampleTemp);
			}
			Double1d ydatasFittedResampleInOrigin = new Double1d(YAxisCassis.convert(ydatasFittedResampleTemp, opFit.getYAxis(), opFit.getSpectrum().getyAxis(),
					xDatasResampleInMhz.getArray(), opFit.getSpectrum().getCassisMetadataList()));

			double [] ydatasFittedNotResampleTemp = ydatasFittedNotResample.toArray();
			if (opFit.getXAxis().isInverted()) {
				ydatasFittedNotResampleTemp = UtilArrayList.reverse(ydatasFittedNotResampleTemp);
			}
			Double1d ydatasFittedNotResampleInOrigin  = new Double1d(
					YAxisCassis.convert(ydatasFittedNotResampleTemp, opFit.getYAxis(), opFit.getSpectrum().getyAxis(),
							opFit.getSpectrum().getFrequenciesSignal(), opFit.getSpectrum().getCassisMetadataList()));


			YAxisCassis yAxisCassisOrigin = opFit.getSpectrum().getyAxis();
			List<CassisMetadata> cassisMetadatas = opFit.getSpectrum().getCassisMetadataList();

			this.ydatasFittedNotResampleInOriginArray[numPlot] = ydatasFittedNotResampleInOrigin;

			CommentedSpectrum spectrum;
			int sizeNewData = xDatasResampleInMhz.length();

			spectrum = new CommentedSpectrum(null, xDatasResampleInMhz.getArray(), ydatasFittedResampleInOrigin.getArray(), "Fit");
			spectrum.getCassisMetadataList().addAll(cassisMetadatas);
			spectrum.setxAxisOrigin(opFit.getXAxis());
			spectrum.setFreqRef(opFit.getSpectrum().getFreqRef());
			spectrum.setVlsr(opFit.getSpectrum().getVlsr());
			spectrum.getCassisMetadataList().add(new CassisMetadata(CassisMetadata.ORIGIN_DATA,
					opFit.getKey(), "name of the fitted curve", "none"));
			spectrum.setyAxis(yAxisCassisOrigin);
			XYSpectrumSeries serie = new XYSpectrumSeries("Fit", opFit.getXAxis(), opFit.getYAxis() ,TypeCurve.FIT, spectrum);
			serie.getConfigCurve().setColor(Color.RED);


			XYSpectrumSeries residualSerie = computeSubstractCurveFromFullSpectrum(InfoPanelConstants.FIT_RESIDUAL_TITLE,
					new Color(255, 102, 0), TypeCurve.FIT_RESIDUAL, numPlot);
			residualSerie.setToFit(false);


			// Compute y for sum Compo
			LinkedList<Double1d> ydatasCompoResample = new LinkedList<>();
			for (AbstractModel modelCompo : result.getComponents()) {
				ydatasCompoResample.add(modelCompo.result(xDatasResample));
			}
			LinkedList<Double1d> ydatasCompoResampleInOriginAxis = new LinkedList<>();

			for (int i = 0; i < ydatasCompoResample.size(); i++) {
				ydatasCompoResampleInOriginAxis.add( new Double1d(YAxisCassis.convert(ydatasCompoResample.get(i).getArray(),
						opFit.getYAxis(), opFit.getSpectrum().getyAxis(),
						xDatasResampleInMhz.getArray(), opFit.getSpectrum().getCassisMetadataList())));
			}
			List<XYSpectrumSeries> serieCompoFit = new ArrayList<>();
			int nbCompo = ydatasCompoResampleInOriginAxis.size();
			if (nbCompo > 1) {
				double[][] yCompo = new double[nbCompo][sizeNewData];
				if (opFit.getXAxis().isInverted()) {
					for (int i = 0; i < sizeNewData; i++) {
						for (int cpt = 0; cpt < nbCompo; cpt++)
							yCompo[cpt][i] = ydatasCompoResampleInOriginAxis.get(cpt).get(sizeNewData-i-1);
					}
				} else {
					for (int i = 0; i < sizeNewData; i++) {
						for (int cpt = 0; cpt < nbCompo; cpt++)
							yCompo[cpt][i] = ydatasCompoResampleInOriginAxis.get(cpt).get(i);
					}
				}

				Color[] colorCompo = { Color.BLUE, Color.green, Color.CYAN };
				Color color = null;
				for (int cpt = 0; cpt < nbCompo; cpt++) {
					color = colorCompo[cpt % colorCompo.length];
					spectrum = new CommentedSpectrum(null, xDatasResampleInMhz.getArray(), yCompo[cpt], "Fit " + (cpt + 1));
					spectrum.getCassisMetadataList().addAll(cassisMetadatas);
					spectrum.setVlsr(opFit.getSpectrum().getVlsr());
					spectrum.setxAxisOrigin(opFit.getXAxis());
					spectrum.setyAxis(yAxisCassisOrigin);
					spectrum.getCassisMetadataList().add(new CassisMetadata(CassisMetadata.ORIGIN_DATA,
							opFit.getKey(), "name of the fitted curve", "none"));
					XYSpectrumSeries serieComp = new XYSpectrumSeries("Fit" + (cpt + 1), opFit.getXAxis(), opFit.getYAxis(),
							TypeCurve.FIT_COMPO, spectrum);
					serieComp.getConfigCurve().setColor(color);
					serieCompoFit.add(serieComp);
				}
			}

			spectrumView.displayFitAndResidual(serie, residualSerie, serieCompoFit);
		}
	}

	/*
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#fitAllClicked(java.util.EventObject)
	 */
	@Override
	public void fitAllClicked(EventObject ev) {
		// No fit add in Full Spectrum.
	}

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#selectFileClicked(java.util.EventObject)
	 */
	@Override
	public boolean selectFileClicked(EventObject ev) {
		if (interfaceTelescope != null &&
				(interfaceTelescope.getTelescope() == null ||
					"???".equals(interfaceTelescope.getTelescope()) ||
					"????".equals(interfaceTelescope.getTelescope()))) {
			int retour = JOptionPane.showConfirmDialog(null,
					"You do not have selected a telescope. Do you want select one now?",
					"Telescope Error!", JOptionPane.YES_NO_CANCEL_OPTION);
			if (retour == JOptionPane.YES_OPTION) {
				JFileChooser loadFile = new CassisJFileChooser(Software.getTelescopePath(),
						Software.getLastFolder("lte_radex-model-telescope"));

				loadFile.setDialogTitle("Select a telescope file");
				int ret = loadFile.showOpenDialog(null);
				if (ret == JFileChooser.APPROVE_OPTION) {
					interfaceTelescope.setTelescope(loadFile.getSelectedFile().getAbsolutePath());
					Software.setLastFolder("lte_radex-model-telescope",
							loadFile.getSelectedFile().getParent());
				}
			} else if (retour == JOptionPane.CANCEL_OPTION || retour == JOptionPane.CLOSED_OPTION) {
				return false;
			}
		}

		String[] choice = { "Create a new File", "Open an existing File", "Cancel" };
		int choix = JOptionPane.showOptionDialog(fitPanel, "What do you want?", "Write the log in a file",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, choice, choice[0]);
		CassisJFileChooser fc = new CassisJFileChooser(Software.getDataPath(), Software.getLastFolder("fit"));
		fc.resetChoosableFileFilters();
		String ext = null;
		FileNameExtensionFilter filter = null;
		if (lineAnalysis) {
			ext = "rotd";
			filter = new FileNameExtensionFilter("Rotational Diagram file (*.rotd)", ext);
		} else {
			ext = "safit";
			filter = new FileNameExtensionFilter("Spectrum Analysis fit (*.safit)", ext);
		}
		fc.addChoosableFileFilter(filter);
		fc.setFileFilter(filter);

		if (choix >= 0) {
			if (choix == 1) { // open a file
				int returnVal = fc.showOpenDialog(fitPanel);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					selectedFile = fc.getSelectedFile();
					spectrumView.setLogParameters(fc.getSelectedFile(), true);
					Software.setLastFolder("fit", fc.getSelectedFile().getParent());
				}
			} else if (choix == 0) { // create a file
				createFile(fc, ext);
			} else if (choix == 2) {
				return false;
			}
		}
		if (selectedFile != null) {
			fitPanel.getSelectButton().setToolTipText(selectedFile.getPath());
		}
		return true;
	}

	private void createFile(JFileChooser fc, final String ext) {
		int returnVal = fc.showDialog(fitPanel, "Create");

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File temp = fc.getSelectedFile();
			String fileName = temp.getPath();
			if (!FileUtils.getFileExtension(fileName).equals(ext))
				fileName = fileName + "." + ext;

			File file = new File(fileName);

			// replace the existing file or not
			if (file.exists()) {
				String message = "This file " + file.toString() +
						" already exists.\nDo you want to replace it?";
				// Modal dialog with yes/no button
				int result = JOptionPane.showConfirmDialog(fitPanel, message,
						"Replace existing file?", JOptionPane.YES_NO_OPTION);
				if (result != JOptionPane.YES_OPTION) {
					file = null;
					selectedFile = null;
				}
			}
			if (file != null) {
				selectedFile = file;
				spectrumView.setLogParameters(file, false);
				Software.setLastFolder("fit", selectedFile.getParent());
			}
		}
	}

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#saveFitClicked(java.util.EventObject)
	 */
	@Override
	public void saveFitClicked(EventObject ev) {
		spectrumView.saveAllLineAnalysisInCurrentFile();
	}

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#substractFitClicked(java.util.EventObject)
	 */
	@Override
	public void substractFitClicked(EventObject ev) {
		if (!spectrumView.isOnGallery()) {
			subtractFit();
		}
	}

	private XYSpectrumSeries computeSubstractCurveFromFullSpectrum(String key, Color color, TypeCurve typecurve,
			int numberPlot) {
		OperationFit operationFit = opFitArray[numberPlot];
		String keySerie = key;
		if (keySerie == null) {
			keySerie = operationFit.getKey();
		}
		Color colorSerie = color;
		if (colorSerie == null)
			colorSerie = operationFit.getColor();

		TypeCurve typeSerie = typecurve;
		if (typeSerie == null)
			typeSerie = operationFit.getTypeCurve();

		final double[] frequencySignal = operationFit.getSpectrum().getFrequenciesSignal();
		int sizeNewData = frequencySignal.length;
		double[] x = new double[sizeNewData];
		double[] y = new double[sizeNewData];

		final double[] intensity = operationFit.getSpectrum().getIntensities();

		for (int i = 0; i < sizeNewData; i++) {
			x[i] = frequencySignal[i];
			y[i] = intensity[i] - ydatasFittedNotResampleInOriginArray[numberPlot].get(i);
		}

		CommentedSpectrum spectrum = new CommentedSpectrum(null,x, y, keySerie);
		spectrum.getCassisMetadataList().addAll(operationFit.getSpectrum().getCassisMetadataList());
		spectrum.setVlsr(operationFit.getSpectrum().getVlsr());
		spectrum.setxAxisOrigin(operationFit.getSpectrum().getxAxisOrigin());
		spectrum.setyAxis(operationFit.getSpectrum().getyAxis());
		spectrum.setFreqRef(operationFit.getSpectrum().getFreqRef());
		spectrum.setLoFreq(operationFit.getSpectrum().getLoFreq());
		spectrum.getCassisMetadataList().add(new CassisMetadata(CassisMetadata.ORIGIN_DATA,
				String.valueOf(operationFit.getKey()), "name of the fitted curve", "none"));

		XYSpectrumSeries newDataSerie = new XYSpectrumSeries(keySerie, operationFit.getXAxis(), operationFit.getYAxis(),
				typeSerie, spectrum);
		newDataSerie.getConfigCurve().setColor(colorSerie);

		return newDataSerie;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#divideByFitClicked(java.util.EventObject)
	 */
	@Override
	public void divideByFitClicked(EventObject object) {
		if (!spectrumView.isOnGallery()) {
			divideByFit();
		}
	}

	private XYSpectrumSeries computeDivideCurve() {
		XYSpectrumSeries dataserie = fitPanel.getCurrentSerie();
		AbstractModel fitModel = fitPanel.getLastFitResult().getSumModel();
		Double xOffset = fitPanel.getLastFitResult().getOffsetX();

		Double1d newxDatas = new Double1d();

		int sizeData = dataserie.getSpectrum().getSize();
		double[] xValues = dataserie.getSpectrum().getXData(dataserie.getXAxis());
		for (int cpt = 0; cpt < sizeData; cpt++) {
			newxDatas.append((xValues[cpt] + xOffset)
					* fitPanel.getLastFitResult().getMultX());
		}

		Double1d ydatas = fitModel.result(newxDatas);

		int sizeNewData = newxDatas.length();
		List<ChannelDescription> channels = new ArrayList<>();
		List<Double> velo = new ArrayList<>();
		List<Double> deltaVelo = new ArrayList<>();
		for (int i = 0; i < sizeNewData; i++) {
			ChannelDescription channelDescription = new ChannelDescription(0., 0., dataserie.getSpectrum()
					.getIntensities()[i] / ydatas.get(sizeNewData - i - 1), 0.);
			channels.add(channelDescription);

			velo.add(xValues[sizeNewData - i - 1]);
			deltaVelo.add(dataserie.getSpectrum().getDeltaV()[sizeNewData - i - 1]);
		}

		CommentedSpectrum spectrum = new CommentedSpectrum(null, channels, dataserie.getKey() + "-div");

		XYSpectrumSeries newDataSerie = new XYSpectrumSeries(dataserie.getKey() + "-div",
			dataserie.getXAxis(), dataserie.getTypeCurve(), spectrum);

		newDataSerie.getConfigCurve().setColor(dataserie.getConfigCurve().getColor());
		newDataSerie.setToFit(true);
		return newDataSerie;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#overlayFitClicked(java.util.EventObject)
	 */
	@Override
	public void overlayFitClicked(EventObject ev) {
		spectrumView.setSeriesVisible(fitPanel.isSelectedFit(), TypeCurve.FIT);
		spectrumView.setSeriesVisible(fitPanel.isSelectedFit(), TypeCurve.FIT_COMPO);
	}

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#overlayResidualClicked(java.util.EventObject)
	 */
	@Override
	public void overlayResidualClicked(EventObject ev) {
		spectrumView.setSeriesVisible(fitPanel.isSelectedResidual(), TypeCurve.FIT_RESIDUAL);
	}

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#historicFitClicked(java.util.EventObject)
	 */
	@Override
	public void historicFitClicked(EventObject ev) {
		spectrumView.setDataSeries(fitPanel.getCurrentSerie());
	}

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#displaySelectionClicked(java.util.EventObject)
	 */
	@Override
	public void displaySelectionClicked(EventObject ev) {
		if (ev.getSource() instanceof JCheckBox) {
			JCheckBox checkBox = (JCheckBox) ev.getSource();
			spectrumView.displaySelection(checkBox.isSelected());
		}
	}

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#resetLastSelectionClicked(java.util.EventObject)
	 */
	@Override
	public void resetLastSelectionClicked(EventObject ev) {
		spectrumView.resetLastSelection();
	}

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#resetAllSelectionClicked(java.util.EventObject)
	 */
	@Override
	public void resetAllSelectionClicked(EventObject ev) {
		spectrumView.resetAllSelection();
	}

	/**
	 * @param fitPanel
	 *            the fitPanel to set
	 */
	public final void setFitPanel(FitPanel fitPanel) {
		this.fitPanel = fitPanel;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#divideByFit()
	 */
	@Override
	public void divideByFit() {
		XYSpectrumSeries substractSerie = computeDivideCurve();
		fitPanel.updateHistory(substractSerie);
		spectrumView.setDataSeries(substractSerie);
	}

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#subtractFit()
	 */
	@Override
	public void subtractFit() {
		int currPlot = spectrumView.getNumCurrentSpectrum();
		if (!(opFitArray != null && ydatasFittedNotResampleInOriginArray != null &&
				opFitArray.length > currPlot && ydatasFittedNotResampleInOriginArray.length > currPlot &&
				opFitArray[currPlot] != null && ydatasFittedNotResampleInOriginArray[currPlot] != null)) {
			return;
		}

		XYSpectrumSeries substractSerie = computeSubstractCurveFromFullSpectrum(null, null, null, currPlot);
		substractSerie.setToFit(true);
		substractSerie.getSpectrum().setListOfLines(opFitArray[currPlot].getSpectrum().getListOfLines());
		fitPanel.updateHistory(substractSerie);
		spectrumView.setDataSeries(substractSerie);
	}

	/**
	 * @see eu.omp.irap.cassis.gui.fit.FitPanelListener#addIntervalMarker(eu.omp.irap.cassis.gui.plot.util.InterValMarkerCassis)
	 */
	@Override
	public void addIntervalMarker(InterValMarkerCassis marker) {
		spectrumView.addIntervalMarker(marker);
	}
}
