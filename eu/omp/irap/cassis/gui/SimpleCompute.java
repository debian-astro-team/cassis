/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.cassisd.Server;
import eu.omp.irap.cassis.cassisd.ServerImpl;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.ParameterDescription;
import eu.omp.irap.cassis.common.RadexException;
import eu.omp.irap.cassis.database.access.UnknowMoleculeException;
import eu.omp.irap.cassis.file.FileManagerAsciiCassis;
import eu.omp.irap.cassis.gui.model.lineanalysis.LineAnalysisModel;
import eu.omp.irap.cassis.gui.model.lteradex.LteRadexModel;
import eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent.ComponentDescription;
import eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent.EmAbTabbedPanelModel;
import eu.omp.irap.cassis.lineanalysis.LineAnalysisParameters;
import eu.omp.irap.cassis.lineanalysis.LineAnalysisResult;
import eu.omp.irap.cassis.properties.Software;
import fr.jmmc.jmcs.logging.LoggingService;

/**
 * Simple class to run CASSIS to do only computation without GUI.
 *
 * @author M. Boiziot
 */
public class SimpleCompute {

	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleCompute.class);

	private static final int ALGO_INDEX = 0;

	// Names of usable algorithm.
	private static final String LTE_RADEX = "LTERADEX";
	private static final String LINE_ANALYSIS = "LINEANALYSIS";

	// Index for LTERADEX algorithm.
	private static final int LTE_RADEX_FILE_INDEX = 1;
	private static final int LTE_RADEX_SAVE_FILE_PATH_INDEX = 2;

	// Index for Line Analysis algorithm.
	private static final int LA_FILE_INDEX = 1;
	private static final int LA_SAVE_FILE_DATA_PATH_INDEX = 2;
	private static final int LA_SAVE_FILE_MODEL_PATH_INDEX = 3;

	// Index for return code
	private static final int RETURN_CODE_ERROR = 1;
	private static final int RETURN_CODE_SUCCESS = 0;


	/**
	 * Main function. Handle the arguments to handle the correct function.
	 *
	 * @param args The arguments.
	 */
	public static void main(String[] args) {
		if (args == null || args.length == 0) {
			LOGGER.error("No arguments. Exiting");
			exit(RETURN_CODE_ERROR);
		}

		initSystem();

		String algo = args[ALGO_INDEX];
		if (LTE_RADEX.equalsIgnoreCase(algo)) {
			if (haveEnoughArgs(args, 3)) {
				computeLteRadex(args);
			} else {
				LOGGER.error("Not enough arguments for LTERADEX. Exiting");
				exit(RETURN_CODE_ERROR);
			}
		} else if (LINE_ANALYSIS.equalsIgnoreCase(algo)) {
			if (haveEnoughArgs(args, 4)) {
				computeLineAnalysis(args);
			} else {
				LOGGER.error("Not enough arguments for LINEANALYSIS. Exiting");
				exit(RETURN_CODE_ERROR);
			}
		} else {
			LOGGER.error("Wrong algorithm called");
			exit(RETURN_CODE_ERROR);
		}
	}

	/**
	 * Check then return if the given args array have enough arguments.
	 *
	 * @param args The arguments array.
	 * @param neededArgs The number of needed arguments
	 * @return true if there is enough arguments, false otherwise.
	 */
	private static boolean haveEnoughArgs(String[] args, int neededArgs) {
		if (args.length < neededArgs) {
			return false;
		}
		return true;
	}

	/**
	 * Compute an LTE-RADEX model then save a file.
	 *
	 * @param args The arguments. Need a least 3 args:
	 *  0: is the algorithm to use.
	 *  1: is the configuration file to use for LTERADEX.
	 *  2: is the path to the file to save.
	 */
	private static void computeLteRadex(String[] args) {
		String configFilePath = args[LTE_RADEX_FILE_INDEX];
		String fileSavePath = args[LTE_RADEX_SAVE_FILE_PATH_INDEX];
		if (!new File(configFilePath).exists()) {
			LOGGER.error("The given config file can not be found. Exiting.");
			exit(RETURN_CODE_ERROR);
		}
		CommentedSpectrum result = runLteRadex(configFilePath);

		FileManagerAsciiCassis fileManager = new FileManagerAsciiCassis(null, false);
		if (fileManager.save(new File(fileSavePath), result)) {
			exit(RETURN_CODE_SUCCESS);
		} else {
			exit(RETURN_CODE_ERROR);
		}
	}

	/**
	 * Run LTE-RADEX.
	 *
	 * @param configFilePath The path of the LTE-RADEX configuration file.
	 * @return the CommentedSpectrum of LTE-RADEX.
	 */
	private static CommentedSpectrum runLteRadex(String configFilePath) {
		LteRadexModel lteRadexModel = new LteRadexModel();
		try {
			lteRadexModel.loadConfig(configFilePath);
		} catch (IOException e) {
			LOGGER.error("Error in the given config file. Exiting.");
			exit(RETURN_CODE_ERROR);
		}
		Server server = new ServerImpl();
		CommentedSpectrum result = null;
		try {
			result = server.computeLteRadexSpectrum(lteRadexModel.getMapParameter(),
					lteRadexModel.getEmAbTabbedPanelModel().getComponentsWithMoleculeSelected(),
					lteRadexModel.getParametersModel().getTelescope());
		} catch (UnknowMoleculeException ume) {
			LOGGER.error("A molecule (tag: {}) is unknown, stopping the operation",
					ume.getTag(), ume);
			exit(RETURN_CODE_ERROR);
		} catch (RadexException re) {
			LOGGER.error("A RadexException occured while invoking the spectrum computation",
					re);
			exit(RETURN_CODE_ERROR);
		} catch (Exception e) {
			LOGGER.error("Error while invoking the spectrum computation", e);
			exit(RETURN_CODE_ERROR);
		}
		return result;
	}

	/**
	 * Compute a line analysis model then save a file.
	 *
	 * @param args The arguments. Need a least 4 args:
	 *  0: is the algorithm to use (not used here).
	 *  1: is the configuration file to use for LTE-RADEX.
	 *  2: is the path to the file to save the data.
	 *  3: us the path to the file to save the model.
	 */
	private static void computeLineAnalysis(String[] args) {
		String configFilePath = args[LA_FILE_INDEX];
		String fileDataSavePath = args[LA_SAVE_FILE_DATA_PATH_INDEX];
		String fileModelSavePath = args[LA_SAVE_FILE_MODEL_PATH_INDEX];

		if (!new File(configFilePath).exists()) {
			LOGGER.error("The given config file can not be found. Exiting.");
			exit(RETURN_CODE_ERROR);
		}

		LineAnalysisResult laResult = runLa(configFilePath);
		if (laResult != null) {
			FileManagerAsciiCassis fileManager = new FileManagerAsciiCassis(null, false);
			if (fileManager.saveOnGallery(new File(fileDataSavePath),
					laResult.getFileSpectrumList())
					&& fileManager.saveOnGallery(new File(fileModelSavePath),
							laResult.getSumSpectrumList())) {
				exit(RETURN_CODE_SUCCESS);
			} else {
				exit(RETURN_CODE_ERROR);
			}
		}
		exit(RETURN_CODE_ERROR);
	}

	/**
	 * Run line analysis.
	 *
	 * @param configFilePath The path of the line analysis configuration file.
	 * @return The LineAnalysisResult.
	 */
	private static LineAnalysisResult runLa(String configFilePath) {
		LineAnalysisModel laModel = new LineAnalysisModel();
		try {
			laModel.loadConfig(configFilePath);
		} catch (IOException e) {
			LOGGER.error("Error in the given config file. Exiting.");
			exit(RETURN_CODE_ERROR);
		}

		CassisSpectrum cassisSpectrum = laModel.getLoadDataModel().getCassisSpectrum();
		if (cassisSpectrum == null) {
			LOGGER.error("No data or data file not found");
			return null;
		}

		EmAbTabbedPanelModel lteRadexModel = laModel.getAbsorptionModel();
		List<ComponentDescription> componentsDescription = null;
		if (laModel.isLteRadexSelected()) {
			componentsDescription = lteRadexModel.getComponentsWithMoleculeSelected();
		}

		Map<String, ParameterDescription> mapParameter = laModel.getMapParameter();
		Server server = new ServerImpl();
		LineAnalysisResult result = null;
		boolean umeException = false;
		boolean radexException = false;
		try {
			LineAnalysisParameters parameters  = new LineAnalysisParameters(mapParameter,
					componentsDescription, laModel.getMoleculesSelected(),
					laModel.getParametersModel().getTelescope());
			result = server.computeLineAnalysisSpectrum(parameters, cassisSpectrum);
		} catch (UnknowMoleculeException ume) {
			umeException = true;
			LOGGER.error("A molecule (tag: {}) is unknown, stopping the operation",
					ume.getTag(), ume);
		} catch (RadexException re) {
			radexException = true;
			LOGGER.error("A RadexException occured while invoking the spectrum computation",
					re);
		} catch (Exception e) {
			LOGGER.error("Error while invoking the spectrum computation", e);
		}

		if (!umeException && !radexException && result == null) {
			System.err.println("Impossible to compute the spectrum");
		} else if (result != null) {
			result.setTelescope(laModel.getLoadDataModel().getTelescope());
		}
		return result;
	}

	/**
	 * Initialize the system.
	 */
	private static void initSystem() {
		Software.setApp();
		LoggingService.getInstance("eu/omp/irap/cassis/logger/ReleaseConfiguration.xml");
	}

	/**
	 * Stop the JVM.
	 *
	 * @param returnCode The return code to send.
	 */
	private static void exit(int returnCode) {
		System.exit(returnCode);
	}
}
