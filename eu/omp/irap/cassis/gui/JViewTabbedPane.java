/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.WindowConstants;

import eu.omp.irap.cassis.gui.plot.CassisViewInterface;
import eu.omp.irap.cassis.gui.util.HelpPopupMenu;
import eu.omp.irap.cassis.properties.Software;

/**
 * @author glorian
 *
 */
@SuppressWarnings("serial")
public class JViewTabbedPane extends JTabbedPane {

	private CassisViewInterface[] cassisView;
	private Component[] listCurrentView;
	private JFrame[] listframe;
	private HelpPopupMenu[] listHelpPopupMenu;


	public JViewTabbedPane() {
		super();
	}

	private void initialyse(CassisViewInterface[] listView) {
		cassisView = listView;
		listCurrentView = new Component[listView.length];
		listHelpPopupMenu = new HelpPopupMenu[listView.length];
		listframe = new JFrame[listView.length];
		for (int cpt = 0; cpt < listView.length; cpt++) {
			listframe[cpt] = null;
			listCurrentView[cpt] = listView[cpt].getComponent();
			listHelpPopupMenu[cpt] = getHelpPopuMenu(cassisView[cpt], cpt);
		}

		refreshTab();
	}

	private void refreshTab() {
		int index = getSelectedIndex();
		removeAll();
		for (int cpt = 0; cpt < cassisView.length; cpt++) {
			addTab(cassisView[cpt].getTitle(), listCurrentView[cpt]);
		}
		if (index >= 0)
			setSelectedIndex(index);
		invalidate();
	}

	private void attachView(int i) {
		setSelectedIndex(i);
		listCurrentView[i] = cassisView[i].getComponent();

		refreshTab();
		if (listframe[i] != null)
			listframe[i].dispose();
		listframe[i] = null;
	}

	private void detachView(int indice) {
		listCurrentView[indice] = new JPanel();
		refreshTab();
	}

	private HelpPopupMenu getHelpPopuMenu(CassisViewInterface listView, final int i) {
		String url = listView.getHelpUrl();
		url = "file://" + Software.getHelpPath() + File.separator + url;
		HelpPopupMenu menu = new HelpPopupMenu("PopuView");
		menu.add(new JSeparator());
		menu.add(HelpPopupMenu.getNewHelpPopuMenuItem(url));
		menu.add(new JSeparator());

		final JMenuItem detachItem = new JMenuItem("Detach");
		menu.add(detachItem);
		final JMenuItem attach = new JMenuItem("Attach");
		menu.add(attach);
		attach.setEnabled(false);

		detachItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				detachView(i);
				detachItem.setEnabled(false);
				attach.setEnabled(true);
				JFrame frame = null;
				if (listframe[i] == null) {
					listframe[i] = new JFrame(cassisView[i].getTitle());
					frame = listframe[i];
					frame.addWindowListener(new WindowAdapter() {

						/**
						 * (non-Javadoc)
						 *
						 * @see
						 * java.awt.event.WindowAdapter#windowClosed(java.awt
						 * .event.WindowEvent)
						 */
						@Override
						public void windowClosed(WindowEvent e) {
							attachView(i);
							attach.setEnabled(false);
							detachItem.setEnabled(true);
						}
					});
					frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
					frame.setContentPane((Container) cassisView[i]);
					frame.setVisible(true);
					frame.pack();
				}
			}
		});

		attach.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				attachView(i);
				detachItem.setEnabled(true);
				attach.setEnabled(false);
			}
		});
		return menu;
	}

	public HelpPopupMenu getListHelpPopupMenu(int cpt) {
		return listHelpPopupMenu[cpt];
	}

	@Override
	public void setSelectedComponent(Component c) {
		int index = indexOfComponent(c);
		if (index != -1) {
			setSelectedIndex(index);
		}
		else {
			if (c instanceof CassisViewInterface) {
				CassisViewInterface[] views = null;
				CassisViewInterface view = (CassisViewInterface) c;
				if (cassisView == null) {
					views = new CassisViewInterface[1];
					views[0] = view;
				} else {
					int size = cassisView.length + 1;
					views = new CassisViewInterface[size];
					System.arraycopy(cassisView, 0, views, 0, cassisView.length);

					views[size - 1] = view;
				}
				initialyse(views);
			}
			setSelectedComponent(c);
		}
	}

}
