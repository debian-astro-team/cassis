/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.database.access.InfoDataBase;
import eu.omp.irap.cassis.gui.help.CassisUpdateAutomatic;
import eu.omp.irap.cassis.gui.menu.CassisToolBar;
import eu.omp.irap.cassis.gui.menu.action.SpectrumManagerAction;
import eu.omp.irap.cassis.gui.util.CassisRessource;
import eu.omp.irap.cassis.properties.Software;
import eu.omp.irap.cassis.scripts.JythonFrame;

public class PanelFrame extends JFrame implements Printable {

	private static final long serialVersionUID = -4015578643875499416L;
	private static final Logger LOGGER = LoggerFactory.getLogger(PanelFrame.class);
	private PanelView panelView;
	private static PanelFrame panelFrame;
	public static final int WIDTH = 1020;
	public static final int HEIGHT = 700;


	protected PanelFrame() {
		LOGGER.debug("Creation Java View");
		panelView = PanelView.getInstance(this);

		setContentPane(panelView);

		setJMenuBar(panelView.getMenuBar());
		panelView.add(new CassisToolBar(), BorderLayout.NORTH);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		updateTitle();

		this.setIconImage(CassisRessource.getCassisIcon());

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				exitCassis();
			}

		});

		panelView.getSeparatorPanel().setVisible(false);
		pack();
		if (!Software.isOnlineMode())
			CassisUpdateAutomatic.checkUpdateAutomatic(false);

		panelView.getTabManager().addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				if (!panelView.getSeparatorPanel().isVisible()) {
					panelView.getSeparatorPanel().setVisible(true);
					pack();
					setSize(WIDTH, HEIGHT);
				}
			}
		});

		preloadJython();

		setVisible(true);
	}

	/**
	 * Preload some Jython class to avoid crash of the JVM when upgrading CASSIS.
	 */
	private void preloadJython() {
		try {
			if (!Software.isOnlineMode()) {
				ClassLoader.getSystemClassLoader().loadClass(
						"eu.omp.irap.cassis.scripts.JythonFrame");
				ClassLoader.getSystemClassLoader().loadClass(
						"eu.omp.irap.cassis.scripts.JythonView");
			}
		} catch (ClassNotFoundException cnfe) {
			LOGGER.warn("Can not load Jython class", cnfe);
		}
	}

	public static void exitCassis() {
		int answer;
		if (Software.getUserConfiguration().isTestMode()) {
			answer = JOptionPane.YES_OPTION;
		} else {
			JOptionPane jop = new JOptionPane("Do you want to exit?",
					JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION);
			JDialog dialog = jop.createDialog("Exit");
			dialog.setAlwaysOnTop(true);
			dialog.setVisible(true);
			Object value = jop.getValue();
			if(value instanceof Integer) {
				answer =  ((Integer)value).intValue();
			} else {
				answer = JOptionPane.CANCEL_OPTION;
			}
		}
		if (answer == JOptionPane.YES_OPTION) {
			exitCassisApp();
			System.exit(0);
		}
	}

	public static void exitCassisApp() {
		try {
			if (JythonFrame.isInstanced()) {
				JythonFrame.getInstance(null).getJythonView().saveAll(true);
			}
		} catch (Throwable t) {
			// Catching Throwable is bad, but less than an error at every update.
		}
		PanelFrame.getInstance().dispose();
		panelFrame = null;
		PanelView.clearPanelViewInstance();
		SpectrumManagerAction.clearSpectrumManagerInstance();
	}

	public PanelView getPanelView() {
		return panelView;
	}

	@Override
	public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
		if (pageIndex != 0)
			return NO_SUCH_PAGE;
		final Container container = this.getContentPane();
		final Dimension dim = container.getSize();
		final double scaleX = pageFormat.getImageableWidth() / dim.width;
		final double scaleY = pageFormat.getImageableHeight() / dim.height;
		final double scale = Math.min(scaleX, scaleY);
		final Graphics2D g2D = (Graphics2D) graphics;
		g2D.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
		g2D.scale(scale, scale);
		container.print(g2D);
		return PAGE_EXISTS;
	}

	public static PanelFrame getInstance() {
		if (panelFrame == null) {
			panelFrame = new PanelFrame();
		}
		return panelFrame;
	}

	public void updateTitle() {
		StringBuilder sb = new StringBuilder();
		sb.append(Software.getFriendlyVersion());
		sb.append(" - ");
		sb.append(InfoDataBase.getInfo());
		setTitle(sb.toString());
	}

}
