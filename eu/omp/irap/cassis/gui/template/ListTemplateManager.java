/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.template;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.event.EventListenerList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.common.Template;
import eu.omp.irap.cassis.properties.Software;
import eu.omp.irap.cassis.template.SqlTemplateManager;

public class ListTemplateManager {

	private static final Logger LOGGER = LoggerFactory.getLogger(ListTemplateManager.class);

	private EventListenerList listeners;
	private List<String> templateList;
	private static ListTemplateManager instance;

	private ListTemplateManager() {
		super();
		listeners = new EventListenerList();
	}

	public static ListTemplateManager getInstance() {
		if (instance == null)
			instance = new ListTemplateManager();
		return instance;
	}

	public void addListTemplateListener(ListTemplateListener listener) {
		listeners.add(ListTemplateListener.class, listener);
	}

	public void removeListTemplateListener(ListTemplateListener l) {
		listeners.remove(ListTemplateListener.class, l);
	}

	public boolean deleteTemplate(String template) {
		if (template.startsWith("Full") || Template.FULL_TEMPLATE.equals(template)) {
			LOGGER.info("You can't delete this template {}", template);
			return false;
		}

		File templateFile = new File(Software.getTemplateEnablePath() + File.separator + "template" + template + ".tec");
		if (templateFile.exists()) {
			File disableTemplateFolder = new File(Software.getTemplateDisablePath());
			if (!disableTemplateFolder.exists()) {
				LOGGER.debug("Disabled template folder does not exists");
				if (disableTemplateFolder.mkdirs()) {
					LOGGER.debug("Disabled template folder successfully created");
				} else {
					LOGGER.error("Unable to create the disabled template folder at {}",
							disableTemplateFolder.getAbsolutePath());
				}
			}
			boolean ismoved = templateFile.renameTo(new File(Software.getTemplateDisablePath() + File.separator
					+ "template" + template + ".tec"));
			if (ismoved) {
				templateList.remove(template);
				fireListTemplateChanged();
				LOGGER.debug("Template {} successfully removed", template);
				return true;
			}
		} else {
			LOGGER.warn("Cannot found the given template {} to delete", template);
		}
		LOGGER.error("Unable to delete the template {}", template);
		return false;
	}

	public void importTemplate(File templateFile) {
		String templateName;
		try {
			templateName = SqlTemplateManager.getInstance().importTemplate(templateFile);
		} catch (IOException ioe) {
			LOGGER.error("Error while importing template {}", templateFile.getAbsolutePath(), ioe);
			templateName = null;
		}

		if (templateName != null) {
			templateList.add(templateName);
			fireListTemplateChanged();
		}
	}

	public void fireListTemplateChanged() {
		ListTemplateListener[] listenerList = listeners.getListeners(ListTemplateListener.class);
		for (ListTemplateListener listener : listenerList) {
			listener.refreshListTemplate(new ListTemplateEvent(this, templateList));
		}
	}

	/**
	 * Refresh the templates.
	 */
	public void refreshTemplates() {
		templateList = null;
		templateList = SqlTemplateManager.getInstance().getTemplatesName();
		fireListTemplateChanged();
	}

	@SuppressWarnings("unchecked")
	public List<String> getTemplates() {
		if (templateList == null) {
			templateList = SqlTemplateManager.getInstance().getTemplatesName();
		}
		return (List<String>) ((ArrayList<String>) templateList).clone();
	}

	/**
	 * Check then return if the default template (Template.DEFAULT_TEMPLATE) exist.
	 *
	 * @return true if the default template exist, false otherwise.
	 */
	public boolean containsDefaultTemplate() {
		return getTemplates().contains(Template.DEFAULT_TEMPLATE) ||
				Template.DEFAULT_TEMPLATE == Template.FULL_TEMPLATE;
	}

	/**
	 * Check then return if the default template selected by the user exist.
	 *
	 * @return true if the default template selected by the user exist, false otherwise.
	 */
	public boolean containsUserDefaultTemplate() {
		String userDefaultTemplate = Software.getUserConfiguration().getUserDefaultTemplate();
		return getTemplates().contains(userDefaultTemplate)
				|| userDefaultTemplate.equals(Template.FULL_TEMPLATE);
	}

	/**
	 * @param templateName
	 *            : the name of the new template
	 * @param moleculesSelected
	 *            : the list of molecule selected to create the template
	 */
	public void addTemplate(String templateName, List<MoleculeDescription> moleculesSelected) {
		boolean ok = SqlTemplateManager.getInstance().createTemplate(templateName, moleculesSelected);
		if (ok) {
			templateList.add(templateName);
			fireListTemplateChanged();
		}
	}

	/**
	 * Return the default template to use, using by order of priority user selection,
	 *  default or full. This also check if the template exists before returning it.
	 *
	 * @return the default template to use.
	 */
	public String getDefaultTemplateToUse() {
		if (containsUserDefaultTemplate()) {
			return Software.getUserConfiguration().getUserDefaultTemplate();
		} else if (containsDefaultTemplate()) {
			return Template.DEFAULT_TEMPLATE;
		} else {
			return Template.FULL_TEMPLATE;
		}
	}
}
