/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.template;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JDialog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.Molecule;
import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.common.events.ListenerManager;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.gui.model.table.CassisTableAbstract;
import eu.omp.irap.cassis.gui.model.table.CassisTableAbstractModel;
import eu.omp.irap.cassis.gui.model.table.CassisTableMolecule;
import eu.omp.irap.cassis.gui.model.table.CassisTableMoleculeModel;
import eu.omp.irap.cassis.properties.Software;
import eu.omp.irap.cassis.template.SqlTemplateManager;

public class ManageTemplateModel extends ListenerManager {

	public static final String DENSITY_EVENT = "density";
	private static final Logger LOGGER = LoggerFactory.getLogger(ManageTemplateModel.class);

	private String lastTemplateAdded;
	private double density;
	private CassisTableAbstractModel<MoleculeDescription> selectedSpeciesTableModel;
	private CassisTableAbstractModel<MoleculeDescription> newSpeciesTableModel;
	private CassisTableAbstractModel<MoleculeDescription> ctam;
	private JDialog jd;


	/**
	 * Constructor.
	 */
	public ManageTemplateModel() {
		this.density = 1.;
	}

	public List<String> getTemplates() {
		return ListTemplateManager.getInstance().getTemplates();
	}

	public void setCTAM(CassisTableAbstractModel<MoleculeDescription> ctamMd) {
		this.ctam = ctamMd;
	}

	public CassisTableAbstractModel<MoleculeDescription> getCTAM() {
		return this.ctam;
	}

	public void setJDialog(JDialog jd) {
		this.jd = jd;
	}

	public JDialog getJDialog() {
		return jd;
	}

	/**
	 * Gets the list of the molecules of a template chosen.
	 *
	 * @param templateName
	 *            The name of the template
	 * @return The list of the molecules of the template chosen
	 */
	public List<MoleculeDescription> getTemplateMoleculeList(String templateName) {
		return SqlTemplateManager.getInstance().getTemplateMolecules(templateName);
	}

	/**
	 * Set a new List of {@link MoleculeDescription} and configure theirs
	 *  collisions files if necessary.
	 *
	 * @param moleculeArray The molecules list.
	 */
	public void setMoleculesList(List<MoleculeDescription> moleculeArray) {
		getSelectedSpeciesTableModel().getSourceList().clear();

		for (MoleculeDescription moleculeDescription : moleculeArray) {
			String moleculeColl;
			if (Software.getRadexMoleculesManager().isRADEXMolecules(Integer.toString(moleculeDescription.getTag()))) {
				moleculeColl = Software.getRadexMoleculesManager().getCollisionParDefault(moleculeDescription.getTag());
			} else {
				moleculeColl = Molecule.NO_COLLISION;
			}
			moleculeDescription.setCollision(moleculeColl);
		}
		getSelectedSpeciesTableModel().setList(moleculeArray);
		try {
			getSelectedSpeciesTableModel().fireTableDataChanged();
		}
		catch (Exception e) {
			LOGGER.error("Error while settig the new list of MoleculeDescription", e);
		}
	}

	/**
	 * Check if the database contains the tag.
	 *
	 * @param tag
	 *            tag to search
	 * @return true if the tag exists, false if not
	 */
	public boolean containsTag(int tag) {
		for (CassisTableAbstract<MoleculeDescription> moleculeTable : getNewSpeciesTableModel().getSourceList()) {
			if (moleculeTable.getLine().getTag() == tag) {
				return true;
			}
		}
		return false;
	}

	public boolean containsTag(int tag, List<MoleculeDescription> moles) {
		for (MoleculeDescription moleculeDescription : moles) {
			if (moleculeDescription.getTag() == tag) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets the description molecules list of the selected molecules from the top template.
	 *
	 * @return The description molecules list of the selected molecules
	 */
	public List<MoleculeDescription> getMoleculesSelected() {
		MoleculeDescription moleculeItem;
		List<CassisTableAbstract<MoleculeDescription>> sourceList = getSelectedSpeciesTableModel().getSourceList();
		List<MoleculeDescription> moleculesSelected = new ArrayList<>();

		for (int i = 0; i < sourceList.size(); i++) {
			moleculeItem = sourceList.get(i).getLine();
			if (moleculeItem.isCompute()) {
				moleculesSelected.add(moleculeItem);
			}
		}
		return moleculesSelected;
	}

	/**
	 * Gets the description molecules list of the selected molecules from the new template list (bottom).
	 *
	 * @return The description molecules list of the selected molecules
	 */
	public List<MoleculeDescription> getMoleculesSelectedFromNewTemplate() {
		List<MoleculeDescription> moleculesSelected = new ArrayList<>();
		List<CassisTableAbstract<MoleculeDescription>> newMoleculesist = getNewSpeciesTableModel()
				.getSourceList();

		for (CassisTableAbstract<MoleculeDescription> molec : newMoleculesist) {
			if (molec.getLine().isCompute()) {
				MoleculeDescription mol = (MoleculeDescription) molec.getLine().clone();
				moleculesSelected.add(mol);
			}
		}
		return moleculesSelected;
	}

	/**
	 * @return the density
	 */
	public double getDensity() {
		return density;
	}

	/**
	 * @param density : the density to set
	 */
	public void setDensity(double density) {
		this.density = density;
		fireDataChanged(new ModelChangedEvent(DENSITY_EVENT, density));
	}

	/**
	 * @return the lastTemplateAdded
	 */
	public String getLastTemplateAdded() {
		return lastTemplateAdded;
	}

	/**
	 * @param lastTemplateAdded : the lastTemplateAdded to set
	 */
	public void setLastTemplateAdded(String lastTemplateAdded) {
		this.lastTemplateAdded = lastTemplateAdded;
	}

	public void setSelectedSpeciesTableModel(CassisTableAbstractModel<MoleculeDescription> model) {
		selectedSpeciesTableModel = model;
	}

	public void setNewSpeciesTableModel(CassisTableAbstractModel<MoleculeDescription> model) {
		newSpeciesTableModel = model;
	}

	/**
	 * @return the selectedSpeciesTableModel
	 */
	public final CassisTableAbstractModel<MoleculeDescription> getSelectedSpeciesTableModel() {
		if (selectedSpeciesTableModel == null) {
			selectedSpeciesTableModel = new CassisTableMoleculeModel(
					new ArrayList<>(),
					new String[] { "Species", "Tag", "Database", "Coll", "C-Dens",
							"Abun", "Beta", "Tex", "TKin", "Fwhm", "Size",
							"<html>V<sub>exp</sub></html>", "Selected" },
					new Integer[] {
							CassisTableMolecule.NAME_INDICE, CassisTableMolecule.TAG_INDICE,
							CassisTableMolecule.DATA_SOURCE_INDICE, CassisTableMolecule.COLLISION_INDICE,
							CassisTableMolecule.DENSITY_INDICE, CassisTableMolecule.RELATIVE_ABUNDANCE_INDICE,
							CassisTableMolecule.BETA_INDICE, CassisTableMolecule.TEMPERATURE_INDICE,
							CassisTableMolecule.TKIN_INDICE, CassisTableMolecule.VELOCITY_DISPERSION_INDICE,
							CassisTableMolecule.SOURCE_SIZE_INDICE, CassisTableMolecule.VEXP_INDICE,
							CassisTableMolecule.COMPUTE_INDICE });
		}
		return selectedSpeciesTableModel;
	}


	/**
	 * @return the newSpeciesTableModel
	 */
	public final CassisTableAbstractModel<MoleculeDescription> getNewSpeciesTableModel() {
		return newSpeciesTableModel;
	}

	/**
	 * Set the given molecules in the newSpeciesTableModel.
	 *
	 * @param moles The molecules to set.
	 */
	public void setSelectedMoleculeList(List<MoleculeDescription> moles) {
		getNewSpeciesTableModel().getSourceList().clear();
		for (MoleculeDescription mol : moles) {
			if (mol.isCompute()) {
				MoleculeDescription clone = (MoleculeDescription) mol.clone();
				getNewSpeciesTableModel().add(clone);
			}
		}
		getNewSpeciesTableModel().fireTableDataChanged();
	}

}
