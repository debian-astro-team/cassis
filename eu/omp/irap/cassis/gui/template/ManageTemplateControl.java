/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.template;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.common.Template;
import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.database.access.InfoDataBase;
import eu.omp.irap.cassis.gui.model.table.CassisTableAbstract;
import eu.omp.irap.cassis.properties.Software;
import eu.omp.irap.cassis.template.SqlTemplateManager;

public class ManageTemplateControl implements ActionListener, ListSelectionListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(ManageTemplateControl.class);
	private ManageTemplateModel model;
	private static ManageTemplateControl instance = null;
	private JFrame templateFrame;
	private ManageTemplatePanel view;


	private ManageTemplateControl(ManageTemplateModel model) {
		this.model = model;
		view = new ManageTemplatePanel(this);
	}

	private ManageTemplateControl(ManageTemplateModel model, ManageTemplatePanel panel) {
		this.model = model;
		view = panel;
	}

	public static ManageTemplateControl getInstance() {
		if (instance == null) {
			ManageTemplateModel model = new ManageTemplateModel();
			instance = new ManageTemplateControl(model);
		}
		return instance;
	}

	public static boolean isInstanceExists() {
		return instance != null;
	}

	/**
	 * Get instance.
	 *
	 * @param model
	 *            {@link ManageTemplateModel}
	 * @return the controler
	 */
	public static ManageTemplateControl getInstance(ManageTemplateModel model) {
		if (instance == null) {
			instance = new ManageTemplateControl(model);
		}
		return instance;
	}

	public static ManageTemplateControl getInstance(ManageTemplatePanel panel) {
		if (instance == null) {
			instance = new ManageTemplateControl(new ManageTemplateModel(), panel);
		}
		return instance;
	}

	/**
	 * Refresh if needed the list of templates in the view.
	 */
	public static void refreshTemplates() {
		if (isInstanceExists() && getInstance().getView() != null) {
			JList<String> l = getInstance().getView().getTemplateListView();
			l.setListData(getInstance().getModel().getTemplates().toArray(
					new String[getInstance().getModel().getTemplates().size()]));
		}
	}

	/**
	 * @return the model
	 */
	public ManageTemplateModel getModel() {
		return model;
	}

	/**
	 * @return the view
	 */
	public ManageTemplatePanel getView() {
		return view;
	}

	public JFrame getManageTemplateControlFrame() {
		if (templateFrame == null) {
			templateFrame = new JFrame();
			templateFrame.setContentPane(view);
			templateFrame.pack();
			templateFrame.setSize(1016, 680);
			updateTitle();
		}
		return templateFrame;
	}

	public void updateTitle() {
		if (templateFrame == null) {
			return;
		}
		templateFrame.setTitle("Manage Templates - " + InfoDataBase.getInfo());
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == view.getAddSelectedSpeciesBtn()) {
			addSelectedSpecesBtnClicked();
		} else if (e.getSource() == view.getClearAllNewTemplate()) {
			clearAllNewTemplateClicked();
		} else if (e.getSource() == view.getSaveAsBtn()) {
			saveAsBtnClicked();
		} else if (e.getSource() == view.getDeleteBtn()) {
			deleteBtnClicked();
		} else if (e.getSource() == view.getImportBtn()) {
			importBtnClicked();
		} else if (e.getSource() == view.getExportBtn()) {
			exportBtnClicked();
		} else if (e.getSource() == view.getAddBtn()) {
			addBtnClicked();
		} else if (e.getSource() == view.getSetAsDefaultBtn()) {
			setAsDefaultBtnClicked();
		}
	}

	/**
	 * Handle click on "Use as default" button. Select the selected template as default.
	 */
	private void setAsDefaultBtnClicked() {
		List<String> selectedTemplatesList = view.getTemplateListView().getSelectedValuesList();
		if (selectedTemplatesList.isEmpty()) {
			JOptionPane.showMessageDialog(view,
					"One template must be selected to complete the operation.",
					"No template selected", JOptionPane.ERROR_MESSAGE);
			return;
		} else if (selectedTemplatesList.size() > 1) {
			JOptionPane.showMessageDialog(view,
					"Only one template can be selected to complete the operation.",
					"Template selection error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		String template = selectedTemplatesList.get(0);
		boolean change = !ListTemplateManager.getInstance()
				.getDefaultTemplateToUse().equals(template);
		if (change) {
			Software.getUserConfiguration().setUserDefaultTemplate(template);
			JOptionPane.showMessageDialog(view,
					"Please restart CASSIS for the change to take effect.",
					"Restart", JOptionPane.INFORMATION_MESSAGE);
		}
	}

	public void addBtnClicked() {
		List<CassisTableAbstract<MoleculeDescription>> newMoleculesist = model.getNewSpeciesTableModel()
				.getSourceList();

		List<CassisTableAbstract<MoleculeDescription>> targetList = model.getCTAM().getSourceList();

		for (CassisTableAbstract<MoleculeDescription> molec : newMoleculesist) {
			if (molec.getLine().isCompute() && !checkInList(targetList, molec.getLine())) {
				MoleculeDescription mol = (MoleculeDescription) molec.getLine().clone();
				model.getCTAM().add(mol);
			}
		}

		model.getCTAM().fireTableDataChanged();
		view.enableAddBtn(false);
		model.getJDialog().dispose();
	}

	private void exportBtnClicked() {
		List<String> selectedTemplateList = view.getTemplateListView().getSelectedValuesList();

		JFileChooser fc = new CassisJFileChooser(null, Software.getLastFolder("template"));
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int val = fc.showSaveDialog(view);
		File selectedFile = fc.getSelectedFile();

		if (selectedFile != null && !selectedFile.isDirectory()) {
			JOptionPane.showMessageDialog(view, "You must select a directory.",
					"Directory selection error", JOptionPane.ERROR_MESSAGE);
			return;
		}

		if (val == JFileChooser.APPROVE_OPTION && selectedFile != null) {
			Software.setLastFolder("template", selectedFile.getAbsolutePath());
			try {
				int numberFile = 0;
				for (int i = 0; i < selectedTemplateList.size(); i++) {
					File exportFile = new File(selectedFile, selectedTemplateList.get(i) + ".tec");
					if (exportFile.exists()) {
						String message = "Template file " + exportFile.getName()
								+ " already exists. Do you want to overwrite it?";
						int answer = JOptionPane.showConfirmDialog(view, message, "Delete the selected template?",
								JOptionPane.YES_NO_OPTION);
						if (answer == JOptionPane.YES_OPTION) {
							SqlTemplateManager.getInstance().exportTemplate(selectedTemplateList.get(i), exportFile);
							numberFile++;
						}
					} else {
						SqlTemplateManager.getInstance().exportTemplate(selectedTemplateList.get(i), exportFile);
						numberFile++;
					}
				}

				JOptionPane.showMessageDialog(view,
						numberFile + " templates exported to" + selectedFile.getAbsolutePath(), "Template Information",
						JOptionPane.INFORMATION_MESSAGE);
			} catch (IOException ioe) {
				LOGGER.error("Error while saving the template", ioe);
			}
		}
	}

	private void importBtnClicked() {
		JFileChooser fc = new CassisJFileChooser(null, Software.getLastFolder("template"));
		fc.setMultiSelectionEnabled(true);
		fc.setAcceptAllFileFilterUsed(false);
		fc.resetChoosableFileFilters();
		FileFilter ff = new FileNameExtensionFilter("Template CASSIS (*.tec)","tec");
		fc.addChoosableFileFilter(ff);
		fc.setFileFilter(ff);
		int val = fc.showOpenDialog(view);
		File[] selectedFiles = fc.getSelectedFiles();

		if (val == JFileChooser.APPROVE_OPTION && selectedFiles != null) {
			Software.setLastFolder("template", fc.getCurrentDirectory().getAbsolutePath());
			try {
				if (!importTemplate(selectedFiles)) {
					JOptionPane.showMessageDialog(view,
							"An error occured for the template import."
							+ " The template was not imported.",
							"Template import error", JOptionPane.ERROR_MESSAGE);
				}
			} catch (IOException e) {
				LOGGER.error("Error while importing template", e);
			}
		}
	}

	private void deleteBtnClicked() {
		if (view.getDeleteBtn().isEnabled()) {
			String message = "Do you want to delete the selected templates?";
			int answer = JOptionPane.showConfirmDialog(view, message, "Delete the selected template?",
					JOptionPane.YES_NO_OPTION);
			if (answer == JOptionPane.YES_OPTION) {
				List<String> selectedTemplatesList = view.getTemplateListView().getSelectedValuesList();

				// Manque la suppression de fichier template
				for (int i = 0; i < selectedTemplatesList.size(); i++) {
					ListTemplateManager.getInstance().deleteTemplate(selectedTemplatesList.get(i));
				}

				clearListSpeciesTemplateTable();
				clearAllNewTemplateClicked();

				view.getTemplateListView().removeListSelectionListener(this);
				view.getTemplateListView().setListData(model.getTemplates().toArray(new String[model.getTemplates().size()]));
				view.getTemplateListView().addListSelectionListener(this);
			}
		}
	}

	/**
	 * ClearAllNewTemplateClicked.
	 */
	private void clearAllNewTemplateClicked() {
		clearListSpeciesTemplateTable();
		try {
			model.getSelectedSpeciesTableModel().fireTableDataChanged();
		} catch (NullPointerException npe) {
			LOGGER.error("Error while clearing the table", npe);
		}
	}

	/**
	 * ClearListSpeciesTemplateTable.
	 */
	private void clearListSpeciesTemplateTable() {
		model.getNewSpeciesTableModel().getSourceList().clear();
		try {
			model.getNewSpeciesTableModel().fireTableDataChanged();
		} catch (NullPointerException npe) {
			LOGGER.error("Error while clearing the table", npe);
		}
	}

	/**
	 * Ask the user for a new template name then save it.
	 *
	 * @return true if the save succeeded, false otherwise.
	 */
	public boolean saveAsBtnClicked() {
		List<MoleculeDescription> moleculesSelected = model.getMoleculesSelectedFromNewTemplate();
		if (moleculesSelected.isEmpty()) {
			JOptionPane.showMessageDialog(view, "Saving an empty template is not possible.",
					"Template", JOptionPane.WARNING_MESSAGE);
			return false;
		}

		String templateName = (String) JOptionPane.showInputDialog(view, "Save template as:", "Save Template",
				JOptionPane.QUESTION_MESSAGE, null, null, null);
		if (templateName == null)
			return false;

		if (templateName.startsWith("Full") || Template.FULL_TEMPLATE.equals(templateName)) {
			LOGGER.error("You can't create a template beginning with 'Full'");
			return false;
		}

		templateName = templateName.replace(' ', '_');

		if (templateName != null && !"".equals(templateName)) {
			boolean templateExist = containsTemplate(templateName);

			if (templateExist) {
				String message1 = "Template " + templateName + " already exists.\n" + "Do you want to replace it?";
				int answer1 = JOptionPane.showConfirmDialog(view, "Replace existing template?", message1,
						JOptionPane.YES_NO_OPTION);
				if (answer1 == JOptionPane.NO_OPTION) {
					templateName = null;
					JOptionPane.showMessageDialog(view, "No template added", "Template Information",
							JOptionPane.INFORMATION_MESSAGE);
				}
			}
			if (templateName != null) {
				if (templateExist) {
					ListTemplateManager.getInstance().deleteTemplate(templateName);
				}

				ListTemplateManager.getInstance().addTemplate(templateName, moleculesSelected);
				model.setLastTemplateAdded(templateName);

				JOptionPane.showMessageDialog(view, "New template added", "Template Information",
						JOptionPane.INFORMATION_MESSAGE);

				view.getTemplateListView().removeListSelectionListener(this);
				view.getTemplateListView().setListData(model.getTemplates().toArray(new String[model.getTemplates().size()]));
				view.getTemplateListView().addListSelectionListener(this);
				view.getTemplateListView().setSelectedValue(templateName, true);
				return true;
			}
		}
		else {
			JOptionPane.showMessageDialog(view, "No template added", "Template Information",
					JOptionPane.INFORMATION_MESSAGE);
		}
		return false;
	}

	/**
	 * Check if it contains the given template.
	 *
	 * @param templateName : name of the template to search
	 * @return "true" if it exist, "false" if not
	 */
	public boolean containsTemplate(String templateName) {
		return ListTemplateManager.getInstance().getTemplates().contains(templateName);
	}

	/**
	 * AddSelectedSpecesBtnClicked.
	 */
	public void addSelectedSpecesBtnClicked() {
		List<CassisTableAbstract<MoleculeDescription>> selectedMoleculesist = model.getSelectedSpeciesTableModel()
				.getSourceList();
		List<CassisTableAbstract<MoleculeDescription>> newMoleculesist = model.getNewSpeciesTableModel()
				.getSourceList();

		for (CassisTableAbstract<MoleculeDescription> molec : selectedMoleculesist) {
			if (molec.getLine().isCompute() && !checkInList(newMoleculesist, molec.getLine())) {
				MoleculeDescription mol = (MoleculeDescription) molec.getLine().clone();
				model.getNewSpeciesTableModel().add(mol);
			}
		}

		if (!model.getNewSpeciesTableModel().getSourceList().isEmpty()) {
			model.getNewSpeciesTableModel().fireTableDataChanged();
		}
	}

	/**
	 * Check if the provided molecule is in the provided List.
	 *
	 * @param newMoleculesList The list.
	 * @param moleculeDescription The molecule.
	 * @return true if the molecule is in the list, false otherwise.
	 */
	private boolean checkInList(List<CassisTableAbstract<MoleculeDescription>> newMoleculesList,
			MoleculeDescription moleculeDescription) {
		for (CassisTableAbstract<MoleculeDescription> mol : newMoleculesList) {
			if (mol.getLine().getTag() == moleculeDescription.getTag())
				return true;
		}
		return false;
	}

	public boolean importTemplate(final File[] file) throws FileNotFoundException, IOException {
		boolean error = false;
		for (int i = 0; i < file.length; i++) {
			try (BufferedReader in = new BufferedReader(new FileReader(file[i]))) {
				String line = in.readLine();
				String templateName = "";

				if (line == null || !line.startsWith("@")) {
					LOGGER.info("File " + file[i].toString() + " invalid");
					error = true;
					continue;
				}

				templateName = line.substring(1);
				if (SqlTemplateManager.getInstance().isTemplateExist(templateName)) {
					String message = "The template " + templateName + " already exists.\nDo you want to replace it?";

					int answer = JOptionPane.showConfirmDialog(view, message, "Template Information",
							JOptionPane.YES_NO_CANCEL_OPTION);
					if (answer != JOptionPane.YES_OPTION) {
						LOGGER.info("Template " + templateName + " already exists and the user doesn't want to add it");
						continue;
					}
					ListTemplateManager.getInstance().deleteTemplate(templateName);
				}
				ListTemplateManager.getInstance().importTemplate(file[i]);

				view.getTemplateListView().removeListSelectionListener(this);
				view.getTemplateListView().setListData(model.getTemplates().toArray(new String[model.getTemplates().size()]));
				view.getTemplateListView().addListSelectionListener(this);
				view.getTemplateListView().setSelectedValue(templateName, true);
			}
		}
		return !error;
	}

	/**
	 * TemplateList changed event handle.
	 */
	@Override
	public void valueChanged(ListSelectionEvent e) {
		if (e.getSource() == view.getTemplateListView()) {
			templateListViewSelected();
		}
	}

	/**
	 * TemplateListViewSelected.
	 */
	private void templateListViewSelected() {
		List<String> templatesSelectedList = view.getTemplateListView().getSelectedValuesList();

		if (templatesSelectedList.isEmpty()) {
			view.getDeleteBtn().setEnabled(false);
		} else {
			if (templatesSelectedList.size() == 1) {
				String templateName = templatesSelectedList.get(0);
				if (templateName.startsWith("Full") || Template.FULL_TEMPLATE.equals(templateName)) {
					view.getDeleteBtn().setEnabled(false);
				} else {
					view.getDeleteBtn().setEnabled(true);
				}
			} else if (templatesSelectedList.size() > 1) {
				view.getDeleteBtn().setEnabled(true);
			}

			List<MoleculeDescription> moleculeList = new ArrayList<>();

			for (int i = 0; i < templatesSelectedList.size(); i++) {
				String templateName = templatesSelectedList.get(i);
				List<MoleculeDescription> molecules = SqlTemplateManager.getInstance().getTemplateMolecules(
						templateName);
				for (MoleculeDescription moleculeDescription : molecules) {
					if (!model.containsTag(moleculeDescription.getTag(), moleculeList)) {
						moleculeList.add(moleculeDescription);
						moleculeDescription.setCompute(true);
					}
				}
			}
			model.setMoleculesList(moleculeList);
		}
	}

	/**
	 * @param model
	 *            the model to set
	 */
	public void setModel(ManageTemplateModel model) {
		this.model = model;
	}

	public static void main(String[] args) {
		ManageTemplateModel manageTemplateModel = new ManageTemplateModel();
		ManageTemplateControl control = ManageTemplateControl
				.getInstance(manageTemplateModel);
		ManageTemplatePanel panel = control.getView();
		JFrame frame = new JFrame();
		frame.setContentPane(panel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.pack();
	}

}
