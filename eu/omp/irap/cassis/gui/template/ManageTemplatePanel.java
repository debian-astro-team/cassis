/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.template;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent.DensityActionListener;
import eu.omp.irap.cassis.gui.model.table.CassisTableAbstractModel;
import eu.omp.irap.cassis.gui.model.table.CassisTableMolecule;
import eu.omp.irap.cassis.gui.model.table.CassisTableMoleculeModel;
import eu.omp.irap.cassis.gui.model.table.JCassisTable;
import eu.omp.irap.cassis.gui.util.TextFieldFormatFilter;

public class ManageTemplatePanel extends JPanel implements ModelListener {

	private static final long serialVersionUID = 1927437401504558133L;
	private static final Logger LOGGER = LoggerFactory.getLogger(ManageTemplatePanel.class);
	public static final int DENSITY_COLUMN_INDEX = 5;
	public static final int ABUNDANCE_COLUMN_INDEX = 6;

	private ManageTemplateModel model;
	private ManageTemplateControl control;
	private JList<String> templateListView;
	private JButton buttonDelete;
	private JCassisTable<MoleculeDescription> selectedSpeciesTable;
	private JCassisTable<MoleculeDescription> newSpeciesTemplateTable;
	private JComponent listTemplWestPanel;
	private JSplitPane listsSpeciesPanel;
	private JComponent buttonsListTempPanel;
	private JPanel listsSelectedSpeciesPanel;
	private JPanel listSpeciesTemplPanel;
	private JComponent buttonsListSPeciesTemplPanel;
	private JPanel buttonsListSelectedSpeciesPanel;
	private JButton addSelectedSpeciesBtn;
	private JButton saveAsBtn;
	private JButton clearAllNewTemplate;
	private JButton importBtn;
	private JButton exportBtn;
	private JButton addBtn;
	private JLabel densityLabel;
	private JTextField densityField;
	private JButton setAsDefaultBtn;


	public ManageTemplatePanel() {
		this.control = ManageTemplateControl.getInstance(this);
		model = control.getModel();

		JSplitPane splitPaneWest = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, getListTemplWestPanel(),
				getListsSpeciesPanel());
		splitPaneWest.setOneTouchExpandable(true);
		this.setLayout(new BorderLayout());
		this.add(splitPaneWest, BorderLayout.CENTER);

		model.addModelListener(this);
	}

	public ManageTemplatePanel(ManageTemplateControl control) {
		this.control = control;
		model = control.getModel();

		JSplitPane splitPaneWest = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, getListTemplWestPanel(),
				getListsSpeciesPanel());
		splitPaneWest.setOneTouchExpandable(true);
		this.setLayout(new BorderLayout());
		this.add(splitPaneWest, BorderLayout.CENTER);

		model.addModelListener(this);
	}

	/**
	 * Change the model.
	 *
	 * @param model : the model to set
	 */
	public void setModel(ManageTemplateModel model) {
		this.model = model;
		model.addModelListener(this);
	}

	/**
	 * Return the model.
	 *
	 * @return the model.
	 */
	public ManageTemplateModel getModel() {
		return this.model;
	}

	private Component getListTemplWestPanel() {
		if (listTemplWestPanel == null) {
			listTemplWestPanel = new JPanel(new BorderLayout());
			listTemplWestPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
					"Templates of CASSIS"));
			JPanel panel = new JPanel(new BorderLayout());
			panel.add(new JScrollPane(getTemplateListView()), BorderLayout.CENTER);
			panel.add(getButtonsListTempPanel(), BorderLayout.SOUTH);
			listTemplWestPanel.add(panel, BorderLayout.CENTER);
		}
		return listTemplWestPanel;
	}

	private JComponent getButtonsListTempPanel() {
		if (buttonsListTempPanel == null) {
			buttonsListTempPanel = new JPanel(new GridLayout(2, 2, 5, 5));
			buttonsListTempPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
			buttonsListTempPanel.add(getImportBtn());
			buttonsListTempPanel.add(getExportBtn());
			buttonsListTempPanel.add(getDeleteBtn());
			buttonsListTempPanel.add(getSetAsDefaultBtn());
		}
		return buttonsListTempPanel;
	}

	/**
	 * Create if necessary then return the "Import" {@link JButton}.
	 *
	 * @return the "Import" {@link JButton}.
	 */
	public JButton getImportBtn() {
		if (importBtn == null) {
			importBtn = new JButton("Import");
			importBtn.addActionListener(control);
		}

		return importBtn;
	}

	/**
	 * GetExportBtn.
	 *
	 * @return the button to export the template
	 */
	public JButton getExportBtn() {
		if (exportBtn == null) {
			exportBtn = new JButton("Export");
			exportBtn.addActionListener(control);
		}

		return exportBtn;
	}

	private Component getListsSpeciesPanel() {
		if (listsSpeciesPanel == null) {
			listsSpeciesPanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT, getListSpeciesTemplPanel(),
					getListsSelectedSpeciesPanel());
			listsSpeciesPanel.setOneTouchExpandable(true);
		}
		return listsSpeciesPanel;
	}

	/**
	 * @return the table of templates
	 */
	public JCassisTable<MoleculeDescription> getNewSpeciesTemplateTable() {
		if (newSpeciesTemplateTable == null) {
			CassisTableAbstractModel<MoleculeDescription> tableModel = new CassisTableMoleculeModel(
					new ArrayList<>(),
					new String[] { "Species", "Tag", "Database", "Coll", "C-Dens",
							"Abun", "Beta", "Tex", "TKin", "Fwhm","Size",
							"<html>V<sub>exp</sub></html>", "Selected" },
					new Integer[] {
							CassisTableMolecule.NAME_INDICE, CassisTableMolecule.TAG_INDICE,
							CassisTableMolecule.DATA_SOURCE_INDICE, CassisTableMolecule.COLLISION_INDICE,
							CassisTableMolecule.DENSITY_INDICE, CassisTableMolecule.RELATIVE_ABUNDANCE_INDICE,
							CassisTableMolecule.BETA_INDICE, CassisTableMolecule.TEMPERATURE_INDICE,
							CassisTableMolecule.TKIN_INDICE, CassisTableMolecule.VELOCITY_DISPERSION_INDICE,
							CassisTableMolecule.SOURCE_SIZE_INDICE, CassisTableMolecule.VEXP_INDICE,
							CassisTableMolecule.COMPUTE_INDICE });
			this.model.setNewSpeciesTableModel(tableModel);
			newSpeciesTemplateTable = new JCassisTable<>(tableModel);
		}

		return newSpeciesTemplateTable;
	}

	/**
	 * Create if needed the return the species of the selected templates panel.
	 *
	 * @return the species of the selected templates panel.
	 */
	private JComponent getListSpeciesTemplPanel() {
		if (listSpeciesTemplPanel == null) {
			listSpeciesTemplPanel = new JPanel(new BorderLayout());
			listSpeciesTemplPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
					"Species of the selected templates"));
			JScrollPane scrollPanel = new JScrollPane(getSelectedSpeciesTable());
			getSelectedSpeciesTable().setScrollPane(scrollPanel);
			listSpeciesTemplPanel.setPreferredSize(new Dimension(400, 280));
			listSpeciesTemplPanel.add(scrollPanel, BorderLayout.CENTER);
			listSpeciesTemplPanel.add(getButtonsListSPeciesTemplPanel(), BorderLayout.SOUTH);
		}
		return listSpeciesTemplPanel;
	}

	private JComponent getButtonsListSPeciesTemplPanel() {
		if (buttonsListSPeciesTemplPanel == null) {
			buttonsListSPeciesTemplPanel = new JPanel();
			buttonsListSPeciesTemplPanel.add(getAddSelectedSpeciesBtn());
		}
		return buttonsListSPeciesTemplPanel;
	}

	public JButton getAddSelectedSpeciesBtn() {
		if (addSelectedSpeciesBtn == null) {
			addSelectedSpeciesBtn = new JButton("Add selected species to the new template");
			addSelectedSpeciesBtn.addActionListener(control);
		}

		return addSelectedSpeciesBtn;
	}

	/**
	 * GetSelectedSpeciesTable.
	 *
	 * @return theJCassisTable
	 */
	public JCassisTable<MoleculeDescription> getSelectedSpeciesTable() {
		if (selectedSpeciesTable == null) {
			selectedSpeciesTable = new JCassisTable<>(model.getSelectedSpeciesTableModel());
		}
		return selectedSpeciesTable;
	}

	private JComponent getListsSelectedSpeciesPanel() {
		if (listsSelectedSpeciesPanel == null) {
			listsSelectedSpeciesPanel = new JPanel(new BorderLayout());
			listsSelectedSpeciesPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
					"Species of the new template"));
			JScrollPane scrollPanel = new JScrollPane(getNewSpeciesTemplateTable());
			getNewSpeciesTemplateTable().setScrollPane(scrollPanel);
			listsSelectedSpeciesPanel.setPreferredSize(new Dimension(400, 280));
			listsSelectedSpeciesPanel.add(getParametresPanel(), BorderLayout.NORTH);
			listsSelectedSpeciesPanel.add(scrollPanel, BorderLayout.CENTER);
			listsSelectedSpeciesPanel.add(getButtonsListSlectedSpecies(), BorderLayout.SOUTH);
		}
		return listsSelectedSpeciesPanel;
	}

	private Component getParametresPanel() {
		JPanel parametresPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));

		densityLabel = new JLabel("<HTML><p>N(H<sub>2</sub>) [cm\u207B\u00B2] : </p></HTML>");
		densityField = getDensityField();
		densityLabel.setLabelFor(densityField);

		parametresPanel.add(densityLabel);
		parametresPanel.add(densityField);

		return parametresPanel;
	}

	private Component getButtonsListSlectedSpecies() {
		if (buttonsListSelectedSpeciesPanel == null) {
			buttonsListSelectedSpeciesPanel = new JPanel();
			buttonsListSelectedSpeciesPanel.add(getAddBtn());
			buttonsListSelectedSpeciesPanel.add(getClearAllNewTemplate());
			buttonsListSelectedSpeciesPanel.add(getSaveAsBtn());
		}
		return buttonsListSelectedSpeciesPanel;
	}

	public JButton getAddBtn() {
		if (addBtn == null) {
			addBtn = new JButton("Add");
			addBtn.setEnabled(false);
			addBtn.addActionListener(control);
		}
		return addBtn;
	}

	public void enableAddBtn(boolean state) {
		getAddBtn().setEnabled(state);
	}


	/**
	 * @return the saveAsBtn
	 */
	public JButton getSaveAsBtn() {
		if (saveAsBtn == null) {
			saveAsBtn = new JButton("Save as");
			saveAsBtn.addActionListener(control);
		}
		return saveAsBtn;
	}

	/**
	 * @return the clearAllNewTemplate
	 */
	public JButton getClearAllNewTemplate() {
		if (clearAllNewTemplate == null) {
			clearAllNewTemplate = new JButton("Clear the new template");
			clearAllNewTemplate.addActionListener(control);
		}
		return clearAllNewTemplate;
	}

	/**
	 * @return the templateListView
	 */
	public JList<String> getTemplateListView() {
		if (templateListView == null) {
			templateListView = new JList<>(model.getTemplates().toArray(
					new String[model.getTemplates().size()]));
			templateListView.setVisibleRowCount(10);
			templateListView.setBorder(BorderFactory.createEmptyBorder(5, 5, 2, 2));
			templateListView.addListSelectionListener(control);
		}
		return templateListView;
	}

	/**
	 * @return the buttonDelete
	 */
	public JButton getDeleteBtn() {
		if (buttonDelete == null) {
			buttonDelete = new JButton("Delete");
			buttonDelete.addActionListener(control);
		}
		return buttonDelete;
	}

	/**
	 * @return the densityField
	 */
	public JTextField getDensityField() {
		if (densityField == null) {
			densityField = new JTextField(String.valueOf(model.getDensity()), 4);
			densityField.setAlignmentX(RIGHT_ALIGNMENT);
			densityField.addKeyListener(new TextFieldFormatFilter(TextFieldFormatFilter.SCIENTIFIC_NUMBER));
			densityField.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					String s = densityField.getText();
					if (!s.isEmpty()) {
						try {
							double d = Double.parseDouble(s);
							model.setDensity(d);
						} catch (NumberFormatException nfe) {
							LOGGER.error("The density must be a number", nfe);
						}
					}
				}
			});
			densityField.addActionListener(new DensityActionListener(newSpeciesTemplateTable));
		}
		return densityField;
	}

	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (ManageTemplateModel.DENSITY_EVENT.equals(event.getSource())) {
			densityField.setText(event.getValue().toString());
		}
	}

	public ManageTemplateControl getControl() {
		return control;
	}

	/**
	 * Create if needed then return the "Set as default" button.
	 *
	 * @return the "Set as default" button.
	 */
	public JButton getSetAsDefaultBtn() {
		if (setAsDefaultBtn == null) {
			setAsDefaultBtn = new JButton("Set as default");
			setAsDefaultBtn.addActionListener(control);
		}
		return setAsDefaultBtn;
	}
}
