/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.template;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

import eu.omp.irap.cassis.common.Template;

/**
 * @author glorian
 *
 */
@SuppressWarnings("serial")
public class JComboBoxTemplate extends JComboBox<String> {

	private boolean withCommands = false;
	private boolean withChangeDatabase = false;
	public static final String TEMPLATE = "-- Template --";
	public static final String SAVE_TEMPLATE = "Save Template";
	public static final String OPERATIONS_TEMPLATE = "-- Operations --";
	public static final String TEMPLATE_MANAGER = "Template Manager";
	public static final String CHANGE_DATABASE = "Change Database";

	/**
	 * Create a JComboBoxTemplate
	 *
	 * @param withCommands : if true, add commands to save or manage template
	 * @param withChangeDatabase : if true add the possibility to have a link
	 * to change the database
	 */
	public JComboBoxTemplate(boolean withCommands, boolean withChangeDatabase) {
		super();
		this.withCommands = withCommands;
		this.withChangeDatabase  = withChangeDatabase;
		refreshList();
		setVisible(true);
	}

	/**
	 * Create a JComboBoxTemplate
	 *
	 * @param withCommands : if true, add commands to save or manage template
	 */
	public JComboBoxTemplate(boolean withCommands) {
		this(withCommands, false);
	}

	public void refreshList() {
		ListTemplateManager templateManager = ListTemplateManager.getInstance();
		List<String> listTemplates = templateManager.getTemplates();
		Collections.sort(listTemplates);
		refreshList(listTemplates);
	}

	public void refreshList(List<String> listTemplates) {
		List<String> templateList = new ArrayList<>();
		if (listTemplates.contains(Template.FULL_TEMPLATE)) {
			templateList.add(Template.FULL_TEMPLATE);
		}
		for (String template : listTemplates) {
			if (template.startsWith("Full") && !Template.FULL_TEMPLATE.equals(template))
				templateList.add(template);
		}
		for (String template : listTemplates) {
			if (!template.startsWith("Full")&& !Template.FULL_TEMPLATE.equals(template))
				templateList.add(template);
		}
		int indiceTemplate = 0;
		if (withCommands || withChangeDatabase) {
			templateList.add(0, JComboBoxTemplate.OPERATIONS_TEMPLATE);
			indiceTemplate = 1;
			if (withChangeDatabase) {
				templateList.add(1, JComboBoxTemplate.CHANGE_DATABASE);
				indiceTemplate++;
			}
			if (withCommands) {
				templateList.add(indiceTemplate, JComboBoxTemplate.SAVE_TEMPLATE);
				templateList.add(indiceTemplate+1, JComboBoxTemplate.TEMPLATE_MANAGER);
				indiceTemplate = indiceTemplate+2;
			}
		}
		templateList.add(indiceTemplate, JComboBoxTemplate.TEMPLATE);

		setModel(new DefaultComboBoxModel<>(templateList.toArray(
				new String[templateList.size()])));
	}

}
