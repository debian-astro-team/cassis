/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.merge;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import eu.omp.irap.cassis.properties.Software;

/**
 * Create the {@link JDialog} for the import. This is the window who will
 *  interact with the {@link ImportWorker} during the merge operation.
 *
 * @author M. Boiziot
 */
public class ImportDialog extends JDialog implements ImportPanelInterface {

	private static final long serialVersionUID = 8072929393981957210L;
	private int approxNbElement;
	private ImportWorker worker;
	private JTextArea logArea;
	private JButton okButton;
	private JButton cancelButton;
	private JProgressBar progressBar;


	/**
	 * Creates a {@link ImportDialog}.
	 *
	 * @param oldDirectory The old CASSIS directory.
	 * @param newDirectory The new (current) CASSIS directory.
	 * @param elementsToImport The map containing the elements to import with
	 *  usual name for keys and directory for values.
	 */
	public ImportDialog(File oldDirectory, File newDirectory, Map<String, String> elementsToImport) {
		super((Frame) null, true);
		setTitle("Importing...");
		computeElement(elementsToImport);
		createUi();
		pack();
		createWorker(oldDirectory, newDirectory, elementsToImport);
	}

	/**
	 * Do a lazy compute of the number of elements to import then update value
	 *  in the object.
	 *
	 * @param elementsToImport The element to import.
	 */
	private void computeElement(Map<String, String> elementsToImport) {
		int nbElement = elementsToImport.size();
		if (elementsToImport.containsKey("properties")) {
			nbElement++;
		}
		this.approxNbElement = nbElement;
	}

	/**
	 * Creates if needed then return the {@link JProgressBar}.
	 *
	 * @return the {@link JProgressBar}.
	 */
	public JProgressBar getProgressBar() {
		if (progressBar == null) {
			progressBar = new JProgressBar(0, this.approxNbElement);
		}
		return progressBar;
	}

	/**
	 * Creates if needed then return the "Cancel" {@link JButton}.
	 *
	 * @return the "Cancel" {@link JButton}.
	 */
	public JButton getCancelButton() {
		if (cancelButton == null) {
			cancelButton = new JButton("Cancel");
			cancelButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					worker.cancel(true);
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							logArea.append("Stopping import.");
							ImportDialog.this.done();
							ImportDialog.this.setTitle("Import cancelled.");
						}
					});
				}
			});
		}
		return cancelButton;
	}

	/**
	 * Creates if needed then return the "Ok" {@link JButton}.
	 *
	 * @return the "Ok" {@link JButton}.
	 */
	public JButton getOkButton() {
		if (okButton == null) {
			okButton = new JButton("Ok");
			okButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					ImportDialog.this.dispose();
				}
			});
		}
		return okButton;
	}

	/**
	 * Creates if needed then return the log area.
	 *
	 * @return the log area.
	 */
	public JTextArea getLogArea() {
		if (logArea == null) {
			logArea = new JTextArea();
			logArea.setEditable(false);
		}
		return logArea;
	}

	/**
	 * Create the graphical user interface of the dialog.
	 */
	private final void createUi() {
		Container container = this.getContentPane();
		container.setLayout(new BorderLayout());

		container.add(getProgressBar(), BorderLayout.NORTH);
		JScrollPane scrollPane = new JScrollPane(getLogArea());
		scrollPane.setPreferredSize(new Dimension(800, 400));
		container.add(scrollPane, BorderLayout.CENTER);
		container.add(getCancelButton(), BorderLayout.SOUTH);
	}

	/**
	 * Create the worker <b>without</b> starting it.
	 *
	 * @param oldDirectory The old CASSIS directory.
	 * @param newDirectory The new (current) CASSIS directory.
	 * @param elementsToImport The map containing the elements to import as
	 *   <Usual name, directory>.
	 */
	private final void createWorker(File oldDirectory, File newDirectory,
			Map<String, String> elementsToImport) {
		worker = new ImportWorker((ImportPanelInterface)this, oldDirectory, newDirectory, elementsToImport);
		worker.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if ("progress".equals(evt.getPropertyName())) {
					int val = ((Integer)evt.getNewValue()).intValue();
					getProgressBar().getModel().setValue(val);
				}
			}
		});
	}

	/**
	 * @see eu.omp.irap.cassis.gui.merge.ImportPanelInterface#done()
	 */
	@Override
	public void done() {
		Container container = this.getContentPane();
		this.setTitle("Import finished");
		addLog("\n\nThe changes will be taken into account after the restart of CASSIS.");
		getProgressBar().setValue(getProgressBar().getMaximum());
		container.remove(getCancelButton());
		container.add(getOkButton(), BorderLayout.SOUTH);
		container.revalidate();
	}

	/**
	 * @see eu.omp.irap.cassis.gui.merge.ImportPanelInterface#addLog(java.lang.String)
	 */
	@Override
	public void addLog(String log) {
		getLogArea().append(log);
	}

	/**
	 * Start the worker and set the dialog visible.
	 */
	public void start() {
		// Ensure to not run it unintentionally on test mode/eclise...
		if (Software.getUserConfiguration().isTestMode()) {
			int answer = JOptionPane.showConfirmDialog(null,
					"Test mode detected, are you sure you want to import files ?",
					"Test mode detected!", JOptionPane.YES_NO_OPTION);
			if (answer != JOptionPane.YES_OPTION) {
				return;
			}
		}
		worker.execute();
		setVisible(true);
	}
}
