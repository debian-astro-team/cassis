/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.merge;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Dialog for the selection of the elements to import/merge.
 *
 * @author M. Boiziot
 */
public class ImportSelectionDialog extends JDialog {

	private static final long serialVersionUID = -4130279399277321475L;
	private Map<String, String> folders;
	private List<ImportGuiElement> elements;
	private File importFolder;
	private File destinationFolder;
	private JButton selectAllButton;
	private JButton cancelButton;
	private JButton importButton;


	/**
	 * Creates a new {@link ImportSelectionDialog}.
	 *
	 * @param folders The map containing the elements to import with
	 *  usual name for keys and directory for values.
	 * @param importFolder The import folder (the old CASSIS folder).
	 * @param destinationFolder The destination folder (the current CASSIS folder).
	 */
	public ImportSelectionDialog(Map<String, String> folders, File importFolder,
			File destinationFolder) {
		super((Frame) null, true);
		this.folders = folders;
		this.importFolder = importFolder;
		this.destinationFolder = destinationFolder;
		setTitle("Import selection dialog");
		createUi();
	}

	/**
	 * Create the ui.
	 */
	private void createUi() {
		Container container = this.getContentPane();
		container.setLayout(new BorderLayout());

		JPanel topPanel = new JPanel();
		topPanel.add(new JLabel("Select the elements to import:"));
		container.add(topPanel, BorderLayout.NORTH);

		JPanel centerPanel = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridy = 0;
		for (ImportGuiElement element : getElements()) {
			gbc.gridx = 0;
			gbc.fill = GridBagConstraints.HORIZONTAL;
			centerPanel.add(element.getLabel(), gbc);

			gbc.gridx = 1;
			gbc.fill = GridBagConstraints.NONE;
			centerPanel.add(element.getCheckBox(), gbc);

			gbc.gridy += 1;
		}
		container.add(centerPanel, BorderLayout.CENTER);

		JPanel bottomPanel = new JPanel();
		bottomPanel.add(getSelectAllButton());
		bottomPanel.add(getCancelButton());
		bottomPanel.add(getImportButton());
		container.add(bottomPanel, BorderLayout.SOUTH);
	}

	/**
	 * Create if needed then return the list of elements.
	 *
	 * @return the list of elements.
	 */
	private List<ImportGuiElement> getElements() {
		if (elements == null) {
			elements = new ArrayList<>(folders.size());
			for (Entry<String, String> entry : folders.entrySet()) {
				ImportGuiElement element = new ImportGuiElement(
						entry.getKey(), entry.getValue());
				elements.add(element);
			}
		}
		return elements;
	}

	/**
	 * Create if needed then return the "Select all" {@link JButton}.
	 *
	 * @return the "Select all" {@link JButton}.
	 */
	public JButton getSelectAllButton() {
		if (selectAllButton == null) {
			selectAllButton = new JButton("All");
			selectAllButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					for (ImportGuiElement element : elements) {
						element.getCheckBox().setSelected(true);
					}
				}
			});
		}
		return selectAllButton;
	}

	/**
	 * Create if needed then return the "Cancel" {@link JButton}.
	 *
	 * @return the "Cancel" {@link JButton}.
	 */
	public JButton getCancelButton() {
		if (cancelButton == null) {
			cancelButton = new JButton("Cancel");
			cancelButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					ImportSelectionDialog.this.dispose();
				}
			});
		}
		return cancelButton;
	}

	/**
	 * Create if needed then return the "Import" {@link JButton}.
	 *
	 * @return the "Import" {@link JButton}.
	 */
	public JButton getImportButton() {
		if (importButton == null) {
			importButton = new JButton("Import");
			importButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					startImport();
				}
			});
		}
		return importButton;
	}

	/**
	 * Start the import for selected elements and close the dialog.
	 */
	private void startImport() {
		Map<String, String> elementsToImport = new HashMap<>();
		for(ImportGuiElement element : getElements()) {
			if (element.isSelected()) {
				elementsToImport.put(element.getName(), element.getPath());
			}
		}
		this.dispose();
		if (!elementsToImport.isEmpty()) {
			ImportDialog mp = new ImportDialog(importFolder, destinationFolder, elementsToImport);
			mp.start();
		}
	}



	/**
	 * Simple private class representing an element to import and creating GUI
	 *  elements for it.
	 *
	 * @author M. Boiziot
	 */
	private class ImportGuiElement {

		private JLabel label;
		private JCheckBox checkBox;
		private String name;
		private String path;


		/**
		 * Creates a {@link ImportGuiElement}.
		 *
		 * @param element The usual name of the element.
		 * @param path The full path of the element.
		 */
		private ImportGuiElement(String element, String path) {
			this.name = element;
			this.path = path;
		}

		/**
		 * Creates if needed then return the {@link JLabel} of the element.
		 *
		 * @return the {@link JLabel} of the element.
		 */
		private JLabel getLabel() {
			if (label == null) {
				label = new JLabel(this.name);
				label.setToolTipText(this.path);
			}
			return label;
		}

		/**
		 * Creates if needed then return the {@link JCheckBox} of the element.
		 *
		 * @return the {@link JCheckBox} of the element.
		 */
		private JCheckBox getCheckBox() {
			if (checkBox == null) {
				checkBox = new JCheckBox();
			}
			return checkBox;
		}

		/**
		 * Return the usual name of the element.

		 * @return the usual name of the element.
		 */
		private String getName() {
			return name;
		}

		/**
		 * Return the full path of the element.
		 *
		 * @return the full path of the element.
		 */
		private String getPath() {
			return path;
		}

		/**
		 * Return if the element is selected.
		 *
		 * @return true if the element is selected, false otherwise.
		 */
		private boolean isSelected() {
			return checkBox.isSelected();
		}
	}
}
