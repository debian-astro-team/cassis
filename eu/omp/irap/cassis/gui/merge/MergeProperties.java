/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.merge;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.database.access.DatabaseHistory;
import eu.omp.irap.cassis.database.access.TypeDataBase;

/**
 * Class to merge properties files from CASSIS.
 *
 * @author M. Boiziot
 */
public class MergeProperties {

	private static final Logger LOGGER = LoggerFactory.getLogger(MergeProperties.class);

	private ImportWorkerInterface mwInterface;
	private File originPropertiesFolder;
	private File destinationPropertiesFolder;


	/**
	 * Creates a {@link MergeProperties}.
	 *
	 * @param mwInterface The {@link ImportWorkerInterface}.
	 * @param folderBase The old properties folder.
	 * @param folderDestination The current CASSIS properties folder.
	 */
	public MergeProperties(ImportWorkerInterface mwInterface, File folderBase, File folderDestination) {
		this.mwInterface = mwInterface;
		this.originPropertiesFolder = folderBase;
		this.destinationPropertiesFolder = folderDestination;
	}

	/**
	 * Start the merge of the properties.
	 */
	public void merge() {
		mwInterface.publishMessage("\nMerging properties...\n");
		List<String> filesToIgnore = getIgnoredFiles();
		Map<String, List<String>> rules = getIgnoresPropertiesRules();
		for (File file : originPropertiesFolder.listFiles()) {
			String fileName = file.getName();
			if (filesToIgnore.contains(fileName)) {
				continue;
			} else if ("dbHistory.txt".equals(fileName)) {
				mergeDbHistory(file);
			} else {
				@SuppressWarnings("unchecked")
				List<String> ignoreRules = (List<String>) (rules.containsKey(fileName) ?
						rules.get(fileName) : Collections.emptyList());
				File destFile = new File(destinationPropertiesFolder.getAbsolutePath()
						+ File.separatorChar + fileName);
				mergeProperty(file, destFile, ignoreRules);
			}
		}
		mwInterface.publishMessage("Properties merged.\n");
	}

	/**
	 * Merge two properties files.
	 *
	 * @param originFile The origin properties file used a source.
	 * @param destinationFile The destination properties file.
	 * @param doNotUpdate The list of elements to not update.
	 * @return true if the merge worked, false otherwise.
	 */
	private boolean mergeProperty(File originFile, File destinationFile, List<String> doNotUpdate) {
		if (originFile == null || destinationFile == null ||
				!originFile.exists() || !ensureFileExist(destinationFile)) {
			return false;
		}
		Properties oldCassisProp = getProperties(originFile);
		Properties newCassisProp = getProperties(destinationFile);
		for (Object keyObj : oldCassisProp.keySet()) {
			String key = (String) keyObj;
			if (!doNotUpdate.contains(key)) {
				newCassisProp.put(keyObj, oldCassisProp.get(keyObj));
			}
		}
		try (BufferedWriter out = new BufferedWriter(new FileWriter(destinationFile))) {
			newCassisProp.store(out, null);
			out.flush();
		} catch (IOException e) {
			LOGGER.error("An error occured while merging property from file {} to file {}",
					originFile.getAbsolutePath(), destinationFile.getAbsolutePath(), e);
			return false;
		}
		return true;
	}

	/**
	 * Merge the given dbHistory.txt file with the current one of CASSIS.
	 *
	 * @param originFile The file to merge.
	 * @return true if the merge worked, false otherwise.
	 */
	private boolean mergeDbHistory(File originFile) {
		DatabaseHistory dbHistoryOrigin = new DatabaseHistory(originFile.getPath());
		DatabaseHistory dbHistoryCurrent = DatabaseHistory.getInstance();
		for (TypeDataBase tDb : TypeDataBase.values()) {
			for (String db : dbHistoryOrigin.getDb(tDb)) {
				dbHistoryCurrent.addDatabase(tDb, db);
			}
		}
		return true;
	}

	/**
	 * Ensure a file exist, create it if it does not.
	 *
	 * @param file The file to check/creates.
	 * @return true if the file exist at the end, false otherwise.
	 */
	private boolean ensureFileExist(File file) {
		if (!file.exists()) {
			try {
				return file.createNewFile();
			} catch (IOException e) {
				LOGGER.error("Error while trying to create file {}",
						file.getAbsolutePath(), e);
				return false;
			}
		}
		return true;
	}

	/**
	 * Load and return the properties of a given file.
	 *
	 * @param file The file.
	 * @return The properties of the file.
	 */
	private Properties getProperties(File file) {
		Properties properties = new Properties();
		try (FileReader reader = new FileReader(file)) {
			properties.load(reader);
		} catch (IOException e) {
			LOGGER.error("Error while loading the file {}", file.getAbsolutePath(), e);
		}
		return properties;
	}

	/**
	 * Return the rules of the properties ignores.
	 *
	 * @return the rules of the properties ignores.
	 */
	private static Map<String, List<String>> getIgnoresPropertiesRules() {
		Map<String, List<String>> map = new HashMap<>();

		// Ignore version on cassis.properties
		List<String> cassisProp = new ArrayList<>(2);
		cassisProp.add("version");
		cassisProp.add("dataBaseName");
		map.put("cassis.properties", cassisProp);

		// Ignore last-update on config.properties
		List<String> configProp = new ArrayList<>(1);
		configProp.add("last-update");
		map.put("config.properties", configProp);

		return map;
	}

	/**
	 * Return the list of files to ignore for the merge.
	 *
	 * @return the list of files to ignore for the merge.
	 */
	private static List<String> getIgnoredFiles() {
		List<String> ignoredFiles = new ArrayList<>(3);
		ignoredFiles.add("log4j.xml");
		ignoredFiles.add("molesRADEXCASSIS.properties");
		ignoredFiles.add("radex.properties");
		return ignoredFiles;
	}
}
