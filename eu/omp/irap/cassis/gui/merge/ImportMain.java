/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.merge;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import eu.omp.irap.cassis.properties.Software;

/**
 * Main class for importing files/properties from another CASSIS installation.
 *
 * @author M. Boiziot
 */
public class ImportMain {

	private static final String DELIVERY_FOLDER = "delivery";
	private static final String DATABASE_FOLDER = "database";
	private static final String PROPERTIES_FOLDER = "properties";

	/* DELIVERY subfolder */
	private static final String CONTINUUM = "continuum";
	private static final String CONFIG = "config";
	private static final String SCRIPT = "script";
	private static final String TELESCOPE = "telescope";

	/* DATABASE subfolder */
	private static final String LAMDA = "lamda";
	private static final String SQL_PARTITION_MOLE = "sqlPartitionMole";
	private static final String TEMPLATE = "template";

	private File previousCassisFolder;
	private File currentCassisFolder;
	private Map<String, String> knownSubfolders;


	/**
	 * Construct the main object to start an Import.
	 */
	public ImportMain() {
		this.currentCassisFolder = new File(Software.getCassisPath());
		this.knownSubfolders = ImportMain.getFolders();
	}

	/**
	 * Start the importer.
	 */
	public void startImporter() {
		int res = JOptionPane.showConfirmDialog(null,
				"A new window will open for you to select the previous CASSIS installation.",
				"Import previous CASSIS settings.", JOptionPane.OK_CANCEL_OPTION);
		if (res == JOptionPane.OK_OPTION) {
			showFolderChooser();
		}
	}

	/**
	 * Show the folder chooser, in order to select the previous CASSIS folder
	 *  (=the folder from which we want to import old files/config).
	 */
	private void showFolderChooser() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fileChooser.setAcceptAllFileFilterUsed(false);
		fileChooser.setMultiSelectionEnabled(false);

		int res = fileChooser.showOpenDialog(null);

		if (res == JFileChooser.APPROVE_OPTION) {
			File choosedFolder = fileChooser.getSelectedFile();
			if (isCassisDirectory(choosedFolder)) {
				this.previousCassisFolder = choosedFolder;
				updateFolders();
				showMergeSelectionPanel();
			} else {
				int ret = JOptionPane.showConfirmDialog(null,
						"The selected folder does not appear to be a CASSIS installation folder.\n"
						+ "Please select one if you want to import previous settings.",
						"Selection error", JOptionPane.OK_CANCEL_OPTION);
				if (ret == JOptionPane.OK_OPTION) {
					showFolderChooser();
				}
			}

		}
	}

	/**
	 * Do a lazy check to a folder in order to determine if it is a CASSIS
	 * installation folder. To do that, we check if the three subfolders exists :
	 * database, delivery and properties.
	 *
	 * @param folder The folder to check.
	 * @return true if is a CASSIS installation folder, false otherwise.
	 */
	private boolean isCassisDirectory(File folder) {
		if (folder == null || !folder.isDirectory()) {
			return false;
		}
		boolean databaseFound = false;
		boolean deliveryFound = false;
		boolean propertiesFound = false;

		for (File f : folder.listFiles()) {
			if (f.isDirectory()) {
				String name = f.getName();
				if (DATABASE_FOLDER.equals(name)) {
					databaseFound = true;
				} else if (DELIVERY_FOLDER.equals(name)) {
					deliveryFound = true;
				} else if (PROPERTIES_FOLDER.equals(name)) {
					propertiesFound = true;
				}
			}
		}
		return deliveryFound && databaseFound && propertiesFound;
	}

	/**
	 * Update folders to import by removing the ones who does not exist.
	 */
	private void updateFolders() {
		Iterator<Entry<String, String>> it =
				this.knownSubfolders.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, String> entry = it.next();
			String path = this.previousCassisFolder.getAbsolutePath() +
					File.separatorChar + entry.getValue();
			File folder = new File(path);
			if (!folder.exists() || !folder.isDirectory()) {
				it.remove();
			}
		}
	}

	/**
	 * Show the Merge selection panel.
	 */
	private void showMergeSelectionPanel() {
		ImportSelectionDialog msp = new ImportSelectionDialog(knownSubfolders,
				previousCassisFolder, currentCassisFolder);
		msp.pack();
		msp.setVisible(true);
	}

	/**
	 * Return a map of known subfolder to import with as key the usual name
	 *  and as value the path to it from the CASSIS folder.
	 *
	 * @return the map.
	 */
	private static final Map<String, String> getFolders() {
		Map<String, String> maps = new HashMap<>();
		char separatorChar = File.separatorChar;
		maps.put(CONTINUUM, DELIVERY_FOLDER + separatorChar + CONTINUUM);
		maps.put(CONFIG, DELIVERY_FOLDER + separatorChar + CONFIG);
		maps.put(SCRIPT, DELIVERY_FOLDER + separatorChar + SCRIPT);
		maps.put(TELESCOPE, DELIVERY_FOLDER + separatorChar + TELESCOPE);

		maps.put(LAMDA, DATABASE_FOLDER + separatorChar + "lamda-user");
		maps.put("Partition functions", DATABASE_FOLDER + separatorChar + SQL_PARTITION_MOLE);
		maps.put(TEMPLATE, DATABASE_FOLDER + separatorChar + TEMPLATE);

		maps.put(PROPERTIES_FOLDER, PROPERTIES_FOLDER);
		return maps;
	}

}
