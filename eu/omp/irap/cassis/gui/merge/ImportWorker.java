/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.merge;

import java.io.File;
import java.util.List;
import java.util.Map;

import javax.swing.SwingWorker;

/**
 * {@link SwingWorker} for the import functionnality.
 *
 * @author M. Boiziot
 */
public class ImportWorker extends SwingWorker<Boolean, String> implements ImportWorkerInterface {

	private File oldDirectory;
	private File newDirectory;
	private ImportPanelInterface importPanelInterface;
	private Map<String, String> elementsToImport;


	/**
	 * Creates a new ImportWorker.
	 *
	 * @param importPanelInterface The MergePanelInterface.
	 * @param oldDirectory The old CASSIS directory.
	 * @param newDirectory The new (current) CASSIS directory.
	 * @param elementsToImport The map containing the elements to import with
	 *  usual name for keys and directory for values.
	 */
	public ImportWorker(ImportPanelInterface importPanelInterface, File oldDirectory,
			File newDirectory, Map<String, String> elementsToImport) {
		super();
		this.importPanelInterface = importPanelInterface;
		this.oldDirectory = oldDirectory;
		this.newDirectory = newDirectory;
		this.elementsToImport = elementsToImport;
	}

	/**
	 * Do the file import and properties merge if needed.
	 *
	 * @see javax.swing.SwingWorker#doInBackground()
	 */
	@Override
	protected Boolean doInBackground() throws Exception {
		// First merge files.
		ImportFiles mf = new ImportFiles(this, oldDirectory, newDirectory,
				elementsToImport);
		boolean res = mf.startImport();
		if (!res) {
			publish("Error while importing files. Stopping the operation.\n");
			return false;
		}

		// Merge properties if needed.
		if (elementsToImport.containsKey("properties")) {
			File oldPropDir = new File(oldDirectory.getAbsolutePath() +
					File.separatorChar + elementsToImport.get("properties"));
			File newPropDir = new File(newDirectory.getAbsolutePath() +
					File.separatorChar + elementsToImport.get("properties"));
			MergeProperties mp = new MergeProperties(this, oldPropDir, newPropDir);
			mp.merge();
			setProgress(getProgress() + 1);
		}
		return true;
	}

	/**
	 * End of the task, update the gui.
	 *
	 * @see javax.swing.SwingWorker#done()
	 */
	@Override
	protected void done() {
		importPanelInterface.done();
	}

	/**
	 * Update logs.
	 *
	 * @see javax.swing.SwingWorker#process(java.util.List)
	 */
	@Override
	protected void process(List<String> chunks) {
		for (String log : chunks) {
			importPanelInterface.addLog(log);
		}
	}

	/**
	 * Call publish(String...).
	 *
	 * @see eu.omp.irap.cassis.gui.merge.ImportWorkerInterface#publishMessage(java.lang.String)
	 */
	@Override
	public void publishMessage(String element) {
		publish(element);
	}

	/**
	 * Call {@link #setProgress(int)}.
	 *
	 * @see eu.omp.irap.cassis.gui.merge.ImportWorkerInterface#setCurrentProgress(int)
	 */
	@Override
	public void setCurrentProgress(int progress) {
		setProgress(progress);
	}

}
