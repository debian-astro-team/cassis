/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.merge;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import eu.omp.irap.cassis.gui.util.FileUtils;

/**
 * Class to import files.
 *
 * @author M. Boiziot
 */
public class ImportFiles {

	private ImportWorkerInterface mwInterface;
	private File oldDirectory;
	private File newDirectory;
	private Map<String, String> elementsToImport;


	/**
	 * Creates a {@link ImportFiles} object.
	 *
	 * @param mwInterface The {@link ImportWorkerInterface}.
	 * @param oldDirectory The old CASSIS directory.
	 * @param newDirectory The new (current) CASSIS directory.
	 * @param elementsToImport The map containing the elements to import with
	 *  usual name for keys and directory for values.
	 */
	public ImportFiles(ImportWorkerInterface mwInterface, File oldDirectory,
			File newDirectory, Map<String, String> elementsToImport) {
		this.mwInterface = mwInterface;
		this.oldDirectory = oldDirectory;
		this.newDirectory = newDirectory;
		this.elementsToImport = elementsToImport;
	}

	/**
	 * Do the import.
	 *
	 * @return true if the import succeeded, false otherwise.
	 */
	public boolean startImport() {
		String name;
		String path;
		int progress = 0;
		for (Entry<String, String> element : elementsToImport.entrySet()) {
			name = element.getKey();
			path = element.getValue();
			mwInterface.publishMessage("Importing " + name + "...\n");

			File oldDir = new File(oldDirectory.getAbsolutePath() + File.separatorChar + path);
			File newDir = new File(newDirectory.getAbsolutePath() + File.separatorChar + path);
			List<CopyElement> elements = getFilesToCopy(oldDir, newDir);
			for (CopyElement ce : elements) {
				if (ce.copy()) {
					if (ce.isDirectory()) {
						mwInterface.publishMessage(
								" Folder created at " + ce.getDestination() + '\n');
					} else {
						mwInterface.publishMessage(
								" File copied at " + ce.getDestination() + '\n');
					}
				} else {
					if (ce.isDirectory()) {
						mwInterface.publishMessage(" Error while trying to create folder at "
								+ ce.getDestination() + '\n');
					} else {
						mwInterface.publishMessage(" Error while trying to copy file at "
								+ ce.getDestination() + '\n');
					}
				}
			}
			mwInterface.publishMessage("\n");
			progress++;
			mwInterface.setCurrentProgress(progress);
		}
		return true;
	}

	/**
	 * Compute then return the list of {@link CopyElement} to copy.
	 *
	 * @param oldDirectory The old directory.
	 * @param newDirectory The new directory.
	 * @return The list of {@link CopyElement} to copy.
	 */
	private List<CopyElement> getFilesToCopy(File oldDirectory, File newDirectory) {
		File[] filesOldDir = oldDirectory.listFiles();
		String newDirectoryPath = newDirectory.getAbsolutePath();
		String oldDirectoryPath = oldDirectory.getAbsolutePath();
		Arrays.sort(filesOldDir);
		List<CopyElement> folders = new ArrayList<>();
		List<CopyElement> files = new ArrayList<>();

		for (File file : filesOldDir) {
			String fullPath = file.getAbsolutePath();
			String subTractedPath = fullPath.substring(oldDirectoryPath.length());
			String path = newDirectoryPath + subTractedPath;
			File newFile = new File(path);

			if (!newFile.exists()) {
				CopyElement ce = new CopyElement(file, newFile);
				if (file.isDirectory()) {
					folders.add(ce);
					for (CopyElement subCe : getFilesToCopy(ce.getSource(), ce.getDestination())) {
						if (subCe.isDirectory()) {
							folders.add(subCe);
						} else {
							files.add(subCe);
						}
					}
				} else {
					files.add(ce);
				}
			} else if (newFile.isDirectory()) {
				CopyElement ce = new CopyElement(file, newFile);
				for (CopyElement subCe : getFilesToCopy(ce.getSource(), ce.getDestination())) {
					if (subCe.isDirectory()) {
						folders.add(subCe);
					} else {
						files.add(subCe);
					}
				}
			}
		}
		List<CopyElement> returnList = new ArrayList<>(folders.size() + files.size());
		returnList.addAll(folders);
		returnList.addAll(files);

		return returnList;
	}



	/**
	 * Simple class describing an element to copy.
	 *
	 * @author M. Boiziot
	 */
	private class CopyElement {

		private File source;
		private File destination;


		/**
		 * Creates a {@link CopyElement}.
		 *
		 * @param source The source file.
		 * @param destination The destination file.
		 */
		private CopyElement(File source, File destination) {
			this.source = source;
			this.destination = destination;
		}

		/**
		 * Return the source file.
		 *
		 * @return the source file.
		 */
		private File getSource() {
			return source;
		}

		/**
		 * Return the destination file.
		 *
		 * @return the destination file.
		 */
		private File getDestination() {
			return destination;
		}

		/**
		 * Return if the element to copy is a directory.
		 *
		 * @return true if the element to copy is a directory, false otherwise.
		 */
		private boolean isDirectory() {
			return source.isDirectory();
		}

		/**
		 * Copy the element (create the directory or copy the file).
		 *
		 * @return true if the copy succeeded, false otherwise.
		 */
		private boolean copy() {
			if (isDirectory()) {
				return destination.mkdirs();
			} else {
				return FileUtils.copyFile(source, destination);
			}
		}

		/*
		 * Return a String representation of the object.
		 *
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			String type = isDirectory() ? "Directory" : "File";
			return type + " : " + source.getAbsolutePath() + " ==> " + destination.getAbsolutePath();
		}
	}
}
