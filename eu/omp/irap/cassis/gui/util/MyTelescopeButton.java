/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.util;

import java.io.File;

import javax.swing.JButton;

/**
 * JButton with specific changes for telescope ones.
 *
 * @author M. Boiziot
 */
public class MyTelescopeButton extends JButton {

	private static final long serialVersionUID = -7835829885371784432L;
	private int limit;


	/**
	 * Create a specific Button for telescope with no size limite.
	 * This is equals to MyTescopeButton(text, 0)
	 *
	 * @param text Text to set.
	 */
	public MyTelescopeButton(String text) {
		this(text, 0);
	}

	/**
	 * Create a specific Button for telescope with eventually a limit of the text of the JButton.
	 *
	 * @param text Text to set.
	 * @param limit The limit of the text char in the button.
	 *  The limit must be superior to 4, or 0 for no limit.
	 */
	public MyTelescopeButton(String text, int limit) {
		super(text);
		if (limit <= 4) {
			limit = 0;
		}
		this.limit = limit;

		if (limit > 0 && text.length() > limit) {
			setText(text);
		} else {
			super.setToolTipText(text);
		}
	}

	@Override
	public void setText(String text) {
		String finalText = new File(text).getName();
		super.setToolTipText(finalText);
		if (limit > 0 && finalText.length() > limit) {
			super.setText(finalText.substring(0, limit - 3) + "...");
		} else {
			super.setText(finalText);
		}
	}
}
