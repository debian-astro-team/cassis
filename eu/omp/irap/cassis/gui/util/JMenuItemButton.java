/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.util;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.event.EventListenerList;


public class JMenuItemButton extends JMenuItem {

	private static final String LAB = "         ";
	private static final long serialVersionUID = -359943218403322519L;
	private EventListenerList buttonListeners;
	private JButton button;
	private boolean hideParent;


	/**
	 * Constructor.
	 *
	 * @param textLabel Text of the label.
	 */
	public JMenuItemButton(String textLabel) {
		this(textLabel, false);
	}

	/**
	 * Constructor.
	 *
	 * @param textLabel Text of the label.
	 * @param hide If the JMenu parent should be hided after a click on the
	 * 			button or not.
	 */
	public JMenuItemButton(String textLabel, boolean hide) {
		super(textLabel + LAB);
		this.hideParent = hide;
		buttonListeners = new EventListenerList();
		setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 0));
		add(initButton());
	}

	/**
	 * Creates, configures then return the button.
	 *
	 * @return The "X" button
	 */
	private JButton initButton() {
		button = new JButton("X");
		button.setBorder(BorderFactory.createEmptyBorder(0, 0, 2, 0));
		button.setOpaque(false);
		button.setContentAreaFilled(false);
		button.setBorderPainted(false);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				handleClick(ae);
			}
		});
		return button;
	}

	/**
	 * Add an ActionListener to the X button.
	 *
	 * @param al The ActionListener to add.
	 */
	public void addDeleteButtonListener(ActionListener al) {
		buttonListeners.add(ActionListener.class, al);
	}

	/**
	 * Remove an ActionListener from the X button.
	 *
	 * @param al The ActionListener to remove.
	 */
	public void removeDeleteButtonListener(ActionListener al) {
		buttonListeners.remove(ActionListener.class, al);
	}

	/**
	 * Handle the actions on the click on the X button.
	 *
	 * @param ae The ActionEvent from the click.
	 */
	private void handleClick(ActionEvent ae) {
		/* Get the parent first, as our object can be removed from it's
		 * parent in the listeners. It this case, getParent() would return
		 * null...
		 */
		Container c = getParent();

		// Fire listeners.
		ActionListener[] als = buttonListeners.getListeners(ActionListener.class);
		for (ActionListener al : als) {
			al.actionPerformed(ae);
		}
		// Hide or pack the parent if needed.
		if (c != null && c.isVisible()) {
			if (hideParent) {
				c.setVisible(false);
			} else if (c instanceof JPopupMenu) {
				JPopupMenu jpm = (JPopupMenu) c;
				jpm.pack();
			}
		}
		// Remove listeners on the buttons.
		for (ActionListener al : als) {
			buttonListeners.remove(ActionListener.class, al);
		}
	}

	/**
	 * Return the text from the label without the whitespace added at the end.
	 *
	 * @return the text from the label without the whitespace added at the end.
	 */
	public String getTrueText() {
		String s = super.getText();
		return s.replace(LAB, "");
	}
}
