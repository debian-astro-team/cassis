/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.util;

import java.awt.Dimension;
import java.awt.FontMetrics;
import java.util.Vector;

import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;

/**
 * Simple JComboBox but with the size of the popup change
 * to the size of elements even if the size JComboBox is
 * smaller unlike to JComboBox.
 *
 * @author M. Boiziot
 */
public class ExtentedJComboBox<E> extends JComboBox<E> {

	private static final long serialVersionUID = -107487371170058255L;
	private boolean layingOut = false;


	public ExtentedJComboBox() {
		super();
	}

	public ExtentedJComboBox(final E[] items) {
		super(items);
	}

	public ExtentedJComboBox(ComboBoxModel<E> aModel) {
		super(aModel);
	}

	public ExtentedJComboBox(Vector<E> items) {
		super(items);
	}

	@Override
	public void doLayout() {
		try {
			layingOut = true;
			super.doLayout();
		} finally {
			layingOut = false;
		}
	}

	@Override
	public Dimension getSize() {
		Dimension dim = super.getSize();
		if (!layingOut) {
			FontMetrics fm = getFontMetrics(getFont());
			int size = 0;
			int newSize = 0;
			for (int i = 0; i < this.getItemCount(); i++) {
				newSize = fm.stringWidth(getItemAt(i).toString());
				if (newSize > size) {
					size = newSize;
				}
			}
			dim.width = Math.max(dim.width, size+20);
		}
		return dim;
	}
}
