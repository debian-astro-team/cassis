/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.util;

import java.awt.Image;
import java.awt.Toolkit;
import java.io.InputStream;
import java.net.URL;

import javax.swing.ImageIcon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.properties.Software;

public class CassisRessource {

	private static final Logger LOGGER = LoggerFactory.getLogger(CassisRessource.class);

	private static final String OPEN = "/ressource/toolbar/open.gif";
	private static final String SAVE = "/ressource/toolbar/save.gif";
	private static final String PRINT = "/ressource/toolbar/print.gif";
	private static final String SPECTRUM = "/ressource/toolbar/spectrum.gif";
	private static final String RADEX = "/ressource/toolbar/radex.gif";
	private static final String LOOMIS_WOOD = "/ressource/toolbar/loomiswood.gif";
	private static final String LINE = "/ressource/toolbar/line.gif";
	private static final String ROTATIONAL = "/ressource/toolbar/rotational.gif";
	private static final String TEMPLATE = "/ressource/toolbar/template.gif";
	private static final String CASSIS = "/ressource/logo_cassis.gif";
	private static final String LOGO_CASSIS_PATH = "/ressource/cassis_logo.jpg";
	private static final String LOGO_CNRS_PATH = "/ressource/logo_cnrs.jpg";
	private static final String LOGO_UPS_PATH = "/ressource/logo_ups.jpg";
	private static final String LOGO_IRAP_PATH = "/ressource/logo_irap.jpg";
	private static final String SPECTRUM_MANAGER = "/ressource/toolbar/spectrumManager.gif";

	private static Image openIcon;
	private static Image saveIcon;
	private static Image printIcon;
	private static Image spectrumIcon;
	private static Image radexIcon;
	private static Image loomisWoodIcon;
	private static Image lineIcon;
	private static Image rotationalIcon;
	private static Image templateIcon;
	private static Image cassisIcon;
	private static Image spectrumManagerIcon;


	/**
	 * Returns the image icon of the given path, or null if there was an error.
	 *
	 * @param path The path of the icon.
	 * @return The image icon of the given path, or null if there was an error.
	 */
	public static ImageIcon createImageIcon(String path) {
		try {
			URL imgURL = Software.getModePath().getIcon("/ressource/toolbar/" + path);
			if (imgURL != null) {
				return new ImageIcon(imgURL);
			}
		} catch (Exception e) {
			LOGGER.error("Error while creating image icon for {}", path, e);
		}
		return null;
	}

	public static Image createImage(String path) {
		return Toolkit.getDefaultToolkit().getImage(
				Software.getModePath().getIcon("/ressource/toolbar/" + path));
	}

	private static Image getIcon(String pathIcon) {
		return Toolkit.getDefaultToolkit().getImage(
				Software.getModePath().getIcon(pathIcon));
	}

	public static Image getOpenIcon() {
		if (openIcon == null) {
			openIcon = getIcon(OPEN);
		}
		return openIcon;
	}

	public static Image getSaveIcon() {
		if (saveIcon == null) {
			saveIcon = getIcon(SAVE);
		}
		return saveIcon;
	}

	public static Image getPrintIcon() {
		if (printIcon == null) {
			printIcon = getIcon(PRINT);
		}
		return printIcon;
	}

	public static Image getSpectrumIcon() {
		if (spectrumIcon == null) {
			spectrumIcon = getIcon(SPECTRUM);
		}
		return spectrumIcon;
	}

	public static Image getRadexIcon() {
		if (radexIcon == null) {
			radexIcon = getIcon(RADEX);
		}
		return radexIcon;
	}

	public static Image getLoomisWoodIcon() {
		if (loomisWoodIcon == null) {
			loomisWoodIcon = getIcon(LOOMIS_WOOD);
		}
		return loomisWoodIcon;
	}

	public static Image getLineIcon() {
		if (lineIcon == null) {
			lineIcon = getIcon(LINE);
		}
		return lineIcon;
	}

	public static Image getRotationalIcon() {
		if (rotationalIcon == null) {
			rotationalIcon = getIcon(ROTATIONAL);
		}
		return rotationalIcon;
	}

	public static Image getTemplateIcon() {
		if (templateIcon == null) {
			templateIcon = getIcon(TEMPLATE);
		}
		return templateIcon;
	}

	public static Image getCassisIcon() {
		if (cassisIcon == null) {
			cassisIcon = getIcon(CASSIS);
		}
		return cassisIcon;
	}


	public static Image getSpectrumManagerIcon() {
		if (spectrumManagerIcon == null){
			spectrumManagerIcon = getIcon(SPECTRUM_MANAGER);
		}
		return spectrumManagerIcon;
	}

	public static Image getCassisLogo() {
		return new ImageIcon(Software.getModePath().getIcon(LOGO_CASSIS_PATH), "logo cassis").getImage();
	}

	public static Image getUpsLogo() {
		return new ImageIcon(Software.getModePath().getIcon(LOGO_UPS_PATH), "logo UPS").getImage();
	}

	public static Image getIrapLogo() {
		return new ImageIcon(Software.getModePath().getIcon(LOGO_IRAP_PATH), "logo IRAP").getImage();
	}

	public static Image getCnrsLogo() {
		return new ImageIcon(Software.getModePath().getIcon(LOGO_CNRS_PATH), "logo CNRS").getImage();
	}

	public static ImageIcon getSplashImage() {
		return  new ImageIcon(Software.getModePath().getIcon("/ressource/cassisSplash.gif"));
	}

	public static InputStream getAuthors() {
		return Software.getModePath().getAuthors();
	}

}
