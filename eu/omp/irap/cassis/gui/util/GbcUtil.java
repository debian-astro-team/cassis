/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.util;

import java.awt.GridBagConstraints;

/**
 * Utility class to configure {@link GridBagConstraints}.
 *
 * @author M. Boiziot
 */
public class GbcUtil {

	/**
	 * Configure the parameters fill, gridx and gridy of the provided {@link GridBagConstraints}.
	 *
	 * @param gbc The {@link GridBagConstraints} to configure
	 * @param fill The fill value to set
	 * @param gridx The gridx value to set
	 * @param gridy The gridy value to set
	 */
	public static void configureFillGrids(GridBagConstraints gbc, int fill, int gridx, int gridy) {
		gbc.fill = fill;
		gbc.gridx = gridx;
		gbc.gridy = gridy;
	}

	/**
	 * Configure the parameters fill, gridx, gridy and anchor of the provided {@link GridBagConstraints}.
	 *
	 * @param gbc The {@link GridBagConstraints} to configure
	 * @param fill The fill value to set
	 * @param gridx The gridx value to set
	 * @param gridy The gridy value to set
	 * @param anchor The anchor value to set
	 */
	public static void configureFillGridsAnchor(GridBagConstraints gbc, int fill, int gridx, int gridy, int anchor) {
		gbc.fill = fill;
		gbc.gridx = gridx;
		gbc.gridy = gridy;
		gbc.anchor = anchor;
	}
	/**
	 * Configure the parameters fill, gridx, gridy and weightx of the provided {@link GridBagConstraints}.
	 *
	 * @param gbc The {@link GridBagConstraints} to configure
	 * @param fill The fill value to set
	 * @param gridx The gridx value to set
	 * @param gridy The gridy value to set
	 * @param weightx The weightx to set
	 */
	public static void configureFillGridsWeightx(GridBagConstraints gbc, int fill, int gridx, int gridy, double weightx) {
		gbc.fill = fill;
		gbc.gridx = gridx;
		gbc.gridy = gridy;
		gbc.weightx = weightx;
	}

	/**
	 * Configure the parameters gridx, gridy and gridwidth of the provided {@link GridBagConstraints}.
	 *
	 * @param gbc The {@link GridBagConstraints} to configure
	 * @param gridx The gridx value to set
	 * @param gridy The gridy value to set
	 * @param gridwidth The gridwidth to set
	 */
	public static void configureGrids(GridBagConstraints gbc, int gridx, int gridy, int gridwidth) {
		gbc.gridx = gridx;
		gbc.gridy = gridy;
		gbc.gridwidth = gridwidth;
	}

}
