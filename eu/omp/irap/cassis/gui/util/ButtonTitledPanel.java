/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.util;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToggleButton;

/**
 * Class defining a panel with a Check box title.<BR>
 * This class is managing 2 panels: the outer panel and the inner panel. The
 * inner panel acts as a "content pane", so every method for adding/removing
 * components or laying out the contents must be called on the inner panel. The
 * method <code>getSubPanel()</code> is made for this purpose.
 */
public class ButtonTitledPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	protected JButton buttonClear = null;
	protected JToggleButton buttonOnOff = null;
	private JComponent labelTitle = null;
	private final JPanel subPanel;
	private String title = "";
	private boolean isBlockable = false;


	public ButtonTitledPanel() {
		this("No title", true);
		for (int i = 0; i < 3; i++) {
			this.getSubPanel().add(new JTextField(9));
		}
		setSize(new Dimension(350, 75));
	}

	public ButtonTitledPanel(String title) {
		this(title, true);
	}

	/**
	 * Constructs a new Check box titled panel.
	 *
	 * @param title The title of the panel
	 * @param withCheckbox with checkbox or not.
	 */
	public ButtonTitledPanel(String title, boolean withCheckbox) {
		// The outer panel is set with no layout
		super();
		this.title = title;
		isBlockable = withCheckbox;
		setLayout(new BorderLayout());

		// Create the sub-panel
		subPanel = new JPanel();
		subPanel.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
		// Define the outer panel with titled border (on which
		// will be over-painted the check box)
		setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(18, 0, 0, 0),
				BorderFactory.createTitledBorder("")));

		// Add the 2 components: check box and sub-panel
		add(getLabelTitle());
		add(getButtonOnOff());
		add(getButtonClear());
		add(subPanel, BorderLayout.CENTER);

		// Add the listener for panel resizing
		addComponentListener(correctSize());
	}

	public Component getLabelTitle() {
		if (labelTitle == null) {
			if (isBlockable) {
				labelTitle = new JCheckBox(title);
				labelTitle.setOpaque(true);
				labelTitle.setSize(labelTitle.getPreferredSize());
				labelTitle.setBorder(null);
			}
			else {

				labelTitle = new JLabel(title);
				labelTitle.setOpaque(true);
				labelTitle.setSize(labelTitle.getPreferredSize());
				labelTitle.setBorder(null);
			}
		}
		return labelTitle;
	}

	public Component getButtonOnOff() {
		if (buttonOnOff == null) {
			buttonOnOff = new JToggleButton("ON", false);
			buttonOnOff.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (buttonOnOff.isSelected())
						buttonOnOff.setText("OFF");
					else
						buttonOnOff.setText("ON");
				}
			});
			buttonOnOff.setOpaque(true);
			buttonOnOff.setSize(30, 3);
			buttonOnOff.setMargin(new Insets(0, 0, 0, 0));
		}
		return buttonOnOff;
	}

	private Component getButtonClear() {
		// Creates the check box for the title, enabled by default,
		// set its size and no border
		if (buttonClear == null) {
			buttonClear = new JButton();
			buttonClear.setOpaque(true);
			buttonClear.setText("X");
			buttonClear.setSize(30, 3);
			buttonClear.setMargin(new Insets(0, 0, 0, 0));
		}
		return buttonClear;
	}

	private ComponentAdapter correctSize() {
		return new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				Dimension size = getSize();
				Insets insets = getInsets();

				// Relocate and resize the sub-panel
				subPanel.setLocation(insets.left, insets.top);
				subPanel.setSize(size.width - insets.left - insets.right, size.height - insets.top - insets.bottom);
				subPanel.doLayout();

				// Relocate and resize the check box
				labelTitle.setLocation(insets.left + 2, 5);
				labelTitle.setSize(labelTitle.getSize().width, insets.top);
				buttonOnOff.setLocation(insets.right
						+ (getSize().width - 5 - buttonOnOff.getSize().width - 15 - buttonClear.getSize().width), 5);
				buttonOnOff.setSize(buttonClear.getSize().width, insets.top);
				buttonClear.setLocation(insets.right + (getSize().width - 10 - buttonClear.getSize().width), 5);
				buttonClear.setSize(buttonClear.getSize().width, insets.top);
			}
		};
	}

	/**
	 * Add a listener to the component that will be fired when user click on the
	 * button.
	 *
	 * @param e
	 *            actionListener
	 */
	public void addButtonClearListener(ActionListener e) {
		buttonClear.addActionListener(e);
	}

	/**
	 * Add a listener to the component that will be fired when user click on the
	 * button.
	 *
	 * @param e
	 *            actionListener
	 */
	public void addButtonONOFFListener(ActionListener e) {
		buttonOnOff.addActionListener(e);
	}

	/**
	 * Returns the sub-panel (content pane).
	 *
	 * @return panel the sub-panel
	 */
	public JPanel getSubPanel() {
		return subPanel;
	}

	public void addSubPanel(Component comp) {
		subPanel.add(comp);
	}

	public void addActionListener(ActionListener l) {
		if (labelTitle.getClass().equals(JCheckBox.class)) {
			((JCheckBox) labelTitle).addActionListener(l);
			((JCheckBox) labelTitle).setActionCommand("checkAll");
		}
	}

	public boolean isChecked() {
		if (labelTitle.getClass().equals(JCheckBox.class)) {
			return ((JCheckBox) labelTitle).isSelected();
		}
		return false;
	}

	/**
	 * @return the title
	 */
	public final String getTitle() {
		return title;
	}

	/**
	 * @return the isBlockable
	 */
	public final boolean isBlockable() {
		return isBlockable;
	}

}
