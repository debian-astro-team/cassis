/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.util;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

/**
 * JPanel with a JCheckBox as Border.
 * The panel by itself must not be used to add components directly.
 * The to add components to the panel, the panel from getPanel() must be used.
 *
 * @author M. Boiziot
 */
public class PanelWithCheckBoxBorder extends JPanel {

	private static final long serialVersionUID = -8649678100031475603L;

	private JCheckBox checkBox;
	private JPanel panel;
	private boolean cbListener;


	/**
	 * The constructor.
	 *
	 * @param title Title of the checkbox
	 * @param selected If the checkbox is selected by default.
	 * @param cbListener If the checkbox enable/disable the components.
	 */
	public PanelWithCheckBoxBorder(String title, boolean selected, boolean cbListener) {
		super(new BorderLayout());
		this.cbListener = cbListener;
		setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(12, 0, 0, 0),
				BorderFactory.createTitledBorder("")));

		checkBox = new JCheckBox(title);
		checkBox.setOpaque(true);
		checkBox.setBorder(null);
		checkBox.setFocusable(false);
		checkBox.setSize(checkBox.getPreferredSize());
		checkBox.setSelected(selected);
		if (cbListener) {
			checkBox.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					refreshComponentsStates();
				}
			});
		}

		panel = new JPanel();
		panel.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
		add(checkBox);
		add(panel, BorderLayout.CENTER);

		addComponentListener(getComponentAdaptater());
	}

	/**
	 * Return the {@link JCheckBox}.
	 *
	 * @return the {@link JCheckBox}.
	 */
	public JCheckBox getCheckBox() {
		return checkBox;
	}

	/**
	 * Create and return a {@link ComponentAdapter}.
	 *
	 * @return a {@link ComponentAdapter}.
	 */
	private ComponentAdapter getComponentAdaptater() {
		return new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				Dimension size = getSize();
				Insets insets = getInsets();

				// Relocate and resize the sub-panel
				panel.setLocation(insets.left, insets.top);
				panel.setSize(size.width - insets.left - insets.right,
						size.height - insets.top - insets.bottom);
				panel.doLayout();

				// Relocate and resize the check box
				checkBox.setLocation(insets.left + 4, 8);
				checkBox.setSize(checkBox.getSize().width, insets.top);
			}
		};
	}

	/**
	 * Return the center panel who must be used as the main JPanel to add components.
	 *
	 * @return the center panel who must be used as the main JPanel to add components.
	 */
	public JPanel getPanel() {
		return panel;
	}

	/**
	 * Enables or disables the components in the main panel.
	 *
	 * @param enabled If the components are enabled.
	 */
	private void setComponentsEnabled(boolean enabled) {
		Component[] comps = panel.getComponents();
		for (Component component : comps) {
			component.setEnabled(enabled);
		}
	}

	/**
	 * Enables or disables the components in the main panel if needed.
	 */
	public void refreshComponentsStates() {
		if (cbListener) {
			setComponentsEnabled(checkBox.isSelected());
		}
	}
}
