/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.util;

import java.awt.Color;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@SuppressWarnings("serial")
public class JDoubleCassisTextField extends JFormattedTextField {

	private static final Logger LOGGER = LoggerFactory.getLogger(JDoubleCassisTextField.class);
	private AbstractFormatterFactory decimal;
	private AbstractFormatterFactory scientifique;
	private DecimalFormat decimalFormat;
	private DecimalFormat scientificFormat;
	private Map<String, Double> symbols;
	private double valMin = 0.0001;
	private double valMax = 9999.999;


	public JDoubleCassisTextField(DecimalFormat decimalFormat, DecimalFormat scientificFormat,
			Map<String, Double> symbols) {
		super();
		this.decimalFormat = decimalFormat;
		this.scientificFormat = scientificFormat;
		this.symbols = symbols;

		decimalFormat.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.US));
		scientificFormat.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.US));
		decimal = new DefaultFormatterFactory(new CassisFormateurFactory(decimalFormat));
		scientifique = new DefaultFormatterFactory(new CassisFormateurFactory(scientificFormat));
		setFormatterFactory(decimal);
		setColumns(9);
		setValue(Double.valueOf(0.0));
		setInputVerifier(new FormattedTextFieldVerifier());

		setHorizontalAlignment(JTextField.RIGHT);
	}

	public JDoubleCassisTextField(DecimalFormat decimalFormat) {
		this(decimalFormat, new DecimalFormat("#.###E0"), new HashMap<>());
		symbols.put("*", Double.MAX_VALUE);
	}

	public JDoubleCassisTextField() {
		this(new DecimalFormat("##0.0###"), new DecimalFormat("#.###E0"), new HashMap<>());
		symbols.put("*", Double.MAX_VALUE);
	}

	public JDoubleCassisTextField(int nbColumns, Double initValue) {
		this();
		setColumns(nbColumns);
		setValue(initValue);
	}

	/**
	 * @see javax.swing.JFormattedTextField#setValue(java.lang.Object)
	 */
	@Override
	public void setValue(Object value) {
		super.setValue(value);
		if (value instanceof Long) { // for polynomial
			Long val = Math.abs((Long) value);
			if (val == 0) {
				setFormatterFactory(decimal);
			}
			else if (val > valMax || val < valMin) {
				setFormatterFactory(scientifique);
			}
			else {
				setFormatterFactory(decimal);
			}
		}
		else if (value instanceof Double) {
			Double val = Math.abs((Double) value);
			if (val == 0) {
				setFormatterFactory(decimal);
			}
			else if (val > valMax || val < valMin) {
				setFormatterFactory(scientifique);
			}
			else {
				setFormatterFactory(decimal);
			}
		}
	}

	public class FormattedTextFieldVerifier extends InputVerifier {
		@Override
		public boolean verify(JComponent input) {
			if (input instanceof JFormattedTextField) {
				JFormattedTextField ftf = (JFormattedTextField) input;
				AbstractFormatter formatter = ftf.getFormatter();
				if (formatter != null) {
					String text = ftf.getText();
					if (symbols.containsKey(text)) {
						ftf.setBackground(Color.WHITE);
						return true;
					}
					try {
						formatter.stringToValue(text);
						ftf.setBackground(Color.WHITE);

						return true;
					} catch (ParseException pe) {
						LOGGER.debug("ParseException", pe);
						ftf.setBackground(Color.RED);
						return false;
					}
				}
			}
			return true;
		}

		@Override
		public boolean shouldYieldFocus(JComponent input) {
			return verify(input);
		}
	}

	public class CassisFormateurFactory extends NumberFormatter {

		public CassisFormateurFactory(DecimalFormat decimalformat) {
			super(decimalformat);
		}

		/**
		 * @see
		 * javax.swing.text.InternationalFormatter#valueToString(java.lang.Object
		 * )
		 */
		@Override
		public String valueToString(Object value) throws ParseException {
			if (value != null) {
				if (symbols.containsValue(value)) {
					return getKey(value);
				}
				else if (value instanceof Double) {
					Double val = Math.abs((Double) value);
					if (val == 0) {
						return decimalFormat.format(value);
					}
					else if (val > valMax || val < valMin) {
						return scientificFormat.format(value);
					}
					else {
						return decimalFormat.format(value);
					}
				}
				else if (value instanceof Long) {
					Long val = Math.abs((Long) value);
					if (val == 0) {
						return decimalFormat.format(value);
					}
					else if (val > valMax || val < valMin) {
						return scientificFormat.format(value);
					}
					else {
						return decimalFormat.format(value);
					}
				}
			}
			return super.valueToString(value);
		}

		private String getKey(Object value) {
			String val = null;
			for (String key : symbols.keySet()) {
				if (symbols.get(key).equals(value))
					val = key;
			}
			return val;
		}

		/**
		 * @see
		 * javax.swing.text.InternationalFormatter#stringToValue(java.lang.String
		 * )
		 */
		@Override
		public Object stringToValue(String text) throws ParseException {
			text = text.toUpperCase();
			if (symbols.containsKey(text)) {
				text = String.valueOf(symbols.get(text));
			}
			return super.stringToValue(text);
		}
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame("Test");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel panel = new JPanel();
		panel.add(new JDoubleCassisTextField());
		DecimalFormat decimalFormat = new DecimalFormat("##0.0###");
		DecimalFormat scientificFormat = new DecimalFormat("##.##E0");
		Map<String, Double> symbols = new HashMap<>();
		symbols.put("vlsr File", Double.MIN_VALUE);
		panel.add(new JDoubleCassisTextField(decimalFormat, scientificFormat, symbols));
		frame.setContentPane(panel);
		frame.pack();
		frame.setVisible(true);

	}

	public void setSymbols(Map<String, Double> symbols) {
		this.symbols = symbols;
	}

	/**
	 * @param decimalFormat
	 *            the decimalFormat to set
	 */
	public final void setDecimalFormat(DecimalFormat decimalFormat) {
		this.decimalFormat = decimalFormat;
		decimalFormat.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.US));
		decimal = new DefaultFormatterFactory(new CassisFormateurFactory(decimalFormat));
	}

	/**
	 * @param scientificFormat
	 *            the scientificFormat to set
	 */
	public final void setScientificFormat(DecimalFormat scientificFormat) {
		this.scientificFormat = scientificFormat;
		scientificFormat.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.US));
		scientifique = new DefaultFormatterFactory(new CassisFormateurFactory(scientificFormat));
	}

	/**
	 * @param valMax
	 *            the valMax to set
	 */
	public final void setValMax(double valMax) {
		this.valMax = valMax;
	}

	public void setValMin(double valMin) {
		this.valMin = valMin;
	}

	/**
	 * Add symbol * to the symbols.
	 */
	public void addSymbolEtoile() {
		symbols.put("*", Double.MAX_VALUE);
	}
}
