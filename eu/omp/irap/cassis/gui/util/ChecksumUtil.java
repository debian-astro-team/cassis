/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class to compute the checksum (md5) of a file.
 */
public class ChecksumUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChecksumUtil.class);


	/**
	 * Utility class, should not be used.
	 */
	private ChecksumUtil() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * Return the MD5 for a file.
	 *
	 * @param path The path to the file.
	 * @return the MD5 or null if there was an error.
	 */
	public static String getMd5(String path) {
		String md5 = null;
		try {
			md5 = getHex(createChecksum(path));
		} catch (Exception e) {
			LOGGER.debug("Unable to create md5sum", e);
		}
		return md5;
	}

	/**
	 * Create the checksum of a file.
	 *
	 * @param filename The path of the filename.
	 * @return the hexadecimal value of the checksum.
	 * @throws NoSuchAlgorithmException
	 * @throws IOException
	 */
	private static byte[] createChecksum(String filename) throws NoSuchAlgorithmException, IOException {
		MessageDigest complete = null;

		try (InputStream fis = new FileInputStream(filename)) {
			byte[] buffer = new byte[1024];
			complete = MessageDigest.getInstance("MD5");
			int numRead;

			do {
				numRead = fis.read(buffer);
				if (numRead > 0) {
					complete.update(buffer, 0, numRead);
				}
			} while (numRead != -1);
		}

		return complete.digest();
	}

	/**
	 * Convert hexadecimal value to string.
	 *
	 * @param rawValue The hexadecimal value.
	 * @return The string value.
	 */
	private static String getHex(byte[] rawValue) {
		if (rawValue == null) {
			throw new IllegalArgumentException("The rawValue argument cannot be null.");
		}

		final String HEXES = "0123456789ABCDEF";
		final StringBuilder hex = new StringBuilder(2 * rawValue.length);

		for (final byte b : rawValue) {
			hex.append(HEXES.charAt((b & 0xF0) >> 4)).append(HEXES.charAt(b & 0x0F));
		}
		return hex.toString().toLowerCase();
	}

}
