/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.file.gui.download.FileDownloadHelper;


/**
 * SwingWorker to download a list of files.
 *
 * @author M.Boiziot
 */
public class DownloadMultiWorker extends SwingWorker<Void, Void> {

	private static final int BUFFER_SIZE = 8192;
	private static final Logger LOGGER = LoggerFactory.getLogger(DownloadMultiWorker.class);
	private DownloadMultiGui gui;
	private List<String> urls;
	private String pathFolderDestination;
	private List<String> pathDownloadedFiles;
	private boolean downloadError;


	/**
	 * Constructor.
	 *
	 * @param gui Graphical user interface associated to this download.
	 * @param urls The URLs of the file to download.
	 * @param pathFolderDestination The path to the folder to download the files.
	 */
	public DownloadMultiWorker(DownloadMultiGui gui, List<String> urls, String pathFolderDestination) {
		this.gui = gui;
		this.urls = urls;
		this.pathFolderDestination = pathFolderDestination;
		this.downloadError = false;
		this.pathDownloadedFiles = new ArrayList<>(urls.size());
	}

	/**
	 * Download the files in a background thread.
	 *
	 * @see javax.swing.SwingWorker#doInBackground()
	 */
	@Override
	protected Void doInBackground() throws Exception {
		int nbFile = urls.size();
		int current = 0;
		String localPath = null;
		double percentByFile = 100.0 / nbFile;
		for (String url : urls) {
			if (isCancelled() || downloadError) {
				return null;
			}
			try (FileDownloadHelper fdh = new FileDownloadHelper(url)) {
				InputStream is = fdh.download();

				localPath = getUniquePath(this.pathFolderDestination, fdh.getFileName());

				try (FileOutputStream fos = new FileOutputStream(localPath)) {
					byte[] buffer = new byte[BUFFER_SIZE];
					int read = -1;
					long totalRead = 0;
					long size = fdh.getSize();

					while ((read = is.read(buffer)) != -1 && !isCancelled()) {
						fos.write(buffer, 0, read);
						if (size != -1) {
							totalRead += read;
							updateProgress(current, percentByFile, totalRead, size);
						}
						if (isCancelled()) {
							return null;
						}
					}
				}

			} catch (IOException ioe) {
				LOGGER.error("Error while downloading file {}", url, ioe);
				this.downloadError = true;
				setProgress(0);
				cancel(true);
			} finally {
				if (isCancelled() || downloadError) {
					File downloadedFile = new File(localPath);
					if (downloadedFile.exists()) {
						try {
							if (!downloadedFile.delete()) {
								LOGGER.debug("Can not delete file {}", localPath);
							}
						} catch (Exception e) {
							LOGGER.debug("Can not delete file {}", localPath, e);
							// Fail Silently.
						}
					}
				} else {
					pathDownloadedFiles.add(localPath);
				}
			}
			current++;
		}
		return null;
	}

	/**
	 * Update the progress of the download.
	 *
	 * @param current Index of the spectrum in the list (starting at 0).
	 * @param percentByFile Percent of the download by file.
	 * @param totalRead Total value downloaded for the current file.
	 * @param size Size of the currently downloaded file.
	 */
	private void updateProgress(int current, double percentByFile, long totalRead,
			long size) {
		double progressCurrentFile = ((double) totalRead) / size;
		int totalProgress = (int) (current * percentByFile + progressCurrentFile * percentByFile);
		if (totalProgress >= 0 && totalProgress <= 100) {
			setProgress(totalProgress);
		}
	}

	/**
	 * Display an error message in EDT in case of error.
	 *
	 * @see javax.swing.SwingWorker#done()
	 */
	@Override
	protected void done() {
		if (isCancelled() && downloadError) {
				JOptionPane.showMessageDialog(gui, "Error during download.",
						"Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Return the paths of the downloaded files.
	 *
	 * @return the paths of the downloaded files.
	 */
	public List<String> getPathDownloadedFiles() {
		return pathDownloadedFiles;
	}

	/**
	 * Return the extension of a file.
	 *
	 * @param filePath the path of the file.
	 * @return the extension.
	 */
	private static String getFileExtension(String filePath) {
		File tmpFile = new File(filePath);
		int dotIndex = tmpFile.getName().lastIndexOf('.');
		if (dotIndex > 0 && dotIndex <= tmpFile.getName().length() - 2) {
			return tmpFile.getName().substring(dotIndex + 1);
		}
		return "";
	}

	/**
	 * Get unique path to download a file.
	 *
	 * @param pathFolderDestination The path of the folder who will contains the file.
	 * @param filename The original file name.
	 * @return The path with the eventual new file name.
	 */
	private static String getUniquePath(String pathFolderDestination, String filename) {
		File f = Paths.get(pathFolderDestination, filename).toFile();
		if (f.exists()) {
			String ext = getFileExtension(filename);
			String newFileName = null;
			int val = 0;
			while (f.exists()) {
				val++;
				if (ext.isEmpty() || ext.length() > 10) {
					newFileName = filename + '_' + val;
				} else {
					int index = filename.indexOf(ext);
					newFileName = filename.substring(0, index - 1) + "_" + val + "." + ext;
				}
				f = Paths.get(pathFolderDestination, newFileName).toFile();
			}
		}
		return f.getPath();
	}
}
