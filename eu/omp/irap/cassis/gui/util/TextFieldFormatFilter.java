/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.util;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JTextField;

/**
 * This class looks at the text entered in the TextField each time there is a
 * modification, and checks that the text corresponds to the format (TEXT,
 * INTEGER, DECIMAL, SCIENTIFIC) choosed at the instanciation.
 *
 * @author pelouas
 * @since 12 mai 2006
 */
public class TextFieldFormatFilter extends KeyAdapter {

	public static final int TEXT = 0;
	public static final int INTEGER = 1;
	public static final int DECIMAL = 2;
	public static final int SCIENTIFIC = 3;
	public static final int SCIENTIFIC_NUMBER = 4;

	public static final int POSITIVE = 0;
	public static final int ALL = 1;

	public static final int POINT = 0;
	public static final int COMMA = 1;

	private String vOld; // store the old value
	private int format; // format choosed
	private int sign; // sign restriction choosed
	private char defSep; // default separator
	private char otherSep; // other separator that will be replaced by the defSep


	/**
	 * Constructor makes a new TextFieldFormatFilter. The initial value is put
	 * at "" if text and 0 if other. If INTEGER or DECIAML is choosed, values
	 * are restricted to positive sign, and default separator is set to point.
	 *
	 * @param format
	 *            format choosed: it can be TEXT, INTEGER, or DECIMAL.
	 */
	public TextFieldFormatFilter(int format) {
		this.format = format;
		this.sign = POSITIVE;
		this.defSep = '.';
		this.otherSep = ',';
		if (format == TEXT) {
			this.vOld = "";
		}
		else if (format == SCIENTIFIC) {
			this.vOld = "0E0";
		}
		else if (format == SCIENTIFIC_NUMBER) {
			this.vOld = "0E0";
		}
		else {
			this.vOld = "0";
		}
	}

	/**
	 * Constructor makes a new TextFieldFormatFilter. The initial value is put
	 * at "" if text and 0 if other.If INTEGER or DECIAML is choosed, default
	 * separator is set to point.
	 *
	 * @param format
	 *            format choosed: it can be TEXT, INTEGER, or DECIMAL
	 * @param sign
	 *            choose if the value can be ony POSITIVE or if ALL sign is
	 *            accepted
	 */
	public TextFieldFormatFilter(int format, int sign) {
		this.format = format;
		this.defSep = '.';
		this.otherSep = ',';
		if (format == TEXT) {
			this.vOld = "";
		}
		else if (format == SCIENTIFIC) {
			this.vOld = "0E0";
		}
		else {
			this.vOld = "0";
		}

		if (sign == POSITIVE) {
			this.sign = POSITIVE;
		}
		else {
			this.sign = ALL;
		}
	}

	/**
	 * Constructor makes a new TextFieldFormatFilter with a initial value. If
	 * INTEGER or DECIAML is choosed, values are restricted to positive sign and
	 * default separator is set to point.
	 *
	 * @param format
	 *            format choosed: it can be TEXT, INTEGER, or DECIMAL.
	 * @param val
	 *            initial value
	 */
	public TextFieldFormatFilter(int format, String val) {
		this.format = format;
		this.vOld = val;
		this.sign = POSITIVE;
		this.defSep = '.';
		this.otherSep = ',';
	}

	/**
	 * Constructor makes a new TextFieldFormatFilter with a initial value.
	 * Default separator is set to point.
	 *
	 * @param format
	 *            format choosed: it can be TEXT, INTEGER, or DECIMAL
	 * @param val
	 *            initial value
	 * @param sign
	 *            choose if the value can be ony POSITIVE or if ALL sign is
	 *            accepted
	 */
	public TextFieldFormatFilter(int format, String val, int sign) {
		this.format = format;
		this.vOld = val;
		this.defSep = '.';
		this.otherSep = ',';

		if (sign == POSITIVE) {
			this.sign = POSITIVE;
		}
		else {
			this.sign = ALL;
		}
	}

	/**
	 * Constructor makes a new TextFieldFormatFilter with a initial value.
	 * Default separator is set to point.
	 *
	 * @param format
	 *            format choosed: it can be TEXT, INTEGER, or DECIMAL
	 * @param val
	 *            initial value
	 * @param sign
	 *            choose if the value can be ony POSITIVE or if ALL sign is
	 *            accepted
	 * @param separator
	 *            separator used for decimal numbers
	 */
	public TextFieldFormatFilter(int format, String val, int sign, int separator) {
		this.format = format;
		this.vOld = val;

		if (separator == POINT) {
			this.defSep = '.';
			this.otherSep = ',';
		}
		else {
			this.defSep = ',';
			this.otherSep = '.';
		}

		if (sign == POSITIVE) {
			this.sign = POSITIVE;
		}
		else {
			this.sign = ALL;
		}
	}

	/**
	 * When the user release the key, checks the new value and returns to old if
	 * the format is incorrect.
	 *
	 * @param e
	 *            key event
	 */
	@Override
	public void keyReleased(KeyEvent e) {

		// recover the new value
		String vNew = ((JTextField) e.getSource()).getText();

		// recover the caret position
		int c = ((JTextField) e.getSource()).getCaretPosition();

		int pts = 0;
		int pts2 = 0;
		int eLetter = 0;

		// check the new value according to the selected format
		boolean ok = true;
		switch (this.format) {
			case TEXT:
				// nothing...
				break;

			case INTEGER:
				// Check each char
				for (int i = 0; (i < vNew.length()); i++) {
					if (vNew.charAt(i) < '0' || vNew.charAt(i) > '9') {
						ok = false;
					}
				}
				break;

			case DECIMAL:
				// Check each char and count points
				for (int i = 0; (i < vNew.length()); i++) {
					// Change comma to point.
					if (vNew.charAt(i) == this.otherSep) {
						vNew = vNew.replace(this.otherSep, this.defSep);
						((JTextField) e.getSource()).setText(vNew);
					}

					if ((vNew.charAt(i) < '0' || vNew.charAt(i) > '9') && vNew.charAt(i) != this.defSep
							&& vNew.charAt(i) != '-') {
						ok = false;
					}

					if (vNew.charAt(i) == '-' && (i > 0 || this.sign == POSITIVE)) {
						ok = false;
					}

					// count points
					if (vNew.charAt(i) == this.defSep) {
						pts++;
					}
				}

				// Check point number
				if (pts > 1) {
					ok = false;
				}
				break;

			case SCIENTIFIC:
				// Check each char and count points
				for (int i = 0; (i < vNew.length()); i++) {
					// Change comma to point.
					if (vNew.charAt(i) == this.otherSep) {
						vNew = vNew.replace(this.otherSep, this.defSep);
						((JTextField) e.getSource()).setText(vNew);
					}

					// Change 'e' to 'E'.
					if (vNew.charAt(i) == 'e') {
						vNew = vNew.replace('e', 'E');
						((JTextField) e.getSource()).setText(vNew);
					}

					if ((vNew.charAt(i) < '0' || vNew.charAt(i) > '9') && vNew.charAt(i) != this.defSep
							&& vNew.charAt(i) != 'E' && vNew.charAt(i) != '-') {
						ok = false;
					}

					if (vNew.charAt(i) == '-' && (i > 0 || this.sign == POSITIVE)) {
						ok = false;
					}

					// count 'E'
					if (vNew.charAt(i) == 'E') {
						eLetter++;
					}

					// count points
					if (vNew.charAt(i) == this.defSep) {
						if (eLetter == 0) {
							pts++;
						}
						else {
							pts2++;
						}
					}
				}

				// check that we havent two points before or after the E
				if (pts > 1 || pts2 > 1) {
					ok = false;
				}

				// Check 'E' number
				if (eLetter > 1) {
					ok = false;
				}
				// Check each char and count points
				for (int i = 0; (i < vNew.length()); i++) {
					// Change comma to point.
					if (vNew.charAt(i) == this.otherSep) {
						vNew = vNew.replace(this.otherSep, this.defSep);
						((JTextField) e.getSource()).setText(vNew);
					}
					// Change 'e' to 'E'.
					if (vNew.charAt(i) == 'e') {
						vNew = vNew.replace('e', 'E');
						((JTextField) e.getSource()).setText(vNew);
					}
					if ((vNew.charAt(i) < '0' || vNew.charAt(i) > '9') && vNew.charAt(i) != this.defSep
							&& vNew.charAt(i) != 'E' && vNew.charAt(i) != '-') {
						ok = false;
					}

					// count 'E'
					if (vNew.charAt(i) == 'E') {
						eLetter++;
					}

					// count points
					if (vNew.charAt(i) == this.defSep) {
						if (eLetter == 0) {
							pts++;
						}
						else {
							pts2++;
						}
					}
				}

				// check that we havent two points before or after the E
				if (pts > 1 || pts2 > 1) {
					ok = false;
				}

				// Check 'E' number
				if (eLetter > 1) {
					ok = false;
				}
				break;

			case SCIENTIFIC_NUMBER:
				// Check each char and count points
				for (int i = 0; (i < vNew.length()); i++) {
					// Change comma to point.
					if (vNew.charAt(i) == this.otherSep) {
						vNew = vNew.replace(this.otherSep, this.defSep);
						((JTextField) e.getSource()).setText(vNew);
					}
					// Change 'e' to 'E'.
					if (vNew.charAt(i) == 'e') {
						vNew = vNew.replace('e', 'E');
						((JTextField) e.getSource()).setText(vNew);
					}
					if ((vNew.charAt(i) < '0' || vNew.charAt(i) > '9') && vNew.charAt(i) != this.defSep
							&& vNew.charAt(i) != 'E' && vNew.charAt(i) != '-') {
						ok = false;
					}

					// count 'E'
					if (vNew.charAt(i) == 'E') {
						eLetter++;
					}

					// count points
					if (vNew.charAt(i) == this.defSep) {
						if (eLetter == 0) {
							pts++;
						}
						else {
							pts2++;
						}
					}
				}

				// check that we havent two points before or after the E
				if (pts > 1 || pts2 > 1) {
					ok = false;
				}

				// Check 'E' number
				if (eLetter > 1) {
					ok = false;
				}
				break;
		}

		// check if an error has been found
		if (ok) {
			// no error: save the new value
			this.vOld = vNew;
		}
		else {
			// an error has been found: restore the old value
			((JTextField) e.getSource()).setText(this.vOld);

			c--;
		}

		// control caret position
		vNew = ((JTextField) e.getSource()).getText();
		if (c < 0) {
			c = 0;
		}
		else if (c > vNew.length()) {
			c = vNew.length();
		}

		// replace the caret
		((JTextField) e.getSource()).setCaretPosition(c);
	}

}
