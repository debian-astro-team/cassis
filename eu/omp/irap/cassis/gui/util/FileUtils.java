/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.file.EXTENSION;

public class FileUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(FileUtils.class);


	/**
	 * Copy the file to dst file. If the dst file does not exist it is created.
	 *
	 * @param src The file to copy.
	 * @param dst The new file.
	 * @throws IOException In case of IO error.
	 */
	public static void copy(File src, File dst) throws IOException {
		String parentDir = dst.getParent();
		if (!new File(parentDir).exists()) {
			new File(parentDir).mkdirs();
		}
		try (InputStream in = new FileInputStream(src);
				OutputStream out = new FileOutputStream(dst)){
			// Transfer bytes from in to out
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
		}
	}

	/**
	 * Copy a file from a source to a destination. The IOException are
	 * directly catched here and not propagated.
	 *
	 * @param source The file to copy
	 * @param destination The new file
	 * @return true if the operation  was successful, false otherwise.
	 */
	public static boolean copyFile(File source, File destination) {
		boolean copyOk = false;
		try {
			FileUtils.copy(source, destination);
			copyOk = true;
		} catch (IOException e) {
			LOGGER.error("An exception occured while copying file {} to {}",
					source.getAbsolutePath(), destination.getAbsolutePath(), e);
			// Stay with copyOk to false;
		}
		return copyOk;
	}

	/**
	 * TODO force argument true not supported !<br>
	 * Delete recursivly the specified directory and its content.<br>
	 * Warning : this method doesn't check if path is null.
	 *
	 * @param path
	 *            the directory to delete
	 *
	 * @throws IOException
	 *             if one of the file can't be deleted or an IOException occurs.
	 * @throws FileNotFoundException
	 *             if path not exists
	 */
	public static void recursiveDelete(File path) throws IOException, FileNotFoundException {
		if (!path.exists())
			throw new FileNotFoundException(path.getAbsolutePath());

		if (path.isDirectory()) {
			File[] children = path.listFiles();
			for (int i = 0; children != null && i < children.length; i++)
				recursiveDelete(children[i]);
			if (!path.delete()) {
				throw new IOException("No delete path '" + path.getAbsolutePath() + "'");
			}
		}
		else if (!path.delete()) {
			throw new IOException("No delete file '" + path.getAbsolutePath() + "'");
		}
	}

	public static String getFileExtension(String nomFichier) {
		File tmpFichier = new File(nomFichier);
		tmpFichier.getName();
		int posPoint = tmpFichier.getName().lastIndexOf('.');
		if (0 < posPoint && posPoint <= tmpFichier.getName().length() - 2) {
			return tmpFichier.getName().substring(posPoint + 1);
		}
		return "";
	}

	/**
	 * Download a file with the given url and rename it if necessary (needed for vospecflow for exemple).
	 *
	 * @param stringUrl The url of the file.
	 * @param isTemp If the file should be removed when exiting the application.
	 * @return the downloaded file or null if there was an error while downloading the file.
	 */
	public static File downloadAndRenameIfNeeded(String stringUrl, boolean isTemp) {
		File file = download(stringUrl, true);
		if (file != null) {
			String nameBaseFile = file.getName();
			int index = nameBaseFile.lastIndexOf('.');
			String extension = null;
			if (index != -1) {
				extension = nameBaseFile.substring(index + 1);
			}

			if (extension == null || !EXTENSION.exists(extension)) {
				try {
					String contentType = file.toURI().toURL().openConnection().getContentType();
					if ("application/xml".equals(contentType)) {
						File newFile = null;
						if (nameBaseFile.length() > 50) {
							newFile = File.createTempFile("CassisTempFile_", ".votable");
						} else {
							newFile = new File(file.getAbsolutePath() + ".votable");
						}

						boolean rename = move(file, newFile);
						if (rename) {
							file = new File(newFile.getAbsolutePath());
							if (isTemp) {
								file.deleteOnExit();
							}
						}
					}
				} catch (Exception e) {
					LOGGER.debug("An Exception occured while trying to rename the file", e);
				}
			}
		}
		return file;
	}

	/**
	 * Try to move/rename a file.
	 *
	 * @param baseFile The file to move.
	 * @param newFile The new file.
	 * @return true if the file was moved, false else.
	 */
	public static boolean move(File baseFile, File newFile) {
		if (!newFile.exists()) {
			if (!baseFile.renameTo(newFile)) {
				try {
					copy(baseFile, newFile);
					if (!baseFile.delete()) {
						LOGGER.error("Unable to delete file {}", baseFile.getAbsolutePath());
					}
				} catch (IOException e) {
					LOGGER.error("Error while renaming/moving file {} to {}",
							baseFile.getAbsoluteFile(), newFile.getAbsolutePath(), e);
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Download a file with the given url.
	 *
	 * @param stringUrl The url of the file.
	 * @param isTemp If the file should be removed when exiting the application.
	 * @return the downloaded file or null if there was an error while downloading the file.
	 */
	public static File download(String stringUrl, boolean isTemp) {
		if (stringUrl == null) {
			return null;
		}

		ReadableByteChannel rbc = null;
		FileOutputStream fileOutputStream = null;
		try {
			URL urlSource = new URL(stringUrl);
			URLConnection urlConnection = urlSource.openConnection();
			urlConnection.setUseCaches(false);
			urlConnection.setDefaultUseCaches(false);
			rbc = Channels.newChannel(urlConnection.getInputStream());

			int index = stringUrl.lastIndexOf('/');

			String filename = null;
			if (index != -1) {
				filename = stringUrl.substring(index + 1);
			}

			index = filename.lastIndexOf('.');
			String extention = null;
			String name = null;
			if (index != -1) {
				name = filename.substring(0, index);
				extention = "." + filename.substring(index + 1);
			}

			if (name != null && name.length() > 50)
				name = "";
			if (extention != null && extention.length() > 10)
				extention = "";

			File file = File.createTempFile("CassisTempFile_" + name, extention);
			if (isTemp) {
				file.deleteOnExit();
			}

			fileOutputStream = new FileOutputStream(file);
			FileChannel out = fileOutputStream.getChannel();

			long bytesTransferred;
			long position = 0L;

			while (true) {
				bytesTransferred = out.transferFrom(rbc, position, 65536);
				position += bytesTransferred;
				if (bytesTransferred == 0) {
					break;
				}
			}

			return file;
		} catch (Exception e) {
			LOGGER.error("An error occured while downloading file {}", stringUrl);
			return null;
		} finally {
			if (fileOutputStream != null) {
				try {
					fileOutputStream.close();
				} catch (IOException e) {
					LOGGER.error("Unable to close the FileOutputStream", e);
				}
			}
			if (rbc != null) {
				try {
					rbc.close();
				} catch (IOException e) {
					LOGGER.error("Unable to close the ReadableByteChannel", e);
				}
			}
		}
	}
}
