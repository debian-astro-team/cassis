/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.util;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.swing.JButton;

import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYDataset;

/**
 * Class which allows to have a great chart.
 *
 * @author Rose Nerriere
 * @since 09/07/2007
 */
public class GraphicParameter {

	private static final String FONT = "Lucida Bright";
	private static final DecimalFormatSymbols formatSymbol = new DecimalFormatSymbols(Locale.US);

	/**
	 * Initialize the graphic of the plot.
	 *
	 * @param nameWindow The name of the window.
	 * @param inPlot The plot.
	 * @param inXAxis The X axis.
	 * @param inYAxis The Y axis.
	 * @param inData The dataset.
	 */
	public static void initializeGraphic(String nameWindow, XYPlot inPlot,
			ValueAxis inXAxis, ValueAxis inYAxis, XYDataset inData) {
		// Increase the size of the labels of axes
		initializeFontLabel(inXAxis, inYAxis);
		initializeTicks(inXAxis, inYAxis);

		// the frame of the plot is black
		XYPlot plot = inPlot;
		plot.setOutlinePaint(Color.BLACK);
		plot.setOutlineStroke(new BasicStroke(1));
		// processing of ticks of the right and height axes
		NumberAxis xAxis2 = new NumberAxis("");
		NumberAxis yAxis2 = new NumberAxis("");
		yAxis2.setRange(inYAxis.getRange());
		yAxis2.setDefaultAutoRange(inYAxis.getRange());
		yAxis2.setAutoRangeIncludesZero(false);
		yAxis2.setAutoRange(false);
		yAxis2.setAutoRangeStickyZero(false);
		xAxis2.setRange(inXAxis.getRange());
		xAxis2.setDefaultAutoRange(inXAxis.getRange());
		xAxis2.setAutoRangeIncludesZero(false);
		xAxis2.setAutoRange(false);

		plot.setRangeAxis(1, yAxis2);

		if (!"LineSpectrum".equals(nameWindow)) {
			xAxis2.setAutoRangeIncludesZero(false);
			if ("FullSpectrum".equals(nameWindow)) {
				xAxis2.setNumberFormatOverride(new DecimalFormat("#.####", formatSymbol));
			}
			plot.setDomainAxis(1, xAxis2);
		}
		else {
			initializeFontLabel(plot.getDomainAxis(1), yAxis2);
			initializeTicks(plot.getDomainAxis(1), yAxis2);
		}
		JButton colorButton = new JButton();
		xAxis2.setTickLabelPaint(colorButton.getBackground());
		yAxis2.setTickLabelPaint(colorButton.getBackground());
		yAxis2.setNumberFormatOverride(new DecimalFormat("0.000", formatSymbol));
		Font font = new Font(FONT, Font.PLAIN, 13);
		xAxis2.setTickLabelFont(font);
		yAxis2.setTickLabelFont(font);
		initializeTicks(xAxis2, yAxis2);
		// Associate the data to the second axis
		if (inData != null) {
			List<Integer> axisIndices = Arrays.asList(new Integer[] { 0, 1 });
			plot.mapDatasetToRangeAxes(0, axisIndices);

			if (!"LineSpectrum".equals(nameWindow)) {
				plot.mapDatasetToDomainAxes(0, axisIndices);
			}
		}
	}

	/**
	 * Initialize the type of the font.
	 *
	 * @param inXAxis The X axis.
	 * @param inYAxis The Y axis.
	 */
	public static void initializeFontLabel(ValueAxis inXAxis, ValueAxis inYAxis) {
		Font font = new Font(FONT, Font.BOLD, 15);
		inXAxis.setLabelFont(font);
		inYAxis.setLabelFont(font);
		font = new Font(FONT, Font.PLAIN, 13);
		inXAxis.setTickLabelFont(font);
		inYAxis.setTickLabelFont(font);
	}

	/**
	 * Initialize the type of the font.
	 *
	 * @param axis The axis to modify.
	 */
	public static void initializeFontLabel(ValueAxis axis) {
		Font font = new Font(FONT, Font.BOLD, 15);
		axis.setLabelFont(font);
		font = new Font(FONT, Font.PLAIN, 13);
		axis.setTickLabelFont(font);
	}

	/**
	 * Initialize the size of ticks.
	 *
	 * @param inXAxis The X axis.
	 * @param inYAxis The Y axis.
	 */
	public static void initializeTicks(ValueAxis inXAxis, ValueAxis inYAxis) {
		initializeTicks(inXAxis);
		initializeTicks(inYAxis);
	}

	/**
	 * Initialize the size of ticks.
	 *
	 * @param axis The axis to modify.
	 */
	public static void initializeTicks(ValueAxis axis) {
		axis.setTickMarkPaint(Color.BLACK);
		axis.setTickMarkOutsideLength(0);
		axis.setTickMarkInsideLength(8);
	}

}
