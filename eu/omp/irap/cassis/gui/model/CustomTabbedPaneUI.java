/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;

import javax.swing.plaf.basic.BasicTabbedPaneUI;

/**
 * Basic TabbedPane UI with a look close to TabbedPanel (from itp-gpl library).
 * It <b>MUST</b> only be used with tab on top.
 *
 * @author M. Boiziot
 */
public class CustomTabbedPaneUI extends BasicTabbedPaneUI {

	private Color bgColor;


	/**
	 * Constructor.
	 *
	 * @param bgColor
	 *            The color to use for the background.
	 */
	public CustomTabbedPaneUI(Color bgColor) {
		super();
		this.bgColor = bgColor;
	}

	/**
	 * @see javax.swing.plaf.basic.BasicTabbedPaneUI#paintTabBackground(java.awt.Graphics, int, int, int, int, int, int, boolean)
	 */
	@Override
	protected void paintTabBackground(Graphics g, int tabPlacement, int tabIndex, int x,
			int y, int w, int h, boolean isSelected) {
		g.setColor(bgColor);
		g.fillRect(x + 1, y + 1, w - 3, h - 1);
	}

	/**
	 * @see javax.swing.plaf.basic.BasicTabbedPaneUI#paintContentBorder(java.awt.Graphics, int, int)
	 */
	@Override
	protected void paintContentBorder(Graphics g, int tabPlacement, int selectedIndex) {
		boolean tabsOverlapBorder = true;
		int width = tabPane.getWidth();
		int height = tabPane.getHeight();
		Insets insets = tabPane.getInsets();
		Insets tabAreaInsets = getTabAreaInsets(tabPlacement);

		int x = insets.left;
		int y = insets.top;
		int w = width - insets.right - insets.left;
		int h = height - insets.top - insets.bottom;

		y += calculateTabAreaHeight(tabPlacement, runCount, maxTabHeight);
		if (tabsOverlapBorder) {
			y -= tabAreaInsets.bottom;
		}
		h -= y - insets.top;

		if (tabPane.getTabCount() > 0) {
			g.setColor(bgColor);
			g.fillRect(x, y, w, h);
		}

		paintContentBorderTopEdge(g, tabPlacement, selectedIndex, x, y, w, h);
		paintContentBorderLeftEdge(g, tabPlacement, selectedIndex, x, y, w, h);
		paintContentBorderBottomEdge(g, tabPlacement, selectedIndex, x, y, w, h);
		paintContentBorderRightEdge(g, tabPlacement, selectedIndex, x, y, w, h);
	}

	/**
	 * @see javax.swing.plaf.basic.BasicTabbedPaneUI#paintTabBorder(java.awt.Graphics, int, int, int, int, int, int, boolean)
	 */
	@Override
	protected void paintTabBorder(Graphics g, int tabPlacement, int tabIndex, int x,
			int y, int w, int h, boolean isSelected) {
		g.setColor(shadow);
		g.drawLine(x, y + 2, x, y + h - 1); // left highlight
		g.drawLine(x + 1, y + 1, x + 1, y + 1); // top-left highlight
		g.drawLine(x + 2, y, x + w - 3, y); // top highlight

		g.setColor(darkShadow);
		g.drawLine(x + w - 2, y + 2, x + w - 2, y + h - 1); // right shadow

		g.drawLine(x + w - 2, y + 1, x + w - 2, y + 1); // top-right shadow
	}

	/**
	 * @see javax.swing.plaf.basic.BasicTabbedPaneUI#paintFocusIndicator(java.awt.Graphics, int, java.awt.Rectangle[], int, java.awt.Rectangle, java.awt.Rectangle, boolean)
	 */
	@Override
	protected void paintFocusIndicator(Graphics g, int tabPlacement, Rectangle[] rects,
			int tabIndex, Rectangle iconRect, Rectangle textRect, boolean isSelected) {
		// Do no paint focus indicator.
	}

	/**
	 * @see javax.swing.plaf.basic.BasicTabbedPaneUI#paintContentBorderTopEdge(java.awt.Graphics, int, int, int, int, int, int)
	 */
	@Override
	protected void paintContentBorderTopEdge(Graphics g, int tabPlacement,
			int selectedIndex, int x, int y, int w, int h) {
		Rectangle selRect = selectedIndex < 0 ? null : getTabBounds(selectedIndex,
				calcRect);

		g.setColor(shadow);

		// Draw unbroken line if tabs are not on TOP, OR
		// selected tab is not in run adjacent to content, OR
		// selected tab is not visible (SCROLL_TAB_LAYOUT)
		if (tabPlacement != TOP || selectedIndex < 0
				|| selRect.y + selRect.height + 1 < y
				|| selRect.x < x || selRect.x > x + w) {
			g.drawLine(x, y, x + w - 2, y);
		} else {
			// Break line to show visual connection to selected tab
			g.drawLine(x, y, selRect.x - 1, y);
			if (selRect.x + selRect.width < x + w - 2) {
				g.drawLine(selRect.x + selRect.width, y, x + w - 2, y);
			} else {
				g.setColor(shadow);
				g.drawLine(x + w - 2, y, x + w - 2, y);
			}
		}
	}
}
