/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model;

import java.io.Serializable;

import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.gui.model.table.ModelIdentifiedInterface;

/**
 * This class represent a model for Cassis software.
 *
 * @author girard
 */
public class CassisModel implements Serializable {
	private static final long serialVersionUID = 4049637888738406964L;

	// To do the link with TableModel.
	private int modelId;

	private CommentedSpectrum spectrumSignal;

	private String configName = null;

	private String spectrumName = null;

	// The value of a table -> to recognize them in the arraylist of children.
	private static int uniqueID = 1;

	private ModelIdentifiedInterface model;


	/**
	 * Constructor makes standart a new CassisModel.
	 *
	 * @param inSpectrum The spectrum.
	 * @param model The model interface.
	 */
	public CassisModel(CommentedSpectrum inSpectrum, ModelIdentifiedInterface model) {
		this.model = model;
		setSpectrumSignal(inSpectrum);
	}

	public CassisModel(CommentedSpectrum inSpectrum) {
		setSpectrumSignal(inSpectrum);
	}

	/**
	 * Gives the model id.
	 *
	 * @return model id
	 */
	public int getModelId() {
		return modelId;
	}

	/**
	 * Changes the model id.
	 *
	 * @param modelId
	 *            new model id
	 */
	public void setModelId(int modelId) {
		this.modelId = modelId;
	}

	/**
	 * Gives the spectrum of this model.
	 *
	 * @return spectrum
	 */
	public CommentedSpectrum getSpectrumSignal() {
		return spectrumSignal;
	}

	/**
	 * Changes the spectrum of this model.
	 *
	 * @param inSpectrum
	 *            new spectrum
	 */
	private void setSpectrumSignal(CommentedSpectrum inSpectrum) {
		spectrumSignal = inSpectrum;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((configName == null) ? 0 : configName.hashCode());
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		result = prime * result + modelId;
		result = prime * result
				+ ((spectrumName == null) ? 0 : spectrumName.hashCode());
		result = prime * result
				+ ((spectrumSignal == null) ? 0 : spectrumSignal.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		boolean result = true;
		if (this == obj)
			return true;

		if (obj == null || obj.getClass() != this.getClass())
			return false;

		CassisModel casMod = (CassisModel) obj;

		// Due to the type of those values (boolean) we can use " == "
		result = this.modelId == casMod.modelId;

		return result;
	}

	public void setConfigName(String title) {
		configName = title;
	}

	/**
	 * @return the configName
	 */
	public final String getConfigName() {
		return configName;
	}

	public void setSpectrumName(String spectrumName) {
		this.spectrumName = spectrumName;
	}

	/**
	 * @return the spectrumName
	 */
	public final String getSpectrumName() {
		return spectrumName;
	}

	public static int setNewModelId() {
		int id = uniqueID;
		uniqueID++;

		return id;
	}

	public void change(CassisModel cassisModel) {
		setConfigName(cassisModel.getConfigName());
		setModelId(cassisModel.getModelId());
		setSpectrumSignal(cassisModel.getSpectrumSignal());
		setSpectrumName(cassisModel.getConfigName());
	}

	/**
	 * @return the model
	 */
	public ModelIdentifiedInterface getModel() {
		return model;
	}

	/**
	 * Set model.
	 * @param model The model to set.
	 */
	public void setModel(ModelIdentifiedInterface model) {
		this.model = model;
	}
}
