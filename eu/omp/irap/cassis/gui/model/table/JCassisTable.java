/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.table;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.common.Template;
import eu.omp.irap.cassis.gui.PanelView;
import eu.omp.irap.cassis.gui.template.JComboBoxTemplate;
import eu.omp.irap.cassis.gui.template.ListTemplateEvent;
import eu.omp.irap.cassis.gui.template.ListTemplateListener;
import eu.omp.irap.cassis.gui.template.ListTemplateManager;
import eu.omp.irap.cassis.gui.template.ManageTemplateControl;
import eu.omp.irap.cassis.gui.template.ManageTemplateModel;
import eu.omp.irap.cassis.gui.template.ManageTemplatePanel;
import eu.omp.irap.cassis.gui.util.Deletable;
import eu.omp.irap.cassis.gui.util.ListenersUtil;
import eu.omp.irap.cassis.template.SqlTemplateManager;
import eu.omp.irap.ssap.util.StringNumComparator;

public class JCassisTable<T> extends JTable implements ListTemplateListener, Deletable, TableListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(JCassisTable.class);
	private static final long serialVersionUID = 1L;
	private boolean computeCheckedAll = false;
	private JScrollPane scrollPane = null;
	private JComboBoxTemplate templateCombo;
	private CassisTableAbstractModel<T> dm;
	private JDialog jdialog;
	private boolean askForMerge;
	private boolean selectOnAnyColumn;
	private int lastSelectedIndex;


	/**
	 * Constructor. Create a JCassisTable with the merge disabled.
	 *
	 * @param dm The model.
	 */
	public JCassisTable(CassisTableAbstractModel<T> dm) {
		this(dm, false, false);
	}

	/**
	 * Constructor.
	 *
	 * @param dm The model.
	 * @param askForMerge True to ask for merge of molecule when possible,
	 *  false otherwise.
	 * @param selectOnAnyColumn True to add a listener allowing to select a
	 *  species on a click on any column, false otherwise.
	 */
	public JCassisTable(CassisTableAbstractModel<T> dm, boolean askForMerge,
			boolean selectOnAnyColumn) {
		super(dm);
		this.dm = dm;
		this.selectOnAnyColumn = selectOnAnyColumn;
		setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.getTableHeader().addMouseListener(getHeaderMouseListener());
		this.addMouseListener(getMouseListener());
		this.setRowSorter(new CassisTableRowSorter<T>(this.dm));
		this.setFillsViewportHeight(true);
		this.setDefaultRenderer(Double.class, new DoubleRenderer());
		this.askForMerge = askForMerge;
		this.lastSelectedIndex = -1;
		TableListenerList.getInstance().addTableListener(this);
		sort();
	}

	public void setModel(CassisTableAbstractModel<T> dm) {
		this.dm = dm;
		this.setRowSorter(new CassisTableRowSorter<T>(this.dm));
		if (!dm.getSourceList().isEmpty())
			super.setModel(this.dm);
	}

	/**
	 * @return the scrollPane
	 */
	public final JScrollPane getScrollPane() {
		return scrollPane;
	}

	/**
	 * @param templateCombo
	 *            link templateCombo with the JTable
	 */
	public final void setTemplateCombo(final JComboBoxTemplate templateCombo) {
		this.templateCombo = templateCombo;
		templateCombo.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				refreshMolecules((String) templateCombo.getSelectedItem());
			}
		});
		ListTemplateManager.getInstance().addListTemplateListener(this);
	}

	/**
	 * @param selectedTemplate the selectedTemplate to set
	 */
	public final void setSelectedTemplate(String selectedTemplate) {
		templateCombo.setSelectedItem(selectedTemplate);
	}

	private JDialog getJDialog() {
		if (jdialog == null) {
			jdialog = new JDialog();
		}
		return jdialog;
	}

	/**
	 * Handle the change of the template item selected, and to as needed save,
	 *  open template manager or simply change the template.
	 *
	 * @param template The template item selected.
	 */
	public void refreshMolecules(String template) {
		if (template != null) {
			if (JComboBoxTemplate.SAVE_TEMPLATE.equals(template)) {
				saveTemplate();
			} else if (JComboBoxTemplate.TEMPLATE_MANAGER.equals(template)) {
				openTemplateManager();
			} else if (JComboBoxTemplate.CHANGE_DATABASE.equals(template)) {
				PanelView.getInstance().getCassisActionMenu().getDatabaseConfigurationAction().actionPerformed(null);
			} else if (!JComboBoxTemplate.OPERATIONS_TEMPLATE.equals(template)
					&& !JComboBoxTemplate.TEMPLATE.equals(template)) {
				changeTemplate(template);
			}
		}
	}

	/**
	 * Change template.
	 *
	 * @param template The template to use.
	 */
	private void changeTemplate(String template) {
		CassisTableMoleculeModel ctmm = (CassisTableMoleculeModel) this.getModel();
		List<MoleculeDescription> selectedMolecules = ctmm.getSelectedMolecules();
		List<MoleculeDescription> templateMoleculeList =
				SqlTemplateManager.getInstance().getTemplateMolecules(template);

		if (selectedMolecules.isEmpty()) {
			ctmm.setList(templateMoleculeList);
		} else {
			if (askForMerge && haveSameMolecule(templateMoleculeList, selectedMolecules)
					&& askMerge()) {
				ctmm.setList(merge(templateMoleculeList, selectedMolecules));
			} else {
				ctmm.setList(templateMoleculeList);
			}
		}
		setSelectedTemplate(template);
	}

	/**
	 * Check if the new list contains a molecule who is in the oldListSelected.
	 *
	 * @param newList The new list.
	 * @param oldListSelected The old list of selected molecule.
	 * @return true if the new list contains a molecule in the old list, false otherwise.
	 */
	private boolean haveSameMolecule(List<MoleculeDescription> newList,
			List<MoleculeDescription> oldListSelected) {
		for (MoleculeDescription mol : oldListSelected) {
			if (getMol(newList, mol.getTag()) != null) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Search then return in the given list of molecules the one with the given tag.
	 *
	 * @param mols The list of molecules
	 * @param tag The tag.
	 * @return the molecule found in the list with the given tag or null if not found.
	 */
	private MoleculeDescription getMol(List<MoleculeDescription> mols, int tag) {
		for (MoleculeDescription mol : mols) {
			if (mol.getTag() == tag) {
				return mol;
			}
		}
		return null;
	}

	/**
	 * Merge the two list then sort the new one, settings the selected molecules firsts.
	 *
	 * @param baseMolecules The molecules of the template.
	 * @param molsToMerge The selected molecules of the previous template.
	 * @return the new molecule list.
	 */
	private List<MoleculeDescription> merge(List<MoleculeDescription> baseMolecules, List<MoleculeDescription> molsToMerge) {
		List<MoleculeDescription> mergedList = new ArrayList<>(baseMolecules.size());
		for (MoleculeDescription mol : baseMolecules) {
			MoleculeDescription molFound = getMol(molsToMerge, mol.getTag());
			if (molFound == null) {
				mergedList.add(mol);
			} else {
				mergedList.add(molFound);
			}
		}
		Collections.sort(mergedList, new Comparator<MoleculeDescription>() {
			@Override
			public int compare(MoleculeDescription firstMol, MoleculeDescription secondMol) {
				if ((firstMol.isCompute() && secondMol.isCompute()) ||
						(!firstMol.isCompute() && !secondMol.isCompute())) {
					return 0;
				} else if (firstMol.isCompute() && !secondMol.isCompute()) {
					return -1;
				} else {
					return 1;
				}
			}
		});
		return mergedList;
	}

	/**
	 * Ask the user if he want to merge the selected species with the new template.
	 *
	 * @return true to merge, false otherwise.
	 */
	private boolean askMerge() {
		int res = JOptionPane.showConfirmDialog(null,
				"There is at least one selected molecule in your current template who is also in the new one.\n"
				+ "Do you want to merge the molecules who are in both template ?",
				"Template merging", JOptionPane.YES_NO_OPTION);
		return res == JOptionPane.YES_OPTION;
	}

	/**
	 * Open the template manager.
	 */
	@SuppressWarnings("unchecked")
	private void openTemplateManager() {
		ManageTemplateControl control = ManageTemplateControl.getInstance();
		ManageTemplateModel model = control.getModel();
		ManageTemplatePanel panel = control.getView();
		panel.enableAddBtn(true);
		model.setCTAM((CassisTableAbstractModel<MoleculeDescription>) dm);

		jdialog = getJDialog();
		jdialog.setModal(true);
		model.setJDialog(jdialog);
		jdialog.setSize(1015, 680);
		jdialog.setLocationRelativeTo(null);
		jdialog.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		jdialog.setContentPane(panel);
		jdialog.setVisible(true);
		jdialog.pack();
	}

	/**
	 * Save the template.
	 */
	private void saveTemplate() {
		ManageTemplateControl templateControl = ManageTemplateControl.getInstance();
		ManageTemplateModel templateModel = templateControl.getModel();
		final List<MoleculeDescription> listMol =
				((CassisTableMoleculeModel) this.getModel()).getList();
		templateModel.setSelectedMoleculeList(listMol);
		if (templateControl.saveAsBtnClicked()) {
			getTemplateCombo().refreshList();
			refreshMolecules(templateModel.getLastTemplateAdded());
			((CassisTableMoleculeModel) this.getModel()).selectAll();
		}
	}

	/**
	 * @see eu.omp.irap.cassis.gui.template.ListTemplateListener#refreshListTemplate(eu.omp.irap.cassis.gui.template.ListTemplateEvent)
	 */
	@Override
	public void refreshListTemplate(ListTemplateEvent event) {
		String templateSelected = (String) getTemplateCombo().getSelectedItem();
		getTemplateCombo().refreshList(event.getListTemplates());
		if (JComboBoxTemplate.CHANGE_DATABASE.equals(templateSelected)) {
			templateSelected = Template.DEFAULT_TEMPLATE;
		}

		refreshMolecules(templateSelected);
	}

	/**
	 * @return the scrollPane
	 */
	public final JComboBoxTemplate getTemplateCombo() {
		return templateCombo;
	}

	/**
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable {
		ListTemplateManager.getInstance().removeListTemplateListener(this);
		TableListenerList.getInstance().removeTableListener(this);
		super.finalize();
	}

	/**
	 * @param scrollPane
	 *            the scrollPane to set
	 */
	public final void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}

	/**
	 * @return MouseListener of the header of JCassisMoleculeTable
	 */
	public MouseAdapter getHeaderMouseListener() {
		return new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent me) {
				if (getModel().getRowCount() == 0) {
					return;
				}
				final int selectedColumn = JCassisTable.this.getColumnModel().getColumnIndexAtX(me.getPoint().x);
				final int columnModelIndex = JCassisTable.this.getColumnModel().getColumn(selectedColumn).getModelIndex();
				List<CassisTableAbstract<T>> lines = getLines();
				int selectedColumnIndice = lines.get(0).convertIndice(columnModelIndex);

				if (SwingUtilities.isLeftMouseButton(me)) {
					handleHeaderLeftClick(columnModelIndex, lines, selectedColumnIndice);
				} else if (SwingUtilities.isRightMouseButton(me)) {
					handleHeaderRightClick(me, selectedColumn, columnModelIndex);
				}
				((AbstractTableModel) JCassisTable.this.getModel()).fireTableDataChanged();
			}
		};
	}

	public class DoubleRenderer extends JTextField implements TableCellRenderer {

		private static final long serialVersionUID = 1L;

		public DoubleRenderer() {
			setOpaque(true); // MUST do this for background to show up
			this.setHorizontalAlignment(JTextField.RIGHT);
			this.setBorder(new EmptyBorder(0, 0, 0, 0));
			this.addFocusListener(new FocusAdapter() {

				@Override
				public void focusLost(FocusEvent e) {
					DoubleRenderer.this.validate();
				}
			});
		}

		@Override
		public Component getTableCellRendererComponent(JTable table, Object doubleVal, boolean isSelected,
				boolean hasFocus, int row, int column) {
			if (doubleVal == null) {
				return null;
			}
			Double val = (Double) doubleVal;
			String pattern;
			if ((Math.abs(val) >= 10000 || Math.abs(val) <= 0.001) && val !=0) {
				pattern = "0.00E0";
			} else {
				pattern = "0.00#";
			}

			DecimalFormat myFormatter = new DecimalFormat(pattern);
			this.setText(myFormatter.format(val));

			return this;
		}
	}

	@Override
	public Component prepareRenderer(TableCellRenderer renderer, int rowIndex, int vColIndex) {
		if (renderer == null)
			return null;
		Component c = super.prepareRenderer(renderer, rowIndex, vColIndex);
		if (rowIndex % 2 == 0)
			c.setBackground(new Color(221, 221, 221));
		else
			c.setBackground(getBackground());// If not shaded, match the table's background

		JCassisTableConfiguration conf = JCassisTableConfiguration.getInstance();
		Color foreground = JCassisTableConfiguration.DEFAULT_COLOR;

		int ind = this.dm.getIndice(CassisTableMolecule.DATA_SOURCE_INDICE);
		ind = convertColumnIndexToView(ind);
		if (ind != -1) {
			foreground = conf.getColorFor((String)getValueAt(rowIndex, ind));
		}

		if (isCellSelected(rowIndex, vColIndex)) {
			Color c2 = Color.ORANGE;
			c.setBackground(c2);
		}
		c.setForeground(foreground);

		return c;
	}

	protected boolean selectMolecule(final int selectedColumn, final JDialog dialog, String text) {
		boolean res = false;
		res = selectLine(selectedColumn, text);
		dialog.dispose();
		this.requestFocus();
		return res;
	}

	public boolean selectLine(final int selectedColumn, String text) {
		clearSelection();

		int i = 0;
		int firstFoundIndex = -1;
		int nbLigne = getModel().getRowCount();
		List<CassisTableAbstract<T>> lines = getLines();
		int computeIndex = ((CassisTableAbstractModel<?>)dataModel).getIndice(
				CassisTableMolecule.COMPUTE_INDICE);
		boolean select = false;
		for (i = 0; i < nbLigne; i++) {
			if (String.valueOf(getValueAt(i, selectedColumn)).equals(text)) {
				if (firstFoundIndex == -1) {
					firstFoundIndex = i;
				}
				if (computeIndex != -1) {
					int modelRowIndex = convertRowIndexToModel(i);
					lines.get(modelRowIndex).set(true, computeIndex);
					select = true;
				}
			}
		}

		if (firstFoundIndex != -1) {
			setRowSelectionInterval(firstFoundIndex, firstFoundIndex);
			if (getScrollPane() != null) {
				int value = (getScrollPane().getVerticalScrollBar().getMaximum() - getScrollPane()
						.getVerticalScrollBar().getMinimum()) / nbLigne;
				getScrollPane().getVerticalScrollBar().setValue(value * (firstFoundIndex - 4));
			}
		}
		return select;
	}

	public CassisTableAbstractModel<T> getDM() {
		return dm;
	}

	/**
	 * Remove the listeners.
	 */
	public void removeListeners() {
		ListenersUtil.removeMouseListeners(this.getTableHeader());
		ListenersUtil.removeActionListeners(templateCombo);
		ListTemplateManager.getInstance().removeListTemplateListener(this);
		TableListenerList.getInstance().removeTableListener(this);
	}

	/**
	 * @see eu.omp.irap.cassis.gui.util.Deletable#delete()
	 */
	@Override
	public void delete() {
		removeListeners();
	}

	@Override
	public void refreshColor() {
		repaint();
	}

	/**
	 * Convert to double a given String value. Return Double.NaN if the value
	 *  can not be converted.
	 *
	 * @param value The value as a String.
	 * @return The converted value as a double or Double.NaN if the value can
	 *  not be converted.
	 */
	private double getDouble(String value) {
		double doubleValue;
		if (value == null) {
			doubleValue = Double.NaN;
		} else {
			try {
				doubleValue = Double.parseDouble(value);
			} catch (NumberFormatException nfe) {
				doubleValue = Double.NaN;
				LOGGER.debug("Bad input", nfe);
			}
		}
		return doubleValue;
	}

	/**
	 * Handle a left click on the header of the table.
	 *
	 * @param columnModelIndex The index of the column for the model.
	 * @param lines The lines.
	 * @param selectedColumnIndice The selected element (see CassisTableMolecule).
	 */
	@SuppressWarnings("unchecked")
	private void handleHeaderLeftClick(final int columnModelIndex,
			List<CassisTableAbstract<T>> lines, int selectedColumnIndice) {
		if (selectedColumnIndice == lines.get(0).getComputeIndex()) {
			headerComputeSelected(columnModelIndex, lines);
		} else if (((CassisTableAbstractModel<T>) JCassisTable.this.getModel()).getEditable()[columnModelIndex]
				&& selectedColumnIndice != CassisTableMolecule.COLLISION_INDICE) {
			String inputValue = JOptionPane.showInputDialog(
					JCassisTable.this.getParent(), "Value of "
							+ ((CassisTableAbstractModel<T>) JCassisTable.this.getModel())
									.getColumnName(columnModelIndex), 0.);
			double newDoubleValue = getDouble(inputValue);
			if (!Double.isNaN(newDoubleValue)) {
				for (CassisTableAbstract<T> line : lines) {
					line.set(newDoubleValue, columnModelIndex);
				}
			}
		}
		this.lastSelectedIndex = getSelectedRow();
	}

	/**
	 * @param columnModelIndex
	 * @param lines
	 */
	protected void headerComputeSelected(final int columnModelIndex,
			List<CassisTableAbstract<T>> lines) {
		this.computeCheckedAll = !this.computeCheckedAll;
		for (CassisTableAbstract<T> line : lines) {
			line.set(this.computeCheckedAll, columnModelIndex);
		}
	}

	/**
	 * Handle a left click on the header of the table.
	 *
	 * @param me The mouse event.
	 * @param selectedColumn The index of the column for the view.
	 * @param columnModelIndex The index of the column for the model.
	 */
	private void handleHeaderRightClick(MouseEvent me, final int selectedColumn,
			final int columnModelIndex) {
		JDialog searchDialog;
		Component comp = SwingUtilities.getRoot(this);
		if (comp instanceof JFrame) {
			JFrame frame = (JFrame) comp;
			searchDialog = new JDialog(frame);
		} else if (comp instanceof JDialog) {
			JDialog dialog = (JDialog) comp;
			searchDialog = new JDialog(dialog);
		} else {
			searchDialog = new JDialog();
		}
		searchDialog.setTitle("Search column (" + getModel().getColumnName(columnModelIndex) + ")");

		final JPanel panel = new JPanel(new BorderLayout());
		final JTextField textField = new JTextField(5);
		textField.setUI(new HintTextFieldUI("Type your search here. Hit enter to validate a result."));

		panel.add(textField, BorderLayout.NORTH);
		final JList<String> list = new JList<>();
		panel.add(new JScrollPane(list), BorderLayout.CENTER);
		textField.requestFocus();

		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					selectMolecule(selectedColumn, searchDialog, String.valueOf(list.getSelectedValue()));
				} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					list.requestFocus();
					list.setSelectedIndex(1);
				} else {
					final List<CassisTableAbstract<T>> sourceList = getLines();
					List<String> listData = new ArrayList<>();
					Class<?> c = sourceList.get(0).get(columnModelIndex).getClass();
					boolean numberData = c == Integer.class || c == Double.class || c == Float.class;
					for (int i = 0; i < sourceList.size(); i++) {
						String name = sourceList.get(i).get(columnModelIndex).toString();
						if (name.contains(textField.getText()) && !listData.contains(name)) {
							listData.add(name);
						}
					}
					Collections.sort(listData, new StringNumComparator(numberData));
					list.setListData(listData.toArray(new String[listData.size()]));
					list.setSelectedIndex(0);
					list.validate();
				}
			}
		});

		list.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					selectMolecule(selectedColumn, searchDialog, String.valueOf(list.getSelectedValue()));
				} else if (e.getKeyCode() == KeyEvent.VK_UP && list.getSelectedIndex() == 0) {
					textField.requestFocus();
				}
			}
		});
		list.addMouseListener(new MouseAdapter() {

			/**
			 * @see java.awt.event.MouseAdapter#mouseClicked(java.awt.event.MouseEvent)
			 */
			@Override
			public void mouseClicked(MouseEvent e) {
				super.mouseClicked(e);
				selectMolecule(selectedColumn, searchDialog, String.valueOf(list.getSelectedValue()));
			}
		});
		searchDialog.setModal(true);
		searchDialog.setSize(360, 200);
		searchDialog.setLocationRelativeTo(me.getComponent());
		searchDialog.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		searchDialog.setContentPane(panel);
		searchDialog.setVisible(true);
		searchDialog.pack();
	}

	/**
	 * Sort by name, then tag.
	 */
	private void sort() {
		List<RowSorter.SortKey> sortKeys = new ArrayList<>(2);
		int nameIdx = dm.getIndice(CassisTableMolecule.NAME_INDICE);
		if (nameIdx != -1) {
			sortKeys.add(new RowSorter.SortKey(nameIdx, SortOrder.ASCENDING));
		}
		int tagIdx = dm.getIndice(CassisTableMolecule.TAG_INDICE);
		if (tagIdx != -1) {
			sortKeys.add(new RowSorter.SortKey(tagIdx, SortOrder.ASCENDING));
		}
		getRowSorter().setSortKeys(sortKeys);
	}

	/**
	 * Compute then return the list of model indexes for given indexes between
	 *  lower and upper view indexes.
	 *
	 * @param lower The minimum view index.
	 * @param upper The maximum view index.
	 * @return The list of model indexes.
	 */
	private List<Integer> getRowModelIndexes(int lower, int upper) {
		List<Integer> indexes = new ArrayList<>(Math.abs(upper - lower) + 1);
		for (int i = lower; i <= upper; i++) {
			indexes.add(convertRowIndexToModel(i));
		}
		return indexes;
	}

	/**
	 * Check if for the given indexes (except the first one) the lines have all
	 *  the compute value selected.
	 * Ignore the first index as this is used for a left click then shift+left
	 *  click. The first click may change the first value, we then don't take
	 *  this one into account here.
	 *
	 * @param indexes The list of indexes.
	 * @param lines The list of lines.
	 * @param computeIndex The compute index (model).
	 * @return true if all species (except for the first one unchecked) have
	 *  compute checked, false otherwise.
	 */
	private boolean isAllCompute(List<Integer> indexes,
			List<CassisTableAbstract<T>> lines, int computeIndex) {
		boolean first = true;
		for (int i : indexes) {
			if (first) {
				first = false;
				continue;
			}
			boolean comp = (boolean) lines.get(i).get(computeIndex);
			if (!comp) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Return the lines.
	 *
	 * @return the lines.
	 */
	@SuppressWarnings("unchecked")
	private List<CassisTableAbstract<T>> getLines() {
		return ((CassisTableAbstractModel<T>) JCassisTable.this.getModel()).getSourceList();
	}

	/**
	 * Handle a left mouse with shift modifier.
	 *
	 * @param selectedRowIndex The selected row index (view).
	 * @param selectedColIndex The selected column index (view).
	 */
	private void handleLeftShiftClick(int selectedRowIndex, int selectedColIndex) {
		if (lastSelectedIndex == -1) {
			return;
		}
		if (!(isTagIndex(selectedColIndex) || isNameIndex(selectedColIndex))) {
			return;
		}
		List<CassisTableAbstract<T>> lines = getLines();
		int min = Math.min(selectedRowIndex, lastSelectedIndex);
		int max = Math.max(selectedRowIndex, lastSelectedIndex);
		int computeIndex = ((CassisTableAbstractModel<?>)dataModel).getIndice(
				CassisTableMolecule.COMPUTE_INDICE);
		List<Integer> indexes = getRowModelIndexes(min, max);
		boolean compute = !isAllCompute(indexes, lines, computeIndex);
		AbstractTableModel model = (AbstractTableModel) JCassisTable.this.getModel();
		for (int i : indexes) {
			lines.get(i).set(compute, computeIndex);
			model.fireTableCellUpdated(i, computeIndex);
		}
		model.fireTableDataChanged();
	}

	/**
	 * Check if the given view column index match a type of data.
	 *
	 * @param viewColumnIndex The column index (view).
	 * @param ref The type of data (from {@link CassisTableMolecule}).
	 * @return true if the given view column index match the given type of data,
	 *  false otherwise.
	 */
	private boolean isColumnIndex(int viewColumnIndex, int ref) {
		int refModelIndex = ((CassisTableAbstractModel<?>)dataModel).getIndice(ref);
		int clickedModelIndex = convertColumnIndexToModel(viewColumnIndex);
		return refModelIndex == clickedModelIndex;
	}

	/**
	 * Check then return if the given index (view) is a name column.
	 *
	 * @param viewColumnIndex A view index.
	 * @return true if the given index (view) is a name column, false otherwise.
	 */
	private boolean isNameIndex(int viewColumnIndex) {
		return isColumnIndex(viewColumnIndex, CassisTableMolecule.NAME_INDICE);
	}

	/**
	 * Check then return if the given index (view) is a tag column.
	 *
	 * @param viewColumnIndex A view index.
	 * @return true if the given index (view) is a tag column, false otherwise.
	 */
	private boolean isTagIndex(int viewColumnIndex) {
		return isColumnIndex(viewColumnIndex, CassisTableMolecule.TAG_INDICE);
	}

	/**
	 * Handle a left click.
	 *
	 * @param idxRowView The row index (view).
	 * @param idxColView The column index (view).
	 */
	private void handleLeftClick(int idxRowView, int idxColView) {
		if (idxRowView < 0 || idxColView < 0) {
			return;
		}
		int computeIndex = ((CassisTableAbstractModel<?>)dataModel).getIndice(
				CassisTableMolecule.COMPUTE_INDICE);
		if (convertColumnIndexToModel(idxColView) == computeIndex) {
			return;
		}
		int indRowModel = convertRowIndexToModel(idxRowView);
		CassisTableAbstract<T> line = getLines().get(indRowModel);
		boolean compute = (boolean) line.get(computeIndex);
		line.set(!compute, computeIndex);
		((AbstractTableModel) JCassisTable.this.getModel()).fireTableCellUpdated(indRowModel, computeIndex);
	}

	/**
	 * Create then return the mouse listener.
	 *
	 * @return the mouse listener.
	 */
	public MouseAdapter getMouseListener() {
		return new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent me) {
				if (getModel().getRowCount() == 0) {
					return;
				}
				// In some case, getSelectedRow/Column return bad result with a selection listener.
				int idxRowView = rowAtPoint(me.getPoint());
				int idxColView = columnAtPoint(me.getPoint());

				if (SwingUtilities.isLeftMouseButton(me) && me.getClickCount() == 1) {
					if (me.isShiftDown()) {
						handleLeftShiftClick(idxRowView, idxColView);
					} else if (selectOnAnyColumn) {
						handleLeftClick(idxRowView, idxColView);
					}
				}
				lastSelectedIndex = idxRowView;
			}
		};
	}
}
