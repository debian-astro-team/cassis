/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.table;

import java.util.ArrayList;
import java.util.List;

import eu.omp.irap.cassis.common.MoleculeDescription;

/**
 * Class adapter on MoleculeDescription to be used in a model of a JCassisTable.
 *
 * @author glorian
 */
public class CassisTableMolecule extends CassisTableAbstract<MoleculeDescription> {

	public static final int COLLISION_INDICE = 1;
	public static final int NAME_INDICE = 2;
	public static final int COMPUTE_INDICE = 3;
	public static final int DENSITY_INDICE = 4;
	public static final int BETA_INDICE = 5;
	public static final int RELATIVE_ABUNDANCE_INDICE = 6;
	public static final int TEMPERATURE_INDICE = 7;
	public static final int VELOCITY_DISPERSION_INDICE = 8;
	public static final int SOURCE_SIZE_INDICE = 9;
	public static final int VEXP_INDICE = 10;
	public static final int DATA_SOURCE_INDICE = 11;
	public static final int TAG_INDICE = 12;
	public static final int TKIN_INDICE = 13;
	public static final int GAMMA_SELF_MEAN_INDICE = 14;
	public static final int MOLECULAR_MASS_INDICE = 15;


	public CassisTableMolecule(MoleculeDescription line, Integer... indices) {
		super(line, indices);
	}

	public static List<CassisTableMolecule> convert(List<MoleculeDescription> lines, Integer... indices) {
		List<CassisTableMolecule> tableLines = new ArrayList<>(lines.size());
		for (MoleculeDescription line : lines) {
			tableLines.add(new CassisTableMolecule(line, indices));
		}

		return tableLines;
	}

	@Override
	public Object get(int index) {
		Object res = null;
		Integer indice = convertIndice(index);
		switch (indice) {
			case COLLISION_INDICE:
				return line.getCollision();
			case NAME_INDICE:
				return line.getName();
			case COMPUTE_INDICE:
				return line.isCompute();
			case DENSITY_INDICE:
				return line.getDensity();
			case BETA_INDICE:
				return line.getBeta();
			case RELATIVE_ABUNDANCE_INDICE:
				return line.getRelativeAbundance();
			case TEMPERATURE_INDICE:
				return line.getTemperature();
			case VELOCITY_DISPERSION_INDICE:
				return line.getVelocityDispersion();
			case SOURCE_SIZE_INDICE:
				return line.getSourceSize();
			case VEXP_INDICE:
				return line.getVexp();
			case DATA_SOURCE_INDICE:
				return line.getDataSource();
			case TAG_INDICE:
				return line.getTag();
			case TKIN_INDICE:
				return line.getTKin();
			case GAMMA_SELF_MEAN_INDICE:
				return line.getGammaSelfMean();
			case MOLECULAR_MASS_INDICE:
				return line.getMolecularMass();
		}

		return res;
	}

	@Override
	public void set(Object value, int col) {
		Integer indice = convertIndice(col);
		switch (indice) {
			case COLLISION_INDICE:
				line.setCollision((String) value);
				break;
			case NAME_INDICE:
				line.setName((String) value);
				break;
			case COMPUTE_INDICE:
				line.setCompute((Boolean) value);
				break;
			case DENSITY_INDICE:
				line.setDensity(
						CassisTableMolecule.getDoubleFromObject(value));
				break;
			case BETA_INDICE:
				line.setBeta((Double) value);
				break;
			case RELATIVE_ABUNDANCE_INDICE:
				line.setRelativeAbundance(
						CassisTableMolecule.getDoubleFromObject(value));
				break;
			case TEMPERATURE_INDICE:
				line.setTemperature((Double) value);
				break;
			case VELOCITY_DISPERSION_INDICE:
				line.setVelocityDispersion((Double) value);
				break;
			case SOURCE_SIZE_INDICE:
				line.setSourceSize((Double) value);
				break;
			case VEXP_INDICE:
				line.setVexp((Double) value);
				break;
			case DATA_SOURCE_INDICE:
				line.setDataSource((String) value);
				break;
			case TAG_INDICE:
				line.setTag((Integer) value);
				break;
			case TKIN_INDICE:
				line.setTKin((Double) value);
				break;
			case GAMMA_SELF_MEAN_INDICE:
				line.setGammaSelfMean((Double) value);
				break;
			case MOLECULAR_MASS_INDICE:
				line.setMolecularMass((Double) value);
				break;
		}
	}

	@Override
	public int getComputeIndex() {
		return COMPUTE_INDICE;
	}

	private static double getDoubleFromObject(Object ob) {
		if (ob instanceof String) {
			return Double.parseDouble((String)ob);
		} else {
			return ((Double)ob).doubleValue();
		}
	}
}
