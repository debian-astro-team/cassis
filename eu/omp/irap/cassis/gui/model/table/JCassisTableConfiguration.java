/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.table;


import java.awt.Color;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.properties.Software;


/**
 * Configuration for the {@link JCassisTable}.
 *
 * @author M. Boiziot
 */
public class JCassisTableConfiguration {

	private static final Logger LOGGER =
			LoggerFactory.getLogger(JCassisTableConfiguration.class);
	public static final Color DEFAULT_COLOR = Color.BLACK;
	private static final String FILE_PROPERTIES = "sourceSpecies.properties";
	private static final Color DEFAULT_COLOR_VASTEL = Color.BLUE;
	private static final Color DEFAULT_COLOR_NIST = Color.BLACK;
	private static final Color DEFAULT_COLOR_CDMS = Color.RED;
	private static final Color DEFAULT_COLOR_JPL = Color.GREEN.darker().darker();
	private static final Color DEFAULT_COLOR_IRAP = Color.ORANGE.darker();
	private static final Color DEFAULT_COLOR_FORCOM = Color.MAGENTA;
	private static final Color DEFAULT_COLOR_HFS = Color.CYAN.darker().darker();


	private static JCassisTableConfiguration instance;
	private Map<String, Color> map;


	/**
	 * Constructor.
	 */
	private JCassisTableConfiguration() {
		map = new Hashtable<>();
		readConfig();
	}

	/**
	 * Create if needed then return the instance of
	 * {@link JCassisTableConfiguration}.
	 *
	 * @return the instance of {@link JCassisTableConfiguration}.
	 */
	public static JCassisTableConfiguration getInstance() {
		if (instance == null) {
			instance = new JCassisTableConfiguration();
		}
		return instance;
	}

	/**
	 * Return the color for a source if one is definited for it
	 * or the default color.
	 *
	 * @param source The source for which we want the color.
	 * @return The color for the provided source.
	 */
	public Color getColorFor(String source) {
		Color color = DEFAULT_COLOR;
		if (source != null && map.containsKey(source)) {
			color = map.get(source);
		}
		return color;
	}

	/**
	 * Read the config file and initialize the properties.
	 */
	private void readConfig() {
		Properties prop = Software.getModePath().getProperties(FILE_PROPERTIES);
		if (prop != null) {
			Set<Object> keys = prop.keySet();
			for (Object oKey : keys) {
				String key = (String) oKey;
				try {
					int rgb = Integer.parseInt((String) prop.get(oKey));
					Color color = new Color(rgb);
					map.put(key, color);
				} catch (NumberFormatException nfe) {
					LOGGER.warn("Color value for {} is invalid.", key, nfe);
				}
			}
		}
		initDefaultValues();
	}

	/**
	 * Init default values for VASTEL, NIST, CDMS, JPL, IRAP, FORCOM and HRS
	 *  if not present.
	 */
	private void initDefaultValues() {
		if (!map.containsKey("VASTEL")) {
			map.put("VASTEL", DEFAULT_COLOR_VASTEL);
		}
		if (!map.containsKey("NIST")) {
			map.put("NIST", DEFAULT_COLOR_NIST);
		}
		if (!map.containsKey("CDMS")) {
			map.put("CDMS", DEFAULT_COLOR_CDMS);
		}
		if (!map.containsKey("JPL")) {
			map.put("JPL", DEFAULT_COLOR_JPL);
		}
		if (!map.containsKey("IRAP")) {
			map.put("IRAP", DEFAULT_COLOR_IRAP);
		}
		if (!map.containsKey("FORCOM")) {
			map.put("FORCOM", DEFAULT_COLOR_FORCOM);
		}
		if (!map.containsKey("HFS")) {
			map.put("HFS", DEFAULT_COLOR_HFS);
		}
	}

	/**
	 * Save the current parameters in the config file.
	 */
	public void saveConfig() {
		Properties prop = new Properties();
		Set<String> keys = map.keySet();
		for (String key : keys) {
			prop.put(key, String.valueOf(map.get(key).getRGB()));
		}

		try (FileWriter writer = new FileWriter(
				Software.getPropertiesPath() + File.separator + FILE_PROPERTIES)) {
			prop.store(writer, "This is the JTable rgb color for the source.");
		} catch (IOException e) {
			LOGGER.error("Error while saving the table configuration", e);
		}
	}

	/**
	 * Add a color for a source.
	 *
	 * @param source The name of the source.
	 * @param rgbColorValue The rgb color value associated with the source.
	 */
	public void addElement(String source, int rgbColorValue) {
		map.put(source, new Color(rgbColorValue));
	}

	/**
	 * Return the configured sources.
	 *
	 * @return the configured sources
	 */
	public Set<String> getKeys() {
		return map.keySet();
	}
}
