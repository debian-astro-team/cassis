/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.table;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

public abstract class CassisTableAbstractModel<T> extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	private List<CassisTableAbstract<T>> sourceList;
	private final String[] columnNames;
	private final Integer[] indices;
	protected Boolean[] editable;
	protected CassisValidator cassisValidator;


	public CassisTableAbstractModel(List<T> sourceList, final String[] columnNames, final Integer[] indices) {
		if (sourceList != null)
			this.sourceList = convert(sourceList, indices);
		else
			this.sourceList = new ArrayList<>();
		this.columnNames = columnNames;
		this.indices = indices;
	}

	public CassisTableAbstractModel(List<T> sourceList, final String[] columnNames, final Integer[] indices,
			Boolean[] editable) {
		super();
		this.sourceList = convert(sourceList, indices);
		this.columnNames = columnNames;
		this.editable = editable;
		this.indices = indices;
	}

	public abstract List<CassisTableAbstract<T>> convert(List<T> lines, Integer... indices);

	public abstract CassisTableAbstract<T> convert(T lines, Integer... indices);

	public int getIndice(Integer cassisTableMoleculeIndice) {
		for (int cpt = 0; cpt < indices.length; cpt++) {
			if (indices[cpt].equals(cassisTableMoleculeIndice))
				return cpt;
		}
		return -1;
	}

	public void setValidator(CassisValidator validator) {
		this.cassisValidator = validator;
	}

	@Override
	public String getColumnName(int column) {
		return columnNames[column];
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		return sourceList.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if (sourceList.isEmpty())
			return null;
		return sourceList.get(rowIndex).get(columnIndex);
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return editable[columnIndex];
	}

	@Override
	public Class<?> getColumnClass(int c) {
		Object object = getValueAt(0, c);
		if (object != null) {
			return getValueAt(0, c).getClass();
		} else {
			return String.class;
		}
	}

	@Override
	public void setValueAt(Object value, int row, int col) {
		if (cassisValidator != null) {
			cassisValidator.isValueValid(value, row, col);
		}
		sourceList.get(row).set(value, col);
		fireTableCellUpdated(row, col);
	}

	/**
	 * @return the editable
	 */
	public final Boolean[] getEditable() {
		return editable;
	}

	/**
	 * @return the sourceList
	 */
	public final List<CassisTableAbstract<T>> getSourceList() {
		return sourceList;
	}

	public void setList(List<T> sourceList) {
		this.sourceList = convert(sourceList, indices);
		if (!sourceList.isEmpty()) {
			this.fireTableDataChanged();
		}
	}

	public void add(T element) {
		sourceList.add(convert(element, indices));
		fireTableDataChanged();
	}

}
