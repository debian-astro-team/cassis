/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.lteradex;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.CellEditor;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.cassisd.Server;
import eu.omp.irap.cassis.cassisd.ServerImpl;
import eu.omp.irap.cassis.cassisd.model.synthetic.RadexModel;
import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.ComponentAlgo;
import eu.omp.irap.cassis.common.ISpectrumComputation;
import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.common.ProgressDialogConstants;
import eu.omp.irap.cassis.common.RadexException;
import eu.omp.irap.cassis.common.Telescope;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.database.access.AccessDataBase;
import eu.omp.irap.cassis.database.access.DataBaseConnection;
import eu.omp.irap.cassis.database.access.PartitionFunction;
import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB;
import eu.omp.irap.cassis.database.access.SqlPartitionMole;
import eu.omp.irap.cassis.database.access.UnknowMoleculeException;
import eu.omp.irap.cassis.gui.model.CassisModel;
import eu.omp.irap.cassis.gui.model.parameter.ParametersModel;
import eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent.ComponentDescription;
import eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent.ComponentSpecies;
import eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent.ComponentSpeciesPanel;
import eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent.EmAbTabbedPanelModel;
import eu.omp.irap.cassis.gui.model.parameter.telescope.TelescopeModel;
import eu.omp.irap.cassis.gui.model.table.CassisTableDisplayEvent;
import eu.omp.irap.cassis.gui.model.table.CassisTableDisplayListenerList;
import eu.omp.irap.cassis.gui.optionpane.ProgressDialog;
import eu.omp.irap.cassis.gui.util.Deletable;
import eu.omp.irap.cassis.gui.util.FileUtils;
import eu.omp.irap.cassis.properties.Software;
import radex.PartnerModel;

public class LteRadexControl implements ModelListener, ISpectrumComputation, ActionListener, Deletable {

	private static final Logger LOGGER = LoggerFactory.getLogger(LteRadexControl.class);
	protected LteRadexModel model;
	protected LteRadexPanel view;
	protected CassisTableDisplayListenerList listeners = new CassisTableDisplayListenerList();
	private CommentedSpectrum result;


	public LteRadexControl(LteRadexModel model, LteRadexPanel lteRadexPanel) {
		this.model = model;
		view = lteRadexPanel;
		model.addModelListener(this);
		model.getParametersModel().addModelListener(this);
		model.getModelTuningModel().getTelescopeModel().addModelListener(this);
	}

	public LteRadexModel getModel() {
		return model;
	}

	/**
	 * @return the listeners
	 */
	public CassisTableDisplayListenerList getListeners() {
		return listeners;
	}

	/**
	 * @return the view
	 */
	public LteRadexPanel getView() {
		return view;
	}

	public void setView(LteRadexPanel view) {
		this.view = view;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == view.getButtonsPanel().getLoadConfigButton()) {
			onLoadConfigButton();
		}
		else if (e.getSource() == view.getButtonsPanel().getDisplayButton()) {
			onDisplayButton();
		}
		else if (e.getSource() == view.getButtonsPanel().getSaveConfigButton()) {
			onSaveConfigButton();
		}
	}

	/**
	 * On save config button clicked handle.
	 */
	private void onSaveConfigButton() {
		File configFile = null;

		JFileChooser fc = new CassisJFileChooser(Software.getLteRadexConfigPath(),
				Software.getLastFolder("lte_radex-model-config"));
		fc.setDialogType(JFileChooser.SAVE_DIALOG);
		final FileNameExtensionFilter filter = new FileNameExtensionFilter("LteRadex Module (*.ltm)","ltm");
		fc.addChoosableFileFilter(filter);
		fc.setFileFilter(filter);
		if (fc.showSaveDialog(view) == JFileChooser.APPROVE_OPTION) {
			try {
				File file = fc.getSelectedFile();
				String name = file.getAbsolutePath();
				if (!"ltm".equals(FileUtils.getFileExtension(name)))
					name += ".ltm";
				configFile = new File(name);
				// If template name already exists
				if (configFile.exists()) {
					String message = "Configuration " + configFile.getName() + " already exists.\n"
							+ "Do you want to replace it?";
					// Modal dialog with yes/no button
					int answer = JOptionPane.showConfirmDialog(view, message, "Replace existing configuration file?",
							JOptionPane.YES_NO_OPTION);
					if (answer == JOptionPane.NO_OPTION) {
						configFile = null;
					}
				}

				if (configFile != null) {
					model.saveConfig(configFile);
				}
				Software.setLastFolder("lte_radex-model-config", file.getParent());
			} catch (Exception e) {
				LOGGER.error("Unable to save configuration.", e);
			}
		}
	}

	/**
	 * On display button clicked handle.
	 */
	public void onDisplayButton() {
		if (checkParameters()) {
			computeLteRadexSpectrum();
		}
	}

	/**
	 * Compute the spectrum with the LteRadex Model.
	 */
	public void computeLteRadexSpectrum() {
		try {
			File file = new File(Software.getConfigPath() +
					File.separator + "lte_radex-model"+ File.separator + "last.ltm");
			if (!file.getParentFile().exists()) {
				file.getParentFile().mkdirs();
			}
			model.saveConfig(file);
		} catch (Exception e) {
		}

		if (Software.getUserConfiguration().isTestMode()) {
			invokeSpectrumComputation();
			display();
		}
		else {
			new ProgressDialog(view, this);
		}
	}

	public static boolean checkTelescope(Component view, Telescope telescope, String selectedTelescope) {
		boolean res = telescope.getFileName().equals(selectedTelescope) || Telescope.isPathOfTelescope(telescope.getFileName()) != null ;
		if (! res){
			JOptionPane.showMessageDialog(view, "There is a problem when loadind selected telescope.\n"
				+ "Please reselect it or choose another", "Error",
				JOptionPane.ERROR_MESSAGE);
		}
		return res;
	}

	@Override
	public void invokeSpectrumComputation() {
		Server server = new ServerImpl();
		result = null;
		boolean umeException = false;
		boolean radexException = false;
		try {
			result = server.computeLteRadexSpectrum(model.getMapParameter(),
					model.getEmAbTabbedPanelModel().getComponentsWithMoleculeSelected(),
					model.getParametersModel().getTelescope());
		} catch (UnknowMoleculeException ume) {
			umeException = true;
			LOGGER.error("A molecule (tag: {}) is unknown, stopping the operation", ume.getTag(), ume);
			JOptionPane.showMessageDialog(view, ume.getInterruptedMessage(),
					"Unknow molecule", JOptionPane.ERROR_MESSAGE);
		} catch (RadexException re) {
			radexException = true;
			LOGGER.error("A RadexException occured while invoking the spectrum computation", re);
			JOptionPane.showMessageDialog(view, re.getMessage(),
					"RADEX error", JOptionPane.ERROR_MESSAGE);
		} catch (Exception e) {
			LOGGER.error("Error while invoking the spectrum computation", e);
			if (e.getMessage() == null) {
				JOptionPane.showMessageDialog(view, "No line found with this threshold", "WARNING",
						JOptionPane.WARNING_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(view, e.getMessage(), "Alert", JOptionPane.ERROR_MESSAGE);
			}
		}

		if (!umeException && !radexException && result == null) {
			JOptionPane.showMessageDialog(view,
					"No lines found within the given thresholds.\n" +
					"Most likely, your Eup max is too low and/or your Aij min is too high.",
					"Warning", JOptionPane.WARNING_MESSAGE);
		}
	}

	public CommentedSpectrum getResult() {
		return result;
	}

	public void setResult(CommentedSpectrum  spectrum) {
		result = spectrum;
	}

	/**
	 * Check parameters.
	 *
	 * @return true if the parameters are OK, false otherwise.
	 */
	private boolean checkParameters() {
		stopEditingComponent(view.getEmAbTabbedPanelView().getTabbedPane().getSelectedComponent());
		return checkNbPoints(this.view, model.getModelTuningModel().getNbPoints()) &&
			   checkParametersForRadex(this.view, model.getEmAbTabbedPanelModel()) &&
			   checkParametersForLte(this.view, model.getEmAbTabbedPanelModel()) &&
			   checkComponentsParameters(this.view, model.getEmAbTabbedPanelModel().getComponentsWithMoleculeSelected()) &&
			   checkTelescope(this.view, model.getParametersModel().getTelescope(),model.getParametersModel().getSelectedTelescope());
	}

	public static boolean checkParametersForLte(Component view, EmAbTabbedPanelModel emAbTabbedPanelModel) {
		boolean res = true;
		StringBuilder stringBuilder = new StringBuilder();
		List<ComponentSpecies> compoSpecies =
				emAbTabbedPanelModel.getEnabledComponentFromType(ComponentAlgo.FULL_LTE_ALGO);
		for (int index = 0; index < compoSpecies.size(); index++) {
			final ComponentSpecies compo = compoSpecies.get(index);
			List<MoleculeDescription> molsSelected = compo.getSelectedMoleculeDescription();
			for (int i = 0; res && i < molsSelected.size(); i++) {
				MoleculeDescription mol = molsSelected.get(i);
				DataBaseConnection db = AccessDataBase.getDataBaseConnection();

				try {
					PartitionFunction partitionFunction;
					partitionFunction = db.getPartitionFunction(mol.getTag(), true);
					if (! partitionFunction.isInRange(mol.getTemperature())) {
						res = false;
						String origin = partitionFunction.getOrigin();
						if (origin == null) {
							stringBuilder.append(
									"Error: no partition function detected for the species"+ mol.getName() + "("+mol.getTag()+ ")\n"
											+ "CASSIS has created the file \n"
											+ SqlPartitionMole.getPartMolePath() + "/" + mol.getTag() + ".txt.\n"
											+ "Please modify this file respecting the format of the files present in this directory.");

						}
						else if (origin.equals("database")){
							stringBuilder.append("Error: the selected excitation temperature is not in "
									+ "\nthe range provided by the selected database for the species "+ mol.getName()
									+ " (" + partitionFunction.getMinValue() + "-" + partitionFunction.getMaxValue() + ") " + "K. \n"
									+ "CASSIS has created the file \n"
									+ SqlPartitionMole.getPartMolePath() + "/" + mol.getTag() + ".txt."
									+ "Please modify this file respecting the format of the files present in this directory.");
//									+ "Please add a text file defining the Partition Function you want to use in the directory \n"
//									+ SqlPartitionMole.getPartMolePath()
//									+ ", respecting the format of the files present in this directory.");
							partitionFunction.createFile(db.getMoleculeDescriptionDB(new SimpleMoleculeDescriptionDB(mol.getTag(), mol.getName()) ));


						} else {
							stringBuilder.append("Error: the selected excitation temperature is not in "
									+ "the range provided by the file for the species "+ mol.getName() + "\n"
									+ SqlPartitionMole.getPartMolePath() + "/" + mol.getTag() + ".txt. "
									+ "(" + partitionFunction.getMinValue() + "-" + partitionFunction.getMaxValue() +") K \n"
									+ "Please modify this file respecting the format of the files present in this directory.");
						}
					}
				} catch (UnknowMoleculeException e) {
					res = false;
					e.printStackTrace();
					stringBuilder.append("Warning: Species " + mol.getTag() + "is unknow");
				}

			}
		}
		if (!res) {
			JOptionPane.showMessageDialog(view,
					stringBuilder.toString(),
					"Lte parameter error", JOptionPane.ERROR_MESSAGE);
		}


		return res;
	}

	public static boolean checkParametersForRadex(Component view, EmAbTabbedPanelModel emAbTabbedPanelModel) {
		boolean res = true;
		if (!emAbTabbedPanelModel.isDensityOkForRadex()) {
			JOptionPane.showMessageDialog(view,
					"Radex input is invalid. The density must be between 1e5 and 1e25.",
					"RADEX parameter error", JOptionPane.ERROR_MESSAGE);
			res = false;
		}

		if (res) {
			List<ComponentSpecies> compoSpecies =
					emAbTabbedPanelModel.getEnabledComponentFromType(ComponentAlgo.FULL_RADEX_ALGO);
			res = checkCollisionFileAndRangeTkinForRadexCompo(view, compoSpecies);
		}
		return res;
	}

	public static boolean checkNbPoints(Component view, int points) {
		boolean res = true;
		if (points > 100000) {
			int choice = JOptionPane.showConfirmDialog(view,
					"Caution, calculated spectra will contains more than 100000 points.\n" +
					"Do you yant to continue?", "Warning: large number of points",
					JOptionPane.YES_NO_OPTION);
			if (choice == JOptionPane.NO_OPTION) {
				res = false;
			}
		} else if (points < 3) {
			JOptionPane.showMessageDialog(view, "The dv value is too large.",
					"Parameter error", JOptionPane.ERROR_MESSAGE);
			res = false;
		}
		return res;
	}

	public static void stopEditingComponent(final Component selectedComponent) {

		if (selectedComponent instanceof ComponentSpeciesPanel) {
			CellEditor c = ((ComponentSpeciesPanel) selectedComponent).getMoleculesTable().getCellEditor();
			if (c != null)
				c.stopCellEditing();
		}
	}

	/**
	 * @param view
	 * @param components
	 * @return
	 */
	public static boolean checkComponentsParameters(final Component view, final List<ComponentDescription> components) {
		boolean res = true;

		List<ComponentSpecies> compoSpecies= getComponentSpecies(components);
		if (compoSpecies.isEmpty()) {
			JOptionPane.showMessageDialog(view,
					"You must have at least one component with species",
					"Component parameter error", JOptionPane.ERROR_MESSAGE);
			res = false;
		}
		if (res && !isCompoInteractingAfter(compoSpecies)) {
			JOptionPane.showMessageDialog(view,
					"Interacting component must be after non interacting compoenent",
					"Component parameter error", JOptionPane.ERROR_MESSAGE);
			res =  false;
		}
		if (res) {
			int numNoMolecule = haveOneMoleculeSelected(compoSpecies);
			if (numNoMolecule != -1) {
				JOptionPane.showMessageDialog(view,
						"You must select one molecule for component " + (numNoMolecule +1),
						"Component parameter error", JOptionPane.ERROR_MESSAGE);
				res = false;
			}
		}

		if (res) {
			List<ComponentSpecies> compos = getInteractingSpeciesComponent(components);
			if (!compos.isEmpty()) {
				int numBadCompo = haveOneTheta(compos);
				if (numBadCompo != -1) {
					JOptionPane.showMessageDialog(view,
							"All molecules of any interacting component must have the same size",
							"Component parameter error", JOptionPane.ERROR_MESSAGE);
					res = false;
				}
			}
		}

		return res;
	}

	private static boolean checkCollisionFileAndRangeTkinForRadexCompo(Component view, List<ComponentSpecies> components) {
		boolean res = true;
		for (int index = 0; index < components.size(); index++) {
			final ComponentSpecies compo = components.get(index);
			if (compo.isFullRadexMode()) {
				List<MoleculeDescription> molsSelected = compo.getSelectedMoleculeDescription();
				for (int i = 0; res && i < molsSelected.size(); i++) {
					MoleculeDescription mol = molsSelected.get(i);
					String collision = RadexModel.getMoleFileFromCollision(mol.getCollision());
					File file = Software.getRadexMoleculesManager().getPathCollisionFile(collision);
					if (file == null ||! file.exists()) {
						res= false;
						JOptionPane.showMessageDialog(view,
								"Collision file " + collision + " not found.\n"
								+ "Please check if the file has not be moved or deleted.",
								"Collision file error", JOptionPane.ERROR_MESSAGE);

					} else {
						List<PartnerModel> partnersdb = Software.getRadexMoleculesManager().getPartners(collision);
						List<PartnerModel> partnersChoose = RadexModel.getPartnersFromCollision(mol.getCollision());
						double tKin = mol.getTKin();
						for (int j = 0; res && j < partnersChoose.size(); j++) {
							PartnerModel partnerChoose = partnersChoose.get(j);
							for (int k = 0; res && k < partnersdb.size(); k++) {
								PartnerModel partner = partnersdb.get(k);
								if (partnerChoose.getTypePartnerId() == partner.getTypePartnerId()) {
									double[] kineticTemperatures = partner.getKineticTemperatures();
									if (tKin < kineticTemperatures[0] ||
										tKin > kineticTemperatures[kineticTemperatures.length-1] ) {
										JOptionPane.showMessageDialog(view,
												"Warning: the selected kinetic temperature ("
												+ Double.toString(tKin)
												+ ") for collider "
												+ partnerChoose.getTypePartner()
												+ " is outside the range provided in the "
												+ collision
												+ " collision file. Please modify T$_k$.",
												"Species parameter error", JOptionPane.WARNING_MESSAGE);
										res= true;
									}
								}
							}
						}
					}


				}
			}
		}

		return res;
	}

	private static boolean isCompoInteractingAfter(List<ComponentSpecies> compoSpecies) {
		boolean res = true;
		boolean findInteracting = false;
		for (int i = 0; res && i < compoSpecies.size(); i++) {
			boolean interacting = compoSpecies.get(i).isInteracting();
			if (findInteracting && !interacting) {
				res = false;
			}
			if (res && ! interacting) {
				findInteracting = true;
			}
		}
		return res;
	}

	private static List<ComponentSpecies> getComponentSpecies(List<ComponentDescription> components) {
		List<ComponentSpecies> compo = new ArrayList<>();

		for (ComponentDescription component : components) {
			if (!component.isContinuum()) {
				compo.add((ComponentSpecies) component);
			}
		}
		return compo;
	}

	/**
	 * get the interacting species component
	 * @param components
	 * @return
	 */
	public static List<ComponentSpecies> getInteractingSpeciesComponent(
			List<ComponentDescription> components) {
		int cpt = 0;
		List<ComponentSpecies> compo = new ArrayList<>();

		for (cpt = 0; cpt < components.size(); cpt++) {
			ComponentDescription componentDescription = components.get(cpt);
			if (componentDescription.isInteracting()) {
				break;
			}
		}

		if (cpt != components.size()) {
			for (int cpt2 = cpt; cpt2 < components.size(); cpt2++) {
				ComponentDescription componentDescription = components.get(cpt2);
				compo.add((ComponentSpecies)componentDescription);
			}
		}
		return compo;
	}

	private static int haveOneMoleculeSelected(List<ComponentSpecies> compos) {
		int res = -1;
		for (int cpt = 0; res==-1 && cpt < compos.size(); cpt++) {
			ComponentSpecies compoSpecies = compos.get(cpt);
			List<MoleculeDescription> molecules = compoSpecies.getMoleculeList();
			if (molecules.isEmpty()) {
				res = cpt;
			}
		}

		return res;
	}


	private static int haveOneTheta(List<ComponentSpecies> compos) {
		int res = -1;
		for (int cpt = 0; res==-1 && cpt < compos.size(); cpt++) {
			ComponentSpecies compoSpecies = compos.get(cpt);
			List<MoleculeDescription> molecules = compoSpecies.getMoleculeList();
			double firstThetaMol = molecules.get(0).getSourceSize();
			for (int i = 0; res==-1 && i < molecules.size(); i++) {
				MoleculeDescription mol = molecules.get(i);
				if (firstThetaMol != mol.getSourceSize()) {
					res = cpt;
				}
			}
		}

		return res;
	}

	public static boolean isThetaInteraCompoIncreasing(List<ComponentSpecies> compos) {
		boolean res = true;
		double thetaCompMin = Double.NEGATIVE_INFINITY;

		for (int cpt = 0; res && cpt < compos.size(); cpt++) {
			ComponentSpecies compoSpecies = compos.get(cpt);
			List<MoleculeDescription> molecules = compoSpecies.getMoleculeList();
			double firstThetaMol = molecules.get(0).getSourceSize();
			if (thetaCompMin> firstThetaMol) {
				res = false;
			} else {
				thetaCompMin = firstThetaMol;
			}
		}

		return res;
	}

	/**
	 * Load config file.
	 */
	private void onLoadConfigButton() {
		JFileChooser fc = new CassisJFileChooser(Software.getLteRadexConfigPath(),
				Software.getLastFolder("lte_radex-model-config"));

		fc.setFileFilter(new FileNameExtensionFilter("LteRadex Module (*.ltm)","ltm"));
		if (fc.showOpenDialog(view) == JFileChooser.APPROVE_OPTION) {
			File selectedFile = fc.getSelectedFile();
			// Read file
			if (selectedFile != null) {
				loadConfig(selectedFile);
				Software.setLastFolder("lte_radex-model-config", selectedFile.getParent());
			}
		}
	}

	public void loadConfig(File selectedFile) {
		try {
			model.loadConfig(selectedFile.getPath());
		} catch (IOException exc) {
			LOGGER.error("Unable to read config file", exc);
			JOptionPane.showMessageDialog(view, "Unable to read config file. ", "Alert", JOptionPane.ERROR_MESSAGE);
		}
	}

	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (ParametersModel.TELESCOPE_EVENT.equals(event.getSource())) {
			String telescope = (String) event.getValue();
			model.getModelTuningModel().getTelescopeModel().setSelectedTelescope(telescope);
			refreshNoiseUnit(telescope);
		}
		else if (LteRadexModel.TELESCOPE_ERROR_EVENT.equals(event.getSource())) {
			view.displayTelescopeError(event.getValue().toString());
		}
		else if (TelescopeModel.SELECTED_TELESCOPE_EVENT.equals(event.getSource())) {
			String selectedTelescope = (String) event.getValue();
			if (!model.getParametersModel().getSelectedTelescope().equals(selectedTelescope)) {
				model.getParametersModel().setSelectedTelescope(selectedTelescope);
				refreshNoiseUnit(selectedTelescope);
			}
		}
	}

	/**
	 * Refresh the noise unit according to the telescope.
	 *
	 * @param telescope The telescope.
	 */
	public void refreshNoiseUnit(String telescope) {
		if (telescope != null &&
				Telescope.getNameStatic(telescope).startsWith("spire")) {
			model.getNoiseModel().setUnit(UNIT.M_JANSKY);
		} else {
			model.getNoiseModel().setUnit(UNIT.M_KELVIN);
		}
	}

	@Override
	public void display() {
		if (ProgressDialogConstants.workerInterrupted) {
			JOptionPane.showMessageDialog(view, "The spectrum computation was interrupted", "Information",
					JOptionPane.INFORMATION_MESSAGE);
			return;
		}

		if (result == null) {
			return;
		} else if (result.getListOfLines().isEmpty()) {
			JOptionPane.showMessageDialog(view, "No lines found within the given thresholds.\n"+
					"Most likely, your Eup max is too low and/or you Aij min is too high","Warning",
					JOptionPane.WARNING_MESSAGE);
		} else {
			List<LineDescription> negTexLines = getNegativeTexLine(result.getListOfLines());
			if (!negTexLines.isEmpty()) {
				displayNegativeTexLinesPopup(negTexLines);
			}
			CassisModel cassisModel = new CassisModel(result, model);
			listeners.fireCassisTableDisplayListener(new CassisTableDisplayEvent(this, cassisModel, model, model
					.getName()));
		}
	}

	/**
	 * Generate a message and display it as a dialog.
	 *
	 * @param negTexLines The list of lines with negative Tex value.
	 */
	private void displayNegativeTexLinesPopup(List<LineDescription> negTexLines) {
		StringBuilder sb = new StringBuilder();
		if (negTexLines.size() == 1) {
			sb.append("One line has a negative Tex value:\n");
		} else { // More than 1
			sb.append("Some lines have a negative Tex value:\n");
		}
		for (LineDescription line : negTexLines) {
			sb.append("\t- ");
			sb.append(line.getMolName());
			sb.append(" at frequency ");
			sb.append(line.getObsFrequency());
			sb.append(" MHz.");
			sb.append("Tex value is ");
			sb.append(line.getTex());
			sb.append(" K\n");
		}
		JOptionPane.showMessageDialog(view, sb.toString(), "Negative Tex",
				JOptionPane.INFORMATION_MESSAGE);
	}

	/**
	 * Return the lines with a negative Tex value from a list of line.
	 *
	 * @param lines The list of lines to check.
	 * @return The lines with a negative Tex value.
	 */
	private List<LineDescription> getNegativeTexLine(List<LineDescription> lines) {
		List<LineDescription> negTexLines = new ArrayList<>();
		for (LineDescription line : lines) {
			if (line.getTex() < 0) {
				negTexLines.add(line);
			}
		}
		return negTexLines;
	}

	/**
	 * Set a new model.
	 *
	 * @param newLteRadexModel The new LteRadexModel to use.
	 */
	public void setModel(LteRadexModel newLteRadexModel) {
		// Remove old listeners.
		removeListeners();
		// Set new model
		model = newLteRadexModel;
		// Add news listeners.
		model.addModelListener(this);
		model.getParametersModel().addModelListener(this);
		model.getModelTuningModel().getTelescopeModel().addModelListener(this);

		view.refresh();
	}

	public void loadConfigButtonClicked(MouseEvent e) {
		final String pathname = Software.getConfigPath() +
				 File.separator + "lte_radex-model" + File.separator + "last.ltm";

		if (SwingUtilities.isRightMouseButton(e) &&
			Software.getUserConfiguration().isTestMode() &&
			new File(pathname).exists()) {
			try {
				getModel().loadConfig(pathname);
			} catch (IOException ioe) {
				LOGGER.error("Unable to read config file", ioe);
			}
		}
	}

	/**
	 * Remove the listeners.
	 */
	private void removeListeners() {
		model.removeModelListener(this);
		model.getParametersModel().removeModelListener(this);
		model.getModelTuningModel().getTelescopeModel().removeModelListener(this);
	}

	/**
	 * @see eu.omp.irap.cassis.gui.util.Deletable#delete()
	 */
	@Override
	public void delete() {
		removeListeners();
		view.getEmAbTabbedPanelView().getControl().delete();
	}
}
