/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.lteradex;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import eu.omp.irap.cassis.gui.model.ButtonsPanel;
import eu.omp.irap.cassis.gui.model.CustomTabbedPaneUI;
import eu.omp.irap.cassis.gui.model.parameter.ParametersPanel;
import eu.omp.irap.cassis.gui.model.parameter.frequency.FrequencyPanel;
import eu.omp.irap.cassis.gui.model.parameter.imin.TbgPanel;
import eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent.EmAbTabbedPanelView;
import eu.omp.irap.cassis.gui.model.parameter.modeltuning.ModelTuningPanel;
import eu.omp.irap.cassis.gui.model.parameter.noise.NoisePanel;
import eu.omp.irap.cassis.gui.model.parameter.observing.ObservingPanel;
import eu.omp.irap.cassis.gui.model.parameter.threshold.ThresholdPanel;
import eu.omp.irap.cassis.properties.Software;

@SuppressWarnings("serial")
public class LteRadexPanel extends JPanel {

	private LteRadexControl control;
	private ModelTuningPanel modelTuningPanel;
	private ThresholdPanel thresholdPanel;
	private ParametersPanel parametersPanel;
	private NoisePanel noisePanel;
	private EmAbTabbedPanelView emAbTabbedPanelView;
	private TbgPanel tbgPanel;
	private FrequencyPanel freqPanel;
	private ButtonsPanel buttonsPanel;
	private JPanel modelPanel;
	private ObservingPanel observingPanel;


	/**
	 * Constructor makes a new LteRadexTableView.
	 */
	public LteRadexPanel() {
		this(new LteRadexModel());
	}

	public LteRadexPanel(LteRadexModel lteRadexModel) {
		control = new LteRadexControl(lteRadexModel, this);
		initComponent();
		setSize(1016, 600);
		setPreferredSize(new Dimension(1016, 600));
	}

	private void initComponent() {
		setLayout(new BorderLayout());
		JPanel topCenterPanel = new JPanel(new GridLayout(2, 1));
		topCenterPanel.add(getModelTuningPanel());
		topCenterPanel.add(getThresholdPanel());

		JPanel topPanel = new JPanel(new BorderLayout());
		topPanel.add(getButtonsPanel(), BorderLayout.EAST);
		topPanel.add(topCenterPanel, BorderLayout.CENTER);

		this.add(topPanel, BorderLayout.NORTH);
		this.add(getModelPanel(), BorderLayout.CENTER);
	}

	public ModelTuningPanel getModelTuningPanel() {
		if (modelTuningPanel == null) {
			modelTuningPanel = new ModelTuningPanel(control.getModel().getModelTuningModel());
			modelTuningPanel.setTypeName("lte_radex-model");
		}
		return modelTuningPanel;
	}

	public ThresholdPanel getThresholdPanel() {
		if (thresholdPanel == null) {
			thresholdPanel = new ThresholdPanel(control.getModel().getThresholdModel());
		}

		return thresholdPanel;
	}

	public ButtonsPanel getButtonsPanel() {
		if (buttonsPanel == null) {
			buttonsPanel = new ButtonsPanel();
			buttonsPanel.getLoadConfigButton().addMouseListener(new MouseAdapter() {

				@Override
				public void mouseClicked(MouseEvent e) {
					control.loadConfigButtonClicked(e);
				}
			});
			buttonsPanel.getLoadConfigButton().addActionListener(control);
			buttonsPanel.getSaveConfigButton().addActionListener(control);
			buttonsPanel.getDisplayButton().addActionListener(control);
		}
		return buttonsPanel;
	}

	public ParametersPanel getParametersPanel() {
		if (parametersPanel == null) {
			parametersPanel = new ParametersPanel(control.getModel().getParametersModel());
			parametersPanel.setPreferredSize(new Dimension(450, 60));
		}
		return parametersPanel;
	}

	private JPanel getModelPanel() {
		if (modelPanel == null) {
			modelPanel = new JPanel(new BorderLayout());
			JTabbedPane tabSourceManager = new JTabbedPane();
			tabSourceManager.setUI(new CustomTabbedPaneUI(this.getBackground()));
			JPanel tabPanel = new JPanel(new BorderLayout());
			JPanel centerTopPanel = new JPanel(new BorderLayout());

			observingPanel = new ObservingPanel(control.getModel().getObservingModel());
			observingPanel.setPreferredSize(new Dimension(170, 60));
			JPanel centerTopPanelLeftLeft = new JPanel(new BorderLayout());
			centerTopPanelLeftLeft.add(getParametersPanel(), BorderLayout.CENTER);
			centerTopPanelLeftLeft.add(observingPanel, BorderLayout.EAST);

			tbgPanel = new TbgPanel(control.getModel().getTbgModel());
			tbgPanel.setPreferredSize(new Dimension(170, 60));
			JPanel centerTopPanelLeft = new JPanel(new BorderLayout());


			centerTopPanelLeft.add(centerTopPanelLeftLeft, BorderLayout.CENTER);
			centerTopPanelLeft.add(tbgPanel, BorderLayout.EAST);
			noisePanel = new NoisePanel(control.getModel().getNoiseModel());
			noisePanel.setPreferredSize(new Dimension(170, 60));
			control.refreshNoiseUnit(control.getModel().getModelTuningModel().getTelescope().getFileName());
			centerTopPanel.add(centerTopPanelLeft, BorderLayout.CENTER);
			centerTopPanel.add(noisePanel, BorderLayout.EAST);
			JPanel tempPanel = new JPanel(new BorderLayout());
			freqPanel = new FrequencyPanel(control.getModel().getFrequencyModel());
			freqPanel.setPreferredSize(new Dimension(250, 60));
			tempPanel.add(centerTopPanel, BorderLayout.CENTER);
			tempPanel.add(freqPanel, BorderLayout.EAST);
			tabPanel.add(tempPanel, BorderLayout.NORTH);
			emAbTabbedPanelView = new EmAbTabbedPanelView(control.getModel().getEmAbTabbedPanelModel());
			tabPanel.add(emAbTabbedPanelView);

			JPanel panel2 = new JPanel(new FlowLayout());
			JCheckBox modelCheckBox = new JCheckBox();
			modelCheckBox.setSelected(true);
			modelCheckBox.setEnabled(false);
			panel2.add(new JLabel("LTE-RADEX"));
			panel2.add(modelCheckBox);
			tabSourceManager.insertTab("LTE-RADEX", null, tabPanel, null, 0);
			tabSourceManager.setTabComponentAt(0, panel2);
			modelPanel.add(tabSourceManager, BorderLayout.CENTER);
		}
		return modelPanel;
	}

	public LteRadexControl getControl() {
		return control;
	}

	/**
	 * @return the emAbTabbedPanelView
	 */
	public final EmAbTabbedPanelView getEmAbTabbedPanelView() {
		return emAbTabbedPanelView;
	}

	public void displayTelescopeError(String path) {
		String telescopeName = new File(path).getName();
		boolean deliveryFileExist = new File(Software.getTelescopePath()+ File.separator +telescopeName).exists();
		Object[] options = {"Select another telescope", "Select the delivery telescope ", "Cancel"};
		String message = "Telescope file \"" + path + "\" not found.\n"
				+ "You can select another file or choose the delivery telescope with the same name:";
		if (!deliveryFileExist) {
			options = new Object[2];
			options[0] = "Select another telescope";
			options[1] =  "Cancel"	;

			message = "Telescope file \"" + path + "\" not found.\n"
					+ "Please select another file:";
		}

		int choice = JOptionPane.showOptionDialog(this, message,
				"Cassis error", JOptionPane.YES_NO_OPTION,
				JOptionPane.ERROR_MESSAGE, null, options, options[0]);
		if (choice == 0)
			modelTuningPanel.getTelescopePanel().getTelescopeButton().doClick();
		else if (choice == 1 && deliveryFileExist) {

			modelTuningPanel.getTelescopePanel().getModel().setSelectedTelescope(
					Software.getTelescopePath()+ File.separator +telescopeName);
		}
	}

	/**
	 * Refresh the view from the model. Designed to be used after a model change.
	 * Do not use it for a simple element change.
	 */
	public void refresh() {
		LteRadexModel lrm = control.getModel();

		modelTuningPanel.setModel(lrm.getModelTuningModel());
		thresholdPanel.setModel(lrm.getThresholdModel());
		parametersPanel.getControl().setModel(lrm.getParametersModel());
		tbgPanel.setModel(lrm.getTbgModel());
		noisePanel.setModel(lrm.getNoiseModel());
		freqPanel.setModel(lrm.getFrequencyModel());
		emAbTabbedPanelView.getControl().setModel(lrm.getEmAbTabbedPanelModel());
	}
}
