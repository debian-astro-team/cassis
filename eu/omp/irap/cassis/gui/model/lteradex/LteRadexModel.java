/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.lteradex;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.BufferedWriterProperty;
import eu.omp.irap.cassis.common.ParameterDescription;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.events.DataModel;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.gui.model.parameter.ParametersModel;
import eu.omp.irap.cassis.gui.model.parameter.frequency.FrequencyModel;
import eu.omp.irap.cassis.gui.model.parameter.imin.TbgModel;
import eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent.EmAbTabbedPanelModel;
import eu.omp.irap.cassis.gui.model.parameter.modeltuning.ModelTuningModel;
import eu.omp.irap.cassis.gui.model.parameter.noise.NoiseModel;
import eu.omp.irap.cassis.gui.model.parameter.observing.ObservingModel;
import eu.omp.irap.cassis.gui.model.parameter.threshold.ThresholdModel;
import eu.omp.irap.cassis.gui.model.table.ModelIdentifiedInterface;
import eu.omp.irap.cassis.gui.util.PropertiesLinkedHashMap;
import eu.omp.irap.cassis.gui.util.ScriptUtilities;

public class LteRadexModel extends DataModel implements ModelIdentifiedInterface {

	public static final String MODEL_ID_EVENT = "modelId";
	public static final String TELESCOPE_ERROR_EVENT = "telescopeError";

	private static final Logger LOGGER = LoggerFactory.getLogger(LteRadexModel.class);
	public static final double DEFAULT_RESOLUTION = 0.1;
	public static final UNIT DEFAULT_UNIT_RESOLUTION = UNIT.MHZ;

	private int modelId = -1;
	private String name = "";
	private ModelTuningModel tuningModel;
	private ThresholdModel thresholdModel;
	private EmAbTabbedPanelModel emAbTabbedPanelModel;
	private ParametersModel parametersModel;
	private TbgModel iminModel;
	private NoiseModel noiseModel;
	private FrequencyModel freqModel;
	private ObservingModel observingModel;


	public LteRadexModel() {
		tuningModel = new ModelTuningModel(115.0, 116.0, UNIT.GHZ, DEFAULT_RESOLUTION, DEFAULT_UNIT_RESOLUTION);
		tuningModel.setLineValue(115.5);
		tuningModel.setBandValue(4.0);
		thresholdModel = new ThresholdModel();

		parametersModel = new ParametersModel();
		iminModel = new TbgModel();
		noiseModel = new NoiseModel();
		emAbTabbedPanelModel = new EmAbTabbedPanelModel();
		freqModel = new FrequencyModel();
		observingModel = new ObservingModel();
	}

	public ModelTuningModel getModelTuningModel() {
		return tuningModel;
	}

	@Override
	public ThresholdModel getThresholdModel() {
		return thresholdModel;
	}

	public ParametersModel getParametersModel() {
		return parametersModel;
	}

	public NoiseModel getNoiseModel() {
		return noiseModel;
	}

	public FrequencyModel getFrequencyModel() {
		return freqModel;
	}

	public TbgModel getTbgModel() {
		return iminModel;
	}

	/**
	 * @return the emAbTabbedPanelModel
	 */
	public final EmAbTabbedPanelModel getEmAbTabbedPanelModel() {
		return emAbTabbedPanelModel;
	}

	/**
	 * Gives the model id.
	 *
	 * @return model id
	 */
	@Override
	public int getModelId() {
		return modelId;
	}

	/**
	 * Changes the model id.
	 *
	 * @param modelId
	 *            new model idsetProperty
	 */
	@Override
	public void setModelId(int modelId) {
		this.modelId = modelId;
		fireDataChanged(new ModelChangedEvent(MODEL_ID_EVENT, modelId));
	}

	/**
	 * Get a list of properties.
	 */
	private Map<String, String> getProperties() {
		Map<String, String> map = new PropertiesLinkedHashMap();
		map.put("Name", "");

		map.putAll(tuningModel.getProperties());
		map.putAll(thresholdModel.getProperties());
		map.putAll(parametersModel.getProperties());
		map.putAll(noiseModel.getProperties());
		map.putAll(emAbTabbedPanelModel.getProperties());

		return map;
	}

	/**
	 * Make a config file for python script
	 * @param configname : config filename
	 * @param nameValue : a value for the 'Name' key
	 */
	public void makeConfig(String configname, String nameValue) {
		Map<String, String> map = getProperties();
		map.put("Name", nameValue);
		ScriptUtilities.makeConfig(configname, map);
	}

	public Map<String, ParameterDescription> getMapParameter() {
		Map<String, ParameterDescription> result = new HashMap<>();
		result.putAll(tuningModel.getMapParameter());
		result.putAll(thresholdModel.getMapParameter());
		result.putAll(parametersModel.getMapParameter());
		result.putAll(observingModel.getMapParameter());
		result.putAll(noiseModel.getMapParameter());
		result.putAll(freqModel.getMapParameter());
		result.putAll(iminModel.getMapParameter());

		return result;
	}

	@Override
	public boolean isIdentified() {
		return modelId != -1;
	}

	/**
	 * Save the config in a file.
	 *
	 * @param configFile The file where to save the config.
	 */
	public void saveConfig(File configFile) {
		try (BufferedWriterProperty out =
				new BufferedWriterProperty(new FileWriter(configFile))){
			saveConfig(out);
			out.flush();
		} catch (Exception e) {
			LOGGER.error("Error while saving the config", e);
		}
	}

	@Override
	public void saveConfig(BufferedWriterProperty out) throws IOException {
		tuningModel.saveConfig(out);
		thresholdModel.saveConfig(out);
		parametersModel.saveConfig(out);
		observingModel.saveConfig(out);
		iminModel.saveConfig(out);
		noiseModel.saveConfig(out);
		freqModel.saveConfig(out);
		emAbTabbedPanelModel.saveConfig(out, "");
	}

	public void loadConfig(String sfile) throws IOException {
		if (!"".equals(sfile)) {
			try (FileInputStream fis = new FileInputStream(sfile)) {
				Properties prop = new Properties();
				prop.load(fis);
				loadConfig(prop);
			}
		}
	}

	@Override
	public void loadConfig(Properties prop) throws IOException {
		tuningModel.loadConfig(prop);
		thresholdModel.loadConfig(prop);
		parametersModel.loadConfig(prop);
		observingModel.loadConfig(prop);
		iminModel.loadConfig(prop);
		noiseModel.loadConfig(prop);
		freqModel.loadConfig(prop);
		emAbTabbedPanelModel.loadConfig(prop);

		if (tuningModel.getTelescopeModel().getTelescopeError() != null || parametersModel.getTelescopeError() != null) {
			if (parametersModel.getTelescopeError() != null) {
				fireDataChanged(new ModelChangedEvent(TELESCOPE_ERROR_EVENT,
						parametersModel.getTelescopeError()));
			} else {
				fireDataChanged(new ModelChangedEvent(TELESCOPE_ERROR_EVENT,
						tuningModel.getTelescopeModel().getTelescopeError()));
			}
			tuningModel.getTelescopeModel().resetTelescopeError();
			parametersModel.resetTelescopeError();
		}
	}

	@Override
	public void setIdentified(boolean b) {
		if (!b)
			modelId = -1;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public XAxisCassis getXaxisAskToDisplay() {
		return XAxisCassis.getXAxisCassis(tuningModel.getValUnit());
	}

	public ObservingModel getObservingModel() {
		return observingModel;
	}

}
