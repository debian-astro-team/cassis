/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.lineanalysis;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.gui.model.ButtonsPanel;
import eu.omp.irap.cassis.gui.model.CustomTabbedPaneUI;
import eu.omp.irap.cassis.gui.model.parameter.ParametersPanel;
import eu.omp.irap.cassis.gui.model.parameter.data.LoadDataPanel;
import eu.omp.irap.cassis.gui.model.parameter.imin.TbgPanel;
import eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent.EmAbTabbedPanelView;
import eu.omp.irap.cassis.gui.model.parameter.noise.NoisePanel;
import eu.omp.irap.cassis.gui.model.parameter.observing.ObservingPanel;
import eu.omp.irap.cassis.gui.model.parameter.oversampling.OversamplingPanel;
import eu.omp.irap.cassis.gui.model.parameter.threshold.ThresholdPanel;
import eu.omp.irap.cassis.gui.model.parameter.tuning.TuningPanel;
import eu.omp.irap.cassis.gui.model.table.JCassisTable;
import eu.omp.irap.cassis.gui.template.JComboBoxTemplate;

public class LineAnalysisPanel extends JPanel {

	private static final long serialVersionUID = -8037298351028626855L;

	private JPanel topLeftPanel;
	private TuningPanel tuningPanel;
	private JPanel speciesPanel;
	private JCassisTable<MoleculeDescription> moleculesTable;
	private JComboBoxTemplate comboBoxTemplate;
	private ButtonsPanel buttonPanel;
	private ThresholdPanel thresholdPanel;
	private LoadDataPanel loadDataPanel;

	private JPanel topPanel;
	private JPanel modelPanel;
	private JTabbedPane tabSourceManager;
	private EmAbTabbedPanelView modelTabbedPanel;
	private JCheckBox modelCheckBox;
	private LineAnalysisControl control;
	private ParametersPanel parametersPanel;
	private TbgPanel tbgPanel;
	private JPanel parametersIminNoisePanel;
	private NoisePanel noisePanel;
	private OversamplingPanel oversamplingPanel;
	private ObservingPanel observingPanel;


	public LineAnalysisPanel() {
		this(new LineAnalysisModel());
	}

	public LineAnalysisPanel(LineAnalysisModel lineAnalysisModel) {
		super();

		this.control = new LineAnalysisControl(lineAnalysisModel, this);
		this.control.setView(this);

		setLayout(new BorderLayout());
		setSize(new Dimension(1024, 600));
		setPreferredSize(new Dimension(1024, 600));

		add(getTopPanelView(), BorderLayout.NORTH);
		add(getModelPanel(), BorderLayout.CENTER);

		revalidate();
	}

	public JPanel getTopPanelView() {
		if (topPanel == null) {
			topPanel = new JPanel(new BorderLayout());
			topPanel.add(getTopLeftPanel(), BorderLayout.WEST);
			topPanel.add(getSpeciesPanel(), BorderLayout.CENTER);
			topPanel.add(getButtonPanel(), BorderLayout.EAST);
			topPanel.setPreferredSize(new Dimension(1000, 250));
			topPanel.setSize(1000, 250);
		}
		return topPanel;
	}

	private JPanel getModelPanel() {
		if (modelPanel == null) {
			modelPanel = new JPanel();

			modelPanel.setLayout(new BorderLayout());
			modelPanel.add(getTabSourceManager(), BorderLayout.CENTER);
		}
		return modelPanel;
	}

	public JTabbedPane getTabSourceManager() {
		if (tabSourceManager == null) {
			tabSourceManager = new JTabbedPane();
			tabSourceManager.setUI(new CustomTabbedPaneUI(this.getBackground()));
			JPanel tabPanel = new JPanel(new BorderLayout());
			tabPanel.add(getParametersIminNoisePanel(), BorderLayout.NORTH);
			tabPanel.add(getLteRadexTabbedPanel());
			JPanel panel = new JPanel(new FlowLayout());
			panel.add(new JLabel("LTE-RADEX"));
			panel.add(getModelCheckBox());
			tabSourceManager.insertTab("LTE-RADEX", null, tabPanel, null, 0);
			tabSourceManager.setTabComponentAt(0, panel);
		}
		return tabSourceManager;
	}

	public EmAbTabbedPanelView getLteRadexTabbedPanel() {
		if (modelTabbedPanel == null) {
			modelTabbedPanel = new EmAbTabbedPanelView(control.getModel().getAbsorptionModel());
		}
		return modelTabbedPanel;
	}

	public JCheckBox getModelCheckBox() {
		if (modelCheckBox == null) {
			modelCheckBox = new JCheckBox();
			modelCheckBox.setSelected(control.getModel().isLteRadexSelected());
			modelCheckBox.setEnabled(true);
			modelCheckBox.addActionListener(control);
		}
		return modelCheckBox;
	}

	private JComponent getParametersIminNoisePanel() {
		if (parametersIminNoisePanel == null) {
			parametersIminNoisePanel = new JPanel(new BorderLayout());
			parametersPanel = new ParametersPanel(control.getModel().getParametersModel());
			JPanel panelTemp = new JPanel(new BorderLayout());
			JPanel panelTemp2 = new JPanel(new BorderLayout());
			panelTemp.add(getTbgPanel(), BorderLayout.CENTER);
			panelTemp.add(getNoisePanel(), BorderLayout.EAST);
			panelTemp2.add(panelTemp, BorderLayout.CENTER);
			panelTemp2.add(getOversamplingPanel(), BorderLayout.EAST);

			observingPanel = new ObservingPanel(control.getModel().getObservingModel());
			observingPanel.setPreferredSize(new Dimension(170, 60));
			JPanel centerTopPanelLeftLeft = new JPanel(new BorderLayout());
			centerTopPanelLeftLeft.add(parametersPanel, BorderLayout.CENTER);
			centerTopPanelLeftLeft.add(observingPanel, BorderLayout.EAST);


			parametersIminNoisePanel.add(centerTopPanelLeftLeft, BorderLayout.CENTER);
			parametersIminNoisePanel.add(panelTemp2, BorderLayout.EAST);
		}
		return parametersIminNoisePanel;
	}

	private JComponent getTbgPanel() {
		if (tbgPanel == null) {
			tbgPanel = new TbgPanel(control.getModel().getTbgModel());
		}
		return tbgPanel;
	}

	private JComponent getNoisePanel() {
		if (noisePanel == null) {
			noisePanel = new NoisePanel(control.getModel().getNoiseModel());
			control.refreshNoiseUnit(control.getModel().getParametersModel().getTelescope().getName());
		}
		return noisePanel;
	}

	private JComponent getOversamplingPanel() {
		if (oversamplingPanel == null) {
			oversamplingPanel = new OversamplingPanel(control.getModel().getOversamplingModel());
		}
		return oversamplingPanel;
	}

	/**
	 * @return the control
	 */
	public final LineAnalysisControl getControl() {
		return control;
	}

	/**
	 * Refresh the view from the model. Designed to be used after a model change.
	 * Do not use it for a simple element change.
	 */
	public void refresh() {
		LineAnalysisModel lam = control.getModel();
		loadDataPanel.setModel(lam.getLoadDataModel());
		tuningPanel.setModel(lam.getTunningModel());
		thresholdPanel.setModel(lam.getThresholdModel());
		moleculesTable.setModel(lam.getCassisTableMoleculeModel());

		modelCheckBox.setSelected(control.getModel().isLteRadexSelected());

		parametersPanel.getControl().setModel(lam.getParametersModel());
		tbgPanel.setModel(lam.getTbgModel());
		noisePanel.setModel(lam.getNoiseModel());
		oversamplingPanel.setModel(lam.getOversamplingModel());

		modelTabbedPanel.getControl().setModel(lam.getAbsorptionModel());
	}

	public ThresholdPanel getThresholdPanel() {
		if (thresholdPanel == null) {
			thresholdPanel = new ThresholdPanel(control.getModel().getThresholdModel());
		}
		return thresholdPanel;
	}

	private JComponent getTopLeftPanel() {
		if (topLeftPanel == null) {
			topLeftPanel = new JPanel();
			BoxLayout layout = new BoxLayout(topLeftPanel, BoxLayout.Y_AXIS);
			topLeftPanel.setLayout(layout);
			topLeftPanel.add(getLoadDataPanel());
			topLeftPanel.add(getTuningPanel());
			topLeftPanel.add(getThresholdPanel());
		}
		return topLeftPanel;
	}

	public LoadDataPanel getLoadDataPanel() {
		if (loadDataPanel == null) {
			loadDataPanel = new LoadDataPanel(control.getModel().getLoadDataModel(), 8);
		}
		return loadDataPanel;
	}

	public TuningPanel getTuningPanel() {
		if (tuningPanel == null) {
			tuningPanel = new TuningPanel(control.getModel().getTunningModel());
		}
		return tuningPanel;
	}

	public ButtonsPanel getButtonPanel() {
		if (buttonPanel == null) {
			buttonPanel = new ButtonsPanel();
			buttonPanel.setBorder(BorderFactory.createEmptyBorder(8, 1, 3, 2));
			buttonPanel.getLoadConfigButton().addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.onLoadConfigButtonTopPanel();
				}
			});

			buttonPanel.getLoadConfigButton().addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					control.loadConfigButtonClicked(e);
				}
			});

			buttonPanel.getSaveConfigButton().addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.onSaveConfigButtonTopPanel();
				}
			});
			buttonPanel.getDisplayButton().addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.onDisplayButtonTopPanel();
				}
			});
		}

		return buttonPanel;
	}

	private JComponent getSpeciesPanel() {
		if (speciesPanel == null) {
			speciesPanel = new JPanel();
			speciesPanel.setLayout(new BorderLayout());
			speciesPanel.setBorder(new TitledBorder("Template"));

			speciesPanel.add(getComboBoxTemplate(), BorderLayout.NORTH);
			JScrollPane scrollPane = new JScrollPane(getMoleculesTable());
			speciesPanel.add(scrollPane, BorderLayout.CENTER);
			getMoleculesTable().setScrollPane(scrollPane);
		}
		return speciesPanel;
	}

	public JComboBoxTemplate getComboBoxTemplate() {
		if (comboBoxTemplate == null) {
			comboBoxTemplate = new JComboBoxTemplate(false, true);
			comboBoxTemplate.setSelectedItem(control.getModel().getTemplate());
			comboBoxTemplate.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					String template = (String) comboBoxTemplate.getSelectedItem();
					if (template == null) {
						return;
					}
					control.getModel().setTemplate(template);
				}
			});

		}
		return comboBoxTemplate;
	}

	public JCassisTable<MoleculeDescription> getMoleculesTable() {
		if (moleculesTable == null) {
			moleculesTable = new JCassisTable<>(
					control.getModel().getCassisTableMoleculeModel(), true, true);
			moleculesTable.setTemplateCombo(getComboBoxTemplate());
			moleculesTable.getColumn("Tag").setMinWidth(52);
			moleculesTable.getColumn("Tag").setMaxWidth(52);
			moleculesTable.getColumn("Tag").setPreferredWidth(52);
			moleculesTable.getColumn("Database").setMinWidth(0);
			moleculesTable.getColumn("Database").setMaxWidth(0);
			moleculesTable.getColumn("Database").setPreferredWidth(0);
			moleculesTable.getColumn("Sel.").setMinWidth(25);
			moleculesTable.getColumn("Sel.").setMaxWidth(25);
			moleculesTable.getColumn("Sel.").setPreferredWidth(25);
			moleculesTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
				@Override
				public void valueChanged(ListSelectionEvent event) {
					moleculesTable.getSelectionModel().clearSelection();
				}
			});

			moleculesTable.getModel().addTableModelListener(new TableModelListener() {
				@Override
				public void tableChanged(TableModelEvent e) {
					if (e.getType() == 0 && e.getColumn() == 3) { // Update on Sel. column.
						Object value = moleculesTable.getModel().getValueAt(e.getFirstRow(), e.getColumn());
						if (getBool(value)) {
							control.getModel().addMolecule(e.getFirstRow());
						}
					}
				}
			});
		}
		return moleculesTable;
	}

	/**
	 * Return a boolean for a given object. Return the value of the object if it
	 * is a boolean, false otherwise.
	 *
	 * @param object The object.
	 * @return the value of the object if it is a boolean, false otherwise.
	 */
	private boolean getBool(Object object) {
		if (object instanceof Boolean) {
			return (boolean) object;
		}
		return false;
	}

	/**
	 * Display a pop-up for a load configuration error.
	 *
	 * @param message The message to display.
	 */
	public void displayLoadConfigError(String message) {
		JOptionPane.showMessageDialog(this, message, "Load configuration error",
				JOptionPane.WARNING_MESSAGE);
	}
}
