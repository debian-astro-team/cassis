/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.lineanalysis;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.BufferedWriterProperty;
import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.common.ParameterDescription;
import eu.omp.irap.cassis.common.Template;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.events.DataModel;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.gui.model.parameter.ParametersModel;
import eu.omp.irap.cassis.gui.model.parameter.data.LoadDataModel;
import eu.omp.irap.cassis.gui.model.parameter.imin.TbgModel;
import eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent.EmAbTabbedPanelModel;
import eu.omp.irap.cassis.gui.model.parameter.noise.NoiseModel;
import eu.omp.irap.cassis.gui.model.parameter.observing.ObservingModel;
import eu.omp.irap.cassis.gui.model.parameter.oversampling.OversamplingModel;
import eu.omp.irap.cassis.gui.model.parameter.threshold.ThresholdModel;
import eu.omp.irap.cassis.gui.model.parameter.tuning.TuningModel;
import eu.omp.irap.cassis.gui.model.table.CassisTableModelInterface;
import eu.omp.irap.cassis.gui.model.table.CassisTableMolecule;
import eu.omp.irap.cassis.gui.model.table.CassisTableMoleculeModel;
import eu.omp.irap.cassis.gui.template.ListTemplateManager;
import eu.omp.irap.cassis.template.SqlTemplateManager;

/*
 * Model for the LTE line table component.
 *
 * @author girard
 * @since 20 dec. 2004
 *
 * @author Nerriere Rose
 * @since juillet 2008
 */
public class LineAnalysisModel extends DataModel implements CassisTableModelInterface {

	public static final String ADD_MOLECULE_FROM_TOP_EVENT = "addMoleculeFromTop";
	public static final String COMPUTE_EVENT = "Compute";
	public static final String LTE_RADEX_SELECTED_EVENT = "lteRadexSelected";
	public static final String SCROLL_EVENT = "scroll";
	public static final String TAG_MOLECULE_SELECTED_EVENT = "tagMoleculeSelected";
	public static final String TEMPLATE_EVENT = "templateChange";
	public static final String LOAD_CONFIG_ERROR_EVENT = "loadConfigLaError";

	private static final Logger LOGGER = LoggerFactory.getLogger(LineAnalysisModel.class);

	private LoadDataModel loadDataPanelModel;
	private TuningModel tunningModel;
	private ThresholdModel thresholdModel;

	private String template;
	private CassisTableMoleculeModel cassisTableMoleculeModel;

	private String name = "";
	private boolean lteRadexModelSelected;
	private boolean identified = false;
	private int modelId;
	private boolean telescopeChangedOnce;

	private LteRadexModel lteRadexModel;


	/**
	 * Constructor makes a new LineAnalysisModel.
	 */
	public LineAnalysisModel() {
		template = ListTemplateManager.getInstance().getDefaultTemplateToUse();
		thresholdModel = new ThresholdModel();
		tunningModel = new TuningModel(true, true, UNIT.KM_SEC_MOINS_1);
		loadDataPanelModel = new LoadDataModel();
		lteRadexModel = new LteRadexModel();
		identified = false;
		telescopeChangedOnce = false;
		setTelescopeChanged(false);
	}

	public EmAbTabbedPanelModel getAbsorptionModel() {
		return lteRadexModel.getComponentManagementModel();
	}

	public EmAbTabbedPanelModel getComponentManagementModel() {
		return lteRadexModel.getComponentManagementModel();
	}

	public boolean isLteRadexSelected() {
		return lteRadexModelSelected;
	}

	public void setLteRadexSelected(boolean lteRadexSelected) {
		this.lteRadexModelSelected = lteRadexSelected;
		fireDataChanged(new ModelChangedEvent(LTE_RADEX_SELECTED_EVENT, lteRadexSelected));
	}

	@Override
	public boolean isIdentified() {
		return identified;
	}

	public Map<String, ParameterDescription> getMapParameter() {
		Map<String, ParameterDescription> result = new HashMap<>();
		result.putAll(lteRadexModel.getParametersModel().getMapParameter());
		result.putAll(lteRadexModel.getObservingModel().getMapParameter());
		result.put("noise", new ParameterDescription(lteRadexModel.getNoiseModel().getNoise()));
		result.put("oversampling", new ParameterDescription(lteRadexModel.getOversamplingModel().getOversampling()));
		result.putAll(lteRadexModel.getTbgModel().getMapParameter());

		result.putAll(tunningModel.getMapParameter());
		result.putAll(thresholdModel.getMapParameter());
		return result;
	}

	/*
	 * @see eu.omp.irap.cassis.gui.model.table.ModelIdentifiedInterface#setIdentified(boolean)
	 */
	@Override
	public void setIdentified(boolean b) {
		identified = b;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.model.table.ModelIdentifiedInterface#setModelId(int)
	 */
	@Override
	public void setModelId(int modelId) {
		this.modelId = modelId;
		this.setIdentified(true);
	}

	/**
	 * @see eu.omp.irap.cassis.gui.model.table.ModelIdentifiedInterface#getModelId()
	 */
	@Override
	public int getModelId() {
		return modelId;
	}

	@Override
	public void loadConfig(String sfile) throws IOException {
		if (!"".equals(sfile)) {
			try (InputStreamReader reader =
					new InputStreamReader(new FileInputStream(sfile))) {
				Properties prop = new Properties();
				prop.load(reader);
				loadConfig(prop);
			}
		}
	}

	@Override
	public void loadConfig(Properties prop) throws IOException {
		loadDataPanelModel.loadConfig(prop);
		tunningModel.loadConfig(prop);
		thresholdModel.loadConfig(prop);

		// Jtable
		template = prop.getProperty("template", prop.getProperty("selectedTemplate"));
		if ("Full Database".equals(template))
			template = Template.FULL_TEMPLATE;
		setTemplateMoleculeList(template);
		setTemplate(template);


		boolean found = false;
		int numFirstMol = 0;

		if (prop.containsKey("nbMoleculesSelected")) {
			int nbMoleculesSelected = Integer.parseInt(prop.getProperty("nbMoleculesSelected"));
			for (int i = 0; i < nbMoleculesSelected; i++) {
				String selectedMolTag = prop.getProperty("moleculeSelectedNum" + i);
				if (selectedMolTag != null) {
					for (MoleculeDescription mol : cassisTableMoleculeModel.getList()) {
						if (!found)
							numFirstMol++;
						if (mol.getTag() == Integer.parseInt(selectedMolTag)) {
							found = true;
							mol.setCompute(true);
							break;
						}
					}
				}
			}
		} else {
			int moleculeSelected = Integer.parseInt(prop.getProperty("moleculeSelected"));
			for (MoleculeDescription mol : cassisTableMoleculeModel.getList()) {
				if (!found)
					numFirstMol++;
				if (mol.getTag() == moleculeSelected) {
					mol.setCompute(true);
					break;
				}
			}
		}

		// Needed to determine the pos of the scrollbar
		setScrollBar(numFirstMol);

		boolean lteRadexSelected = Boolean.valueOf(prop.getProperty("absorptionSelected"))
				|| Boolean.valueOf(prop.getProperty("emissionSelected"))
				|| Boolean.valueOf(prop.getProperty("lteRadexSelected")) ;
		setLteRadexSelected(lteRadexSelected);

		lteRadexModel.getParametersModel().loadConfig(prop);
		lteRadexModel.getObservingModel().loadConfig(prop);
		lteRadexModel.getTbgModel().loadConfig(prop);
		lteRadexModel.getNoiseModel().loadConfig(prop);
		lteRadexModel.getOversamplingModel().loadConfig(prop);
		lteRadexModel.getFreqModel().loadConfig(prop);
		getAbsorptionModel().loadConfig(prop);

		handleLoadConfigErrors();
	}

	/**
	 * Handle load configuration errors if it is needed. It create a message,
	 *  fire an event and reset the errors.
	 */
	private void handleLoadConfigErrors() {
		if (loadDataPanelModel.haveError() || lteRadexModel.getParametersModel().haveTelescopeError()) {
			boolean dataError = loadDataPanelModel.haveDataFileError();
			boolean telescopeError = loadDataPanelModel.haveTelescopeError() ||
					lteRadexModel.getParametersModel().haveTelescopeError();
			StringBuilder sb = new StringBuilder();
			sb.append("<html>");
			if (dataError && telescopeError) {
				sb.append("Some errors were detected in the provided configuration file:<br>");
			} else {
				sb.append("An error was detected in the provided configuration file:<br>");
			}
			if (dataError) {
				sb.append(" - The data file (");
				sb.append(loadDataPanelModel.getDataFileError());
				sb.append(") can not be found.<br>");
			}
			if (telescopeError) {
				String telescopePathError = loadDataPanelModel.haveTelescopeError() ?
						loadDataPanelModel.getTelescopeError()
						: lteRadexModel.getParametersModel().getTelescopeError();
				sb.append(" - The telescope file (");
				sb.append(telescopePathError);
				sb.append(") can not be found.");
			}
			sb.append("</html>");
			loadDataPanelModel.resetDataFileError();
			loadDataPanelModel.resetTelescopeError();
			lteRadexModel.getParametersModel().resetTelescopeError();

			fireDataChanged(new ModelChangedEvent(LOAD_CONFIG_ERROR_EVENT, sb.toString()));
		}
	}

	/**
	 * Compute the needed position of the scroll bar then send an event to set
	 *  it to this position.
	 *
	 * @param numFirstMol Number (index) of the molecule in the list.
	 */
	private void setScrollBar(int numFirstMol) {
		int nbMol = cassisTableMoleculeModel.getList().size();
		Double percent;
		Double value;
		percent = (Double.valueOf(100) / (double) nbMol) * (double) (numFirstMol - 1);
		value = percent / 100;
		fireDataChanged(new ModelChangedEvent(SCROLL_EVENT, value));
	}

	public ParametersModel getParametersModel() {
		return lteRadexModel.getParametersModel();
	}

	public NoiseModel getNoiseModel() {
		return lteRadexModel.getNoiseModel();
	}

	public TbgModel getTbgModel() {
		return lteRadexModel.getTbgModel();
	}

	public OversamplingModel getOversamplingModel() {
		return lteRadexModel.getOversamplingModel();
	}

	public void saveConfig(File configFile) throws IOException {
		try (BufferedWriterProperty out =
				new BufferedWriterProperty(new FileWriter(configFile))) {
			saveConfig(out);
		} catch (Exception e) {
			LOGGER.error("Error while saving the config", e);
		}
	}

	@Override
	public void saveConfig(BufferedWriterProperty out) throws IOException {
		out.writeTitle("Generals Parameters");
		getLoadDataModel().saveConfig(out);
		tunningModel.saveConfig(out);
		thresholdModel.saveConfig(out);
		out.writeProperty("template", template);
		out.writeProperty("nbMoleculesSelected", String.valueOf(getTagsMoleculesSelected().size()));
		int i = 0;
		String key;
		String val;
		for (int tag : getTagsMoleculesSelected()) {
			key = "moleculeSelectedNum" + i;
			val = String.valueOf(tag);
			out.writeProperty(key, val);
			i++;
		}
		out.newLine();
		out.writeProperty("lteRadexSelected", String.valueOf(isLteRadexSelected()));
		lteRadexModel.saveConfig(out);
		out.newLine();
		out.flush();
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public XAxisCassis getXaxisAskToDisplay() {
		return XAxisCassis.getXAxisCassis(tunningModel.getValUnit());
	}

	@Override
	public ThresholdModel getThresholdModel() {
		return thresholdModel;
	}

	public TuningModel getTunningModel() {
		return tunningModel;
	}

	public LoadDataModel getLoadDataModel() {
		return loadDataPanelModel;
	}

	/**
	 * Get selected template name.
	 *
	 * @return Selected template name
	 */
	public String getTemplate() {
		return template;
	}

	/**
	 * Set the molecules of the given template name.
	 *
	 * @param templateName The template name.
	 */
	public void setTemplateMoleculeList(String templateName) {
		List<MoleculeDescription> templateMoleculeList =
				SqlTemplateManager.getInstance().getTemplateMolecules(templateName);
		getCassisTableMoleculeModel().setList(templateMoleculeList);
	}

	/**
	 * Set selected template in combobox.
	 *
	 * @param template
	 *            Selected template namesetTemplate
	 */
	public void setTemplate(String template) {
		if ("Full Database".equals(template))
			template = Template.FULL_TEMPLATE;
		this.template = template;
		fireDataChanged(new ModelChangedEvent(TEMPLATE_EVENT, this.template));
	}

	/**
	 * Get molecule list.
	 *
	 * @return -list list of molecule
	 */
	public List<MoleculeDescription> getMoleculeList() {
		return getCassisTableMoleculeModel().getList();
	}

	/**
	 * Return the selected molecules.
	 *
	 * @return the selected molecules.
	 */
	public List<MoleculeDescription> getMoleculesSelected() {
		return getCassisTableMoleculeModel().getSelectedMolecules();
	}

	/**
	 * @return the cassisTableMoleculeModel
	 */
	public CassisTableMoleculeModel getCassisTableMoleculeModel() {
		if (cassisTableMoleculeModel == null) {
			this.cassisTableMoleculeModel = new CassisTableMoleculeModel(new ArrayList<>(),
					new String[] {"Name", "Tag", "Database", "Sel." }, new Integer[] { CassisTableMolecule.NAME_INDICE,
							CassisTableMolecule.TAG_INDICE, CassisTableMolecule.DATA_SOURCE_INDICE, CassisTableMolecule.COMPUTE_INDICE });
			List<MoleculeDescription> templateMoleculeList =
					SqlTemplateManager.getInstance().getTemplateMolecules(template);
			this.cassisTableMoleculeModel.setList(templateMoleculeList);
		}
		return cassisTableMoleculeModel;
	}

	public List<Integer> getTagsMoleculesSelected() {
		List<Integer> list = new ArrayList<>();
		for (MoleculeDescription mol : getCassisTableMoleculeModel().getList()) {
			if (mol.isCompute())
				list.add(mol.getTag());
		}
		return list;
	}

	public void setTagMoleculeSelected(int tag) {
		for (MoleculeDescription mol : getCassisTableMoleculeModel().getList()) {
			if (mol.getTag() == tag) {
				mol.setCompute(true);
				fireDataChanged(new ModelChangedEvent(TAG_MOLECULE_SELECTED_EVENT, tag));
				break;
			}
		}
	}

	public void addMolecule(int number) {
		if (number >= 0 && number < getCassisTableMoleculeModel().getList().size()) {
			MoleculeDescription mol = getCassisTableMoleculeModel().getList().get(number);
			if (mol != null) {
				fireDataChanged(new ModelChangedEvent(ADD_MOLECULE_FROM_TOP_EVENT, mol.clone()));
			}
		}
	}

	public void changeComputing(int number) {
		if (number >= 0 && number < getCassisTableMoleculeModel().getList().size()) {
			MoleculeDescription mol = getCassisTableMoleculeModel().getList().get(number);
			if (mol != null) {
				mol.setCompute(!mol.isCompute());
				fireDataChanged(new ModelChangedEvent(COMPUTE_EVENT));
			}
		}
	}

	public ObservingModel getObservingModel() {
		return lteRadexModel.getObservingModel();
	}

	public boolean isTelescopeChangedOnce() {
		return telescopeChangedOnce;
	}

	public void setTelescopeChanged(boolean telescopeChangedOnce) {
		this.telescopeChangedOnce = telescopeChangedOnce;
	}

}
