/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.lineanalysis;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.cassisd.Server;
import eu.omp.irap.cassis.cassisd.ServerImpl;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.ISpectrumComputation;
import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.common.ParameterDescription;
import eu.omp.irap.cassis.common.ProgressDialogConstants;
import eu.omp.irap.cassis.common.RadexException;
import eu.omp.irap.cassis.common.Telescope;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.database.access.AccessDataBase;
import eu.omp.irap.cassis.database.access.DataBaseConnection;
import eu.omp.irap.cassis.database.access.UnknowMoleculeException;
import eu.omp.irap.cassis.gui.model.lteradex.LteRadexControl;
import eu.omp.irap.cassis.gui.model.parameter.ParametersModel;
import eu.omp.irap.cassis.gui.model.parameter.data.LoadDataModel;
import eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent.ComponentDescription;
import eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent.ComponentSpecies;
import eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent.EmAbTabbedPanelControl;
import eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent.EmAbTabbedPanelModel;
import eu.omp.irap.cassis.gui.model.parameter.threshold.ThresholdModel;
import eu.omp.irap.cassis.gui.model.parameter.tuning.TuningModel;
import eu.omp.irap.cassis.gui.optionpane.ProgressDialog;
import eu.omp.irap.cassis.gui.util.Deletable;
import eu.omp.irap.cassis.gui.util.FileUtils;
import eu.omp.irap.cassis.lineanalysis.LineAnalysisParameters;
import eu.omp.irap.cassis.lineanalysis.LineAnalysisResult;
import eu.omp.irap.cassis.properties.Software;

/**
 * Class LteLineControl: Control les evenements de LineAnalysisTopPanel
 *
 * @author thachtn
 *
 */
public class LineAnalysisControl implements ActionListener, ModelListener, ISpectrumComputation, Deletable {

	private static final Logger LOGGER = LoggerFactory.getLogger(LineAnalysisControl.class);
	private LineAnalysisModel model;
	private LineAnalysisPanel view;
	private EmAbTabbedPanelControl modelTabbedPanelControl;
	private List<LineAnalysisDisplayListener> listeners = new ArrayList<>();
	private LineAnalysisResult result;


	/**
	 * Constructor.
	 *
	 * @param lteLineTableModel The model.
	 * @param view The view.
	 */
	public LineAnalysisControl(LineAnalysisModel lteLineTableModel, LineAnalysisPanel view) {
		this.model = lteLineTableModel;

		model.addModelListener(this);
		model.getLoadDataModel().addModelListener(this);
		model.getParametersModel().addModelListener(this);
	}

	/**
	 * @return the modelTabbedPanelControl
	 */
	public EmAbTabbedPanelControl getAbsSourceControl() {
		return modelTabbedPanelControl;
	}

	/**
	 * Handle onDisplayButtonClick Evenement.
	 */
	public void onDisplayButtonTopPanel() {
		if (!checkParameters())
			return;
		computeSpectrum();
	}

	/**
	 * Compute LineAnalysisSpectrum.
	 *
	 * @return the result of LteLine
	 */
	public LineAnalysisResult computeSpectrum() {
		try {
			File file = new File(Software.getConfigPath() +
					File.separator + "line-analysis" + File.separator + "last.lam");
			if (!file.getParentFile().exists()) {
				file.getParentFile().mkdirs();
			}
			model.saveConfig(file);
		} catch (Exception e) {
			LOGGER.warn("Error while saving the configuration", e);
		}
		if (Software.getUserConfiguration().isTestMode()) {
			invokeSpectrumComputation();
			display();
		} else {
			new ProgressDialog(view, this);
		}
		return result;
	}

	/**
	 * Run the LineAnalysisSpectrum computation.
	 */
	@Override
	public void invokeSpectrumComputation() {
		result = null;

		CassisSpectrum cassisSpectrum = model.getLoadDataModel().getCassisSpectrum();
		if (cassisSpectrum == null) {
			JOptionPane.showMessageDialog(view, "Please select data to display this model.", "Alert",
					JOptionPane.ERROR_MESSAGE);
			return;
		}

		EmAbTabbedPanelModel lteRadexModel = model.getAbsorptionModel();

		List<ComponentDescription> componentsDescription = null;
		if (model.isLteRadexSelected()) {
			componentsDescription = lteRadexModel.getComponentsWithMoleculeSelected();
		}

		Map<String, ParameterDescription> mapParameter = model.getMapParameter();

		Server server = new ServerImpl();

		boolean umeException = false;
		boolean radexException = false;
		try {
			LineAnalysisParameters parameters  = new LineAnalysisParameters(mapParameter,
					componentsDescription, model.getMoleculesSelected(), model.getParametersModel().getTelescope());
			result = server.computeLineAnalysisSpectrum(parameters, cassisSpectrum);
		} catch (UnknowMoleculeException ume) {
			umeException = true;
			LOGGER.error("A molecule (tag: {}) is unknown, stopping the operation", ume.getTag(), ume);
			JOptionPane.showMessageDialog(view, ume.getInterruptedMessage(),
					"Unknow molecule", JOptionPane.ERROR_MESSAGE);
		} catch (RadexException re) {
			radexException = true;
			LOGGER.error("A RadexException occured while invoking the spectrum computation", re);
			JOptionPane.showMessageDialog(view, re.getMessage(),
					"RADEX error", JOptionPane.ERROR_MESSAGE);
		} catch (Exception e) {
			LOGGER.error("Error while invoking the spectrum computation", e);
			if (!Software.getUserConfiguration().isTestMode()) {
				JOptionPane.showMessageDialog(view, e.getMessage(), "Alert", JOptionPane.ERROR_MESSAGE);
			}
		}

		if (!umeException && !radexException && result == null) {
			JOptionPane.showMessageDialog(view, "Impossible to compute the spectrum", "Warning",
					JOptionPane.WARNING_MESSAGE);
		} else if (result != null) {
			result.setTelescope(model.getLoadDataModel().getTelescope());
		}
	}

	public LineAnalysisResult getResult() {
		return result;
	}

	public void setResult(LineAnalysisResult lineAnalysisResult) {
		result = lineAnalysisResult;
	}

	/**
	 * Get model.
	 *
	 * @return {@link LineAnalysisModel}
	 */
	public LineAnalysisModel getModel() {
		return model;
	}

	/**
	 * Add LteLineTableListener.
	 *
	 * @param listener
	 *            {@link LineAnalysisDisplayListener}
	 */
	public void addLteLineTableListener(LineAnalysisDisplayListener listener) {
		listeners.add(listener);
	}

	/**
	 * Remove LteLineTableListener.
	 *
	 * @param listener
	 *            {@link LineAnalysisDisplayListener}
	 */
	public void removeLteLineTableListener(LineAnalysisDisplayListener listener) {
		listeners.remove(listener);
	}

	/**
	 * Fire LineAnalysisDisplayEvent event.
	 *
	 * @param e
	 *            {@link LineAnalysisDisplayEvent}
	 */
	public void fireLteLineTableDisplayEvent(LineAnalysisDisplayEvent e) {
		for (LineAnalysisDisplayListener listener : listeners) {
			listener.lteLineTableDisplayClicked(e);
		}
	}

	/**
	 * @return the view
	 */
	public LineAnalysisPanel getView() {
		return view;
	}

	/**
	 * Set the view.
	 *
	 * @param view The view to set.
	 */
	public void setView(LineAnalysisPanel view) {
		this.view = view;

		if (view != null)
			modelTabbedPanelControl = view.getLteRadexTabbedPanel().getControl();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == view.getModelCheckBox()) {
			if (view.getModelCheckBox().isSelected()) {
				view.getTabSourceManager().setSelectedIndex(0);
			}
			modelTabbedPanelControl.getView().setEnabled(view.getModelCheckBox().isSelected());
			model.setLteRadexSelected(view.getModelCheckBox().isSelected());

			view.getModelCheckBox().setSelected(model.isLteRadexSelected());
			enableAbsPanel(view.getModelCheckBox().isSelected());

			if (model.isLteRadexSelected())
				view.getTabSourceManager().setSelectedIndex(0);
		}
	}

	public void onLoadConfigButtonTopPanel() {
		JFileChooser fc = new CassisJFileChooser(Software.getLineAnalysisConfigPath(),
				Software.getLastFolder("line-analysis-config"));
		fc.resetChoosableFileFilters();
		FileFilter filter = new FileNameExtensionFilter("Line Analysis Module (*.lam)","lam");
		fc.addChoosableFileFilter(filter);
		fc.addChoosableFileFilter(new FileNameExtensionFilter("LteRadex Module (*.ltm)","ltm"));
		fc.setFileFilter(filter);
		if (fc.showOpenDialog(getView()) == JFileChooser.APPROVE_OPTION) {
			if (fc.getSelectedFile() != null) {
				loadConfig(fc.getSelectedFile());
			}
			Software.setLastFolder("line-analysis-config", fc.getSelectedFile().getParent());
		}
	}

	public void loadConfigButtonClicked(MouseEvent e) {
		final String pathname = Software.getConfigPath() +
		 File.separator + "line-analysis"+File.separator + "last.lam";

		if (SwingUtilities.isRightMouseButton(e) &&
			Software.getUserConfiguration().isTestMode() &&
					new File(pathname).exists())
		try {
			LineAnalysisControl.this.getModel().loadConfig(pathname);
		} catch (IOException ioe) {
			LOGGER.error("Unable to read config file", ioe);
		}
	}

	public void onSaveConfigButtonTopPanel() {
		File configFile = null;
		JFileChooser fc = new CassisJFileChooser(Software.getLineAnalysisConfigPath(),
				Software.getLastFolder("line-analysis-config"));

		final FileNameExtensionFilter filter = new FileNameExtensionFilter("Line Analysis Module (*.lam)","lam");
		fc.addChoosableFileFilter(filter);
		fc.setFileFilter(filter);
		if (fc.showSaveDialog(view) == JFileChooser.APPROVE_OPTION) {
			try {
				File file = fc.getSelectedFile();
				String name = file.getAbsolutePath();
				if (!"lam".equals(FileUtils.getFileExtension(name)))
					name += ".lam";
				configFile = new File(name);
				// If template name already exists
				if (configFile.exists()) {
					String message = "Configuration " + configFile.getName() + " already exists.\n"
							+ "Do you want to replace it?";
					// Modal dialog with yes/no button
					int answer = JOptionPane.showConfirmDialog(view, message, "Replace existing configuration file?",
							JOptionPane.YES_NO_OPTION);
					if (answer == JOptionPane.NO_OPTION)
						configFile = null;
				}

				if (configFile != null) {
					model.saveConfig(configFile);
				}
				Software.setLastFolder("line-analysis-config", file.getParent());
			} catch (Exception e) {
				LOGGER.error("Unable to save configuration.", e);
				JOptionPane.showMessageDialog(view, "Unable to save configuration: " + e.getMessage(), "Alert",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	public void loadConfig(File selectedFile) {
		try {
			model.loadConfig(selectedFile.getPath());

			if (model.isLteRadexSelected()) {

				if (!model.getAbsorptionModel().haveContinnumCompo()) {
					JOptionPane.showMessageDialog(view, "No Continuum detected. A continuum 0 is added", "Warning",
							JOptionPane.WARNING_MESSAGE);
					model.getAbsorptionModel().addContinuumComponent(true);
				}
				view.getTabSourceManager().setSelectedIndex(0);

			}
		} catch (IOException exc) {
			LOGGER.error("Unable to read config file", exc);
			JOptionPane.showMessageDialog(view, "Unable to read config file. " + exc.getMessage(), "Alert",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	private boolean checkParameters() {
		boolean res = true;
		if (model.getTunningModel().getMinValue() > model.getTunningModel().getMaxValue()) {
			JOptionPane.showMessageDialog(view, "Maximum frequency must be higher than minimum frequency.", "ERROR",
					JOptionPane.ERROR_MESSAGE);
			res = false;
		}
		if (res && model.getLoadDataModel().getCassisSpectrum() == null) {
			JOptionPane.showMessageDialog(view, "Please select a data to display this model.", "ERROR",
					JOptionPane.ERROR_MESSAGE);
			res = false;
		}

		if (res && model.getMoleculesSelected().isEmpty()) {
			JOptionPane.showMessageDialog(view, "You must select a species to display this model.", "ERROR",
					JOptionPane.ERROR_MESSAGE);
			res = false;
		}

		if (res && model.isLteRadexSelected()) {
			if (!haveComponentSpecies()) {
				JOptionPane.showMessageDialog(view,
						"You must have at least one component with species",
						"Component parameter error", JOptionPane.ERROR_MESSAGE);
				res = false;
			} else if (!checkFirstComponentMoleculesSelected()) {
				res = false;
			}
		}

		LteRadexControl.stopEditingComponent(view.getLteRadexTabbedPanel().getTabbedPane().getSelectedComponent());
		return res && (!model.isLteRadexSelected() || checkModelParameters());
	}

	/**
	 * Check then return if the model parameters are OK.
	 *
	 * @return true if the model parameters are OK, false otherwise.
	 */
	private boolean checkModelParameters() {
		return LteRadexControl.checkParametersForRadex(this.view,
				model.getComponentManagementModel()) &&
				LteRadexControl.checkParametersForLte(this.view,
						model.getComponentManagementModel()) &&
				LteRadexControl.checkComponentsParameters(this.view,
						model.getComponentManagementModel().getComponentsWithMoleculeSelected()) &&
				LteRadexControl.checkTelescope(this.view,
						model.getParametersModel().getTelescope(),
						model.getParametersModel().getSelectedTelescope());
	}

	/**
	 * Check if first component of the model contains all selected molecules.
	 * Display an error message with all missing molecule if it is not the case.
	 *
	 * @return if the first component of the model contains all the selected molecules.
	 */
	private boolean checkFirstComponentMoleculesSelected() {
		List<Integer> list = model.getTagsMoleculesSelected();
		List<Integer> missingMol = new ArrayList<>();
		for (int molTag : list) {
			List<MoleculeDescription> listMol =
					modelTabbedPanelControl.getModel().getMoleculesList(0);
			MoleculeDescription moleculeDescription =
					MoleculeDescription.getMoleculeDescription(molTag, listMol);
			if (moleculeDescription == null || !moleculeDescription.isCompute()) {
				missingMol.add(molTag);
			}
		}
		if (!missingMol.isEmpty()) {
			StringBuilder sb = new StringBuilder(
					"You must have the selected species in your first component containing species. ");
			sb.append(missingMol.size() > 1 ? "The missing species are:\n" : "The missing species is:\n");
			DataBaseConnection db = AccessDataBase.getDataBaseConnection();
			for (int molTag : missingMol) {
				sb.append("  - Name: ").append(db.getMolName(molTag)).append("; Tag: ").append(molTag).append('\n');
			}
			JOptionPane.showMessageDialog(view, sb.toString(), "ERROR", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}

	/**
	 * Change the enabled state of the absorption panel.
	 * @param val The enabled state to set.
	 */
	public void enableAbsPanel(boolean val) {
		view.getModelCheckBox().setSelected(val);
		model.setLteRadexSelected(val);
	}

	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (LoadDataModel.CASSIS_SPECTRUM_EVENT.equals(event.getSource())) {
			handleCassisSpectrumEvent();
		} else if (LineAnalysisModel.COMPUTE_EVENT.equals(event.getSource())) {
			view.getMoleculesTable().updateUI();
		} else if (LineAnalysisModel.TEMPLATE_EVENT.equals(event.getSource())) {
			view.getComboBoxTemplate().setSelectedItem(model.getTemplate());
		} else if (LineAnalysisModel.SCROLL_EVENT.equals(event.getSource())) {
			double percent = Double.parseDouble(String.valueOf(event.getValue()));
			scrollMoleculeTable(percent);
		} else if (ParametersModel.TELESCOPE_EVENT.equals(event.getSource())) {
			String telescopeName = (String) event.getValue();
			handleTelescopeEvent(telescopeName);
		} else if (LineAnalysisModel.LTE_RADEX_SELECTED_EVENT.equals(event.getSource())) {
			if (view != null) {
				boolean value = ((Boolean) event.getValue()).booleanValue();
				view.getModelCheckBox().setSelected(value);
			}
		} else if (LineAnalysisModel.TAG_MOLECULE_SELECTED_EVENT.equals(event.getSource())) {
			List<MoleculeDescription> molsList = model.getMoleculesSelected();
			if (model.isLteRadexSelected()) {
				model.getAbsorptionModel().setMolMandatory(molsList);
			}
		} else if (LineAnalysisModel.ADD_MOLECULE_FROM_TOP_EVENT.equals(event.getSource())) {
			MoleculeDescription molecule = (MoleculeDescription) event.getValue();
			addMoleculeFromTop(molecule);
		} else if (LoadDataModel.TELESCOPE_DATA_EVENT.equals(event.getSource())) {
			String telescope = event.getValue().toString();
			handleTelescopeDataEvent(telescope);
		} else if (LineAnalysisModel.LOAD_CONFIG_ERROR_EVENT.equals(event.getSource())) {
			String msg = event.getValue().toString();
			view.displayLoadConfigError(msg);
		}
	}

	/**
	 * Add a molecule from top molecule table to the model.
	 *
	 * @param molecule The molecule to add.
	 */
	private void addMoleculeFromTop(MoleculeDescription molecule) {
		int selectedTab = view.getLteRadexTabbedPanel().getTabbedPane().getSelectedIndex();
		int countTab = view.getLteRadexTabbedPanel().getTabbedPane().getTabCount();
		if (selectedTab == countTab - 1) {
			selectedTab--;
			view.getLteRadexTabbedPanel().getTabbedPane().setSelectedIndex(selectedTab);
		} else if (selectedTab == -1) {
			return;
		}
		addAMolecule(molecule, selectedTab);
	}

	/**
	 * @param molecule
	 * @param selectedTab
	 */
	protected void addAMolecule(MoleculeDescription molecule, int selectedTab) {
		ComponentSpecies component = (ComponentSpecies)model.getAbsorptionModel().getComponentList().get(selectedTab);
		component.addAMolecule(molecule);
	}

	/**
	 * Scroll the vertical scroll bar of the molecule table.
	 *
	 * @param percent The percent value of the new position.
	 */
	private void scrollMoleculeTable(double percent) {
		JScrollPane scroll = view.getMoleculesTable().getScrollPane();
		scroll.validate();
		double max = (double) scroll.getVerticalScrollBar().getMaximum();
		int posToGo = (int) (max * percent);
		scroll.getVerticalScrollBar().setValue(posToGo);
	}

	/**
	 * Handle LoadDataModel.TELESCOPE_DATA_EVENT.
	 *
	 * @param telescope The name of the new telescope.
	 */
	private void handleTelescopeDataEvent(String telescope) {
		boolean currentlyChanged = model.isTelescopeChangedOnce();
		model.setTelescopeChanged(true);
		if (!model.getParametersModel().getSelectedTelescope().equals(telescope))
			model.getParametersModel().setSelectedTelescope(telescope);
		telescope = model.getParametersModel().getTelescope().getName();
		if (telescope.startsWith("spire")) {
			model.getTunningModel().setBandValue(TuningModel.SPIRE_BAND_KMS);
			if (!currentlyChanged) {
				model.getThresholdModel().setThresEupMax(ThresholdModel.SPIRE_EUP_MAX_K);
			}
			model.getNoiseModel().setUnit(UNIT.M_JANSKY);
			model.getTunningModel().setBandUnit(UNIT.KM_SEC_MOINS_1);
		} else if ("pacs".equalsIgnoreCase(telescope)) {
			model.getTunningModel().setBandValue(TuningModel.PACS_BAND_KMS);
			if (!currentlyChanged) {
				model.getThresholdModel().setThresEupMax(ThresholdModel.PACS_EUP_MAX_K);
			}
			model.getNoiseModel().setUnit(UNIT.M_KELVIN);
			model.getTunningModel().setBandUnit(UNIT.KM_SEC_MOINS_1);
		}
	}

	/**
	 * Handle ParametersModel.TELESCOPE_EVENT.
	 *
	 * @param telescopeName The name of the new telescope.
	 */
	private void handleTelescopeEvent(String telescopeName) {
		boolean currentlyChanged = model.isTelescopeChangedOnce();
		model.setTelescopeChanged(true);
		if ("spire".equalsIgnoreCase(telescopeName)) {
			model.getTunningModel().setBandValue(TuningModel.SPIRE_BAND_KMS);
			if (!currentlyChanged) {
				model.getThresholdModel().setThresEupMax(ThresholdModel.SPIRE_EUP_MAX_K);
			}
			model.getTunningModel().setBandUnit(UNIT.KM_SEC_MOINS_1);
		}
		else if ("pacs".equalsIgnoreCase(telescopeName)) {
			model.getTunningModel().setBandValue(TuningModel.PACS_BAND_KMS);
			if (!currentlyChanged) {
				model.getThresholdModel().setThresEupMax(ThresholdModel.PACS_EUP_MAX_K);
			}
			model.getTunningModel().setBandUnit(UNIT.KM_SEC_MOINS_1);
		}

		if (!model.getLoadDataModel().getTelescope().equals(telescopeName)) {
			model.getLoadDataModel().setTelescope(telescopeName);
			refreshNoiseUnit(telescopeName);
		}
	}

	/**
	 * Handle LoadDataModel.CASSIS_SPECTRUM_EVENT: update unit and TunningModel.
	 */
	private void handleCassisSpectrumEvent() {
		CassisSpectrum cassisSpectrum = model.getLoadDataModel().getCassisSpectrum();
		XAxisCassis axisCassis = cassisSpectrum.getxAxisOrigin();
		if (UNIT.MHZ.equals(axisCassis.getUnit())&& cassisSpectrum.getFreqMin() > 1000)
			axisCassis.setUnit(UNIT.GHZ);
		Double val1 = axisCassis.convertFromMhzFreq(cassisSpectrum.getFreqMin());
		Double val2 = axisCassis.convertFromMhzFreq(cassisSpectrum.getFreqMax());
		Double valMin = Math.min(val1, val2);
		Double valMax = Math.max(val1, val2);
		model.getTunningModel().setMinValue(valMin);
		model.getTunningModel().setMaxValue(valMax);
		model.getTunningModel().setValUnit(axisCassis.getUnit());
	}

	/**
	 * Refresh the noise unit according to the telescope.
	 *
	 * @param telescope The telescope.
	 */
	public void refreshNoiseUnit(String telescope) {
		if (telescope != null &&
				Telescope.getNameStatic(telescope).startsWith("spire")) {
			model.getNoiseModel().setUnit(UNIT.M_JANSKY);
		} else {
			model.getNoiseModel().setUnit(UNIT.M_KELVIN);
		}
	}

	/**
	 * Show a warning message for transitions because to small bandwidth.

	 * @param nbTransitionsMissing Number of missing transitions.
	 * @return The selection of the user.
	 */
	public int showWarningMessage(int nbTransitionsMissing) {
		if (nbTransitionsMissing != 0){
		String message = nbTransitionsMissing  +" spectra cannot be displayed because the bandwidth "
				+ "is too small at these frequencies";
		JOptionPane.showMessageDialog(view, message, "Information",
				JOptionPane.WARNING_MESSAGE);
		}
		return JOptionPane.YES_OPTION;
	}

	@Override
	public void display() {
		if (ProgressDialogConstants.workerInterrupted) {
			JOptionPane.showMessageDialog(view, "The spectrum computation was interrupted", "Information",
					JOptionPane.INFORMATION_MESSAGE);
			return;
		}

		if (result == null) {
			return;
		} else if (result.getFileSpectrumList().isEmpty() && result.getWarning() == 0) {
			JOptionPane.showMessageDialog(view, "No lines found within the given thresholds.\n" +
					"Most likely, your Eup max is too low and/or your Aij min is too high.",
					"Warning", JOptionPane.WARNING_MESSAGE);
		} else if (result.getFileSpectrumList().isEmpty() && result.getWarning() != 0){
			int nbTransitionsMissing = result.getWarning();
			if (nbTransitionsMissing !=0){
			String message = " No lines could be displayed because the bandwidth "
					+ "is too small at all frequencies";
			JOptionPane.showMessageDialog(view, message, "Information",
					JOptionPane.WARNING_MESSAGE);
			}
		} else {
			result.setModelIdentifiedInterface(model);
			if (showWarningMessage(result.getWarning()) == JOptionPane.YES_OPTION) {
				fireLteLineTableDisplayEvent(new LineAnalysisDisplayEvent(this, model, result));
			}
		}
	}

	/**
	 * Check then return if there is at least one ComponentSpecies.
	 *
	 * @return true if there is at lease one ComponentSpecies, false otherwise.
	 */
	private boolean haveComponentSpecies() {
		List<ComponentDescription> components =
				model.getComponentManagementModel().getComponentList();
		for (ComponentDescription component : components) {
			if (!component.isContinuum()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Set a new model.
	 *
	 * @param newLineAnalysisModel The new LineAnalysisModel to use.
	 */
	public void setModel(LineAnalysisModel newLineAnalysisModel) {
		// Remove olds listeners
		removeListeners();

		// Set news models.
		model = newLineAnalysisModel;

		// Add news listeners.
		model.addModelListener(this);
		model.getLoadDataModel().addModelListener(this);
		model.getParametersModel().addModelListener(this);

		view.refresh();
	}

	/**
	 * Remove the listeners.
	 */
	private void removeListeners() {
		model.removeModelListener(this);
		model.getLoadDataModel().removeModelListener(this);
		model.getParametersModel().removeModelListener(this);
	}

	/**
	 * @see eu.omp.irap.cassis.gui.util.Deletable#delete()
	 */
	@Override
	public void delete() {
		removeListeners();
		view.getMoleculesTable().delete();
		view.getLteRadexTabbedPanel().getControl().delete();
	}
}
