/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.spectrumanalysis;

import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.CassisException;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.ISpectrumComputation;
import eu.omp.irap.cassis.common.ProgressDialogConstants;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.gui.model.CassisModel;
import eu.omp.irap.cassis.gui.model.parameter.data.LoadDataModel;
import eu.omp.irap.cassis.gui.model.table.CassisTableDisplayEvent;
import eu.omp.irap.cassis.gui.model.table.CassisTableDisplayListenerList;
import eu.omp.irap.cassis.gui.optionpane.ProgressDialog;
import eu.omp.irap.cassis.properties.Software;

public class SpectrumAnalysisControl implements ModelListener, ISpectrumComputation {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpectrumAnalysisControl.class);

	private SpectrumAnalysisModel model;
	private final SpectrumAnalysisPanel view;

	private CassisTableDisplayListenerList listeners = new CassisTableDisplayListenerList();
	private List<CassisSpectrumListener> cassisSpectrumListeners = new ArrayList<>();
	private CommentedSpectrum[] results;


	public SpectrumAnalysisControl(SpectrumAnalysisModel model, SpectrumAnalysisPanel panel) {
		this.model = model;
		this.view = panel;
		model.addModelListener(this);
		model.getLoadDataModel().addModelListener(this);
	}

	public SpectrumAnalysisControl(SpectrumAnalysisModel model) {
		this(model, null);
	}

	public SpectrumAnalysisModel getModel() {
		return model;
	}

	public void loadConfig() {
		JFileChooser chooser = new CassisJFileChooser(Software.getSpectrumAnalysisConfigPath(),
				Software.getLastFolder("spectrum-analysis-config"));
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Spectrum Analysis Model files (*.sam)", "sam");
		chooser.setFileFilter(filter);

		int returnVal = chooser.showOpenDialog(view);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			try {
				model.loadConfig(chooser.getSelectedFile());
			} catch (IOException e) {
				LOGGER.error("Unable to read the config file", e);
			}
			Software.setLastFolder("spectrum-analysis-config",
					chooser.getCurrentDirectory().getAbsolutePath());
		}
	}

	public void saveConfig() {
		JFileChooser chooser = new CassisJFileChooser(Software.getSpectrumAnalysisConfigPath(),
			Software.getLastFolder("spectrum-analysis-config"));
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Spectrum Analysis Model files (*.sam)", "sam");
		chooser.setFileFilter(filter);
		int returnVal = chooser.showSaveDialog(view);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			try {
				File selectedFile = chooser.getSelectedFile();

				if (!selectedFile.getPath().endsWith(".sam"))
					selectedFile = new File(selectedFile.getPath() + ".sam");

				if (selectedFile.exists()) {
					String message = "Configuration " + selectedFile.getName() + " already exists.\n"
							+ "Do you want to replace it?";
					// Modal dialog with yes/no button
					int answer = JOptionPane.showConfirmDialog(view, message, "Replace existing configuration file?",
							JOptionPane.YES_NO_OPTION);
					if (answer == JOptionPane.NO_OPTION)
						selectedFile = null;
				}

				if (selectedFile != null) {
					model.saveConfig(selectedFile);
				}
				Software.setLastFolder("spectrum-analysis-config",
						chooser.getCurrentDirectory().getAbsolutePath());
			} catch (IOException e) {
				LOGGER.error("Unable to save configuration", e);
				JOptionPane.showMessageDialog(view, "Unable to save configuration: ", "Alert",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	public void onDisplayButtonClicked() {
		if (model.getLoadDataModel().getCassisSpectrum() == null) {
			JOptionPane.showMessageDialog(view, "No data defined", "Alert", JOptionPane.ERROR_MESSAGE);
			return;
		}

		if (!cassisSpectrumListeners.isEmpty()) {
			// fire cassis spectrum event
			// pour le cas affichage de l'overlays dans lineanalysis
			fireCassisSpectrumEvent(new EventObject(model.getLoadDataModel().getCassisSpectrum()));
			return;
		}

		try {
			CommentedSpectrum[] result = computeSpectrums();

			if (ProgressDialogConstants.workerInterrupted) {
				JOptionPane.showMessageDialog(view, "The spectrum computation was interrupted", "Information",
						JOptionPane.INFORMATION_MESSAGE);
				return;
			}

			if (result == null) {
				throw new CassisException("result = null");
			}
			else {
				CassisModel fileModel = new CassisModel(result[0], model);
				fileModel.setSpectrumName(result[0].getTitle());
				result[0].setTitle("SA" + "-" + result[0].getTitle());
				listeners.fireCassisTableDisplayListener(new CassisTableDisplayEvent(this, fileModel, model,
						"Spectrum Analysis " + model.getName()));
			}
		} catch (CassisException e) {
			LOGGER.error("Can not compute the file spectrum", e);
			JOptionPane.showMessageDialog(view, "Can't compute the file spectrum: " + e.getMessage(), "Alert",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	public CommentedSpectrum[] computeSpectrums() {
		try {
			File file = new File(Software.getConfigPath() +
					File.separator + "spectrum-analysis"+File.separator + "last.sam");
			if (!file.getParentFile().exists()) {
				file.getParentFile().mkdirs();
			}
			model.saveConfig(file);
		} catch (IOException e) {
			LOGGER.debug("Error while saving the Spectrum Analysis configuration", e);
		}

		if (Software.getUserConfiguration().isTestMode())
			invokeSpectrumComputation();
		else
			new ProgressDialog(view, this);
		return results;
	}

	@Override
	public void invokeSpectrumComputation() {
		try {
			results = model.readFile();
		} catch (CassisException ce) {
			LOGGER.error("Error while invoking the spectrum computation", ce);
		}
	}

	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (LoadDataModel.CASSIS_SPECTRUM_EVENT.equals(event.getSource())) {
			CassisSpectrum cassisSpectrum = model.getLoadDataModel().getCassisSpectrum();

			XAxisCassis axisCassis = XAxisCassis.getXAxisUnknown();
			try {
				axisCassis = cassisSpectrum.getxAxisOrigin().clone();
				if (UNIT.MHZ.equals(axisCassis.getUnit())&& cassisSpectrum.getFreqMin() > 1000) {
					axisCassis.setUnit(UNIT.GHZ);
				}
				Double val1 = axisCassis.convertFromMhzFreq(cassisSpectrum.getFreqMin());
				Double val2 = axisCassis.convertFromMhzFreq(cassisSpectrum.getFreqMax());
				Double valMin = Math.min(val1, val2);
				Double valMax = Math.max(val1, val2);
				model.setValMin(valMin);
				model.setValMax(valMax);
				model.setValUnit(axisCassis.getUnit());

			} catch (CloneNotSupportedException e) {
				LOGGER.error(e.getMessage());
			}
		} else if (SpectrumAnalysisModel.LOAD_CONFIG_ERROR_EVENT.equals(event.getSource())) {
			view.displayLoadConfigError(event.getValue().toString());
		}
	}

	/**
	 * Add cassis spectrum listeners
	 *
	 * @param l The CassisSpectrumListener to add.
	 */
	public void addCassisSpectrumListeners(CassisSpectrumListener l) {
		cassisSpectrumListeners.add(l);
	}

	/**
	 * Fire cassis spectrum event to all abonnements.
	 *
	 * @param e The event.
	 */
	public void fireCassisSpectrumEvent(EventObject e) {
		for (CassisSpectrumListener listener : cassisSpectrumListeners) {
			listener.cassisSpectrumEvent(e);
		}
	}

	/**
	 * @return the listeners
	 */
	public CassisTableDisplayListenerList getListeners() {
		return listeners;
	}

	@Override
	public void display() {
		// Unused here
	}

	/**
	 * Set a new model.
	 *
	 * @param newSpectrumAnalysisModel The new SpectrumAnalysisModel to use.
	 */
	public void setModel(SpectrumAnalysisModel newSpectrumAnalysisModel) {
		// Remove olds listeners
		model.removeModelListener(this);
		model.getLoadDataModel().removeModelListener(this);
		// Set new model
		model = newSpectrumAnalysisModel;
		// Add news listeners
		model.addModelListener(this);
		model.getLoadDataModel().addModelListener(this);

		view.refresh();
	}

	public void loadConfigButtonClicked(MouseEvent e) {
		final String pathname = Software.getConfigPath() +
				 File.separator + "spectrum-analysis"+File.separator + "last.sam";

		if (SwingUtilities.isRightMouseButton(e) &&
				Software.getUserConfiguration().isTestMode() &&
				new File(pathname).exists()) {
			try {
				getModel().loadConfig(new File(pathname));
			} catch (IOException ioe) {
				LOGGER.warn("Error while reading the last configuration file {}", pathname, ioe);
			}
		}
	}
}
