/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.spectrumanalysis;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import eu.omp.irap.cassis.gui.model.parameter.data.LoadDataPanel;
import eu.omp.irap.cassis.gui.model.parameter.tuning.TuningPanel;

public class SpectrumAnalysisPanel extends JPanel {

	public static final String WILDCARD = "*";
	private static final long serialVersionUID = 1L;
	private JButton displayButton;
	private LoadDataPanel dataPanel;
	private JButton loadButton;
	private JButton saveButton;
	private JPanel buttonPanel;
	private SpectrumAnalysisControl control;
	private TuningPanel tuningPanel;


	public SpectrumAnalysisPanel() {
		SpectrumAnalysisModel model = new SpectrumAnalysisModel();
		control = new SpectrumAnalysisControl(model, this);
		initialyse();
	}

	public SpectrumAnalysisPanel(SpectrumAnalysisModel model) {
		control = new SpectrumAnalysisControl(model, this);
		initialyse();
	}

	public SpectrumAnalysisPanel(SpectrumAnalysisControl control) {
		this.control = control;
		initialyse();
	}

	private void initialyse() {
		setLayout(new BorderLayout());
		setSize(new Dimension(1016, 150));
		setPreferredSize(new Dimension(1016, 150));
		JPanel northWestPanel = new JPanel();
		northWestPanel.setLayout(new BoxLayout(northWestPanel, BoxLayout.Y_AXIS));
		northWestPanel.add(getLoadDataPanel(), northWestPanel);
		northWestPanel.add(getTuningPanel(), northWestPanel);
		add(getButtonsPanel(), BorderLayout.EAST);
		add(northWestPanel, BorderLayout.CENTER);
	}

	/**
	 * @return the control
	 */
	public final SpectrumAnalysisControl getControl() {
		return control;
	}

	private TuningPanel getTuningPanel() {
		if (tuningPanel == null) {
			tuningPanel = new TuningPanel(control.getModel().getTuningModel());
		}
		return tuningPanel;
	}

	public JPanel getButtonsPanel() {
		if (buttonPanel == null) {
			buttonPanel = new JPanel(new BorderLayout());
			buttonPanel.add(getLoadButton(), BorderLayout.NORTH);
			getLoadButton().addMouseListener(new MouseAdapter() {

				@Override
				public void mouseClicked(MouseEvent e) {
					control.loadConfigButtonClicked(e);
				}
			});

			saveButton = new JButton("Save config");
			saveButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.saveConfig();
				}
			});
			buttonPanel.add(saveButton, BorderLayout.SOUTH);

			buttonPanel.add(getDisplayButton(), BorderLayout.CENTER);
			buttonPanel.setBorder(BorderFactory.createEmptyBorder(8, 1, 3, 2));
		}
		return buttonPanel;
	}

	public JButton getLoadButton() {
		if (loadButton == null) {
			loadButton = new JButton("Load config");
			loadButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.loadConfig();
				}
			});
		}
		return loadButton;
	}

	public LoadDataPanel getLoadDataPanel() {
		if (dataPanel == null) {
			dataPanel = new LoadDataPanel(control.getModel().getLoadDataModel());
		}
		return dataPanel;
	}

	public JButton getDisplayButton() {
		if (displayButton == null) {
			displayButton = new JButton("Display");
			displayButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.onDisplayButtonClicked();
				}
			});
		}
		return displayButton;
	}

	/**
	 * Refresh the view.
	 */
	public void refresh() {
		SpectrumAnalysisModel sam = control.getModel();
		dataPanel.setModel(sam.getLoadDataModel());
		tuningPanel.setModel(sam.getTuningModel());
	}

	/**
	 * Display a pop-up for a load configuration error.
	 *
	 * @param message The message to display.
	 */
	public void displayLoadConfigError(String message) {
		JOptionPane.showMessageDialog(this, message, "Load configuration error",
				JOptionPane.WARNING_MESSAGE);
	}

	public static void main(String[] args) {
		SpectrumAnalysisPanel panel = new SpectrumAnalysisPanel();

		JFrame frame = new JFrame("Spectrum Analysis");
		frame.setContentPane(panel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.pack();
	}
}
