/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.spectrumanalysis;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import eu.omp.irap.cassis.cassisd.Server;
import eu.omp.irap.cassis.cassisd.ServerImpl;
import eu.omp.irap.cassis.common.BufferedWriterProperty;
import eu.omp.irap.cassis.common.CassisException;
import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.events.DataModel;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.gui.model.parameter.data.LoadDataModel;
import eu.omp.irap.cassis.gui.model.parameter.threshold.ThresholdModel;
import eu.omp.irap.cassis.gui.model.parameter.tuning.TuningModel;
import eu.omp.irap.cassis.gui.model.table.ModelIdentifiedInterface;

public class SpectrumAnalysisModel extends DataModel implements ModelIdentifiedInterface {

	public static final String LOAD_CONFIG_ERROR_EVENT = "loadConfigSaError";

	private boolean identified = false;
	private int modelId;
	private String name = "";

	private LoadDataModel loadDataModel;
	private TuningModel tuningModel;


	public SpectrumAnalysisModel() {
		setLoadDataModel(new LoadDataModel());
		tuningModel = new TuningModel(true, false, UNIT.GHZ);
	}

	public void loadConfig(File selectedFile) throws IOException {
		try (FileInputStream fis = new FileInputStream(selectedFile)) {
			Properties prop = new Properties();
			prop.load(fis);
			loadConfig(prop);
		}
	}

	@Override
	public void loadConfig(Properties prop) throws IOException {
		// old file config
		if (prop.containsKey("frequencyMin")) {
			String nameData = prop.getProperty("datafile");
			File file = new File(nameData);
			if (file.exists())
				loadDataModel.loadData(file);
			tuningModel.setMinValue(Double.valueOf(prop.getProperty("frequencyMin")));
			tuningModel.setMaxValue(Double.valueOf(prop.getProperty("frequencyMax")));

			return;
		}

		loadDataModel.loadConfig(prop);
		tuningModel.loadConfig(prop);

		handleLoadConfigErrors();
	}

	/**
	 * Handle load configuration errors if it is needed. It create a message,
	 *  fire an event and reset the errors.
	 */
	private void handleLoadConfigErrors() {
		if (loadDataModel.haveError()) {
			boolean dataError = loadDataModel.haveDataFileError();
			boolean telescopeError = loadDataModel.haveTelescopeError();
			StringBuilder sb = new StringBuilder();
			sb.append("<html>");
			if (dataError && telescopeError) {
				sb.append("Some errors were detected in the provided configuration file:<br>");
			} else {
				sb.append("An error was detected in the provided configuration file:<br>");
			}
			if (dataError) {
				sb.append(" - The data file (");
				sb.append(loadDataModel.getDataFileError());
				sb.append(") can not be found.<br>");
			}
			if (telescopeError) {
				sb.append(" - The telescope file (");
				sb.append(loadDataModel.getTelescopeError());
				sb.append(") can not be found.");
			}
			sb.append("</html>");
			loadDataModel.resetDataFileError();
			loadDataModel.resetTelescopeError();
			fireDataChanged(new ModelChangedEvent(LOAD_CONFIG_ERROR_EVENT, sb.toString()));
		}
	}

	public void saveConfig(File selectedFile) throws IOException {
		try (BufferedWriterProperty out =
				new BufferedWriterProperty(new FileWriter(selectedFile))) {
			saveConfig(out);
			out.flush();
		}
	}

	public CommentedSpectrum[] readFile() throws CassisException {
		Server server = new ServerImpl();
		XAxisCassis xAxisCassis = XAxisCassis.getXAxisCassis(tuningModel.getValUnit());
		Double valMin = tuningModel.getMinValue();
		Double valMax = tuningModel.getMaxValue();

		if (xAxisCassis.isInverted()) {
			valMin = tuningModel.getMaxValue();
			valMax = tuningModel.getMinValue();
		}

		CommentedSpectrum[] readFile = server.readFile(loadDataModel.getCassisSpectrum(),
				xAxisCassis.convertToMHzFreq(valMin),
				xAxisCassis.convertToMHzFreq(valMax));

		CassisMetadata cassisMetadata = loadDataModel.
				getCassisSpectrum().getCassisMetadata("RefFreq");
		if (cassisMetadata != null){
			readFile[0].setFreqRef(Double.valueOf(cassisMetadata.getValue()));
		}
		return readFile;
	}

	/**
	 * @param loadDataModel the loadDataModel to set
	 */
	public void setLoadDataModel(LoadDataModel loadDataModel) {
		this.loadDataModel = loadDataModel;
	}

	/**
	 * @return the loadDataModel
	 */
	public LoadDataModel getLoadDataModel() {
		return loadDataModel;
	}

	public void setCassisSpectrum(CassisSpectrum cassisSpectrum) {
		loadDataModel.setCassisSpectrum(cassisSpectrum);
	}

	@Override
	public void saveConfig(BufferedWriterProperty out) throws IOException {
		loadDataModel.saveConfig(out);
		tuningModel.saveConfig(out);
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	// TODO to remove
	/**
	 * Gives the model id.
	 *
	 * @return model id
	 */
	@Override
	public int getModelId() {
		return modelId;
	}

	/**
	 * Changes the model id.
	 *
	 * @param modelId
	 *            new model id
	 */
	@Override
	public void setModelId(int modelId) {
		this.modelId = modelId;
		this.setIdentified(true);
	}

	@Override
	public boolean isIdentified() {
		return identified;
	}

	@Override
	public void setIdentified(boolean b) {
		identified = b;
	}

	public TuningModel getTuningModel() {
		return tuningModel;
	}

	@Override
	public XAxisCassis getXaxisAskToDisplay() {
		return XAxisCassis.getXAxisCassis(tuningModel.getValUnit());
	}


	/**
	 * @param bandUnit
	 *            the bandUnit to set
	 */
	public void setBandUnit(UNIT bandUnit) {
		tuningModel.setBandUnit(bandUnit);
	}
	/**
	 * @param valMin
	 *            the valMin to set
	 */
	public final void setValMin(double valMin) {
		tuningModel.setMinValue(valMin);
	}

	/**
	 * @param valMax
	 *            the valMax to set
	 */
	public final void setValMax(double valMax) {
		tuningModel.setMaxValue(valMax);
	}

	public void setValUnit(UNIT unit) {
		tuningModel.setValUnit(unit);
	}

	public void setBand(Double band) {
		if (band == null)
			return;
		tuningModel.setBandValue(band);
	}

	@Override
	public ThresholdModel getThresholdModel() {
		return null;
	}
}
