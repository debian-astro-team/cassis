/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.rotationaldiagram;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.gui.util.Deletable;

/**
 * Controller class for the MoleculeTabbedPane.
 *
 * @author M. Boiziot
 */
public class MoleculeTabbedPaneControl implements ModelListener, Deletable {

	private MoleculeTabbedPaneView view;
	private MoleculeTabbedPaneModel model;


	/**
	 * Constructor.
	 *
	 * @param theView The view.
	 * @param theModel The model.
	 */
	public MoleculeTabbedPaneControl(MoleculeTabbedPaneView theView, MoleculeTabbedPaneModel theModel) {
		this.view = theView;
		this.model = theModel;
		model.addModelListener(this);
	}

	/**
	 * Return the model.
	 *
	 * @return The model.
	 */
	public MoleculeTabbedPaneModel getModel() {
		return model;
	}

	/**
	 * Return the view.
	 *
	 * @return The view.
	 */
	public MoleculeTabbedPaneView getView() {
		return view;
	}

	/**
	 * @see eu.omp.irap.cassis.common.events.ModelListener#dataChanged(eu.omp.irap.cassis.common.events.ModelChangedEvent)
	 */
	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (MoleculeTabbedPaneModel.REMOVE_ALL_COMPS_EVENT.equals(event.getSource())) {
			view.removeAllTab();
		} else if (MoleculeTabbedPaneModel.ADD_COMP_EVENT.equals(event.getSource())) {
			RotationalMoleculeConf addedMoleculeComponent = (RotationalMoleculeConf) event.getValue();
			view.addTab(addedMoleculeComponent);
		}
	}

	/**
	 * Set a new model.
	 *
	 * @param newModel The new model to set.
	 */
	public void setModel(MoleculeTabbedPaneModel newModel) {
		model.removeModelListener(this);
		this.model = newModel;
		this.model.addModelListener(this);
		view.refresh();
	}

	/**
	 * @see eu.omp.irap.cassis.gui.util.Deletable#delete()
	 */
	@Override
	public void delete() {
	}

}
