/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.rotationaldiagram;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.BufferedWriterProperty;
import eu.omp.irap.cassis.common.events.DataModel;
import eu.omp.irap.cassis.gui.model.parameter.rotationaldata.RotationalDataModel;
import eu.omp.irap.cassis.gui.model.parameter.rotationaltuning.RotationalTuningModel;
import eu.omp.irap.cassis.gui.model.parameter.telescope.TelescopeModel;
import eu.omp.irap.cassis.parameters.PointInformation;
import eu.omp.irap.cassis.parameters.RotationalComponent;
import eu.omp.irap.cassis.parameters.RotationalFile;
import eu.omp.irap.cassis.parameters.RotationalMolecule;
import eu.omp.irap.cassis.parameters.RotationalSelectionData;

/**
 * Model of the RotationalDiagram module.
 *
 * @author M. Boiziot
 */
public class RotationalDiagramModel extends DataModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(RotationalDiagramModel.class);

	private RotationalDataModel rotationalDataModel;
	private RotationalTuningModel rotationalTuningModel;
	private MoleculeTabbedPaneModel moleculeTabbedPaneModel;


	/**
	 * Constructor.
	 */
	public RotationalDiagramModel() {
		this.rotationalDataModel = new RotationalDataModel();
		this.rotationalTuningModel = new RotationalTuningModel();
		this.moleculeTabbedPaneModel = new MoleculeTabbedPaneModel();
	}

	/**
	 * @see eu.omp.irap.cassis.common.events.DataModel#saveConfig(eu.omp.irap.cassis.common.BufferedWriterProperty)
	 */
	@Override
	public void saveConfig(BufferedWriterProperty out) throws IOException {
		rotationalDataModel.saveConfig(out);
		out.newLine();
		rotationalTuningModel.saveConfig(out);
		out.newLine();
		moleculeTabbedPaneModel.saveConfig(out);
	}

	/**
	 * Save a config.
	 *
	 * @param configFile Where to save the config.
	 * @throws IOException Required by {@link DataModel} but shouldn't happend here.
	 */
	public void saveConfig(File configFile) throws IOException {
		try (BufferedWriterProperty out = new BufferedWriterProperty(
				new FileWriter(configFile))) {
			saveConfig(out);
		} catch (Exception e) {
			LOGGER.error("Error while saving the config", e);
		}
	}

	/**
	 * @see eu.omp.irap.cassis.common.events.DataModel#loadConfig(java.util.Properties)
	 */
	@Override
	public void loadConfig(Properties prop) throws IOException {
		rotationalDataModel.loadConfig(prop);
		rotationalTuningModel.loadConfig(prop);
		moleculeTabbedPaneModel.loadConfig(prop);
	}

	/**
	 * Open a config file.
	 *
	 * @param sfile The path of the file to open.
	 * @throws IOException In case of IO error while reading the file.
	 */
	public void loadConfig(String sfile) throws IOException {
		if (!"".equals(sfile)) {
			try (InputStreamReader reader = new InputStreamReader(
					new FileInputStream(sfile))) {
				Properties prop = new Properties();
				prop.load(reader);
				loadConfig(prop);
			}
		}
	}

	/**
	 * Return the {@link RotationalDataModel}.
	 *
	 * @return the {@link RotationalDataModel}.
	 */
	public RotationalDataModel getRotationalDataModel() {
		return rotationalDataModel;
	}

	/**
	 * Return the {@link RotationalTuningModel}.
	 *
	 * @return the {@link RotationalTuningModel}.
	 */
	public RotationalTuningModel getRotationalTuningModel() {
		return rotationalTuningModel;
	}

	/**
	 * Return the {@link MoleculeTabbedPaneModel}.
	 *
	 * @return the {@link MoleculeTabbedPaneModel}.
	 */
	public MoleculeTabbedPaneModel getMoleculeTabbedPaneModel() {
		return moleculeTabbedPaneModel;
	}

	/**
	 * Refresh the components from the data.
	 */
	public void refreshFakeComponents() {
		moleculeTabbedPaneModel.removeAllComponents();
		for (RotationalMolecule rotMol : rotationalDataModel.getData().getMoleculeList()) {
			RotationalMoleculeConf mc = new RotationalMoleculeConf(rotMol.getTag(), "Species from file");
			mc.setEnabled(false);
			for (RotationalComponent rc : rotMol.getComponentsList()) {
				List<RotationalSelectionData> allowedData = rc.getListPoints().get(0).getAllowedType();
				RotationalComp rco = new RotationalComp(rc.getNumCompo());
				rco.setListDataAllowed(allowedData);
				rco.setDataSelected(allowedData.get(0));
				mc.addComponent(rco);
			}
			moleculeTabbedPaneModel.addComponent(mc);
		}
	}

	/**
	 * Create then return a model with fake data.
	 *
	 * @return a model with fake data.
	 */
	public static RotationalDiagramModel createModelWithFakeData() {
		RotationalDiagramModel model = new RotationalDiagramModel();
		List<RotationalMolecule> listRotMol = new ArrayList<>();
		final RotationalMolecule rotMol = new RotationalMolecule(0, "No name");
		RotationalComponent component = rotMol.addComponent(0, 0);
		final PointInformation point1 = new PointInformation();
		point1.setFwhmGaussianFit(1.0);
		point1.setWFit(1.0);
		point1.setTelescope(TelescopeModel.DEFAULT_TELESCOPE);
		point1.setTypeData(RotationalSelectionData.GAUSSIAN_TYPE_DATA);
		List<RotationalSelectionData> allowList = new ArrayList<>(1);
		allowList.add(RotationalSelectionData.GAUSSIAN_TYPE_DATA);
		point1.setAllowedType(allowList);
		final PointInformation point2 = new PointInformation();
		point2.setFwhmGaussianFit(1.0);
		point2.setWFit(1.0);
		point2.setTelescope(TelescopeModel.DEFAULT_TELESCOPE);
		point2.setTypeData(RotationalSelectionData.GAUSSIAN_TYPE_DATA);
		List<RotationalSelectionData> allowList2 = new ArrayList<>(1);
		allowList2.add(RotationalSelectionData.GAUSSIAN_TYPE_DATA);
		point2.setAllowedType(allowList);
		component.addPoint(point1);
		component.addPoint(point2);
		listRotMol.add(rotMol);
		RotationalFile rf = new RotationalFile(listRotMol);
		model.getRotationalDataModel().setData(rf, true);
		model.refreshFakeComponents();
		return model;
	}
}
