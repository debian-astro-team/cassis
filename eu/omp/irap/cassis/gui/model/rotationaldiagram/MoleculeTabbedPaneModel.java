/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.rotationaldiagram;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import eu.omp.irap.cassis.common.BufferedWriterProperty;
import eu.omp.irap.cassis.common.events.DataModel;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;

/**
 * Model class of the MoleculeTabbedPane.
 *
 * @author M. Boiziot
 */
public class MoleculeTabbedPaneModel extends DataModel {

	public static final String REMOVE_ALL_COMPS_EVENT = "removeAllComponents";
	public static final String ADD_COMP_EVENT = "addComponent";
	private static final String MOL_NUMBER_SAVE = "moleculeNumber";
	private List<RotationalMoleculeConf> componentList;


	/**
	 * Constructor.
	 */
	public MoleculeTabbedPaneModel() {
		componentList = new ArrayList<>();
	}

	/**
	 * Return the list of {@link RotationalMoleculeConf}.
	 *
	 * @return The list of {@link RotationalMoleculeConf}.
	 */
	public List<RotationalMoleculeConf> getComponentList() {
		return componentList;
	}

	/**
	 * Add a {@link RotationalMoleculeConf}.
	 *
	 * @param comp The {@link RotationalMoleculeConf} to add.
	 */
	public void addComponent(RotationalMoleculeConf comp) {
		componentList.add(comp);
		fireDataChanged(new ModelChangedEvent(ADD_COMP_EVENT, comp));
	}

	/**
	 * Remove all {@link RotationalMoleculeConf}.
	 */
	public void removeAllComponents() {
		componentList.clear();
		fireDataChanged(new ModelChangedEvent(REMOVE_ALL_COMPS_EVENT));
	}

	/**
	 * @see eu.omp.irap.cassis.common.events.DataModel#saveConfig(eu.omp.irap.cassis.common.BufferedWriterProperty)
	 */
	@Override
	public void saveConfig(BufferedWriterProperty out) throws IOException {
		out.writeProperty(MOL_NUMBER_SAVE, String.valueOf(componentList.size()));
		int i = 1;
		for (RotationalMoleculeConf mc : componentList) {
			mc.setPrefixNumber(i);
			mc.saveConfig(out);
			out.newLine();
			i++;
		}
	}

	/**
	 * @see eu.omp.irap.cassis.common.events.DataModel#loadConfig(java.util.Properties)
	 */
	@Override
	public void loadConfig(Properties prop) throws IOException {
		removeAllComponents();
		int compNumber = Integer.parseInt(prop.getProperty(MOL_NUMBER_SAVE, "0"));
		for (int i = 1; i <= compNumber; i++) {
			RotationalMoleculeConf mc = new RotationalMoleculeConf();
			mc.setPrefixNumber(i);
			mc.loadConfig(prop);
			addComponent(mc);
		}
	}

}
