/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.rotationaldiagram;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent.ButtonTabComponent;

/**
 * The view component for the {@link RotationalMoleculeConf}.
 *
 * @author M. Boiziot
 */
public class RotationalMoleculeConfView extends JPanel implements ModelListener {

	private static final long serialVersionUID = 2704498358912594607L;
	private RotationalMoleculeConf model;
	private ButtonTabComponent tab;
	private List<RotationalCompPanel> listCompPanel;


	/**
	 * Constructor.
	 *
	 * @param moleculeComponent The {@link RotationalMoleculeConf} used as model.
	 */
	public RotationalMoleculeConfView(RotationalMoleculeConf moleculeComponent) {
		super();
		this.model = moleculeComponent;
		model.addModelListener(this);
		listCompPanel = new ArrayList<>(model.getComponents().size());
		initComponents();
	}

	/**
	 * @see eu.omp.irap.cassis.common.events.ModelListener#dataChanged(eu.omp.irap.cassis.common.events.ModelChangedEvent)
	 */
	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (RotationalMoleculeConf.ENABLED_STATE_EVENT.equals(event.getSource())) {
			refreshEnableState();
		}
	}

	/**
	 * Create if needed then return the {@link ButtonTabComponent} of the molecule.
	 *
	 * @return The {@link ButtonTabComponent} of the molecule.
	 */
	public ButtonTabComponent getTab() {
		if (tab == null) {
			tab = new ButtonTabComponent(model.getMoleculeName(), false);
			tab.getEnableCheckBox().setSelected(model.isEnabled());
			tab.getEnableCheckBox().addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					model.setEnabled(tab.isActivated());
				}
			});
		}
		return tab;
	}

	/**
	 * Initializes the graphics components.
	 */
	private void initComponents() {
		this.setLayout(new GridBagLayout());
		this.setBorder(BorderFactory.createTitledBorder("Components"));
		GridBagConstraints gbc = new GridBagConstraints(0, GridBagConstraints.RELATIVE,
				1, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(0, 3, 0, 3), 0, 0);
		for (RotationalComp comp : model.getComponents()) {
			RotationalCompPanel rcp = new RotationalCompPanel(comp);
			listCompPanel.add(rcp);
			this.add(rcp, gbc);
		}
		refreshEnableState();
	}

	/**
	 * Remove all the listeners.
	 */
	public void removeAllListeners() {
		this.model.removeAllListener();

		for (RotationalCompPanel rcp : listCompPanel) {
			rcp.removeAllListeners();
		}
		getTab().removeAllListeners();
	}

	/**
	 * Refresh the enabled state of the graphicals components.
	 */
	public void refreshEnableState() {
		boolean enableState = model.isEnabled();
		getTab().getEnableCheckBox().setSelected(enableState);

		this.setEnabled(enableState);
		for (RotationalCompPanel compPanel : listCompPanel) {
			compPanel.setEnabled(enableState);
		}
	}

	/**
	 * Return the list of {@link RotationalCompPanel}.
	 *
	 * @return the list of {@link RotationalCompPanel}.
	 */
	public List<RotationalCompPanel> getListCompPanel() {
		return listCompPanel;
	}
}
