/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.rotationaldiagram;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

/**
 * View class for the MoleculeTabbedPane.
 *
 * @author M. Boiziot
 */
public class MoleculeTabbedPaneView extends JPanel {

	private static final long serialVersionUID = -3562173605175168239L;
	private MoleculeTabbedPaneControl control;
	private JTabbedPane tabbedPane;


	/**
	 * The constructor.
	 *
	 * @param model The model.
	 */
	public MoleculeTabbedPaneView(MoleculeTabbedPaneModel model) {
		super(new BorderLayout());
		this.control = new MoleculeTabbedPaneControl(this, model);
		add(getTabbedPane(), BorderLayout.CENTER);
	}

	/**
	 * Create if needed then return the {@link JTabbedPane}.
	 *
	 * @return The {@link JTabbedPane}.
	 */
	public JTabbedPane getTabbedPane() {
		if (tabbedPane == null) {
			tabbedPane = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.SCROLL_TAB_LAYOUT);
			for (RotationalMoleculeConf mc : control.getModel().getComponentList()) {
				addTab(mc);
			}
		}
		return tabbedPane;
	}

	/**
	 * Add a new tab to the {@link JTabbedPane}.
	 *
	 * @param molComp The {@link RotationalMoleculeConf} used as model for the new tab.
	 */
	public void addTab(RotationalMoleculeConf molComp) {
		RotationalMoleculeConfView mcv = new RotationalMoleculeConfView(molComp);
		getTabbedPane().insertTab(molComp.getMoleculeName(), null, mcv, null, getTabbedPane().getTabCount());
		getTabbedPane().setTabComponentAt(getTabbedPane().getTabCount() - 1, mcv.getTab());
	}

	/**
	 * Remove all tab.
	 */
	public void removeAllTab() {
		while (getTabbedPane().getTabCount() > 0) {
			RotationalMoleculeConfView mcv = (RotationalMoleculeConfView) getTabbedPane().getComponentAt(0);
			mcv.removeAllListeners();
			getTabbedPane().remove(0);
		}
	}

	/**
	 * Return the controller.
	 *
	 * @return The controller.
	 */
	public MoleculeTabbedPaneControl getControl() {
		return control;
	}

	/**
	 * Refresh the view.
	 */
	public void refresh() {
		removeAll();
		for (RotationalMoleculeConf mc : control.getModel().getComponentList()) {
			addTab(mc);
		}
		repaint();
	}
}
