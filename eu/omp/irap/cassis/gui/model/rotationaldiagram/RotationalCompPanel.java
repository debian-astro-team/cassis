/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.rotationaldiagram;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.gui.util.JDoubleCassisTextField;
import eu.omp.irap.cassis.gui.util.ListenersUtil;
import eu.omp.irap.cassis.parameters.RotationalSelectionData;

/**
 * The view component for the {@link RotationalComp}.
 *
 * @author M. Boiziot
 */
public class RotationalCompPanel extends JPanel implements ModelListener {

	private static final long serialVersionUID = -6362900386383179778L;
	private RotationalComp model;
	private JComboBox<RotationalSelectionData> comboBox;
	private JCheckBox enableCheckBox;
	private JCheckBox beamDilutionCheckBox;
	private JDoubleCassisTextField sourceSizeField;
	private JLabel sourceSizeLabel;


	/**
	 * Constructor.
	 *
	 * @param rotComp The {@link RotationalComp} used as model.
	 */
	public RotationalCompPanel(final RotationalComp rotComp) {
		super();
		this.model = rotComp;
		this.model.addModelListener(this);
		initComponents();
	}

	/**
	 * Initializes the graphics components.
	 */
	private void initComponents() {
		setBorder(BorderFactory.createEtchedBorder());
		add(getEnableCheckBox());
		add(getComboBox());
		add(getBeamDilutionCheckBox());
		add(getSourceSizeLabel());
		add(getSourceSizeField());
		this.setMinimumSize(new Dimension(750, 60));
		refreshBeamComponentsPossible();
	}

	/**
	 * Create if needed then return the "enable" {@link JCheckBox}.
	 *
	 * @return the "enable" {@link JCheckBox}.
	 */
	public JCheckBox getEnableCheckBox() {
		if (enableCheckBox == null) {
			enableCheckBox = new JCheckBox("Component " + model.getNumber() + ": ", model.isEnabled());
			enableCheckBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					model.setEnabled(enableCheckBox.isSelected());
					comboBox.setEnabled(enableCheckBox.isSelected());
				}
			});
		}
		return enableCheckBox;
	}

	/**
	 * Create if needed then return the {@link JComboBox}.
	 *
	 * @return the {@link JComboBox}.
	 */
	public JComboBox<RotationalSelectionData> getComboBox() {
		if (comboBox == null) {
			comboBox = new JComboBox<>(
					model.getListDataAllowed().toArray(
							new RotationalSelectionData[
									model.getListDataAllowed().size()]));
			comboBox.setSelectedItem(model.getDataSelected());
			comboBox.setEnabled(enableCheckBox.isSelected());
			comboBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					RotationalSelectionData selected = (RotationalSelectionData)
							comboBox.getSelectedItem();
					model.setDataSelected(selected);
				}
			});
		}
		return comboBox;
	}

	/**
	 * @see javax.swing.JComponent#setEnabled(boolean)
	 */
	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		boolean compEnabled = model.isEnabled();
		boolean doubleEnabled = enabled && compEnabled;
		boolean beamField = doubleEnabled ?
				model.isBeamDilutionPossible() && model.isBeamDilution() : false;
		boolean beamCheck = doubleEnabled ? model.isBeamDilutionPossible() : false;

		getEnableCheckBox().setEnabled(enabled);
		getComboBox().setEnabled(doubleEnabled);
		getBeamDilutionCheckBox().setEnabled(beamCheck);
		getSourceSizeLabel().setEnabled(beamCheck);
		getSourceSizeField().setEnabled(beamField);
	}

	/**
	 * @see eu.omp.irap.cassis.common.events.ModelListener#dataChanged(eu.omp.irap.cassis.common.events.ModelChangedEvent)
	 */
	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (RotationalComp.ENABLED_EVENT.equals(event.getSource())) {
			boolean enabledModel = model.isEnabled();
			getEnableCheckBox().setSelected(enabledModel);
			setEnabled(true);
		} else if (RotationalComp.DATA_TYPE_EVENT.equals(event.getSource())) {
			getComboBox().setSelectedItem(model.getDataSelected());
		} else if (RotationalComp.BEAM_DILUTION_EVENT.equals(event.getSource())) {
			getBeamDilutionCheckBox().setSelected(model.isBeamDilution());
			getSourceSizeField().setEnabled(model.isBeamDilution());
		} else if (RotationalComp.SOURCE_SIZE_EVENT.equals(event.getSource())) {
			getSourceSizeField().setValue(model.getSourceSize());
		} else if (RotationalComp.BEAM_DILUTION_POSSIBLE_EVENT.equals(event.getSource())) {
			refreshBeamComponentsPossible();
		}
	}

	/**
	 * Refresh the components following if beam dilution is possible.
	 */
	private void refreshBeamComponentsPossible() {
		boolean beamPossible = model.isBeamDilutionPossible();
		getBeamDilutionCheckBox().setEnabled(beamPossible);
		if (!beamPossible) {
			getBeamDilutionCheckBox().setSelected(true);
		}
		getSourceSizeLabel().setEnabled(beamPossible);
		getSourceSizeField().setEnabled(beamPossible);
	}


	/**
	 * Remove all the listeners.
	 */
	public void removeAllListeners() {
		this.model.removeAllListener();
		ListenersUtil.removeActionListeners(getEnableCheckBox());
		ListenersUtil.removeActionListeners(getComboBox());
		ListenersUtil.removeItemListener(getBeamDilutionCheckBox());
		ListenersUtil.removeActionListeners(getSourceSizeField());
	}

	/**
	 * Create if needed then return the source size {@link JDoubleCassisTextField}.
	 *
	 * @return the source size {@link JDoubleCassisTextField}.
	 */
	public JDoubleCassisTextField getSourceSizeField() {
		if (sourceSizeField == null) {
			Map<String, Double> map = new HashMap<>();
			map.put("Multiple values found", Double.NaN);
			sourceSizeField = new JDoubleCassisTextField(new DecimalFormat("##0.0#####"),
					new DecimalFormat("##0.0#####"), map);
			sourceSizeField.setColumns(14);
			sourceSizeField.setValue(model.getSourceSize());
			sourceSizeField.addPropertyChangeListener("value", new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					double val = Double.parseDouble(evt.getNewValue().toString());
					model.setSourceSize(val);
				}
			});
		}
		return sourceSizeField;
	}

	/**
	 * Create if needed then return the beam dilution correction {@link JCheckBox}.
	 *
	 * @return The beam dilution correction {@link JCheckBox}.
	 */
	public JCheckBox getBeamDilutionCheckBox() {
		if (beamDilutionCheckBox == null) {
			beamDilutionCheckBox = new JCheckBox("Beam dilution correction");
			beamDilutionCheckBox.setSelected(model.isBeamDilution());
			beamDilutionCheckBox.addItemListener(new ItemListener() {

				@Override
				public void itemStateChanged(ItemEvent e) {
					boolean selected = beamDilutionCheckBox.isSelected();
					model.setBeamDilution(selected);
				}
			});
		}
		return beamDilutionCheckBox;
	}

	/**
	 * Create if needed then return the source size {@link JLabel}.
	 *
	 * @return the source size {@link JLabel}.
	 */
	private JLabel getSourceSizeLabel() {
		if (sourceSizeLabel == null) {
			sourceSizeLabel = new JLabel("Source Size [\"]: ");
		}
		return sourceSizeLabel;
	}
}
