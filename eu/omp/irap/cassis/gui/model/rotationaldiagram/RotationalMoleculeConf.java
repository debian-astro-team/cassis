/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.rotationaldiagram;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import eu.omp.irap.cassis.common.BufferedWriterProperty;
import eu.omp.irap.cassis.common.events.DataModel;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.database.access.AccessDataBase;
import eu.omp.irap.cassis.database.access.DataBaseConnection;

/**
 * This class describes the configuration of a rotational molecule.
 *
 * @author M. Boiziot
 */
public class RotationalMoleculeConf extends DataModel {

	public static final String ENABLED_STATE_EVENT = "enableState";

	private static final String PREFIX_SAVE = "MolComp";
	private static final String ENABLED_SAVE = "enabled";
	private static final String MOLECULE_TAG_SAVE = "moleculeTag";
	private static final String MOLECULE_NAME_SAVE = "moleculeName";

	private static final String COMPONENTS_SIZE_SAVE = "componentsSize";
	private static final String COMPONENT_SAVE = "component";

	private int prefixNumber;
	private boolean enabled;
	private int moleculeTag;
	private String moleculeName;
	private List<RotationalComp> listComp;


	/**
	 * Constructor, should only be used before using loadConfig.
	 */
	public RotationalMoleculeConf() {
		this.listComp = new ArrayList<>();
	}

	/**
	 * Constructor.
	 *
	 * @param molTag The tag of the molecule.
	 */
	public RotationalMoleculeConf(int molTag) {
		this(molTag, AccessDataBase.getDataBaseConnection().getMolName(molTag));
	}

	/**
	 * Constructor.
	 *
	 * @param molTag The tag of the molecule.
	 * @param molName The name of the molecule.
	 */
	public RotationalMoleculeConf(int molTag, String molName) {
		this.moleculeTag = molTag;
		this.moleculeName = molName;
		this.listComp = new ArrayList<>();
		this.enabled = !DataBaseConnection.NOT_IN_DATABASE.equals(molName);
	}

	/**
	 * If the molecule is enabled for the computing.
	 *
	 * @return if the molecule is enabled for the computing.
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * Change the enabled state.
	 *
	 * @param enabled The new enabled state.
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
		fireDataChanged(new ModelChangedEvent(ENABLED_STATE_EVENT));
	}

	/**
	 * Return the molecule name.
	 *
	 * @return the molecule name.
	 */
	public String getMoleculeName() {
		return moleculeName;
	}

	/**
	 * Return the molecule tag.
	 *
	 * @return the molecule tag.
	 */
	public int getMoleculeTag() {
		return moleculeTag;
	}

	/**
	 * @see eu.omp.irap.cassis.common.events.DataModel#saveConfig(eu.omp.irap.cassis.common.BufferedWriterProperty)
	 */
	@Override
	public void saveConfig(BufferedWriterProperty out) throws IOException {
		String prefix = PREFIX_SAVE + prefixNumber;
		out.writeProperty(prefix + ENABLED_SAVE, String.valueOf(enabled));
		out.writeProperty(prefix + MOLECULE_TAG_SAVE, String.valueOf(moleculeTag));
		out.writeProperty(prefix + MOLECULE_NAME_SAVE, moleculeName);

		out.writeProperty(prefix + COMPONENTS_SIZE_SAVE, String.valueOf(listComp.size()));
		for (int i = 1; i <= listComp.size(); i++) {
			RotationalComp comp = listComp.get(i - 1);
			comp.setSavePrefix(prefix + COMPONENT_SAVE + i);
			comp.saveConfig(out);
		}
	}

	/**
	 * @see eu.omp.irap.cassis.common.events.DataModel#loadConfig(java.util.Properties)
	 */
	@Override
	public void loadConfig(Properties prop) throws IOException {
		String prefix = PREFIX_SAVE + prefixNumber;
		setEnabled(Boolean.parseBoolean(prop.getProperty(prefix + ENABLED_SAVE, "true")));
		moleculeTag = Integer.parseInt(prop.getProperty(prefix + MOLECULE_TAG_SAVE, "0"));
		moleculeName = prop.getProperty(prefix + MOLECULE_NAME_SAVE, "");

		listComp.clear();
		int compSize = Integer.parseInt(prop.getProperty(prefix + COMPONENTS_SIZE_SAVE, "1"));
		for (int i = 1; i <= compSize; i++) {
			RotationalComp rc = new RotationalComp(-1);
			rc.setSavePrefix(prefix + COMPONENT_SAVE + i);
			rc.loadConfig(prop);
			addComponent(rc);
		}
	}

	/**
	 * Set a new prefix number for the save/load.
	 *
	 * @param prefixNumber The prefix number for the save/load.
	 */
	public void setPrefixNumber(int prefixNumber) {
		this.prefixNumber = prefixNumber;
	}

	/**
	 * Return the list of {@link RotationalComp} of the molecule.
	 *
	 * @return the list of {@link RotationalComp} of the molecule.
	 */
	public List<RotationalComp> getComponents() {
		return listComp;
	}

	/**
	 * Return the {@link RotationalComp} defined by a number.
	 *
	 * @param compNumber The number of the component to return.
	 * @return The {@link RotationalComp} with the given number or null if not found.
	 */
	public RotationalComp getComponent(int compNumber) {
		for (RotationalComp rotComp : listComp) {
			if (rotComp.getNumber() == compNumber) {
				return rotComp;
			}
		}
		return null;
	}

	/**
	 * Add a {@link RotationalComp} to the molecule.
	 *
	 * @param rc the {@link RotationalComp} to add.
	 */
	public void addComponent(RotationalComp rc) {
		listComp.add(rc);
	}
}
