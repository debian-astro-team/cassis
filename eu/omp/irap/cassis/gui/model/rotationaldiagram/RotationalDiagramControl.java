/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.rotationaldiagram;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.cassisd.Server;
import eu.omp.irap.cassis.cassisd.ServerImpl;
import eu.omp.irap.cassis.common.ISpectrumComputation;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.database.access.UnknowMoleculeException;
import eu.omp.irap.cassis.gui.model.parameter.rotationaldata.RotationalDataModel;
import eu.omp.irap.cassis.gui.model.parameter.rotationaltuning.TYPE_TEMPERATURE;
import eu.omp.irap.cassis.gui.optionpane.ProgressDialog;
import eu.omp.irap.cassis.gui.util.FileUtils;
import eu.omp.irap.cassis.parameters.RotationalComponent;
import eu.omp.irap.cassis.parameters.RotationalDiagramInput;
import eu.omp.irap.cassis.parameters.RotationalDiagramResult;
import eu.omp.irap.cassis.parameters.RotationalFile;
import eu.omp.irap.cassis.parameters.RotationalMolecule;
import eu.omp.irap.cassis.parameters.RotationalSelectionData;
import eu.omp.irap.cassis.properties.Software;

/**
 * Control of the RotationalDiagram module.
 *
 * @author M. Boiziot
 */
public class RotationalDiagramControl implements ModelListener, ISpectrumComputation {

	private static final Logger LOGGER = LoggerFactory.getLogger(RotationalDiagramControl.class);
	private RotationalDiagramModel model;
	private RotationalDiagramPanel view;
	private RotationalDiagramResult result;
	private List<RotationalDiagramDisplayListener> listeners;


	/**
	 * Constructor.
	 *
	 * @param aModel The model.
	 * @param aView The view.
	 */
	public RotationalDiagramControl(RotationalDiagramModel aModel, RotationalDiagramPanel aView) {
		this.model = aModel;
		this.view = aView;
		this.listeners = new ArrayList<>();
		this.model.getRotationalDataModel().addModelListener(this);
	}

	/**
	 * Return the view.
	 *
	 * @return the view.
	 */
	public RotationalDiagramPanel getView() {
		return view;
	}

	/**
	 * Return the model.
	 *
	 * @return the model.
	 */
	public RotationalDiagramModel getModel() {
		return model;
	}

	/**
	 * Open a {@link JFileChooser} to select a config file to open then open it.
	 */
	public void loadConfig() {
		JFileChooser fc = new CassisJFileChooser(Software.getRotationalDiagramConfigPath(),
				Software.getLastFolder("rotational-model"));
		fc.resetChoosableFileFilters();
		FileFilter filter = new FileNameExtensionFilter("Rotational Diagram Module (*.rdm)","rdm");
		fc.addChoosableFileFilter(filter);
		fc.setFileFilter(filter);
		if (fc.showOpenDialog(getView()) == JFileChooser.APPROVE_OPTION) {
			if (fc.getSelectedFile() != null) {
				loadConfig(fc.getSelectedFile());
			}
			Software.setLastFolder("rotational-model", fc.getCurrentDirectory().getAbsolutePath());
		}
	}

	/**
	 * Open last config file if there is one.
	 */
	public void loadLastConfig() {
		final String pathname = Software.getConfigPath() + File.separatorChar
				+ "rotational-model" + File.separatorChar + "last.rdm";

		if (new File(pathname).exists()) {
			try {
				getModel().loadConfig(pathname);
			} catch (IOException ioe) {
				LOGGER.warn("Unable to read the config file {}", pathname, ioe);
			}
		}
	}

	/**
	 * Open a {@link JFileChooser} to select a file for saving a config then
	 * save it.
	 */
	public void saveConfig() {
		if (view == null)
			return;
		File configFile = null;
		JFileChooser fc = new CassisJFileChooser(Software.getRotationalDiagramConfigPath(),
				Software.getLastFolder("rotational-model"));
		fc.addChoosableFileFilter(new FileNameExtensionFilter("Rotational Diagram Module (*.rdm)","rdm"));
		if (fc.showSaveDialog(view) == JFileChooser.APPROVE_OPTION) {
			try {
				File file = fc.getSelectedFile();
				String name = file.getAbsolutePath();
				if (!"rdm".equals(FileUtils.getFileExtension(name)))
					name += ".rdm";
				configFile = new File(name);
				// If template name already exists
				if (configFile.exists()) {
					String message = "Configuration " + configFile.getName() + " already exists.\n"
							+ "Do you want to replace it?";
					// Modal dialog with yes/no button
					int answer = JOptionPane.showConfirmDialog(view, message, "Replace existing configuration file?",
							JOptionPane.YES_NO_OPTION);
					if (answer == JOptionPane.NO_OPTION)
						configFile = null;
				}

				if (configFile != null) {
					model.saveConfig(configFile);
				}
				Software.setLastFolder("rotational-model", fc.getCurrentDirectory().getAbsolutePath());
			} catch (Exception e) {
				LOGGER.error("Unable to save configuration", e);
				JOptionPane.showMessageDialog(view, "Unable to save configuration: " + e.getMessage(), "Alert",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	/**
	 * Load a config file.
	 *
	 * @param selectedFile The config {@link File} to open.
	 */
	public void loadConfig(File selectedFile) {
		if (view == null)
			return;
		try {
			model.loadConfig(selectedFile.getPath());
		} catch (IOException exc) {
			LOGGER.error("Unable to read config file", exc);
			JOptionPane.showMessageDialog(view,
					"Unable to read config file. " + exc.getMessage(), "Alert",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Set a model.
	 *
	 * @param newRotationalDiagramModel The new model to set.
	 */
	public void setModel(RotationalDiagramModel newRotationalDiagramModel) {
		model = newRotationalDiagramModel;
		if (view != null)
			view.refresh();
	}

	/**
	 * @see eu.omp.irap.cassis.common.events.ModelListener#dataChanged(eu.omp.irap.cassis.common.events.ModelChangedEvent)
	 */
	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (RotationalDataModel.ROTATIONAL_NEW_DATA_EVENT.equals(event.getSource())) {
			RotationalFile rf = model.getRotationalDataModel().getData();
			model.getMoleculeTabbedPaneModel().removeAllComponents();
			for (RotationalMolecule rotMol : rf.getMoleculeList()) {
				RotationalMoleculeConf mc = new RotationalMoleculeConf(rotMol.getTag());
				for (RotationalComponent rc : rotMol.getComponentsList()) {
					List<RotationalSelectionData> allowedData;
					if (rotMol.isOldVersion()) {
						allowedData = new ArrayList<>(1);
						allowedData.add(RotationalSelectionData.GAUSSIAN_TYPE_DATA);
					} else {
						allowedData = new ArrayList<>(2);
						if (!model.getRotationalDataModel().getData().isAllFluxFirstMomentToZero()) {
							allowedData.add(RotationalSelectionData.FIRST_TYPE_DATA);
						}
						allowedData.add(rc.getDataType());
					}
					RotationalComp rco = new RotationalComp(rc.getNumCompo());
					rco.setListDataAllowed(allowedData);
					if (allowedData.contains(RotationalSelectionData.GAUSSIAN_TYPE_DATA)){
						rco.setDataSelected(RotationalSelectionData.GAUSSIAN_TYPE_DATA);
					} else {
						rco.setDataSelected(allowedData.get(0));
					}
					rco.setBeamDilutionPossible(rf.isBeamAllowed(rc.getTag(), rc.getNumCompo()));
					rco.setSourceSize(rc.getSourceSize());
					mc.addComponent(rco);
				}
				model.getMoleculeTabbedPaneModel().addComponent(mc);
			}
			if (rf.isTmb()) {
				model.getRotationalTuningModel().setTypeTemperature(TYPE_TEMPERATURE.TMB);
				model.getRotationalTuningModel().setTaTmbConv(true);
			} else {
				model.getRotationalTuningModel().setTypeTemperature(TYPE_TEMPERATURE.TA);
			}
			if (view != null) {
				view.repack();
			}
		}
	}

	/**
	 * @see eu.omp.irap.cassis.common.ISpectrumComputation#invokeSpectrumComputation()
	 */
	@Override
	public void invokeSpectrumComputation() {
		result = null;
		Server server = new ServerImpl();

		if (model.getRotationalDataModel().getData() == null) {
			if (view != null) {
				JOptionPane.showMessageDialog(view, "No data selected",
						"Warning", JOptionPane.WARNING_MESSAGE);
			}
		} else {
			RotationalDiagramInput input = new RotationalDiagramInput(
					model.getRotationalDataModel().getNameData(),
					model.getRotationalDataModel().getData().getMoleculeList(),
					model.getMoleculeTabbedPaneModel().getComponentList(),
					model.getRotationalTuningModel().getTypeTemperature(),
					model.getRotationalTuningModel().isTaTmbConv());
			try {
				result = server.computeRotationalDiagram(input);
			} catch (UnknowMoleculeException ume) {
				LOGGER.error("A molecule (tag: {}) is unknown, stopping the operation", ume.getTag(), ume);
				if (view != null) {
					JOptionPane.showMessageDialog(view, ume.getInterruptedMessage(),
						"Unknow molecule", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	/**
	 * @see eu.omp.irap.cassis.common.ISpectrumComputation#display()
	 */
	@Override
	public void display() {
		if (result == null || result.getResult().isEmpty()) {
			if (view != null) {
				JOptionPane.showMessageDialog(view, "No result found",
						"Warning", JOptionPane.WARNING_MESSAGE);
			}
			return;
		}
		fireDisplayEvent(result);
	}

	/**
	 * Compute the rotational diagram.
	 */
	public void computeRotationalDiagram() {
		try {
			File file = new File(Software.getConfigPath() + File.separatorChar
					+ "rotational-model" + File.separatorChar + "last.rdm");
			if (!file.getParentFile().exists()) {
				file.getParentFile().mkdirs();
			}
			model.saveConfig(file);
		} catch (IOException e) {
			LOGGER.warn("Error while saving the configuration", e);
		}

		if (Software.getUserConfiguration().isTestMode()) {
			invokeSpectrumComputation();
			display();
		} else {
			if (view != null) {
				new ProgressDialog(view, this);
			}
		}
	}

	/**
	 * Add a {@link RotationalDiagramDisplayListener}.
	 *
	 * @param listener The listener to add.
	 */
	public void addDisplayListener(RotationalDiagramDisplayListener listener) {
		listeners.add(listener);
	}

	/**
	 * Remove a {@link RotationalDiagramDisplayListener}.
	 *
	 * @param listener The listener to remove.
	 */
	public void removeDisplayListener(RotationalDiagramDisplayListener listener) {
		listeners.remove(listener);
	}

	/**
	 * Fire a display event.
	 *
	 * @param res The result to display.
	 */
	public void fireDisplayEvent(RotationalDiagramResult res) {
		for (RotationalDiagramDisplayListener listener : listeners) {
			listener.rotationalDisplayClicked(res);
		}
	}

	/**
	 * Return the result.
	 *
	 * @return the result.
	 */
	public RotationalDiagramResult getResult() {
		return result;
	}
}
