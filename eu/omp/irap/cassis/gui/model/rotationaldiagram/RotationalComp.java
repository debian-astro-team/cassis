/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.rotationaldiagram;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import eu.omp.irap.cassis.common.BufferedWriterProperty;
import eu.omp.irap.cassis.common.events.DataModel;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.parameters.RotationalSelectionData;

/**
 * Class describing a Rotataional component configuration.
 *
 * @author M. Boiziot
 */
public class RotationalComp extends DataModel {

	public static final String BEAM_DILUTION_EVENT = "beamDilution";
	public static final String BEAM_DILUTION_POSSIBLE_EVENT = "beamDilutionPossible";
	public static final String SOURCE_SIZE_EVENT = "sourceSize";
	public static final String ENABLED_EVENT = "enabledComp";
	public static final String DATA_TYPE_EVENT = "dataTypeChange";
	private static final String NUMBER_SAVE = "number";
	private static final String DATA_SELECTED_SAVE = "dataSelected";
	private static final String DATA_ALLOWED_SAVE = "dataAllowed";
	private static final String SIZE_DATA_ALLOWED_SAVE = "sizeDataAllowed";
	private static final String ENABLED_SAVE = "enabled";
	private static final String BEAM_DILUTION_SAVE = "beamDilution";
	private static final String BEAM_DILUTION_POSSIBLE_SAVE = "beamDilutionPossible";
	private static final String SOURCE_SIZE_SAVE = "sourceSize";
	private List<RotationalSelectionData> listDataAllowed;
	private RotationalSelectionData dataSelected;
	private int number;
	private boolean enabled;
	private String savePrefix;
	private boolean beamDilution;
	private boolean beamDilutionPossible;
	private double sourceSize;


	/**
	 * Constructor.
	 *
	 * @param number The number of the component.
	 */
	public RotationalComp(int number) {
		this.number = number;
		this.enabled = true;
		this.beamDilutionPossible = true;
	}

	/**
	 * Return the number of the component.
	 *
	 * @return The number of the component.
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * Return if the component is enabled.
	 *
	 * @return If the component is enabled.
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * Change the enabled state of the component.
	 *
	 * @param enabled The new enabled state of the component.
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
		fireDataChanged(new ModelChangedEvent(ENABLED_EVENT));
	}

	/**
	 * Return the {@link RotationalSelectionData} selected.
	 *
	 * @return The {@link RotationalSelectionData} selected.
	 */
	public RotationalSelectionData getDataSelected() {
		return dataSelected;
	}

	/**
	 * Change the {@link RotationalSelectionData} selected.
	 *
	 * @param dataSelected The new {@link RotationalSelectionData} selected.
	 */
	public void setDataSelected(RotationalSelectionData dataSelected) {
		this.dataSelected = dataSelected;
		fireDataChanged(new ModelChangedEvent(DATA_TYPE_EVENT));
	}

	/**
	 * Return the list of {@link RotationalSelectionData} allowed.
	 *
	 * @return The list of {@link RotationalSelectionData} allowed.
	 */
	public List<RotationalSelectionData> getListDataAllowed() {
		return listDataAllowed;
	}

	/**
	 * Set the new list of {@link RotationalSelectionData} allowed.
	 *
	 * @param listDataAllowed the list of {@link RotationalSelectionData} allowed.
	 */
	public void setListDataAllowed(List<RotationalSelectionData> listDataAllowed) {
		this.listDataAllowed = listDataAllowed;
	}

	/**
	 * Change the prefix needed for load/save.
	 *
	 * @param savePrefix The prefix to set.
	 */
	public void setSavePrefix(String savePrefix) {
		this.savePrefix = savePrefix;
	}

	/**
	 * @see eu.omp.irap.cassis.common.events.DataModel#saveConfig(eu.omp.irap.cassis.common.BufferedWriterProperty)
	 */
	@Override
	public void saveConfig(BufferedWriterProperty out) throws IOException {
		out.writeProperty(savePrefix + NUMBER_SAVE, String.valueOf(number));
		out.writeProperty(savePrefix + ENABLED_SAVE, String.valueOf(enabled));
		out.writeProperty(savePrefix + DATA_SELECTED_SAVE, dataSelected.name());

		out.writeProperty(savePrefix + SIZE_DATA_ALLOWED_SAVE, String.valueOf(listDataAllowed.size()));
		for (int i = 1; i <= listDataAllowed.size(); i++) {
			out.writeProperty(savePrefix + DATA_ALLOWED_SAVE + i, listDataAllowed.get(i-1).name());
		}
		out.writeProperty(savePrefix + BEAM_DILUTION_SAVE, String.valueOf(beamDilution));
		out.writeProperty(savePrefix + BEAM_DILUTION_POSSIBLE_SAVE, String.valueOf(beamDilutionPossible));
		out.writeProperty(savePrefix + SOURCE_SIZE_SAVE, String.valueOf(sourceSize));
	}

	/**
	 * @see eu.omp.irap.cassis.common.events.DataModel#loadConfig(java.util.Properties)
	 */
	@Override
	public void loadConfig(Properties prop) throws IOException {
		number = Integer.parseInt(prop.getProperty(savePrefix + NUMBER_SAVE, "-1"));
		setEnabled(Boolean.parseBoolean(prop.getProperty(savePrefix + ENABLED_SAVE, "true")));
		int sizeCompAllowed = Integer.parseInt(prop.getProperty(
				savePrefix + SIZE_DATA_ALLOWED_SAVE, "1"));
		List<RotationalSelectionData> allowedData = new ArrayList<>(sizeCompAllowed);
		for (int i = 1; i <= sizeCompAllowed; i++) {
			String ad = prop.getProperty(savePrefix + DATA_ALLOWED_SAVE + i);
			if (ad != null) {
				allowedData.add(RotationalSelectionData.valueOf(ad));
			}
		}
		setListDataAllowed(allowedData);
		setDataSelected(RotationalSelectionData.valueOf(
				prop.getProperty(savePrefix + DATA_SELECTED_SAVE,
						allowedData.get(0).name())));
		setBeamDilution(Boolean.parseBoolean(prop.getProperty(savePrefix + BEAM_DILUTION_SAVE, "false")));
		setBeamDilutionPossible(Boolean.parseBoolean(prop.getProperty(savePrefix + BEAM_DILUTION_POSSIBLE_SAVE, "true")));
		setSourceSize(Double.parseDouble(prop.getProperty(savePrefix + SOURCE_SIZE_SAVE, "0.0")));
	}

	/**
	 * Return if the beam dilution is enabled.
	 *
	 * @return if the beam dilution is enabled.
	 */
	public boolean isBeamDilution() {
		return beamDilution;
	}

	/**
	 * Change the state of the beam dilution.
	 *
	 * @param beamDilution The new state of the beam dilution.
	 */
	public void setBeamDilution(boolean beamDilution) {
		this.beamDilution = beamDilution;
		fireDataChanged(new ModelChangedEvent(BEAM_DILUTION_EVENT));
	}

	/**
	 * Return if the beam dilution is possible.
	 *
	 * @return if the beam dilution is possible.
	 */
	public boolean isBeamDilutionPossible() {
		return beamDilutionPossible;
	}

	/**
	 * Change the possibility the beam dilution.
	 *
	 * @param beamDilutionPossible The new possibility of the beam dilution.
	 */
	public void setBeamDilutionPossible(boolean beamDilutionPossible) {
		this.beamDilutionPossible = beamDilutionPossible;
		fireDataChanged(new ModelChangedEvent(BEAM_DILUTION_POSSIBLE_EVENT));
	}

	/**
	 * Return the source size.
	 *
	 * @return the source size.
	 */
	public double getSourceSize() {
		return sourceSize;
	}

	/**
	 * Change the source size value.
	 *
	 * @param sourceSize The new source size value.
	 */
	public void setSourceSize(double sourceSize) {
		this.sourceSize = sourceSize;
		fireDataChanged(new ModelChangedEvent(SOURCE_SIZE_EVENT));
	}
}
