/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.rotationaldiagram;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import eu.omp.irap.cassis.gui.model.parameter.rotationaldata.RotationalDataPanel;
import eu.omp.irap.cassis.gui.model.parameter.rotationaltuning.RotationalTuningPanel;
import eu.omp.irap.cassis.properties.Software;

/**
 * View of the RotationalDiagram module.
 *
 * @author M. Boiziot
 */
public class RotationalDiagramPanel extends JPanel {

	private static final long serialVersionUID = -5176600426486916240L;
	private RotationalDiagramControl control;
	private RotationalDataPanel rotationalDataPanel;
	private RotationalTuningPanel rotationalTuningPanel;
	private MoleculeTabbedPaneView moleculeTabbedPaneView;
	private JButton loadConfigButton;
	private JButton displayButton;
	private JButton saveConfigButton;
	private JFrame frame;


	/**
	 * Constructor.
	 */
	public RotationalDiagramPanel() {
		this(RotationalDiagramModel.createModelWithFakeData());
	}

	/**
	 * Constructor.
	 *
	 * @param model The model
	 */
	public RotationalDiagramPanel(RotationalDiagramModel model) {
		super(new BorderLayout());
		this.control = new RotationalDiagramControl(model, this);

		JPanel northPanel = new JPanel();
		northPanel.setLayout(new BorderLayout());
		northPanel.add(getRotationalDataPanel(), BorderLayout.WEST);
		northPanel.add(getRotationalTuningPanel(), BorderLayout.EAST);

		JPanel northWestPanel = new JPanel();
		northWestPanel.setLayout(new BoxLayout(northWestPanel, BoxLayout.Y_AXIS));
		northWestPanel.add(northPanel);
		northWestPanel.add(getMoleculeTabbedPaneView());
		add(northWestPanel, BorderLayout.CENTER);

		JPanel buttonsPanel = new JPanel(new BorderLayout());
		buttonsPanel.add(getLoadConfigButton(), BorderLayout.NORTH);
		buttonsPanel.add(getDisplayButton(), BorderLayout.CENTER);
		buttonsPanel.add(getSaveConfigButton(), BorderLayout.SOUTH);
		buttonsPanel.setBorder(BorderFactory.createEmptyBorder(5, 2, 5, 2));
		add(buttonsPanel, BorderLayout.EAST);
	}


	/**
	 * Create if needed then return the {@link RotationalDataPanel}.
	 *
	 * @return the {@link RotationalDataPanel}.
	 */
	public RotationalDataPanel getRotationalDataPanel() {
		if (rotationalDataPanel == null) {
			rotationalDataPanel = new RotationalDataPanel(
					control.getModel().getRotationalDataModel());
		}
		return rotationalDataPanel;
	}

	/**
	 * Create if needed then return the {@link RotationalTuningPanel}.
	 *
	 * @return the {@link RotationalTuningPanel}.
	 */
	public RotationalTuningPanel getRotationalTuningPanel() {
		if (rotationalTuningPanel == null) {
			rotationalTuningPanel = new RotationalTuningPanel(
					control.getModel().getRotationalTuningModel());
		}
		return rotationalTuningPanel;
	}

	/**
	 * Create if needed then return the {@link MoleculeTabbedPaneView}.
	 *
	 * @return the {@link MoleculeTabbedPaneView}.
	 */
	public MoleculeTabbedPaneView getMoleculeTabbedPaneView() {
		if (moleculeTabbedPaneView == null) {
			moleculeTabbedPaneView = new MoleculeTabbedPaneView(
					control.getModel().getMoleculeTabbedPaneModel());
		}
		return moleculeTabbedPaneView;
	}

	/**
	 * Create if needed then return the "Load config" {@link JButton}.
	 *
	 * @return the "Load config" {@link JButton}.
	 */
	public JButton getLoadConfigButton() {
		if (loadConfigButton == null) {
			loadConfigButton = new JButton("Load config");
			loadConfigButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.loadConfig();
				}
			});
			loadConfigButton.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (SwingUtilities.isRightMouseButton(e) &&
							Software.getUserConfiguration().isTestMode()) {
						control.loadLastConfig();
					}
				}
			});
		}
		return loadConfigButton;
	}

	/**
	 * Create if needed then return the "Display" {@link JButton}.
	 *
	 * @return the "Display" {@link JButton}.
	 */
	public JButton getDisplayButton() {
		if (displayButton == null) {
			displayButton = new JButton("Display");
			displayButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					control.computeRotationalDiagram();
				}
			});
		}
		return displayButton;
	}

	/**
	 * Create if needed then return the "Save config" {@link JButton}.
	 *
	 * @return the "Save config" {@link JButton}.
	 */
	public JButton getSaveConfigButton() {
		if (saveConfigButton == null) {
			saveConfigButton = new JButton("Save config");
			saveConfigButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					control.saveConfig();
				}
			});
		}
		return saveConfigButton;
	}

	/**
	 * Refresh the view.
	 */
	public void refresh() {
		RotationalDiagramModel model = control.getModel();
		rotationalDataPanel.setModel(model.getRotationalDataModel());
		moleculeTabbedPaneView.getControl().setModel(model.getMoleculeTabbedPaneModel());
		revalidate();
		repaint();
	}

	/**
	 * Return the controller.
	 *
	 * @return the controller.
	 */
	public RotationalDiagramControl getControl() {
		return control;
	}

	/**
	 * Set the {@link JFrame} contening this panel.
	 *
	 * @param frame The container.
	 */
	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	/**
	 * Repack the {@link JFrame} if one was declared.
	 */
	public void repack() {
		if (this.frame != null) {
			frame.pack();
		}
	}

}
