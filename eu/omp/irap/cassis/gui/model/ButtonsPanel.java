/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model;

import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class ButtonsPanel extends JPanel {

	private JButton loadConfigButton;
	private JButton displayButton;
	private JButton saveConfigButton;

	public ButtonsPanel() {
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		add(getLoadConfigButton());
		add(getDisplayButton());
		add(getSaveConfigButton());
		setBorder(BorderFactory.createEmptyBorder(8, 1, 1, 2));
	}

	public JButton getDisplayButton() {
		if (displayButton == null) {
			displayButton = new JButton("Display");
			displayButton.setMinimumSize(new Dimension(100, 100));
			displayButton.setPreferredSize(new Dimension(100, 100));
			displayButton.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		}
		return displayButton;
	}

	public JButton getLoadConfigButton() {
		if (loadConfigButton == null) {
			loadConfigButton = new JButton("Load config");
			loadConfigButton.setMinimumSize(new Dimension(100, 16));
			loadConfigButton.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		}
		return loadConfigButton;
	}

	public JButton getSaveConfigButton() {
		if (saveConfigButton == null) {
			saveConfigButton = new JButton("Save config");
			saveConfigButton.setMinimumSize(new Dimension(100, 16));
			saveConfigButton.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		}
		return saveConfigButton;
	}

}
