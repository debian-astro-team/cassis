/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.loomisanalysis;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.gui.model.parameter.continuum.ContinuumPanel;
import eu.omp.irap.cassis.gui.model.table.JCassisTable;
import eu.omp.irap.cassis.gui.template.JComboBoxTemplate;
import eu.omp.irap.cassis.gui.util.JDoubleCassisTextField;

public class LoomisWoodParameterPanel extends JPanel {

	private static final long serialVersionUID = 6069123565887927179L;
	private LoomisWoodParameterControl control;
	private JCassisTable<MoleculeDescription> moleculesTable;
	private JPanel northPanel;
	private JDoubleCassisTextField vlsrTextField;

	private JComboBoxTemplate comboBoxTemplate;
	private ContinuumPanel continuumPanel;
	private JComboBox<String> comboBoxMode;


	public LoomisWoodParameterPanel() {
		this(new LoomisWoodParameterControl(new LoomisWoodParameterModel()));
		setSize(500, 200);
	}

	/**
	 * Constructor.
	 *
	 * @param control The controller.
	 */
	public LoomisWoodParameterPanel(final LoomisWoodParameterControl control) {
		this.control = control;
		control.setView(this);
		setBorder(new TitledBorder(""));
		setLayout(new BorderLayout(5, 5));
		add(getNorthPanel(), BorderLayout.NORTH);

		JScrollPane scrollPane = new JScrollPane(getMoleculesTable());
		add(scrollPane, BorderLayout.CENTER);
		getMoleculesTable().setScrollPane(scrollPane);
		getMoleculesTable().setTemplateCombo(getComboBoxTemplate());
		setPreferredSize(new Dimension(300, 300));
	}

	private JPanel getNorthPanel() {
		if (northPanel == null) {
			northPanel = new JPanel();
			comboBoxTemplate = new JComboBoxTemplate(true, true);

			northPanel.add(comboBoxTemplate);
			continuumPanel = new ContinuumPanel(control.getModel().getContinuumModel());
			continuumPanel.setTypeName("loomis-analysis-continuum");
			northPanel.add(continuumPanel);
			JPanel panel = new JPanel();
			vlsrTextField = new JDoubleCassisTextField();
			vlsrTextField.setHorizontalAlignment(JTextField.RIGHT);
			vlsrTextField.setColumns(7);
			vlsrTextField.setValue(Double.valueOf(0.0));
			vlsrTextField.addPropertyChangeListener("value", new PropertyChangeListener() {

				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					control.vlsrChanged(((Number) vlsrTextField.getValue()).doubleValue());
				}
			});

			JLabel vlsrLabel = new JLabel(
					"<HTML><font color='black'><p>&nbsp; &nbsp; V<sub>lsr</sub> [km/s] : </p></font></HTML>");
			panel.add(vlsrLabel);
			panel.add(vlsrTextField);
			northPanel.add(panel);

			JPanel modePanel = new JPanel();
			comboBoxMode = new JComboBox<>(new String[] { "Emission", "Absorption" });
			comboBoxMode.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					getControl().getModel().setMode(comboBoxMode.getSelectedItem().toString());
				}
			});
			modePanel.add(comboBoxMode);
			northPanel.add(modePanel);
		}
		return northPanel;
	}

	/**
	 * @return the continuumPanel
	 */
	public final ContinuumPanel getContinuumPanel() {
		return continuumPanel;
	}

	public JCassisTable<MoleculeDescription> getMoleculesTable() {
		if (moleculesTable == null) {
			moleculesTable = new JCassisTable<>(
					control.getModel().getCassisTableMoleculeModel(), true, false);
		}
		return moleculesTable;
	}

	/**
	 * @return the vlsrTextField
	 */
	public final JFormattedTextField getVlsrTextField() {
		return vlsrTextField;
	}

	/**
	 * @return the comboBoxTemplate
	 */
	public final JComboBoxTemplate getComboBoxTemplate() {
		return comboBoxTemplate;
	}

	/**
	 * @return the control
	 */
	public final LoomisWoodParameterControl getControl() {
		return control;
	}

	public JComboBox<String> getComboBoxMode() {
		return comboBoxMode;
	}

	/**
	 * Refresh the view.
	 */
	public void refresh() {
		LoomisWoodParameterModel lwpm = control.getModel();

		comboBoxTemplate.setSelectedItem(lwpm.getTemplate());
		continuumPanel.setModel(lwpm.getContinuumModel());
		vlsrTextField.setValue(lwpm.getVlsr());
		comboBoxMode.setSelectedItem(lwpm.getMode());
		moleculesTable.setModel(lwpm.getCassisTableMoleculeModel());
	}

}
