/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.loomisanalysis;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.CellEditor;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.event.EventListenerList;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.cassisd.model.LoomisModel;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.ISpectrumComputation;
import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.common.Telescope;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.database.access.UnknowMoleculeException;
import eu.omp.irap.cassis.gui.model.parameter.data.LoadDataModel;
import eu.omp.irap.cassis.gui.model.parameter.threshold.ThresholdModel;
import eu.omp.irap.cassis.gui.optionpane.ProgressDialog;
import eu.omp.irap.cassis.gui.util.Deletable;
import eu.omp.irap.cassis.properties.Software;

public class LoomisWoodControl implements ModelListener, ISpectrumComputation, Deletable {

	private static final Logger LOGGER = LoggerFactory.getLogger(LoomisWoodControl.class);

	private ActionListener displayActionListener;
	private final LoomisWoodPanel view;

	private LoomisWoodModel model;
	private LoomisWoodResult loomisWoodResult;

	private EventListenerList listeners;


	public LoomisWoodControl(LoomisWoodPanel loomisWoodPanel, LoomisWoodModel loomisWoodModel) {
		this.view = loomisWoodPanel;
		this.model = loomisWoodModel;
		model.addModelListener(this);
		model.getLoadDataModel().addModelListener(this);
		this.listeners = new EventListenerList();
	}

	public ActionListener getDisplayActionListener() {
		if (displayActionListener == null) {
			displayActionListener = new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					onDisplayClick();
				}
			};
		}
		return displayActionListener;
	}

	private void onDisplayClick() {
		try {
			File file = new File(Software.getConfigPath() +
					File.separator + "loomis-analysis"+File.separator + "last.lom");
			if (!file.getParentFile().exists()) {
				file.getParentFile().mkdirs();
			}
			model.saveConfig(file);
		} catch (Exception e) {
			LOGGER.warn("Error while saving the configuration", e);
		}
		CellEditor c = view.getLoomisParametersPanel().getMoleculesTable().getCellEditor();
		if (c != null)
			c.stopCellEditing();

		if (Software.getUserConfiguration().isTestMode()) {
			invokeSpectrumComputation();
			display();
		}
		else {
			new ProgressDialog(view, this);
		}
	}

	@Override
	public void invokeSpectrumComputation() {
		loomisWoodResult = null;
		List<MoleculeDescription> moles = model.getWoodParameterModel().getSelectedListMolecules();
		Double[] vals = model.getTuningModel().getValuesInMHz();
		LoomisModelParameters parameters = new LoomisModelParameters(
				vals[0], vals[1], vals[2], model.getCassisSpectrum(),
				model.getTresholdmodel(),
				moles,
				model.getWoodParameterModel().getVlsr(),
				model.getWoodParameterModel().getContinuumModel().getContinuumParams(),
				model.getLoadDataModel().getTelescope());

		LoomisModel loomisModel = new LoomisModel(parameters);
		try {
			loomisWoodResult = loomisModel.compute();

			if (!loomisWoodResult.isHaveLines() && !moles.isEmpty())
				JOptionPane.showMessageDialog(view, "No line found with this threshold!",
						"Warning", JOptionPane.WARNING_MESSAGE);

			loomisWoodResult.setId(model.getId());
			loomisWoodResult.setMode(model.getWoodParameterModel().getMode());
		} catch (UnknowMoleculeException ume) {
			LOGGER.error("A molecule (tag: {}) is unknown, stopping the operation", ume.getTag(), ume);
			JOptionPane.showMessageDialog(view, ume.getInterruptedMessage(),
					"Unknow molecule", JOptionPane.ERROR_MESSAGE);
		}
	}

	public void addLoomisListener(LoomisWoodListener listener) {
		listeners.add(LoomisWoodListener.class, listener);
	}

	public void removeLoomisListener(LoomisWoodListener l) {
		listeners.remove(LoomisWoodListener.class, l);
	}

	private void fireLoomisModelCompute(LoomisWoodResult loomisWoodResult) {
		LoomisWoodListener[] listenerList = listeners.getListeners(LoomisWoodListener.class);

		for (LoomisWoodListener listener : listenerList) {
			listener.setLoomisModelResult(loomisWoodResult);
		}
	}

	public void loadModel() {
		JFileChooser chooser = new CassisJFileChooser(Software.getLoomisAnalysisConfigPath(),
				Software.getLastFolder("loomis-analysis-config"));
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Loomis Model files (*.lom)", "lom");
		chooser.setFileFilter(filter);

		int returnVal = chooser.showOpenDialog(view);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			Software.setLastFolder("loomis-analysis-config", chooser.getSelectedFile().getParent());
			try {
				model.loadConfig(chooser.getSelectedFile());
			} catch (IOException e) {
				LOGGER.error("Unable to read config file", e);
			}
		}
	}

	public void saveModel() {
		JFileChooser chooser = new CassisJFileChooser(Software.getLoomisAnalysisConfigPath(),
				Software.getLastFolder("loomis-analysis-config"));
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Loomis Model files (*.lom)", "lom");
		chooser.setFileFilter(filter);
		int returnVal = chooser.showSaveDialog(view);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			try {
				File selectedFile = chooser.getSelectedFile();

				if (!selectedFile.getPath().endsWith(".lom"))
					selectedFile = new File(selectedFile.getPath() + ".lom");

				if (selectedFile.exists()) {
					String message = "Configuration " + selectedFile.getName() + " already exists.\n"
							+ "Do you want to replace it?";
					// Modal dialog with yes/no button
					int answer = JOptionPane.showConfirmDialog(view, message, "Replace existing configuration file?",
							JOptionPane.YES_NO_OPTION);
					if (answer == JOptionPane.NO_OPTION)
						selectedFile = null;
				}

				if (selectedFile != null) {
					model.saveConfig(selectedFile);
					Software.setLastFolder("loomis-analysis-config", selectedFile.getParent());
				}
			} catch (IOException e) {
				LOGGER.error("Unable to save configuration.", e);
				JOptionPane.showMessageDialog(view, "Unable to save configuration: ", "Alert",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	/**
	 * @return the view
	 */
	public final LoomisWoodPanel getView() {
		return view;
	}

	/**
	 * @return the model
	 */
	public final LoomisWoodModel getModel() {
		return model;
	}

	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (LoadDataModel.CASSIS_SPECTRUM_EVENT.equals(event.getSource())) {
			handleCassisSpectrumEvent();
		} else if (LoadDataModel.TELESCOPE_DATA_EVENT.equals(event.getSource())) {
			String telescopeName = event.getValue() != null ?
					Telescope.getNameStatic((String)event.getValue()) : null;
			handleTelescopeDataEvent(telescopeName);
		} else if (LoomisWoodModel.LOAD_CONFIG_ERROR_EVENT.equals(event.getSource())) {
			view.displayLoadConfigError(event.getValue().toString());
		}
	}

	/**
	 * Handle LoadDataModel.TELESCOPE_DATA_EVENT.
	 *
	 * @param telescopeName The name of the new telescope.
	 */
	private void handleTelescopeDataEvent(String telescopeName) {
		if ("spire".equalsIgnoreCase(telescopeName)) {
			model.getTresholdmodel().setThresEupMax(ThresholdModel.SPIRE_EUP_MAX_K);
		} else if ("pacs".equalsIgnoreCase(telescopeName)) {
			model.getTresholdmodel().setThresEupMax(ThresholdModel.PACS_EUP_MAX_K);
		} else {
			model.getTresholdmodel().setThresEupMax(ThresholdModel.DEFAULT_EUP_MAX_K);
		}
	}

	/**
	 * Handle LoadDataModel.CASSIS_SPECTRUM_EVENT: update unit and TunningModel.
	 */
	private void handleCassisSpectrumEvent() {
		CassisSpectrum cassisSpectrum = model.getLoadDataModel().getCassisSpectrum();
		XAxisCassis axisCassis = cassisSpectrum.getxAxisOrigin();
		if (UNIT.MHZ.equals(axisCassis.getUnit())&& cassisSpectrum.getFreqMin() > 1000)
			axisCassis.setUnit(UNIT.GHZ);
		Double val1 = axisCassis.convertFromMhzFreq(cassisSpectrum.getFreqMin());
		Double val2 = axisCassis.convertFromMhzFreq(cassisSpectrum.getFreqMax());
		Double valMin = Math.min(val1, val2);
		Double valMax = Math.max(val1, val2);
		model.getTuningModel().setMinValue(valMin);
		model.getTuningModel().setMaxValue(valMax);
		model.getTuningModel().setBandValue(valMax - valMin);
		model.getTuningModel().setValUnit(axisCassis.getUnit());
		model.getTuningModel().setBandUnit(axisCassis.getUnit());
	}

	@Override
	public void display() {
		if (loomisWoodResult != null) {
			fireLoomisModelCompute(loomisWoodResult);
		}
	}

	/**
	 * Set a new model.
	 *
	 * @param newLoomisWoodModel The new LoomisWoodModel to use.
	 */
	public void setModel(LoomisWoodModel newLoomisWoodModel) {
		removeListeners();
		model = newLoomisWoodModel;
		model.addModelListener(this);
		model.getLoadDataModel().addModelListener(this);
		view.refresh();
	}

	public void loadConfigButtonClicked(MouseEvent e) {
		final String pathname = Software.getConfigPath() + File.separator
				+ "loomis-analysis" + File.separator + "last.lom";

		if (SwingUtilities.isRightMouseButton(e) && Software.getUserConfiguration().isTestMode()
				&& new File(pathname).exists())
			try {
				getModel().loadConfig(new File(pathname));
			} catch (IOException ioe) {
				LOGGER.error("Unable to read config file", ioe);
			}
	}

	/**
	 * Remove the listeners.
	 */
	private void removeListeners() {
		model.removeModelListener(this);
		model.getLoadDataModel().removeModelListener(this);
	}

	/**
	 * @see eu.omp.irap.cassis.gui.util.Deletable#delete()
	 */
	@Override
	public void delete() {
		removeListeners();
		view.getLoomisParametersPanel().getControl().delete();
	}
}
