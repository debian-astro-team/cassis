/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.loomisanalysis;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import eu.omp.irap.cassis.common.BufferedWriterProperty;
import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.common.events.DataModel;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.gui.model.parameter.continuum.ContinuumModel;
import eu.omp.irap.cassis.gui.model.table.CassisTableMolecule;
import eu.omp.irap.cassis.gui.model.table.CassisTableMoleculeModel;

public class LoomisWoodParameterModel extends DataModel {

	public static final String MODE_EVENT = "mode";
	public static final String TEMPLATE_EVENT = "template";
	public static final String VLSR_EVENT = "vlsr";

	private Double vlsr = 0.;
	private String template;
	private ContinuumModel continuumModel;
	private String mode = "emm";
	private CassisTableMoleculeModel cassisTableMoleculeModel;


	public LoomisWoodParameterModel() {
		continuumModel = new ContinuumModel();
		cassisTableMoleculeModel = new CassisTableMoleculeModel(null, new String[] {
				"Species", "Tag", "Database", "Compute", "N(Sp) (/cm2)", "Tex(K)" }, new Integer[] {
				CassisTableMolecule.NAME_INDICE, CassisTableMolecule.TAG_INDICE,
				CassisTableMolecule.DATA_SOURCE_INDICE, CassisTableMolecule.COMPUTE_INDICE,
				CassisTableMolecule.DENSITY_INDICE, CassisTableMolecule.TEMPERATURE_INDICE });
	}

	/**
	 * Return the list of molecules.
	 *
	 * @return the list of molecules.
	 */
	public final List<MoleculeDescription> getListMolecules() {
		return getCassisTableMoleculeModel().getList();
	}

	/**
	 * Return the list of selected molecules.
	 *
	 * @return the list of selected molecules.
	 */
	public final List<MoleculeDescription> getSelectedListMolecules() {
		return getCassisTableMoleculeModel().getSelectedMolecules();
	}

	public void setVlsr(Double value) {
		vlsr = value;
		fireDataChanged(new ModelChangedEvent(VLSR_EVENT, value));
	}

	/**
	 * @return the vlsr
	 */
	public final Double getVlsr() {
		return vlsr;
	}

	@Override
	public void saveConfig(BufferedWriterProperty out) throws IOException {
		out.writeProperty("vlsr", String.valueOf(vlsr));
		out.writeProperty("continuum", continuumModel.getSelectedContinuum());
		out.writeProperty("template", this.template);
		List<MoleculeDescription> listMolecules = getListMolecules();
		out.writeProperty("nbMol", String.valueOf(listMolecules.size()));
		for (int cpt = 0; cpt < listMolecules.size(); cpt++) {
			MoleculeDescription mol = listMolecules.get(cpt);
			out.writeProperty("mol" + cpt + "Name", mol.getName());
			out.writeProperty("mol" + cpt + "Tag", String.valueOf(mol.getTag()));
			out.writeProperty("mol" + cpt + "Source", mol.getDataSource());
			out.writeProperty("mol" + cpt + "Density", String.valueOf(mol.getDensity()));
			out.writeProperty("mol" + cpt + "Compute", String.valueOf(mol.isCompute()));
			out.writeProperty("mol" + cpt + "Tex", String.valueOf(mol.getTemperature()));
		}
	}

	public ContinuumModel getContinuumModel() {
		return continuumModel;
	}

	@Override
	public void loadConfig(Properties prop) {
		setVlsr(Double.valueOf(prop.getProperty("vlsr")));
		getContinuumModel().setSelectedContinuum(prop.getProperty("continuum"));
		setTemplate(prop.getProperty("template"));
		int nbMol = Integer.parseInt(prop.getProperty("nbMol"));
		List<MoleculeDescription> listMolecules = new ArrayList<>(nbMol);
		for (int cpt = 0; cpt < nbMol; cpt++) {
			MoleculeDescription mol = new MoleculeDescription();
			mol.setName(prop.getProperty("mol" + cpt + "Name"));
			mol.setTag(Integer.valueOf(prop.getProperty("mol" + cpt + "Tag")));
			mol.setDataSource(prop.getProperty("mol" + cpt + "Source"));
			mol.setDensity(Double.valueOf(prop.getProperty("mol" + cpt + "Density")));
			mol.setCompute(Boolean.valueOf(prop.getProperty("mol" + cpt + "Compute")));
			mol.setTemperature(Double.valueOf(prop.getProperty("mol" + cpt + "Tex")));
			if (mol.isCompute())
				listMolecules.add(mol);
		}
		getCassisTableMoleculeModel().setList(listMolecules);
	}

	/**
	 * @return the template
	 */
	public final String getTemplate() {
		return template;
	}

	/**
	 * @param template
	 *            the template to set
	 */
	public final void setTemplate(String template) {
		this.template = template;
		fireDataChanged(new ModelChangedEvent(TEMPLATE_EVENT, template));
	}

	/**
	 * @param mode
	 *            the mode to set
	 */
	public void setMode(String mode) {
		this.mode = mode;
		fireDataChanged(new ModelChangedEvent(MODE_EVENT, this.mode));
	}

	/**
	 * @return the mode
	 */
	public String getMode() {
		return mode;
	}

	public CassisTableMoleculeModel getCassisTableMoleculeModel() {
		return cassisTableMoleculeModel;
	}

}
