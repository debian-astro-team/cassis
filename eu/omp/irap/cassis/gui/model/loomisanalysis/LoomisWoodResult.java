/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.loomisanalysis;

import java.util.Arrays;
import java.util.List;

import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.gui.model.CassisResult;
import eu.omp.irap.cassis.gui.model.parameter.threshold.ThresholdModel;
import eu.omp.irap.cassis.gui.model.table.ModelIdentifiedInterface;
import eu.omp.irap.cassis.parameters.ContinuumParameters;

/**
 * @author glorian
 *
 */
public class LoomisWoodResult implements CassisResult{

	private final CommentedSpectrum[] commentedSpectrums;
	private final CommentedSpectrum[][] lineSpectrums;
	private ContinuumParameters continuum;
	private String mode;
	private int id = -1;
	private boolean haveLines;
	private String telescope;


	public LoomisWoodResult(CommentedSpectrum[] commentedSpectrums, CommentedSpectrum[][] lineSpectrums2,
			ContinuumParameters continuum, boolean haveLines1, String telescope) {
		this.commentedSpectrums = commentedSpectrums;
		this.lineSpectrums = lineSpectrums2;
		this.continuum = continuum;
		this.haveLines = haveLines1;
		this.telescope = telescope;
	}

	/**
	 * @return the commentedSpectrums
	 */
	public final CommentedSpectrum[] getCommentedSpectrums() {
		return commentedSpectrums;
	}

	/**
	 * @return the lineSpectrums
	 */
	public final CommentedSpectrum[][] getLineSpectrums() {
		return lineSpectrums;
	}


	public List<CommentedSpectrum> getSumSpectrumList() {
		return null;
	}

	public List<CommentedSpectrum> getFileSpectrumList() {
		return  Arrays.asList(commentedSpectrums);
	}

	/**
	 * @return the mode
	 */
	public final String getMode() {
		return mode;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public final int getId() {
		return id;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}
	public boolean isHaveLines() {
		return haveLines;
	}

	public ContinuumParameters getContinuum() {
		return continuum;
	}

	public String getTelescope() {
		return telescope;
	}

	public ModelIdentifiedInterface getModelIdentifiedInterface() {
		return new ModelIdentifiedInterface() {

			@Override
			public void setName(String name) {
				// Not used.
			}

			@Override
			public void setModelId(int modelId) {
				// Not used.
			}

			@Override
			public void setIdentified(boolean b) {
				// Not used.
			}

			@Override
			public boolean isIdentified() {
				return false;
			}

			@Override
			public XAxisCassis getXaxisAskToDisplay() {
				return null;
			}

			@Override
			public ThresholdModel getThresholdModel() {
				return new ThresholdModel();
			}

			@Override
			public String getName() {
				return null;
			}

			@Override
			public int getModelId() {
				return 0;
			}
		};
	}


	public XAxisCassis getAxisData(){
		return getFileSpectrumList().get(0).getxAxisOrigin();
	}

}
