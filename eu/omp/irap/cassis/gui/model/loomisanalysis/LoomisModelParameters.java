/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.loomisanalysis;

import java.util.List;

import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.gui.model.parameter.threshold.ThresholdModel;
import eu.omp.irap.cassis.parameters.ContinuumParameters;

/**
 * @author glorian
 *
 */
public class LoomisModelParameters {

	private double vlsr;
	private double freqRef;

	private ContinuumParameters continuumParameters;
	private String telescope;
	private CassisSpectrum cassisSpectrum;
	private List<MoleculeDescription> moles;
	private Double min;
	private Double max;
	private Double valBand;
	private ThresholdModel thresholdModel;


	public LoomisModelParameters(Double min, Double max, Double valBand,
			CassisSpectrum cassisSpectrum, ThresholdModel thresholdModel,
			List<MoleculeDescription> moles, Double vlsr,
			ContinuumParameters continuumParameters, String telescope) {
		this.min = min;
		this.max = max;
		this.valBand = valBand;
		this.cassisSpectrum = cassisSpectrum;
		this.thresholdModel = thresholdModel;
		this.vlsr = vlsr;
		this.continuumParameters = continuumParameters;
		this.telescope = telescope;
		this.moles = moles;
	}

	/**
	 * @return the vlsr
	 */
	public final double getVlsr() {
		return vlsr;
	}

	/**
	 * @return the freqRef
	 */
	public final double getFreqRef() {
		return freqRef;
	}

	public void setVlsr(Double vlsr) {
		this.vlsr = vlsr;
	}

	public void setFreqRef(Double freqRef) {
		this.freqRef = freqRef;
	}

	/**
	 * @return the continuumParameters
	 */
	public final ContinuumParameters getContinuumParameters() {
		return continuumParameters;
	}

	/**
	 * @return the telescope
	 */
	public final String getTelescope() {
		return telescope;
	}

	public CassisSpectrum getCassisSpectrum() {
		return cassisSpectrum;
	}

	public List<MoleculeDescription> getMoles() {
		return moles;
	}

	public Double getMin() {
		return min;
	}

	public Double getMax() {
		return max;
	}

	public Double getValBand() {
		return valBand;
	}

	public ThresholdModel getThresholdModel() {
		return thresholdModel;
	}
}
