/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.loomisanalysis;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import eu.omp.irap.cassis.common.BufferedWriterProperty;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.events.DataModel;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.gui.model.parameter.data.LoadDataModel;
import eu.omp.irap.cassis.gui.model.parameter.threshold.ThresholdModel;
import eu.omp.irap.cassis.gui.model.parameter.tuning.TuningModel;

public class LoomisWoodModel extends DataModel {

	public static final String LOAD_CONFIG_ERROR_EVENT = "loadConfigLwError";

	private ThresholdModel tresholdmodel;
	private TuningModel tunningModel;
	private static int idStatic = 0;
	private Integer id = -1;

	private LoomisWoodParameterModel woodParameterModel;
	private LoadDataModel loadDataModel;


	public LoomisWoodModel() {
		woodParameterModel = new LoomisWoodParameterModel();
		tresholdmodel = new ThresholdModel();
		tunningModel = new TuningModel(true, true, UNIT.GHZ);
		tunningModel.setBandUnit(UNIT.GHZ);
		tunningModel.setBandValue(0.0);
		loadDataModel = new LoadDataModel();
		id = idStatic++;
	}

	public void loadConfig(File selectedFile) throws IOException {
		try (FileInputStream fis = new FileInputStream(selectedFile)) {
			Properties prop = new Properties();
			prop.load(fis);
			loadConfig(prop);
		}
	}

	@Override
	public void loadConfig(Properties prop) throws IOException {
		loadDataModel.loadConfig(prop);

		tresholdmodel.loadConfig(prop);
		tunningModel.loadConfig(prop);

		handleLoadConfigErrors();

		if (prop.getProperty("telescope") != null) {
			getLoadDataModel().setTelescope(prop.getProperty("telescope"));
		}

		woodParameterModel.loadConfig(prop);
	}

	/**
	 * Handle load configuration errors if it is needed. It create a message,
	 *  fire an event and reset the errors.
	 */
	private void handleLoadConfigErrors() {
		if (loadDataModel.haveError()) {
			boolean dataError = loadDataModel.haveDataFileError();
			boolean telescopeError = loadDataModel.haveTelescopeError();
			StringBuilder sb = new StringBuilder();
			sb.append("<html>");
			if (dataError && telescopeError) {
				sb.append("Some errors were detected in the provided configuration file:<br>");
			} else {
				sb.append("An error was detected in the provided configuration file:<br>");
			}
			if (dataError) {
				sb.append(" - The data file (");
				sb.append(loadDataModel.getDataFileError());
				sb.append(") can not be found.<br>");
			}
			if (telescopeError) {
				sb.append(" - The telescope file (");
				sb.append(loadDataModel.getTelescopeError());
				sb.append(") can not be found.");
			}
			sb.append("</html>");
			loadDataModel.resetDataFileError();
			loadDataModel.resetTelescopeError();
			fireDataChanged(new ModelChangedEvent(LOAD_CONFIG_ERROR_EVENT, sb.toString()));
		}
	}

	public void saveConfig(File selectedFile) throws IOException {
		try (BufferedWriterProperty out =
				new BufferedWriterProperty(new FileWriter(selectedFile))) {
			saveConfig(out);
			out.flush();
		}
	}

	public CassisSpectrum getCassisSpectrum() {
		return getLoadDataModel().getCassisSpectrum();
	}

		/**
	 * @return the woodParameterModel
	 */
	public final LoomisWoodParameterModel getWoodParameterModel() {
		return woodParameterModel;
	}

	/**
	 * @return the tresholdmodel
	 */
	public final ThresholdModel getTresholdmodel() {
		return tresholdmodel;
	}

	/**
	 * @return the id
	 */
	public final Integer getId() {
		return id;
	}

	@Override
	public void saveConfig(BufferedWriterProperty out) throws IOException {
		getLoadDataModel().saveConfig(out);
		tresholdmodel.saveConfig(out);
		tunningModel.saveConfig(out);
		out.newLine();
		woodParameterModel.saveConfig(out);
	}

	/**
	 * @param loadDataModel the loadDataModel to set
	 */
	public void setLoadDataModel(LoadDataModel loadDataModel) {
		this.loadDataModel = loadDataModel;
	}

	/**
	 * @return the loadDataModel
	 */
	public LoadDataModel getLoadDataModel() {
		return loadDataModel;
	}

	public TuningModel getTuningModel() {
		return tunningModel;
	}

}
