/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.loomisanalysis;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BoxLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import eu.omp.irap.cassis.gui.model.ButtonsPanel;
import eu.omp.irap.cassis.gui.model.parameter.data.LoadDataPanel;
import eu.omp.irap.cassis.gui.model.parameter.threshold.ThresholdPanel;
import eu.omp.irap.cassis.gui.model.parameter.tuning.TuningPanel;

/**
 * @author glorian
 *
 */
@SuppressWarnings("serial")
public class LoomisWoodPanel extends JPanel {

	public static final String WILDCARD = "*";
	private LoomisWoodControl control;
	private ThresholdPanel thresholdPanel;
	private TuningPanel tuningPanel;
	private LoadDataPanel dataPanel;
	private LoomisWoodParameterPanel loomisParametersPanel;
	private ButtonsPanel buttonsPanel;


	public LoomisWoodPanel() {
		this(new LoomisWoodModel());
	}

	public LoomisWoodPanel(LoomisWoodModel model) {
		control = new LoomisWoodControl(this, model);
		initialyse();
	}

	private void initialyse() {
		setLayout(new BorderLayout());
		setSize(1016, 600);
		setPreferredSize(new Dimension(1016, 600));
		JPanel nortPanel = new JPanel(new BorderLayout());
		JPanel northWestPanel = new JPanel();
		northWestPanel.setLayout(new BoxLayout(northWestPanel, BoxLayout.Y_AXIS));
		northWestPanel.add(getLoadDataPanel(), northWestPanel);
		northWestPanel.add(getTuningPanel(), northWestPanel);
		northWestPanel.add(getThresholdPanel(), northWestPanel);
		nortPanel.add(getButtonPanel(), BorderLayout.EAST);
		nortPanel.add(northWestPanel, BorderLayout.CENTER);

		JTabbedPane pane = new JTabbedPane();
		pane.add("Loomis Wood", getLoomisParametersPanel());
		add(pane, BorderLayout.CENTER);
		add(nortPanel, BorderLayout.NORTH);
	}

	/**
	 * @return the control
	 */
	public final LoomisWoodControl getControl() {
		return control;
	}

	public LoomisWoodParameterPanel getLoomisParametersPanel() {
		if (loomisParametersPanel == null) {
			loomisParametersPanel = new LoomisWoodParameterPanel(new LoomisWoodParameterControl(
					control.getModel().getWoodParameterModel()));
		}
		return loomisParametersPanel;
	}

	public ThresholdPanel getThresholdPanel() {
		if (thresholdPanel == null) {
			thresholdPanel = new ThresholdPanel(control.getModel().getTresholdmodel());
		}
		return thresholdPanel;
	}

	private Component getTuningPanel() {
		if (tuningPanel == null) {
			tuningPanel = new TuningPanel(control.getModel().getTuningModel());
		}
		return tuningPanel;
	}

	private JPanel getButtonPanel() {
		if (buttonsPanel == null) {
			buttonsPanel = new ButtonsPanel();
			buttonsPanel.getLoadConfigButton().addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.loadModel();
				}
			});

			buttonsPanel.getLoadConfigButton().addMouseListener(new MouseAdapter() {

				@Override
				public void mouseClicked(MouseEvent e) {
					control.loadConfigButtonClicked(e);
				}
			});
			buttonsPanel.getSaveConfigButton().addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.saveModel();
				}
			});
			buttonsPanel.getDisplayButton().addActionListener(control.getDisplayActionListener());
		}

		return buttonsPanel;
	}

	public LoadDataPanel getLoadDataPanel() {
		if (dataPanel == null) {
			dataPanel = new LoadDataPanel(control.getModel().getLoadDataModel());
		}
		return dataPanel;
	}

	/**
	 * Refresh the view.
	 */
	public void refresh() {
		LoomisWoodModel lwm = control.getModel();

		dataPanel.setModel(lwm.getLoadDataModel());
		tuningPanel.setModel(lwm.getTuningModel());
		thresholdPanel.setModel(lwm.getTresholdmodel());
		loomisParametersPanel.getControl().setModel(lwm.getWoodParameterModel());
	}

	/**
	 * Display a pop-up for a load configuration error.
	 *
	 * @param message The message to display.
	 */
	public void displayLoadConfigError(String message) {
		JOptionPane.showMessageDialog(this, message, "Load configuration error",
				JOptionPane.WARNING_MESSAGE);
	}
}
