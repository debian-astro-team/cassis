/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import eu.omp.irap.cassis.common.BufferedWriterProperty;
import eu.omp.irap.cassis.common.ParameterDescription;
import eu.omp.irap.cassis.common.Telescope;
import eu.omp.irap.cassis.common.events.DataModel;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.gui.model.parameter.telescope.TelescopeModel;
import eu.omp.irap.cassis.gui.util.PropertiesLinkedHashMap;
import eu.omp.irap.cassis.properties.Software;

public class ParametersModel extends DataModel {

	public static final String TELESCOPE_EVENT = "telescope";
	public static final String TELESCOPE_LOAD_EVENT = "telescopeLoad";
	public static final String TMB_TA_EVENT = "tmbTa";

	private Boolean tmbTa;
	private Telescope telescope;

	// Telescope
	private String selectedTelescope = "";
	private List<String> telescopeList = new ArrayList<>();
	private String telescopeError = null;

	public ParametersModel() {
		tmbTa = false;
		selectedTelescope = TelescopeModel.DEFAULT_TELESCOPE;
		telescope = new Telescope("");

		init();
	}

	/**
	 * Initialize telescope parameters.
	 */
	public void init() {
		String base = selectedTelescope;
		if (!new File(selectedTelescope).exists()) {
			selectedTelescope = Software.getTelescopePath() + File.separatorChar
					+ selectedTelescope;
		}
		if (!new File(selectedTelescope).exists()) {
			String filename = new File(base).getName();
			String path = Software.getTelescopePath() + File.separatorChar + filename;
			if (new File(path).exists()) {
				selectedTelescope = path;
			}
		}

		if (new File(selectedTelescope).exists() && ! new File(selectedTelescope).isDirectory()) {
			telescope.setFilename(selectedTelescope);
		}
	}

	/**
	 * @return TmbTa
	 */
	public Boolean isTmbTa() {
		return tmbTa;
	}

	/**
	 * Change TmbTa.
	 *
	 * @param compute The tmbTa value to set.
	 */
	public void setTmbTa(Boolean compute) {
		tmbTa = compute;
		fireDataChanged(new ModelChangedEvent(TMB_TA_EVENT, this.tmbTa));
	}

	/**
	 * @return the tmbTa
	 */
	public final Boolean getTmbTa() {
		return tmbTa;
	}

	/**
	 * @return the telescope
	 */
	public final Telescope getTelescope() {
		return telescope;
	}

	/**
	 * Set telescope name list.
	 *
	 * @param telescopeNames The telescope name list.
	 */
	public void setTelescopeList(ArrayList<String> telescopeNames) {
		telescopeList = telescopeNames;
	}

	/**
	 * Get telescope name list.
	 *
	 * @return the telescope name list.
	 */
	public List<String> getTelescopeList() {
		return telescopeList;
	}

	/**
	 * Set selected telescope in combobox for telescopes with option for changing the event name
	 * (Useful for don't catching the event in LineAnalysisControl).
	 *
	 * @param selectedTelescope The selected telescope.
	 * @param eventName The name of the event to fire.
	 */
	private void setSelectedTelescope(String selectedTelescope, String eventName) {
		this.selectedTelescope = selectedTelescope;
		init();
		fireDataChanged(new ModelChangedEvent(eventName, this.selectedTelescope));
	}

	/**
	 * Set selected telescope in combobox for telescopes.
	 *
	 * @param selectedTelescope The selected telescope.
	 */
	public void setSelectedTelescope(String selectedTelescope) {
		setSelectedTelescope(selectedTelescope, TELESCOPE_EVENT);
	}


	/**
	 * Get selected telescope name.
	 *
	 * @return the selected telescope name.
	 */
	public String getSelectedTelescope() {
		return selectedTelescope;
	}


	@Override
	public void saveConfig(BufferedWriterProperty out) throws IOException {
		out.writeProperty("telescope", String.valueOf(selectedTelescope));
		out.writeProperty("tmbBox", String.valueOf(tmbTa));
		out.flush();
	}

	@Override
	public void loadConfig(Properties prop) {
		if (prop.containsKey("telescope") && Telescope.exist(prop.getProperty("telescope"))) {
			setSelectedTelescope(prop.getProperty("telescope"), TELESCOPE_LOAD_EVENT);
			setTmbTa(Boolean.valueOf(prop.getProperty("tmbBox")));
		} else {
			telescopeError = prop.getProperty("telescope");
		}

	}

	public HashMap<String, ParameterDescription> getMapParameter() {
		HashMap<String, ParameterDescription> result = new HashMap<>();
		result.put("telaper", new ParameterDescription(telescope.getDiameter()));
		if (tmbTa) {
			result.put("tmbTa", null);
		}
		return result;
	}

	public Map<String, String> getProperties() {
		Map<String, String> map = new PropertiesLinkedHashMap();
		map.put("telescope", selectedTelescope);
		map.put("tmbBox", String.valueOf(tmbTa));
		return map;
	}

	/**
	 * Return the telescope error, the path of the telescope file.
	 *
	 * @return the path of the telescope file if there is an error, null otherwise.
	 */
	public String getTelescopeError() {
		return telescopeError;
	}

	/**
	 * Remove the telescope error.
	 */
	public void resetTelescopeError() {
		telescopeError = null;
	}

	/**
	 * Return if there is an error with the telescope.
	 *
	 * @return true if there is an error with the telescope, false otherwise.
	 */
	public boolean haveTelescopeError() {
		return telescopeError != null;
	}
}

