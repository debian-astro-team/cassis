/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.ComponentAlgo;
import eu.omp.irap.cassis.common.ComponentMode;
import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.gui.model.table.CassisTableAbstractModel;
import eu.omp.irap.cassis.gui.model.table.CassisTableMolecule;
import eu.omp.irap.cassis.gui.model.table.CassisTableMoleculeModel;
import eu.omp.irap.cassis.gui.model.table.CassisValidator;
import eu.omp.irap.cassis.gui.model.table.JCassisTable;
import eu.omp.irap.cassis.gui.template.JComboBoxTemplate;
import eu.omp.irap.cassis.gui.util.CassisUtilGui;
import eu.omp.irap.cassis.gui.util.GbcUtil;
import eu.omp.irap.cassis.gui.util.ListenersUtil;
import eu.omp.irap.cassis.gui.util.TextFieldFormatFilter;
import eu.omp.irap.cassis.properties.Software;

@SuppressWarnings("serial")
public class ComponentSpeciesPanel extends ComponentDescriptionPanel implements CassisValidator {

	private static final Logger LOGGER = LoggerFactory.getLogger(ComponentDescriptionPanel.class);

	private JPanel northPanel;
	private JPanel rightNorthSourcePanel;

	private JLabel densityLabel;
	private JTextField densityField;
	private JLabel vlsrLabel;
	private JTextField vlsrField;
	private JLabel geometryLabel;
	private EmAbTabbedFocusAdapter myAdapter = new EmAbTabbedFocusAdapter();
	private JComboBoxTemplate templateCombo;
	private JCassisTable<MoleculeDescription> moleculeTable;

	private JPanel panelLteParam;
	private JCheckBox interactingCheckBox;

	private JPanel interactingAndGeometryPanel;
	private JComboBox<ComponentMode> geometryBox;
	private JComboBox<ComponentAlgo> modeBox;
	private JPanel leftNorthSourcePanel;
	private JLabel moleculeLabel;
	private JLabel modeLabel;
	private JComboBox<UNIT> vlsrUnitComboBox;


	public ComponentSpeciesPanel() {
		this(new ComponentSpecies("Component 1", new ArrayList<>(), 7.5E22,
				0.0));
	}

	public ComponentSpeciesPanel(ComponentDescription componentDescription) {
		this(componentDescription, null);

	}

	public ComponentSpeciesPanel(ComponentDescription componentDescription,
			ButtonTabComponent buttonTab) {
		super(componentDescription, buttonTab);
		getComponentDescription().addModelListener(this);
		setMinimumSize(new Dimension(970, 350));
		setPreferredSize(new Dimension(970, 350));
		setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		setBorder(new TitledBorder(""));
		setLayout(new BorderLayout());
		add(getSourcePanel(), BorderLayout.NORTH);
		JScrollPane scrollPane = new JScrollPane(getMoleculesTable());
		getMoleculesTable().setScrollPane(scrollPane);
		add(scrollPane, BorderLayout.CENTER);
		setTableModel();
	}

	/**
	 * Change the enabled state of the molecule table.
	 *
	 * @param isEnabled true if the molecule table is enable, false otherwise.
	 */
	public void setEnabledMoleculeTable(boolean isEnabled) {
		moleculeTable.setEnabled(isEnabled);
	}

	/**
	 * Creates the source part of the panel if necessary then return it.
	 *
	 * @return the source panel.
	 */
	public JComponent getSourcePanel() {
		if (northPanel == null) {
			northPanel = new JPanel();
			northPanel.setLayout(new BoxLayout(northPanel, BoxLayout.LINE_AXIS));
			northPanel.add(buildLeftNorthSourcePanel());
			northPanel.add(buildInteractingAndGeometryPanel());
			northPanel.add(buildParamLteAndRadexPanel());
			northPanel.add(buildRightNorthSourcePanel());
		}
		return northPanel;
	}

	/**
	 * @return the model
	 */
	public ComponentSpecies getComponentDescription() {
		return (ComponentSpecies)model;
	}

	/**
	 * @return the moleculeTable
	 */
	public JCassisTable<MoleculeDescription> getMoleculesTable() {
		if (moleculeTable == null) {
			getComponentDescription().getCassisTableMoleculeModel().setValidator(this);
			moleculeTable = new JCassisTable<>(
					getComponentDescription().getCassisTableMoleculeModel(), true, false);
			moleculeTable.setTemplateCombo(templateCombo);
			resizeColumn("Collision", 0, 0, 0);
			resizeColumn("TKin (K)", 0, 0, 0);
			resizeColumn("Tex (K)", 10, 500, 100);

			densityField.addActionListener(new DensityActionListener(moleculeTable));
			moleculeTable.setPreferredScrollableViewportSize(new Dimension(960, 250));
			setUpCellToolTips();
			moleculeTable.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (e.getSource() == moleculeTable && e.getButton() == MouseEvent.BUTTON1) {

						CassisTableAbstractModel<MoleculeDescription> modelIndice = getComponentDescription()
								.getCassisTableMoleculeModel();
						int rowIndex = moleculeTable.rowAtPoint(e.getPoint());
						if (rowIndex == -1 && moleculeTable.getRowCount() == 1) {
							rowIndex = 0;
						}
						if (rowIndex != -1) {
							rowIndex = moleculeTable.convertRowIndexToModel(rowIndex);
						}
						int columnView = moleculeTable.columnAtPoint(e.getPoint());
						int columnModel = moleculeTable.convertColumnIndexToModel(columnView);

						if (columnModel == modelIndice.getIndice(CassisTableMolecule.COLLISION_INDICE) &&
								modeBox.getSelectedItem() == ComponentAlgo.FULL_RADEX_ALGO) {

							displayCollisionManager(rowIndex);
						}
					}
				}
			});
		}
		return moleculeTable;
	}

	private void displayCollisionManager(int rowIndex) {
		int tag = -1;
		if (rowIndex != -1) {
			tag = (Integer) moleculeTable.getModel().getValueAt(rowIndex, 1);
		}

		if (tag != -1)  {
			MoleculeDescription mol = null;
			List<MoleculeDescription> moleculeList = getComponentDescription().getMoleculeList();
			for (int index = 0; mol == null && index < moleculeList.size(); index++) {
				MoleculeDescription molTemp = moleculeList.get(index);
				if (molTemp.getTag() == tag) {
					mol = molTemp;
				}
			}

			if (mol != null) {
				List<String> listMolesFiles = Software.getRadexMoleculesManager()
						.cassisNameToMoleFile(mol.getTag());

				if (RADEXPartnerDialog.getInstance() == null) {
					RADEXPartnerDialog dialog =
							RADEXPartnerDialog.getInstance(mol, listMolesFiles);
					dialog.setModal(true);
					dialog.setVisible(true);
				}
			}
		}
	}


	private void setUpCellToolTips() {
		CassisTableAbstractModel<MoleculeDescription> modelIndice = (CassisTableMoleculeModel) moleculeTable.getModel();
		if (modelIndice.getIndice(CassisTableMolecule.DENSITY_INDICE) != -1) {
			JCassisTable<?>.DoubleRenderer r = moleculeTable.new DoubleRenderer();
			r.setToolTipText("<html>Column density of the species. Changing this <br/>parameter will change the abundance of <br/>the corresponding species, according to: <br/>Abundance = N(Sp)/N(H<sub>2</sub>)</html>");
			moleculeTable.getColumnModel().getColumn(modelIndice.getIndice(CassisTableMolecule.DENSITY_INDICE))
					.setCellRenderer(r);
		}

		if (modelIndice.getIndice(CassisTableMolecule.RELATIVE_ABUNDANCE_INDICE) != -1) {
			JCassisTable<?>.DoubleRenderer r = moleculeTable.new DoubleRenderer();
			r.setToolTipText("<html>Abundance of the species. Changing this  <br/>parameter will change the column density <br/>of the corresponding species, N(Sp), <br/>according to: N(Sp) = N(H<sub>2</sub>)*Abundance</html>");
			moleculeTable.getColumnModel()
					.getColumn(modelIndice.getIndice(CassisTableMolecule.RELATIVE_ABUNDANCE_INDICE)).setCellRenderer(r);
		}

		if (modelIndice.getIndice(CassisTableMolecule.COLLISION_INDICE) != -1) {
			DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
			renderer.setToolTipText("Ctrl + left click to add a collision file.");
			moleculeTable.getColumnModel().getColumn(modelIndice.getIndice(
					CassisTableMolecule.COLLISION_INDICE)).setCellRenderer(renderer);
		}
	}

	public void setTableModel() {
		ComponentAlgo selected = (ComponentAlgo) modeBox.getSelectedItem();
		if (selected == ComponentAlgo.FULL_LTE_ALGO) {
			resizeColumn("Collision", 0, 0, 0);
			resizeColumn("TKin (K)", 0, 0, 0);
			resizeColumn("Tex (K)", 10, 500, 100);
		} else if (selected == ComponentAlgo.FULL_RADEX_ALGO) {
			resizeColumn("Collision", 10, 500, 100);
			resizeColumn("TKin (K)", 10, 500, 100);
			resizeColumn("Tex (K)", 0, 0, 0);


		}
		changeStateGeometryBox();
		setUpCellToolTips();
	}

	private void resizeColumn(String nameColumn, int min, int max, int preferred) {
		moleculeTable.getColumn(nameColumn).setMinWidth(min);
		moleculeTable.getColumn(nameColumn).setMaxWidth(max);
		moleculeTable.getColumn(nameColumn).setPreferredWidth(preferred);
		moleculeTable.getColumn(nameColumn).setWidth(preferred);
	}

	private JPanel buildRightNorthSourcePanel() {
		if (rightNorthSourcePanel == null) {
			rightNorthSourcePanel = new JPanel();
		}
		return rightNorthSourcePanel;
	}

	private JPanel buildInteractingAndGeometryPanel() {
		if (interactingAndGeometryPanel == null) {
			interactingCheckBox = new JCheckBox("Interacting");
			interactingCheckBox.setSelected(getComponentDescription().isInteracting());
			interactingCheckBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					Boolean stateInteratectingCheckBox = ((JCheckBox) e.getSource()).isSelected();
					getComponentDescription().setInteracting(stateInteratectingCheckBox);
				}
			});

			geometryLabel = new JLabel("Geometry:  ");
			final ComponentMode[] componentModes = new ComponentMode[]{ComponentMode.SPHERE_MODE, ComponentMode.SLAB_MODE, ComponentMode.EXPANDING_SPHERE_MODE};
			geometryBox = new JComboBox<>(componentModes);
			geometryBox.setSelectedItem(getComponentDescription().getGeometryMode());
			geometryBox.setPreferredSize(new Dimension(110, 25));
			geometryBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					getComponentDescription().setGeometryMode((ComponentMode)((JComboBox<?>)e.getSource()).getSelectedItem());
				}
			});

			changeStateGeometryBox();

			interactingAndGeometryPanel = new JPanel();
			interactingAndGeometryPanel.setLayout(new GridBagLayout());
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.insets = new Insets(1, 0, 1, 0);
			GbcUtil.configureFillGridsAnchor(gbc, GridBagConstraints.NONE, 0, 0, GridBagConstraints.LINE_START);
			interactingAndGeometryPanel.add(interactingCheckBox, gbc);

			GbcUtil.configureFillGridsAnchor(gbc, GridBagConstraints.NONE, 0, 1, GridBagConstraints.LINE_END);
			interactingAndGeometryPanel.add(geometryLabel, gbc);

			GbcUtil.configureFillGridsAnchor(gbc, GridBagConstraints.HORIZONTAL, 1, 1, GridBagConstraints.LINE_START);
			interactingAndGeometryPanel.add(geometryBox, gbc);
		}
		return interactingAndGeometryPanel;
	}

	private JPanel buildParamLteAndRadexPanel() {
		if (panelLteParam == null) {


			// -- Density
			densityLabel = new JLabel("<HTML><p>N(H<sub>2</sub>) [cm\u207B\u00B2]: </p></HTML>");
			densityField = new JTextField(String.valueOf(getComponentDescription().getDensity()), 5);
			densityField.addKeyListener(new TextFieldFormatFilter(TextFieldFormatFilter.SCIENTIFIC_NUMBER));
			densityLabel.setLabelFor(densityField);
			densityField.addFocusListener(myAdapter);
			String densityToolTip = "<html>H<sub>2</sub> column density. Changing this parameter <br/>will change the column densities of all species, <br/>N(Sp), according to: N(Sp) = N(H<sub>2</sub>)*Abundance</html>";
			densityField.setToolTipText(densityToolTip);
			densityLabel.setToolTipText(densityToolTip);
			densityField.setSize(70, 25);
			densityField.setPreferredSize(new Dimension(70, 25));
			densityField.setMinimumSize(new Dimension(70, 25));

			// -- Vlsr
			vlsrLabel = new JLabel("<HTML><p>&nbsp; &nbsp; V<sub>lsr</sub>: </p></HTML>");
			double vlsr = getComponentDescription().getVlsr();
			String vlsrString = "";
			if (vlsr == Double.MAX_VALUE) {
				vlsrString = ComponentSpecies.VLSR_FILE_STRING;
			} else {
				vlsrString = String.valueOf(vlsr);
			}
			vlsrField = new JTextField(vlsrString, 6);
			vlsrField.setText(vlsrString);
			vlsrField.setToolTipText("Enter a real or " + ComponentSpecies.VLSR_FILE_STRING);
			vlsrField.addFocusListener(myAdapter);
			vlsrLabel.setLabelFor(vlsrField);
			vlsrField.setSize(70, 25);
			vlsrField.setPreferredSize(new Dimension(70, 25));
			vlsrField.setMinimumSize(new Dimension(70, 25));

			vlsrUnitComboBox = new JComboBox<>(new UNIT[] { getComponentDescription().getVlsrUnit() });
			vlsrUnitComboBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					getComponentDescription().setVlsrUnit((UNIT) ((JComboBox<?>)e.getSource()).getSelectedItem());
				}
			});

			panelLteParam = new JPanel();
			panelLteParam.setLayout(new GridBagLayout());
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.insets = new Insets(1, 4, 1, 4);

			// Top
			GbcUtil.configureFillGridsWeightx(gbc, GridBagConstraints.NONE, 0, 0, 0.7);
			panelLteParam.add(densityLabel, gbc);

			GbcUtil.configureFillGridsWeightx(gbc, GridBagConstraints.HORIZONTAL, 1, 0, 1);
			panelLteParam.add(densityField, gbc);


			GbcUtil.configureFillGridsWeightx(gbc, GridBagConstraints.NONE, 2, 0, 0.7);


			GbcUtil.configureFillGridsWeightx(gbc, GridBagConstraints.HORIZONTAL, 3, 0, 1);


			// Bottom
			GbcUtil.configureFillGridsWeightx(gbc, GridBagConstraints.NONE, 0, 1, 0.7);
			panelLteParam.add(vlsrLabel, gbc);

			GbcUtil.configureFillGridsWeightx(gbc, GridBagConstraints.HORIZONTAL, 1, 1, 1);
			panelLteParam.add(vlsrField, gbc);

			GbcUtil.configureFillGridsWeightx(gbc, GridBagConstraints.NONE, 2, 1, 1);
			panelLteParam.add(vlsrUnitComboBox, gbc);
		}
		return panelLteParam;
	}

	private JPanel buildLeftNorthSourcePanel() {
		if (leftNorthSourcePanel == null) {
			moleculeLabel = new JLabel("Molecules:");
			moleculeLabel.setLabelFor(getTemplateCombo());

			modeLabel = new JLabel("Mode:");
			ComponentAlgo[] componentAlgo = new ComponentAlgo[] {ComponentAlgo.FULL_LTE_ALGO, ComponentAlgo.FULL_RADEX_ALGO};
			modeBox = new JComboBox<>(componentAlgo);
			modeBox.setSelectedItem(getComponentDescription().getTypeAlgo());
			modeBox.setPreferredSize(new Dimension(148, 25));
			modeBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent ae) {
					setTableModel();
					ComponentAlgo algo = (ComponentAlgo) ((JComboBox<?>)ae.getSource()).getSelectedItem();
					getComponentDescription().setTypeAlgo(algo);
				}
			});

			leftNorthSourcePanel = new JPanel();
			leftNorthSourcePanel.setLayout(new GridBagLayout());
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.insets = new Insets(1, 0, 1, 0);
			GbcUtil.configureFillGrids(gbc, GridBagConstraints.NONE, 0, 0);
			leftNorthSourcePanel.add(modeLabel, gbc);

			GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 0);
			leftNorthSourcePanel.add(modeBox, gbc);

			GbcUtil.configureFillGrids(gbc, GridBagConstraints.NONE, 0, 1);
			leftNorthSourcePanel.add(moleculeLabel, gbc);

			GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 1);
			leftNorthSourcePanel.add(templateCombo, gbc);
		}
		return leftNorthSourcePanel;
	}

	public JComboBoxTemplate getTemplateCombo() {
		if (templateCombo == null){
			templateCombo = new JComboBoxTemplate(true, true);
			templateCombo.setSelectedItem(getComponentDescription().getSelectedTemplate());
			templateCombo.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					String template = (String) templateCombo.getSelectedItem();
					if (template == null) {
						return;
					}
					else if (!JComboBoxTemplate.OPERATIONS_TEMPLATE.equals(template) && !JComboBoxTemplate.TEMPLATE.equals(template)) {
						getComponentDescription().setSelectedTemplate(template);
					}
				}
			});
			templateCombo.setPreferredSize(new Dimension(148, 25));
		}
		return templateCombo;
	}

	/**
	 * @return the geometryLabel
	 */
	public JLabel getGeometryLabel() {
		return geometryLabel;
	}

	/**
	 * @return the vlsrLabel
	 */
	public JLabel getVlsrLabel() {
		return vlsrLabel;
	}

	/**
	 * @return the densityLabel
	 */
	public JLabel getDensityLabel() {
		return densityLabel;
	}

	/**
	 * Set selected template.
	 *
	 * @param templateName
	 *            template name
	 */
	public void setSelectedTemplate(String templateName) {
		templateCombo.setSelectedItem(templateName);
		getComponentDescription().setSelectedTemplate(templateName);
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		getComponentDescription().setSelected(enabled);
	}

	/**
	 * Initialize the panel : enabled or not
	 */
	public void enableByMode() {
		if (getComponentDescription().isSelected())
			setTableModel();
		else
			deselectView();
	}

	/**
	 * When the panel is not selected : all isn't enabled
	 */
	public void deselectView() {
		templateCombo.setEnabled(false);

		densityField.setEnabled(false);
		vlsrField.setEnabled(false);
		densityLabel.setText("<HTML><font color='gray'><p>N(H<sub>2</sub>) [cm\u207B\u00B2]: </p></font></HTML>");
		vlsrLabel.setText("<HTML><font color='gray'><p>&nbsp; &nbsp; V<sub>lsr</sub> [km/s]: </p></font></HTML>");
	}


	public JTextField getDensityField() {
		return densityField;
	}

	public JTextField getVlsrField() {
		return vlsrField;
	}

	/**
	 * Define a custom FocusAdapter.
	 *
	 * @author thachtn
	 */
	private class EmAbTabbedFocusAdapter extends FocusAdapter {

		@Override
		public void focusLost(FocusEvent e) {
			if (e.getSource() == densityField) {
				getComponentDescription().setDensity(Double.valueOf(densityField.getText()));
			}
			else if (e.getSource() == vlsrField) {
				try {
					getComponentDescription().setVlsr(Double.valueOf(vlsrField.getText()));
				} catch (Exception ex) {
					LOGGER.error("Error while updating the vlsr value", ex);
					JOptionPane.showMessageDialog(ComponentSpeciesPanel.this, ex.getMessage());
				}
			}
		}
	}

	@Override
	public void dataChanged(ModelChangedEvent event) {

		if (ComponentSpecies.DENSITY_EVENT.equals(event.getSource())) {
			densityField.setText(((Double) event.getValue()).toString());
		} else if (ComponentSpecies.VLSR_EVENT.equals(event.getSource())) {
			vlsrField.setText(((Double) event.getValue()).toString());
		} else if (ComponentSpecies.VLSR_UNIT_EVENT.equals(event.getSource())) {
			vlsrUnitComboBox.setSelectedItem((UNIT) event.getValue());
		} else if (ComponentSpecies.INTERACTING_EVENT.equals(event.getSource())) {
			interactingCheckBox.setSelected((Boolean) event.getValue());
		} else if (ComponentSpecies.COMPONENT_ALGO_EVENT.equals(event.getSource())) {
			setTableModel();
		} else if (ComponentSpecies.CHANGE_ON_MOLECULE_TABLE_EVENT.equals(event.getSource())) {
			getMoleculesTable().updateUI();
		} else {
			super.dataChanged(event);
		}
	}

	@Override
	public void isValueValid(Object value, int row, int col) {
		CassisTableAbstractModel<MoleculeDescription> modelIndice = (CassisTableMoleculeModel) moleculeTable.getModel();
		try {
			if (col == modelIndice.getIndice(CassisTableMolecule.DENSITY_INDICE)) {
				double rowDensity;
				if (value instanceof String) {
					rowDensity = Double.parseDouble((String)value);
				} else {
					rowDensity = (Double) value;
				}

				if (getComponentDescription().isLteRadexMode() || getComponentDescription().isFullRadexMode()) {
					if (rowDensity < 1e5 || rowDensity > 1e25)
						JOptionPane.showMessageDialog(this,
								"The value is out of RADEX range. You must enter a double number beetwen 1e5 and 1e25",
								"Error", JOptionPane.ERROR_MESSAGE);
					else {
						double globalDensity = Double.parseDouble(densityField.getText());
						double newAbundance = rowDensity / globalDensity;
						getComponentDescription().getMoleculeList().get(row).setRelativeAbundance(newAbundance);
					}
				}
				else {
					double globalDensity = Double.parseDouble(densityField.getText());
					double newAbundance = rowDensity / globalDensity;
					getComponentDescription().getMoleculeList().get(row).setRelativeAbundance(newAbundance);
				}
			}
			else if (col == modelIndice.getIndice(CassisTableMolecule.RELATIVE_ABUNDANCE_INDICE)) {
				double rowAbundance = (Double) value;
				double globalDensity = Double.parseDouble(densityField.getText());
				double newDensity = rowAbundance * globalDensity;
				getComponentDescription().getMoleculeList().get(row).setDensity(newDensity);
			}
			else if (col == modelIndice.getIndice(CassisTableMolecule.TKIN_INDICE)) {
				Double tkin = (Double) value;
				if ((getComponentDescription().isLteRadexMode() || getComponentDescription().isFullRadexMode())
						&& (tkin < 0.1 || tkin > 1e4)) {
					JOptionPane.showMessageDialog(this,
							"The value is out of RADEX range. You must enter a double number beetwen 0.1 and 1e4",
							"Error", JOptionPane.ERROR_MESSAGE);
				}
			}
			else if (col == modelIndice.getIndice(CassisTableMolecule.VELOCITY_DISPERSION_INDICE)) {
				Double velocityDispersion = (Double) value;
				if ((getComponentDescription().isLteRadexMode() || getComponentDescription().isFullRadexMode())
						&& (velocityDispersion < 1e-3 || velocityDispersion > 1e3)) {
					JOptionPane.showMessageDialog(this,
							"The value is out of RADEX range. You must enter a double number beetwen 1e-3 and 1e3",
							"Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		} catch (NumberFormatException e) {
			LOGGER.warn("The value is not correct. You must enter numbers and not letter", e);
			JOptionPane.showMessageDialog(this, "The value is not correct. You must enter numbers and not letters.",
					"Error", JOptionPane.ERROR_MESSAGE);
		}
		((CassisTableMoleculeModel) getMoleculesTable().getModel()).setList(getComponentDescription().getMoleculeList());
	}

	public void changeStateGeometryBox() {
		ComponentAlgo selected = (ComponentAlgo) modeBox.getSelectedItem();
		if (selected == ComponentAlgo.FULL_LTE_ALGO) {
			CassisUtilGui.setComponentEnabled(geometryLabel, false);
			CassisUtilGui.setComponentEnabled(geometryBox, false);
		}
		else if (selected == ComponentAlgo.FULL_RADEX_ALGO) {
			CassisUtilGui.setComponentEnabled(geometryLabel, true);
			CassisUtilGui.setComponentEnabled(geometryBox, true);
		}
	}

	/**
	 * Remove all listeners.
	 */
	public void removeAllListeners() {
		getComponentDescription().removeModelListener(this);

		moleculeTable.removeListeners();

		ListenersUtil.removeMouseListeners(moleculeTable);
		ListenersUtil.removeActionListeners(interactingCheckBox);
		ListenersUtil.removeActionListeners(geometryBox);
		ListenersUtil.removeKeyListeners(densityField);
		ListenersUtil.removeActionListeners(densityField);
		ListenersUtil.removeActionListeners(vlsrUnitComboBox);
		ListenersUtil.removeActionListeners(templateCombo);
		ListenersUtil.removeActionListeners(modeBox);

		vlsrField.removeFocusListener(myAdapter);
		densityField.removeFocusListener(myAdapter);
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ComponentSpeciesPanel componentPanel = new ComponentSpeciesPanel();
		frame.setContentPane(componentPanel);
		frame.pack();
		frame.setVisible(true);
	}

}
