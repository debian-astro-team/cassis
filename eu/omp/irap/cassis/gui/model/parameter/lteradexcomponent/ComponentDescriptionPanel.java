/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent;

import javax.swing.JPanel;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;

@SuppressWarnings("serial")
public abstract class ComponentDescriptionPanel extends JPanel implements ModelListener {

	protected ComponentDescription model;
	private ButtonTabComponent btc = null;


	public ComponentDescriptionPanel(ComponentDescription componentDescription, ButtonTabComponent btc) {
		this(componentDescription);
		this.btc = btc;
	}

	public ComponentDescriptionPanel(ComponentDescription componentDescription) {
		model = componentDescription;
		model.addModelListener(this);
	}

	public void removeAllListeners() {
		if (btc != null) {
			btc.removeAllListeners();
		}
	}

	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (ComponentDescription.COMPONENT_NAME_EVENT.equals(event.getSource())&& btc != null) {
			String name = (String) event.getValue();
			btc.changeLabelText(name);
		}
	}

	public void setBTC(ButtonTabComponent btc) {
		this.btc = btc;
	}

	/**
	 * @return the model
	 */
	public ComponentDescription getComponentDescription() {
		return model;
	}

	/**
	 * @param model the model to set
	 */
	public void setModel(ComponentDescription model) {
		this.model = model;
	}

	public ButtonTabComponent getBTC() {
		return btc;
	}

}
