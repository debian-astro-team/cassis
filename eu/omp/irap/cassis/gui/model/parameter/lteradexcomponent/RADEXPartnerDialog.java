/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.plaf.basic.BasicComboBoxRenderer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.cassisd.model.synthetic.RadexModel;
import eu.omp.irap.cassis.common.Molecule;
import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.properties.Software;
import radex.PartnerModel;
import radex.RadexUtils;
import radex.ReadRadexFile;
import radex.exception.MoleculeException;
import radex.exception.OutOfBoundCollisionPartnersException;
import radex.exception.OutOfBoundCollisionRatesException;
import radex.exception.OutOfBoundCollisionTemperatureException;
import radex.exception.OutOfBoundEnergyLevelsException;
import radex.exception.OutOfBoundSpectralLinesException;

/**
 * @author ta, jglorian
 *
 */
public class RADEXPartnerDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(RADEXPartnerDialog.class);
	private static RADEXPartnerDialog dialog;
	private JPanel jContentPane;
	private JTextField[] jtfDensities;

	private JButton jbtnSave;
	private JButton jbtnCancel;
	private JComboBox<String> jComboBox;
	private MoleculeDescription molDescription;
	private List<PartnerModel> listPartners;
	private List<String> listMolesFiles;
	private String moleFile = "";

	public final String[] collisionners = new String[]{"H2", "p-H2", "o-H2", "e", "H", "He", "H+"};
	private JButton addButton;
	private JButton deleteButton;

	private final String LAST_COLLISION_FOLDER = "radex-collision";


	private RADEXPartnerDialog(MoleculeDescription moleculeDescription,
			List<String> listMolesFiles) {
		super();
		this.listMolesFiles = listMolesFiles;

		initialize();

		this.molDescription = moleculeDescription;

		String collision = moleculeDescription.getCollision();

		this.listPartners = RadexModel.getPartnersFromCollision(collision);
		this.moleFile = RadexModel.getMoleFileFromCollision(collision);

		initializeValue();

		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent we) {
				RADEXPartnerDialog.this.dispose();
				if (RADEXPartnerDialog.this.listMolesFiles.contains(Molecule.NO_COLLISION)) {
					RADEXPartnerDialog.this.listMolesFiles.remove(Molecule.NO_COLLISION);
				}
				RADEXPartnerDialog.dialog = null;
			}
		});
	}

	/**
	 * Singleton implementation for {@link RADEXPartnerDialog}, creating the
	 *  object if necessary.
	 *
	 * @param moleculeDescription The molecule description.
	 * @param listMolesFiles The list of melcules file.
	 * @return the Radex partner dialog.
	 */
	public static RADEXPartnerDialog getInstance(MoleculeDescription moleculeDescription,
			List<String> listMolesFiles) {
		if (dialog == null) {
			dialog = new RADEXPartnerDialog(moleculeDescription, listMolesFiles);
		}
		return dialog;
	}

	public static RADEXPartnerDialog getInstance() {
		return dialog;
	}

	/**
	 * Disable all JTextField.
	 */
	private void disableAllDensityJTextField() {
		for (JTextField jTextField : jtfDensities) {
			jTextField.setEnabled(false);
		}
	}

	/**
	 * @author thachtn
	 *
	 */
	private void initializeValue() {
		if (!"".equals(moleFile)) {
			for (int i = 0; i < listMolesFiles.size(); i++) {
				if (listMolesFiles.get(i).equals(moleFile))
					jComboBox.setSelectedIndex(i);
			}
		} else if (jComboBox.getItemCount() >= 1) {
			jComboBox.setSelectedIndex(0);
		}

		disableAllDensityJTextField();

		if (listPartners != null) {
			PartnerModel pModel;
			String text;
			DecimalFormat format = new DecimalFormat("0.0E00", new DecimalFormatSymbols(new Locale("en")));
			for (int i = 0; i < listPartners.size(); i++) {
				pModel = listPartners.get(i);
				text = format.format(pModel.getDensity());
				enableAndSetText(jtfDensities[pModel.getTypePartnerId()-1], text);

			}
		}
	}

	/**
	 * Enable a JTextField and set a text to it.
	 *
	 * @param textField The JTextField to change properties.
	 * @param textToSet The new text to set.
	 */
	private void enableAndSetText(JTextField textField, String textToSet) {
		textField.setEnabled(true);
		textField.setText(textToSet);
	}

	/**
	 * This method initializes this.
	 */
	private void initialize() {
		this.setSize(306, 480);
		this.setTitle("Collisions Files and Partners Densities");
		this.setContentPane(getJContentPane());
	}
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel(new BorderLayout());
			JPanel topPanel = new JPanel(new BorderLayout());
			topPanel.setPreferredSize(new Dimension(100, 75));
			topPanel.setBorder(BorderFactory.createTitledBorder("Collision Files:"));
			topPanel.add(getJComboBox(), BorderLayout.NORTH);
			JPanel topBottomPanel = new JPanel(new BorderLayout());
			topBottomPanel.add(getAddButton(), BorderLayout.WEST);
			topBottomPanel.add(getDeleteButton(), BorderLayout.EAST);
			topPanel.add(topBottomPanel, BorderLayout.CENTER);
			jContentPane.add(topPanel, BorderLayout.NORTH);

			final GridLayout gridLayout = new GridLayout(collisionners.length,  2);
			gridLayout.setHgap(0);

			JPanel middlePanel = new JPanel(gridLayout);
			middlePanel.setBorder(BorderFactory.createTitledBorder("Partners Densities:"));
			jtfDensities = new JTextField[collisionners.length];
			for (int i = 0; i < collisionners.length; i++) {
				JLabel jLabel = new JLabel(collisionners[i]);
				middlePanel.add(jLabel);
				JTextField jTextField = new JTextField();
				jtfDensities[i] = jTextField;
				middlePanel.add(jtfDensities[i]);
			}
			JPanel middlePanel1 = new JPanel(new BorderLayout());
			middlePanel.setPreferredSize(new Dimension(300, 200));
			middlePanel1.add(middlePanel, BorderLayout.WEST);
			jContentPane.add(middlePanel1, BorderLayout.CENTER);
			JPanel bottomPanel = new JPanel();
			bottomPanel.setPreferredSize(new Dimension(100, 75));
			bottomPanel.add(getJbtnSave());
			bottomPanel.add(getJbtnCancel());
			jContentPane.add(bottomPanel, BorderLayout.SOUTH);
		}
		return jContentPane;
	}

	/**
	 * @return
	 */
	public JButton getAddButton() {
		if (addButton == null) {
			addButton = new JButton("Add new");
			addButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					File selectedFile = selectCollisionFile();

					// Read file
					if (selectedFile != null) {
						Software.setLastFolder(LAST_COLLISION_FOLDER, selectedFile.getParent());
						addColisionFile(selectedFile);
					}
				}
			});
		}
		return addButton;
	}

	/**
	 * This method initializes jButton1
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getDeleteButton() {
		if (deleteButton == null) {
			deleteButton = new JButton("Detach");
			deleteButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					int selectIndex = jComboBox.getSelectedIndex();
					if (selectIndex != -1) {
						String fileName = jComboBox.getItemAt(selectIndex);
						deleteCollisionFile(selectIndex, fileName);
					}
				}
			});
		}
		return deleteButton;
	}

	/**
	 * Delete the given collision file.
	 *
	 * @param selectIndex The index of the file in the JList.
	 * @param collisionFile The file name.
	 */
	private void deleteCollisionFile(int selectIndex, String collisionFile) {
		boolean toDelete = true;
		try {
			if (Software.getRadexMoleculesManager().isInCollisionCassisPath(collisionFile)) {
				JOptionPane.showMessageDialog(RADEXPartnerDialog.this,
						"Unable to delete a collision file in default Cassis collision folder.",
						"Alert", JOptionPane.ERROR_MESSAGE);
				toDelete = false;
			} else if (Software.getRadexMoleculesManager().deleteLamdaFileInCassis(collisionFile, molDescription.getTag())){
				Path path = Paths.get(Software.getUserCollisionFiles());
				boolean removed = false;
				try {
					List<String> readAllLines = Files.readAllLines(path, StandardCharsets.UTF_8);
					for (Iterator<String> iterator = readAllLines.iterator(); ! removed && iterator.hasNext();) {
						String file = (String) iterator.next();
						if (file.endsWith(collisionFile)) {
							iterator.remove();
							removed = true;
						}
					}
					if (removed) {
						Files.write(path, readAllLines, StandardCharsets.UTF_8);
					}

				} catch (IOException e) {
					LOGGER.error("Error when reading or writing " + path.toString(), e);
				}
			}
			if (toDelete) {
				jComboBox.removeItemAt(selectIndex);
				if (jComboBox.getSelectedIndex() == -1) {
					molDescription.setCollision(Molecule.NO_COLLISION);
				}
			}
		} catch (IOException ioe) {
			LOGGER.error("Unable to delete collision file", ioe);
			JOptionPane.showMessageDialog(this, "Unable to read config file. "
					+ ioe.getMessage(), "Alert", JOptionPane.ERROR_MESSAGE);
		}
	}


	/**
	 * Add the given collision file.
	 *
	 * @param selectedFile The file to add.
	 */
	private void addColisionFile(File selectedFile) {
		try {
			boolean add = addNewCollisionFile(selectedFile,
					Software.getRadexMoleculesManager().subStringCommaInName(molDescription.getName()),
					Integer.toString(molDescription.getTag()));
			if (add) {
				listMolesFiles.add(selectedFile.getName());
				jComboBox.addItem(selectedFile.getName());
			}
			String collision = Software.getRadexMoleculesManager().getCollisionParDefault(molDescription.getTag());
			molDescription.setCollision(collision);
		} catch (IOException exc) {
			LOGGER.error("Unable to add collision file", exc);
			JOptionPane.showMessageDialog(this, "Unable to read config file. "
					+ exc.getMessage(), "Alert", JOptionPane.ERROR_MESSAGE);
		} catch (MoleculeException me) {
			LOGGER.error("The collision file is invalide.", me);
			JOptionPane.showMessageDialog(this,
					"Collision file invalide. " + me.getMessage(), "Alert", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Open a JFileChooser to select a lamda file.
	 *
	 * @return the selected file or null if the user cancelled the selection.
	 */
	private File selectCollisionFile() {
		JFileChooser fc = new CassisJFileChooser(Software.getCassisRadexCollisionsPath(),
				Software.getLastFolder(LAST_COLLISION_FOLDER));
		fc.setDialogTitle("Add new collision file:");
		fc.resetChoosableFileFilters();
		fc.addChoosableFileFilter(new FileNameExtensionFilter("Collision Lamda File (*.dat)","dat"));
		int val = fc.showOpenDialog(this);
		File selectedFile = val == JFileChooser.APPROVE_OPTION ?
				fc.getSelectedFile() : null;
		return selectedFile;
	}



	/**
	 * This method initializes jComboBox
	 *
	 * @return javax.swing.JComboBox
	 */
	@SuppressWarnings("unchecked")
	private JComboBox<String> getJComboBox() {
		if (jComboBox == null) {
			jComboBox = new JComboBox<>(listMolesFiles.toArray(new String[listMolesFiles.size()]));
			jComboBox.setRenderer(new MyComboBoxRenderer());
			jComboBox.addMouseMotionListener(new MouseMotionAdapter() {

				@Override
				public void mouseMoved(MouseEvent e) {
					JComboBox<String> l = (JComboBox<String>) e.getSource();
					ListModel<String> m = l.getModel();
					int index = l.getSelectedIndex();
					if (index > -1) {
						final String elementAt = m.getElementAt(index);
						final File file = Software.getRadexMoleculesManager().getPathCollisionFile(elementAt);
						l.setToolTipText(file.getAbsolutePath());
					}
				}

			});
			jComboBox.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					String selection = getSelectedCollision();
					if (!moleFile.equals(selection) &&
							!Molecule.NO_COLLISION.equals(selection)) {
						handleCollisionFileSelection();
					} else if (!moleFile.equals(selection) &&
							Molecule.NO_COLLISION.equals(selection)) {
						handleNoCollisionSelection();
					}
				}
			});
		}
		return jComboBox;
	}

	@SuppressWarnings("serial")
	class MyComboBoxRenderer extends BasicComboBoxRenderer {

		public Component getListCellRendererComponent(@SuppressWarnings("rawtypes") JList list, Object value, int index, boolean isSelected,
				boolean cellHasFocus) {
			if (isSelected) {
				setBackground(list.getSelectionBackground());
				setForeground(list.getSelectionForeground());
				if (-1 < index) {
					final String elementAt = (String)list.getModel().getElementAt(index);
					final File file = Software.getRadexMoleculesManager().getPathCollisionFile(elementAt);
					list.setToolTipText(file.getAbsolutePath());
				}
			} else {
				setBackground(list.getBackground());
				setForeground(list.getForeground());
			}
			setFont(list.getFont());
			setText((value == null) ? "" : value.toString());
			return this;
		}
	}
	/**
	 * Return the selected Collision file as a String.
	 *
	 * @return the selected Collision file as a String.
	 */
	private String getSelectedCollision() {
		return (String) getJComboBox().getSelectedItem();
	}
	/**
	 * Handle the selection of a collision file.
	 */
	private void handleCollisionFileSelection() {
		moleFile = getSelectedCollision();
		listPartners = Software.getRadexMoleculesManager().getPartners(moleFile);
		if(listPartners != null) {
			for (int i = 0; i < listPartners.size(); i++)
				listPartners.get(i).setDensity(EmAbTabbedPanelView.DEFAULT_COLLISION_PARTNER_DENSITY);
		}

		initializeValue();
	}

	/**
	 * Handle the selection of a "-no-" collision file.
	 */
	private void handleNoCollisionSelection() {
		for (int i = 0; i < collisionners.length; i++) {
			jtfDensities[i].setEnabled(false);
			jtfDensities[i].setText("");
		}
	}

	/**
	 * This method initializes jbtnSave
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJbtnSave() {
		if (jbtnSave == null) {
			jbtnSave = new JButton();
			jbtnSave.setPreferredSize(new Dimension(100, 38));
//			jbtnSave.setBounds(new Rectangle(46, 390, 101, 38));
			jbtnSave.setText("Save");
			jbtnSave.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						RADEXPartnerDialog.this.checkForm();
					} catch (Exception ex) {
						LOGGER.warn("{}", ex.getMessage(), ex);
						JOptionPane.showMessageDialog(RADEXPartnerDialog.this, ex.getMessage(), "Data error",
								JOptionPane.ERROR_MESSAGE);
						return;
					}
					String collision = RADEXPartnerDialog.this.getDataForm2();
					if (!collision.isEmpty())
						RADEXPartnerDialog.this.molDescription.setCollision(collision);

					RADEXPartnerDialog.this.dispose();
					if (RADEXPartnerDialog.this.listMolesFiles.contains(Molecule.NO_COLLISION)) {
						RADEXPartnerDialog.this.listMolesFiles.remove(Molecule.NO_COLLISION);
					}
					RADEXPartnerDialog.dialog = null;
				}
			});
		}
		return jbtnSave;
	}

	/**
	 * This method initializes jbtnCancel
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJbtnCancel() {
		if (jbtnCancel == null) {
			jbtnCancel = new JButton();
			jbtnCancel.setPreferredSize(new Dimension(100, 38));
//			jbtnCancel.setBounds(new Rectangle(158, 389, 101, 38));
			jbtnCancel.setText("Cancel");
			jbtnCancel.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					RADEXPartnerDialog.this.dispose();
					RADEXPartnerDialog.dialog = null;
				}
			});
		}
		return jbtnCancel;
	}

	/**
	 * @return the dataFormated
	 */
	public String getDataForm2() {
		String res = Molecule.NO_COLLISION;
		Object selectedItem = jComboBox.getSelectedItem();

		if (selectedItem != null) {
			StringBuilder sb = new StringBuilder((String) selectedItem);

			for (int i = 0; i < listPartners.size(); i++) {
				PartnerModel pModel = listPartners.get(i);
				if (pModel.getTypePartnerId() >= 1 && pModel.getTypePartnerId() <= 7) {
					sb.append("&(").append(pModel.getTypePartner()).append(";");
					sb.append(jtfDensities[pModel.getTypePartnerId()-1].getText());

					sb.append(")");
				}
			}
			res = sb.toString();
		}

		return res;
	}

	/**
	 * Check if the form is correct.
	 *
	 * @return true if the form is OK.
	 * @throws Exception If there is an error with the form.
	 */
	public boolean checkForm() throws Exception {
		String message = "";

		for (int i = 0; i < collisionners.length; i++) {
			if (jtfDensities[i].isEnabled()) {
				try {
					String density = jtfDensities[i].getText();
					double dDensity = Double.parseDouble(density);
					if (dDensity < 1e-3 || dDensity > 1e13)
						throw new NumberFormatException();
				} catch (NumberFormatException e) {
					message += "Density of "
							+ collisionners[i]
							+ ": Please enter a double value between 1e-3 and 1e13 \n";
				}
			}
		}

		if (!message.isEmpty()) {
			throw new Exception(message);
		}

		return true;
	}

	public static void main(String[] args) {
		MoleculeDescription mol = new MoleculeDescription();

		mol.setTag(28601);
		mol.setCollision("-yes-");
		List<String> filesColission = new ArrayList<>();
		filesColission.add("co_high_j.dat");
		RADEXPartnerDialog dialog = RADEXPartnerDialog.getInstance(mol, filesColission);
		dialog.setVisible(true);
	}

	/**
	 * Add a new collision file.
	 *
	 * @param collisionFile The file to add.
	 * @param cassisMoleName The name of the molecule.
	 * @param tag The tag of the molecule.
	 * @throws MoleculeException If there is problem with the molecule.
	 * @throws IOException If there is an IO problem.
	 * return True if a new collision reference is added in molesRADEXCASSIS
	 */
	public boolean addNewCollisionFile(File collisionFile, String cassisMoleName, String tag)
			throws MoleculeException, IOException {
		boolean addCollision = false;
		if (validateCollisionFile(collisionFile)) {
			List<PartnerModel> listPartners = RadexUtils.getCollisionsPartners(
					collisionFile.getParent(), collisionFile.getName());

			StringBuilder collision = new StringBuilder();
			collision.append(collisionFile.getName());
			for (int i = 0; i < listPartners.size(); i++) {
				collision.append("&(" + listPartners.get(i).getTypePartner() + ";1e4)");
			}
			boolean duplicate = Software.getRadexMoleculesManager().isAlreadyExist(collisionFile.getName());

			if (Software.getRadexMoleculesManager().addNewRADEXMoleculeIntoCASSIS(
					collisionFile, cassisMoleName, tag, collision.toString())) {
				addCollision = true;
				if (!duplicate) {
					addLamdaUserFile(collisionFile, Software.getUserCollisionFiles());
				}

			} else {
				throw new MoleculeException("Name of the lamda file already exists");
			}
		} else {
			throw new MoleculeException(collisionFile.getName());
		}
		return addCollision;
	}

	/**
	 * Validate the collision file and display an error dialog if needed.
	 *
	 * @param collisionFile The collision file.
	 * @return true for success, false otherwise.
	 */
	private boolean validateCollisionFile(File collisionFile) {
		try {
			RadexUtils.validateCollisionFileWithException(collisionFile);
			return true;
		} catch (OutOfBoundEnergyLevelsException e) {
			displayError(e.getEnergyLevelNumber() + " energy levels in this file.\n"
					+ "The max number of energy level is fixed to "
					+ ReadRadexFile.getMaxLev()
					+ ", please modify the bounds in \"Preferences/Radex\".");
		} catch (OutOfBoundSpectralLinesException e) {
			displayError(e.getLinesNumber() + " colliders in this file.\n"
					+ "The max number of collider is fixed to "
					+ ReadRadexFile.getMaxLine()
					+ ", please modify the bounds in \"Preferences/Radex\".");
		} catch (OutOfBoundCollisionPartnersException e) {
			displayError(e.getCollisionsPartnersNumber() + " collisional transitions in this file.\n"
					+ "The max number of collisional transitions if fixed to"
					+ ReadRadexFile.getMaxColl()
					+ ", please modify the bounds in \"Preferences/Radex\".");
		} catch (OutOfBoundCollisionRatesException e) {
			displayError(e.getCollisionNumber() + " collision rates in this file.\n"
					+ "The max num of collision rates is fixed to "
					+ ReadRadexFile.getMaxColl() +
					", please modify the bounds in \"Preferences/Radex\".");
		} catch (OutOfBoundCollisionTemperatureException e) {
			displayError(e.getTemperatureNumber() + " temperatures in this file.\n"
					+ "The max number of temperature is fixed to "
					+ ReadRadexFile.getMaxTemp() +
					", please modify the bounds in \"Preferences/Radex\".");
		} catch (IOException e) {
			displayError("Unable to read the given collision file.");
		}
		return false;
	}

	/**
	 * Display an error dialog.
	 *
	 * @param message The message to display.
	 */
	private void displayError(String message) {
		JOptionPane.showMessageDialog(this, message, "Collision file error",
				JOptionPane.ERROR_MESSAGE);
	}

	private void addLamdaUserFile(File collisionFile, String allLamdaFiles) {
		Path path = Paths.get(allLamdaFiles);
		try {
			List<String> readAllLines = Files.readAllLines(path, StandardCharsets.UTF_8);
			readAllLines.add(collisionFile.getAbsolutePath());
			 Files.write(path, readAllLines, StandardCharsets.UTF_8);
		} catch (IOException e) {
			LOGGER.error("Error when reading or writing " + path.toString(), e);
		}
	}
}
