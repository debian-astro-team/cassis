/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.border.TitledBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.gui.util.CassisRessource;
import eu.omp.irap.cassis.gui.util.CassisUtilGui;

/** TODO rename to LteRadexComponentsView. */
public class EmAbTabbedPanelView extends JPanel{

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(EmAbTabbedPanelView.class);

	public static final int DENSITY_COLUMN_INDEX = 5;
	public static final int ABUNDANCE_COLUMN_INDEX = 6;

	public static final double DEFAULT_COLLISION_PARTNER_DENSITY = 1.E04;

	private static final int CLONE = 0;
	private static final int MOVE = 1;

	private static final int POS_FIRST = 0;
	private static final int POS_PREVIOUS = 1;
	private static final int POS_NEXT = 2;
	private static final int POS_LAST = 3;

	private EmAbTabbedPanelControl control;
	private JTabbedPane tabbedPane;


	public EmAbTabbedPanelView() {
		this(new EmAbTabbedPanelModel());
	}

	/**
	 * Constructor.
	 *
	 * @param model The model.
	 */
	public EmAbTabbedPanelView(EmAbTabbedPanelModel model) {
		this.control = new EmAbTabbedPanelControl(model, this);

		setMinimumSize(new Dimension(970, 350));
		setPreferredSize(new Dimension(970, 350));
		setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		setBorder(new TitledBorder(""));

		setLayout(new BorderLayout());
		add(getTabbedPane());
		if (model.getComponentList().isEmpty()) {
			model.addContinuumComponent(true);
			model.addNewComponent();
		} else {
			for (ComponentDescription cd : model.getComponentList()) {
				if (cd.isContinuum()) {
					addTabContinuum((ComponentContinuum)cd, false);
				} else {
					addTab((ComponentSpecies)cd);
				}
			}
		}
	}

	public JTabbedPane getTabbedPane() {
		if (tabbedPane == null) {
			tabbedPane = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.SCROLL_TAB_LAYOUT);
			tabbedPane.insertTab("",  CassisRessource.createImageIcon("newtab.gif"), null, "add a component", 0);
			tabbedPane.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					control.tabbedClicked(e);
				}
			});

		}
		return tabbedPane;
	}

	public void addTabContinuum(final ComponentContinuum componentDescription, boolean inFirst) {
			int index = tabbedPane.getTabCount()-1;
			if (inFirst) {
				index = 0;
			}
			final ComponentContinuumPanel component =
						new ComponentContinuumPanel((ComponentContinuum)componentDescription);
			getTabbedPane().insertTab(componentDescription.getComponentName(), null,
					component, null, index);

	}

	public void addTab(final ComponentSpecies componentSpecies) {
		final ButtonTabComponent buttonTab = new ButtonTabComponent(componentSpecies.getComponentName());
		final ComponentSpeciesPanel component =
				new ComponentSpeciesPanel(componentSpecies, buttonTab);
		getTabbedPane().insertTab(componentSpecies.getComponentName(), null,
				component, null, tabbedPane.getTabCount()-1);

		getTabbedPane().setTabComponentAt(tabbedPane.getTabCount()-2,component.getBTC());
		getTabbedPane().setSelectedComponent(component);
		setButtonListener(buttonTab, component, componentSpecies);
		componentSpecies.addModelListener(control);
	}

	private void setButtonListener(final ButtonTabComponent buttonTab, final ComponentSpeciesPanel component, final ComponentDescription componentDescription) {
		buttonTab.getButtonDelete().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (control.getModel().getComponentList().size() > 1) {
					if (getTabbedPane().indexOfComponent(component) == control.getModel().getComponentList().size() - 1) {
						getTabbedPane().setSelectedIndex(getTabbedPane().indexOfComponent(component) - 1);
					}
					control.getModel().removeComponent(componentDescription);
					getTabbedPane().remove(component);
					renameTabs();
					component.removeAllListeners();
				}
			}
		});

		buttonTab.getButtonDelete().addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				getTabbedPane().setSelectedComponent(component);
			}
		});

		buttonTab.getEnableCheckBox().addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				getTabbedPane().setSelectedComponent(component);
			}
		});

		buttonTab.getEnableCheckBox().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				CassisUtilGui.setComponentEnabled(component, buttonTab.getEnableCheckBox().isSelected());
				if (buttonTab.getEnableCheckBox().isSelected()) {
					component.changeStateGeometryBox();
				}
				componentDescription.setEnabled(buttonTab.getEnableCheckBox().isSelected());
			}
		});
	}

	private List<JMenuItem> createSubMenu(int index, int nbTab, int type) {
		List<JMenuItem> listMenuItem = new ArrayList<>();
		if (index >= 1 || type == CLONE) {
			JMenuItem firstItem = new JMenuItem("First");
			addListener(firstItem, type, index, nbTab, POS_FIRST);
			listMenuItem.add(firstItem);

			JMenuItem previousItem = new JMenuItem("Previous");
			addListener(previousItem, type, index, nbTab, POS_PREVIOUS);
			listMenuItem.add(previousItem);
		}

		if (index < nbTab - 1 || type == CLONE) {
			JMenuItem nextItem = new JMenuItem("Next");
			addListener(nextItem, type, index, nbTab, POS_NEXT);
			listMenuItem.add(nextItem);

			JMenuItem lastItem = new JMenuItem("Last");
			addListener(lastItem, type, index, nbTab, POS_LAST);
			listMenuItem.add(lastItem);
		}

		return listMenuItem;
	}

	private void addListener(JMenuItem menuItem, final int type, final int numTab, final int nbTab, final int pos) {
		menuItem.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent me) {
				if (me.getButton() == MouseEvent.BUTTON1) {
					if (type == CLONE) {
						doClone(numTab, nbTab, pos);
					} else if (type == MOVE) {
						doMove(numTab, nbTab, pos);
					} else {
						LOGGER.error("Error in addListener from EmAbTabbedPanelView: wrong type used.");
					}
					renameTabs();
				}
			}
		});

	}

	/**
	 * Compute the destination position then move a tab.
	 *
	 * @param indexTab The index of the tab to move.
	 * @param nbTab The number of tab.
	 * @param typeMove The type of move to do.
	 */
	private void doMove(final int indexTab, final int nbTab, final int typeMove) {
		int posDest = -1;
		switch (typeMove) {
			case POS_FIRST:
				posDest = 0;
				break;
			case POS_PREVIOUS:
				posDest = indexTab - 1;
				break;
			case POS_NEXT:
				posDest = indexTab + 1;
				break;
			case POS_LAST:
				posDest = nbTab - 1;
				break;
			default:
				LOGGER.error("Error: wrong type pos used");
				break;
		}
		if (posDest != -1) {
			moveTab(indexTab, posDest);
		}
	}

	/**
	 * Compute the destination position then move a tab.
	 *
	 * @param indexTab The index of the tab to clone.
	 * @param nbTab The number of tab.
	 * @param typeMove The type of move to do.
	 */
	private void doClone(final int indexTab, final int nbTab, final int typeMove) {
		int posDest = -1;
		switch (typeMove) {
			case POS_FIRST:
				posDest = 0;
				break;
			case POS_PREVIOUS:
				posDest = indexTab;
				break;
			case POS_NEXT:
				posDest = indexTab + 1;
				break;
			case POS_LAST:
				posDest = nbTab;
				break;
			default:
				LOGGER.error("Error: wrong type pos used");
				break;
		}
		if (posDest != -1) {
			cloneTab(indexTab, posDest);
		}
	}

	public void renameTabs() {
		int i = 1;

		for (ComponentDescription cds : control.getModel().getComponentList()) {
			if (!cds.isContinuum()) {
				cds.setComponentName("Component " + i);
				i++;
			}

		}
	}

	public void cloneTab(int numTab, int numTabDest) {
		int numTabDestTemp = control.getModel().getComponentList().size();

		ComponentSpeciesPanel component = (ComponentSpeciesPanel) tabbedPane.getComponentAt(numTab);
		ComponentDescription newComponentModel = (ComponentDescription) component.getComponentDescription().clone();
		String txt = "Clone of " + newComponentModel.getComponentName();
		newComponentModel.setComponentName(txt);
		ButtonTabComponent buttonTab = new ButtonTabComponent(txt);
		ComponentSpeciesPanel newComponentPanel = new ComponentSpeciesPanel(newComponentModel, buttonTab);
		setButtonListener(newComponentPanel.getBTC(), newComponentPanel, newComponentModel);

		tabbedPane.insertTab(txt, null, newComponentPanel, null, numTabDestTemp);
		tabbedPane.setTabComponentAt(numTabDestTemp, newComponentPanel.getBTC());
		control.getModel().addComponentManually(newComponentModel);
		newComponentModel.addModelListener(control);

		// Restore state of the panel.
		Boolean buttonTabOriginState = newComponentModel.isEnabled();
		newComponentPanel.getBTC().getEnableCheckBox().setSelected(buttonTabOriginState);
		CassisUtilGui.setComponentEnabled(newComponentPanel, buttonTabOriginState);
		if (buttonTabOriginState) {
			newComponentPanel.changeStateGeometryBox();
		}

		moveTab(control.getModel().getComponentList().size() - 1, numTabDest);
	}

	public void moveTab(int numTab, int numTabDest) {
		ComponentDescriptionPanel component = (ComponentDescriptionPanel) tabbedPane.getComponentAt(numTab);
		ComponentDescription cd = component.getComponentDescription();
		String txtTab = cd.getComponentName();

		if (!cd.isContinuum()) {
			setButtonListener(component.getBTC(), (ComponentSpeciesPanel)component, cd);
		}


		tabbedPane.remove(numTab);

		tabbedPane.insertTab(txtTab, null, component, null, numTabDest);
		tabbedPane.setTabComponentAt(numTabDest, component.getBTC());

		control.getModel().getComponentList().remove(cd);
		control.getModel().getComponentList().add(numTabDest, cd);

		getTabbedPane().setSelectedComponent(component);

		// Interacting
		boolean interacting = cd.isInteracting();
		if (interacting) {
			if (numTabDest != control.getModel().getComponentList().size() - 1
					&& !control.getModel().getComponentList().get(numTabDest + 1).isInteracting()) {
				cd.setInteracting(false);
			}
		} else {
			if (numTabDest != 0 && control.getModel().getComponentList().get(numTabDest - 1).isInteracting()) {
				cd.setInteracting(true);
			}
		}
//		changeStateOfContinuums();
	}

	public void showPopup(MouseEvent event) {
		final JTabbedPane tabPane = (JTabbedPane) event.getSource();
		 // Don't count "new tab" tab here as it is a special tab.
		int nbTab = tabPane.getTabCount() - 1;
		int index = tabPane.getUI().tabForCoordinate(tabPane, event.getX(), event.getY());
		ComponentDescription componentDescription = control.getModel().getComponentList().get(index);
		JPopupMenu popupMenu = new JPopupMenu();
		if (!componentDescription.isContinuum()) {
			JMenu cloneMenu = new JMenu("Clone to");
			List<JMenuItem> listCloneItem = createSubMenu(index, nbTab, CLONE);
			for (JMenuItem item : listCloneItem) {
				cloneMenu.add(item);
			}
			popupMenu.add(cloneMenu);
		}

		if (nbTab >= 2) {
			JMenu moveMenu = new JMenu("Move to");
			List<JMenuItem> listMoveItem = createSubMenu(index, nbTab, MOVE);
			for (JMenuItem item : listMoveItem) {
				moveMenu.add(item);
			}
			popupMenu.add(moveMenu);
		}

		popupMenu.show(event.getComponent(), event.getX(), event.getY());
	}

	public void disableComponent(ComponentDescription component) {
		int nb = tabbedPane.getComponentCount() - 1;
		for (int i = 0; i < nb; i++) {
			ComponentDescriptionPanel compPanel =  (ComponentDescriptionPanel) tabbedPane.getComponentAt(i);
			if (compPanel.getComponentDescription() == component) {
				compPanel.getBTC().getEnableCheckBox().setSelected(false);
				CassisUtilGui.setComponentEnabled(compPanel, false);
				break;
			}
		}
	}

//	public void changeStateOfContinuums() {
//		int nbInteractingFound = 0;
//		int nbComponent = getTabbedPane().getTabCount() - 1;
//		for (int i = 0; i < nbComponent; i++) {
//			ComponentDescriptionPanel cdpt = (ComponentDescriptionPanel) getTabbedPane().getComponentAt(i);
//			ComponentDescription cd = cdpt.getComponentDescription();
//
//			if (!cd.isInteracting()) {
//				cdpt.enableContinuum();
//			} else {
//				nbInteractingFound++;
//				if (nbInteractingFound == 1) {
//					cdpt.enableContinuum();
//				} else {
//					cdpt.disableContinuum();
//				}
//			}
//		}
//	}

	public EmAbTabbedPanelControl getControl() {
		return control;
	}

	/**
	 * Refresh the view.
	 */
	public void refresh() {
		while (getTabbedPane().getTabCount() > 1) {
			Component comp = getTabbedPane().getComponentAt(0);
			getTabbedPane().remove(0);
			if (comp instanceof ComponentDescriptionPanel) {
				ComponentDescriptionPanel cdp = (ComponentDescriptionPanel) comp;
				cdp.removeAllListeners();
			}
		}

		// Add the ones from the model.
		for (ComponentDescription cd : control.getModel().getComponentList()) {
			if (cd.isContinuum()) {
				addTabContinuum((ComponentContinuum)cd, false);
			} else {
				addTab((ComponentSpecies)cd);
			}

			if (!cd.isEnabled()) {
				disableComponent(cd);
			}
		}

		renameTabs();
		repaint();
	}

}
