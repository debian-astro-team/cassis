/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.BufferedWriterProperty;
import eu.omp.irap.cassis.common.ComponentAlgo;
import eu.omp.irap.cassis.common.ComponentInterface;
import eu.omp.irap.cassis.common.ComponentMode;
import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.Molecule;
import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.gui.model.table.CassisTableMolecule;
import eu.omp.irap.cassis.gui.model.table.CassisTableMoleculeModel;
import eu.omp.irap.cassis.gui.template.JComboBoxTemplate;
import eu.omp.irap.cassis.properties.Software;

public class ComponentSpecies extends ComponentDescription implements  ComponentInterface {

	public static final String ADD_A_LINE_EVENT = "addALine";
	public static final String CHANGE_ON_MOLECULE_TABLE_EVENT = "changeOnMoleculesTable";
	public static final String COMPONENT_ALGO_EVENT = "componentAlgo";
	public static final String COMPONENT_MODE_EVENT = "componentMode";
	public static final String DENSITY_EVENT = "density";
	public static final String LIST_OF_LINES_EVENT = "listOfLines";
	public static final String SELECTED_TEMPLATE_EVENT = "selectedTemplate";
	public static final String VLSR_EVENT = "vlsr";
	public static final String VLSR_UNIT_EVENT = "vlsrUnit";

	private CassisTableMoleculeModel cassisTableMoleculeModel;
	private Double density = 7.5E22;
	private Double vlsr = 0.0;
	private UNIT vlsrUnit = UNIT.KM_SEC_MOINS_1;


	private ComponentMode componentMode = ComponentMode.SPHERE_MODE;
	private ComponentAlgo componentAlgo = ComponentAlgo.FULL_LTE_ALGO;


	public static final String VLSR_FILE_STRING = "Vlsr File";

	private ArrayList<LineDescription> listOfLines;
	private String selectedTemplate = JComboBoxTemplate.OPERATIONS_TEMPLATE;


	private static final String[] columnNames = { "Species", "Tag", "Database",
			"Collision", "Compute", "N(Sp) (cm\u207B\u00B2)", "Abundance (/H2)",
			"Tex (K)", "TKin (K)", "FWHM (km/s)", "Size (\")" };

	private static final Logger LOGGER = LoggerFactory.getLogger(ComponentSpecies.class);


	public ComponentSpecies(String componentName, ArrayList<MoleculeDescription> moleculeList, Double density,
			Double vlsr) {
		super(componentName);
		this.cassisTableMoleculeModel = new CassisTableMoleculeModel(moleculeList,
				columnNames, new Integer[] { CassisTableMolecule.NAME_INDICE,
						CassisTableMolecule.TAG_INDICE, CassisTableMolecule.DATA_SOURCE_INDICE,
						CassisTableMolecule.COLLISION_INDICE, CassisTableMolecule.COMPUTE_INDICE,
						CassisTableMolecule.DENSITY_INDICE, CassisTableMolecule.RELATIVE_ABUNDANCE_INDICE,
						CassisTableMolecule.TEMPERATURE_INDICE, CassisTableMolecule.TKIN_INDICE,
						CassisTableMolecule.VELOCITY_DISPERSION_INDICE, CassisTableMolecule.SOURCE_SIZE_INDICE });
		this.density = density;
		this.vlsr = vlsr;
		this.listOfLines = new ArrayList<>();
	}

	public UNIT getVlsrUnit() {
		return vlsrUnit;
	}

	public void setVlsrUnit(UNIT unitVlsr) {
		vlsrUnit = unitVlsr;
		fireDataChanged(new ModelChangedEvent(VLSR_UNIT_EVENT, this.vlsrUnit));
	}

	public Double getDensity() {
		return density;
	}

	public void setDensity(Double density) {
		this.density = density;
		fireDataChanged(new ModelChangedEvent(DENSITY_EVENT, this.density));
	}

	public List<MoleculeDescription> getMoleculeList() {
		return getCassisTableMoleculeModel().getList();
	}

	public void keepOnlyComputeMoleculeList() {
		getCassisTableMoleculeModel().setList(
				getCassisTableMoleculeModel().getSelectedMolecules());
	}

	/**
	 * @return the cassisTableMoleculeModel
	 */
	public CassisTableMoleculeModel getCassisTableMoleculeModel() {
		return cassisTableMoleculeModel;
	}

	public Double getVlsr() {
		return vlsr;
	}

	public void setVlsr(Double vlsr) {
		this.vlsr = vlsr;
		fireDataChanged(new ModelChangedEvent(VLSR_EVENT, this.vlsr));
	}

	/**
	 * Clear the list of lines.
	 */
	public void clearListOfLines() {
		this.listOfLines.clear();
		fireDataChanged(new ModelChangedEvent(LIST_OF_LINES_EVENT, this.listOfLines));
	}

	/**
	 * Set list of lines for this component.
	 *
	 * @param listLines The list of lines to set.
	 */
	public void setListOfLines(ArrayList<LineDescription> listLines) {
		this.listOfLines = listLines;
		fireDataChanged(new ModelChangedEvent(LIST_OF_LINES_EVENT, this.listOfLines));
	}

	/**
	 * Return the list of lines for this component.
	 */
	@Override
	public ArrayList<LineDescription> getListOfLines() {
		return this.listOfLines;
	}

	/**
	 * Add a line for this component.
	 *
	 * @param line The line to add.
	 */
	public void addALine(LineDescription line) {
		this.listOfLines.add(line);
		fireDataChanged(new ModelChangedEvent(ADD_A_LINE_EVENT, line));
	}

	public void addAMolecule(MoleculeDescription mol) {
		mol.setCompute(true);
		boolean alreadyInList = mol.isIn((List<MoleculeDescription>) this.cassisTableMoleculeModel.getList());

		if (!alreadyInList) {
			mol.setCompute(true);
			this.cassisTableMoleculeModel.add(mol);
			fireDataChanged(new ModelChangedEvent(CHANGE_ON_MOLECULE_TABLE_EVENT));
		}
		else {
			for (MoleculeDescription molecule : this.cassisTableMoleculeModel.getList()) {
				if (molecule.isSameMolecule(mol)) {
					molecule.setCompute(true);
					cassisTableMoleculeModel.fireTableDataChanged();
					fireDataChanged(new ModelChangedEvent(CHANGE_ON_MOLECULE_TABLE_EVENT));
					return;
				}
			}
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public Object clone() {
		ComponentSpecies compo = null;
		compo = (ComponentSpecies) super.clone();
		if (compo != null) {
			ArrayList<MoleculeDescription> moleculeList = null;
			if (getMoleculeList() != null) {
				moleculeList = new ArrayList<>();
				for (MoleculeDescription mol : getMoleculeList()) {
					moleculeList.add((MoleculeDescription) mol.clone());
				}
			}
			compo.cassisTableMoleculeModel = new CassisTableMoleculeModel(new ArrayList<>(),
					columnNames, new Integer[] { CassisTableMolecule.NAME_INDICE,
					CassisTableMolecule.TAG_INDICE, CassisTableMolecule.DATA_SOURCE_INDICE,
					CassisTableMolecule.COLLISION_INDICE, CassisTableMolecule.COMPUTE_INDICE,
					CassisTableMolecule.DENSITY_INDICE, CassisTableMolecule.RELATIVE_ABUNDANCE_INDICE,
					CassisTableMolecule.TEMPERATURE_INDICE, CassisTableMolecule.TKIN_INDICE,
					CassisTableMolecule.VELOCITY_DISPERSION_INDICE, CassisTableMolecule.SOURCE_SIZE_INDICE });
			compo.cassisTableMoleculeModel.setList(moleculeList);
			compo.density = density;
			compo.vlsr = vlsr;

			compo.componentMode = componentMode;
			compo.componentAlgo = componentAlgo;

			compo.listOfLines = (ArrayList<LineDescription>) listOfLines.clone();

			compo.vlsrUnit = vlsrUnit;
			compo.selectedTemplate = selectedTemplate;

		}
		return compo;
	}

	public Boolean isSlabMode() {
		return ComponentMode.SLAB_MODE.equals(componentMode);
	}

	public void setGeometryMode(ComponentMode mode) {
		this.componentMode = mode;
		fireDataChanged(new ModelChangedEvent(COMPONENT_MODE_EVENT, componentMode));
	}

	public ComponentMode getGeometryMode() {
		return this.componentMode;
	}

	public Boolean isSphereMode() {
		return ComponentMode.SPHERE_MODE.equals(componentMode);
	}

	public Boolean isExpandingSphereMode() {
		return ComponentMode.EXPANDING_SPHERE_MODE.equals(componentMode);
	}

	public boolean isLteRadexMode() {
		return ComponentAlgo.LTE_RADEX_ALGO.equals(componentAlgo);
	}

	/**
	 * @param mode
	 *            if the parameter = false, the function do nothing for a
	 *            compatibility of an old code
	 */
	public void setLteRadexMode(boolean mode) {
		if (mode) {
			this.componentAlgo = ComponentAlgo.LTE_RADEX_ALGO;
			fireDataChanged(new ModelChangedEvent(COMPONENT_ALGO_EVENT, componentAlgo));
		}
	}

	public Boolean isFullRadexMode() {
		return ComponentAlgo.FULL_RADEX_ALGO.equals(componentAlgo);
	}

	/**
	 * @param fullRadexMode
	 *            if the parameter = false, the function do nothing for a
	 *            compatibility of an old code
	 */
	public void setFullRadexMode(boolean fullRadexMode) {
		if (fullRadexMode) {
			this.componentAlgo = ComponentAlgo.FULL_RADEX_ALGO;
			fireDataChanged(new ModelChangedEvent(COMPONENT_ALGO_EVENT, componentAlgo));
		}
	}

	public void setTypeAlgo(ComponentAlgo componentAlgo) {
		this.componentAlgo = componentAlgo;
		fireDataChanged(new ModelChangedEvent(COMPONENT_ALGO_EVENT, this.componentAlgo));
	}

	public ComponentAlgo getTypeAlgo() {
		return this.componentAlgo;
	}

	public Boolean isFullLteMode() {
		return ComponentAlgo.FULL_LTE_ALGO.equals(componentAlgo);
	}

	/**
	 * @param fullLteMode
	 *            if the parameter = false, the function do nothing for a
	 *            compatibility of an old code
	 */
	public void setFullLteMode(boolean fullLteMode) {
		if (fullLteMode) {
			this.componentAlgo = ComponentAlgo.FULL_LTE_ALGO;
			fireDataChanged(new ModelChangedEvent(COMPONENT_ALGO_EVENT, componentAlgo));
		}
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ComponentDescription [componentName=");
		builder.append(getComponentName());
		builder.append(", moleculeList=");
		builder.append(getMoleculeList());
		builder.append(", density=");
		builder.append(density);
		builder.append(", vlsr=");
		builder.append(vlsr);
		builder.append(", componentMode=");
		builder.append(componentMode);
		builder.append(", componentAlgo=");
		builder.append(componentAlgo);
		builder.append(", listOfLines=");
		builder.append(listOfLines);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public void saveConfig(BufferedWriterProperty out) throws IOException {
		String prefix = getPrefix();
		super.saveConfig(out);
		if (prefix != null) {
			setPrefix(prefix);
		} else {
			prefix = getPrefix();
		}

		String vlsrString = "";

		if (vlsr.equals(Double.MAX_VALUE))
			vlsrString = ComponentSpecies.VLSR_FILE_STRING;
		else {
			vlsrString = String.valueOf(this.getVlsr());
		}

		out.writeProperty(prefix + "Algo", componentAlgo.name());
		out.writeProperty(prefix + "Density", String.valueOf(density));
		out.writeProperty(prefix + "Geometry", componentMode.name());
		out.writeProperty(prefix + "Vlsr", vlsrString);
		out.writeProperty(prefix + "VlsrUnit", String.valueOf(vlsrUnit));


		out.flush();

		exportTemplate(prefix, out, (List<MoleculeDescription>) getCassisTableMoleculeModel().getList());
	}


	public void exportTemplate(String prefix, BufferedWriterProperty out, List<MoleculeDescription> moleculeList)
			throws IOException {
		int localCpt = 0;
		out.newLine();
		// calcul du nombre de molecule
		for (int i = 0; i < moleculeList.size(); i++) {
			MoleculeDescription mol = moleculeList.get(i);
			if (mol.isCompute())
				localCpt++;
		}
		out.writeProperty(prefix + "NbMol", String.valueOf(localCpt));

		localCpt = 1;
		for (int i = 0; i < moleculeList.size(); i++) {
			MoleculeDescription mol = moleculeList.get(i);
			if (mol.isCompute()) {
				out.writeProperty(prefix + "Mol" + localCpt + "Species", String.valueOf(mol.getName()));
				out.writeProperty(prefix + "Mol" + localCpt + "Tag", String.valueOf(mol.getTag()));
				out.writeProperty(prefix + "Mol" + localCpt + "Database", String.valueOf(mol.getDataSource()));
				out.writeProperty(prefix + "Mol" + localCpt + "Collision", String.valueOf(mol.getCollision()));
				out.writeProperty(prefix + "Mol" + localCpt + "Compute", String.valueOf(mol.isCompute()));
				out.writeProperty(prefix + "Mol" + localCpt + "NSp", String.valueOf(mol.getDensity()));
				out.writeProperty(prefix + "Mol" + localCpt + "Abundance", String.valueOf(mol.getRelativeAbundance()));
				out.writeProperty(prefix + "Mol" + localCpt + "Tex", String.valueOf(mol.getTemperature()));
				out.writeProperty(prefix + "Mol" + localCpt + "TKin", String.valueOf(mol.getTKin()));
				out.writeProperty(prefix + "Mol" + localCpt + "FWHM", String.valueOf(mol.getVelocityDispersion()));
				out.writeProperty(prefix + "Mol" + localCpt + "Size", String.valueOf(mol.getSourceSize()));
				out.newLine();
				localCpt++;
				out.flush();
			}
		}
	}


	@Override
	public void loadConfig(Properties prop) throws IOException {
		String prefix = getPrefix();
		super.loadConfig(prop);

		if (prefix != null) {
			setPrefix(prefix);
		} else {
			prefix = getPrefix();
		}

		// Vlsr
		String vlsrString = prop.getProperty(prefix + "Vlsr");
		Double vlsrTemp;
		if (ComponentSpecies.VLSR_FILE_STRING.equals(vlsrString)) {
			vlsrTemp = Double.MAX_VALUE;
		} else {
			vlsrTemp = Double.valueOf(vlsrString);
		}
		this.setVlsr(vlsrTemp);

		//Vlsr unit
		String stringUnit = prop.getProperty(prefix + "VlrsUnit");
		if (stringUnit != null) {
			UNIT vlsrUnitTemp = UNIT.toUnit(stringUnit);
			if (vlsrUnitTemp != UNIT.UNKNOWN)
				this.setVlsrUnit(vlsrUnitTemp);
		}

		//Density
		String densityString = prop.getProperty(prefix + "Density");
		if (densityString != null) {
			Double densityTemp = Double.valueOf(densityString);
			this.setDensity(densityTemp);
		}

		// Algo (Mode in the panel)
		String algoString = prop.getProperty(prefix + "Algo");
		if (algoString != null) {
			ComponentAlgo algoTemp = ComponentAlgo.valueOf(algoString);
			this.setTypeAlgo(algoTemp);
		} else {
			this.setFullLteMode(Boolean.valueOf(prop.getProperty(prefix + "FullLTEMode")));

			String lteRadexModeString = prop.getProperty(prefix + "LTERADEXMode");
			if (lteRadexModeString != null) {
				Boolean lteRadexMode = Boolean.valueOf(lteRadexModeString);
				if (lteRadexMode)
					this.setFullLteMode(true);
			}
			String fullRadexModeString = prop.getProperty(prefix + "FullRADEXMode");
			if (fullRadexModeString != null) {
				Boolean fullRADEXMode = Boolean.valueOf(fullRadexModeString);
				this.setFullRadexMode(fullRADEXMode);
			}
		}

		//Geometry - Mode
		String geometryString = prop.getProperty(prefix + "Geometry");
		if (geometryString != null) {
			ComponentMode modeTemp = ComponentMode.valueOf(geometryString);
			this.setGeometryMode(modeTemp);
		} else if (prop.containsKey(prefix + "Mode")) {
			this.componentMode = ComponentMode.valueOf(prop.getProperty(prefix + "Mode"));
		} else {
			if (Boolean.valueOf(prop.getProperty(prefix + "SphereMode")))
				this.componentMode = ComponentMode.SPHERE_MODE;
			else if (Boolean.valueOf(prop.getProperty(prefix + "SlabMode")))
				this.componentMode = ComponentMode.SLAB_MODE;
			else{
				String expandingSphereModeString = prop.getProperty(prefix + "ExpandingSphereMode");
				if (expandingSphereModeString != null && Boolean.valueOf(expandingSphereModeString)) {
					this.componentMode = ComponentMode.EXPANDING_SPHERE_MODE;
				}
			}
		}

		//Interacting (true by default)
		Boolean interactingTemp = Boolean.valueOf(prop.getProperty(prefix + "Interacting", "true"));
		this.setInteracting(interactingTemp);



		// Molecules list
		getCassisTableMoleculeModel().setList(importTemplate(prefix, prop));

		// Name (toChange will be modified in EmAbTabbedPanelModel)
		String nameTemp = prop.getProperty(prefix + "Name", "toChange");
		setComponentName(nameTemp);
	}

	public ArrayList<MoleculeDescription> importTemplate(String prefix, Properties prop) {
		ArrayList<MoleculeDescription> molList = new ArrayList<>();
		int size = Integer.parseInt(prop.getProperty(prefix + "NbMol"));
		for (int cpt = 1; cpt <= size; cpt++) {
			MoleculeDescription mol = new MoleculeDescription();
			mol.setName(prop.getProperty(prefix + "Mol" + cpt + "Species"));
			mol.setTag(Integer.valueOf(prop.getProperty(prefix + "Mol" + cpt + "Tag")));
			mol.setDataSource(prop.getProperty(prefix + "Mol" + cpt + "Database"));
			String collision = prop.getProperty(prefix + "Mol" + cpt + "Collision");
			if ("-none-".equals(collision))
				collision = Molecule.NO_COLLISION;
			if ("-auto-".equals(collision))
				collision = "-yes-";
			String correspondingCollision = getCorrespondingCollision(collision);
			if (correspondingCollision != null) {
				collision = correspondingCollision;
			}

			mol.setCollision(collision);

			mol.setCompute(Boolean.valueOf(prop.getProperty(prefix + "Mol" + cpt + "Compute")));
			mol.setDensity(Double.valueOf(prop.getProperty(prefix + "Mol" + cpt + "NSp")));
			mol.setRelativeAbundance(Double.valueOf(prop.getProperty(prefix + "Mol" + cpt + "Abundance")));
			mol.setTemperature(Double.valueOf(prop.getProperty(prefix + "Mol" + cpt + "Tex")));
			mol.setTKin(Double.valueOf(prop.getProperty(prefix + "Mol" + cpt + "TKin")));
			mol.setVelocityDispersion(Double.valueOf(prop.getProperty(prefix + "Mol" + cpt + "FWHM")));
			mol.setSourceSize(Double.valueOf(prop.getProperty(prefix + "Mol" + cpt + "Size")));
			molList.add(mol);
		}
		return molList;
	}

	private String getCorrespondingCollision(String collision) {
		String[] split = collision.split("&");
		String name = split[0];
		String res = null;
		if (name.endsWith(".dat")) {
			File fileCorrespond = new File (Software.getCassisRadexCollisionsPath()
					+ File.separatorChar + "correspondance-coll.txt");
			if (fileCorrespond.exists()) {
				Path path = Paths.get(fileCorrespond.getAbsolutePath());
				List<String> readAllLines = null;
				try {
					readAllLines = Files.readAllLines(path, StandardCharsets.UTF_8);
					boolean find = false;
					for (int i = 0; !find && i < readAllLines.size(); i++) {
						final String[] split2 = readAllLines.get(i).split("\t");
						String string = split2[0];
						if (string.endsWith(name)){
							find = true;
							res = split2[1];
						}
					}
				} catch (IOException e) {
					LOGGER.error("Error when reading " + path.toString(), e);
				}

			}
		}
		return res;
	}

	public String getSelectedTemplate() {
		return selectedTemplate;
	}

	public void setSelectedTemplate(String templateName) {
		selectedTemplate = templateName;
		fireDataChanged(new ModelChangedEvent(SELECTED_TEMPLATE_EVENT, selectedTemplate));
	}

	public List<MoleculeDescription> getSelectedMoleculeDescription() {
		List<MoleculeDescription> listAllMols = getMoleculeList();
		List<MoleculeDescription> returnList = new ArrayList<>();

		for (MoleculeDescription mol : listAllMols) {
			if (mol.isCompute()) {
				returnList.add(mol);
			}
		}
		return returnList;
	}

	/**
	 * Check if the density is between 1e5 and 1e25 for the selected molecules.
	 *
	 * @return true if the density is OK, false otherwise.
	 */
	public boolean isDensityOkForRadex() {
		List<MoleculeDescription> selectedMolecules = getSelectedMoleculeDescription();
		for (MoleculeDescription mol : selectedMolecules) {
			if (mol.getDensity() < 1e5 || mol.getDensity() > 1e25) {
				return false;
			}
		}
		return true;
	}

	public boolean isContinuum() {
		return false;
	}

	@Override
	public Map<String, String> writeProperties(String prefix, int cpt) {
		Map<String, String> map = new HashMap<>();

		Double vlsr = getVlsr();
		String vlsrString = "";

		if (vlsr.equals(Double.MAX_VALUE))
			vlsrString = ComponentSpecies.VLSR_FILE_STRING;
		else {
			vlsrString = String.valueOf(getVlsr());
		}

		map.put(prefix + "Comp" + cpt + "FullLTEMode",
				String.valueOf(isFullLteMode()));
		map.put(prefix + "Comp" + cpt + "LTERADEXMode",
				String.valueOf(isLteRadexMode()));
		map.put(prefix + "Comp" + cpt + "FullRADEXMode",
				String.valueOf(isFullRadexMode()));
		map.put(prefix + "Comp" + cpt + "Density",
				String.valueOf(getDensity()));
		map.put(prefix + "Comp" + cpt + "Vlsr", vlsrString);
		map.put(prefix + "Comp" + cpt + "SphereMode",
				String.valueOf(isSphereMode()));
		map.put(prefix + "Comp" + cpt + "SlabMode",
				String.valueOf(isSlabMode()));
		map.put(prefix + "Comp" + cpt + "ExpandingSphereMode",
				String.valueOf(isExpandingSphereMode()));

		exportTemplate(prefix + "Comp" + cpt, map, getMoleculeList());
		return map;
	}

	private void exportTemplate(String prefix, Map<String, String> map,
			List<MoleculeDescription> moleculeList) {
		int cpt = 0;
		// Calculate sum of molecules
		for (int i = 0; i < moleculeList.size(); i++) {
			MoleculeDescription mol = moleculeList.get(i);
			if (mol.isCompute()) {
				cpt++;
			}
		}
		map.put(prefix + "NbMol", String.valueOf(cpt));

		cpt = 1;
		for (int i = 0; i < moleculeList.size(); i++) {
			MoleculeDescription mol = moleculeList.get(i);
			if (mol.isCompute()) {
				map.put(prefix + "Mol" + cpt + "Species",
						String.valueOf(mol.getName()));
				map.put(prefix + "Mol" + cpt + "Tag",
						String.valueOf(mol.getTag()));
				map.put(prefix + "Mol" + cpt + "Database",
						String.valueOf(mol.getDataSource()));
				map.put(prefix + "Mol" + cpt + "Collision",
						String.valueOf(mol.getCollision()));
				map.put(prefix + "Mol" + cpt + "Compute",
						String.valueOf(mol.isCompute()));
				map.put(prefix + "Mol" + cpt + "NSp",
						String.valueOf(mol.getDensity()));
				map.put(prefix + "Mol" + cpt + "Abundance",
						String.valueOf(mol.getRelativeAbundance()));
				map.put(prefix + "Mol" + cpt + "Tex",
						String.valueOf(mol.getTemperature()));
				map.put(prefix + "Mol" + cpt + "TKin",
						String.valueOf(mol.getTKin()));
				map.put(prefix + "Mol" + cpt + "FWHM",
						String.valueOf(mol.getVelocityDispersion()));
				map.put(prefix + "Mol" + cpt + "Size",
						String.valueOf(mol.getSourceSize()));
				cpt++;
			}
		}
	}
}
