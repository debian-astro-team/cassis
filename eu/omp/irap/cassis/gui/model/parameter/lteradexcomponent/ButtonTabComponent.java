/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import eu.omp.irap.cassis.gui.util.ListenersUtil;

public class ButtonTabComponent extends JPanel {

	private static final long serialVersionUID = -1500783826230340154L;
	private JButton buttonDelete;
	private JCheckBox enableCheckBox;
	private JLabel label;


	public ButtonTabComponent(final String title) {
		this(title, true);
	}

	public ButtonTabComponent(final String title, boolean haveDeleteButton) {
		//unset default FlowLayout' gaps
		super(new FlowLayout(FlowLayout.LEFT, 0, 0));

		setOpaque(false);

		//tab button
		enableCheckBox = new JCheckBox();
		enableCheckBox.setSelected(true);
		enableCheckBox.setOpaque(false);
		add(enableCheckBox);

		label = new JLabel(title);

		add(label);
		if (haveDeleteButton) {
			add(new JLabel("  "));
			buttonDelete = new JButton("X");
			buttonDelete.setOpaque(false);
			buttonDelete.setSize(2 * 10, 15);
			buttonDelete.setPreferredSize(new Dimension(2 * 10, 15));
			buttonDelete.setMargin(new Insets(2, 2, 0, 2));
			add(buttonDelete);
		}
	}

	public void changeLabelText(String newLabelText) {
		label.setText(newLabelText);
	}

	public boolean isActivated() {
		return enableCheckBox.isSelected();
	}

	public JCheckBox getEnableCheckBox() {
		return enableCheckBox;
	}

	public JButton getButtonDelete() {
		return buttonDelete;
	}

	/**
	 * Remove all listeners.
	 */
	public void removeAllListeners() {
		if (buttonDelete != null) {
			ListenersUtil.removeActionListeners(buttonDelete);
			ListenersUtil.removeMouseListeners(buttonDelete);
		}
		ListenersUtil.removeActionListeners(enableCheckBox);
		ListenersUtil.removeMouseListeners(enableCheckBox);
	}
}
