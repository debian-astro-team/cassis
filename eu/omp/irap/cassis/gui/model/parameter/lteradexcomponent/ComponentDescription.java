/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import eu.omp.irap.cassis.common.BufferedWriterProperty;
import eu.omp.irap.cassis.common.events.DataModel;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.parameters.ContinuumParameters;
import eu.omp.irap.cassis.properties.Software;

public abstract class ComponentDescription extends DataModel implements Cloneable {

	public static final String COMPONENT_NAME_EVENT = "componentName";
	public static final String SELECTED_EVENT = "selected";
	public static final String INTERACTING_EVENT = "interacting";

	private String componentName;
	private boolean selected;
	protected boolean interacting = true;
	private String prefix = null;
	private int cpt = -1;
	private boolean enabled = true;

	public ComponentDescription(String componentName) {
		super();
		this.componentName = componentName;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
		fireDataChanged(new ModelChangedEvent(COMPONENT_NAME_EVENT, this.componentName));
	}

	@Override
	public Object clone() {
		ComponentDescription compo = null;
		compo = (ComponentDescription) super.clone();
		if (compo != null) {
			compo.componentName = componentName;
			compo.interacting = interacting;
			compo.selected = selected;
			compo.prefix = prefix;
			compo.cpt = cpt;
		}
		return compo;
	}


	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ComponentDescription [componentName=");
		builder.append(componentName);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public void saveConfig(BufferedWriterProperty out) throws IOException {
		if (prefix == null)
			prefix = componentName.replace(" ", "_");


		if (cpt != -1)
		out.writeTitle("Component parameters " + cpt);
		out.writeProperty(prefix + "Name", componentName);
		out.writeProperty(prefix + "Enabled", String.valueOf(enabled));
		out.writeProperty(prefix + "Interacting", String.valueOf(interacting));

		out.flush();


		cpt = -1;
	}




	@Override
	public void loadConfig(Properties prop) throws IOException {
		if (prefix == null) {
			prefix = componentName.replace(" ", "_");
		}
		//Interacting (true by default)
		Boolean interactingTemp = Boolean.valueOf(prop.getProperty(prefix + "Interacting", "true"));
		this.setInteracting(interactingTemp);

		//Continuum
		if (prop.containsKey(prefix + "Continuum")) {
			String continuumTemp = prop.getProperty(prefix + "Continuum");
			if (!(ContinuumParameters.DEFAULT_0.equals(continuumTemp) ||
				  ContinuumParameters.DEFAULT_1.equals(continuumTemp)
					|| continuumTemp.endsWith("continuum-0")
					|| continuumTemp.endsWith("continuum-1"))
					&& !new File(continuumTemp).exists() &&
					   !new File(Software.getContinuumPath() + File.separator + continuumTemp).exists()) {
			}
		}
		// Name (toChange will be modified in EmAbTabbedPanelModel)
		String nameTemp = prop.getProperty(prefix + "Name", "toChange");
		setComponentName(nameTemp);
	}



	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selec) {
		selected = selec;
		fireDataChanged(new ModelChangedEvent(SELECTED_EVENT, selected));
	}

	public boolean isInteracting() {
		return interacting;
	}

	public void setInteracting(boolean val) {
		this.interacting = val;
		fireDataChanged(new ModelChangedEvent(INTERACTING_EVENT, interacting, this));
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setCpt(int nb) {
		cpt = nb;
	}

	public void setEnabled(boolean state) {
		enabled = state;
	}

	public boolean isEnabled() {
		return enabled;
	}


	public abstract boolean isContinuum();

	public abstract Map<String, String> writeProperties(String prefix, int cpt) ;

}
