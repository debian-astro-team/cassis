/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.text.DecimalFormatSymbols;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.gui.model.parameter.continuum.ContinuumPanel;
import eu.omp.irap.cassis.gui.util.GbcUtil;
import eu.omp.irap.cassis.gui.util.ListenersUtil;
import eu.omp.irap.cassis.gui.util.TextFieldFormatFilter;

@SuppressWarnings("serial")
public class ComponentContinuumPanel extends ComponentDescriptionPanel {

	private static final Logger LOGGER = LoggerFactory.getLogger(ComponentContinuumPanel.class);

	private FocusSizeAdapter myAdapter = new FocusSizeAdapter();
	private DecimalFormatSymbols formatSymbol;

	private ContinuumPanel continuumPanel;
	private JTextField constantContinuumField;

	private JPanel mainPanel;

	private ButtonGroup buttonGroup;
	private JRadioButton fileRadioButton;
	private JRadioButton constantRadioButton;


	public ComponentContinuumPanel() {
		this(new ComponentContinuum());
	}

	public ComponentContinuumPanel(ComponentContinuum componentDescription) {
		super(componentDescription);
		model = componentDescription;
		model.addModelListener(this);
		formatSymbol = new DecimalFormatSymbols();
		formatSymbol.setDecimalSeparator('.');
		setMinimumSize(new Dimension(970, 350));
		setPreferredSize(new Dimension(970, 350));
		setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		setBorder(new TitledBorder(""));
		setLayout(new BorderLayout());
		add(getMainPanel(), BorderLayout.NORTH);
	}

	/**
	 * Create if needed then return the main panel.
	 *
	 * @return the main panel.
	 */
	private JPanel getMainPanel() {
		if (mainPanel == null) {
			mainPanel = new JPanel(new GridBagLayout());
			initButtonGroup();
			mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.insets = new Insets(5, 5, 5, 5);
			GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 0, 0);
			mainPanel.add(getFileRadioButton(), gbc);
			GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 0);
			mainPanel.add(getContinuumPanel().getContinuumButton(), gbc);
			GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 0, 1);
			mainPanel.add(getConstantRadioButton(), gbc);
			GbcUtil.configureFillGrids(gbc, GridBagConstraints.HORIZONTAL, 1, 1);
			mainPanel.add(getConstantContinuumField(), gbc);
			refreshRadioButton();
		}
		return mainPanel;
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		model.setSelected(enabled);
	}

	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (ComponentContinuum.CONSTANT_CONTINUUM_EVENT.equals(event.getSource())) {
			getConstantContinuumField().setText(((Double) event.getValue()).toString());
		} else if (ComponentContinuum.CONTINUUM_TYPE_EVENT.equals(event.getSource())) {
			refreshRadioButton();
		}
	}

	/**
	 * @return the componentDescription
	 */
	public ComponentDescription getComponentDescription() {
		return model;
	}

	/**
	 * Remove all listeners.
	 */
	public void removeAllListeners() {
		model.removeModelListener(this);
		ListenersUtil.removeKeyListeners(getConstantContinuumField());
		ListenersUtil.removeActionListeners(getConstantContinuumField());
		getConstantContinuumField().removeFocusListener(myAdapter);
	}

	/**
	 * Initialize the Button group for the radio buttons.
	 */
	private void initButtonGroup() {
		buttonGroup = new ButtonGroup();
		buttonGroup.add(getFileRadioButton());
		buttonGroup.add(getConstantRadioButton());
	}

	/**
	 * Handle a change of selection for the radio buttons.
	 */
	private void refreshRadioButton() {
		ContinuumTypeSource type = getModel().getSelectedContinuumTypeSource();
		switch (type) {
			case CONSTANT:
				getFileRadioButton().setSelected(false);
				getConstantRadioButton().setSelected(true);
				getContinuumPanel().disableContinuumButton();
				getConstantContinuumField().setEnabled(true);
				break;
			case FILE:
				getFileRadioButton().setSelected(true);
				getConstantRadioButton().setSelected(false);
				getContinuumPanel().enableContinuumButton();
				getConstantContinuumField().setEnabled(false);
				break;
			default:
				LOGGER.error("Unexpected continuum source " + type + '.');
		}
	}

	/**
	 * Create if needed then return the file radio button.
	 *
	 * @return the file radio button.
	 */
	private JRadioButton getFileRadioButton() {
		if (fileRadioButton == null) {
			fileRadioButton = new JRadioButton("Continuum from file",
					getModel().getSelectedContinuumTypeSource() == ContinuumTypeSource.FILE);
			fileRadioButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					getModel().setSelectedContinuumTypeSource(ContinuumTypeSource.FILE);
				}
			});
		}
		return fileRadioButton;
	}

	/**
	 * Create if needed then return the constant radio button.
	 *
	 * @return the constant radio button.
	 */
	private JRadioButton getConstantRadioButton() {
		if (constantRadioButton == null) {
			constantRadioButton = new JRadioButton("Constant continuum; value (K)",
					getModel().getSelectedContinuumTypeSource() == ContinuumTypeSource.CONSTANT);
			constantRadioButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					getModel().setSelectedContinuumTypeSource(ContinuumTypeSource.CONSTANT);
				}
			});
		}
		return constantRadioButton;
	}

	/**
	 * Create if needed then return the continuum panel.
	 *
	 * @return the continuum panel.
	 */
	private ContinuumPanel getContinuumPanel() {
		if (continuumPanel == null) {
			continuumPanel = new ContinuumPanel(((ComponentContinuum) model).getContinuumModel());
			continuumPanel.remove(continuumPanel.getContinuumButton());
		}
		return continuumPanel;
	}

	/**
	 * Create if needed then return the constant continuum JTextField.
	 *
	 * @return the constant continuum JTextField.
	 */
	private JTextField getConstantContinuumField() {
		if (constantContinuumField == null) {
			constantContinuumField = new JTextField(String.valueOf(((ComponentContinuum) model).getConstantContinuum()), 5);
			constantContinuumField.addKeyListener(new TextFieldFormatFilter(TextFieldFormatFilter.SCIENTIFIC_NUMBER));
			constantContinuumField.addFocusListener(myAdapter);
			constantContinuumField.setToolTipText("Continuum constant value in K,"
					+ " if equal 0 then button continuum is considered");
			constantContinuumField.setSize(70, 25);
			constantContinuumField.setPreferredSize(new Dimension(70, 25));
			constantContinuumField.setMinimumSize(new Dimension(70, 25));
		}
		return constantContinuumField;
	}

	public ComponentContinuum getModel() {
		return (ComponentContinuum) model;
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ComponentContinuumPanel componentPanel = new ComponentContinuumPanel();
		frame.setContentPane(componentPanel);
		frame.pack();
		frame.setVisible(true);
	}

	/**
	 * Define a custom FocusAdapter.
	 *
	 * @author thachtn
	 */
	private class FocusSizeAdapter extends FocusAdapter {

		@Override
		public void focusLost(FocusEvent e) {
			if (e.getSource() == constantContinuumField) {
				((ComponentContinuum) model).setConstantContinuum(Double.valueOf(constantContinuumField.getText()));
			}
		}
	}
}
