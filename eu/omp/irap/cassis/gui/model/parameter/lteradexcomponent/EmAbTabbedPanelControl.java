/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent;

import java.awt.Component;
import java.awt.event.MouseEvent;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.gui.util.Deletable;

/**
 * Control of EmAbTabbedPanelView.
 *
 * @author thachtn
 */
//TODO rename to LteRadexComponentsControl
public class EmAbTabbedPanelControl implements ModelListener, Deletable {

	private EmAbTabbedPanelModel model;
	private EmAbTabbedPanelView view;


	public EmAbTabbedPanelControl(EmAbTabbedPanelModel model, EmAbTabbedPanelView emAbTabbedPanelView) {
		this.model = model;
		this.view = emAbTabbedPanelView;
		model.addModelListener(this);
	}

	/**
	 * @return the view
	 */
	public EmAbTabbedPanelView getView() {
		return view;
	}

	/**
	 * @return the model
	 */
	public EmAbTabbedPanelModel getModel() {
		return model;
	}

	public void addNewComponent() {
		model.addNewComponent();
	}

	public void tabbedClicked(MouseEvent event) {
		if (view.getTabbedPane().getSelectedIndex() == view.getTabbedPane().getTabCount()-1) {// "New Tab" tab.
			addNewComponent();
		} else { // Another tab (component)
			if (event.getButton() == MouseEvent.BUTTON3) {
				view.showPopup(event);
			}
		}
	}

	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (EmAbTabbedPanelModel.NEW_COMPONENT_EVENT.equals(event.getSource())
				|| EmAbTabbedPanelModel.ADD_COMPONENT_EVENT.equals(event.getSource())) {
			view.addTab((ComponentSpecies)event.getValue());

		} else if (EmAbTabbedPanelModel.ADD_CONTINUUM_COMPONENT_IN_FIRST_EVENT.equals(event.getSource())) {
			view.addTabContinuum((ComponentContinuum)event.getValue(), true);
		}

		else if (EmAbTabbedPanelModel.ADD_CONTINUUM_COMPONENT_EVENT.equals(event.getSource())) {
			view.addTabContinuum((ComponentContinuum)event.getValue(), false);
		}
		else if (EmAbTabbedPanelModel.REMOVE_ALL_COMPONENTS_EVENT.equals(event.getSource())) {
			while (view.getTabbedPane().getTabCount() > 1) {
				Component comp = view.getTabbedPane().getComponentAt(0);
				view.getTabbedPane().remove(0);
				if (comp instanceof ComponentDescriptionPanel) {
					ComponentDescriptionPanel cdp = (ComponentDescriptionPanel) comp;
					cdp.removeAllListeners();
				}
			}
		} else if (EmAbTabbedPanelModel.DISABLED_COMPONENT_EVENT.equals(event.getSource())) {
			view.disableComponent((ComponentDescription)event.getValue());
		} else if (ComponentDescription.INTERACTING_EVENT.equals(event.getSource())) {
			boolean value = Boolean.parseBoolean(event.getValue().toString());
			ComponentDescription cdOrigin = (ComponentDescription) event.getOrigin();
			model.interactingChange(cdOrigin, value);
		}
	}

	/**
	 * Set a new model.
	 *
	 * @param newModel The new EmAbTabbedPanelModel to use.
	 */
	public void setModel(EmAbTabbedPanelModel newModel) {
		model.removeModelListener(this);
		model = newModel;
		model.addModelListener(this);
		view.refresh();
	}

	/**
	 * @see eu.omp.irap.cassis.gui.util.Deletable#delete()
	 */
	@Override
	public void delete() {
		model.removeModelListener(this);
		while (0 < view.getTabbedPane().getTabCount()) {
			Component comp = view.getTabbedPane().getComponentAt(0);
			view.getTabbedPane().remove(0);
			if (comp instanceof ComponentDescriptionPanel) {
				ComponentDescriptionPanel cdp = (ComponentDescriptionPanel) comp;
				cdp.removeAllListeners();
			}
		}
	}
}
