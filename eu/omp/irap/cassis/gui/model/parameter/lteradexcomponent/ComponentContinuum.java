/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.BufferedWriterProperty;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.gui.model.parameter.continuum.ContinuumModel;
import eu.omp.irap.cassis.parameters.ContinuumParameters;
import eu.omp.irap.cassis.properties.Software;

/**
 * @author jglorian
 *
 */
public class ComponentContinuum extends ComponentDescription {

	private static final Logger LOGGER = LoggerFactory.getLogger(ComponentContinuum.class);

	public static final String CONSTANT_CONTINUUM_EVENT = "constant_continuum";
	public static final String CONTINUUM_TYPE_EVENT = "continuumTypeChanged";

	private double constantContinuum = 0;
	private ContinuumModel continuumModel;
	private ContinuumTypeSource selectedContinuumTypeSource;


	public ComponentContinuum() {
		super("Continuum");
		continuumModel = new ContinuumModel();
		interacting = false;
		selectedContinuumTypeSource = ContinuumTypeSource.FILE;
	}

	@Override
	public boolean isContinuum() {
		return true;
	}

	@Override
	public void setInteracting(boolean val) {
		interacting = false;
	}

	public ContinuumModel getContinuumModel() {
		return continuumModel;
	}

	public double getConstantContinuum() {
		return constantContinuum;
	}

	public void setConstantContinuum(Double constanteContinuum) {
		this.constantContinuum = constanteContinuum;
		fireDataChanged(new ModelChangedEvent(CONSTANT_CONTINUUM_EVENT, this.constantContinuum));
	}

	@Override
	public Map<String, String> writeProperties(String prefix, int cpt) {
		Map<String, String> map = new HashMap<>();
		map.put(prefix + "Comp" + cpt + "ContinuumSize",
				String.valueOf(constantContinuum));
		map.put(prefix + "Comp" + cpt + "Continuum",
				String.valueOf(continuumModel.getContinuumParams().getSelectedContinuum()));
		return map;

	}

	@Override
	public Object clone() {
		ComponentContinuum compo = null;
		compo = (ComponentContinuum) super.clone();

		if (compo != null) {
			String selectedContinuum = continuumModel.getContinuumParams().getSelectedContinuum();
			ContinuumParameters continuumParams = new ContinuumParameters(selectedContinuum);
			compo.continuumModel = new ContinuumModel(continuumParams);
			compo.constantContinuum = constantContinuum;
			compo.selectedContinuumTypeSource = selectedContinuumTypeSource;
		}

		return compo;
	}

	@Override
	public void saveConfig(BufferedWriterProperty out) throws IOException {
		String prefix = getPrefix();
		super.saveConfig(out);
		setPrefix(prefix);
		out.writeProperty(prefix + "ContinuumSelected", selectedContinuumTypeSource.name());
		out.writeProperty(prefix + "Continuum", continuumModel.getSelectedContinuum());
		out.writeProperty(prefix + "ContinuumSize", String.valueOf(constantContinuum));
	}

	@Override
	public void loadConfig(Properties prop) throws IOException {
		String prefix = getPrefix();
		super.loadConfig(prop);
		setPrefix(prefix);

		//Density
		String constantContinuumString = prop.getProperty(prefix + "ContinuumSize");
		if (constantContinuumString != null) {
			Double constantContinuum = Double.valueOf(constantContinuumString);
			this.setConstantContinuum(constantContinuum);
		}

		//Continuum
		if (prop.containsKey(prefix + "ContinuumSelected")) {
			setSelectedContinuumTypeSource(ContinuumTypeSource.valueOf(
					prop.getProperty(prefix + "ContinuumSelected")));
		} else {
			setSelectedContinuumTypeSource(ContinuumTypeSource.FILE);
		}
		if (prop.containsKey(prefix + "Continuum")) {
			String continuumTemp = prop.getProperty(prefix + "Continuum");
			if (!(ContinuumParameters.DEFAULT_0.equals(continuumTemp) || ContinuumParameters.DEFAULT_1.equals(continuumTemp)
					|| continuumTemp.endsWith("continuum-0") || continuumTemp.endsWith("continuum-1"))
					&& !new File(continuumTemp).exists() && !new File(Software.getContinuumPath() + File.separator + continuumTemp).exists()) {
				continuumModel.setSelectedContinuum(ContinuumParameters.DEFAULT_0);
			} else {
				continuumModel.setSelectedContinuum(continuumTemp);
			}
		} else if (prop.containsKey("continuum")) {
			String continuumTemp = prop.getProperty("continuum");
			if (!(ContinuumParameters.DEFAULT_0.equals(continuumTemp) || ContinuumParameters.DEFAULT_1.equals(continuumTemp)
					|| continuumTemp.endsWith("continuum-0") || continuumTemp.endsWith("continuum-1"))
					&& !new File(continuumTemp).exists() && !new File(Software.getContinuumPath() + File.separator + continuumTemp).exists()) {
				continuumModel.setSelectedContinuum(ContinuumParameters.DEFAULT_0);
			} else {
				continuumModel.setSelectedContinuum(continuumTemp);
			}
		} else {
			continuumModel.setSelectedContinuum(ContinuumParameters.DEFAULT_0);
		}
		setInteracting(false);
	}

	/**
	 * Return the selected ContinuumTypeSource.
	 *
	 * @return the selected ContinuumTypeSource.
	 */
	public ContinuumTypeSource getSelectedContinuumTypeSource() {
		return selectedContinuumTypeSource;
	}

	/**
	 * Set a new selected ContinuumTypeSource.
	 *
	 * @param type The new ContinuumTypeSource to set.
	 */
	public void setSelectedContinuumTypeSource(ContinuumTypeSource type) {
		selectedContinuumTypeSource = type;
		fireDataChanged(new ModelChangedEvent(CONTINUUM_TYPE_EVENT, this.selectedContinuumTypeSource));
	}

	/**
	 * Compute then return the ContinuumParameters to use.
	 *
	 * @return the ContinuumParameters to use.
	 */
	public ContinuumParameters getSelectedContinuumParameters() {
		ContinuumParameters param = null;
		switch (selectedContinuumTypeSource) {
		case FILE:
			param = getContinuumModel().getContinuumParams();
			break;
		case CONSTANT:
			param = new ContinuumParameters(constantContinuum);
			break;
		default:
			LOGGER.error("Unknown continuum type source");
			break;
		}
		return param;
	}

}
