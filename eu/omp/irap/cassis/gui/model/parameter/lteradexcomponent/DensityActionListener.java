/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.gui.model.table.CassisTableMoleculeModel;
import eu.omp.irap.cassis.gui.model.table.JCassisTable;

public class DensityActionListener implements ActionListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(DensityActionListener.class);

	private final JCassisTable<MoleculeDescription> moleculesTable;


	/**
	 * Constructor makes a new DensityActionListener.
	 *
	 * @param moleculesTable
	 *            molecules table
	 */
	public DensityActionListener(JCassisTable<MoleculeDescription> moleculesTable) {
		super();
		this.moleculesTable = moleculesTable;
	}

	/**
	 * Intercept all events.
	 */
	@Override
	public void actionPerformed(final ActionEvent evt) {
		final JTextField textfield = (JTextField) evt.getSource();
		if (textfield.getText().isEmpty()) {
			return;
		}
		textfield.setText(textfield.getText().toUpperCase());

		// get global density
		double globalDensity;
		try {
			globalDensity = Double.parseDouble(textfield.getText());
		} catch (NumberFormatException nfe) {
			LOGGER.error("Global Density must be a number", nfe);
			return;
		}

		CassisTableMoleculeModel tableModel = (CassisTableMoleculeModel) moleculesTable.getModel();

		for (MoleculeDescription mol : tableModel.getList()) {
			mol.setDensity(mol.getRelativeAbundance() * globalDensity);
		}
		tableModel.fireTableDataChanged();
	}

}
