/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.lteradexcomponent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.BufferedWriterProperty;
import eu.omp.irap.cassis.common.ComponentAlgo;
import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.common.events.DataModel;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.gui.util.PropertiesLinkedHashMap;

/**
 * Model of the EmAbTabbedPanelView which represents the values of parameters of
 * the view used in LTERadex and Line Analysis.
 *
 * @author nerriere
 * @since juillet 2008
 */
//TODO rename to LteRadexComponentsModel
public class EmAbTabbedPanelModel extends DataModel {

	public static final String ADD_COMPONENT_EVENT = "addComponent";
	public static final String DISABLED_COMPONENT_EVENT = "disabledComponent";
	public static final String NEW_COMPONENT_EVENT = "newComponent";
	public static final String REMOVE_ALL_COMPONENTS_EVENT = "removeAllComponents";
	public static final String REMOVE_COMPONENT_EVENT = "removeComponent";
	public static final String ADD_CONTINUUM_COMPONENT_EVENT = "addContinuumComponent";
	public static final String ADD_CONTINUUM_COMPONENT_IN_FIRST_EVENT = "addContinuumComponentinFirst";

	private static final Logger LOGGER = LoggerFactory.getLogger(EmAbTabbedPanelModel.class);
	private List<ComponentDescription> componentList;


	/**
	 * Constructor.
	 */
	public EmAbTabbedPanelModel() {
		componentList = new ArrayList<>();
	}

	/**
	 * Return the molecule list of the source.
	 *
	 * @param comp The component number for which we want the molecule list.
	 * @return The molecule list for the provided component number.
	 */
	public List<MoleculeDescription> getMoleculesList(int comp) {
		List<MoleculeDescription> res = null;
		while (componentList.get(comp).isContinuum() && comp < componentList.size()) {
			comp++;
		}
		if(comp != componentList.size()) {
			res = ((ComponentSpecies)componentList.get(comp)).getMoleculeList();
		}
		return res;
	}

	public void removeComponent(ComponentDescription componentDescription) {
		componentList.remove(componentDescription);
		fireDataChanged(new ModelChangedEvent(REMOVE_COMPONENT_EVENT,
				componentDescription));
	}

	public void removeAllComponents() {
		componentList.clear();
		fireDataChanged(new ModelChangedEvent(REMOVE_ALL_COMPONENTS_EVENT,
				componentList));
	}
	/**
	 * Creates and add new {@link ComponentContinuum} in the componentList.
	 *
	 * @return the added component.
	 */
	public ComponentContinuum addContinuumComponent(boolean inFirst) {
		ComponentContinuum component = new ComponentContinuum();
		if (inFirst) {
			componentList.add(0,component);
			fireDataChanged(new ModelChangedEvent(ADD_CONTINUUM_COMPONENT_IN_FIRST_EVENT, component));
		} else {
			componentList.add(component);
			fireDataChanged(new ModelChangedEvent(ADD_CONTINUUM_COMPONENT_EVENT, component));
		}

		return component;
	}


	/**
	 * Creates and add new {@link ComponentDescription} in the componentList.
	 *
	 * @return the added component.
	 */
	public ComponentDescription addNewComponent() {
		String componentName = "Component " + (componentList.size() );
		ComponentSpecies component = new ComponentSpecies(
				componentName, new ArrayList<>(), 7.5E22, 0.0);

		componentList.add(component);
		fireDataChanged(new ModelChangedEvent(NEW_COMPONENT_EVENT, component));
		return component;
	}

	/**
	 * Add a new {@link ComponentDescription} in the componentList.
	 *
	 * @param component The component to add.
	 * @return The added component.
	 */
	public ComponentDescription addComponent(ComponentDescription component) {
		componentList.add(component);
		fireDataChanged(new ModelChangedEvent(ADD_COMPONENT_EVENT, component));
		return component;
	}

	public ComponentDescription addDisabledComponent(
			ComponentDescription component) {
		componentList.add(component);
		fireDataChanged(new ModelChangedEvent(ADD_COMPONENT_EVENT, component));
		fireDataChanged(new ModelChangedEvent(DISABLED_COMPONENT_EVENT, component));
		component.setEnabled(false);
		return component;
	}

	/**
	 * Add a {@link ComponentDescription} manually, without event.
	 *
	 * @param component The component to add.
	 */
	public void addComponentManually(ComponentDescription component) {
		componentList.add(component);
	}

	/**
	 * Get component name list without commands.
	 *
	 * @return Component name list without commands
	 */
	public List<ComponentDescription> getComponentList() {
		return componentList;
	}

	/**
	 * Get component name list without commands.
	 *
	 * @return Component name list without commands
	 */
	public List<ComponentDescription> getComponentsWithMoleculeSelected() {
		if (getComponentList() == null)
			return null;
		List<ComponentDescription> compos = new ArrayList<>();
		for (ComponentDescription compo : getComponentList()) {
			if (compo.isEnabled()) {
				ComponentDescription compoClone = (ComponentDescription) compo.clone();
				if (compoClone.isContinuum()) {
					compos.add(compoClone);
				} else  {
					ComponentSpecies componentSpecies = (ComponentSpecies)compoClone;
					componentSpecies.keepOnlyComputeMoleculeList();
					compos.add(compoClone);

					if (LOGGER.isDebugEnabled()) {
						for (int cpt = 0; cpt < componentSpecies.getMoleculeList().size(); cpt++) {
							MoleculeDescription mol = componentSpecies.getMoleculeList().get(cpt);
							LOGGER.debug("Mol of compo selected : {}", mol.getName());
						}
					}
				}

			}
		}
		return compos;
	}

	public boolean saveConfig(String prefix, BufferedWriterProperty out)
			throws IOException {
		setProperty(prefix, out);
		return true;
	}

	public boolean saveConfig(BufferedWriterProperty out, String prefix)
			throws IOException {
		try {
			// save components
			setProperty(prefix, out);
		} catch (Exception e) {
			LOGGER.error("Error while saving the config", e);
			return false;
		}
		return true;
	}

	private void setProperty(String prefix, BufferedWriterProperty out)
			throws IOException {
		Iterator<ComponentDescription> itComp = componentList.iterator();
		int cpt = 1;
		while (itComp.hasNext()) {
			ComponentDescription component = itComp.next();
			component.setCpt(cpt);
			component.setPrefix(prefix + "Comp" + cpt);
			component.saveConfig(out);
			cpt++;
		}
	}

	/**
	 * Write properties to the provided map.
	 *
	 * @param prefix The prefix to use for the key.
	 * @param map The map.
	 */
	public void writeProperties(String prefix, Map<String, String> map) {
		Iterator<ComponentDescription> itComp = componentList.iterator();
		int cpt = 1;
		while (itComp.hasNext()) {
			ComponentDescription compo = itComp.next();
			map.putAll(compo.writeProperties(prefix,cpt));
			cpt++;
		}
	}


	@Override
	public void loadConfig(Properties prop) {
		int offset = 0;
		removeAllComponents();

		if (prop.containsKey("continuum")&&
			(!prop.containsKey("Comp1Name") ||
			  !prop.get("Comp1Name").equals("Continuum"))){
			if (prop.containsKey("Comp1FullLTEMode")) {
				ComponentContinuum addContinuumComponent = addContinuumComponent(true);
				addContinuumComponent.getContinuumModel().setSelectedContinuum(prop.get("continuum").toString());
			    };
			}

		offset = loadConfig("Component_", prop, offset);
		offset = loadConfig("Comp", prop, offset);
		offset = loadConfig("EmmComp", prop, offset);
		offset = loadConfig("AbsComp", prop, offset);
		// If we don't find a composent, create a new default composent
		// as we always need a composent.
		if (offset == 0)
			addNewComponent();
	}

	private int loadConfig(String prefix, Properties prop, int offset) {
		int cpt = 0;
		int cptName = 0;
		String componentName = prefix + String.valueOf(cpt + 1);
		try {
			if (!(prop.containsKey(componentName + "NbMol") &&
				Integer.valueOf(prop.getProperty(componentName + "NbMol")) == 0))
				while (prop.containsKey(componentName + "Vlsr")||
					   prop.containsKey(componentName + "Name")) {
					ComponentDescription componentDescription;
					if (prop.containsKey(componentName + "Vlsr")) {
						componentDescription = new ComponentSpecies(componentName,
							new ArrayList<>(), 0.0, 0.0);
						cptName++;
						// Name 1 ... 2 ... 3 ..
						componentDescription.setComponentName("Component " + cptName);
						componentDescription.setPrefix(componentName);
						componentDescription.loadConfig(prop);
						componentDescription.setComponentName("Component " + cptName);
						// Component disabled ?
						if (Boolean.valueOf(prop.getProperty(componentName + "Enabled", "true"))) {
							addComponent(componentDescription);
						}
						else
							addDisabledComponent(componentDescription);

					}else {
						componentDescription = addContinuumComponent(false);
						componentDescription.setPrefix(componentName);
						componentDescription.loadConfig(prop);
					}

					cpt++;
					componentName = prefix + String.valueOf(cpt + 1);
				}
		} catch (IOException e) {
			LOGGER.error("Error while loading the config", e);
			return -1;
		}
		return offset + cptName;
	}

	public void setMolMandatory(List<MoleculeDescription> molmandatory) {
		if (molmandatory != null) {
			final ComponentSpecies firstCompoSpecies = getFirstCompoSpecies();
			if (firstCompoSpecies != null) {
				List<MoleculeDescription> sourceList = firstCompoSpecies.getMoleculeList();
				MoleculeDescription moleculeDescription = MoleculeDescription
						.getMoleculeDescription(molmandatory.get(0).getTag(),
								sourceList);
				if (moleculeDescription == null)
					sourceList.add(0, molmandatory.get(0));
				else if (!moleculeDescription.isCompute())
					moleculeDescription.setCompute(true);
			}
		}
	}

	private ComponentSpecies getFirstCompoSpecies() {
		boolean found = false;
		ComponentSpecies componentSpecies = null;
		for (int i = 0; !found && i < componentList.size() ; i++) {
			ComponentDescription componentDescription = componentList.get(i);
			if (!componentDescription.isContinuum()) {
				found = true;
				componentSpecies = (ComponentSpecies)componentDescription;
			}
		}
		return componentSpecies;
	}

	public Map<String, String> getProperties() {
		Map<String, String> map = new PropertiesLinkedHashMap();
		writeProperties("", map);
		return map;
	}

	@Override
	public void saveConfig(BufferedWriterProperty out) throws IOException {
		// Not used.
	}

	public void interactingChange(ComponentDescription origin, boolean value) {
		if (origin != null) {
			String nameCompoOrigin = origin.getComponentName();
			boolean foundOrigin = false;
			if (value) {
				for (ComponentDescription cd : getComponentList()) {
					if (foundOrigin) {
						cd.setInteracting(value);
						return;
					}
					if (cd.getComponentName() == nameCompoOrigin) {
						foundOrigin = true;
					}
				}
			} else {
				int nbCompo = getComponentList().size();
				for (int i = nbCompo - 1; i >= 0; i--) {
					ComponentDescription cd = getComponentList().get(i);

					if (foundOrigin) {
						cd.setInteracting(value);
						return;
					}

					if (cd.getComponentName().equals(nameCompoOrigin)) {
						foundOrigin = true;
					}
				}
			}
		} else {
			LOGGER.error("Error on interacting change");
		}
	}

	public boolean isFirstInteracting(ComponentDescription componentToTest) {
		boolean result = true;
		for (ComponentDescription cd : componentList) {
			if (cd == componentToTest) {
				break;
			} else {
				if (cd.isInteracting()) {
					result = false;
					break;
				}
			}
		}
		return result;
	}

	/**
	 * Return the list of enabled {@link ComponentDescription} of provided Algo.
	 *
	 * @param model The algo type.
	 * @return the list of enabled {@link ComponentDescription} of provided Algo.
	 */
	public List<ComponentSpecies> getEnabledComponentFromType(ComponentAlgo model) {
		List<ComponentSpecies> returnList = new ArrayList<>();
		List<ComponentDescription> allComponents = getComponentList();

		for (ComponentDescription compo : allComponents) {
			if (!compo.isContinuum()) {
				ComponentSpecies comp = (ComponentSpecies)compo;
				if (comp.isEnabled() && comp.getTypeAlgo() == model) {
					returnList.add(comp);
				}
			}

		}
		return returnList;
	}

	/**
	 * Check for all RADEX enabled component if the density of the selected molecules are OK.
	 *
	 * @return true if the density is OK, false otherwise.
	 */
	public boolean isDensityOkForRadex() {
		List<ComponentSpecies> listCompo =
				getEnabledComponentFromType(ComponentAlgo.FULL_RADEX_ALGO);
		for (ComponentSpecies cd : listCompo) {
			if (!cd.isDensityOkForRadex()) {
				return false;
			}
		}
		return true;
	}

	public boolean haveContinnumCompo() {
		boolean res = false;
		final List<ComponentDescription> compos = getComponentList();
		for (int i = 0; ! res && i < compos.size(); i++) {
			if (compos.get(i).isContinuum()) {
				res = true;
			}

		}
		return res;
	}
}
