/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.noise;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.gui.util.JDoubleCassisTextField;

public class NoisePanel extends JPanel implements ModelListener {

	private static final long serialVersionUID = 1L;
	protected NoiseModel model;
	private JDoubleCassisTextField noiseText;
	private JComboBox<UNIT> noiseUnitBox;


	public NoisePanel() {
		this(new NoiseModel());
	}

	public NoisePanel(NoiseModel model) {
		this.model = model;
		model.addModelListener(this);
		initComponents();
	}

	/**
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable {
		model.removeModelListener(this);
		super.finalize();
	}

	private void initComponents() {
		setBorder(new TitledBorder("Noise"));
		add(new JLabel("rms:"));
		add(getNoiseTextField());
		add(getNoiseUnitBox());
		setPreferredSize(new Dimension(180, 60));
		FlowLayout flowLayout = (FlowLayout) getLayout();
		flowLayout.setAlignment(FlowLayout.CENTER);
	}

	public JComboBox<UNIT> getNoiseUnitBox() {
		if (noiseUnitBox == null) {
			UNIT[] unitChoices = new UNIT[] {UNIT.M_KELVIN};
			noiseUnitBox = new JComboBox<>(unitChoices);
			noiseUnitBox.setSelectedItem(model.getUnit());
			noiseUnitBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					UNIT value = (UNIT) ((JComboBox<?>)e.getSource()).getSelectedItem();
					model.setUnit(value);
				}
			});
		}
		return noiseUnitBox;
	}

	public JDoubleCassisTextField getNoiseTextField() {
		if (noiseText == null) {
			noiseText = new JDoubleCassisTextField(4, model.getNoise());
			noiseText.addPropertyChangeListener("value", new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					model.setNoise(Double.valueOf(evt.getNewValue().toString()));
				}
			});
		}
		return noiseText;
	}

	public NoiseModel getModel() {
		return model;
	}

	@Override
	public void dataChanged(ModelChangedEvent e) {
		if (NoiseModel.NOISE_EVENT.equals(e.getSource())) {
			noiseText.setValue(model.getNoise());
		} else if (NoiseModel.NOISE_UNIT_EVENT.equals(e.getSource())) {
			UNIT value = model.getUnit();
			noiseUnitBox.removeAllItems();
			noiseUnitBox.addItem(value);
			noiseUnitBox.setSelectedItem(value);
		}
	}

	/**
	 * Set a new Model.
	 *
	 * @param newModel The new NoiseModel to use.
	 */
	public void setModel(NoiseModel newModel) {
		model.removeModelListener(this);
		model = newModel;
		model.addModelListener(this);
		refresh();
	}

	/**
	 * Refresh the view.
	 */
	private void refresh() {
		getNoiseUnitBox().removeAllItems();
		getNoiseUnitBox().addItem(model.getUnit());
		getNoiseUnitBox().setSelectedItem(model.getUnit());
		getNoiseTextField().setValue(model.getNoise());
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(new NoisePanel());
		frame.pack();
		frame.setVisible(true);
	}

}
