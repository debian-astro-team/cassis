/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.oversampling;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.gui.util.JDoubleCassisTextField;

public class OversamplingPanel extends JPanel implements ModelListener {

	private static final long serialVersionUID = 1L;
	private OversamplingModel model;
	private JDoubleCassisTextField oversamplingText;


	public OversamplingPanel() {
		this(new OversamplingModel());
	}

	public OversamplingPanel(OversamplingModel model) {
		this.model = model;
		model.addModelListener(this);

		initComponents();
	}

	/**
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable {
		model.removeModelListener(this);
		super.finalize();
	}

	private void initComponents() {
		setBorder(new TitledBorder("Oversampling"));
		add(new JLabel("Oversampling:"));
		add(getOverSamplingTextField());
		setPreferredSize(new Dimension(190, 60));
		FlowLayout flowLayout = new FlowLayout();
		flowLayout.setAlignment(FlowLayout.CENTER);
		setLayout(flowLayout);
	}

	public JDoubleCassisTextField getOverSamplingTextField() {
		if (oversamplingText == null) {
			oversamplingText = new JDoubleCassisTextField(4, 1.0);
			oversamplingText.addPropertyChangeListener("value", new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					model.setOversampling(Double.valueOf(evt.getNewValue().toString()));
				}
			});
			oversamplingText.setValue(model.getOversampling());
		}
		return oversamplingText;
	}

	public OversamplingModel getModel() {
		return model;
	}

	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (OversamplingModel.OVERSAMPLING_EVENT.equals(event.getSource())) {
			oversamplingText.setValue(model.getOversampling());
		}
	}

	/**
	 * Set a new model.
	 *
	 * @param newModel The new OversamplingModel to use.
	 */
	public void setModel(OversamplingModel newModel) {
		model.removeModelListener(this);
		model = newModel;
		model.addModelListener(this);
		refresh();
	}

	/**
	 * Refresh the view.
	 */
	private void refresh() {
		getOverSamplingTextField().setValue(model.getOversampling());
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(new OversamplingPanel());
		frame.pack();
		frame.setVisible(true);
	}

}
