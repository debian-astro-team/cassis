/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.data;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.BufferedWriterProperty;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.Telescope;
import eu.omp.irap.cassis.common.TypeFrequency;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.common.events.DataModel;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.file.CassisFileFilter;
import eu.omp.irap.cassis.gui.menu.action.SpectrumManagerAction;
import eu.omp.irap.cassis.gui.util.FileUtils;
import uk.ac.starlink.table.StarTable;
import uk.ac.starlink.table.StarTableFactory;
import uk.ac.starlink.table.TableFormatException;
import uk.ac.starlink.table.ValueInfo;

public class LoadDataModel extends DataModel implements LoadVoDataInterface {

	public static final String CASSIS_SPECTRUM_EVENT = "cassisSpectrum";
	public static final String NAME_DATA_EVENT = "nameData";
	public static final String TELESCOPE_DATA_EVENT = "telescopeData";
	public static final String TELESCOPE_DATA_LOAD_EVENT = "telescopeDataLoad";
	public static final String TYPE_FREQUENCY_EVENT = "typeFrequency";
	public static final String VLSR_DATA_EVENT = "vlsrData";
	public static final String VO_LIST_FILES_EVENT = "voListFiles";

	private static final Logger LOGGER = LoggerFactory.getLogger(LoadDataModel.class);

	private String nameData;
	private CassisSpectrum cassisSpectrum;
	private String telescope;
	private TypeFrequency typeFrequency;
	private String telescopeError;
	private String dataFileError;


	public LoadDataModel() {
		this.telescope = "???";
		this.typeFrequency = TypeFrequency.REST;
	}

	@Override
	public void saveConfig(BufferedWriterProperty out) throws IOException {
		out.writeProperty("nameData", getNameData());
		out.writeProperty("telescopeData", telescope);
		out.writeProperty("typeFrequency", typeFrequency.name());
	}

	@Override
	public void loadConfig(Properties prop) {
		String loadedNameData = prop.getProperty("nameData", prop.getProperty("datafile"));

		if (!"null".equals(loadedNameData)) {
			File file = new File(loadedNameData);
			if (file.exists()) {
				loadData(file);
			} else {
				dataFileError = loadedNameData;
			}
		}

		if (prop.containsKey("telescopeData")) {
			if (Telescope.exist(prop.getProperty("telescopeData")))
				setTelescope(prop.getProperty("telescopeData"), TELESCOPE_DATA_LOAD_EVENT);
			else
				telescopeError = prop.getProperty("telescopeData");
		}
		if (prop.containsKey("typeFrequency")) {
			setTypeFrequency(TypeFrequency.valueOf(prop.getProperty("typeFrequency")));
		}
	}

	@Override
	public void loadData(String path) {
		File file = new File(path);

		if (file.exists()) {
			loadData(file);
			return;
		} else {
			file = FileUtils.downloadAndRenameIfNeeded(path, true);
			if (file != null) {
				loadData(file);
				return;
			}
		}
	}

	public void loadData(Object data) {
		if (data instanceof File) {
			File file = (File) data;
			if (CassisFileFilter.isVotableFile(file.getName())) {
				StarTable starTable;
				try {
					starTable = new StarTableFactory().makeStarTable(file.getAbsolutePath(), "votable");
					int nbColumn = starTable.getColumnCount();
					for (int i = 0; i < nbColumn; i++) {
						ValueInfo info = starTable.getColumnInfo(i);
						if ("url".equalsIgnoreCase(info.getName()) ||
								"meta.ref.url".equalsIgnoreCase(info.getUCD()) ||
								"ssa:access:reference".equalsIgnoreCase(info.getUtype()) ||
								"DATA_LINK".equalsIgnoreCase(info.getUCD())) {
							fireDataChanged(new ModelChangedEvent(VO_LIST_FILES_EVENT, starTable));
							return;
						}
					}
				} catch (TableFormatException e) {
					LOGGER.warn("The file {} is malformed", file.getAbsolutePath(), e);
					return;
				} catch (IOException e) {
					LOGGER.error("Error while reading the file {}", file.getAbsolutePath(), e);
					return;
				}
			}
			loadDataFile(file);
		}
	}

	private void setVlsrData(double vlsrData) {
		fireDataChanged(new ModelChangedEvent(VLSR_DATA_EVENT, vlsrData));
	}

	private void loadDataFile(File file) {
		SpectrumManagerAction spectrumManager = SpectrumManagerAction.getInstance();
		spectrumManager.addRessource(file, false);
		CassisSpectrum cs = spectrumManager.getSelectedSpectrum();
		setNameData(spectrumManager.getPathOrName(cs));
		setCassisSpectrum(cs);
	}

	public String getNameData() {
		return nameData;
	}

	public final void setNameData(String nameData) {
		this.nameData = nameData;
		fireDataChanged(new ModelChangedEvent(NAME_DATA_EVENT, nameData));
	}

	public CassisSpectrum getCassisSpectrum() {
		return cassisSpectrum;
	}

	public final void setCassisSpectrum(CassisSpectrum cassisSpectrum) {
		setVlsrData(cassisSpectrum.getVlsr());
		this.cassisSpectrum = cassisSpectrum;
		this.cassisSpectrum.setTypeFrequency(typeFrequency);
		setTelescope(cassisSpectrum.getTelescope(), TELESCOPE_DATA_EVENT);
		fireDataChanged(new ModelChangedEvent(CASSIS_SPECTRUM_EVENT, cassisSpectrum));
	}

	private void setTelescope(String telescope, String event) {
		if (telescope == null || telescope.isEmpty())
			telescope = "???";

		if (!telescope.equalsIgnoreCase(this.telescope)) {
			this.telescope = telescope;
			fireDataChanged(new ModelChangedEvent(event, telescope));
		}

		String telescopeName = new File(telescope).getName();
		if (cassisSpectrum != null &&
				telescopeName.startsWith("spire")) {
			cassisSpectrum.setYAxis(YAxisCassis.getYAxisJansky());
		}
	}

	public void setTelescope(String telescope) {
		setTelescope(telescope, TELESCOPE_DATA_EVENT);
	}

	/**
	 * @return the telescope
	 */
	public final String getTelescope() {
		return telescope;
	}

	public void setTypeFrequency(TypeFrequency value) {
		this.typeFrequency = value;
		if (cassisSpectrum != null)
			cassisSpectrum.setTypeFrequency(typeFrequency);
		fireDataChanged(new ModelChangedEvent(TYPE_FREQUENCY_EVENT, typeFrequency));
	}

	public TypeFrequency getTypeFrequency() {
		return typeFrequency;
	}

	/**
	 * Return the telescope error, the path of the telescope file.
	 *
	 * @return the path of the telescope file if there is an error, null otherwise.
	 */
	public String getTelescopeError() {
		return telescopeError;
	}

	/**
	 * Remove the telescope error.
	 */
	public void resetTelescopeError() {
		telescopeError = null;
	}

	/**
	 * Return the data file error, the path of the data file.
	 *
	 * @return the path of the data file if there is an error, null otherwise.
	 */
	public String getDataFileError() {
		return dataFileError;
	}

	/**
	 * Remove the data file error.
	 */
	public void resetDataFileError() {
		dataFileError = null;
	}

	/**
	 * Return if there is an error with the telescope.
	 *
	 * @return true if there is an error with the telescope, false otherwise.
	 */
	public boolean haveTelescopeError() {
		return telescopeError != null;
	}

	/**
	 * Return if there is an error with the data file.
	 *
	 * @return true if there is an error with the data file, false otherwise.
	 */
	public boolean haveDataFileError() {
		return dataFileError != null;
	}

	/**
	 * Return if there is an error with the data or telescope file.
	 *
	 * @return true if there is an error with the data or telescope file, false otherwise.
	 */
	public boolean haveError() {
		return haveDataFileError() || haveTelescopeError();
	}

}
