/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.data;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.TypeFrequency;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.gui.PanelView;
import eu.omp.irap.cassis.gui.util.DropCassisSpectrumListener;
import eu.omp.irap.cassis.gui.util.JDoubleCassisTextField;
import eu.omp.irap.cassis.gui.util.ListenersUtil;
import eu.omp.irap.cassis.gui.util.MyTelescopeButton;
import eu.omp.irap.cassis.properties.Software;
import uk.ac.starlink.table.StarTable;

@SuppressWarnings("serial")
public class LoadDataPanel extends JPanel implements ModelListener, DropCassisSpectrumListener {

	private JDoubleCassisTextField vlsrDataField;
	private JButton telescopeButton;
	private JTextField dataFileText;
	private LoadDataModel model;
	private JButton loadButton;
	private JComboBox<String> vlsrcomboBox;
	private JComboBox<String> typeFrequencyComboBox;
	private int telescopeButtonLimit = 0;


	public LoadDataPanel() {
		this(new LoadDataModel());
	}

	public LoadDataPanel(LoadDataModel model) {
		this(model, 0);
	}

	public LoadDataPanel(LoadDataModel model, int telescopeButtonLimit) {
		super(new FlowLayout(FlowLayout.LEFT));
		this.model = model;
		this.telescopeButtonLimit = telescopeButtonLimit;
		model.addModelListener(this);
		initComponents();
	}

	void initComponents() {
		add(getLoadButton());
		add(getDataText());
		add(new JLabel("Vlsr data:"));
		add(getVlsrData());
		add(getVlsrCombox());
		add(new JLabel("in:"));
		add(getTypeFrequency());
		add(new JLabel("Telescope"));
		add(getTelescopeButton());
		setBorder(new TitledBorder("Data"));
		setPreferredSize(new Dimension(700, 60));
		setSize(new Dimension(700, 60));
	}

	public JComboBox<String> getTypeFrequency() {
		if (typeFrequencyComboBox == null) {
			typeFrequencyComboBox = new JComboBox<>(new String[]{TypeFrequency.REST.forSave(),
												 TypeFrequency.SKY.forSave()});

			typeFrequencyComboBox.addActionListener(new ActionListener() {
					@Override
				public void actionPerformed(ActionEvent e) {
					TypeFrequency value = TypeFrequency.toFreq((String) ((JComboBox<?>)e.getSource()).getSelectedItem());
					model.setTypeFrequency(value);
				}
			});
			typeFrequencyComboBox.setSelectedItem(model.getTypeFrequency().forSave());
			typeFrequencyComboBox.setEnabled(false);
		}
		return typeFrequencyComboBox;
	}

	public JComboBox<String> getVlsrCombox() {
		if (vlsrcomboBox == null) {
			vlsrcomboBox = new JComboBox<>();
			vlsrcomboBox.addItem("km/s");
		}
		return vlsrcomboBox;
	}

	public JTextField getDataText() {
		if (dataFileText == null) {
			dataFileText = new JTextField(13);
			dataFileText.setText(model.getNameData());
			dataFileText.addKeyListener(new KeyAdapter() {

				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						model.loadData(dataFileText.getText());
					}
				}
			});
		}

		return dataFileText;
	}

	/**
	 * Create if necessary then return the "Load" {@link JButton}.
	 * The action listener is added here in PanelView.
	 *
	 * @return the "Load" {@link JButton}.
	 */
	public JButton getLoadButton() {
		if (loadButton == null) {
			loadButton = new JButton("Load");
		}
		return loadButton;
	}

	private JDoubleCassisTextField getVlsrData() {
		if (vlsrDataField == null) {
			vlsrDataField = new JDoubleCassisTextField(4, 0.);
			vlsrDataField.setEditable(false);
			vlsrDataField.setForeground(Color.RED);
		}
		return vlsrDataField;
	}

	public JButton getTelescopeButton() {
		if (telescopeButton == null) {
			// Telescope
			telescopeButton = new MyTelescopeButton("???", telescopeButtonLimit);
			if (model.getTelescope() != null)
				telescopeButton.setText(model.getTelescope());

			telescopeButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					browseTelescopeFile();
				}
			});
		}
		return telescopeButton;
	}

	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (LoadDataModel.NAME_DATA_EVENT.equals(event.getSource())) {
			setDataName(getModel().getNameData());

			// null on hipe plugin : do nothing, as the openRecent is disabled on the plugin.
			if (PanelView.getInstance() != null) {
				PanelView.getInstance().getMenuBar().getOpenControl().fireNewCassisFileOpen(getModel().getNameData());
			}
		}
		else if (LoadDataModel.CASSIS_SPECTRUM_EVENT.equals(event.getSource())) {
			CassisSpectrum cassisSpectrum = getModel().getCassisSpectrum();
			if (cassisSpectrum.isxError() || cassisSpectrum.isyError()) {
				JOptionPane.showMessageDialog(this, cassisSpectrum.getErrorMessage(), "Warning", JOptionPane.WARNING_MESSAGE);
			}
			setVlsrData(getModel().getCassisSpectrum().getVlsr());
		}
		else if (LoadDataModel.TELESCOPE_DATA_EVENT.equals(event.getSource())
				|| LoadDataModel.TELESCOPE_DATA_LOAD_EVENT.equals(event.getSource())) {
			validateTelescope();
		}
		else if (LoadDataModel.VLSR_DATA_EVENT.equals(event.getSource())) {
			double value = (Double) event.getValue();
			setVlsrData(value);
			refreshTypeFrequency(value);
		}
		else if (LoadDataModel.TYPE_FREQUENCY_EVENT.equals(event.getSource())) {
			TypeFrequency type = (TypeFrequency) event.getValue();
			getTypeFrequency().setSelectedItem(type.forSave());
		}
		else if (LoadDataModel.VO_LIST_FILES_EVENT.equals(event.getSource())) {
			final VoListFiles voListFiles = new VoListFiles((StarTable) event.getValue(), model);
			final JFrame jframe = new JFrame("Votable reader");
			jframe.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			jframe.setContentPane(voListFiles);
			jframe.setVisible(true);
			jframe.pack();
			if (voListFiles.getFileFoundCount() >= 1) {
				voListFiles.getValidateButton().addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						jframe.dispose();
						ListenersUtil.removeActionListeners(voListFiles.getValidateButton());
					}
				});
			}
		}
	}

	/**
	 * @return the model
	 */
	public final LoadDataModel getModel() {
		return model;
	}

	public void setDataName(String nameData) {
		dataFileText.setText(nameData);
	}

	public void setVlsrData(double vlsr) {
		vlsrDataField.setValue(vlsr);
	}

	@Override
	public void setCassisSpectrum(CassisSpectrum cassisSpectrum) {
		model.setCassisSpectrum(cassisSpectrum);
	}

	@Override
	public void setNameData(String title) {
		model.setNameData(title);
	}

	private void validateTelescope() {
		getTelescopeButton().setText(model.getTelescope());
	}

	private void browseTelescopeFile() {
		JFileChooser fc = new CassisJFileChooser(Software.getTelescopePath(),
				Software.getLastFolder("lte_radex-model-telescope"));
		fc.setDialogTitle("Select a telescope file");
		int ret = fc.showOpenDialog(this);
		if (ret == JFileChooser.APPROVE_OPTION) {
			model.setTelescope(fc.getSelectedFile().getAbsolutePath());
			Software.setLastFolder("lte_radex-model-telescope",
					fc.getSelectedFile().getParent());
		}
	}

	/**
	 * Set a new model.
	 *
	 * @param newModel The new LoadDataModel to use.
	 */
	public void setModel(LoadDataModel newModel) {
		model.removeModelListener(this);
		model = newModel;
		model.addModelListener(this);
		refresh();
	}

	/**
	 * Refresh the view.
	 */
	public void refresh() {
		setDataName(getModel().getNameData());
		CassisSpectrum cassisSpectrum = getModel().getCassisSpectrum();
		if (cassisSpectrum.isxError() || cassisSpectrum.isyError()) {
			JOptionPane.showMessageDialog(this, cassisSpectrum.getErrorMessage(), "Warning", JOptionPane.WARNING_MESSAGE);
		}
		double vlsrTmp = getModel().getCassisSpectrum().getVlsr();
		setVlsrData(vlsrTmp);
		refreshTypeFrequency(vlsrTmp);
		getTypeFrequency().setSelectedItem(model.getTypeFrequency());
		validateTelescope();
	}

	/**
	 * Refresh the TypeFrequency.
	 *
	 * @param vlsr the vlsr used to determine the TypeFrequency.
	 */
	private void refreshTypeFrequency(double vlsr) {
		if (vlsr == 0) {
			model.setTypeFrequency(TypeFrequency.SKY);
		} else {
			model.setTypeFrequency(TypeFrequency.REST);
		}
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(new LoadDataPanel());
		frame.pack();
		frame.setVisible(true);
	}

}
