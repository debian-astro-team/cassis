/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.data;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.starlink.table.ColumnInfo;
import uk.ac.starlink.table.RowSequence;
import uk.ac.starlink.table.StarTable;

public class VoListFiles extends JPanel {

	private static final long serialVersionUID = -864396275745902416L;
	private static final Logger LOGGER = LoggerFactory.getLogger(VoListFiles.class);
	private StarTable starTable;
	private LoadVoDataInterface loadInterface;
	private List<ArrayList<String>> data = new ArrayList<>();
	private List<ArrayList<String>> columnInfo = new ArrayList<>();
	private int numColTitle;
	private int numColUrl;
	private JTable tableFileInfo;
	private JList<String> fileList;
	private JButton validateButton;
	private int fileFoundCount = 0;


	public VoListFiles(StarTable value, LoadVoDataInterface loadInterface) {
		super(new BorderLayout());

		this.starTable = value;
		this.loadInterface = loadInterface;
		initsComponents();
	}

	public void initsComponents() {
		if (starTable == null) {
			return;
		}

		fileFoundCount = parseInfo(starTable);
		if (fileFoundCount >= 1) {
			JScrollPane listScroller = new JScrollPane(getJList());
			listScroller.setPreferredSize(new Dimension(1300, 200));

			add(listScroller, BorderLayout.NORTH);

			JScrollPane tableScroller = new JScrollPane(getTableFileInfo());
			JPanel centerPanel = new JPanel(new BorderLayout());
			centerPanel.add(new JLabel(" "), BorderLayout.NORTH);
			centerPanel.add(tableScroller, BorderLayout.CENTER);
			add(centerPanel, BorderLayout.CENTER);

			getValidateButton().setPreferredSize(new Dimension(100, 30));

			JPanel buttonPanel = new JPanel();
			buttonPanel.add(getValidateButton());
			add(buttonPanel, BorderLayout.SOUTH);
			this.setPreferredSize(new Dimension(1300, 600));
		} else {
			this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
			add(new JLabel(UIManager.getIcon("OptionPane.errorIcon")), BorderLayout.WEST);
			add(new JLabel("No file found."), BorderLayout.CENTER);
		}
		repaint();
	}

	public int getFileFoundCount() {
		return fileFoundCount;
	}

	public JButton getValidateButton() {
		if (validateButton == null) {
			validateButton = new JButton("Validate");
			validateButton.setEnabled(false);
			validateButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					if (getJList().getSelectedIndex() >= 0 || getJList().getSelectedIndex() < data.size()) {
						String url = data.get(getJList().getSelectedIndex()).get(numColUrl).trim();
						if (loadInterface != null) {
							loadInterface.loadData(url);
						}
					}
				}
			});
		}
		return validateButton;
	}

	private JList<String> getJList() {
		if (fileList == null) {
			DefaultListModel<String>listModel = new DefaultListModel<>();
			for (int i = 0; i < data.size(); i++) {
				listModel.addElement(data.get(i).get(numColTitle));
			}
			fileList = new JList<>(listModel);
			fileList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			fileList.setLayoutOrientation(JList.VERTICAL);
			fileList.setVisibleRowCount(5);

			fileList.addListSelectionListener(new ListSelectionListener() {
				@Override
				public void valueChanged(ListSelectionEvent e) {
					if (!e.getValueIsAdjusting() && fileList.getSelectedIndex() != -1) {
						updateJTable(fileList.getSelectedIndex());
						getValidateButton().setEnabled(true);
					}

				}
			});
		}
		return fileList;
	}

	private int parseInfo(StarTable starTable) {
		int result = 0;
		ArrayList<String> currentList;
		int nbColumn = starTable.getColumnCount();

		for (int i = 0; i < nbColumn; i++) {
			ColumnInfo info = starTable.getColumnInfo(i);
			currentList = new ArrayList<>();
			currentList.add(info.getName());
			currentList.add(info.getUCD());
			currentList.add(info.getUtype());
			currentList.add(info.getDescription());

			if (info.getUtype()!= null) {
				if ("ssa:DataID.Title".equalsIgnoreCase(info.getUtype())) {
					numColTitle = i;
				} else if ("ssa:access.reference".equalsIgnoreCase(info.getUtype())) {
					numColUrl = i;
				}
			}
			if (info.getUCD() != null &&
					("DATA_LINK".equalsIgnoreCase(info.getUCD())
							|| "meta.ref.url".equalsIgnoreCase(info.getUCD())))
				numColUrl = i;

			columnInfo.add(currentList);
		}

		if (numColTitle == -1 || numColUrl == -1) {
			return result;
		}

		RowSequence rseq = null;
		try {
			rseq = starTable.getRowSequence();

			ArrayList<String> list;
			Object[] row;

			while (rseq.next()) {
				row = rseq.getRow();
				list = new ArrayList<>(nbColumn);

				for (int col = 0; col < nbColumn; col++) {
					list.add(String.valueOf(row[col]));
				}
				data.add(list);
				result++;
			}
		} catch (IOException e) {
			LOGGER.error("An exception occured while parsing the file", e);
			return result;
		} finally {
			if (rseq != null) {
				try {
					rseq.close();
				} catch (IOException ioe) {
					LOGGER.trace("Error while closing the RowSequence", ioe);
				}
			}
		}
		return result;
	}

	private JTable getTableFileInfo() {
		if (tableFileInfo == null) {
			String[] columNames = {"Name", "UCD", "UTYPE", "Description", "Value"};
			DefaultTableModel model = new DefaultTableModel(columnInfo.size(), columNames.length);
			model.setColumnIdentifiers(columNames);
			tableFileInfo = new JTable(model);
			tableFileInfo.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
		}
		return tableFileInfo;
	}

	private void updateJTable(int value) {
		if (value < 0 || value >= data.size()) {
			return;
		}
		String cellValue = null;
		for (int i = 0; i < columnInfo.size(); i++) {
			for (int j = 0; j < 5; j++) {
				switch (j) {
				case 0:
				case 1:
				case 2:
				case 3:
					cellValue = columnInfo.get(i).get(j);
					break;
				case 4:
					cellValue = data.get(value).get(i);
					break;
				}
				getTableFileInfo().setValueAt(cellValue, i, j);
			}
		}
	}

	// Used on plugin.
	public void setInterface(LoadVoDataInterface newInterface) {
		this.loadInterface = newInterface;
	}
}
