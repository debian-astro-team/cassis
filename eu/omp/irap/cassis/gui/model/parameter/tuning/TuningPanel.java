/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.tuning;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.util.HashMap;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.gui.util.JDoubleCassisTextField;
import eu.omp.irap.cassis.gui.util.ListenersUtil;

@SuppressWarnings("serial")
public class TuningPanel extends JPanel implements ModelListener, ActionListener {

	private TuningModel model;
	private JDoubleCassisTextField minText;
	private JDoubleCassisTextField maxText;
	private JDoubleCassisTextField bandText;
	private boolean bandVisible = false;
	private JLabel bandLabel;
	private JComboBox<UNIT> valUnitComboBox;
	private JComboBox<UNIT> bandUnitComboBox;


	public TuningPanel() {
		this(new TuningModel(true, true, UNIT.KM_SEC_MOINS_1));
		setPreferredSize(new Dimension(790, 60));
		setSize(new Dimension(790, 60));
	}

	public TuningPanel(TuningModel model) {
		this.model = model;
		model.addModelListener(this); // handle messages from Model

		setLayout(new FlowLayout(FlowLayout.LEFT));
		setBorder(new TitledBorder("Tuning"));
		add(new JLabel("Range min:"));
		add(getMinText());
		add(new JLabel("max:"));
		add(getMaxText());
		add(getValUnitComboBox());

		if (model.haveBandVisible()) {
			bandVisible = true;
			add(getBandLabel());
			add(getBandText());
			add(getBandUnitComboBox());
		}
	}

	public JLabel getBandLabel() {
		if (bandLabel == null) {
			bandLabel = new JLabel("Band:");
		}
		return bandLabel;
	}

	/**
	 * @return the model
	 */
	public final TuningModel getModel() {
		return model;
	}

	/**
	 * @return the minText
	 */
	public final JDoubleCassisTextField getMinText() {
		if (minText == null) {
			minText = new JDoubleCassisTextField(new DecimalFormat("0.0##############"),
					new DecimalFormat("#.###############E0"), new HashMap<>());
			minText.setColumns(7);
			minText.setValMin(0.000001);
			minText.setValMax(999999.999);
			minText.addPropertyChangeListener("value", new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					model.setMinValue(Double.valueOf(evt.getNewValue().toString()));
				}
			});
			setMinText(model.getMinValue());
			minText.setSymbols(new HashMap<>());
		}
		return minText;
	}

	/**
	 * @return the maxText
	 */
	public final JDoubleCassisTextField getMaxText() {
		if (maxText == null) {
			maxText = new JDoubleCassisTextField(new DecimalFormat("0.0##############"),
					new DecimalFormat("#.###############E0"), new HashMap<>());
			maxText.setColumns(7);
			maxText.setValMin(0.000001);
			maxText.setValMax(999999.999);
			maxText.addSymbolEtoile();
			maxText.addPropertyChangeListener("value", new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					model.setMaxValue(Double.valueOf(evt.getNewValue().toString()));
				}
			});
			setMaxText(model.getMaxValue());
		}
		return maxText;
	}

	/**
	 * @return the bandText
	 */
	public final JDoubleCassisTextField getBandText() {
		if (bandText == null) {
			bandText = new JDoubleCassisTextField(new DecimalFormat("0.0##############"),
					new DecimalFormat("#.###############E0"), new HashMap<>());
			bandText.setColumns(7);
			bandText.addSymbolEtoile();
			bandText.setValMin(0.000001);
			bandText.setValMax(999999.999);
			bandText.addPropertyChangeListener("value", new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					Double val = Double.valueOf(evt.getNewValue().toString());
					model.setBandValue(val);
				}
			});
			setBandText(model.getBandValue());
		}
		return bandText;
	}

	/**
	 * @return the refFreqUnitComboBox
	 */
	public final JComboBox<UNIT> getValUnitComboBox() {
		if (valUnitComboBox == null) {
			valUnitComboBox = new JComboBox<>(UNIT.getXValUnit());
			valUnitComboBox.setSelectedItem(model.getValUnit());
			valUnitComboBox.addActionListener(this);
		}
		return valUnitComboBox;
	}

	/**
	 * @return the bandUnitComboBox
	 */
	public final JComboBox<UNIT> getBandUnitComboBox() {
		if (bandUnitComboBox == null) {
			bandUnitComboBox = new JComboBox<>(UNIT.getXBandUnit());
			bandUnitComboBox.setSelectedItem(model.getBandUnit());
			bandUnitComboBox.addActionListener(this);
		}
		return bandUnitComboBox;
	}

	/**
	 * @param minText
	 *            the minTextField to set
	 */
	private final void setMinText(double value) {
		minText.setValue(value);
	}

	/**
	 * @param maxText
	 *            the maxTextField to set
	 */
	private final void setMaxText(double value) {
		maxText.setValue(value);
	}

	/**
	 * @param bandText
	 *            the bandTextField to set
	 */
	private final void setBandText(double value) {
		bandText.setValue(value);
	}

	/**
	 * Change the unit of the range.
	 * @param unit The unit to set.
	 */
	private void setValUnit(UNIT unit) {
		valUnitComboBox.setSelectedItem(unit);
	}

	/**
	 * Change the unit of the band.
	 * @param unit The unit to set.
	 */
	private void setBandUnit(UNIT unit) {
		bandUnitComboBox.setSelectedItem(unit);
	}

	/**
	 * @see eu.omp.irap.cassis.common.events.ModelListener#dataChanged(eu.omp.irap.cassis.common.events.ModelChangedEvent)
	 */
	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (TuningModel.MIN_VALUE_EVENT.equals(event.getSource())) {
			setMinText((Double) event.getValue());
		}
		else if (TuningModel.MAX_VALUE_EVENT.equals(event.getSource())) {
			setMaxText((Double) event.getValue());
		}
		else if (TuningModel.BAND_VALUE_EVENT.equals(event.getSource())) {
			if (model.haveBandVisible())
				setBandText((Double) event.getValue());
		}
		else if (TuningModel.BAND_UNIT_EVENT.equals(event.getSource())) {
			if (model.haveBandVisible())
				setBandUnit((UNIT) event.getValue());
		}
		else if (TuningModel.VAL_UNIT_EVENT.equals(event.getSource())) {
			setValUnit((UNIT) event.getValue());
		}
		else if (TuningModel.UNIQUE_UNIT_EVENT.equals(event.getSource())) {
			getValUnitComboBox().setVisible((Boolean)event.getValue());
		}
	}

	/**
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == getValUnitComboBox()) {
			UNIT unit = (UNIT) getValUnitComboBox().getSelectedItem();
			UNIT oldUnit = model.getValUnit();

			XAxisCassis anXAxisCassis = XAxisCassis.getXAxisCassis(unit);
			XAxisCassis oldXAxisCassis = XAxisCassis.getXAxisCassis(oldUnit);

			double minValue = model.getMinValue();
			double maxValue = model.getMaxValue();
			boolean minZero = minValue == 0.0;
			boolean maxEtoile = "*".equals(getMaxText().getText());
			boolean invertedRelativeToOld = anXAxisCassis.isInverted() != oldXAxisCassis.isInverted();

			if (invertedRelativeToOld) {
				if (minZero) {
					getMaxText().setValue(Double.MAX_VALUE);
				} else {
					getMaxText().setValue(XAxisCassis.convert(minValue, oldXAxisCassis, anXAxisCassis));
				}

				if (maxEtoile) {
					getMinText().setValue(0.0);
				} else {
					getMinText().setValue(XAxisCassis.convert(maxValue, oldXAxisCassis, anXAxisCassis));
				}
			} else {
				if (!minZero) {
					getMinText().setValue(XAxisCassis.convert(minValue, oldXAxisCassis, anXAxisCassis));
				}
				if (!maxEtoile) {
					getMaxText().setValue(XAxisCassis.convert(maxValue, oldXAxisCassis, anXAxisCassis));
				}
			}
			model.setValUnit(unit);
		}else if (e.getSource() == getBandUnitComboBox()) {
			model.setBandUnit((UNIT) getBandUnitComboBox().getSelectedItem());
		}
	}

	/**
	 * Set a new model.
	 *
	 * @param newModel The new TuningModel to use.
	 */
	public void setModel(TuningModel newModel) {
		model.removeModelListener(this);
		model = newModel;
		model.addModelListener(this);
		refresh();
	}

	/**
	 * Refresh the view.
	 */
	private void refresh() {
		getValUnitComboBox().setSelectedItem(model.getValUnit());
		setMinText(model.getMinValue());
		setMaxText(model.getMaxValue());

		/* Caution : model.haveBandVisible != bandVisible
		 * bandVisible is the current view, the other is the model.
		 * At this point, they could be different !
		 * At the end of this function, the must be the same. */
		if (model.haveBandVisible()) {
			if (!bandVisible) {
				bandVisible = true;
				add(getBandLabel());
				add(getBandText());
				add(getBandUnitComboBox());
				revalidate();
			} else {
				bandUnitComboBox.setSelectedItem(model.getBandUnit());
				setBandText(model.getBandValue());
			}
		} else if (bandVisible) {
			bandVisible = false;
			remove(bandLabel);
			bandLabel = null;

			ListenersUtil.removePropertyChangeListeners(bandText);

			remove(bandText);
			bandText = null;

			bandUnitComboBox.removeActionListener(this);
			remove(bandUnitComboBox);
			bandUnitComboBox = null;
		}
		repaint();
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(new TuningPanel());
		frame.pack();
		frame.setVisible(true);
	}
}
