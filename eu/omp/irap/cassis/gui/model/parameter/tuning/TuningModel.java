/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.tuning;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import eu.omp.irap.cassis.common.BufferedWriterProperty;
import eu.omp.irap.cassis.common.ParameterDescription;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.XAxisVelocity;
import eu.omp.irap.cassis.common.events.DataModel;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;


public class TuningModel extends DataModel {

	public static final String BAND_UNIT_EVENT = "bandUnit";
	public static final String BAND_VALUE_EVENT = "bandValue";
	public static final String MAX_VALUE_EVENT = "maxValue";
	public static final String MIN_VALUE_EVENT = "minValue";
	public static final String UNIQUE_UNIT_EVENT = "uniqueUnit";
	public static final String VAL_UNIT_EVENT = "valUnit";

	private double minValue;
	private double maxValue;
	private double bandValue;
	private UNIT valUnit;
	private UNIT bandUnit;
	public static final double PACS_BAND_KMS = 10000;
	public static final double SPIRE_BAND_KMS = 10000;
	public static final double DEFAULT_BAND_KMS = 60;
	private boolean uniqueUnit = false;
	private boolean haveBand;
	private boolean haveBandVisible;
	private UNIT failbackLoadBandUnit;


	public TuningModel(boolean band, UNIT failbackLoadBandUnit) {
		this(band, band, failbackLoadBandUnit);
	}

	public TuningModel(boolean haveBand, boolean haveBandVisible, UNIT failbackLoadBandUnit) {
		this.haveBand = haveBand;
		this.haveBandVisible = haveBandVisible;
		if (haveBandVisible && !haveBand)
			this.haveBand = true;

		this.failbackLoadBandUnit = failbackLoadBandUnit;
		minValue = 0.;
		maxValue = Double.MAX_VALUE;
		bandValue = DEFAULT_BAND_KMS;
		valUnit = UNIT.GHZ;
		bandUnit = UNIT.KM_SEC_MOINS_1;
	}

	/**
	 * @return the minValue
	 */
	public final double getMinValue() {
		return minValue;
	}

	/**
	 * @return the maxValue
	 */
	public final double getMaxValue() {
		return maxValue;
	}

	/**
	 * @return the bandValue
	 */
	public final double getBandValue() {
		return bandValue;
	}

	/**
	 * @return the valUnit
	 */
	public final UNIT getValUnit() {
		return valUnit;
	}

	/**
	 * @return the bandUnit
	 */
	public final UNIT getBandUnit() {
		return bandUnit;
	}

	/**
	 * @param minValue
	 *            the minValue to set
	 */
	public final void setMinValue(double minValue) {
		this.minValue = minValue;
		fireDataChanged(new ModelChangedEvent(MIN_VALUE_EVENT, this.minValue));
	}

	/**
	 * @param maxValue
	 *            the maxValue to set
	 */
	public final void setMaxValue(double maxValue) {
		this.maxValue = maxValue;
		fireDataChanged(new ModelChangedEvent(MAX_VALUE_EVENT, this.maxValue));
	}

	/**
	 * @param bandValue
	 *            the bandValue to set
	 */
	public final void setBandValue(double bandValue) {
		this.bandValue = bandValue;
		fireDataChanged(new ModelChangedEvent(BAND_VALUE_EVENT, this.bandValue));
	}

	/**
	 * @param valUnit
	 *            the valUnit to set
	 */
	public final void setValUnit(UNIT valUnit) {
		this.valUnit = valUnit;
		fireDataChanged(new ModelChangedEvent(VAL_UNIT_EVENT, this.valUnit));
	}

	/**
	 * @param bandUnit
	 *            the bandUnit to set
	 */
	public final void setBandUnit(UNIT bandUnit) {
		this.bandUnit = bandUnit;
		fireDataChanged(new ModelChangedEvent(BAND_UNIT_EVENT, this.bandUnit));
	}

	@Override
	public void saveConfig(BufferedWriterProperty out) throws IOException {
		out.writeProperty("minValue", String.valueOf(getMinValue()));
		out.writeProperty("maxValue", String.valueOf(getMaxValue()));
		out.writeProperty("valUnit", getValUnit().name());
		if (haveBand) {
			out.writeProperty("bandValue", String.valueOf(getBandValue()));
			out.writeProperty("bandUnit", getBandUnit().name());
		}
		out.flush();
	}

	@Override
	public void loadConfig(Properties prop) {
		setMinValue(Double.valueOf(prop.getProperty("minValue", prop.getProperty("rangeMin", prop.getProperty("valMin")))));
		setMaxValue(Double.valueOf(prop.getProperty("maxValue", prop.getProperty("rangeMax", prop.getProperty("valMax")))));
		String tmpValUnit = prop.getProperty("valUnit", UNIT.GHZ.name());
		if ("null".equals(tmpValUnit))
			setValUnit(UNIT.GHZ);
		else
			this.valUnit = UNIT.valueOf(tmpValUnit);

		if (haveBand) {
			String tmpBandUnit = prop.getProperty("bandUnit");
			if (tmpBandUnit == null || "null".equals(tmpBandUnit) || "Unknow".equals(tmpBandUnit) ||
					UNIT.valueOf(tmpBandUnit) == UNIT.UNKNOWN) {
				setBandUnit(failbackLoadBandUnit);
			} else {
				setBandUnit(UNIT.valueOf(tmpBandUnit));
			}
			if (prop.containsKey("bandwith")|| prop.containsKey("bandValue") || prop.containsKey("valBand")) {
				String tmpBandValue = prop.getProperty("bandValue", prop.getProperty("bandwith", prop.getProperty("valBand")));
				if (tmpBandValue != null && !"null".equals(tmpBandValue)) {
					setBandValue(Double.valueOf(tmpBandValue));
				}
			}
		}
	}

	public void setUniqueUnit(boolean uniqueUnit) {
		this.uniqueUnit = uniqueUnit;
		fireDataChanged(new ModelChangedEvent(UNIQUE_UNIT_EVENT, this.uniqueUnit));
		setBandUnit(getValUnit());
	}

	public boolean isUniqueUnit() {
		return this.uniqueUnit;
	}

	/**
	 * @return [min, max, band] value in MHz
	 */
	public Double[] getValuesInMHz() {
		Double[] vals = new Double[3];
		XAxisCassis axisCassis = XAxisCassis.getXAxisCassis(getValUnit());
		Double valMin = axisCassis.convertToMHzFreq(getMinValue());
		Double valMax = axisCassis.convertToMHzFreq(getMaxValue());
		vals[0] = Math.min(valMin, valMax);
		vals[1] = Math.max(valMin, valMax);
		XAxisCassis axisCassisBand = XAxisCassis.getXAxisCassis(getBandUnit());

		if (UNIT.KM_SEC_MOINS_1.equals(getBandUnit())) {
			((XAxisVelocity) axisCassisBand).setCoeff(0., vals[1]);
		}
		vals[2] = getBandValue();

		if (!(vals[2] == null || vals[2] == 0. || Double.isNaN(vals[2]) || vals[2].equals(Double.MAX_VALUE))) {
			vals[2] =  axisCassisBand.convertToMHzFreq(getBandValue());
		}
		else{
			vals[2] = Math.abs(valMin - valMax);
		}

		return vals;
	}

	public Map<String, ParameterDescription> getMapParameter() {
		HashMap<String, ParameterDescription> result = new HashMap<>();
		Double[] valuesInMHz = getValuesInMHz();
		result.put("freqMin", new ParameterDescription(valuesInMHz[0]));
		result.put("freqMax", new ParameterDescription(valuesInMHz[1]));
		result.put("bandwidth", new ParameterDescription(getBandValue()));
		ParameterDescription parameterDescriptionUnit = new ParameterDescription();
		parameterDescriptionUnit.setVarName(getBandUnit().name());
		result.put("bandwidthUnit", parameterDescriptionUnit);
		return result;
	}

	/**
	 * @return if the band is displayed on the view.
	 */
	public boolean haveBandVisible() {
		return haveBandVisible;
	}
}
