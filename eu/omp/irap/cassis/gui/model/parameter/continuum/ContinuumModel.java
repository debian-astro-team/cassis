/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.continuum;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import eu.omp.irap.cassis.common.BufferedWriterProperty;
import eu.omp.irap.cassis.common.ParamArrayDescription;
import eu.omp.irap.cassis.common.ParameterDescription;
import eu.omp.irap.cassis.common.events.DataModel;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.gui.util.PropertiesLinkedHashMap;
import eu.omp.irap.cassis.parameters.ContinuumParameters;
import eu.omp.irap.cassis.properties.Software;

public class ContinuumModel extends DataModel implements Cloneable {

	public static final String CONTINUUM_EVENT = "continuum";
	public static final String CONTINUUM_NOT_FOUND_EVENT = "continuumNotFound";

	protected ContinuumParameters continuumParams;


	public ContinuumModel() {
		continuumParams = new ContinuumParameters();
		continuumParams.resetContinuumParameters();
	}

	public ContinuumModel(ContinuumParameters continuumParams) {
		this.continuumParams = continuumParams;
	}

	/**
	 * Get selected continuum name.
	 *
	 * @return Selected continuum name
	 */
	public String getSelectedContinuum() {
		return continuumParams.getSelectedContinuum();
	}

	/**
	 * Set selected continuum
	 *
	 * @param continuum : Selected continuum name
	 */
	public void setSelectedContinuum(String continuum) {
		String originalContinuum = continuum;
		boolean error = false;
		if (!continuum.endsWith(ContinuumParameters.DEFAULT_0)
				&& !continuum.endsWith(ContinuumParameters.DEFAULT_1)
				&& !continuum.endsWith("continuum-0")
				&& !continuum.endsWith("continuum-1")
				&& !new File(continuum).exists()
				&& !new File(Software.getContinuumPath() + File.separator + continuum).exists()) {
			continuum = ContinuumParameters.DEFAULT_1;
			error = true;
		}
		continuumParams.setSelectedContinuum(continuum);
		fireDataChanged(new ModelChangedEvent(CONTINUUM_EVENT, continuum));
		if (error) {
			fireDataChanged(new ModelChangedEvent(CONTINUUM_NOT_FOUND_EVENT, originalContinuum));
		}
	}

	/**
	 * @return the continuumParams
	 */
	public final ContinuumParameters getContinuumParams() {
		return continuumParams;
	}

	@Override
	public void saveConfig(BufferedWriterProperty out) throws IOException {
		out.writeProperty("continuum", continuumParams.getSelectedContinuum());
		out.flush();
	}

	@Override
	public void loadConfig(Properties prop) {
		setSelectedContinuum(prop.getProperty("continuum"));
	}

	public Map<String, ParameterDescription> getMapParameter() {
		Map<String, ParameterDescription> result = new HashMap<>();

		if (continuumParams.isPowerlaw()) {
			result.put("continuumAlpha", new ParameterDescription(continuumParams.getContinuumAlpha()));
			result.put("continuumBeta", new ParameterDescription(continuumParams.getContinuumBeta()));
		}
		else if (continuumParams.isConstantContinuum()) {
			result.put("continuum", new ParameterDescription(continuumParams.getContinuum()));
		}
		else {
			result.put("continuumFreqList", new ParamArrayDescription(continuumParams.getFreqContinuumList()));
			result.put("continuumValueList", new ParamArrayDescription(continuumParams.getValueContinuumList()));
		}

		return result;
	}

	public Map<String, String> getProperties() {
		Map<String, String> map = new PropertiesLinkedHashMap();
		map.put("continuum", continuumParams.getSelectedContinuum());
		return map;
	}
}
