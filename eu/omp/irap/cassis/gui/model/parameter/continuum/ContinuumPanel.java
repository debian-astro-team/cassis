/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.continuum;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.properties.Software;

public class ContinuumPanel extends JPanel implements ModelListener {

	private static final long serialVersionUID = 1L;
	private JButton continuumButton;
	private ContinuumModel model;
	private String typeName = "lte_radex-model-continuum";

	public ContinuumPanel() {
		this(new ContinuumModel());
	}

	public ContinuumPanel(ContinuumModel model) {
		this.model = model;
		this.setBorder(new TitledBorder("Continuum"));

		continuumButton = new eu.omp.irap.cassis.gui.util.MyButton("Browse");
		continuumButton.setText(model.getSelectedContinuum());
		continuumButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				browseContinuumFile();
			}
		});
		add(continuumButton);
		setSize(170, 60);
		setPreferredSize(new Dimension(170, 60));
		model.addModelListener(this);
		displayUnit();
	}

	protected void browseContinuumFile() {
		JFileChooser loadFile = new CassisJFileChooser(Software.getContinuumPath(),
				Software.getLastFolder(typeName));
		loadFile.setDialogTitle("Select a continuum file");
		int ret = loadFile.showOpenDialog(this);
		if (ret == JFileChooser.APPROVE_OPTION) {
			model.setSelectedContinuum(loadFile.getSelectedFile().getAbsolutePath());
			Software.setLastFolder(typeName, loadFile.getSelectedFile().getParent());
		}
	}

	/**
	 * @return the model
	 */
	public final ContinuumModel getModel() {
		return model;
	}

	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (ContinuumModel.CONTINUUM_EVENT.equals(event.getSource())) {
			continuumButton.setText(model.getSelectedContinuum());
			displayUnit();
		}
		else if (ContinuumModel.CONTINUUM_NOT_FOUND_EVENT.equals(event.getSource())) {
			String continuumPath = event.getValue().toString();
			JOptionPane.showMessageDialog(null, "Continuum file \"" + continuumPath +
					"\" not found! The continuum was set to Continuum 1 [K].",
					"Cassis error", JOptionPane.ERROR_MESSAGE);
		}
	}

	private void displayUnit() {
		String unitX = model.getContinuumParams().getXAxisCassis().getUnit().toString();
		String unitY = model.getContinuumParams().getYAxisCassis().getUnit().toString();
		this.setToolTipText("<html>X Axis : " + unitX + "<br/>" +
				"Y Axis : " + unitY + "</html>");
		continuumButton.setToolTipText("<html>X Axis : " + unitX + "<br/>" +
				"Y Axis : " + unitY + "</html>");
	}

	/**
	 * @param typeName the typeName to set
	 */
	public final void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public void disableContinuumButton() {
		continuumButton.setEnabled(false);
	}

	public void enableContinuumButton() {
		continuumButton.setEnabled(true);
	}

	/**
	 * Set a new model.
	 *
	 * @param newModel The new ContinuumModel to use.
	 */
	public void setModel(ContinuumModel newModel) {
		model.removeModelListener(this);
		model = newModel;
		model.addModelListener(this);
		refresh();
	}

	/**
	 * Refresh the view.
	 */
	public void refresh() {
		model.setSelectedContinuum(model.getSelectedContinuum());
		displayUnit();
	}

	/**
	 * Return the continuum button.
	 *
	 * @return the continuum button.
	 */
	public JButton getContinuumButton() {
		return continuumButton;
	}

}
