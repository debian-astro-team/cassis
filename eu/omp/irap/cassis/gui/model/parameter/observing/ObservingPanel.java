/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.observing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;

public class ObservingPanel extends JPanel implements ModelListener {

	private static final long serialVersionUID = 1L;
	private ObservingModel model;
	private JComboBox<ObservingMode> observingBox;


	public ObservingPanel(ObservingModel model) {
		this.model = model;
		model.addModelListener(this);

		initComponents();
	}

	private void initComponents() {
		ObservingMode[] observingChoices = new ObservingMode[] {ObservingMode.PSDBS, ObservingMode.FSW};
		observingBox = new JComboBox<>(observingChoices);
		observingBox.setSelectedItem(model.getObservingMode());
		observingBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ObservingMode value = (ObservingMode) ((JComboBox<?>)e.getSource()).getSelectedItem();
				model.setObserving(value);
			}
		});

		setBorder(new TitledBorder("Observing Mode"));
		add(observingBox);
	}

	/**
	 * Refresh the view.
	 */
	public void refresh() {
		observingBox.setSelectedItem(model.getObservingMode());
	}

	public ObservingModel getModel() {
		return model;
	}

	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (ObservingModel.OBSERVING_MODE_EVENT.equals(event.getSource())) {
			ObservingMode value = model.getObservingMode();
			observingBox.setSelectedItem(value);
		}
	}

	/**
	 * Set a new model.
	 *
	 * @param newModel The new FrequencyModel to use.
	 */
	public void setModel(ObservingModel newModel) {
		model.removeModelListener(this);
		model = newModel;
		model.addModelListener(this);
		refresh();
	}
}
