/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.imin;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.gui.util.TextFieldFormatFilter;

public class TbgPanel extends JPanel implements ModelListener {

	private static final long serialVersionUID = 1L;
	private TbgModel model;
	private JTextField tbgText;
	private JComboBox<UNIT> tbgUnitBox;


	public TbgPanel() {
		this(new TbgModel());
	}

	public TbgPanel(TbgModel model) {
		this.model = model;
		model.addModelListener(this);

		initComponents();
		setVisible(true);
	}

	private void initComponents() {
		tbgText = new JTextField(4);
		tbgText.addKeyListener(new TextFieldFormatFilter(TextFieldFormatFilter.SCIENTIFIC_NUMBER));
		tbgText.setText(String.valueOf(model.getTbg()));
		tbgText.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				model.setTbg(Double.valueOf(tbgText.getText()));
			}
		});

		tbgText.addFocusListener(new FocusAdapter() {

			@Override
			public void focusLost(FocusEvent e) {
				model.setTbg(Double.valueOf(tbgText.getText()));
			}
		});

		UNIT[] unitChoices = new UNIT[] {UNIT.KELVIN};
		tbgUnitBox = new JComboBox<>(unitChoices);
		tbgUnitBox.setSelectedItem(model.getUnit());
		tbgUnitBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				UNIT value = (UNIT) ((JComboBox<?>)e.getSource()).getSelectedItem();
				model.setUnit(value);
			}
		});

		setBorder(new TitledBorder("Background"));
		add(new JLabel("Tbg:"));
		add(tbgText);
		add(tbgUnitBox);
		setPreferredSize(new Dimension(180, 60));
		FlowLayout flowLayout = new FlowLayout();
		flowLayout.setAlignment(FlowLayout.CENTER);
		setLayout(flowLayout);
	}

	/**
	 * Refresh the view.
	 */
	public void refresh() {
		tbgText.setText(String.valueOf(model.getTbg()));
		tbgUnitBox.setSelectedItem(model.getUnit());
	}

	public TbgModel getModel() {
		return model;
	}

	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (TbgModel.TBG_EVENT.equals(event.getSource())) {
			String value = String.valueOf(model.getTbg());
			tbgText.setText(value);
		} else if (TbgModel.TBG_UNIT_EVENT.equals(event.getSource())) {
			UNIT value = model.getUnit();
			tbgUnitBox.setSelectedItem(value);
		}
	}

	/**
	 * Set a new model.
	 *
	 * @param newModel The new IminModel to use.
	 */
	public void setModel(TbgModel newModel) {
		model.removeModelListener(this);
		model = newModel;
		model.addModelListener(this);
		refresh();
	}
}
