/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.imin;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import eu.omp.irap.cassis.common.BufferedWriterProperty;
import eu.omp.irap.cassis.common.ParameterDescription;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.events.DataModel;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.gui.util.PropertiesLinkedHashMap;

public class TbgModel extends DataModel {

	public static final String TBG_EVENT = "tbg";
	public static final String TBG_UNIT_EVENT = "tbgUnit";

	private double tbg;
	private UNIT tbgUnit;


	public TbgModel() {
		tbg = 2.73;
		tbgUnit = UNIT.KELVIN;
	}

	/**
	 * @return the imin
	 */
	public Double getTbg() {
		return tbg;
	}

	/**
	 * @param value the value of imin value to set
	 */
	public void setTbg(Double value) {
		this.tbg = value;
		fireDataChanged(new ModelChangedEvent(TBG_EVENT, value));
	}

	/**
	 * @return the unit
	 */
	public UNIT getUnit() {
		return tbgUnit;
	}

	/**
	 * @param value the UNIT to set
	 */
	public void setUnit(UNIT value) {
		this.tbgUnit = value;
		fireDataChanged(new ModelChangedEvent(TBG_UNIT_EVENT, value));
	}

	@Override
	public void saveConfig(BufferedWriterProperty out) throws IOException {
		out.writeProperty(TBG_EVENT, String.valueOf(tbg));
		out.writeProperty(TBG_UNIT_EVENT, String.valueOf(tbgUnit));
		out.newLine();
	}

	@Override
	public void loadConfig(Properties prop) throws IOException {
		String iminTemp = prop.getProperty(TBG_EVENT, "2.73");
		setTbg(Double.parseDouble(iminTemp));

		String iminUnitTemp = prop.getProperty(TBG_UNIT_EVENT);
		if (iminUnitTemp != null)
			setUnit(UNIT.toUnit(iminUnitTemp));
		else
			setUnit(UNIT.KELVIN);
	}

	public Map<String, ParameterDescription> getMapParameter() {
		Map<String, ParameterDescription> result = new HashMap<>();

		ParameterDescription param = new ParameterDescription(tbg);
		result.put(TBG_EVENT, param);
		return result;
	}

	public Map<String, String> getProperties() {
		Map<String, String> map = new PropertiesLinkedHashMap();
		map.put("tbg", String.valueOf(tbg));
		return map;
	}

}
