/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.telescope;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import eu.omp.irap.cassis.common.BufferedWriterProperty;
import eu.omp.irap.cassis.common.ParameterDescription;
import eu.omp.irap.cassis.common.Telescope;
import eu.omp.irap.cassis.common.events.DataModel;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.properties.Software;

public class TelescopeModel extends DataModel {

	public static final String SELECTED_TELESCOPE_EVENT = "selectedTelescope";

	public static final String DEFAULT_TELESCOPE = "apex";
	public static final String LOCAL_TELESCOPE_EVENT = "localTelescope";
	private Telescope telescope;
	private String selectedTelescope;
	private String telescopeError = null;


	public TelescopeModel() {
		selectedTelescope = DEFAULT_TELESCOPE;
		telescope = new Telescope("");
		init();
	}

	public void init() {
		if (!new File(selectedTelescope).exists()) {
			selectedTelescope = Software.getTelescopePath() + File.separatorChar
					+ selectedTelescope;
		}
		telescope.setFilename(selectedTelescope);
	}

	public String getSelectedTelescope() {
		return selectedTelescope;
	}

	public void setSelectedTelescope(String name) {
		setSelectedTelescope(name, SELECTED_TELESCOPE_EVENT);
	}

	public void setSelectedTelescope(String name, String event) {
		if (name == null || "".equals(name)) {
			return;
		}
		selectedTelescope = name;
		telescope.setFilename(selectedTelescope);
		fireDataChanged(new ModelChangedEvent(event, name));
	}

	public Telescope getTelescope() {
		return telescope;
	}

	public Map<String, ParameterDescription> getMapParameter() {
		Map<String, ParameterDescription> result = new HashMap<>();
		result.put("telaper", new ParameterDescription(telescope.getDiameter()));
		return result;
	}

	/**
	 * Write properties to the provided map.
	 *
	 * @param map the map.
	 */
	public void writeProperties(Map<String, String> map) {
		map.put("telescope", selectedTelescope);
	}

	@Override
	public void saveConfig(BufferedWriterProperty out) throws IOException {
		out.writeProperty("telescope", selectedTelescope);
	}

	@Override
	public void loadConfig(Properties prop) {
		if (prop.containsKey("telescope")) {
			if (Telescope.exist(prop.getProperty("telescope")))
				setSelectedTelescope(prop.getProperty("telescope"));
			else
				telescopeError = prop.getProperty("telescope");
		}
	}

	public Map<String, String> getProperties() {
		return null;
	}

	public String getTelescopeError() {
		return telescopeError;
	}

	public void resetTelescopeError() {
		telescopeError = null;
	}
}
