/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.telescope;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.gui.util.MyTelescopeButton;
import eu.omp.irap.cassis.properties.Software;

public class TelescopePanel extends JPanel implements ModelListener {

	private static final long serialVersionUID = 1L;
	protected TelescopeModel model;
	private JButton telescopeButton;
	private String typeName = "lte_radex-model-telescope";


	public TelescopePanel() {
		this(new TelescopeModel());
	}

	public TelescopePanel(TelescopeModel model) {
		this.model = model;
		model.addModelListener(this); // handle messages from Model

		initComponents();
		refresh();
	}

	void initComponents() {
		// Telescope
		telescopeButton = new MyTelescopeButton("Browse");
		telescopeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				browseTelescopeFile();
			}
		});

		setBorder(new TitledBorder("Parameters"));
		add(new JLabel("Telescope:"));
		add(telescopeButton);
		setSize(250, 60);
		FlowLayout flowLayout = (FlowLayout) getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
	}

	protected void browseTelescopeFile() {
		JFileChooser loadFile = new CassisJFileChooser(Software.getTelescopePath(),
				Software.getLastFolder(typeName));
		loadFile.setDialogTitle("Select a telescope file");
		int ret = loadFile.showOpenDialog(this);
		if (ret == JFileChooser.APPROVE_OPTION) {
			model.setSelectedTelescope(loadFile.getSelectedFile().getAbsolutePath());
			Software.setLastFolder(typeName, loadFile.getCurrentDirectory().getAbsolutePath());
		}
	}

	public void setBorder(boolean border) {
		if (!border) {
			setBorder(null);
		}
		else {
			setBorder(new TitledBorder("Parameters"));
		}
	}

	public JButton getTelescopeButton() {
		return telescopeButton;
	}

	public TelescopeModel getModel() {
		return model;
	}

	@Override
	public void dataChanged(ModelChangedEvent e) {
		if (model != null
				&& (TelescopeModel.SELECTED_TELESCOPE_EVENT.equals(e.getSource())
				|| TelescopeModel.LOCAL_TELESCOPE_EVENT.equals(e.getSource()))) {
			String selectedTelescope = (String) e.getValue();
			telescopeButton.setText(selectedTelescope);
		}
	}

	/**
	 * @param typeName the typeName to set
	 */
	public final void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	/**
	 * Set a new model.
	 *
	 * @param newModel The new TelescopeModel to use.
	 */
	public void setModel(TelescopeModel newModel) {
		model.removeModelListener(this);
		model = newModel;
		model.addModelListener(this);
		refresh();
	}

	/**
	 * Refresh the view.
	 */
	private void refresh() {
		getTelescopeButton().setText(model.getSelectedTelescope());
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(new TelescopePanel());
		frame.pack();
		frame.setVisible(true);
	}

}
