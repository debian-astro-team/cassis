/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.rotationaltuning;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.gui.util.ListenersUtil;

/**
 * View for the RotationalTuningPanel.
 *
 * @author M. Boiziot
 */
public class RotationalTuningPanel extends JPanel implements ModelListener {

	private static final long serialVersionUID = -3727425471817572643L;
	private RotationalTuningModel model;
	private JTextField unitTextField;
	private JCheckBox taTmbConvCheckBox;


	/**
	 * Constructor.
	 *
	 * @param model The model.
	 */
	public RotationalTuningPanel(RotationalTuningModel model) {
		super(new FlowLayout(FlowLayout.LEFT));
		this.model = model;
		model.addModelListener(this);
		initComponents();
	}

	/**
	 * Initializes the graphics components.
	 */
	private void initComponents() {
		this.setBorder(new TitledBorder("Tuning"));
		add(new JLabel("yUnit: "));
		add(getUnitTextField());
		add(getTaTmbConvCheckBox());
		setPreferredSize(new Dimension(280, 60));
		setSize(new Dimension(280, 60));
	}

	/**
	 * Create if needed then return the ta to tmb conversion {@link JCheckBox}.
	 *
	 * @return The ta to tmb conversion {@link JCheckBox}.
	 */
	public JCheckBox getTaTmbConvCheckBox() {
		if (taTmbConvCheckBox == null) {
			taTmbConvCheckBox = new JCheckBox("Ta->Tmb conv");
			taTmbConvCheckBox.setSelected(model.isTaTmbConv());
			taTmbConvCheckBox.addItemListener(new ItemListener() {

				@Override
				public void itemStateChanged(ItemEvent e) {
					boolean selected = taTmbConvCheckBox.isSelected();
					model.setTaTmbConv(selected);
				}
			});
		}
		return taTmbConvCheckBox;
	}

	/**
	 * Create if needed then return the unit {@link JTextField}.
	 *
	 * @return the unit {@link JTextField}.
	 */
	public JTextField getUnitTextField() {
		if (unitTextField == null) {
			unitTextField = new JTextField(4);
			unitTextField.setEditable(false);
			unitTextField.setText(model.getTypeTemperature().toString());
		}
		return unitTextField;
	}

	/**
	 * @see eu.omp.irap.cassis.common.events.ModelListener#dataChanged(eu.omp.irap.cassis.common.events.ModelChangedEvent)
	 */
	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (RotationalTuningModel.TA_TMB_CONV_EVENT.equals(event.getSource())) {
			getTaTmbConvCheckBox().setSelected(model.isTaTmbConv());
		} else if (RotationalTuningModel.TYPE_TEMPERATURE_EVENT.equals(event.getSource())) {
			refreshTypeTemperature();
		}
	}

	/**
	 * Refresh the temperature type and the other components related to it.
	 */
	private void refreshTypeTemperature() {
		TYPE_TEMPERATURE tt = model.getTypeTemperature();
		getUnitTextField().setText(tt.toString());
		if (tt == TYPE_TEMPERATURE.TA) {
			getTaTmbConvCheckBox().setEnabled(true);
		} else {
			getTaTmbConvCheckBox().setEnabled(false);
			model.setTaTmbConv(true);
		}
	}

	/**
	 * Remove all the listeners.
	 */
	public void removeAllListeners() {
		this.model.removeAllListener();
		ListenersUtil.removeItemListener(taTmbConvCheckBox);
	}
}
