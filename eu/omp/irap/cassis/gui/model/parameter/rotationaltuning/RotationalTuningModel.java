/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.rotationaltuning;

import java.io.IOException;
import java.util.Properties;

import eu.omp.irap.cassis.common.BufferedWriterProperty;
import eu.omp.irap.cassis.common.events.DataModel;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;

/**
 * Model for the RotationalTuning.
 *
 * @author M. Boiziot
 */
public class RotationalTuningModel extends DataModel {

	public static final String TA_TMB_CONV_EVENT = "taTmbConv";
	public static final String TYPE_TEMPERATURE_EVENT = "typeTemperature";

	private static final String TA_TMB_CONV_SAVE = "taTmbConv";
	private static final String TYPE_TEMPERATURE_SAVE = "typeTemperature";

	private static final boolean TA_TMB_CONV_DEFAULT = false;
	private static final TYPE_TEMPERATURE TYPE_TEMPERATURE_DEFAULT = TYPE_TEMPERATURE.TA;

	private boolean taTmbConv;
	private TYPE_TEMPERATURE typeTemperature;


	/**
	 * Constructor.
	 */
	public RotationalTuningModel() {
		this(TA_TMB_CONV_DEFAULT, TYPE_TEMPERATURE_DEFAULT);
	}

	/**
	 * Constructor.
	 *
	 * @param taTmbConv of the ta to tmb conversion is selected.
	 * @param typeTemperature The temperature type.
	 */
	public RotationalTuningModel(boolean taTmbConv, TYPE_TEMPERATURE typeTemperature) {
		this.taTmbConv = taTmbConv;
		this.typeTemperature = typeTemperature;
	}

	/**
	 * Return if the ta to tmb conversion is enabled.
	 *
	 * @return if the ta to tmb conversion is enabled.
	 */
	public boolean isTaTmbConv() {
		return taTmbConv;
	}

	/**
	 * Change the state of the ta to tmb conversion.
	 *
	 * @param taTmbConv The new state of the ta to tmb conversion.
	 */
	public void setTaTmbConv(boolean taTmbConv) {
		this.taTmbConv = taTmbConv;
		fireDataChanged(new ModelChangedEvent(TA_TMB_CONV_EVENT));
	}

	/**
	 * Return the temperature type.
	 *
	 * @return the temperature type.
	 */
	public TYPE_TEMPERATURE getTypeTemperature() {
		return typeTemperature;
	}

	/**
	 * Set the temperature type.
	 *
	 * @param typeTemp the temperature type to set.
	 */
	public void setTypeTemperature(TYPE_TEMPERATURE typeTemp) {
		this.typeTemperature = typeTemp;
		fireDataChanged(new ModelChangedEvent(TYPE_TEMPERATURE_EVENT));
	}

	/**
	 * @see eu.omp.irap.cassis.common.events.DataModel#saveConfig(eu.omp.irap.cassis.common.BufferedWriterProperty)
	 */
	@Override
	public void saveConfig(BufferedWriterProperty out) throws IOException {
		out.writeProperty(TA_TMB_CONV_SAVE, String.valueOf(taTmbConv));
		out.writeProperty(TYPE_TEMPERATURE_SAVE, typeTemperature.name());
	}

	/**
	 * @see eu.omp.irap.cassis.common.events.DataModel#loadConfig(java.util.Properties)
	 */
	@Override
	public void loadConfig(Properties prop) throws IOException {
		boolean tatmb = Boolean.parseBoolean(prop.getProperty(TA_TMB_CONV_SAVE,
				String.valueOf(TA_TMB_CONV_DEFAULT)));
		setTaTmbConv(tatmb);
		String tt = prop.getProperty(TYPE_TEMPERATURE_SAVE, TYPE_TEMPERATURE_DEFAULT.name());
		setTypeTemperature(TYPE_TEMPERATURE.valueOf(tt));
	}

}
