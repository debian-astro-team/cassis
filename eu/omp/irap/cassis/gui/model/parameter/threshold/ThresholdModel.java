/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.threshold;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import eu.omp.irap.cassis.common.BufferedWriterProperty;
import eu.omp.irap.cassis.common.GenericParameterDescription;
import eu.omp.irap.cassis.common.ParameterDescription;
import eu.omp.irap.cassis.common.events.DataModel;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.gui.util.PropertiesLinkedHashMap;

public class ThresholdModel extends DataModel {

	public static final String THRES_AIJ_MAX_EVENT = "thresAijMax";
	public static final String THRES_AIJ_MIN_EVENT = "thresAijMin";
	public static final String THRES_EUP_MAX_EVENT = "thresEupMax";
	public static final String THRES_EUP_MIN_EVENT = "thresEupMin";

	public static final double PACS_EUP_MAX_K = 15000;
	public static final double SPIRE_EUP_MAX_K = 15000;
	public static final double DEFAULT_EUP_MAX_K = 150;

	private double eupMax = DEFAULT_EUP_MAX_K;
	private double eupMin = 0.;
	private double aijMin = 0.;
	private double aijMax = Double.MAX_VALUE;
	private QuantumNumberThresholdModel qntm;


	public ThresholdModel() {
		this.qntm = new QuantumNumberThresholdModel();
	}

	/**
	 * @param thresEupMax
	 *            the thresEupMax to set
	 */
	public void setThresEupMax(double thresEupMax) {
		this.eupMax = thresEupMax;
		fireDataChanged(new ModelChangedEvent(THRES_EUP_MAX_EVENT, thresEupMax));
	}

	/**
	 * @return the thresEupMax
	 */
	public double getThresEupMax() {
		return eupMax;
	}

	/**
	 * @param thresEupMin
	 *            the thresEupMin to set
	 */
	public void setThresEupMin(double thresEupMin) {
		this.eupMin = thresEupMin;
		fireDataChanged(new ModelChangedEvent(THRES_EUP_MIN_EVENT, thresEupMin));
	}

	/**
	 * @return the thresEupMin
	 */
	public double getThresEupMin() {
		return eupMin;
	}

	/**
	 * @return the thresAij
	 */
	public double getThresAijMax() {
		return aijMax;
	}

	/**
	 * @param thresAij
	 *            the thresAij to set
	 */
	public void setThresAijMin(double thresAij) {
		this.aijMin = thresAij;
		fireDataChanged(new ModelChangedEvent(THRES_AIJ_MIN_EVENT, thresAij));
	}

	/**
	 * @param thresAij
	 *            the thresAij to set
	 */
	public void setThresAijMax(double thresAij) {
		this.aijMax = thresAij;
		fireDataChanged(new ModelChangedEvent(THRES_AIJ_MAX_EVENT, thresAij));
	}

	/**
	 * @return the thresAij
	 */
	public double getThresAijMin() {
		return aijMin;
	}

	@Override
	public void saveConfig(BufferedWriterProperty out) throws IOException {
		qntm.saveConfig(out);
		Map<String, String> properties = getProperties();
		for (String val : properties.keySet()) {
			out.writeProperty(val, properties.get(val));
		}
		out.newLine();
		out.flush();
	}

	@Override
	public void loadConfig(Properties prop) {
		qntm.loadConfig(prop);
		setThresEupMin(Double.valueOf(prop.getProperty("thresEupMin", prop.getProperty("eupMin"))));
		setThresEupMax(Double.valueOf(prop.getProperty("thresEupMax", prop.getProperty("eupMax"))));
		setThresAijMin(Double.valueOf(prop.getProperty("thresAij", prop.getProperty("aij",
				 prop.getProperty("thresAijMin")))));
		setThresAijMax(Double.valueOf(
				 prop.getProperty("thresAijMax", String.valueOf(Double.MAX_VALUE))));
	}

	public Map<String, ParameterDescription> getMapParameter() {
		Map<String, ParameterDescription> result = new HashMap<>();
		result.put("thresEupMax", new ParameterDescription(eupMax));
		result.put("thresEupMin", new ParameterDescription(eupMin));
		result.put("thresAijMin", new ParameterDescription(aijMin));
		result.put("thresAijMax", new ParameterDescription(aijMax));
		result.put("quantumNumbers", new GenericParameterDescription(qntm.getQuantumNumber()));

		return result;
	}

	public Map<String, String> getProperties() {
		Map<String, String> map = new PropertiesLinkedHashMap();
		map.put("thresEupMin", String.valueOf(eupMin));
		map.put("thresEupMax", String.valueOf(eupMax));
		map.put("thresAijMin", String.valueOf(aijMin));
		map.put("thresAijMax", String.valueOf(aijMax));

		return map;
	}

	public QuantumNumberThresholdModel getQntm() {
		return qntm;
	}

	public void setQntm(QuantumNumberThresholdModel qntm) {
		this.qntm = qntm;
	}
}
