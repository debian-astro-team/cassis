/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.threshold;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.gui.util.JDoubleCassisTextField;

public class ThresholdPanel extends JPanel implements ModelListener {

	private static final long serialVersionUID = 1L;
	private ThresholdModel model;
	private JDoubleCassisTextField eupMinField;
	private JDoubleCassisTextField eupMaxField;
	private JDoubleCassisTextField aijMinTextField;
	private JDoubleCassisTextField aijMaxField;

	private JComboBox<UNIT> eupUnitComboBox;
	private JComboBox<UNIT> aijUnitComboBox;

	private JPanel northPanel;
	private QuantumNumberThresholdPanel qntp;


	public ThresholdPanel() {
		this(new ThresholdModel());
	}

	public ThresholdPanel(ThresholdModel model) {
		this.model = model;
		model.addModelListener(this);

		setBorder(new TitledBorder("Threshold"));
		setLayout(new GridLayout(2, 1));
		add(getNorthPanel());
		add(getQuantumNumberThresholdPanel());
	}

	public QuantumNumberThresholdPanel getQuantumNumberThresholdPanel() {
		if (qntp == null) {
			qntp = new QuantumNumberThresholdPanel(model.getQntm());
			qntp.setBorder(new EmptyBorder(1, 1, 1, 1));
		}
		return qntp;
	}

	private Component getNorthPanel() {
		if (northPanel == null) {
			northPanel = new JPanel();
			northPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

			northPanel.add(new JLabel("Eup min:"));
			northPanel.add(getEupMinField());
			northPanel.add(new JLabel("max:"));
			northPanel.add(getEupMaxField());
			northPanel.add(getEupUnit());
			northPanel.add(new JLabel("Aij min:"));
			northPanel.add(getAijMinField());
			northPanel.add(new JLabel("max:"));
			northPanel.add(getAijMaxField());
			northPanel.add(getAijUnit());
		}
		return northPanel;
	}

	private JComboBox<UNIT> getAijUnit() {
		if (aijUnitComboBox == null) {
			aijUnitComboBox = new JComboBox<>(new UNIT[] { UNIT.CM_MINUS_1 });
			aijUnitComboBox.setVisible(false);
		}
		return aijUnitComboBox;
	}

	private JComboBox<UNIT> getEupUnit() {
		if (eupUnitComboBox == null) {
			eupUnitComboBox = new JComboBox<>(new UNIT[] { UNIT.KELVIN });
		}
		return eupUnitComboBox;
	}

	private JDoubleCassisTextField getAijMaxField() {
		if (aijMaxField == null) {
			aijMaxField = new JDoubleCassisTextField(6, model.getThresAijMax());
			aijMaxField.setEditable(true);
			aijMaxField.addPropertyChangeListener("value", new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					model.setThresAijMax(Double.valueOf(evt.getNewValue().toString()));
				}
			});
		}
		return aijMaxField;
	}

	public JDoubleCassisTextField getAijMinField() {
		if (aijMinTextField == null) {
			aijMinTextField = new JDoubleCassisTextField(6, model.getThresAijMin());
			HashMap<String, Double> symbols = new HashMap<>();
			symbols.put("*", 0.0);
			aijMinTextField.setSymbols(symbols);
			aijMinTextField.addPropertyChangeListener("value", new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					model.setThresAijMin(Double.valueOf(evt.getNewValue().toString()));
				}
			});
		}
		return aijMinTextField;
	}

	public JDoubleCassisTextField getEupMaxField() {
		if (eupMaxField == null) {
			eupMaxField = new JDoubleCassisTextField(4, model.getThresEupMax());
			eupMaxField.addPropertyChangeListener("value", new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					model.setThresEupMax(Double.valueOf(evt.getNewValue().toString()));
				}
			});
		}
		return eupMaxField;
	}

	private JDoubleCassisTextField getEupMinField() {
		if (eupMinField == null) {
			eupMinField = new JDoubleCassisTextField(4, model.getThresEupMin());
			eupMinField.addPropertyChangeListener("value", new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					model.setThresEupMin(Double.valueOf(evt.getNewValue().toString()));
				}
			});
		}
		return eupMinField;
	}

	/**
	 * @return the model
	 */
	public final ThresholdModel getModel() {
		return model;
	}

	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (ThresholdModel.THRES_EUP_MIN_EVENT.equals(event.getSource())) {
			getEupMinField().setValue(model.getThresEupMin());
		}
		else if (ThresholdModel.THRES_EUP_MAX_EVENT.equals(event.getSource())) {
			getEupMaxField().setValue(model.getThresEupMax());
		}
		else if (ThresholdModel.THRES_AIJ_MIN_EVENT.equals(event.getSource())) {
			getAijMinField().setValue(model.getThresAijMin());
		}
		else if (ThresholdModel.THRES_AIJ_MAX_EVENT.equals(event.getSource())) {
			getAijMaxField().setValue(model.getThresAijMax());
		}
	}

	/**
	 * Set a new model.
	 *
	 * @param newModel The new ThresholdModel to use.
	 */
	public void setModel(ThresholdModel newModel) {
		model.removeModelListener(this);
		model = newModel;
		model.addModelListener(this);
		qntp.setModel(model.getQntm());
		refresh();
	}

	/**
	 * Refresh the view.
	 */
	private void refresh() {
		getEupMinField().setValue(model.getThresEupMin());
		getEupMaxField().setValue(model.getThresEupMax());
		getAijMinField().setValue(model.getThresAijMin());
		getAijMaxField().setValue(model.getThresAijMax());
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(new ThresholdPanel());
		frame.pack();
		frame.setVisible(true);
	}
}
