/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.threshold;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;

public class QuantumNumberThresholdPanel extends JPanel implements ModelListener {

	private static final long serialVersionUID = -196304910767221535L;
	private QuantumNumberThresholdModel model;
	private JTextField jupMinField;
	private JTextField jupMaxField;
	private JTextField kupMinField;
	private JTextField kupMaxField;
	private JTextField lupMinField;
	private JTextField lupMaxField;
	private JTextField mupMinField;
	private JTextField mupMaxField;


	public QuantumNumberThresholdPanel() {
		this(new QuantumNumberThresholdModel());
	}

	public QuantumNumberThresholdPanel(QuantumNumberThresholdModel model) {
		this.model = model;
		model.addModelListener(this);

		initComponents();
	}

	private void initComponents() {
		setLayout(new FlowLayout(FlowLayout.LEFT));
		add(new JLabel("Jup min:"));
		add(getJupMinField());

		add(new JLabel("max:"));
		add(getJupMaxField());

		add(new JLabel("Kup min:"));
		add(getKupMinField());

		add(new JLabel("max:"));
		add(getKupMaxField());

		add(new JLabel("Lup min:"));
		add(getLupMinField());

		add(new JLabel("max:"));
		add(getLupMaxField());

		add(new JLabel("Mup min:"));
		add(getMupMinField());

		add(new JLabel("max:"));
		add(getMupMaxField());
	}

	/**
	 * Create if necessary then return the Mup Max {@link JTextField}.
	 *
	 * @return the Mup Max {@link JTextField}.
	 */
	private JTextField getMupMaxField() {
		if (mupMaxField == null) {
			mupMaxField = new JTextField(model.getMupMax(), 2);
			mupMaxField.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					model.setMupMax(mupMaxField.getText());
				}
			});
			mupMaxField.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					model.setMupMax(mupMaxField.getText());
				}
			});
		}
		return mupMaxField;
	}

	/**
	 * Create if necessary then return the Mup Mix {@link JTextField}.
	 *
	 * @return the Mup Mix {@link JTextField}.
	 */
	private JTextField getMupMinField() {
		if (mupMinField == null) {
			mupMinField = new JTextField(model.getMupMin(), 2);
			mupMinField.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					model.setMupMin(mupMinField.getText());
				}
			});
			mupMinField.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					model.setMupMin(mupMinField.getText());
				}
			});
		}
		return mupMinField;
	}

	/**
	 * Create if necessary then return the Lup Max {@link JTextField}.
	 *
	 * @return the Lup Max {@link JTextField}.
	 */
	private JTextField getLupMaxField() {
		if (lupMaxField == null) {
			lupMaxField = new JTextField(model.getLupMax(), 2);
			lupMaxField.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					model.setLupMax(lupMaxField.getText());
				}
			});
			lupMaxField.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					model.setLupMax(lupMaxField.getText());
				}
			});
		}
		return lupMaxField;
	}

	/**
	 * Create if necessary then return the Lup Min {@link JTextField}.
	 *
	 * @return the Lup Min {@link JTextField}.
	 */
	private JTextField getLupMinField() {
		if (lupMinField == null) {
			lupMinField = new JTextField(model.getLupMin(), 2);
			lupMinField.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					model.setLupMin(lupMinField.getText());
				}
			});
			lupMinField.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					model.setLupMin(lupMinField.getText());
				}
			});
		}
		return lupMinField;
	}

	/**
	 * Create if necessary then return the Kup Max {@link JTextField}.
	 *
	 * @return the Kup Max {@link JTextField}.
	 */
	private JTextField getKupMaxField() {
		if (kupMaxField == null) {
			kupMaxField = new JTextField(model.getKupMax(), 2);
			kupMaxField.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					model.setKupMax(kupMaxField.getText());
				}
			});
			kupMaxField.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					model.setKupMax(kupMaxField.getText());
				}
			});
		}
		return kupMaxField;
	}

	/**
	 * Create if necessary then return the Kup Min {@link JTextField}.
	 *
	 * @return the Kup Min {@link JTextField}.
	 */
	private JTextField getKupMinField() {
		if (kupMinField == null) {
			kupMinField = new JTextField(model.getKupMin(), 2);
			kupMinField.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					model.setKupMin(kupMinField.getText());
				}
			});
			kupMinField.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					model.setKupMin(kupMinField.getText());
				}
			});
		}
		return kupMinField;
	}

	/**
	 * Create if necessary then return the Jup Max {@link JTextField}.
	 *
	 * @return the Jup Max {@link JTextField}.
	 */
	private JTextField getJupMaxField() {
		if (jupMaxField == null) {
			jupMaxField = new JTextField(model.getJupMax(), 2);
			jupMaxField.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					model.setJupMax(jupMaxField.getText());
				}
			});
			jupMaxField.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					model.setJupMax(jupMaxField.getText());
				}
			});
		}
		return jupMaxField;
	}

	/**
	 * Create if necessary then return the Jup Min {@link JTextField}.
	 *
	 * @return the Jup Min {@link JTextField}.
	 */
	private JTextField getJupMinField() {
		if (jupMinField == null) {
			jupMinField = new JTextField(model.getJupMin(), 2);
			jupMinField.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					model.setJupMin(jupMinField.getText());
				}
			});
			jupMinField.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					model.setJupMin(jupMinField.getText());
				}
			});
		}
		return jupMinField;
	}

	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (QuantumNumberThresholdModel.JUP_MIN_EVENT.equals(event.getSource())) {
			jupMinField.setText(model.getJupMin());
		}
		else if (QuantumNumberThresholdModel.JUP_MAX_EVENT.equals(event.getSource())) {
			jupMaxField.setText(model.getJupMax());
		}
		else if (QuantumNumberThresholdModel.KUP_MIN_EVENT.equals(event.getSource())) {
			kupMinField.setText(model.getKupMin());
		}
		else if (QuantumNumberThresholdModel.KUP_MAX_EVENT.equals(event.getSource())) {
			kupMaxField.setText(model.getKupMax());
		}
		else if (QuantumNumberThresholdModel.LUP_MIN_EVENT.equals(event.getSource())) {
			lupMinField.setText(model.getLupMin());
		}
		else if (QuantumNumberThresholdModel.LUP_MAX_EVENT.equals(event.getSource())) {
			lupMaxField.setText(model.getLupMax());
		}
		else if (QuantumNumberThresholdModel.MUP_MIN_EVENT.equals(event.getSource())) {
			mupMinField.setText(model.getMupMin());
		}
		else if (QuantumNumberThresholdModel.MUP_MAX_EVENT.equals(event.getSource())) {
			mupMaxField.setText(model.getMupMax());
		}
	}

	/**
	 * Set a new model.
	 *
	 * @param newModel The new QuantumNumberThresholdModel to use.
	 */
	public void setModel(QuantumNumberThresholdModel newModel) {
		model.removeModelListener(this);
		model = newModel;
		model.addModelListener(this);
		refresh();
	}

	/**
	 * Refresh the view.
	 */
	private void refresh() {
		jupMinField.setText(model.getJupMin());
		jupMaxField.setText(model.getJupMax());
		kupMinField.setText(model.getKupMin());
		kupMaxField.setText(model.getKupMax());
		lupMinField.setText(model.getLupMin());
		lupMaxField.setText(model.getLupMax());
		mupMinField.setText(model.getMupMin());
		mupMaxField.setText(model.getMupMax());
	}
}
