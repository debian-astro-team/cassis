/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.threshold;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Properties;

import eu.omp.irap.cassis.common.BufferedWriterProperty;
import eu.omp.irap.cassis.common.events.DataModel;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.gui.util.PropertiesLinkedHashMap;

public class QuantumNumberThresholdModel extends DataModel {

	public static final String JUP_MAX_EVENT = "jupMax";
	public static final String JUP_MIN_EVENT = "jupMin";
	public static final String KUP_MAX_EVENT = "kupMax";
	public static final String KUP_MIN_EVENT = "kupMin";
	public static final String LUP_MAX_EVENT = "lupMax";
	public static final String LUP_MIN_EVENT = "lupMin";
	public static final String MUP_MAX_EVENT = "mupMax";
	public static final String MUP_MIN_EVENT = "mupMin";

	private String jupMin;
	private String jupMax;
	private String jLowMin;
	private String jLowMax;

	private String kupMin;
	private String kupMax;
	private String kLowMin;
	private String kLowMax;

	private String lupMin;
	private String lupMax;
	private String lLowMin;
	private String lLowMax;

	private String mupMin;
	private String mupMax;
	private String mLowMin;
	private String mLowMax;


	public QuantumNumberThresholdModel() {
		this("*", "*", "*", "*", "*", "*", "*", "*",
			"*", "*", "*", "*", "*", "*", "*", "*");
	}

	public QuantumNumberThresholdModel(
			String jupMin, String jupMax, String jLowMin, String jLowMax,
			String kupMin, String kupMax, String kLowMin, String kLowMax,
			String lupMin, String lupMax, String lLowMin, String lLowMax,
			String mupMin, String mupMax, String mLowMin, String mLowMax) {
		this.jupMin = jupMin;
		this.jupMax = jupMax;
		this.jLowMin = jLowMin;
		this.jLowMax = jLowMax;
		this.kupMin = kupMin;
		this.kupMax = kupMax;
		this.kLowMin = kLowMin;
		this.kLowMax = kLowMax;
		this.lupMin = lupMin;
		this.lupMax = lupMax;
		this.lLowMin = lLowMin;
		this.lLowMax = lLowMax;
		this.mupMin = mupMin;
		this.mupMax = mupMax;
		this.mLowMin = mLowMin;
		this.mLowMax = mLowMax;
	}


	public String getJupMin() {
		return jupMin;
	}

	public void setJupMin(String jupMin) {
		this.jupMin = jupMin;
		fireDataChanged(new ModelChangedEvent(JUP_MIN_EVENT, jupMin));
	}

	public String getJupMax() {
		return jupMax;
	}

	public void setJupMax(String jupMax) {
		this.jupMax = jupMax;
		fireDataChanged(new ModelChangedEvent(JUP_MAX_EVENT, jupMax));
	}

	public String getKupMin() {
		return kupMin;
	}

	public void setKupMin(String kupMin) {
		this.kupMin = kupMin;
		fireDataChanged(new ModelChangedEvent(KUP_MIN_EVENT, kupMin));
	}

	public String getKupMax() {
		return kupMax;
	}

	public void setKupMax(String kupMax) {
		this.kupMax = kupMax;
		fireDataChanged(new ModelChangedEvent(KUP_MAX_EVENT, kupMax));
	}

	public String getLupMin() {
		return lupMin;
	}

	public void setLupMin(String lupMin) {
		this.lupMin = lupMin;
		fireDataChanged(new ModelChangedEvent(LUP_MIN_EVENT, lupMin));
	}

	public String getLupMax() {
		return lupMax;
	}

	public void setLupMax(String lupMax) {
		this.lupMax = lupMax;
		fireDataChanged(new ModelChangedEvent(LUP_MAX_EVENT, lupMax));
	}

	public String getMupMin() {
		return mupMin;
	}

	public void setMupMin(String mupMin) {
		this.mupMin = mupMin;
		fireDataChanged(new ModelChangedEvent(MUP_MIN_EVENT, mupMin));
	}

	public String getMupMax() {
		return mupMax;
	}

	public void setMupMax(String mupMax) {
		this.mupMax = mupMax;
		fireDataChanged(new ModelChangedEvent(MUP_MAX_EVENT, mupMax));
	}

	public String getjLowMin() {
		return jLowMin;
	}

	public void setjLowMin(String jLowMin) {
		this.jLowMin = jLowMin;
	}

	public String getjLowMax() {
		return jLowMax;
	}

	public void setjLowMax(String jLowMax) {
		this.jLowMax = jLowMax;
	}

	public String getkLowMin() {
		return kLowMin;
	}

	public void setkLowMin(String kLowMin) {
		this.kLowMin = kLowMin;
	}

	public String getkLowMax() {
		return kLowMax;
	}

	public void setkLowMax(String kLowMax) {
		this.kLowMax = kLowMax;
	}

	public String getlLowMin() {
		return lLowMin;
	}

	public void setlLowMin(String lLowMin) {
		this.lLowMin = lLowMin;
	}

	public String getlLowMax() {
		return lLowMax;
	}

	public void setlLowMax(String lLowMax) {
		this.lLowMax = lLowMax;
	}

	public String getmLowMin() {
		return mLowMin;
	}

	public void setmLowMin(String mLowMin) {
		this.mLowMin = mLowMin;
	}

	public String getmLowMax() {
		return mLowMax;
	}

	public void setmLowMax(String mLowMax) {
		this.mLowMax = mLowMax;
	}

	@Override
	public void saveConfig(BufferedWriterProperty out) throws IOException {
		out.writeProperty("jupMin", jupMin);
		out.writeProperty("jupMax", jupMax);
		out.writeProperty("jLowMin", jLowMin);
		out.writeProperty("jLowMax", jLowMax);
		out.writeProperty("kupMin", kupMin);
		out.writeProperty("kupMax", kupMax);
		out.writeProperty("kLowMin", kLowMin);
		out.writeProperty("kLowMax", kLowMax);
		out.writeProperty("lupMin", lupMin);
		out.writeProperty("lupMax", lupMax);
		out.writeProperty("lLowMin", lLowMin);
		out.writeProperty("lLowMax", lLowMax);
		out.writeProperty("mupMin", mupMin);
		out.writeProperty("mupMax", mupMax);
		out.writeProperty("mLowMin", mLowMin);
		out.writeProperty("mLowMax", mLowMax);
	}

	@Override
	public void loadConfig(Properties prop) {
		setJupMin(prop.getProperty("jupMin", "*"));
		setJupMax(prop.getProperty("jupMax", "*"));
		setjLowMin(prop.getProperty("jLowMin", "*"));
		setjLowMax(prop.getProperty("jLowMax", "*"));
		setKupMin(prop.getProperty("kupMin", "*"));
		setKupMax(prop.getProperty("kupMax", "*"));
		setkLowMin(prop.getProperty("kLowMin", "*"));
		setkLowMax(prop.getProperty("kLowMax", "*"));
		setLupMin(prop.getProperty("lupMin", "*"));
		setLupMax(prop.getProperty("lupMax", "*"));
		setlLowMin(prop.getProperty("lLowMin", "*"));
		setlLowMax(prop.getProperty("lLowMax", "*"));
		setMupMin(prop.getProperty("mupMin", "*"));
		setMupMax(prop.getProperty("mupMax", "*"));
		setmLowMin(prop.getProperty("mLowMin", "*"));
		setmLowMax(prop.getProperty("mLowMax", "*"));
	}

	public Map<String, String> getProperties() {
		Map<String, String> map = new PropertiesLinkedHashMap();
		map.put("jupMin", jupMin);
		map.put("jupMax", jupMax);
		map.put("jLowMin", jLowMin);
		map.put("jLowMax", jLowMax);
		map.put("kupMin", kupMin);
		map.put("kupMax", kupMax);
		map.put("kLowMin", kLowMin);
		map.put("kLowMax", kLowMax);
		map.put("lupMin", lupMin);
		map.put("lupMax", lupMax);
		map.put("lLowMin", lLowMin);
		map.put("lLowMax", lLowMax);
		map.put("mupMin", mupMin);
		map.put("mupMax", mupMax);
		map.put("mLowMin", mLowMin);
		map.put("mLowMax", mLowMax);
		return map;
	}

	public ArrayList<String> getQuantumNumber() {
		ArrayList<String> arrayList = new ArrayList<>();
		String[] props = { "jupMin", "jupMax", "jLowMin", "jLowMax",
			"kupMin", "kupMax", "kLowMin", "kLowMax",
			"lupMin", "lupMax", "lLowMin", "lLowMax",
			"mupMin", "mupMax", "mLowMin", "mLowMax"};
		Map<String, String> map = getProperties();
		for (String string : props) {
			arrayList.add(map.get(string));
		}
		return arrayList;
	}
}
