/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.rotationaldata;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.BufferedWriterProperty;
import eu.omp.irap.cassis.common.events.DataModel;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.database.access.AccessDataBase;
import eu.omp.irap.cassis.database.access.DataBaseConnection;
import eu.omp.irap.cassis.file.rot.ReadFileData;
import eu.omp.irap.cassis.gui.util.FileUtils;
import eu.omp.irap.cassis.parameters.PointInformation;
import eu.omp.irap.cassis.parameters.PointInformation.VERSION;
import eu.omp.irap.cassis.parameters.RotationalComponent;
import eu.omp.irap.cassis.parameters.RotationalFile;
import eu.omp.irap.cassis.parameters.RotationalMolecule;
import eu.omp.irap.cassis.parameters.RotationalSelectionData;
import eu.omp.irap.cassis.parameters.TelescopeDescriptionRD;
import eu.omp.irap.cassis.properties.Software;

/**
 * Model for the RotationalData.
 *
 * @author M. Boiziot
 */
public class RotationalDataModel extends DataModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(RotationalDataModel.class);

	public static final String ROTATIONAL_NAME_DATA_EVENT = "rotationalNameData";
	public static final String ROTATIONAL_NEW_DATA_EVENT = "rotationalNewData";
	public static final String ROTATIONAL_CALIBRATION_EVENT = "rotationalCalibration";
	public static final String ROTATIONAL_RMS_EVENT = "rotationalRms";
	public static final String BAD_FWHM_EVENT = "badFwhm";
	public static final String BAD_FLUX_EVENT = "badFlux";
	public static final String BAD_TELESCOPE_EVENT = "badTelescope";
	public static final String BAD_FREQUENCY_TELESCOPE_EVENT = "badFrequencyTelescope";
	public static final String NOT_ENOUGH_POINT_EVENT = "notEnoughPoint";
	public static final String NOTHING_TO_DISPLAY_EVENT = "nothingToDisplay";
	public static final String MOL_NOT_FOUND_IN_DATABASE_EVENT = "molNotFoundInDatabase";
	private static final String ROTATIONAL_NAME_DATA_SAVE = "rotationalNameData";

	private static final int DATA_OK = 0;
	private static final int DATA_NOT_IN_DATABASE = 1;
	private static final int DATA_ERROR = 2;

	private String nameData;
	private RotationalFile data;
	private boolean dataCorrected;


	/**
	 * Constructor.
	 */
	public RotationalDataModel() {
		this.nameData = "";
	}

	/**
	 * Create a Java File from a path or download a file if the provided path is an url the load the file.
	 *
	 * @param path The path or url of the file to load
	 */
	public void loadData(String path) {
		File file = new File(path);

		if (file.exists()) {
			loadData(file);
			return;
		} else {
			file = FileUtils.downloadAndRenameIfNeeded(path, true);
			if (file != null) {
				loadData(file);
				return;
			}
		}
	}

	/**
	 * Load a file.
	 *
	 * @param file The file to load.
	 */
	public void loadData(File file) {
		setNameData(file.getPath());
		try {
			setData(ReadFileData.getReader(file.getPath()).readFile(file.getPath()), false);
		} catch (IOException ioe) {
			LOGGER.error("Error while loading the file {}", file.getAbsolutePath(), ioe);
		}
	}

	/**
	 * Set the data.
	 *
	 * @param rotationalFile The rotational file to set.
	 */
	public void setData(RotationalFile rotationalFile, boolean isFakeData) {
		this.data = rotationalFile;
		if (isFakeData) {
			fireDataChanged(new ModelChangedEvent(ROTATIONAL_NEW_DATA_EVENT));
		} else {
			int res = checkAndModifyData(getAllPointsInformation());
			if (res == DATA_OK || res == DATA_NOT_IN_DATABASE) {
				fireDataChanged(new ModelChangedEvent(ROTATIONAL_NEW_DATA_EVENT));
			} else {
				this.data = null;
			}
		}
	}

	/**
	 * Return the data (as a {@link RotationalFile}).
	 *
	 * @return the data.
	 */
	public RotationalFile getData() {
		return data;
	}

	/**
	 * Return the name of the data.
	 *
	 * @return The name of the data.
	 */
	public String getNameData() {
		return nameData;
	}

	/**
	 * Change the name of the data.
	 *
	 * @param nameData The name of the data to set.
	 */
	public final void setNameData(String nameData) {
		this.nameData = nameData;
		fireDataChanged(new ModelChangedEvent(ROTATIONAL_NAME_DATA_EVENT, nameData));
	}

	/**
	 * @see eu.omp.irap.cassis.common.events.DataModel#saveConfig(eu.omp.irap.cassis.common.BufferedWriterProperty)
	 */
	@Override
	public void saveConfig(BufferedWriterProperty out) throws IOException {
		out.writeProperty(ROTATIONAL_NAME_DATA_SAVE, getNameData());
	}

	/**
	 * @see eu.omp.irap.cassis.common.events.DataModel#loadConfig(java.util.Properties)
	 */
	@Override
	public void loadConfig(Properties prop) throws IOException {
		String loadedNameData = prop.getProperty(ROTATIONAL_NAME_DATA_SAVE);
		if (loadedNameData != null) {
			File file = new File(loadedNameData);
			if (file.exists()) {
				loadData(file);
			}
		}
	}

	/**
	 * Correct calibration and delta if needed.
	 *
	 * @param calibration The new calibration.
	 */
	public void correctCalibration(double calibration) {
		for (PointInformation point : getAllPointsInformation()) {
			if (point.getCalibration() == 0) {
				point.setCalibration(calibration);
			}
		}
		computeDeltaValues();
	}

	/**
	 * Recompute the delta values.
	 */
	private void computeDeltaValues() {
		double cal;
		// Delta on 1st moment
		for (PointInformation point : getAllPointsInformation()) {
			if (point.getDeltaWFirstMoment() == 0 &&
					point.getAllowedType().contains(RotationalSelectionData.FIRST_TYPE_DATA)) {
				cal = point.getCalibration();
				double deltaFlux = Math.sqrt(Math.pow(point.getRms() *
						Math.sqrt(2 * point.getDeltaV() * point.getFwhmFirstMoment()) , 2) +
						Math.pow(cal / 100.0 * point.getWFirstMoment(), 2));
				point.setDeltaWFirstMoment(deltaFlux);
				point.setDeltaWCalc(deltaFlux);
			}
		}
		// Delta on other type.
		for (PointInformation point : getAllPointsInformation()) {
			if (point.getDeltaWFit() == 0) {
				cal = point.getCalibration();
				double fwhm = RotationalDataModel.getFwhm(point);
				if (!Double.isNaN(fwhm)) {
					double deltaFlux = Math.sqrt(Math.pow(point.getRms() *
							Math.sqrt(2 * point.getDeltaV() * fwhm) , 2) +
							Math.pow(cal / 100.0 * point.getWFit(), 2));
					point.setDeltaWFit(deltaFlux);
					point.setDeltaWCalc(deltaFlux);
				}
			}
		}
	}

	/**
	 * Return the {@link RotationalSelectionData} on the provided List who is
	 * not {@link RotationalSelectionData#FIRST_TYPE_DATA}, or null if not found.
	 *
	 * @param list The List of {@link RotationalSelectionData}.
	 * @return the {@link RotationalSelectionData} on the provided List who is
	 * not {@link RotationalSelectionData#FIRST_TYPE_DATA}, or null if not found.
	 */
	private static RotationalSelectionData getDataType(List<RotationalSelectionData> list) {
		for (RotationalSelectionData data : list) {
			if (data != RotationalSelectionData.FIRST_TYPE_DATA) {
				return data;
			}
		}
		return null;
	}

	/**
	 * Return the FWHM of the provided {@link PointInformation} for his
	 * {@link RotationalSelectionData} who is not
	 * {@link RotationalSelectionData#FIRST_TYPE_DATA}.
	 *
	 * @param point The point for which we want the FWHM.
	 * @return the FWHM of the provided {@link PointInformation} for his
	 * {@link RotationalSelectionData} who is not
	 * {@link RotationalSelectionData#FIRST_TYPE_DATA} or {@link Double#NaN}
	 * if not found.
	 */
	private static double getFwhm(PointInformation point) {
		RotationalSelectionData data = getDataType(point.getAllowedType());
		double fwhm;
		if (data != null) {
			switch (data) {
			case GAUSSIAN_TYPE_DATA:
				fwhm = point.getFwhmGaussianFit();
				break;
			case LORENTZIAN_TYPE_DATA:
				fwhm = point.getFwhmLorentzianFit();
				break;
			case VOIGHT_TYPE_DATA:
				fwhm = Math.sqrt(point.getFwhmGaussianFit() * point.getFwhmGaussianFit() +
						point.getFwhmLorentzianFit() * point.getFwhmLorentzianFit());
				break;
			default:
				fwhm = Double.NaN;
				break;
			}
			return fwhm;
		}
		return Double.NaN;
	}

	/**
	 * Return if the calibration is correct.
	 *
	 * @return if the calibration is correct.
	 */
	private boolean isCalibrationOk() {
		for (PointInformation point : getAllPointsInformation()) {
			if (point.getCalibration() == 0) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Return if the RMS is correct.
	 *
	 * @return if the RMS is correct.
	 */
	private boolean isRmsOk() {
		for (PointInformation point : getAllPointsInformation()) {
			if (point.getRms() == 0 && point.getVersion() != VERSION.OLD) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Return the {@link PointInformation} for all molecules and components.
	 *
	 * @return the PointInformations for all molecules and components.
	 */
	private List<PointInformation> getAllPointsInformation() {
		List<PointInformation> l = new ArrayList<>();
		for (RotationalMolecule rm : data.getMoleculeList()) {
			for (RotationalComponent rc : rm.getComponentsList()) {
				l.addAll(rc.getListPoints());
			}
		}
		return l;
	}

	/**
	 * Do some checking about the data and ask for some correction if needed.
	 *
	 * @param listPoints The list of {@link PointInformation} to check.
	 * @return DATA_OK, DATA_NOT_IN_DATABASE or DATA_ERROR = 2;
	 */
	private int checkAndModifyData(List<PointInformation> listPoints) {
		if (!areDataInDatabase(data.getMoleculeList())) {
			fireDataChanged(new ModelChangedEvent(MOL_NOT_FOUND_IN_DATABASE_EVENT));
			return DATA_NOT_IN_DATABASE;
		}
		if (!isFwhmOk(listPoints)) {
			fireDataChanged(new ModelChangedEvent(BAD_FWHM_EVENT));
			return DATA_ERROR;
		}

		List<PointInformation> badPoints = checkFlux(listPoints);
		if (!badPoints.isEmpty()) {
			dataCorrected = false;
			fireDataChanged(new ModelChangedEvent(BAD_FLUX_EVENT, badPoints));
			if (!dataCorrected) {
				return DATA_ERROR;
			}
		}

		List<String> badTelescopes = checkTelescope(listPoints);
		if (!badTelescopes.isEmpty()) {
			dataCorrected = false;
			fireDataChanged(new ModelChangedEvent(BAD_TELESCOPE_EVENT, badTelescopes));
			if (!dataCorrected) {
				return DATA_ERROR;
			}
		}

		if (checkPointsInComponents()) {
			if (data.getMoleculeList().isEmpty()) {
				fireDataChanged(new ModelChangedEvent(NOTHING_TO_DISPLAY_EVENT));
				return DATA_ERROR;
			} else {
				fireDataChanged(new ModelChangedEvent(NOT_ENOUGH_POINT_EVENT));
			}
		}

		if (!isCalibrationOk()) {
			fireDataChanged(new ModelChangedEvent(ROTATIONAL_CALIBRATION_EVENT));
		} else {
			computeDeltaValues();
		}

		if (!isRmsOk()) {
			fireDataChanged(new ModelChangedEvent(ROTATIONAL_RMS_EVENT));
		}

		return DATA_OK;
	}

	/**
	 * Check the {@link PointInformation} for each {@link RotationalComponent}
	 *  for each {@link RotationalMolecule} and remove them if there is not enought
	 *  points.
	 *
	 * @return if a {@link RotationalComponent} or {@link RotationalMolecule} was removed.
	 */
	private boolean checkPointsInComponents() {
		boolean corrected = false;
		for (Iterator<RotationalMolecule> itMol = this.data.getMoleculeList().iterator(); itMol.hasNext();) {
			RotationalMolecule rm = itMol.next();
			for (Iterator<RotationalComponent> itComp = rm.getComponentsList().iterator(); itComp.hasNext();) {
				RotationalComponent rc = itComp.next();
				if (rc.getListPoints().size() < 2) {
					itComp.remove();
					corrected = true;
				}
			}
			if (rm.getComponentsList().isEmpty()) {
				itMol.remove();
				corrected = true;
			}
		}
		return corrected;
	}

	/**
	 * Check the telescopes for a list of {@link PointInformation} then return
	 *  the list of telescope (as String) who doesn't exist.
	 *
	 * @param listPoints The List of {@link PointInformation} to check.
	 * @return The list of Telescope (as String) not found.
	 */
	private List<String> checkTelescope(List<PointInformation> listPoints) {
		List<String> badTelescopes = new ArrayList<>();

		for (PointInformation point : listPoints) {
			String telescope = point.getTelescope();
			if ((telescope == null && !badTelescopes.contains(null))
					|| (telescope != null
					&& !new File(telescope).exists()
					&& !new File(Software.getTelescopePath() + File.separatorChar + telescope).exists()
					&& !badTelescopes.contains(telescope))) {
				badTelescopes.add(telescope);
			}
		}
		return badTelescopes;
	}

	/**
	 * Check the flux for a provided list of {@link PointInformation} then return
	 *  the list of them with bad flux.
	 *
	 * @param listPoints The list of {@link PointInformation} to check.
	 * @return The list of {@link PointInformation} with bad flux.
	 */
	private List<PointInformation> checkFlux(List<PointInformation> listPoints) {
		List<PointInformation> badPoints = new ArrayList<>();
		boolean interrupt = false;
		for (int i = 0; i < listPoints.size() && !interrupt; i++) {
			PointInformation pointInformation = listPoints.get(i);
			if (pointInformation.getWFirstMoment() <= 0 &&
				pointInformation.getWFit() <= 0) {
				badPoints.add(pointInformation);
			}
		}
		return badPoints;
	}

	/**
	 * Check the FWHM of a list of {@link PointInformation}.
	 *
	 * @param listPoints The list of {@link PointInformation} to check.
	 * @return if the FWHM is ok.
	 */
	private boolean isFwhmOk(List<PointInformation> listPoints) {
		boolean fwhmOk = true;
		for (int i = 0; i < listPoints.size() && fwhmOk; i++) {
			PointInformation pointInformation = listPoints.get(i);
			if (pointInformation.getFwhmGaussianFit() == 0
					&& pointInformation.getFwhmLorentzianFit() == 0) {
				fwhmOk = false;
			}
		}
		return fwhmOk;
	}

	/**
	 * Remove a List of {@link PointInformation} from the data.
	 *
	 * @param badPoints The List of {@link PointInformation} to remove.
	 */
	public void removePoints(List<PointInformation> badPoints) {
		for (RotationalMolecule rm : this.data.getMoleculeList()) {
			for (RotationalComponent rc : rm.getComponentsList()) {
				for (Iterator<PointInformation> it = rc.getListPoints().iterator(); it.hasNext();) {
					PointInformation point = it.next();
					if (badPoints.contains(point)) {
						it.remove();
					}
				}
			}
		}
		this.dataCorrected = true;
	}

	/**
	 * Do a correction on Telescope (telescope file link not present on
	 * and the correction).
	 *
	 * @param mapCorrection The Map of correction with bad telescope as key
	 *  and telescope correction as value.
	 */
	public void correctTelescope(Map<String, String> mapCorrection) {
		boolean freqError = false;
		for (PointInformation point : getAllPointsInformation()) {
			if (mapCorrection.containsKey(point.getTelescope())) {
				String telescopeOrigin = point.getTelescope();
				String telescopeCorrected = mapCorrection.get(telescopeOrigin);
				point.setTelescope(telescopeCorrected);
				TelescopeDescriptionRD telDesc = new TelescopeDescriptionRD(
						telescopeCorrected);
				if (point.getNu() > telDesc.getMaxFreq() || point.getNu() < telDesc.getMinFreq()) {
					freqError = true;
				}
			}
		}
		if (freqError) {
			fireDataChanged(new ModelChangedEvent(BAD_FREQUENCY_TELESCOPE_EVENT));
		} else {
			this.dataCorrected = true;
		}
	}

	/**
	 * Check then return if all molecules are in the database.
	 *
	 * @param mols The molecules.
	 * @return true if all the molecules are in the database, false otherwise.
	 */
	private boolean areDataInDatabase(List<RotationalMolecule> mols) {
		for (RotationalMolecule mol : mols) {
			String name = AccessDataBase.getDataBaseConnection().getMolName(mol.getTag());
			if (DataBaseConnection.NOT_IN_DATABASE.equals(name)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Mark the data as corrected/OK.
	 */
	public void setDataAsOk() {
		this.dataCorrected = true;
	}
}
