/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.rotationaldata;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.Telescope;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.gui.PanelView;
import eu.omp.irap.cassis.parameters.PointInformation;
import eu.omp.irap.cassis.properties.Software;

/**
 * View for the RotationalDataModel.
 *
 * @author M. Boiziot
 */
public class RotationalDataPanel extends JPanel implements ModelListener {

	private static final long serialVersionUID = 3074476649355799501L;
	private static final Logger LOGGER = LoggerFactory.getLogger(RotationalDataPanel.class);

	private RotationalDataModel model;
	private JTextField dataFileText;
	private JButton loadButton;


	/**
	 * Constructor.
	 *
	 * @param model The model.
	 */
	public RotationalDataPanel(RotationalDataModel model) {
		super(new FlowLayout(FlowLayout.LEFT));
		this.model = model;
		model.addModelListener(this);
		initComponents();
	}

	/**
	 * Initializes the graphics components.
	 */
	private void initComponents() {
		add(getLoadButton());
		add(getDataText());
		setBorder(new TitledBorder("Data"));
		setPreferredSize(new Dimension(490, 60));
		setSize(new Dimension(490, 60));
	}

	/**
	 * Create if needed then return the "Load" {@link JButton}.
	 *
	 * @return The "Load" {@link JButton}.
	 */
	public JButton getLoadButton() {
		if (loadButton == null) {
			loadButton = new JButton("Load");
			loadButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					onLoadButtonClicked();
				}
			});
		}
		return loadButton;
	}

	/**
	 * Create if needed then return the data {@link JTextField}.
	 *
	 * @return The data {@link JTextField}.
	 */
	public JTextField getDataText() {
		if (dataFileText == null) {
			dataFileText = new JTextField(32);
			dataFileText.setText(model.getNameData());
			dataFileText.addKeyListener(new KeyAdapter() {

				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						model.loadData(dataFileText.getText());
					}
				}
			});
		}

		return dataFileText;
	}

	/**
	 * @see eu.omp.irap.cassis.common.events.ModelListener#dataChanged(eu.omp.irap.cassis.common.events.ModelChangedEvent)
	 */
	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (RotationalDataModel.ROTATIONAL_NAME_DATA_EVENT.equals(event.getSource())) {
			setDataName(model.getNameData());

			// null on hipe plugin : do nothing, as the openRecent is disabled on the plugin.
			if (PanelView.getInstance() != null) {
				PanelView.getInstance().getMenuBar().getOpenControl().fireNewCassisFileOpen(model.getNameData());
			}
		} else if (RotationalDataModel.ROTATIONAL_CALIBRATION_EVENT.equals(event.getSource())) {
			handleCalibrationEvent();
		} else if (RotationalDataModel.ROTATIONAL_RMS_EVENT.equals(event.getSource())) {
			JOptionPane.showMessageDialog(this,
					"You do not have computed the rms for some lines, the error"
					+ " bars will be computed with the calibration error only",
					"Warning : rms not computed", JOptionPane.WARNING_MESSAGE);
		} else if (RotationalDataModel.BAD_FLUX_EVENT.equals(event.getSource())) {
			@SuppressWarnings("unchecked")
			List<PointInformation> badPoints = (List<PointInformation>) event.getValue();
			handleBadFluxEvent(badPoints);
		} else if (RotationalDataModel.BAD_FWHM_EVENT.equals(event.getSource())) {
			JOptionPane.showMessageDialog(this, "Impossible to display the spectrum,"
					+ "\nthis fit has not been performed.", "",
					JOptionPane.ERROR_MESSAGE);
		} else if (RotationalDataModel.BAD_TELESCOPE_EVENT.equals(event.getSource())) {
			@SuppressWarnings("unchecked")
			List<String> badTelescopes = (List<String>) event.getValue();
			handleBadTelescopeEvent(badTelescopes);
		} else if (RotationalDataModel.BAD_FREQUENCY_TELESCOPE_EVENT.equals(event.getSource())) {
			handleBadTelescopeFrequencyEvent();
		} else if (RotationalDataModel.NOT_ENOUGH_POINT_EVENT.equals(event.getSource())) {
			JOptionPane.showMessageDialog(this,
					"Due to lack of points some components have been removed.",
					"Not enough points", JOptionPane.WARNING_MESSAGE);
		} else if (RotationalDataModel.NOTHING_TO_DISPLAY_EVENT.equals(event.getSource())) {
			JOptionPane.showMessageDialog(this,
					"Due to lack of points there is nothing to display.",
					"Nothing to display", JOptionPane.ERROR_MESSAGE);
		} else if (RotationalDataModel.MOL_NOT_FOUND_IN_DATABASE_EVENT.equals(event.getSource())) {
			displayMoleculeNotFoundErrorDialog();
		}
	}

	/**
	 * Show a popup saying that selected Telescope can not observe at the frequency
	 *  of the data and ask if the user want to continue anyway.
	 */
	private void handleBadTelescopeFrequencyEvent() {
		String[] choix = { "Yes", "No" };
		String mess = "Some lines are observed by a telescope which can not observe"
				+ " at the associated frequency.\nWould you like to continue?";
		int answer = JOptionPane.showOptionDialog(this, mess,
				"Telescope and Frequency Error", JOptionPane.YES_NO_OPTION,
				JOptionPane.ERROR_MESSAGE, null, choix, choix[0]);
		if (answer == JOptionPane.YES_OPTION) {
			model.setDataAsOk();
		}
	}

	/**
	 * Propose to change the path of each Telescope not found.
	 *
	 * @param badTelescopes The list of telescope not found.
	 */
	private void handleBadTelescopeEvent(List<String> badTelescopes) {
		Map<String, String> mapTelescopes = new HashMap<>(
				badTelescopes.size());
		for (String telescope : badTelescopes) {
			String correction = selectTelescope(telescope);
			if (correction == null) {
				return;
			} else {
				mapTelescopes.put(telescope, correction);
			}
		}
		model.correctTelescope(mapTelescopes);
	}

	/**
	 * Display a popup asking the user if he want to remove the
	 *  {@link PointInformation}s with a bad flux.
	 *
	 * @param badPoints The list of {@link PointInformation} with a bad flux.
	 */
	private void handleBadFluxEvent(List<PointInformation> badPoints) {
		int userChoice = JOptionPane.showConfirmDialog(this,
				"One or more lines flux are <= 0. "
				+ "Do you want to remove these points and continue?",
				"Continue?", JOptionPane.YES_NO_OPTION);
		if (userChoice == JOptionPane.YES_OPTION) {
			model.removePoints(badPoints);
		}
	}

	/**
	 * Display a message asking for a new calibration.
	 */
	private void handleCalibrationEvent() {
		String result = JOptionPane.showInputDialog(this,
				"One or more lines calibration are = 0."
				+ " Please enter a calibration error in %", Double.valueOf(1.0));
		if (result != null && !result.isEmpty()) {
			try {
				double calibration = Double.parseDouble(result);
				model.correctCalibration(calibration);
			} catch (NumberFormatException nfe) {
				LOGGER.warn("The calibration is wrong. This may lead to some errors latter");
				JOptionPane.showMessageDialog(this,
						"The provided calibration look like wrong and was dropped."
						+ " This may lead to some errors latter.",
						"Calibration error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	/**
	 * Change the data name.
	 *
	 * @param nameData The path of the file.
	 */
	public void setDataName(String nameData) {
		dataFileText.setText(nameData);
	}

	/**
	 * Return the model.
	 *
	 * @return The model.
	 */
	public RotationalDataModel getModel() {
		return model;
	}

	/**
	 * Open a {@link JFileChooser} to select a file to open.
	 */
	public void onLoadButtonClicked() {
		try {
			JFileChooser chooser = new CassisJFileChooser(Software.getDataPath(),
					Software.getLastFolder("rotational-diagram"));
			chooser.setPreferredSize(new Dimension(600, 300));

			if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				if (file != null && file.exists()) {
					model.loadData(file);
					Software.setLastFolder("rotational-diagram", file.getParent());
				} else {
					JOptionPane.showMessageDialog(this, "The file " + file + "doesn't exist!", "Alert", JOptionPane.ERROR_MESSAGE);
				}
			}
		} catch (Exception exc) {
			LOGGER.error("Error during the reading of the file", exc);
			JOptionPane.showMessageDialog(this, "Error during opening the file.", "Alert", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Set a new model.
	 *
	 * @param newModel The new model to use.
	 */
	public void setModel(RotationalDataModel newModel) {
		model.removeModelListener(this);
		model = newModel;
		model.addModelListener(this);
		refresh();
	}

	/**
	 * Refresh the view.
	 */
	public void refresh() {
		setDataName(model.getNameData());
	}

	/**
	 * Display a popup for choosing a telescope.
	 *
	 * @param badPathProvided Path of the telescope to change.
	 * @return the new path for the telescope.
	 */
	public static String selectTelescope(String badPathProvided) {
		String returnTelescope = null;
		String telescopeName = badPathProvided == null ? null : Telescope.getNameStatic(badPathProvided);

		String telPath = Software.getTelescopePath() + File.separator + telescopeName;
		boolean deliveryFileExist = badPathProvided == null ? false : new File(telPath).exists();

		if (badPathProvided == null) {
			Object[] options = { "Select another telescope" };
			String message = "You have not chosen a telescope file, please choose one.";
			int choice = JOptionPane.showOptionDialog(null, message, "File error",
					JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE, null, options,
					options[0]);
			if (choice == 0) {
				returnTelescope = displayTelescopeChooser(badPathProvided);
			}
		} else if (deliveryFileExist) {
			Object[] options = { "Select another telescope",
					"Select the delivery telescope", "Cancel" };
			String message = "Telescope file \""
					+ badPathProvided
					+ "\" not found.\n"
					+ "You can select another file or choose the delivery telescope with the same name:";
			int choice = JOptionPane.showOptionDialog(null, message, "Cassis error",
					JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE, null, options,
					options[0]);
			if (choice == 0) {
				returnTelescope = displayTelescopeChooser(badPathProvided);
			} else if (choice == 1) {
				returnTelescope = telPath;
			}
		} else {
			Object[] options = { "Select another telescope", "Cancel" };
			String message = "Telescope file \""
					+ badPathProvided
					+ "\" not found.\n"
					+ "To continue, you must select another telescope.";
			int choice = JOptionPane.showOptionDialog(null, message, "Cassis error",
					JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE, null, options,
					options[0]);
			if (choice == 0) {
				returnTelescope = displayTelescopeChooser(badPathProvided);
			}
		}
		return returnTelescope;
	}

	/**
	 * Open a file chooser for selecting a telescope file.
	 *
	 * @param badPathProvided path of the telescope who do not exist (the one to change).
	 * @return The path of the new telescope file.
	 */
	private static String displayTelescopeChooser(String badPathProvided) {
		String parentPath = badPathProvided == null || badPathProvided.isEmpty() ?
				null : new File(badPathProvided).getParent();
		JFileChooser loadFile = new CassisJFileChooser(Software.getTelescopePath(), parentPath);
		loadFile.setDialogTitle("Telescope " + badPathProvided
				+ " do not exist, select a new one.");
		int ret = loadFile.showOpenDialog(null);

		if (ret == JFileChooser.APPROVE_OPTION) {
			return loadFile.getSelectedFile().getAbsolutePath();
		}
		return null;
	}

	/**
	 * Display a dialog indicating a Molecule was not found in the database.
	 */
	public void displayMoleculeNotFoundErrorDialog() {
		JOptionPane.showMessageDialog(this,
				"Species/tag not found in the currently selected database.\n"
				+ "Please change the database and load your file again.", "Database error",
				JOptionPane.ERROR_MESSAGE);
	}

}
