/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.modeltuning;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import eu.omp.irap.cassis.common.BufferedWriterProperty;
import eu.omp.irap.cassis.common.ParamArrayDescription;
import eu.omp.irap.cassis.common.ParameterDescription;
import eu.omp.irap.cassis.common.SideBand;
import eu.omp.irap.cassis.common.Telescope;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.events.DataModel;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.gui.model.parameter.telescope.TelescopeModel;
import eu.omp.irap.cassis.gui.util.PropertiesLinkedHashMap;

public class ModelTuningModel extends DataModel {

	public static final String BAND_VALUE_EVENT = "bandValue";
	public static final String BAND_UNIT_EVENT = "bandUnit";
	public static final String DSB_SELECTED_EVENT = "dsbSelected";
	public static final String LINE_VALUE_EVENT = "lineValue";
	public static final String LOFREQ_EVENT = "loFreq";
	public static final String MAX_VALUE_EVENT = "maxValue";
	public static final String MIN_VALUE_EVENT = "minValue";
	public static final String RESOLUTION_EVENT = "resolution";
	public static final String RESOLUTION_UNIT_EVENT = "resolutionUnit";
	public static final String SIDEBAND_EVENT = "sideBand";
	public static final String TELESCOPE_EVENT = "telescope";
	public static final String TUNING_MODE_EVENT = "tuningMode";
	public static final String VAL_UNIT_EVENT = "valUnit";

	private TuningMode tuningMode = TuningMode.RANGE;

	private double minValue;
	private double maxValue;
	private UNIT valUnit = UNIT.GHZ;
	private double resolution;
	private UNIT resolutionUnit = UNIT.MHZ;

	private double lineValue;
	private double bandValue;
	private UNIT bandUnit = UNIT.GHZ;

	private boolean dsbSelected = false;
	private SideBand sideBand = SideBand.LSB;
	private double loFreq;

	private TelescopeModel telescopeModel;
	private boolean onLoading = false;


	public ModelTuningModel(double minValue, double maxValue, UNIT valUnit, double resolution, UNIT resolutionUnit) {
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.valUnit = valUnit;
		this.resolution = resolution;
		this.resolutionUnit = resolutionUnit;

		telescopeModel = new TelescopeModel();
	}

	public boolean isRangeMode() {
		return TuningMode.RANGE.equals(tuningMode);
	}

	public boolean isLineMode() {
		return TuningMode.LINE.equals(tuningMode);
	}

	/**
	 * @return the tuningMode
	 */
	public final TuningMode getTuningMode() {
		return tuningMode;
	}

	/**
	 * @param tuningMode
	 *            the tuningMode to set
	 */
	public final void setTuningMode(TuningMode tuningMode) {
		this.tuningMode = tuningMode;
		fireDataChanged(new ModelChangedEvent(TUNING_MODE_EVENT, tuningMode));
	}

	/**
	 * @return the minValue
	 */
	public final double getMinValue() {
		return minValue;
	}

	/**
	 * @return the maxValue
	 */
	public final double getMaxValue() {
		return maxValue;
	}

	/**
	 * @return the valUnit
	 */
	public final UNIT getValUnit() {
		return valUnit;
	}

	/**
	 * @return the resolution
	 */
	public final double getResolution() {
		return resolution;
	}

	/**
	 * @return the resolutionUnit
	 */
	public final UNIT getResolutionUnit() {
		return resolutionUnit;
	}

	/**
	 * @return the lineValue
	 */
	public final double getLineValue() {
		return lineValue;
	}

	/**
	 * @return the bandValue
	 */
	public final double getBandValue() {
		return bandValue;
	}

	/**
	 * @return the bandUnit
	 */
	public final UNIT getBandUnit() {
		return bandUnit;
	}

	/**
	 * @return the dsbSelected
	 */
	public final boolean isDsbSelected() {
		return dsbSelected;
	}

	/**
	 * @return the sideBand
	 */
	public final SideBand getSideBand() {
		return sideBand;
	}

	/**
	 * @return the loFreq
	 */
	public final double getLoFreq() {
		return loFreq;
	}

	/**
	 * @return the telescope
	 */
	public final Telescope getTelescope() {
		return telescopeModel.getTelescope();
	}

	/**
	 * @param minValue the minValue to set
	 */
	public final void setMinValue(double minValue) {
		this.minValue = minValue;
		fireDataChanged(new ModelChangedEvent(MIN_VALUE_EVENT, minValue));
	}

	/**
	 * @param maxValue the maxValue to set
	 */
	public final void setMaxValue(double maxValue) {
		this.maxValue = maxValue;
		fireDataChanged(new ModelChangedEvent(MAX_VALUE_EVENT, maxValue));
	}

	/**
	 * @param valUnit the valUnit to set
	 */
	public final void setValUnit(UNIT valUnit) {
		this.valUnit = valUnit;
		fireDataChanged(new ModelChangedEvent(VAL_UNIT_EVENT, valUnit));
	}

	/**
	 * @param resolution the resolution to set
	 */
	public final void setResolution(double resolution) {
		this.resolution = resolution;
		fireDataChanged(new ModelChangedEvent(RESOLUTION_EVENT, resolution));
	}

	/**
	 * @param resolutionUnit the resolutionUnit to set
	 */
	public final void setResolutionUnit(UNIT resolutionUnit) {
		this.resolutionUnit = resolutionUnit;
		fireDataChanged(new ModelChangedEvent(RESOLUTION_UNIT_EVENT, resolutionUnit));
	}

	/**
	 * @param lineValue the lineValue to set
	 */
	public final void setLineValue(double lineValue) {
		this.lineValue = lineValue;
		fireDataChanged(new ModelChangedEvent(LINE_VALUE_EVENT, lineValue));
	}

	/**
	 * @param bandValue the bandValue to set
	 */
	public final void setBandValue(double bandValue) {
		this.bandValue = bandValue;
		fireDataChanged(new ModelChangedEvent(BAND_VALUE_EVENT, bandValue));
	}

	/**
	 * @param bandUnit the bandUnit to set
	 */
	public final void setBandUnit(UNIT bandUnit) {
		this.bandUnit = bandUnit;
		fireDataChanged(new ModelChangedEvent(BAND_UNIT_EVENT, bandUnit));
	}

	/**
	 * @param dsbSelected the dsbSelected to set
	 */
	public final void setDsbSelected(boolean dsbSelected) {
		this.dsbSelected = dsbSelected;
		fireDataChanged(new ModelChangedEvent(DSB_SELECTED_EVENT, dsbSelected));
	}

	/**
	 * @param sideBand the sideBand to set
	 */
	public final void setSideBand(SideBand sideBand) {
		this.sideBand = sideBand;
		fireDataChanged(new ModelChangedEvent(SIDEBAND_EVENT, sideBand));
	}

	/**
	 * @param loFreq the loFreq to set
	 */
	public final void setLoFreq(double loFreq) {
		this.loFreq = loFreq;
		fireDataChanged(new ModelChangedEvent(LOFREQ_EVENT, loFreq));
	}

	/**
	 * @param telescope the telescope to set
	 */
	public final void setTelescope(Telescope telescope) {
		this.telescopeModel.setSelectedTelescope(telescope.getFileName());
		fireDataChanged(new ModelChangedEvent(TELESCOPE_EVENT, telescope));
	}

	@Override
	public void loadConfig(Properties prop) {
		onLoading = true;
		if (prop.containsKey("frequencyMode")) {
			boolean modeFreq = "1".equals(prop.getProperty("frequencyMode"))
					|| "true".equals(prop.getProperty("frequencyMode"));

			if (modeFreq) {
				setTuningMode(TuningMode.RANGE);
			} else {
				setTuningMode(TuningMode.LINE);
			}
		}
		else {
			setTuningMode(TuningMode.valueOf(prop.getProperty("tuningMode", "RANGE")));
		}

		if (prop.containsKey("rangeMin")) {
			setMinValue(Double.valueOf(prop.getProperty("rangeMin")));
		} else {
			setMinValue(Double.valueOf(prop.getProperty("minValue", "115.0")));
		}

		if (prop.containsKey("rangeMax")) {
			setMaxValue(Double.valueOf(prop.getProperty("rangeMax")));
		} else {
			setMaxValue(Double.valueOf(prop.getProperty("maxValue", "116.0")));
		}

		setValUnit(UNIT.valueOf(prop.getProperty("valUnit", "GHZ")));
		setBandUnit(UNIT.valueOf(prop.getProperty("bandUnit", "GHZ")));

		if (prop.containsKey("dsbMode")) {
			boolean dsbMode = "1".equals(prop.getProperty("dsbMode")) || "true".equals(prop.getProperty("dsbMode"));
			setDsbSelected(dsbMode);
		} else {
			setDsbSelected(Boolean.valueOf(prop.getProperty("dsbSelected", "false")));
		}
		setSideBand(SideBand.valueOf(prop.getProperty("dsb", prop.getProperty("sideBand"))));

		telescopeModel.loadConfig(prop);

		if (prop.containsKey("bandwith")) {
			setBandValue(Double.valueOf(prop.getProperty("bandwith", prop.getProperty("bandValue"))));
		} else {
			setBandValue(Double.valueOf(prop.getProperty("bandwidth", prop.getProperty("bandValue"))));
		}

		setResolution(Double.valueOf(prop.getProperty("resolution")));
		setResolutionUnit(UNIT.valueOf(prop.getProperty("resolutionUnit", "MHZ")));
		setLoFreq(Double.valueOf(prop.getProperty("loFreq")));
		setLineValue(Double.valueOf(prop.getProperty("line", prop.getProperty("lineValue"))));
		onLoading = false;
	}

	@Override
	public void saveConfig(BufferedWriterProperty out) throws IOException {
		Map<String, String> properties = getProperties();
		for (String val : properties.keySet()) {
			out.writeProperty(val, properties.get(val));
		}
		out.newLine();
		out.flush();
	}

	public Map<String, ParameterDescription> getMapParameter() {
		Map<String, ParameterDescription> result = new HashMap<>();

		result.put("resolution", new ParameterDescription(resolution));
		ParameterDescription paraDescUnitReso = new ParameterDescription();
		paraDescUnitReso.setVarName(resolutionUnit.name());
		result.put("resolutionUnit", paraDescUnitReso);

		if (TuningMode.LINE.equals(getTuningMode())) {
			final Double lineInMhz = XAxisCassis.getXAxisCassis(bandUnit).convertToMHzFreq(lineValue);
			result.put("line_freq", new ParameterDescription(lineInMhz));

			double tmp = XAxisCassis.getXAxisCassis(bandUnit).convertToMHzFreq(lineValue - bandValue / 2);
			double tmp2 = XAxisCassis.getXAxisCassis(bandUnit).convertToMHzFreq(lineValue + bandValue / 2);
			result.put("freqMin", new ParameterDescription(Math.min(tmp, tmp2)));
			result.put("freqMax", new ParameterDescription(Math.max(tmp, tmp2)));

			if (dsbSelected) {
				result.put("dsbMode", new ParameterDescription(1));
				result.put("lsbList", new ParamArrayDescription(telescopeModel.getTelescope().getLsbList()));
				result.put("usbList", new ParamArrayDescription(telescopeModel.getTelescope().getUsbList()));
				result.put("intermediateList", new ParamArrayDescription(telescopeModel.getTelescope().getIntermediateFrequency()));
				if (SideBand.LSB.equals(sideBand)) {
					result.put("dsb", new ParameterDescription(1));
				} else {
					result.put("dsb", new ParameterDescription(-1));
				}
				result.put("loFreq",new ParameterDescription(XAxisCassis.getXAxisCassis(valUnit).convertToMHzFreq(loFreq)));
			}
			else {
				result.put("dsbMode", new ParameterDescription(0));
			}
		} else {
			result.put("dsbMode", new ParameterDescription(0));
			double tmp = XAxisCassis.getXAxisCassis(valUnit).convertToMHzFreq(minValue);
			double tmp2 = XAxisCassis.getXAxisCassis(valUnit).convertToMHzFreq(maxValue);
			result.put("freqMin", new ParameterDescription(Math.min(tmp, tmp2)));
			result.put("freqMax", new ParameterDescription(Math.max(tmp, tmp2)));
		}

		result.put("telaper", new ParameterDescription(telescopeModel.getTelescope().getDiameter()));
		result.put("frequencyList", new ParamArrayDescription(telescopeModel.getTelescope().getFrequencyList()));

		return result;
	}

	public Map<String, String> getProperties() {
		Map<String, String> map = new PropertiesLinkedHashMap();
		map.put("tuningMode", getTuningMode().name());
		map.put("minValue", String.valueOf(getMinValue()));
		map.put("maxValue", String.valueOf(getMaxValue()));
		map.put("valUnit", getValUnit().name());
		map.put("lineValue", String.valueOf(getLineValue()));

		map.put("dsbSelected", String.valueOf(isDsbSelected()));
		map.put("dsb", getSideBand().name());
		map.put("loFreq", String.valueOf(getLoFreq()));
		map.put("telescope", String.valueOf(getTelescope().getFileName()));
		map.put("bandwidth", String.valueOf(getBandValue()));
		map.put("bandUnit", getBandUnit().name());
		map.put("resolution", String.valueOf(getResolution()));
		map.put("resolutionUnit", getResolutionUnit().name());

		return map;
	}

	public TelescopeModel getTelescopeModel() {
		return telescopeModel;
	}

	public int getNbPoints() {
		double min;
		double max;
		if (tuningMode == TuningMode.RANGE) {
			min = XAxisCassis.getXAxisCassis(resolutionUnit).convertFromMhzFreq(
					XAxisCassis.getXAxisCassis(valUnit).convertToMHzFreq(minValue));
			max = XAxisCassis.getXAxisCassis(resolutionUnit).convertFromMhzFreq(
					XAxisCassis.getXAxisCassis(valUnit).convertToMHzFreq(maxValue));
		} else if (tuningMode == TuningMode.LINE) {
			min = XAxisCassis.getXAxisCassis(resolutionUnit).convertFromMhzFreq(
					XAxisCassis.getXAxisCassis(bandUnit).convertToMHzFreq(lineValue - bandValue / 2));
			max = XAxisCassis.getXAxisCassis(resolutionUnit).convertFromMhzFreq(
					XAxisCassis.getXAxisCassis(bandUnit).convertToMHzFreq(lineValue + bandValue / 2));
		} else {
			return -1;
		}
		double band = Math.abs(max - min);

		if (band < resolution) {
			return 0;
		}
		return (int) Math.ceil(band / resolution);
	}

	/**
	 * @return the onLoading
	 */
	public boolean isOnLoading() {
		return onLoading;
	}

	/**
	 * @param onLoading the onLoading to set
	 */
	public void setOnLoading(boolean onLoading) {
		this.onLoading = onLoading;
	}
}
