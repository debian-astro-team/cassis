/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter.modeltuning;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.util.HashMap;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.TitledBorder;

import eu.omp.irap.cassis.common.SideBand;
import eu.omp.irap.cassis.common.Telescope;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.gui.model.parameter.telescope.TelescopeModel;
import eu.omp.irap.cassis.gui.model.parameter.telescope.TelescopePanel;
import eu.omp.irap.cassis.gui.util.JDoubleCassisTextField;

public class ModelTuningPanel extends JPanel implements ModelListener {

	private static final long serialVersionUID = 1L;
	private ModelTuningModel model;

	private JRadioButton rangeRadio;

	private JLabel rangeMinLabel;
	private JDoubleCassisTextField rangeMinField;

	private JLabel rangeMaxLabel;
	private JDoubleCassisTextField rangeMaxField;

	private JRadioButton lineRadio;
	private JDoubleCassisTextField lineField;
	private JCheckBox dsbBox;
	private JComboBox<SideBand> dsbCombo;
	private JLabel loFreqLabel;
	private JDoubleCassisTextField loFreqField;

	private JLabel resolutionLabel;
	private JDoubleCassisTextField resolutionField;
	private JLabel bandWidthLabel;
	private JDoubleCassisTextField bandWidthField;
	private JComboBox<UNIT> rangeUnitComboBox;
	private JComboBox<UNIT> resolutionUnitComboBox;
	private JComboBox<UNIT> lineUnitComboBox;
	private TelescopePanel telescopePanel;
	private JPanel rangePanel;
	private JPanel linePanel;


	public ModelTuningPanel() {
		this(new ModelTuningModel(0.0, 0.0, UNIT.GHZ, 0.0, UNIT.MHZ));
	}

	public ModelTuningPanel(ModelTuningModel model) {
		this.model = model;
		model.addModelListener(this);
		model.getTelescopeModel().addModelListener(this);
		initComponents();
		telescopePanel.setBorder(false);
	}

	public void initComponents() {
		setBorder(new TitledBorder("Tuning"));
		setLayout(new GridLayout(2, 1));
		add(getRangePanel());
		add(getLinePanel());

		// -- Button group
		ButtonGroup modeGroup = new ButtonGroup();
		modeGroup.add(getRangeRadio());
		modeGroup.add(getLineRadio());
		model.setTuningMode(TuningMode.RANGE);
		changeLoFrequency();
	}

	public JPanel getRangePanel() {
		if (rangePanel == null) {
			rangePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
			rangeMinLabel = new JLabel("min:");
			rangeMinLabel.setLabelFor(getRangeMinField());
			rangeMaxLabel = new JLabel("max:");
			rangeMaxLabel.setLabelFor(getRangeMaxField());
			resolutionLabel = new JLabel("dv");
			rangePanel.add(getRangeRadio());
			rangePanel.add(rangeMinLabel);
			rangePanel.add(getRangeMinField());
			rangePanel.add(rangeMaxLabel);
			rangePanel.add(getRangeMaxField());
			rangePanel.add(getRangeUnitComboBox());
			rangePanel.add(resolutionLabel);
			rangePanel.add(getResolutionField());
			rangePanel.add(getResolutionUnitComboBox());
		}
		return rangePanel;
	}

	private JDoubleCassisTextField getResolutionField() {
		if (resolutionField == null) {
			resolutionField = new JDoubleCassisTextField(3, 0.);
			resolutionField.addPropertyChangeListener("value", new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					model.setResolution(Double.valueOf(evt.getNewValue().toString()));
				}
			});
			resolutionField.setValue(model.getResolution());
		}
		return resolutionField;
	}

	private JComboBox<UNIT> getRangeUnitComboBox() {
		if (rangeUnitComboBox == null) {
			rangeUnitComboBox = new JComboBox<>(UNIT.getXValUnit());
			rangeUnitComboBox.setSelectedItem(model.getValUnit());
			rangeUnitComboBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					UNIT unit = (UNIT) getRangeUnitComboBox().getSelectedItem();
					UNIT oldUnit = model.getValUnit();

					XAxisCassis anXAxisCassis = XAxisCassis.getXAxisCassis(unit);
					XAxisCassis oldXAxisCassis = XAxisCassis.getXAxisCassis(oldUnit);

					double minValue = model.getMinValue();
					double maxValue = model.getMaxValue();
					boolean minZero = minValue == 0.0;
					boolean maxEtoile = "*".equals(getRangeMaxField().getText());
					boolean invertedRelativeToOld = anXAxisCassis.isInverted() != oldXAxisCassis.isInverted();

					if (invertedRelativeToOld) {
						if (minZero) {
							getRangeMaxField().setValue(Double.MAX_VALUE);
						} else {
							getRangeMaxField().setValue(XAxisCassis.convert(minValue, oldXAxisCassis, anXAxisCassis));
						}

						if (maxEtoile) {
							getRangeMinField().setValue(0.0);
						} else {
							getRangeMinField().setValue(XAxisCassis.convert(maxValue, oldXAxisCassis, anXAxisCassis));
						}
					} else {
						if (!minZero) {
							getRangeMinField().setValue(XAxisCassis.convert(minValue, oldXAxisCassis, anXAxisCassis));
						}
						if (!maxEtoile) {
							getRangeMaxField().setValue(XAxisCassis.convert(maxValue, oldXAxisCassis, anXAxisCassis));
						}
					}
					model.setValUnit(unit);
				}
			});
		}
		return rangeUnitComboBox;
	}

	public JDoubleCassisTextField getRangeMaxField() {
		if (rangeMaxField == null) {
			rangeMaxField = new JDoubleCassisTextField(new DecimalFormat("0.0##############"),
					new DecimalFormat("#.###############E0"), new HashMap<>());
			rangeMaxField.setColumns(6);
			rangeMaxField.setValMin(0.000001);
			rangeMaxField.setValMax(999999.999);
			rangeMaxField.addPropertyChangeListener("value", new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					model.setMaxValue(Double.valueOf(evt.getNewValue().toString()));
				}
			});
			rangeMaxField.setValue(model.getMaxValue());
		}
		return rangeMaxField;
	}

	private JComboBox<UNIT> getResolutionUnitComboBox() {
		if (resolutionUnitComboBox == null) {
			resolutionUnitComboBox = new JComboBox<>(UNIT.getXValUnit());
			resolutionUnitComboBox.setSelectedItem(model.getResolutionUnit());
			resolutionUnitComboBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					UNIT unit = (UNIT) getResolutionUnitComboBox().getSelectedItem();
					model.setResolutionUnit(unit);
				}
			});
		}
		return resolutionUnitComboBox;
	}

	private JRadioButton getRangeRadio() {
		if (rangeRadio == null) {
			rangeRadio = new JRadioButton("Range");
			rangeRadio.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					model.setTuningMode(TuningMode.RANGE);
				}
			});
		}
		return rangeRadio;
	}

	public JDoubleCassisTextField getRangeMinField() {
		if (rangeMinField == null) {
			rangeMinField = new JDoubleCassisTextField(new DecimalFormat("0.0##############"),
					new DecimalFormat("#.###############E0"), new HashMap<>());
			rangeMinField.setValMin(0.000001);
			rangeMinField.setValMax(999999.999);
			rangeMinField.setColumns(6);
			rangeMinField.addPropertyChangeListener("value", new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					model.setMinValue(Double.valueOf(evt.getNewValue().toString()));
				}
			});
			rangeMinField.setValue(model.getMinValue());
		}
		return rangeMinField;
	}

	public JPanel getLinePanel() {
		if (linePanel == null) {
			linePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));

			// -- LO freq parameter
			loFreqLabel = new JLabel("LO freq:");
			loFreqLabel.setLabelFor(getLoFreqField());

			bandWidthLabel = new JLabel("Bandwidth:");

			linePanel.add(getLineRadio());
			linePanel.add(getLineField());
			linePanel.add(bandWidthLabel);
			linePanel.add(getBandWidthField());
			linePanel.add(getLineUnitComboBox());
			linePanel.add(getDsbBox());
			linePanel.add(getDsbCombo());
			linePanel.add(loFreqLabel);
			linePanel.add(getLoFreqField());
			linePanel.add(getTelescopePanel());
			enableDsb(false);
		}
		return linePanel;
	}

	public TelescopePanel getTelescopePanel() {
		if (telescopePanel == null) {
			telescopePanel = new TelescopePanel(model.getTelescopeModel());
//			telescopePanel.getTelescopeButton().addActionListener(new ActionListener() {
//				@Override
//				public void actionPerformed(ActionEvent e) {
//					changeLoFrequency();
//				}
//			});
		}
		return telescopePanel;
	}

	private JDoubleCassisTextField getBandWidthField() {
		if (bandWidthField == null) {
			bandWidthField = new JDoubleCassisTextField(new DecimalFormat("0.0##############"),
					new DecimalFormat("#.###############E0"), new HashMap<>());
			bandWidthField.setColumns(3);
			bandWidthField.addPropertyChangeListener("value", new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					model.setBandValue(Double.valueOf(evt.getNewValue().toString()));
				}
			});
			bandWidthField.setValue(model.getBandValue());
		}
		return bandWidthField;
	}

	private JDoubleCassisTextField getLoFreqField() {
		if (loFreqField  == null) {
			loFreqField = new JDoubleCassisTextField(new DecimalFormat("0.0##############"),
					new DecimalFormat("#.###############E0"), new HashMap<>());
			loFreqField.setColumns(6);
			loFreqField.addPropertyChangeListener("value", new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					changeLineFrequency();
				}
			});
			loFreqField.setValue(model.getLoFreq());
		}
		return loFreqField;
	}

	private JComboBox<SideBand> getDsbCombo() {
		if (dsbCombo == null) {
			dsbCombo = new JComboBox<>(new SideBand[] { SideBand.LSB, SideBand.USB });
			dsbCombo.setSelectedItem(model.getSideBand());
			dsbCombo.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					model.setSideBand((SideBand) dsbCombo.getSelectedItem());
				}
			});
		}
		return dsbCombo;
	}

	private JCheckBox getDsbBox() {
		if (dsbBox == null) {
			dsbBox = new JCheckBox("DSB");
			dsbBox.setSelected(model.isDsbSelected());
			dsbBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					model.setDsbSelected(dsbBox.isSelected());
				}
			});
		}
		return dsbBox;
	}

	private JComboBox<UNIT> getLineUnitComboBox() {
		if (lineUnitComboBox == null) {
			lineUnitComboBox = new JComboBox<>(UNIT.getXValUnit());
			lineUnitComboBox.setSelectedItem(model.getBandUnit());
			lineUnitComboBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					UNIT unit = (UNIT) getLineUnitComboBox().getSelectedItem();
					UNIT oldUnit = model.getBandUnit();
					XAxisCassis oldAxis = XAxisCassis.getXAxisCassis(oldUnit);
					XAxisCassis newAxis = XAxisCassis.getXAxisCassis(unit);
					double lineMHz = oldAxis.convertToMHzFreq(model.getLineValue());

					double newLineValue = newAxis.convertFromMhzFreq(lineMHz);
					model.setBandUnit(unit);
					model.setLineValue(newLineValue);

					double bandValue = newAxis.convertDeltaFromMhz(
							oldAxis.convertDeltaToMhz(model.getBandValue(), lineMHz),
							lineMHz);
					model.setBandValue(bandValue);
				}
			});
		}
		return lineUnitComboBox;
	}

	private JDoubleCassisTextField getLineField() {
		if (lineField == null) {
			lineField = new JDoubleCassisTextField(new DecimalFormat("0.0##############"),
					new DecimalFormat("#.###############E0"), new HashMap<>());
			lineField.setColumns(6);
			lineField.setValMin(0.000001);
			lineField.setValMax(999999.999);
			lineField.addPropertyChangeListener("value", new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					model.setLineValue(Double.valueOf(evt.getNewValue().toString()));
					changeLoFrequency();
				}
			});
			lineField.setValue(model.getLineValue());
		}
		return lineField;
	}

	public JRadioButton getLineRadio() {
		if (lineRadio == null) {
			lineRadio = new JRadioButton("Line:");
			lineRadio.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					model.setTuningMode(TuningMode.LINE);
				}
			});
		}
		return lineRadio;
	}

	private void setRangeUnit(UNIT unit) {
		rangeUnitComboBox.setSelectedItem(unit);
	}

	private void enableDsb(boolean b) {
		dsbCombo.setEnabled(b);
		loFreqLabel.setEnabled(b);
		loFreqField.setEnabled(b);
		telescopePanel.setEnabled(b);
	}

	protected void selectTuningMode(TuningMode range) {
		if (TuningMode.LINE.equals(range)) {
			lineRadio.setSelected(true);
			enableRangePanel(false);
			enableLinePanel(true);
			enableDsb(dsbBox.isSelected());
			changeLoFrequency();
		}
		else if (TuningMode.RANGE.equals(range)) {
			rangeRadio.setSelected(true);
			enableRangePanel(true);
			enableLinePanel(false);
			enableDsb(false);
		}
	}

	private void enableRangePanel(boolean b) {
		rangeMinLabel.setEnabled(b);
		rangeMinField.setEnabled(b);
		rangeMaxLabel.setEnabled(b);
		rangeMaxField.setEnabled(b);
		rangeUnitComboBox.setEnabled(b);
	}

	private void enableLinePanel(boolean b) {
		lineField.setEnabled(b);
		bandWidthLabel.setEnabled(b);
		bandWidthField.setEnabled(b);
		lineUnitComboBox.setEnabled(b);
		dsbBox.setEnabled(b);
	}

	private void changeLoFrequency() {
		if (model.isOnLoading())
			return;
		double lo = getTelescopePanel().getModel().getTelescope().computeLoFreq(
				XAxisCassis.getXAxisCassis(model.getBandUnit()).convertToMHzFreq(model.getLineValue()),
				model.getSideBand());
		model.setLoFreq(XAxisCassis.getXAxisCassis(model.getBandUnit()).convertFromMhzFreq(lo));
	}

	private void changeLineFrequency() {
		if (model.isOnLoading())
			return;
		double line = getTelescopePanel().getModel().getTelescope().computeLineFreq(
				XAxisCassis.getXAxisCassis(model.getBandUnit()).convertToMHzFreq(model.getLoFreq()),
				model.getSideBand());
		model.setLineValue(XAxisCassis.getXAxisCassis(model.getBandUnit()).convertFromMhzFreq(line));
	}

	@Override
	public void dataChanged(ModelChangedEvent event) {
		if (ModelTuningModel.TUNING_MODE_EVENT.equals(event.getSource())) {
			TuningMode value = (TuningMode) event.getValue();
			selectTuningMode(value);
		}
		else if (ModelTuningModel.MIN_VALUE_EVENT.equals(event.getSource())) {
			Double value = (Double) event.getValue();
			rangeMinField.setValue(value);
		}
		else if (ModelTuningModel.MAX_VALUE_EVENT.equals(event.getSource())) {
			Double value = (Double) event.getValue();
			rangeMaxField.setValue(value);
		}
		else if (ModelTuningModel.VAL_UNIT_EVENT.equals(event.getSource())) {
			UNIT value = (UNIT) event.getValue();
			setRangeUnit(value);
		}
		else if (ModelTuningModel.RESOLUTION_EVENT.equals(event.getSource())) {
			Double value = (Double) event.getValue();
			resolutionField.setValue(value);
		}
		else if (ModelTuningModel.RESOLUTION_UNIT_EVENT.equals(event.getSource())) {
			UNIT value = (UNIT) event.getValue();
			getResolutionUnitComboBox().setSelectedItem(value);
		}
		else if (ModelTuningModel.LINE_VALUE_EVENT.equals(event.getSource())) {
			Double value = (Double) event.getValue();
			lineField.setValue(value);
		}
		else if (ModelTuningModel.BAND_VALUE_EVENT.equals(event.getSource())) {
			bandWidthField.setValue((Double) event.getValue());
		}
		else if (ModelTuningModel.BAND_UNIT_EVENT.equals(event.getSource())) {
			lineUnitComboBox.setSelectedItem((UNIT) event.getValue());
		}
		else if (ModelTuningModel.DSB_SELECTED_EVENT.equals(event.getSource())) {
			Boolean value = (Boolean) event.getValue();
			dsbBox.setSelected(value);
			enableDsb(dsbBox.isSelected());
		}
		else if (ModelTuningModel.SIDEBAND_EVENT.equals(event.getSource())) {
			SideBand value = (SideBand) event.getValue();
			dsbCombo.setSelectedItem(value);
			changeLoFrequency();
		}
		else if (ModelTuningModel.LOFREQ_EVENT.equals(event.getSource())) {
			Double value = (Double) event.getValue();
			loFreqField.setValue(value);
		}
		else if (ModelTuningModel.TELESCOPE_EVENT.equals(event.getSource())) {
			telescopePanel.getModel().setSelectedTelescope(((Telescope) event.getValue()).getFileName());
			changeLoFrequency();
		}
		else if (TelescopeModel.SELECTED_TELESCOPE_EVENT.equals(event.getSource()) ||
				TelescopeModel.LOCAL_TELESCOPE_EVENT.equals(event.getSource())) {
			String telescopeName = new Telescope((String) event.getValue()).getName();
			if (telescopeName.startsWith("spire") || "pacs".equalsIgnoreCase(telescopeName)) {
				model.setResolution(3.0);
			} else {
				model.setResolution(0.1);
			}
			model.setResolutionUnit(UNIT.MHZ);
			changeLoFrequency();
		}
	}

	/**
	 * @param typeName the typeName to set
	 */
	public final void setTypeName(String typeName) {
		telescopePanel.setTypeName(typeName+"-telescope");
	}

	/**
	 * Set a new model.
	 *
	 * @param newModel The new ModelTuningModel to use.
	 */
	public void setModel(ModelTuningModel newModel) {
		model.getTelescopeModel().removeModelListener(this);
		model.removeModelListener(this);
		model = newModel;
		model.getTelescopeModel().addModelListener(this);
		model.addModelListener(this);
		refresh();
	}

	/**
	 * Refresh the view.
	 */
	private void refresh() {
		model.setOnLoading(true);
		getRangeUnitComboBox().setSelectedItem(model.getValUnit());
		getRangeMinField().setValue(model.getMinValue());
		getRangeMaxField().setValue(model.getMaxValue());

		getResolutionUnitComboBox().setSelectedItem(model.getResolutionUnit());
		getResolutionField().setValue(model.getResolution());

		getLineUnitComboBox().setSelectedItem(model.getBandUnit());
		getLineField().setValue(model.getLineValue());
		getBandWidthField().setValue(model.getBandValue());

		getDsbBox().setSelected(model.isDsbSelected());
		getDsbCombo().setSelectedItem(model.getSideBand());

		getTelescopePanel().setModel(model.getTelescopeModel());
		getLoFreqField().setValue(model.getLoFreq());
		selectTuningMode(model.getTuningMode());
		model.setOnLoading(false);
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(new ModelTuningPanel());
		frame.pack();
		frame.setVisible(true);
	}

}
