/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.gui.util.MyTelescopeButton;
import eu.omp.irap.cassis.properties.Software;

@SuppressWarnings("serial")
public class ParametersPanel extends JPanel {

	// Telescope Panel
	protected JButton telescopeButton;
	private JCheckBox tmbTaBox;
	private ParametersControl control;
	private String typeName = "line-analysis-telescope";


	public ParametersPanel() {
		this(new ParametersModel());
	}

	public ParametersPanel(ParametersModel model) {
		// --- Telescope panel ---
		control = new ParametersControl(model, this);
		setLayout(new FlowLayout(FlowLayout.LEFT));
		setBorder(new TitledBorder("Telescope"));
		setSize(700, 75);

		telescopeButton = new MyTelescopeButton(model.getSelectedTelescope());
		telescopeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				browseTelescopeFile();
			}
		});

		add(telescopeButton);
		add(getTmbTaBox());
	}

	protected void browseTelescopeFile() {
		JFileChooser loadFile = new CassisJFileChooser(Software.getTelescopePath(),
				Software.getLastFolder(typeName));

		loadFile.setDialogTitle("Select a telescope file");
		int ret = loadFile.showOpenDialog(this);
		if (ret == JFileChooser.APPROVE_OPTION) {
			control.getModel().setSelectedTelescope(loadFile.getSelectedFile().getAbsolutePath());
			Software.setLastFolder(typeName, loadFile.getSelectedFile().getParent());
		}
	}

	public ParametersModel getModel() {
		return control.getModel();
	}



	public JCheckBox getTmbTaBox() {
		if (tmbTaBox == null) {
			tmbTaBox = new JCheckBox();
			tmbTaBox.setText("Tmb->Ta *");
			tmbTaBox.addActionListener(control);
			tmbTaBox.setSelected(control.getModel().isTmbTa());
		}
		return tmbTaBox;
	}

	/**
	 * Refresh the view.
	 */
	public void refresh() {
		ParametersModel pm = control.getModel();

		telescopeButton.setText(pm.getSelectedTelescope());
		if (pm.getTelescope().getName().startsWith("spire")) {
			tmbTaBox.setEnabled(false);
		} else {
			tmbTaBox.setEnabled(true);
		}
		tmbTaBox.setSelected(pm.getTmbTa());
	}

	public ParametersControl getControl() {
		return control;
	}
}
