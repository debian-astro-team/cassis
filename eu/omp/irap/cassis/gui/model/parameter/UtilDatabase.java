/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.model.parameter;

import java.util.Iterator;
import java.util.List;

import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.UtilArrayList;
import eu.omp.irap.cassis.database.access.LineDescriptionDB;
import eu.omp.irap.cassis.gui.plot.util.SORTING_PLOT;

public class UtilDatabase {


	public static boolean isInThresholdQn(String quanticNumber, List<String> quantumTreshold) {
		int nbQuanticToCompare = quantumTreshold.size() / 4;
		boolean result = true;
		String cq = quanticNumber;

		String[] cqTab = cq.split(":");
		int size = cqTab.length / 2;
		for (int i = 0; i < Math.min(size, nbQuanticToCompare) && result; i++) {
			result = result && isQnumberSup(cqTab[i], quantumTreshold.get(i * 4))
					&& isQnumberInf(cqTab[i], quantumTreshold.get(i * 4 + 1))
					&& isQnumberSup(cqTab[i + size], quantumTreshold.get(i * 4 + 2))
					&& isQnumberInf(cqTab[i + size], quantumTreshold.get(i * 4 + 3));
		}

		return result;
	}

	private static boolean isQnumberSup(String quantumNb, String qnToCompare) {
		boolean result;
		if ("*".equals(qnToCompare)) {
			result = true;
		}
		else if ("+".equals(qnToCompare) || "-".equals(qnToCompare) || " ".equals(qnToCompare)) {
			result = qnToCompare.equals(quantumNb);
		}
		else {
			double valToCompare = 0;
			try {
				if (qnToCompare.indexOf('/') != -1) {
					String[] operande = qnToCompare.split("/");
					if (operande.length != 2)
						throw new NumberFormatException();
					else {
						valToCompare = Double.parseDouble(operande[0]) / Double.parseDouble(operande[1]);
					}
				}
				else {
					valToCompare = Double.parseDouble(qnToCompare);
				}

				result = Double.parseDouble(quantumNb) >= valToCompare;
			} catch (NumberFormatException e) {
				result = true;
			}

		}
		return result;
	}

	private static boolean isQnumberInf(String quantumNb, String qnToCompare) {
		boolean result = false;
		if ("*".equals(qnToCompare)) {
			result = true;
		}
		else if ("+".equals(qnToCompare) || "-".equals(qnToCompare) || " ".equals(qnToCompare)) {
			result = qnToCompare.equals(quantumNb);
		}
		else {
			double valToCompare = 0;
			try {
				if (qnToCompare.indexOf('/') != -1) {
					String[] operande = qnToCompare.split("/");
					if (operande.length != 2)
						throw new NumberFormatException();
					else {
						valToCompare = Double.parseDouble(operande[0]) / Double.parseDouble(operande[1]);
					}
				}
				else {
					valToCompare = Double.parseDouble(qnToCompare);
				}

				result = Double.parseDouble(quantumNb) <= valToCompare;
			} catch (NumberFormatException e) {
				result = true;
			}

		}
		return result;
	}

	/**
	 * Compute the indexes of the provided lines sorted according to the sortingLine parameter.
	 *
	 * @param listOfLines The lines.
	 * @param sortingLine The sorted parameter.
	 * @return the indice of lines sorted.
	 */
	public static int[] sortLines(List<LineDescription> listOfLines, SORTING_PLOT sortingLine) {
		// create a new list of lines for sorting
		int nline = listOfLines.size();

		// if there is no lines, return an empty list
		if (nline <= 0) {
			return null;
		}

		double[] tosort = new double[nline];
		int[] indexes = new int[nline];

		for (int k = 0; k < nline; k++) {
			LineDescription line = listOfLines.get(k);

			// Sorting by Frequency
			if (SORTING_PLOT.SORTING_FREQUENCY.equals(sortingLine)) {
				tosort[k] = line.getObsFrequency();
			}
			else if (SORTING_PLOT.SORTING_EUP.equals(sortingLine)) {// Sorting by Eup
				tosort[k] = line.getEUpK();
			}
			else if (SORTING_PLOT.SORTING_AIJ.equals(sortingLine)) {// Sorting by Aij
				tosort[k] = -line.getAij();
			}

			indexes[k] = k;
		}

		UtilArrayList.quicksortindex(tosort, indexes, 0, nline - 1);

		// return the result
		return indexes;
	}

	/**
	 * Check in a list of String if all String is an asterisk (*).
	 *
	 * @param quantumNumbersArray The list of String.
	 * @return if all the string are asterisk.
	 */
	private static boolean isAllAsterisk(List<String> quantumNumbersArray) {
		for (String string : quantumNumbersArray) {
			if (!"*".equals(string)) {
				return false;
			}
		}
		return true;
	}

	public static void removeLineOverQuantumFilter(List<LineDescription> listOfLines,
			final List<String> quantumNumbersArray2) {
		if (isAllAsterisk(quantumNumbersArray2)) {
			return;
		}
		if (quantumNumbersArray2 != null) {
			for (Iterator<LineDescription> it = listOfLines.iterator(); it.hasNext();) {
				LineDescription line = it.next();
				if (!UtilDatabase.isInThresholdQn(line.getQuanticN(),quantumNumbersArray2)) {
					it.remove();
				}
			}
		}
	}

	public static void removeLineDBOverQuantumFilter(List<LineDescriptionDB> listOfLines,
			final List<String> quantumNumbersArray2) {
		if (isAllAsterisk(quantumNumbersArray2)) {
			return;
		}
		if (quantumNumbersArray2 != null) {
			for (Iterator<LineDescriptionDB> it = listOfLines.iterator(); it.hasNext();) {
				LineDescriptionDB line = it.next();
				if (!UtilDatabase.isInThresholdQn(line.getQuanticNumbers(),quantumNumbersArray2)) {
					it.remove();
				}
			}
		}
	}
}
