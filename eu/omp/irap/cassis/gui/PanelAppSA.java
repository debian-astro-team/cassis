/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui;

import java.awt.AWTEvent;
import java.awt.event.AWTEventListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Locale;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.plaf.metal.MetalLookAndFeel;
import javax.swing.plaf.metal.OceanTheme;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.database.access.AccessDataBase;
import eu.omp.irap.cassis.database.access.DatabaseException;
import eu.omp.irap.cassis.database.access.InfoDataBase;
import eu.omp.irap.cassis.database.access.TypeDataBase;
import eu.omp.irap.cassis.demo.UpdateGui;
import eu.omp.irap.cassis.gui.scripts.JythonEditorConfiguration;
import eu.omp.irap.cassis.gui.util.CassisRessource;
import eu.omp.irap.cassis.properties.Software;
import eu.omp.irap.cassis.scripts.JythonControl;
import eu.omp.irap.cassis.scripts.JythonFrame;
import eu.omp.irap.cassis.scripts.util.ScriptUtil;
import eu.omp.irap.cassis.vo.VoCassis;
import eu.omp.irap.mac.MacHandling;
import fr.jmmc.jmcs.logging.LoggingService;

public class PanelAppSA {

	private static final Logger LOGGER = LoggerFactory.getLogger(PanelAppSA.class);
	private static final String DEBUG_CONFIG = "eu/omp/irap/cassis/logger/DevelopmentConfiguration.xml";
	private static final String RELEASE_CONFIG = "eu/omp/irap/cassis/logger/ReleaseConfiguration.xml";


	/**
	 * This method is the main of Cassis Application.
	 *
	 * @param args The arguments.
	 */
	public PanelAppSA(String[] args) {
		initLookAndFeel();

		JFrame frame = new JFrame();
		frame.setSize(PanelFrame.WIDTH, PanelFrame.HEIGHT);
		if (args.length == 2 && "online".equals(args[0])) {
			System.out.println("Welcome to CASSIS online.");
			System.out.println(Software.NAME_EXT + " - " + Software.getSociety() + " - " + Software.getYear());
			String xmlUrl = args[1];
			UpdateGui updateGui = new UpdateGui(frame, xmlUrl);
			updateGui.start();
			Software.setOnlineMode();
		} else {
			Software.setApp();
			System.out.println("Welcome to " + Software.getVersion());
			System.out.println(Software.NAME_EXT + " - " + Software.getSociety() + " - " + Software.getYear());
		}
		initLogger();
		MacHandling.getInstance().setDockIconImage(CassisRessource.getCassisIcon());

		boolean splashScreenVisible = haveSplashScreenArg(args) && false;

		SplashWindow splash = null;
		if (splashScreenVisible) {
			splash = beginSplash(frame);
			splash.start();
		}
		try {
			if (Software.getUserConfiguration().isSampOnAutoStart()) {
				VoCassis.getInstance().connect();
			}

			ScriptUtil.initScriptConfiguration(new JythonEditorConfiguration());

			if(!handleDatabase(frame)) {
				if (splashScreenVisible) {
					endSplash(frame, splash);
				}
				System.exit(0);
			}

			// Show PanelFrame
			PanelFrame.getInstance();

			// Disable update menu on online mode.
			if (Software.isOnlineMode()) {
				PanelFrame.getInstance().getPanelView().getMenuBar().getMenuHelp().getItem(2).setEnabled(false);
			}
		} catch (Exception e) {
			LOGGER.error("Error during the initialisation of CASSIS, please try to run it agait!", e);
			if (splashScreenVisible) {
				endSplash(frame, splash);
			}
			System.exit(0);
		}

		if (splashScreenVisible) {
			endSplash(frame, splash);
		}

		if (args.length >= 2) {
			final String jythonFile = getJython(args);
			if (jythonFile != null) {
				if (new File(jythonFile).exists()) {
					PanelFrame.getInstance().getPanelView().getCassisActionMenu().getScriptsLteRadexAction().actionPerformed(null);
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							final JythonControl control = JythonFrame.getInstance(null).getJythonView().getControl();
							control.loadScript(new File(jythonFile), true);
							control.runScript(false);
						}
					});
				} else {
					LOGGER.warn("File " + jythonFile + "does not exist. Skip it.");
				}
			}
		}
	}

	/**
	 * Search for a "-jython" followed by a file in args then return the file
	 *  if this is the case and it is not empty, null otherwise.
	 *
	 * @param args The array of arguments.
	 * @return Return the following argument after a "-jython" argument
	 *  or null if not found.
	 */
	private static String getJython(String... args) {
		if (args == null || args.length < 2) {
			return null;
		}
		boolean found = false;
		for (String arg : args) {
			if (found) {
				if (!"".contentEquals(arg)) {
					return arg;
				}
				return null;
			}
			if ("-jython".equals(arg)) {
				found = true;
			}
		}
		return null;
	}

	/**
	 * Test the database and allow to choose another one if the one currently
	 *  used does not work.
	 *
	 * @param frame The main frame of CASSIS.
	 * @return true is the database is working, false otherwise.
	 */
	public static boolean handleDatabase(JFrame frame) {
		boolean baseOk = false;
		try {
			// Init the database.
			AccessDataBase.getDataBaseConnection();
			baseOk = true;
		} catch (Exception e) {
			LOGGER.error("Error during the initialization of the database", e);
			baseOk = false;
		}
		if (!baseOk && !changeDatabaseConnection(frame)) {
			return false;
		}
		return true;
	}

	public static void main(final String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new PanelAppSA(args);
			}
		});
	}

	private static void endSplash(JFrame frame, SplashWindow splash) {
		splash.close();
		frame.dispose();
	}

	private static SplashWindow beginSplash(JFrame frame) {
		return new SplashWindow(frame, 20);
	}

	/**
	 * @param args
	 *            the list of arguments
	 * @return true if an argument equals -noSplashScreen
	 */
	private static boolean haveSplashScreenArg(String[] args) {
		boolean result = true;
		for (String arg : args) {
			if ("-noSplashScreen".equals(arg)) {
				result = false;
				break;
			}
		}
		return result;
	}

	/**
	 * Init the look and feel of the application at Metal and Theme Ocean
	 * and the language in English.
	 */
	public static void initLookAndFeel() {
		ToolTipManager.sharedInstance().setDismissDelay(20000);
		Locale.setDefault(Locale.ENGLISH);
		String lookAndFeel = UIManager.getCrossPlatformLookAndFeelClassName();

		try {
			UIManager.setLookAndFeel(lookAndFeel);
			MetalLookAndFeel.setCurrentTheme(new OceanTheme());
			UIManager.setLookAndFeel(new MetalLookAndFeel());
		} catch (Exception e) {
			LOGGER.warn("Couldn't get specified look and feel ({}), for some reason.\n"
					+ "Using the default look and feel.", lookAndFeel, e);
		}

		replacePommeByControl();
	}

	private static void replacePommeByControl() {
		java.awt.Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {
			@Override
			public void eventDispatched(AWTEvent event) {
				KeyEvent kev = (KeyEvent) event;
				if (kev.getID() == KeyEvent.KEY_PRESSED || kev.getID() == KeyEvent.KEY_RELEASED) {
					if ((kev.getModifiersEx() & KeyEvent.META_DOWN_MASK) != 0 && !((kev.getModifiersEx() & KeyEvent.CTRL_DOWN_MASK) != 0)) {
						kev.consume(); // Drop the original event, this is really optional.
						KeyEvent fake = new KeyEvent(kev.getComponent(),
								kev.getID(),
								kev.getWhen(),
								(kev.getModifiersEx() & ~KeyEvent.META_DOWN_MASK) | KeyEvent.CTRL_DOWN_MASK,
								kev.getKeyCode(), kev.getKeyChar());
						java.awt.Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(fake);
					}
				}
			}
		}, KeyEvent.KEY_EVENT_MASK);
	}

	/**
	 * Change the database connection (use a SQLite connection).
	 *
	 * @param frame The main frame.
	 * @return true if a database is configured, false otherwise.
	 */
	public static boolean changeDatabaseConnection(JFrame frame) {
		boolean value = false;
		String[] choices = { "Select another database", "Do not use database", "Exit" };
		String dbname = InfoDataBase.getInstance().getDbname();
		String msg = "The selected database '" + dbname + "' does not work.";
		int ret = JOptionPane.showOptionDialog(frame, msg, "Information",
				JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null,
				choices, null);

		if (ret == 0) { // "Select another database"
			// Show open file chooser dialog
			String parent = dbname == null || "".equals(dbname) ? null : new File(dbname).getParent();
			JFileChooser chooser = new CassisJFileChooser(Software.getDatabasePath(), parent);

			chooser.setDialogTitle("Select database file");
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			if (chooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
				final String basePath = chooser.getSelectedFile()
						.getAbsolutePath();
				// Update the InfoDatabase
				try {
					InfoDataBase.getInstance().setDataBaseType(
							TypeDataBase.SQLITE.name(), basePath,
							InfoDataBase.getInstance().isInMemoryDatabase());
					value = true;
				} catch (DatabaseException de) {
					LOGGER.error("Error with the database", de);
					value = false;
				}
			}
		} else if (ret == 1) { // "Do not use database"
			InfoDataBase.getInstance().setDataBaseType(
					TypeDataBase.NO.name(), "",
					InfoDataBase.getInstance().isInMemoryDatabase());
			value = true;
		} else if (ret == 2) { // "Exit"
			value = false;
		}
		return value;
	}

	/**
	 * Init the logger.
	 */
	private void initLogger() {
		String pathConfig = Software.getUserConfiguration().isTestMode() ?
				DEBUG_CONFIG : RELEASE_CONFIG;
		LoggingService.getInstance(pathConfig);
	}

}
