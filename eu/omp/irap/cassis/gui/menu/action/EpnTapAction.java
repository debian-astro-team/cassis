/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.menu.action;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.common.gui.ScreenUtil;
import eu.omp.irap.cassis.gui.util.DownloadMultiGui;
import eu.omp.irap.cassis.properties.Software;
import eu.omp.irap.vespa.epntapclient.gui.mainpanel.MainPanelCtrl;
import eu.omp.irap.vespa.epntapclient.gui.requestpanel.paramfield.DataProductTypeField;
import eu.omp.irap.vespa.votable.votable.VOTableException.CanNotParseDataException;
import eu.omp.irap.vespa.votable.votabledata.VOTableData;

/**
 *  Action related to the EPN-TAP module.
 *
 * @author M. Boiziot
 */
public class EpnTapAction extends AbstractAction {

	private static final long serialVersionUID = -7839158802545761147L;
	private static final Logger LOGGER = LoggerFactory.getLogger(EpnTapAction.class);
	private static final String KEY_PATH_DOWNLOAD = "epntap-download";

	private final JFrame cassisFrame;
	private JFrame epnTapFrame;
	private MainPanelCtrl mainPanelCtrl;

	private JButton downloadButton;
	private JButton displayButton;
	private JButton openResultButton;


	/**
	 * Constructor.
	 *
	 * @param cassisFrame The owner frame.
	 */
	public EpnTapAction(JFrame cassisFrame) {
		super("EPN-TAP");
		this.cassisFrame = cassisFrame;
	}

	/**
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		getEpnTapFrame().setVisible(true);
		ScreenUtil.center(this.cassisFrame, getEpnTapFrame());
	}

	/**
	 * Create if necessary then return the EPN-TAP JFrame.
	 *
	 * @return the EPN-TAP JFrame.
	 */
	public JFrame getEpnTapFrame() {
		if (epnTapFrame == null) {
			epnTapFrame = new JFrame("EPN-TAP");
			epnTapFrame.setContentPane(getMainPanelCtrl().getView());
			epnTapFrame.setSize(1000, 700);
		}
		return epnTapFrame;
	}

	/**
	 * Create if necessary the EPN-TAP controller and customize it for CASSIS
	 *  needs, then return it.
	 *
	 * @return the EPN-TAP controller
	 */
	public MainPanelCtrl getMainPanelCtrl() {
		if (mainPanelCtrl == null) {
			mainPanelCtrl = new MainPanelCtrl();
			configureModule();
			mainPanelCtrl.acquireServices();
		}
		return mainPanelCtrl;
	}

	/**
	 * Configure the Epn-TAP module.
	 */
	private void configureModule() {
		configureSettings();
		configureUi();
	}

	/**
	 * Configure the settings of the Epn-TAP module for CASSIS needs.
	 */
	private void configureSettings() {
		mainPanelCtrl.getRequestCtrl().setNbMaxResult(20);

		mainPanelCtrl.getRequestCtrl().getColumnNames().clear();
		mainPanelCtrl.getRequestCtrl().getColumnNames().add("*");

		mainPanelCtrl.getView().getRequestPanel().getDataProductTypeField().
				getComboBox().setSelectedItem(DataProductTypeField.DataProductType.SP);
	}

	/**
	 * Configure the user interface of the Epn-TAP module.
	 */
	private void configureUi() {
		JPanel buttonsPanel = new JPanel();
		buttonsPanel.add(getDownloadButton());
		buttonsPanel.add(getDisplayButton());
		buttonsPanel.add(getOpenResultButton());

		mainPanelCtrl.getView().getStatusBarPanelView().add(buttonsPanel, BorderLayout.EAST);
	}

	/**
	 * Create if needed then return the Download {@link JButton}.
	 *
	 * @return the Download button.
	 */
	public JButton getDownloadButton() {
		if (downloadButton == null) {
			downloadButton = new JButton("Download...");
			downloadButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					JPopupMenu menu = new JPopupMenu();
					JMenuItem selectedItem = new JMenuItem("Selected");
					JMenuItem allItem = new JMenuItem("All");

					menu.add(selectedItem);
					menu.add(allItem);
					addDownloadListeners(selectedItem, allItem);

					menu.show(downloadButton, 0, downloadButton.getHeight());
				}
			});
		}
		return downloadButton;
	}

	/**
	 * Create if needed then return the Display {@link JButton}.
	 *
	 * @return the Display button.
	 */
	public JButton getDisplayButton() {
		if (displayButton == null) {
			displayButton = new JButton("Display...");
			displayButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					JPopupMenu menu = new JPopupMenu();
					JMenuItem selectedItem = new JMenuItem("Selected");
					JMenuItem allItem = new JMenuItem("All");

					menu.add(selectedItem);
					menu.add(allItem);
					addDisplayListeners(selectedItem, allItem);

					menu.show(displayButton, 0, displayButton.getHeight());
				}
			});
		}
		return displayButton;
	}

	/**
	 * Create if needed then return the Open result {@link JButton}.
	 *
	 * @return the Open result button.
	 */
	public JButton getOpenResultButton() {
		if (openResultButton == null) {
			openResultButton = new JButton("Open result");
			openResultButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					openResult();
				}
			});
		}
		return openResultButton;
	}

	/**
	 * Add listener on download {@link JMenuItem}s.
	 *
	 * @param selectedItem The Selected item.
	 * @param allItem The All item.
	 */
	private void addDownloadListeners(JMenuItem selectedItem, JMenuItem allItem) {
		selectedItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				downloadSelected();
			}
		});
		allItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				downloadAll();
			}
		});
	}

	/**
	 * Add listener on display {@link JMenuItem}s.
	 *
	 * @param selectedItem The Selected item.
	 * @param allItem The All item.
	 */
	private void addDisplayListeners(JMenuItem selectedItem, JMenuItem allItem) {
		selectedItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				displaySelected();
			}
		});
		allItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				displayAll();
			}
		});
	}

	/**
	 * Display the selected spectra.
	 */
	public void displaySelected() {
		if (checkResult()) {
			OpenAction.openVotable(getFocusedVotablePath(), getSelectedRows(), true);
		}
	}

	/**
	 * Display all spectra from result.
	 */
	public void displayAll() {
		if (checkResult()) {
			OpenAction.openVotable(getFocusedVotablePath(), true);
		}
	}

	/**
	 * Download the selected spectra.
	 */
	public void downloadSelected() {
		List<String> urls = getUrls(true);
		download(urls);
	}

	/**
	 * Download all spectra from result.
	 */
	public void downloadAll() {
		List<String> urls = getUrls(false);
		download(urls);
	}

	/**
	 * Open the result in Spectrum Manager.
	 */
	public void openResult() {
		if (checkResult()) {
			OpenAction.openVotable(getFocusedVotablePath(), false);
		}
	}

	/**
	 * Return the index of the selected rows (starting at 0).
	 *
	 * @return the index of the selected rows (starting at 0).
	 */
	private List<Integer> getSelectedRows() {
		return mainPanelCtrl.getResultsCtrl().getView().getSelectedRows();
	}

	/**
	 * Check then return if the column "native_access_url" is in the columns list
	 *  of the EPN-TAP result.
	 *
	 * @return true if the result contains the column "native_access_url", false otherwise.
	 */
	private boolean containsNativeAccessUrl() {
		boolean res = false;
		if (haveResult()) {
			String[] columns = getSelectedVOTableData().getColumnsName();
			for (String column : columns) {
				if ("native_access_url".equals(column)) {
					res = true;
					break;
				}
			}
		}
		return res;
	}

	/**
	 * Return the elements from objectsList at the given indexList. Also, return
	 *  them as List of String instead of Object.
	 *
	 * @param objectsList The elements.
	 * @param indexList The indexes.
	 * @return the elements from objectsList at the given indexList.
	 */
	private List<String> getElements(List<Object> objectsList, List<Integer> indexList) {
		List<String> l = new ArrayList<>(indexList.size());
		for (int idx : indexList) {
			l.add((String) objectsList.get(idx));
		}
		return l;
	}

	/**
	 * Return the URLs to use.
	 *
	 * @param limitToSelection true to limit to the current selection, false to
	 *  get all URLs.
	 * @return The list of URLs or null if there was an error.
	 */
	private List<String> getUrls(boolean limitToSelection) {
		if (!checkResult()) {
			return null;
		}
		List<Integer> selected = getSelectedRows();
		if (limitToSelection && getSelectedRows().isEmpty()) {
			return null;
		}
		String urlColumnName = getUrlColumnName(limitToSelection);
		if (urlColumnName == null) {
			return null;
		}
		List<Object> rows;
		try {
			rows = getSelectedVOTableData().getColumn(urlColumnName);
		} catch (CanNotParseDataException e) {
			LOGGER.error("Error while parsing the result", e);
			JOptionPane.showMessageDialog(getEpnTapFrame(), "Error while parsing the result",
					"EPN-TAP error", JOptionPane.ERROR_MESSAGE);
			return null;
		}

		if (limitToSelection) {
			return getElements(rows, selected);
		} else {
			return asStringList(rows);
		}
	}
	/**
	 * Return the name of the column to get the URL from. In case there is a
	 *  column with the name "native_access_url" and
	 *  the value of the column with the name "native_access_format" is not jpg or ascii
	 *  we use this one, or ask the
	 *  user for the column to use.
	 * @param limitToSelection true to limit to the current selection, false to
	 *  search in all the rows.
	 * @return the name of the column to get the URL from.
	 */

	public String getUrlColumnName(boolean limitToSelection) {
		String result = null;
		if (haveResult()) {
			if (containsNativeAccessUrl()){
				String format = null;
				try {
					int indiceSelected =  0;
					if (limitToSelection) {
						indiceSelected = getSelectedRows().get(0);
					}
					format = (String)(getSelectedVOTableData().getColumn("native_access_format").get(indiceSelected));
				} catch (CanNotParseDataException e) {
					LOGGER.error("Error while parsing the result", e);
				}
				if (format != null && !"jpg".equals(format) &&
						!"ascii".equals(format)){
					result =  "native_access_url";
				}
			}
			if (result == null){
				String[] columns = getSelectedVOTableData().getColumnsName();
				JComboBox<String> columnsCombobox = new JComboBox<>(columns);
				int res = JOptionPane.showOptionDialog(getEpnTapFrame(), columnsCombobox,
						"Please select the columns containing the URL",
						JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,
						null, null, null);
				if (res == JOptionPane.OK_OPTION) {
					result =  (String) columnsCombobox.getSelectedItem();
				}
			}
		}
		return result;
	}

	/**
	 * Convert a List of Object as a List of String.
	 *
	 * @param objs The List of Object.
	 * @return The List of String.
	 */
	private List<String> asStringList(List<Object> objs) {
		List<String> res = new ArrayList<>(objs.size());
		for (Object obj : objs) {
			res.add((String) obj);
		}
		return res;
	}

	/**
	 * Ask for the user to choose a folder.
	 *
	 * @param uniqueFile true in case of only one file, false otherwise.
	 * @return the path, or null in case of error or if the user cancelled the operation.
	 */
	private String askWhereToSave(boolean uniqueFile) {
		JFileChooser fileChooser = new CassisJFileChooser(Software.getDataPath(),
				Software.getLastFolder(KEY_PATH_DOWNLOAD));
		String title = uniqueFile ?
				"Please select a folder to save the file."
				: "Please select a folder to save the files.";

		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fileChooser.setDialogTitle(title);
		fileChooser.setDialogType(JFileChooser.SAVE_DIALOG);

		String path;
		if (fileChooser.showSaveDialog(getEpnTapFrame()) == JFileChooser.APPROVE_OPTION) {
			Software.setLastFolder(KEY_PATH_DOWNLOAD,
					fileChooser.getSelectedFile().getAbsolutePath());
			path = fileChooser.getSelectedFile().getAbsolutePath();
		} else {
			path = null;
		}

		return path;
	}

	/**
	 * Download the files with the given URLs.
	 *
	 * @param urls The list of URL of the files to download.
	 */
	private void download(List<String> urls) {
		if (urls == null || urls.isEmpty()) {
			return;
		}

		String path = askWhereToSave(urls.size() == 1);
		if (path == null) {
			return;
		}

		DownloadMultiGui dmg = new DownloadMultiGui();
		dmg.setVisible(true);
		dmg.startDownload(urls, path);
	}

	/**
	 * Return if the selected table have result. Warning: check if there is
	 *  result with {@link #haveTabResult()} before this to avoid error.
	 *
	 * @return true if there is result on current selected table, false otherwise.
	 */
	private boolean haveResult() {
		return getSelectedVOTableData() != null && getSelectedVOTableData().getNbRows() >= 1;
	}

	/**
	 * Return the VOTableData associated with the selected JTable.
	 *
	 * @return the VOTableData associated with the selected JTable.
	 */
	private VOTableData getSelectedVOTableData() {
		return mainPanelCtrl.getResultsCtrl().getView().getCurrentTablePanel().getVOTableData();
	}

	/**
	 * Return if there is a least a result tab.
	 *
	 * @return true if there is a least a result tab, false otherwise.
	 */
	private boolean haveTabResult() {
		return mainPanelCtrl.getResultsCtrl().getView().getTabCount() >= 1;
	}

	/**
	 * Check if there is result, display a Dialog if there is not.
	 *
	 * @return true if there is result, false otherwise.
	 */
	private boolean checkResult() {
		if (!(haveTabResult() && haveResult())) {
			JOptionPane.showMessageDialog(epnTapFrame, "No data to process.",
					"No data yet!", JOptionPane.INFORMATION_MESSAGE);
			return false;
		}
		return true;
	}

	/**
	 * Return the path of the file of focused VOtable.
	 *
	 * @return the path of the file of focused VOtable.
	 */
	private String getFocusedVotablePath() {
		return mainPanelCtrl.getResultsCtrl().getFocusedVOTablePath();
	}
}
