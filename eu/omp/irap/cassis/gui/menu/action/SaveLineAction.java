/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.menu.action;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.database.access.MoleculeDescriptionDB;
import eu.omp.irap.cassis.gui.plot.CassisViewInterface;
import eu.omp.irap.cassis.properties.Software;

/**
 * @author glorian
 *
 */
@SuppressWarnings("serial")
public class SaveLineAction extends AbstractAction {

	private static final Logger LOGGER = LoggerFactory.getLogger(SaveLineAction.class);

	private CassisViewInterface cassisView;
	private Component owner;


	public SaveLineAction(Component owner, CassisViewInterface fullSpectrumView) {
		super("Save lines");
		cassisView = fullSpectrumView;
		this.owner = owner;
	}

	/**
	 * @param cassisView
	 *            the cassisView to set
	 */
	public final void setCassisView(CassisViewInterface cassisView) {
		this.cassisView = cassisView;
	}

	/**
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		List<LineDescription> lines = cassisView.getSyntheticLineVisible();
		List<LineDescription> otherSpecieslines = cassisView.getOtherSpeciesLineVisible();
		lines.addAll(otherSpecieslines);
		if (lines == null || lines.isEmpty() ) {
			JOptionPane.showMessageDialog(owner, "No line to save", "Alert", JOptionPane.WARNING_MESSAGE);
			return;
		}
		JFileChooser fc = new CassisJFileChooser(Software.getDataPath(),
				Software.getLastFolder("data"));

		int returnVal = fc.showSaveDialog(owner);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			Software.setLastFolder("data", fc.getSelectedFile().getParent());
			File file;
			String filename = fc.getSelectedFile().getAbsolutePath();
			if (!filename.contains(".")) {
				file = new File(filename + ".txt");
			}
			else {
				file = new File(filename);
			}
			// replace the existing file or not
			if (file.exists()) {
				String message = "This file " + file.toString() + " already exists.\n" + "Do you want to replace it?";
				// Modal dialog with yes/no button
				int result = JOptionPane.showConfirmDialog(owner, message, "Replace existing file?",
						JOptionPane.YES_NO_OPTION);
				if (result == JOptionPane.NO_OPTION)
					return;
			}
			if (!saveLines(file, lines))
				JOptionPane.showMessageDialog(owner, "Can't save the file", "Alert", JOptionPane.WARNING_MESSAGE);
		}
	}

	private boolean saveLines(File file, List<LineDescription> lines) {
		DecimalFormatSymbols ds = new DecimalFormatSymbols();
		ds.setDecimalSeparator('.');

		DecimalFormat freqFormat = new DecimalFormat("######0.000", ds);
		DecimalFormat intensityFormat = new DecimalFormat("0.00E0", ds);
		DecimalFormat eupFormat = new DecimalFormat("0.###", ds);
		DecimalFormat aijFormat = new DecimalFormat("0.00E0", ds);
		DecimalFormat tauFormat = new DecimalFormat("0.000E0", ds);
		DecimalFormat texFormat = new DecimalFormat("#####.##", ds);

		try (BufferedWriter fileAscii = new BufferedWriter(new FileWriter(file))){
			fileAscii.write("Transition\tTag\tFrequency(Ghz)\tEup(K)\tAij\tTau\tTex\tIntensity(K)");
			fileAscii.flush();
			fileAscii.newLine();
			List<String> citations = new ArrayList<>();

			for (LineDescription line : lines) {
				String citation2 = line.getCitation();
				if (!citation2.isEmpty() && !citations.contains(line.getCitation())){
					citations.add(citation2);
				}
				fileAscii.write(line.getMolName() + " " + MoleculeDescriptionDB.constitueLine(line.getQuanticN()) + " "
						+ "\t");
				fileAscii.write(line.getMolTag() + "\t");
				fileAscii.write(freqFormat.format(line.getObsFrequency()) + "\t");
				fileAscii.write(eupFormat.format(line.getEUpK()) + "\t");
				fileAscii.write(aijFormat.format(line.getAij()) + "\t");
				fileAscii.write(tauFormat.format(line.getTau()) + "\t");
				fileAscii.write(texFormat.format(line.getTex()) + "\t");
				// TODO replace with max intensity of the channel
				fileAscii.write(intensityFormat.format(line.getChannelFlux())
						+ "\t");
				fileAscii.flush();
				fileAscii.newLine();
			}
			fileAscii.flush();
			if (!citations.isEmpty()){
				String absolutePath = file.getAbsolutePath();
				String[] split = absolutePath.split("[.]");
				String fileCitation = split[0]+ "-VAMDC-Citation." +split[1];
				try (BufferedWriter fileAscii2 = new BufferedWriter(new FileWriter(fileCitation))){
					for (String citation : citations) {
						fileAscii2.write(citation);
						fileAscii2.flush();
						fileAscii2.newLine();
					}

				} catch (IOException e) {
					LOGGER.error("Error while trying to save the file.", e);
					return false;
				}
			}
			return true;
		} catch (IOException e) {
			LOGGER.error("Error while trying to save the file.", e);
			return false;
		}
	}
}
