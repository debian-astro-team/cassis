/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.menu.action;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.AbstractAction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.properties.Software;

/**
 * @author glorian
 *
 */
@SuppressWarnings("serial")
public class HelpAction extends AbstractAction {

	private static final Logger LOGGER = LoggerFactory.getLogger(HelpAction.class);
	private static final String CASSIS_HELP_URL_ONLINE = "http://cassis.irap.omp.eu/help/";


	public HelpAction() {
		super("User's manual");

	}


	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			if (haveHelpOnlineConnection()) {
				displayUrlHelp(CASSIS_HELP_URL_ONLINE);
			} else {
				String url = Software.getHelpPath() + File.separator;
				Path p1 = Paths.get(url + "index.html");
				displayUrlHelp(p1.toUri().toString());
			}
		} catch (IOException | URISyntaxException exc) {
			LOGGER.error("Cannot display the HELP", exc.getMessage());
		}
	}

	public static void displayUrlHelp(String url) throws IOException, URISyntaxException {
		if (Desktop.isDesktopSupported()) {
			Desktop desktop = Desktop.getDesktop();
			if (desktop.isSupported(Desktop.Action.BROWSE)) {
				desktop.browse(new URI(url));
			}
		}
	}

	/**
	 * Try to open a connection to cassis help website then return if is was sucessfull.
	 *
	 * @return if the connection to cassis help website was sucessfull.
	 */
	private static boolean haveHelpOnlineConnection() {
		boolean connection = true;
		try {

			URL url = new URL(CASSIS_HELP_URL_ONLINE);
			URLConnection con = url.openConnection();
			con.connect();
		} catch (Exception e) {
			LOGGER.trace("Exception while trying to connect to CASSIS site, no connection detected", e);
			connection = false;
		}
		return connection;
	}
}
