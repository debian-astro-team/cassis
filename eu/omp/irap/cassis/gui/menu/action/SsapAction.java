/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.menu.action;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.gui.ScreenUtil;
import eu.omp.irap.ssap.SsapControl;
import eu.omp.irap.ssap.request.RequestDisplay;
import eu.omp.irap.ssap.request.RequestEvent;
import eu.omp.irap.ssap.request.RequestListener;


/**
 * Action related to the Simple Spectral Acces Protocol (SSAP) module.
 *
 * @author M. Boiziot
 */
public class SsapAction extends AbstractAction {

	private static final Logger LOGGER = LoggerFactory.getLogger(SsapAction.class);
	private static final long serialVersionUID = 1;
	private SsapControl ssapControl;
	private JFrame ssapFrame;
	private final JFrame cassisFrame;


	/**
	 * Constructor.
	 *
	 * @param cassisFrame The owner frame.
	 */
	public SsapAction(JFrame cassisFrame) {
		super("SSA Query");
		this.cassisFrame = cassisFrame;
	}

	/**
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		getSsapFrame().setVisible(true);
		ScreenUtil.center(this.cassisFrame, getSsapFrame());
	}

	/**
	 * Create if necessary then return the SsapControl.
	 *
	 * @return the ssap control.
	 */
	private SsapControl getSsapControl() {
		if (ssapControl == null) {
			ssapControl = new SsapControl();
			ssapControl.getModel().addVotableListener(new VoTableListener());
		}
		return ssapControl;
	}

	/**
	 * Create if necessary then return the SSA JFrame.
	 *
	 * @return the Ssap JFrame.
	 */
	private JFrame getSsapFrame() {
		if (ssapFrame == null) {
			ssapFrame = new JFrame("Simple Spectral Access (SSA)");
			ssapFrame.setContentPane(getSsapControl().getView());
			ssapFrame.pack();
		}
		return ssapFrame;
	}



	/**
	 * RequestListener specifically implemented for VOTable in CASSIS.
	 *
	 * @author M. Boiziot
	 */
	private class VoTableListener implements RequestListener {

		/**
		 * Handle VOTable event result
		 *
		 * @param requestEvent The event
		 * @see eu.omp.irap.ssap.request.RequestListener#newRequestResult(eu.omp.irap.ssap.request.RequestEvent)
		 */
		@Override
		public void newRequestResult(RequestEvent requestEvent) {
			final List<Integer> idx = convertToListOfInteger(requestEvent.getResult());
			final String requestDisplay = requestEvent.getName();
			final String votableContent = requestEvent.getVotable();
			final String votable = writeToTempFile(votableContent);
			if (votable == null) {
				return;
			}

			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					switch (requestDisplay) {
					case RequestDisplay.CHOICE_DISPLAY:
						OpenAction.openVotable(votable, idx, false);
						break;

					case RequestDisplay.DIRECT_DISPLAY:
					default:
						OpenAction.openVotable(votable, idx, true);
						break;
					}
				}
			});
		}

		/**
		 * Convert a List of Object (of true type Integer) to a List of Integer.
		 *
		 * @param objs The List to convert.
		 * @return The converted List.
		 */
		private List<Integer> convertToListOfInteger(List<Object> objs) {
			List<Integer> integerList = new ArrayList<>(objs.size());
			for (Object obj : objs) {
				integerList.add((Integer)obj);
			}
			return integerList;
		}

		/**
		 * Write the given content to a temporary file then return the path of the file.
		 *
		 * @param content The content to write.
		 * @return The path of the created file.
		 */
		private String writeToTempFile(String content) {
			String path = null;
			try {
				File f = File.createTempFile("cassisSSAP", ".votable");
				path = f.getAbsolutePath();
				try (FileWriter fw = new FileWriter(f)) {
					fw.write(content);
					fw.flush();
				}
			} catch (IOException e) {
				LOGGER.error("Error while writing VOTable file from SSAP");
			}
			return path;
		}
	}
}
