/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.menu.action;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.file.EXTENSION;
import eu.omp.irap.cassis.gui.plot.CassisViewInterface;
import eu.omp.irap.cassis.gui.plot.save.CassisSaveViewInterface;
import eu.omp.irap.cassis.properties.Software;

/**
 * @author glorian
 *
 */
@SuppressWarnings("serial")
public class SaveAction extends AbstractAction {

	private static final Logger LOGGER = LoggerFactory.getLogger(SaveAction.class);
	private Component mainFrame;
	private CassisViewInterface cassisView;


	public SaveAction(Component mainFrame, CassisViewInterface cassisView) {
		super("Save");
		this.mainFrame = mainFrame;
		this.cassisView = cassisView;
	}

	/**
	 * @param cassisView
	 *            the cassisView_ to set
	 */
	public final void setCassisView(CassisViewInterface cassisView) {
		this.cassisView = cassisView;
	}

	/**
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		savePlot();
	}

	public void savePlot() {
		CassisSaveViewInterface save = cassisView.getSave();
		JFileChooser fc = save.getfc();
		if (fc == null) {
			JOptionPane.showMessageDialog(mainFrame, "No data to save!", "Alert", JOptionPane.WARNING_MESSAGE);
			return;
		}

		int returnVal = fc.showSaveDialog(mainFrame);

		String ext = null;
		if (fc.getFileFilter() instanceof FileNameExtensionFilter) {
			String[] extensions = ((FileNameExtensionFilter)fc.getFileFilter()).getExtensions();
			if (extensions.length >= 1) {
				ext = "." + extensions[0];
			}
		}

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			Software.setLastFolder("data", fc.getSelectedFile().getParent());
			File file;
			String filename = fc.getSelectedFile().getAbsolutePath();
			if (filename.contains(".")) {
				if (ext == null)
					ext = filename.substring(filename.lastIndexOf('.'));

				if (!ext.equals(filename.substring(filename.lastIndexOf('.'))))
					file = new File(filename + ext);
				else
					file = new File(filename);
			}
			else {
				if (ext == null) {
					JOptionPane.showMessageDialog(null, "You must select an extension for the file. File not saved.");
					 savePlot();
					 return;
				}
				file = new File(filename + ext);
			}

			if (!cassisView.checkIfSaveIsPossible(file, EXTENSION.getExtensionByExtensionName(ext)))
				return;

			try {
				savePlot(save, ext, file);
			} catch (Exception e) {
				LOGGER.error("Error during the save", e);
				JOptionPane.showMessageDialog(mainFrame, "Can't save the file",
						"Alert", JOptionPane.WARNING_MESSAGE);
			}
		}
	}

	public static boolean savePlot(CassisSaveViewInterface save, String ext, File file) {
		switch (ext) {
		case ".fus":
		case ".lis":
		case ".txt":
		case ".rotd":
			save.savePlotAsciiCassis(file);
			break;

		case ".fca":
			save.savePlotAscii(file);
			break;

		case ".fits":
			save.savePlotFits(file);
			break;

		case ".pdf":
			save.savePlotPdf(file);
			break;

		case ".png":
			save.savePlotPng(file);
			break;

		case ".xml":
			save.savePlotVOTable(file);
			break;

		case ".votable":
			save.savePlotVOTable(file);
			break;

		case ".spec":
			save.savePlotSpec(file);
			break;

		default:
			return false;
		}
		return true;
	}

}
