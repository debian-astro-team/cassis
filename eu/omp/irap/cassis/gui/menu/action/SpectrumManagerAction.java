/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.menu.action;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.xml_cml.schema.ArrayList;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.MultiScanCassisSpectrum;
import eu.omp.irap.cassis.common.gui.ScreenUtil;
import eu.omp.irap.cassis.file.gui.FilePanel;
import eu.omp.irap.cassis.file.gui.cassisspectrum.CassisSpectrumInfo;
import eu.omp.irap.cassis.file.gui.cassisspectrum.CassisSpectrumModel;
import eu.omp.irap.cassis.file.gui.cassisspectrum.CassisSpectrumPanel;
import eu.omp.irap.cassis.file.gui.cassisspectrum.SelectionMultiScanSpectrumDialog;
import eu.omp.irap.cassis.file.gui.cassisspectrum.VotableDownloadGui;
import eu.omp.irap.cassis.file.gui.cassisspectrum.VotableWithUrl;
import eu.omp.irap.cassis.file.gui.medatada.CASSIS_METADATA;
import eu.omp.irap.cassis.file.votable.VotableType;
import eu.omp.irap.cassis.gui.PanelFrame;
import eu.omp.irap.cassis.gui.PanelView;
import eu.omp.irap.cassis.gui.model.spectrumanalysis.SpectrumAnalysisControl;
import eu.omp.irap.cassis.gui.util.ListenersUtil;
import uk.ac.starlink.table.TableFormatException;

/**
 * Simple class to handle Spectrum Manager in CASSIS.
 *
 * @author M. Boiziot
 */
public class SpectrumManagerAction extends AbstractAction {

	private static final long serialVersionUID = -1499797130144542190L;
	private static final Logger LOGGER = LoggerFactory.getLogger(SpectrumManagerAction.class);
	private static final String FRAME_NAME = "Spectrum Manager";
	private static SpectrumManagerAction instance;
	private JFrame spectrumManagerFrame;
	private FilePanel filePanel;
	private CassisSpectrumPanel cassisSpectrumPanel;
	private JButton displaySelectButton;
	private Runnable directDisplayAction;
	private boolean display;


	/**
	 * Private constructor, use {@link #getInstance()} to get a {@link SpectrumManagerAction} object.
	 */
	private SpectrumManagerAction() {
		super(FRAME_NAME);
		this.filePanel = new FilePanel();
		this.cassisSpectrumPanel = filePanel.getCassisSpectrumPanel();
		changeListenerForOpenLocalResourceButton();
		addDisplaySelectButton();
	}

	/**
	 * Create if necessary then return the instance of {@link SpectrumManagerAction}.
	 *
	 * @return the instance of {@link SpectrumManagerAction}.
	 */
	public static SpectrumManagerAction getInstance() {
		if (instance == null) {
			instance = new SpectrumManagerAction();
		}
		return instance;
	}

	public static void clearSpectrumManagerInstance() {
		instance = null;
	}

	/**
	 * Add the Display/Select spectrum button to the CassisSpectrumPanel.
	 */
	private void addDisplaySelectButton() {
		GridBagConstraints c = new GridBagConstraints();
		c.gridy = 4;
		c.ipady = 4;
		c.insets = new Insets(1, 0, 1, 0);
		c.fill = GridBagConstraints.BOTH;
		cassisSpectrumPanel.getButtonsPanel().add(getDisplaySelectButton(), c);
	}

	/**
	 * Create if necessary the Display/Select spectrum button then return it. This button
	 *  should have his text and action changed according to the modal status of the JFrame.
	 *
	 * @return the Display/Select button.
	 */
	private JButton getDisplaySelectButton() {
		if (displaySelectButton == null) {
			displaySelectButton = new JButton("Display spectrum");
			displaySelectButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					cassisSpectrumPanel.getControl().getAction().run();
				}
			});
		}
		return displaySelectButton;
	}

	/**
	 * Change the listener for the "Open local resource" button.
	 */
	private void changeListenerForOpenLocalResourceButton() {
		JButton button = cassisSpectrumPanel.getOpenLocalResourceButton();
		ListenersUtil.removeActionListeners(button);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PanelView.getInstance().getCassisActionMenu().getOpenAction().openJFileChooser(spectrumManagerFrame, display);
			}
		});
	}

	/**
	 * Create a Spectrum Manager frame.
	 */
	private void createFrame() {
		if (spectrumManagerFrame == null) {
			spectrumManagerFrame = new JFrame(FRAME_NAME);
			filePanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
			spectrumManagerFrame.setContentPane(this.filePanel);
			spectrumManagerFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			spectrumManagerFrame.setPreferredSize(new Dimension(800, 550));
			spectrumManagerFrame.setMinimumSize(new Dimension(800, 550));
			spectrumManagerFrame.pack();
			ScreenUtil.center(PanelFrame.getInstance(), spectrumManagerFrame);
		}
	}

	/**
	 * Open the window as a JFrame, add a Runnable to execute on click of the
	 *  Select button then make the JFrame visible.
	 *
	 * @param cmd The Runnable to execute once the spectrum is selected.
	 * @param title The title of the frame for which the spectrum will be send.
	 */
	public void setLoadBinding(Runnable cmd, String title) {
		display = false;
		getDisplaySelectButton().setText("Select spectrum");
		cassisSpectrumPanel.getControl().setParsingActionText("Select spectrum");
		getDisplaySelectButton().setForeground(Color.GREEN.darker().darker());
		cassisSpectrumPanel.getControl().setAction(cmd);
		createFrame();
		this.spectrumManagerFrame.setTitle(FRAME_NAME + " (" + title + ")");
		this.spectrumManagerFrame.setVisible(true);
	}

	/**
	 * Open the window as a simple JFrame.
	 */
	public void setNormal() {
		display = true;
		getDisplaySelectButton().setText("Display spectrum");
		cassisSpectrumPanel.getControl().setParsingActionText("Display spectrum");
		getDisplaySelectButton().setForeground(null);
		cassisSpectrumPanel.getControl().setAction(getDirectDisplayAction());
		createFrame();
		this.spectrumManagerFrame.setTitle(FRAME_NAME);
		this.spectrumManagerFrame.setVisible(true);
	}

	/**
	 * Add an array of files to the spectrum manager module.
	 *
	 * @param files The array of files to add.
	 */
	public void addRessource(File[] files) {
		cassisSpectrumPanel.getControl().addResourceToJTree(files);
	}

	/**
	 * Add an array of files to the spectrum manager module.
	 *
	 * @param file The file to add.
	 * @param params the Map representing the metadatas list
	 */
	public void addRessourceWithAdditional(File file, Map<String, String> params) {
		cassisSpectrumPanel.getControl().addResourceToJTree(file, params);
	}

	/**
	 * Add a file to the Spectrum manager module.
	 *
	 * @param file The file to add.
	 * @param displayAlreadyOpened True to display message about file already
	 *  open if necessary, false to do not display it.
	 */
	public void addRessource(File file, boolean displayAlreadyOpened) {
		cassisSpectrumPanel.getControl().addResourceToJTree(file, displayAlreadyOpened);
	}

	/**
	 * Add a votable file to the Spectrum manager module according to index of spectra
	 *
	 * @param pathFileVotable The path of votable file to add
	 * @param indexListSelected The index of the spectra to use
	 */
	public void addVotable(String pathFileVotable, List<Integer> indexListSelected) {
		File file = new File(pathFileVotable);
		cassisSpectrumPanel.getControl().addVotableWithUrlToJTree(file, indexListSelected);
	}

	/**
	 * Add a votable file to the Spectrum manager module
	 *
	 * @param pathFileVotable The path of votable file to add
	 */
	public void addVotable(String pathFileVotable) {
		File file = new File(pathFileVotable);
		cassisSpectrumPanel.getControl().addVotableWithUrlToJTree(file);
	}

	/**
	 * Download in Spectrum Manager and display in Cassis, spectra according to index list
	 *
	 * @param pathFileVotable The path of votable file
	 * @param indexListSelected The index of spectra to use
	 * @param display true to display, false to do not display
	 */
	public void downloadAndDisplayVotable(String pathFileVotable, List<Integer> indexListSelected, boolean display) {
		try {
			VotableWithUrl votableWithUrl = new VotableWithUrl(new File(pathFileVotable), indexListSelected);
			downloadAndDisplayVotable(votableWithUrl, indexListSelected, display);
		} catch (TableFormatException e) {
			LOGGER.error("VOTable incorrectly formated", e);
		} catch (IOException e) {
			LOGGER.error("IO error", e);
		}
	}

	/**
	 * Download in Spectrum Manager and display in Cassis, spectra according to index list
	 *
	 * @param votableWithUrl The VotableWithUrl object
	 * @param indexListSelected The index of spectra to use
	 * @param display true to display, false to do not display
	 */
	public void downloadAndDisplayVotable(VotableWithUrl votableWithUrl, List<Integer> indexListSelected, final boolean display) {
		List<DefaultMutableTreeNode> votableWithUrlNodeList = cassisSpectrumPanel.getModel().getVotableWithUrlNodeList();
		DefaultMutableTreeNode correspondingNode = null;
		for (DefaultMutableTreeNode node : votableWithUrlNodeList) {
			VotableWithUrl votableWithUrlJTree = (VotableWithUrl) node.getUserObject();
			if (votableWithUrlJTree.getName().equals(votableWithUrl.getName())) {
				// Recovery of node and VotableWithUrl corresponding to the path of votable file
				correspondingNode = node;
				votableWithUrl = votableWithUrlJTree;
				break;
			}
		}
		final VotableWithUrl finalVotableWithUrl = votableWithUrl; // The VotableWithUrl object corresponding to the path of votable file
		final DefaultMutableTreeNode node = correspondingNode; // The node containing the VotableWithUrl on the JTree
		for (final int index : indexListSelected) {
			if (VotableWithUrl.NOT_ACCESS_URL.equals(finalVotableWithUrl.getAccessUrl()) ||
					finalVotableWithUrl.getType() == VotableType.SPECTRUM) {
				// The user probably did not choose the field to use for the URL for downloading the files.
				// We can not do anything...
				continue;
			} else if (finalVotableWithUrl.getFilesDownloadState()[index] == VotableWithUrl.NOT_DOWNLOADED) {
				// Start the download
				final VotableDownloadGui votableDownloadGui = new VotableDownloadGui(votableWithUrl, index);
				Runnable r = new Runnable() {
					@Override
					public void run() {
						if (finalVotableWithUrl.getFilesDownloadState()[index] == VotableWithUrl.DOWNLOADED) {
							CassisSpectrumInfo cassisSpectrumInfo = finalVotableWithUrl.getCassisSpectrumInfoTab()[index];
							if (cassisSpectrumInfo == null) {
								Component comp = isSpectrumManagerFrameVisible() ?
										spectrumManagerFrame : PanelFrame.getInstance();
								JOptionPane.showMessageDialog(comp,
										"Unable to open this spectrum",
										"Reading error", JOptionPane.ERROR_MESSAGE);
								LOGGER.error("Unable to open this spectrum.");
								return;
							}
							cassisSpectrumInfo.setPath(votableDownloadGui.getLocalPathFile());
							DefaultMutableTreeNode childNode = null;
							for (int i = 0; i < node.getChildCount(); i++) {
								childNode = (DefaultMutableTreeNode) node.getChildAt(i);
								CassisSpectrumInfo cassisSpectrumInfo2 = (CassisSpectrumInfo) (childNode.getUserObject());
								if (cassisSpectrumInfo2.getName().equals(cassisSpectrumInfo.getName())) {
									break;
								}
							}
							cassisSpectrumPanel.getModel().notifyAddSpectra(childNode);
							if (childNode.equals(cassisSpectrumPanel.getTree().getLastSelectedPathComponent())) {
								cassisSpectrumPanel.getModel().notifyCassisSpectrumInfoSelected(childNode);
								cassisSpectrumPanel.downloadFileSelected(childNode);
								cassisSpectrumPanel.getTree().expandPath(new TreePath(childNode.getPath()));
							}
							if (display) {
								// Display all cassisSpectrum in Cassis
								CassisSpectrum cassisSpectrum = CassisSpectrum.mergeCassisSpectrumList(cassisSpectrumInfo.getCassisSpectrumList());
								displayDirectly(cassisSpectrum);
							}
						}
					}
				};
				votableDownloadGui.setFinishAction(r);
			} else if (finalVotableWithUrl.getFilesDownloadState()[index] == VotableWithUrl.DOWNLOADED && display) {
				CassisSpectrumInfo cassisSpectrumInfo = finalVotableWithUrl.getCassisSpectrumInfoTab()[index];
				CassisSpectrum cassisSpectrum = CassisSpectrum.mergeCassisSpectrumList(cassisSpectrumInfo.getCassisSpectrumList());
				displayDirectly(cassisSpectrum);
			}
		}
	}

	/**
	 * Download in Spectrum Manager and display in Cassis directly all the spectra in votable file
	 *
	 * @param pathFileVotable
	 *            The path of votable file
	 */
	public void downloadAndDisplayVotable(String pathFileVotable) {
		try {
			VotableWithUrl votableWithUrl = new VotableWithUrl(new File(pathFileVotable));
			downloadAndDisplayVotable(pathFileVotable, votableWithUrl.getUrlIndexList(), true);
		} catch (IOException e) {
			LOGGER.error("Error while downloading and displaying all spectra in VOTable file {}",
					pathFileVotable, e);
		}
	}

	/**
	 * Display last spectrum opened.
	 */
	public void displayLastSpectrum() {
		getDirectDisplayAction().run();
	}

	/**
	 * Create a Runnable describing the action to execute to directly display a
	 *  selected element in the CassisSpectrumPanel {@link JTree}.
	 *
	 * @return The Runnable with the action to execute a to directly display a
	 *  selected element in the CassisSpectrumPanel {@link JTree}.
	 */
	private Runnable getDirectDisplayAction() {
		if (directDisplayAction == null) {
			directDisplayAction = new Runnable() {
				@Override
				public void run() {
					CassisSpectrum cs = getSelectedSpectrum();
					if (cs != null) {
						final CassisSpectrum[] cassisSpectrums = new CassisSpectrum[]{cs};
						filePanel.getControl().checkUnitsErrors(Arrays.asList(cassisSpectrums));
						displayDirectly(cs);
					}
				}
			};
		}
		return directDisplayAction;
	}

	/**
	 * Directly display in spectrum analysis the given spectrum.
	 *
	 * @param spectrum
	 *            The spectrum to display.
	 */
	private void displayDirectly(CassisSpectrum spectrum) {
		if (spectrum == null) {
			LOGGER.info("Spectrum is null. Unable to display it.");
			return;
		}
		SpectrumAnalysisControl control = PanelView.getInstance().getCassisActionMenu()
				.getSpectrumAnalysisAction().getSpectrumAnalysisAction();
		control.getModel().getLoadDataModel().setNameData(
				SpectrumManagerAction.getInstance().getPathOrName(spectrum));
		control.getModel().getLoadDataModel().setCassisSpectrum(spectrum);
		control.onDisplayButtonClicked();
	}

	/**
	 * Open normal Spectrum Manager JFrame.
	 *
	 * @param e
	 *            unused here.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		SpectrumManagerAction.getInstance().setNormal();
	}

	/**
	 * Return the selected spectrum on the Spectrum Manager {@link JTree}.
	 *
	 * @return the selected spectrum on the Spectrum Manager {@link JTree}.
	 */
	public CassisSpectrum getSelectedSpectrum() {
		CassisSpectrum cassisSpectrum = null;
		Object selectedObject = cassisSpectrumPanel.getSelected();
		switch (cassisSpectrumPanel.getTypeElementSelected()) {
		case CASSIS_SPECTRUM:
			cassisSpectrum = (CassisSpectrum) selectedObject;
			if (cassisSpectrum instanceof MultiScanCassisSpectrum) {
				cassisSpectrum = handleSubScan((MultiScanCassisSpectrum) cassisSpectrum);
			}
			break;
		case DOWNLOAD_FILE:
		case CASSIS_SPECTRUM_INFO:
			CassisSpectrumInfo cassisSpectrumInfo = (CassisSpectrumInfo) selectedObject;
			if (cassisSpectrumInfo.getCassisSpectrumList().size()==1 &&
				cassisSpectrumInfo.getCassisSpectrumList().get(0) instanceof MultiScanCassisSpectrum){
				cassisSpectrum = handleSubScan((MultiScanCassisSpectrum) cassisSpectrumInfo.getCassisSpectrumList().get(0));
			}else{
				cassisSpectrum = CassisSpectrum.mergeCassisSpectrumList(cassisSpectrumInfo.getCassisSpectrumList());
			}
			break;
		default:
			LOGGER.warn("Error: nothing selected or not a spectrum");
			break;
		}
		CassisSpectrum cassisSpectrumClone = cassisSpectrum;
		if (cassisSpectrum == null) {
			return null;
		}
		try {
			cassisSpectrumClone = cassisSpectrum.clone();
		} catch (CloneNotSupportedException e) {
			LOGGER.error("Clone error on CassisSpectrum", e);
		}
		return cassisSpectrumClone;
	}

	/**
	 * Create and display the Spectrum Manager in "normal" mode only if not existing.
	 *  Else, display it with current mode.
	 */
	public void createIfNeeded() {
		if (spectrumManagerFrame == null) {
			setNormal();
		} else {
			spectrumManagerFrame.setVisible(true);
		}
	}

	/**
	 * Return the path of the file of the given spectrum, or the title if the
	 *  path can not be found.
	 *
	 * @param spectrum The {@link CassisSpectrum}, can not be null.
	 * @return the path of the file of the given spectrum, or his title if the
	 *  path can not be found.
	 */
	public String getPathOrName(CassisSpectrum spectrum) {
		CassisSpectrumModel csm = cassisSpectrumPanel.getControl().getModel();
		String res;

		if (csm.isOpen(spectrum)) {
			res = csm.getPath(spectrum);
		} else {
			CassisMetadata metadata = spectrum.getCassisMetadata(CASSIS_METADATA.FROM.toString());
			if (metadata != null && (new File(metadata.getValue()).exists()) ) {
				res = metadata.getValue();
			} else {
				res =  spectrum.getTitle();
			}
		}
		return res;
	}

	/**
	 * Handle spectrum with sub-scans.
	 *
	 * @param mscs The MultiScanCassisSpectrum
	 * @return The CassisSpectrum created from the spectrum with multiple scans.
	 */
	private CassisSpectrum handleSubScan(MultiScanCassisSpectrum mscs) {
		SelectionMultiScanSpectrumDialog dialog =
				new SelectionMultiScanSpectrumDialog(
						spectrumManagerFrame,
						mscs);
		dialog.setSize(300, 130);
		dialog.setLocationRelativeTo(null);
		dialog.setVisible(true);
		if (dialog.isDisplayOk()) {
			return dialog.getFinalSpectrum();
		}
		return null;
	}

	/**
	 * Return the FilePanel. This should only be used for tests.
	 *
	 * @return the FilePanel.
	 */
	public FilePanel getFilePanel() {
		return filePanel;
	}

	/**
	 * Check then return if the Spectrum Manager frame is visible.
	 *
	 * @return true if the Spectrum Manager frame is visible, false otherwise.
	 */
	private boolean isSpectrumManagerFrameVisible() {
		return spectrumManagerFrame != null && spectrumManagerFrame.isVisible();
	}

	public boolean isLastReadSuccessful() {
		return cassisSpectrumPanel.getControl().isLastReadSuccessful();
	}


}
