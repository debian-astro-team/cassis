/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.menu.action;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.AbstractAction;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.file.gui.download.DownloadGui;
import eu.omp.irap.cassis.properties.Software;

/**
 * Action to download CASSIS updater and launch it to download a new database.
 */
public class InstallDatabaseAction extends AbstractAction {

	private static final Logger LOGGER = LoggerFactory.getLogger(InstallDatabaseAction.class);
	private static final long serialVersionUID = 5019676179327100939L;
	private static final String INSTALLER_CLASS =
			"eu.omp.irap.cassis.wizard.newdb.InstallAdditionalDatabaseWizard";
	private static final String JAR_URL =
			Software.getUserConfiguration().getUpdatePath() + "cassis.setup.wizard.jar";

	private JFrame cassisFrame;


	/**
	 * Constructor.
	 */
	public InstallDatabaseAction(JFrame cassisFrame) {
		super("Install a new database");
		this.cassisFrame = cassisFrame;
	}

	/**
	 * Download then execute the installer to install database.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		final DownloadGui dg = new DownloadGui();
		dg.setTitle("Downloading data");
		dg.setUrl(JAR_URL);
		Runnable r = new Runnable() {
			@Override
			public void run() {
				File jarFile = dg.getDownloadedFile();
				jarFile.deleteOnExit();
				if (!jarFile.exists() || jarFile.length() == 0L) {
					JOptionPane.showMessageDialog(InstallDatabaseAction.this.cassisFrame,
							"Unable to start the database installer.",
							"CASSIS error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				startInstaller(jarFile);
			}
		};
		dg.setFinishAction(r);
		dg.setVisible(true);
		dg.startDownload();
	}

	/**
	 * Start the installer.
	 *
	 * @param jarFile The jar file of the installer.
	 */
	private void startInstaller(File jarFile) {
		String command = getCommand(jarFile);
		Runtime rt = Runtime.getRuntime();
		try {
			rt.exec(command);
		} catch (IOException ioe) {
			LOGGER.error("An IOException occured while starting the installer.", ioe);
		}
	}

	/**
	 * Construct the command to launch to start the installer.
	 *
	 * @param jarFile The jar file of the installer.
	 * @return The command to execute.
	 */
	private String getCommand(File jarFile) {
		StringBuilder sb = new StringBuilder();
		sb.append("java -cp ");
		sb.append(jarFile.getAbsolutePath());
		sb.append(" ");
		sb.append(INSTALLER_CLASS);
		sb.append(" ");
		sb.append(Software.getCassisPath());
		return sb.toString();
	}

}
