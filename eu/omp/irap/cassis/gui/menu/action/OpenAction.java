/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.menu.action;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.file.CassisExtensionFilter;
import eu.omp.irap.cassis.file.CassisFileFilter;
import eu.omp.irap.cassis.file.FileReaderCassis;
import eu.omp.irap.cassis.gui.PanelView;
import eu.omp.irap.cassis.gui.menu.OpenControl;
import eu.omp.irap.cassis.gui.model.lineanalysis.LineAnalysisControl;
import eu.omp.irap.cassis.gui.model.lteradex.LteRadexControl;
import eu.omp.irap.cassis.gui.model.rotationaldiagram.RotationalDiagramControl;
import eu.omp.irap.cassis.gui.model.spectrumanalysis.SpectrumAnalysisControl;
import eu.omp.irap.cassis.gui.template.ManageTemplateControl;
import eu.omp.irap.cassis.gui.util.FileUtils;
import eu.omp.irap.cassis.properties.Software;

/**
 * @author thomas
 *
 */
public class OpenAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(OpenAction.class);
	private Component mainFrame;
	private OpenControl control = null;


	public OpenAction(Component mainFrame) {
		super("Open");
		this.mainFrame = mainFrame;
	}

	/**
	 * @param control
	 *            control to set
	 */
	public final void setControl(OpenControl control) {
		this.control = control;
	}

	/**
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		openJFileChooser(null, true);
	}

	public void openJFileChooser(Component parent, boolean allowDirectOpening) {
		// create a file dialog
		JFileChooser chooser = new CassisJFileChooser(Software.getDataPath(), Software.getLastFolder("data"));
		// Add filter on cassis files (*.10m, *.30m)
		chooser.addChoosableFileFilter(new CassisExtensionFilter());
		chooser.setPreferredSize(new Dimension(600, 300));
		// open file dialog
		final int result = chooser.showOpenDialog(parent == null ? mainFrame : parent);
		// get selected file name
		if (result == JFileChooser.APPROVE_OPTION) {
			File file = chooser.getSelectedFile();
			Software.setLastFolder("data", file.getParent());
			String path = openCassisFile(file, allowDirectOpening);
			if (path != null && control != null) {
				control.fireNewCassisFileOpen(path);
			}
		}
	}

	/**
	 * Open configuration file.
	 *
	 * @param file The file to open
	 * @param extension The extension of the file.
	 * @throws IOException in case of error during the opening.
	 */
	private static void openConfigFile(File file, String extension) throws IOException {
		if ("sam".equals(extension)) {
			openSpectrumAnalysisConfigFile(file);
		} else if ("ltm".equals(extension)) {
			openLteRadexConfigFile(file);
		} else if ("lam".equals(extension)) {
			openLineAnalysisConfigFile(file);
		} else if ("rdm".equals(extension)) {
			openRotationalDiagramConfigFile(file);
		}
	}

	/**
	 * Open Rotational Diagram configuration file.
	 *
	 * @param file The file to open.
	 */
	private static void openRotationalDiagramConfigFile(File file) {
		RotationalDiagramControl control = PanelView.getInstance().addRotationalDiagramView();
		control.loadConfig(file);
	}

	/**
	 * Open Line Analysis configuration file.
	 *
	 *
	 * @param file The file to open.
	 */
	private static void openLineAnalysisConfigFile(File file) {
		LineAnalysisControl control = PanelView.getInstance().getCassisActionMenu()
				.getLineAnalysisModelAction().getLineAnalysisModelAction();
		control.loadConfig(file);
	}

	/**
	 * Open LTE-RADEX configuration file.
	 *
	 * @param file The file to open.
	 */
	private static void openLteRadexConfigFile(File file) {
		LteRadexControl control = PanelView.getInstance().getCassisActionMenu().getLteRadexAction()
				.getLteRadexAction();
		control.loadConfig(file);
	}

	/**
	 * Open Spectrum Analysis configuration file.
	 *
	 * @param file The file to open.
	 */
	private static void openSpectrumAnalysisConfigFile(File file) throws IOException {
		SpectrumAnalysisControl control = PanelView.getInstance().getCassisActionMenu()
				.getSpectrumAnalysisAction().getSpectrumAnalysisAction();
		control.getModel().loadConfig(file);
	}

	public static String openCassisFile(File file) {
		return openCassisFile(file, true);
	}

	public static String openCassisFile(File file, boolean allowDirectOpening) {
		String path = null;
		try {
			path = openCassisFileWithException(file, allowDirectOpening);
		} catch (Exception exc) {
			LOGGER.error("Error during opening the file", exc);
			JOptionPane.showMessageDialog(null, "Error during opening the file.", "Alert", JOptionPane.ERROR_MESSAGE);
		}
		return path;
	}

	/**
	 * @param file
	 * @param allowDirectOpening
	 * @param path
	 * @return
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static String openCassisFileWithException(File file, boolean allowDirectOpening) throws IOException, FileNotFoundException {
		String path = null;
		if (file != null) {
			final String filename = file.toString();
			final String extension = FileUtils.getFileExtension(filename);
			String[] ext = { "fus", "fits", "fts", "fca", "lis", "votable", "vot" };
			List<String> extList = new ArrayList<>();
			Collections.addAll(extList, ext);
			Collections.addAll(extList, CassisFileFilter.extClass);

			String[] configExt = { "sam", "ltm", "lcm", "lbm", "lam", "rdm" };
			List<String> configExtList = Arrays.asList(configExt);
			if ("rotd".equalsIgnoreCase(extension) || file.getPath().endsWith(".rotd.txt")) {
				openRotDiagramDataFile(file);
			} else if (extList.contains(extension.toLowerCase())) {
				// Fichier de données pour Line ou Spectrum analysis, au
				// choix de l'utilisateur + .fus, .fits, .votable, .fca,
				// .lis
				openDataFile(file, allowDirectOpening);
			} else if (configExtList.contains(extension.toLowerCase())) {
				openConfigFile(file, extension);
			} else if ("tec".equals(extension)) {
				openTemplate(file);
			} else {
				openDataFile(file, allowDirectOpening);
			}
			path = file.getPath();
		}
		return path;
	}

	/**
	 * Open (and import) the given template file (.tec).
	 *
	 * @param file The file to open.
	 * @throws FileNotFoundException If the file was not found.
	 * @throws IOException If there was an IOException.
	 */
	private static void openTemplate(File file) throws FileNotFoundException, IOException {
		String[] options = { "Yes", "No" };
		String message = "Do you want to add this template to CASSIS?";
		int choice = JOptionPane.showOptionDialog(null, message,
				"Add this template to CASSIS?",
				JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE,
				null, options, null);
		if (choice == JOptionPane.YES_OPTION) {
			File[] fileTab = { file };
			ManageTemplateControl control = PanelView.getInstance().addManageTemplateFrame();
			control.importTemplate(fileTab);
		}
	}

	/**
	 * Open a rotational diagram data file.
	 *
	 * @param file The file to open.
	 */
	private static void openRotDiagramDataFile(File file) {
		RotationalDiagramControl control = PanelView.getInstance().addRotationalDiagramView();
		control.getModel().getRotationalDataModel().loadData(file);
	}

	public static void openDataFileList(List<File> fileList) {
		boolean globalError = false;
		boolean errorCurrentFile = false;
		StringBuilder sb = new StringBuilder("Error while reading:\n");
		for (File file : fileList) {
			errorCurrentFile = false;

			CassisSpectrum spectrum = FileReaderCassis.createCassisSpectrumFromFile(file);
			if (spectrum != null) {
				SpectrumAnalysisControl control = PanelView.getInstance().getCassisActionMenu()
						.getSpectrumAnalysisAction().getSpectrumAnalysisAction();
				control.getModel().getLoadDataModel().setNameData(file.getPath());
				control.getModel().getLoadDataModel().setCassisSpectrum(spectrum);
				control.onDisplayButtonClicked();
			} else {
				errorCurrentFile = true;
			}

			if (errorCurrentFile) {
				globalError = true;
				sb.append("- ").append(file.getName()).append("\n");
			}

		}
		if (globalError) {
			JOptionPane.showMessageDialog(null, sb.toString(),
					"Warning : Read error.", JOptionPane.WARNING_MESSAGE);
		}
		// Add to Spectrum Manager module
		File[] files = new File[fileList.size()];
		files = fileList.toArray(files);
		SpectrumManagerAction.getInstance().addRessource(files);
	}

	public static void openCassisFileInSpectrumAnalysis(File file) {
		CassisSpectrum spectrum = FileReaderCassis.createCassisSpectrumFromFile(file);
		if (spectrum == null) {
			JOptionPane.showMessageDialog(null, "Cassis can not open this file.",
					"File reading error", JOptionPane.ERROR_MESSAGE);
		} else {
			SpectrumAnalysisControl control = PanelView.getInstance().getCassisActionMenu()
					.getSpectrumAnalysisAction().getSpectrumAnalysisAction();
			control.getModel().getLoadDataModel().setNameData(file.getPath());
			control.getModel().getLoadDataModel().setCassisSpectrum(spectrum);
			control.onDisplayButtonClicked();
		}
		// Add to Spectrum Manager module
		SpectrumManagerAction.getInstance().addRessource(file, false);
	}

	public OpenControl getOpenControl() {
		return control;
	}

	/**
	 * Open the VOTable file, which is not a spectrum but a result who should
	 *  contains a list of spectra. Limit the opening to the indexes of the selected
	 *  spectra.
	 *
	 * @param pathFileVotable The path of the file containing the VOTable.
	 * @param indexListSelected The index of the spectra to use.
	 * @param display true to display the spectra, false to only open them in Spectrum Manager.
	 */
	public static void openVotable(String pathFileVotable, List<Integer> indexListSelected, boolean display) {
		SpectrumManagerAction spectrumManagerAction = SpectrumManagerAction.getInstance();
		spectrumManagerAction.addVotable(pathFileVotable, indexListSelected);
		if (display) {
			spectrumManagerAction.downloadAndDisplayVotable(pathFileVotable, indexListSelected, display);
		}
	}

	/**
	 * Open the VOTable file, which is not a spectrum but a result who should
	 *  contains a list of spectra.
	 *
	 * @param pathFileVotable The path of the file containing the VOTable.
	 * @param display true to display the spectra, false to only open them in Spectrum Manager.
	 */
	public static void openVotable(String pathFileVotable, boolean display) {
		SpectrumManagerAction spectrumManagerAction = SpectrumManagerAction.getInstance();
		spectrumManagerAction.addVotable(pathFileVotable);
		if (display) {
			spectrumManagerAction.downloadAndDisplayVotable(pathFileVotable);
		} else {
			SpectrumManagerAction.getInstance().createIfNeeded();
		}
	}

	/**
	 * Open the given data file.
	 *
	 * @param file The file
	 * @param allowDirectOpening true to allow to open it directly, false otherwise.
	 */
	private static void openDataFile(File file, boolean allowDirectOpening) {
		SpectrumManagerAction.getInstance().createIfNeeded();
		SpectrumManagerAction.getInstance().addRessource(file, false);
		if (allowDirectOpening &&
				Software.getUserConfiguration().isOpenFileDirectly()
				&& SpectrumManagerAction.getInstance().isLastReadSuccessful()) {
			SpectrumManagerAction.getInstance().displayLastSpectrum();
		}
	}

	public static void openCassisFileFromSamp(File file, Map<String, String> params) {
		SpectrumManagerAction.getInstance().createIfNeeded();
		SpectrumManagerAction.getInstance().addRessourceWithAdditional(file, params);
		if (
				Software.getUserConfiguration().isOpenFileDirectly()
				&& SpectrumManagerAction.getInstance().isLastReadSuccessful()) {
			SpectrumManagerAction.getInstance().displayLastSpectrum();
		}

	}
}
