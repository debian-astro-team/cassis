/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.menu.action;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;

import eu.omp.irap.cassis.common.gui.ScreenUtil;
import eu.omp.irap.cassis.database.access.InfoDataBase;
import eu.omp.irap.cassis.database.access.TypeDataBase;
import eu.omp.irap.cassis.database.creation.DbCreationPanel;
import eu.omp.irap.cassis.gui.configuration.UserConfigurationView;

/**
 * Action related to Database creation module.
 *
 * @author M. Boiziot
 */
public class DatabaseAction extends AbstractAction {

	private static final long serialVersionUID = 492650483687964829L;
	private DbCreationPanel dbCreationPanel;
	private JFrame databaseFrame;
	private final JFrame cassisFrame;
	private JButton createUseDatabaseButton;


	/**
	 * Constructor.
	 *
	 * @param cassisFrame The owner frame.
	 */
	public DatabaseAction(JFrame cassisFrame) {
		super("Database Creation");
		this.cassisFrame = cassisFrame;
	}

	/**
	 * @see eu.omp.irap.cassis.gui.menu.action.AbstractAction#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		getDatabaseFrame().setVisible(true);
		ScreenUtil.center(this.cassisFrame, getDatabaseFrame());
	}

	/**
	 * Create if necessary then return the Database JFrame.
	 *
	 * @return the Database JFrame.
	 */
	private JFrame getDatabaseFrame() {
		if (databaseFrame == null) {
			databaseFrame = new JFrame("Database Creation");
			databaseFrame.setContentPane(getDbCreationPanel());
			databaseFrame.setMinimumSize(new Dimension(650, 300));
			databaseFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			databaseFrame.pack();
		}
		return databaseFrame;
	}

	/**
	 * Create if needed then return the {@link DbCreationPanel}. It also add
	 *  the "Create then use database" button to it.
	 *
	 * @return the DbCreationPanel.
	 */
	private DbCreationPanel getDbCreationPanel() {
		if (dbCreationPanel == null) {
			dbCreationPanel = new DbCreationPanel();
			GridBagConstraints c = new GridBagConstraints();
			c.anchor = GridBagConstraints.BASELINE_TRAILING;
			c.insets.right = 10;
			dbCreationPanel.getButtonsPanel().add(getCreateUseDatabaseButton(), c);
		}
		return dbCreationPanel;
	}

	/**
	 * Create if needed then return the "Create then use database" button.
	 *
	 * @return the "Create then use database" button.
	 */
	private JButton getCreateUseDatabaseButton() {
		if (createUseDatabaseButton == null) {
			createUseDatabaseButton = new JButton("Create then use database");
			createUseDatabaseButton.setToolTipText(
					"This will create the database then use it in CASSIS.");
			createUseDatabaseButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					String db = getDbCreationPanel().getControl().createDatabase();
					if (db != null) {
						InfoDataBase.getInstance().setDataBaseType(
								TypeDataBase.SQLITE.name(), db,
								InfoDataBase.getInstance().isInMemoryDatabase());
						UserConfigurationView.doSaveUpdateTitle();
					}
				}
			});
		}
		return createUseDatabaseButton;
	}
}
