/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.menu.action;

import java.awt.AWTException;
import java.awt.Component;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.gui.plot.CassisViewInterface;
import eu.omp.irap.cassis.gui.util.BrowserControl;
import eu.omp.irap.cassis.properties.Software;

/**
 * @author glorian
 *
 */
@SuppressWarnings("serial")
public class CaptureAction extends AbstractAction {

	private static final Logger LOGGER = LoggerFactory.getLogger(CaptureAction.class);
	private JMenu menuFile = null;
	private CassisViewInterface view;
	private Component mainFrame;


	public CaptureAction(JMenu menuFile, Component mainFrame, CassisViewInterface view) {
		super("Capture image");
		this.view = view;
		this.menuFile = menuFile;
		this.mainFrame = mainFrame;
	}

	public CaptureAction(JFrame owner, CassisViewInterface cassisViewInterface) {
		this(null, owner, cassisViewInterface);
	}

	/**
	 * @param cassisView
	 *            the cassisView_ to set
	 */
	public final void setCassisView(CassisViewInterface cassisView) {
		this.view = cassisView;
	}

	/**
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		new Thread() {
			@Override
			public void run() {
				try {
					sleep(500);
				} catch (InterruptedException ie) {
					LOGGER.warn("Error while trying to sleep", ie);
				}
				doCaptureAndDisplay();
			}
		}.start();
	}

	public void setMenuFile(JMenu newMenuFile) {
		this.menuFile = newMenuFile;
	}

	/**
	 * Do the needed actions to capture the interface and display the resulting
	 *  file in browser.
	 */
	private void doCaptureAndDisplay() {
		if (isComponentShowing()) {
			disarmMenu();

			if (doScreenshot()) {
				String path = Software.getCassisPath() + File.separatorChar + "capture.html";
				writeHtmlFile(path);

				path = new File(path).toURI().toString();
				BrowserControl.displayURL(path);
			}
		}
	}

	/**
	 * Check if the component is showing. Display an error message if it is not.
	 *
	 * @return true if the component is showing, false otherwise.
	 */
	private boolean isComponentShowing() {
		if (!view.getComponent().isShowing()) {
			JOptionPane.showMessageDialog(mainFrame, "Nothing to capture!",
					"Alert", JOptionPane.WARNING_MESSAGE);
			return false;
		}
		return true;
	}

	/**
	 * Disarm the menu file.
	 */
	private void disarmMenu() {
		if (menuFile != null) {
			menuFile.setArmed(false);
		}
	}

	/**
	 * Create an image, get the output file then save it in it.
	 *
	 * @return true if screenshot was a success, false otherwise.
	 */
	private boolean doScreenshot() {
		BufferedImage image = createImage();
		if (image != null) {
			File output = getOutputFile();
			writeImage(image, output);
			return true;
		}
		return false;
	}

	/**
	 * Create the image then return it.
	 *
	 * @return the image created or null if there was an error.
	 */
	private BufferedImage createImage() {
		Robot robot = null;
		try {
			robot = new Robot();
		} catch (AWTException ae) {
			LOGGER.error("Error while creating a new Robot", ae);
			return null;
		}

		return robot.createScreenCapture(new Rectangle(
				view.getComponent().getLocationOnScreen().x,
				view.getComponent().getLocationOnScreen().y,
				view.getComponent().getWidth(), view.getComponent().getHeight()));

	}

	/**
	 * Write the HTML file.
	 *
	 * @param path The path to the file used to save the HTML content.
	 */
	private void writeHtmlFile(String path) {
		try (BufferedWriter out = new BufferedWriter(new FileWriter(path))) {
			out.write(getHtmlContent());
		} catch (IOException e) {
			LOGGER.error("Error while saving the file {}", path, e);
		}
	}

	/**
	 * Write an image to a file.
	 *
	 * @param image The image to save.
	 * @param file The file used to save the image.
	 */
	private void writeImage(BufferedImage image, File file) {
		try {
			ImageIO.write(image, "JPG", file);
		} catch (IOException ioe) {
			LOGGER.error("Error while saving image file", ioe);
		}
	}

	/**
	 * Delete the output file it if already exist, then return it.
	 *
	 * @return the output file.
	 */
	private File getOutputFile() {
		File output = new File(Software.getCassisPath() + File.separatorChar + "capture.jpg");
		if (output.exists() && !output.delete()) {
			LOGGER.error("Error when deleting the file {}", output.getPath());
		}
		return output;
	}

	/**
	 * Create then return the content to set in the HTML file.
	 *
	 * @return the content to set in the HTML file.
	 */
	private String getHtmlContent() {
		StringBuilder sb = new StringBuilder();
		sb.append("<!DOCTYPE html PUBLIC ");
		sb.append("\"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n");
		sb.append("<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"fr\" >\n");
		sb.append("\t<head>\n");
		sb.append("\t\t<title>Screen capture CASSIS !</title>\n");
		sb.append("\t\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n");
		sb.append("\t</head>\n");
		sb.append("\t<body>\n");
		sb.append("\t\t<img src=\"capture.jpg\"/> \n");
		sb.append("\t</body> \n");
		sb.append("</html>");
		return sb.toString();
	}
}
