/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.menu;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.JMenu;

import eu.omp.irap.cassis.gui.configuration.UserConfigurationView;
import eu.omp.irap.cassis.gui.help.AboutDialog;
import eu.omp.irap.cassis.gui.menu.action.CaptureAction;
import eu.omp.irap.cassis.gui.menu.action.CassisFormalismAction;
import eu.omp.irap.cassis.gui.menu.action.ConfigurationAction;
import eu.omp.irap.cassis.gui.menu.action.CopyAction;
import eu.omp.irap.cassis.gui.menu.action.CutAction;
import eu.omp.irap.cassis.gui.menu.action.DatabaseAction;
import eu.omp.irap.cassis.gui.menu.action.EpnTapAction;
import eu.omp.irap.cassis.gui.menu.action.ExitAction;
import eu.omp.irap.cassis.gui.menu.action.HelpAction;
import eu.omp.irap.cassis.gui.menu.action.HelpUpdateAction;
import eu.omp.irap.cassis.gui.menu.action.ImportFilesAction;
import eu.omp.irap.cassis.gui.menu.action.InstallDatabaseAction;
import eu.omp.irap.cassis.gui.menu.action.LineAnalysisModelAction;
import eu.omp.irap.cassis.gui.menu.action.LogAction;
import eu.omp.irap.cassis.gui.menu.action.LoomisWoodAction;
import eu.omp.irap.cassis.gui.menu.action.LteRadexAction;
import eu.omp.irap.cassis.gui.menu.action.ManageTemplateAction;
import eu.omp.irap.cassis.gui.menu.action.OpenAction;
import eu.omp.irap.cassis.gui.menu.action.OpenUrlAction;
import eu.omp.irap.cassis.gui.menu.action.PastAction;
import eu.omp.irap.cassis.gui.menu.action.PrintAction;
import eu.omp.irap.cassis.gui.menu.action.RotationalDiagramAction;
import eu.omp.irap.cassis.gui.menu.action.SaveAction;
import eu.omp.irap.cassis.gui.menu.action.SaveLineAction;
import eu.omp.irap.cassis.gui.menu.action.ScriptsLteRadexAction;
import eu.omp.irap.cassis.gui.menu.action.SpectrumAnalysisAction;
import eu.omp.irap.cassis.gui.menu.action.SpectrumManagerAction;
import eu.omp.irap.cassis.gui.menu.action.SsapAction;
import eu.omp.irap.cassis.gui.menu.action.VOConnectAction;
import eu.omp.irap.cassis.gui.menu.action.VOCreateAction;
import eu.omp.irap.cassis.gui.menu.action.VODisConnectAction;
import eu.omp.irap.cassis.gui.plot.CassisViewInterface;

/**
 * @author thomas
 *
 */
public class CassisActionMenu extends AbstractActionMenu {

	protected SaveAction saveItemAction;
	protected SaveLineAction saveLineAction;
	protected CaptureAction captureItemAction;
	protected PrintAction printItemAction;
	protected OpenAction openAction;
	private OpenUrlAction openUrlAction;
	private AbstractAction vOCreateAction;
	private AbstractAction vOConnectAction;
	private AbstractAction vODisConnectAction;
	private ScriptsLteRadexAction scriptsLteRadexAction;
	private ConfigurationAction configAction;
	private ConfigurationAction databaseConfigAction;
	private ConfigurationAction radexConfigAction;
	private ConfigurationAction fitConfigAction;
	private HelpUpdateAction updateAction;
	private SsapAction ssapAction;
	private SpectrumAnalysisAction spectrumAnalysisAction;
	private RotationalDiagramAction rotationalDiagramAction;
	private LineAnalysisModelAction lineAnalysisModelAction;
	private LoomisWoodAction loomisWoodAction;
	private LteRadexAction lteRadexAction;
	private AbstractAction multipleSpectrumViewAction;
	private AbstractAction rotationalDiagramViewtAction;
	private AbstractAction fullSpectrumViewAction;
	private AbstractAction lineViewAction;
	private ManageTemplateAction manageTemplateAction;
	private ImportFilesAction importFilesAction;
	private DatabaseAction databaseAction;
	private EpnTapAction epnTapAction;
	private LogAction logAction;
	private Action installDatabaseAction;


	public CassisActionMenu(JFrame owner, final MenuManagerInterface manager,
			CassisViewInterface cassisView) {
		super(owner, manager, cassisView);

		initFileActions(owner);
		initPreferencesActions(owner);
		initModelsActions(owner, manager);
		initViewsActions(manager);
		initManageTemplateActions(manager);
		initIvoaActions(owner);
		initUpdateAction(owner);
		logAction = new LogAction();
	}

	private void initUpdateAction(JFrame owner) {
		updateAction = new HelpUpdateAction("Update", owner);
	}

	private void initIvoaActions(JFrame owner) {
		ssapAction = new SsapAction(owner);
		epnTapAction = new EpnTapAction(owner);
	}

	private void initManageTemplateActions(final MenuManagerInterface manager) {
		manageTemplateAction = new ManageTemplateAction(manager);
	}

	public void initScriptActions(JFrame owner) {
		scriptsLteRadexAction = new ScriptsLteRadexAction(owner, "Jython", "python.gif", "Jython scripting");
	}

	@SuppressWarnings("serial")
	private void initViewsActions(final MenuManagerInterface manager) {
		fullSpectrumViewAction = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				manager.switchToSpectrumTab();
			}
		};

		lineViewAction = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				manager.switchToMoleculeTab();
			}
		};

		multipleSpectrumViewAction =  new AbstractAction("Loomis spectrums") {
			@Override
			public void actionPerformed(ActionEvent e) {
				manager.switchToMultipleViewTab();
			}
		};

		rotationalDiagramViewtAction = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				manager.switchToRotationalTab();
			}
		};
	}

	private void initPreferencesActions(JFrame owner) {
		configAction = new ConfigurationAction("General",
				"Edit general settings", owner, UserConfigurationView.TAB_GENERAL_INDEX);
		getDatabaseConfigurationAction();
		radexConfigAction = new ConfigurationAction("Radex",
				"Edit radex settings", owner, UserConfigurationView.TAB_RADEX_INDEX);

		fitConfigAction = new ConfigurationAction("Fit",
				"Fit settings", owner, UserConfigurationView.TAB_FIT_INDEX);



		importFilesAction = new ImportFilesAction();
	}

	private void initModelsActions(JFrame owner, final MenuManagerInterface manager) {
		spectrumAnalysisAction = new SpectrumAnalysisAction(manager);
		lteRadexAction = new LteRadexAction(manager);
		loomisWoodAction = new LoomisWoodAction(manager);
		lineAnalysisModelAction = new LineAnalysisModelAction(manager);
		rotationalDiagramAction = new RotationalDiagramAction(manager);
		databaseAction = new DatabaseAction(owner);
	}

	private void initFileActions(JFrame owner) {
		openAction = new OpenAction(owner);
		openUrlAction = new OpenUrlAction();
		saveItemAction = new SaveAction(owner, cassisViewInterface);
		saveLineAction = new SaveLineAction(owner, cassisViewInterface);
		captureItemAction = new CaptureAction(owner, cassisViewInterface);
		printItemAction = new PrintAction(cassisViewInterface);
	}

	@SuppressWarnings("serial")
	@Override
	public Action getHelpAboutAction() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				AboutDialog.createInstance(owner);
			}
		};
	}

	@Override
	public Action getHelpUpdateAction() {
		return updateAction;
	}

	@Override
	public Action getHelpUsersManualAction() {
		return new HelpAction();
	}

	@Override
	public Action getConfigurationAction() {
		return configAction;
	}

	@Override
	public AbstractAction getCopyAction() {
		return new CopyAction();
	}

	@Override
	public AbstractAction getCutAction() {
		return new CutAction();
	}

	@Override
	public AbstractAction getPasteAction() {
		return new PastAction();
	}

	@Override
	public LineAnalysisModelAction getLineAnalysisModelAction() {
		return lineAnalysisModelAction;
	}

	@Override
	public LteRadexAction getLteRadexAction() {
		return lteRadexAction;
	}

	@Override
	public Action getManageTemplateAction() {
		return manageTemplateAction;
	}

	@Override
	public RotationalDiagramAction getRotationalDiagramAction() {
		return rotationalDiagramAction;
	}

	@Override
	public SpectrumAnalysisAction getSpectrumAnalysisAction() {
		return spectrumAnalysisAction;
	}

	@Override
	public Action getFullLineViewAction() {
		return lineViewAction;
	}

	@Override
	public Action getFullSpectrumView() {
		return fullSpectrumViewAction;
	}

	@Override
	public Action getRotationalDiagramViewAction() {
		return rotationalDiagramViewtAction;
	}

	@Override
	public Action getExitAction() {
		return new ExitAction();
	}

	@Override
	public OpenAction getOpenAction() {
		return openAction;
	}

	@Override
	public OpenUrlAction getOpenUrlAction() {
		return openUrlAction;
	}

	@Override
	public AbstractAction getCaptureAction(JMenu menuFile) {
		captureItemAction.setMenuFile(menuFile);
		return captureItemAction;
	}

	@Override
	public CaptureAction getCaptureAction() {
		return captureItemAction;
	}

	@Override
	public PrintAction getPrintAction() {
		return printItemAction;
	}

	@Override
	public SaveAction getSaveAction() {
		return saveItemAction;
	}

	@Override
	public SaveLineAction getSaveLineAction() {
		return saveLineAction;
	}

	@Override
	public AbstractAction getVOCreateAction() {
		if (vOCreateAction == null) {
			vOCreateAction = new VOCreateAction();
		}
		return vOCreateAction;
	}

	@Override
	public AbstractAction getVOConnectAction() {
		if (vOConnectAction == null) {
			vOConnectAction = new VOConnectAction();
		}
		return vOConnectAction;
	}

	@Override
	public AbstractAction getVODisConnectAction() {
		if (vODisConnectAction == null) {
			vODisConnectAction = new VODisConnectAction();
		}
		return vODisConnectAction;
	}

	@Override
	public LoomisWoodAction getmenuItemLoomisWoodAction() {
		return loomisWoodAction;
	}

	@Override
	public Action getMultipleSpectrumView() {
		return multipleSpectrumViewAction;
	}

	@Override
	public Action getScriptsLteRadexAction() {
		return scriptsLteRadexAction;
	}

	@Override
	public Action getSsapAction()  {
		return ssapAction;
	}

	@Override
	public Action getCassisFormalismAction() {
		return new CassisFormalismAction();
	}

	@Override
	public Action getImportFilesAction() {
		return importFilesAction;
	}

	@Override
	public Action getDatabaseAction() {
		return databaseAction;
	}

	@Override
	public Action getSpectrumManagerAction() {
		return SpectrumManagerAction.getInstance();
	}

	@Override
	public Action getEpnTapAction() {
		return epnTapAction;
	}

	@Override
	public Action getLogAction() {
		return logAction;
	}

	@Override
	public Action getDatabaseConfigurationAction() {
		if (databaseConfigAction == null) {
			databaseConfigAction = new ConfigurationAction("Database Settings",
				"Edit database settings", owner, UserConfigurationView.TAB_DATABASE_INDEX);
		}
		return databaseConfigAction;
	}

	@Override
	public Action getRadexConfigurationAction() {
		return radexConfigAction;
	}

	@Override
	public Action getFitConfigurationAction() {
		return fitConfigAction;
	}

	@Override
	public Action getInstallDatabaseAction() {
		if (installDatabaseAction == null) {
			installDatabaseAction = new InstallDatabaseAction(this.owner);
		}
		return installDatabaseAction;
	}
}
