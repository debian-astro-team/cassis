/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JSeparator;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import eu.omp.irap.cassis.gui.menu.action.OpenAction;
import eu.omp.irap.cassis.gui.menu.action.SpectrumManagerAction;
import eu.omp.irap.cassis.gui.util.JMenuItemButton;
import eu.omp.irap.mac.MacHandling;

public class JMenuCassisBar extends JMenuBar {

	private static final long serialVersionUID = 1L;

	private JRadioButtonMenuItem menuItemFullSpectrum;
	private JRadioButtonMenuItem menuItemLineSpectrum;
	private JRadioButtonMenuItem menuItemRotationalDiagram;
	private JRadioButtonMenuItem menuItemMultipleSpectrum;
	protected AbstractActionMenu abstractActionMenu;
	private JMenu menuWindows;
	private JMenu menuView;
	private JMenu menuEdit;
	private JMenuItem menuSave;
	private JMenuItem menuSaveLine;
	private JMenuItem menuExit;
	protected JMenu menuFile;
	private JMenuItem menuCapture;
	private JMenu menuOpenRecent;
	private JMenuItem menuPrint;
	private OpenControl openControl;
	private JMenu menuHelp;
	private JMenu menuDatabase;
	private JMenuItem menuItemTemplateManage;
	private JMenuItem menuItemSpectrumAnalysis;
	private JMenuItem menuItemLteRadex;
	private JMenuItem menuItemLoomisWood;
	private JMenu menuModules;
	private JMenuItem menuItemRotational;
	private JMenuItem menuItemLineAnaLysis;
	private JMenuItem menuItemSpectrumManager;
	private JMenu menuPreferences;
	private JMenu menuVO;
	private JMenuItem menuItemOpen;
	private JMenuItem menuItemOpenUrl;
	private JMenu menuScripts;
	private SendToMenu sendToMenu;
	private JMenuItem menuItemDatabase;
	private JMenuItem menuItemInstallDatabase;
	private boolean macMenu;


	public JMenuCassisBar(AbstractActionMenu abstractActionMenu, boolean enableMacMenu) {
		super();
		this.macMenu = enableMacMenu;
		this.abstractActionMenu = abstractActionMenu;
		getOpenControl();

		this.add(getMenuFile());
		this.add(getMenuEdit());
		this.add(getMenuModules());
		this.add(getMenuView());
		this.add(getMenuScripts());
		this.add(getMenuWindows());
		this.add(getMenuDatabase());
		this.add(getMenuPreferences());
		this.add(getMenuVO());
		this.add(getMenuHelp());
	}

	public OpenControl getOpenControl() {
		if (openControl == null) {
			OpenRecent openRecent = new OpenRecent();
			this.openControl = new OpenControl(getMenuOpenRecent(), openRecent);
			((OpenAction) this.abstractActionMenu.getOpenAction()).setControl(openControl);
			getMenuOpenRecent().addMenuListener(new MenuListener() {

				@Override
				public void menuSelected(MenuEvent e) {
					openControl.refreshItems();
				}

				@Override
				public void menuDeselected(MenuEvent e) {
					// Nothing to do, only here as present in MenuListener interface.
				}

				@Override
				public void menuCanceled(MenuEvent e) {
					// Nothing to do, only here as present in MenuListener interface.
				}
			});
		}
		return openControl;
	}

	/**
	 * @return the menuItemFullSpectrum
	 */
	public JRadioButtonMenuItem getMenuItemFullSpectrum() {
		if (menuItemFullSpectrum == null) {
			menuItemFullSpectrum = new JRadioButtonMenuItem("Full Spectrum", false);
			menuItemFullSpectrum.addActionListener(abstractActionMenu.getFullSpectrumView());
		}
		return menuItemFullSpectrum;
	}

	/**
	 * @return the menuItemLineSpectrum
	 */
	public JRadioButtonMenuItem getMenuItemLineSpectrum() {
		if (menuItemLineSpectrum == null) {
			menuItemLineSpectrum = new JRadioButtonMenuItem("Line Spectrum", false);
			menuItemLineSpectrum.addActionListener(abstractActionMenu.getFullLineViewAction());
		}
		return menuItemLineSpectrum;
	}

	public JMenu getMenuWindows() {
		if (menuWindows == null) {
			menuWindows = new JMenu("Windows");
		}
		return menuWindows;
	}

	/**
	 * @return the menuItemRotationalDiagram
	 */
	public JRadioButtonMenuItem getMenuItemRotationalDiagram() {
		if (menuItemRotationalDiagram == null) {
			menuItemRotationalDiagram = new JRadioButtonMenuItem("Rotational Diagram", false);
			menuItemRotationalDiagram.addActionListener(abstractActionMenu.getRotationalDiagramViewAction());
		}
		return menuItemRotationalDiagram;
	}

	public JMenu getMenuHelp() {
		if (menuHelp == null) {
			menuHelp = new JMenu("Help");
			// User's manual item
			menuHelp.add(new JMenuItem(abstractActionMenu.getHelpUsersManualAction()));

			// CASSIS Formalism
			menuHelp.add(new JMenuItem(abstractActionMenu.getCassisFormalismAction()));

			// Update item
			menuHelp.add(new JMenuItem(abstractActionMenu.getHelpUpdateAction()));

			menuHelp.add(new JMenuItem(abstractActionMenu.getLogAction()));

			// About item
			JMenuItem aboutItem = new JMenuItem(abstractActionMenu.getHelpAboutAction());
			aboutItem.setText("About");
			if (macMenu) {
				MacHandling.getInstance().setAboutAction(new Runnable() {
					@Override
					public void run() {
						abstractActionMenu.getHelpAboutAction().actionPerformed(null);
					}
				});
			}
			menuHelp.add(aboutItem);
		}
		return menuHelp;
	}

	private JMenu getMenuPreferences() {
		if (menuPreferences == null) {
			menuPreferences = new JMenu("Preferences");
			JMenuItem confItem = new JMenuItem(abstractActionMenu.getConfigurationAction());
			if (macMenu) {
				MacHandling.getInstance().setPreferencesAction(new Runnable() {
					@Override
					public void run() {
						abstractActionMenu.getConfigurationAction().actionPerformed(null);
					}
				});
			}
			menuPreferences.add(confItem);
			menuPreferences.add(new JMenuItem(
					abstractActionMenu.getDatabaseConfigurationAction()));
			menuPreferences.add(new JMenuItem(
					abstractActionMenu.getRadexConfigurationAction()));
			menuPreferences.add(new JMenuItem(
					abstractActionMenu.getFitConfigurationAction()));
			menuPreferences.addSeparator();
			menuPreferences.add(new JMenuItem(
					abstractActionMenu.getImportFilesAction()));
		}
		return menuPreferences;
	}

	private JMenu getMenuVO() {
		if (menuVO == null) {
			menuVO = new JMenu("VO");
			JMenu sampMenu = new JMenu("SAMP");
			sampMenu.add(new JMenuItem(abstractActionMenu.getVOCreateAction()));
			sampMenu.add(new JMenuItem(abstractActionMenu.getVOConnectAction()));
			sampMenu.add(new JMenuItem(abstractActionMenu.getVODisConnectAction()));
			sampMenu.add(getSendToMenu());
			menuVO.add(sampMenu);
			menuVO.add(new JMenuItem(abstractActionMenu.getSsapAction()));
			menuVO.add(new JMenuItem(abstractActionMenu.getEpnTapAction()));
		}
		return menuVO;
	}

	public JMenu getMenuEdit() {
		if (menuEdit == null) {
			menuEdit = new JMenu("Edit");
			menuEdit.add(new JMenuItem(abstractActionMenu.getCutAction()));
			menuEdit.add(new JMenuItem(abstractActionMenu.getCopyAction()));
			menuEdit.add(new JMenuItem(abstractActionMenu.getPasteAction()));
		}
		return menuEdit;
	}

	/**
	 * Create the menu of different modules.
	 *
	 * @return JMenu the menu to display
	 */
	public JMenu getMenuModules() {
		if (menuModules == null) {
			menuModules = new JMenu("Modules");
			menuModules.setName("Modules");
			menuModules.add(getSpectrumAnalysisItem());
			menuModules.add(getLteRadexItem());
			menuModules.add(getLoomisWoodItem());
			menuModules.add(new JSeparator());
			menuModules.add(getLineAnalysisModel());
			menuModules.add(new JSeparator());
			menuModules.add(getRotationalDiagramItem());
			menuModules.add(new JSeparator());
			menuModules.add(getSpectrumManagerItem());
		}
		return menuModules;
	}

	public JMenuItem getRotationalDiagramItem() {
		if (menuItemRotational == null) {
			menuItemRotational = new JMenuItem(abstractActionMenu.getRotationalDiagramAction());
		}
		return menuItemRotational;
	}

	public JMenuItem getSpectrumManagerItem() {
		if (menuItemSpectrumManager == null) {
			menuItemSpectrumManager = new JMenuItem("Spectrum Manager");
			menuItemSpectrumManager.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					SpectrumManagerAction.getInstance().setNormal();
				}
			});
		}
		return menuItemSpectrumManager;
	}

	public JMenuItem getLineAnalysisModel() {
		if (menuItemLineAnaLysis == null) {
			menuItemLineAnaLysis = new JMenuItem(abstractActionMenu.getLineAnalysisModelAction());
		}
		return menuItemLineAnaLysis;
	}

	private JMenuItem getLoomisWoodItem() {
		if (menuItemLoomisWood == null) {
			menuItemLoomisWood = new JMenuItem(abstractActionMenu.getmenuItemLoomisWoodAction());
		}
		return menuItemLoomisWood;
	}

	public JMenuItem getLteRadexItem() {
		if (menuItemLteRadex == null) {
			menuItemLteRadex = new JMenuItem(abstractActionMenu.getLteRadexAction());
		}
		return menuItemLteRadex;
	}

	public JMenuItem getSpectrumAnalysisItem() {
		if (menuItemSpectrumAnalysis == null) {
			menuItemSpectrumAnalysis = new JMenuItem(abstractActionMenu.getSpectrumAnalysisAction());
		}
		return menuItemSpectrumAnalysis;
	}

	private JMenu getMenuDatabase() {
		if (menuDatabase == null) {
			menuDatabase = new JMenu("Database");
			JMenuItem confItem = new JMenuItem(
					abstractActionMenu.getDatabaseConfigurationAction());
			if (macMenu) {
				MacHandling.getInstance().setPreferencesAction(new Runnable() {
					@Override
					public void run() {
						abstractActionMenu.getConfigurationAction().actionPerformed(null);
					}
				});
			}
			menuDatabase.add(confItem);
			menuDatabase.add(getMenuTemplateItem());
			menuDatabase.add(new JSeparator());
			menuDatabase.add(getMenuItemDatabase());
			menuDatabase.add(new JSeparator());
			menuDatabase.add(getMenuItemInstallDatabase());
		}
		return menuDatabase;
	}

	private JMenuItem getMenuTemplateItem() {
		if (menuItemTemplateManage == null) {
			menuItemTemplateManage = new JMenuItem(abstractActionMenu.getManageTemplateAction());
		}
		return menuItemTemplateManage;
	}

	/**
	 * Create the menu of different views.
	 *
	 * @return JMenu the menu to display
	 */
	public JMenu getMenuView() {
		if (menuView == null) {
			menuView = new JMenu("View");
			ButtonGroup group = new ButtonGroup();

			group.add(getMenuItemFullSpectrum());
			menuView.add(getMenuItemFullSpectrum());

			group.add(getMenuItemLineSpectrum());
			menuView.add(getMenuItemLineSpectrum());

			group.add(getMenuItemMultipleSpectrum());
			menuView.add(getMenuItemMultipleSpectrum());

			group.add(getMenuItemRotationalDiagram());
			menuView.add(getMenuItemRotationalDiagram());
		}
		return menuView;
	}

	/**
	 * Create if needed then return the File menu.
	 *
	 * @return the File menu.
	 */
	public JMenu getMenuFile() {
		if (menuFile == null) {
			menuFile = new JMenu("File");

			menuFile.add(getMenuItemOpen());
			menuFile.add(getMenuItemOpenUrl());
			menuFile.add(getMenuOpenRecent());

			menuFile.add(getMenuSave());
			menuFile.add(getMenuSaveLine());
			menuFile.addSeparator();

			menuFile.add(getMenuCapture());
			menuFile.add(getMenuPrint());
			menuFile.addSeparator();

			menuFile.add(getMenuExit());
		}

		return menuFile;
	}

	private JMenuItem getMenuItemOpen() {
		if (menuItemOpen == null) {
			menuItemOpen = new JMenuItem(abstractActionMenu.getOpenAction());
			menuItemOpen.setEnabled(true);
		}
		return menuItemOpen;
	}

	private JMenuItem getMenuItemOpenUrl() {
		if (menuItemOpenUrl == null) {
			menuItemOpenUrl = new JMenuItem(abstractActionMenu.getOpenUrlAction());
			menuItemOpenUrl.setEnabled(true);
		}
		return menuItemOpenUrl;
	}

	protected JMenu getMenuOpenRecent() {
		if (menuOpenRecent == null) {
			menuOpenRecent = new JMenu("Open Recent");
		}
		return menuOpenRecent;
	}

	protected JMenuItem getMenuSaveLine() {
		if (menuSaveLine == null) {
			menuSaveLine = new JMenuItem(abstractActionMenu.getSaveLineAction());
		}
		return menuSaveLine;
	}

	protected JMenuItem getMenuCapture() {
		if (menuCapture == null) {
			menuCapture = new JMenuItem(abstractActionMenu.getCaptureAction(menuFile));
		}
		return menuCapture;
	}

	protected JMenuItem getMenuPrint() {
		if (menuPrint == null) {
			menuPrint = new JMenuItem(abstractActionMenu.getPrintAction());
		}
		return menuPrint;
	}

	public JMenuItem getMenuExit() {
		if (menuExit == null) {
			menuExit = new JMenuItem(abstractActionMenu.getExitAction());
			menuExit.setText("Exit");
			if (macMenu) {
				MacHandling.getInstance().setQuitAction(new Runnable() {
					@Override
					public void run() {
						abstractActionMenu.getExitAction().actionPerformed(null);
					}
				});
			}
		}
		return menuExit;
	}

	/**
	 * @return the JMenuItem of Save
	 */
	public JMenuItem getMenuSave() {
		if (menuSave == null) {
			menuSave = new JMenuItem(abstractActionMenu.getSaveAction());
		}
		return menuSave;
	}

	public void addMenuWindows(JMenuItem lteRadexMenu) {
		menuWindows.add(lteRadexMenu);
	}

	/**
	 * Remove the menu item of a frame when the frame is closed.
	 *
	 * @param window The frame corresponding to the menu item.
	 * @return true if a menu item was removed, false otherwise.
	 */
	public boolean removeWindowMenuItem(JFrame window) {
		String currentMenu;
		String frameTitle = window.getTitle();
		for (int i = 0; i < menuWindows.getItemCount(); i++) {
			if (menuWindows.getItem(i) instanceof JMenuItemButton) {
				currentMenu = ((JMenuItemButton)menuWindows.getItem(i)).getTrueText();
			} else {
				currentMenu = menuWindows.getItem(i).getText();
			}
			if (currentMenu.equals(frameTitle)) {
				menuWindows.remove(i);
				return true;
			}
		}
		return false;
	}

	public JRadioButtonMenuItem getMenuItemMultipleSpectrum() {
		if (menuItemMultipleSpectrum == null) {
			menuItemMultipleSpectrum = new JRadioButtonMenuItem("Loomis Spectrum", false);
			menuItemMultipleSpectrum.addActionListener(abstractActionMenu.getMultipleSpectrumView());
		}
		return menuItemMultipleSpectrum;
	}

	/**
	 * Create the menu 'Scripts'.
	 *
	 * @return JMenu - the menu to display
	 */
	public JMenu getMenuScripts() {
		if (menuScripts == null) {
			menuScripts = new JMenu("Scripts");
			menuScripts.add(abstractActionMenu.getScriptsLteRadexAction());
		}
		return menuScripts;
	}

	public SendToMenu getSendToMenu() {
		if (sendToMenu == null) {
			sendToMenu = new SendToMenu();
		}
		return sendToMenu;
	}

	public JMenuItem getMenuItemDatabase() {
		if (menuItemDatabase == null) {
			menuItemDatabase = new JMenuItem(abstractActionMenu.getDatabaseAction());
		}
		return menuItemDatabase;
	}

	public JMenuItem getMenuItemInstallDatabase() {
		if (menuItemInstallDatabase == null) {
			menuItemInstallDatabase = new JMenuItem(abstractActionMenu.getInstallDatabaseAction());
		}
		return menuItemInstallDatabase;
	}
}
