/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Map;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.gui.plot.CassisViewInterface;
import eu.omp.irap.cassis.gui.plot.save.SaveGraphic;
import eu.omp.irap.cassis.vo.VoCassis;

/**
 * Manage the "Send To" menu in eu.omp.irap.cassis.
 *
 * @author Mickaël Boiziot
 *
 */
public class SendToMenu extends JMenu {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(SendToMenu.class);

	private CassisViewInterface cassisViewInterface;


	public SendToMenu() {
		super("Send to...");

		this.addMenuListener(new MenuListener() {

			@Override
			public void menuSelected(MenuEvent e) {
				removeOldActionListeners();
				removeAll();
				addClientsToMenu();
			}

			@Override
			public void menuDeselected(MenuEvent e) {
				// Nothing to do, only here as present in MenuListener interface.
			}

			@Override
			public void menuCanceled(MenuEvent e) {
				// Nothing to do, only here as present in MenuListener interface.
			}
		});
	}

	/**
	 * Add the clients (as item) to the menu.
	 */
	private void addClientsToMenu() {
		Map<String, String> items = VoCassis.getInstance().getVoTableClients();

		if (cassisViewInterface != null && cassisViewInterface.getSave().isSampSendPossible()) {
			if (items.size() > 1) {
				addClientToMenu("All", "All");
			}
			for (Map.Entry<String, String> voClient : items.entrySet()) {
				addClientToMenu(voClient.getValue(), voClient.getKey());
			}
		}
	}

	/**
	 * Add an JMenuItem for a client.
	 *
	 * @param clientName The name that should appear in the menu
	 * @param clientId The client id of the client.
	 */
	private void addClientToMenu(String clientName, final String clientId) {
		if (clientName == null || clientName.isEmpty() || clientId == null || clientId.isEmpty()) {
			return;
		}

		JMenuItem newClient = new JMenuItem(clientName);
		newClient.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				File file = null;
				try {
					file = File.createTempFile("cassis", ".votable");
				} catch (IOException ioe) {
					LOGGER.error("Error while creating file", ioe);
				}

				saveAndSendSamp(clientId, file);
			}
		});
		add(newClient);
	}

	/**
	 * Save the spectra, send them through SAMP and handle the result.
	 *
	 * @param clientId The clientId to send the plot.
	 * @param file The file were to save the plot.
	 */
	private void saveAndSendSamp(final String clientId, File file) {
		switch (cassisViewInterface.getSave().saveForSamp(file)) {
			case SaveGraphic.SUCCESS:
				handleSuccess(clientId, file);
				break;
			case SaveGraphic.ERROR:
				handleError(file);
				break;
			case SaveGraphic.CANCELED:
				deleteFile(file);
				break;
			case SaveGraphic.NOTHING_TO_SAVE:
				handleNothingToSave(file);
				break;
			default:
				break;
		}
	}

	/**
	 * Handle the action to do if the save was a success. This send the file
	 *  through SAMP and display an error message if the sending created an error.
	 *
	 * @param clientId The id of the SAMP client to send the file to.
	 * @param file The file to send.
	 */
	private void handleSuccess(final String clientId, File file) {
		file.deleteOnExit();
		if (!VoCassis.getInstance().sendCurrentVoTable(file, clientId)) {
			JOptionPane.showMessageDialog(null, "There was an error while sending the file through samp.",
					"Samp error", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Handle the action to do if there is an error. This display an error
	 *  message and delete the file.
	 *
	 * @param file The file.
	 */
	private void handleError(File file) {
		deleteFile(file);
		JOptionPane.showMessageDialog(null, "There was an error while file creation." +
				"\nSamp sending canceled.", "Cassis save error", JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * Handle the action to do if there is nothing to save. This display an error
	 *  message and delete the file.
	 *
	 * @param file The file.
	 */
	private void handleNothingToSave(File file) {
		deleteFile(file);
		JOptionPane.showMessageDialog(null, "There is not data to send through samp.",
				"Cassis save", JOptionPane.WARNING_MESSAGE);
	}

	/**
	 * Remove the old ActionListeners.
	 */
	private void removeOldActionListeners() {
		for (ActionListener listener : getActionListeners()) {
			removeActionListener(listener);
		}
	}

	/**
	 * Change the CassisViewInterface.
	 *
	 * @param cassisViewInterface The new CassisViewInterface to use.
	 */
	public void setCassisView(CassisViewInterface cassisViewInterface) {
		this.cassisViewInterface = cassisViewInterface;
	}

	/**
	 * Return the current {@link CassisViewInterface}.
	 *
	 * @return the current {@link CassisViewInterface}.
	 */
	public CassisViewInterface getCassisView() {
		return this.cassisViewInterface;
	}

	/**
	 * Delete given file if it exists, log it in case of failure.
	 *
	 * @param fileToDelete The file to delete.
	 */
	private void deleteFile(File fileToDelete) {
		if (fileToDelete != null && fileToDelete.exists() && !fileToDelete.delete()) {
			LOGGER.error("Unable to delete file '" + fileToDelete.getAbsolutePath() + "'.");
		}
	}

}
