/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.menu;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.properties.Software;

/**
 * @author thomas
 *
 */
public class OpenRecent {

	public static final int MAX_NB_FILES = 5;
	private static final Logger LOGGER = LoggerFactory.getLogger(OpenRecent.class);
	private List<String> openRecentList = new ArrayList<>();
	private File userRecent = new File(Software.getCassisPath() + File.separator +
			Software.PROPERTIES_FOLDER + File.separator + Software.USER_OPEN_RECENT_FILE);


	public OpenRecent() {
		if (userRecent.exists()) {
			boolean needRewrite = false;
			try (BufferedReader fileInputStream = new BufferedReader(
					new InputStreamReader(new FileInputStream(userRecent)))) {
				for (int i = 0; i < MAX_NB_FILES; i++) {
					String line = fileInputStream.readLine();
					if (line != null && !line.isEmpty()) {
						if (new File(line).exists()) {
							openRecentList.add(line);
						} else {
							needRewrite = true;
						}
					} else {
						break;
					}
				}
			} catch (IOException e) {
				LOGGER.debug("Can not load recent data", e);
			}
			if (needRewrite) {
				exportList();
			}
		}
	}

	/**
	 * @return the openRecentList
	 */
	public List<String> getOpenRecentList() {
		int size = openRecentList.size();
		boolean needRewrite = false;
		for (int i = size - 1; i >= 0; i--) {
			String path = openRecentList.get(i);
			if (!new File(path).exists()) {
				openRecentList.remove(i);
				needRewrite = true;
			}
		}
		if (needRewrite) {
			exportList();
		}
		return openRecentList;
	}

	/**
	 * Update the open recent list, adding the provided file.
	 *
	 * @param path the file to add to the openRecentList.
	 * @return the open recent list.
	 */
	public List<String> updateOpenRecentList(String path) {
		if (path != null) {
			openRecentList.remove(path);
			openRecentList.add(0, path);
			exportList();
		}
		return openRecentList;
	}

	/**
	 * Update the file openRecent Software.USER_OPEN_RECENT_FILE with
	 * the list of openRecentList
	 */
	public void exportList() {
		try (BufferedWriter out = new BufferedWriter(new FileWriter(userRecent))) {
			for (int i = 0; i < MAX_NB_FILES && i < openRecentList.size(); i++) {
				out.append(openRecentList.get(i));
				out.append("\n");
				out.flush();
			}
		} catch (IOException e) {
			LOGGER.debug("Can not save recent data", e);
		}
	}

}
