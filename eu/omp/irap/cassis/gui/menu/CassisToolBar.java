/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.menu;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;

import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JToolBar;
import javax.swing.TransferHandler;
import javax.swing.WindowConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.gui.PanelView;
import eu.omp.irap.cassis.gui.menu.action.OpenAction;
import eu.omp.irap.cassis.gui.menu.action.SpectrumManagerAction;
import eu.omp.irap.cassis.gui.util.CassisRessource;

/**
 * @author thomas
 *
 */
public class CassisToolBar extends JToolBar {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(CassisToolBar.class);

	private JButton buttonOpen;
	private JButton buttonSpectrumManager;
	private JButton buttonSave;
	private JButton buttonPrint;
	private JButton buttonSpectrumAnalysis;
	private JButton buttonLteRadex;
	private JButton buttonLoomisWood;
	private JButton buttonLineAnalysis;
	private JButton buttonRotationalDiagram;
	private JButton buttonTemplates;


	/**
	 * Constructor, create the CassisToolBar.
	 */
	@SuppressWarnings("serial")
	public CassisToolBar() {
		// Add some buttons to the toolbar
		buttonOpen = addToolbarButton("open", "Open an existing file", CassisRessource.getOpenIcon());
		buttonSpectrumManager = addToolbarButton("spectrumManager", "Spectrum Manager", CassisRessource.getSpectrumManagerIcon());
		buttonSave = addToolbarButton("save", "Save the current view", CassisRessource.getSaveIcon());
		buttonPrint = addToolbarButton("print", "Print the current view", CassisRessource.getPrintIcon());
		buttonSpectrumAnalysis = addToolbarButton("spectrum", "Spectrum Analysis", CassisRessource.getSpectrumIcon());
		buttonLteRadex = addToolbarButton("radex", "LTE Radex", CassisRessource.getRadexIcon());
		buttonLoomisWood = addToolbarButton("loomiswood", "Loomis Wood", CassisRessource.getLoomisWoodIcon());
		buttonLineAnalysis = addToolbarButton("line", "Line Analysis", CassisRessource.getLineIcon());
		buttonRotationalDiagram = addToolbarButton("rotational", "Rotational Diagram", CassisRessource.getRotationalIcon());
		buttonTemplates = addToolbarButton("template", "Manage Templates", CassisRessource.getTemplateIcon());

		addAlternativeToolbarButton(PanelView.getInstance().getCassisActionMenu().getScriptsLteRadexAction());

		buttonOpen.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PanelView.getInstance().getCassisActionMenu().getOpenAction().actionPerformed(e);
			}
		});

		buttonOpen.setTransferHandler(new TransferHandler() {
			/**
			 * @see
			 * javax.swing.TransferHandler#canImport(javax.swing.JComponent,
			 * java.awt.datatransfer.DataFlavor[])
			 */
			@Override
			public boolean canImport(JComponent comp, DataFlavor[] transferFlavors) {
				for (int i = 0; i < transferFlavors.length; i++) {
					if (transferFlavors[i].equals(DataFlavor.stringFlavor)) {
						return true;
					}
				}
				return false;
			}

			/**
			 * @see
			 * javax.swing.TransferHandler#importData(javax.swing.JComponent,
			 * java.awt.datatransfer.Transferable)
			 */
			@Override
			public boolean importData(JComponent comp, Transferable t) {
				DataFlavor[] flavors = t.getTransferDataFlavors();
				for (int i = 0; i < flavors.length; i++) {
					DataFlavor flavor = flavors[i];
					try {
						if (flavor.equals(DataFlavor.stringFlavor)) {
							String fileOrURL = (String) t.getTransferData(flavor);
							try {
								URL url = new URL(fileOrURL);
								String path = URLDecoder.decode(url.toString(), "UTF-8");
								path = path.substring(5, path.length());

								String file = OpenAction.openCassisFile(new File(path));
								if (file != null) {
									PanelView.getInstance().getCassisActionMenu().getOpenAction().getOpenControl()
											.fireNewCassisFileOpen(file);
								}
							} catch (Exception e) {
								LOGGER.error("Error while importing data", e);
							}
						}
					} catch (IOException ioe) {
						LOGGER.error("IOError getting data", ioe);
					} catch (UnsupportedFlavorException ufe) {
						LOGGER.error("Unsupported flavor", ufe);
					}
				}
				// If you get here, I didn't like the flavor.
				Toolkit.getDefaultToolkit().beep();
				return false;
			}
		});

		buttonSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PanelView.getInstance().getCassisActionMenu().getSaveAction().savePlot();
			}
		});

		buttonPrint.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PanelView.getInstance().getCassisActionMenu().getPrintAction().getPrintAction();
			}
		});

		buttonSpectrumAnalysis.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PanelView.getInstance().getCassisActionMenu().getSpectrumAnalysisAction().getSpectrumAnalysisAction();
			}
		});

		buttonLteRadex.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PanelView.getInstance().getCassisActionMenu().getLteRadexAction().getLteRadexAction();
			}
		});

		buttonLoomisWood.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				 PanelView.getInstance().getCassisActionMenu().getmenuItemLoomisWoodAction().getLoomisWoodAction();
			}
		});

		buttonLineAnalysis.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PanelView.getInstance().getCassisActionMenu().getLineAnalysisModelAction().getLineAnalysisModelAction();
			}
		});

		buttonRotationalDiagram.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PanelView.getInstance().getCassisActionMenu().getRotationalDiagramAction().getRotationalDiagramAction();
			}
		});

		buttonTemplates.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PanelView.getInstance().addManageTemplateFrame();
			}
		});

		buttonSpectrumManager.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
//				PanelView.getInstance().getCassisActionMenu().getOpenAction();
				SpectrumManagerAction.getInstance().setNormal();
			}
		});
	}

	// Helper method to create new toolbar buttons.
	public JButton addToolbarButton(String sButton, String sToolHelp, Image icon) {
		// Create a new button
		JButton b = new JButton(new ImageIcon(icon));

		// Add the button to the toolbar
		add(b);

		// Only a graphic, so make the button smaller
		b.setMargin(new Insets(0, 0, 0, 0));

		// Add optional tooltip help
		b.setToolTipText(sToolHelp);

		// Make sure this button sends a message when the user clicks it
		b.setActionCommand("Toolbar:" + sButton);
		return b;
	}


	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setSize(new Dimension(400, 130));
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		frame.setVisible(true);
		frame.getContentPane().add(new CassisToolBar(), BorderLayout.NORTH);
	}

	public void addAlternativeToolbarButton(Action action) {
		JButton btn = new JButton(action);
		if (btn.getIcon() != null) {
			btn.setText("");
		}
		btn.setMargin(new Insets(0, 0, 0, 0));
		add(btn);
	}

}
