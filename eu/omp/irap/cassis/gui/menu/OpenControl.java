/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.menu;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import eu.omp.irap.cassis.gui.menu.action.OpenAction;

/**
 * @author thomas
 *
 */
public class OpenControl {

	private JMenu jMenu;
	private OpenRecent openRecent;


	public OpenControl(JMenu jMenu, OpenRecent openRecent) {
		this.jMenu = jMenu;
		this.openRecent = openRecent;
		refreshItems();
	}

	public void fireNewCassisFileOpen(String path) {
		openRecent.updateOpenRecentList(path);
	}

	@SuppressWarnings("serial")
	public void refreshItems() {
		List<String> list = openRecent.getOpenRecentList();
		jMenu.removeAll();
		if (list.isEmpty()) {
			JMenuItem none = new JMenuItem("None");
			none.setEnabled(false);
			jMenu.add(none);
		} else {
			for (int i = 0; (i < list.size() && i < OpenRecent.MAX_NB_FILES); i++) {
				final String pathFile = list.get(i);
				JMenuItem item = new JMenuItem();
				item.setAction(new AbstractAction(new File(pathFile).getName()) {
					@Override
					public void actionPerformed(ActionEvent e) {
						String file = OpenAction.openCassisFile(new File(pathFile));
						if (file != null) {
							OpenControl.this.fireNewCassisFileOpen(pathFile);
						}
					}
				});
				jMenu.add(item);
				jMenu.validate();
			}
		}
	}
}
