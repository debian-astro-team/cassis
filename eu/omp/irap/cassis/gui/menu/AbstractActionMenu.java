/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui.menu;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.JMenu;

import eu.omp.irap.cassis.gui.plot.CassisViewInterface;

/**
 * @author thomas
 *
 */
public abstract class AbstractActionMenu {

	protected JFrame owner;
	protected MenuManagerInterface manager;
	protected CassisViewInterface cassisViewInterface;

	protected AbstractActionMenu(JFrame owner, MenuManagerInterface manager, CassisViewInterface cassisViewInterface) {
		this.owner = owner;
		this.manager = manager;
		this.cassisViewInterface = cassisViewInterface;
	}

	public abstract Action getHelpUpdateAction();

	public abstract Action getHelpAboutAction();

	public abstract Action getHelpUsersManualAction();

	public abstract Action getConfigurationAction();

	public abstract Action getDatabaseConfigurationAction();

	public abstract Action getRadexConfigurationAction();

	public abstract Action getFitConfigurationAction();

	public abstract AbstractAction getCutAction();

	public abstract AbstractAction getCopyAction();

	public abstract AbstractAction getPasteAction();

	public abstract Action getRotationalDiagramAction();

	public abstract Action getLineAnalysisModelAction();

	public abstract Action getLteRadexAction();

	public abstract Action getSpectrumAnalysisAction();

	public abstract Action getManageTemplateAction();

	public abstract Action getFullSpectrumView();

	public abstract Action getRotationalDiagramViewAction();

	public abstract Action getFullLineViewAction();

	public abstract Action getOpenAction();

	public abstract Action getOpenUrlAction();

	public abstract Action getExitAction();

	public abstract AbstractAction getCaptureAction(JMenu menuFile);

	public abstract AbstractAction getPrintAction();

	public abstract AbstractAction getSaveAction();

	public abstract AbstractAction getSaveLineAction();

	public void setView(CassisViewInterface cassisViewInterface) {
		this.cassisViewInterface = cassisViewInterface;
	}

	public abstract AbstractAction getCaptureAction();

	public abstract AbstractAction getVOCreateAction();

	public abstract AbstractAction getVOConnectAction();

	public abstract AbstractAction getVODisConnectAction();

	public abstract Action getmenuItemLoomisWoodAction();

	public abstract Action getMultipleSpectrumView();

	public abstract Action getScriptsLteRadexAction();

	public abstract Action getSsapAction();

	public abstract Action getCassisFormalismAction();

	public abstract Action getImportFilesAction();

	public abstract Action getDatabaseAction();

	public abstract Action getInstallDatabaseAction();

	public abstract Action getSpectrumManagerAction();

	public abstract Action getEpnTapAction();

	public abstract Action getLogAction();
}
