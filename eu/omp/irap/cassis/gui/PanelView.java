/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.gui.ScreenUtil;
import eu.omp.irap.cassis.gui.fit.advanced.gui.AdvancedFitFrame;
import eu.omp.irap.cassis.gui.menu.CassisActionMenu;
import eu.omp.irap.cassis.gui.menu.JMenuCassisBar;
import eu.omp.irap.cassis.gui.menu.MenuManagerInterface;
import eu.omp.irap.cassis.gui.menu.SendToMenu;
import eu.omp.irap.cassis.gui.menu.action.SaveAction;
import eu.omp.irap.cassis.gui.menu.action.SaveLineAction;
import eu.omp.irap.cassis.gui.menu.action.SpectrumManagerAction;
import eu.omp.irap.cassis.gui.model.CassisModel;
import eu.omp.irap.cassis.gui.model.lineanalysis.LineAnalysisControl;
import eu.omp.irap.cassis.gui.model.lineanalysis.LineAnalysisDisplayEvent;
import eu.omp.irap.cassis.gui.model.lineanalysis.LineAnalysisDisplayListener;
import eu.omp.irap.cassis.gui.model.lineanalysis.LineAnalysisPanel;
import eu.omp.irap.cassis.gui.model.loomisanalysis.LoomisWoodControl;
import eu.omp.irap.cassis.gui.model.loomisanalysis.LoomisWoodListener;
import eu.omp.irap.cassis.gui.model.loomisanalysis.LoomisWoodPanel;
import eu.omp.irap.cassis.gui.model.loomisanalysis.LoomisWoodResult;
import eu.omp.irap.cassis.gui.model.lteradex.LteRadexControl;
import eu.omp.irap.cassis.gui.model.lteradex.LteRadexPanel;
import eu.omp.irap.cassis.gui.model.parameter.data.LoadDataModel;
import eu.omp.irap.cassis.gui.model.rotationaldiagram.RotationalDiagramControl;
import eu.omp.irap.cassis.gui.model.rotationaldiagram.RotationalDiagramDisplayListener;
import eu.omp.irap.cassis.gui.model.rotationaldiagram.RotationalDiagramPanel;
import eu.omp.irap.cassis.gui.model.spectrumanalysis.SpectrumAnalysisControl;
import eu.omp.irap.cassis.gui.model.spectrumanalysis.SpectrumAnalysisModel;
import eu.omp.irap.cassis.gui.model.spectrumanalysis.SpectrumAnalysisPanel;
import eu.omp.irap.cassis.gui.model.table.CassisTableDisplayEvent;
import eu.omp.irap.cassis.gui.model.table.CassisTableDisplayListener;
import eu.omp.irap.cassis.gui.model.table.CassisTableViewInterface;
import eu.omp.irap.cassis.gui.model.table.ModelIdentifiedInterface;
import eu.omp.irap.cassis.gui.plot.CassisViewInterface;
import eu.omp.irap.cassis.gui.plot.curve.XYSeriesCassis;
import eu.omp.irap.cassis.gui.plot.full.FullSpectrumControl;
import eu.omp.irap.cassis.gui.plot.full.FullSpectrumListener;
import eu.omp.irap.cassis.gui.plot.full.FullSpectrumModel;
import eu.omp.irap.cassis.gui.plot.full.FullSpectrumView;
import eu.omp.irap.cassis.gui.plot.line.LineSpectrumView;
import eu.omp.irap.cassis.gui.plot.multi.MultiSpectrumView;
import eu.omp.irap.cassis.gui.plot.rotdiagram.RotationalView;
import eu.omp.irap.cassis.gui.template.ManageTemplateControl;
import eu.omp.irap.cassis.gui.template.ManageTemplateModel;
import eu.omp.irap.cassis.gui.util.CassisRessource;
import eu.omp.irap.cassis.gui.util.Deletable;
import eu.omp.irap.cassis.gui.util.JMenuItemButton;
import eu.omp.irap.cassis.lineanalysis.LineAnalysisResult;
import eu.omp.irap.cassis.parameters.RotationalDiagramResult;
import eu.omp.irap.mac.MacHandling;

/**
 * View of Cassis.
 *
 * @author thomas
 * @author glorian
 *
 */
public class PanelView extends JPanel implements FullSpectrumListener,
		CassisTableDisplayListener, MenuManagerInterface {

	private static final long serialVersionUID = -994115442835421996L;

	private static PanelView panelView = null;

	private int numWindowLteRadex = 0;
	private int numWindowSpectrumAnalysis = 0;
	private int numWindowLoomisWood = 0;
	private int numWindowLineAnalysis = 0;
	private int numWindowRotationalDiagram = 0;

	// The views of the tabs
	private FullSpectrumView fullSpectrumView;
	private LineSpectrumView lineSpectrumView;
	private RotationalView rotationalView;
	private MultiSpectrumView multipleSpectrumView;

	private JViewTabbedPane tabManager;

	// list of the different frame/table in order to display them in the full
	// spectrum
	private Map<Integer, JFrame> listOfFrame;

	private PanelFrame mainFrame;

	private SaveAction saveAction;
	private SaveLineAction saveLineAction;
	private SendToMenu sendToMenu;

	private CassisActionMenu cassisActionMenu;

	private transient WindowListener windowClosingListener;

	private JMenuCassisBar menuCassisBar;

	private JSeparator separatorPanel;


	private PanelView(PanelFrame frame) {
		setLayout(new BorderLayout());
		getRotationalView();
		mainFrame = frame;
		getFullSpectrumView();
		getLineSpectrumView();
		multipleSpectrumView = new MultiSpectrumView();
		listOfFrame = new HashMap<>();
		handleMac();
		add(getSeparatorPanel(), BorderLayout.CENTER);
	}

	public static PanelView getInstance(PanelFrame frame) {
		if (panelView == null) {
			panelView = new PanelView(frame);
		}
		return panelView;
	}

	public static void clearPanelViewInstance() {
		panelView = null;
	}

	public static PanelView getInstance() {
		return panelView;
	}

	public RotationalView getRotationalView() {
		if (rotationalView == null) {
			rotationalView = new RotationalView();
		}
		return rotationalView;
	}

	public LineSpectrumView getLineSpectrumView() {
		if (lineSpectrumView == null) {
			lineSpectrumView = new LineSpectrumView();
		}
		return lineSpectrumView;
	}

	public int getNumWindowLineAnalysis() {
		return numWindowLineAnalysis;
	}

	public FullSpectrumView getFullSpectrumView() {
		if (fullSpectrumView == null) {
			FullSpectrumModel fullSpectrumModel = new FullSpectrumModel();
			FullSpectrumControl fullSpectrumControl = new FullSpectrumControl(
					fullSpectrumModel);
			fullSpectrumView = fullSpectrumControl.getView();
			fullSpectrumControl.addFullSpectrumListener(this);
		}
		return fullSpectrumView;
	}

	public JSeparator getSeparatorPanel() {
		if (separatorPanel == null) {
			// Creation of the separator and adding the tab manager
			separatorPanel = new JSeparator();
			separatorPanel.setLayout(new BorderLayout());
			separatorPanel.add(getTabManager(), BorderLayout.CENTER);
		}
		return separatorPanel;
	}

	public JViewTabbedPane getTabManager() {
		if (tabManager == null) {
			fullSpectrumView.getComponent().setVisible(false);
			lineSpectrumView.getComponent().setVisible(false);
			rotationalView.getComponent().setVisible(false);
			multipleSpectrumView.getComponent().setVisible(false);
			tabManager = new JViewTabbedPane();

			tabManager.addChangeListener(new ChangeListener() {
				@Override
				public void stateChanged(ChangeEvent e) {
					if (tabManager.getSelectedComponent() == null) {
						return;
					}
					handleTabSelection();
				}
			});
		}
		return tabManager;
	}

	/**
	 * Handle the action to do on the selection of a tab.
	 */
	private void handleTabSelection() {
		if (tabManager.getSelectedComponent().equals(fullSpectrumView)) {
			setAction(fullSpectrumView);
			cassisActionMenu.getSaveLineAction().setEnabled(true);
			menuCassisBar.getMenuItemFullSpectrum().setSelected(true);
			AdvancedFitFrame.getFrame().displayModel(fullSpectrumView.getModel().getCurrentFitModel());
			fullSpectrumView.getCreationPanel().refreshRms();
		} else if (tabManager.getSelectedComponent().equals(lineSpectrumView)) {
			setAction(lineSpectrumView);
			cassisActionMenu.getSaveLineAction().setEnabled(true);
			menuCassisBar.getMenuItemLineSpectrum().setSelected(true);
			AdvancedFitFrame.getFrame().displayModel(lineSpectrumView.getControl().getModel().getCurrentFitModel());
			if (lineSpectrumView.getControl().isFittable()) {
				AdvancedFitFrame.getFrame().openFrame(true);
				lineSpectrumView.getCreationPanel().refreshRms();
			}
		} else if (tabManager.getSelectedComponent().equals(rotationalView)) {
			setAction(rotationalView);
			cassisActionMenu.getSaveLineAction().setEnabled(false);
			menuCassisBar.getMenuItemRotationalDiagram().setSelected(true);
			AdvancedFitFrame.getFrame().openFrame(false);
		} else if (tabManager.getSelectedComponent().equals(
				multipleSpectrumView)) {
			setAction(multipleSpectrumView);
			cassisActionMenu.getSaveLineAction().setEnabled(false);
			menuCassisBar.getMenuItemMultipleSpectrum().setSelected(true);
			AdvancedFitFrame.getFrame().displayModel(multipleSpectrumView.getControl().getModel().getCurrentFitModel());
			if (multipleSpectrumView.getControl().isFittable()) {
				AdvancedFitFrame.getFrame().openFrame(true);
				multipleSpectrumView.getCreationPanel().refreshRms();
			}
		}
	}

	/**
	 * Put the correction CassisViewInterface on the needed actions.
	 *
	 * @param cassisViewInterface The interface to set.
	 */
	private void setAction(CassisViewInterface cassisViewInterface) {
		cassisActionMenu.getCaptureAction().setCassisView(cassisViewInterface);
		cassisActionMenu.getPrintAction().setCassisView(cassisViewInterface);
		saveAction.setCassisView(cassisViewInterface);
		saveLineAction.setCassisView(cassisViewInterface);
		sendToMenu.setCassisView(cassisViewInterface);
		menuCassisBar.getMenuItemFullSpectrum().setSelected(true);
	}
	/**
	 * Return the list of differents frames which are opened.
	 *
	 * @return the list of differents frames which are opened.
	 */
	public Map<Integer, JFrame> getListOfFrame() {
		return listOfFrame;
	}

	/**
	 * Build the MenuBar.
	 *
	 * @return JMenuBar
	 */
	public JMenuCassisBar getMenuBar() {
		if (menuCassisBar == null) {
			saveAction = getCassisActionMenu().getSaveAction();
			saveLineAction = getCassisActionMenu().getSaveLineAction();
			menuCassisBar = new JMenuCassisBar(getCassisActionMenu(), true);
			sendToMenu = menuCassisBar.getSendToMenu();
		}
		return menuCassisBar;
	}

	public CassisActionMenu getCassisActionMenu() {
		if (cassisActionMenu == null) {
			cassisActionMenu = new CassisActionMenu(mainFrame, this,
					fullSpectrumView);
			cassisActionMenu.initScriptActions(mainFrame);
		}
		return cassisActionMenu;
	}

	/**
	 * Add a LTE + Radex Tab pane in applet.
	 *
	 * @param lteRadexPanel The lteRadex panel.
	 * @param visible If the panel should be visible.
	 * @return The controller of the lteRadex.
	 */
	public LteRadexControl addLteRadexView(LteRadexPanel lteRadexPanel, boolean visible) {
		LteRadexControl control = lteRadexPanel.getControl();
		control.getListeners().addCassisTableDisplayListener(this);
		numWindowLteRadex++;
		createFrameModel(lteRadexPanel.getControl().getModel(), "LTE + RADEX "
				+ numWindowLteRadex, lteRadexPanel, control, visible,
				CassisRessource.getRadexIcon());

		return control;
	}

	@Override
	public LteRadexControl addLteRadexView() {
		LteRadexPanel panel = new LteRadexPanel();
		return addLteRadexView(panel, true);
	}

	/**
	 * Add a File Table pane in applet.
	 */
	@Override
	public SpectrumAnalysisControl addSpectrumAnalysisView(double fmin, double fmax,
			XAxisCassis xAxisCassis) {
		SpectrumAnalysisPanel panel = new SpectrumAnalysisPanel();

		// TODO A revoir
		SpectrumAnalysisModel model = panel.getControl().getModel();
		model.setBandUnit(xAxisCassis.getUnit());
		model.setValMin(fmin);
		model.setValMax(fmax);
		panel.getControl().getListeners().addCassisTableDisplayListener(this);
		numWindowSpectrumAnalysis++;

		JFrame frame = createFrameModel(model,
				"Spectrum Analysis " + numWindowSpectrumAnalysis, panel,
				null, CassisRessource.getSpectrumIcon());
		addLoadActionListener(frame, panel.getLoadDataPanel().getModel(),
				panel.getLoadDataPanel().getLoadButton(), frame.getTitle());

		return panel.getControl();
	}

	/**
	 * Add a LTeLine table window in cassis.
	 */
	@Override
	public LineAnalysisControl addLtelineView() {
		LineAnalysisPanel lteLineTablePanel = new LineAnalysisPanel();
		return addLtelineView(lteLineTablePanel, true);
	}

	public LineAnalysisControl addLtelineView(LineAnalysisPanel panel, boolean visible) {
		panel.getControl().addLteLineTableListener(new LineAnalysisDisplayListener() {
			@Override
			public void lteLineTableDisplayClicked(LineAnalysisDisplayEvent e) {
				displayLineAnalysis(e.getLteLineResult());
			}
		});

		final LineAnalysisControl control = panel.getControl();
		numWindowLineAnalysis++;
		final JFrame frame = new JFrame(
				"Line Analysis " + numWindowLineAnalysis);
		frame.setContentPane(panel);
		frame.setMinimumSize(new Dimension(980, 600));
		frame.setPreferredSize(new Dimension(980, 600));
		frame.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		frame.setSize(1016, 680);
		frame.setIconImage(CassisRessource.getLineIcon());
		frame.setVisible(visible);
		ScreenUtil.center(mainFrame, frame);
		addLoadActionListener(frame, panel.getLoadDataPanel().getModel(),
				panel.getLoadDataPanel().getLoadButton(), frame.getTitle());
		frame.addWindowListener(getWindowClosing());

		final JMenuItemButton menuItem = new JMenuItemButton(frame.getTitle());
		menuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PanelView.setToFront(frame);
			}
		});
		menuItem.addDeleteButtonListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				getMenuBar().getMenuWindows().remove(menuItem);
				control.delete();
				frame.dispose();
			}
		});
		getMenuBar().addMenuWindows(menuItem);

		return panel.getControl();
	}

	private JFrame createFrameModel(ModelIdentifiedInterface model, String title,
			JPanel panel, Deletable deletable, Image icon) {
		return createFrameModel(model, title, panel, deletable, true, icon);
	}

	private JFrame createFrameModel(ModelIdentifiedInterface model, String title,
			JPanel panel, final Deletable deletable, boolean show, Image icon) {
		final JFrame frame = new JFrame(title);
		final int id = CassisModel.setNewModelId();
		listOfFrame.put(id, frame);
		model.setModelId(id);
		frame.setContentPane(panel);
		frame.pack();
		frame.setVisible(show);
		ScreenUtil.center(mainFrame, frame);
		if (icon != null) {
			frame.setIconImage(icon);
		} else {
			frame.setIconImage(CassisRessource.getCassisIcon());
		}
		frame.setName(title);
		model.setName(title);
		frame.addWindowListener(getWindowClosing());

		final JMenuItemButton menuItem = new JMenuItemButton(title);
		menuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PanelView.setToFront(frame);
			}
		});
		menuItem.addDeleteButtonListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				getMenuBar().getMenuWindows().remove(menuItem);
				getFullSpectrumView().getControl().removeJPanelCurve(frame.getTitle());
				listOfFrame.remove(id);
				if (deletable != null) {
					deletable.delete();
				}
				frame.dispose();
			}
		});
		getMenuBar().addMenuWindows(menuItem);
		return frame;
	}

	/**
	 * Select spectrum tab in view.
	 */
	@Override
	public FullSpectrumView switchToSpectrumTab() {
		tabManager.setSelectedComponent(fullSpectrumView);
		cassisActionMenu.getSaveLineAction().setEnabled(true);
		return fullSpectrumView;
	}

	/**
	 * Select molecule tab in view.
	 */
	@Override
	public LineSpectrumView switchToMoleculeTab() {
		tabManager.setSelectedComponent(lineSpectrumView);
		cassisActionMenu.getSaveLineAction().setEnabled(true);
		return lineSpectrumView;
	}

	/**
	 * Select rotational tab in view.
	 */
	@Override
	public RotationalView switchToRotationalTab() {
		tabManager.setSelectedComponent(rotationalView);
		cassisActionMenu.getSaveLineAction().setEnabled(false);
		return rotationalView;
	}

	/**
	 * Display the model of the table frame on the FullSpectrum.
	 *
	 * @param fileModel
	 * @param tableModel
	 */
	private void displayModel(CassisModel fileModel, ModelIdentifiedInterface tableModel,
			String nameFrame) {
		/* create the file plot and display it on the chart */
		fileModel.setModelId(tableModel.getModelId());
		fileModel.setConfigName(nameFrame);
		// refresh the plot by the new plot
		fullSpectrumView.getControl().createFileplot(fileModel);

		// change of tab
		switchToSpectrumTab();
		PanelView.setToFront(mainFrame);
	}

	/**
	 * When the table frame are closed, the plot corresponding to the table is
	 * deleted.
	 */
	private WindowListener getWindowClosing() {
		if (windowClosingListener == null) {
			windowClosingListener = new WindowAdapter() {
				@Override
				public void windowClosing(WindowEvent e) {
					if (e.getSource() instanceof JFrame
							&& e.getSource() instanceof CassisTableViewInterface) {
						JFrame frame = (JFrame) e.getSource();
						CassisTableViewInterface tableView = (CassisTableViewInterface) e
								.getSource();

						if (tableView.getModel().isIdentified())
							frame.setVisible(false);
						else {
							getMenuBar().removeWindowMenuItem(frame);
							frame.dispose();
						}
					}
				}
			};
		}
		return windowClosingListener;
	}

	@Override
	public ManageTemplateControl addManageTemplateFrame() {
		ManageTemplateModel manageTemplateModel = new ManageTemplateModel();
		ManageTemplateControl control = ManageTemplateControl
				.getInstance(manageTemplateModel);
		JFrame templateFrame = control.getManageTemplateControlFrame();
		templateFrame.setIconImage(CassisRessource.getTemplateIcon());
		templateFrame.setVisible(true);
		ScreenUtil.center(mainFrame, templateFrame);
		PanelView.setToFront(templateFrame);
		return control;
	}

	public void displayLineAnalysis(LineAnalysisResult lteLineResult) {
		switchToMoleculeTab();
		PanelView.setToFront(mainFrame);
		lineSpectrumView.displayResult(lteLineResult);
	}

	@Override
	public void fullSpectrumXYPlotLeftMouseCliked(EventObject event) {
		if (event.getSource() instanceof XYSeriesCassis) {
			XYSeriesCassis fullSeries = (XYSeriesCassis) event.getSource();
			JFrame frame = getListOfFrame().get(fullSeries.getId());
			if (frame != null) {
				PanelView.setToFront(mainFrame);
			}
		}
	}

	@Override
	public void fullSpectrumXYPlotRemoved(EventObject event) {
		if (event.getSource() instanceof XYSeriesCassis) {
			XYSeriesCassis fullSeries = (XYSeriesCassis) event.getSource();
			int id = fullSeries.getId();
			JFrame frame = getListOfFrame().remove(id);
			if (frame != null) {
				getMenuBar().removeWindowMenuItem(frame);// remove the menu
				frame.dispose();// dispose the frame
			}
		}
	}

	@Override
	public void cassisTableDisplayClicked(CassisTableDisplayEvent event) {
		displayModel(event.getCassisModel(), event.getModel(), event.getModel().getName());
	}

	@Override
	public RotationalDiagramControl addRotationalDiagramView() {
		RotationalDiagramPanel rotPanel = new RotationalDiagramPanel();

		numWindowRotationalDiagram++;
		int num = numWindowRotationalDiagram;
		final JFrame frame = new JFrame("Rotational Diagram " + num);
		frame.setIconImage(CassisRessource.getLoomisWoodIcon());
		frame.setContentPane(rotPanel);
		frame.addWindowListener(getWindowClosing());
		frame.pack();
		frame.setVisible(true);
		ScreenUtil.center(mainFrame, frame);
		rotPanel.setFrame(frame);

		rotPanel.getControl().addDisplayListener(new RotationalDiagramDisplayListener() {
			@Override
			public void rotationalDisplayClicked(RotationalDiagramResult res) {
				switchToRotationalTab();
				PanelView.setToFront(mainFrame);
				getRotationalView().displayResult(res);
			}
		});
		final JMenuItemButton rotationalDiagramMenu = new JMenuItemButton(frame.getTitle());
		rotationalDiagramMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PanelView.setToFront(frame);
			}
		});
		rotationalDiagramMenu.addDeleteButtonListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				getMenuBar().getMenuWindows().remove(rotationalDiagramMenu);
				frame.dispose();
			}
		});
		getMenuBar().addMenuWindows(rotationalDiagramMenu);

		return rotPanel.getControl();
	}

	@Override
	public MultiSpectrumView switchToMultipleViewTab() {
		tabManager.setSelectedComponent(multipleSpectrumView);
		cassisActionMenu.getSaveLineAction().setEnabled(true);
		return multipleSpectrumView;
	}

	@Override
	public LoomisWoodControl addLoomisView() {
		LoomisWoodPanel panel = new LoomisWoodPanel();
		JButton loadButton = panel.getLoadDataPanel().getLoadButton();

		final LoomisWoodControl control = panel.getControl();
		control.addLoomisListener(new LoomisWoodListener() {
			@Override
			public void setLoomisModelResult(LoomisWoodResult loomisWoodResult) {
				switchToMultipleViewTab();
				PanelView.setToFront(mainFrame);
				multipleSpectrumView.getControl().setLoomisModelResult(loomisWoodResult);
			}
		});
		// Create File Table view
		numWindowLoomisWood++;
		int num = numWindowLoomisWood;
		final JFrame frame = new JFrame("Loomis Wood " + num);
		frame.setIconImage(CassisRessource.getLoomisWoodIcon());
		frame.setContentPane(panel);
		frame.addWindowListener(getWindowClosing());
		frame.pack();
		frame.setVisible(true);
		ScreenUtil.center(mainFrame, frame);
		addLoadActionListener(frame, panel.getLoadDataPanel().getModel(), loadButton, frame.getTitle());

		final JMenuItemButton loomisWoodMenu = new JMenuItemButton(frame.getTitle());
		loomisWoodMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PanelView.setToFront(frame);
			}
		});
		loomisWoodMenu.addDeleteButtonListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				getMenuBar().getMenuWindows().remove(loomisWoodMenu);
				control.delete();
				frame.dispose();
			}
		});
		getMenuBar().addMenuWindows(loomisWoodMenu);

		return control;
	}

	private static void setToFront(final JFrame frame) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				frame.setVisible(true);
				frame.toFront();
				frame.repaint();
			}
		});
	}

	/**
	 * Add an ActionListener on Load {@link JButton}.
	 *
	 * @param frame The {@link JFrame}.
	 * @param ldm The {@link LoadDataModel}.
	 * @param loadButton The "Load" {@link Button}.
	 * @param frameTitle The title of the frame.
	 */
	private static void addLoadActionListener(final JFrame frame, final LoadDataModel ldm,
			JButton loadButton, final String frameTitle) {
		loadButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Runnable cmd = new Runnable() {
					@Override
					public void run() {
						CassisSpectrum cs = SpectrumManagerAction.getInstance().getSelectedSpectrum();
						if (cs == null) {
							JOptionPane.showMessageDialog(null,
									"Please select a spectrum first.",
									"No spectrum selected!",
									JOptionPane.WARNING_MESSAGE);
						} else {
							ldm.setNameData(SpectrumManagerAction.getInstance().getPathOrName(cs));
							ldm.setCassisSpectrum(cs);
							setToFront(frame);
						}
					}
				};
				SpectrumManagerAction.getInstance().setLoadBinding(cmd, frameTitle);
			}
		});
	}

	private void handleMac() {
		if (MacHandling.getInstance().isSupported()) {
			PopupMenu menu = new PopupMenu();

			MenuItem saItem = new MenuItem("Spectrum Analysis");
			saItem.addActionListener(getCassisActionMenu().getSpectrumAnalysisAction());

			MenuItem lteRadexItem = new MenuItem("LTE + RADEX");
			lteRadexItem.addActionListener(getCassisActionMenu().getLteRadexAction());

			MenuItem loomisItem = new MenuItem("Loomis Wood");
			loomisItem.addActionListener(getCassisActionMenu().getmenuItemLoomisWoodAction());

			MenuItem laItem = new MenuItem("Line Analysis");
			laItem.addActionListener(getCassisActionMenu().getLineAnalysisModelAction());

			MenuItem rdItem = new MenuItem("Rotational Diagram");
			rdItem.addActionListener(getCassisActionMenu().getRotationalDiagramAction());


			MenuItem smItem = new MenuItem("Spectrum Manager");
			smItem.addActionListener(getCassisActionMenu().getSpectrumManagerAction());

			MenuItem ssaItem = new MenuItem("SSA Query");
			ssaItem.addActionListener(getCassisActionMenu().getSsapAction());

			MenuItem jythonItem = new MenuItem("Jython");
			jythonItem.addActionListener(getCassisActionMenu().getScriptsLteRadexAction());

			menu.add(saItem);
			menu.add(lteRadexItem);
			menu.add(loomisItem);
			menu.add(laItem);
			menu.add(rdItem);
			menu.add(ssaItem);
			menu.add(jythonItem);

			MacHandling.getInstance().setDockMenu(menu);
		}
	}
}
