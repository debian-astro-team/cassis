/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JWindow;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.gui.util.CassisRessource;

public class SplashWindow extends Thread {

	private static final Logger LOGGER = LoggerFactory.getLogger(SplashWindow.class);

	private JProgressBar progressBar = null;
	private int maxValue = 0;
	private JWindow window;
	private AtomicBoolean abort;


	public SplashWindow(Frame f, int intProgressMaxValue) {
		super();
		window = new JWindow(f);

		abort = new AtomicBoolean(false);

		// initialise la valeur a laquelle le splash screen doit etre fermé
		this.maxValue = intProgressMaxValue;

		// ajoute la progress bar
		progressBar = new JProgressBar(0, intProgressMaxValue);
		window.getContentPane().add(progressBar, BorderLayout.SOUTH);

		// cree un label avec notre image
		JLabel image = new JLabel(CassisRessource.getSplashImage());
		// ajoute le label au panel
		window.getContentPane().add(image, BorderLayout.CENTER);
		window.pack();

		Dimension labelSize = image.getPreferredSize();
		int width = f.getWidth() / 2;
		int height = f.getHeight() / 2;

		window.setLocation(width - labelSize.width / 2, height - labelSize.height / 2);

		window.setVisible(true);
	}

	/**
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {
		while (!abort.get()) {
			for (int cpt = 0; cpt < maxValue; cpt++) {
				progressBar.setValue(cpt);
				progressBar.repaint();
				try {
					Thread.sleep(250);
				} catch (InterruptedException e) {
					LOGGER.warn("Error during the sleep", e);
				}
			}
		}
		close();
	}

	public void close() {
		if (SwingUtilities.isEventDispatchThread()) {
			closerRunner.run();
		} else {
			try {
				SwingUtilities.invokeAndWait(closerRunner);
			} catch (InterruptedException | InvocationTargetException e) {
				LOGGER.warn("Error while closing the window", e);
			}
		}
	}

	/** Thread to close the splash screen. */
	final Runnable closerRunner = new Runnable() {
		@Override
		public void run() {
			abort.set(true);
			window.setVisible(false);
			window.dispose();
		}
	};
}
