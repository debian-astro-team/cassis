/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file;

import java.util.List;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.file.gui.ColumnsDetected;

/**
 * Interface which permit to manage files.
 *
 * @author fort
 */
public interface InterfaceFileManager {

	/**
	 * Create a CassisSpectrum to build the chart.
	 *
	 * @return a CassisSpectrum
	 */
	CassisSpectrum read();

	/**
	 * Return a {@link CassisSpectrum} according to spectrum index.
	 *
	 * @param spectrumIndex
	 *            The index of spectrum
	 * @return The CassisSpectrum
	 */
	CassisSpectrum read(int spectrumIndex);

	/**
	 * Read file and return a {@link CassisSpectrum} list which contains all {@link CassisSpectrum} file.
	 *
	 * @return the list of spectra
	 */
	List<CassisSpectrum> readAll();

	/**
	 * Return the number of spectrum containing in a file.
	 *
	 * @return the spectrum number
	 */
	int readNbCassisSpectra();

	/**
	 * Return {@link CassisMetadata} list according to spectrum index.
	 *
	 * @param spectrumIndex
	 *            Represent the spectrum index
	 * @return the {@link CassisMetadata} List
	 */
	List<CassisMetadata> readCassisMetadata(int spectrumIndex);

	/**
	 * Research the metadata with keyword name according to spectrum index containing in a file.
	 *
	 * @param name
	 *            Represent metadata keyword
	 * @param spectrumIndex
	 *            The index of spectrum
	 * @return the {@link CassisMetadata} to obtain his name, value, comment and unit
	 */
	CassisMetadata getCassisMetadata(String name, int spectrumIndex);

	/**
	 * Return a list of common metadata.
	 *
	 * @return cassisMetadata list
	 */
	List<CassisMetadata> getCommonCassisMetadataList();

	/**
	 * Getter for wave columns of data detected on file
	 *
	 * @return {@link List} of wave columns of data detected for each spectrum
	 */
	List<ColumnsDetected> getWaveColumnsDetected();

	/**
	 * Getter for flux columns of data detected on file
	 *
	 * @return {@link List} of flux columns of data detected for each spectrum
	 */
	List<ColumnsDetected> getFluxColumnsDetected();

	/**
	 * Set additional metadata list, usually known from outside the spectrum file.
	 *
	 * @param additionalMetadataList The additional metadata list.
	 */
	void setAdditionalMetadataList(List<CassisMetadata> additionalMetadataList);
}
