/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.linelist;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.cassisd.model.LinesDescription;
import eu.omp.irap.cassis.common.Formula;
import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.parameters.LineIdentificationUtils;
import uk.ac.starlink.table.ColumnInfo;
import uk.ac.starlink.table.RowSequence;
import uk.ac.starlink.table.StarTable;
import uk.ac.starlink.table.StarTableFactory;
import uk.ac.starlink.table.TableFormatException;

/**
 * Utility class to read SpectralLineList HIPE files.
 *
 * @author M. Boiziot
 */
public class SpectralLineListFileReader {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpectralLineListFileReader.class);

	private static final String COLUMN_SPECIES = "species";
	private static final String COLUMN_TRANSITION = "transition";
	private static final String COLUMN_POSITION = "position";
	private static final String COLUMN_FREQUENCY = "restFrequency";
	private static final String COLUMN_EUP = "eup";
	private static final String COLUMN_AIJ = "aij";
	private static final String COLUMN_STD_POSITION = "stdPosition";
	private static final String COLUMN_V0 = "v0";
	private static final String COLUMN_SNR = "signalToNoiseRatio";
	private static final String OLD_COLUMN_SNR = "snr";

	private static final int INDEX_INTERN_SPECIES = 0;
	private static final int INDEX_INTERN_TRANSITION = 1;
	private static final int INDEX_INTERN_FREQUENCY = 2;
	private static final int INDEX_INTERN_EUP = 3;
	private static final int INDEX_INTERN_AIJ = 4;
	private static final int INDEX_INTERN_STD_POSITION = 5;
	private static final int INDEX_INTERN_V0 = 6;
	private static final int INDEX_INTERN_POSITION = 7;
	private static final int INDEX_INTERN_SNR = 8;

	private static final int INDEX_NOT_FOUND = -1;


	/**
	 * Utility class, can not be instancied.
	 */
	private SpectralLineListFileReader() {
	}

	/**
	 * Read the given files and return the associated {@link LinesDescription}.
	 * If a vlsr data is provided (non NaN) the freqCompute and vlsrData of the lines
	 *    is computed and set.
	 *
	 * @param path The path of the file.
	 * @param vlsrData The vlsr data.
	 * @return The list of {@link LineDescription} read in the file, or null if
	 *    there was an error while reading the file.
	 */
	public static List<LineDescription> getLines(String path, double vlsrData) {
		File file = new File(path);
		if (!file.exists()) {
			return null;
		}
		List<LineDescription> listLines = new ArrayList<>();
		try {
			StarTable st = new StarTableFactory().makeStarTable(file.getAbsolutePath());
			int[] columnIndex;
			try {
				final String[] columnNames = {COLUMN_SPECIES, COLUMN_TRANSITION,
						COLUMN_FREQUENCY, COLUMN_EUP, COLUMN_AIJ,
						COLUMN_STD_POSITION, COLUMN_V0, COLUMN_POSITION, COLUMN_SNR};
				columnIndex = SpectralLineListFileReader.getColumnsIndex(columnNames, st);
			} catch (ColumnNotFoundException ce) {
				// Maybe an old file format. Try with older SNR column name.
				LOGGER.debug("Column not found on Linelist file {}. Maybe an older format", path, ce);
				final String[] columnNames = {COLUMN_SPECIES, COLUMN_TRANSITION,
						COLUMN_FREQUENCY, COLUMN_EUP, COLUMN_AIJ,
						COLUMN_STD_POSITION, COLUMN_V0, COLUMN_POSITION, OLD_COLUMN_SNR};
				columnIndex = SpectralLineListFileReader.getColumnsIndex(columnNames, st);
			}

			RowSequence rseq = st.getRowSequence();
			while (rseq.next()) {
				Object[] rows = rseq.getRow();
				String species = (String) rows[columnIndex[INDEX_INTERN_SPECIES]];
				String transition = (String) rows[columnIndex[INDEX_INTERN_TRANSITION]];
				double position = (Double) rows[columnIndex[INDEX_INTERN_POSITION]] * 1000;
				double restFrequency = (Double) rows[columnIndex[INDEX_INTERN_FREQUENCY]] * 1000;
				double eup = (Double) rows[columnIndex[INDEX_INTERN_EUP]];
				double aij = (Double) rows[columnIndex[INDEX_INTERN_AIJ]];
				double deltaFreq = (Double) rows[columnIndex[INDEX_INTERN_STD_POSITION]] * 1000;
				double vlsr = (Double) rows[columnIndex[INDEX_INTERN_V0]];
				double snr = (Double) rows[columnIndex[INDEX_INTERN_SNR]];

				LineDescription line;
				if ("Unknown".equals(species)) {
					line = SpectralLineListFileReader.createLine(
							vlsrData, species, transition, position, eup, aij,
							deltaFreq, 0.0);
					line.setIdentification(LineIdentificationUtils
							.createSpectralLineListUnknownIdentification(position,
									deltaFreq, vlsr, snr));
				} else {
					line = SpectralLineListFileReader.createLine(
							vlsrData, species, transition, position, eup, aij,
							deltaFreq, 0.0);
					line.setIdentification(
							LineIdentificationUtils.createSpectralLineListIdentification(
									species, transition, restFrequency, eup, aij,
									deltaFreq, vlsr, snr));
				}
				listLines.add(line);
			}
		} catch (TableFormatException tfe) {
			LOGGER.debug("Wrong format used for the file. Is it really a SpectralLineList file ?", tfe);
			return null;
		} catch (IOException ioe) {
			LOGGER.error("Error while trying to read the linelist file {}", path, ioe);
			return null;
		} catch (ColumnNotFoundException cnfe) {
			LOGGER.debug("Column not found on Linelist file {}. Can not read the file", path, cnfe);
			return null;
		}
		return !listLines.isEmpty() ? listLines : null;
	}

	/**
	 * Read the given files and return the associated {@link LinesDescription} if
	 *    they are between the given minimum and maximum frequency.
	 * If a vlsr data is provided (non NaN) the freqCompute and vlsrData of the lines
	 *    is set.
	 *
	 * @param path The path of the file.
	 * @param minFreq The minimum frequency.
	 * @param maxFreq The maximum frequency.
	 * @param vlsrData The vlsr data.
	 * @return The list of {@link LineDescription} read in the file, or null if
	 *    there was an error while reading the file.
	 */
	public static List<LineDescription> getLines(String path, double minFreq, double maxFreq, double vlsrData) {
		List<LineDescription> linesWithoutFreqRestriction = getLines(path, vlsrData);
		if (linesWithoutFreqRestriction == null) {
			return null;
		}
		List<LineDescription> goodLines = new ArrayList<>();
		for (LineDescription line : linesWithoutFreqRestriction) {
			if (line.getObsFrequency() < maxFreq && line.getObsFrequency() > minFreq) {
				goodLines.add(line);
			}
		}
		return goodLines;
	}

	/**
	 * Search the indexes for the column with the given columns name.
	 *
	 * @param columnsName The columns name.
	 * @param starTable The starTable.
	 * @return The indexes for of the column.
	 * @throws ColumnNotFoundException Exception if a column was not found.
	 */
	private static int[] getColumnsIndex(final String[] columnsName, StarTable starTable)
			throws ColumnNotFoundException {
		HashMap<String, Integer> map = new HashMap<>(columnsName.length);
		int[] columnsIndex = new int[columnsName.length];
		Arrays.fill(columnsIndex, INDEX_NOT_FOUND);
		int i = 0;
		for (String columnName : columnsName) {
			map.put(columnName, i);
			i++;
		}

		for (int index = 0; index < starTable.getColumnCount(); index++) {
			ColumnInfo ci = starTable.getColumnInfo(index);

			if (map.containsKey(ci.getName())) {
				columnsIndex[map.get(ci.getName())] = index;
			}
		}

		StringBuilder sb = new StringBuilder("Index not found:");
		boolean error = false;
		for (int index = 0; index < columnsIndex.length; index++) {
			int valueIndex = columnsIndex[index];
			if (valueIndex == INDEX_NOT_FOUND) {
				error = true;
				sb.append(' ').append(columnsName[index]).append(",");
			}
		}

		if (error) {
			sb.setCharAt(sb.length() - 1, '.');
			throw new ColumnNotFoundException(sb.toString());
		}
		return columnsIndex;
	}

	/**
	 * Create a {@link LineDescription} with the given parameters. It also
	 * create the Identification message.
	 *
	 * @param vlsrData The vlsr of the data.
	 * @param species The species name.
	 * @param transition The transition.
	 * @param frequency The frequency of the line (MHz).
	 * @param eup The eup value.
	 * @param aij The aij value.
	 * @param deltaFreq The delta frequency (MHz).
	 * @param vlsr The vlsr.
	 * @return The created line.
	 */
	public static LineDescription createLine(double vlsrData, String species,
			String transition, double frequency, double eup, double aij,
			double deltaFreq, double vlsr) {
		LineDescription line = new LineDescription();
		line.setMolName(species + transition);
		line.setObsFrequency(frequency);
		line.setEUpK(eup);
		line.setAij(aij);
		line.setDObsFrequency(deltaFreq);
		line.setVlsr(vlsr);
		if (!Double.isNaN(vlsrData)) {
			line.setFreqCompute(Formula.calcFreqWithVlsr(frequency,
					 vlsr - vlsrData, frequency));
			line.setVlsrData(vlsrData);
		}
		return line;
	}

}
