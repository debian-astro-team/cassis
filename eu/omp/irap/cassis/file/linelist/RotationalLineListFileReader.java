/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.linelist;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.cassisd.model.LinesDescription;
import eu.omp.irap.cassis.common.Formula;
import eu.omp.irap.cassis.common.LineDescription;
import eu.omp.irap.cassis.parameters.LineIdentificationUtils;


/**
 * Utility class to read .rotd files as overlay linelist.
 * @author M. Boiziot
 */
public class RotationalLineListFileReader {

	private static final Logger LOGGER = LoggerFactory.getLogger(RotationalLineListFileReader.class);


	/**
	 * Utility class, can not be instancied.
	 */
	private RotationalLineListFileReader() {
	}

	/**
	 * Read the given files and return the associated {@link LinesDescription}.
	 * If a vlsr data is provided (non NaN) the freqCompute and vlsrData of the lines
	 *    is computed and set.
	 *
	 * @param path The path of the file.
	 * @param vlsrData The vlsr data.
	 * @return The list of {@link LineDescription} read in the file, or null if
	 *    there was an error while reading the file.
	 */
	public static List<LineDescription> getLines(String path, double vlsrData) {
		boolean headerReaded = false;
		boolean unitsReaded = false;
		List<LineDescription> lines = new ArrayList<>();

		try (BufferedReader br = new BufferedReader(new FileReader(path))) {
			String line;
			br.readLine(); // Jump version.
			while ((line = br.readLine()) != null) {
				if (line.trim().isEmpty()){
					continue;
				}
				if (!headerReaded) {
					if (line.startsWith("#")) {
						continue;
					} else {
						headerReaded = true;
					}
				}

				if (!unitsReaded) {
					if (!line.startsWith("id")) {
						// TODO readUnitsHere
						unitsReaded = true;
					}
				} else {
					String[] explodedLine = line.split("\t");
					int tag = Integer.parseInt(explodedLine[0]);
					String molName = explodedLine[2];
					String qn = explodedLine[3];
					double frequency = Double.parseDouble(explodedLine[4]);
					double eup = Double.parseDouble(explodedLine[5]);
					int gup = Integer.parseInt(explodedLine[6]);
					double aij = Double.parseDouble(explodedLine[7]);
					double vo = Double.parseDouble(explodedLine[10]);

					LineDescription ld = createLine(vlsrData, molName, qn, frequency, eup, aij, vo, tag, gup);
					lines.add(ld);
				}
			}
		} catch (IOException e) {
			LOGGER.error("Error while reading the file {}", path, e);
		}
		return lines;
	}

	/**
	 * Read the given files and return the associated {@link LinesDescription} if
	 *    they are between the given minimum and maximum frequency.
	 * If a vlsr data is provided (non NaN) the freqCompute and vlsrData of the lines
	 *    is set.
	 *
	 * @param path The path of the file.
	 * @param minFreq The minimum frequency.
	 * @param maxFreq The maximum frequency.
	 * @param vlsrData The vlsr data.
	 * @return The list of {@link LineDescription} read in the file, or null if
	 *    there was an error while reading the file.
	 */
	public static List<LineDescription> getLines(String path, double minFreq, double maxFreq, double vlsrData) {
		List<LineDescription> linesWithoutFreqRestriction = getLines(path, vlsrData);
		List<LineDescription> goodLines = new ArrayList<>();
		for (LineDescription line : linesWithoutFreqRestriction) {
			if (line.getObsFrequency() < maxFreq && line.getObsFrequency() > minFreq) {
				goodLines.add(line);
			}
		}
		return goodLines;
	}

	/**
	 * Create a {@link LineDescription} with the given parameters. It also
	 * create the Identification message.
	 *
	 * @param vlsrData The vlsr of the data.
	 * @param species The species name.
	 * @param transition The transition.
	 * @param restFrequency The rest frequency of the line (MHz).
	 * @param eup The eup value.
	 * @param aij The aij value.
	 * @param vlsr The vlsr.
	 * @param tag The tag of the molecule.
	 * @param gup The gup.
	 * @return The created line.
	 */
	public static LineDescription createLine(double vlsrData, String species,
			String transition, double restFrequency, double eup, double aij,
			double vlsr, int tag, int gup) {
		LineDescription line = new LineDescription();
		line.setMolName(species + transition);
		line.setObsFrequency(restFrequency);
		line.setEUpK(eup);
		line.setAij(aij);
		line.setVlsr(vlsr);
		line.setMolTag(tag);
		line.setGu(gup);
		if (!Double.isNaN(vlsrData)) {
			line.setFreqCompute(Formula.calcFreqWithVlsr(restFrequency,
					 vlsr - vlsrData, restFrequency));
			line.setVlsrData(vlsrData);
		}
		String id = LineIdentificationUtils.createRotationalLineIdentification(
				species, transition, restFrequency, eup, aij, vlsr, gup);
		line.setIdentification(id);
		return line;
	}

}
