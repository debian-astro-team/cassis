/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.CassisMultipleSpectrum;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.file.gui.ColumnsDetected;

public class FileManagerClass extends FileManager {

	// === indexes for the scan header array (sh).
	private static final int IND_SCANJUMP = 1;
	private static final int IND_DATAJUMP = 2;
	private static final int IND_NCHANNEL = 3;
	private static final int IND_RESTF = 4;
	private static final int IND_FRES = 5;
	private static final int IND_BAD = 6;
	private static final int IND_RCHAN = 7;
	private static final int IND_DOPPLER = 8;
	private static final int IND_IMAGE = 9;
	private static final int IND_VOFF = 10;
	private static final int IND_VRES = 11;
	private static final int IND_FOFF = 12;

	private static final int SIZE_WORD = 4;

	private CLASS_VERSION version = CLASS_VERSION.V2;
	private int reclen = 512;
	private int nscan = 1;
	private String title = "UNKOW";
	private int indiceSource = 4;
	private double[][] sh;
	private int[] nbChannelsByScan;

	private List<CassisSpectrum> cassisSpectrumList;
	private List<CassisMetadata> cassisMetadataList;

	public enum CLASS_VERSION {
		V1_SINGLE, V1_MULTIPLE, V2, UNKNOW
	}

	private static final int CLASS_IDENTIFIER = 1;
	private static final int SPECTRO_SECTION = -4;
	private static final int ENTRY_SIZE_INBLOCK_INDEX = 32;
	private boolean dopplerEffect = true;
	private String telescope;
	private String csour;
	private String line;


	/**
	 * Construct a FileManagerClass with exceptions displayed.
	 *
	 * @param file The input File.
	 */
	public FileManagerClass(File file) {
		this(file, true);
	}

	/**
	 * Construct a FileManagerClass.
	 *
	 * @param file The input File
	 * @param displayException If the exceptions should be displayed.
	 */
	public FileManagerClass(File file, boolean displayException) {
		super(file, displayException);
		cassisSpectrumList = new ArrayList<CassisSpectrum>();
		cassisMetadataList = new ArrayList<CassisMetadata>();
	}

	@Override
	public CassisSpectrum read() {
		CassisMultipleSpectrum cassisMultipleSpectrum = null;
		int nbChannels = 0;

		try (RandomAccessFile randomAccessFile = new RandomAccessFile(super.file, "r")) {
			FileChannel fcr = randomAccessFile.getChannel();
			// --- mapping of the file in memory
			MappedByteBuffer byteBuffer = fcr.map(FileChannel.MapMode.READ_ONLY, 0, fcr.size());

			// Read fileIndex
			if (!setParameters(byteBuffer)) {
				return null;
			}

			if (version == CLASS_VERSION.V2) {
				reclen = byteBuffer.getInt((1) * SIZE_WORD);
				int kind = byteBuffer.getInt((2) * SIZE_WORD);
				if (kind != CLASS_IDENTIFIER)
					nbChannels = 0;
				else
					nbChannels = readVersion2(byteBuffer);

			} else if (version == CLASS_VERSION.V1_SINGLE || version == CLASS_VERSION.V1_MULTIPLE) {
				nbChannels = readVersion1(byteBuffer);
			}

			if (nbChannels > 0 && nscan > 0) {
				double[] intensities = new double[nbChannels];
				double[] frequencies = new double[nbChannels];
				double[] frequenciesIm = new double[nbChannels];
				double[] vlsr = new double[nbChannels];

				CassisSpectrum[] scanSpectrum = new CassisSpectrum[nscan];

				for (int iscan = 0, kdum0 = 0; iscan < nscan; iscan++) {
					kdum0 = createSubSpectrum(nbChannels, byteBuffer, intensities, frequencies, frequenciesIm, vlsr, scanSpectrum, iscan, kdum0);
				}

				double loFrequency = frequencies[0] + (frequenciesIm[0] - frequencies[0]) / 2.0;
				final CassisSpectrum generateCassisSpectrum = CassisSpectrum.generateCassisSpectrum(title, sh[0][IND_BAD], frequencies, intensities, loFrequency, vlsr[0],
						XAxisCassis.getXAxisFrequency(), YAxisCassis.getYAxisKelvin());
				generateCassisSpectrum.addCassisMetadata(new CassisMetadata(CassisMetadata.FILE_TYPE,
						FileManager.FileType.CLASS.name(), null, null), true);
				cassisMultipleSpectrum = new CassisMultipleSpectrum(generateCassisSpectrum);
				cassisMultipleSpectrum.setScan(scanSpectrum);
				cassisMultipleSpectrum.addCassisMetadata(new CassisMetadata(CassisMetadata.FILE_TYPE,
						FileManager.FileType.CLASS.name(), null, null), true);
			}
		} catch (IOException errmsg) {
			if (displayException) {
				errmsg.printStackTrace();
			}
		}
		return cassisMultipleSpectrum;
	}

	private int createSubSpectrum(int nbChannels, MappedByteBuffer byteBuffer, double[] intensities, double[] frequencies, double[] frequenciesIm, double[] vlsr, CassisSpectrum[] scanSpectrum,
			int iscan, int kdum0) {
		int kdebdata;
		int kfindata;
		int currentChannel;
		double[] intensitiesLocal = new double[nbChannelsByScan[iscan]];
		double[] frequenciesLocal = new double[nbChannelsByScan[iscan]];
		double[] frequenciesImLocal = new double[nbChannelsByScan[iscan]];
		double[] vlsrLocal = new double[nbChannels];
		int numLocal = 0;

		// --- decode the data
		final int kdeb = (int) (sh[iscan][IND_SCANJUMP]);
		final int nbChannel = (int) sh[iscan][IND_NCHANNEL];

		kdebdata = (int) (kdeb + sh[iscan][IND_DATAJUMP]);
		kfindata = (kdebdata + SIZE_WORD * nbChannel);
		currentChannel = kdum0;
		for (int cpt = kdebdata; cpt < kfindata; cpt += SIZE_WORD) {
			intensities[currentChannel] = byteBuffer.getFloat(cpt);
			intensitiesLocal[numLocal] = intensities[currentChannel];
			currentChannel++;
			numLocal++;
		}
		// --- create the frequency vector
		currentChannel = kdum0;
		numLocal = 0;

		for (int i = 0; i < nbChannel; i++) {
			double resolution = sh[iscan][IND_FRES];
			if (dopplerEffect && sh[iscan][IND_DOPPLER] != -1)
				resolution = sh[iscan][IND_FRES] / (1.0 + sh[iscan][IND_DOPPLER]);
			frequencies[currentChannel] = (i + 1 - sh[iscan][IND_RCHAN]) * resolution + sh[iscan][IND_RESTF];
			frequenciesLocal[numLocal] = frequencies[currentChannel];
			frequenciesIm[currentChannel] = -(i + 1 - sh[iscan][IND_RCHAN]) * resolution + sh[iscan][IND_IMAGE];
			frequenciesImLocal[numLocal] = frequenciesIm[currentChannel];

			vlsr[currentChannel] = sh[iscan][IND_VOFF];
			vlsrLocal[numLocal] = sh[iscan][IND_VOFF];

			currentChannel++;

			numLocal++;
		}
		kdum0 = currentChannel;

		double loFrequency = frequenciesLocal[0] + (frequenciesImLocal[0] - frequenciesLocal[0]) / 2.0;

		CassisSpectrum cassisSpectrum = CassisSpectrum.generateCassisSpectrum(title,
				sh[iscan][IND_BAD], frequenciesLocal, intensitiesLocal, loFrequency,
				vlsrLocal[0], XAxisCassis.getXAxisFrequency(),
				YAxisCassis.getYAxisKelvin());
		cassisSpectrum.addCassisMetadata(new CassisMetadata(CassisMetadata.FILE_TYPE,
				FileManager.FileType.CLASS.name(), null, null), true);
		scanSpectrum[iscan] = cassisSpectrum;
		return kdum0;
	}

	private int readVersion1(MappedByteBuffer byteBuffer) {
		int nbChannels = 0;

		// int iNext = byteBuffer.getInt((2-1)*SIZE_WORD);
		int lex = byteBuffer.getInt((3 - 1) * SIZE_WORD);
		//		System.out.println("lex=" + lex);
		int nex = byteBuffer.getInt((4 - 1) * SIZE_WORD);
		int inex = byteBuffer.getInt((5 - 1) * SIZE_WORD);
		int[] aex = new int[nex];
		for (int i = 0; i < nex; i++) {
			aex[i] = byteBuffer.getInt((6 - 1 + i) * SIZE_WORD);
		}
		int aExt0 = (int) ((aex[0] - 1) * reclen);

		nscan = inex-1;
		nbChannelsByScan = new int[nscan];
		sh = new double[nscan][13];

		//		System.out.println("scan = " + nscan);

		int beginEntry = reclen * (byteBuffer.getInt(aExt0) - 1);

		int aExtCurrent = aExt0;
		int nbentryInExtension = 0;
		int numExtCurrent = 0;
		for (int cpt = 0; cpt < nscan; cpt++) {
			csour = "";
			for (int i = 0; i < 12; i++) {
				csour = csour + (char) byteBuffer.get(aExt0 + (indiceSource - 1) * SIZE_WORD + i);
			}
			title = csour.trim();

			line = "";
			for (int i = 0; i < 12; i++)
				line = line + (char) byteBuffer.get(aExt0 + (3 + indiceSource - 1) * SIZE_WORD + i);


			telescope = "";
			for (int i = 0; i < 12; i++)
				telescope = telescope + (char) byteBuffer.get(aExt0 + (6 + indiceSource - 1) * SIZE_WORD + i);

			// Read Observation Index ---------------------------------------------

			//			int nbloc = (int)byteBuffer.getInt(beginEntry + (3 - 1) * SIZE_WORD);
			final int adata = byteBuffer.getInt(beginEntry + (5 - 1) * SIZE_WORD);// adata
			final int ldata = byteBuffer.getInt(beginEntry + (6 - 1) * SIZE_WORD);
			final int nsec = byteBuffer.getInt(beginEntry + (8 - 1) * SIZE_WORD);

			int aSpectro = 0;

			for (int i = 0; i < nsec; i++) {
				int sectionsNumber = byteBuffer.getInt(beginEntry + (10 - 1 + i) * SIZE_WORD);
				int sectionsAdresses = byteBuffer.getInt(beginEntry + (10 - 1 + 2 * nsec + i) * SIZE_WORD);
				if (sectionsNumber == SPECTRO_SECTION) {
					aSpectro = sectionsAdresses * SIZE_WORD;
				}
			}

			sh[cpt][IND_DATAJUMP] = SIZE_WORD * (adata - 1);
			sh[cpt][IND_SCANJUMP] = beginEntry;
			sh[cpt][IND_NCHANNEL] = ldata;
			nbChannelsByScan[cpt] = (int) sh[cpt][IND_NCHANNEL];
			nbChannels += sh[cpt][IND_NCHANNEL];

			readParameters(byteBuffer, aSpectro, beginEntry, cpt);

			//			System.out.println("Scan" + cpt + "=" + nbChannelsByScan[cpt]);
			//			System.out.println("ldata=" + ldata);
			//			System.out.println("offset=" + beginEntry);
			nbentryInExtension++;
			if (nbentryInExtension == lex) {
				numExtCurrent++;
				nbentryInExtension = 0;
			}
			aExtCurrent = ((int) ((aex[numExtCurrent] - 1) * reclen));
			beginEntry = reclen * (byteBuffer.getInt(aExtCurrent + nbentryInExtension * ENTRY_SIZE_INBLOCK_INDEX * SIZE_WORD) - 1);

		}

		return nbChannels;
	}

	/**
	 * @param byteBuffer
	 * @return the number oc channel to read, 0 otherwise
	 */
	private int readVersion2(MappedByteBuffer byteBuffer) {
		int nbChannels = 0;
		// File descriptor
		int lind = byteBuffer.getInt((4) * SIZE_WORD);
		int lex1 = byteBuffer.getInt((11) * SIZE_WORD);
		int nex = byteBuffer.getInt((12) * SIZE_WORD);
		int gex = byteBuffer.getInt((13) * SIZE_WORD);
		int xnext = (int) byteBuffer.getLong(6 * SIZE_WORD);

		nscan = xnext-1;
		nbChannelsByScan = new int[nscan];
		sh = new double[nscan][13];
		int nbscan = 0;

//		long[] aex = new long[nex];
		for (int indiceExt = 0; indiceExt < nex; indiceExt++) {
			long aex = byteBuffer.getLong((14 + indiceExt * 2) * SIZE_WORD);
			int aExt0 = (int) ((aex - 1) * reclen) * SIZE_WORD;

			// Extension index
			int numRecEntry = (int) byteBuffer.getLong(aExt0);
			int numWordEntry = byteBuffer.getInt(aExt0 + (3 - 1) * SIZE_WORD);
			int beginEntry = ((numRecEntry - 1) * reclen + (numWordEntry - 1)) * SIZE_WORD;
			int nbEntriesMax = (int) (lex1*Math.pow((gex/10),indiceExt));
			for (int cpt=0; cpt< nbEntriesMax;cpt++){
				csour = "";
				for (int i = 0; i < 12; i++)
					csour = csour + (char) byteBuffer.get(aExt0 + (7 - 1 + lind*cpt) * SIZE_WORD + i );

				line = "";
				for (int i = 0; i < 12; i++)
					line = line + (char) byteBuffer.get(aExt0 + (10 - 1+ lind*cpt) * SIZE_WORD + i);

				telescope = "";
				for (int i = 0; i < 12; i++)
					telescope = telescope + (char) byteBuffer.get(aExt0 + (13 - 1+ lind*cpt) * SIZE_WORD + i);

				// Entry descriptor
				int tmpVersion = byteBuffer.getInt(beginEntry + (2 - 1) * SIZE_WORD);
				if (tmpVersion!=0 &&tmpVersion != 1 && tmpVersion != 2){
					System.out.println("Warning Entry Description =" + tmpVersion +
							"CASSIS can only open version 1 or 2");
					return 0;
				}
				int nsec = byteBuffer.getInt(beginEntry + (3 - 1) * SIZE_WORD);
				int nword = (int)byteBuffer.getLong(beginEntry + (4 - 1) * SIZE_WORD);

				final int adata = (int) byteBuffer.getLong(beginEntry + (6 - 1) * SIZE_WORD);
				final int ldata = (int) byteBuffer.getLong(beginEntry + (8 - 1) * SIZE_WORD);

				int aSpectro = -1, sectionsNumber, sectionsAdresses;

				for (int i = 0; i < nsec; i++) {
					sectionsNumber = byteBuffer.getInt(beginEntry + (12 - 1 + i) * SIZE_WORD);

					if (sectionsNumber == SPECTRO_SECTION) {// spectroscopy Section
						sectionsAdresses = (int) byteBuffer.getLong(beginEntry
								+ (12 - 1 + nsec + 2 * nsec + 2 * i) * SIZE_WORD);
						aSpectro = sectionsAdresses * SIZE_WORD;

					}
				}
				if (aSpectro != -1){

					sh[nbscan][IND_DATAJUMP] = SIZE_WORD * (adata - 1); // indice
					sh[nbscan][IND_SCANJUMP] = beginEntry;
					sh[nbscan][IND_NCHANNEL] = ldata;
					nbChannelsByScan[nbscan] = (int) sh[nbscan][IND_NCHANNEL];
					nbChannels += nbChannelsByScan[nbscan];

					int offset = beginEntry;
					title = csour;
					readParameters(byteBuffer, aSpectro, offset, nbscan);
					nbscan++;
				} else {
					if (indiceExt !=(nex -1)){
						System.out.println("entry index " + cpt + " "
								+ "with no spectrum, indiceExt/numTotal "
								+ indiceExt +"/"+(nex -1));
					}
				}

				beginEntry = beginEntry + nword * SIZE_WORD;

			}
		}
		nscan = nbscan;
		return nbChannels;
	}

	/**
	 * Set the version of the file according to the class documentation.
	 *
	 * @param versionChar The char contening the version.
	 * @return true if the version of the file is OK, false otherwise.
	 */
	private boolean setVersion(char versionChar) {
		boolean versionOk = true;
		if (versionChar == '1') {
			version = CLASS_VERSION.V1_MULTIPLE;
		} else if (versionChar == '9') {
			version = CLASS_VERSION.V1_SINGLE;
		} else if (versionChar == '2') {
			version = CLASS_VERSION.V2;
		} else {
			version = CLASS_VERSION.UNKNOW;
			versionOk = false;
		}
		return versionOk;
	}

	/**
	 * Set the byte order in the provided MappedByteBuffer.
	 *
	 * @param byteBuffer The MappedByteBuffer.
	 * @param byteOrder The char contening the byte order according to class documentation.
	 * @return true if the byte order is set, false otherwise
	 */
	private boolean setByteOrder(MappedByteBuffer byteBuffer, char byteOrder) {
		boolean ok = true;
		if (byteOrder == 'A') {
			byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
		} else if (byteOrder == 'B') {
			byteBuffer.order(ByteOrder.BIG_ENDIAN);
		} else {
			ok = false;
		}
		return ok;
	}

	private void readParameters(MappedByteBuffer byteBuffer, int aSpectro, int offset, int cpt) {
		//		System.out.println("offset=" + offset);
		sh[cpt][IND_RESTF] = byteBuffer.getDouble(offset + aSpectro + (3 - 1) * SIZE_WORD);
		sh[cpt][IND_NCHANNEL] = byteBuffer.getInt(offset + aSpectro + (5 - 1) * SIZE_WORD);
		sh[cpt][IND_RCHAN] = byteBuffer.getFloat(offset + aSpectro + (6 - 1) * SIZE_WORD);
		sh[cpt][IND_FRES] = byteBuffer.getFloat(offset + aSpectro + (7 - 1) * SIZE_WORD);
		sh[cpt][IND_FOFF] = byteBuffer.getFloat(offset + aSpectro + (8 - 1) * SIZE_WORD);
		sh[cpt][IND_VRES] = byteBuffer.getFloat(offset + aSpectro + (9 - 1) * SIZE_WORD);
		sh[cpt][IND_VOFF] = byteBuffer.getFloat(offset + aSpectro + (10 - 1) * SIZE_WORD);
		sh[cpt][IND_BAD] = byteBuffer.getFloat(offset + aSpectro + (11 - 1) * SIZE_WORD);
		sh[cpt][IND_IMAGE] = byteBuffer.getDouble(offset + aSpectro + (12 - 1) * SIZE_WORD);
		sh[cpt][IND_DOPPLER] = byteBuffer.getDouble(offset + aSpectro + (15 - 1) * SIZE_WORD);
	}

	/**
	 * @return the dopplerEffect
	 */
	public boolean isDopplerEffect() {
		return dopplerEffect;
	}

	/**
	 * @param dopplerEffect
	 *            the dopplerEffect to set
	 */
	public void setDopplerEffect(boolean dopplerEffect) {
		this.dopplerEffect = dopplerEffect;
	}

	@Override
	public FileType getType() {
		return FileType.CLASS;
	}

	@Override
	public CassisSpectrum read(int spectrumIndex) {
		return cassisSpectrumList.get(spectrumIndex);
	}

	@Override
	public List<CassisMetadata> readCassisMetadata(int spectrumIndex) {
		return cassisSpectrumList.get(spectrumIndex).getOriginalMetadataList();
	}

	@Override
	public List<CassisSpectrum> readAll() {
		CassisMultipleSpectrum cassisMultipleSpectrum = null;
		int nbChannels = 0;

		try (RandomAccessFile randomAccessFile = new RandomAccessFile(super.file, "r");) {
			FileChannel fcr = randomAccessFile.getChannel();
			// --- mapping of the file in memory
			MappedByteBuffer byteBuffer = fcr.map(FileChannel.MapMode.READ_ONLY, 0, fcr.size());

			// Read fileIndex
			if (!setParameters(byteBuffer)) {
				return null;
			}

			if (version == CLASS_VERSION.V2) {
				reclen = byteBuffer.getInt((1) * SIZE_WORD);
				int kind = byteBuffer.getInt((2) * SIZE_WORD);
				if (kind != CLASS_IDENTIFIER)
					nbChannels = 0;
				else
					nbChannels = readVersion2(byteBuffer);

			} else if (version == CLASS_VERSION.V1_SINGLE || version == CLASS_VERSION.V1_MULTIPLE) {
				nbChannels = readVersion1(byteBuffer);
			}

			if (nbChannels > 0) {
				double[] intensities = new double[nbChannels];
				double[] frequencies = new double[nbChannels];
				double[] frequenciesIm = new double[nbChannels];
				double[] vlsr = new double[nbChannels];

				CassisSpectrum[] scanSpectrum = new CassisSpectrum[nscan];

				for (int iscan = 0, kdum0 = 0; iscan < nscan; iscan++) {
					kdum0 = createSubSpectrum(nbChannels, byteBuffer, intensities, frequencies, frequenciesIm, vlsr, scanSpectrum, iscan, kdum0);
				}

				double loFrequency = frequencies[0] + (frequenciesIm[0] - frequencies[0]) / 2.0;
				final CassisSpectrum generateCassisSpectrum = CassisSpectrum.generateCassisSpectrum(title, sh[0][IND_BAD], frequencies, intensities, loFrequency, vlsr[0],
						XAxisCassis.getXAxisFrequency(), YAxisCassis.getYAxisKelvin());
				cassisMultipleSpectrum = new CassisMultipleSpectrum(generateCassisSpectrum);
				cassisMultipleSpectrum.setScan(scanSpectrum);
				cassisMetadataList.add(new CassisMetadata("title", title, "", ""));
				cassisMetadataList.add(new CassisMetadata("dopplerEffect", String.valueOf(dopplerEffect), "", ""));
				cassisMetadataList.add(new CassisMetadata("version", version.toString(), "", ""));
				cassisMetadataList.add(new CassisMetadata("csour", csour, "", ""));
				cassisMetadataList.add(new CassisMetadata("line", line, "", ""));
				if (!telescope.trim().isEmpty()) {
					cassisMetadataList.add(new CassisMetadata("telescope", telescope.trim(), "", ""));
				}
				cassisMetadataList.add(new CassisMetadata("Lofreq", String.valueOf(cassisMultipleSpectrum.getLoFrequency()), "", ""));
				cassisMetadataList.add(new CassisMetadata("Vlrs", String.valueOf(cassisMultipleSpectrum.getVlsr()), "", ""));
				cassisMetadataList.add(new CassisMetadata("NbScan", String.valueOf(nscan), "", ""));
				cassisMultipleSpectrum.setOriginalMetadataList(cassisMetadataList);
				cassisSpectrumList.add(cassisMultipleSpectrum);
			}
		} catch (IOException errmsg) {
			if (displayException) {
				errmsg.printStackTrace();
			}
		}
		return cassisSpectrumList;
	}

	/**
	 * Set the parameters of the file (version and byte order).
	 *
	 * @param byteBuffer The MappedByteBuffer of the file.
	 * @return true if the parameters are ok, false otherwise.
	 */
	private boolean setParameters(MappedByteBuffer byteBuffer) {
		char versionChar = (char) byteBuffer.get(0);
		char byteOrder = (char) byteBuffer.get(1);
		return setVersion(versionChar) && setByteOrder(byteBuffer, byteOrder);
	}

	@Override
	public CassisMetadata getCassisMetadata(String name, int spectrumIndex) {
		CassisMetadata cassisMetadata = new CassisMetadata();
		if (spectrumIndex >= 0 && spectrumIndex <= readNbCassisSpectra()) {
			List<CassisMetadata> cassisMetadataList = cassisSpectrumList.get(spectrumIndex).getOriginalMetadataList();
			for (CassisMetadata cassisMetadataTemp : cassisMetadataList) {
				if (cassisMetadataTemp.getName().equalsIgnoreCase(name)) {
					cassisMetadata = cassisMetadataTemp;
				}
			}
		}
		return cassisMetadata;
	}

	@Override
	public List<CassisMetadata> getCommonCassisMetadataList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ColumnsDetected> getWaveColumnsDetected() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ColumnsDetected> getFluxColumnsDetected() {
		// TODO Auto-generated method stub
		return null;
	}
}
