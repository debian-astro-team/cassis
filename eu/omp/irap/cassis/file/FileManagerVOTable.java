/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.file.gui.ColumnInformation;
import eu.omp.irap.cassis.file.gui.ColumnsDetected;
import uk.ac.starlink.table.ColumnInfo;
import uk.ac.starlink.table.DefaultValueInfo;
import uk.ac.starlink.table.DescribedValue;
import uk.ac.starlink.table.RandomStarTable;
import uk.ac.starlink.table.RowSequence;
import uk.ac.starlink.table.StarTable;
import uk.ac.starlink.table.StarTableFactory;
import uk.ac.starlink.table.StarTableOutput;
import uk.ac.starlink.table.TableFormatException;
import uk.ac.starlink.votable.DataFormat;
import uk.ac.starlink.votable.VOElement;
import uk.ac.starlink.votable.VOElementFactory;
import uk.ac.starlink.votable.VOTableWriter;

public class FileManagerVOTable extends FileManager implements SaveSpectrumInterface {

	private enum COLUMN_TYPE { FLUX, WAVE };
	private List<CassisMetadata> cassisMetadataList;
	private List<CassisSpectrum> cassisSpectrumList;
	private List<ColumnsDetected> fluxColumnsList;
	private List<ColumnsDetected> waveColumnsList;


	/**
	 * Construct a FileManagerVOTable with exceptions displayed.
	 *
	 * @param file The input File.
	 */
	public FileManagerVOTable(File file) {
		this(file, true);
	}

	/**
	 * Construct a FileManagerVOTable.
	 *
	 * @param file The input File
	 * @param displayException If the exceptions should be displayed.
	 */
	public FileManagerVOTable(File file, boolean displayException) {
		super(file, displayException);
		cassisMetadataList = new ArrayList<CassisMetadata>();
		cassisSpectrumList = new ArrayList<CassisSpectrum>();
	}

	/**
	 * load the file in a CassisSpectrum
	 */
	@Override
	public CassisSpectrum read() {
		StarTable starTable = null;
		try {
			starTable = new StarTableFactory().makeStarTable(file.getAbsolutePath(), "votable");
		} catch (TableFormatException e) {
			return null;
		} catch (IOException e) {
			return null;
		}

		cassisMetadataList = transformParametersListToCassisMetadataList(starTable.getParameters());
		double vlsr = extractVlsr(cassisMetadataList);

		String unitX = null;
		String unitY = null;
		String informationNameY = null;
		int numColWave = 0, numColFlux = 1;
		int nCol = starTable.getColumnCount();

		String forcedFluxAxisName = getForcedAxis();
		String forcedWaveAxisName = getForcedWave();

		fluxColumnsList = getFluxColumnsDetected(starTable);
		waveColumnsList = getWaveColumnsDetected(starTable);

		if (forcedFluxAxisName != null) {
			for (int i = 0; i < nCol; i++) {
				ColumnInfo ci = starTable.getColumnInfo(i);
				if (ci.getName().equals(forcedFluxAxisName)) {
					unitY = ci.getUnitString();
					informationNameY = ci.getName();
					numColFlux = i;
					setColumnUsed(forcedFluxAxisName, unitY, numColFlux, COLUMN_TYPE.FLUX);
					break;
				}
			}
		} else if (fluxColumnsList != null && !fluxColumnsList.isEmpty()) {
			ColumnsDetected cd = fluxColumnsList.get(0);
			if (!cd.isColumnsDetectedListEmpty()) {
				ColumnInformation ci = cd.getColumnsDetectedList().get(0);
				String name = ci.getName();
				informationNameY = ci.getName();
				numColFlux = ci.getIndex();
				unitY = ci.getUnitString();
				if ((unitY == null || unitY.isEmpty()) && name != null && !name.isEmpty()) {
					informationNameY = name;
				}
				cd.setColumnUsed(name);
			}
		} else if (fluxColumnsList.isEmpty() && nCol > 1) {
			ColumnInfo column1 = starTable.getColumnInfo(1);
			if ((column1 != null) && (column1.getUnitString() != null)){
				unitY = column1.getUnitString();
				String name = column1.getName();
				if (name != null) {
					informationNameY = name;
				}
				setColumnUsed(name, unitY, 1, COLUMN_TYPE.FLUX);
			}
		}

		if (forcedWaveAxisName != null) {
			for (int i = 0; i < nCol; i++) {
				ColumnInfo ci = starTable.getColumnInfo(i);
				if (ci.getName().equals(forcedWaveAxisName)) {
					unitX = ci.getUnitString();
					numColWave = i;
					setColumnUsed(forcedWaveAxisName, unitX, numColWave, COLUMN_TYPE.WAVE);
					break;
				}
			}
		} else if (waveColumnsList != null && !waveColumnsList.isEmpty()) {
			ColumnsDetected cd = waveColumnsList.get(0);
			if (!cd.isColumnsDetectedListEmpty()) {
				ColumnInformation ci = cd.getColumnsDetectedList().get(0);
				numColWave = ci.getIndex();
				unitX = ci.getUnitString();
				cd.setColumnUsed(ci.getName());
			}
		}

		CassisSpectrum cassisSpectrum;
		try {
			cassisSpectrum = getCassisSpectrum(starTable, vlsr,
					XAxisCassis.getXAxisCassis(unitX), numColWave,
					YAxisCassis.getYAxisCassis(unitY, informationNameY), numColFlux);
			cassisSpectrum.setOriginalMetadataList(cassisMetadataList);
			cassisSpectrum.addCassisMetadata(new CassisMetadata(CassisMetadata.FILE_TYPE,
					FileManager.FileType.VOTABLE.name(), null, null), true);
			if (unitX == null || UNIT.toUnit(unitX) == UNIT.UNKNOWN) {
				cassisSpectrum.setxError(true);
			}
			if (unitY == null) {
				cassisSpectrum.setyError(true);
			}
			cassisSpectrumList.add(cassisSpectrum);
		} catch (IOException e) {
			return null;
		}
		return cassisSpectrum;
	}

	private double extractVlsr(List<CassisMetadata> cassisMetadataList) {
		double vlsr = 0;
		for (CassisMetadata cassisMetadata : cassisMetadataList) {
			if ("vlsr".equals(cassisMetadata.getName())) {
				vlsr = Double.valueOf(cassisMetadata.getValue());
			}
		}
		return vlsr;
	}

	@SuppressWarnings("rawtypes")
	public List<CassisMetadata> transformParametersListToCassisMetadataList(List parametersList) {
		List<CassisMetadata> cassisMetadataList = new ArrayList<CassisMetadata>();
		for (Object object : parametersList) {
			DescribedValue describedValue = (DescribedValue) object;
			try {
				final String name = describedValue.getInfo().getName();

				String value = "";
				if (describedValue.getValue() != null) {
					value = describedValue.getValue().toString();
				}

				String unitString = describedValue.getInfo().getUnitString();
				if (name.endsWith("Unit") && (unitString == null || unitString.isEmpty())) {
					unitString = value;
				}

				final CassisMetadata cassisMetadata = new CassisMetadata(name, value,
						describedValue.getInfo().getDescription(), unitString);
				cassisMetadataList.add(cassisMetadata);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return cassisMetadataList;
	}

	@Override
	public boolean save(File file, CommentedSpectrum commentedSpectrum) {
		StarTableOutput sto = new StarTableOutput();
		VOTableWriter voWriter = new VOTableWriter(DataFormat.TABLEDATA, true);
		StarTable table = createStarTable(commentedSpectrum);
		try {
			voWriter.writeStarTable(table, file.getAbsolutePath(), sto);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private StarTable createStarTable(final CommentedSpectrum commentedSpectrum) {
		return new RandomStarTable() {

			private ColumnInfo columnInfoWave;
			private ColumnInfo columnInfoIntensities;
			private ColumnInfo[] colInfos_;

			{
				columnInfoWave = new ColumnInfo("frequency", Double.class, "");
				columnInfoWave.setUCD("em.freq");
				columnInfoWave.setUnitString("MHz");
				columnInfoWave.setUtype("Data.SpectralAxis.Value");
				columnInfoIntensities = new ColumnInfo("intensities", Double.class, "");
				columnInfoIntensities.setUCD("phot.flux.density");
				columnInfoIntensities.setUnitString(commentedSpectrum.getyAxis().getUnit().toString());
				columnInfoIntensities.setUtype("Data.FluxAxis.Value");
				colInfos_ = new ColumnInfo[] { columnInfoWave, columnInfoIntensities };
			}

			@Override
			public String getName() {
				return "spectrum";
			}

			@SuppressWarnings("rawtypes")
			@Override
			public List getParameters() {
				List<DescribedValue> list = new ArrayList<DescribedValue>();

				final DefaultValueInfo defaultValueInfo = new DefaultValueInfo("vlsr");
				DescribedValue describedValue = new DescribedValue(defaultValueInfo, commentedSpectrum.getVlsr());
				list.add(describedValue);

				DefaultValueInfo nameValueInfo = new DefaultValueInfo("TargetName", String.class);
				nameValueInfo.setUCD("meta.id;src");
				nameValueInfo.setUtype("Spectrum.Target.Name");
				DescribedValue nameValue = new DescribedValue(nameValueInfo, commentedSpectrum.getTitle());
				list.add(nameValue);

				DefaultValueInfo nameValueInfo2 = new DefaultValueInfo("Telescope", String.class);
				nameValueInfo2.setUCD("meta.id;instr.tel");
				nameValueInfo2.setUtype("ObsConfig.Facility.Name");
				list.add(new DescribedValue(nameValueInfo2, commentedSpectrum.getCassisMetadata("telescope").getValue()));


				return list;
			}

			@Override
			public ColumnInfo getColumnInfo(int arg0) {
				return colInfos_[arg0];
			}

			@Override
			public int getColumnCount() {
				return 2;
			}

			@Override
			public long getRowCount() {
				return commentedSpectrum.getSize();
			}

			@Override
			public Object getCell(long lrow, int icol) {
				int irow = checkedLongToInt(lrow);
				switch (icol) {
				case 0:
					return commentedSpectrum.getFrequenciesSignal()[irow];
				case 1:
					return commentedSpectrum.getIntensities()[irow];
				default:
					throw new IllegalArgumentException();
				}
			}
		};
	}

	private CassisSpectrum getCassisSpectrum(StarTable starTable, double vlsrValue, XAxisCassis xAxisCassis, int numColWave, YAxisCassis yAxisCassis, int numColFlux) throws IOException {
		int nbPoints = (int) starTable.getRowCount();
		double[] signalFrequencies = new double[nbPoints];
		double[] intensities = new double[nbPoints];

		RowSequence rseq = null;
		try {
			rseq = starTable.getRowSequence();
			if (!xAxisCassis.isInverted()) {
				int i = 0;
				while (rseq.next()) {
					Object[] row = rseq.getRow();
					signalFrequencies[i] = xAxisCassis.convertToMHzFreq(Double.parseDouble(starTable.getColumnInfo(numColWave).formatValue(row[numColWave], 30)));
					intensities[i] = Double.parseDouble(starTable.getColumnInfo(numColFlux).formatValue(row[numColFlux], 30));// intensity
					i++;
				}
			} else {
				int i = nbPoints - 1;
				while (rseq.next()) {
					Object[] row = rseq.getRow();
					signalFrequencies[i] = xAxisCassis.convertToMHzFreq(Double.parseDouble(starTable.getColumnInfo(numColWave).formatValue(row[numColWave], 30)));
					intensities[i] = Double.parseDouble(starTable.getColumnInfo(numColFlux).formatValue(row[numColFlux], 30));// intensity
					i--;
				}
			}
		} finally {
			if (rseq != null) {
				try {
					rseq.close();
				} catch (IOException ioe) {
				}
			}
		}

		return CassisSpectrum.generateCassisSpectrum(starTable.getName(), signalFrequencies, intensities, vlsrValue, xAxisCassis, yAxisCassis);
	}

	/**
	 * Know if votable file contains datalink
	 *
	 * @param path
	 *            The path file
	 * @return True if file contains datalink and false if not
	 */
	public static boolean containDatalink(String path) {
		boolean datalink = false;
		VOElement rootElement;
		try {
			rootElement = new VOElementFactory().makeVOElement(new File(path));
			NodeList resourceList = rootElement.getElementsByVOTagName("RESOURCE");
			if (resourceList.getLength() > 1) {
				datalink = true;
			}
		} catch (SAXException | IOException e) {
		}
		return datalink;
	}

	@Override
	public FileType getType() {
		return FileType.VOTABLE;
	}

	@Override
	public CassisSpectrum read(int spectrumIndex) {
		return cassisSpectrumList.get(spectrumIndex);
	}

	@Override
	public List<CassisMetadata> readCassisMetadata(int spectrumIndex) {
		return cassisSpectrumList.get(spectrumIndex).getOriginalMetadataList();
	}

	@Override
	public List<CassisSpectrum> readAll() {
		read();
		return cassisSpectrumList;
	}

	@Override
	public CassisMetadata getCassisMetadata(String name, int spectrumIndex) {
		CassisMetadata cassisMetadata = new CassisMetadata();
		if (spectrumIndex >= 0 && spectrumIndex <= readNbCassisSpectra()) {
			List<CassisMetadata> cassisMetadataList = cassisSpectrumList.get(spectrumIndex).getOriginalMetadataList();
			for (CassisMetadata cassisMetadataTemp : cassisMetadataList) {
				if (cassisMetadataTemp.getName().equalsIgnoreCase(name)) {
					cassisMetadata = cassisMetadataTemp;
				}
			}
		}
		return cassisMetadata;
	}

	@Override
	public List<CassisMetadata> getCommonCassisMetadataList() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Return the wave columns. <b>Warning. The spectrum must be read first and the list
	 *  will return only one element at most which contains all wave columns</b>.
	 *  This is done like this for spectrum format who can have multiple spectrum,
	 *  which is not the case here.
	 *
	 * @return the wave columns detected.
	 * @see eu.omp.irap.cassis.file.InterfaceFileManager#getWaveColumnsDetected()
	 */
	@Override
	public List<ColumnsDetected> getWaveColumnsDetected() {
		return waveColumnsList;
	}

	/**
	 * Return the flux columns. <b>Warning. The spectrum must be read first and the list
	 *  will return only one element at most which contains all flux columns</b>.
	 *  This is done like this for spectrum format who can have multiple spectrum,
	 *  which is not the case here.
	 *
	 * @return the flux columns detected.
	 * @see eu.omp.irap.cassis.file.InterfaceFileManager#getFluxColumnsDetected()
	 */
	@Override
	public List<ColumnsDetected> getFluxColumnsDetected() {
		return fluxColumnsList;
	}

	/**
	 * Return the flux columns for the given StarTable.
	 *  <b>Warning. The spectrum must be read first and the list will return only
	 *  one element at most which contains all flux columns</b>.
	 *  This is done like this for spectrum format who can have multiple spectrum,
	 *  which is not the case here.
	 *
	 * @param starTable The StarTable object.
	 * @return the flux columns detected.
	 */
	private List<ColumnsDetected> getFluxColumnsDetected(StarTable starTable) {
		int nCol = starTable.getColumnCount();
		List<ColumnsDetected> cols = new ArrayList<ColumnsDetected>(1);
		List<ColumnInformation> colInfoList = new ArrayList<>();
		for (int i = 0; i < nCol; i++) {
			ColumnInfo field = starTable.getColumnInfo(i);
			if (isFluxField(field)) {
				colInfoList.add(new ColumnInformation(field.getName(), field.getUnitString(), false, i));
			}
		}
		cols.add(new ColumnsDetected(null, colInfoList));
		return cols;
	}

	/**
	 * Return the wave columns for the given StarTable.
	 *  <b>Warning. The spectrum must be read first and the list will return only
	 *  one element at most which contains all wave columns</b>.
	 *  This is done like this for spectrum format who can have multiple spectrum,
	 *  which is not the case here.
	 *
	 * @param starTable The StarTable object.
	 * @return the wave columns detected.
	 */
	private List<ColumnsDetected> getWaveColumnsDetected(StarTable starTable) {
		int nCol = starTable.getColumnCount();
		List<ColumnsDetected> cols = new ArrayList<ColumnsDetected>(1);
		List<ColumnInformation> colInfoList = new ArrayList<>();
		for (int i = 0; i < nCol; i++) {
			ColumnInfo field = starTable.getColumnInfo(i);
			if (isWaveField(field)) {
				colInfoList.add(new ColumnInformation(field.getName(), field.getUnitString(), false,i));
			}
		}
		cols.add(new ColumnsDetected(null, colInfoList));
		return cols;
	}

	/**
	 * Check if the provided field/column is considered as a wave column.
	 *
	 * @param field The field/column.
	 * @return true if the field is considered as a wave column, false otherwise.
	 */
	private boolean isWaveField(ColumnInfo field) {
		return ((field.getUtype() != null) && (field.getUtype().contains("Data.SpectralAxis.Value")))
				|| "wave".equalsIgnoreCase(field.getName()) || "wavelength".equalsIgnoreCase(field.getName())
				|| "freq".equalsIgnoreCase(field.getName()) || "usbfrequency".equalsIgnoreCase(field.getName())
				|| "frequency".equalsIgnoreCase(field.getName()) || "lofrequency".equalsIgnoreCase(field.getName())
				|| "swaawave".equalsIgnoreCase(field.getName()) || UNIT.isWaveUnit(UNIT.toUnit(field.getUnitString()));
	}


	/**
	 * Check if the provided field/column is considered as a flux column.
	 *
	 * @param field The field/column.
	 * @return true if the field is considered as a flux column, false otherwise.
	 */
	private boolean isFluxField(ColumnInfo field) {
		return !UNIT.isWaveUnit(UNIT.toUnit(field.getUnitString())) ;
	}

	/**
	 * Set the column used.
	 *
	 * @param name The name of the column used.
	 * @param unit The unit of the values in the column.
	 * @param index The index of the column used.
	 * @param type The type of the column.
	 */
	private void setColumnUsed(String name, String unit, int index, COLUMN_TYPE type) {
		ensureListNotNull(type);
		List<ColumnsDetected> list = type == COLUMN_TYPE.FLUX ?
				fluxColumnsList : waveColumnsList;

		if (list.isEmpty()) {
			List<ColumnInformation> colInfoList = new ArrayList<>();
			colInfoList.add(new ColumnInformation(name,  unit, false, index));
			list.add(new ColumnsDetected(name, colInfoList));
		} else {
			ColumnsDetected cd = list.get(0);
			if (!cd.containsColumn(name)) {
				cd.getColumnsDetectedList().add(new ColumnInformation(name, unit, false,index));
			}
			cd.setColumnUsed(name);
		}
	}

	/**
	 * Ensure the list for the given type is not null, create it if necessary.
	 *
	 * @param type The type of the column.
	 */
	private void ensureListNotNull(COLUMN_TYPE type) {
		if (type == COLUMN_TYPE.FLUX && fluxColumnsList == null) {
			fluxColumnsList = new ArrayList<>(1);
		} else if (type == COLUMN_TYPE.WAVE && waveColumnsList == null) {
			waveColumnsList = new ArrayList<>(1);
		}
	}

}
