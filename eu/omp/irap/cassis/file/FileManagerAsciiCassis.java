/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.regex.Pattern;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.CassisMultipleSpectrum;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.XAxisVelocity;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.file.gui.ColumnsDetected;

/**
 * Read file *.fus from Full Spectrum and *.lis from Line Spectrum
 */
public class FileManagerAsciiCassis extends FileManager implements SaveSpectrumInterface {

	private int nbPoints;
	private double vlsr;
	private String line;
	private List<CassisMetadata> cassisMetadataList;
	private List<CassisSpectrum> cassisSpectrumList;
	private int nbCassisSpectra;
	private CassisSpectrum cassisSpectrum;


	/**
	 * Construct a FileManagerAsciiCassis with exceptions displayed.
	 *
	 * @param file The input File.
	 */
	public FileManagerAsciiCassis(File file) {
		this(file, true);
	}

	/**
	 * Construct a FileManagerAscii.
	 *
	 * @param file The input File.
	 * @param displayException If the exceptions should be displayed.
	 */
	public FileManagerAsciiCassis(File file, boolean displayException) {
		super(file, displayException);
		cassisMetadataList = new ArrayList<CassisMetadata>();
		cassisSpectrumList = new ArrayList<CassisSpectrum>();
	}

	@Override
	public CassisSpectrum read() {
		CassisSpectrum cassisSpectrum = null;
		try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
			try {
				cassisSpectrum = read(br, file.getName());
			} catch (Exception e) {
				if (displayException) {
					e.printStackTrace();
				}
				cassisSpectrum = null;
			}
		} catch (IOException e) {
		}
		return cassisSpectrum;
	}

	public CassisSpectrum read(BufferedReader br, String title) throws IOException {
		cassisSpectrum = null;
		String line = readHeader(br);
		if (line == null) {
			cassisSpectrum = null;
		} else {
			double[] signalFrequencies = null;
			double[] intensities = null;
			double[] imageFrequencies = null;
			ArrayList<Double> loFreqs = new ArrayList<Double>();
			ArrayList<Double> freqs = new ArrayList<Double>();
			signalFrequencies = new double[nbPoints];
			intensities = new double[nbPoints];
			imageFrequencies = new double[nbPoints];

			for (int cpt = 0; cpt < nbPoints; cpt++) {
				line = br.readLine();
				if (line == null)
					break;
				if (line.equals(""))
					continue;
				String[] val = Pattern.compile("[\t]+").split(line);
				signalFrequencies[cpt] = Double.valueOf(val[0]);
				imageFrequencies[cpt] = Double.valueOf(val[2]);
				intensities[cpt] = Double.valueOf(val[4]);
			}

			double lastLoFrequency = 0;

			for (int cpt = 0; cpt < nbPoints; cpt++) {
				if (imageFrequencies[cpt] != 0) {
					double currentLoFrequency = signalFrequencies[cpt] + (imageFrequencies[cpt] - signalFrequencies[cpt]) / 2.0;
					if (Math.abs(currentLoFrequency - lastLoFrequency) > 1e-6) {
						lastLoFrequency = currentLoFrequency;
						loFreqs.add(lastLoFrequency);
						freqs.add(signalFrequencies[cpt]);
					}
				}
			}

			double loFrequency = 0;
			boolean haveImage = !loFreqs.isEmpty();

			if (haveImage)
				loFrequency = signalFrequencies[0] + (imageFrequencies[0] - signalFrequencies[0]) / 2.0;
			cassisSpectrum = CassisSpectrum.generateCassisSpectrum(title, -1.0, signalFrequencies, intensities, loFrequency, vlsr, XAxisCassis.getXAxisFrequency(), YAxisCassis.getYAxisKelvin());

			if (haveImage && loFreqs.size() > 1) {
				try {
					cassisSpectrum = createMultiSpectrum(cassisSpectrum, loFreqs, freqs, title, cassisSpectrum.getOriginalMetadataList());
				} catch (Exception e) {
					cassisSpectrum.setLoFrequency(Double.NaN);
				}
			}
			cassisSpectrum.addCassisMetadata(new CassisMetadata(CassisMetadata.FILE_TYPE,
					FileManager.FileType.ASCIICASSIS.name(), null, null), true);
		}
		return cassisSpectrum;
	}

	@Override
	public List<CassisSpectrum> readAll() {
		try {
			readData();
		} catch (IOException e) {
			if (displayException) {
				e.printStackTrace();
			}
		}
		return cassisSpectrumList;
	}

	/**
	 * Read all data in file and fill the cassisSpectrumList.
	 *
	 * @throws IOException
	 *             Fail or interruption operations
	 */
	public void readData() throws IOException {
		double[] signalFrequencies = null;
		double[] intensities = null;
		double[] imageFrequencies = null;
		ArrayList<Double> loFreqs = new ArrayList<Double>();
		ArrayList<Double> freqs = new ArrayList<Double>();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
			line = br.readLine();
			while (line != null) {
				if (!line.trim().equals("")) {
					boolean isSpectrum = readTheHeader(br);
					if (isSpectrum) {
						nbCassisSpectra++;
						signalFrequencies = new double[nbPoints];
						intensities = new double[nbPoints];
						imageFrequencies = new double[nbPoints];

						for (int cpt = 0; cpt < nbPoints; cpt++) {
							line = br.readLine();
							if (line == null)
								break;
							if (line.equals(""))
								continue;
							String[] val = Pattern.compile("[\t]+").split(line);
							signalFrequencies[cpt] = Double.valueOf(val[0]);
							imageFrequencies[cpt] = Double.valueOf(val[2]);
							intensities[cpt] = Double.valueOf(val[4]);
							double lastLoFrequency = 0;

							if (imageFrequencies[cpt] != 0) {
								double currentLoFrequency = signalFrequencies[cpt] + (imageFrequencies[cpt] - signalFrequencies[cpt]) / 2.0;
								if (Math.abs(currentLoFrequency - lastLoFrequency) > 1e-6) {
									lastLoFrequency = currentLoFrequency;
									loFreqs.add(lastLoFrequency);
									freqs.add(signalFrequencies[cpt]);
								}
							}
						}

						double loFrequency = 0;
						boolean haveImage = !loFreqs.isEmpty();

						if (haveImage) {
							loFrequency = signalFrequencies[0] + (imageFrequencies[0] - signalFrequencies[0]) / 2.0;
						}

						CassisSpectrum cassisSpectrum = CassisSpectrum.generateCassisSpectrum(file.getName(), -1.0, signalFrequencies, intensities, loFrequency, vlsr, XAxisCassis.getXAxisFrequency(),
								YAxisCassis.getYAxisKelvin());
						cassisSpectrum.setOriginalMetadataList(cassisMetadataList);
						cassisSpectrum.addCassisMetadata(new CassisMetadata(CassisMetadata.FILE_TYPE,
								FileManager.FileType.ASCIICASSIS.name(), null, null), true);
						cassisSpectrumList.add(cassisSpectrum);
					}
				}
				line = br.readLine();
			}
		}
	}

	/**
	 * Fill cassisMetadataList according to spectrum.
	 *
	 * @param br
	 * @return Return true if there is a spectrum and false if not
	 * @throws IOException
	 *             Fail or interruption operations
	 */
	private boolean readTheHeader(BufferedReader br) throws IOException {
		boolean isSpectrum = false;
		cassisMetadataList = new ArrayList<CassisMetadata>();
		while ((line != null) && (line.startsWith("//"))) {
			String[] metadata = line.replace("//", "").split(":");
			String key = metadata[0].trim().toLowerCase();
			String value = metadata[1].trim();
			if (key.equals("number of line")) {
				nbPoints = Integer.parseInt(value);
				isSpectrum = true;
				cassisMetadataList.add(new CassisMetadata("Point number", value, "", ""));
			} else if (key.equals("vlsr")) {
				vlsr = Double.valueOf(value);
				cassisMetadataList.add(new CassisMetadata("vlsr", value, "", ""));
			} else if (key.equals("telescope")) {
				cassisMetadataList.add(new CassisMetadata("Telescope", value, "", ""));
			} else if (line.startsWith("// ")) {
				cassisMetadataList.add(new CassisMetadata(key, value, "", ""));
			}
			line = br.readLine();
		}
		return isSpectrum;
	}

	private CassisSpectrum createMultiSpectrum(CassisSpectrum cassisSpectrum, List<Double> loFreqs, List<Double> freqs, String title, List<CassisMetadata> cassisMetadataList) {
		double[] signalFrequencies;
		double[] intensities;
		CassisMultipleSpectrum cassisMultipleSpectrum = new CassisMultipleSpectrum(cassisSpectrum);

		CassisSpectrum[] spectrums = new CassisSpectrum[loFreqs.size()];
		int currentIndice = 0;
		signalFrequencies = cassisSpectrum.getFrequencies();
		intensities = cassisSpectrum.getIntensities();

		for (int i = 0; i < spectrums.length - 1; i++) {
			int indiceLastfreq = currentIndice;
			while (indiceLastfreq < signalFrequencies.length && signalFrequencies[indiceLastfreq] < freqs.get(i + 1))
				indiceLastfreq++;

			final double[] newFrequencies = Arrays.copyOfRange(signalFrequencies, currentIndice, indiceLastfreq);
			final double[] newIntensities = Arrays.copyOfRange(intensities, currentIndice, indiceLastfreq);
			double newLo = loFreqs.get(i);

			CassisSpectrum spectrum = CassisSpectrum.generateCassisSpectrum(title, -1.0, newFrequencies, newIntensities, newLo, vlsr, XAxisCassis.getXAxisFrequency(), YAxisCassis.getYAxisKelvin());
			cassisSpectrum.setOriginalMetadataList(cassisMetadataList);
			spectrums[i] = spectrum;
			currentIndice = indiceLastfreq;
		}
		double newLo = loFreqs.get(spectrums.length - 1);
		CassisSpectrum spectrum = CassisSpectrum.generateCassisSpectrum(title, -1.0, Arrays.copyOfRange(signalFrequencies, currentIndice, cassisSpectrum.getNbChannel()),
				Arrays.copyOfRange(intensities, currentIndice, cassisSpectrum.getNbChannel()), newLo, vlsr, XAxisCassis.getXAxisFrequency(), YAxisCassis.getYAxisKelvin());
		cassisSpectrum.setOriginalMetadataList(cassisMetadataList);
		spectrums[spectrums.length - 1] = spectrum;
		cassisMultipleSpectrum.setScan(spectrums);
		return cassisMultipleSpectrum;
	}

	/**
	 * @param br
	 * @return the next line after the header
	 * @throws IOException
	 */
	public String readHeader(BufferedReader br) throws IOException {
		cassisMetadataList = new ArrayList<CassisMetadata>();
		String line = null;
		do {
			line = br.readLine();
			if (line != null && line.startsWith("//")) {
				String[] metadata = line.replace("//", "").split(":");
				String key = metadata[0].trim().toLowerCase();
				String value = metadata[1].trim();
				if (key.equals("number of line")) {
					nbPoints = Integer.parseInt(value);
					cassisMetadataList.add(new CassisMetadata("Point number", value, "", ""));
				} else if (key.equals("vlsr")) {
					vlsr = Double.valueOf(value);
					cassisMetadataList.add(new CassisMetadata("vlsr", value, "", ""));
				} else if (key.equals("telescope")) {
					cassisMetadataList.add(new CassisMetadata("Telescope", value, "", ""));
				} else if (line.startsWith("// ")) {
					cassisMetadataList.add(new CassisMetadata(key, value, "", ""));
				}
			}
		} while (line != null && line.startsWith("//"));
		return line;
	}

	@Override
	public boolean save(File file, CommentedSpectrum spectrum) {
		try (BufferedWriter fileAscii = new BufferedWriter(new FileWriter(file))) {
			List<String> lines = getSaveContent(spectrum);
			fileAscii.write("// number of line : " + lines.size());
			fileAscii.newLine();
			fileAscii.write("// vlsr : " + spectrum.getVlsr());
			fileAscii.newLine();
			fileAscii.write("FreqLsb VeloLsb FreqUsb VeloUsb Intensity DeltaF DeltaV");
			for (String line : lines) {
				fileAscii.newLine();
				fileAscii.write(line);
				fileAscii.flush();
			}
			fileAscii.newLine();
			return true;
		} catch (IOException e) {
			logger.log(Level.FINER, "Error during the save ", e);
			return false;
		}
	}

	public boolean save(File file, List<CommentedSpectrum> spectrums) {
		double saveVlsr = 0.;
		int size = 0;
		String[][][] tabs = new String[spectrums.size()][][];

		// recuperation des donnees a ecrire
		int nbSeries = getDataCurves(tabs, spectrums);
		if (nbSeries == 0) {
			return false;
		}

		CommentedSpectrum spectrum = spectrums.get(0);
		saveVlsr = spectrum.getVlsr();
		size = spectrum.getSize();
		return writeFileAscii(file, tabs, saveVlsr, size, nbSeries);
	}

	/**
	 * Create the output ascii file.
	 *
	 * @param file
	 *            file whitch is written
	 * @param tab
	 *            the array of data
	 * @param vlsr
	 * @param nbSeries
	 *            the number of series
	 * @param nbSeries2
	 * @return if the file is create
	 */
	private boolean writeFileAscii(File file, String[][][] tab, double vlsr, int size, int nbSeries) {
		try (BufferedWriter fileAscii = new BufferedWriter(new FileWriter(file))) {
			fileAscii.write("// number of line : " + size);
			fileAscii.newLine();
			fileAscii.write("// vlsr : " + vlsr);
			fileAscii.newLine();
			int maxLenght = tab[0][0].length;

			for (int i = 1; i < nbSeries; i++) {
				maxLenght = Math.max(maxLenght, tab[i][0].length);
			}

			for (int i = 0; i < maxLenght; i++) {
				for (int j = 0; j < nbSeries; j++) {
					for (int k = 0; k < 7; k++) {

						if (tab[j][k].length > i) {
							fileAscii.write(tab[j][k][i] + "\t");
						} else {
							for (int l = 0; l < 7; l++) {
								fileAscii.write("\t");
							}
							k = 6;
						}
					}
				}
				fileAscii.flush();
				fileAscii.newLine();
			}
			return true;
		} catch (IOException e) {
			logger.log(Level.FINER, "Error while writing the file.");
			return false;
		}
	}

	/**
	 * Build the array tabs and return the number of serie.
	 *
	 * @param tabs
	 *            array which contain all of the data to build the output file
	 * @return the number of serie
	 */
	private int getDataCurves(String[][][] tabs, List<CommentedSpectrum> spectrums) {
		int nbSeries = 0;
		for (CommentedSpectrum spectrum : spectrums) {
			String[][] tab = new String[7][spectrum.getSize() + 1];
			setTableauData(tab, spectrum);
			tabs[nbSeries] = tab;
			nbSeries++;
		}
		return nbSeries;
	}

	/**
	 * Update the data of the array used by savePlotAscii.
	 *
	 * @param tab
	 * @param fullSerie
	 */
	private void setTableauData(String[][] tab, CommentedSpectrum spectrum) {
		tab[0][0] = "FreqLsb";
		tab[1][0] = "VeloLsb";
		tab[2][0] = "FreqUsb";
		tab[3][0] = "VeloUsb";
		tab[4][0] = "Int";
		tab[5][0] = "DeltaF";
		tab[6][0] = "DeltaV";

		DecimalFormatSymbols ds = new DecimalFormatSymbols();
		ds.setDecimalSeparator('.');
		DecimalFormat deltaFreqFormat = new DecimalFormat("###0.000000000", ds);// 8c
		DecimalFormat deltaVeloFormat = new DecimalFormat("###0.000", ds);// 8c
		DecimalFormat veloFormat = new DecimalFormat("#####0.000", ds);// 10c
		DecimalFormat freqFormat = new DecimalFormat("######0.000000000", ds);// 11c
		DecimalFormat intensityFormat = new DecimalFormat("##0.0000", ds);// 8c

		double[] frequencySignal = spectrum.getFrequenciesSignal();
		double[] deltaV = spectrum.getDeltaV();
		double[] deltaF = spectrum.getDeltaF();
		double[] intensity = spectrum.getIntensities();
		double[] velocitySignal = spectrum.getVelocitySignal();
		// TODO
		// double[] velocityImage = fullSerie.getVelocityImage();
		int size = frequencySignal.length;
		for (int j = 0; j < size; j++) {
			tab[0][j + 1] = freqFormat.format(frequencySignal[j]);// fred LSB
			tab[1][j + 1] = veloFormat.format(velocitySignal[size - 1 - j]);// velo LSB
			double frequencyImage = 0;
			final double loFreq = spectrum.getLoFreq();
			if (!Double.isNaN(loFreq)) {
				frequencyImage = frequencySignal[j] + 2 * (loFreq - frequencySignal[j]);
			}
			tab[2][j + 1] = freqFormat.format(frequencyImage);// freq USB
			tab[3][j + 1] = "000.00";// velo USB
			if (Double.isNaN(intensity[j])) {
				tab[4][j + 1] = "NaN";
			} else {
				tab[4][j + 1] = intensityFormat.format(intensity[j]);// intensity
			}
			tab[5][j + 1] = deltaFreqFormat.format(deltaF[j]);// deltaF
			tab[6][j + 1] = deltaVeloFormat.format(deltaV[j]);// deltaV
		}
	}

	public boolean save(File file, CassisSpectrum spectrum) {
		DecimalFormatSymbols ds = new DecimalFormatSymbols();
		ds.setDecimalSeparator('.');

		DecimalFormat deltaVeloFormat = new DecimalFormat("###0.000", ds);// 8c
		DecimalFormat deltaFreqFormat = new DecimalFormat("###0.000000000", ds);// 8c
		DecimalFormat veloFormat = new DecimalFormat("#####0.000", ds);// 10c
		DecimalFormat freqFormat = new DecimalFormat("######0.000000000", ds);// 11c
		DecimalFormat intensityFormat = new DecimalFormat("##0.0000", ds);// 8c

		try (BufferedWriter fileAscii = new BufferedWriter(new FileWriter(file))) {
			double[] frequencySignal = spectrum.getFrequencies();
			double[] intensity = spectrum.getIntensities();

			int size = frequencySignal.length;
			fileAscii.write("// number of line : " + size);
			fileAscii.newLine();
			fileAscii.write("// vlsr : " + spectrum.getVlsr());
			fileAscii.newLine();
			fileAscii.write("// version : " + "4.2");
			fileAscii.newLine();
			fileAscii.write("// origin : " + "CASSIS");
			fileAscii.newLine();
			fileAscii.write("// date : " + new Date());
			fileAscii.newLine();
			for (CassisMetadata meta :spectrum.getCassisMetadataList()){
				fileAscii.write("// " + meta.getName() + " : " + meta.getValue());
				fileAscii.newLine();
			}
			fileAscii.write("FreqLsb VeloLsb FreqUsb VeloUsb Intensity DeltaF DeltaV");
			XAxisVelocity axisCassis = XAxisCassis.getXAxisVelocity();
			axisCassis.setVlsr(spectrum.getVlsr());
			axisCassis.setFreqRef(spectrum.getFreqMax() - spectrum.getFreqMin() / 2 + spectrum.getFreqMin());

			for (int i = 0; i < size; i++) {
				fileAscii.newLine();
				double frequencyImage = 0;
				final double loFreq = spectrum.getLoFrequency(frequencySignal[i]);
				if (!Double.isNaN(loFreq)) {
					frequencyImage = frequencySignal[i] + 2 * (loFreq - frequencySignal[i]);
				}
				double deltaF = (i != size - 1) ? Math.abs(frequencySignal[i + 1] - frequencySignal[i]) : Math.abs(frequencySignal[i - 1] - frequencySignal[i]);
				double deltaV = axisCassis.convertDeltaFromMhz(deltaF, axisCassis.getFreqRef());
				if (Double.isNaN(intensity[i])) {
					fileAscii.write(freqFormat.format(frequencySignal[i]) + "\t" + veloFormat.format(axisCassis.convertToMHzFreq(frequencySignal[i])) + "\t" + freqFormat.format(frequencyImage) + "\t"
							+ 0.0 + "\t" + "NaN" + "\t" + deltaFreqFormat.format(deltaF) + "\t" + deltaVeloFormat.format(deltaV));
				} else {
					fileAscii.write(freqFormat.format(frequencySignal[i]) + "\t" + veloFormat.format(axisCassis.convertToMHzFreq(frequencySignal[i])) + "\t" + freqFormat.format(frequencyImage) + "\t"
							+ 0.0 + "\t" + intensityFormat.format(intensity[i]) + "\t" + deltaFreqFormat.format(deltaF) + "\t" + deltaVeloFormat.format(deltaV));
				}

				fileAscii.flush();
			}
			fileAscii.newLine();
			return true;
		} catch (IOException e) {
			logger.log(Level.FINER, "Error during the save ", e);
			return false;
		}
	}

	public List<String> getSaveContent(CommentedSpectrum spectrum) {
		DecimalFormatSymbols ds = new DecimalFormatSymbols();
		ds.setDecimalSeparator('.');

		DecimalFormat deltaVeloFormat = new DecimalFormat("###0.000", ds);// 8c
		DecimalFormat deltaFreqFormat = new DecimalFormat("###0.000000000", ds);// 8c
		DecimalFormat veloFormat = new DecimalFormat("#####0.000", ds);// 10c
		DecimalFormat freqFormat = new DecimalFormat("######0.000000000", ds);// 11c
		DecimalFormat intensityFormat = new DecimalFormat("##0.0000", ds);// 8c

		List<String> listLines = new ArrayList<String>();
		double[] frequencySignal = spectrum.getFrequenciesSignal();
		double[] deltaV = spectrum.getDeltaV();
		double[] deltaF = spectrum.getDeltaF();
		double[] intensity = spectrum.getIntensities();
		double[] velocitySignal = spectrum.getVelocitySignal();
		int size = frequencySignal.length;
		int coeffMulti = 1;// getCoefMulti(intensity);

		String line;
		for (int i = 0; i < size; i++) {
			double frequencyImage = 0;
			final Double loFreq = spectrum.getLoFreq();
			if (!Double.isNaN(loFreq)) {
				frequencyImage = frequencySignal[i] + 2 * (loFreq - frequencySignal[i]);
			}
			if (Double.isNaN(intensity[i])) {
				line = freqFormat.format(frequencySignal[i]) + "\t" + veloFormat.format(velocitySignal[size - 1 - i]) + "\t" + freqFormat.format(frequencyImage) + "\t" + 0.0 + "\t" + "NaN" + "\t"
						+ deltaFreqFormat.format(deltaF[i]) + "\t" + deltaVeloFormat.format(deltaV[i]);
			} else {
				line = freqFormat.format(frequencySignal[i]) + "\t" + veloFormat.format(velocitySignal[size - 1 - i]) + "\t" + freqFormat.format(frequencyImage) + "\t" + 0.0 + "\t"
						+ intensityFormat.format(intensity[i] / coeffMulti) + "\t" + deltaFreqFormat.format(deltaF[i]) + "\t" + deltaVeloFormat.format(deltaV[i]);
			}
			listLines.add(line);
		}
		return listLines;
	}

	public boolean saveOnGallery(File file, List<CommentedSpectrum> spectrums) {
		List<String> allLines = new ArrayList<String>();

		for (CommentedSpectrum cs : spectrums) {
			allLines.addAll(getSaveContent(cs));
		}

		try (BufferedWriter fileAscii = new BufferedWriter(new FileWriter(file))) {
			fileAscii.write("// number of line : " + allLines.size());
			fileAscii.newLine();
			fileAscii.write("// vlsr : " + spectrums.get(0).getVlsr());
			fileAscii.newLine();
			// TODO remove method getCoefMulti
			int coeffMulti = 1;// getCoefMulti(intensity);
			if (coeffMulti != 1) {
				fileAscii.write("//scaling_factor_y : " + coeffMulti);
				fileAscii.newLine();
			}
			fileAscii.write("FreqLsb VeloLsb FreqUsb VeloUsb Intensity DeltaF DeltaV");
			for (String line : allLines) {
				fileAscii.newLine();
				fileAscii.write(line);
				fileAscii.flush();
			}
			fileAscii.newLine();
			return true;
		} catch (Exception e) {
			logger.log(Level.FINER, "Error during the save ", e);
			return false;
		}
	}

	@Override
	public FileType getType() {
		return FileType.ASCIICASSIS;
	}

	@Override
	public CassisSpectrum read(int spectrumIndex) {
		return cassisSpectrumList.get(spectrumIndex);
	}

	@Override
	public List<CassisMetadata> readCassisMetadata(int spectrumIndex) {
		return cassisSpectrumList.get(spectrumIndex).getOriginalMetadataList();
	}

	@Override
	public CassisMetadata getCassisMetadata(String name, int spectrumIndex) {
		CassisMetadata cassisMetadata = new CassisMetadata();
		if (spectrumIndex >= 0 && spectrumIndex <= readNbCassisSpectra()) {
			List<CassisMetadata> cassisMetadataList = cassisSpectrumList.get(spectrumIndex).getOriginalMetadataList();
			for (CassisMetadata cassisMetadataTemp : cassisMetadataList) {
				if (cassisMetadataTemp.getName().equalsIgnoreCase(name)) {
					cassisMetadata = cassisMetadataTemp;
				}
			}
		}
		return cassisMetadata;
	}

	@Override
	public int readNbCassisSpectra() {
		return nbCassisSpectra;
	}

	@Override
	public List<CassisMetadata> getCommonCassisMetadataList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ColumnsDetected> getWaveColumnsDetected() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ColumnsDetected> getFluxColumnsDetected() {
		// TODO Auto-generated method stub
		return null;
	}
}
