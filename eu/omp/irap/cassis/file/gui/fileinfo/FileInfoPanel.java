/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.fileinfo;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.file.gui.cassisspectrum.CassisSpectrumInfo;

/**
 * Create a Panel with all information about a spectrum
 *
 * @author Lea Foissac
 */
public class FileInfoPanel extends JPanel {

	private static final long serialVersionUID = 5848821408695017903L;

	private JTextArea textArea;
	private CassisSpectrum cassisSpectrum;
	private CassisSpectrumInfo cassisSpectrumInfo;

	/**
	 * Constructor Create FileInfoPanel using CassisSpectrum and CassisSpectrumInfo.
	 *
	 * @param cassisSpectrum
	 *            The CassisSpectrum
	 * @param cassisSpectrumInfo
	 *            The CassisSpectrumInfo
	 */
	public FileInfoPanel(CassisSpectrum cassisSpectrum, CassisSpectrumInfo cassisSpectrumInfo) {
		this.cassisSpectrum = cassisSpectrum;
		this.cassisSpectrumInfo = cassisSpectrumInfo;
		getTextArea();
		addInformationToTextArea();
	}

	public FileInfoPanel(CommentedSpectrum commentedSpectrum) {
		getTextArea(commentedSpectrum);
	}

	/**
	 * Create if it not exists JTextArea With all information about CassisSpectrum.
	 *
	 * @return JTextarea
	 */
	public JTextArea getTextArea() {
		if (textArea == null) {
			textArea = new JTextArea();
			new JScrollPane(textArea);
			add(textArea);
			textArea.setEditable(false);
			textArea.setBackground(this.getBackground());
		}
		return textArea;
	}

	/**
	 * Add information in JTextArea.
	 */
	private void addInformationToTextArea() {
		StringBuilder sb = new StringBuilder();
		sb.append("Path: ").append(cassisSpectrumInfo.getPath()).append('\n');
		sb.append("Name: ").append(cassisSpectrumInfo.getName()).append('\n');
		sb.append('\n');
		sb.append("Points number: ").append(cassisSpectrum.getNbChannel()).append('\n');
		sb.append('\n');
		sb.append("Xunit: ").append(cassisSpectrum.getxAxisOrigin().toString()).append('\n');

		sb.append("Xmin: ").append(cassisSpectrum.getOriginalWaveMin()).append('\n');
		sb.append("Xmax: ").append(cassisSpectrum.getOriginalWaveMax()).append('\n');
		sb.append('\n');
		sb.append("Yunit: ").append(cassisSpectrum.getYAxis().toString()).append('\n');
		sb.append("Ymin: ").append(cassisSpectrum.getIntensityMin()).append('\n');
		sb.append("Ymax: ").append(cassisSpectrum.getIntensityMax());

		textArea.setText(sb.toString());
	}

	/**
	 * Create if not exists {@link JTextArea} with all information about {@link CommentedSpectrum}
	 *
	 * @param commentedSpectrum
	 *            The {@link CommentedSpectrum}
	 * @return All information about {@link CommentedSpectrum} given in a {@link JTextArea}
	 */
	public JTextArea getTextArea(CommentedSpectrum commentedSpectrum) {
		if (textArea == null) {
			textArea = new JTextArea();
			new JScrollPane(textArea);
			add(textArea);
			textArea.setEditable(false);
			textArea.setBackground(this.getBackground());
			addInformationToTextArea(commentedSpectrum);
		}
		return textArea;
	}

	/**
	 * Add information about {@link CommentedSpectrum} on {@link JTextArea}
	 *
	 * @param commentedSpectrum
	 *            The {@link CommentedSpectrum}
	 */
	private void addInformationToTextArea(CommentedSpectrum commentedSpectrum) {
		StringBuilder sb = new StringBuilder();
		sb.append("Name: ").append(commentedSpectrum.getTitle()).append('\n');
		sb.append('\n');
		sb.append("Points number: ").append(commentedSpectrum.getSize()).append('\n');
		sb.append('\n');
		sb.append("Xunit: ").append(commentedSpectrum.getxAxisOrigin().toString()).append('\n');
		sb.append("Xmin: ").append(commentedSpectrum.getFrequencySignalMin()).append('\n');
		sb.append("Xmax: ").append(commentedSpectrum.getFrequencySignalMax()).append('\n');
		sb.append('\n');
		sb.append("Yunit: ").append(commentedSpectrum.getyAxis().toString()).append('\n');
		sb.append("Ymin: ").append(commentedSpectrum.getIntensityMin()).append('\n');
		sb.append("Ymax: ").append(commentedSpectrum.getIntensityMax());

		textArea.setText(sb.toString());
	}
}
