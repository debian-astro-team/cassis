/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.medatada;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import eu.omp.irap.cassis.common.CassisMetadata;

/**
 * Create a {@link OriginalMetadataTableModel} for {@link JTable} on {@link OriginalMetadataPanel}
 *
 * @author Lea Foissac
 *
 * @param <T>
 */
public class OriginalMetadataTableModel<T extends CassisMetadata> extends AbstractTableModel {
	private static final long serialVersionUID = 4194182518656758255L;

	public static final String[] columnNames = { "Name", "Value", "Unit", "Comment" };
	private List<CassisMetadata> data;

	/**
	 * Create a {@link JTable} model using a {@link CassisMetadata} {@link List}
	 *
	 * @param data
	 *            The {@link CassisMetadata} {@link List}
	 */
	public OriginalMetadataTableModel(List<CassisMetadata> data) {
		this.data = data;
	}

	/**
	 * Create a {@link JTable} model
	 */
	public OriginalMetadataTableModel() {
		this.data = new ArrayList<CassisMetadata>();
	}

	/**
	 * Add a {@link CassisMetadata} List on the {@link JTable} model
	 *
	 * @param cassisMetadataList
	 *            The {@link CassisMetadata} List to add
	 */
	public void addMetadataList(List<CassisMetadata> cassisMetadataList) {
		try {
			this.data.addAll(cassisMetadataList);
		} catch (NullPointerException e) {
		} finally {
			fireTableDataChanged();
		}
	}

	/**
	 * Clear the {@link JTable} model
	 */
	public void refresh() {
		this.data.clear();
		fireTableDataChanged();
	}

	/**
	 * Getter for the {@link CassisMetadata} {@link List}
	 *
	 * @return The list of {@link CassisMetadata}
	 */
	public List<CassisMetadata> getData() {
		return data;
	}

	@Override
	public String getColumnName(int column) {
		return columnNames[column];
	}

	@Override
	public int getRowCount() {
		return data.size();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public Object getValueAt(int row, int column) {
		if (row >= data.size()) {
			return null;
		}
		CassisMetadata cassisMetadata = data.get(row);
		Object res = null;
		switch (column) {
		case 0:
			res = cassisMetadata.getName();
			break;
		case 1:
			res = cassisMetadata.getValue();
			break;
		case 2:
			res = cassisMetadata.getUnit();
			break;
		case 3:
			res = cassisMetadata.getComment();
			break;
		default:
			break;
		}
		return res;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

}
