/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.medatada;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import eu.omp.irap.cassis.common.CassisMetadata;

/**
 * Create a {@link OriginalMetadataPanel} which allows to display original metadata on a {@link JTable}
 *
 * @author Lea Foissac
 */
public class OriginalMetadataPanel extends JPanel {
	private static final long serialVersionUID = 7688521508530046820L;

	private OriginalMetadataControl control;
	private JTable table;
	private JScrollPane scroll;

	/**
	 * Create a {@link OriginalMetadataPanel} using a {@link OriginalMetadataModel}
	 *
	 * @param model
	 *            The model {@link OriginalMetadataModel}
	 */
	public OriginalMetadataPanel(OriginalMetadataModel model) {
		super(new BorderLayout());
		this.control = new OriginalMetadataControl(this, model);
		this.scroll = new JScrollPane(getJTable());
		this.scroll.getViewport().setBackground(Color.WHITE);
		this.add(this.scroll, BorderLayout.CENTER);
	}

	/**
	 * Create {@link OriginalMetadataPanel} and a new {@link OriginalMetadataModel}
	 */
	public OriginalMetadataPanel() {
		super(new BorderLayout());
		OriginalMetadataModel model = new OriginalMetadataModel();
		this.control = new OriginalMetadataControl(this, model);
		this.scroll = new JScrollPane(getJTable());
		this.scroll.getViewport().setBackground(Color.WHITE);
		this.add(this.scroll, BorderLayout.CENTER);
	}

	/**
	 * Create a {@link JTable} if it not exists
	 *
	 * @return The table {@link JTable}
	 */

	public JTable getJTable() {
		if (table == null) {
			table = new JTable(control.getModel().getTableModel());
			table.getTableHeader().addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					control.mouseClickedOnTableHeader(e);
				}
			});
			for (int i = 0; i < CassisMetadataTableModel.columnNames.length; i++) {
				table.getColumnModel().getColumn(i).setCellRenderer(new CassisToolTipRenderer());
			}
			table.setRowSorter(new BasicTableRowSorter<CassisMetadata>(control.getModel().getTableModel()));
		}
		return table;
	}

	/**
	 * Getter for the scroll
	 *
	 * @return The scroll {@link JScrollPane}
	 */
	public JScrollPane getScroll() {
		return scroll;
	}

	/**
	 * Getter for the controller of {@link OriginalMetadataPanel}
	 *
	 * @return the controller {@link OriginalMetadataControl}
	 */
	public OriginalMetadataControl getControl() {
		return control;
	}

	/**
	 * Getter for the model of {@link OriginalMetadataPanel}
	 *
	 * @return the model {@link OriginalMetadataModel}
	 */
	public OriginalMetadataModel getModel() {
		return control.getModel();
	}
}
