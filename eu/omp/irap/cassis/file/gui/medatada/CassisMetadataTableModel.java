/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.medatada;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.file.gui.ColumnsDetected;

/**
 * Create a {@link CassisMetadataTableModel} for {@link JTable} on {@link CassisMetadataPanel}
 *
 * @author Lea Foissac
 */
public class CassisMetadataTableModel<T extends CassisMetadata> extends AbstractTableModel {
	private static final long serialVersionUID = 4194182518656758255L;

	public static final String[] columnNames = { "Name", "Value", "Unit", "Comment" };
	private List<CassisMetadata> data;
	private ColumnsDetected waveColumnsDetected;
	private ColumnsDetected fluxColumnsDetected;

	boolean isEditable;

	/**
	 * Create a {@link JTable} model using a list of {@link CassisMetadata}, x and y columns of data detected
	 *
	 * @param data
	 *            The {@link CassisMetadata} {@link List}
	 * @param xColumnsDetected
	 *            The x columns of data detected
	 * @param yColumnsDetected
	 *            The y columns of data detected
	 */
	public CassisMetadataTableModel(List<CassisMetadata> data, ColumnsDetected xColumnsDetected, ColumnsDetected yColumnsDetected) {
		this.data = data;
		this.waveColumnsDetected = xColumnsDetected;
		this.fluxColumnsDetected = yColumnsDetected;
		isEditable = true;
	}

	/**
	 * Create a {@link JTable} model
	 */
	public CassisMetadataTableModel() {
		this.data = new ArrayList<CassisMetadata>();
		isEditable = true;
	}

	/**
	 * Add a {@link CassisMetadata} List on the {@link JTable} model
	 *
	 * @param cassisMetadataList
	 *            The {@link CassisMetadata} List to add
	 */
	public void addMetadataList(List<CassisMetadata> cassisMetadataList) {
		try {
			this.data.addAll(cassisMetadataList);
		} catch (NullPointerException e) {
		} finally {
			fireTableDataChanged();
		}
	}

	/**
	 * Remove row from {@link JTable} model
	 *
	 * @param rowIndex
	 *            The row index to delete
	 */
	public void removeRow(int rowIndex) {
		this.data.remove(rowIndex);
		fireTableRowsDeleted(rowIndex, rowIndex);
	}

	/**
	 * Clear the {@link JTable} model
	 */
	public void refresh() {
		this.data.clear();
		fireTableDataChanged();
	}

	/**
	 * Getter for {@link CassisMetadata} {@link List}
	 *
	 * @return The {@link CassisMetadata} {@link List}
	 */
	public List<CassisMetadata> getData() {
		return data;
	}

	/**
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	/**
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount() {
		return data.size();
	}

	/**
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	@Override
	public String getColumnName(int column) {
		return columnNames[column];
	}

	/**
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	@Override
	public Object getValueAt(int row, int column) {
		CassisMetadata cassisMetadata = data.get(row);
		Object res = null;
		switch (column) {
		case 0:
			res = cassisMetadata.getName();
			break;
		case 1:
			if (cassisMetadata.getName().equals(CASSIS_METADATA.WAVE_COLUMNS_DETECTED.getName())){
				if (waveColumnsDetected != null){
					res = waveColumnsDetected.getColumnsDetected();
				}
				else {
					res = cassisMetadata.getValue();
				}
			} else if (cassisMetadata.getName().equals(CASSIS_METADATA.FLUX_COLUMNS_DETECTED.getName())) {
				if (fluxColumnsDetected != null){
					res = fluxColumnsDetected.getColumnsDetected();
				}else {
					res = cassisMetadata.getValue();
				}
			} else {
				res = cassisMetadata.getValue();
			}
			break;
		case 2:
			res = cassisMetadata.getUnit();
			break;
		case 3:
			res = cassisMetadata.getComment();
			break;
		default:
			break;
		}
		return res;
	}

	/**
	 * @see javax.swing.table.AbstractTableModel#setValueAt(java.lang.Object, int, int)
	 */
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		if (aValue != null) {
			switch (columnIndex) {
			case 0:
				data.get(rowIndex).setName(aValue.toString());
				break;
			case 1:
				data.get(rowIndex).setValue(aValue.toString());
				break;
			case 2:
				data.get(rowIndex).setUnit(aValue.toString());
				break;
			case 3:
				data.get(rowIndex).setComment(aValue.toString());
				break;
			default:
				break;
			}
			fireTableCellUpdated(rowIndex, columnIndex);
		}
	}

	/**
	 * @see javax.swing.table.AbstractTableModel#isCellEditable(int, int)
	 */
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		boolean isEditable = this.isEditable;
		if (columnIndex == 0) {
			isEditable = false;
		}
		return isEditable;
	}

	/**
	 * Setter to change edition of table
	 *
	 * @param isEditable
	 *            True if the table is editable and false if not
	 */
	public void setCellEditable(boolean isEditable) {
		this.isEditable = isEditable;
	}

	/**
	 * Getter for wave columns detected
	 *
	 * @return The wave columns detected
	 */
	public ColumnsDetected getWaveColumnsDetected() {
		return waveColumnsDetected;
	}

	/**
	 * Getter for flux columns detected
	 *
	 * @return The flux columns detected
	 */
	public ColumnsDetected getFluxColumnsDetected() {
		return fluxColumnsDetected;
	}

	/**
	 * Setter to change the wave columns detected
	 *
	 * @param waveColumnsDetected
	 *            The new wave columns detected
	 */
	public void setWaveColumnDetected(ColumnsDetected waveColumnsDetected) {
		this.waveColumnsDetected = waveColumnsDetected;
	}

	/**
	 * Setter to change the flux columns detected
	 *
	 * @param fluxColumnsDetected
	 *            The new flux columns detected
	 */
	public void setFluxColumnDetected(ColumnsDetected fluxColumnsDetected) {
		this.fluxColumnsDetected = fluxColumnsDetected;
	}

	/**
	 * Return the row for the given metadata name.
	 *
	 * @param metadataName The name of the metadata.
	 * @return the row or -1 if not found.
	 */
	private int getRow(String metadataName) {
		for (int row = 0; row < data.size(); row++) {
			if (data.get(row).getName().equals(metadataName)) {
				return row;
			}
		}
		return -1;
	}

	/**
	 * Change the value for the given metadata.
	 *
	 * @param metadataName The name of the metadata.
	 * @param value The value to set.
	 */
	public void setValue(String metadataName, String value) {
		int row = getRow(metadataName);
		if (row >= 0) {
			setValueAt(value, row, 3);
		}
	}
}
