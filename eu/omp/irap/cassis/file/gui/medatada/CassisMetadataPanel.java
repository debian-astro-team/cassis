/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.medatada;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import eu.omp.irap.cassis.common.CassisMetadata;

/**
 * Create a {@link CassisMetadataPanel} which allow to display metadata on a {@link JTable}
 *
 * @author Lea Foissac
 *
 */
public class CassisMetadataPanel extends JPanel {
	private static final long serialVersionUID = 5858057854199995490L;

	private CassisMetadataControl control;
	private JTable table;
	private JScrollPane scroll;
	private JPanel buttonPanel;
	private JButton addButton;
	private JButton spectrumInfoButton;
	private JButton deleteButton;
	private JButton saveButton;

	/**
	 * Create a {@link CassisMetadataPanel} using a {@link CassisMetadataModel}
	 *
	 * @param model
	 *            The model {@link CassisMetadataModel}
	 */
	public CassisMetadataPanel(CassisMetadataModel model) {
		super(new BorderLayout());
		this.control = new CassisMetadataControl(this, model);
		this.scroll = new JScrollPane(getJTable());
		this.scroll.getViewport().setBackground(Color.WHITE);
		add(this.scroll, BorderLayout.CENTER);
		add(getButtonPanel(), BorderLayout.SOUTH);
	}

	/**
	 * Create a {@link CassisMetadataPanel}
	 */
	public CassisMetadataPanel() {
		super(new BorderLayout());
		CassisMetadataModel model = new CassisMetadataModel();
		this.control = new CassisMetadataControl(this, model);
		this.scroll = new JScrollPane(getJTable());
		this.scroll.getViewport().setBackground(Color.WHITE);
		add(this.scroll, BorderLayout.CENTER);
		add(getButtonPanel(), BorderLayout.SOUTH);
	}

	/**
	 * Create a {@link JTable} if not exists
	 *
	 * @return The table {@link JTable}
	 */
	public JTable getJTable() {
		if (table == null) {
			table = new JTable(control.getModel().getTableModel());
			table.getTableHeader().addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					control.mouseClickedOnTableHeader(e);
				}
			});
			for (int i = 0; i < CassisMetadataTableModel.columnNames.length; i++) {
				table.getColumnModel().getColumn(i).setCellRenderer(new CassisMetadataRenderer());
			}
			CassisCellEditor cellEditor = new CassisCellEditor(control.getModel());
			table.getColumnModel().getColumn(1).setCellEditor(cellEditor);
			table.getColumnModel().getColumn(2).setCellEditor(cellEditor);
			table.setRowSorter(new BasicTableRowSorter<CassisMetadata>(control.getModel().getTableModel()));
			table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
				@Override
				public void valueChanged(ListSelectionEvent e) {
					control.rowSelected();
				}
			});
			table.getColumnModel().getColumn(3).setCellEditor(new UnitCellEditor(control.getModel()));
		}
		return table;
	}

	/**
	 * Getter for the scroll
	 *
	 * @return The scroll {@link JScrollPane}
	 */
	public JScrollPane getScroll() {
		return scroll;
	}

	/**
	 * Create a {@link JPanel} if not exists which contains all {@link JButton}
	 *
	 * @return {@link JPanel} containing all {@link JButton}
	 */
	public JPanel getButtonPanel() {
		if (buttonPanel == null) {
			buttonPanel = new JPanel(new GridBagLayout());
			GridBagConstraints gridConstraints = new GridBagConstraints();
			gridConstraints.fill = GridBagConstraints.HORIZONTAL;
			// Add Button
			gridConstraints.weightx = 0.5;
			gridConstraints.gridy = 0;
			gridConstraints.gridx = 0;
			gridConstraints.insets = new Insets(10, 0, 2, 2);
			buttonPanel.add(getAddButton(), gridConstraints);
			// Delete Button
			gridConstraints.gridx = 1;
			gridConstraints.insets = new Insets(10, 0, 2, 0);
			buttonPanel.add(getDeleteButton(), gridConstraints);
			// SpectrumInfo button
			gridConstraints.weightx = 0;
			gridConstraints.gridwidth = 2;
			gridConstraints.gridx = 0;
			gridConstraints.gridy = 2;
			gridConstraints.insets = new Insets(0, 0, 2, 0);
			buttonPanel.add(getSpectrumInfoButton(), gridConstraints);
			// SaveValue button
			gridConstraints.insets = new Insets(15, 0, 0, 0);
			gridConstraints.gridy = 3;
			buttonPanel.add(getSaveButton(), gridConstraints);
		}
		return buttonPanel;
	}

	/**
	 * Create if not exists a "Add metadata" {@link JButton}
	 *
	 * @return The "Add metadata" {@link JButton}
	 */
	public JButton getAddButton() {
		if (addButton == null) {
			addButton = new JButton("Add metadata");
			addButton.setEnabled(false);
			addButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.openAddMetadataFrame();
				}
			});
		}
		return addButton;
	}

	/**
	 * Create if not exists a "Delete metadata" {@link JButton}
	 *
	 * @return The "Delete metadata" {@link JButton}
	 */
	public JButton getDeleteButton() {
		if (deleteButton == null) {
			deleteButton = new JButton("Delete metadata");
			deleteButton.setEnabled(false);
			deleteButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.deleteRowToJTable(table);
				}
			});
		}
		return deleteButton;
	}

	/**
	 * Create if not exists a SpectrumInfo {@link JButton}
	 *
	 * @return The SpectrumInfo{@link JButton}
	 */
	public JButton getSpectrumInfoButton() {
		if (spectrumInfoButton == null) {
			spectrumInfoButton = new JButton("Spectrum info");
			spectrumInfoButton.setEnabled(false);
			spectrumInfoButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.openSpectrumInfoFrame();
				}
			});
		}
		return spectrumInfoButton;
	}

	/**
	 * Create if not exists a Save {@link JButton}
	 *
	 * @return The save {@link JButton}
	 */
	public JButton getSaveButton() {
		if (saveButton == null) {
			saveButton = new JButton("Save spectrum");
			saveButton.setEnabled(false);
			saveButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {

				}
			});
		}
		return saveButton;
	}

	/**
	 * Open {@link JOptionPane}
	 */
	public void openNoRowSelectedJOptionPane() {
		JOptionPane.showMessageDialog(this, "<html>You haven't selected row to delete.</html>", "No row selected", JOptionPane.WARNING_MESSAGE);
	}

	/**
	 * Open {@link JOptionPane}
	 */
	public void openNoRowExistsJOptionPane() {
		JOptionPane.showMessageDialog(this, "<html>You can't delete row because there is no row in table.</html>", "No row in table", JOptionPane.WARNING_MESSAGE);
	}

	/**
	 * Getter for the controller {@link CassisMetadataControl}
	 *
	 * @return the controller {@link CassisMetadataControl}
	 */
	public CassisMetadataControl getControl() {
		return control;
	}

	/**
	 * Getter for the model {@link CassisMetadataModel}
	 *
	 * @return the model {@link CassisMetadataModel}
	 */
	public CassisMetadataModel getModel() {
		return control.getModel();
	}
}
