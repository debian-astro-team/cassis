/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.medatada;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.axes.UNIT;

/**
 * Create a {@link JFrame} allowing to choose name, value, comment and unit for a {@link CassisMetadata}
 *
 * @author Lea Foissac
 *
 */
public class AddMetadataFrame extends JFrame {
	private static final long serialVersionUID = -6104515251519568365L;

	private JTextField nameTextField;
	private JTextField valueTextField;
	private JTextField commentTextField;
	private JComboBox<String> unitComboBox;
	private JPanel addMetadataPanel;
	private JPanel textFieldsPanel;
	private JPanel buttonsPanel;
	private JButton okButton;
	private JButton cancelButton;

	/**
	 * Create a {@link AddMetadataFrame}
	 */
	public AddMetadataFrame() {
		super("Add metadata");
		initialize();
	}

	/**
	 * Initialize {@link JFrame}
	 */
	private void initialize() {
		this.addMetadataPanel = new JPanel(new BorderLayout());
		this.addMetadataPanel.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
		this.addMetadataPanel.add(getTextFieldsPanel(), BorderLayout.CENTER);
		this.addMetadataPanel.add(getButtonsPanel(), BorderLayout.SOUTH);
		setContentPane(this.addMetadataPanel);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setMinimumSize(new Dimension(400, 200));
		pack();
		setVisible(true);
	}

	/**
	 * Create if not exists {@link JPanel} containing all {@link JTextField} with {@link JLabel}
	 *
	 * @return The {@link JPanel} containing all {@link JTextField} with {@link JLabel}
	 */
	public JPanel getTextFieldsPanel() {
		if (textFieldsPanel == null) {
			textFieldsPanel = new JPanel(new GridLayout(4, 2));
			textFieldsPanel.add(new JLabel("Name"));
			textFieldsPanel.add(getNameTextField());
			textFieldsPanel.add(new JLabel("Value"));
			textFieldsPanel.add(getValueTextField());
			textFieldsPanel.add(new JLabel("Comment"));
			textFieldsPanel.add(getCommentTextField());
			textFieldsPanel.add(new JLabel("Unit"));
			textFieldsPanel.add(getUnitComboBox());
			textFieldsPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 15, 0));
		}
		return textFieldsPanel;
	}

	/**
	 * Create if not exists a {@link JTextField} for metadata name
	 *
	 * @return The {@link JTextField} for metadata name
	 */
	public JTextField getNameTextField() {
		if (nameTextField == null) {
			nameTextField = new JTextField();
		}
		return nameTextField;
	}

	/**
	 * Create if not exists a {@link JTextField} for metadata value
	 *
	 * @return The {@link JTextField} for metadata value
	 */
	public JTextField getValueTextField() {
		if (valueTextField == null) {
			valueTextField = new JTextField();
		}
		return valueTextField;
	}

	/**
	 * Create if not exists a {@link JTextField} for metadata comment
	 *
	 * @return The {@link JTextField} for metadata comment
	 */
	public JTextField getCommentTextField() {
		if (commentTextField == null) {
			commentTextField = new JTextField();
		}
		return commentTextField;
	}

	/**
	 * Create if not exists a {@link JComboBox} for metadata unit
	 *
	 * @return The {@link JComboBox} for metadata unit
	 */
	public JComboBox<String> getUnitComboBox() {
		if (unitComboBox == null) {
			unitComboBox = new JComboBox<String>();
			for (UNIT unit : UNIT.values()) {
				unitComboBox.addItem(unit.toString());
			}
			unitComboBox.setEditable(true);
		}
		return unitComboBox;
	}

	/**
	 * Create if not exists a {@link JPanel} containing all {@link JButton}
	 *
	 * @return The {@link JPanel} containing all {@link JButton}
	 */
	public JPanel getButtonsPanel() {
		if (buttonsPanel == null) {
			buttonsPanel = new JPanel(new FlowLayout());
			buttonsPanel.add(getOkButton());
			buttonsPanel.add(getCancelButton());
		}
		return buttonsPanel;
	}

	/**
	 * Create if not exists an "Ok" {@link JButton}
	 *
	 * @return The "Ok" {@link JButton}
	 */
	public JButton getOkButton() {
		if (okButton == null) {
			okButton = new JButton("Ok");
			add(okButton);
		}
		return okButton;
	}

	/**
	 * Create if not exists a "Cancel" {@link JButton}
	 *
	 * @return The "Cancel" {@link JButton}
	 */
	private JButton getCancelButton() {
		if (cancelButton == null) {
			cancelButton = new JButton("Cancel");
			cancelButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					dispose();
				}
			});
		}
		return cancelButton;
	}

	/**
	 * Open a {@link JOptionPane} when a {@link JTextField} is empty
	 *
	 * @param attribut
	 *            Required attribute
	 */
	public void openWarningJOptionPane(String attribut) {
		JOptionPane.showMessageDialog(this, "Please enter a metadata " + attribut, "Missing metadata " + attribut, JOptionPane.WARNING_MESSAGE);
	}

	/**
	 * Open a {@link JOptionPane} when a metadata name given by user already exists on the {@link CassisMetadata} {@link List}
	 *
	 * @param name
	 *            Metadata name given by user
	 */
	public void openMetadataNameAlreadyExistsJOptionPane(String name) {
		JOptionPane.showMessageDialog(this, "There is already a metadata with the name : " + name, "Metadata name already exists", JOptionPane.WARNING_MESSAGE);
	}

}
