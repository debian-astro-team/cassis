/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.medatada;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseEvent;
import java.util.EventObject;
import java.util.List;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextField;

import eu.omp.irap.cassis.common.CassisMetadata;

/**
 * Create a custom CellEditor to change columns of data detected
 *
 * @author Lea Foissac
 *
 */
public class CassisCellEditor extends DefaultCellEditor {

	private static final long serialVersionUID = -8711073028981134004L;

	private CassisMetadataTableModel<CassisMetadata> tableModel;
	private CassisMetadataModel model;

	/**
	 * Create a {@link CassisCellEditor} according to the model of {@link CassisMetadataPanel}
	 *
	 * @param model
	 */
	public CassisCellEditor(CassisMetadataModel model) {
		super(new JTextField());
		this.model = model;
		this.tableModel = model.getTableModel();
		this.clickCountToStart = 1;
	}

	@SuppressWarnings("serial")
	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		List<CassisMetadata> datas = tableModel.getData();
		final CassisMetadata cassisMetadata = datas.get(table.convertRowIndexToModel(row));

		if (isWaveOrFluxColumn(cassisMetadata.getName()) && column == 1) {
			return manageWaveOfFluxColumn(value, cassisMetadata);

		} else if (CASSIS_METADATA.FLUX.getName().equals(cassisMetadata.getName())
				&& (column == 1 ||column == 2)) {
			return manageFluxMetadata(value, column, cassisMetadata);
		} else {
			final JTextField textField = new JTextField();
			if (value != null) {
				textField.setText(value.toString());
			}

			editorComponent = textField;
			delegate = new EditorDelegate() {
				@Override
				public void setValue(Object value) {
					textField.setText((value != null) ? value.toString() : "");
				}

				@Override
				public Object getCellEditorValue() {
					return textField.getText();
				}
			};
			textField.addActionListener(delegate);
		}
		return super.getTableCellEditorComponent(table, value, isSelected, row, column);
	}

	@SuppressWarnings("serial")
	private Component manageFluxMetadata(Object value, int column, final CassisMetadata cassisMetadata) {
		final JTextField textField = new JTextField();
		if (value != null) {
			textField.setText(value.toString());
		}

		delegate = new EditorDelegate() {
			@Override
			public void setValue(Object value) {
				textField.setText((value != null) ? value.toString() : "");
			}

			@Override
			public Object getCellEditorValue() {
				return textField.getText();
			}

			@Override
			public void actionPerformed(ActionEvent e) {
				CassisCellEditor.this.stopCellEditing();
				String value = textField.getText();

				if (column == 1 && cassisMetadata.getName().equals(CASSIS_METADATA.FLUX.getName())) {
					model.notifyFluxAxisNameChange(value);
					cassisMetadata.setValue(value);
				} else if (column == 2 && cassisMetadata.getName().equals(CASSIS_METADATA.FLUX.getName())) {
					model.notifyFluxUnitChange(value);
					cassisMetadata.setUnit(value);
				}
			}
		};
		textField.addActionListener(delegate);
		return textField;
	}

	@SuppressWarnings("serial")
	private Component manageWaveOfFluxColumn(Object value, final CassisMetadata cassisMetadata) {
		String[] columnTab = (String[]) value;
		final JComboBox<String> comboBox = new JComboBox<String>(columnTab);

		editorComponent = comboBox;
		this.clickCountToStart = 1;
		comboBox.putClientProperty("JComboBox.isTableCellEditor", Boolean.TRUE);
		delegate = new EditorDelegate() {
			@Override
			public void setValue(Object value) {
				comboBox.setSelectedItem(value);
			}

			@Override
			public Object getCellEditorValue() {
				return comboBox.getSelectedItem();
			}

			@Override
			public boolean shouldSelectCell(EventObject anEvent) {
				if (anEvent instanceof MouseEvent) {
					MouseEvent e = (MouseEvent) anEvent;
					return e.getID() != MouseEvent.MOUSE_DRAGGED;
				}
				return true;
			}

			@Override
			public boolean stopCellEditing() {
				if (comboBox.isEditable()) {
					// Commit edited value.
					comboBox.actionPerformed(new ActionEvent(CassisCellEditor.this, 0, ""));
				}
				return super.stopCellEditing();
			}

			@Override
			public void actionPerformed(ActionEvent e) {
				CassisCellEditor.this.stopCellEditing();
				String item = comboBox.getSelectedItem().toString();
				cassisMetadata.setValue(item);
				if (cassisMetadata.getName().equals(CASSIS_METADATA.WAVE_COLUMNS_DETECTED.getName())) {
					model.notifyWaveColumnChange(item,
							tableModel.getWaveColumnsDetected().getColumn(item)
									.getXAxisCassis().toString());
					tableModel.getWaveColumnsDetected().setColumnUsed(item);
				} else if (cassisMetadata.getName().equals(CASSIS_METADATA.FLUX_COLUMNS_DETECTED.getName())) {
					model.notifyFluxColumnChange(item,
							tableModel.getFluxColumnsDetected().getColumn(item)
									.getYAxisCassis().getUnitString());
					tableModel.getFluxColumnsDetected().setColumnUsed(item);
				} else if (cassisMetadata.getName().equals(CASSIS_METADATA.FLUX.getName())) {
					model.notifyFluxAxisNameChange(item);
				}
			}
		};
		comboBox.addActionListener(delegate);

		comboBox.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				CassisCellEditor.this.stopCellEditing();
			}
		});

		return comboBox;
	}

	private boolean isWaveOrFluxColumn(String metadataName) {
		return CASSIS_METADATA.WAVE_COLUMNS_DETECTED.getName().equals(metadataName) ||
				CASSIS_METADATA.FLUX_COLUMNS_DETECTED.getName().equals(metadataName);
	}
}
