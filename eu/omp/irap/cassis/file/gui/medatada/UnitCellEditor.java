/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.medatada;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextField;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.axes.UNIT;

/**
 * Simple CellEditor for unit column.
 *
 * @author M. Boiziot
 */
public class UnitCellEditor extends DefaultCellEditor {

	private static final long serialVersionUID = 2518393076564603313L;
	private CassisMetadataModel model;


	/**
	 * Constructor.
	 *
	 * @param model The model.
	 */
	public UnitCellEditor(CassisMetadataModel model) {
		super(new JTextField());
		this.model = model;
	}

	/**
	 * Create a JComboBox component for units and add listener to it.
	 *
	 * @param table The JTable.
	 * @param value The value.
	 * @param isSelected true if the cell is to be rendered with highlighting.
	 * @param row the The row of the cell being edited
	 * @param column The column of the cell being edited
	 * @return The component for editing
	 * @see javax.swing.DefaultCellEditor#getTableCellEditorComponent(javax.swing.JTable, java.lang.Object, boolean, int, int)
	 */
	@SuppressWarnings("serial")
	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		List<CassisMetadata> datas = model.getTableModel().getData();
		final CassisMetadata cassisMetadata = datas.get(table.convertRowIndexToModel(row));
		final JComboBox<String> unitComboBox = new JComboBox<String>();
		if (CASSIS_METADATA.WAVE.getName().equals(cassisMetadata.getName())) {
			for (UNIT unit : UNIT.values()) {
				if (UNIT.isWaveUnit(unit) && !UNIT.isVelocity(unit)) {
					unitComboBox.addItem(unit.toString());
				}
			}
		} else {
			for(UNIT unit : UNIT.values()) {
				unitComboBox.addItem(unit.toString());
			}
			unitComboBox.setEditable(true);
		}
		unitComboBox.setSelectedItem(cassisMetadata.getUnit());

		delegate = new EditorDelegate() {
			@Override
			public void setValue(Object value) {
				unitComboBox.setSelectedItem(value);
			}

			@Override
			public Object getCellEditorValue() {
				return unitComboBox.getSelectedItem();
			}

			@Override
			public void actionPerformed(ActionEvent e) {
				stopCellEditing();
				String newUnit = (String) unitComboBox.getSelectedItem();
				if (CASSIS_METADATA.WAVE.getName().equals(cassisMetadata.getName())) {
					model.notifyWaveUnitChange(newUnit);
				} else if (CASSIS_METADATA.FLUX.getName().equals(cassisMetadata.getName())) {
					model.notifyFluxUnitChange(newUnit);
				}
			}
		};
		unitComboBox.addActionListener(delegate);

		return unitComboBox;
	}

}
