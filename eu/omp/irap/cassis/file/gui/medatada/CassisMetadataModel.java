/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.medatada;

import javax.swing.JTable;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.events.ListenerManager;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.file.gui.FileControl;
import eu.omp.irap.cassis.file.gui.fileinfo.FileInfoFrame;

/**
 * Create a {@link CassisMetadataModel}
 *
 * @author Lea Foissac
 *
 */
public class CassisMetadataModel extends ListenerManager {

	public static final String DELETE_ROW = "deleteRow";
	public static final String ADD_METADATA = "addMetadata";
	public static final String OPEN_SPECTRUM_INFO = "openSpectrumInfo";
	public static final String WAVE_COLUMN_CHANGE = "waveColumnChange";
	public static final String FLUX_COLUMN_CHANGE = "fluxColumnChange";
	public static final String WAVE_UNIT_CHANGE = "waveUnitChange";
	public static final String FLUX_UNIT_CHANGE = "fluxUnitChange";
	public static final String FLUX_AXIS_NAME_CHANGE =" fluxAxisNameChange";

	private CassisMetadataTableModel<CassisMetadata> tableModel;


	/**
	 * Create a {@link CassisMetadataModel} and a new {@link CassisMetadataTableModel}
	 */
	public CassisMetadataModel() {
		tableModel = new CassisMetadataTableModel<CassisMetadata>();
	}

	/**
	 * Getter for the table model {@link CassisMetadataTableModel}
	 *
	 * @return the table model {@link CassisMetadataTableModel}
	 */
	public CassisMetadataTableModel<CassisMetadata> getTableModel() {
		return tableModel;
	}

	/**
	 * Notify to top controller {@link FileControl} that a row was deleted in {@link JTable}
	 *
	 * @param selectedRow
	 *            The index of delete row
	 */
	public void notifyRemoveRow(int selectedRow) {
		fireDataChanged(new ModelChangedEvent(DELETE_ROW, selectedRow));
	}

	/**
	 * Notify to top controller {@link FileControl} that a metadata was added in {@link JTable}
	 */
	public void notifyAddMetadataFrame() {
		fireDataChanged(new ModelChangedEvent(ADD_METADATA));
	}

	/**
	 * Notify to top controller {@link FileControl} that the user want to open a {@link FileInfoFrame}
	 */
	public void notifyOpenSpectrumInfoFrame() {
		fireDataChanged(new ModelChangedEvent(OPEN_SPECTRUM_INFO));
	}

	/**
	 * Notify to top controller {@link FileControl} that the user selected a new
	 *  wave column of data on {@link JTable}.
	 *
	 * @param column The name of column selected.
	 * @param unit The unit of the selected column.
	 */
	public void notifyWaveColumnChange(String column, String unit) {
		fireDataChanged(new ModelChangedEvent(WAVE_COLUMN_CHANGE, column));
		tableModel.setValue(CASSIS_METADATA.WAVE.getName(), unit);
	}

	/**
	 * Notify to top controller {@link FileControl} that the user selected a new
	 *  flux column of data on {@link JTable}.
	 *
	 * @param column The name of column selected.
	 * @param unit The unit of the selected column.
	 */
	public void notifyFluxColumnChange(String column, String unit) {
		fireDataChanged(new ModelChangedEvent(FLUX_COLUMN_CHANGE, column));
		tableModel.setValue(CASSIS_METADATA.FLUX.getName(), unit);
	}

	/**
	 * Create and fire an event to notify a change of flux unit.
	 *
	 * @param newUnit The new unit to set.
	 */
	public void notifyFluxUnitChange(String newUnit) {
		fireDataChanged(new ModelChangedEvent(FLUX_UNIT_CHANGE, newUnit));
	}

	/**
	 * Create and fire an event to notify a change of wave unit.
	 *
	 * @param newUnit The new unit to set.
	 */
	public void notifyWaveUnitChange(String newUnit) {
		fireDataChanged(new ModelChangedEvent(WAVE_UNIT_CHANGE, newUnit));
	}

	/**
	 * Create and fire an event to notify a change of flux name.
	 *
	 * @param newName The new name to set.
	 */
	public void notifyFluxAxisNameChange(String newName) {
		fireDataChanged(new ModelChangedEvent(FLUX_AXIS_NAME_CHANGE, newName));
	}
}
