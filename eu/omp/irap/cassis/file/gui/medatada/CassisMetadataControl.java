/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.medatada;

import java.awt.BorderLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import eu.omp.irap.cassis.common.CassisMetadata;

/**
 * Create a controller {@link CassisMetadataControl} to interact with the panel {@link CassisMetadataPanel} and the model {@link CassisMetadataModel}
 *
 * @author Lea Foissac
 *
 */
public class CassisMetadataControl {

	private CassisMetadataPanel panel;
	private CassisMetadataModel model;

	/**
	 * Create a {@link CassisMetadataControl}
	 *
	 * @param panel
	 *            The panel {@link CassisMetadataPanel}
	 * @param model
	 *            The model {@link CassisMetadataModel}
	 */
	public CassisMetadataControl(CassisMetadataPanel panel, CassisMetadataModel model) {
		this.panel = panel;
		this.model = model;
	}

	/**
	 * Getter for the model {@link CassisMetadataModel}
	 *
	 * @return The model {@link CassisMetadataModel}
	 */
	public CassisMetadataModel getModel() {
		return model;
	}

	/**
	 * Delete row from {@link JTable}
	 *
	 * @param table {@link JTable}
	 */
	public void deleteRowToJTable(JTable table) {
		int row = table.getSelectedRow();
		if (row == -1) { // No row selected
			if (model.getTableModel().getRowCount() > 0) {
				panel.openNoRowSelectedJOptionPane();
			} else {
				panel.openNoRowExistsJOptionPane();
			}
		} else {
			model.getTableModel().removeRow(row);
			model.notifyRemoveRow(row);
		}
	}

	/**
	 * Open a addMetadataFrame
	 */
	public void openAddMetadataFrame() {
		model.notifyAddMetadataFrame();
	}

	/**
	 * Traitment realized when click on TableHeader was made.
	 *
	 * @param e
	 *            The mouse event
	 */
	public void mouseClickedOnTableHeader(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON3) {
			if (model.getTableModel().getRowCount() == 0) {
				return;
			}
			final int viewIndexColumn = panel.getJTable().getColumnModel().getColumnIndexAtX(e.getPoint().x);
			final int modelIndexColumn = panel.getJTable().convertColumnIndexToModel(viewIndexColumn);
			final JFrame frame = new JFrame("Search column (" + panel.getJTable().getColumnName(viewIndexColumn) + ")");
			final JPanel panel = new JPanel(new BorderLayout());
			final JTextField textField = new JTextField(5);
			textField.setUI(new HintTextFieldUI("Type your search here. Hit enter to validate a result."));
			panel.add(textField, BorderLayout.NORTH);

			final JList<String> list = new JList<String>();
			panel.add(new JScrollPane(list), BorderLayout.CENTER);
			textField.requestFocus();

			textField.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						selectLine(modelIndexColumn, frame, panel, list.getSelectedValue());
					} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
						list.requestFocus();
						list.setSelectedIndex(1);
					} else {
						List<CassisMetadata> data = model.getTableModel().getData();
						ArrayList<String> listData = new ArrayList<String>();
						for (int i = 0; i < data.size(); i++) {
							String name;
							switch (modelIndexColumn) {
							case 0:
								name = data.get(i).getName();
								break;
							case 1:
								name = data.get(i).getValue();
								break;
							case 2:
								name = data.get(i).getComment();
								break;
							case 3:
								name = data.get(i).getUnit();
								break;
							default:
								name = data.get(i).getName();
								break;
							}
							try {
								if (name.toLowerCase().contains(textField.getText().toLowerCase())) {
									listData.add(name);
								}
							} catch (NullPointerException e2) {
								e2.printStackTrace();
							}
						}
						list.setListData(listData.toArray(new String[listData.size()]));
						list.setSelectedIndex(0);
						list.validate();
					}
				}
			});
			list.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						selectLine(modelIndexColumn, frame, panel, list.getSelectedValue());
					} else if (e.getKeyCode() == KeyEvent.VK_UP) {
						if (list.getSelectedIndex() == 0)
							textField.requestFocus();
					}
				}
			});
			list.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					super.mouseClicked(e);
					selectLine(modelIndexColumn, frame, panel, list.getSelectedValue());
				}
			});
			frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			frame.setContentPane(panel);
			frame.setSize(360, 200);
			frame.setVisible(true);

			model.getTableModel().fireTableDataChanged();
		}
	}

	private void selectLine(final int modelSelectedColumn, final JFrame frame, final JPanel panel, String text) {
		this.panel.getJTable().clearSelection();
		int i = 0;
		boolean find = false;
		int nbLigne = model.getTableModel().getRowCount();
		for (i = 0; i < nbLigne; i++)
			if (String.valueOf((model.getTableModel().getValueAt(i, modelSelectedColumn))).equals(text)) {
				find = true;
				break;
			}
		if (find) {
			this.panel.getJTable().setRowSelectionInterval(i, i);
			if (this.panel.getScroll() != null) {
				int value = (this.panel.getScroll().getVerticalScrollBar().getMaximum() - this.panel.getScroll().getVerticalScrollBar().getMinimum()) / nbLigne;
				this.panel.getScroll().getVerticalScrollBar().setValue(value * (i - 4));
			}
		}
		panel.setVisible(false);
		this.panel.getJTable().requestFocus();
		frame.dispose();
	}

	/**
	 * Open a spectrumInfo frame
	 */
	public void openSpectrumInfoFrame() {
		model.notifyOpenSpectrumInfoFrame();
	}

	/**
	 * Action to do once row is selected on the CassisMetadata table
	 */
	public void rowSelected() {
		int row =  panel.getJTable().getSelectedRow();
		if (row != -1) {
			List<CassisMetadata> data = model.getTableModel().getData();
			CassisMetadata cassisMetadata = data.get(row);
			String name = cassisMetadata.getName();
			panel.getDeleteButton().setEnabled(true);
			for (CASSIS_METADATA cassisMetadataEnum : CASSIS_METADATA.values()) {
				if (cassisMetadataEnum.getName().equals(name)) {
					panel.getDeleteButton().setEnabled(false);
					break;
				}
			}
		}
	}
}
