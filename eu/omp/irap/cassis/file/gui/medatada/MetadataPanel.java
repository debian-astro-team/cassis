/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.medatada;

import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

/**
 * Create a {@link MetadataPanel} containing {@link CassisMetadataPanel}, {@link OriginalMetadataPanel} and {@link JPanel}
 *
 * @author Lea Foissac
 *
 */
public class MetadataPanel extends JPanel {
	private static final long serialVersionUID = -4766190020663933663L;

	private CassisMetadataPanel cassisMetadataPanel;
	private OriginalMetadataPanel originalMetadataPanel;
	private JPanel datalinkPanel;
	private JTabbedPane tabbedPane;

	/**
	 * Create a {@link MetadataPanel}
	 */
	public MetadataPanel() {
		super(new BorderLayout());
		this.add(getTabbedPane());
	}

	/**
	 * Create if not exists a {@link JTabbedPane}
	 *
	 * @return The {@link JTabbedPane} containing the 3 tabs : "Cassis Metadata" tab, "Original metadata" tab and "Datalink" tab
	 */
	public JTabbedPane getTabbedPane() {
		if (tabbedPane == null) {
			tabbedPane = new JTabbedPane();
			tabbedPane.add("Cassis Metadata", getCassisMetadataPanel());
			tabbedPane.add("Original Metadata", getOriginalMetadataPanel());
			tabbedPane.add("Datalink", getDatalinkPanel());
			tabbedPane.setEnabledAt(2, false);
			tabbedPane.setSelectedComponent(getCassisMetadataPanel());
		}
		return tabbedPane;
	}

	/**
	 * Create if not exists a {@link CassisMetadataPanel}
	 *
	 * @return The panel {@link CassisMetadataPanel} which allow to display metadata
	 */
	public CassisMetadataPanel getCassisMetadataPanel() {
		if (cassisMetadataPanel == null) {
			cassisMetadataPanel = new CassisMetadataPanel();
			cassisMetadataPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		}
		return cassisMetadataPanel;
	}

	/**
	 * Create if not exists a {@link OriginalMetadataPanel}
	 *
	 * @return The panel {@link OriginalMetadataPanel} which allows to display original metadata
	 */
	public OriginalMetadataPanel getOriginalMetadataPanel() {
		if (originalMetadataPanel == null) {
			originalMetadataPanel = new OriginalMetadataPanel();
			originalMetadataPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		}
		return originalMetadataPanel;
	}

	/**
	 * Create if not exists a {@link JPanel}. It will display datalink information
	 *
	 * @return The panel which allows to display datalink information
	 */
	public JPanel getDatalinkPanel() {
		if (datalinkPanel == null) {
			datalinkPanel = new JPanel(new BorderLayout());
		}
		return datalinkPanel;
	}

	/**
	 * Getter for {@link CassisMetadataControl}
	 *
	 * @return The controller of {@link CassisMetadataPanel}
	 */
	public CassisMetadataControl getCassisMetadataControl() {
		return getCassisMetadataPanel().getControl();
	}

	/**
	 * Getter for {@link OriginalMetadataControl}
	 *
	 * @return The controller of {@link OriginalMetadataPanel}
	 */
	public OriginalMetadataControl getOriginalMetadataControl() {
		return getOriginalMetadataPanel().getControl();
	}

}
