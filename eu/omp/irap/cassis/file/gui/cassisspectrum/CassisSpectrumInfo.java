/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.cassisspectrum;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.file.FileManager;
import eu.omp.irap.cassis.file.FileManagerVOTable;
import eu.omp.irap.cassis.file.gui.ColumnsDetected;
import eu.omp.irap.cassis.file.gui.datalink.DatalinkModel;

/**
 * Create CassisSpectrumInfo to obtain path, name, cassisSpectrum list and metadata common list according to spectrum file.
 *
 * @author Lea Foissac
 */
public class CassisSpectrumInfo {

	private String path;
	private String name;
	private List<CassisMetadata> cassisMetadataList;
	private List<CassisSpectrum> cassisSpectrumList;
	private List<ColumnsDetected> xColumnsDetected;
	private List<ColumnsDetected> yColumnsDetected;
	private DatalinkModel[] datalinkModelTab;
	private boolean metadataMerge;

	/**
	 * Create a {@link CassisSpectrumInfo}
	 */
	public CassisSpectrumInfo() {
		this.path = "";
		this.name = "";
		this.cassisMetadataList = new ArrayList<CassisMetadata>();
		this.cassisSpectrumList = new ArrayList<CassisSpectrum>();
		this.metadataMerge = false;
	}

	/**
	 * Constructor which create CassisSpectrumInfo using CassisSpectrum, path file and name file, and CassisMetadata list.
	 *
	 * @param cassisSpectrum
	 *            The CassisSpectrum corresponding with path file, file name
	 * @param path
	 *            The path file
	 * @param name
	 *            The name file
	 * @param cassisMetadataList
	 *            The cassisMetadata common list corresponding to CassisSpectrum
	 */
	public CassisSpectrumInfo(CassisSpectrum cassisSpectrum, String path, String name, List<CassisMetadata> cassisMetadataList) {
		this.cassisSpectrumList = new ArrayList<CassisSpectrum>();
		this.cassisSpectrumList.add(cassisSpectrum);
		this.path = path;
		this.name = name;
		this.cassisMetadataList = cassisMetadataList;
		this.metadataMerge = false;
	}

	/**
	 * Constructor which create CassisSpectrumInfo using File.
	 *
	 * @param file
	 *            Type File
	 */
	public CassisSpectrumInfo(File file) {
		this.path = file.getAbsolutePath();
		this.name = file.getName();
		this.cassisSpectrumList = new ArrayList<CassisSpectrum>();
		this.cassisMetadataList = new ArrayList<CassisMetadata>();
		this.metadataMerge = false;
	}

	/**
	 * Getter to List<CassisMetadata>.
	 *
	 * @return cassisMetadataList
	 *
	 */
	public List<CassisMetadata> getMetadataList() {
		return cassisMetadataList;
	}

	/**
	 * Getter to List<CassisSpectrum>.
	 *
	 * @return cassisSpectrumList
	 */
	public List<CassisSpectrum> getCassisSpectrumList() {
		return cassisSpectrumList;
	}

	/**
	 * Setter to modify cassisSpectrum list.
	 *
	 * @param cassisSpectrumList
	 *            The new cassisSpectrumList
	 */
	public void setCassisSpectrumList(List<CassisSpectrum> cassisSpectrumList) {
		this.cassisSpectrumList = cassisSpectrumList;
	}

	/**
	 * Setter to modify cassisMetadataList list.
	 *
	 * @param cassisMetadataList
	 *            The new metadatas
	 */
	public void setMetadatasList(List<CassisMetadata> cassisMetadataList) {
		this.cassisMetadataList = cassisMetadataList;
	}

	/**
	 * Getter to path file.
	 *
	 * @return path file
	 */
	public String getPath() {
		return this.path;
	}

	/**
	 * Getter to name file.
	 *
	 * @return name file
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Setter for the name file
	 *
	 * @param name
	 *            The new name file
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Setter for the path name
	 *
	 * @param path
	 *            The new path file
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * Getter for the number of spectrum
	 *
	 * @return The number of spectrum
	 */
	public int getSpectrumNumber() {
		return cassisSpectrumList.size();
	}

	/**
	 * Merge between common metadata and first spectrum metadata when there is only one spectrum in the spectrum list
	 */
	public void mergeCommonMetadataWithFirstSpectrumMetadata() {
		if (!metadataMerge) {
			metadataMerge = true;
			if (cassisSpectrumList != null && !cassisSpectrumList.isEmpty() && cassisMetadataList != null && !cassisMetadataList.isEmpty()) {
				if (cassisSpectrumList.size() == 1) {
					List<CassisMetadata> spectrumMetadataList = cassisSpectrumList.get(0).getOriginalMetadataList();
					if (!spectrumMetadataList.equals(cassisMetadataList)) {
						for (CassisMetadata commonMetadata : cassisMetadataList) {
							boolean exists = false;
							for (CassisMetadata spectrumMetadata : spectrumMetadataList) {
								if (commonMetadata.getName().equals(spectrumMetadata.getName())) {
									exists = true;
									break;
								}
							}
							if (!exists) {
								spectrumMetadataList.add(commonMetadata);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Getter for the datalink model
	 *
	 * @return the datalink model array or null if file doesn't contain datalink
	 */
	public DatalinkModel[] getDatalinkModelTab() {
		if (FileManagerVOTable.containDatalink(path) && datalinkModelTab == null) {
			datalinkModelTab = new DatalinkModel[getSpectrumNumber()];
			for (int i = 0; i < getSpectrumNumber(); i++) {
				datalinkModelTab[i] = new DatalinkModel(path, i);
			}
		}
		return datalinkModelTab;
	}

	/**
	 * Getter for x columns of data detected
	 *
	 * @return the x columns of data detected
	 */
	public List<ColumnsDetected> getXColumnsDetected() {
		return xColumnsDetected;
	}

	/**
	 * Getter for y columns of data detected
	 *
	 * @return the y columns of data detected
	 */
	public List<ColumnsDetected> getYColumnsDetected() {
		return yColumnsDetected;
	}

	/**
	 * Setter for x columns of data detected
	 *
	 * @param xColumnsDetected
	 *            The new list of x columns of data
	 */
	public void setXColumnsDetected(List<ColumnsDetected> xColumnsDetected) {
		this.xColumnsDetected = xColumnsDetected;
	}

	/**
	 * Setter for y columns of data detected
	 *
	 * @param yColumnsDetected
	 *            The new list of y columns of data
	 */
	public void setYColumnsDetected(List<ColumnsDetected> yColumnsDetected) {
		this.yColumnsDetected = yColumnsDetected;
	}

	/***
	 * get the index of the default CassisSpectrum to display
	 * @return the index of the default CassisSpectrum to display
	 */
	public int getDefaultIndex() {
		int res = 0;
		boolean find = false;
		for (int i = 0; !find && i < cassisSpectrumList.size(); i++) {
			CassisSpectrum spectrum = cassisSpectrumList.get(i);
			CassisMetadata fileTypeMetadata = spectrum.getCassisMetadata(CassisMetadata.FILE_TYPE);
			String fileTypeValue = fileTypeMetadata == null ? null : fileTypeMetadata.getValue();
			if (FileManager.FileType.ASCII.name().equals(fileTypeValue)) {
				return -1;
			}
		}
		for (int i = 0; !find && i < cassisSpectrumList.size(); i++) {
			CassisSpectrum spectrum = cassisSpectrumList.get(i);
			CassisMetadata hduNameMetadata = spectrum.getOriginalMetadata(CassisMetadata.HDU_NAME);
			String hduNameValue = hduNameMetadata == null ? null : hduNameMetadata.getValue();
			if ("SPECTRUM".equals(hduNameValue)) {
				res = i;
				find = true;
			}
		}
		for (int i = 0; !find && i < cassisSpectrumList.size(); i++) {
			CassisSpectrum spectrum = cassisSpectrumList.get(i);
			CassisMetadata hduNameMetadata = spectrum.getOriginalMetadata(CassisMetadata.HDU_NAME);
			String hduNameValue = hduNameMetadata == null ? null : hduNameMetadata.getValue();
			if ("dataset".equals(hduNameValue)) {
				res = i;
				find = true;
			}
		}

		return res;
	}
}
