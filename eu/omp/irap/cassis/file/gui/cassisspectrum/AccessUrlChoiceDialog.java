/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.cassisspectrum;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;


/**
 * Create a {@link JDialog} for the choice of access URL on VOTable file
 *
 * @author Lea Foissac
 */
public class AccessUrlChoiceDialog extends JDialog {
	private static final long serialVersionUID = -5748783374712961067L;

	private VotableWithUrl votableWithUrl;
	private FieldNameDescription[] fieldsWithUrls;
	private JPanel panel;
	private JPanel buttonsPanel;
	private JButton okButton;
	private JButton spectrumDataOnVotableButton;
	private JComboBox<FieldNameDescription> columnComboBox;
	private JLabel descriptionLabel;


	/**
	 * Create a {@link JDialog} for the choice of access URL on {@link VotableWithUrl} file
	 *
	 * @param votableWithUrl
	 *            The {@link VotableWithUrl} element corresponding to VOTtable file
	 */
	public AccessUrlChoiceDialog(VotableWithUrl votableWithUrl) {
		super();
		this.setModal(true);
		this.setTitle("Access URL");
		this.setName("accessUrlDialog");
		this.votableWithUrl = votableWithUrl;
		this.fieldsWithUrls = votableWithUrl.getFieldsWithUrls();
		this.setMaximumSize(new Dimension(600, 400));
		this.setContentPane(getPanel());
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	/**
	 * Getter for the general {@link JPanel} containing a {@link JComboBox} and 2 {@link JButton}
	 *
	 * @return The panel
	 */
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel(new BorderLayout());
			panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
			panel.add(new JLabel("Select the field to use to retrieve the file:"), BorderLayout.NORTH);
			JPanel tmpPanel = new JPanel(new BorderLayout());
			tmpPanel.add(getColumnComboBox(), BorderLayout.NORTH);
			JPanel centerTmpPanel = new JPanel(new BorderLayout());
			centerTmpPanel.setBorder(BorderFactory.createTitledBorder("Description"));
			centerTmpPanel.getInsets().set(5, 10, 5, 10);
			centerTmpPanel.add(getDescriptionLabel(), BorderLayout.CENTER);
			tmpPanel.add(centerTmpPanel, BorderLayout.CENTER);
			panel.add(tmpPanel, BorderLayout.CENTER);
			panel.add(getButtonsPanel(), BorderLayout.SOUTH);
		}
		return panel;
	}

	/**
	 * Getter for the {@link JPanel} containing all {@link JButton}
	 *
	 * @return The panel containing all {@link JButton}
	 */
	private JPanel getButtonsPanel() {
		if (buttonsPanel == null) {
			buttonsPanel = new JPanel(new FlowLayout());
			buttonsPanel.add(getOkButton());
			if (votableWithUrl.getType() ==
					eu.omp.irap.cassis.file.votable.VotableType.UNKNOWN) {
				buttonsPanel.add(getSpectrumDataOnVotableButton());
			}
		}
		return buttonsPanel;
	}

	/**
	 * Getter for the {@link JComboBox} containing the name of column in VOTable
	 *
	 * @return The {@link JComboBox} element
	 */
	private JComboBox<FieldNameDescription> getColumnComboBox() {
		if (columnComboBox == null) {
			columnComboBox = new JComboBox<FieldNameDescription>(fieldsWithUrls);
			columnComboBox.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
			columnComboBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					refreshDescription();
				}
			});
		}
		return columnComboBox;
	}

	/**
	 * Getter for the "ok" {@link JButton}
	 *
	 * @return The "ok" {@link JButton}
	 */
	private JButton getOkButton() {
		if (okButton == null) {
			okButton = new JButton("Ok");
			okButton.setName("Ok");
			okButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					votableWithUrl.setAccessUrl(columnComboBox.getSelectedItem().toString());
					AccessUrlChoiceDialog.this.dispose();
				}
			});
		}
		return okButton;
	}

	/**
	 * Getter for the "Spectrum data is on the votable" {@link JButton}
	 * @return The "Spectrum data is on the votable" {@link JButton}
	 */
	private JButton getSpectrumDataOnVotableButton() {
		if (spectrumDataOnVotableButton == null) {
			spectrumDataOnVotableButton = new JButton("Spectrum data is on the votable");
			spectrumDataOnVotableButton.setName("Spectrum data is on the votable");
			spectrumDataOnVotableButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					votableWithUrl.setAccessUrl(VotableWithUrl.NOT_ACCESS_URL);
					votableWithUrl.setType(eu.omp.irap.cassis.file.votable.VotableType.SPECTRUM);
					AccessUrlChoiceDialog.this.dispose();
				}
			});
		}
		return spectrumDataOnVotableButton;
	}

	/**
	 * Create if necessary then return the description label.
	 *
	 * @return the description label.
	 */
	private JLabel getDescriptionLabel() {
		if (descriptionLabel == null) {
			descriptionLabel = new JLabel();
			refreshDescription();
		}
		return descriptionLabel;
	}

	/**
	 * Refresh the description label (and resize the dialog).
	 */
	private void refreshDescription() {
		FieldNameDescription fnd = (FieldNameDescription) getColumnComboBox().getSelectedItem();
		if (fnd != null) {
			getDescriptionLabel().setText(fnd.getDescription());
		}
		pack();
	}
}
