/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.cassisspectrum;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import eu.omp.irap.cassis.file.gui.download.FileDownloadHelper;

public class VotableDownloadWorker extends SwingWorker<Void, Void> {

	private final static String TEMP_DIRECTORY = System.getProperty("java.io.tmpdir");
	private final static int BUFFER_SIZE = 8192;

	private VotableDownloadGui votableDownloadGui;
	private int index;
	private String localPath;
	private boolean downloadError;


	/**
	 * Create a SwingWorker to download file according to index and a {@link VotableDownloadGui}
	 *
	 * @param votableDownloadGui
	 *            The gui associated to this download
	 * @param index
	 *            The index of file to download in the {@link VotableWithUrl}
	 */
	public VotableDownloadWorker(VotableDownloadGui votableDownloadGui, int index) {
		this.votableDownloadGui = votableDownloadGui;
		this.index = index;
		this.downloadError = false;
	}

	/**
	 * Download the file in a background thread
	 *
	 * @see javax.swing.SwingWorker#doInBackground()
	 */
	@Override
	protected Void doInBackground() throws Exception {
		votableDownloadGui.getVotableWithUrl().getFilesDownloadState()[index] =
				VotableWithUrl.DOWNLOAD_IN_PROGRESS;
		String url = votableDownloadGui.getVotableWithUrl().getUrl(index);
		try (FileDownloadHelper fileDownloadHelper = new FileDownloadHelper(url)) {
			InputStream inputStream = fileDownloadHelper.download();
			String ext = fileDownloadHelper.getExtension();
			String fileName = fileDownloadHelper.getFileName();
			localPath = getUniqueFilePath(fileName, ext);
			try (FileOutputStream fos = new FileOutputStream(localPath)) {
				byte[] buffer = new byte[BUFFER_SIZE];
				int read = -1;
				long totalRead = 0;
				long size = fileDownloadHelper.getSize();
				while ((read = inputStream.read(buffer)) != -1 &&!isCancelled()) {
					fos.write(buffer, 0, read);
					if (size != -1) {
						totalRead += read;
						int currentProgress = (int) (totalRead * 100 / size);
						if (currentProgress >= 0 && currentProgress <= 100) {
							setProgress(currentProgress);
						}
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			this.downloadError = true;
			setProgress(0);
			cancel(true);
		} finally {
			if (isCancelled() || downloadError) {
				File downloadedFile = new File(localPath);
				if (downloadedFile.exists()) {
					try {
						downloadedFile.delete();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		return null;
	}

	/**
	 * Display an error message in EDT in error case and update file status
	 *
	 * @see javax.swing.SwingWorker#done()
	 */
	@Override
	protected void done() {
		if (isCancelled() && downloadError) {
			JOptionPane.showMessageDialog(votableDownloadGui,
					"Error downloading file.", "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			votableDownloadGui.getVotableWithUrl().getFilesDownloadState()[index] =
					VotableWithUrl.DOWNLOADED;
		}
	}

	/**
	 * Getter for the local path
	 *
	 * @return The local path
	 */
	public String getLocalPath() {
		return localPath;
	}

	/**
	 * Get a path to a non existing file.
	 *
	 * @param fileNameOrigin The name of the file to start with.
	 * @param extFile The extension of the file.
	 * @return The unique path.
	 */
	private String getUniqueFilePath(String fileNameOrigin, String extFile) {
		String filePath;
		String fileName;
		File tmpFile;
		int val = 1;
		do {
			val++;
			if (extFile.isEmpty() || extFile.length() >= 10) {
				fileName = fileNameOrigin + "_" + val;
			} else {
				int index = fileNameOrigin.indexOf(extFile);
				fileName = fileNameOrigin.substring(0, index - 1) + "_" + val + "." + extFile;
			}
			filePath = TEMP_DIRECTORY + File.separatorChar + fileName;
			tmpFile = new File(filePath);
		} while (tmpFile.exists());
		return filePath;
	}

	/**
	 * Getter for the index of file to download
	 *
	 * @return The file index to download
	 */
	public int getIndex() {
		return index;
	}

}
