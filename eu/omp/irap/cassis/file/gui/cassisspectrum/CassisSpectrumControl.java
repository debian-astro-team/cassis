/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.cassisspectrum;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.file.ascii.parser.integration.ParsingModuleUtils;
import eu.omp.irap.cassis.file.gui.cassisspectrum.CassisSpectrumPanel.TypeElementSelected;
import eu.omp.irap.cassis.file.gui.medatada.CASSIS_METADATA;
import eu.omp.irap.cassis.file.votable.VotableType;
import eu.omp.irap.cassis.file.votable.VotableTypeChecker;

/**
 * Create controller to interact with the panel (CassisSpectrumPanel class) and the model (CassisSpectrumModel class).
 *
 * @author Lea Foissac
 */
public class CassisSpectrumControl {

	private enum State { OPEN, SPECTRUM, NOT_OPEN };
	private CassisSpectrumModel model;
	private CassisSpectrumPanel panel;

	private Runnable action;
	private boolean lastReadSuccessful = true;


	/**
	 * Constructor Create a CassisSpectrumControl.
	 *
	 * @param panel
	 *            The panel of CassisSpectrumControl
	 * @param model
	 *            The model of CassisSpectrumControl
	 */
	public CassisSpectrumControl(CassisSpectrumPanel panel, CassisSpectrumModel model) {
		this.panel = panel;
		this.model = model;
	}

	/**
	 * Return the model.
	 *
	 * @return model Return the model (CassisSpectrumModel)
	 */
	public CassisSpectrumModel getModel() {
		return model;
	}

	/**
	 * Open file info frame according to CassiSpectrum selected in JTree.
	 *
	 * @param tree
	 *            JTree in CassisSpectrumPanel
	 *
	 */
	public void openFileInfoFrame(JTree tree) {
		try {
			TreePath selectionPath = tree.getSelectionPath();
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) selectionPath.getLastPathComponent();
			if (((DefaultMutableTreeNode) node.getParent()).isRoot()) {
				// Only one spectrum in file
				CassisSpectrumInfo cassisSpectrumInfoSelected = (CassisSpectrumInfo) node.getUserObject();
				panel.openResourceInfo(cassisSpectrumInfoSelected.getCassisSpectrumList().get(0), cassisSpectrumInfoSelected);
			} else {
				// Several spectra in file
				CassisSpectrum cassisSpectrumSelected = (CassisSpectrum) node.getUserObject();
				CassisSpectrumInfo cassisSpectrumInfoParent = (CassisSpectrumInfo) ((DefaultMutableTreeNode) node.getParent()).getUserObject();
				panel.openResourceInfo(cassisSpectrumSelected, cassisSpectrumInfoParent);
			}
		} catch (Exception e) {
			panel.openSelectCassisSpectrumJOptionPane();
		}
	}

	/**
	 * Remove resource according to CassisSpectrumInfo selected in JTree.
	 *
	 * @param tree
	 *            JTree in CassisSpectrumPanel
	 */
	public void removeResourceFromJTree(JTree tree) {
		try {
			int selectionCount = tree.getSelectionCount();
			if (selectionCount > 1) {
				final TreePath[] selectionPaths = tree.getSelectionPaths();
				for (int i = 0; i < selectionPaths.length; i++) {
					DefaultMutableTreeNode node = (DefaultMutableTreeNode) selectionPaths[i].getLastPathComponent();
					if (node.getUserObject() instanceof CassisSpectrumInfo) {
						model.removeResource(node);
					}
				}
				if (tree.getRowCount() > 0) {
					tree.setSelectionRow(1);
				}
			} else {
				TreePath selectionPath = tree.getSelectionPath();
				DefaultMutableTreeNode node = (DefaultMutableTreeNode) selectionPath.getLastPathComponent();
				if (node.getUserObject() instanceof CassisSpectrumInfo) {
					model.removeResource(node);
					if (tree.getRowCount() > 0) {
						tree.setSelectionRow(1);
					}
				} else {
					panel.openResourceNotSelectedJOptionPane();
				}
			}
		} catch (Exception e) {
			panel.openResourceNotSelectedJOptionPane();
		}
	}

	/**
	 * Remove resource according to {@link VotableWithUrl} selected on {@link JTree}
	 *
	 * @param tree
	 *            The {@link JTree} on the {@link CassisSpectrumPanel}
	 */
	public void removeVotableWithUrlFromJTree(JTree tree) {
		try {
			int selectionCount = tree.getSelectionCount();
			if (selectionCount > 1) {
				final TreePath[] selectionPaths = tree.getSelectionPaths();
				for (int i = 0; i < selectionPaths.length; i++) {
					DefaultMutableTreeNode node = (DefaultMutableTreeNode) selectionPaths[i].getLastPathComponent();
					getModel().getDefaultTreeModel().removeNodeFromParent(node);
					getModel().getVotableWithUrlList().remove(node.getUserObject());
					getModel().getVotableWithUrlNodeList().remove(node);
				}
				if (tree.getRowCount() > 0) {
					tree.setSelectionRow(1);
				}
			} else {
				TreePath selectionPath = tree.getSelectionPath();
				DefaultMutableTreeNode node = (DefaultMutableTreeNode) selectionPath.getLastPathComponent();
				getModel().getDefaultTreeModel().removeNodeFromParent(node);
				getModel().getVotableWithUrlList().remove(node.getUserObject());
				getModel().getVotableWithUrlNodeList().remove(node);
				if (tree.getRowCount() > 0) {
					tree.setSelectionRow(1);
				} else {
					panel.openResourceNotSelectedJOptionPane();
				}
			}
		} catch (Exception e) {
			panel.openResourceNotSelectedJOptionPane();
		}
	}



	/**
	 * Add selected resources to JTree.
	 *
	 * @param file
	 * @param params
	 */
	public void addResourceToJTree(File file, Map<String, String> params) {
		boolean alreadyOpen = false;
		lastReadSuccessful = true;

		if (file.exists()) {
			CassisSpectrumInfo cassisSpectrumInfo = new CassisSpectrumInfo(file);
			final ArrayList<CassisMetadata> cassisMetadataList = new ArrayList<CassisMetadata>();
			if (params !=null){
				for (String name : params.keySet()) {
					cassisMetadataList.add(new CassisMetadata(name, params.get(name),"", ""));
				}
			}
			cassisSpectrumInfo.setMetadatasList(cassisMetadataList);
			if (isResourceOpen(cassisSpectrumInfo)) {
				alreadyOpen = true;
			} else {
				openSpectrum(cassisSpectrumInfo);
			}


		} else {
			panel.openResourceNotExistsJOptionPane();
		}

		if (alreadyOpen) {
			panel.openResourceIsOpenJOptionPane();
		}
	}


	/**
	 * Add selected resources to JTree.
	 *
	 * @param files
	 *            Files to add to the JTree
	 */
	public void addResourceToJTree(File[] files) {
		boolean alreadyOpen = false;
		lastReadSuccessful = true;
		for (File file : files) {
			alreadyOpen = addResourceToTree(file, alreadyOpen);
		}
		if (alreadyOpen) {
			selectTreeFor(files[files.length-1]);
			panel.openResourceIsOpenJOptionPane();
		}
	}

	/**
	 * Add resource to the JTree.
	 *
	 * @param file The file to add.
	 * @param displayAlreadyOpen True to display the already open dialog if the
	 *  file is already open, false to do not display it.
	 */
	public void addResourceToJTree(File file, boolean displayAlreadyOpen) {
		boolean alreadyOpen = addResourceToTree(file, false);
		if (alreadyOpen) {
			selectTreeFor(file);
			if (displayAlreadyOpen) {
				panel.openResourceIsOpenJOptionPane();
			}
		}
	}

	/**
	 * Add file to JTree.
	 *
	 * @param file The file.
	 * @param alreadyOpen The state of already open file.
	 * @return the new value of "alreadyOpen" after opening the given file.
	 */
	private boolean addResourceToTree(File file, boolean alreadyOpen) {
		lastReadSuccessful = true;
		if (file.exists()) {
			CassisSpectrumInfo cassisSpectrumInfo = new CassisSpectrumInfo(file);
			if (isResourceOpen(cassisSpectrumInfo)) {
				alreadyOpen = true;
			} else {
				VotableTypeChecker vtc = new VotableTypeChecker(file);

				if (!vtc.containsError()) {//Not Votable or not well formed
					if (vtc.isSpectrum()) {
						openSpectrum(cassisSpectrumInfo);
					} else {
						State state = addVotableWithUrlToJTree(
								file, false, vtc.getSpecializedType());
						if (state == State.SPECTRUM) {
							openSpectrum(cassisSpectrumInfo);
						}
					}
				} else { // all  the other file
					openSpectrum(cassisSpectrumInfo);
				}
			}
		} else {
			panel.openResourceNotExistsJOptionPane();
		}
		return alreadyOpen;
	}

	/**
	 * Open the given CassisSpectrumInfo.
	 *
	 * @param cassisSpectrumInfo The CassisSpectrumInfo to open.
	 */
	public void openSpectrum(CassisSpectrumInfo cassisSpectrumInfo) {
		DefaultMutableTreeNode newResource = model.addResource(cassisSpectrumInfo);

		TreeNode[] pathToRoot = model.getDefaultTreeModel().getPathToRoot(newResource);
		TreePath path = new TreePath(pathToRoot);
		panel.getTree().expandPath(path);
		final TreeNode treeNode = pathToRoot[pathToRoot.length-1];
		if (treeNode.isLeaf() || cassisSpectrumInfo.getDefaultIndex() == -1){
			panel.getTree().setSelectionPath(path);
		} else {
			panel.getTree().setSelectionPath(path.pathByAddingChild(treeNode.getChildAt(
					cassisSpectrumInfo.getDefaultIndex())));
		}
	}

	/**
	 * Add a VOTable file containing URL on the JTree
	 *
	 * @param file
	 *            The VOTable file {@link File}
	 * @param indexList
	 *            The index of URL to download {@link List}
	 */
	public void addVotableWithUrlToJTree(File file, List<Integer> indexList) {
		VotableTypeChecker vtc = new VotableTypeChecker(file);
		addVotableWithUrlToJTree(file, indexList, vtc.getSpecializedType());
	}

	/**
	 * Add a VOTable file containing URL on the JTree
	 *
	 * @param file
	 *            The VOTable file {@link File}
	 * @param indexList
	 *            The index of URL to download {@link List}
	 * * @param type
	 *            The type of the file to force it
	 */
	public void addVotableWithUrlToJTree(File file, List<Integer> indexList, VotableType type) {
		try {
			VotableWithUrl votableWithUrl;
			if (indexList == null || indexList.isEmpty()) {
				votableWithUrl = new VotableWithUrl(file, type);
			} else {
				Collections.sort(indexList);
				votableWithUrl = new VotableWithUrl(file, indexList, type);
			}
			openVotableWithUrl(votableWithUrl);
		} catch (IOException e) {
			panel.openVotableFileWithUrlErrorOptionPane();
			e.printStackTrace();
		}
	}

	/**
	 * Add a VOTable file containing URL on the JTree, display error.
	 *
	 * @param file The VOTable file to open
	 * @return The state of the VOTable.
	 */
	public State addVotableWithUrlToJTree(File file) {
		VotableTypeChecker vtc = new VotableTypeChecker(file);
		return addVotableWithUrlToJTree(file, true, vtc.getSpecializedType());
	}

	/**
	 * Add a file containing with may be a VOTable with URLs on the JTree.
	 * This function is specifically done to handle case where we don't know
	 * if is a VOTable with URL but can be. In that case we can't avoid to
	 *  display an error.
	 *
	 * @param file The VOTable file to open
	 * @param displayError true to display Exception/GUI error pop-up, false otherwise.
	 * @param type The type of the file.
	 * @return The state of the VOTable.
	 */
	public State addVotableWithUrlToJTree(File file, boolean displayError, VotableType type) {
		State state = State.NOT_OPEN;
		try {
			VotableWithUrl votableWithUrl = new VotableWithUrl(file, type);
			state = openVotableWithUrl(votableWithUrl);
		} catch (IOException e) {
			state = State.NOT_OPEN;
			if (displayError) {
				panel.openVotableFileWithUrlErrorOptionPane();
				e.printStackTrace();
			}
		}
		return state;
	}

	/**
	 * Open a VOTable with URL file
	 *
	 * @param votableWithUrl
	 *            The VOTable with URL file to open
	 * @return The state of the VOTable.
	 * @throws IOException
	 *             VOTable format problem
	 */
	private State openVotableWithUrl(VotableWithUrl votableWithUrl) throws IOException {
		State state = State.NOT_OPEN;
		if (!isVotableWithUrlOpen(votableWithUrl)) {
			// When VOTable file isn't open
			if (!votableWithUrl.isAccessUrlOk() &&
					votableWithUrl.getFieldsWithUrls().length >= 1) {
				new AccessUrlChoiceDialog(votableWithUrl);
			} else if (!(votableWithUrl.getType() == VotableType.EPN_TAP
					|| votableWithUrl.getType() == VotableType.SSA) ||
					!votableWithUrl.isAccessUrlOk()) {
				votableWithUrl.setType(VotableType.SPECTRUM);
			}
			if (votableWithUrl.getType() != VotableType.SPECTRUM &&
					!votableWithUrl.getAccessUrl().equals(VotableWithUrl.NOT_ACCESS_URL)) {
				state = State.OPEN;
				votableWithUrl.recoverNameFile();
				model.addVotableURLFile(votableWithUrl);
				panel.getTree().expandRow(0);
				panel.getTree().setSelectionRow(panel.getTree().getRowCount() - 1);
			} else if (votableWithUrl.getType() == VotableType.SPECTRUM) {
				state = State.SPECTRUM;
			}
		} else {
			// When VOTable file is already open
			VotableWithUrl voWithUrl = recoverVotableWithUrlOpenWithSameIndex(votableWithUrl);
			if (voWithUrl != null) {
				// The index are not the same. Adding of the new index
				state = State.OPEN;
				model.addIndexToVotableURLFile(voWithUrl, votableWithUrl.getUrlIndexList());
				panel.getTree().expandRow(0);
			} else {
				// The same index are open so VOTable file isn't open again
				state = State.OPEN;
				panel.openResourceIsOpenJOptionPane();
			}
		}
		return state;
	}

	/**
	 * Open a {@link VotableDownloadGui} to download a file
	 *
	 * @param selectedNode
	 *            The selected node on the {@link JTree}
	 * @param votableWithUrl
	 *            The object type {@link VotableWithUrl}
	 * @param index
	 *            The file index in VOTable file to download
	 */
	public void downloadFile(final DefaultMutableTreeNode selectedNode,
			final VotableWithUrl votableWithUrl, final int index) {
		final VotableDownloadGui votableDownloadGui = new VotableDownloadGui(votableWithUrl, index);
		Runnable r = new Runnable() {
			@Override
			public void run() {
				if (votableWithUrl.getFilesDownloadState()[index] == VotableWithUrl.DOWNLOADED) {
					votableWithUrl.getCassisSpectrumInfoTab()[index].setPath(votableDownloadGui.getLocalPathFile());
					model.notifyAddSpectra(selectedNode);
					if (selectedNode.equals(panel.getTree().getLastSelectedPathComponent())) {
						model.notifyCassisSpectrumInfoSelected(selectedNode);
						panel.downloadFileSelected(selectedNode);
						panel.getTree().expandPath(new TreePath(selectedNode.getPath()));
					}
				}
			}
		};
		votableDownloadGui.setFinishAction(r);
	}

	/**
	 * Specific if resource (CassisSpectrumInfo) is already open or not.
	 *
	 * @param cassisSpectrumInfo
	 *            The object type CassisSpectrumInfo
	 * @return true if resource is already open and false if it's not open
	 */
	private boolean isResourceOpen(CassisSpectrumInfo cassisSpectrumInfo) {
		for (int i = 0; i < model.getCassisSpectrumInfoList().size(); i++) {
			if (cassisSpectrumInfo.getPath().equals(model.getCassisSpectrumInfoList().get(i).getPath())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Specific if {@link VotableWithUrl} resource is already open or not.
	 *
	 * @param votableWithUrl
	 *            The object type {@link VotableWithUrl}
	 * @return true if resource is already open and false if it's not open
	 */
	private boolean isVotableWithUrlOpen(VotableWithUrl votableWithUrl) {
		for (int i = 0; i < model.getVotableWithUrlList().size(); i++) {
			if (votableWithUrl.getFile().equals(model.getVotableWithUrlList().get(i).getFile())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Research on the {@link VotableWithUrl} list if there is a {@link VotableWithUrl} with the same index list that the {@link VotableWithUrl} param
	 *
	 * @param votableWithUrl
	 *            The {@link VotableWithUrl}
	 * @return The {@link VotableWithUrl} with same index or null if the isn't
	 */
	private VotableWithUrl recoverVotableWithUrlOpenWithSameIndex(VotableWithUrl votableWithUrl) {
		VotableWithUrl voWithUrl = null;
		for (int i = 0; i < model.getVotableWithUrlList().size(); i++) {
			VotableWithUrl votable = model.getVotableWithUrlList().get(i);
			if (votableWithUrl.getFile().equals(votable.getFile())) {
				List<Integer> list1 = votable.getUrlIndexList();
				List<Integer> list2 = votableWithUrl.getUrlIndexList();
				if (!(list1.containsAll(list2) && list2.containsAll(list1))) {
					voWithUrl = votable;
					break;
				}
			}
		}
		return voWithUrl;
	}

	/**
	 * Specific if node is a CassisSpectrumInfo type or not.
	 *
	 * @param node
	 *            The DefaultMutableTreeNode which we want know the object type
	 * @return true if the selected node is a CassisSpectrumInfo and false if not
	 */
	public boolean isCassisSpectrumInfo(DefaultMutableTreeNode node) {
		return node.getUserObject() instanceof CassisSpectrumInfo;
	}

	/**
	 * Specific if node is a CassisSpectrum type or not.
	 *
	 * @param node
	 *            The DefaultMutableTreeNode which we want know the object type
	 * @return true if the selected node is a CassisSpectrum and false if not
	 */
	public boolean isCassisSpectrum(DefaultMutableTreeNode node) {
		return node.getUserObject() instanceof CassisSpectrum;
	}

	/**
	 * Specific if node is a {@link VotableWithUrl} type or not
	 *
	 * @param node
	 *            The {@link DefaultMutableTreeNode} which we want know the object type
	 *
	 * @return true if the selected node is a {@link VotableWithUrl} and false if not
	 */
	public boolean isVotableWithURL(DefaultMutableTreeNode node) {
		return node.getUserObject() instanceof VotableWithUrl;
	}

	/**
	 * Specific if node is a a download file type or not
	 *
	 * @param node
	 *            The {@link DefaultMutableTreeNode} which we want know the object type
	 *
	 * @return true if the selected node is a download file and false if not
	 */
	public boolean isDownloadFile(DefaultMutableTreeNode node) {
		return (node != null)&&
			   (node.getParent() instanceof DefaultMutableTreeNode) &&
				((DefaultMutableTreeNode) node.getParent()).getUserObject() instanceof VotableWithUrl;
	}

	/**
	 * Sets the action that can be executed by the ASCII Parser
	 *
	 * @param action The action to execute
	 */
	public void setAction(Runnable action) {
		this.action = action;
	}

	/**
	 * Returns the action that can be executed by the ASCII Parser
	 *
	 * @return The action to execute
	 */
	public Runnable getAction() {
		return action;
	}

	/**
	 * Changes the text of the action button in the parsing module
	 *
	 * @param text The new text
	 */
	public void setParsingActionText(String text) {
		ParsingModuleUtils.getPanelInstance(this).getButtonPanel().getActionButton().setText(text);
	}

	/**
	 * Opens a new ascii parsing module, replacing the previous one if it exists.
	 *
	 * @param originalFile The file with wich the module must be created, if not null
	 * @param withFileChooser True if the module must be opened with a JFileChooser, false otherwise
	 */
	public void openAsciiParser(File originalFile, boolean withFileChooser) {
		ParsingModuleUtils.getFrameInstance(this).setVisible(true);
		if (originalFile != null) {
			ParsingModuleUtils.getPanelInstance(this).getControl().getModel().loadFile(originalFile);
		}
		if (withFileChooser) {
			ParsingModuleUtils.getPanelInstance(this).getButtonPanel().getImportFile().doClick();
		}
	}

	/**
	 * Check if the last opened resource was read with success, or if
	 * it causes to ask the user to open the Ascii Parser
	 *
	 * @return true if the resource was opened without issue, false if the user
	 * was prompt to use the Ascii Parser
	 */
	public boolean isLastReadSuccessful() {
		return lastReadSuccessful;
	}

	/**
	 * Sets if the last opened resource was read with success, or if
	 * it causes to ask the user to open the Ascii Parser
	 *
	 * @param lastReadSuccessful
	 */
	public void setLastReadSuccessful(boolean lastReadSuccessful) {
		this.lastReadSuccessful = lastReadSuccessful;
	}

	/**
	 * Select the corresponding node in the JTree for the given file.
	 *
	 * @param file the file.
	 */
	public void selectTreeFor(File file) {
		String filePath = file.getAbsolutePath();
		DefaultTreeModel dtm = model.getDefaultTreeModel();
		int childCount = dtm.getChildCount(dtm.getRoot());
		for (int i = 0; i < childCount; i++) {
			Object child = dtm.getChild(dtm.getRoot(), i);
			List<PathTreePath> ps = getPath((DefaultMutableTreeNode) child);
			for (PathTreePath ptp : ps) {
				if (ptp.getPath().equals(filePath)) {
					TreePath path = ptp.getTreePath();
					panel.getTree().expandPath(path);
					panel.getTree().setSelectionPath(path);
					return;
				}
			}
		}
	}

	/**
	 * Return the TreePath for a given node.
	 *
	 * @param node The node.
	 * @return the associated TreePath.
	 */
	private TreePath getTreePath(DefaultMutableTreeNode node) {
		return new TreePath(model.getDefaultTreeModel().getPathToRoot(node));
	}

	/**
	 * Return the PathTreePath list for the given node.
	 *
	 * @param node The node.
	 * @return the PathTreePath list for the given node.
	 */
	public List<PathTreePath> getPath(DefaultMutableTreeNode node) {
		Object object = node.getUserObject();
		TypeElementSelected type = getType(object);
		List<PathTreePath> ptp = new ArrayList<>();
		switch (type) {
		case CASSIS_SPECTRUM:
			CassisSpectrum cs = (CassisSpectrum) object;
			CassisMetadata from = cs.getCassisMetadata(CASSIS_METADATA.FROM.getName());
			if (from != null) {
				ptp.add(new PathTreePath(from.getValue(), getTreePath(node)));
			}
			break;
		case CASSIS_SPECTRUM_INFO:
			CassisSpectrumInfo csi = (CassisSpectrumInfo) object;
			ptp.add(new PathTreePath(csi.getPath(), getTreePath(node)));
			break;
		case VOTABLE_WITH_URL:
			for (int i = 0; i < node.getChildCount(); i++) {
				DefaultMutableTreeNode subNode = (DefaultMutableTreeNode) node.getChildAt(i);
				ptp.addAll(getPath(subNode));
			}
			break;
		default:
			// Nothing to do here. Fall through.
			break;
		}
		return ptp;
	}

	/**
	 * Return the type of element.
	 *
	 * @return the type of element.
	 */
	public TypeElementSelected getType(Object object) {
		if (object == null) {
			return TypeElementSelected.NONE;
		} else if (object instanceof String) {
			return TypeElementSelected.STRING;
		} else if (object instanceof VotableWithUrl) {
			return TypeElementSelected.DOWNLOAD_FILE;
		} else if (object instanceof CassisSpectrumInfo) {
			return TypeElementSelected.CASSIS_SPECTRUM_INFO;
		} else if (object instanceof CassisSpectrum) {
			return TypeElementSelected.CASSIS_SPECTRUM;
		} else if (object instanceof VotableWithUrl) {
			return TypeElementSelected.VOTABLE_WITH_URL;
		} else {
			return TypeElementSelected.UNKNOWN;
		}
	}


	/**
	 * Simple class to store TreePath for a given path.
	 *
	 * @author M. Boiziot
	 */
	private class PathTreePath {

		private final String path;
		private final TreePath treePath;

		/**
		 * Constructor.
		 *
		 * @param path Path of the file.
		 * @param treePath TreePath for the JTree corresponding to the given path.
		 */
		public PathTreePath(String path, TreePath treePath) {
			this.path = path;
			this.treePath = treePath;
		}

		/**
		 * Return the path.
		 *
		 * @return the path.
		 */
		public String getPath() {
			return path;
		}

		/**
		 * Return the TreePath.
		 *
		 * @return the TreePath.
		 */
		public TreePath getTreePath() {
			return treePath;
		}
	}
}
