/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.cassisspectrum;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.MultiScanCassisSpectrum;
import eu.omp.irap.cassis.common.MultiScanCassisSpectrum.OPERATION;

/**
 * Simple dialog for selecting the scans to use in a {@link MultiScanCassisSpectrum}.
 *
 * @author M. Boiziot
 */
public class SelectionMultiScanSpectrumDialog extends JDialog {

	private static final long serialVersionUID = 7231200037181904431L;

	private MultiScanCassisSpectrum spectrum;
	private JButton displayButton;
	private JCheckBox selectAllCheckBox;
	private JTextField selectionTextField;
	private JComboBox<OPERATION> operationComboBox;
	private boolean displayOk;
	private List<Integer> selectedScans;


	/**
	 * Constructor.
	 *
	 * @param owner The frame from which the dialog is displayed.
	 * @param spectrum The {@link MultiScanCassisSpectrum}.
	 */
	public SelectionMultiScanSpectrumDialog(Frame owner, MultiScanCassisSpectrum spectrum) {
		super (owner, "Sub-scans selection", true);
		this.spectrum = spectrum;

		setContentPane(createMainPanel());
	}

	/**
	 * Create then return the main panel.
	 *
	 * @return the main panel.
	 */
	private JPanel createMainPanel() {
		JPanel centerPanel = new JPanel(new GridLayout(3, 2));
		centerPanel.add(new JLabel("Scans selection:"));
		centerPanel.add(getSelectionTextField());
		centerPanel.add(new JLabel(""));
		centerPanel.add(getSelectAllCheckBox());
		centerPanel.add(new JLabel("Operation:"));
		centerPanel.add(getOperationComboBox());

		JPanel mainPanel = new JPanel(new BorderLayout());
		mainPanel.add(centerPanel, BorderLayout.CENTER);
		mainPanel.add(getDisplayButton(), BorderLayout.SOUTH);

		return mainPanel;
	}

	/**
	 * Create if needed then return the Display button.
	 *
	 * @return the Display button.
	 */
	public JButton getDisplayButton() {
		if (displayButton == null) {
			displayButton = new JButton("Display");
			displayButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						List<Integer> scanList = getSelection();
						if (checkSelection(scanList)) {
							selectedScans = scanList;
							displayOk = true;
							dispose();
						}
					} catch (IllegalArgumentException iea) {
						handleError(iea.getMessage());
					}
				}
			});
		}
		return displayButton;
	}

	/**
	 * Return the selection.
	 *
	 * @return the selection.
	 */
	private List<Integer> getSelection() {
		List<Integer> scanList;
		if (selectAllCheckBox.isSelected()) {
			scanList = getListOfSpectrums("*", spectrum.getNbScans());
		} else {
			scanList = getListOfSpectrums(getSelectionTextField().getText(),
					spectrum.getNbScans());
		}
		return scanList;
	}

	/**
	 * Check the validity of the selection. This display an error message and
	 *  reset the variable if the selection is not valid.
	 *
	 * @param selection The selection
	 * @return true if the selection is valid, false otherwise.
	 */
	private boolean checkSelection(List<Integer> selection) {
		if (selection == null || selection.isEmpty()) {
			handleError("No scan selected");
		}

		for (int i : selection) {
			if (i < 0 || i > spectrum.getNbChannel() - 1) {
				handleError("The selected scans must be between 1 and "
						+ spectrum.getNbChannel() + ".");
				return false;
			}
		}
		return true;
	}

	/**
	 * Handle error: display an error message and reset the variables to
	 *  reflect the error.
	 *
	 * @param message The error message to display.
	 */
	private void handleError(String message) {
		selectedScans = null;
		displayOk = false;
		JOptionPane.showMessageDialog(SelectionMultiScanSpectrumDialog.this,
				message, "Error", JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * Create if needed then return the "Select all scans" checkBox.
	 *
	 * @return the "Select all scans" checkBox.
	 */
	public JCheckBox getSelectAllCheckBox() {
		if (selectAllCheckBox == null) {
			selectAllCheckBox = new JCheckBox("Select all scans");
			selectAllCheckBox.setToolTipText("If this checkbox is checked, all scans will be used");
			selectAllCheckBox.setSelected(true);
			selectAllCheckBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					getSelectionTextField().setEnabled(!selectAllCheckBox.isSelected());
				}
			});
		}
		return selectAllCheckBox;
	}

	/**
	 * Create if needed then return the selection text field.
	 *
	 * @return the selection text field.
	 */
	public JTextField getSelectionTextField() {
		if (selectionTextField == null) {
			selectionTextField = new JTextField("1-" + (spectrum.getNbScans() + 1));
			selectionTextField.setEnabled(!getSelectAllCheckBox().isSelected());
			selectionTextField.setToolTipText("<html>Selection of the scans to use.<br/>"
					+ "It allow to select unique value, in that case they have"
					+ " to be separated by \";\", like \"1;3;18\" will select"
					+ " 1, 3 and 18.<br/>"
					+ "It also allow to select range, they have to be separated"
					+ " by \"-\", like \"1-4\" will select 1, 2, 3 and 4.<br/>"
					+ "It may have combinaison of it like \"1-3;6;8-10\" will "
					+ "select 1, 2, 3, 6, 8, 9 and 10.<br/>"
					+ "To select everything you can also use \"*\" or check the"
					+ " checkbox beside.<br/></html>");
		}
		return selectionTextField;
	}

	/**
	 * Create if needed then return the JComboBox for selecting the operation to do.
	 *
	 * @return the JComboBox for selecting the operation to do.
	 */
	public JComboBox<OPERATION> getOperationComboBox() {
		if (operationComboBox == null) {
			operationComboBox = new JComboBox<>(OPERATION.values());
		}
		return operationComboBox;
	}

	/**
	 * Return the created spectrum following the selection of the scan and the operation.
	 *
	 * @return the created spectrum following the selection of the scan and the operation.
	 */
	public CassisSpectrum getFinalSpectrum() {
		return spectrum.getCassisSpectrum(selectedScans,
				(OPERATION) getOperationComboBox().getSelectedItem());
	}

	/**
	 * Return if the parameters selected by the user allow to create/display the spectrum.
	 *
	 * @return true if the spectrum can be created/displayed, false otherwise.
	 */
	public boolean isDisplayOk() {
		return displayOk;
	}

	/**
	 * Return a list of scan number from a String.
	 *
	 * @param text The string.
	 * @param scanNumber The number of scans (number higher than this one will be ignored).
	 * @return The list of scans.
	 * @throws IllegalArgumentException If the text is null, empty or can not be parsed.
	 */
	private static List<Integer> getListOfSpectrums(String text, int scanNumber) throws IllegalArgumentException {
		if (text == null || text.trim().isEmpty()) {
			throw new IllegalArgumentException("The provided text input is empty.");
		}

		if ("*".equals(text)) {
			List<Integer> listFilter = new ArrayList<Integer>(scanNumber);
			for (int i = 0; i < scanNumber; i++) {
				listFilter.add(i);
			}
			return listFilter;
		}

		Pattern pattern = Pattern.compile("[^0-9\\-, ]");
		Matcher matcher = pattern.matcher(text);
		if (matcher.find() || text.trim().isEmpty()) {
			throw new IllegalArgumentException("The input string \"" + text + "\" can not be parsed.");
		}

		Pattern patternComma = Pattern.compile("[,]");
		String[] results = patternComma.split(text.trim());

		List<Integer> filter = new ArrayList<Integer>();
		for (int i = 0; i < results.length; i++) {
			splitTrait(results[i], filter, scanNumber);
		}

		return filter;
	}

	/**
	 * Parse the text and add the found scans numbers in the filter if not already in.
	 *
	 * @param text The text to parse.
	 * @param filter The list of plots numbers
	 * @param scanNumber The number of scans (number higher than this one will be ignored).
	 */
	private static void splitTrait(String text, List<Integer> filter, int scanNumber) {
		if (text.trim().startsWith("-") || text.trim().endsWith("-")) {
			return;
		}

		Pattern pattern = Pattern.compile("[\\-]");
		String[] results = pattern.split(text);

		if (results.length == 1) {
			String sub = results[0].trim();
			if (!"".equals(sub)) {
				int val = Integer.parseInt(sub);
				if (val >= 1 && val <= scanNumber && !filter.contains(val - 1)) {
					filter.add(val - 1);
				}
			}
		} else if (results.length == 2) {
			String sub1 = results[0].trim();
			String sub2 = results[1].trim();
			if (!"".equals(sub1) && !"".equals(sub2)) {
				int left = Integer.parseInt(sub1);
				int right = Integer.parseInt(sub2);
				for (int i = left; i <= right; i++) {
					if (i <= scanNumber) {
						if (i >= 1 && !filter.contains(i - 1)) {
							filter.add(i - 1);
						}
					}
					else {
						break;
					}
				}
			}
		}
	}

}
