/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.cassisspectrum;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.ToolTipManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeSelectionModel;

import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.file.CassisFileFilter;
import eu.omp.irap.cassis.file.gui.download.DownloadGui;
import eu.omp.irap.cassis.file.gui.fileinfo.FileInfoFrame;

/**
 * Create a CassisSpectrumPanel which allow to open spectrum file.
 *
 * @author Lea Foissac
 */
public class CassisSpectrumPanel extends JPanel {

	private static final long serialVersionUID = 6271017746352568119L;

	public static enum TypeElementSelected {
		CASSIS_SPECTRUM, CASSIS_SPECTRUM_INFO, DOWNLOAD_FILE, VOTABLE_WITH_URL, NONE, STRING, UNKNOWN
	}

	private CassisSpectrumControl control;
	private JTree tree;
	private JPanel panelButton;
	private JButton openLocalResourceButton;
	private JButton openUrlResourceButton;
	private JButton openAsciiResourceButton;
	private JButton closeResourceButton;

	/**
	 * Create CassisSpectrumPanel.
	 */
	public CassisSpectrumPanel() {
		this(new CassisSpectrumModel());
	}

	/**
	 * Constructor Create CassisSpectrumPanel using CassisSpectrumModel.
	 *
	 * @param model
	 *            The model of CassisSpectrumPanel
	 */
	public CassisSpectrumPanel(CassisSpectrumModel model) {
		super(new BorderLayout());
		this.control = new CassisSpectrumControl(this, model);
		setBorder(BorderFactory.createCompoundBorder(new TitledBorder("Cassis Spectrum"), new EmptyBorder(10, 10, 10, 10)));
		JScrollPane scrollPane = new JScrollPane(getTree());
		scrollPane.setPreferredSize(new Dimension(176, 0));
		add(scrollPane, BorderLayout.CENTER);
		add(getButtonsPanel(), BorderLayout.SOUTH);
	}

	/**
	 * Create a JTree if it's not exists.
	 *
	 * @return the JTree
	 */
	public JTree getTree() {
		if (tree == null) {
			tree = new JTree();
			tree.getSelectionModel().setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
			tree.setModel(control.getModel().getDefaultTreeModel());
			tree.setSelectionRow(0);

			TreeSelectionListener treeSelectionListener = new TreeSelectionListener() {
				@Override
				public void valueChanged(TreeSelectionEvent e) {
					DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
					switch (getTypeElementSelected()) {
					case STRING:
						stringSelected();
						control.getModel().notifyDisableTabDatalink();
						break;
					case DOWNLOAD_FILE:
						downloadFileSelected(selectedNode);
						control.getModel().notifyDisplayDatalink(selectedNode);
						break;
					case CASSIS_SPECTRUM_INFO:
						cassisSpectrumInfoSelected(selectedNode);
						control.getModel().notifyDisplayDatalink(selectedNode);
						break;
					case CASSIS_SPECTRUM:
						cassisSpectrumSelected(selectedNode);
						control.getModel().notifyDisplayDatalink(selectedNode);
						break;
					case VOTABLE_WITH_URL:
						votableWithUrlSelected(selectedNode);
						control.getModel().notifyDisableTabDatalink();
						control.getModel().notifyDisplayMetadataOfVotableWithUrl((VotableWithUrl)selectedNode.getUserObject());
						break;
					case NONE:
					case UNKNOWN:
					default:
						// Do nothing.
						break;
					}
				}
			};
			tree.addTreeSelectionListener(treeSelectionListener);
			tree.setCellRenderer(new CassisSpectrumInfoCellRenderer());
			ToolTipManager.sharedInstance().registerComponent(tree);
			tree.setToggleClickCount(0);
			tree.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					int selRow = tree.getRowForLocation(e.getX(), e.getY());
					if (selRow != -1 && e.getClickCount() == 2 && control.getAction() != null) {
						control.getAction().run();
					}
				}
			});
		}
		return tree;
	}

	/**
	 * Create if not exists a JPanel which contains buttons to manage files and spectra.
	 *
	 * @return JPanel panelButton
	 */
	public JPanel getButtonsPanel() {
		if (panelButton == null) {
			panelButton = new JPanel(new GridBagLayout());
			panelButton.setBorder(new EmptyBorder(10, 0, 0, 0));
			GridBagConstraints c = new GridBagConstraints();
			c.gridy = 0;
			c.insets = new Insets(1, 0, 1, 0);
			c.fill = GridBagConstraints.HORIZONTAL;
			panelButton.add(getOpenLocalResourceButton(), c);
			c.gridy += 1;
			panelButton.add(getOpenUrlResourceButton(), c);
			c.gridy += 1;
			panelButton.add(getOpenAsciiResourceButton(), c);
			c.gridy += 1;
			c.insets = new Insets(0, 0, 5, 0);
			panelButton.add(getCloseResourceButton(), c);
		}
		return panelButton;
	}

	/**
	 * Create "Open local resource" button if necessary then return it.
	 *
	 * @return The "Open local resource" button.
	 */
	public JButton getOpenLocalResourceButton() {
		if (openLocalResourceButton == null) {
			openLocalResourceButton = new JButton("Open local resource");
			openLocalResourceButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					openJFileChooser();
				}
			});
		}
		return openLocalResourceButton;
	}

	public JButton getOpenAsciiResourceButton() {
		if (openAsciiResourceButton == null) {
			openAsciiResourceButton = new JButton("Open ASCII parser");
			openAsciiResourceButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
					if (node == null || !(node.getUserObject() instanceof CassisSpectrumInfo)) {
						control.openAsciiParser(null, true);
					} else {
						openParserWithDialog(node);
					}
				}
			});
		}
		return openAsciiResourceButton;
	}

	private void openParserWithDialog(DefaultMutableTreeNode node) {
		int answer = JOptionPane.showOptionDialog(
				CassisSpectrumPanel.this.getTopLevelAncestor(),
				"Would you like to open the selected resource ?",
				"Open ASCII Parser",
				JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE,
				null,
				new Object[] { "Open selected resource", "Open another resource" },
				null);
		if (answer == JOptionPane.YES_OPTION) {
			CassisSpectrumInfo spectInfo = (CassisSpectrumInfo) node.getUserObject();
			control.openAsciiParser(new File(spectInfo.getPath()), false);
		} else {
			control.openAsciiParser(null, true);
		}
	}

	/**
	 * Create closeResourceButton if not exists.
	 *
	 * @return JButton closeResourceButton
	 */
	public JButton getCloseResourceButton() {
		if (closeResourceButton == null) {
			closeResourceButton = new JButton("Close resource");
			closeResourceButton.setEnabled(false);
			closeResourceButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.getModel().notifyDisableAllButton();
					control.getModel().notifyDisableTabDatalink();
					closeResourceButton.setEnabled(false);
					DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
					if (selectedNode.getUserObject() instanceof CassisSpectrumInfo) {
						control.removeResourceFromJTree(tree);
					} else if (selectedNode.getUserObject() instanceof VotableWithUrl) {
						control.removeVotableWithUrlFromJTree(tree);
					}
				}
			});
		}
		return closeResourceButton;
	}

	/**
	 * Open JOptionPane warning that the selected resource is already open.
	 */
	public void openResourceIsOpenJOptionPane() {
		JOptionPane.showMessageDialog(this, "<html>You have selected resource which is already open.<br>" + "Please select another resource.</html>", "Resource already selected",
				JOptionPane.WARNING_MESSAGE);
	}

	/**
	 * Open JFileChooser to select file to open.
	 */
	public void openJFileChooser() {
		JFileChooser chooser = new JFileChooser();
		chooser.setFileFilter(new FileNameExtensionFilter("Class file", CassisFileFilter.extClass));
		chooser.setFileFilter(new FileNameExtensionFilter("Votable file", CassisFileFilter.extVotable));
		chooser.setFileFilter(new FileNameExtensionFilter("Other", "txt", "fca", "fus", "lis", "sam", "ltm", "lcm", "lbm", "lam", "tec"));
		chooser.setFileFilter(new FileNameExtensionFilter("Fits file", CassisFileFilter.extFits));

		chooser.setDialogTitle("Open resource");
		chooser.setMultiSelectionEnabled(true);
		int returnVal = chooser.showOpenDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File[] files = chooser.getSelectedFiles();
			control.addResourceToJTree(files);
		}
	}

	/**
	 * Open JOptionPane which warning that there is no resource selected.
	 */
	public void openResourceNotSelectedJOptionPane() {
		JOptionPane.showMessageDialog(this, "<html>You haven't selected resource.<br>Please select resource.</html>", "No selected resource", JOptionPane.WARNING_MESSAGE);
	}

	/**
	 * Open JOptionPane which warning that there is no cassis spectrum selected.
	 */
	public void openSelectCassisSpectrumJOptionPane() {
		JOptionPane.showMessageDialog(this, "<html>You haven't selected a Cassis Spectrum.<br>" + "Please select Cassis Spectrum or open resource.</html>", "No Cassis Spectrum selected",
				JOptionPane.WARNING_MESSAGE);
	}

	/**
	 * Open JOptionPane which warning that the selected file not exists.
	 */
	public void openResourceNotExistsJOptionPane() {
		JOptionPane.showMessageDialog(this, "<html>You have selected an inexisting file.<br>" + "Please select an existing file.</html>", "Inexisting file", JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * Open file info window according CassisSpectrum and CassisSpectrumInfo selected.
	 *
	 * @param cassisSpectrum
	 *            The selected CassisSpectrum.
	 * @param cassisSpectrumInfo
	 *            The cassisSpectrumInfo corresponding to the selected CassisSpectrum.
	 */
	public void openResourceInfo(CassisSpectrum cassisSpectrum, CassisSpectrumInfo cassisSpectrumInfo) {
		JFrame fif = new FileInfoFrame(cassisSpectrum, cassisSpectrumInfo);
		fif.setVisible(true);
	}

	/**
	 * Open {@link JOptionPane} for enter a access URL required for the votable file with URL inside
	 *
	 * @return the access URL
	 */
	public String openAccessUrlJOptionPane(String name, List<String> columnInfo) {
		columnInfo.add(0, VotableWithUrl.NOT_ACCESS_URL);
		Object[] columnInfoTab = columnInfo.toArray();
		String accessURL = (String) JOptionPane.showInputDialog(this, "Enter the access URL field for the " + name + " file", "Access URL", JOptionPane.QUESTION_MESSAGE, null, columnInfoTab, VotableWithUrl.NOT_ACCESS_URL);
		return accessURL;
	}

	/**
	 * Open {@link JOptionPane} which warning that the access URL is not found
	 *
	 */
	public void openAccessURLErrorJOptionPane() {
		JOptionPane.showMessageDialog(this, "Access URL not found", "Error", JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * Open {@link JOptionPane} which warning that the votable file with URL inside can't be read
	 */
	public void openVotableFileWithUrlErrorOptionPane() {
		JOptionPane.showMessageDialog(this, "Error this file can't be read", "Error", JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * Return the model.
	 *
	 * @return CassisSpectrumModel
	 */
	public CassisSpectrumModel getModel() {
		return control.getModel();
	}

	/**
	 * Return the controller.
	 *
	 * @return CassisSpectrumControl
	 */
	public CassisSpectrumControl getControl() {
		return control;
	}

	/**
	 * Create "Open URL resource" button if necessary then return it.
	 *
	 * @return The "Open URL resource" button.
	 */
	public JButton getOpenUrlResourceButton() {
		if (openUrlResourceButton == null) {
			openUrlResourceButton = new JButton("Open URL resource");
			openUrlResourceButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					final DownloadGui dg = new DownloadGui();
					Runnable r = new Runnable() {
						@Override
						public void run() {
							File[] files = new File[] { dg.getDownloadedFile() };
							control.addResourceToJTree(files);
						}
					};
					dg.setFinishAction(r);
					dg.setVisible(true);
				}
			});
		}
		return openUrlResourceButton;
	}

	/**
	 * Action to do once a {@link String} is selected on the JTree.
	 */
	private void stringSelected() {
		getModel().notifyRefreshTable();
		closeResourceButton.setEnabled(false);
		control.getModel().notifyDisableAllButton();
	}

	/**
	 * Action to do once a {@link CassisSpectrum} is selected on the JTree.
	 *
	 * @param selectedNode
	 *            The selected node.
	 */
	private void cassisSpectrumSelected(DefaultMutableTreeNode selectedNode) {
		getModel().notifyDisplayMetadata(selectedNode);
		closeResourceButton.setEnabled(false);
		control.getModel().notifyDisableButtonForCassisSpectrum();
	}

	/**
	 * Action to do once a {@link CassisSpectrumInfo} is selected on the JTree.
	 *
	 * @param selectedNode
	 *            The selected node.
	 */
	public void cassisSpectrumInfoSelected(DefaultMutableTreeNode selectedNode) {
		getModel().notifyRefreshTable();
		getModel().notifyCassisSpectrumInfoSelected(selectedNode);
		tree.expandPath(tree.getSelectionPath());
		closeResourceButton.setEnabled(true);
		if (selectedNode.isLeaf()) { // Only one spectrum in file
			control.getModel().notifyDisableButtonForCassisSpectrum();
		} else { // Several spectra in file
			control.getModel().notifyDisableButtonForCassisSpectrumInfo();
		}
	}

	/**
	 * Action do to once a download file is selected on the JTree
	 *
	 * @param selectedNode
	 *            The selected node
	 */
	public void downloadFileSelected(DefaultMutableTreeNode selectedNode) {
		getModel().notifyRefreshTable();
		getCloseResourceButton().setEnabled(false);
		control.getModel().notifyDisableAllButton();
		CassisSpectrumInfo cassisSpectrumInfo = (CassisSpectrumInfo) selectedNode.getUserObject();
		VotableWithUrl votableWithUrl = ((VotableWithUrl) (((DefaultMutableTreeNode) selectedNode.getParent()).getUserObject()));
		int index = votableWithUrl.getIndexOfCassisSpectrumInfo(cassisSpectrumInfo);
		if (votableWithUrl.getFilesDownloadState()[index] == VotableWithUrl.NOT_DOWNLOADED) {
			control.downloadFile(selectedNode, votableWithUrl, index);
		} else if (votableWithUrl.getFilesDownloadState()[index] == VotableWithUrl.DOWNLOADED) {
			tree.expandPath(tree.getSelectionPath());
			control.getModel().notifyCassisSpectrumInfoSelected(selectedNode);
			if (selectedNode.isLeaf()) { // Only one spectrum in file
				control.getModel().notifyDisableButtonForCassisSpectrum();
			} else { // Several spectra in file
				control.getModel().notifyDisableButtonForCassisSpectrumInfo();
			}
		}
	}

	/**
	 * Action to do once a {@link VotableWithUrl} is selected on the {@link JTree}
	 *
	 * @param selectedNode
	 *            The selected node on the {@link JTree}
	 */
	public void votableWithUrlSelected(DefaultMutableTreeNode selectedNode) {
		getModel().notifyRefreshTable();
		tree.expandPath(tree.getSelectionPath());
		control.getModel().notifyDisableAllButton();
		closeResourceButton.setEnabled(true);
	}

	/**
	 * Return the type of element selected in the JTree.
	 *
	 * @return the type of element selected in the JTree.
	 */
	public TypeElementSelected getTypeElementSelected() {
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
		if (node == null) {
			return TypeElementSelected.NONE;
		} else if (node.getUserObject() instanceof String) {
			return TypeElementSelected.STRING;
		} else if (control.isDownloadFile(node)) {
			return TypeElementSelected.DOWNLOAD_FILE;
		} else if (control.isCassisSpectrumInfo(node)) {
			return TypeElementSelected.CASSIS_SPECTRUM_INFO;
		} else if (control.isCassisSpectrum(node)) {
			return TypeElementSelected.CASSIS_SPECTRUM;
		} else if (control.isVotableWithURL(node)) {
			return TypeElementSelected.VOTABLE_WITH_URL;
		} else {
			return TypeElementSelected.UNKNOWN;
		}
	}

	/**
	 * Return the last selected element on the JTree.
	 *
	 * @return the last selected element on the JTree.
	 */
	public Object getSelected() {
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
		if (node == null) {
			return null;
		}
		return node.getUserObject();
	}

}
