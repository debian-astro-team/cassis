/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.cassisspectrum;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.events.ListenerManager;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;

/**
 * Create a model to CassisSpectrumPanel.
 *
 * @author Lea Foissac
 */
public class CassisSpectrumModel extends ListenerManager {

	public static final String DISPLAY_METADATA = "displayMetadata";
	public static final String ADD_SPECTRA = "addSpectra";
	public static final String DISPLAY_COMMON_METADATA = "displayCommonMetadata";
	public static final String REFRESH_TABLE = "refreshTable";
	public static final String DISABLE_ALL_BUTTON = "disableMetadataButton";
	public static final String DISABLE_BUTTON_SPECTRUM = "disableButtonForCassisSpectrum";
	public static final String DISABLE_BUTTON_SPECTRUM_INFO = "disableButtonForCassisSpectrumInfo";
	public static final String DISABLE_TAB_DATALINK = "disableTabDatalink";
	public static final String DISPLAY_DATALINK = "displayDatalink";
	public static final String DISPLAY_METADATA_OF_VOTABLE_WITH_URL = "displayMetadataOfVotableWithUrl";

	private DefaultTreeModel defaultTreeModel;
	private DefaultMutableTreeNode root;
	private List<CassisSpectrumInfo> cassisSpectrumInfoList;
	private List<VotableWithUrl> votableWithUrlList;
	private List<DefaultMutableTreeNode> votableWithUrlNodeList;

	/**
	 * Constructor Create a CassisSpectrumModel according to CassisSpectrumPanel and CassisSpectrumControl.
	 */
	public CassisSpectrumModel() {
		this.root = new DefaultMutableTreeNode("Resources");
		this.defaultTreeModel = new DefaultTreeModel(root);
		this.cassisSpectrumInfoList = new ArrayList<CassisSpectrumInfo>();
		this.votableWithUrlList = new ArrayList<VotableWithUrl>();
		this.votableWithUrlNodeList = new ArrayList<DefaultMutableTreeNode>();
	}

	/**
	 * Add a CassisSpectrumInfo to JTree.
	 *
	 * @param cassisSpectrumInfo
	 *            CassisSpectrumInfo to add to JTree
	 * @return the defaultMutableTreeNode added
	 */
	public DefaultMutableTreeNode addResource(CassisSpectrumInfo cassisSpectrumInfo) {
		DefaultMutableTreeNode treeNode = new DefaultMutableTreeNode(cassisSpectrumInfo);
		this.defaultTreeModel.insertNodeInto(treeNode, root, defaultTreeModel.getChildCount(root));
		this.cassisSpectrumInfoList.add(cassisSpectrumInfo);

		fireDataChanged(new ModelChangedEvent(ADD_SPECTRA, treeNode));
		return treeNode;
	}

	/**
	 * Add a {@link VotableWithUrl} to {@link JTree}
	 *
	 * @param votableUrlInfo
	 *            The {@link VotableWithUrl} to add on the {@link JTree}
	 */
	public void addVotableURLFile(VotableWithUrl votableUrlInfo) {
		votableWithUrlList.add(votableUrlInfo);
		DefaultMutableTreeNode fileNode = new DefaultMutableTreeNode(votableUrlInfo);
		votableWithUrlNodeList.add(fileNode);
		this.defaultTreeModel.insertNodeInto(fileNode, root, defaultTreeModel.getChildCount(root));
		for (int index : votableUrlInfo.getUrlIndexList()) {
			DefaultMutableTreeNode cassisSpectrumInfo = new DefaultMutableTreeNode(votableUrlInfo.getCassisSpectrumInfoTab()[index]);
			this.defaultTreeModel.insertNodeInto(cassisSpectrumInfo, fileNode, defaultTreeModel.getChildCount(fileNode));
		}
		notifyDisplayMetadataOfVotableWithUrl(votableUrlInfo);
	}

	/**
	 * Add the new index of file to a {@link VotableWithUrl} already on the JTree
	 *
	 * @param votableWithUrlOnJTree
	 *            The {@link VotableWithUrl} which is already on the JTree
	 * @param newUrlIndexList
	 *            The index of url in {@link VotableWithUrl} to add on the JTree
	 */
	public void addIndexToVotableURLFile(VotableWithUrl votableWithUrlOnJTree, List<Integer> newUrlIndexList) {
		for (DefaultMutableTreeNode fileNode : votableWithUrlNodeList) {
			if (((VotableWithUrl) fileNode.getUserObject()).equals(votableWithUrlOnJTree)) {
				// Recovering of node on JTree to add the new file
				List<Integer> oldList = votableWithUrlOnJTree.getUrlIndexList();
				List<Integer> addToJTree = new ArrayList<Integer>();
				for (Integer index : newUrlIndexList) { // Recover of index to add on the JTree
					if (!votableWithUrlOnJTree.getUrlIndexList().contains(index)) {
						addToJTree.add(index);
					}
				}
				for (int index : addToJTree) { // Adding of new file at the correct index
					for (int i = 0; i < votableWithUrlOnJTree.getFileNumber(); i++) {
						int oldIndex = oldList.get(i);
						if (index < oldList.get(oldList.size() - 1)) {
							if (index < oldList.get(0)) {
								DefaultMutableTreeNode cassisSpectrumInfo = new DefaultMutableTreeNode(votableWithUrlOnJTree.getCassisSpectrumInfoTab()[index]);
								this.defaultTreeModel.insertNodeInto(cassisSpectrumInfo, fileNode, 0);
								oldList.add(0, index);
								break;
							} else if (index > oldIndex && index < oldList.get(i + 1)) {
								DefaultMutableTreeNode cassisSpectrumInfo = new DefaultMutableTreeNode(votableWithUrlOnJTree.getCassisSpectrumInfoTab()[index]);
								this.defaultTreeModel.insertNodeInto(cassisSpectrumInfo, fileNode, i + 1);
								oldList.add(i + 1, index);
								break;
							}
						} else {
							DefaultMutableTreeNode cassisSpectrumInfo = new DefaultMutableTreeNode(votableWithUrlOnJTree.getCassisSpectrumInfoTab()[index]);
							this.defaultTreeModel.insertNodeInto(cassisSpectrumInfo, fileNode, oldList.size());
							oldList.add(oldList.size(), index);
							break;
						}

					}
				}
			}
		}
	}

	/**
	 * Add CassisSpectrumList to a DefaultMutableTreeNode in a JTree.
	 *
	 * @param node
	 *            DefaultMutableTreeNode which will be the parent of CassisSpectrumList
	 * @param cassisSpectrumList
	 *            List<CassisSpectrum> which add to the node parent
	 */
	public void addSpectra(DefaultMutableTreeNode node, List<CassisSpectrum> cassisSpectrumList) {
		for (CassisSpectrum cassisSpectrum : cassisSpectrumList) {
			DefaultMutableTreeNode spectrum = new DefaultMutableTreeNode(cassisSpectrum);
			defaultTreeModel.insertNodeInto(spectrum, node, defaultTreeModel.getChildCount(node));
		}
	}

	/**
	 * Notify that we want to display common metadata
	 *
	 * @param node
	 *            Selected node in a JTree in CassisSpectrumPanel
	 */
	public void notifyCassisSpectrumInfoSelected(DefaultMutableTreeNode node) {
		fireDataChanged(new ModelChangedEvent(DISPLAY_COMMON_METADATA, node));
	}

	/**
	 * Remove resource from the defaultListModel.
	 *
	 * @param node
	 *            Selected node in a JTree in CassisSpectrumPanel
	 */
	public void removeResource(DefaultMutableTreeNode node) {
		defaultTreeModel.removeNodeFromParent(node);
		cassisSpectrumInfoList.remove(node.getUserObject());
		notifyRefreshTable();
	}

	/**
	 * Notify to top controller that we want to display metadata.
	 *
	 * @param node
	 *            Selected node in a JTree in CassisSpectrumPanel
	 */
	public void notifyDisplayMetadata(DefaultMutableTreeNode node) {
		fireDataChanged(new ModelChangedEvent(DISPLAY_METADATA, node));
	}

	/**
	 * Notify to top controller that we want to refresh the table.
	 */
	public void notifyRefreshTable() {
		fireDataChanged(new ModelChangedEvent(REFRESH_TABLE));
	}

	/**
	 * Notify to top controller that we want to disable all button on MetadataPanel.
	 */
	public void notifyDisableAllButton() {
		fireDataChanged(new ModelChangedEvent(DISABLE_ALL_BUTTON));
	}

	/**
	 * Notify to top controller that we want to disable button when CassisSpectrum is selected.
	 */
	public void notifyDisableButtonForCassisSpectrum() {
		fireDataChanged(new ModelChangedEvent(DISABLE_BUTTON_SPECTRUM));
	}

	/**
	 * Notify to top controller that we want to disable button when CassisSpectrumInfo is selected.
	 */
	public void notifyDisableButtonForCassisSpectrumInfo() {
		fireDataChanged(new ModelChangedEvent(DISABLE_BUTTON_SPECTRUM_INFO));
	}

	public void notifyAddSpectra(DefaultMutableTreeNode node) {
		fireDataChanged(new ModelChangedEvent(ADD_SPECTRA, node));
	}

	/**
	 * Notify to the top controller that we want to disable the "datalink" tab
	 */
	public void notifyDisableTabDatalink() {
		fireDataChanged(new ModelChangedEvent(DISABLE_TAB_DATALINK));
	}

	/**
	 * Notify to top controller that we want to display datalink information according to the selected node on {@link JTree}
	 *
	 * @param node
	 *            The selected node on {@link JTree}
	 */
	public void notifyDisplayDatalink(DefaultMutableTreeNode node) {
		fireDataChanged(new ModelChangedEvent(DISPLAY_DATALINK, node));
	}

	public void notifyDisplayMetadataOfVotableWithUrl(VotableWithUrl votableWithUrl) {
		fireDataChanged(new ModelChangedEvent(DISPLAY_METADATA_OF_VOTABLE_WITH_URL, votableWithUrl));
	}

	/**
	 * Getter to DefaultTreeModel.
	 *
	 * @return the tree model
	 */
	public DefaultTreeModel getDefaultTreeModel() {
		return defaultTreeModel;
	}

	/**
	 * Getter to List<CassisSpectrumInfo>.
	 *
	 * @return the cassisSpectrumInfo list
	 */
	public List<CassisSpectrumInfo> getCassisSpectrumInfoList() {
		return cassisSpectrumInfoList;
	}

	/**
	 * Getter for {@link VotableWithUrl} list
	 *
	 * @return the {@link VotableWithUrl} list
	 */
	public List<VotableWithUrl> getVotableWithUrlList() {
		return votableWithUrlList;
	}

	/**
	 * Getter for the {@link DefaultMutableTreeNode} list containing {@link VotableWithUrl} object
	 *
	 * @return the {@link DefaultMutableTreeNode} list
	 */
	public List<DefaultMutableTreeNode> getVotableWithUrlNodeList() {
		return votableWithUrlNodeList;
	}

	/**
	 * Return if the given {@link CassisSpectrum} is open.
	 *
	 * @param cassisSpectrum
	 *            The {@link CassisSpectrum}.
	 * @return true of the given {@link CassisSpectrum} is open, false otherwise.
	 */
	public boolean isOpen(CassisSpectrum cassisSpectrum) {
		if (cassisSpectrum != null) {
			for (CassisSpectrumInfo csi : cassisSpectrumInfoList) {
				for (CassisSpectrum cs : csi.getCassisSpectrumList()) {
					if (cassisSpectrum.equals(cs)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Return if the path of the given {@link CassisSpectrum}.
	 *
	 * @param cassisSpectrum
	 *            The {@link CassisSpectrum}.
	 * @return the path of the given {@link CassisSpectrum} or null if not opened/found.
	 */
	public String getPath(CassisSpectrum cassisSpectrum) {
		if (cassisSpectrum != null) {
			for (CassisSpectrumInfo csi : cassisSpectrumInfoList) {
				for (CassisSpectrum cs : csi.getCassisSpectrumList()) {
					if (cassisSpectrum.equals(cs)) {
						return csi.getPath();
					}
				}
			}
		}
		return null;
	}

}
