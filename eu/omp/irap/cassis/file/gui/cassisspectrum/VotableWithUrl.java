/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.cassisspectrum;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.file.FileManagerVOTable;
import eu.omp.irap.cassis.file.gui.datalink.DatalinkModel;
import eu.omp.irap.cassis.file.votable.VotableType;
import uk.ac.starlink.table.ColumnInfo;
import uk.ac.starlink.table.StarTable;
import uk.ac.starlink.table.StarTableFactory;

/**
 * Create a {@link VotableWithUrl} which allows to read a VOTable file with URL and download the file at the URL
 *
 * @author Lea Foissac
 */
public class VotableWithUrl {

	public static final String ACCESS_URL = "access_url";
	public static final String ACCESS_FORMAT = "access_format";
	public static final String NOT_ACCESS_URL = "Not access url";

	public static final int NOT_DOWNLOADED = -1;
	public static final int DOWNLOAD_IN_PROGRESS = 0;
	public static final int DOWNLOADED = 1;

	private String name;
	private File file;
	private String path;
	private List<Integer> urlIndexList;
	private String accessUrl;
	private VotableType type;

	private CassisSpectrumInfo[] cassisSpectrumInfoTab;

	private StarTable starTable;

	private int[] filesDownloadState;
	private int fileNumber;
	private int columnNumber;
	private String[] columnInfo;
	private FieldNameDescription[] fieldsWithUrls;
	private DatalinkModel[] datalinkModelTab;
	private List<CassisMetadata> originalMetadataList;
	private List<List<CassisMetadata>> cassisMetadataForEachSpectrum;


	/**
	 * Create a {@link VotableWithUrl} using a VOtable {@link File}
	 *
	 * @param file
	 *            The VOTable file to read
	 * @throws IOException
	 *             I/O error or VOTable format problem is encountered
	 */
	public VotableWithUrl(File file) throws IOException {
		this(file, null, VotableType.UNKNOWN);
	}

	/**
	 * Create a {@link VotableWithUrl} using a VOTable {@link File}
	 *
	 * @param file
	 *            The VOTable file to read
	 * @param origin
	 *            The type of the file to force it
	 * @throws IOException
	 *             I/O error or VOTable format problem is encountered
	 */
	public VotableWithUrl(File file, VotableType origin) throws IOException {
		this(file, null, origin);
	}

	/**
	 * Create a {@link VotableWithUrl} using a VOTable {@link File} and {@link List}
	 *
	 * @param file
	 *            The VOTable file to read
	 * @param urlIndexList
	 *            The URL index in file to download
	 * @throws IOException
	 *             An I/O error is encountered
	 */
	public VotableWithUrl(File file, List<Integer> urlIndexList) throws IOException {
		this(file, urlIndexList, VotableType.UNKNOWN);
	}

	/**
	 * Create a {@link VotableWithUrl} using a VOTable {@link File} and {@link List}
	 *
	 * @param file
	 *            The VOTable file to read
	 * @param urlIndexList
	 *            The URL index in file to download
	 * @param type
	 *            The type of the file to force it
	 * @throws IOException
	 *             An I/O error is encountered
	 */
	public VotableWithUrl(File file, List<Integer> urlIndexList, VotableType type) throws IOException {
		this.file = file;
		this.name = file.getName();
		this.path = file.getAbsolutePath();
		this.starTable = new StarTableFactory().makeStarTable(file.getAbsolutePath(), "votable");
		this.fileNumber = (int) starTable.getRowCount();
		this.columnNumber = starTable.getColumnCount();
		this.cassisSpectrumInfoTab = new CassisSpectrumInfo[fileNumber];
		if (urlIndexList == null) {
			this.urlIndexList = new ArrayList<Integer>();
			for (int i = 0; i < fileNumber; i++) {
				this.urlIndexList.add(i);
			}
		} else {
			this.urlIndexList = urlIndexList;
		}
		this.type = type;
		computeAccessUrl();
		getFilesDownloadState();
	}

	/**
	 * Verify if the access URL is OK.
	 *
	 * @return True if the access URL is OK
	 * @throws IOException
	 */
	public boolean isAccessUrlOk() throws IOException {
		boolean accessUrlOk = false;
		if (type == VotableType.SSA) {
			accessUrlOk = accessUrl != null;
		} else {
			for (int i = 0; i < columnNumber; i++) {
				if (this.starTable.getColumnInfo(i).getName().equals(accessUrl)) {
					if (accessUrl.equals(ACCESS_URL)) {
						accessUrlOk = verifyAccessFormat();
					} else {
						accessUrlOk = true;
					}
					this.setAccessUrl(accessUrl);
					break;
				}
			}
		}
		return accessUrlOk;
	}

	private boolean verifyAccessFormat() throws IOException {
		boolean isAccessFormat = false;
		for (int i = 0; i < getFileNumber(); i++) {
			Object[] info = starTable.getRow(i);
			int j = 0;
			for (Object object : info) {
				if (this.starTable.getColumnInfo(j).getName().equals(ACCESS_FORMAT)) {
					if (object != null) {
						String accessFormat = object.toString();
						if (accessFormat.equalsIgnoreCase("fits") ||
								accessFormat.equalsIgnoreCase("votable") ||
								accessFormat.equalsIgnoreCase("application/fits")) {
							isAccessFormat = true;
							break;
						}
					}
				}
				j++;
			}
		}
		return isAccessFormat;
	}

	/**
	 * Research on votable file all names files
	 *
	 * @throws IOException
	 *             If there is an error reading the data
	 */
	public void recoverNameFile() throws IOException {
		/* Avoid duplication of the name. This can append with services from SSAP like POLLUX */
		List<String> names = new ArrayList<>(getFileNumber());
		for (int index = 0; index < getFileNumber(); index++) {
			CassisSpectrumInfo cassisSpectrumInfo = new CassisSpectrumInfo();
			this.cassisSpectrumInfoTab[index] = cassisSpectrumInfo;
			Object[] info = starTable.getRow(index);
			int j = 0;
			for (Object object : info) {
				if (starTable.getColumnInfo(j).getName().equals(getAccessUrl())) {
					if (object != null) {
						String[] tab = object.toString().split("/");
						String name = tab[tab.length - 1];
						if (names.contains(name)) {
							name = name + "_";
						} else {
							names.add(name);
						}
						cassisSpectrumInfo.setName(name);
					}
				}
				j++;
			}
		}
		this.setCassisSpectrumInfoTab(cassisSpectrumInfoTab);
	}

	/**
	 * Getter for knowing the state of the download of the files. There is three
	 * states for them:<br/>
	 * - {@link VotableWithUrl#NOT_DOWNLOADED}: the file is not downloaded and not started.<br/>
	 * - {@link VotableWithUrl#DOWNLOAD_IN_PROGRESS}: the download is in progress.<br/>
	 * - {@link VotableWithUrl#DOWNLOADED}: The file is downloaded.
	 *
	 * @return the state of the download of the files.
	 */
	public int[] getFilesDownloadState() {
		if (filesDownloadState == null) {
			this.filesDownloadState = new int[fileNumber];
			for (int i = 0; i < fileNumber; i++) {
				this.filesDownloadState[i] = NOT_DOWNLOADED;
			}
		}
		return filesDownloadState;
	}

	/**
	 * Getter for the index of {@link CassisSpectrumInfo} in the CassisSpectrumInfo array
	 *
	 * @param cassisSpectrumInfo
	 *            The {@link CassisSpectrumInfo} object that we want to know the index in CassisSpectrumInfo array
	 * @return the index of the given {@link CassisSpectrumInfo} in the CassisSpectrumInfo array
	 */
	public int getIndexOfCassisSpectrumInfo(CassisSpectrumInfo cassisSpectrumInfo) {
		int index = -1;
		for (int i = 0; i < getFileNumber(); i++) {
			if (cassisSpectrumInfo.equals(getCassisSpectrumInfoTab()[i])) {
				index = i;
				break;
			}
		}
		return index;
	}

	/**
	 * Getter for the file url according to the index file
	 *
	 * @param index
	 *            The index of url
	 * @return The URL according to index of url in votable file
	 */
	public String getUrl(int index) {
		String url = "";
		try {
			Object[] info = starTable.getRow(index);
			int j = 0;
			for (Object object : info) {
				if (starTable.getColumnInfo(j).getName().equals(getAccessUrl())) {
					if (object != null)
						url = object.toString();
				}
				j++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return url;
	}

	/**
	 * Getter for the votable file
	 *
	 * @return The votable file
	 */
	public File getFile() {
		return this.file;
	}

	/**
	 * Getter for the votable name file
	 *
	 * @return The votable name file
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Getter for the url index list. Permit to know how url in file must be downloaded
	 *
	 * @return the url index list
	 */
	public List<Integer> getUrlIndexList() {
		return this.urlIndexList;
	}

	/**
	 * Getter for the {@link CassisSpectrumInfo} array
	 *
	 * @return the {@link CassisSpectrumInfo} array
	 */
	public CassisSpectrumInfo[] getCassisSpectrumInfoTab() {
		return this.cassisSpectrumInfoTab;
	}

	/**
	 * Getter for the access url in votable file
	 *
	 * @return The access url
	 */
	public String getAccessUrl() {
		return this.accessUrl;
	}

	/**
	 * Getter for the number of columns in votable file
	 *
	 * @return the columns number in votable file
	 */
	public int getColumnNumber() {
		return this.columnNumber;
	}

	/**
	 * Getter for the number of files containing in the votable
	 *
	 * @return the files number
	 */
	public int getFileNumber() {
		return this.fileNumber;
	}

	/**
	 * Setter to change the accessUrl
	 *
	 * @param accessUrl
	 *            The new access url
	 */
	public void setAccessUrl(String accessUrl) {
		this.accessUrl = accessUrl;
	}

	/**
	 * Setter to change the {@link CassisSpectrumInfo} array
	 *
	 * @param cassisSpectrumInfoList
	 *            The new {@link CassisSpectrumInfo} array
	 */
	private void setCassisSpectrumInfoTab(CassisSpectrumInfo[] cassisSpectrumInfoList) {
		this.cassisSpectrumInfoTab = cassisSpectrumInfoList;
	}

	/**
	 * Return the names of the fields of the VOTable file.
	 *
	 * @return the names of the fields.
	 */
	public String[] getColumnInfo() {
		if (columnInfo == null) {
			columnInfo = new String[columnNumber];
			for (int i = 0; i < columnNumber; i++) {
				columnInfo[i] = starTable.getColumnInfo(i).getName();
			}
		}
		return columnInfo;
	}

	/**
	 * Getter for the path file
	 *
	 * @return The path file
	 */
	public String getPath() {
		return path;
	}

	/**
	 * Getter for the datalink model
	 *
	 * @return the datalink model array or null if file doesn't contain datalink
	 */
	public DatalinkModel[] getDatalinkModelTab() {
		if (FileManagerVOTable.containDatalink(path) && datalinkModelTab == null) {
			datalinkModelTab = new DatalinkModel[getFileNumber()];
			for (int i = 0; i < getFileNumber(); i++) {
				datalinkModelTab[i] = new DatalinkModel(path, i);
			}
		}
		return datalinkModelTab;
	}

	public List<CassisMetadata> getOriginalMetadataList() {
		if (originalMetadataList == null) {
			FileManagerVOTable fileManagerVOTable = new FileManagerVOTable(file);
			originalMetadataList = fileManagerVOTable.transformParametersListToCassisMetadataList(starTable.getParameters());
		}
		return originalMetadataList;
	}

	/**
	 * Getter for the votable metadata of spectrum
	 *
	 * @return the votable metadata of spectrum
	 */
	public List<List<CassisMetadata>> getCassisMetadataForEachSpectrum() {
		if (cassisMetadataForEachSpectrum == null) {
			cassisMetadataForEachSpectrum = new ArrayList<List<CassisMetadata>>();
			for (int i = 0; i < getFileNumber(); i++) {
				List<CassisMetadata> cassisMetadataList = new ArrayList<CassisMetadata>();
				try {
					Object[] info = starTable.getRow(i);
					int j = 0;
					for (Object object : info) {
						if (object != null)
							cassisMetadataList.add(new CassisMetadata(
									starTable.getColumnInfo(j).getName(),
									object.toString(),
									starTable.getColumnInfo(j).getDescription(),
									starTable.getColumnInfo(j).getUnitString()));
						j++;
					}
					cassisMetadataForEachSpectrum.add(cassisMetadataList);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return cassisMetadataForEachSpectrum;
	}

	/**
	 * Parse the file and return the name of the column who contains the URL of
	 *  the files to download.
	 *
	 * @return the name of the column who contains the URL of the files to download.
	 *  If the column is not found, return null.
	 */
	public String getSsaAccessUrl() {
		ColumnInfo ci = null;
		String res = null;

		for (int i = 0; res == null && i < starTable.getColumnCount(); i++) {
			ci = starTable.getColumnInfo(i);
			if ("ssa:access.reference".equalsIgnoreCase(ci.getUtype()) &&
				"DATA_LINK".equals(ci.getName()) &&
				"meta.ref.url".equals(ci.getUCD())) {
				res = ci.getName();
			}
		}

		for (int i = 0; res ==null && i < starTable.getColumnCount(); i++) {
			ci = starTable.getColumnInfo(i);
			if ("ssa:access.reference".equalsIgnoreCase(ci.getUtype()) ||
					"DATA_LINK".equals(ci.getUCD()) ||
					"meta.ref.url".equals(ci.getUCD())) {
				res = ci.getName();
			}
		}
		return res;
	}

	/**
	 * Compute the column name to use to download the files.
	 */
	private void computeAccessUrl() {
		if (type == VotableType.SSA) {
			accessUrl = getSsaAccessUrl();
		} else {
			accessUrl = NOT_ACCESS_URL;
		}
	}

	/**
	 * Return the fields with URLs.
	 *
	 * @return the fields with URLs.
	 */
	public FieldNameDescription[] getFieldsWithUrls() {
		if (fieldsWithUrls == null) {
			List<FieldNameDescription> fieldsWithUrlsList = new ArrayList<>();
			for (int i = 0; i < columnNumber; i++) {
				ColumnInfo ci = starTable.getColumnInfo(i);
				if (ci.getUCD() != null && ci.getUCD().toLowerCase().contains("meta.ref.url")) {
					fieldsWithUrlsList.add(new FieldNameDescription(ci.getName(), ci.getDescription()));
				}
			}
			fieldsWithUrls = fieldsWithUrlsList.toArray(new FieldNameDescription[fieldsWithUrlsList.size()]);
		}
		return fieldsWithUrls;
	}

	/**
	 * Get the type of VOTable.
	 *
	 * @return the type of VOTable.
	 */
	public VotableType getType() {
		return type;
	}

	/**
	 * Change the type of VOTable.
	 *
	 * @param type The type to set.
	 */
	public void setType(VotableType type) {
		this.type = type;
	}
}
