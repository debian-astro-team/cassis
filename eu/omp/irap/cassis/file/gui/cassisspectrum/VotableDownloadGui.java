/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.cassisspectrum;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

/**
 * Create a frame to download file containing in {@link VotableWithUrl} file
 *
 * @author Lea Foissac
 *
 */
public class VotableDownloadGui extends JFrame implements PropertyChangeListener {

	private static final long serialVersionUID = 3540790270429877990L;

	private JProgressBar progressBar;
	private JButton cancelButton;

	private int index;
	private VotableWithUrl votableWithUrl;
	private VotableDownloadWorker votableDownloadWorker;
	private boolean isDownloading;
	private Runnable finishAction;

	/**
	 * Create a {@link VotableDownloadGui} using a {@link VotableWithUrl} and the file index to download
	 *
	 * @param votableWithUrl
	 *            The {@link VotableWithUrl} containing file to download
	 * @param index
	 *            Index of file to download
	 */
	public VotableDownloadGui(VotableWithUrl votableWithUrl, int index) {
		super("File download");
		initialize();
		isDownloading = false;
		this.votableWithUrl = votableWithUrl;
		this.index = index;
		this.votableDownloadWorker = new VotableDownloadWorker(this, index);
		addWindowsListener();
		startDownload();
	}

	/**
	 * Initialize GUI
	 */
	public void initialize() {
		JPanel panel = new JPanel(new BorderLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

		JPanel progressBarPanel = new JPanel(new BorderLayout());
		progressBarPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 20, 0));
		JLabel label = new JLabel("Downloading ...");
		progressBarPanel.add(label, BorderLayout.LINE_START);
		progressBarPanel.add(getProgressBar(), BorderLayout.AFTER_LAST_LINE);

		panel.add(progressBarPanel, BorderLayout.CENTER);
		panel.add(getCancelButton(), BorderLayout.SOUTH);

		setContentPane(panel);
		pack();
		setVisible(true);
		setLocationRelativeTo(null);
	}

	/**
	 * Getter for the progress bar. Create if not exists the progress bar.
	 *
	 * @return The progress bar
	 */
	private JProgressBar getProgressBar() {
		if (progressBar == null) {
			progressBar = new JProgressBar(0, 100);
			progressBar.setVisible(true);
			progressBar.setPreferredSize(new Dimension(200, 30));
			progressBar.setStringPainted(true);
			progressBar.setIndeterminate(true);
		}
		return progressBar;
	}

	/**
	 * Getter for the cancel button. Create if not exists the cancel button.
	 *
	 * @return The cancel button
	 */
	private JButton getCancelButton() {
		if (cancelButton == null) {
			cancelButton = new JButton("Cancel");
			cancelButton.setEnabled(true);
			cancelButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					cancelDownload();
				}
			});
		}
		return cancelButton;
	}

	/**
	 * Cancel the file download
	 */
	private void cancelDownload() {
		votableWithUrl.getFilesDownloadState()[getIndex()] = VotableWithUrl.NOT_DOWNLOADED;
		votableDownloadWorker.cancel(true);
		votableDownloadWorker.removePropertyChangeListener(this);
		this.dispose();
	}

	/**
	 * Action when user click on the cancel cross
	 */
	private void addWindowsListener() {
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent windowEvent) {
				if (isDownloading()) {
					cancelDownload();
				} else {
					VotableDownloadGui.this.dispose();
				}
			}
		});
	}

	/**
	 * Start the file download
	 */
	private void startDownload() {
		if (!isDownloading) {
			getProgressBar().setValue(0);
			votableDownloadWorker.addPropertyChangeListener(this);
			votableDownloadWorker.execute();
		}
	}

	/**
	 * Action to do once the download is over
	 *
	 * @param path
	 *            The path of the downloaded file
	 */
	private void downloadFinished(String path) {
		if (finishAction != null) {
			finishAction.run();
		}
		dispose();
	}

	/**
	 * Set an action to do once the file download is finished
	 *
	 * @param endAction
	 *            The action to do once the file download is finished
	 */
	public void setFinishAction(Runnable endAction) {
		this.finishAction = endAction;
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals("progress")) {
			int progress = (Integer) evt.getNewValue();
			getProgressBar().setIndeterminate(false);
			getProgressBar().setValue(progress);
		} else if (evt.getPropertyName().equals("state") && (SwingWorker.StateValue) evt.getNewValue() == SwingWorker.StateValue.DONE) {
			String path = votableDownloadWorker.getLocalPath();
			downloadFinished(path);
		} else if ((SwingWorker.StateValue) evt.getNewValue() == SwingWorker.StateValue.STARTED) {
			isDownloading = true;
		}
	}

	/**
	 * Getter to know if the download file is in progress
	 *
	 * @return True if the download file is in progress and false if not
	 */
	private boolean isDownloading() {
		return isDownloading;
	}

	/**
	 * Getter for the file votable with url
	 *
	 * @return the file votable with url
	 */
	public VotableWithUrl getVotableWithUrl() {
		return votableWithUrl;
	}

	/**
	 * Getter for the local path of file
	 *
	 * @return the local path
	 */
	public String getLocalPathFile() {
		return votableDownloadWorker.getLocalPath();
	}

	/**
	 * Getter for the index of file to download on the votable file
	 *
	 * @return the index of file to download
	 */
	public int getIndex() {
		return index;
	}

}
