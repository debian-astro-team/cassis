/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui;

import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.YAxisCassis;

/**
 * Simple class to store informations on a column (name, unit and index).
 *
 * @author M. Boiziot
 */
public class ColumnInformation {

	private final String name;
	private final String unit;
	private final Integer index;
	private final boolean errorUnit;


	/**
	 * Constructor.
	 *
	 * @param name The name of the column.
	 * @param unit The unit of the value in the column.
	 * @param errorUnit
	 * @param index The index of the column.
	 */
	public ColumnInformation(String name, String unit, boolean errorUnit, Integer index) {
		this.name = name;
		this.unit = unit;
		this.errorUnit = errorUnit;
		this.index = index;
	}

	/**
	 * Return the name of the column.
	 *
	 * @return the name of the column.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Return the raw unit as a String.
	 *
	 * @return the raw unit as a String.
	 */
	public String getUnitString() {
		return unit;
	}


	/**
	 * Return the index.
	 *
	 * @return the index.
	 */
	public Integer getIndex() {
		return index;
	}

	/**
	 * Return a {@link XAxisCassis} object corresponding to the unit.
	 *
	 * @return a {@link XAxisCassis} object corresponding to the unit.
	 */
	public XAxisCassis getXAxisCassis() {
		return XAxisCassis.getXAxisCassis(unit);
	}

	/**
	 * Return a {@link YAxisCassis} object corresponding to the unit.
	 *
	 * @return a {@link YAxisCassis} object corresponding to the unit.
	 */
	public YAxisCassis getYAxisCassis() {
		return YAxisCassis.getYAxisCassis(unit, name);
	}

	public boolean isErrorUnit() {
		return errorUnit;
	}

}
