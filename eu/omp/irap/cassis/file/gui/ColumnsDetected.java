/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui;

import java.util.List;

/**
 * This object contains information about different column of data read in a resource
 *
 * @author Lea Foissac
 *
 */
public class ColumnsDetected {

	private String columnUsed;
	private List<ColumnInformation> columnsDetectedList;
	private String[] columnsDetected;
	private int indexOfColumnUsed;


	/**
	 * Create a {@link ColumnsDetected} containing information about different column of data read in a resource
	 *
	 * @param columnUsed
	 *            The name of the column used to read data in a resource
	 * @param columnsDetectedList
	 *            Columns name and index find on the resource
	 */
	public ColumnsDetected(String columnUsed, List<ColumnInformation> columnsDetectedList) {
		this.columnUsed = columnUsed;
		this.columnsDetectedList = columnsDetectedList;
	}

	/**
	 * Getter for the name of column used to read data on a resource
	 *
	 * @return The name of column used to read data
	 */
	public String getColumnUsed() {
		return columnUsed;
	}

	/**
	 * Getter for the list of columns detected.
	 *
	 * @return the list of columns detected.
	 */
	public List<ColumnInformation> getColumnsDetectedList() {
		return columnsDetectedList;
	}

	/**
	 * Return if the list of columns detected is empty.
	 *
	 * @return true if the list of columns detected is empty, false otherwise.
	 */
	public boolean isColumnsDetectedListEmpty() {
		return columnsDetectedList.isEmpty();
	}

	/**
	 * Check then return if the list of columns detected contains a column with
	 *  the given name.
	 *
	 * @param name The name of the searched column.
	 * @return true if the list of columns detected contains a column with the
	 *  given name, false otherwise.
	 */
	public boolean containsColumn(String name) {
		return getColumn(name) != null;
	}

	/**
	 * Return the column with the given name.
	 *
	 * @param name The name of the searched column.
	 * @return the column with the given name or null if not found.
	 */
	public ColumnInformation getColumn(String name) {
		for (ColumnInformation ci : columnsDetectedList) {
			if (ci.getName().equals(name)) {
				return ci;
			}
		}
		return null;
	}

	/**
	 * Setter for the column name used to read data
	 *
	 * @param columnUsed
	 *            The new name of the column used to read data
	 */
	public void setColumnUsed(String columnUsed) {
		this.columnUsed = columnUsed;
	}

	/**
	 * Return columns detected on resource.
	 * Put the column used to read data on the top of array.
	 *
	 * @return Columns detected on resource
	 */
	public String[] getColumnsDetected() {
		columnsDetected = new String[getColumnsDetectedList().size()];
		columnsDetected[0] = getColumnUsed();
		int i = 1;
		for (ColumnInformation ci : getColumnsDetectedList()) {
			if (!ci.getName().equals(getColumnUsed())) {
				columnsDetected[i] = ci.getName();
				i++;
			}
		}
		return columnsDetected;
	}

	/**
	 * Return the index of column used to read data
	 *
	 * @return The index of column used to read data
	 */
	public int indexOfColumnUsed() {
		indexOfColumnUsed = -1;
		for (ColumnInformation ci : getColumnsDetectedList()) {
			if (ci.getName().equals(getColumnUsed())) {
				indexOfColumnUsed = ci.getIndex();
				break;
			}
		}
		return indexOfColumnUsed;
	}

}
