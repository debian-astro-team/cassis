/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.common.axes.YAxisGeneric;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.file.FileManager;
import eu.omp.irap.cassis.file.FileReaderCassis;
import eu.omp.irap.cassis.file.InterfaceFileManager;
import eu.omp.irap.cassis.file.gui.cassisspectrum.CassisSpectrumControl;
import eu.omp.irap.cassis.file.gui.cassisspectrum.CassisSpectrumInfo;
import eu.omp.irap.cassis.file.gui.cassisspectrum.CassisSpectrumModel;
import eu.omp.irap.cassis.file.gui.cassisspectrum.VotableWithUrl;
import eu.omp.irap.cassis.file.gui.datalink.DatalinkModel;
import eu.omp.irap.cassis.file.gui.datalink.DatalinkPanel;
import eu.omp.irap.cassis.file.gui.fileinfo.FileInfoFrame;
import eu.omp.irap.cassis.file.gui.medatada.AddMetadataFrame;
import eu.omp.irap.cassis.file.gui.medatada.CASSIS_METADATA;
import eu.omp.irap.cassis.file.gui.medatada.CassisMetadataModel;
import eu.omp.irap.cassis.file.gui.medatada.CassisMetadataPanel;
import eu.omp.irap.cassis.file.gui.medatada.CassisMetadataTableModel;
import eu.omp.irap.cassis.file.gui.medatada.MetadataPanel;
import eu.omp.irap.cassis.file.gui.medatada.OriginalMetadataPanel;

/**
 * Create controller to interact with the panel (FilePanel class) and the model (FileModel class).
 *
 * @author Lea Foissac
 */
public class FileControl implements ModelListener {

	private FilePanel panel;
	private FileModel model;

	/**
	 * Constructor Create FileControl using FilePanel and FileModel.
	 *
	 * @param panel
	 *            The panel of FileControl
	 * @param model
	 *            The model of FileControl
	 */
	public FileControl(FilePanel panel, FileModel model) {
		this.panel = panel;
		this.model = model;
		this.model.addModelListener(this);
		this.model.getCassisSpectrumModel().addModelListener(this);
		this.panel.getCassisMetadataPanel().getControl().getModel().addModelListener(this);
	}

	/**
	 * Getter to FilePanel.
	 *
	 * @return the panel FilePanel
	 */
	public FilePanel getPanel() {
		return panel;
	}

	/**
	 * Getter to FileModel.
	 *
	 * @return the model FileModel
	 */
	public FileModel getModel() {
		return model;
	}

	@Override
	public void dataChanged(ModelChangedEvent modelChangedEvent) {
		Object objectSource = modelChangedEvent.getSource();
		Object objectValue = modelChangedEvent.getValue();
		if (objectSource.equals(CassisSpectrumModel.DISPLAY_COMMON_METADATA)) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) objectValue;
			displayCommonMetadata(node);
		} else if (objectSource.equals(CassisSpectrumModel.ADD_SPECTRA)) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) objectValue;
			addSpectra(node);
		} else if (objectSource.equals(CassisSpectrumModel.DISPLAY_METADATA)) {
			handleDisplayMetadataEvent(objectValue);
		} else if (objectSource.equals(CassisSpectrumModel.REFRESH_TABLE)) {
			panel.getMetadataPanel().getOriginalMetadataControl().getModel().getTableModel().refresh();
			panel.getMetadataPanel().getCassisMetadataControl().getModel().getTableModel().refresh();
		} else if (objectSource.equals(CassisSpectrumModel.DISABLE_ALL_BUTTON)) {
			updateButtonsState(false, false, false, false);
		} else if (objectSource.equals(CassisSpectrumModel.DISABLE_BUTTON_SPECTRUM)) {
			updateButtonsState(true, false, false, true);
		} else if (objectSource.equals(CassisSpectrumModel.DISABLE_BUTTON_SPECTRUM_INFO)) {
			updateButtonsState(false, false, false, false);
		} else if (objectSource.equals(CassisMetadataModel.DELETE_ROW)) {
			int row = (Integer) objectValue;
			deleteRow(row);
		} else if (objectSource.equals(CassisMetadataModel.OPEN_SPECTRUM_INFO)) {
			openSpectrumInfo();
		} else if (objectSource.equals(CassisMetadataModel.ADD_METADATA)) {
			openAddMetadataFrame();
		} else if (objectSource.equals(CassisSpectrumModel.DISABLE_TAB_DATALINK)) {
			disableTabDatalink();
		} else if (objectSource.equals(CassisSpectrumModel.DISPLAY_DATALINK)) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) objectValue;
			displayDatalink(node);
		} else if (objectSource.equals(CassisSpectrumModel.DISPLAY_METADATA_OF_VOTABLE_WITH_URL)) {
			List<CassisMetadata> cassisMetadataList = ((VotableWithUrl) objectValue).getOriginalMetadataList();
			displayMetadataOfVotableWithUrl(cassisMetadataList);
		} else if (objectSource.equals(CassisMetadataModel.WAVE_COLUMN_CHANGE) ||
				objectSource.equals(CassisMetadataModel.FLUX_COLUMN_CHANGE)) {
			handleWaveOrFluxColumnChange(objectSource, objectValue);
		} else if (objectSource.equals(CassisMetadataModel.FLUX_UNIT_CHANGE)) {
			YAxisCassis yAxis = getCassisSpectrumSelected().getYAxis();
			String information = "";
			if (yAxis instanceof YAxisGeneric) {
				information = yAxis.getInformationName();
			}
			YAxisCassis yAxisCassis = YAxisCassis.getYAxisCassis((String)objectValue);
			getCassisSpectrumSelected().setYAxis(yAxisCassis);
			if (yAxisCassis instanceof YAxisGeneric) {
				yAxisCassis.setInformationName(information);
			}
			getCassisSpectrumSelected().setyError(false);
		} else if (objectSource.equals(CassisMetadataModel.WAVE_UNIT_CHANGE)) {
			getCassisSpectrumSelected().setXAxisConv(XAxisCassis.getXAxisCassis((String)objectValue));
			getCassisSpectrumSelected().setxError(false);
		} else if (objectSource.equals(CassisMetadataModel.FLUX_AXIS_NAME_CHANGE)) {
			getCassisSpectrumSelected().getYAxis().setInformationName((String)objectValue);
		}
	}

	/**
	 * Handle DISPLAY_METADATA event.
	 *
	 * @param objectValue The node.
	 */
	private void handleDisplayMetadataEvent(Object objectValue) {
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) objectValue;
		CassisSpectrum cassisSpectrum = (CassisSpectrum) node.getUserObject();
		CassisSpectrumInfo cassisSpectrumInfo = (CassisSpectrumInfo) ((DefaultMutableTreeNode)node.getParent()).getUserObject();
		int index = indexOfSpectrum(cassisSpectrum, cassisSpectrumInfo.getCassisSpectrumList());
		ColumnsDetected xColumnsDetected = null;
		ColumnsDetected yColumnsDetected = null;
		if (isColumnsDetected(cassisSpectrumInfo.getXColumnsDetected(), index)) {
			xColumnsDetected = cassisSpectrumInfo.getXColumnsDetected().get(index);
		}
		if (isColumnsDetected(cassisSpectrumInfo.getYColumnsDetected(), index)) {
			yColumnsDetected = cassisSpectrumInfo.getYColumnsDetected().get(index);
		}
		displayMetadata(cassisSpectrum, xColumnsDetected, yColumnsDetected);
	}

	/**
	 * Handle a wave or flux column change.
	 *
	 * @param objectSource The source.
	 * @param objectValue The value.
	 */
	private void handleWaveOrFluxColumnChange(Object objectSource, Object objectValue) {
		TreePath selectionPath = panel.getCassisSpectrumPanel().getTree().getSelectionPath();
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) selectionPath.getLastPathComponent();
		if (node.getUserObject() instanceof CassisSpectrum &&
				((DefaultMutableTreeNode) node.getParent()).getUserObject() instanceof CassisSpectrumInfo){
			CassisSpectrumInfo spectrumInfo = (CassisSpectrumInfo)(((DefaultMutableTreeNode) node.getParent()).getUserObject());
			CassisSpectrum csi = (CassisSpectrum) node.getUserObject();
			String waveColumn = objectSource.equals(CassisMetadataModel.WAVE_COLUMN_CHANGE) ?
					objectValue.toString() : csi.getCassisMetadata(
							CASSIS_METADATA.WAVE_COLUMNS_DETECTED.getName()).getValue();
			String fluxColumn = objectSource.equals(CassisMetadataModel.FLUX_COLUMN_CHANGE) ?
					objectValue.toString() : csi.getCassisMetadata(
							CASSIS_METADATA.FLUX_COLUMNS_DETECTED.getName()).getValue();
			List<CassisMetadata> additionMetadatas = new ArrayList<CassisMetadata>();
			additionMetadatas.addAll(csi.getOriginalMetadataList());
			CassisMetadata.replace(FileManager.WAVEAXIS_KEYWORD,
					waveColumn, additionMetadatas);
			CassisMetadata.replace(FileManager.FLUXAXIS_KEYWORD,
					fluxColumn, additionMetadatas);


			FileReaderCassis fileReaderCassis = new FileReaderCassis();
			final Integer index = Integer.valueOf(csi.getCassisMetadata(
					CASSIS_METADATA.SPECTRUM_INDEX.getName()).getValue());
			CassisSpectrum cassisSpectrum = fileReaderCassis.createCassisSpectrumListFromFile(
					new File(spectrumInfo.getPath()), additionMetadatas).get(index);

			spectrumInfo.getXColumnsDetected().get(index).setColumnUsed(waveColumn);
			spectrumInfo.getYColumnsDetected().get(index).setColumnUsed(fluxColumn);
			addPrimaryCassisMetadata(cassisSpectrum, spectrumInfo, index,
					spectrumInfo.getXColumnsDetected(), spectrumInfo.getYColumnsDetected());
			node.setUserObject(cassisSpectrum);

		} else if (node.getUserObject() instanceof CassisSpectrumInfo){
					CassisSpectrumInfo csi = (CassisSpectrumInfo) node.getUserObject();
			String waveColumn = objectSource.equals(CassisMetadataModel.WAVE_COLUMN_CHANGE) ?
					objectValue.toString() : csi.getCassisSpectrumList().get(0).getCassisMetadata(
							CASSIS_METADATA.WAVE_COLUMNS_DETECTED.getName()).getValue();
			String fluxColumn = objectSource.equals(CassisMetadataModel.FLUX_COLUMN_CHANGE) ?
					objectValue.toString() : csi.getCassisSpectrumList().get(0).getCassisMetadata(
							CASSIS_METADATA.FLUX_COLUMNS_DETECTED.getName()).getValue();
			List<CassisMetadata> metadataList = new ArrayList<CassisMetadata>();
			metadataList.add(new CassisMetadata(FileManager.WAVEAXIS_KEYWORD, waveColumn, null, null));
			metadataList.add(new CassisMetadata(FileManager.FLUXAXIS_KEYWORD, fluxColumn, null, null));
			replaceSpectrum(node, metadataList);
		}

	}

	/**
	 * Display metadata of a CassisSpectrum.
	 *
	 * @param cassisSpectrum
	 *            The CassisSpectrum.
	 */
	private void displayMetadata(CassisSpectrum cassisSpectrum,
			ColumnsDetected xColumnsDetected, ColumnsDetected yColumnsDetected) {
		OriginalMetadataPanel originalMetadataPanel = panel.getMetadataPanel()
				.getOriginalMetadataPanel();
		originalMetadataPanel.getScroll().getVerticalScrollBar().setValue(0);
		originalMetadataPanel.getModel().getTableModel().refresh();
		originalMetadataPanel.getModel().getTableModel()
				.addMetadataList(cassisSpectrum.getOriginalMetadataList());

		updateDisplayOfCassisMetadata(cassisSpectrum, xColumnsDetected, yColumnsDetected);
	}

	/**
	 * Update display of metadata on the "Cassis Metadata" pane
	 *
	 * @param cassisSpectrum
	 *            {@link CassisSpectrum} selected on the {@link JTree}
	 * @param xColumnsDetected
	 *            The x columns of data detected
	 * @param yColumnsDetected
	 *            The y columns of data detected
	 */
	private void updateDisplayOfCassisMetadata(CassisSpectrum cassisSpectrum,
			ColumnsDetected xColumnsDetected, ColumnsDetected yColumnsDetected) {
		CassisMetadataPanel cassisMetadataPanel = panel.getMetadataPanel().getCassisMetadataPanel();
		cassisMetadataPanel.getScroll().getVerticalScrollBar().setValue(0);
		CassisMetadataTableModel<CassisMetadata> tableModel = cassisMetadataPanel.getModel()
				.getTableModel();
		tableModel.refresh();
		tableModel.addMetadataList(cassisSpectrum.getCassisMetadataList());
		if (xColumnsDetected != null) {
			tableModel.setWaveColumnDetected(xColumnsDetected);
		}
		if (yColumnsDetected != null) {
			tableModel.setFluxColumnDetected(yColumnsDetected);
		}
	}

	/**
	 * Delete a row.
	 *
	 * @param row
	 *            The row to delete.
	 */
	private void deleteRow(int row) {
		TreePath selectionPath = panel.getCassisSpectrumPanel().getTree().getSelectionPath();
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) selectionPath.getLastPathComponent();
		if (((DefaultMutableTreeNode) node.getParent()).isRoot()
				|| ((DefaultMutableTreeNode) node.getParent())
						.getUserObject() instanceof VotableWithUrl) {
			CassisSpectrumInfo cassisSpectrumInfo = (CassisSpectrumInfo) node.getUserObject();
			CassisSpectrum cassisSpectrum = cassisSpectrumInfo.getCassisSpectrumList().get(0);
			List<CassisMetadata> cassisMetadataList = cassisSpectrum.getCassisMetadataList();
			cassisMetadataList.remove(row);
		} else {
			CassisSpectrum cassisSpectrum = (CassisSpectrum) node.getUserObject();
			List<CassisMetadata> cassisMetadataList = cassisSpectrum.getCassisMetadataList();
			cassisMetadataList.remove(row);
		}
		if (row > 0) {
			panel.getCassisMetadataPanel().getJTable().setRowSelectionInterval(row - 1, row - 1);
		} else {
			if (panel.getCassisMetadataPanel().getJTable().getRowCount() > 0) {
				panel.getCassisMetadataPanel().getJTable().setRowSelectionInterval(0, 0);
			}
		}
	}

	/**
	 * Action for display common metadata
	 *
	 * @param node
	 *            The node.
	 */
	private void displayCommonMetadata(DefaultMutableTreeNode node) {
		CassisSpectrumInfo cassisSpectrumInfo = (CassisSpectrumInfo) node.getUserObject();
		MetadataPanel metadataPanel = panel.getMetadataPanel();
		metadataPanel.getOriginalMetadataPanel().getScroll().getVerticalScrollBar().setValue(0);
		metadataPanel.getOriginalMetadataControl().getModel().getTableModel().refresh();
		metadataPanel.getCassisMetadataPanel().getScroll().getVerticalScrollBar().setValue(0);
		metadataPanel.getCassisMetadataControl().getModel().getTableModel().refresh();

		if (cassisSpectrumInfo.getCassisSpectrumList().size() == 1) {
			cassisSpectrumInfo.mergeCommonMetadataWithFirstSpectrumMetadata();
			metadataPanel.getOriginalMetadataControl().getModel().getTableModel().addMetadataList(
					cassisSpectrumInfo.getCassisSpectrumList().get(0).getOriginalMetadataList());
			CassisMetadataTableModel<CassisMetadata> cassiMetadataTableModel = metadataPanel
					.getCassisMetadataControl().getModel().getTableModel();
			cassiMetadataTableModel.addMetadataList(
					cassisSpectrumInfo.getCassisSpectrumList().get(0).getCassisMetadataList());
			if (isColumnsDetected(cassisSpectrumInfo.getXColumnsDetected(), 0) &&
					isColumnsDetected(cassisSpectrumInfo.getYColumnsDetected(), 0)) {
				cassiMetadataTableModel
						.setWaveColumnDetected(cassisSpectrumInfo.getXColumnsDetected().get(0));
				cassiMetadataTableModel
						.setFluxColumnDetected(cassisSpectrumInfo.getYColumnsDetected().get(0));
			}
		} else {
			if (cassisSpectrumInfo.getMetadataList() != null) {
				panel.getMetadataPanel().getOriginalMetadataControl().getModel().getTableModel()
						.addMetadataList(cassisSpectrumInfo.getMetadataList());
			}
		}
	}

	/**
	 * Check for error in the given CassisSpectrum list and display an error
	 *  message if necessary.
	 *
	 * @param listCassisSpectrum The list of spectrum to check for errors.
	 */
	public void checkUnitsErrors(List<CassisSpectrum> listCassisSpectrum) {
		boolean containsXUnitError = false;
		boolean containsYUnitError = false;
		if (listCassisSpectrum == null) {
			return;
		}
		for (CassisSpectrum cs : listCassisSpectrum) {
			if (!containsXUnitError && cs.isxError()) {
				containsXUnitError = true;
			}
			if (!containsYUnitError && cs.isyError()) {
				containsYUnitError = true;
			}
			if (containsXUnitError && containsYUnitError) {
				break;
			}
		}
		if (containsXUnitError || containsYUnitError) {
			panel.displayUnitError(containsXUnitError, containsYUnitError);
		}
	}

	/**
	 * Action for add spectra.
	 *
	 * @param node
	 *            The node.
	 */
	private void addSpectra(DefaultMutableTreeNode node) {
		CassisSpectrumInfo cassisSpectrumInfo = (CassisSpectrumInfo) node.getUserObject();
		DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) node.getParent();
		boolean alreadyParsed = false;
		FileReaderCassis fileReaderCassis = new FileReaderCassis();
		List<CassisSpectrum> cassisSpectrumList;
		final CassisSpectrumControl cassisSpectrumControl = getPanel().getCassisSpectrumPanel().getControl();

		if (cassisSpectrumControl.isVotableWithURL(parentNode)) {
			VotableWithUrl votableWithUrl = (VotableWithUrl) parentNode.getUserObject();
			int index = votableWithUrl.getIndexOfCassisSpectrumInfo(cassisSpectrumInfo);
			List<CassisMetadata> voTableMetadatas = votableWithUrl.getCassisMetadataForEachSpectrum().get(index);

			cassisSpectrumList = fileReaderCassis.createCassisSpectrumListFromFile(
					new File(cassisSpectrumInfo.getPath()), voTableMetadatas);

		} else {
			cassisSpectrumList = cassisSpectrumInfo.getCassisSpectrumList();
			if (cassisSpectrumList != null && cassisSpectrumList.isEmpty()) {
				cassisSpectrumList = fileReaderCassis.createCassisSpectrumListFromFile(
						new File(cassisSpectrumInfo.getPath()), cassisSpectrumInfo.getMetadataList());
			} else {
				alreadyParsed = true;
			}
		}

//		checkUnitsErrors(cassisSpectrumList);

		List<CassisMetadata> cassisMetadatas;
		List<ColumnsDetected> waveColumns, fluxColumns;
		final InterfaceFileManager fileManager = fileReaderCassis.getFileManager();
		if (alreadyParsed) {
			cassisMetadatas = cassisSpectrumInfo.getMetadataList();
			waveColumns = new ArrayList<ColumnsDetected>();
			fluxColumns = new ArrayList<ColumnsDetected>();
		} else {
			cassisMetadatas = fileManager.getCommonCassisMetadataList();
			waveColumns = fileManager.getWaveColumnsDetected();
			fluxColumns = fileManager.getFluxColumnsDetected();

		}
		if (fileManager != null){
			waveColumns = fileManager.getWaveColumnsDetected();
			fluxColumns = fileManager.getFluxColumnsDetected();
		}

		if (cassisMetadatas == null && cassisSpectrumList != null && !cassisSpectrumList.isEmpty()) {
			cassisMetadatas = addMissingMetadata(cassisSpectrumList.get(0).getxAxisOrigin(),
					cassisSpectrumList.get(0).getYAxis(), cassisSpectrumInfo.getPath());
		}

		if (cassisSpectrumList == null) {
			panel.openUnreadFileOptionPane(new File(cassisSpectrumInfo.getPath()));
			panel.getCassisSpectrumPanel().getControl().getModel().removeResource(node);
		} else if (cassisSpectrumList.isEmpty()) {
			panel.openNotSpectrumJOptionPane(cassisSpectrumInfo.getName());
			panel.getCassisSpectrumPanel().getControl().getModel().removeResource(node);
		} else  {
			cassisSpectrumInfo.setCassisSpectrumList(cassisSpectrumList);
			cassisSpectrumInfo.setMetadatasList(cassisMetadatas);
			if (cassisMetadatas != null){
				CassisMetadata cassisMetadata = CassisMetadata.getCassisMetadata("fileName", cassisMetadatas);
				if (cassisMetadata != null){
					cassisSpectrumInfo.setName(cassisMetadata.getValue());
				}
			}
			if (cassisSpectrumList.size() > 1){
				panel.getCassisSpectrumPanel().getModel().addSpectra(node, cassisSpectrumList);
			}
		}
		if (cassisSpectrumList != null) {
			if (cassisSpectrumControl.isVotableWithURL(parentNode)) {
				VotableWithUrl votableWithUrl = (VotableWithUrl) parentNode.getUserObject();
				int index = votableWithUrl.getIndexOfCassisSpectrumInfo(cassisSpectrumInfo);
				List<CassisMetadata> voTableMetadatas =
						votableWithUrl.getCassisMetadataForEachSpectrum().get(index);
				if (cassisSpectrumInfo.getMetadataList() == null) {
					cassisSpectrumInfo.setMetadatasList(voTableMetadatas);
				} else {
					cassisSpectrumInfo.getMetadataList().addAll(voTableMetadatas);
				}
				for (int i = 0; i < cassisSpectrumList.size(); i++) {
					CassisSpectrum cassisSpectrum = cassisSpectrumList.get(i);
					addPrimaryCassisMetadata(cassisSpectrum, cassisSpectrumInfo, i,
							waveColumns, fluxColumns);
					List<CassisMetadata> list = votableWithUrl.getCassisMetadataForEachSpectrum()
							.get(index);
					for (CassisMetadata meta : list) {
						cassisSpectrum.addOriginalMetadata(meta, false);
					}
					cassisSpectrum.addCassisMetadata(
							new CassisMetadata(CASSIS_METADATA.RESOURCE_INDEX.getName(),
									String.valueOf(index), "", ""),
							true);
					cassisSpectrum.addCassisMetadata(
							new CassisMetadata(CASSIS_METADATA.VOTABLE_RESOURCE.getName(),
									votableWithUrl.getPath(), "", ""),
							true);
					cassisSpectrum.addCassisMetadata(
							new CassisMetadata(CASSIS_METADATA.RESOURCE_URL.getName(),
									votableWithUrl.getUrl(index), "", ""),
							true);
				}
			} else {
				for (int i = 0; i < cassisSpectrumList.size(); i++) {
//					waveColumns = fileReaderCassis.getFileManager().getWaveColumnsDetected().get(i);
//					fluxColumns = fileReaderCassis.getFileManager().getFluxColumnsDetected(i);
					if (waveColumns == null){
						waveColumns = new ArrayList<>();
					}
					if (fluxColumns == null){
						fluxColumns = new ArrayList<>();
					}
					addPrimaryCassisMetadata(cassisSpectrumList.get(i), cassisSpectrumInfo, i,
							waveColumns, fluxColumns);
				}
			}
		}
	}

	/**
	 * Add missing metadata for spectrum without common metadata.
	 *
	 * @param xAxisOrigin xaxis orgin
	 * @param yAxis yAxis Origin
	 * @param path the path
	 * @return
	 */
	private List<CassisMetadata> addMissingMetadata(XAxisCassis xAxisOrigin,YAxisCassis yAxis,  String path) {
		List<CassisMetadata> cassisMetadatas = new ArrayList<CassisMetadata>(3);
				cassisMetadatas.add(new CassisMetadata(CASSIS_METADATA.WAVE.getName(),
				xAxisOrigin.getAxis().toString(), "",
				xAxisOrigin.toString()));

		cassisMetadatas.add(new CassisMetadata(CASSIS_METADATA.FLUX.getName(),
				yAxis.getTitleWithoutUnit(), "",
				yAxis.getUnitString()));

		cassisMetadatas.add(new CassisMetadata(CASSIS_METADATA.FROM.getName(),
				path, "", ""));
		return cassisMetadatas;
	}

	/**
	 * Replace an existing spectrum.
	 *
	 * @param node The note
	 * @param metadata Some forced metadata for the new reading of the spectrum.
	 */
	private void replaceSpectrum(DefaultMutableTreeNode node, List<CassisMetadata> metadata) {
		CassisSpectrumInfo cassisSpectrumInfo = (CassisSpectrumInfo) node.getUserObject();
		DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) node.getParent();

		FileReaderCassis fileReaderCassis = new FileReaderCassis();
		List<CassisMetadata> additionalMeta;
		List<CassisSpectrum> cassisSpectrumList;
		if (getPanel().getCassisSpectrumPanel().getControl().isVotableWithURL(parentNode)) {
			VotableWithUrl votableWithUrl = (VotableWithUrl) parentNode.getUserObject();
			int index = votableWithUrl.getIndexOfCassisSpectrumInfo(cassisSpectrumInfo);
			List<CassisMetadata> voTableMetadatas =
					votableWithUrl.getCassisMetadataForEachSpectrum().get(index);
			additionalMeta = merge(voTableMetadatas, metadata);
		} else {
			additionalMeta = metadata;
		}

		cassisSpectrumList = fileReaderCassis.createCassisSpectrumListFromFile(
				new File(cassisSpectrumInfo.getPath()), additionalMeta);

		List<CassisMetadata> cassisMetadatas = fileReaderCassis.getFileManager()
				.getCommonCassisMetadataList();
		List<ColumnsDetected> waveColumns = fileReaderCassis.getFileManager()
				.getWaveColumnsDetected();
		List<ColumnsDetected> fluxColumns = fileReaderCassis.getFileManager()
				.getFluxColumnsDetected();
		if (cassisSpectrumList == null) {
			panel.openUnreadFileOptionPane(new File(cassisSpectrumInfo.getPath()));
			panel.getCassisSpectrumPanel().getControl().getModel().removeResource(node);
		} else if (cassisSpectrumList.isEmpty()) {
			panel.openNotSpectrumJOptionPane(cassisSpectrumInfo.getName());
			panel.getCassisSpectrumPanel().getControl().getModel().removeResource(node);
		} else if (cassisSpectrumList.size() == 1) {
			cassisSpectrumInfo.setCassisSpectrumList(cassisSpectrumList);
			cassisSpectrumInfo.setMetadatasList(cassisMetadatas);
		} else {
			panel.getCassisSpectrumPanel().getModel().addSpectra(node, cassisSpectrumList);
			cassisSpectrumInfo.setCassisSpectrumList(cassisSpectrumList);
			cassisSpectrumInfo.setMetadatasList(cassisMetadatas);
		}
		if (cassisSpectrumList != null) {
			if (getPanel().getCassisSpectrumPanel().getControl().isVotableWithURL(parentNode)) {
				VotableWithUrl votableWithUrl = (VotableWithUrl) parentNode.getUserObject();
				int index = votableWithUrl.getIndexOfCassisSpectrumInfo(cassisSpectrumInfo);
				List<CassisMetadata> voTableMetadatas =
						votableWithUrl.getCassisMetadataForEachSpectrum().get(index);
				if (cassisSpectrumInfo.getMetadataList() == null) {
					cassisSpectrumInfo.setMetadatasList(voTableMetadatas);
				} else {
					cassisSpectrumInfo.getMetadataList().addAll(voTableMetadatas);
				}
				for (int i = 0; i < cassisSpectrumList.size(); i++) {
					CassisSpectrum cassisSpectrum = cassisSpectrumList.get(i);
					addPrimaryCassisMetadata(cassisSpectrum, cassisSpectrumInfo, i,
							waveColumns, fluxColumns);
					List<CassisMetadata> list = votableWithUrl.getCassisMetadataForEachSpectrum()
							.get(index);
					for (CassisMetadata meta : list) {
						cassisSpectrum.addOriginalMetadata(meta, false);
					}
					cassisSpectrum.addCassisMetadata(
							new CassisMetadata(CASSIS_METADATA.RESOURCE_INDEX.getName(),
									String.valueOf(index), "", ""),
							true);
					cassisSpectrum.addCassisMetadata(
							new CassisMetadata(CASSIS_METADATA.VOTABLE_RESOURCE.getName(),
									votableWithUrl.getPath(), "", ""),
							true);
					cassisSpectrum.addCassisMetadata(
							new CassisMetadata(CASSIS_METADATA.RESOURCE_URL.getName(),
									votableWithUrl.getUrl(index), "", ""),
							true);
				}
			} else {
				for (int i = 0; i < cassisSpectrumList.size(); i++) {
					addPrimaryCassisMetadata(cassisSpectrumList.get(i), cassisSpectrumInfo, i,
							waveColumns, fluxColumns);
				}
			}
		}
	}

	private void addPrimaryCassisMetadata(CassisSpectrum cassisSpectrum,
			CassisSpectrumInfo cassisSpectrumInfo, int index, List<ColumnsDetected> xColumnsList,
			List<ColumnsDetected> yColumnsList) {
		cassisSpectrum.addCassisMetadata(new CassisMetadata(
				CASSIS_METADATA.SPECTRUM_INDEX.getName(), Integer.toString(index), "", ""), true);
		cassisSpectrum.addCassisMetadata(new CassisMetadata(CASSIS_METADATA.WAVE.getName(),
				cassisSpectrum.getxAxisOrigin().getAxis().toString(), "",
				cassisSpectrum.getxAxisOrigin().toString()), true);
		cassisSpectrum.addCassisMetadata(new CassisMetadata(CASSIS_METADATA.FLUX.getName(),
				cassisSpectrum.getYAxis().getTitleWithoutUnit(), "",
				cassisSpectrum.getYAxis().getUnitString()), true);
		cassisSpectrum.addCassisMetadata(new CassisMetadata(CASSIS_METADATA.VLSR.getName(),
				String.valueOf(cassisSpectrum.getVlsr()), "",
				UNIT.KM_SEC_MOINS_1.getValString()), true);
		cassisSpectrum.addCassisMetadata(new CassisMetadata(CASSIS_METADATA.LOFREQ.getName(),
				String.valueOf(cassisSpectrum.getLoFrequency()), "",
				UNIT.MHZ.getValString()), true);
		cassisSpectrum.addCassisMetadata(new CassisMetadata(CASSIS_METADATA.TELESCOPE.getName(),
				cassisSpectrum.getOriginalTelescope(), "", ""), true);
		cassisSpectrum.addCassisMetadata(new CassisMetadata(CASSIS_METADATA.FROM.getName(),
				cassisSpectrumInfo.getPath(), "", ""), true);

//		if (isColumnsDetected(xColumnsList, index)) {
		if (!xColumnsList.isEmpty()){
			cassisSpectrumInfo.setXColumnsDetected(xColumnsList);
			ColumnsDetected xColumns = xColumnsList.get(index);
			if (xColumns != null){
				cassisSpectrum.addCassisMetadata(
						new CassisMetadata(CASSIS_METADATA.WAVE_COLUMNS_DETECTED.getName(),
								xColumns.getColumnUsed(), "", ""),
						true);
			}
		}
//		if (isColumnsDetected(yColumnsList, index)) {
		if (!yColumnsList.isEmpty()){
			cassisSpectrumInfo.setYColumnsDetected(yColumnsList);
			ColumnsDetected yColumns = yColumnsList.get(index);
			if (yColumns != null){
				cassisSpectrum.addCassisMetadata(
						new CassisMetadata(CASSIS_METADATA.FLUX_COLUMNS_DETECTED.getName(),
								yColumns.getColumnUsed(), "", ""),
						true);
			}
		}
	}

	/**
	 * Open a {@link FileInfoFrame}
	 */
	private void openSpectrumInfo() {
		TreePath selectionPath = panel.getCassisSpectrumPanel().getTree().getSelectionPath();
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) selectionPath.getLastPathComponent();
		if (((DefaultMutableTreeNode) node.getParent()).isRoot() || ((DefaultMutableTreeNode) node.getParent()).getUserObject() instanceof VotableWithUrl) {
			// Only one spectrum in file
			CassisSpectrumInfo cassisSpectrumInfo = (CassisSpectrumInfo) node.getUserObject();
			CassisSpectrum cassisSpectrum = cassisSpectrumInfo.getCassisSpectrumList().get(0);
			displayFileInfoFrame(cassisSpectrum, cassisSpectrumInfo);
		} else {
			// Several spectra in file
			CassisSpectrum cassisSpectrum = (CassisSpectrum) node.getUserObject();
			CassisSpectrumInfo cassisSpectrumInfo = (CassisSpectrumInfo) ((DefaultMutableTreeNode) node.getParent()).getUserObject();
			displayFileInfoFrame(cassisSpectrum, cassisSpectrumInfo);
		}
	}

	/**
	 * Display the FileInfoFrame for the given CassisSpectrum.
	 *
	 * @param spectrum The CassisSpectrum
	 * @param spectrumInfo The CassisSpectrumInfo
	 */
	private void displayFileInfoFrame(CassisSpectrum spectrum, CassisSpectrumInfo spectrumInfo) {
		JFrame fif = new FileInfoFrame(spectrum, spectrumInfo);
		fif.setVisible(true);
	}

	/**
	 * Open a {@link AddMetadataFrame}
	 */
	private void openAddMetadataFrame() {
		final AddMetadataFrame frame = new AddMetadataFrame();
		frame.getOkButton().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String name = frame.getNameTextField().getText();
				String value = frame.getValueTextField().getText();
				String comment = frame.getCommentTextField().getText();
				String unit = frame.getUnitComboBox().getSelectedItem().toString();
				if (name.isEmpty()) {
					frame.openWarningJOptionPane("name");
				} else if (value.isEmpty()) {
					frame.openWarningJOptionPane("value");
				} else {
					CassisMetadata cassisMetadata = new CassisMetadata(name, value, comment, unit);
					CassisSpectrum cassisSpectrum = getCassisSpectrumSelected();
					List<CassisMetadata> cassisMetadataList = cassisSpectrum.getCassisMetadataList();
					if (metadataNameOnMetadataList(cassisMetadata, cassisMetadataList)) {
						frame.openMetadataNameAlreadyExistsJOptionPane(name);
					} else {
						addMetadata(cassisMetadata);
						frame.dispose();
					}
				}
			}
		});
	}

	/**
	 * Add a {@link CassisMetadata} to {@link JTable}
	 *
	 * @param cassisMetadata
	 *            The {@link CassisMetadata} to add on {@link JTable}
	 */
	private void addMetadata(CassisMetadata cassisMetadata) {
		CassisSpectrum cassisSpectrum = getCassisSpectrumSelected();
		CassisMetadataTableModel<CassisMetadata> model = panel.getCassisMetadataPanel().getControl().getModel().getTableModel();
		List<CassisMetadata> cassisMetadatasList = cassisSpectrum.getCassisMetadataList();
		cassisMetadatasList.add(cassisMetadata);
		model.refresh();
		model.addMetadataList(cassisSpectrum.getCassisMetadataList());
		JTable table = panel.getMetadataPanel().getCassisMetadataPanel().getJTable();
		table.setRowSelectionInterval(model.getRowCount() - 1, model.getRowCount() - 1); // Last row selected
		table.scrollRectToVisible(new Rectangle(table.getCellRect(model.getRowCount() - 1, 0, true))); // Scroll bottom
	}

	/**
	 *
	 * Return true if the name of cassisMetadata exists on the cassisMetadataList
	 *
	 * @param cassisMetadata
	 * @param cassisMetadatasList
	 * @return true if the name of cassisMetadata exists on the cassisMetadataList
	 */
	public static boolean metadataNameOnMetadataList(CassisMetadata cassisMetadata, List<CassisMetadata> cassisMetadatasList) {
		boolean exist = false;
		for (CassisMetadata cm : cassisMetadatasList) {
			if (cm.getName().equalsIgnoreCase(cassisMetadata.getName())) {
				exist = true;
				break;
			}
		}
		return exist;
	}

	/**
	 * @return cassisSpectrum selected on JTree
	 */
	private CassisSpectrum getCassisSpectrumSelected() {
		TreePath selectionPath = panel.getCassisSpectrumPanel().getTree().getSelectionPath();
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) selectionPath.getLastPathComponent();
		CassisSpectrum cassisSpectrum;
		if (((DefaultMutableTreeNode) node.getParent()).isRoot() || ((DefaultMutableTreeNode) node.getParent()).getUserObject() instanceof VotableWithUrl) {
			CassisSpectrumInfo cassisSpectrumInfo = (CassisSpectrumInfo) node.getUserObject();
			cassisSpectrum = cassisSpectrumInfo.getCassisSpectrumList().get(0);
		} else {
			cassisSpectrum = (CassisSpectrum) node.getUserObject();
		}
		return cassisSpectrum;
	}

	/**
	 * Update the enabled state of the add, delete, display all metadata, display original metadata and save value buttons.
	 *
	 * @param add
	 *            The new state for the add button.
	 * @param delete
	 *            The new state for the delete button.
	 * @param displayAllMetadata
	 *            The new state for the display all metadata button.
	 * @param displayOriginalMetadata
	 *            The new state for the original metadata button.
	 * @param saveValue
	 *            The new state for the save value button.
	 */
	private void updateButtonsState(boolean add, boolean delete, boolean saveValue, boolean spectrumInfo) {
		MetadataPanel mp = panel.getMetadataPanel();
		mp.getCassisMetadataPanel().getAddButton().setEnabled(add);
		mp.getCassisMetadataPanel().getDeleteButton().setEnabled(delete);
		mp.getCassisMetadataPanel().getSaveButton().setEnabled(saveValue);
		mp.getCassisMetadataPanel().getSpectrumInfoButton().setEnabled(spectrumInfo);
	}

	/**
	 * Disable "Datalink" tab and select "OriginalMetadata" tab
	 */
	private void disableTabDatalink() {
		panel.getMetadataPanel().getDatalinkPanel().removeAll();
		panel.getMetadataPanel().getTabbedPane().setEnabledAt(2, false);
		panel.getMetadataPanel().getTabbedPane().setSelectedIndex(1);
	}

	/**
	 * Display datalink information on "Datalink" tab according to the node selected on JTree
	 *
	 * @param node
	 *            Selected node on JTree
	 */
	private void displayDatalink(DefaultMutableTreeNode node) {
		TreeNode parent = node.getParent();
		DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) parent;
		if (getPanel().getCassisSpectrumPanel().getControl().isCassisSpectrum(node)) {
			if (getPanel().getCassisSpectrumPanel().getControl().isDownloadFile(parentNode)) {
				DefaultMutableTreeNode grandFatherNode = (DefaultMutableTreeNode) parentNode.getParent();
				VotableWithUrl votableWithUrl = (VotableWithUrl) grandFatherNode.getUserObject();
				DatalinkModel[] datalinkModelTab = votableWithUrl.getDatalinkModelTab();
				displayDatalink(datalinkModelTab, votableWithUrl.getPath(), grandFatherNode.getIndex(parent));
			} else if (getPanel().getCassisSpectrumPanel().getControl().isCassisSpectrumInfo(parentNode)) {
				CassisSpectrumInfo cassisSpectrumInfo = (CassisSpectrumInfo) parentNode.getUserObject();
				DatalinkModel[] datalinkModelTab = cassisSpectrumInfo.getDatalinkModelTab();
				displayDatalink(datalinkModelTab, cassisSpectrumInfo.getPath(), parentNode.getIndex(node));
			}
		} else if (getPanel().getCassisSpectrumPanel().getControl().isDownloadFile(node)) {
			VotableWithUrl votableWithUrl = (VotableWithUrl) parentNode.getUserObject();
			DatalinkModel[] datalinkModelTab = votableWithUrl.getDatalinkModelTab();
			displayDatalink(datalinkModelTab, votableWithUrl.getPath(), parent.getIndex(node));
		} else if (getPanel().getCassisSpectrumPanel().getControl().isCassisSpectrumInfo(node)) {
			CassisSpectrumInfo cassisSpectrumInfo = (CassisSpectrumInfo) node.getUserObject();
			if (node.isLeaf()) {
				DatalinkModel[] datalinkModelTab = cassisSpectrumInfo.getDatalinkModelTab();
				displayDatalink(datalinkModelTab, cassisSpectrumInfo.getPath(), -1);
			} else {
				disableTabDatalink();
			}
		}
	}

	private void displayDatalink(DatalinkModel[] datalinkModelTab, String path, int index) {
		if (datalinkModelTab != null && index >= 0) {
			JPanel datalinkPanel = panel.getMetadataPanel().getDatalinkPanel();
			datalinkPanel.removeAll();
			datalinkPanel.add(new DatalinkPanel(path, datalinkModelTab[index]), BorderLayout.CENTER);
			panel.getMetadataPanel().getTabbedPane().setEnabledAt(2, true);
			if (panel.getMetadataPanel().getTabbedPane().getSelectedIndex() == 2) {
				panel.getMetadataPanel().getTabbedPane().setSelectedIndex(1);
				panel.getMetadataPanel().getTabbedPane().setSelectedIndex(2);
			}
		} else {
			disableTabDatalink();
		}
	}

	/**
	 * Display {@link VotableWithUrl} metadata on the "Original metadata"
	 *
	 * @param cassisMetadataList
	 *            List containing metadata to display
	 */
	private void displayMetadataOfVotableWithUrl(List<CassisMetadata> cassisMetadataList) {
		panel.getMetadataPanel().getOriginalMetadataControl().getModel().getTableModel().addMetadataList(cassisMetadataList);
	}

	/**
	 * Return true if columns of data are detected and false if not
	 *
	 * @param columnsDetectedList
	 *            The List of columns of data detected for each spectrum
	 * @param index
	 *            The index of spectrum
	 * @return True if columns are detected and false if not
	 */
	private boolean isColumnsDetected(List<ColumnsDetected> columnsDetectedList, int index) {
		boolean detected = false;
		if (columnsDetectedList != null && !columnsDetectedList.isEmpty()) {
			if (columnsDetectedList.size() > index) {
				detected = true;
			}
		}
		return detected;
	}

	/**
	 * Return the index of spectrum in a spectrum list
	 *
	 * @param cassisSpectrum
	 *            The spectrum that we want to know the index
	 * @param cassisSpectrumList
	 *            The spectrum list
	 * @return The index of spectrum in a spectrum list
	 */
	public int indexOfSpectrum(CassisSpectrum cassisSpectrum, List<CassisSpectrum> cassisSpectrumList) {
		int index = -1;
		for (int i = 0; i < cassisSpectrumList.size(); i++) {
			CassisSpectrum cs = cassisSpectrumList.get(i);
			if (cassisSpectrum.equals(cs)) {
				index = i;
				break;
			}
		}
		return index;
	}

	/**
	 * Merge two list of metadata into a new one then return it.
	 *
	 * @param origin The first metadata list.
	 * @param added The second metadata list (who have the priority if there is
	 *  two metadata with the same name).
	 * @return the new metadata list.
	 */
	private List<CassisMetadata> merge(List<CassisMetadata> origin, List<CassisMetadata> added) {
		List<CassisMetadata> newList = new ArrayList<CassisMetadata>(origin);
		for (CassisMetadata metaToAdd : added) {
			Iterator<CassisMetadata> it = newList.iterator();
			CassisMetadata metaOrigin;
			while (it.hasNext()) {
				metaOrigin = it.next();
				if (metaOrigin.getName().equals(metaToAdd.getName())) {
					it.remove();
					break;
				}
			}
			newList.add(metaToAdd);
		}
		return newList;
	}
}
