/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.datalink;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 * Create a {@link JPanel} containing all information about datalink containing in the votable file
 *
 * @author Lea Foissac
 */
public class DatalinkPanel extends JPanel {

	private static final long serialVersionUID = -3458568846086532558L;

	private List<ResourcePanel> resourcePanelList;
	private DatalinkControl control;
	private JPanel datalinkPanel;
	private JScrollPane scroll;


	/**
	 * Create a {@link DatalinkPanel} with path file and spectrum index to display only datalink information about spectrum
	 *
	 * @param pathFile
	 *            The path of votable file
	 * @param spectrumIndex
	 *            The index of spectrum
	 */
	public DatalinkPanel(String pathFile, int spectrumIndex) {
		super(new BorderLayout());
		this.resourcePanelList = new ArrayList<ResourcePanel>();
		DatalinkModel model = new DatalinkModel(pathFile, spectrumIndex);
		this.control = new DatalinkControl(this, model);
		this.scroll = new JScrollPane(getDatalinkPanel());
		this.add(scroll, BorderLayout.CENTER);
	}

	/**
	 * Create a {@link DatalinkPanel} with path file and the {@link DatalinkPanel} model
	 *
	 * @param pathFile
	 *            The path of votable file
	 * @param model
	 *            Model to use with {@link DatalinkPanel}
	 */
	public DatalinkPanel(String pathFile, DatalinkModel model) {
		super(new BorderLayout());
		this.resourcePanelList = new ArrayList<ResourcePanel>();
		this.control = new DatalinkControl(this, model);
		this.scroll = new JScrollPane(getDatalinkPanel());
		this.add(scroll, BorderLayout.CENTER);
	}

	/**
	 * Getter for the {@link JPanel} containing one or multiple {@link ResourcePanel}
	 *
	 * @return The {@link JPanel} containing one or multiple {@link ResourcePanel}
	 */
	public JPanel getDatalinkPanel() {
		if (datalinkPanel == null) {
			datalinkPanel = new JPanel();
			datalinkPanel.setLayout(new BoxLayout(datalinkPanel, BoxLayout.PAGE_AXIS));
			datalinkPanel.setBorder(BorderFactory.createEmptyBorder(6, 6, 6, 6));
		}
		return datalinkPanel;
	}

	/**
	 * Getter for the {@link ResourcePanel} containing in the {@link DatalinkPanel}
	 *
	 * @return All {@link ResourcePanel} containing in the {@link DatalinkPanel}
	 */
	public List<ResourcePanel> getResourcePanelList() {
		return resourcePanelList;
	}

	/**
	 * Getter for the controller
	 *
	 * @return The controller {@link DatalinkControl}
	 */
	public DatalinkControl getControl() {
		return control;
	}

}
