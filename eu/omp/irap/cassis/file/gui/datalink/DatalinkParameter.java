/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.datalink;

import java.util.ArrayList;
import java.util.List;


/**
 * Simple class for defining a Datalink parameter.
 *
 * @author M. Boiziot
 */
public class DatalinkParameter {

	protected final String name;
	private final String description;
	protected List<String> values;
	private String currentValue;
	private boolean refValue;


	/**
	 * Constructor.
	 *
	 * @param name Name of the parameter.
	 * @param description Description of the parameter.
	 * @param valuesProv List of values eventually provided by the service.
	 * @param defaultValue The default value.
	 * @param refValue Define if this parameter was defined with a ref value.
	 */
	public DatalinkParameter(String name, String description, List<String> valuesProv, String defaultValue, boolean refValue) {
		this.name = name;
		this.description = description;
		this.values = valuesProv == null ? new ArrayList<String>(0) : new ArrayList<String>(valuesProv);
		this.currentValue = defaultValue;
		this.refValue = refValue;
	}

	/**
	 * Return the name of the parameter.
	 *
	 * @return the name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Return the description of the parameter.
	 *
	 * @return the description of the parameter.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Return the provided values.
	 *
	 * @return The values.
	 */
	public List<String> getProvidedValues() {
		return values;
	}

	/**
	 * Return the current value of the parameter.
	 *
	 * @return the current value of the parameter.2
	 */
	public String getCurrentValue() {
		return currentValue;
	}

	/**
	 * Return if this parameter was defined with a ref value.
	 *
	 * @return true if this parameter was defined with a ref value, false otherwise.
	 */
	public boolean isRefValue() {
		return refValue;
	}

}
