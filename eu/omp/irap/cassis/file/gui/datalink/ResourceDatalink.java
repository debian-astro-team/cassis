/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.datalink;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import uk.ac.starlink.table.RowSequence;
import uk.ac.starlink.table.StarTable;
import uk.ac.starlink.table.StarTableFactory;
import uk.ac.starlink.votable.ParamElement;
import uk.ac.starlink.votable.VOElement;
import uk.ac.starlink.votable.VOElementFactory;

/**
 * Create a model to {@link ResourcePanel}
 *
 * @author Lea Foissac
 */
public class ResourceDatalink {

	private VOElement voElement;
	private int spectrumIndex;
	private String path;
	private String rawData;
	private String id;
	private String type;
	private String utype;
	private String accessUrl;
	private String resourceIdentifier;
	private String standardID;
	private List<DatalinkParameter> inputParamList;


	/**
	 * Create a model
	 *
	 * @param voElement
	 *            The {@link VOElement} of resource
	 * @param path
	 *            The path of file containing the resource
	 * @param spectrumIndex
	 *            The index of spectrum to analyze
	 */
	public ResourceDatalink(VOElement voElement, String path, int spectrumIndex) {
		this.path = path;
		this.voElement = voElement;
		this.spectrumIndex = spectrumIndex;
		inputParamList = new ArrayList<DatalinkParameter>();
		recoverParam();
		recoverInputParams();
	}

	/**
	 * Recover "accessURL", "resourceIdentifier" and "standardID" parameters on the resource
	 */
	public void recoverParam() {
		VOElement[] paramChildren = voElement.getChildrenByName("PARAM");
		for (VOElement child : paramChildren) {
			ParamElement param = (ParamElement) child;
			switch (param.getName()) {
			case "accessURL":
				accessUrl = param.getObject().toString();
				break;
			case "resourceIdentifier":
				resourceIdentifier = param.getObject().toString();
				break;
			case "standardID":
				standardID = param.getObject().toString();
				break;
			default:
				break;
			}
		}
	}

	/**
	 * Recover all "inputParams" on the resource
	 */
	public void recoverInputParams() {
		VOElement element = getInputParamsElement();
		if (element != null) {
			VOElement[] paramChildren = element.getChildrenByName("PARAM");
			for (VOElement child : paramChildren) {
				inputParamList.add(getParameter(child));
			}
		}
	}

	/**
	 * Search then return the "inputParams" element.
	 *
	 * @return the "inputParams" element or null if not found.
	 */
	private VOElement getInputParamsElement() {
		VOElement[] groupChildren = voElement.getChildrenByName("GROUP");
		VOElement element = null;
		for (VOElement child : groupChildren) {
			if ("inputParams".equals(child.getName())) {
				element = child;
				break;
			}
		}
		return element;
	}

	/**
	 * Return a {@link DatalinkParameter} object from a "PARAM" {@link VOElement}.
	 *
	 * @param element The element
	 * @return the {@link DatalinkParameter}.
	 */
	private DatalinkParameter getParameter(VOElement element) {
		String name = element.getName();
		String description = element.getDescription();
		String defaultValue = null;
		List<String> values = null;
		boolean refValue = false;

		ParamElement param = (ParamElement) element;
		String ref = param.getAttribute("ref");

		if (ref != null && !ref.equals("")) {
			defaultValue = getRefValue(ref);
			refValue = true;
		} else {
			defaultValue = element.getAttribute("value");
			values = getValues(param);
		}
		return new DatalinkParameter(
				name, description, values, defaultValue, refValue);
	}

	/**
	 * Return the values of the given parameter.
	 *
	 * @param param The parameter
	 * @return the list of values, or null if not found.
	 */
	private List<String> getValues(ParamElement param) {
		List<String> values = null;
		VOElement[] voElementValues = param.getChildrenByName("VALUE");
		if (voElementValues.length >= 1) {
			values = new ArrayList<String>(voElementValues.length);
			VOElement childValue = voElementValues[0];
			for (VOElement childOption : childValue.getChildren()) {
				values.add(childOption.getAttribute("value"));
			}
		}
		return values;
	}

	/**
	 * Get the ref value.
	 *
	 * @param ref The name of column
	 * @return the ref value, or null if not found.
	 */
	public String getRefValue(String ref) {
		StarTable starTable;
		String result = null;
		try {
			starTable = new StarTableFactory().makeStarTable(path, "votable");
			RowSequence rseq = starTable.getRowSequence();
			int s = 0;
			while (rseq.next()) {
				Object[] row = rseq.getRow();
				if (s == spectrumIndex) {
					int j = 0;
					for (Object object : row) {
						if (starTable.getColumnInfo(j).getName().equals(ref)) {
							result = object.toString();
						}
						j++;
					}
				}
				s++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Getter for the {@link VOElement} of resource
	 *
	 * @return the {@link VOElement} of resource
	 */
	public VOElement getVoElement() {
		return voElement;
	}

	/**
	 * Getter for the id of resource
	 *
	 * @return The id of resource
	 */
	public String getId() {
		if (id == null) {
			id = voElement.getID();
		}
		return id;
	}

	/**
	 * Getter for the type of resource
	 *
	 * @return The type of resource
	 */
	public String getType() {
		if (type == null) {
			type = voElement.getAttribute("type");
		}
		return type;
	}

	/**
	 * Getter for the uType of resource
	 *
	 * @return The uType of resource
	 */
	public String getUtype() {
		if (utype == null) {
			utype = voElement.getAttribute("utype");
		}
		return utype;
	}

	/**
	 * Getter for the access url of resource
	 *
	 * @return The access url of resource
	 */
	public String getAccessUrl() {
		return accessUrl;
	}

	/**
	 * Getter for the standardID of resource
	 *
	 * @return The standardID of resource
	 */
	public String getStandardId() {
		return standardID;
	}

	/**
	 * Getter for the resourceIdentifier of resource
	 *
	 * @return The resourceIdentifier of resource
	 */
	public String getResourceIdentifier() {
		return resourceIdentifier;
	}

	/**
	 * Getter for the path of file
	 *
	 * @return The path of file
	 */
	public String getPath() {
		return path;
	}

	/**
	 * Getter for the raw data of resource
	 *
	 * @return The raw data of resource
	 */
	public String getRawData() {
		if (rawData == null) {
			rawData = "";
			try (Scanner file = new Scanner(new File(path))) {
				boolean find = false;
				while (file.hasNext()) {
					String line = file.nextLine();
					if (line.contains("<resource") || line.contains("<RESOURCE")) {
						if (line.contains(getUtype()) && line.contains(getType()) && line.contains(getId())) {
							find = true;
						}
					}
					if (find) {
						rawData = rawData + "\n" + line;
					}
					if ((line.contains("</RESOURCE>") || line.contains("</resource>")) && find) {
						find = false;
					}
				}
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
		}
		return rawData;
	}

	/**
	 * Return the list of input parameters.
	 *
	 * @return  the list of input parameters.
	 */
	public List<DatalinkParameter> getInputParamsList() {
		return inputParamList;
	}

	/**
	 * Getter for the index of spectrum to analyze. Contained ALL_SPECTRA constant (-1)
	 *  if all spectra must be analyzed. Contained the index of spectrum if only this
	 *  spectrum must be analyzed.
	 *
	 * @return The index of spectrum to analyzed
	 */
	public int getSpectrumIndex() {
		return spectrumIndex;
	}

	/**
	 * Getter for the resource contained in the file in the form of {@link VOElement} object
	 *
	 * @param voElement
	 *            The {@link VOElement} of file
	 * @return The resource contained in the file in the form of {@link VOElement} object
	 */
	private static List<VOElement> getResourceList(VOElement voElement) {
		List<VOElement> voElements = new ArrayList<VOElement>();
		NodeList resourceList = voElement.getElementsByVOTagName("RESOURCE");
		for (int i = 0; i < resourceList.getLength(); i++) {
			VOElement el = (VOElement) resourceList.item(i);
			if ("adhoc:service".equals(el.getAttribute("utype"))) {
				voElements.add(el);
			}
		}
		return voElements;
	}

	public static void main(String[] args) throws SAXException, IOException {
		List<VOElement> lVo = getResourceList(new VOElementFactory().makeVOElement(new File("/home/mboiziot/pollux.votable")));
		ResourceDatalink rd = new ResourceDatalink(lVo.get(0), "/home/mboiziot/pollux.votable",0);
		System.out.println(rd.getRawData());
	}
}
