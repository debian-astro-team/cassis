/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.datalink;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import uk.ac.starlink.votable.VOElement;
import uk.ac.starlink.votable.VOElementFactory;

/**
 * Create a model to {@link DatalinkPanel}
 *
 * @author Lea Foissac
 *
 */
public class DatalinkModel {

	private String path;
	private int spectrumIndex;
	private List<ResourceDatalink> resourceDatalinkList;


	/**
	 * Create a model with the path of votable file and the spectrum index
	 *
	 * @param path
	 *            The path file
	 * @param spectrumIndex
	 *            The index of spectrum in file
	 */
	public DatalinkModel(String path, int spectrumIndex) {
		this.path = path;
		this.spectrumIndex = spectrumIndex;
	}

	/**
	 * Getter for the resource containing in the file to analyze
	 *
	 * @return All resource containing in the file to analyze
	 */
	public List<ResourceDatalink> getResourceDatalinkList() {
		if (resourceDatalinkList == null) {
			try {
				resourceDatalinkList = new ArrayList<ResourceDatalink>();
				VOElement rootElement = new VOElementFactory().makeVOElement(new File(path));
				List<VOElement> voElementList = getResourceList(rootElement);
				for (VOElement voElement : voElementList) {
					resourceDatalinkList.add(new ResourceDatalink(voElement, path, spectrumIndex));
				}
			} catch (SAXException | IOException e) {
				e.printStackTrace();
			}
		}
		return resourceDatalinkList;
	}

	/**
	 * Getter for the resource contained in the file in the form of {@link VOElement} object
	 *
	 * @param voElement
	 *            The {@link VOElement} of file
	 * @return The resource contained in the file in the form of {@link VOElement} object
	 */
	private List<VOElement> getResourceList(VOElement voElement) {
		List<VOElement> voElements = new ArrayList<VOElement>();
		NodeList resourceList = voElement.getElementsByVOTagName("RESOURCE");
		for (int i = 0; i < resourceList.getLength(); i++) {
			VOElement el = (VOElement) resourceList.item(i);
			if ("adhoc:service".equals(el.getAttribute("utype"))) {
				voElements.add(el);
			}
		}
		return voElements;
	}

}
