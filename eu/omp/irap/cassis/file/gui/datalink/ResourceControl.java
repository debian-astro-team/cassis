/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.datalink;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JTextField;

/**
 * Create a controller for the {@link ResourcePanel}
 *
 * @author Lea Foissac
 *
 */
public class ResourceControl {

	private ResourcePanel panel;
	private ResourceDatalink datalink;

	private List<RessourceDatalinkAction>ressourceDatalinkActions;

	/**
	 * Create a controller to interact with the panel {@link ResourcePanel} and the model {@link ResourceDatalink}
	 *
	 * @param panel
	 *            The panel of {@link DatalinkControl}
	 * @param datalink
	 *            The model of {@link DatalinkControl}
	 */
	public ResourceControl(ResourcePanel panel, ResourceDatalink datalink) {
		this.panel = panel;
		this.datalink = datalink;
		ressourceDatalinkActions = new ArrayList<RessourceDatalinkAction>();
		ressourceDatalinkActions.add(new DisplayBrowserAction());
	}

	/**
	 * Update the access URL {@link JTextField}.
	 */
	public void updateAccessUrl() {
		int i = 1;
		StringBuilder sb = new StringBuilder();
		sb.append(datalink.getAccessUrl());
		sb.append('?');
		for (Entry<String, JComponent> entry : panel.getParameterComponents().entrySet()) {
			String selection = getSelection(entry.getValue());
			if (selection != null && !selection.isEmpty()) {
				if (i == 1) {
					sb.append(entry.getKey()).append('=').append(selection);
				} else {
					sb.append('&').append(entry.getKey()).append('=').append(selection);
				}
				i++;
			}
		}
		panel.getAccessUrlTextField().setText(sb.toString());
	}

	/**
	 * Return the selection for the given component.
	 *
	 * @param component The component.
	 * @return The selection for the given component or null if not found.
	 */
	private String getSelection(JComponent component) {
		String selection = null;
		if (component instanceof JComboBox) {
			selection = (String)((JComboBox<?>) component).getSelectedItem();
		} else if (component instanceof JTextField) {
			selection = ((JTextField) component).getText();
		}
		return selection;
	}

	/**
	 * Action to do once a "copy Source" {@link JButton} is clicked
	 *
	 * @param rawData
	 *            The raw data to put on the clipboard
	 */
	public void copySourceAction(String rawData) {
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Clipboard clipboard = toolkit.getSystemClipboard();
		clipboard.setContents(new StringSelection(rawData), null);
	}

	/**
	 * Open the browser on the access URL.
	 *
	 * @param accessUrl The access URL.
	 */
	public void sendAction(String accessUrl) {
		for (RessourceDatalinkAction ressourceDatalinkAction : ressourceDatalinkActions) {
			ressourceDatalinkAction.run(panel, accessUrl);
		}
	}

	/**
	 * Getter for the model
	 *
	 * @return The model
	 */
	public ResourceDatalink getModel() {
		return datalink;
	}


	public void addRessourceDatalinkAction(RessourceDatalinkAction ressourceDatalinkAction) {
		ressourceDatalinkActions.add(ressourceDatalinkAction);
	}

	public RessourceDatalinkAction removeRessourceDatalinkAction(String name) {
		RessourceDatalinkAction res = null;
		for (Iterator<RessourceDatalinkAction> iterator = ressourceDatalinkActions.iterator(); iterator.hasNext();) {
			RessourceDatalinkAction ressourceDatalinkAction = (RessourceDatalinkAction) iterator.next();
			if (ressourceDatalinkAction.getName().equals(name)) {
				iterator.remove();
				res = ressourceDatalinkAction;
				break;
			}
		}
		return res;
	}

}
