/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.datalink;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

/**
 * Create a {@link JPanel} containing information about only one ressource of datalink file
 *
 * @author Lea Foissac
 *
 */
public class ResourcePanel extends JPanel {

	private static final long serialVersionUID = 5076469202714123227L;

	private JButton copySource;
	private JButton send;
	private JPanel infoPanel;
	private JPanel buttonsPanel;
	private JTextArea rawData;
	private JTextField accessUrlTextField;
	private Map<String, JComponent> parameterComponents;
	private ResourceControl control;
	private ResourceDatalink resourceDatalink;


	/**
	 * Create a {@link ResourcePanel} with model of data, the {@link ResourceDatalink}
	 *
	 * @param datalink
	 */
	public ResourcePanel(ResourceDatalink datalink) {
		super();
		this.setLayout(new BorderLayout());
		this.resourceDatalink = datalink;
		this.control = new ResourceControl(this, datalink);
		this.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(datalink.getId()), new EmptyBorder(6, 6, 6, 6)));
		this.add(getInfoPanel(), BorderLayout.CENTER);
		this.add(getButtonsPanel(), BorderLayout.SOUTH);
	}

	/**
	 * Getter for the {@link JPanel} containing all informations
	 *
	 * @return All informations about the resource
	 */
	public JPanel getInfoPanel() {
		if (infoPanel == null) {
			infoPanel = new JPanel();
			if (resourceDatalink.getType().equals("meta")) {
				infoPanel.setLayout(new GridBagLayout());
				GridBagConstraints gbc = new GridBagConstraints();
				int line = addAccessUrl(infoPanel, gbc, 0);
				line = addOtherInfo(infoPanel, gbc, line);
				line = addInputParams(infoPanel, gbc, line);
			} else {
				infoPanel.setLayout(new BorderLayout());
				infoPanel.add(getRawData(), BorderLayout.CENTER);
			}
		}
		return infoPanel;
	}

	/**
	 * Add the "Access URL" components.
	 *
	 * @param panel The panel used to add the new components.
	 * @param gbc The constraints for the panel.
	 * @param line The line number.
	 * @return the new line number.
	 */
	private int addAccessUrl(JPanel panel, GridBagConstraints gbc, int line) {
		int nLine = line;
		accessUrlTextField = new JTextField(resourceDatalink.getAccessUrl());
		accessUrlTextField.setBackground(Color.WHITE);
		accessUrlTextField.setEditable(false);
		createLine(panel, gbc, "Access URL", null, accessUrlTextField, nLine);
		nLine++;
		return nLine;

	}

	/**
	 * Getter for the {@link JTextField} of access url
	 *
	 * @return The {@link JTextField} of access url
	 */
	public JTextField getAccessUrlTextField() {
		return accessUrlTextField;
	}

	/**
	 * Add additional information, like resourceIdentifier and standardID if they are known.
	 *
	 * @param panel The panel used to add the new components.
	 * @param gbc The constraints for the panel.
	 * @param line The line number.
	 * @return the new line number.
	 */
	private int addOtherInfo(JPanel panel, GridBagConstraints gbc, int line) {
		int nLine = line;
		if (resourceDatalink.getResourceIdentifier() != null &&
				!resourceDatalink.getResourceIdentifier().isEmpty()) {
			JTextField text = new JTextField(resourceDatalink.getResourceIdentifier());
			text.setEditable(false);
			createLine(panel, gbc, "resourceIdentifier", null, text, line);
			nLine++;
		}
		if (resourceDatalink.getStandardId() != null &&
				!resourceDatalink.getStandardId().isEmpty()) {
			JTextField text = new JTextField(resourceDatalink.getStandardId());
			text.setEditable(false);
			createLine(panel, gbc, "standardID", null, text, line);
			nLine++;
		}
		return nLine;
	}

	/**
	 * Getter for the {@link JPanel} containing all the "Copy source" and "Send" {@link JButton}
	 *
	 * @return The {@link JPanel} containing all {@link JButton}
	 */
	public JPanel getButtonsPanel() {
		if (buttonsPanel == null) {
			buttonsPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
			buttonsPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));
			if (resourceDatalink.getType().equals("meta")) {
				buttonsPanel.add(getCopySourceButton());
				buttonsPanel.add(getSendButton());
			} else {
				buttonsPanel.add(getCopySourceButton());
			}
		}
		return buttonsPanel;
	}

	/**
	 * Getter for the "Copy source" {@link JButton}
	 *
	 * @return The "Copy source" {@link JButton}
	 */
	public JButton getCopySourceButton() {
		if (copySource == null) {
			copySource = new JButton("Copy source");
			copySource.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.copySourceAction(resourceDatalink.getRawData());
				}
			});
		}
		return copySource;
	}

	/**
	 * Getter for the "Send" {@link JButton}
	 *
	 * @return The "Send" {@link JButton}
	 */
	public JButton getSendButton() {
		if (send == null) {
			send = new JButton("Send");
			send.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.sendAction(getAccessUrlTextField().getText());
				}
			});
		}
		return send;
	}

	/**
	 * Create the components for the inputs parameters then add them to the given panel.
	 *
	 * @param panel The panel used to add the new components.
	 * @param gbc The constraints for the panel.
	 * @param line The line number.
	 * @return the new line number.
	 */
	public int addInputParams(JPanel panel, GridBagConstraints gbc, int line) {
		int nLine = line;
		List<DatalinkParameter> inputParamsList = resourceDatalink.getInputParamsList();
		if (!inputParamsList.isEmpty()) {
			for (DatalinkParameter param : inputParamsList) {
				nLine = addParameter(param, panel, gbc, nLine);
			}
			control.updateAccessUrl();
		}
		return nLine;
	}

	/**
	 * Create component for a parameter then add them on the panel.
	 *
	 * @param parameter The parameter.
	 * @param panel The panel used to add the new components.
	 * @param gbc The constraints for the panel.
	 * @param line The line number.
	 * @return the new line number.
	 */
	private int addParameter(DatalinkParameter parameter, JPanel panel,
			GridBagConstraints gbc, int line) {
		int nLine = line;
		JComponent comp;
		if (parameter.getProvidedValues() == null ||
				parameter.getProvidedValues().isEmpty()) {
			comp = createParameterTextField(parameter);
		} else {
			comp = createParameterJComboBox(parameter);
		}
		createLine(panel, gbc, parameter.getName(), parameter.getDescription(), comp, nLine);
		getParameterComponents().put(parameter.getName(), comp);
		nLine++;
		return nLine;
	}

	/**
	 * Create then return a {@link JTextField} for the given parameter.
	 *
	 * @param parameter The parameter.
	 * @return the {@link JTextField} for the given parameter.
	 */
	private JTextField createParameterTextField(DatalinkParameter parameter) {
		JTextField field = parameter.getCurrentValue() != null ?
				new JTextField(parameter.getCurrentValue()) : new JTextField();
		if (parameter.isRefValue()) {
			field.setEditable(false);
		}
		field.setPreferredSize(new Dimension(125, 20));
		field.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				control.updateAccessUrl();
			}
			@Override
			public void keyPressed(KeyEvent e) {
				control.updateAccessUrl();
			}
		});
		return field;
	}

	/**
	 * Create then return a {@link JComboBox} for the given parameter.
	 *
	 * @param parameter The parameter.
	 * @return the {@link JComboBox} for the given parameter.
	 */
	private JComboBox<String> createParameterJComboBox(DatalinkParameter parameter) {
		JComboBox<String> comboBox = new JComboBox<>(parameter.getProvidedValues().toArray(
				new String[parameter.getProvidedValues().size()]));
		comboBox.setPreferredSize(new Dimension(125, 20));
		comboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				control.updateAccessUrl();
			}
		});
		return comboBox;
	}

	/**
	 * Create if needed then return the map with the names of the parameters as
	 *  keys and components as values.
	 *
	 * @return the map with the names of the parameters as keys and components as values.
	 */
	public Map<String, JComponent> getParameterComponents() {
		if (parameterComponents == null) {
			parameterComponents = new HashMap<String, JComponent>();
		}
		return parameterComponents;
	}

	/**
	 * Getter for the {@link JTextArea} containing raw data
	 *
	 * @return The {@link JTextArea} containing raw data
	 */
	public JTextArea getRawData() {
		if (rawData == null) {
			rawData = new JTextArea(resourceDatalink.getRawData());
			rawData.setEditable(false);
		}
		return rawData;
	}

	/**
	 * Getter for the controller
	 *
	 * @return The controller
	 */
	public ResourceControl getControl() {
		return control;
	}

	/**
	 * Configure a {@link GridBagConstraints}.
	 *
	 * @param gbc The {@link GridBagConstraints} to configure.
	 * @param x The new gridx value.
	 * @param y The new gridy value.
	 */
	private void setGridBagParameters(GridBagConstraints gbc, int x, int y) {
		gbc.gridx = x;
		gbc.gridy = y;
		gbc.gridwidth = 1;
		gbc.insets = new Insets(3, 2, 4, 3);
		gbc.anchor = GridBagConstraints.WEST;
	}

	/**
	 * Create and configure a "line" of components with a {@link JLabel} and
	 * an other component.
	 *
	 * @param panel The panel where the line of components will be added.
	 * @param gbc The {@link GridBagConstraints} for the line.
	 * @param labelText The text to be set to the {@link JLabel}.
	 * @param toolTipText The tool tip text to be set to component and label.
	 * @param comp The first component.
	 * @param line The index of the line in the panel.
	 */
	private void createLine(JPanel panel, GridBagConstraints gbc,
			String labelText, String toolTipText, JComponent comp,
			int line) {
		setGridBagParameters(gbc, 0, line);
		gbc.weightx = 0.2;
		JLabel label = new JLabel(labelText + ':');
		panel.add(label, gbc);

		setGridBagParameters(gbc, 1, line);
		gbc.weightx = 1.0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		comp.setPreferredSize(new Dimension(360, 20));
		panel.add(comp, gbc);

		if (labelText != null && !labelText.isEmpty()) {
			label.setToolTipText(toolTipText);
			comp.setToolTipText(toolTipText);
		}
	}
}
