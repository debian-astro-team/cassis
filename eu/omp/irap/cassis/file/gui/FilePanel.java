/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui;

import java.awt.BorderLayout;
import java.io.File;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import eu.omp.irap.cassis.file.gui.cassisspectrum.CassisSpectrumPanel;
import eu.omp.irap.cassis.file.gui.medatada.CassisMetadataPanel;
import eu.omp.irap.cassis.file.gui.medatada.MetadataPanel;
import eu.omp.irap.cassis.file.gui.medatada.OriginalMetadataPanel;

/**
 * Create a FilePanel using CassisSpetrumPanel and MetadataPanel.
 *
 * @author Lea Foissac
 */
public class FilePanel extends JPanel {
	private static final long serialVersionUID = 7377223397972559420L;

	private FileControl control;
	private CassisSpectrumPanel cassisSpectrumPanel;
	private MetadataPanel metadataPanel;


	/**
	 * Constructor.
	 */
	public FilePanel() {
		super(new BorderLayout(15, 0));
		FileModel model = new FileModel();
		this.cassisSpectrumPanel = new CassisSpectrumPanel(model.getCassisSpectrumModel());
		this.metadataPanel = new MetadataPanel();
		this.control = new FileControl(this, model);
		add(cassisSpectrumPanel, BorderLayout.WEST);
		add(metadataPanel, BorderLayout.CENTER);
	}

	/**
	 * Getter to FileControl.
	 *
	 * @return the control FileControl
	 */
	public FileControl getControl() {
		return control;
	}

	/**
	 * Getter to CassisSPectrumPanel.
	 *
	 * @return the panel CassisSpectrumPanel
	 */
	public CassisSpectrumPanel getCassisSpectrumPanel() {
		return cassisSpectrumPanel;
	}

	/**
	 * Getter to MetadataPanel.
	 *
	 * @return the panel MetadataPanel
	 */
	public MetadataPanel getMetadataPanel() {
		return metadataPanel;
	}

	/**
	 * Getter to CassisMetadataPanel
	 *
	 * @return {@link CassisMetadataPanel}}
	 */
	public CassisMetadataPanel getCassisMetadataPanel() {
		return metadataPanel.getCassisMetadataPanel();
	}

	/**
	 * Getter to OriginalMetadataPanel
	 * @return {@link OriginalMetadataPanel}
	 */
	public OriginalMetadataPanel getOriginalMetadataPanel() {
		return metadataPanel.getOriginalMetadataPanel();
	}

	/**
	 * Open JOption pane to warning that there is no spectrum in resource selected.
	 */
	public void openNotSpectrumJOptionPane(String name) {
		JOptionPane.showMessageDialog(this,
				"We haven't detected a spectrum in " + name + " resource.",
				"No Cassis Spectrum in resource", JOptionPane.WARNING_MESSAGE);
	}

	/**
	 * Open JOption pane to warn that the resource is unread.
	 */
	public void openUnreadFileOptionPane(File file) {
		getCassisSpectrumPanel().getControl().setLastReadSuccessful(false);
		int option = JOptionPane.showConfirmDialog(this, "<html>" + file.getName() + " can't be read.<br>Would you like to open it in the ASCII parser ?</html>",
				"Unread resource", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
		if (option == JOptionPane.YES_OPTION) {
			getCassisSpectrumPanel().getControl().openAsciiParser(file, false);
		}
	}

	public void displayUnitError(boolean containsXUnitError, boolean containsYUnitError) {
		if (!containsXUnitError && !containsYUnitError) {
			return;
		}
		StringBuilder sb = new StringBuilder();
		sb.append("<html>");
		if (containsXUnitError && containsYUnitError) {
			sb.append("Unable to found or interpret wave and flux units.<br>");
		} else if (containsXUnitError && !containsYUnitError) {
			sb.append("Unable to found or interpret wave unit.<br>");
		} else if (!containsXUnitError && containsYUnitError) {
			sb.append("Unable to found or interpret flux unit.<br>");
		}
		sb.append("You can set the corrects units in the \"Cassis Metadata\" tab.");
		sb.append("</html>");
		JOptionPane.showMessageDialog(this, sb.toString(), "CASSIS warning unit", JOptionPane.WARNING_MESSAGE);
	}
}
