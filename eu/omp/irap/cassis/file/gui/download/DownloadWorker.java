/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.download;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;


/**
 * SwingWorker to download a file.
 *
 * @author M.Boiziot
 */
public class DownloadWorker extends SwingWorker<Void, Void> {

	private final static String TEMP_DIRECTORY = System.getProperty("java.io.tmpdir");
	private final static int BUFFER_SIZE = 8192;
	private DownloadGui gui;
	private String url;
	private String localPath;
	private boolean downloadError;


	/**
	 * Constructor.
	 *
	 * @param gui Graphical user interface associated to this download.
	 * @param url The URL of the file to download.
	 */
	public DownloadWorker(DownloadGui gui, String url) {
		this.gui = gui;
		this.url = url;
		this.downloadError = false;
	}

	/**
	 * Download the file in a background thread.
	 *
	 * @see javax.swing.SwingWorker#doInBackground()
	 */
	@Override
	protected Void doInBackground() throws Exception {
		try (FileDownloadHelper fdh = new FileDownloadHelper(this.url)) {
			InputStream is = fdh.download();

			localPath = getUniquePath(fdh.getFileName());

			try (FileOutputStream fos = new FileOutputStream(localPath)) {
				byte[] buffer = new byte[BUFFER_SIZE];
				int read = -1;
				long totalRead = 0;
				long size = fdh.getSize();

				while ((read = is.read(buffer)) != -1 && !isCancelled()) {
					fos.write(buffer, 0, read);
					if (size != -1) {
						totalRead += read;
						int currentProgress = (int)(totalRead * 100 / size);
						if (currentProgress >= 0 && currentProgress <= 100) {
							setProgress(currentProgress);
						}
					}
				}
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
			this.downloadError = true;
			setProgress(0);
			cancel(true);
		} finally {
			if (isCancelled() || downloadError) {
				File downloadedFile = new File(localPath);
				if (downloadedFile.exists()) {
					try {
						downloadedFile.delete();
					} catch (Exception e) {
						// Fail Silently.
					}
				}
			}
		}
		return null;
	}

	/**
	 * Display an error message in EDT in case of error.
	 *
	 * @see javax.swing.SwingWorker#done()
	 */
	@Override
	protected void done() {
		if (isCancelled() && downloadError) {
				JOptionPane.showMessageDialog(gui, "Error downloading file.",
						"Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Return the path of the downloaded file, should only be called once
	 *
	 * @return the path of the downloaded file.
	 */
	public String getLocalPath() {
		return localPath;
	}

	/**
	 * Return the extension of a file.
	 *
	 * @param filePath the path of the file.
	 * @return the extension.
	 */
	private static String getFileExtension(String filePath) {
		File tmpFile = new File(filePath);
		int dotIndex = tmpFile.getName().lastIndexOf('.');
		if (dotIndex > 0 && dotIndex <= tmpFile.getName().length() - 2) {
			return tmpFile.getName().substring(dotIndex + 1);
		}
		return "";
	}

	/**
	 * Get unique path to download a file.
	 *
	 * @param filename The original file name.
	 * @return The path with the eventual new file name.
	 */
	private static String getUniquePath(String filename) {
		File f = new File(TEMP_DIRECTORY + File.separatorChar + filename);
		if (f.exists()) {
			String ext = getFileExtension(filename);
			String newFileName = null;
			int val = 0;
			while (f.exists()) {
				val++;
				if (ext.isEmpty() || ext.length() > 10) {
					newFileName = filename + '_' + val;
				} else {
					int index = filename.indexOf(ext);
					newFileName = filename.substring(0, index - 1) + "_" + val + "." + ext;
				}
				f = new File(TEMP_DIRECTORY + File.separator + newFileName);
			}
		}
		return f.getPath();
	}
}
