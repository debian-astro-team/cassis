/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.download;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SwingWorker;
import javax.swing.SwingWorker.StateValue;

/**
 * Simple frame to download a file.
 *
 * @author M. Boiziot
 */
public class DownloadGui extends JFrame implements PropertyChangeListener {

	private static final long serialVersionUID = 6274556661572653220L;
	private JTextField urlTextField;
	private JButton downloadButton;
	private JButton cancelButton;
	private JProgressBar progressBar;
	private DownloadWorker downloadWorker;
	private Runnable finishAction;


	/**
	 * Constructor.
	 */
	public DownloadGui() {
		super("Open URL resource");
		JPanel panel = new JPanel(new BorderLayout());
		this.setContentPane(panel);

		JPanel topPanel = new JPanel();
		topPanel.add(new JLabel("URL: "));
		topPanel.add(getUrlTextField());

		JPanel centerPanel = new JPanel();
		centerPanel.add(getCancelButton());
		centerPanel.add(getDownloadButton());

		panel.add(topPanel, BorderLayout.NORTH);
		panel.add(centerPanel, BorderLayout.CENTER);
		panel.add(getProgressBar(), BorderLayout.SOUTH);

		addWindowsListener();
		pack();
		setLocationRelativeTo(null);
	}

	/**
	 * Check for progress and end of the worker.
	 *
	 * @param evt The event.
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals("progress")) {
			int progress = (Integer) evt.getNewValue();
			getProgressBar().setIndeterminate(false);
			getProgressBar().setValue(progress);
		} else if (evt.getPropertyName().equals("state") &&
				(SwingWorker.StateValue) evt.getNewValue() == SwingWorker.StateValue.DONE) {
			String path = downloadWorker.getLocalPath();
			downloadFinished(path);
		}
	}

	/**
	 * Create if necessary then return the URL {@link JTextField}.
	 *
	 * @return the URL {@link JTextField}.
	 */
	private JTextField getUrlTextField() {
		if (urlTextField == null) {
			urlTextField = new JTextField(30);
		}
		return urlTextField;
	}

	/**
	 * Create if necessary then return the "Download" {@link JButton}.
	 *
	 * @return the "Download" {@link JButton}.
	 */
	private JButton getDownloadButton() {
		if (downloadButton == null) {
			downloadButton = new JButton("Download");
			downloadButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (!getUrlTextField().getText().trim().isEmpty()) {
						startDownload(getUrlTextField().getText().trim());
					} else {
						JOptionPane.showMessageDialog(DownloadGui.this,
								"Please enter an URL first!", "No URL",
								JOptionPane.WARNING_MESSAGE);
					}
				}
			});
		}
		return downloadButton;
	}

	/**
	 * Create if necessary then return the "Cancel" {@link JButton}.
	 *
	 * @return the "Cancel" {@link JButton}.
	 */
	private JButton getCancelButton() {
		if (cancelButton == null) {
			cancelButton = new JButton("Cancel");
			cancelButton.setEnabled(false);
			cancelButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					cancelDownload();
				}
			});
		}
		return cancelButton;
	}

	/**
	 * Create if necessary then return the {@link JProgressBar}.
	 *
	 * @return the {@link JProgressBar}.
	 */
	private JProgressBar getProgressBar() {
		if (progressBar == null) {
			progressBar = new JProgressBar(0, 100);
			progressBar.setVisible(false);
			progressBar.setPreferredSize(new Dimension(200, 30));
			progressBar.setStringPainted(true);
			progressBar.setIndeterminate(true);
		}
		return progressBar;
	}

	/**
	 * Add a windows listener mainly to cancel download/avoid closing the
	 *  windows in case of download.
	 */
	private void addWindowsListener() {
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent windowEvent) {
				if (isDownloading()) {
					int response = JOptionPane.showConfirmDialog(DownloadGui.this,
							"A download is currently in progress. " +
							"Do you want to cancel the download and close the windows?",
							"Stop download?",
							JOptionPane.YES_NO_OPTION,
							JOptionPane.QUESTION_MESSAGE);
					if (response == JOptionPane.YES_OPTION) {
						downloadWorker.cancel(true);
						DownloadGui.this.dispose();
					}
				} else {
					DownloadGui.this.dispose();
				}
			}
		});
	}

	/**
	 * Return if there is a download in progress.
	 *
	 * @return true if there is a download in progress, false otherwise.
	 */
	private boolean isDownloading() {
		return downloadWorker != null &&
				downloadWorker.getState() == StateValue.STARTED;
	}

	/**
	 * Start the download of the given URL.
	 *
	 * @param url The URL of the file to download.
	 */
	private void startDownload(String url) {
		if (isDownloading()) {
			JOptionPane.showMessageDialog(this,
					"A download is already in progress.", "Error",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		if (downloadWorker != null) {
			downloadWorker.removePropertyChangeListener(this);
		}

		getProgressBar().setValue(0);
		getProgressBar().setVisible(true);
		pack();
		downloadWorker = new DownloadWorker(this, url);
		downloadWorker.addPropertyChangeListener(this);
		downloadWorker.execute();
		changeButtonStates(true, false);

	}

	/**
	 * Cancel the download in progress if there is one.
	 */
	private void cancelDownload() {
		if (downloadWorker == null) {
			return;
		}
		getProgressBar().setValue(0);
		downloadWorker.cancel(true);
		changeButtonStates(false, true);
		downloadWorker.removePropertyChangeListener(this);
		getProgressBar().setVisible(false);
		pack();
	}

	/**
	 * Change the enabled states of the Cancel and Download {@link JButton}.
	 *
	 * @param cancel The state to set for Cancel {@link JButton}.
	 * @param download The state to set for Download {@link JButton}.
	 */
	private void changeButtonStates(boolean cancel, boolean download) {
		getCancelButton().setEnabled(cancel);
		getDownloadButton().setEnabled(download);
	}

	/**
	 * Action to do once the download is over.
	 *
	 * @param path Local path of the downloaded file.
	 */
	private void downloadFinished(String path) {
		changeButtonStates(false, true);
		if (finishAction != null) {
			finishAction.run();
		}
		dispose();
	}

	/**
	 * Return the downloaded file.
	 *
	 * @return the downloaded file.
	 */
	public File getDownloadedFile() {
		return new File(downloadWorker.getLocalPath());
	}

	/**
	 * Set an action to do once the download of a file is finished.
	 *
	 * @param endAction The action to do once the download of a file is finished.
	 */
	public void setFinishAction(Runnable endAction) {
		this.finishAction = endAction;
	}

	/**
	 * Change the URL in the text field.
	 *
	 * @param url The new URL to set.
	 */
	public void setUrl(String url) {
		getUrlTextField().setText(url);
	}

	/**
	 * Start the download.
	 */
	public void startDownload() {
		getDownloadButton().doClick();
	}

}
