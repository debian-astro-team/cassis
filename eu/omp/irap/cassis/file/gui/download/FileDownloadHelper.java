/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.gui.download;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Class to help to Download a File from an URL.
 *
 * @author M. Boiziot
 */
public class FileDownloadHelper implements Closeable {

	private String urlFileToDownload;
	private String fileName;
	private String ext;
	private long size;
	private InputStream inputStream;
	private HttpURLConnection httpConnection;


	/**
	 * Constructor.
	 *
	 * @param urlFileToDownload The URL of the file to download.
	 */
	public FileDownloadHelper(String urlFileToDownload) {
		this.urlFileToDownload = urlFileToDownload;
	}

	/**
	 * Initialization of the download of a file by opening an HTTP Connection,
	 * parsing the header to get the size and file name then return the
	 * {@link InputStream} of the connection.
	 *
	 * @return The InputStream of the opened HTTP Connection.
	 * @throws IOException In case of error.
	 */
	public InputStream download() throws IOException {
		URL url = null;
		try {
			url = new URL(urlFileToDownload);
		} catch (MalformedURLException mue) {
			if (!urlFileToDownload.startsWith("http://") &&
					!urlFileToDownload.startsWith("https://")) {
				url = new URL("http://" + urlFileToDownload);
			}
		}
		if (url == null) {
			throw new MalformedURLException(urlFileToDownload + " is not a valid http URL.");
		}
		httpConnection = (HttpURLConnection) url.openConnection();
		httpConnection.setUseCaches(false);
		httpConnection.setDefaultUseCaches(false);
		if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
			size = httpConnection.getContentLengthLong();

			ext = getFileExtension(urlFileToDownload);
			fileName = getFileName(urlFileToDownload, httpConnection, ext);
			ext = getFileExtension(fileName);

			inputStream = httpConnection.getInputStream();
			return inputStream;
		}
		throw new IOException("Unable to download the file. Error code: " + httpConnection.getResponseCode());
	}

	/**
	 * Return the file name of the file to download given by the server.
	 * If the server do not return the name of the file, try from the URL.
	 * This function must only be called after {@link #download()}.
	 *
	 * @return The name of the file to download.
	 */
	public String getFileName() {
		if (fileName == null) {
			fileName = urlFileToDownload.substring(urlFileToDownload.lastIndexOf("/") + 1, urlFileToDownload.length());
		}
		return fileName;
	}

	/**
	 * Return the size of the file given by "content-length" http header.
	 * This function must only be called after {@link #download()}.
	 *
	 * @return the size of the file;
	 */
	public long getSize() {
		return size;
	}

	/**
	 * Close the connection.
	 *
	 * @see java.io.Closeable#close()
	 */
	@Override
	public void close() throws IOException {
		if (inputStream != null) {
			try {
				inputStream.close();
			} catch (IOException ioe) {
				// Do nothing.
			}
		}
		if (httpConnection != null) {
			httpConnection.disconnect();
		}
	}


	/**
	 * Return the extension of a file.
	 *
	 * @param filePath the path of the file.
	 * @return the extension.
	 */
	public static String getFileExtension(String filePath) {
		File tmpFile = new File(filePath);
		int dotIndex = tmpFile.getName().lastIndexOf('.');
		if (dotIndex > 0 && dotIndex <= tmpFile.getName().length() - 2) {
			return tmpFile.getName().substring(dotIndex + 1);
		}
		return "";
	}

	/**
	 * Search in HTTP header for a name to use for the file, if not present try with the URL.
	 *
	 * @param stringUrl The URL as a String.
	 * @param connection The {@link HttpURLConnection}.
	 * @param ext The extension of the file.
	 * @return The file name to use.
	 */
	private String getFileName(String stringUrl, HttpURLConnection connection,
			String ext) {
		String fileName;
		if (ext.isEmpty() || ext.startsWith("php") || ext.length() >= 8) {
			String header = connection.getHeaderField("Content-Disposition");
			if (header != null && header.indexOf('=') != -1) {
				fileName = header.split("=")[1].replaceAll("\"", "").replaceAll("[^a-zA-Z0-9.-]", "_");
			} else {
				fileName = new File(stringUrl).getName().replaceAll("[^a-zA-Z0-9.-]", "_");
			}
		} else {
			fileName = new File(stringUrl).getName();
		}
		return fileName;
	}

	/**
	 * Return the extension of the file.
	 *
	 * @return the extension of the file.
	 */
	public String getExtension() {
		return ext;
	}
}
