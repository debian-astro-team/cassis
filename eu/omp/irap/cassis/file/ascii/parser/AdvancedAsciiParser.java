/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.ascii.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.file.ascii.parser.configuration.AdvancedAsciiFileConfiguration;
import eu.omp.irap.cassis.file.ascii.parser.data.AdvancedAsciiDataColumn;
import eu.omp.irap.cassis.file.ascii.parser.data.ParsedAsciiFile;
import eu.omp.irap.cassis.file.ascii.parser.util.AdvancedAsciiParsingUtils;

/**
 * @author bastienkovac
 *
 * This class handles the parsing of a given ASCII file
 *
 */
public class AdvancedAsciiParser {

	private static final int MAX_LINES_PREVIEW = 100;

	private File inputFile;
	private ParsedAsciiFile outputFile;
	private AdvancedAsciiFileConfiguration configuration;
	private String metaSeparator, headerSeparator, dataSeparator;
	private int totalDataLine;
	private boolean shouldRedefineHeaders;


	/**
	 * Creates a new AdvancedAsciiParser
	 *
	 * @param inputFile The input File
	 * @param configuration The corresponding configuration
	 * @param outputFile The ParsedAsciiFile that will be modified during parsing
	 */
	public AdvancedAsciiParser(File inputFile, AdvancedAsciiFileConfiguration configuration, ParsedAsciiFile outputFile) {
		this.inputFile = inputFile;
		this.configuration = configuration;
		this.outputFile = outputFile;
		this.metaSeparator = this.configuration.getMetadataSeparator();
		this.headerSeparator = this.configuration.getHeaderSeparator();
		this.dataSeparator = this.configuration.getDataSeparator();
		this.totalDataLine = configuration.getEndData() - configuration.getStartData() + 1;
		for (AdvancedAsciiDataColumn col : this.outputFile.getDataColumns()) {
			col.resize(totalDataLine);
		}
		this.shouldRedefineHeaders = false;
	}

	/**
	 * Inform the parser that the header separator has been changed, so
	 * it needs to completely redefine the columns
	 */
	public void redefineHeaders() {
		this.shouldRedefineHeaders = true;
	}

	/**
	 * Handles the parsing of the input file by filling the output file and returning it
	 *
	 * @return The parsedFile
	 * @throws IllegalStateException if an error occured during parsing
	 */
	public ParsedAsciiFile parseFile() throws IllegalStateException {
		return executeSafeParsing(true);
	}

	/**
	 * Handles the parsing of the 100 first lines of the file : used only for preview
	 *
	 * @return The parsedFile
	 * @throws IllegalStateException if an error occured during parsing
	 */
	public ParsedAsciiFile previewParsing() throws IllegalStateException {
		return executeSafeParsing(false);
	}

	private ParsedAsciiFile executeSafeParsing(boolean fullyComplete) throws IllegalStateException {
		if (inputFile == null) {
			return outputFile;
		}
		outputFile.dataClear();
		if (shouldRedefineHeaders) {
			outputFile.fullClear();
			shouldRedefineHeaders = false;
		}
		try {
			parse(fullyComplete);
		} catch (Exception e) {
			throw new IllegalStateException(e.getMessage(), e.getCause());
		}
		return outputFile;
	}

	private void parse(boolean fullyComplete) throws IllegalStateException {
		int currentLine = 0;
		String line;
		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(new FileInputStream(inputFile), StandardCharsets.UTF_8))) {
			while ((line = br.readLine()) != null) {
				tryToReadMetadata(line, currentLine);
				tryToReadHeaders(line, currentLine);
				tryToReadData(line, currentLine);
				currentLine++;
				if (!fullyComplete && currentLine == MAX_LINES_PREVIEW) {
					return;
				}
			}
		} catch (IOException e) {
			throw new IllegalStateException();
		}
	}

	private void tryToReadMetadata(String line, int currentLine) {
		if (configuration.isMetadataPresent() && configuration.liesInsideMetadata(currentLine)) {
			readMetadata(line);
		}
	}

	private void tryToReadHeaders(String line, int currentLine) {
		if (configuration.isHeadersPresent() && outputFile.getNbColumns() != configuration.getNbColumns()) {
			if (configuration.liesInsideHeaders(currentLine)) {
				readHeader(line);
			}
		} else {
			buildDummyColumns();
		}
	}

	private void buildDummyColumns() {
		int i = 0;
		while (outputFile.getNbColumns() < configuration.getNbColumns()) {
			AdvancedAsciiDataColumn col = new AdvancedAsciiDataColumn("Column Unknown " + i, totalDataLine);
			col.setIndexColumn(i);
			outputFile.addAsciiDataColumn(col);
			i++;
		}
	}

	private void tryToReadData(String line, int currentLine) {
		if (!line.isEmpty()) {
			if (configuration.liesInsideData(currentLine)) {
				if (configuration.isAsColumns()) {
					readDataAsColumns(line);
				} else {
					outputFile.setColumnData(currentLine - configuration.getStartData(),
							readDataAsLines(line));
				}
			}
		}
	}

	private void readMetadata(String line) {
		List<String> splittedLine = AdvancedAsciiParsingUtils.readLine(line, this.metaSeparator);
		while (splittedLine.size() < 4) {
			splittedLine.add("");
		}
		buildMetadata(splittedLine);
	}

	private void buildMetadata(List<String> splittedLine) {
		CassisMetadata metadata = new CassisMetadata();
		String name = splittedLine.get(0);
		metadata.setName(name);
		String value = splittedLine.get(1);
		metadata.setValue(value);
		metadata.setUnit(splittedLine.get(2));
		metadata.setComment(splittedLine.get(3));
		outputFile.setVlsr(AdvancedAsciiParsingUtils.readVlsr(name, value));
		outputFile.setLoFrequency(AdvancedAsciiParsingUtils.readLoFrequency(name, value));
		outputFile.addCassisMetadata(metadata);
	}

	private void readHeader(String line) {
		List<String> splittedLine = AdvancedAsciiParsingUtils.readLine(line, this.headerSeparator);
		int count = 1;
		for (String header : splittedLine) {
			AdvancedAsciiDataColumn col = new AdvancedAsciiDataColumn(header.trim(), totalDataLine);
			if (!AdvancedAsciiParsingUtils.readUnit(header).equals(UNIT.UNKNOWN)) {
				col.setUnit(AdvancedAsciiParsingUtils.readUnit(header).getValString());
			}
			col.setIndexColumn(count - 1);
			outputFile.addAsciiDataColumn(col);
			if (count == configuration.getNbColumns())
				return;
			count++;
		}
	}

	private double[] readDataAsLines(String line) {
		List<String> splittedLine = AdvancedAsciiParsingUtils.readLine(line, this.dataSeparator);
		return AdvancedAsciiParsingUtils.asDataArray(splittedLine, configuration.getDefaultNanValue());
	}

	private void readDataAsColumns(String line) {
		List<String> splittedLine = AdvancedAsciiParsingUtils.readLine(line, this.dataSeparator);
		double[] dataLine = AdvancedAsciiParsingUtils.asDataArray(splittedLine, configuration.getDefaultNanValue());
		for (int i = 0; i < configuration.getNbColumns(); i++) {
			try {
				outputFile.addColumnData(i, dataLine[i]);
			} catch (IndexOutOfBoundsException e) {
				return;
			}
		}
	}

}
