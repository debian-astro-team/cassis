/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.ascii.parser.data.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import eu.omp.irap.cassis.file.ascii.parser.AdvancedAsciiControl;
import eu.omp.irap.cassis.file.ascii.parser.data.ParsedAsciiFile;

/**
 * @author bastienkovac
 *
 * A JPanel used to preview the parsing of a file
 *
 */
public class PreviewDataPanel extends JPanel {

	/**
	 *
	 */
	private static final long serialVersionUID = 7206927895856314118L;
	private MetadataTable metadataTable;
	private ParsingDataTable dataTable;
	private JPanel dataPanel, metadataPanel;
	private AdvancedAsciiControl control;
	private JTextField title;


	/**
	 * Creates a new PreviewDataPanel
	 *
	 * @param control The controller
	 */
	public PreviewDataPanel(AdvancedAsciiControl control) {
		super();
		this.control = control;
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Preview"),
				BorderFactory.createEmptyBorder(10, 10, 10, 10)));
		JPanel innerLayout = new JPanel();
		innerLayout.setToolTipText("<html>This preview only shows the first 100 lines of data.<br>The file will be fully parsed when exporting the spectrum.</html>");
		innerLayout.setLayout(new BoxLayout(innerLayout, BoxLayout.Y_AXIS));
		innerLayout.add(getMetadataPanel());
		innerLayout.add(getDataPanel());
		add(innerLayout);
	}

	/**
	 * @return A {@link ParsingDataTable} that displays the first few lines of the parsed file for the user
	 * to check if the configuration is correct
	 */
	public ParsingDataTable getDataTable() {
		if (dataTable == null) {
			dataTable = new ParsingDataTable();
			dataTable.getTableHeader().setReorderingAllowed(false);
			dataTable.setName("Data preview");
		}
		return dataTable;
	}

	/**
	 * @return A {@link JTable} that displays the metadata read from the file by the parsing module
	 */
	public MetadataTable getMetadataTable() {
		if (metadataTable == null) {
			metadataTable = new MetadataTable(control);
			metadataTable.setName("Metadata preview");
		}
		return metadataTable;
	}

	/**
	 * Update the model from the DataTable to display a new given parsed file
	 *
	 * @param newParsedFile The new parsed file
	 */
	public void updateDataModel(ParsedAsciiFile newParsedFile) {
		getDataTable().updateParsedFile(newParsedFile);
	}

	/**
	 * Update the model from the MetadataTable to display the metadata from a new given parsed file
	 *
	 * @param newParsedFile The new parsed file
	 */
	public void updateMetadataModel(ParsedAsciiFile newParsedFile) {
		getMetadataTable().updateModel(newParsedFile);
	}

	/**
	 * Clears both tables
	 */
	public void resetTables() {
		getDataTable().updateParsedFile(new ParsedAsciiFile());
		getMetadataTable().reset();
	}

	/**
	 * @return A JTextField that allows the edition of the spectrum's title
	 */
	public JTextField getTitle() {
		if (title == null) {
			title = new JTextField();
			title.setColumns(20);
			title.getDocument().addDocumentListener(new DocumentListener() {

				@Override
				public void removeUpdate(DocumentEvent e) {
					control.changeSpectrumTitle(title.getText());
				}

				@Override
				public void insertUpdate(DocumentEvent e) {
					control.changeSpectrumTitle(title.getText());
				}

				@Override
				public void changedUpdate(DocumentEvent e) {
					control.changeSpectrumTitle(title.getText());
				}
			});
		}
		return title;
	}

	private JPanel getDataPanel() {
		if (dataPanel == null) {
			dataPanel = new JPanel(new BorderLayout());
			dataPanel.setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Data"),
					BorderFactory.createEmptyBorder(10, 10, 10, 10)));
			dataPanel.add(new JScrollPane(getDataTable()));
		}
		return dataPanel;
	}

	private JPanel getMetadataPanel() {
		if (metadataPanel == null) {
			metadataPanel = new JPanel();
			metadataPanel.setLayout(new BoxLayout(metadataPanel, BoxLayout.Y_AXIS));
			metadataPanel.setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Metadata"),
					BorderFactory.createEmptyBorder(10, 10, 10, 10)));

			JPanel tmp = new JPanel(new FlowLayout(FlowLayout.LEFT));
			tmp.add(new JLabel("Spectrum's title:"));
			tmp.add(Box.createHorizontalStrut(20));
			tmp.add(getTitle());

			metadataPanel.add(tmp);
			final JScrollPane scroll = new JScrollPane(getMetadataTable());
			scroll.setPreferredSize(new Dimension(scroll.getPreferredSize().width, getMetadataTable().getRowHeight() * 6));
			scroll.addMouseListener(new MouseAdapter() {

				@Override
				public void mouseReleased(MouseEvent e) {
					if (SwingUtilities.isRightMouseButton(e)) {
						getMetadataTable().getContextMenu().show(scroll, e.getX(), e.getY());
					}
				}
			});
			metadataPanel.add(scroll);
		}
		return metadataPanel;
	}

}
