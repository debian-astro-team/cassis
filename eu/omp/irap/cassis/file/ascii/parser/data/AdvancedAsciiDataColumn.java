/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.ascii.parser.data;

import java.util.Arrays;

import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.axes.UNIT;

/**
 * @author bastienkovac
 *
 * This class represents a column of data that can be used to build a {@link CassisSpectrum}
 *
 */
public class AdvancedAsciiDataColumn {

	private String name;
	private String unit;
	private double[] dataArray;	// Using primitive array to reduce memory and CPU overhead /!\ Initial capacity is very important
	private int indexColumn;
	private int lastIndex;


	/**
	 * Creates an AdvancedAsciiDataColumn with unknown name, unit and no data
	 */
	public AdvancedAsciiDataColumn() {
		this("Column Unknown");
	}

	/**
	 * Creates an AdvancedAsciiDataColumn with given name, unknown unit, and no data
	 *
	 * @param name Name of the column
	 */
	public AdvancedAsciiDataColumn(String name) {
		this(name, 1);
	}

	/**
	 * Creates an AdvancedAsciiDataColumn with given initial capacity (reduces time complexity
	 * when adding data)
	 *
	 * @param name The name of the column
	 * @param initialCapacity The initial capacity of this column
	 */
	public AdvancedAsciiDataColumn(String name, int initialCapacity) {
		this.name = name;
		this.unit = UNIT.UNKNOWN.getValString();
		this.dataArray = new double[initialCapacity];
		lastIndex = 0;
	}

	/**
	 * @param index The position of the column in the parsed file
	 */
	public void setIndexColumn(int index) {
		this.indexColumn = index;
	}

	/**
	 * @return The position of the column in the parsed file
	 */
	public int getIndexColumn() {
		return indexColumn;
	}

	/**
	 * @return The data of the AsciiDataColumn as an array of double
	 */
	public double[] getDataAsArray() {
		return dataArray;
	}

	/**
	 * Sets the name of the column
	 *
	 * @param name
	 *            The name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Sets the unit of the column
	 *
	 * @param unit
	 *            The unit
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * @param data
	 *            The data
	 */
	public void setData(double[] data) {
		this.dataArray = data;
		lastIndex = data.length;
	}

	/**
	 * Replace the data at the given index by the given data. Does nothing if index >= dataArray.length
	 *
	 * @param index Index of the data to replace
	 * @param data New data value
	 */
	public void setDataAt(int index, double data) {
		if (index < dataArray.length) {
			this.dataArray[index] = data;
		}
	}

	/**
	 * Adds data to the column. If the column's initial capacity isn't enough, the array is resized, which can
	 * induce overhead (Array size is doubled)
	 *
	 * @param data
	 *            The data
	 */
	public void addData(double data) {
		if (lastIndex >= this.dataArray.length) {
			this.dataArray = Arrays.copyOf(this.dataArray, this.dataArray.length * 2);
		}
		this.dataArray[lastIndex] = data;
		lastIndex++;
	}

	/**
	 * Resizes the internal array to match the new size
	 *
	 * @param newSize The new size of the array
	 */
	public void resize(int newSize) {
		this.dataArray = Arrays.copyOf(this.dataArray, newSize);
	}

	/**
	 * Clears the column's data
	 */
	public void clearData() {
		this.dataArray = new double[dataArray.length];
		lastIndex = 0;
	}

	/**
	 * Return the name of the column
	 *
	 * @return The name of the column
	 */
	public String getName() {
		return name;
	}

	/**
	 * Return the unit of the column
	 *
	 * @return The unit of the column
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * @return The effective size of the data array
	 */
	public int getSize() {
		return lastIndex;
	}

	@Override
	public String toString() {
		return name;
	}

	/**
	 * Return the value from the given index (or row) of the column
	 *
	 * @param index : The index
	 * @return The data value from the given index
	 */
	public Double getData(int index) {
		return this.dataArray[index];
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataArray == null) ? 0 : Arrays.hashCode(dataArray));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((unit == null) ? 0 : unit.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AdvancedAsciiDataColumn other = (AdvancedAsciiDataColumn) obj;
		if (indexColumn != other.getIndexColumn()) {
			return false;
		}
		return true;
	}

}
