/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.ascii.parser.data.gui;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.file.ascii.parser.AdvancedAsciiControl;
import eu.omp.irap.cassis.file.ascii.parser.configuration.gui.SpectrumGenerationPanel;

/**
 * @author bastienkovac
 *
 * JPanel handling different action for the parsing module
 *
 */
public class ButtonPanel extends JPanel {

	/**
	 *
	 */
	private static final long serialVersionUID = -6048340417079983173L;

	private JButton exportSpectrum, actionButton;
	private JButton importFile;
	private JButton visualizeFile;
	private JButton saveConfiguration, loadConfiguration;
	private AdvancedAsciiControl control;


	/**
	 * Creates a new ButtonPanel
	 *
	 * @param control The controller
	 */
	public ButtonPanel(AdvancedAsciiControl control) {
		super(new GridLayout(1, 2));
		this.control = control;
		JPanel right = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		right.add(getExportSpectrumButton());
		if (control.getModel().isIntegrated()) {
			right.add(getActionButton());
		}
		JPanel left = new JPanel(new FlowLayout(FlowLayout.LEFT));
		left.add(getImportFile());
		left.add(getVisualizeFile());
		left.add(getSaveConfiguration());
		left.add(getLoadConfiguration());
		add(left);
		add(right);
	}

	/**
	 * @return A JButton that exports a {@link CassisSpectrum} using the selected columns
	 * from the {@link SpectrumGenerationPanel}
	 */
	public JButton getExportSpectrumButton() {
		if (exportSpectrum == null) {
			exportSpectrum = new JButton("Export to spectrum manager");
			exportSpectrum.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					control.runExportAction();
				}
			});
		}
		return exportSpectrum;
	}

	public JButton getActionButton() {
		if (actionButton == null) {
			actionButton = new JButton("Display spectrum");
			actionButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					control.runSpectrumManagerAction();
				}
			});
		}
		return actionButton;
	}

	/**
	 * @return A {@link JButton} used to import a file to parse
	 */
	public JButton getImportFile() {
		if (importFile == null) {
			importFile = new JButton("Open file");
			importFile.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					control.importFile();
				}
			});
		}
		return importFile;
	}

	/**
	 * @return A {@link JButton} used to visualize the loaded file
	 */
	public JButton getVisualizeFile() {
		if (visualizeFile == null) {
			visualizeFile = new JButton("Visualize raw data");
			visualizeFile.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					control.visualizeLoadedFile();
				}
			});
		}
		return visualizeFile;
	}

	/**
	 * @return A {@link JButton} used to save the current configuration
 	 */
	public JButton getSaveConfiguration() {
		if (saveConfiguration == null) {
			saveConfiguration = new JButton("Save configuration");
			saveConfiguration.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					control.saveConfiguration();
				}
			});
		}
		return saveConfiguration;
	}

	/**
	 * @return A {@link JButton} used to load the current configuration
 	 */
	public JButton getLoadConfiguration() {
		if (loadConfiguration == null) {
			loadConfiguration = new JButton("Load configuration");
			loadConfiguration.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					control.loadConfiguration();
				}
			});
		}
		return loadConfiguration;
	}

}
