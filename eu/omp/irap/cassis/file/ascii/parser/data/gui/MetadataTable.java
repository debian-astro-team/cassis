/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.ascii.parser.data.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.table.AbstractTableModel;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.file.ascii.parser.AdvancedAsciiControl;
import eu.omp.irap.cassis.file.ascii.parser.data.ParsedAsciiFile;

/**
 * @author bastienkovac
 *
 * JTable used to display and edit the parsed metadata
 *
 */
@SuppressWarnings("serial")
public class MetadataTable extends JTable {

	private static final String[] COLUMN_NAMES = new String[] {"Name", "Value", "Unit", "Comment"};
	private final AdvancedAsciiControl control;
	private JPopupMenu contextMenu;

	/**
	 * Creates a new MetadataTable
	 *
	 * @param control The controller
	 */
	public MetadataTable(AdvancedAsciiControl control) {
		super(new MetadataTableModel());
		this.control = control;
		getTableHeader().setReorderingAllowed(false);
		addDeleteKey();
	}

	/**
	 * Updates the underlying model with the new data
	 *
	 * @param newParsedFile The newly parsed file
	 */
	public void updateModel(ParsedAsciiFile newParsedFile) {
		((MetadataTableModel) getModel()).updateModel(newParsedFile);
	}

	/**
	 * Resets the JTable model
	 */
	public void reset() {
		setModel(new MetadataTableModel());
	}

	/**
	 * Creates if needed then returns a context menu used to add a Metadata to the model
	 *
	 * @return The JPopupMenu
	 */
	public JPopupMenu getContextMenu() {
		if (contextMenu == null) {
			contextMenu = new JPopupMenu();
			JMenuItem addMetadata = new JMenuItem("Add new metadata");
			addMetadata.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					control.getModel().addMetadata(new CassisMetadata());
				}
			});
			contextMenu.add(addMetadata);
		}
		return contextMenu;
	}

	private void addDeleteKey() {
		InputMap inputMap = getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		ActionMap actionMap = getActionMap();

		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "delete");
		actionMap.put("delete", new AbstractAction() {

			@Override
			public void actionPerformed(ActionEvent e) {
				control.getModel().removeMetadata(getSelectedRows());
			}
		});
	}

	/**
	 * @author bastienkovac
	 *
	 * TableModel used to display the parsed metadata
	 *
	 */
	private static class MetadataTableModel extends AbstractTableModel {

		private ParsedAsciiFile parsedFile;


		/**
		 * Creates a new MetadataTableModel with an empty ParsedFile
		 */
		public MetadataTableModel() {
			this(new ParsedAsciiFile());
		}

		/**
		 * Creates a new MetadataTableModel from a given parsed file
		 *
		 * @param parsedFile The parsedFile from which the metadata will be displayed
		 */
		public MetadataTableModel(ParsedAsciiFile parsedFile) {
			this.parsedFile = parsedFile;
		}

		/**
		 * Updates the underlying model with the new data
		 *
		 * @param newParsedFile The newly parsed file
		 */
		public void updateModel(ParsedAsciiFile newParsedFile) {
			this.parsedFile = newParsedFile;
			fireTableStructureChanged();
		}

		@Override
		public int getRowCount() {
			return this.parsedFile.getListCassisMetadata().size();
		}

		@Override
		public int getColumnCount() {
			return COLUMN_NAMES.length;
		}

		@Override
		public String getColumnName(int col) {
			return COLUMN_NAMES[col];
		}

		@Override
		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return true;
		}

		@Override
		public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
			super.setValueAt(aValue, rowIndex, columnIndex);
			CassisMetadata metadata = this.parsedFile.getListCassisMetadata().get(rowIndex);
			switch (columnIndex) {
			case 0:
				metadata.setName((String) aValue);
				break;
			case 1:
				metadata.setValue((String) aValue);
				break;
			case 2:
				metadata.setUnit((String) aValue);
				break;
			case 3:
				metadata.setComment((String) aValue);
				break;
			default:
				break;
			}
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			CassisMetadata metadata = this.parsedFile.getListCassisMetadata().get(rowIndex);
			switch (columnIndex) {
			case 0 :
				return metadata.getName();
			case 1 :
				return metadata.getValue();
			case 2 :
				return metadata.getUnit();
			case 3 :
				return metadata.getComment();
			default :
				return null;
			}
		}

	}

}
