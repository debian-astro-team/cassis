/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.ascii.parser.data.gui;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;

import eu.omp.irap.cassis.file.ascii.parser.data.AdvancedAsciiDataColumn;
import eu.omp.irap.cassis.file.ascii.parser.data.ParsedAsciiFile;

/**
 * @author bastienkovac
 *
 * JTable handling the parsing's preview
 *
 */
public class ParsingDataTable extends JTable {

	/**
	 *
	 */
	private static final long serialVersionUID = -7445866123415118844L;
	private ParsedAsciiFile parsedFile;


	/**
	 * Creates a new ParsingDataTable with an empty ParsedFile
	 */
	public ParsingDataTable() {
		super();
		setModel(new ParsingDataTableModel());
		getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				setRowHeight(getRowHeight());
				for (int row : getSelectedRows()) {
					setRowHeight(row, 20);
				}
			}
		});
	}

	/**
	 * Updates the underlying model
	 *
	 * @param newParsedFile the parsedFile
	 */
	public void updateParsedFile(ParsedAsciiFile newParsedFile) {
		this.parsedFile = newParsedFile;
		((ParsingDataTableModel) getModel()).fireTableStructureChanged();
		updateColumnSize();
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return getValueAt(row, column).equals(Double.NaN);
	}

	@Override
	public boolean getScrollableTracksViewportWidth() {
		return getPreferredSize().width < getParent().getWidth();
	}

	private void updateColumnSize() {
		int width;
		Component comp;
		for (int column = 0; column < getColumnCount(); column++) {
			comp = getTableHeader().getDefaultRenderer().getTableCellRendererComponent(this,
					getColumnModel().getColumn(column).getHeaderValue(), false, false, 0, 0);
			width = comp.getPreferredSize().width;
			for (int row = 0; row < getRowCount(); row++) {
				TableCellRenderer renderer = getCellRenderer(row, column);
				comp = prepareRenderer(renderer, row, column);
				width = Math.max(comp.getPreferredSize().width, width) + getIntercellSpacing().width;
			}
			getColumnModel().getColumn(column).setPreferredWidth(width);
		}
	}

	private class ParsingDataTableModel extends AbstractTableModel {

		/**
		 *
		 */
		private static final long serialVersionUID = 1825087108293765580L;

		private static final int MAX_ROWS = 100;


		@Override
		public int getRowCount() {
			if (parsedFile == null) {
				return 0;
			}
			return MAX_ROWS <= parsedFile.getMaxRowSize() ? MAX_ROWS : parsedFile.getMaxRowSize();
		}

		@Override
		public int getColumnCount() {
			return parsedFile == null ? 0 : parsedFile.getNbColumns();
		}

		@Override
		public String getColumnName(int col) {
			AdvancedAsciiDataColumn column = parsedFile.getAsciiDataColumn(col);
			return column.getName() + " (" + column.getUnit() + ")";
		}

		@Override
		public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
			super.setValueAt(aValue, rowIndex, columnIndex);
			double newValue;
			try {
				newValue = Double.parseDouble((String) aValue);
			} catch (NumberFormatException e) {
				newValue = Double.NaN;
			}
			parsedFile.getAsciiDataColumn(columnIndex).setDataAt(rowIndex, newValue);
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			return parsedFile.getAsciiDataColumn(columnIndex).getData(rowIndex);
		}

	}

}
