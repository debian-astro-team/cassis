/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.ascii.parser.data;

import java.util.ArrayList;
import java.util.List;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.YAxisCassis;

/**
 * @author bastienkovac
 *
 * This class represents a parsed ASCII file. It holds a list of {@link CassisMetadata} and a list of A
 * {@link AdvancedAsciiDataColumn}
 *
 */
public class ParsedAsciiFile {

	private List<CassisMetadata> listCassisMetadata;
	private List<AdvancedAsciiDataColumn> columns;
	private double loFrequency, vlsr;
	private String fileTitle;


	/**
	 * Create a new ParsedAsciiFile
	 */
	public ParsedAsciiFile() {
		this.columns = new ArrayList<>();
		this.listCassisMetadata = new ArrayList<>();
	}

	/**
	 * Add a {@link CassisMetadata} to the underlying list
	 *
	 * @param cassisMetadata
	 *            The cassisMetadata to add
	 * @return If the metadata was properly added
	 */
	public boolean addCassisMetadata(CassisMetadata cassisMetadata) {
		return listCassisMetadata.add(cassisMetadata);
	}

	/**
	 * @return The list of {@link CassisMetadata}
	 */
	public List<CassisMetadata> getListCassisMetadata() {
		return listCassisMetadata;
	}

	/**
	 * Clear the parsedFile, only removing the columns data but keeping their structure
	 */
	public void dataClear() {
		this.listCassisMetadata.clear();
		for (AdvancedAsciiDataColumn column : columns) {
			column.clearData();
		}
	}

	/**
	 * Completely clears the parsedFile, completely removing the columns structure
	 */
	public void fullClear() {
		this.listCassisMetadata.clear();
		this.columns.clear();
	}

	/**
	 * Adds an {@link AdvancedAsciiDataColumn} to the parsed file
	 *
	 * @param dataColumn
	 *            The AsciiDataColumn to add
	 */
	public void addAsciiDataColumn(AdvancedAsciiDataColumn dataColumn) {
		columns.add(dataColumn);
	}

	/**
	 * @param index The index of the column
	 * @return The dataColumn stored at the given index
	 */
	public AdvancedAsciiDataColumn getAsciiDataColumn(int index) {
		return columns.get(index);
	}

	/**
	 * @param index The index of the column
	 * @param newDataColumn The column with which to replace it
	 *
	 * Replace the dataColumn stored at the given index by the given newDataColumn
	 */
	public void updateDataColumn(int index, AdvancedAsciiDataColumn newDataColumn) {
		this.columns.set(index, newDataColumn);
	}

	/**
	 * Adds the given value to the given Column
	 *
	 * @param index Index of the column
	 * @param value Value to add
	 */
	public void addColumnData(int index, double value) {
		this.columns.get(index).addData(value);
	}

	/**
	 * @return All the {@link AdvancedAsciiDataColumn} held by the ParsedAsciiFile
	 */
	public List<AdvancedAsciiDataColumn> getDataColumns() {
		return columns;
	}

	/**
	 * @return The number of columns currently held by the ParsedAsciiFile
	 */
	public int getNbColumns() {
		return columns.size();
	}

	/**
	 * @param loFrequency
	 * Sets the loFrequency that will be used when creating {@link CassisSpectrum}
	 */
	public void setLoFrequency(double loFrequency) {
		this.loFrequency = loFrequency;
	}

	/**
	 * @param vlsr
	 * Sets the vlsr that will be used when creating {@link CassisSpectrum}
	 */
	public void setVlsr(double vlsr) {
		this.vlsr = vlsr;
	}

	/**
	 * @param fileTitle
	 * Sets the fileTitle that will be used when creating {@link CassisSpectrum}
	 */
	public void setFileTitle(String fileTitle) {
		this.fileTitle = fileTitle;
	}

	/**
	 * @return The title of the file
	 */
	public String getFileTitle() {
		return fileTitle;
	}

	/**
	 * @return The maximum amount of rows held by the dataColumns of the file
	 */
	public int getMaxRowSize() {
		int max = 0;
		for (AdvancedAsciiDataColumn col : this.columns) {
			if (col.getSize() > max) {
				max = col.getSize();
			}
		}
		return max;
	}

	/**
	 * @param index Index of the column
	 * @param data Data to set
	 *
	 * Store the given data list in the dataColumn stored at the given index
	 */
	public void setColumnData(int index, double[] data) {
		columns.get(index).setData(data);
	}

	/**
	 * @param waveColumn The column to use as wave
	 * @param fluxColumn The column to use as flux
	 * @return A CassisSpectrum built from a given waveColumn and a given fluxColumn
	 * @throws IllegalArgumentException If the two columns don't have the same number of values
	 */
	public CassisSpectrum buildCassisSpectrum(AdvancedAsciiDataColumn waveColumn, AdvancedAsciiDataColumn fluxColumn) throws IllegalArgumentException {
		if (waveColumn == null || fluxColumn == null) {
			throw new IllegalArgumentException("Please select a wave and a flux column");
		}
		if (waveColumn.getSize() != fluxColumn.getSize() || waveColumn.getSize() <= 3
				|| UNIT.toUnit(waveColumn.getUnit()) == UNIT.UNKNOWN) {
			throw new IllegalArgumentException("Please check that both selected columns are of same length and that the "
					+ "wave unit is defined");
		}
		XAxisCassis xAxis = XAxisCassis.getXAxisCassis(UNIT.toUnit(waveColumn.getUnit()));
		YAxisCassis yAxis = YAxisCassis.getYAxisCassis(fluxColumn.getUnit(), fluxColumn.getName());
		double[] wave = waveColumn.getDataAsArray();
		double[] flux = fluxColumn.getDataAsArray();
		CassisSpectrum spectrum = CassisSpectrum.generateCassisSpectrumFromOriginUnit(fileTitle, wave, flux, xAxis, yAxis);
		spectrum.addCassisMetadata(new CassisMetadata("vlsr", String.valueOf(vlsr), "", ""), true);
		spectrum.addCassisMetadata(new CassisMetadata("lofreq", String.valueOf(loFrequency), "", ""), true);
		return spectrum;
	}

	@Override
	public String toString() {
		StringBuilder build = new StringBuilder("Metadata\n");
		for (CassisMetadata metadata : listCassisMetadata) {
			build.append('\t').append(metadata).append('\n');
		}
		build.append(columns.size()).append(" columns of data :\n");
		for (AdvancedAsciiDataColumn col : columns) {
			build.append('\t').append(col);
			build.append(" (").append(col.getUnit()).append(") - Number of lines : ");
			build.append(col.getSize());
			build.append("\n\t\t");
			for (int i = 0 ; i < 10 ; i++) {
				build.append(col.getData(i));
				if (i != 9)
					build.append(", ");
			}
			build.append("...\n");
		}
		return build.toString();
	}

}
