/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.ascii.parser;

import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import eu.omp.irap.cassis.file.ascii.parser.configuration.gui.ConfigurationPanel;
import eu.omp.irap.cassis.file.ascii.parser.data.gui.ButtonPanel;
import eu.omp.irap.cassis.file.ascii.parser.data.gui.PreviewDataPanel;

/**
 * @author bastienkovac
 *
 * Main JPanel of the parsing module's GUI
 *
 */
public class AdvancedAsciiPanel extends JPanel {

	/**
	 *
	 */
	private static final long serialVersionUID = -7372749836196496048L;

	private AdvancedAsciiControl control;
	private PreviewDataPanel dataPreview;

	private ConfigurationPanel configurationPanel;
	private ButtonPanel buttonPanel;


	/**
	 * Creates a new AdvancedAsciiPanel
	 *
	 * @param model The model
	 * @param standalone If the module is used separately, a JMenuBar replaces the ButtonPanel of the GUI
	 */
	public AdvancedAsciiPanel(AdvancedAsciiModel model, boolean standalone) {
		super(new BorderLayout());
		this.control = new AdvancedAsciiControl(this, model);
		setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		if (!standalone) {
			add(getButtonPanel(), BorderLayout.SOUTH);
		}
		add(getConfigurationPanel(), BorderLayout.WEST);
		add(getDataPreview(), BorderLayout.CENTER);
	}

	/**
	 * @return The {@link ConfigurationPanel}
	 */
	public ConfigurationPanel getConfigurationPanel() {
		if (configurationPanel == null) {
			configurationPanel = new ConfigurationPanel(control);
		}
		return configurationPanel;
	}

	/**
	 * @return The {@link ButtonPanel}
	 */
	public ButtonPanel getButtonPanel() {
		if (buttonPanel == null) {
			buttonPanel = new ButtonPanel(control);
		}
		return buttonPanel;
	}

	/**
	 * @return The {@link PreviewDataPanel}
	 */
	public PreviewDataPanel getDataPreview() {
		if (dataPreview == null) {
			dataPreview = new PreviewDataPanel(control);
		}
		return dataPreview;
	}

	/**
	 * @return The controller
	 */
	public AdvancedAsciiControl getControl() {
		return control;
	}

}
