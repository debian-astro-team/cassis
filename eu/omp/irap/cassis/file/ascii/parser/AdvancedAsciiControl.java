/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.ascii.parser;

import java.awt.Frame;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.common.events.ModelListener;
import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.file.ascii.parser.configuration.AdvancedAsciiFileConfiguration;
import eu.omp.irap.cassis.file.ascii.parser.configuration.gui.DataConfigurationPanel;
import eu.omp.irap.cassis.file.ascii.parser.configuration.gui.HeaderConfigurationPanel;
import eu.omp.irap.cassis.file.ascii.parser.configuration.gui.MetadataConfigurationPanel;
import eu.omp.irap.cassis.file.ascii.parser.configuration.gui.SeparatorComboBox;
import eu.omp.irap.cassis.file.ascii.parser.configuration.gui.SpectrumGenerationPanel;
import eu.omp.irap.cassis.file.ascii.parser.data.AdvancedAsciiDataColumn;
import eu.omp.irap.cassis.file.ascii.parser.util.SpectrumSaver;
import eu.omp.irap.cassis.file.ascii.parser.util.gui.EventSorter;
import eu.omp.irap.cassis.file.ascii.parser.util.gui.EventSorter.EventType;
import eu.omp.irap.cassis.file.ascii.parser.util.gui.RawFileFrame;

/**
 * @author bastienkovac
 *
 * Controller for the parsing module's GUI
 *
 */
public class AdvancedAsciiControl implements ModelListener {

	private AdvancedAsciiModel model;
	private AdvancedAsciiPanel view;


	/**
	 * Creates a new AdvancedAsciiControl
	 *
	 * @param view The view
	 * @param model The model
	 */
	public AdvancedAsciiControl(AdvancedAsciiPanel view, AdvancedAsciiModel model) {
		this.model = model;
		this.model.addModelListener(this);
		this.view = view;
	}

	/**
	 * @return The underlying {@link AdvancedAsciiModel}
	 */
	public AdvancedAsciiModel getModel() {
		return model;
	}

	/**
	 * Changes the orientation of data in the underlying configuration
	 * and starts a preview parsing
	 *
	 * @param asColumns True if the data is organized as columns, false otherwise
	 */
	public void configureDataOrientation(boolean asColumns) {
		if (model.getConfiguration().isAsColumns() == asColumns) {
			return;
		}
		model.setAsColumns(asColumns);
		model.previewParsing();
	}

	/**
	 * Changes the number of columns in the underlying configuration
	 * and starts a preview parsing
	 *
	 * @param newNbColumns The new number of columns
	 */
	public void configureDataNbColumns(int newNbColumns) {
		if (model.getConfiguration().getNbColumns() == newNbColumns) {
			return;
		}
		model.setNbColumns(newNbColumns);
		model.previewParsing();
	}

	/**
	 * Changes the data separator in the underlying configuration
	 * and starts a preview parsing
	 *
	 * @param newSeparator The new data separator
	 */
	public void configureDataSeparator(String newSeparator) {
		if (model.getConfiguration().getDataSeparator().equals(newSeparator)) {
			return;
		}
		model.setDataSeparator(newSeparator);
		model.previewParsing();
	}

	/**
	 * Changes the first line of data in the underlying configuration
	 * and starts a preview parsing
	 *
	 * @param newStart The new first line of data
	 */
	public void configureDataStart(int newStart) {
		if (model.getConfiguration().getStartData() == newStart) {
			return;
		}
		model.setStartData(newStart);
		model.previewParsing();
	}

	/**
	 * Changes the last line of data in the underlying configuration
	 * and starts a preview parsing
	 *
	 * @param newEnd The new last line of data
	 */
	public void configureDataEnd(int newEnd) {
		if (model.getConfiguration().getEndData() == newEnd) {
			return;
		}
		model.setEndData(newEnd);
		model.previewParsing();
	}

	/**
	 * Changes the presence or absence of the headers in the underlying configuration
	 * and starts a preview parsing
	 *
	 * @param headersPresent True if the file contains headers, false otherwise
	 */
	public void configureHeadersPresent(boolean headersPresent) {
		if (model.getConfiguration().isHeadersPresent() == headersPresent) {
			return;
		}
		model.setHeadersPresent(headersPresent);
		model.previewParsing();
	}

	/**
	 * Changes the header separator in the underlying configuration
	 * and starts a preview parsing
	 *
	 * @param newSeparator The new headers separator
	 */
	public void configureHeadersSeparator(String newSeparator) {
		if (model.getConfiguration().getHeaderSeparator().equals(newSeparator)) {
			return;
		}
		model.setHeaderSeparator(newSeparator);
		model.previewParsing();
	}

	/**
	 * Changes the first line of the headers in the underlying configuration
	 * and starts a preview parsing
	 *
	 * @param newStart The new first line where the headers appear
	 */
	public void configureHeadersStart(int newStart) {
		if (model.getConfiguration().getStartHeaders() == newStart) {
			return;
		}
		model.setStartHeaders(newStart);
		model.previewParsing();
	}

	/**
	 * Changes the last line of the headers in the underlying configuration
	 * and starts a preview parsing.
	 *
	 * @param newEnd The new last line where the headers appear
	 */
	public void configureHeadersEnd(int newEnd) {
		if (model.getConfiguration().getEndHeaders() == newEnd) {
			return;
		}
		model.setEndHeaders(newEnd);
		model.previewParsing();
	}

	/**
	 * Changes the presence or absence of metadata in the underlying configuration
	 * and starts a preview parsing.
	 *
	 * @param metadataPresent True if the file contains metadata, false otherwise
	 */
	public void configureMetadataPresent(boolean metadataPresent) {
		if (model.getConfiguration().isMetadataPresent() == metadataPresent) {
			return;
		}
		model.setMetadataPresent(metadataPresent);
		model.previewParsing();
	}

	/**
	 * Changes the metadata separator in the underlying configuration
	 * and starts a preview parsing.
	 *
	 * @param newSeparator The new metadata separator
	 */
	public void configureMetadataSeparator(String newSeparator) {
		if (model.getConfiguration().getMetadataSeparator().equals(newSeparator)) {
			return;
		}
		model.setMetadataSeparator(newSeparator);
		model.previewParsing();
	}

	/**
	 * Changes the first line of metadata in the underlying configuration
	 * and starts a preview parsing.
	 *
	 * @param newStart The new first line where the metadata appear
	 */
	public void configureMetadataStart(int newStart) {
		if (model.getConfiguration().getStartMetadata() == newStart) {
			return;
		}
		model.setStartMetadata(newStart);
		model.previewParsing();
	}

	/**
	 * Changes the last line of metadata in the underlying configuration
	 * and starts a preview parsing.
	 *
	 * @param newEnd The new last line where the metadata appear
	 */
	public void configureMetadataEnd(int newEnd) {
		if (model.getConfiguration().getEndMetadata() == newEnd) {
			return;
		}
		model.setEndMetadata(newEnd);
		model.previewParsing();
	}

	public void configureNanDefaultValue(double defaultNan) {
		if (model.getConfiguration().getDefaultNanValue() == defaultNan) {
			return;
		}
		model.setDefaultNanValue(defaultNan);
		model.previewParsing();
	}

	/**
	 * Selects the column to use as wave
	 *
	 * @param uniqueId The unique ID identifying the column
	 */
	public void selectWaveColumn(int uniqueId) {
		model.setSelectedWaveColumn(uniqueId);
	}

	/**
	 * Selects the column to use as flux
	 *
	 * @param uniqueId The unique ID identifying the column
	 */
	public void selectFluxColumn(int uniqueId) {
		model.setSelectedFluxColumn(uniqueId);
	}

	/**
	 * Changes a column's name, show a warning if a column already exists with this name
	 *
	 * @param uniqueId The unique ID identifying the column
	 * @param newName The new name of the column
	 */
	public void editColumnName(int uniqueId, String newName) {
		editColumnName(uniqueId, newName, false);
	}

	/**
	 * Changes a column's name, show a warning if a column already exists with this name
	 *
	 * @param uniqueId The unique ID identifying the column
	 * @param newName The new name of the column
	 * @param ignoreError Set to true to ignore error and do not display the warning
	 */
	private void editColumnName(int uniqueId, String newName, boolean ignoreError) {
		if (!ignoreError) {
			for (AdvancedAsciiDataColumn column : model.getParsedFile().getDataColumns()) {
				if (column.getName().equals(newName)) {
					showWarning("Column already exists",
							"A column with this name already exists.",
							"Please choose another name.");
					return;
				}
			}
		}
		model.changeName(uniqueId, newName);
	}

	/**
	 * Changes a column's unit
	 *
	 * @param uniqueId The unique ID identifying the column
	 * @param newUnit The new unit of the column
	 */
	public void editColumnUnit(int uniqueId, String newUnit) {
		model.changeUnit(uniqueId, newUnit);
	}

	/**
	 * Changes the title of the future generated spectrum
	 *
	 * @param newTitle The new title of the spectrum
	 */
	public void changeSpectrumTitle(String newTitle) {
		model.getParsedFile().setFileTitle(newTitle);
	}

	/**
	 * Opens a JFileChooser allowing to import a file for parsing
	 */
	public void importFile() {
		File file = getFile(false, null, null);
		if (file != null) {
			model.loadFile(file);
		}
	}

	/**
	 * Opens a JFileChooser allowing to choose a file for saving the underlying configuration
	 */
	public void saveConfiguration() {
		File file = getFile(true, "aar", "Advanced Ascii Reader configuration files (*.aar)");
		if (file != null) {
			model.saveConfiguration(file);
		}
	}

	/**
	 * Opens a JFileChooser allowing to choose a file from which to load a configuration
	 */
	public void loadConfiguration() {
		File file = getFile(false, "aar", "Advanced Ascii Reader configuration files (*.aar)");
		if (file != null) {
			model.loadConfiguration(file);
		}
	}

	/**
	 * Opens a new Frame allowing to visualize the imported file in raw. This frame replaces the
	 * spaces by red dots, and the tabulation by red "/t" so the user can easily determine which separator
	 * to use.
	 */
	public void visualizeLoadedFile() {
		if (model.getInputFile() != null) {
			JFrame fileRead = new RawFileFrame(model.getInputFile());
			fileRead.setVisible(true);
		} else {
			showWarning("No file loaded", "No file loaded.", "Please load a file and try again.");
		}
	}

	/**
	 * Launches a preview parsing
	 */
	public void previewParsing() {
		if (model.getInputFile() == null) {
			showWarning("No file loaded", "No file loaded.", "Please load a file and try again.");
			return;
		}
		model.previewParsing();
	}

	/**
	 * Launches a full parsing of the model, then fire the export action.
	 * If the parsing fails, the action is not fired
	 */
	public void runExportAction() {
		if (!fullParsing()) {
			return;
		}
		model.runExportAction();
	}

	/**
	 * Launches a full parsing of the model, then fire the spectrum manager action.
	 * If the parsing fails, the action is not fired
	 */
	public void runSpectrumManagerAction() {
		if (!fullParsing()) {
			return;
		}
		model.runExportAction();
		model.runSpectrumManagerAction();
	}

	/**
	 * Launches a full parsing of the file, with errors displayed to the user
	 *
	 * @return true if the parsing was successful, false if not
	 */
	public boolean fullParsing() {
		if (testSpectrumValidity()) {
			model.parseFile();
			return true;
		}
		return false;
	}

	/**
	 * Opens a JFileChooser allowing to choose a file in which to save the parsed file
	 *
	 * @return The parsed file
	 */
	public File saveCassisSpectrumInFile() {
		File savingFile = null;
		if (testSpectrumValidity()) {
			try {
				savingFile = getFile(true, "fus", "FUS files (*.fus)");
				if (savingFile != null) {
					SpectrumSaver.saveSpectrum(savingFile, (Frame) view.getTopLevelAncestor(), model);
				}
			} catch (IllegalArgumentException e1) {
				showWarning("Incoherent data", "Can't build spectrum with the current data.", e1.getMessage());
			} catch (Exception e) {
				showWarning("Parsing error", "An error has occured during parsing.", e.getMessage());
			}
		}
		return savingFile;
	}

	private boolean testSpectrumValidity() {
		return isSelectionValid() && isDataValid();
	}

	private boolean isSelectionValid() {
		if (model.getSelectedFlux() == null || model.getSelectedWave() == null) {
			showWarning("Unselected columns", "Two columns are required to build a spectrum.", "Please select two columns.");
			return false;
		}
		if (model.getSelectedFlux().equals(model.getSelectedWave())) {
			showWarning("Selected columns are identical", "The columns must be different to build a spectrum.",
					"Please select two different columns.");
			return false;
		}
		if (model.getSelectedWave().getUnit().equals(UNIT.UNKNOWN.getValString())) {
			showWarning("Units are undefined", "No unit is defined for either of the two columns.",
					"Please choose units for the selected columns.");
			return false;
		}
		return true;
	}

	private boolean isDataValid() {
		if (model.getSelectedFlux().getSize() != model.getSelectedWave().getSize()) {
			showWarning("Selected columns are of unequal size", "The flux and wave columns must have the same size.",
					"Please check that your configuration is correct.");
			return false;
		}
		if (model.getSelectedFlux().getSize() <= 3) {
			showWarning("Too few data to build spectrum", "The columns must hold at least 4 data values.",
					"Please check that your configuration is correct.");
			return false;
		}
		return true;
	}

	/**
	 * Returns a file chosen by the user with a CassisJFileChooser.
	 *
	 * @param saving True if we need to open the JFileChooser in saving mode, false otherwise
	 * @param allowedExtension The additional extension the JFileChooser needs to allow. Sets to null if none is needed
	 * @param description The description of the additional filter.
	 * @return The file chosen by the user
	 */
	private File getFile(boolean saving, String allowedExtension, String description) {
		JFileChooser chooser = new CassisJFileChooser(null, null);
		if (allowedExtension != null && description != null) {
			chooser.setFileFilter(new FileNameExtensionFilter(description, allowedExtension));
		}
		int ret;
		if (saving) {
			ret = chooser.showSaveDialog(view);
		} else {
			ret = chooser.showOpenDialog(view);
		}
		if (ret == JFileChooser.APPROVE_OPTION) {
			File file = chooser.getSelectedFile();
			String name = file.getAbsolutePath();
			if (saving) {
				return createSavingFile(allowedExtension, name);
			}
			return file;
		}
		return null;
	}

	private File createSavingFile(String allowedExtension, String name) {
		String fixedName = name;
		if (allowedExtension != null && !fixedName.endsWith("." + allowedExtension)) {
			fixedName += "." + allowedExtension;
		}
		File fixedFile = new File(fixedName);
		int answer = 0;
		if (fixedFile.exists()) {
			String msg = "The file " + fixedFile.getName() + " already exists.\n" + "Do you want to overwrite it ?";
			answer = JOptionPane.showConfirmDialog(view, msg, "Replace exisiting file ?", JOptionPane.YES_NO_OPTION);
		}
		if (answer == JOptionPane.NO_OPTION) {
			fixedFile = null;
		}
		return fixedFile;
	}

	@Override
	public void dataChanged(ModelChangedEvent event) {
		String strSource = (String) event.getSource();
		EventType typeEvent = EventType.identifyEvent(strSource);
		switch (typeEvent) {
		case METADATA_CONFIGURATION:
			handleMetadataEvent(event);
			break;
		case HEADERS_CONFIGURATION:
			handleHeadersEvent(event);
			break;
		case DATA_CONFIGURATION:
			handleDataEvent(event);
			break;
		case SELECTION_CONFIGURATION:
			handleConfiguredSelectionEvent(event);
			break;
		case ERROR:
			handleErrorEvent(event);
			break;
		case PREVIEW:
			handlePreviewEvent(event);
			break;
		case SPECTRUM_GENERATION:
			handleGenerationEvent();
			break;
		case OTHER:
			handleOtherEvent(event);
			break;
		case NOT_REGISTERED:
			break;
		default:
			break;

		}
	}

	private void handleMetadataEvent(ModelChangedEvent event) {
		MetadataConfigurationPanel metaPanel = view.getConfigurationPanel().getMetadataConfigurationPanel();
		AdvancedAsciiFileConfiguration configuration = model.getConfiguration();

		if (event.getSource().equals(EventSorter.METADATA_PRESENT_EVENT)) {
			metaPanel.getMetadataPresent().setSelected(configuration.isMetadataPresent());
			metaPanel.setChildrenEnabled(configuration.isMetadataPresent());
		}
		if (event.getSource().equals(EventSorter.METADATA_SEPARATOR_EVENT)) {
			updateSeparator(metaPanel.getSeparatorMetadata(), configuration.getMetadataSeparatorRepresentation());
		}
		if (event.getSource().equals(EventSorter.METADATA_FIRST_LINE_EVENT)) {
			setTextFieldValue(metaPanel.getFirstLineMetadata(), configuration.getStartMetadata() + 1);
		}
		if (event.getSource().equals(EventSorter.METADATA_LAST_LINE_EVENT)) {
			setTextFieldValue(metaPanel.getLastLineMetadata(), configuration.getEndMetadata() + 1);
		}
	}

	private void handleHeadersEvent(ModelChangedEvent event) {
		HeaderConfigurationPanel headerPanel = view.getConfigurationPanel().getHeaderConfigurationPanel();
		AdvancedAsciiFileConfiguration configuration = model.getConfiguration();

		if (event.getSource().equals(EventSorter.HEADERS_PRESENT_EVENT)) {
			headerPanel.getHeaderPresent().setSelected(configuration.isHeadersPresent());
			headerPanel.setHeaderChildrenEnabed(configuration.isHeadersPresent());
		}
		if (event.getSource().equals(EventSorter.HEADERS_SEPARATOR_EVENT)) {
			updateSeparator(headerPanel.getSeparatorHeader(), configuration.getHeaderSeparatorRepresentation());
		}
		if (event.getSource().equals(EventSorter.HEADERS_FIRST_LINE_EVENT)) {
			setTextFieldValue(headerPanel.getFirstLineHeader(), configuration.getStartHeaders() + 1);
		}
		if (event.getSource().equals(EventSorter.HEADERS_LAST_LINE_EVENT)) {
			setTextFieldValue(headerPanel.getLastLineHeader(), configuration.getEndHeaders() + 1);
		}
	}

	private void handleDataEvent(ModelChangedEvent event) {
		DataConfigurationPanel dataPanel = view.getConfigurationPanel().getDataConfigurationPanel();
		AdvancedAsciiFileConfiguration configuration = model.getConfiguration();

		if (event.getSource().equals(EventSorter.DATA_ORIENTATION_EVENT)) {
			updateDataOrientation(dataPanel, configuration);
		}
		if (event.getSource().equals(EventSorter.DATA_SEPARATOR_EVENT)) {
			updateSeparator(dataPanel.getSeparatorData(), configuration.getDataSeparatorRepresentation());
		}
		if (event.getSource().equals(EventSorter.DATA_FIRST_LINE_EVENT)) {
			setTextFieldValue(dataPanel.getFirstLineData(), configuration.getStartData() + 1);
		}
		if (event.getSource().equals(EventSorter.DATA_LAST_LINE_EVENT)) {
			setTextFieldValue(dataPanel.getLastLineData(), configuration.getEndData() + 1);
		}
		if (event.getSource().equals(EventSorter.NB_COLUMNS_EVENT)) {
			setTextFieldValue(dataPanel.getTotalNbCols(), configuration.getNbColumns());
		}
		if (event.getSource().equals(EventSorter.DEFAULT_NAN_VALUE_EVENT)) {
			dataPanel.getNanDefaultValue().setText(String.valueOf(configuration.getDefaultNanValue()));
		}
	}

	private void handleConfiguredSelectionEvent(ModelChangedEvent event) {
		if (model.getParsedFile().getNbColumns() <= 0) {
			return;
		}
		SpectrumGenerationPanel spect = view.getConfigurationPanel().getSpectrumGenerationPanel();

		if (event.getSource().equals(EventSorter.WAVE_INDEX_EVENT)) {
			spect.getWaveColumn().setSelectedIndex(model.getConfiguration().getWaveIndex());
		}
		if (event.getSource().equals(EventSorter.WAVE_UNIT_EVENT)) {
			spect.getWaveColumnUnit().setSelectedItem(model.getConfiguration().getWaveUnit());
		}
		if (event.getSource().equals(EventSorter.WAVE_NAME_EVENT)) {
			editColumnName(model.getSelectedWave().getIndexColumn(),
					model.getConfiguration().getWaveColumnName(), true);
			spect.getWaveColumn().getModel().setSelectedItem(
					model.getConfiguration().getWaveColumnName());
		}
		if (event.getSource().equals(EventSorter.FLUX_INDEX_EVENT)) {
			spect.getFluxColumn().setSelectedIndex(model.getConfiguration().getFluxIndex());
		}
		if (event.getSource().equals(EventSorter.FLUX_UNIT_EVENT)) {
			spect.getFluxColumnUnit().setSelectedItem(model.getConfiguration().getFluxUnit());
		}
		if (event.getSource().equals(EventSorter.FLUX_NAME_EVENT)) {
			editColumnName(model.getSelectedFlux().getIndexColumn(),
					model.getConfiguration().getFluxColumnName(), true);
			spect.getFluxColumn().getModel().setSelectedItem(
					model.getConfiguration().getFluxColumnName());
		}
	}

	private void handleErrorEvent(ModelChangedEvent event) {
		if (event.getSource().equals(EventSorter.ESTIMATION_ERROR_EVENT)) {
			showWarning("Estimation error", "The configuration couldn't be properly estimated for the loaded file.",
					"Please configure manually with the menu on the left.");
		}
		if (event.getSource().equals(EventSorter.PARSING_ERROR_EVENT)) {
			showWarning("Parsing error", "An error occurred during parsing.", "Please check that the given configuration "
					+ "is correct.");
		}
		if (event.getSource().equals(EventSorter.CONFIGURATION_SAVING_ERROR_EVENT)) {
			showWarning("Configuration error", "An error occured while saving the configuration", "Please check the configuration.");
		}
		if (event.getSource().equals(EventSorter.CONFIGURATION_LOADING_ERROR_EVENT)) {
			showWarning("Configuration error", "An error occured while loading the configuration", "Please check your file.");
		}
	}

	private void handlePreviewEvent(ModelChangedEvent event) {
		if (event.getSource().equals(EventSorter.PREVIEW_DONE_EVENT) && model.getInputFile() != null) {
			refreshDataPreview();
			updateSpectrumGenerationPanel();
			view.getConfigurationPanel().getSpectrumGenerationPanel().setPanelChildrenEnabled(true);
		}
		if (event.getSource().equals(EventSorter.METADA_EDITED_EVENT)) {
			view.getDataPreview().updateMetadataModel(model.getParsedFile());
		}
	}

	private void handleGenerationEvent() {
		refreshDataPreview();
	}

	private void handleOtherEvent(ModelChangedEvent event) {
		if (event.getSource().equals(EventSorter.FILE_LOADED_EVENT)) {
			onFileLoaded();
		}
	}

	private void showWarning(String title, String firstLine, String lastLine) {
		JOptionPane.showMessageDialog(view, "<html>" + firstLine + "<br> " + lastLine + "</html>",
				title, JOptionPane.WARNING_MESSAGE);
	}

	private void updateDataOrientation(DataConfigurationPanel dataPanel, AdvancedAsciiFileConfiguration configuration) {
		dataPanel.getAsColumns().setSelected(configuration.isAsColumns());
		dataPanel.getAsLines().setSelected(!configuration.isAsColumns());
		String text = configuration.isAsColumns() ? "Number of columns:" : "Number of lines:";
		dataPanel.getNbColsLabel().setText(text);
	}

	private void setTextFieldValue(JFormattedTextField target, int newValue) {
		target.setValue(newValue);
	}

	private void onFileLoaded() {
		updateSpectrumGenerationPanel();
		view.getDataPreview().resetTables();
		view.getDataPreview().getTitle().setText(model.getInputFile().getName());
		updateSpectrumGenerationPanel();
		JFrame frame = (JFrame) view.getTopLevelAncestor();
		frame.setTitle("Advanced ASCII Reader - " + model.getInputFile().getPath());
		setLastLines();
		previewParsing();
	}

	private void setLastLines() {
		int lastLineIndex = model.getLastLineIndex();
		view.getConfigurationPanel().getDataConfigurationPanel().getFirstLineData().setLastLineIndex(lastLineIndex);
		view.getConfigurationPanel().getDataConfigurationPanel().getLastLineData().setLastLineIndex(lastLineIndex);

		view.getConfigurationPanel().getHeaderConfigurationPanel().getFirstLineHeader().setLastLineIndex(lastLineIndex);
		view.getConfigurationPanel().getHeaderConfigurationPanel().getLastLineHeader().setLastLineIndex(lastLineIndex);

		view.getConfigurationPanel().getMetadataConfigurationPanel().getFirstLineMetadata().setLastLineIndex(lastLineIndex);
		view.getConfigurationPanel().getMetadataConfigurationPanel().getLastLineMetadata().setLastLineIndex(lastLineIndex);
	}

	private void refreshDataPreview() {
		view.getDataPreview().updateDataModel(model.getParsedFile());
		view.getDataPreview().updateMetadataModel(model.getParsedFile());
	}

	private void updateSpectrumGenerationPanel() {
		view.getConfigurationPanel().getSpectrumGenerationPanel().refreshDataColumns();
	}

	private void updateSeparator(SeparatorComboBox target, String newValue) {
		target.setSelected(newValue);
	}

}
