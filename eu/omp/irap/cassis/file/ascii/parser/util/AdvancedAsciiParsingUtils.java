/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.ascii.parser.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import eu.omp.irap.cassis.common.axes.UNIT;

/**
 * @author bastienkovac
 *
 * Utility class to handle parsing
 *
 */
public class AdvancedAsciiParsingUtils {

	private static final Pattern SCIENTIFIC_NUMBER_PATTERN = Pattern.compile("[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?");


	private AdvancedAsciiParsingUtils() {

	}

	/**
	 * Split a line using a separator character
	 *
	 * @param line
	 *            The source line
	 * @param separator
	 *            The character used to split the source line
	 * @return A list of String corresponding to the splitted source line
	 */
	public static List<String> readLine(String line, String separator) {
		String[] splittedLine = reformatString(line).split(separator);
		List<String> returnedList = new ArrayList<>();
		for (String s : splittedLine) {
			returnedList.add(s);
		}
		return returnedList;
	}

	/**
	 * Transform a list of String into their double value
	 *
	 * @param splittedLine
	 *            The list of String
	 * @return A list of parsed Double value. Non-numerical values of String are
	 *         replaced by NaN
	 */
	public static double[] asDataArray(List<String> splittedLine, double nanDefault) {
		double[] dataArray = new double[splittedLine.size()];
		for (int i = 0 ; i < splittedLine.size() ; i++) {
			try {
				dataArray[i] = Double.valueOf(splittedLine.get(i));
			} catch (NumberFormatException e) {
				dataArray[i] = nanDefault;
			}
		}
		return dataArray;
	}

	/**
	 * Try to read the unit from a column header
	 *
	 * @param columnHeader
	 *            The String to read
	 * @return The corresponding unit (UNIT.UNKNOWN if not found)
	 */
	public static UNIT readUnit(final String columnHeader) {
		for (UNIT unit : UNIT.values()) {
			String pattern = "\\b(" + unit.getValString() + ")\\b";
			Pattern r = Pattern.compile(pattern);
			Matcher matcher = r.matcher(columnHeader);
			if (matcher.find()) {
				return unit;
			}
		}
		return UNIT.UNKNOWN;
	}

	/**
	 * Tries to read the VLSR value from a parsed metadata
	 *
	 * @param nameLine The name of the metadata
	 * @param valueLine The value of the metadata
	 * @return The parsed value, or 0.0 if the value couldn't be read
	 */
	public static Double readVlsr(String nameLine, String valueLine) {
		if (nameLine.toLowerCase().contains("vlsr")) {
			try {
				String numberOnly = valueLine.replaceAll("[^0-9\\.]", "");
				return Double.valueOf(numberOnly);
			} catch (NumberFormatException e) {
				return 0.0;
			}
		}
		return 0.0;
	}

	/**
	 * Tries to read the LoFrequency value from a parsed metadata
	 *
	 * @param nameLine The name of the metadata
	 * @param valueLine The value of the metadata
	 * @return The parsed value, or 0.0 if the value couldn't be read
	 */
	public static Double readLoFrequency(String nameLine, String valueLine) {
		if (nameLine.toLowerCase().contains("lofreq")) {
			try {
				String numberOnly = valueLine.replaceAll("[^0-9\\.]", "");
				return Double.valueOf(numberOnly);
			} catch (NumberFormatException e) {
				return 0.0;
			}
		}
		return 0.0;
	}

	/**
	 * Counts the number of occurrences of a substring in a string
	 *
	 * @param line The string
	 * @param substring The substring
	 * @return The number of times substring appears in line
	 */
	public static int countOccurrences(String line, String substring) {
		Matcher match = Pattern.compile(substring).matcher(reformatString(line));
		int count = 0;
		while (match.find()) {
			count++;
		}
		return count;
	}

	/**
	 * Checks if the given line contains at least two numbers written in scientific representation
	 *
	 * The method use the following regex : {@code "[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?"}
	 * Some examples of matching values :
	 * 		- 1		1e+1
	 * 		- 1.0	1e-1
	 * 		- -1.0	..
	 *
	 * @param line The line to check
	 * @return If the line contains at least one number
	 */
	public static boolean isProbableDataLine(String line) {
		Matcher match = SCIENTIFIC_NUMBER_PATTERN.matcher(reformatString(line));
		int count = 0;
		while (match.find()) {
			count++;
		}
		return count >= 2;
	}

	private static String reformatString(final String source) {
		String newLine = source.replaceAll("[ ]+", " "); // Shrink block of spaces in only one
		return newLine.trim();
	}

}
