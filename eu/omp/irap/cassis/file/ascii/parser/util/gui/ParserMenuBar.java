/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.ascii.parser.util.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import eu.omp.irap.cassis.file.ascii.parser.AdvancedAsciiControl;

/**
 * @author bastienkovac
 *
 * The JMenuBar for the parsing module
 *
 */
public class ParserMenuBar extends JMenuBar {

	/**
	 *
	 */
	private static final long serialVersionUID = 326881476486745741L;
	// File
	private JMenuItem importFile, visualizeFile;
	// Configuration
	private JMenuItem saveConfiguration, loadConfiguration;
	// Actions
	private JMenuItem generateSpectrum;
	private AdvancedAsciiControl control;


	/**
	 * Creates a new ParserMenuBar
	 *
	 * @param control The controller
	 */
	public ParserMenuBar(AdvancedAsciiControl control) {
		super();
		this.control = control;
		JMenu file = new JMenu("File");
		file.add(getImportFile());
		file.add(getVisualizeFile());
		file.addSeparator();
		file.add(getGenerateSpectrum());
		add(file);
		JMenu conf = new JMenu("Configuration");
		conf.add(getSaveConfiguration());
		conf.add(getLoadConfiguration());
		add(conf);
	}

	/**
	 * @return A menu item handling the file importation
	 */
	public JMenuItem getImportFile() {
		if (importFile == null) {
			importFile = new JMenuItem("Open file");
			importFile.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					control.importFile();
				}
			});
		}
		return importFile;
	}

	/**
	 * @return A menu item handling the currently loaded file visualization
	 */
	public JMenuItem getVisualizeFile() {
		if (visualizeFile == null) {
			visualizeFile = new JMenuItem("Visualize raw file");
			visualizeFile.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					control.visualizeLoadedFile();
				}
			});
		}
		return visualizeFile;
	}

	/**
	 * @return A menu item handling the saving of the current configuration
	 */
	public JMenuItem getSaveConfiguration() {
		if (saveConfiguration == null) {
			saveConfiguration = new JMenuItem("Save");
			saveConfiguration.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					control.saveConfiguration();
				}
			});
		}
		return saveConfiguration;
	}

	/**
	 * @return A menu item handling the loading of the current configuration
	 */
	public JMenuItem getLoadConfiguration() {
		if (loadConfiguration == null) {
			loadConfiguration = new JMenuItem("Load");
			loadConfiguration.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					control.loadConfiguration();
				}
			});
		}
		return loadConfiguration;
	}

	/**
	 * @return A menu item handling the generation of the parsed spectrum
	 */
	public JMenuItem getGenerateSpectrum() {
		if (generateSpectrum == null) {
			generateSpectrum = new JMenuItem("Save spectrum");
			generateSpectrum.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					control.runSpectrumManagerAction();
				}
			});
		}
		return generateSpectrum;
	}

}
