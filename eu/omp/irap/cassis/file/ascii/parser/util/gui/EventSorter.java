/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.ascii.parser.util.gui;

import java.util.ArrayList;
import java.util.List;

/**
 * @author bastienkovac
 *
 * This class is used to sort the different events fired by the AdvancedAsciiModel
 *
 */
public class EventSorter {

	/**
	 * @author bastienkovac
	 *
	 * Enumeration with the different possible types of event
	 *
	 */
	public static enum EventType {

		METADATA_CONFIGURATION(METADATA_EVENTS),
		HEADERS_CONFIGURATION(HEADERS_EVENTS),
		DATA_CONFIGURATION(DATA_EVENTS),
		SELECTION_CONFIGURATION(CONFIGURED_SELECTION_EVENTS),
		SPECTRUM_GENERATION(SPECTRUM_GENERATION_EVENT),
		PREVIEW(PREVIEW_EVENTS),
		ERROR(ERROR_EVENTS),
		OTHER(OTHER_EVENTS),
		NOT_REGISTERED(new String[0]);

		private String[] linkedEvents;

		private EventType(String[] linkedEvents) {
			this.linkedEvents = linkedEvents;
		}

		public boolean isLinkedTo(String event) {
			return arrayContains(linkedEvents, event);
		}

		public static EventType identifyEvent(String event) {
			for (EventType type : EventType.values()) {
				if (type.isLinkedTo(event)) {
					return type;
				}
			}
			return EventType.NOT_REGISTERED;
		}

	}

	// MetadataConfigurationPanel
	public static final String METADATA_PRESENT_EVENT = "metadataPresent";
	public static final String METADATA_SEPARATOR_EVENT = "metadataSeparator";
	public static final String METADATA_FIRST_LINE_EVENT = "metadataFirstLine";
	public static final String METADATA_LAST_LINE_EVENT = "metadataLastLine";

	private static final String[] METADATA_EVENTS = new String[] { METADATA_PRESENT_EVENT, METADATA_SEPARATOR_EVENT,
			METADATA_FIRST_LINE_EVENT, METADATA_LAST_LINE_EVENT };

	// HeadersConfigurationPanel
	public static final String HEADERS_PRESENT_EVENT = "headersPresent";
	public static final String HEADERS_SEPARATOR_EVENT = "headersSeparator";
	public static final String HEADERS_FIRST_LINE_EVENT = "headersFirstLine";
	public static final String HEADERS_LAST_LINE_EVENT = "headersLastLine";

	private static final String[] HEADERS_EVENTS = new String[] { HEADERS_PRESENT_EVENT, HEADERS_SEPARATOR_EVENT,
			HEADERS_FIRST_LINE_EVENT, HEADERS_LAST_LINE_EVENT };

	// DataConfigurationPanel
	public static final String DATA_ORIENTATION_EVENT = "dataOrientation";
	public static final String DATA_SEPARATOR_EVENT = "dataSeparator";
	public static final String DATA_FIRST_LINE_EVENT = "dataFirstLine";
	public static final String DATA_LAST_LINE_EVENT = "dataLastLine";
	public static final String NB_COLUMNS_EVENT = "nbColumns";
	public static final String DEFAULT_NAN_VALUE_EVENT = "defaultNanValueChanged";

	private static final String[] DATA_EVENTS = new String[] { DATA_SEPARATOR_EVENT, NB_COLUMNS_EVENT,
			DATA_FIRST_LINE_EVENT, DATA_LAST_LINE_EVENT, DATA_ORIENTATION_EVENT, DEFAULT_NAN_VALUE_EVENT };

	// ConfiguredSelection
	public static final String WAVE_INDEX_EVENT = "waveIndex";
	public static final String WAVE_UNIT_EVENT = "waveUnit";
	public static final String WAVE_NAME_EVENT = "waveName";
	public static final String FLUX_INDEX_EVENT = "fluxIndex";
	public static final String FLUX_UNIT_EVENT = "fluxUnit";
	public static final String FLUX_NAME_EVENT = "fluxName";

	private static final String[] CONFIGURED_SELECTION_EVENTS = new String[] {
			WAVE_INDEX_EVENT, WAVE_UNIT_EVENT, WAVE_NAME_EVENT,
			FLUX_INDEX_EVENT, FLUX_UNIT_EVENT, FLUX_NAME_EVENT };

	// SpectrumGenerationPanel
	public static final String COLUMN_NAME_EVENT = "columnNameChanged";
	public static final String COLUMN_UNIT_EVENT = "columnUnitChanged";

	private static final String[] SPECTRUM_GENERATION_EVENT = new String[] { COLUMN_NAME_EVENT, COLUMN_UNIT_EVENT };

	// PreviewDataPanel
	public static final String PREVIEW_DONE_EVENT = "previewDone";
	public static final String METADA_EDITED_EVENT = "editionMetadata";

	private static final String[] PREVIEW_EVENTS = new String[] { PREVIEW_DONE_EVENT, METADA_EDITED_EVENT };

	// Errors
	public static final String ESTIMATION_ERROR_EVENT = "estimationError";
	public static final String PARSING_ERROR_EVENT = "parsingError";
	public static final String CONFIGURATION_SAVING_ERROR_EVENT = "configurationSavingError";
	public static final String CONFIGURATION_LOADING_ERROR_EVENT = "configurationLoadingError";

	private static final String[] ERROR_EVENTS = new String[] { ESTIMATION_ERROR_EVENT, PARSING_ERROR_EVENT,
			CONFIGURATION_SAVING_ERROR_EVENT, CONFIGURATION_LOADING_ERROR_EVENT };

	// Other
	public static final String FILE_LOADED_EVENT = "fileLoaded";

	private static final String[] OTHER_EVENTS = new String[] { FILE_LOADED_EVENT };


	private EventSorter() {

	}

	/**
	 * @return An array containing all the events relative to the configuration
	 */
	public static String[] getConfigurationEvents() {
		return mergeArrays(METADATA_EVENTS, HEADERS_EVENTS, DATA_EVENTS, CONFIGURED_SELECTION_EVENTS);
	}

	/**
	 * @return An array containing all the events relative to the estimated configuration, meaning all configuration
	 * events minus the ones linked to the selections of the wave and flux columns and their units
	 */
	public static String[] getInitialConfigurationEvents() {
		return mergeArrays(METADATA_EVENTS, HEADERS_EVENTS, DATA_EVENTS);
	}

	/**
	 * Checks if a given array contains a targetted value
	 *
	 * @param array The array
	 * @param target The value we're looking for
	 * @return True if the array contains the target value, false if not
	 */
	public static <T> boolean arrayContains(T[] array, T target) {
		for (T value : array) {
			if (value.equals(target)) {
				return true;
			}
		}
		return false;
	}

	private static String[] mergeArrays(String[]... arrays) {
		List<String> mergedList = new ArrayList<>();
		for (String[] array : arrays) {
			for (String string : array) {
				mergedList.add(string);
			}
		}
		return mergedList.toArray(new String[mergedList.size()]);
	}

}
