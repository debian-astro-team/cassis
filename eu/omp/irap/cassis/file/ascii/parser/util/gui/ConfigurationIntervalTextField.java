/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.ascii.parser.util.gui;

import java.awt.Color;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.text.DefaultFormatter;
import javax.swing.text.DefaultFormatterFactory;

/**
 * @author bastienkovac
 *
 * This class is a customized JFormattedTextField used to configure the line intervals.
 * Accepted values are integers and the symbol '*', which represents Integer.MAX_VALUE
 *
 */
@SuppressWarnings("serial")
public class ConfigurationIntervalTextField extends JFormattedTextField {

	private Map<String, Integer> symbols;
	private int lastLineIndex;

	/**
	 * Constructor
	 */
	public ConfigurationIntervalTextField(int lastLineIndex) {
		super();
		this.lastLineIndex = lastLineIndex;
		setFormatterFactory(new DefaultFormatterFactory(new IntervalFormatter()));
		setInputVerifier(new IntervalVerifier());
	}

	/**
	 * Sets the index of the last line in the current file,
	 * this value is returned by the the text field when the
	 * symbol '*' is entered
	 *
	 * @param lastLineIndex The index of the last line
	 */
	public void setLastLineIndex(int lastLineIndex) {
		this.lastLineIndex = lastLineIndex;
		this.symbols.put("*", lastLineIndex);
	}

	/**
	 * @return The current value of the text field, as an integer
	 */
	public int getIntValue() {
		Object value = getValue();
		if (value == null || !getInputVerifier().verify(this)) {
			return -1;
		}
		if (value instanceof Integer)
			return (int) value;
		if (value instanceof String)
			return Integer.parseInt((String) value);
		return 0;
	}

	private class IntervalVerifier extends InputVerifier {
		@Override
		public boolean verify(JComponent input) {
			if (input instanceof JFormattedTextField) {
				JFormattedTextField ftf = (JFormattedTextField) input;
				AbstractFormatter formatter = ftf.getFormatter();
				if (formatter != null) {
					String text = ftf.getText();
					if (symbols.containsKey(text)) {
						ftf.setBackground(Color.WHITE);
						return true;
					}
					return tryParseText(text, ftf);
				}
			}
			return true;
		}

		private boolean tryParseText(String text, JFormattedTextField ftf) {
			try {
				int val = Integer.parseInt(text);
				if (val <= 0) {
					throw new NumberFormatException();
				}
				ftf.setBackground(Color.WHITE);
				return true;
			} catch (NumberFormatException pe) {
				ftf.setBackground(Color.RED);
				return false;
			}
		}

	}

	private class IntervalFormatter extends DefaultFormatter {

		public IntervalFormatter() {
			super();
			symbols = new HashMap<>();
			symbols.put("*", lastLineIndex);
		}

		@Override
		public Object stringToValue(String arg0) throws ParseException {
			if (symbols.containsKey(arg0.toUpperCase())) {
				System.out.println(symbols.get(arg0));
				return symbols.get(arg0);
			}
			return arg0;
		}

		@Override
		public String valueToString(Object value) throws ParseException {
			if (value!= null && symbols.containsValue(value)) {
				return getKey(value);
			}
			return super.valueToString(value);
		}

		private String getKey(Object value) {
			for (String key : symbols.keySet()) {
				if (symbols.get(key).equals(value))
					return key;
			}
			return null;
		}

	}

}
