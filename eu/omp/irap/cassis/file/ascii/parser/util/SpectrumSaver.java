/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.ascii.parser.util;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.io.File;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;

import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.file.FileManagerAsciiCassis;
import eu.omp.irap.cassis.file.ascii.parser.AdvancedAsciiModel;

/**
 * @author bastienkovac
 *
 * This class is used to fully parse a file and to save it in the given file.
 * It uses a modal JDialog with a JTextArea and a JProgressBar to inform the user about the state of the process
 *
 */
public class SpectrumSaver {


	private SpectrumSaver() {

	}

	/**
	 * Creates a modal JDialog linked to the parentFrame which will block user's input for the duration of the
	 * saving of the spectrum.
	 * Once created from the model, the spectrum is saved in the savingFile using {@link FileManagerAsciiCassis}
	 *
	 * @param savingFile The file in which the spectrum will be saved
	 * @param parentFrame The frame owner of the JDialog
	 * @param model The model used to build the spectrum
	 */
	public static void saveSpectrum(File savingFile, Frame parentFrame, AdvancedAsciiModel model) {
		ProgressDialog dlg = new ProgressDialog(parentFrame);
		BuilderWorker worker = new BuilderWorker(dlg, model, savingFile);
		worker.execute();
		dlg.setVisible(true);
	}

	@SuppressWarnings("serial")
	private static class ProgressDialog extends JDialog {

		private JProgressBar progressBar;
		private JTextArea displayArea;

		/**
		 * Creates a JDialog used to display the progress of the saving operation.
		 *
		 * @param parent The parent owner of the JDialog
		 */
		public ProgressDialog(Frame parent) {
			super(parent, "Saving the spectrum", true);
			add(BorderLayout.SOUTH, getProgressBar());
			add(BorderLayout.CENTER, new JScrollPane(getDisplayArea()));
			setSize(300, 100);
			setLocationRelativeTo(parent);
			setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
			setResizable(false);
		}

		private JProgressBar getProgressBar() {
			if (progressBar == null) {
				progressBar = new JProgressBar();
				progressBar.setIndeterminate(true);
			}
			return progressBar;
		}

		private JTextArea getDisplayArea() {
			if (displayArea == null) {
				displayArea = new JTextArea("");
				displayArea.setEditable(false);
			}
			return displayArea;
		}

	}

	private static class BuilderWorker extends SwingWorker<File, String> {

		private ProgressDialog dialog;
		private AdvancedAsciiModel model;
		private File savingFile;


		/**
		 * Creates a worker thread that uses the given model to fully parse the file, creates the
		 * associated CassisSpectrum and save it to the given file.
		 * The progress of this operation is displayed in the given ProgressDialog
		 *
		 * @param dialog The dialog in which the progress will be displayed
		 * @param model The model used for parsing
		 * @param savingFile The file in which the spectrum will be saved
		 */
		public BuilderWorker(ProgressDialog dialog, AdvancedAsciiModel model, File savingFile) {
			this.dialog = dialog;
			this.model = model;
			this.savingFile = savingFile;
		}

		@Override
		protected void done() {
			dialog.dispose();
		}

		@Override
		protected void process(List<String> chunks) {
			for (String s : chunks) {
				this.dialog.displayArea.append(s + "\n");
			}
		}

		@Override
		protected File doInBackground() throws Exception {
			publish("Parsing file..");
			this.model.parseFile();
			publish("Building spectrum..");
			CassisSpectrum spect = model.generateCassisSpectrum();
			publish("Saving in file..");
			FileManagerAsciiCassis saver = new FileManagerAsciiCassis(savingFile, false);
			saver.save(savingFile, spect);
			return savingFile;
		}

	}

}
