/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.ascii.parser.util.gui;

import java.awt.Component;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;
import javax.swing.SwingWorker;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;

/**
 * @author bastienkovac
 *
 * JTable used to display a raw file into a {@link RawFileFrame}
 *
 */
@SuppressWarnings("serial")
public class RawFileTable extends JTable {

	private boolean hasExpanded;

	/**
	 * Creates a JTable used to display the file given as parameter.
	 * The file is displayed line by line, with a column displaying the number
	 * of each line
	 *
	 * @param fileToDisplay
	 */
	public RawFileTable(File fileToDisplay) {
		super();
		setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		hasExpanded = false;
		setModel(new RawFileTableModel(fileToDisplay));
		((RawFileTableModel) getModel()).populate();
		setShowGrid(false);
		getColumnModel().getColumn(0).setMaxWidth(100);
		getTableHeader().setReorderingAllowed(false);
	}

	private void expandColumns() {
		if (hasExpanded) {
			return;
		}
		int maxWidth = 0;
		for (int i = 0 ; i < 50 ; i++) {
			String value = (String) getValueAt(i, 1);
			TableCellRenderer renderer = getCellRenderer(i, 1);
			Component c = renderer.getTableCellRendererComponent(this, value, true, true, i, 1);
			maxWidth = Math.max(maxWidth, c.getPreferredSize().width);
		}
		getColumnModel().getColumn(1).setPreferredWidth(maxWidth + 20);
		getColumnModel().getColumn(1).setMaxWidth(maxWidth + 20);
		hasExpanded = true;
	}

	private class RawFileTableModel extends AbstractTableModel {

		/**
		 *
		 */
		private static final long serialVersionUID = -1562565970088020793L;

		private File fileToRead;
		private List<DataLine> lines;
		private final String[] columns = new String[] {"Line", "Text"};


		/**
		 * Creates a new RawFileTableModel from a given file
		 *
		 * @param fileToRead The file to read
		 */
		public RawFileTableModel(File fileToRead) {
			this.fileToRead = fileToRead;
			this.lines = new ArrayList<>();
		}

		@Override
		public int getRowCount() {
			return lines.size();
		}

		@Override
		public int getColumnCount() {
			return columns.length;
		}

		@Override
		public String getColumnName(int col) {
			return columns[col];
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			if (rowIndex >= lines.size())
				return null;
			DataLine line = lines.get(rowIndex);
			return columnIndex == 0 ? line.getNbLine() : "<html><pre>" + line.getTextLine() + "</pre></html>";
		}

		@Override
		public void fireTableRowsInserted(int firstRow, int lastRow) {
			expandColumns();
			super.fireTableRowsInserted(firstRow, lastRow);
		}

		/**
		 * Fill the table with the file's content
		 */
		public void populate() {
			RawFileSwingWorker worker = new RawFileSwingWorker(this);
			worker.execute();
		}

		private class DataLine {

			private int nbLine;
			private String textLine;


			public DataLine(int nbLine, String textLine) {
				this.nbLine = nbLine;
				this.textLine = textLine;
			}

			public int getNbLine() {
				return nbLine;
			}

			public String getTextLine() {
				return textLine;
			}

		}

		private class RawFileSwingWorker extends SwingWorker<RawFileTableModel, DataLine> {

			private RawFileTableModel model;


			public RawFileSwingWorker(RawFileTableModel model) {
				this.model = model;
			}

			@Override
			protected RawFileTableModel doInBackground() throws Exception {
				try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileToRead), StandardCharsets.UTF_8))) {
					int nbLine = 1;
					String line;
					while ((line = br.readLine()) != null) {
						line = line.replace(" ", "<font color=red>" + "." + "</font>");
						line = line.replaceAll("\\t", "<font color=red>" + "\\\\t" + "</font>");
						DataLine dataLine = new DataLine(nbLine++, line);
						publish(dataLine);
					}
				}
				return model;
			}

			@Override
			protected void process(List<DataLine> chunks) {
				int nbLines = getRowCount();
				lines.addAll(chunks);
				fireTableRowsInserted(nbLines, getRowCount() - 1);
			}

		}

	}
}
