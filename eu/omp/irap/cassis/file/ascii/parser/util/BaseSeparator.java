/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.ascii.parser.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author bastienkovac
 *
 * This enumeration represents the default separation characters as well
 * as their full name. Methods permit fast switch between the two representation
 *
 */
public enum BaseSeparator {

	TABULATION("Tabulation", "\t"),
	COMMA("Comma", ","),
	SEMI_COLON("Semi-Colon", ";"),
	SPACE("Space", " "),
	COLON("Colon", ":"),
	WHITESPACE("Whitespace", "\\s+");

	private String stringName;
	private String value;


	private BaseSeparator(String name, String value) {
		this.stringName = name;
		this.value = value;
	}

	/**
	 * @return A string representing the full name of the BaseSeparator (ex. "Comma")
	 */
	public String getStringName() {
		return stringName;
	}

	/**
	 * @return A string representing the true value of the BaseSeparator (ex. ",")
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Switch from full name representation to true value.
	 *
	 * @param input The full name.
	 * @return The BaseSeparator's true value.
	 * If no BaseSeparator with the given full name exists, the input is returned
	 */
	public static String getSeparator(String input) {
		for (BaseSeparator val : BaseSeparator.values()) {
			if (input.equalsIgnoreCase(val.getStringName())) {
				return val.getValue();
			}
		}
		return input;
	}

	/**
	 * Switch from true value to full name representation
	 *
	 * @param input The true value
	 * @return The BaseSeparator's full name representation
	 * If no BaseSeparator with the given value exists, the input is returned
	 */
	public static String getStringName(String input) {
		for (BaseSeparator val : BaseSeparator.values()) {
			if (input.equalsIgnoreCase(val.getValue())) {
				return val.getStringName();
			}
		}
		return input;
	}

	/**
	 * @return A {@link Map} that allows counting occurrences of each BaseSeparator value
	 */
	public static Map<BaseSeparator, Integer> getOccurenceMap() {
		Map<BaseSeparator, Integer> occurenceMap = new HashMap<>();
		for (BaseSeparator sep : values()) {
			occurenceMap.put(sep, 0);
		}
		return occurenceMap;
	}

}
