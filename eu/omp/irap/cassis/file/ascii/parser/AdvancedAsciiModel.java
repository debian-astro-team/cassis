/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.ascii.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.events.ListenerManager;
import eu.omp.irap.cassis.common.events.ModelChangedEvent;
import eu.omp.irap.cassis.file.ascii.parser.configuration.AdvancedAsciiConfigurationEstimator;
import eu.omp.irap.cassis.file.ascii.parser.configuration.AdvancedAsciiFileConfiguration;
import eu.omp.irap.cassis.file.ascii.parser.data.AdvancedAsciiDataColumn;
import eu.omp.irap.cassis.file.ascii.parser.data.ParsedAsciiFile;
import eu.omp.irap.cassis.file.ascii.parser.util.gui.EventSorter;
import eu.omp.irap.cassis.file.gui.cassisspectrum.CassisSpectrumControl;

/**
 * @author bastienkovac
 *
 * Parsing module's model. Holds the {@link AdvancedAsciiFileConfiguration} to use when parsing the input File, that it
 * stores as well. Also hold the {@link ParsedAsciiFile}, that is the result of parsing
 *
 */
public class AdvancedAsciiModel extends ListenerManager {

	private AdvancedAsciiFileConfiguration configuration;
	private File inputFile;
	private ParsedAsciiFile parsedFile;

	private int selectedWave, selectedFlux;
	private boolean headerChanged;

	private CassisSpectrumControl linkedControl;
	private Runnable exportAction;

	private int lastLineIndex;


	/**
	 * Creates a new AdvancedAsciiModel
	 */
	public AdvancedAsciiModel() {
		selectedFlux = -1;
		selectedWave = -1;
		configuration = AdvancedAsciiFileConfiguration.getDefaultConfiguration();
		parsedFile = new ParsedAsciiFile();
		headerChanged = false;
	}

	/**
	 * Loads a file into the model and refresh the {@link AdvancedAsciiFileConfiguration} and
	 * {@link ParsedAsciiFile}
	 *
	 * @param inputFile The file the user selected for parsing
	 */
	public void loadFile(File inputFile) {
		this.inputFile = inputFile;
		parsedFile = new ParsedAsciiFile();
		parsedFile.setFileTitle(this.inputFile.getName());
		AdvancedAsciiConfigurationEstimator estimator = new AdvancedAsciiConfigurationEstimator(inputFile);
		try {
			configuration = estimator.getEstimatedConfiguration();
		} catch (IllegalStateException e) {
			fireDataChanged(new ModelChangedEvent(EventSorter.ESTIMATION_ERROR_EVENT));
		}
		countLines(inputFile);
		fireDataChanged(new ModelChangedEvent(EventSorter.FILE_LOADED_EVENT));
		fireInitialConfigurationEvents();
	}

	private void countLines(File file) {
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			int count = 0;
			while (br.readLine() != null) {
				count++;
			}
			this.lastLineIndex = count;
		} catch (IOException e) {
			this.lastLineIndex = 0;
		}
	}

	/**
	 * @return The index (between 0 and 1) of the last line in the file
	 */
	public int getLastLineIndex() {
		return lastLineIndex;
	}

	/**
	 * @return The current {@link AdvancedAsciiFileConfiguration}
	 */
	public AdvancedAsciiFileConfiguration getConfiguration() {
		return configuration;
	}

	/**
	 * @return The input file
	 */
	public File getInputFile() {
		return inputFile;
	}

	/**
	 * @return the selectedWave
	 */
	public AdvancedAsciiDataColumn getSelectedWave() {
		return getColumn(selectedWave);
	}

	/**
	 * @return the selectedFlux
	 */
	public AdvancedAsciiDataColumn getSelectedFlux() {
		return getColumn(selectedFlux);
	}

	/**
	 * Sets the {@link AdvancedAsciiDataColumn} to use as wave when exporting the {@link CassisSpectrum}
	 *
	 * @param index The id of the column to select
	 */
	public void setSelectedWaveColumn(int index) {
		selectedWave = index;
	}

	/**
	 * Sets the {@link AdvancedAsciiDataColumn} to use as flux when exporting the {@link CassisSpectrum}
	 *
	 * @param index The id of the column to select
	 */
	public void setSelectedFluxColumn(int index) {
		selectedFlux = index;
	}

	/**
	 * Change the unit of an {@link AdvancedAsciiDataColumn} from the {@link ParsedAsciiFile}
	 *
	 * @param index The ID identifying the targeted column
	 * @param newUnit The new unit
	 */
	public void changeUnit(int index, String newUnit) {
		getColumn(index).setUnit(newUnit);
		if (index == selectedFlux) {
			configuration.setFluxUnit(newUnit);
		}
		if (index == selectedWave) {
			configuration.setWaveUnit(newUnit);
		}
		fireDataChanged(new ModelChangedEvent(EventSorter.COLUMN_UNIT_EVENT));
	}

	/**
	 * Change the name of an {@link AdvancedAsciiDataColumn} from the {@link ParsedAsciiFile}
	 *
	 * @param index The ID identifying the targeted column
	 * @param newName The new name
	 */
	public void changeName(int index, String newName) {
		getColumn(index).setName(newName);
		fireDataChanged(new ModelChangedEvent(EventSorter.COLUMN_NAME_EVENT));
	}

	/**
	 * Saves the current {@link AdvancedAsciiFileConfiguration} to the given Path
	 *
	 * @param file The file for saving the configuration
	 */
	public void saveConfiguration(File file) {
		try {
			if (selectedWave != -1) {
				configuration.setWaveIndex(selectedWave);
				configuration.setWaveUnit(getColumn(selectedWave).getUnit());
				configuration.setWaveColumnName(getColumn(selectedWave).getName());
			}
			if (selectedFlux != -1) {
				configuration.setFluxIndex(selectedFlux);
				configuration.setFluxUnit(getColumn(selectedFlux).getUnit());
				configuration.setFluxColumnName(getColumn(selectedFlux).getName());
			}
			configuration.saveConfiguration(file);
		} catch (IllegalArgumentException e) {
			fireDataChanged(new ModelChangedEvent(EventSorter.CONFIGURATION_SAVING_ERROR_EVENT));
		}
	}

	/**
	 * Loads an {@link AdvancedAsciiFileConfiguration} from the given file
	 *
	 * @param file The file from where to load the configuration
	 */
	public void loadConfiguration(File file) {
		try {
			configuration.loadConfiguration(file);
			fireConfigurationEvents();
		} catch (IllegalArgumentException e) {
			fireDataChanged(new ModelChangedEvent(EventSorter.CONFIGURATION_LOADING_ERROR_EVENT));
		}
	}

	/**
	 * Initiate the preview parsing of the input file with the current {@link AdvancedAsciiFileConfiguration}. The
	 * results will be stored into the {@link ParsedAsciiFile}
	 */
	public void previewParsing() {
		AdvancedAsciiParser parser = new AdvancedAsciiParser(inputFile, configuration, parsedFile);
		if (headerChanged) {
			parser.redefineHeaders();
			headerChanged = false;
		}
		try {
			parsedFile = parser.previewParsing();
			fireDataChanged(new ModelChangedEvent(EventSorter.PREVIEW_DONE_EVENT));
		} catch (IllegalStateException e) {
			// No treatment in preview
		}
	}

	/**
	 * Initiate the full parsing of the input file with the current {@link AdvancedAsciiFileConfiguration}. The
	 * results will be stored into the {@link ParsedAsciiFile}
	 */
	public void parseFile() throws IllegalStateException {
		AdvancedAsciiParser parser = new AdvancedAsciiParser(inputFile, configuration, parsedFile);
		if (headerChanged) {
			parser.redefineHeaders();
			headerChanged = false;
		}
		parsedFile = parser.parseFile();
	}

	/**
	 * @return The {@link ParsedAsciiFile}
	 */
	public ParsedAsciiFile getParsedFile() {
		return parsedFile;
	}

	/**
	 * @return A {@link CassisSpectrum} created with the currently selected wave and flux columns
	 */
	public CassisSpectrum generateCassisSpectrum() {
		return parsedFile.buildCassisSpectrum(getColumn(selectedWave), getColumn(selectedFlux));
	}

	/**
	 * Remove the metadata stored at given indexes
	 *
	 * @param indexes The indexes to remove
	 */
	public void removeMetadata(int... indexes) {
		for (int i = indexes.length - 1 ; i >= 0 ; i--) {
			this.parsedFile.getListCassisMetadata().remove(indexes[i]);
		}
		fireDataChanged(new ModelChangedEvent(EventSorter.METADA_EDITED_EVENT));
	}

	/**
	 * Adds a new metadata to the model
	 *
	 * @param metadata The metadata to add
	 */
	public void addMetadata(CassisMetadata metadata) {
		this.parsedFile.addCassisMetadata(metadata);
		fireDataChanged(new ModelChangedEvent(EventSorter.METADA_EDITED_EVENT));
	}

	/**
	 * @param metadataSeparator the metadataSeparator to set
	 */
	public void setMetadataSeparator(String metadataSeparator) {
		this.configuration.setMetadataSeparator(metadataSeparator);
		fireDataChanged(new ModelChangedEvent(EventSorter.METADATA_SEPARATOR_EVENT));
	}

	/**
	 * @param startMetadata the startMetadata to set
	 */
	public void setStartMetadata(int startMetadata) {
		this.configuration.setStartMetadata(startMetadata);
		fireDataChanged(new ModelChangedEvent(EventSorter.METADATA_FIRST_LINE_EVENT));
	}

	/**
	 * @param endMetadata the endMetadata to set
	 */
	public void setEndMetadata(int endMetadata) {
		this.configuration.setEndMetadata(endMetadata);
		fireDataChanged(new ModelChangedEvent(EventSorter.METADATA_LAST_LINE_EVENT));
	}

	/**
	 * @param dataSeparator the dataSeparator to set
	 */
	public void setDataSeparator(String dataSeparator) {
		this.configuration.setDataSeparator(dataSeparator);
		fireDataChanged(new ModelChangedEvent(EventSorter.DATA_SEPARATOR_EVENT));
	}

	/**
	 * @param headerSeparator the headerSeparator to set
	 */
	public void setHeaderSeparator(String headerSeparator) {
		this.configuration.setHeaderSeparator(headerSeparator);
		this.headerChanged = true;
		fireDataChanged(new ModelChangedEvent(EventSorter.HEADERS_SEPARATOR_EVENT));
	}

	/**
	 * @param startHeaders the startHeaders to set
	 */
	public void setStartHeaders(int startHeaders) {
		this.configuration.setStartHeaders(startHeaders);
		this.headerChanged = true;
		fireDataChanged(new ModelChangedEvent(EventSorter.HEADERS_FIRST_LINE_EVENT));
	}

	/**
	 * @param startData the startData to set
	 */
	public void setStartData(int startData) {
		this.configuration.setStartData(startData);
		fireDataChanged(new ModelChangedEvent(EventSorter.DATA_FIRST_LINE_EVENT));
	}

	/**
	 * @param endHeaders the endHeaders to set
	 */
	public void setEndHeaders(int endHeaders) {
		this.configuration.setEndHeaders(endHeaders);
		this.headerChanged = true;
		fireDataChanged(new ModelChangedEvent(EventSorter.HEADERS_LAST_LINE_EVENT));
	}

	/**
	 * @param endData the endData to set
	 */
	public void setEndData(int endData) {
		this.configuration.setEndData(endData);
		fireDataChanged(new ModelChangedEvent(EventSorter.DATA_LAST_LINE_EVENT));
	}

	/**
	 * @param nbColumns the nbColumns to set
	 */
	public void setNbColumns(int nbColumns) {
		this.configuration.setNbColumns(nbColumns);
		headerChanged = true;
		fireDataChanged(new ModelChangedEvent(EventSorter.NB_COLUMNS_EVENT));
	}

	/**
	 * @param metadataPresent the metadataPresent to set
	 */
	public void setMetadataPresent(boolean metadataPresent) {
		this.configuration.setMetadataPresent(metadataPresent);
		fireDataChanged(new ModelChangedEvent(EventSorter.METADATA_PRESENT_EVENT));
	}

	/**
	 * @param headersPresent the headersPresent to set
	 */
	public void setHeadersPresent(boolean headersPresent) {
		this.configuration.setHeadersPresent(headersPresent);
		this.headerChanged = true;
		fireDataChanged(new ModelChangedEvent(EventSorter.HEADERS_PRESENT_EVENT));
	}

	/**
	 * @param asColumns the asColumns to set
	 */
	public void setAsColumns(boolean asColumns) {
		this.configuration.setAsColumns(asColumns);
		fireDataChanged(new ModelChangedEvent(EventSorter.DATA_ORIENTATION_EVENT));
	}

	public void setDefaultNanValue(double nanValue) {
		this.configuration.setDefaultNanValue(nanValue);
		fireDataChanged(new ModelChangedEvent(EventSorter.DEFAULT_NAN_VALUE_EVENT));
	}

	/**
	 * Links the parsing model to the Spectrum Manager
	 *
	 * @param control The Spectrum Manager control
	 */
	public void setLinkedControl(CassisSpectrumControl control) {
		this.linkedControl = control;
	}

	/**
	 * Checks if the parsing model is integrated to the Spectrum Manager
	 *
	 * @return true if the linkedControl exists, false otherwise
	 */
	public boolean isIntegrated() {
		return this.linkedControl != null;
	}

	/**
	 * Runs the runnable defined in the spectrum manager's control if it exists
	 */
	public void runSpectrumManagerAction() {
		if (linkedControl != null) {
			if (linkedControl.getAction() != null) {
				linkedControl.getAction().run();
			}
		}
	}

	/**
	 * Runs the export runnable if it exists
	 */
	public void runExportAction() {
		if (exportAction != null) {
			exportAction.run();
		}
	}

	/**
	 * Sets the action to run when exporting the parsed spectrum
	 *
	 * @param exportAction The action to run
	 */
	public void setExportAction(Runnable exportAction) {
		this.exportAction = exportAction;
	}

	private void fireConfigurationEvents() {
		fireEvents(EventSorter.getConfigurationEvents());
	}

	private void fireInitialConfigurationEvents() {
		fireEvents(EventSorter.getInitialConfigurationEvents());
	}

	private void fireEvents(String[] events) {
		for (String event : events) {
			fireDataChanged(new ModelChangedEvent(event));
		}
	}

	private AdvancedAsciiDataColumn getColumn(int index) {
		for (AdvancedAsciiDataColumn column : parsedFile.getDataColumns()) {
			if (index == column.getIndexColumn()) {
				return column;
			}
		}
		return null;
	}

}
