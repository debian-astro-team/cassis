/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.ascii.parser.configuration.gui;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.file.ascii.parser.AdvancedAsciiControl;
import eu.omp.irap.cassis.file.ascii.parser.configuration.AdvancedAsciiFileConfiguration;

/**
 * @author bastienkovac
 *
 * A JPanel that handles the edition of the {@link AdvancedAsciiFileConfiguration} to use when parsing a file
 *
 */
public class ConfigurationPanel extends JPanel {

	/**
	 *
	 */
	private static final long serialVersionUID = -2149334930413041787L;

	private MetadataConfigurationPanel metadataConfigurationPanel;
	private DataConfigurationPanel dataConfigurationPanel;
	private SpectrumGenerationPanel spectrumGenerationPanel;
	private HeaderConfigurationPanel headerConfigurationPanel;

	private JPanel saveConfigurationPanel;

	private AdvancedAsciiControl control;


	/**
	 * Creates a new ConfigurationPanel
	 *
	 * @param control The controller
	 */
	public ConfigurationPanel(AdvancedAsciiControl control) {
		super();
		this.control = control;
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(getHeaderConf());
		add(getDataConfigurationPanel());
		add(getSaveConfigurationPanel());
		add(Box.createVerticalGlue());
		add(getSpectrumGenerationPanel());
	}

	private JPanel getHeaderConf() {
		JPanel headerConf = new JPanel();
		headerConf.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(), "Headers"));
		headerConf.setLayout(new BoxLayout(headerConf, BoxLayout.Y_AXIS));
		headerConf.add(getMetadataConfigurationPanel());
		headerConf.add(getHeaderConfigurationPanel());
		add(headerConf);
		return headerConf;
	}

	/**
	 * @return The {@link HeaderConfigurationPanel} used to edit the headers information from the current
	 * {@link AdvancedAsciiFileConfiguration}
	 */
	public HeaderConfigurationPanel getHeaderConfigurationPanel() {
		if (headerConfigurationPanel == null) {
			headerConfigurationPanel = new HeaderConfigurationPanel(control);
		}
		return headerConfigurationPanel;
	}

	/**
	 * @return The {@link SpectrumGenerationPanel} used to export a {@link CassisSpectrum}
	 */
	public SpectrumGenerationPanel getSpectrumGenerationPanel() {
		if (spectrumGenerationPanel == null) {
			spectrumGenerationPanel = new SpectrumGenerationPanel(control);
		}
		return spectrumGenerationPanel;
	}

	/**
	 * @return The {@link MetadataConfigurationPanel} used to edit the metadata information from the current
	 * {@link AdvancedAsciiFileConfiguration}
	 */
	public MetadataConfigurationPanel getMetadataConfigurationPanel() {
		if (metadataConfigurationPanel == null) {
			metadataConfigurationPanel = new MetadataConfigurationPanel(control);
		}
		return metadataConfigurationPanel;
	}

	/**
	 * @return The {@link DataConfigurationPanel} used to edit the data information from the current
	 * {@link AdvancedAsciiFileConfiguration}
	 */
	public DataConfigurationPanel getDataConfigurationPanel() {
		if (dataConfigurationPanel == null) {
			dataConfigurationPanel = new DataConfigurationPanel(control);
		}
		return dataConfigurationPanel;
	}

	private JPanel getSaveConfigurationPanel() {
		if (saveConfigurationPanel == null) {
			saveConfigurationPanel = new JPanel();
			saveConfigurationPanel.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
			saveConfigurationPanel.setLayout(new BoxLayout(saveConfigurationPanel, BoxLayout.X_AXIS));
			saveConfigurationPanel.setMaximumSize(saveConfigurationPanel.getPreferredSize());
		}
		return saveConfigurationPanel;
	}

}
