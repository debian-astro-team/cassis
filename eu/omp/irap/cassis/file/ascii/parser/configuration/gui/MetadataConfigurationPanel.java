/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.ascii.parser.configuration.gui;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import eu.omp.irap.cassis.file.ascii.parser.AdvancedAsciiControl;
import eu.omp.irap.cassis.file.ascii.parser.configuration.AdvancedAsciiFileConfiguration;
import eu.omp.irap.cassis.file.ascii.parser.util.gui.ConfigurationIntervalTextField;

/**
 * @author bastienkovac
 *
 * A JPanel used to edit all information relative to the metadata in a {@link AdvancedAsciiFileConfiguration}
 *
 */
public class MetadataConfigurationPanel extends JPanel {

	/**
	 *
	 */
	private static final long serialVersionUID = 4339282939435068168L;

	private JPanel bottomPanel;
	private SeparatorComboBox separatorMetadata;
	private ConfigurationIntervalTextField firstLineMetadata, lastLineMetadata;
	private JCheckBox metadataPresent;

	private AdvancedAsciiControl control;


	/**
	 * Creates a new MetadataConfigurationPanel
	 *
	 * @param control The controller
	 */
	public MetadataConfigurationPanel(AdvancedAsciiControl control) {
		super();
		this.control = control;
		setBorder(new TitledBorder(BorderFactory.createEtchedBorder(), "Metadata"));
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		JPanel tmp = new JPanel(new FlowLayout(FlowLayout.LEADING));
		tmp.add(getMetadataPresent());
		tmp.setMaximumSize(new Dimension(tmp.getMaximumSize().width, tmp.getPreferredSize().height));
		add(tmp);
		add(getBottomPanel());
	}

	/**
	 * @return A {@link JCheckBox} that represents if the metadata is present in the file to parse or not
	 */
	public JCheckBox getMetadataPresent() {
		if (metadataPresent == null) {
			metadataPresent = new JCheckBox("Check if the file contains metadata");
			metadataPresent.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					setChildrenEnabled(metadataPresent.isSelected());
					control.configureMetadataPresent(metadataPresent.isSelected());
				}
			});
			metadataPresent.setName("Enable Metadata");
			metadataPresent.setSelected(control.getModel().getConfiguration().isMetadataPresent());
			setChildrenEnabled(control.getModel().getConfiguration().isHeadersPresent());
		}
		return metadataPresent;
	}

	/**
	 * @return A {@link SeparatorComboBox} used to represent the separator between metadata in the file to parse
	 */
	public SeparatorComboBox getSeparatorMetadata() {
		if (separatorMetadata == null) {
			separatorMetadata = new SeparatorComboBox();
			separatorMetadata.setSelected(control.getModel().getConfiguration().getMetadataSeparatorRepresentation());
			separatorMetadata.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					control.configureMetadataSeparator(separatorMetadata.getSelectedString());
				}
			});
			separatorMetadata.setName("Separator Metadata");
		}
		return separatorMetadata;
	}

	/**
	 * @return A {@link JFormattedTextField} that only takes Integer. Represents the number of the first line
	 * where the metadata is present
	 */
	public ConfigurationIntervalTextField getFirstLineMetadata() {
		if (firstLineMetadata == null) {
			firstLineMetadata = new ConfigurationIntervalTextField(control.getModel().getLastLineIndex());
			firstLineMetadata.addPropertyChangeListener("value", new PropertyChangeListener() {

				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					control.configureMetadataStart(firstLineMetadata.getIntValue() - 1);
				}
			});
			firstLineMetadata.setValue(control.getModel().getConfiguration().getStartMetadata() + 1);
			firstLineMetadata.setName("First line Metadata");
		}
		return firstLineMetadata;
	}

	/**
	 * @return A {@link JFormattedTextField} that only takes Integer. Represents the number of the last line
	 * where the metadata is present
	 */
	public ConfigurationIntervalTextField getLastLineMetadata() {
		if (lastLineMetadata == null) {
			lastLineMetadata = new ConfigurationIntervalTextField(control.getModel().getLastLineIndex());
			lastLineMetadata.addPropertyChangeListener("value", new PropertyChangeListener() {

				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					control.configureMetadataEnd(lastLineMetadata.getIntValue() - 1);
				}
			});
			lastLineMetadata.setValue(control.getModel().getConfiguration().getEndMetadata() + 1);
			lastLineMetadata.setName("Last line Metadata");
		}
		return lastLineMetadata;
	}

	private JPanel getBottomPanel() {
		if (bottomPanel == null) {
			bottomPanel = new JPanel();
			GroupLayout layout = new GroupLayout(bottomPanel);
			layout.setAutoCreateContainerGaps(true);
			layout.setAutoCreateGaps(true);

			JLabel separatorOptions = new JLabel("Separator options:");
			JLabel firstLine = new JLabel("First line:");
			JLabel lastLine = new JLabel("Last line:");

			layout.setHorizontalGroup(
					layout.createSequentialGroup()
						.addGroup(layout.createParallelGroup(Alignment.TRAILING)
								.addComponent(separatorOptions)
								.addComponent(firstLine)
								.addComponent(lastLine))
						.addGroup(layout.createParallelGroup(Alignment.LEADING)
								.addComponent(getSeparatorMetadata())
								.addComponent(getFirstLineMetadata())
								.addComponent(getLastLineMetadata()))
						);

			layout.setVerticalGroup(
					layout.createSequentialGroup()
						.addGroup(layout.createParallelGroup(Alignment.BASELINE)
								.addComponent(separatorOptions)
								.addComponent(getSeparatorMetadata()))
						.addGroup(layout.createParallelGroup(Alignment.BASELINE)
								.addComponent(firstLine)
								.addComponent(getFirstLineMetadata()))
						.addGroup(layout.createParallelGroup(Alignment.BASELINE)
								.addComponent(lastLine)
								.addComponent(getLastLineMetadata()))
						);

			bottomPanel.setLayout(layout);
		}
		return bottomPanel;
	}

	/**
	 * Enables or disables every component of this panel
	 *
	 * @param enabled The value of setEnabled()
	 */
	public void setChildrenEnabled(boolean enabled) {
		getSeparatorMetadata().setEnabled(enabled);
		getFirstLineMetadata().setEnabled(enabled);
		getLastLineMetadata().setEnabled(enabled);
	}

}
