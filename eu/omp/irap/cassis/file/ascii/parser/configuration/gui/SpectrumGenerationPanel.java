/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.ascii.parser.configuration.gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.TitledBorder;

import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.file.ascii.parser.AdvancedAsciiControl;
import eu.omp.irap.cassis.file.ascii.parser.data.AdvancedAsciiDataColumn;

/**
 * @author bastienkovac
 *
 * A JPanel that allows the selection of the Wave and Flux columns used to generate a {@link CassisSpectrum}, as
 * well as their units
 *
 */
public class SpectrumGenerationPanel extends JPanel {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private AdvancedAsciiControl control;
	private JComboBox<AdvancedAsciiDataColumn> waveColumn, fluxColumn;
	private AdvancedAsciiDataColumn lastWaveSelected, lastFluxSelected;
	private JComboBox<String> waveColumnUnit;
	private JComboBox<String> fluxColumnUnit;


	/**
	 * Creates a new SpectrumGenrationPanel
	 *
	 * @param control The controller
	 */
	public SpectrumGenerationPanel(AdvancedAsciiControl control) {
		super();
		this.control = control;
		setBorder(new TitledBorder(BorderFactory.createEtchedBorder(), "Generate spectrum"));
		setPanelChildrenEnabled(false);
		GroupLayout layout = initGroupLayout();
		setLayout(layout);
		setMaximumSize(new Dimension(getMaximumSize().width, getPreferredSize().height));
		refreshDataColumns();
	}

	/**
	 * Enables or disables the combo-boxes in the panel
	 *
	 * @param enabled The value of setEnabled
	 */
	public void setPanelChildrenEnabled(boolean enabled) {
		getWaveColumn().setEnabled(enabled);
		getWaveColumnUnit().setEnabled(enabled);
		getFluxColumn().setEnabled(enabled);
		getFluxColumnUnit().setEnabled(enabled);
	}

	/**
	 * Refresh the data columns held by the view
	 */
	public void refreshDataColumns() {
		clearComboBoxes();
		List<AdvancedAsciiDataColumn> columns = control.getModel().getParsedFile().getDataColumns();
		for (AdvancedAsciiDataColumn column : columns) {
			getWaveColumn().addItem(column);
			getFluxColumn().addItem(column);
		}
		if (columns.size() < 2) {
			return;
		}
		getWaveColumn().setSelectedItem(columns.get(0));
		getFluxColumn().setSelectedItem(columns.get(1));
		lastWaveSelected = columns.get(0);
		lastFluxSelected = columns.get(1);
	}

	/**
	 * @return a JCombobox holding all the columns of the model's parsed file.
	 * The selected column will be used as wave column when creating the {@link CassisSpectrum}
	 */
	public JComboBox<AdvancedAsciiDataColumn> getWaveColumn() {
		if (waveColumn == null) {
			waveColumn = new JComboBox<AdvancedAsciiDataColumn>();
			waveColumn.setEditable(true);
			waveColumn.addActionListener(new ColumnComboBoxListener(waveColumn, getWaveColumnUnit(), lastWaveSelected, true));
			waveColumn.setName("Wave column");
			waveColumn.setPrototypeDisplayValue(new AdvancedAsciiDataColumn("Wave Column"));
		}
		return waveColumn;
	}

	/**
	 * @return a JComboBo  holding all the columns of the model's parsed file.
	 * The selected column will be used as flux column when creating the {@link CassisSpectrum}
	 */
	public JComboBox<AdvancedAsciiDataColumn> getFluxColumn() {
		if (fluxColumn == null) {
			fluxColumn = new JComboBox<AdvancedAsciiDataColumn>();
			fluxColumn.setEditable(true);
			fluxColumn.addActionListener(new ColumnComboBoxListener(fluxColumn, getFluxColumnUnit(), lastFluxSelected, false));
			fluxColumn.setName("Flux column");
			fluxColumn.setPrototypeDisplayValue(new AdvancedAsciiDataColumn("Flux Column"));
		}
		return fluxColumn;
	}

	/**
	 * @return A {@link JComboBox} holding all of Cassis' wave units
	 */
	public JComboBox<String> getWaveColumnUnit() {
		if (waveColumnUnit == null) {
			waveColumnUnit = new JComboBox<>();
			for (UNIT unit : UNIT.values()) {
				if (UNIT.isWaveUnit(unit)) {
					waveColumnUnit.addItem(unit.getValString());
				}
			}
			waveColumnUnit.addItem(UNIT.UNKNOWN.getValString());
			waveColumnUnit.setSelectedItem(UNIT.UNKNOWN.getValString());
			waveColumnUnit.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					if (getWaveColumn().getSelectedItem() instanceof AdvancedAsciiDataColumn) {
						lastWaveSelected = (AdvancedAsciiDataColumn) getWaveColumn().getSelectedItem();
					}
					String newUnit = (String) waveColumnUnit.getSelectedItem();
					if (newUnit != null && lastWaveSelected != null && !newUnit.equals(lastWaveSelected.getUnit())) {
						control.editColumnUnit(lastWaveSelected.getIndexColumn(), newUnit);
					}
				}
			});
			waveColumnUnit.setName("Wave unit");
		}
		return waveColumnUnit;
	}

	/**
	 * @return A {@link JComboBox} holding all of Cassis' flux units
	 */
	public JComboBox<String> getFluxColumnUnit() {
		if (fluxColumnUnit == null) {
			fluxColumnUnit = new JComboBox<>();
			for (UNIT unit : UNIT.values()) {
				if (!UNIT.isWaveUnit(unit)) {
					fluxColumnUnit.addItem(unit.getValString());
				}
			}
			fluxColumnUnit.setSelectedItem(UNIT.UNKNOWN.getValString());
			fluxColumnUnit.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					if (getFluxColumn().getSelectedItem() instanceof AdvancedAsciiDataColumn) {
						lastFluxSelected = (AdvancedAsciiDataColumn) getFluxColumn().getSelectedItem();
					}
					String newUnit = (String) fluxColumnUnit.getSelectedItem();
					if (newUnit != null && lastFluxSelected != null && !newUnit.equals(lastFluxSelected.getUnit())) {
						control.editColumnUnit(lastFluxSelected.getIndexColumn(), newUnit);
					}
				}
			});
			fluxColumnUnit.setName("Flux unit");
			fluxColumnUnit.setEditable(true);
		}
		return fluxColumnUnit;
	}

	private GroupLayout initGroupLayout() {
		GroupLayout layout = new GroupLayout(this);
		layout.setAutoCreateContainerGaps(true);
		layout.setAutoCreateGaps(true);

		JLabel xCol = new JLabel("Wave Column:");
		JLabel yCol = new JLabel("Flux Column:");

		layout.setHorizontalGroup(
				layout.createSequentialGroup()
					.addGroup(layout.createParallelGroup()
							.addComponent(xCol)
							.addGroup(layout.createSequentialGroup()
									.addPreferredGap(xCol, getWaveColumn(), ComponentPlacement.INDENT)
									.addComponent(getWaveColumn()))
							.addComponent(yCol)
							.addGroup(layout.createSequentialGroup()
									.addPreferredGap(yCol, getFluxColumn(), ComponentPlacement.INDENT)
									.addComponent(getFluxColumn())))
					.addGroup(layout.createParallelGroup(Alignment.LEADING)
							.addComponent(getWaveColumnUnit())
							.addComponent(getFluxColumnUnit()))
					);

		layout.setVerticalGroup(
				layout.createSequentialGroup()
					.addComponent(xCol)
					.addGroup(layout.createParallelGroup(Alignment.CENTER)
							.addComponent(getWaveColumn())
							.addComponent(getWaveColumnUnit()))
					.addComponent(yCol)
					.addGroup(layout.createParallelGroup(Alignment.CENTER)
							.addComponent(getFluxColumn())
							.addComponent(getFluxColumnUnit()))
					);

		return layout;
	}

	private void clearComboBoxes() {
		getWaveColumn().removeAllItems();
		getWaveColumn().getEditor().setItem("");
		getFluxColumn().removeAllItems();
		getFluxColumn().getEditor().setItem("");
	}

	private class ColumnComboBoxListener implements ActionListener {

		private JComboBox<AdvancedAsciiDataColumn> linkedComboBox;
		private JComboBox<String> linkedUnitComboBox;
		private AdvancedAsciiDataColumn lastColumnSelected;
		private boolean isWaveColumn;


		public ColumnComboBoxListener(JComboBox<AdvancedAsciiDataColumn> linkedComboBox, JComboBox<String> linkedUnitComboBox,
				AdvancedAsciiDataColumn lastColumnSelected, boolean isWaveColumn) {
			this.linkedComboBox = linkedComboBox;
			this.linkedUnitComboBox = linkedUnitComboBox;
			this.lastColumnSelected = lastColumnSelected;
			this.isWaveColumn = isWaveColumn;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			if ("comboBoxEdited".equals(e.getActionCommand())) {
				editNameColumn();
			} else {
				editSelection();
			}
		}

		private void editNameColumn() {
			if (linkedComboBox.getSelectedItem() instanceof String) {
				String newName = (String) linkedComboBox.getSelectedItem();
				control.editColumnName(lastColumnSelected.getIndexColumn(), newName);
			}
		}

		private void editSelection() {
			Object selectedItem = linkedComboBox.getSelectedItem();
			if (selectedItem != null && selectedItem instanceof AdvancedAsciiDataColumn) {
				lastColumnSelected = (AdvancedAsciiDataColumn) selectedItem;
				linkedUnitComboBox.setSelectedItem(lastColumnSelected.getUnit());
				informControllerAboutSelection();
			}
		}

		private void informControllerAboutSelection() {
			if (isWaveColumn) {
				control.selectWaveColumn(lastColumnSelected.getIndexColumn());
			} else {
				control.selectFluxColumn(lastColumnSelected.getIndexColumn());
			}
		}

	}

}
