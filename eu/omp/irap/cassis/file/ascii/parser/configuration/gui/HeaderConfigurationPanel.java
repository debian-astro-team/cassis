/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.ascii.parser.configuration.gui;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import eu.omp.irap.cassis.file.ascii.parser.AdvancedAsciiControl;
import eu.omp.irap.cassis.file.ascii.parser.configuration.AdvancedAsciiFileConfiguration;
import eu.omp.irap.cassis.file.ascii.parser.util.gui.ConfigurationIntervalTextField;

/**
 * @author bastienkovac
 *
 * A JPanel used to edit all information relative to the headers in a {@link AdvancedAsciiFileConfiguration}
 *
 */
public class HeaderConfigurationPanel extends JPanel {

	/**
	 *
	 */
	private static final long serialVersionUID = -8766688361367633851L;
	private AdvancedAsciiControl control;
	private SeparatorComboBox separatorHeader;
	private ConfigurationIntervalTextField firstLineHeader, lastLineHeader;
	private JCheckBox headerPresent;

	private JPanel bottomPanel;


	/**
	 * Creates a new HeaderConfigurationPanel
	 *
	 * @param control The controller
	 */
	public HeaderConfigurationPanel(AdvancedAsciiControl control) {
		super();
		this.control = control;
		setBorder(new TitledBorder(BorderFactory.createEtchedBorder(), "Column values"));
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		JPanel tmp = new JPanel(new FlowLayout(FlowLayout.LEADING));
		tmp.add(getHeaderPresent());
		tmp.setMaximumSize(new Dimension(tmp.getMaximumSize().width, tmp.getPreferredSize().height));
		add(tmp);
		add(getBottomPanel());
	}

	/**
	 * @return A {@link JCheckBox} that represents if the headers are present in the file to parse or not
	 */
	public JCheckBox getHeaderPresent() {
		if (headerPresent == null) {
			headerPresent = new JCheckBox("Check if the file contains headers");
			headerPresent.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					setHeaderChildrenEnabed(headerPresent.isSelected());
					control.configureHeadersPresent(headerPresent.isSelected());
				}
			});
			headerPresent.setName("Enable Header");
			headerPresent.setSelected(control.getModel().getConfiguration().isHeadersPresent());
			setHeaderChildrenEnabed(control.getModel().getConfiguration().isHeadersPresent());
		}
		return headerPresent;
	}

	/**
	 * @return A {@link SeparatorComboBox} used to represent the separator between headers in the file to parse
	 */
	public SeparatorComboBox getSeparatorHeader() {
		if (separatorHeader == null) {
			separatorHeader = new SeparatorComboBox();
			separatorHeader.setSelected(control.getModel().getConfiguration().getHeaderSeparatorRepresentation());
			separatorHeader.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					control.configureHeadersSeparator(separatorHeader.getSelectedString());
				}
			});
			separatorHeader.setName("Separator Header");
		}
		return separatorHeader;
	}

	/**
	 * @return A {@link JFormattedTextField} that only takes Integer. Represents the number of the first line
	 * where the headers are present
	 */
	public ConfigurationIntervalTextField getFirstLineHeader() {
		if (firstLineHeader == null) {
			firstLineHeader = new ConfigurationIntervalTextField(control.getModel().getLastLineIndex());
			firstLineHeader.setPreferredSize(new Dimension(60, firstLineHeader.getPreferredSize().height));
			firstLineHeader.addPropertyChangeListener("value", new PropertyChangeListener() {

				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					control.configureHeadersStart(firstLineHeader.getIntValue() - 1);
				}
			});
			firstLineHeader.setValue(control.getModel().getConfiguration().getStartHeaders() + 1);
			firstLineHeader.setName("First line Header");
		}
		return firstLineHeader;
	}

	/**
	 * @return A {@link JFormattedTextField} that only takes Integer. Represents the number of the last line
	 * where the headers are present
	 */
	public ConfigurationIntervalTextField getLastLineHeader() {
		if (lastLineHeader == null) {
			lastLineHeader = new ConfigurationIntervalTextField(control.getModel().getLastLineIndex());
			lastLineHeader.addPropertyChangeListener("value", new PropertyChangeListener() {

				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					control.configureHeadersEnd(lastLineHeader.getIntValue() - 1);
				}
			});
			lastLineHeader.setValue(control.getModel().getConfiguration().getEndHeaders() + 1);
			lastLineHeader.setName("Last line Header");
		}
		return lastLineHeader;
	}

	/**
	 * Enables or disables this panel's components
	 *
	 * @param enabled The value of setEnabled()
	 */
	public void setHeaderChildrenEnabed(boolean enabled) {
		getFirstLineHeader().setEnabled(enabled);
		getLastLineHeader().setEnabled(enabled);
		getSeparatorHeader().setEnabled(enabled);
	}

	private JPanel getBottomPanel() {
		if (bottomPanel == null) {
			bottomPanel = new JPanel();
			GroupLayout layout = new GroupLayout(bottomPanel);
			layout.setAutoCreateContainerGaps(true);
			layout.setAutoCreateGaps(true);
			JLabel sepOption = new JLabel("Separator options:");
			JLabel firstLine = new JLabel("First line:");
			JLabel lastLine = new JLabel("Last line:");

			layout.setHorizontalGroup(
					layout.createSequentialGroup()
						.addGroup(layout.createParallelGroup(Alignment.TRAILING)
								.addComponent(sepOption)
								.addComponent(firstLine)
								.addComponent(lastLine))
						.addGroup(layout.createParallelGroup(Alignment.LEADING)
								.addComponent(getSeparatorHeader())
								.addComponent(getFirstLineHeader())
								.addComponent(getLastLineHeader()))
					);

			layout.setVerticalGroup(
					layout.createSequentialGroup()
						.addGroup(layout.createParallelGroup(Alignment.BASELINE)
								.addComponent(sepOption)
								.addComponent(getSeparatorHeader()))
						.addGroup(layout.createParallelGroup(Alignment.BASELINE)
								.addComponent(firstLine)
								.addComponent(getFirstLineHeader()))
						.addGroup(layout.createParallelGroup(Alignment.BASELINE)
								.addComponent(lastLine)
								.addComponent(getLastLineHeader()))
					);

			bottomPanel.setLayout(layout);
		}
		return bottomPanel;
	}

}
