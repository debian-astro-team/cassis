/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.ascii.parser.configuration.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

import eu.omp.irap.cassis.file.ascii.parser.util.BaseSeparator;

/**
 * @author bastienkovac
 *
 * A customized JComboBox used to display and select a parsing separator
 *
 */
public class SeparatorComboBox extends JComboBox<String> implements ActionListener {

	/**
	 *
	 */
	private static final long serialVersionUID = 8618537047914133994L;


	/**
	 * Creates a new SeparatorComoBox filled with the elements of {@link BaseSeparator}
	 */
	public SeparatorComboBox() {
		super();
		for (BaseSeparator val : BaseSeparator.values()) {
			addItem(val.getStringName());
		}
		addActionListener(this);
		setEditable(true);
	}

	/**
	 * Checks if a given String exists in the combo-box
	 *
	 * @param input The String to check
	 * @return If the String exists in the combo-box's model
	 */
	public boolean isInModel(String input) {
		DefaultComboBoxModel<String> model = (DefaultComboBoxModel<String>) getModel();
		return model.getIndexOf(input) != -1;
	}

	/**
	 * Selects a given value, if it doesn't exist, it is added to the model
	 *
	 * @param value The value to select
	 */
	public void setSelected(String value) {
		if (isInModel(value)) {
			setSelectedItem(value);
		} else {
			addItem(value);
			setSelectedItem(value);
		}
	}

	/**
	 * @return The current selected String
	 */
	public String getSelectedString() {
		return (String) getSelectedItem();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		super.actionPerformed(e);
		setSelected(getSelectedString());
	}
}
