/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.ascii.parser.configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.file.ascii.parser.util.BaseSeparator;

/**
 * @author bastienkovac
 *
 * This class holds all the configuration needed to parse an ASCII file.
 * It also includes selected flux and wave columns' indexes, as well as their units
 *
 */
public class AdvancedAsciiFileConfiguration {

	private static final String METADATA_PRESENT = "metadataPresent";
	private static final String METADATA_SEPARATOR = "metadataSeparator";
	private static final String METADATA_START = "metadataStart";
	private static final String METADATA_END = "metadataEnd";

	private static final String DATA_SEPARATOR = "dataSeparator";
	private static final String DATA_START = "dataStart";
	private static final String DATA_END = "dataEnd";
	private static final String NB_COLUMNS = "nbColumns";
	private static final String AS_COLULMNS = "asColumns";

	private static final String HEADERS_PRESENT = "headersPresent";
	private static final String HEADERS_SEPARATOR = "headersSeparator";
	private static final String HEADERS_START = "headersStart";
	private static final String HEADERS_END = "headersEnd";

	private static final String WAVE_INDEX = "waveIndex";
	private static final String WAVE_UNIT = "waveUnit";
	private static final String FLUX_INDEX = "fluxIndex";
	private static final String FLUX_UNIT = "fluxUnit";
	private static final String WAVE_COLUMN_NAME = "waveColumnName";
	private static final String FLUX_COLUMN_NAME = "fluxColumnName";

	private static final String NAN_DEFAULT = "nanDefaultValue";

	// Metadata Configuration
	private String metadataSeparator;
	private int startMetadata, endMetadata;

	// Data Configuration
	private String dataSeparator, headerSeparator;
	private int startHeaders, startData;
	private int endHeaders, endData;
	private int nbColumns;

	private boolean metadataPresent, headersPresent, asColumns;

	private int waveIndex, fluxIndex;
	private String waveUnit, fluxUnit;
	private String waveColumnName, fluxColumnName;

	private double defaultNanValue;


	/**
	 * Default configuration
	 */
	private AdvancedAsciiFileConfiguration() {
		metadataSeparator = "\t";
		startMetadata = 0;
		endMetadata = 0;

		dataSeparator = "\t";
		startHeaders = 0;
		endHeaders = 0;
		startData = 0;
		endData = 0;
		headerSeparator = "\t";

		nbColumns = 0;

		metadataPresent = false;
		headersPresent = false;
		asColumns = true;

		waveIndex = 0;
		fluxIndex = 1;
		waveUnit = UNIT.UNKNOWN.getValString();
		fluxUnit = UNIT.UNKNOWN.getValString();

		waveColumnName = "Column Unknown 0";
		fluxColumnName = "Column Unknown 1";

		defaultNanValue = 0;
	}

	/**
	 * @param line The number of the line to check
	 * @return If the number of the line is between the bounds defined for the Metadata
	 */
	public boolean liesInsideMetadata(int line) {
		return line >= startMetadata && line <= endMetadata;
	}

	/**
	 * @param line The number of the line to check
	 * @return If the number of the line is between the bounds defined for the Headers
	 */
	public boolean liesInsideHeaders(int line) {
		return line >= startHeaders && line <= endHeaders;
	}

	/**
	 * @param line The number of the line to check
	 * @return If the number of the line is between the bounds defined for the Data
	 */
	public boolean liesInsideData(int line) {
		return line >= startData && line <= endData;
	}

	/**
	 * @return A default Configuration
	 */
	public static AdvancedAsciiFileConfiguration getDefaultConfiguration() {
		return new AdvancedAsciiFileConfiguration();
	}

	/**
	 * Saves the configuration in a property file at the given path
	 *
	 * @param propertiesFile The file where to save the configuration
	 */
	public void saveConfiguration(File propertiesFile) throws IllegalArgumentException {
		if (!isValidForSaving()) {
			throw new IllegalArgumentException();
		}
		Properties prop = new Properties();
		try (OutputStream output = new FileOutputStream(propertiesFile)) {
			// Metadata
			prop.setProperty(METADATA_PRESENT, String.valueOf(metadataPresent));
			prop.setProperty(METADATA_SEPARATOR, metadataSeparator);
			prop.setProperty(METADATA_START, String.valueOf(startMetadata));
			prop.setProperty(METADATA_END, String.valueOf(endMetadata));

			// Data
			prop.setProperty(DATA_SEPARATOR, dataSeparator);
			prop.setProperty(DATA_START, String.valueOf(startData));
			prop.setProperty(DATA_END, String.valueOf(endData));
			prop.setProperty(NB_COLUMNS, String.valueOf(nbColumns));
			prop.setProperty(AS_COLULMNS, String.valueOf(asColumns));

			// Headers
			prop.setProperty(HEADERS_PRESENT, String.valueOf(headersPresent));
			prop.setProperty(HEADERS_SEPARATOR, headerSeparator);
			prop.setProperty(HEADERS_START, String.valueOf(startHeaders));
			prop.setProperty(HEADERS_END, String.valueOf(endHeaders));

			// Columns
			prop.setProperty(WAVE_INDEX, String.valueOf(waveIndex));
			prop.setProperty(WAVE_UNIT, waveUnit);
			prop.setProperty(WAVE_COLUMN_NAME, waveColumnName);
			prop.setProperty(FLUX_INDEX, String.valueOf(fluxIndex));
			prop.setProperty(FLUX_UNIT, fluxUnit);
			prop.setProperty(FLUX_COLUMN_NAME, fluxColumnName);

			prop.setProperty(NAN_DEFAULT, String.valueOf(defaultNanValue));

			prop.store(output, null);
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}
	}

	private boolean isValidForSaving() {
		if (startData < 0 || endData < 0)
			return false;
		if (startHeaders < 0 || endHeaders < 0)
			return false;
		if (startMetadata < 0 || endMetadata < 0) {
			return false;
		}
		return true;
	}

	/**
	 * Loads a property file into this configuration from the given path
	 *
	 * @param propertiesFile The file from where to load the configuration
	 */
	public void loadConfiguration(File propertiesFile) throws IllegalArgumentException {
		Properties prop = new Properties();
		try (InputStream input = new FileInputStream(propertiesFile)) {
			prop.load(input);

			metadataPresent = Boolean.valueOf(prop.getProperty(METADATA_PRESENT));
			metadataSeparator = prop.getProperty(METADATA_SEPARATOR);
			startMetadata = Integer.parseInt(prop.getProperty(METADATA_START));
			endMetadata = Integer.parseInt(prop.getProperty(METADATA_END));

			dataSeparator = prop.getProperty(DATA_SEPARATOR);
			startData = Integer.parseInt(prop.getProperty(DATA_START));
			endData = Integer.parseInt(prop.getProperty(DATA_END));
			nbColumns = Integer.parseInt(prop.getProperty(NB_COLUMNS));
			asColumns = Boolean.valueOf(prop.getProperty(AS_COLULMNS));

			headersPresent = Boolean.valueOf(prop.getProperty(HEADERS_PRESENT));
			headerSeparator = prop.getProperty(HEADERS_SEPARATOR);
			startHeaders = Integer.parseInt(prop.getProperty(HEADERS_START));
			endHeaders = Integer.parseInt(prop.getProperty(HEADERS_END));

			waveIndex = Integer.parseInt(prop.getProperty(WAVE_INDEX));
			waveUnit = prop.getProperty(WAVE_UNIT);
			if (prop.containsKey(WAVE_COLUMN_NAME)) {
				waveColumnName = prop.getProperty(WAVE_COLUMN_NAME);
			}
			fluxIndex = Integer.parseInt(prop.getProperty(FLUX_INDEX));
			fluxUnit = prop.getProperty(FLUX_UNIT);
			if (prop.containsKey(FLUX_COLUMN_NAME)) {
				fluxColumnName = prop.getProperty(FLUX_COLUMN_NAME);
			}
			defaultNanValue = Double.parseDouble(prop.getProperty(NAN_DEFAULT, "0"));
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}
	}

	/**
	 * @return the metadataSeparator
	 */
	public String getMetadataSeparator() {
		return metadataSeparator;
	}

	/**
	 * @return The representation of the metadataSeparator
	 */
	public String getMetadataSeparatorRepresentation() {
		return BaseSeparator.getStringName(metadataSeparator);
	}

	/**
	 * @param metadataSeparator the metadataSeparator to set
	 */
	public void setMetadataSeparator(String metadataSeparator) {
		this.metadataSeparator = BaseSeparator.getSeparator(metadataSeparator);
	}

	/**
	 * @return the startMetadata
	 */
	public int getStartMetadata() {
		return startMetadata;
	}

	/**
	 * @param startMetadata the startMetadata to set
	 */
	public void setStartMetadata(int startMetadata) {
		this.startMetadata = startMetadata;
	}

	/**
	 * @return the endMetadata
	 */
	public int getEndMetadata() {
		return endMetadata;
	}

	/**
	 * @param endMetadata the endMetadata to set
	 */
	public void setEndMetadata(int endMetadata) {
		if (endMetadata >= startMetadata) {
			this.endMetadata = endMetadata;
		} else {
			this.endMetadata = this.startMetadata;
			this.startMetadata = endMetadata;
		}
	}

	/**
	 * @return the dataSeparator
	 */
	public String getDataSeparator() {
		return dataSeparator;
	}

	/**
	 * @return The representation of the dataSeparator
	 */
	public String getDataSeparatorRepresentation() {
		return BaseSeparator.getStringName(dataSeparator);
	}

	/**
	 * @param dataSeparator the dataSeparator to set
	 */
	public void setDataSeparator(String dataSeparator) {
		this.dataSeparator = BaseSeparator.getSeparator(dataSeparator);
	}

	/**
	 * @return the headerSeparator
	 */
	public String getHeaderSeparator() {
		return headerSeparator;
	}

	/**
	 * @return The representation of the headerSeparator
	 */
	public String getHeaderSeparatorRepresentation() {
		return BaseSeparator.getStringName(headerSeparator);
	}

	/**
	 * @param headerSeparator the headerSeparator to set
	 */
	public void setHeaderSeparator(String headerSeparator) {
		this.headerSeparator = BaseSeparator.getSeparator(headerSeparator);
	}

	/**
	 * @return the startHeaders
	 */
	public int getStartHeaders() {
		return startHeaders;
	}

	/**
	 * @param startHeaders the startHeaders to set
	 */
	public void setStartHeaders(int startHeaders) {
		this.startHeaders = startHeaders;
	}

	/**
	 * @return the startData
	 */
	public int getStartData() {
		return startData;
	}

	/**
	 * @param startData the startData to set
	 */
	public void setStartData(int startData) {
		this.startData = startData;
	}

	/**
	 * @return the endHeaders
	 */
	public int getEndHeaders() {
		return endHeaders;
	}

	/**
	 * @param endHeaders the endHeaders to set
	 */
	public void setEndHeaders(int endHeaders) {
		if (endHeaders >= startHeaders) {
			this.endHeaders = endHeaders;
		} else {
			this.endHeaders = this.startHeaders;
			this.startHeaders = endHeaders;
		}
	}

	/**
	 * @return the endData
	 */
	public int getEndData() {
		return endData;
	}

	/**
	 * @param endData the endData to set
	 */
	public void setEndData(int endData) {
		if (endData >= startData) {
			this.endData = endData;
		} else {
			this.endData = this.startData;
			this.startData = endData;
		}
	}

	/**
	 * @return The number of columns
	 */
	public int getNbColumns() {
		return nbColumns;
	}

	/**
	 * @param nbColumns the nbColumns to set
	 */
	public void setNbColumns(int nbColumns) {
		this.nbColumns = nbColumns;
	}

	/**
	 * @return the metadataPresent
	 */
	public boolean isMetadataPresent() {
		return metadataPresent;
	}

	/**
	 * @param metadataPresent the metadataPresent to set
	 */
	public void setMetadataPresent(boolean metadataPresent) {
		this.metadataPresent = metadataPresent;
	}

	/**
	 * @return the headersPresent
	 */
	public boolean isHeadersPresent() {
		return headersPresent;
	}

	/**
	 * @param headersPresent the headersPresent to set
	 */
	public void setHeadersPresent(boolean headersPresent) {
		this.headersPresent = headersPresent;
	}

	public void setAsColumns(boolean asColumn) {
		this.asColumns = asColumn;
	}

	/**
	 * @return the asColumns
	 */
	public boolean isAsColumns() {
		return asColumns;
	}

	/**
	 * @return the waveIndex
	 */
	public int getWaveIndex() {
		return waveIndex;
	}

	/**
	 * @param waveIndex the waveIndex to set
	 */
	public void setWaveIndex(int waveIndex) {
		this.waveIndex = waveIndex;
	}

	/**
	 * @return the fluxIndex
	 */
	public int getFluxIndex() {
		return fluxIndex;
	}

	/**
	 * @param fluxIndex the fluxIndex to set
	 */
	public void setFluxIndex(int fluxIndex) {
		this.fluxIndex = fluxIndex;
	}

	/**
	 * @return the waveUnit
	 */
	public String getWaveUnit() {
		return waveUnit;
	}

	/**
	 * @param waveUnit the waveUnit to set
	 */
	public void setWaveUnit(String waveUnit) {
		this.waveUnit = waveUnit;
	}

	/**
	 * @return the fluxUnit
	 */
	public String getFluxUnit() {
		return fluxUnit;
	}

	/**
	 * @param fluxUnit the fluxUnit to set
	 */
	public void setFluxUnit(String fluxUnit) {
		this.fluxUnit = fluxUnit;
	}

	/**
	 * @return the defaultNanValue
	 */
	public double getDefaultNanValue() {
		return defaultNanValue;
	}

	/**
	 * @param defaultNanValue the defaultNanValue to set
	 */
	public void setDefaultNanValue(double defaultNanValue) {
		this.defaultNanValue = defaultNanValue;
	}

	/**
	 * @return the flux column name.
	 */
	public String getFluxColumnName() {
		return fluxColumnName;
	}

	/**
	 * Change the flux column name.
	 *
	 * @param fluxColumnName The new flux column name to set.
	 */
	public void setFluxColumnName(String fluxColumnName) {
		this.fluxColumnName = fluxColumnName;
	}

	/**
	 * @return the wave column name.
	 */
	public String getWaveColumnName() {
		return waveColumnName;
	}

	/**
	 * Change the wave column name.
	 *
	 * @param waveColumnName The new wave column name to set.
	 */
	public void setWaveColumnName(String waveColumnName) {
		this.waveColumnName = waveColumnName;
	}

}
