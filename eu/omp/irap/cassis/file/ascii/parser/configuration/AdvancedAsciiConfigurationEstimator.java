/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.ascii.parser.configuration;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;

import eu.omp.irap.cassis.file.ascii.parser.util.AdvancedAsciiParsingUtils;
import eu.omp.irap.cassis.file.ascii.parser.util.BaseSeparator;

/**
 * @author bastienkovac
 *
 * This class tries to estimate the {@link AdvancedAsciiFileConfiguration} linked to a file,
 * to give a first approximation to the user.
 *
 */
public class AdvancedAsciiConfigurationEstimator {

	private static final int MAX_LINES_TO_ESTIMATE = 100;

	private File fileToParse;
	private AdvancedAsciiFileConfiguration configuration;
	private Map<BaseSeparator, Integer> occurencesDataSep;
	private List<Integer> dataIndexes;
	private String dataLineReference;
	private Map<String, Integer> nonDataLines;


	/**
	 * Creates a new {@link AdvancedAsciiConfigurationEstimator}
	 *
	 * @param fileToParse The file for which we try to estimate the configuration
	 */
	public AdvancedAsciiConfigurationEstimator(File fileToParse) {
		this.configuration = AdvancedAsciiFileConfiguration.getDefaultConfiguration();
		this.fileToParse = fileToParse;
		this.occurencesDataSep = BaseSeparator.getOccurenceMap();
		this.dataIndexes = new ArrayList<>();
		this.nonDataLines = new HashMap<>();
	}

	/**
	 * @return The estimated configuration
	 */
	public AdvancedAsciiFileConfiguration getEstimatedConfiguration() throws IllegalStateException {
		try {
			estimateConfiguration();
		} catch (Exception e) {
			throw new IllegalStateException(e);
		}
		return configuration;
	}

	private void estimateConfiguration() throws IOException {
		handleData();
		handleNonDataLines();
	}

	private void handleNonDataLines()  {
		List<String> metadataLines = new ArrayList<>();
		for (String line : nonDataLines.keySet()) {
			String sep = guessHeaderSeparator(line);
			if (sep != null) {
				handleHeaderLine(line, sep);
			} else {
				metadataLines.add(line);
			}
		}
		handleMetadataLines(metadataLines);
	}

	private String guessHeaderSeparator(String line) {
		for (BaseSeparator sep : BaseSeparator.values()) {
			if (AdvancedAsciiParsingUtils.countOccurrences(line, sep.getValue()) == configuration.getNbColumns() - 1) {
				return sep.getValue();
			}
		}
		return null;
	}

	private void handleHeaderLine(String line, String sep) {
		configuration.setHeadersPresent(true);
		configuration.setHeaderSeparator(sep);
		configuration.setStartHeaders(nonDataLines.get(line));
		configuration.setEndHeaders(nonDataLines.get(line));
	}

	private void handleMetadataLines(List<String> lines) {
		if (!lines.isEmpty()) {
			configuration.setMetadataPresent(true);
			configuration.setMetadataSeparator(guessMetadataSeparator(lines));
			List<Integer> indexes = new ArrayList<>();
			for (String line : lines) {
				indexes.add(nonDataLines.get(line));
			}
			configuration.setStartMetadata(Collections.min(indexes));
			configuration.setEndMetadata(Collections.max(indexes));
		}
	}

	private String guessMetadataSeparator(List<String> lines) {
		Map<BaseSeparator, Integer> occurrencesMap = BaseSeparator.getOccurenceMap();
		countOccurrencesSeparator(occurrencesMap, lines);
		for (BaseSeparator sep : BaseSeparator.values()) {
			for (String line : lines) {
				// If a separator splits the line in more than 4 parts, it can't be the metadata separator
				if (AdvancedAsciiParsingUtils.countOccurrences(line, sep.getValue()) > 3) {
					occurrencesMap.remove(sep);
				}
			}
		}
		return getBestDelimiter(occurrencesMap).getValue();
	}

	private void handleData() throws IOException {
		int currentLine = 0;
		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(new FileInputStream(fileToParse), StandardCharsets.UTF_8))) {
			String line;
			while ((line = br.readLine()) != null) {
				if (currentLine < MAX_LINES_TO_ESTIMATE) {
					sortDataAndNonDataLine(line, currentLine);
				}
				currentLine++;
			}
		}
		configuration.setEndData(currentLine - 1);
		estimateDataConfiguration();
	}

	private void sortDataAndNonDataLine(String line, int currentLine) {
		if (isDataLine(line)) {
			dataIndexes.add(currentLine);
			countOccurrencesSeparator(occurencesDataSep, line);
			dataLineReference = line;
		} else {
			nonDataLines.put(line, currentLine);
		}
	}

	private boolean isDataLine(String line) {
		return AdvancedAsciiParsingUtils.isProbableDataLine(line);
	}

	private void estimateDataConfiguration() {
		configuration.setDataSeparator(getBestDelimiter(occurencesDataSep).getValue());
		trimDataIndexes();
		if (dataIndexes.isEmpty()) {
			throw new IllegalStateException("Error during configuration estimation");
		}
		configuration.setStartData(dataIndexes.get(0));
		checkOrientation();
	}

	private void checkOrientation() {
		String separator = configuration.getDataSeparator();
		int nbCols = AdvancedAsciiParsingUtils.countOccurrences(dataLineReference, separator) + 1;
		int nbRows = dataIndexes.size();
		boolean asColumn = nbRows > nbCols;
		configuration.setAsColumns(asColumn);
		int nbData = asColumn ? nbCols : nbRows;
		configuration.setNbColumns(nbData);
	}

	/**
	 * We assume that data lines must be preceded or followed by another to be valid
	 */
	private void trimDataIndexes() {
		ListIterator<Integer> it = dataIndexes.listIterator();
		Integer curElement, prevElement, nextElement;
		while (it.hasNext()) {
			curElement = it.next();
			prevElement = (it.previousIndex() != -1) ? dataIndexes.get(it.previousIndex()) : curElement;
			nextElement = (it.nextIndex() != dataIndexes.size()) ? dataIndexes.get(it.nextIndex()) : curElement;
			if (!curElement.equals(prevElement + 1) && !(curElement.equals(nextElement - 1))) {
				it.remove();
			}
		}
	}

	private void countOccurrencesSeparator(Map<BaseSeparator, Integer> occurrencesMap, String line) {
		for (Entry<BaseSeparator, Integer> entry : occurrencesMap.entrySet()) {
			int currentCount = entry.getValue();
			entry.setValue(currentCount + AdvancedAsciiParsingUtils.countOccurrences(line, entry.getKey().getValue()));
		}
	}

	private void countOccurrencesSeparator(Map<BaseSeparator, Integer> occurrencesMap, List<String> lines) {
		for (String line : lines) {
			countOccurrencesSeparator(occurrencesMap, line);
		}
	}

	private BaseSeparator getBestDelimiter(Map<BaseSeparator, Integer> occurrencesMap) {
		BaseSeparator result = BaseSeparator.TABULATION;
		int currentMax = 0;
		for (Entry<BaseSeparator, Integer> entry : occurrencesMap.entrySet()) {
			if (entry.getValue() > currentMax) {
				currentMax = entry.getValue();
				result = entry.getKey();
			}
		}
		if (result.equals(BaseSeparator.WHITESPACE)) {
			if (currentMax == occurrencesMap.get(BaseSeparator.SPACE)) {
				result = BaseSeparator.SPACE;
			}
			if (currentMax == occurrencesMap.get(BaseSeparator.TABULATION)) {
				result = BaseSeparator.TABULATION;
			}
		}
		return result;
	}

}
