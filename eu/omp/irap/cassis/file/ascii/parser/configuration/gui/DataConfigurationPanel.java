/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.ascii.parser.configuration.gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.NumberFormat;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.TitledBorder;

import eu.omp.irap.cassis.file.ascii.parser.AdvancedAsciiControl;
import eu.omp.irap.cassis.file.ascii.parser.configuration.AdvancedAsciiFileConfiguration;
import eu.omp.irap.cassis.file.ascii.parser.util.gui.ConfigurationIntervalTextField;

/**
 * @author bastienkovac
 *
 * A JPanel used to edit all information relative to the data in a {@link AdvancedAsciiFileConfiguration}
 *
 */
public class DataConfigurationPanel extends JPanel {

	/**
	 *
	 */
	private static final long serialVersionUID = 8184499312946841991L;
	private JPanel dataOrientationPanel;
	private SeparatorComboBox separatorData;
	private JFormattedTextField totalNbCols, nanDefaultValue;
	private ConfigurationIntervalTextField firstLineData, lastLineData;
	private ButtonGroup dataOrientation;
	private JRadioButton asLines, asColumns;
	private JLabel dataOrientationLabel, nbColsLabel;

	private AdvancedAsciiControl control;


	/**
	 * Creates a new DataConfigurationPanel
	 *
	 * @param control The controller
	 */
	public DataConfigurationPanel(AdvancedAsciiControl control) {
		super();
		this.control = control;
		setBorder(new TitledBorder(BorderFactory.createEtchedBorder(), "Data Values"));
		initDataOrientation();
		initLayout();
		setMaximumSize(new Dimension(getMaximumSize().width, getPreferredSize().height));
	}

	/**
	 * @return A {@link SeparatorComboBox} used to represent the separator between data in the file to parse
	 */
	public SeparatorComboBox getSeparatorData() {
		if (separatorData == null) {
			separatorData = new SeparatorComboBox();
			separatorData.setSelected(control.getModel().getConfiguration().getDataSeparatorRepresentation());
			separatorData.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					control.configureDataSeparator(separatorData.getSelectedString());
				}
			});
			separatorData.setName("Separator Data");
		}
		return separatorData;
	}

	/**
	 * @return A {@link JFormattedTextField} that only takes Integer. Represents the number of the first line
	 * where the data is present
	 */
	public ConfigurationIntervalTextField getFirstLineData() {
		if (firstLineData == null) {
			firstLineData = new ConfigurationIntervalTextField(control.getModel().getLastLineIndex());
			firstLineData.setPreferredSize(new Dimension(60, firstLineData.getPreferredSize().height));
			firstLineData.addPropertyChangeListener("value", new PropertyChangeListener() {

				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					control.configureDataStart(firstLineData.getIntValue() - 1);
				}
			});
			firstLineData.setValue(control.getModel().getConfiguration().getStartData() + 1);
			firstLineData.setName("First line Data");
		}
		return firstLineData;
	}

	/**
	 * @return A {@link JFormattedTextField} that only takes Integer. Represents the number of the last line
	 * where the data is present
	 */
	public ConfigurationIntervalTextField getLastLineData() {
		if (lastLineData == null) {
			lastLineData = new ConfigurationIntervalTextField(control.getModel().getLastLineIndex());
			lastLineData.addPropertyChangeListener("value", new PropertyChangeListener() {

				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					control.configureDataEnd(lastLineData.getIntValue() - 1);
				}
			});
			lastLineData.setValue(control.getModel().getConfiguration().getEndData() + 1);
			lastLineData.setName("Last line Data");
		}
		return lastLineData;
	}

	/**
	 * @return A {@link JFormattedTextField} that only takes Integer. Represents the number of columns (or lines)
	 * of data in the file
	 */
	public JFormattedTextField getTotalNbCols() {
		if (totalNbCols == null) {
			NumberFormat numberFormat = NumberFormat.getIntegerInstance(Locale.getDefault());
			numberFormat.setGroupingUsed(false);
			totalNbCols = new JFormattedTextField(numberFormat);
			totalNbCols.addFocusListener(new FocusAdapter() {

				@Override
				public void focusLost(FocusEvent e) {
					control.configureDataNbColumns(Integer.parseInt(totalNbCols.getText()));
				}

			});
			totalNbCols.setValue(control.getModel().getConfiguration().getNbColumns());
			totalNbCols.setName("Nb columns");
		}
		return totalNbCols;
	}

	public JFormattedTextField getNanDefaultValue() {
		if (nanDefaultValue == null) {
			NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.getDefault());
			numberFormat.setGroupingUsed(false);
			nanDefaultValue = new JFormattedTextField(numberFormat);
			nanDefaultValue.addFocusListener(new FocusAdapter() {

				@Override
				public void focusLost(FocusEvent e) {
					control.configureNanDefaultValue(Double.parseDouble(nanDefaultValue.getText()));
				}

			});
			nanDefaultValue.setValue(control.getModel().getConfiguration().getDefaultNanValue());
		}
		return nanDefaultValue;
	}

	/**
	 * @return A {@link JRadioButton} that represents if the data is stored as lines or not
	 */
	public JRadioButton getAsLines() {
		if (asLines == null) {
			asLines = new JRadioButton("Lines");
			asLines.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					control.configureDataOrientation(false);
				}
			});
		}
		return asLines;
	}

	/**
	 * @return A {@link JRadioButton} that represents if the data is stored as columns or not
	 */
	public JRadioButton getAsColumns() {
		if (asColumns == null) {
			asColumns = new JRadioButton("Columns");
			asColumns.setSelected(true);
			asColumns.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					control.configureDataOrientation(true);
				}
			});
		}
		return asColumns;
	}

	private void initDataOrientation() {
		if (dataOrientation == null) {
			dataOrientation = new ButtonGroup();
			dataOrientation.add(getAsColumns());
			dataOrientation.add(getAsLines());
			if (control.getModel().getConfiguration().isAsColumns()) {
				getAsColumns().setSelected(true);
			} else {
				getAsLines().setSelected(true);
			}
		}
	}

	/**
	 * @return A {@link JLabel} that asks the number of columns or lines, depending on the current data orientation
	 */
	public JLabel getNbColsLabel() {
		if (nbColsLabel == null) {
			nbColsLabel = new JLabel("Number of columns:");
		}
		return nbColsLabel;
	}

	private JPanel getDataOrientationPanel() {
		if (dataOrientationPanel == null) {
			dataOrientationPanel = new JPanel();
			GroupLayout layout = new GroupLayout(dataOrientationPanel);

			layout.setHorizontalGroup(
					layout.createSequentialGroup()
						.addComponent(getAsColumns())
						.addComponent(getAsLines())
					);

			layout.setVerticalGroup(
					layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(getAsLines())
						.addComponent(getAsColumns())
					);

			dataOrientationPanel.setLayout(layout);
		}
		return dataOrientationPanel;
	}

	private JLabel getDataOrientationLabel() {
		if (dataOrientationLabel == null) {
			dataOrientationLabel = new JLabel("Data orientation:");
		}
		return dataOrientationLabel;
	}

	private void initLayout() {
		JLabel first = new JLabel("First line:");
		JLabel last = new JLabel("Last line:");
		JLabel sep = new JLabel("Separator options:");
		JLabel nan = new JLabel("NaN default value:");
		GroupLayout layout = new GroupLayout(this);
		layout.setAutoCreateContainerGaps(true);
		layout.setAutoCreateGaps(true);

		layout.setHorizontalGroup(
				layout.createSequentialGroup()
					.addGroup(layout.createParallelGroup(Alignment.TRAILING)
							.addComponent(getDataOrientationLabel())
							.addComponent(getNbColsLabel())
							.addComponent(sep)
							.addComponent(first)
							.addComponent(last)
							.addComponent(nan))
					.addGroup(layout.createParallelGroup(Alignment.LEADING)
							.addComponent(getDataOrientationPanel())
							.addComponent(getTotalNbCols())
							.addComponent(getSeparatorData())
							.addComponent(getFirstLineData())
							.addComponent(getLastLineData())
							.addComponent(getNanDefaultValue()))
					);

		layout.setVerticalGroup(
				layout.createSequentialGroup()
					.addGroup(layout.createParallelGroup(Alignment.CENTER)
							.addComponent(getDataOrientationLabel())
							.addComponent(getDataOrientationPanel()))
					.addGroup(layout.createParallelGroup(Alignment.BASELINE)
							.addComponent(getNbColsLabel())
							.addComponent(getTotalNbCols()))
					.addGroup(layout.createParallelGroup(Alignment.BASELINE)
							.addComponent(sep)
							.addComponent(getSeparatorData()))
					.addGroup(layout.createParallelGroup(Alignment.BASELINE)
							.addComponent(first)
							.addComponent(getFirstLineData()))
					.addGroup(layout.createParallelGroup(Alignment.BASELINE)
							.addComponent(last)
							.addComponent(getLastLineData()))
					.addGroup(layout.createParallelGroup(Alignment.BASELINE)
							.addComponent(nan)
							.addComponent(getNanDefaultValue()))
					);

		setLayout(layout);
	}

}
