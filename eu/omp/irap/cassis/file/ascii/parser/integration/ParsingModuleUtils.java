/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.ascii.parser.integration;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JMenuBar;

import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.file.ascii.parser.AdvancedAsciiModel;
import eu.omp.irap.cassis.file.ascii.parser.AdvancedAsciiPanel;
import eu.omp.irap.cassis.file.ascii.parser.data.ParsedAsciiFile;
import eu.omp.irap.cassis.file.ascii.parser.util.gui.ParserMenuBar;
import eu.omp.irap.cassis.file.gui.cassisspectrum.CassisSpectrumControl;
import eu.omp.irap.cassis.file.gui.cassisspectrum.CassisSpectrumInfo;

/**
 * @author bastienkovac
 *
 * This class is used to easily open the AsciiParser module in different ways
 *
 */
public class ParsingModuleUtils {

	private static AdvancedAsciiPanel panelInstance;
	private static JFrame frameInstance;


	private ParsingModuleUtils() {

	}

	/**
	 * Returns the instance of the currently active AdvancedAsciiPanel.
	 * If none exists, build it using the given control
	 *
	 * @param control The SpectrumManager's control
	 * @return The AdvancedAsciiPanel instance
	 */
	public static AdvancedAsciiPanel getPanelInstance(final CassisSpectrumControl control) {
		if (panelInstance == null) {
			buildPanel(control);
		}
		return panelInstance;
	}

	/**
	 * Returns the instance of the currently active JFrame.
	 * If none exists, build it using the given control
	 *
	 * @param control The SpectrumManager's control
	 * @return The JFrame instance
	 */
	public static JFrame getFrameInstance(final CassisSpectrumControl control) {
		if (frameInstance == null) {
			frameInstance = getFrame(getPanelInstance(control), false);
			frameInstance.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosing(WindowEvent e) {
					panelInstance = null;
					frameInstance = null;
				}
			});
		}
		return frameInstance;
	}

	private static void buildPanel(final CassisSpectrumControl control) {
		final AdvancedAsciiModel model = new AdvancedAsciiModel();
		model.setLinkedControl(control);
		panelInstance = new AdvancedAsciiPanel(model, false);
		model.setExportAction(new Runnable() {

			@Override
			public void run() {
				if (!panelInstance.getControl().fullParsing()) {
					return;
				}
				AdvancedAsciiModel model = panelInstance.getControl().getModel();
				String path = model.getInputFile().getPath();
				CassisSpectrum spect = model.generateCassisSpectrum();
				ParsedAsciiFile parsed = model.getParsedFile();
				CassisSpectrumInfo info = new CassisSpectrumInfo(spect, path, parsed.getFileTitle(), parsed.getListCassisMetadata());
				control.openSpectrum(info);
			}
		});
	}

	/**
	 * Opens the module in standalone mode, meaning it has a JMenuBar and
	 * exports the spectrum as a .fus file
	 */
	public static void openStandalone() {
		final AdvancedAsciiModel model = new AdvancedAsciiModel();
		final AdvancedAsciiPanel contentPane = new AdvancedAsciiPanel(model, true);
		model.setExportAction(new Runnable() {

			@Override
			public void run() {
				contentPane.getControl().saveCassisSpectrumInFile();
			}
		});
		getFrame(contentPane, true).setVisible(true);;
	}

	private static JFrame getFrame(AdvancedAsciiPanel contentPane, boolean displayMenuBar) {
		JFrame frame = new JFrame("Advanced ASCII Reader");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		if (displayMenuBar) {
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			JMenuBar menuBar = new ParserMenuBar(contentPane.getControl());
			frame.setJMenuBar(menuBar);
		}
		frame.setContentPane(contentPane);
		frame.setLocationRelativeTo(null);
		frame.pack();
		frame.setMinimumSize(frame.getSize());
		return frame;
	}

}
