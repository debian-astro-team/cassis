/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.file.gui.ColumnsDetected;

/**
 * Used for reading .fca files
 *
 * @author glorian
 */
public class FileManagerAscii extends FileManager {

	private int nChannels;
	private double vlsr;
	private XAxisCassis xAxis = XAxisCassis.getXAxisFrequency();
	private YAxisCassis yAxis = YAxisCassis.getYAxisKelvin();
	private List<Double> frequencies, intensities;
	private int nbAllTheChannels = 0;
	private String title;
	private double loFrequency = 0;
	private String line;

	private List<CassisMetadata> cassisMetadataList;
	private List<CassisSpectrum> cassisSpectrumList;
	private int nbCassisSpectra = 0;


	/**
	 * Construct a FileManagerAscii with exceptions displayed.
	 *
	 * @param file The input File.
	 */
	public FileManagerAscii(File file) {
		this(file, true);
	}

	/**
	 * Construct a FileManagerAscii.
	 *
	 * @param file The input File.
	 * @param displayException If the exceptions should be displayed.
	 */
	public FileManagerAscii(File file, boolean displayException) {
		super(file, displayException);
		cassisSpectrumList = new ArrayList<CassisSpectrum>();
		cassisMetadataList = new ArrayList<CassisMetadata>();
	}

	// A revoir
	public CassisSpectrum read() {
		frequencies = new ArrayList<Double>();
		intensities = new ArrayList<Double>();
		nbAllTheChannels = 0;

		title = "File: " + getFile().getName();
		try {
			readData();
		} catch (IOException e) {
			if (displayException) {
				e.printStackTrace();
			}
			return null;
		}

		double[] intensity = new double[nbAllTheChannels];
		double[] frequency = new double[nbAllTheChannels];

		for (int i = 0; i < nbAllTheChannels; i++) {
			frequency[i] = xAxis.convertToMHzFreq(frequencies.get(i));
			intensity[i] = intensities.get(i);
		}

		CassisSpectrum cassisSpectrum = CassisSpectrum.generateCassisSpectrum(title, Double.POSITIVE_INFINITY, frequency, intensity, loFrequency, vlsr, xAxis, yAxis);
		cassisSpectrum.addCassisMetadata(new CassisMetadata(CassisMetadata.FILE_TYPE,
				FileManager.FileType.ASCII.name(), null, null), true);
		return cassisSpectrum;
	}

	/**
	 * Permit to return CassisSpectrum list wich contain all cassis spectrum contain in resource or file.
	 *
	 * @return cassisSpectrumList
	 */
	@Override
	public List<CassisSpectrum> readAll() {
		frequencies = new ArrayList<Double>();
		intensities = new ArrayList<Double>();
		nbAllTheChannels = 0;
		try {
			readData();
		} catch (IOException e) {
			logger.severe(e.getMessage());
		}
		return cassisSpectrumList;
	}

	private void readData() throws FileNotFoundException, IOException {
		ArrayList<Double> imageFrequencies = new ArrayList<Double>();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
			line = br.readLine();
			StringTokenizer tok;
			while (line != null) {
				if (!line.trim().equals("")) {
					boolean isGoodTitle = readTheHeader(br);
					line = br.readLine();
					if (isGoodTitle) {
						nbCassisSpectra++;
						// read Content
						for (int cpt = 0; cpt < nChannels; cpt++) {
							tok = new StringTokenizer(line);

							try {
								intensities.add(Double.valueOf(tok.nextToken()));
							} catch (NumberFormatException e) {
								// Suppose it is a NAN value
								intensities.add(Double.NaN);
							}
							frequencies.add(Double.valueOf(tok.nextToken()));
							imageFrequencies.add(Double.valueOf(tok.nextToken()));
							line = br.readLine();
						}
						nbAllTheChannels += nChannels;

						CassisSpectrum cassisSpectrum = CassisSpectrum.generateCassisSpectrum(title, convertListToArray(frequencies), convertListToArray(intensities), vlsr, xAxis, yAxis);
						cassisSpectrum.setOriginalMetadataList(cassisMetadataList);
						cassisSpectrum.addCassisMetadata(new CassisMetadata(CassisMetadata.FILE_TYPE,
								FileManager.FileType.ASCII.name(), null, null), true);
						cassisSpectrumList.add(cassisSpectrum);
					}else {
						if (line != null && line.trim().equals("")) {
							line = br.readLine();
						}
					}
				}
			}
			if (imageFrequencies.get(0) != null)
				loFrequency = frequencies.get(0) + (imageFrequencies.get(0) - frequencies.get(0)) / 2.0;
		}
	}

	private boolean readTheHeader(BufferedReader br) throws IOException {
		// read the Title
		title = line.split("\t")[0];
		// check if it is a real title
		boolean isGoodTitle = false;
		try {
			Double.parseDouble(title);
		} catch (final NumberFormatException e) {
			isGoodTitle = true;
		}
		if (isGoodTitle) {
			cassisMetadataList = new ArrayList<CassisMetadata>();
			cassisMetadataList.add(new CassisMetadata("Title", title, "", ""));
			readMetadata(br);
			readAsciiHeader(line);
		}
		return isGoodTitle;
	}

	private void readMetadata(BufferedReader br) throws IOException {
		line = br.readLine();
		while ((line != null) && (line.startsWith("//"))) {
			if (line.startsWith("// version")) {
				String version = line.split(":")[1].trim();
				cassisMetadataList.add(new CassisMetadata("Version", version, "", ""));
			} else if (line.startsWith("// xUnit")) {
				xAxis = XAxisCassis.getXAxisCassis(line.split(":")[1].trim());
				cassisMetadataList.add(new CassisMetadata("xAxis", xAxis.toString(), "", ""));
			} else if (line.startsWith("// yUnit")) {
				yAxis = YAxisCassis.getYAxisCassis(line.split(":")[1].trim());
				cassisMetadataList.add(new CassisMetadata("yAxis", yAxis.toString(), "", ""));
			} else if (line.startsWith("// ")) {
				String name = line.split("//")[1].trim();
				name = name.split(":")[0].trim();
				// String value = line.split(":")[1].trim();
				cassisMetadataList.add(new CassisMetadata(name, "", "", ""));
			}
			line = br.readLine();
		}
	}

	/**
	 * Read ascii header.
	 *
	 * @param line
	 *            line of parameters
	 */
	private void readAsciiHeader(String line) {
		StringTokenizer tok;
		logger.info("readAsciiHeader fichier");
		tok = new StringTokenizer(line);
		// nChannel, freqCentralSignal, freqCentralImage, deltaV, vlsr.
		nChannels = Integer.parseInt(tok.nextToken());
		tok.nextToken();
		tok.nextToken();
		tok.nextToken();
		vlsr = Double.valueOf(tok.nextToken());
		cassisMetadataList.add(new CassisMetadata("Channel number", Integer.toString(nChannels), "", ""));
		cassisMetadataList.add(new CassisMetadata("vlsr", Double.toString(vlsr), "", ""));
	}

	@Override
	public CassisSpectrum read(int spectrumIndex) {
		return cassisSpectrumList.get(spectrumIndex);
	}

	@Override
	public List<CassisMetadata> readCassisMetadata(int spectrumIndex) {
		return cassisSpectrumList.get(spectrumIndex).getOriginalMetadataList();
	}

	@Override
	public CassisMetadata getCassisMetadata(String name, int spectrumIndex) {
		CassisMetadata cassisMetadata = new CassisMetadata();
		if (spectrumIndex >= 0 && spectrumIndex <= nbCassisSpectra) {
			List<CassisMetadata> cassisMetadataList = cassisSpectrumList.get(spectrumIndex).getOriginalMetadataList();
			for (CassisMetadata cassisMetadataTemp : cassisMetadataList) {
				if (cassisMetadataTemp.getName().equalsIgnoreCase(name)) {
					cassisMetadata = cassisMetadataTemp;
				}
			}
		}
		return cassisMetadata;
	}

	@Override
	public int readNbCassisSpectra() {
		return nbCassisSpectra;
	}

	/**
	 * Convert Double List to a double array.
	 *
	 * @param list the list to transform in array.
	 * @return tab The double array.
	 */
	public double[] convertListToArray(List<Double> list) {
		double[] tab = new double[list.size()];
		for (int i = 0; i < list.size(); i++) {
			tab[i] = list.get(i).doubleValue();
		}
		return tab;
	}

	@Override
	public List<CassisMetadata> getCommonCassisMetadataList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ColumnsDetected> getWaveColumnsDetected() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ColumnsDetected> getFluxColumnsDetected() {
		// TODO Auto-generated method stub
		return null;
	}

}
