/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.rot;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.parameters.PointInformation;
import eu.omp.irap.cassis.parameters.RotationalFile;



/**
 * Class which reads the dataFile and calcules the coordinates of different
 * points.
 *
 * @author Rose Nerriere
 * @since 05/07
 */
public abstract class ReadFileData {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReadFileData.class);
	protected String file;

	protected boolean cancel;


	public ReadFileData() {
		super();
	}

	public RotationalFile readFile(String filePath) throws IOException {
		this.cancel = false;
		this.file = filePath;
		return null;
	}

	/**
	 * Return the correct rotational file reader for the provided file.
	 *
	 * @param path The path of the file.
	 * @return The correct rotational file reader.
	 */
	public static ReadFileData getReader(String path) {
		if (path == null || path.isEmpty() || !new File(path).exists()) {
			return null;
		}
		PointInformation.VERSION version = getFileVersion(path);
		ReadFileData reader = null;
		switch (version) {
			case OLD:
				reader = new ReadOldFile();
				break;
			case NEW:
				reader = new ReadNewFile();
				break;
			case NEW_2014:
				reader = new ReadNewNewFile();
				break;
			default:
				LOGGER.error("Unknow file version");
				break;
		}
		return reader;
	}

	/**
	 * Check the file version and return it.
	 *
	 * @param path The path of the file.
	 * @return The version of the file.
	 */
	public static PointInformation.VERSION getFileVersion(String path) {
		PointInformation.VERSION v = PointInformation.VERSION.OLD;
		try (BufferedReader br = new BufferedReader(new FileReader(path))) {
			String line = br.readLine();
			if ("version=2".equals(line.trim()) || "version=3".equals(line.trim())) {
				return PointInformation.VERSION.NEW_2014;
			} else {
				line = br.readLine();
				if (line.startsWith("#")) {
					v = PointInformation.VERSION.NEW;
				}
			}
		} catch (IOException ioe) {
			LOGGER.warn("Error while trying to get the version of the file", ioe);
			// Do nothing.
		}
		return v;
	}

	public String getFilePath() {
		return file;
	}

}
