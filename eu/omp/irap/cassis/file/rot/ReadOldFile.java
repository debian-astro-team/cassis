/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.rot;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.parameters.PointInformation;
import eu.omp.irap.cassis.parameters.RotationalComponent;
import eu.omp.irap.cassis.parameters.RotationalFile;
import eu.omp.irap.cassis.parameters.RotationalMolecule;
import eu.omp.irap.cassis.parameters.RotationalSelectionData;


/**
 * @author glorian
 *
 */
public class ReadOldFile extends ReadFileData {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReadOldFile.class);


	public ReadOldFile() {
		super();
	}

	@Override
	public RotationalFile readFile(String filePath) throws IOException {
		super.readFile(filePath);

		String line = null;
		String[] exploded = null;
		int nbLines = -1;
		List<RotationalMolecule> rotationalMolecules = new ArrayList<>();
		RotationalMolecule rotationalMolecule = null;

		try (BufferedReader bufferReader = new BufferedReader(new FileReader(file))) {
			RotationalComponent theComponent = null;
			// read the file
			String telescope = "";
			while ((line = bufferReader.readLine()) != null) {
				exploded = line.split("	");

				if (nbLines == -1) {
					// on the first line, take the name and the tag of the molecule
					String nameMol = exploded[0];
					int tag = Integer.parseInt(exploded[1]);
					rotationalMolecule = new RotationalMolecule(tag, nameMol);
					theComponent = rotationalMolecule.addComponent(tag, 1);
					theComponent.setDataType(RotationalSelectionData.GAUSSIAN_TYPE_DATA);
					nbLines = 0;
				}
				else {
					if (exploded.length == 5 && !cancel) {
						try {
							telescope = exploded[4];
							addOldPointInformation(theComponent, Double.valueOf(exploded[0]), // nu
									Double.valueOf(exploded[1]), // FWHM
									Double.valueOf(exploded[2]), // W
									Double.valueOf(exploded[3]),// Interval
									telescope);// telescope);
							nbLines++;
						}

						catch (Exception exc) {
							LOGGER.error("Error during Rotational Diagram processing", exc);
						}
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while opening file {}", filePath, e);
			JOptionPane.showMessageDialog(null, "Error while opening file.", null, JOptionPane.ERROR_MESSAGE, null);
			return null;
		}
		rotationalMolecules.add(rotationalMolecule);

		return new RotationalFile(rotationalMolecules);
	}

	public void addOldPointInformation(RotationalComponent theComponent,
			double nu, double fwhm, double w,double interval, String telescope) {
		PointInformation point = new PointInformation(nu, telescope, false);
		point.setWFit(w);
		point.setDeltaWFit(interval);
		point.setFwhmGaussianFit(fwhm);
		point.setVersion(PointInformation.VERSION.OLD);
		point.setTypeData(RotationalSelectionData.GAUSSIAN_TYPE_DATA);
		List<RotationalSelectionData> tmpList = new ArrayList<>(1);
		tmpList.add(RotationalSelectionData.GAUSSIAN_TYPE_DATA);
		point.setAllowedType(tmpList);
		theComponent.addPoint(point);
	}
}
