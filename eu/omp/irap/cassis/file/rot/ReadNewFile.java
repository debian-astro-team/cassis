/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.rot;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import eu.omp.irap.cassis.parameters.PointInformation;
import eu.omp.irap.cassis.parameters.RotationalComponent;
import eu.omp.irap.cassis.parameters.RotationalFile;
import eu.omp.irap.cassis.parameters.RotationalMolecule;
import eu.omp.irap.cassis.parameters.RotationalSelectionData;


/**
 * @author glorian
 *
 */
public class ReadNewFile extends ReadFileData {

	public ReadNewFile() {
		super();
	}

	@Override
	public RotationalFile readFile(String filePath) throws IOException {
		super.readFile(filePath);

		String line = null;
		String[] exploded = null;
		int nbLines = -1;
		List<RotationalMolecule> rotationalMolecules = new ArrayList<>();
		try (BufferedReader bufferReader = new BufferedReader(new FileReader(file))) {
			RotationalMolecule rotationalMolecule = null;
			while ((line = bufferReader.readLine()) != null) {
				if (line.trim().isEmpty())
					continue;
				exploded = line.split("\t");

				if (nbLines == -1) {
					// on the first line, take the name and the tag of the
					// molecule
					int tag = Integer.parseInt(exploded[0]);
					String nameMol = exploded[1];
					rotationalMolecule = new RotationalMolecule(tag, nameMol);
					nbLines = 0;
				} else if (nbLines == 0) {
					String val = "";
					if (exploded.length >= 14) {
						val = exploded[13];
					}

					String unit = "unknown";
					// val can be Int(unit) or Int[unit]
					if (val.contains("[")) {
						unit = val.replaceAll("^Int\\[", "").replaceAll("\\]$", "");
					} else if (val.contains("(")) {
						unit = val.replaceAll("^Int\\(", "").replaceAll("\\)$", "");
					}
					if (!"K".equals(unit)) {
						String msg = "The rotational diagram can only use file with unit in K.\n"
								+ "The detected unit is " + unit + ". Exiting.";
						JOptionPane.showMessageDialog(null, msg, "Line File Error",
								JOptionPane.ERROR_MESSAGE);
						return null;
					}
					nbLines++;
				} else if (!cancel) {
					int comp = Integer.parseInt(exploded[0]);
					RotationalComponent currentComponent = null;
					// Check if the comp is known, add to the list if not.
					if (!rotationalMolecule.contains(comp)) {
						currentComponent = rotationalMolecule.addComponent(rotationalMolecule.getTag(), comp);
					} else {
						currentComponent = rotationalMolecule.getComponent(comp);
					}

					String moleNameQuantique = exploded[1]; // transition
					double nu = Double.parseDouble(exploded[2]);// Frequency
					double aij = Double.parseDouble(exploded[4]); // Aij

					double wFirstMoment = Double.parseDouble(exploded[21]);
					double deltaWFirstMoment = Double.parseDouble(exploded[22]);
					double fwhmFirstMoment = Double.parseDouble(exploded[19]);

					double wFit = Double.parseDouble(exploded[15]);
					double deltaWFit = Double.parseDouble(exploded[16]);
					double fwhmGaussianFit = Double.parseDouble(exploded[9]);
					double fwhmLorentzianFit = Double.parseDouble(exploded[11]);

					RotationalSelectionData dataType;

					if (fwhmLorentzianFit == 0.0) {
						dataType = RotationalSelectionData.GAUSSIAN_TYPE_DATA;
					} else if (fwhmGaussianFit == 0.0) {
						dataType = RotationalSelectionData.LORENTZIAN_TYPE_DATA;
					} else {
						dataType = RotationalSelectionData.VOIGHT_TYPE_DATA;
					}

					List<RotationalSelectionData> allowedTypes = new ArrayList<>(2);
					allowedTypes.add(RotationalSelectionData.FIRST_TYPE_DATA);
					allowedTypes.add(dataType);

					// Telescope
					String telescope = exploded[26];
					currentComponent.setDataType(dataType);
					PointInformation point = new PointInformation(nu, telescope, false);
					point.setAllowedType(allowedTypes);
					point.setVersion(PointInformation.VERSION.NEW);
					point.setTypeData(dataType);
					point.setWFirstMoment(wFirstMoment);
					point.setDeltaWFirstMoment(deltaWFirstMoment);
					point.setFwhmFirstMoment(fwhmFirstMoment);

					point.setWFit(wFit);
					point.setDeltaWFit(deltaWFit);
					point.setFwhmGaussianFit(fwhmGaussianFit);
					point.setFwhmLorentzianFit(fwhmLorentzianFit);

					point.setMoleNameQuantique(moleNameQuantique);
					point.setAij(aij);

					point.setRms(Double.parseDouble(exploded[23]) / 1000); // in mk
					point.setCalibration(Double.parseDouble(exploded[25]));
					point.setDeltaV(Double.parseDouble(exploded[24]));

					currentComponent.addPoint(point);
					nbLines++;
				}
			}
			rotationalMolecules.add(rotationalMolecule);
		}
		return new RotationalFile(rotationalMolecules);
	}
}
