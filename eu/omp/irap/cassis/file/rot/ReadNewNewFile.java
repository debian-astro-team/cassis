/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.rot;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.parameters.PointInformation;
import eu.omp.irap.cassis.parameters.RotationalComponent;
import eu.omp.irap.cassis.parameters.RotationalFile;
import eu.omp.irap.cassis.parameters.RotationalMolecule;
import eu.omp.irap.cassis.parameters.RotationalSelectionData;
import eu.omp.irap.cassis.properties.Software;


public class ReadNewNewFile extends ReadFileData {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReadNewNewFile.class);
	public static final String TMB_SAVE = "TMB";
	public static final String BEAM_DILUTION_SAVE = "BEAM";

	/**
	 * @see eu.omp.irap.cassis.file.rot.ReadFileData#readFile(java.lang.String)
	 */
	@Override
	public RotationalFile readFile(String filePath) throws IOException {
		super.readFile(filePath);
		List<RotationalMolecule> rotationalMolecules = new ArrayList<>();
		boolean headerReaded = false;
		boolean unitsReaded = false;
		RotationalFile rf = new RotationalFile();

		boolean allFirstMomentZero = true;

		try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
			String line;
			br.readLine(); // Jump version.
			while ((line = br.readLine()) != null) {
				if (!headerReaded) {
					if (line.startsWith("#")) {
						if (line.startsWith('#' + TMB_SAVE)) {
							rf.setTmb(Boolean.parseBoolean(line.substring(line.lastIndexOf('\t') + 1)));
						} else if (line.startsWith('#' + BEAM_DILUTION_SAVE)) {
							String[] explodedLine = line.split("\t");
							if (explodedLine.length != 4) {
								LOGGER.warn("Malformed beam dilution header: {}", line);
							} else {
								int tag = Integer.parseInt(explodedLine[1]);
								int comp = Integer.parseInt(explodedLine[2]);
								boolean beam = Boolean.parseBoolean(explodedLine[3]);
								if (beam) {
									rf.setBeamDone(tag, comp);
								}
							}
						} else {
							LOGGER.warn("Unknow or malformed header: {}", line);
						}
						continue;
					} else {
						headerReaded = true;
					}
				}

				if (!unitsReaded) {
					if (!line.trim().startsWith("id")) {
						// TODO readUnitsHere
						unitsReaded = true;
					}
				} else {
					if (line.isEmpty()) {
						continue;
					}
					String[] explodedLine = line.split("\t");
					for (int i = 0 ; i < explodedLine.length ; i++) {
						explodedLine[i] = explodedLine[i].trim();
					}
					try {
						int tag = Integer.parseInt(explodedLine[0]);
						int numComp = Integer.parseInt(explodedLine[1]);
						String molName = explodedLine[2];
						String qn = explodedLine[3];
						double frequency = Double.parseDouble(explodedLine[4]);
						double eup = Double.parseDouble(explodedLine[5]);
						int gup = Integer.parseInt(explodedLine[6]);
						double aij = Double.parseDouble(explodedLine[7]);
						double fitFreq = Double.parseDouble(explodedLine[8]);
						double deltaFitFreq = Double.parseDouble(explodedLine[9]);
						double vo = Double.parseDouble(explodedLine[10]);
						double deltaVo = Double.parseDouble(explodedLine[11]);
						double fwhmG = Double.parseDouble(explodedLine[12]);
						double deltaFwhmG = Double.parseDouble(explodedLine[13]);
						double fwhmL = Double.parseDouble(explodedLine[14]);
						double deltaFwhmL = Double.parseDouble(explodedLine[15]);
						double intensity = Double.parseDouble(explodedLine[16]);
						double deltaIntensity = Double.parseDouble(explodedLine[17]);
						double fitFlux = Double.parseDouble(explodedLine[18]);
						double deltaFitFlux = Double.parseDouble(explodedLine[19]);
						double freqIntensityMax = Double.parseDouble(explodedLine[20]);
						double vIntensityMax = Double.parseDouble(explodedLine[21]);
						// fwhm MUST be set to FwhmFirstMoment, not fwhm !!!
						double fwhm = Double.parseDouble(explodedLine[22]);
						double intensityMax = Double.parseDouble(explodedLine[23]);
						double fluxFirstMoment = Double.parseDouble(explodedLine[24]);

						if (allFirstMomentZero && fluxFirstMoment != 0.0) {
							allFirstMomentZero = false;
						}

						double deltaFluxFirstMoment = Double.parseDouble(explodedLine[25]);
						double rms = Double.parseDouble(explodedLine[26]);
						double deltaV = Double.parseDouble(explodedLine[27]);
						double cal = Double.parseDouble(explodedLine[28]);
						double size = Double.parseDouble(explodedLine[29]);

						String telescopePath = explodedLine[30];
						if (telescopePath.equals("delivery/telescope") ||
								telescopePath.equals("delivery/telescope/")) {
							telescopePath = Software.getTelescopePath() + File.separatorChar;
						}
						String telescopeName = null;
						if (explodedLine.length >= 32) {
							telescopeName = explodedLine[31];
						}

						RotationalMolecule rotMol = getRotationalMolecule(rotationalMolecules, tag, molName);
						RotationalComponent rotComp = getRotationalComponent(rotMol, tag, numComp);
						rotComp.setDataType(getDataType(fwhmG, fwhmL));

						String telescopeFullPath = null;
						if (telescopePath != null && telescopeName != null) {
							telescopeFullPath = telescopePath + telescopeName;
						} else {
							telescopeFullPath = null;
						}
						PointInformation point = new PointInformation(frequency, telescopeFullPath, false);
						point.setVersion(PointInformation.VERSION.NEW_2014);
						point.setTypeData(rotComp.getDataType());

						List<RotationalSelectionData> allowedTypes = new ArrayList<>(2);
						allowedTypes.add(RotationalSelectionData.FIRST_TYPE_DATA);
						allowedTypes.add(rotComp.getDataType());
						point.setAllowedType(allowedTypes);

						point.setMoleNameQuantique(molName + qn);
						point.setEup(eup);
						point.setGup(gup);
						point.setAij(aij);
						point.setNuFit(fitFreq);
						point.setDeltaNuFit(deltaFitFreq);
						point.setVelocity(vo);
						point.setDeltaVelocity(deltaVo);
						point.setFwhmGaussianFit(fwhmG);
						point.setDeltaFwhmGaussianFit(deltaFwhmG);
						point.setFwhmLorentzianFit(fwhmL);
						point.setDeltaFwhmLorentzianFit(deltaFwhmL);
						point.setIntensityFit(intensity);
						point.setDeltaIntensityFit(deltaIntensity);
						point.setWFit(fitFlux);
						point.setDeltaWFit(deltaFitFlux);
						point.setNuOfIntensityMax(freqIntensityMax);
						point.setVelocityOfIntensityMax(vIntensityMax);
						point.setFwhmFirstMoment(fwhm);
						point.setIntensityMax(intensityMax);
						point.setWFirstMoment(fluxFirstMoment);
						point.setDeltaWFirstMoment(deltaFluxFirstMoment);
						point.setRms(rms / 1000);
						point.setDeltaV(deltaV);
						point.setCalibration(cal);
						point.setSizeSource(size);

						rotComp.addPoint(point);
					} catch (NumberFormatException nfe) {
						LOGGER.error("Error while reading a line. Skipping the point. The line is \"" + line + "\".");
						continue;
					}
				}
			}
		}
		rf.setAllFluxFirstMomentToZero(allFirstMomentZero);
		rf.setMoleculeList(rotationalMolecules);
		return rf;
	}

	/**
	 * Get the datatype depending on gaussian and lorentzian FWHM.
	 *
	 * @param fwhmG The gaussian FWHM.
	 * @param fwhmL The lorentzian FWHM.
	 * @return The datatype.
	 */
	private static RotationalSelectionData getDataType(double fwhmG, double fwhmL) {
		RotationalSelectionData dataType;
		if (fwhmL == 0.0) {
			dataType = RotationalSelectionData.GAUSSIAN_TYPE_DATA;
		} else if (fwhmG == 0.0) {
			dataType = RotationalSelectionData.LORENTZIAN_TYPE_DATA;
		} else {
			dataType = RotationalSelectionData.VOIGHT_TYPE_DATA;
		}
		return dataType;
	}

	/**
	 * Create if needed then return the {@link RotationalComponent} with given
	 * parameters for the given {@link RotationalMolecule}.
	 *
	 * @param rm The RotationalMolecule.
	 * @param tag The tag.
	 * @param numComp The component number.
	 * @return The {@link RotationalComponent} of the {@link RotationalComponent}
	 *  with the given parameters.
	 */
	private static RotationalComponent getRotationalComponent(
			RotationalMolecule rm, int tag, int numComp) {
		RotationalComponent rc;
		if (rm.contains(numComp)) {
			rc = rm.getComponent(numComp);
		} else {
			rc = rm.addComponent(tag, numComp);
		}
		return rc;
	}

	/**
	 * Check if a RotationalMolecule is in the given {@link List} of
	 * {@link RotationalMolecule}. If it is not in it, create it and add it to
	 * the {@link List}. In both case, return the {@link RotationalMolecule}.
	 *
	 * @param list The List in which to check.
	 * @param molTag The molecule tag.
	 * @param molName The molecule name.
	 * @return The {@link RotationalMolecule}.
	 */
	private static RotationalMolecule getRotationalMolecule(List<RotationalMolecule> list, int molTag, String molName) {
		for (RotationalMolecule rm : list) {
			if (rm.getTag() == molTag) {
				return rm;
			}
		}
		RotationalMolecule rm = new RotationalMolecule(molTag, molName);
		list.add(rm);
		return rm;
	}

}
