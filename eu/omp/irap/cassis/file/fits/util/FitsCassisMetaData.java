/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.fits.util;

import java.util.List;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.XAxisCassisUtil;
import eu.omp.irap.cassis.common.axes.XAxisVelocity;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.common.axes.YAxisGeneric;

public class FitsCassisMetaData {

	private int xIndex = 1;
	private double crVal = 0.;
	private double cdelta = 0.;
	private double bScale = 1.;
	private double bZero = 0.;
	private String xUnit = "Hz";
	private String yUnit = "K";
	private boolean foundYUnit = false;
	private boolean foundXUnit = false;
	private String title = "Unknow";
	private boolean isGildasFile = false;
	private double vlsrValue = 0.;
	private double restFrequency = 0.;
	private double crPix = 0.0;
	private double imageFreq = 0.;
	private double blanckValue = Double.POSITIVE_INFINITY;
	private int type = -1;
	private boolean objectFound = false;

	private XAxisCassis xAxisCassis = XAxisCassis.getXAxisCassis(xUnit);
	private YAxisCassis yAxisCassis = YAxisCassis.getYAxisCassis(yUnit);

	public FitsCassisMetaData(List<CassisMetadata> cassisMetadataList, boolean displayException) {
		xIndex = searchXIndex(cassisMetadataList);
		double vlsrAxis = 0.0;
		double freqRefAxis = 0.0;
		for (CassisMetadata kw : cassisMetadataList) {
			try {
				String name = kw.getName();
				if (name.equals("VELO-LSR")) {
					vlsrValue = Double.valueOf(kw.getValue()) / 1E3;

				} else if (name.equals("VELOSYS")){
					vlsrAxis = Double.valueOf(kw.getValue()) / 1E3;
				} else if(name.equals("RESTFRQ")){
					freqRefAxis = XAxisCassis.getXAxisCassis(kw.getUnit()).
							convertToMHzFreq(Double.valueOf(kw.getValue()));
				} else if (name.equals("RESTFREQ")) {
					isGildasFile = true;
					restFrequency = Double.valueOf(kw.getValue());
					xUnit = "MHz";
					foundXUnit = true;
				} else if (name.equals("ORIGIN")){
					isGildasFile = kw.getValue().trim().equals("CLASS-Grenoble");

				} else if (name.equals("BLANK")) {
					blanckValue = Double.valueOf(kw.getValue());
				} else if (name.equals("IMAGFREQ")) {
					imageFreq = Double.valueOf(kw.getValue());
				} else if (name.equals("CRPIX" + xIndex)) {
					crPix = Double.valueOf(kw.getValue());
				} else if (name.equals("OBJECT") && !kw.getValue().isEmpty()) {
					objectFound = true;
					title = kw.getValue();
				} else if (name.equals("BSCALE")) {
					bScale = Double.valueOf(kw.getValue());
				} else if (name.equals("BZERO")) {
					bZero = Double.valueOf(kw.getValue());
				} else if (name.equals("BTYPE")) {
					if (!foundYUnit) {
						yUnit = kw.getValue();
					}
				} else if (name.equals("CRVAL" + xIndex)) {
					crVal = Double.valueOf(kw.getValue());
				} else if (name.equals("CDELT" + xIndex)) {
					cdelta = Double.valueOf(kw.getValue());
				} else if (name.equals("CD" + xIndex + '_' + xIndex)) {
					cdelta = Double.valueOf(kw.getValue());
				} else if (name.equals("CUNIT" + xIndex)) {
					xUnit = kw.getValue();
					foundXUnit = true;
				} else if (name.equals("ORIGIN")) {
					if (!objectFound) {
						title = kw.getValue();
					}
				} else if (name.equals("BUNIT")) {
					if (!kw.getValue().trim().isEmpty()) {
						yUnit = kw.getValue();
						foundYUnit = true;
					}else if(!kw.getComment().trim().isEmpty()) {
							yUnit = kw.getComment().trim();
							foundYUnit = true;
					}
				} else if (name.equals("type_data")) {
					type = Integer.parseInt(kw.getValue());
				}
			} catch (Exception e) {
				if (displayException) {
					e.printStackTrace();
				}
			}
		}
		if (UNIT.toUnit(xUnit).equals(UNIT.KM_SEC_MOINS_1)){
			XAxisVelocity xAxisCassisVelo = XAxisCassis.getXAxisVelocity();
			xAxisCassisVelo.setFreqRef(freqRefAxis);
			xAxisCassisVelo.setVlsr(vlsrAxis);
			xAxisCassis = xAxisCassisVelo;
		}else {
			xAxisCassis = XAxisCassis.getXAxisCassis(xUnit);
		}

		yAxisCassis = YAxisCassis.getYAxisCassis(yUnit);
		if (yAxisCassis instanceof YAxisGeneric) {
			yAxisCassis.setInformationName("Flux");
		}
	}

	/**
	 * @return the crVal
	 */
	public final double getCrVal() {
		return crVal;
	}

	/**
	 * @return the index
	 */
	public final int getXIndex() {
		return xIndex-1;
	}

	/**
	 * @return the cdelta
	 */
	public final double getCdelta() {
		return cdelta;
	}

	/**
	 * @return the bScale
	 */
	public final double getBScale() {
		return bScale;
	}

	/**
	 * @return the bZero
	 */
	public final double getBZero() {
		return bZero;
	}

	/**
	 * @return the xUnit
	 */
	public final String getXUnit() {
		return xUnit;
	}

	/**
	 * @return the yUnit
	 */
	public final String getYUnit() {
		return yUnit;
	}

	/**
	 * @return the foundYUnit
	 */
	public final boolean isFoundYUnit() {
		return foundYUnit;
	}

	/**
	 * @return the title
	 */
	public final String getTitle() {
		return title;
	}

	/**
	 * @return the isGildasFile
	 */
	public final boolean isGildasFile() {
		return isGildasFile;
	}

	/**
	 * @return the vlsrValue
	 */
	public final double getVlsrValue() {
		return vlsrValue;
	}

	/**
	 * @return the restFrequency
	 */
	public final double getRestFrequency() {
		return restFrequency;
	}

	/**
	 * @return the crPix
	 */
	public final double getCrPix() {
		return crPix;
	}

	/**
	 * @return the imageFreq
	 */
	public final double getImageFreq() {
		return imageFreq;
	}

	/**
	 * @return the blanckValue
	 */
	public final double getBlanckValue() {
		return blanckValue;
	}

	/**
	 * @return the type
	 */
	public final int getType() {
		return type;
	}

	/**
	 * @return the objectFound
	 */
	public final boolean isObjectFound() {
		return objectFound;
	}

	/**
	 * @return the xAxisCassis
	 */
	public XAxisCassis getXAxisCassis() {
		return xAxisCassis;
	}


	/**
	 * @return the yAxisCassis
	 */
	public YAxisCassis getYAxisCassis() {
		return yAxisCassis;
	}


	/**
	 * Search the index to use for x values.
	 *
	 * @param listMeta
	 *            The list of metadata in which we search.
	 * @return the number to use, 1 of none found.
	 */
	private int searchXIndex(List<CassisMetadata> listMeta) {
		int num = 1;
		for (CassisMetadata meta : listMeta) {
			if (meta.getName().startsWith("CUNIT")) {
				String value = meta.getValue().trim();
				if (XAxisCassisUtil.isXAxisCassis(UNIT.toUnit(value))) {
					try {
						num = Integer.parseInt(meta.getName().substring("CUNIT".length()));
					} catch (NumberFormatException nfe) {
						// Ignore.
					}
				}
			}
		}
		return num;
	}

	/**
	 * compute frequencies in Mhz according to the metadata
	 * @param nbPoints 	number of points of frequency to compute
	 *
	 * @return a double array of frequencies values
	 */
	public double[] computeFrequency(int nbPoints){
		double[] frequencies = new double[nbPoints];

		int compCrpix = (int) crPix - 1;
		int decalage = 0;
		for (int i = compCrpix; i >= 0; i--) {
			frequencies[i] = xAxisCassis.convertToMHzFreq(crVal - decalage * cdelta);
			decalage++;
		}
		decalage = 1;
		for (int i = compCrpix + 1; i < nbPoints; i++) {
			frequencies[i] = xAxisCassis.convertToMHzFreq(crVal + decalage * cdelta);
			decalage++;
		}
		return frequencies;
	}

	/**
	 * compute Intensities according to the metadata and the y data
	 * @param data 	y data
	 * @return a double array of intensities
	 */
		public double[] computeIntensities(float[] data){
		double[] intensities = new double[data.length];
		for (int i = 0; i < intensities.length; i++) {
			intensities[i] = data[i] * bScale + bZero;
		}
		return intensities;
	}

	/**
	 * @return the index
	 */
	public final int getYIndex(int[] nAxis) {
		int index = 0;
		while (index < nAxis.length-1){
			index++;
		}
		return index;
	}

	/**
	 * Return if the X unit is found.
	 *
	 * @return true if the X unit is found, false otherwise.
	 */
	public boolean isFoundXUnit() {
		return foundXUnit;
	}
}
