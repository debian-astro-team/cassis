/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.fits.util;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.file.FileManagerFits;

public class CheckFitsFile {

	public static void main(String[] args) {
		String path = "/home/jglorian/CassisDatas";
		CheckFitsFile checkFitsFile = new CheckFitsFile();

		List<File> allFits = ScanFits.getAllFits(path, false);
		System.out.println("Nb fitsfile = " + allFits.size());
		for (File file : allFits) {
			System.out.println(file.getAbsolutePath());
		}

//		checkFitsFile.displayAllMetadataValue(allFits);
		checkFitsFile.displayAllMetadataValue2(allFits);
//		List<CassisMetadata> displayAllMetadata = checkFitsFile.displayAllMetadata(allFits);
//		System.out.println("Nb metadata = " + displayAllMetadata.size());
//		for (CassisMetadata cassisMetadata : displayAllMetadata) {
//			System.out.println(cassisMetadata);
//		}
//
//		List<File> hifiFiles = ScanFits.getHifiSpectrumDataSet(allFits);
//		System.out.println("Nb hifi Files =" + hifiFiles.size());
//		for (File file : hifiFiles) {
//			System.out.println(file.getAbsolutePath());
//		}

//
//		List<File> fitWithLoFreq = ScanFits.getFitsWithLoFreq("/home/jglorian/CassisDatas", false);
//		System.out.println("Nb fitsfile = " + fitWithLoFreq.size());
//		for (File file : fitWithLoFreq) {
//			System.out.println(file.getAbsolutePath());
//		}

	}

//	private List <CassisMetadata> displayAllMetadata(List<File> files) {
//		List<CassisMetadata> metas = new ArrayList<CassisMetadata>();
//		for (File file : files) {
//			FileManagerFits fileManagerFits = new FileManagerFits(file);
//			List<ArrayList<CassisMetadata>> allMeta = fileManagerFits.getAllHduList();
//			for (ArrayList<CassisMetadata> arrayList : allMeta) {
//				for (CassisMetadata cassisMetadata : arrayList) {
//					metas.add(cassisMetadata);
//				}
//			}
//		}
//		return metas;
//	}
//
//
//	private void displayAllMetadataValue(List<File> files) {
//
//		HashMap<String, ArrayList<String>> list = new HashMap<String, ArrayList<String>>();
//		for (File file : files) {
//			FileManagerFits fileManagerFits = new FileManagerFits(file);
//			List<ArrayList<CassisMetadata>> allMeta = fileManagerFits.getAllHduList();
//			for (ArrayList<CassisMetadata> arrayList : allMeta) {
//				for (CassisMetadata cassisMetadata : arrayList) {
//					if (!list.containsKey(cassisMetadata.getName())){
//						ArrayList<String> values = new ArrayList<String>();
//						list.put(cassisMetadata.getName(), values);
//					}
//					list.get(cassisMetadata.getName()).add(cassisMetadata.getValue());
//				}
//			}
//		}
//
//		for (String key : list.keySet()) {
//			System.out.println(key + "\t"+ list.get(key).size()+"\t"+list.get(key));
//		}
//
//	}

	private void displayAllMetadataValue2(List<File> files) {

		HashMap<String, ArrayList<String>> listValues = new HashMap<String, ArrayList<String>>();
		HashMap<String, HashSet<String>> listFiles = new HashMap<String, HashSet<String>>();
		int i = 0;
		for (File file : files) {
			FileManagerFits fileManagerFits = new FileManagerFits(file);
			List<CassisSpectrum> spectra = fileManagerFits.readAll();
			for (CassisSpectrum cassisSpectrum : spectra) {
				i++;
				List<CassisMetadata> arrayList = cassisSpectrum.getOriginalMetadataList();
				for (CassisMetadata cassisMetadata : arrayList) {
					if (!listValues.containsKey(cassisMetadata.getName())){
						ArrayList<String> values = new ArrayList<String>();
						listValues.put(cassisMetadata.getName(), values);
						HashSet<String> valuesFile = new HashSet<String>();
						listFiles.put(cassisMetadata.getName(), valuesFile);
					}
					listValues.get(cassisMetadata.getName()).add(cassisMetadata.getValue());
					listFiles.get(cassisMetadata.getName()).add(file.getName());
				}
			}

		}
		System.out.println("nb spectra = "+ i);

		for (String key : listValues.keySet()) {
			HashSet<String> hashSet = new HashSet<String>(listValues.get(key));
			System.out.println(key + "\t"+ listValues.get(key).size()+"\t"+hashSet +"\t"+listFiles.get(key) );
		}

		HashSet<String> classification = new HashSet<String>();
		for (String key : listValues.keySet()) {
			if (!(key.equals("") || key.equals("HISTORY"))){
				classification.add(listFiles.get(key).toString());
			}
		}
		ArrayList<HashSet<String>> setFiles = new ArrayList<HashSet<String>>();
		for (String classFile : classification) {
			System.out.println(classFile);
			String[] split = classFile.split(", ");
			split[0] = split[0].substring(1);
			split[split.length-1] = split[split.length-1].replaceAll("]", "");
			HashSet<String> temp = new HashSet<String>();
			for (String string : split) {
				temp.add(string);
			}
			setFiles.add(temp);
		}



		System.out.println("********** **************");



		for (HashSet<String> hashSet : setFiles) {
			@SuppressWarnings("unchecked")
			HashSet<String> hashSetCopy = (HashSet<String>) hashSet.clone();
			for (HashSet<String> hashSet2 : setFiles) {
				if (!hashSet.equals(hashSet2)){
					hashSetCopy.removeAll(hashSet2);
				}
			}
			System.out.println(hashSetCopy);
		}

	}


}
