/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.fits.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import eu.omp.irap.cassis.common.CassisMetadata;

public class FileFitsUtil {
	/**
	 * Return the value of DS keyword according to his index contain in a
	 * metadata list
	 *
	 * @param index
	 *            The index DS keyword value
	 * @param cassisMetadataList
	 *            The metadata list
	 * @return the value of DS keyword according to his index contain in a
	 *         metadata list
	 */
	private static String getDsValueKeyword(int index, List<CassisMetadata> cassisMetadataList) {
		String ds = null;
		for (CassisMetadata cassisMetadata : cassisMetadataList) {
			if (cassisMetadata.getName().equals(CassisMetadata.DS + "_" + index)) {
				ds = cassisMetadata.getValue();
			}
		}
		return ds;
	}

	/**
	 * Return the DSETS keyword value contain in metadata list
	 *
	 * @param cassisMetadataList
	 *            The metadata list
	 * @return the DSETS keyword value
	 */
	private static String getDsetsValueKeyword(List<CassisMetadata> cassisMetadataList) {
		String dsets = null;
		for (CassisMetadata cassisMetadata : cassisMetadataList) {
			if (cassisMetadata.getName().contains(CassisMetadata.DSETS + "_")) {
				dsets = cassisMetadata.getValue();
			}
		}
		return dsets;
	}

//	if (i == 0) {
//		CassisMetadata metadata = cassisSpectrum.getOriginalMetadata("EXTEND");
//		if (metadata != null && Boolean.valueOf(metadata.getValue())) {
//			extend = true;
//			metadataListExtend = metaDatas;
//			int indice = 2;
//			do {
//				CassisMetadata metadataExtend = cassisSpectrum.getOriginalMetadata("EXTEN" + indice);
//				if (metadataExtend != null) {
//					indiceExtend.add(indice);
//				}
//				indice++;
//			} while (indice <= getNbHdu());
//		}
//	}

	private static Tree<Integer> obtainChildRecursive(Tree<Integer> tree, int index,
			List<ArrayList<CassisMetadata>> allHduList) {
		List<Integer> leaf = new ArrayList<Integer>();
		if (index == 0 && getDsetsValueKeyword(allHduList.get(0)) == null && CassisMetadata
				.getCassisMetadata("NAXIS", allHduList.get(0)).getValue().equals("0")) {
			int nbHdu = allHduList.size();
			for (int i = 1; i < nbHdu; i++) {
				leaf.add(i);
			}
		}else if (index == 0 && getDsetsValueKeyword(allHduList.get(0)) == null &&
				 CassisMetadata.isIn(allHduList.get(0), "EXTEND", "true")){
			int indice = 2;
			do {
				CassisMetadata metadataExtend = CassisMetadata.getCassisMetadata("EXTEN" + indice, allHduList.get(0));
				if (metadataExtend != null) {
					leaf.add(indice-1);
				}
				indice++;
			} while (indice <= allHduList.size());



		}else {
			if (getDsetsValueKeyword(allHduList.get(index)) != null) {
				int nbChild = Integer.parseInt(getDsetsValueKeyword(allHduList.get(index)));
				for (int j = 0; j < nbChild; j++) {
					int i = Integer.parseInt(getDsValueKeyword(j, allHduList.get(index)));
					if (i < allHduList.size()) {
						leaf.add(i);
					}
				}
			}
		}
		for (Integer i : leaf) {
			if (tree.getTree(i) == null) {
				tree.getTree(index).addLeaf(i);
				obtainChildRecursive(tree, i, allHduList);
			}
		}
		return tree;
	}

	/**
	 * Return a tree representing the final architecture of Fits file
	 *
	 * @return tree representing the final architecture of Fits file
	 */
	public static Tree<Integer> getFinalTree(List<Integer> indexOfHduToIgnore,
			List<ArrayList<CassisMetadata>> allHduList) {

		Tree<Integer> treeOfAllHdu = new Tree<Integer>(-1);
		for (int i = 0; i < allHduList.size(); i++) {
			if (treeOfAllHdu.getTree(Integer.valueOf(i)) == null) {
				treeOfAllHdu.addLeaf(i);
				obtainChildRecursive(treeOfAllHdu, i, allHduList);
			}
		}

		for (int i = 0; i < indexOfHduToIgnore.size(); i++) {
			if (treeOfAllHdu.getTree(indexOfHduToIgnore.get(i)) != null) {
				treeOfAllHdu.deleteElement(indexOfHduToIgnore.get(i));
			}
		}
		return treeOfAllHdu;
	}

	/**
	 * Return a List<ArrayList<CassisMetadata>> with metadatas merge according
	 * to file architecture
	 *
	 * @return hduListWithMetadatasMerge the HDU list with metadatas merge
	 *         according to file architecture
	 */
	public static List<ArrayList<CassisMetadata>> getHduListWithMetadataMerge(
			Tree<Integer> finalTree, List<ArrayList<CassisMetadata>> allHduList) {
		ArrayList<ArrayList<CassisMetadata>> res = new ArrayList<ArrayList<CassisMetadata>>();
		List<ArrayList<CassisMetadata>> allHduListWithHierarchChanged = getHduListWithHierarchTraitment(
				finalTree, allHduList);

		for (int i = 0; i < allHduListWithHierarchChanged.size(); i++) {
			if (finalTree.getTree(i) != null) {
				String branch = finalTree.getBranchOf(i, -1);
				String[] allIndice = branch.split(":");
				String[] indice = new String[allIndice.length - 2];
				int l = 0;
				for (int j = 1; j < allIndice.length - 1; j++) {
					indice[l] = allIndice[j];
					l++;
				}
				if (indice.length > 1) {
					for (int j = 0; j < indice.length; j++) {
						ArrayList<CassisMetadata> cassisMetadatas = allHduListWithHierarchChanged
								.get(Integer.parseInt(indice[j]));
						if (j == 0) {
							res.add(cassisMetadatas);
						} else {
							for (CassisMetadata cassisMetadata : cassisMetadatas) {
								if (!CassisMetadata.isMetadataName(cassisMetadata.getName(),
										res.get(i))
										&& !cassisMetadata.getName().contains("DS_")
										&& !cassisMetadata.getName().contains("DSETS")) {
									CassisMetadata cassisMetadataClone;
									try {
										cassisMetadataClone = cassisMetadata.clone();
										res.get(i).add(cassisMetadataClone);
									} catch (CloneNotSupportedException e) {
										e.printStackTrace();
									}
								}
							}
						}
					}
				} else {
					res.add(allHduListWithHierarchChanged.get(i));
				}
			} else {
				res.add(allHduListWithHierarchChanged.get(i));
			}
		}

		return res;
	}

	/**
	 * Return a List<ArrayList<CassisMetadata>> with hierarch traitment realized
	 *
	 * @return hduListWithHierarchTraitment the HDU list with hierarch traitment
	 *         realized
	 */
	private static List<ArrayList<CassisMetadata>> getHduListWithHierarchTraitment(
			Tree<Integer> indiceSpectra, List<ArrayList<CassisMetadata>> metadatasOfHdu) {
		ArrayList<ArrayList<CassisMetadata>> metadatasHierarchTraitment = new ArrayList<ArrayList<CassisMetadata>>();
		for (int i = 0; i < metadatasOfHdu.size(); i++) {
			if (indiceSpectra.getTree(i) != null) {
				List<CassisMetadata> metadatasHierarch = changeHierarch(metadatasOfHdu.get(i));
				metadatasHierarchTraitment.add(i, new ArrayList<CassisMetadata>());
				for (CassisMetadata cassisMetadata : metadatasHierarch) {
					metadatasHierarchTraitment.get(i).add(cassisMetadata);
				}
			} else {
				metadatasHierarchTraitment.add(metadatasOfHdu.get(i));
			}
		}
		return metadatasHierarchTraitment;
	}

	/**
	 * Return metadatas with the hierarch change according to the convention
	 *
	 * @param cassisMetadatas
	 *            Metadatas in which one we want to change the hierarch keyword
	 * @return Metadatas with the hierarch change according to the convention
	 */
	public static List<CassisMetadata> changeHierarch(List<CassisMetadata> cassisMetadatas) {
		List<CassisMetadata> res = cassisMetadatas;
		HashMap<String, String> hierarchMapping = obtainHierarchMapping(cassisMetadatas);

		if (!hierarchMapping.isEmpty()) {
			for (int i = 0; i < cassisMetadatas.size(); i++) {
				CassisMetadata cassisMetadata = cassisMetadatas.get(i);
				for (Entry<String, String> entry : hierarchMapping.entrySet()) {
					if (cassisMetadata.getName().equalsIgnoreCase(entry.getKey())) {
						final CassisMetadata metadata = new CassisMetadata(entry.getValue(),
								cassisMetadata.getValue(), cassisMetadata.getComment(),
								cassisMetadata.getUnit());
						cassisMetadatas.set(i, metadata);
					}
				}
			}
			res = new ArrayList<CassisMetadata>();
			for (int j = 0; j < cassisMetadatas.size(); j++) {
				CassisMetadata cassisMetadata = cassisMetadatas.get(j);
				Pattern pattern = Pattern.compile("^key.*");
				if (!(pattern.matcher(cassisMetadata.getName().toLowerCase()).find())) {
					res.add(cassisMetadata);
				}
			}
		}
		return res;
	}

	/**
	 * Return a HashMap with key corresponding to metadata name and value
	 * corresponding to the real value of metadata name
	 *
	 * @param cassisMetadatas
	 *            The metadatas list
	 * @return Key and value relative to the hierarch keyword
	 */
	private static HashMap<String, String> obtainHierarchMapping(
			List<CassisMetadata> cassisMetadatas) {
		HashMap<String, String> hierarchMap = new HashMap<String, String>();
		for (CassisMetadata cassisMetadata : cassisMetadatas) {
			Pattern pattern = Pattern.compile("^key.*");
			if (pattern.matcher(cassisMetadata.getName().toLowerCase()).find()) {
				int debut = cassisMetadata.getName().indexOf(".");
				String key = cassisMetadata.getName().substring((debut + 1));
				hierarchMap.put(key, cassisMetadata.getValue());
			}
		}
		return hierarchMap;
	}

}
