/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.fits.util;

import java.util.ArrayList;
import java.util.HashMap;

public class Tree<T> {

	public static final int INDENT = 5;
	private T head;
	private ArrayList<Tree<T>> leafs = new ArrayList<Tree<T>>();
	private Tree<T> parent;
	private HashMap<T, Tree<T>> locate = new HashMap<T, Tree<T>>();


	public Tree(T head) {
		this.head = head;
		locate.put(head, this);
	}

	public Tree<T> addLeaf(T leaf) {
		Tree<T> t = new Tree<T>(leaf);
		leafs.add(t);
		t.parent = this;
		t.locate = this.locate;
		locate.put(leaf, t);
		return t;
	}

	public T getHead() {
		return head;
	}

	public Tree<T> getTree(T element) {
		return locate.get(element);
	}

	public Tree<T> getParent() {
		return parent;
	}

	public Tree<T> deleteElement(T element) {
		if (locate.containsKey(element)) {
			leafs.remove(locate.get(element));
			locate.remove(element);
		}
		return parent;
	}

	private String printTree(int increment) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < increment; ++i) {
			sb.append(' ');
		}
		sb.append(':');
		sb.append(head);
		for (Tree<T> child : leafs) {
			sb.append('\n');
			sb.append(child.printTree(increment + INDENT));
		}
		return sb.toString();
	}

	public String getBranchOf(T val, T root) {
		Tree<T> tree = getTree(val);
		StringBuilder res = new StringBuilder();
		while (tree.getHead() != root) {
			res.append(':');
			res.append(tree.getHead());
			tree = tree.getParent();
		}
		res.append(':');
		res.append(tree.getHead());
		res.append(':');
		return res.toString();
	}

	public int getNbChildHead() {
		return leafs.size();
	}

	public T getValueOfChildHead(int i) {
		return leafs.get(i).getHead();
	}

	@Override
	public String toString() {
		return printTree(0);
	}
}
