/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.fits.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eso.fits.cassis.FitsException;
import org.eso.fits.cassis.FitsFile;
import org.eso.fits.cassis.FitsHDUnit;
import org.eso.fits.cassis.FitsHeader;
import org.eso.fits.cassis.FitsKeyword;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.file.FileManagerFits;
import uk.ac.starlink.table.StarTableFactory;

public class ScanFits {

	public static final String STARTABLE = "Startable";
	public static final String JFITS = "JFits";
	public static final String FILE = "File";
	public static final String HDU_NUMBER = "HDU number";
	public static final String SPECTRUM_NUMBER = "Spectrum number";
	public static final String POINT_NUMBER = "Point Number";
	public static final String XUNIT = "Xunit";
	public static final String XMINMAX = "XminMax";
	public static final String YUNIT = "Yunit";
	public static final String YMINMAX = "YminMax";
	public static final String NAXIS = "NAXIS";
	private static boolean withStartTable;
	private static boolean firstSpectrumWithJFits;
	private static boolean allWithJFits;
	private static String pathDirectoryCsv;
	private static boolean selectedHeader;
	private static boolean displayTreeFits;
	private static boolean generateFitsScanReport;

	private static final Logger logger = Logger.getLogger(ScanFits.class.getName());


//	private static String [] metas1 = new String[]{"SIMPLE","EXTEND","XTENSION","EXTNAME", "INSTRUME",
//			"TELESCOP","DATE", "HDUCLASS", "HDUVERS", "PIXTYPE", "LONGSTRN"};
//	private static String [] metas2 = new String[]{"XTENSION", "TBCOL1"};
//
//	private static String [] metaLo = new String[]{"loFreqAvg", "LoFrequency",
//			"loFrequency", "lofrequency", "LOFREQ"};
//	private static String [] metaOrigin = new String[]{"ORIGIN"};
//
//
//	private static String [] metaLoOrigin = new String[]{"loFreqAvg", "LoFrequency",
//			"loFrequency", "lofrequency", "LOFREQ", "ORIGIN"};
//
//	private static String [] metaVOPUB = new String[]{"VOPUB"};
//
//	private static String [] metaTELESCOP = new String[]{"TELESCOP"};
//	private static String [] metaCLASS = new String[]{"CLASS___"};
//
//	private static String [] metaEXTNAME = new String[]{"EXTNAME", "HDU_NAME"};

	private static String [] metaFITSTYPE = new String[]{"FITS_TYPE"};


	/**
	 * Two arguments<br>
	 * args[0] : The path directory where we want to create the csv file<br>
	 * args[1] : The directory or file path to scan file(s)
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length < 2) {
			System.out.println("Two arguments");
			System.out.println("\t The directory or file path to scan file(s)");
			System.out.println("\t The path directory where data will be generated");

		} else {
			String path = args[0];
			pathDirectoryCsv = args[1];

			System.out.println("Begin scan Fits file");
			List<File> allFits = getAllFits(path, false);
			scanArgOptions(args);
			if (withStartTable){
				scanWithJFits(allFits);
			}
			if (firstSpectrumWithJFits){
				scanFirstSpectrumWithJFits(allFits);
			}
			if (allWithJFits){
				scanAllSpectrumWithJFits(allFits);
			}

			if (selectedHeader){
				scanSelectedHeader(allFits, metaFITSTYPE, true);
			}

			if (displayTreeFits){
				displayTreeFits(allFits);
			}
			if (generateFitsScanReport){
				generateFitsScanReport(args);
			}
			System.out.println("End scan Fits file");
		}
	}





	private static void scanArgOptions(String[] args) {
		for (String val : args) {
			if (val.equalsIgnoreCase("withStartTable")){
				withStartTable = true;
			}else  if (val.equalsIgnoreCase("allWithJFits")){
				allWithJFits = true;
			}else if (val.equalsIgnoreCase("firstSpectrumWithJFits")){
				firstSpectrumWithJFits = true;
			}else if (val.equalsIgnoreCase("selectedHeader")){
				selectedHeader = true;
			}else if (val.equalsIgnoreCase("displayTreeFits")){
				displayTreeFits = true;
			}else if (val.equalsIgnoreCase("generateFitsScanReport")){
				generateFitsScanReport = true;
			}

		}
	}

	private static void scanSelectedHeader(List<File> allFits, String[] metas, boolean justContains){

		HashMap<String, ArrayList<String>> values = new HashMap<String, ArrayList<String>>();
		for (String val : metas) {
			values.put(val, new ArrayList<String>());
		}

		for (int i = 0; i < allFits.size(); i++) {
			FileManagerFits managerFits = new FileManagerFits(allFits.get(i));
			List<CassisSpectrum> spectrums;
			try {

				spectrums = managerFits.readAll();

				for (int index = 0; index < spectrums.size(); index++) {

					for (String meta : metas) {
						final CassisSpectrum spectrum = spectrums.get(index);
						final List<CassisMetadata> metaList = spectrum.getOriginalMetadataList();
						metaList.addAll(spectrum.getCassisMetadataList());
						CassisMetadata cassisMeta = null;
						if (! justContains){
							cassisMeta = CassisMetadata.getCassisMetadataCase(meta, metaList);
						} else {
							cassisMeta = CassisMetadata.containsCassisMetadataCase(meta, metaList);

						}
						if (cassisMeta != null){
							System.out.printf("\t"+ allFits.get(i));
							System.out.printf("\t\t:%s=%s /%s\n",
									cassisMeta.getName(), cassisMeta.getValue(), cassisMeta.getComment());
							values.get(meta).add(cassisMeta.getValue());
						}
					}
				}
			} catch (Exception e) {
				System.out.println("error for "+allFits.get(i));
//				e.printStackTrace();
			}
		}
		System.out.println("Values = ");
		for (String val : metas) {
			final HashSet<String> hashSet = new HashSet<String>(values.get(val));
			System.out.println(val + "=" + hashSet);
		}

	}

	/**
	 * @param allFits
	 *
	 */
	public static void scanAllSpectrumWithJFits(List<File> allFits) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < allFits.size(); i++) {
			HashMap<String, String> map = readAllWithJFits(allFits.get(i));
			builder.append(createReport(map));
			System.out.println((((double) i * 100) / allFits.size()) + "%");
		}
		saveReport(pathDirectoryCsv + "/scanAllSpectrumWithJFits.csv", builder.toString());
	}

	/**
	 * @param allFits
	 *
	 */
	public static void scanFirstSpectrumWithJFits(List<File> allFits) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < allFits.size(); i++) {
			HashMap<String, String> map = readFirstSpectrumWithJFits(allFits.get(i));
			builder.append(createReport(map));
			System.out.println((((double) i * 100) / allFits.size()) + "%");
		}
		saveReport(pathDirectoryCsv + "/scanFirstSpectrumWithJFits.csv", builder.toString());
	}

	/**
	 * @param allFits
	 *
	 */
	public static void scanWithJFits(List<File> allFits) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < allFits.size(); i++) {

			HashMap<String, String> scanMap = new HashMap<String, String>();
			try {
				File file = allFits.get(i);
				FileManagerFits fileManagerFits = new FileManagerFits(file);
				List<CassisSpectrum> cassisSpectra = fileManagerFits.readAll();
				CassisSpectrum mergeCassisSpectrum = CassisSpectrum.mergeCassisSpectrumList(cassisSpectra);

				scanMap = extractData(file, mergeCassisSpectrum);
			} catch (Exception e) {
			}
			builder.append(createReport(scanMap));
			System.out.println((double) ((i+1) * 100 / allFits.size()) + "%");
		}
		saveReport(pathDirectoryCsv + "/scanWithJFits.csv", builder.toString());
	}

	/**
	 * @param pathDirectoryCsv
	 * @param builder
	 */
	private static void saveReport(String generatedFile, String report) {
		try (FileWriter fileWriter = new FileWriter(generatedFile)) {
			fileWriter.write("Fichiers, Provenance, Startable, Remarque Startable, JFits, "
					+ "Remarque JFits, Nombre d'HDU, Nombre de spectres, xUnit, \"[ MinX, MaxX ]\","
					+ " yUnit, \"[ MinY, MaxY ]\", Nombre de points, NAXIS\n");
			fileWriter.write(report);

		} catch (IOException e) {
			e.getMessage();
		}
		System.out.println(generatedFile + " généré");
		System.out.println("*******************************************\n");
	}

	/**
	 * @param map
	 * @return
	 */
	private static String createReport(HashMap<String, String> map) {
		String line = getFromMap(map,FILE) + ", ," + getFromMap(map,STARTABLE) + ", ,"
				+ getFromMap(map,JFITS) + ", ," + getFromMap(map,HDU_NUMBER) + ", "
				+ getFromMap(map,SPECTRUM_NUMBER) + ", \"" + getFromMap(map,XUNIT) + "\", \""
				+ getFromMap(map,XMINMAX) + "\", \"" + getFromMap(map,YUNIT) + "\", \""
				+ getFromMap(map,YMINMAX) + "\", \"" + getFromMap(map,POINT_NUMBER) + "\", \""
				+ getFromMap(map,NAXIS) + "\"\n";
		return line;
	}

	private static String getFromMap(HashMap<String, String> map, String key){
		String res = "";
		if (map.containsKey(key)){
			res = map.get(key);
		}
		if (res.length()>20){
			res = res.substring(0, 20);
		}
		return res;
	}

	/**
	 * Return the list of fits file of the path directory
	 *
	 * @param path the
	 * @return an  ArrayList of string representing the fits files of the path
	 */
	public static ArrayList<String> getFiles(String path) {
		File ressource = new File(path);
		ArrayList<String> fitsFile = new ArrayList<String>();
		if (ressource.isDirectory()) {
			String[] fileTab = ressource.list();

			for (int i = 0; i < fileTab.length; i++) {
				if (fileTab[i].endsWith(".fits") || fileTab[i].endsWith(".fts")) {
					fitsFile.add(fileTab[i]);
				}
			}
		} else {
			fitsFile.add(ressource.getAbsolutePath());
		}
		return fitsFile;
	}

	/**
	 * Realize a complete fits file scan using the method
	 * {@link FileManagerFits#readAll()}.
	 *
	 * @param file
	 *            The File to scan
	 * @return HashMap containing spectrum information relative to file
	 */
	private static HashMap<String, String> readAllWithJFits(File file) {
		FileManagerFits fileManagerFits = new FileManagerFits(file);
		HashMap<String, String> scanMap = new HashMap<String, String>();

		scanMap.put(FILE, file.getName());

		/***************** TEST STARTABLE ***********************/
		try {
			new StarTableFactory().makeStarTable(file.getAbsolutePath());
			scanMap.put(STARTABLE, "OK");
		} catch (IOException e) {
			scanMap.put(STARTABLE, "NOK");
		}

		FitsFile ff = null;
		/***************** TEST FITS *****************************/
		try {
			ff = new FitsFile(file);
			int nbSpectra = fileManagerFits.readNbCassisSpectra();
			scanMap.put(JFITS, "OK");
			scanMap.put(HDU_NUMBER, String.valueOf(fileManagerFits.getNbHdu()));
			scanMap.put(SPECTRUM_NUMBER, Integer.toString(nbSpectra));
			try {
				if (nbSpectra > 0) {
					String nAxis = "";
					String xUnit = "";
					String xMinMax = "";
					String yUnit = "";
					String yMinMax = "";
					String pointNumber = "";
					List<CassisSpectrum> cassisSpectrums = fileManagerFits.readAll();
					for (int i = 0; i < nbSpectra; i++) {
						CassisSpectrum cassisSpectrum = cassisSpectrums.get(i);
						List<CassisMetadata> cassisMetadatasList = cassisSpectrum
								.getOriginalMetadataList();
						for (CassisMetadata cassisMetadata : cassisMetadatasList) {
							if (cassisMetadata.getName().toLowerCase().equals("naxis")) {
								nAxis = nAxis + "[" + cassisMetadata.getValue() + "], ";
							}
						}
						xUnit = xUnit + "[" + cassisSpectrum.getxAxisOrigin() + "], ";
						xMinMax = xMinMax + "[" + cassisSpectrum.getFreqMin() + " , "
								+ cassisSpectrum.getFreqMax() + "], ";
						yUnit = yUnit + "[" + cassisSpectrum.getYAxis() + "], ";
						yMinMax = yMinMax + "[" + cassisSpectrum.getIntensityMin() + " , "
								+ cassisSpectrum.getIntensityMax() + "], ";
						pointNumber = pointNumber + "[" + cassisSpectrum.getNbChannel() + "], ";
					}
					scanMap.put(XUNIT, xUnit);
					scanMap.put(XMINMAX, xMinMax);
					scanMap.put(YUNIT, yUnit);
					scanMap.put(YMINMAX, yMinMax);
					scanMap.put(POINT_NUMBER, pointNumber);
					scanMap.put(NAXIS, nAxis);
				}
			} catch (Exception e) {
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (FitsException e1) {
			scanMap.put(JFITS, "NOK");
			e1.printStackTrace();
		} finally {
			if (ff != null) {
				ff.closeFile();
			}
		}
		return scanMap;
	}

	/**
	 * Realize a fits file scan using the method
	 * {@link FileManagerFits#readAll()}.
	 *
	 * @param file
	 *            The File to scan
	 * @return HashMap containing first spectrum information relative to file
	 */
	private static HashMap<String, String> readFirstSpectrumWithJFits(File file) {
		FileManagerFits fileManagerFits = new FileManagerFits(file);
		HashMap<String, String> scanMap = new HashMap<String, String>();

		scanMap.put(FILE, file.getName());

		/***************** TEST STARTABLE ***********************/
		try {
			new StarTableFactory().makeStarTable(file.getAbsolutePath());
			scanMap.put(STARTABLE, "OK");
		} catch (IOException e) {
			scanMap.put(STARTABLE, "NOK");
		}

		FitsFile ff = null;
		/***************** TEST FITS *****************************/
		try {
			ff = new FitsFile(file);
			int nbSpectra = fileManagerFits.readNbCassisSpectra();
			scanMap.put(JFITS, "OK");
			scanMap.put(HDU_NUMBER, Integer.toString(fileManagerFits.getNbHdu()));
			scanMap.put(SPECTRUM_NUMBER, Integer.toString(nbSpectra));
			try {
				if (nbSpectra > 0) {
					String nAxis = "";
					String xUnit = "";
					String xMinMax = "";
					String yUnit = "";
					String yMinMax = "";
					String pointNumber = "";
					List<CassisSpectrum> cassisSpectrumList = fileManagerFits.readAll();
					CassisSpectrum cassisSpectrum = cassisSpectrumList.get(0);
					List<CassisMetadata> cassisMetadatasList = cassisSpectrum
							.getOriginalMetadataList();
					for (CassisMetadata cassisMetadata : cassisMetadatasList) {
						if (cassisMetadata.getName().toLowerCase().equals("naxis")) {
							nAxis = "[" + cassisMetadata.getValue() + "]";
						}
					}
					xUnit = "[" + cassisSpectrum.getxAxisOrigin() + "]";
					xMinMax = "[" + cassisSpectrum.getFreqMin() + " , "
							+ cassisSpectrum.getFreqMax() + "]";
					yUnit = "[" + cassisSpectrum.getYAxis() + "]";
					yMinMax = "[" + cassisSpectrum.getIntensityMin() + " , "
							+ cassisSpectrum.getIntensityMax() + "]";
					pointNumber = "[" + cassisSpectrum.getNbChannel() + "]";
					scanMap.put(XUNIT, xUnit);
					scanMap.put(XMINMAX, xMinMax);
					scanMap.put(YUNIT, yUnit);
					scanMap.put(YMINMAX, yMinMax);
					scanMap.put(POINT_NUMBER, pointNumber);
					scanMap.put(NAXIS, nAxis);
				}
			} catch (Exception e) {
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (FitsException e1) {
			scanMap.put(JFITS, "NOK");
			e1.printStackTrace();
		} finally {
			if (ff != null) {
				ff.closeFile();
			}
		}
		return scanMap;
	}

	private static HashMap<String, String> extractData(File file, CassisSpectrum cassisSpectrum) {
		HashMap<String, String> scanMap = new HashMap<String, String>();
		scanMap.put(FILE, file.getName());
		String xUnit = "[" + cassisSpectrum.getxAxisOrigin() + "]";
		String xMinMax = "[" + cassisSpectrum.getFreqMin() + " , " + cassisSpectrum.getFreqMax() + "]";
		String yUnit = "[" + cassisSpectrum.getYAxis() + "]";
		String yMinMax = "[" + cassisSpectrum.getIntensityMin() + " , "
				+ cassisSpectrum.getIntensityMax() + "]";
		String pointNumber = "[" + cassisSpectrum.getNbChannel() + "]";
		scanMap.put(XUNIT, xUnit);
		scanMap.put(XMINMAX, xMinMax);
		scanMap.put(YUNIT, yUnit);
		scanMap.put(YMINMAX, yMinMax);
		scanMap.put(POINT_NUMBER, pointNumber);
		return scanMap;
	}

	/**
	 * Display the spectrum information according to a HashMap List.
	 *
	 * @param scanMapList
	 *            The HashMap List containing the spectrum information
	 */
	public static void printScan(List<HashMap<String, String>> scanMapList) {
		for (HashMap<String, String> map : scanMapList) {
			for (Entry<String, String> entry : map.entrySet()) {
				System.out.println(entry.getKey() + " : " + entry.getValue());
			}
			System.out.println("************************************");
		}
	}

	/**
	 * Display the spectrum information according to a HashMap List for the Wiki
	 * in Redmine.
	 *
	 * @param scanMapList
	 *            The HashMap List containing the spectrum information
	 */
	public static void printScanForWikiRedmine(List<HashMap<String, String>> scanMapList) {
		System.out.println("|_. Fichiers  |_. Provenance |_. Startable |_. Remarque Startable "
				+ "|_. JFits |_. Remarque JFits |_. Nombre d'HDU |_. Nombre de spectres |_. xUnit "
				+ "|_. [ MinX, MaxX ] |_. yUnit |_. [ MinY, MaxY ] |_. Nombre de points |_. NAXIS |");

		for (HashMap<String, String> map : scanMapList) {
			System.out.println("| " + getFromMap(map,FILE) + " | | " + getFromMap(map,STARTABLE) +
					" | | " + getFromMap(map,JFITS) + " | | "+ getFromMap(map,HDU_NUMBER) + " | " +
					getFromMap(map,SPECTRUM_NUMBER) + " | " + getFromMap(map,XUNIT) + " | " +
					getFromMap(map,XMINMAX) + " | " + getFromMap(map,YUNIT) + " | " +
					getFromMap(map,YMINMAX) + " | " + getFromMap(map,POINT_NUMBER) + " | " +
					getFromMap(map,NAXIS) + " |");
		}
	}

	public static List<File> getAllFits(String path, boolean recursive) {
		File ressource = new File(path);
		ArrayList<File> files = new ArrayList<File>();
		if (ressource.isDirectory()) {
			File[] fileTab = ressource.listFiles();

			for (File f : fileTab) {
				if (recursive) {
					files.addAll(getAllFits(f.getAbsolutePath(), recursive));
				} else if (!f.isDirectory() &&
						  (f.getAbsolutePath().endsWith(".fits") ||
						   f.getAbsolutePath().endsWith(".fts"))) {
					files.add(new File(f.getAbsolutePath()));
				}
			}
		} else {
			if (ressource.getAbsolutePath().endsWith(".fits")
					|| ressource.getAbsolutePath().endsWith(".fts")) {
				files.add(new File(ressource.getAbsolutePath()));
			}
		}
		return files;
	}

	public static  List<File> getHifiSpectrumDataSet(List<File> files) {
		List<File> res = new ArrayList<File>();
//		for (File file : files) {
//			FileManagerFits fileManagerFits = new FileManagerFits(file);
//			List<ArrayList<CassisMetadata>> allMeta = fileManagerFits.getAllHduList();
//
//			for (ArrayList<CassisMetadata> arrayList : allMeta) {
//				if (FileManagerFits.isHifiSpectrumDataset(arrayList)){
//					res.add(file);
//				}
//			}
//
//		}
		return res;
	}


	/**
	 * Display the tree of the allFits file filter by tree
	 * @param allFits
	 */
	private static void displayTreeFits(List<File> allFits) {
		HashMap<String, ArrayList<File>> treeFilter = new HashMap<String, ArrayList<File>>();
		for (File file : allFits) {
			FileManagerFits fileManagerFits = new FileManagerFits(file);
			Tree<Integer> finalTree = fileManagerFits.getFinalTree();

			String treeString = finalTree.toString();
			if (treeFilter.containsKey(treeString)){
				treeFilter.get(treeString).add(file);
			} else {
				ArrayList<File> files = new ArrayList<File>();
				files.add(file);
				treeFilter.put(treeString, files);
			}

		}
		for (String key : treeFilter.keySet()) {
			System.out.println(treeFilter.get(key));
			key = key.replaceAll(":-1\n", "");
			key = key.replaceAll("\n               :", "::");
			key = key.replaceAll("\n          :",":");
			key = key.replaceAll("     :","");
			key = key.replaceAll("\n",",");
			System.out.println(key);
		}
	}

	public static void generateFitsScanReport(String[] args) {
		if (args.length < 1) {
			logger.severe("arg1 : path of the fits file repertory "
					+ "[filename= fitsScan.html]");
			return;
		}
		logger.info("Read parameters ...");
		String fitsPath = args[0];
		String fileReport = "/home/jglorian/fitsScan.html";
		if (args.length > 1 ) {
			fileReport = args[1]+ "/fitsScan.html";
		}
		logger.info("Begin fits reporting ...");
		try {
			generateReport(fitsPath, fileReport);
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error while selecting a molecule", e);
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		logger.info("End database reporting ...");
	}


	/**
	 * Do the database report.
	 *
	 * @throws SQLException In case of SQL error.
	 * @throws IOException In case of IO error.
	 * @throws InterruptedException In case of Thread error.
	 * @throws ExecutionException In case of Thread error.
	 */
	public static void generateReport(String fitsPath, String fileReport) throws IOException, InterruptedException, ExecutionException {

		BufferedWriter builder = new BufferedWriter(new FileWriter(fileReport));
		addTopGlobalFitsReport(builder, fitsPath);
		for (File fitsFile : getAllFits(fitsPath, false)) {

			addFileInformation(builder,fitsFile.getAbsolutePath());
			System.out.println(fitsFile.getAbsolutePath());
//			String fileName = fitsFile.getName();

//			builder.append("@Test\n");
			//rename fitsFile name for the test
//			String testName = generateTestName(fileName);
			File file = fitsFile;
			FileManagerFits fileManagerFits = new FileManagerFits(file);
			addFitsFileReport(builder, "", fileManagerFits.readAll());



		}


		builder.append("</body>\n</html>");
		builder.flush();
		builder.close();
	}

		/**
		 * Add the top of the report (header, database basic report, sub-database table).
		 *
		 * @param builder The writer.
		 * @param fitsPath
		 * @throws IOException In case of IO error.
		 */
		private static void addTopGlobalFitsReport(BufferedWriter builder, String fitsPath) throws IOException {
			addHeader(builder, fitsPath);

			builder.append("<body>");
			builder.append("<h1>Scan the fits file  of the path ").append(fitsPath);
			builder.append("\".</h1>\n");

		}

		/**
		 * Add HTML header.
		 *
		 * @param builder The writer used to add the header.
		 * @param fitsPath
		 * @throws IOException In case of IO error.
		 */
		private static void addHeader(BufferedWriter builder, String fitsPath) throws IOException {
			builder.append("<!DOCTYPE html>\n");
			builder.append("<html>\n");
			builder.append("<head>\n");
			builder.append("<meta charset=\"UTF-8\">\n");
			builder.append("<title>Scan the fits file  of the path ").append(fitsPath);
			builder.append("</title>\n");
			builder.append("<style>\n");
			builder.append("table, th, td {\n");
			builder.append("  border: 1px solid black;\n");
			builder.append("  border-collapse: collapse;\n");
			builder.append("  text-align: center;\n");
			builder.append("  padding: 6px;\n");
			builder.append("}\n");
			builder.append("td.re {\n");
			builder.append("  color: #FF0000\n");
			builder.append("}\n");
			builder.append("td.ad {\n");
			builder.append("  color: #01DF01\n");
			builder.append("}\n");
			builder.append("td.a {\n");
			builder.append("  color: #43a9de\n");
			builder.append("}\n");
			builder.append("td.b {\n");
			builder.append("  color: #3ac980\n");
			builder.append("}\n");
			builder.append("</style>\n");

			builder.append("<script language=\"javascript\">\n");
			builder.append("function toggle(idToToggle) {\n");
			builder.append("  var ele = document.getElementById(idToToggle);\n");
			builder.append("  if(ele.style.display == \"block\") {\n");
			builder.append("    ele.style.display = \"none\";\n");
			builder.append("  } else {\n");
			builder.append("    ele.style.display = \"block\";\n");
			builder.append("  }\n");
			builder.append("}\n");
			builder.append("</script>\n");

			builder.append("</head>\n");
			builder.flush();
		}

		/**
		 * Add base informations for the two databases.
		 *
		 * @param builder The writer used to add the content.
		 * @throws IOException In case of IO error.
		 */
		private static void addFileInformation(BufferedWriter builder, String fileName) throws IOException {
			FileManagerFits fileManagerFits = new FileManagerFits(new File(fileName));
			fileManagerFits.read();
			int nbHDUSpectrum = fileManagerFits.getNbHdu();
			int nbSpectra = fileManagerFits.readNbCassisSpectra();
			String tree  =fileManagerFits.getFinalTree().toString();
				tree = tree.replaceAll(":-1\n", "");
				tree = tree.replaceAll("\n               :", "::");
				tree = tree.replaceAll("\n          :",":");
				tree = tree.replaceAll("     :","");
				tree = tree.replaceAll("\n",",");
			builder.append("<ul style=\"list-style-type:disc\">\n");
			builder.append("  <li><b>").append("File :" + fileName).append("</b>:</li>\n");
			builder.append("  <ul style=\"list-style-type:circle\">\n");
			builder.append("    <li>Nb HDU : ").append(String.valueOf(nbHDUSpectrum)).append("</li>\n");
			builder.append("    <li>Nb nbSpectra detected: ").append(String.valueOf(nbSpectra)).append("</li>\n");
			builder.append("    <li>Fits Meta architecture: ").append(tree).append("</li>\n");

			builder.append("  </ul>\n");
			builder.append("</ul>\n");
		}


		/**
		 * Add a transitions array.
		 *
		 * @param builder The writer used to add the report.
		 * @param htmlClass The HTML class used.
		 * @param lines The list of lines/transitions.
		 * @throws IOException In case of IO error.
		 */
		private static void addFitsFileReport(BufferedWriter builder, String htmlClass,
				List<CassisSpectrum> spectra) throws IOException {
			builder.append("<table>\n");
			builder.append("  <tr>\n");
			builder.append("    <th>Tittle (HDU name)</th>\n");
			builder.append("    <th>Type</th>\n");
			builder.append("    <th>Nb Points</th>\n");
			builder.append("    <th>XMinMaxAxes</th>\n");
			builder.append("    <th>YMinMaxAxes</th>\n");
			builder.append("    <th>Vlsr</th>\n");
			builder.append("    <th>telescope</th>\n");
			builder.append("    <th>lofreq</th>\n");
			builder.append("    <th>Image</th>\n");
			builder.append("    <th>All meta</th>\n");
			builder.append("  </tr>\n");

			for (CassisSpectrum spectrum : spectra) {
				addCassisSpectrum(builder, spectrum, htmlClass);
			}
			builder.append("</table>\n");
		}




	/**
	 * Add a row for a line.
	 *
	 * @param builder The writer used to add the report.
	 * @param l The line.
	 * @param htmlClass The HTML class used.
	 * @throws IOException In case of IO error.
	 */
	private static void addCassisSpectrum(BufferedWriter builder, CassisSpectrum cassisSpectrum ,
			String htmlClass) throws IOException {

		builder.append("  <tr>\n");
		String hduName = "";
		CassisMetadata hduMeta = cassisSpectrum.getOriginalMetadata(CassisMetadata.HDU_NAME);
		if (hduMeta != null){
			hduName = hduMeta.getValue();
		}
		String name = cassisSpectrum.getTitle() + "("+ hduName +")";
		builder.append("    <td class=\"").append(htmlClass).append("\">").append(name).append("</td>\n");
		final CassisMetadata fitsTypeMetaData = cassisSpectrum.getCassisMetadata(CassisMetadata.FITS_TYPE);
		String type = fitsTypeMetaData.getValue() + "(" + fitsTypeMetaData.getComment()+")";
		builder.append("    <td class=\"").append(htmlClass).append("\">").append(type).append("</td>\n");
		String nbPoints = String.valueOf(cassisSpectrum.getNbChannel());
		builder.append("    <td class=\"").append(htmlClass).append("\">").append(nbPoints).append("</td>\n");


		String x = " [" + cassisSpectrum.getxAxisOrigin() + "]";

		XAxisCassis xAxisOrigin = cassisSpectrum.getxAxisOrigin();
		if (xAxisOrigin.isInverted()){
			x = ""+xAxisOrigin.convertFromMhzFreq(cassisSpectrum.getFreqMax())+ ", " +
				xAxisOrigin.convertFromMhzFreq(cassisSpectrum.getFreqMin()) + x;
		}else {
			x = ""+xAxisOrigin.convertFromMhzFreq(cassisSpectrum.getFreqMin())+ ", " +
					xAxisOrigin.convertFromMhzFreq(cassisSpectrum.getFreqMax()) + x;
		}

		builder.append("    <td class=\"").append(htmlClass).append("\">").append(x).append("</td>\n");

		String y = cassisSpectrum.getIntensityMin() + ", " + cassisSpectrum.getIntensityMax() + ""+
				cassisSpectrum.getYAxis();
		builder.append("    <td class=\"").append(htmlClass).append("\">").append(y).append("</td>\n");

		builder.append("    <td class=\"").append(htmlClass).append("\">");
		final CassisMetadata vlsrMetadata = cassisSpectrum.getCassisMetadata(CassisMetadata.VLSR);
		if (vlsrMetadata == null){
			builder.append("\t"+ vlsrMetadata+",\n");
		}else {
			builder.append("\t"+ vlsrMetadata.getValue()+" ( " + vlsrMetadata.getComment()+ ")["+
								vlsrMetadata.getUnit()+"]\n");
		}
		builder.append("</td>\n");
		builder.append("    <td class=\"").append(htmlClass).append("\">");
		final CassisMetadata telescopeMetadata = cassisSpectrum.getCassisMetadata(CassisMetadata.TELESCOPE);
		if (telescopeMetadata == null){
			builder.append("\t"+ telescopeMetadata+",\n");
		}else {
			builder.append(telescopeMetadata.getName() +"=" +
					telescopeMetadata.getValue()+"\", \"" + telescopeMetadata.getComment()+ "\", \""+
					telescopeMetadata.getUnit()+"\")\n");
		}
		builder.append("</td>\n");
		builder.append("    <td class=\"").append(htmlClass).append("\">");
		final CassisMetadata loFreqMetadata = cassisSpectrum.getCassisMetadata(CassisMetadata.LOFREQ);
		if (loFreqMetadata == null){
			builder.append("\t"+ loFreqMetadata+",\n");
		}else {
			builder.append("\t"+
					loFreqMetadata.getValue()+" (" + loFreqMetadata.getComment()+ ") ["+
					loFreqMetadata.getUnit()+"]\n");
		}
		builder.append("</td>\n");

		builder.append("  </tr>\n");



//		builder.append("    <th>XMinMaxAxes</th>\n");
//		builder.append("    <th>YMinMaxAxes</th>\n");

	}

	/**
	 * Display parameters of file
	 *
	 * @param file
	 */
	public static void inspectFitsFile(File file) {
		FitsFile fitsFile = null;
		try {
			try {
				fitsFile = new FitsFile(file);
			} catch (FitsException | IOException e) {
				return;
			}
			System.out.println("********* Inspect fits " + file.getCanonicalPath());
			int noHdu = fitsFile.getNoHDUnits();
			System.out.println("nbHDU=" + noHdu);
			for (int i = 0; i < noHdu; i++) {
				FitsHDUnit hdu = fitsFile.getHDUnit(i);
				FitsHeader hdr = hdu.getHeader();
				System.out.println("*** HDU" + i + " ***");
				System.out.println("HDU TYPE=" + hdu.getType());
				@SuppressWarnings("rawtypes")
				ListIterator itr = hdr.getKeywords();
				while (itr.hasNext()) {
					FitsKeyword kw = (FitsKeyword) itr.next();
					System.out.println(kw.getName() + "=" + kw.getString() + "// " + kw.getComment());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fitsFile != null) {
				fitsFile.closeFile();
			}
		}
	}



}
