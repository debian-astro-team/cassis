/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.file.gui.ColumnsDetected;

public class FileManagerSimpleData extends FileManager implements SaveSpectrumInterface {


	private List<CassisSpectrum> cassisSpectrumList;
	private List<CassisMetadata> cassisMetadataList;
	private XAxisCassis xAxis = XAxisCassis.getXAxisFrequency();
	private YAxisCassis yAxis = YAxisCassis.getYAxisKelvin();
	private boolean findXAxis = false;
	private boolean findYAxis = false;


	/**
	 * Construct a FileManagerSimpleData with exceptions displayed.
	 *
	 * @param file The input File.
	 */
	public FileManagerSimpleData(File file) {
		this(file, true);
	}

	/**
	 * Construct a FileManagerSimpleData.
	 *
	 * @param file The input File
	 * @param displayException If the exceptions should be displayed.
	 */
	public FileManagerSimpleData(File file, boolean displayException) {
		super(file, displayException);
		cassisMetadataList = new ArrayList<CassisMetadata>();
		cassisSpectrumList = new ArrayList<CassisSpectrum>();
	}

	@Override
	public CassisSpectrum read() {
		CassisSpectrum cassisSpectrum = null;

		try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));) {
			cassisSpectrum = read(br, file.getName());

		} catch (IOException x) {
		    System.err.println(x);
		}

		return cassisSpectrum;
	}

	public CassisSpectrum read(BufferedReader br, String name) throws IOException {
		CassisSpectrum cassisSpectrum;
		double[] wave = new double[CassisSpectrum.NB_MAX_CHANNELS];
		double[] flux = new double[CassisSpectrum.NB_MAX_CHANNELS];
		int nbPoints = 0;
		boolean searchMeta = true;
		String metaIndentifier = null;
		String metaSeparator = null;

		String line = null;
		while ((line = br.readLine()) != null) {
			if (searchMeta) {
				String[] metaParameter = extractMetaParameters(line);
				metaIndentifier = metaParameter[0];
				metaSeparator = metaParameter[1];
				// read the metadata
				if (metaIndentifier != null) {
					while (line != null && line.trim().startsWith(metaIndentifier)) {
						String[] metadata = line.substring(1, line.length()).split(metaSeparator);
						if (metadata.length == 2) {
							String key = metadata[0].trim().toLowerCase();
							String value = metadata[1].trim();
							extractMetaData(key, value);
						}
						line = br.readLine();
					}
				}
			}
			searchMeta = false;

			if (line != null &&! line.trim().startsWith(metaIndentifier)) {
				String[] val = Pattern.compile("[ \t]").split(line);
				if (val.length > 1 ) {
					wave[nbPoints] = Double.valueOf(val[0]);
					flux[nbPoints] = Double.valueOf(val[1]);
					nbPoints++;
				}
			}
		}
		wave = Arrays.copyOf(wave, nbPoints);
		flux = Arrays.copyOf(flux, nbPoints);


		double waveSpec[] = new double[nbPoints];
		double fluxSpec[] = new double[nbPoints];
		if (! xAxis.getUnit().equals(UNIT.MHZ)) {
			if (xAxis.isInverted()) {
				  for (int i = 0; i < nbPoints; i++) {
					fluxSpec[i] = flux[flux.length - 1 - i];
					waveSpec[i] = xAxis.convertToMHzFreq(wave[flux.length - 1 - i]);

				  }
			} else {
				for (int i = 0; i < nbPoints; i++) {
					fluxSpec[i] = flux[i];
					waveSpec[i] = xAxis.convertToMHzFreq(wave[i]);

				  }
			}

		} else {
			waveSpec = wave;
			fluxSpec = flux;
		}
		cassisSpectrum = CassisSpectrum.generateCassisSpectrum(name,
				waveSpec, fluxSpec, 0., xAxis, yAxis);

		cassisSpectrum.setxError(!findXAxis);
		cassisSpectrum.setyError(!findYAxis);
		cassisSpectrum.addCassisMetadata(new CassisMetadata(CassisMetadata.FILE_TYPE,
				FileManager.FileType.ASCII.name(), null, null), true);
		cassisSpectrumList.add(cassisSpectrum);
		return cassisSpectrum;
	}

	private void extractMetaData(String key, String value) {
		String unit = "";
		String comment = "";
		int inBracketIndex = value.indexOf("[");
		int outBracketIndex = value.indexOf("]");
		boolean detectionUnit =  value.split("\\[").length>2?false:true;
		if (inBracketIndex > -1 && outBracketIndex > 1 && detectionUnit){
			unit = value.substring(inBracketIndex+1, outBracketIndex);
			value = value.replace("[" + unit + "]", "");
			unit = unit.trim();
		}
		int commentIndex = value.indexOf("//");
		boolean detectionComment =  value.split("//").length>2?false:true;
		if (commentIndex > -1 && detectionComment) {
			comment = value.substring(commentIndex+2);
			value = value.replace("//" + comment, "");
			comment = comment.trim();
		}

		if (key.equals("number of line")) {
			cassisMetadataList.add(new CassisMetadata("size", value, comment, unit));
		} else if (key.equals("telescop")) {
			cassisMetadataList.add(new CassisMetadata("telescope", value, comment, unit));
		} else if (key.equals("xunit")) {
			xAxis = XAxisCassis.getXAxisCassis(value);
			cassisMetadataList.add(new CassisMetadata("wave", xAxis.getAxis().toString(), comment,  xAxis.getUnit().toString()));
			findXAxis = true;
		} else if (key.equals("yunit")) {
			yAxis = YAxisCassis.getYAxisCassis(value);
			cassisMetadataList.add(new CassisMetadata("flux",  yAxis.getAxis().toString(),  comment, yAxis.getUnit().toString()));
			findYAxis = true;
		} else if (key.equals("xlabel")|| key.equals("wave")) {
			xAxis = XAxisCassis.getXAxisCassis(unit);
			cassisMetadataList.add(new CassisMetadata("wave", xAxis.getAxis().toString(), comment,  xAxis.getUnit().toString()));
			findXAxis = true;
		} else if (key.equals("ylabel") ||key.equals("flux")) {
			yAxis = YAxisCassis.getYAxisCassis(unit);
			cassisMetadataList.add(new CassisMetadata("flux", yAxis.getAxis().toString(), comment,  yAxis.getUnit().toString()));
			findYAxis = true;
		}

		else {
			cassisMetadataList.add(new CassisMetadata(key, value,  comment, unit));
		}
	}

	/**
	 * search the metaIdentifier , or null if not detected and
	 * and the metaSeparator, ':' by default
	 * @param line
	 * @return String[metaIdentifier,metaSeparator]
	 */
	private String[] extractMetaParameters(String line) {
		String[] metaParameter = new String[2];
		metaParameter[0] = line.substring(0,1);
		try {
			Integer.valueOf(metaParameter[0]);
			metaParameter[0] = null ;
		} catch (NumberFormatException e) {

		}

		metaParameter[1] = ":";
		if (metaParameter[0] != null) {
			if (line.split("=").length  == 2) {
				metaParameter[1] = "=";
			}

		}
		return metaParameter;
	}

	@Override
	public CassisSpectrum read(int spectrumIndex) {
		return cassisSpectrumList.get(spectrumIndex);
	}

	@Override
	public List<CassisMetadata> readCassisMetadata(int spectrumIndex) {
		return cassisMetadataList;
	}

	@Override
	public List<CassisSpectrum> readAll() {
		read();
		return cassisSpectrumList;
	}

	@Override
	public CassisMetadata getCassisMetadata(String name, int spectrumIndex) {
		return null;
	}

	@Override
	public List<CassisMetadata> getCommonCassisMetadataList() {
		return cassisMetadataList;
	}

	@Override
	public List<ColumnsDetected> getWaveColumnsDetected() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ColumnsDetected> getFluxColumnsDetected() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean save(File file, CommentedSpectrum commentedSpectrum) {
		try (BufferedWriter fileAscii = new BufferedWriter(new FileWriter(file))) {
			List<CassisMetadata> cassisMetadataList = commentedSpectrum.getOriginalMetadataList();


			for (CassisMetadata cassisMetadata : cassisMetadataList) {
				fileAscii.write("#" + cassisMetadata.getName() + "=" + cassisMetadata.getValue());
				if (!(cassisMetadata.getUnit() == null ||cassisMetadata.getUnit().equals(""))) {
					fileAscii.write("[" + cassisMetadata.getUnit()+ "]");


				}
				fileAscii.newLine();
				fileAscii.flush();
			}


			XAxisCassis xAxisOriginData = commentedSpectrum.getxAxisOrigin();
			double[] xData = commentedSpectrum.getXData(xAxisOriginData);
			double[] intensity = commentedSpectrum.getIntensities(xAxisOriginData);

			fileAscii.flush();
			int size = xData.length;
			for (int i = 0; i < size; i++) {
				fileAscii.write(String.valueOf(xData[i]) + "\t"
						+ String.valueOf(intensity[i]));
				fileAscii.newLine();
				fileAscii.flush();
			}
			return true;
		} catch (IOException e) {
			return false;
		}
	}
}
