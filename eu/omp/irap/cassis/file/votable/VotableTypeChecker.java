/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.votable;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Simple class for determining the type of a VOTable file.
 *
 * @author M. Boiziot
 */
public class VotableTypeChecker {

	private VotableType type;
	private VotableType specializedType;
	private boolean containsError;


	/**
	 * Constructor.
	 *
	 * @param votableContent The content of the VOTable.
	 */
	public VotableTypeChecker(String votableContent) {
		this.type = VotableType.UNKNOWN;
		this.specializedType = VotableType.UNKNOWN;
		this.read(votableContent);
	}

	/**
	 * Constructor.
	 *
	 * @param file The File object of the VOTable.
	 */
	public VotableTypeChecker(File file) {
		this.type = VotableType.UNKNOWN;
		this.specializedType = VotableType.UNKNOWN;
		this.read(file);
	}

	/**
	 * Read the VOTable file.
	 *
	 * @param file The file.
	 */
	private void read(File file) {
		if (file == null || !file.exists()) {
			containsError = false;
			return;
		}
		try {
			Document doc = createDocument(file);
			parse(doc);
		} catch (Exception e) {
			containsError = true;
		}
	}

	/**
	 * Read the content of the VOTable.
	 *
	 * @param content The content.
	 */
	private void read(String content) {
		if (content == null || content.isEmpty()) {
			containsError = true;
			return;
		}

		try {
			Document doc = createDocument(content);
			parse(doc);
		} catch (Exception e) {
			containsError = true;
		}
	}

	/**
	 * Create a Document from a response (String).
	 *
	 * @param response The response as a String.
	 * @return The Document.
	 * @throws ParserConfigurationException In case of parser Exception
	 * @throws SAXException In case of SAX Exception.
	 * @throws IOException In case of IOException.
	 */
	private Document createDocument(String response) throws ParserConfigurationException,
			SAXException, IOException {
		Document doc;
		try (InputStream stream = new ByteArrayInputStream(response.getBytes());) {
			doc = getDocumentBuilder().parse(stream);
			doc.getDocumentElement().normalize();
		}
		return doc;
	}

	/**
	 * Create a Document from a File.
	 *
	 * @param file The file to parse.
	 * @return The Document.
	 * @throws ParserConfigurationException In case of parser Exception
	 * @throws SAXException In case of SAX Exception.
	 * @throws IOException In case of IOException.
	 */
	private Document createDocument(File file) throws ParserConfigurationException,
			SAXException, IOException {
		Document doc = getDocumentBuilder().parse(file);
		doc.getDocumentElement().normalize();
		return doc;
	}

	/**
	 * Create a DocumentBuilder and configure it.
	 *
	 * @return The new created DocumentBuilder.
	 * @throws ParserConfigurationException In case of parser Exception.
	 */
	private DocumentBuilder getDocumentBuilder() throws ParserConfigurationException {
		DocumentBuilderFactory docBuilderFact = DocumentBuilderFactory.newInstance();
		docBuilderFact.setFeature(
				"http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
		docBuilderFact.setFeature(
				"http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
		DocumentBuilder docBuilder = docBuilderFact.newDocumentBuilder();
		docBuilder.setErrorHandler(null);
		return docBuilder;
	}

	private void parse(Document doc) {
		parseVotableElement(doc);
		parseInfos(doc);
		parseTable(doc);
		parseFields(doc);
		trySpecialSsaCase(doc);
		trySpecialSpectrumCase(doc);
	}

	/**
	 * Parse the VOTABLE element.
	 *
	 * @param doc The document.
	 */
	private void parseVotableElement(Document doc) {
		for (Node attribute : getAttributesFromElementName(doc, "VOTABLE")) {
			/* In an attribute of VOTABLE starting with xmlns... */
			if (attribute.getNodeName().contains("xmlns")) {
				/* if the value contains the SSA reference URL, this is a SSA result */
				if (attribute.getNodeValue().contains("http://www.ivoa.net/xml/DalSsap/v1.0")) {
					setTypeSsa();
					return;
				}
			}
		}
	}


	/**
	 * Parse INFO elements.
	 *
	 * @param doc The document.
	 */
	private void parseInfos(Document doc) {
		if (isTypeKnown()) {
			return;
		}
		for (Node element : getElements(doc, "INFO")) {
			/* An INFO element named "SERVICE_PROTOCOL" with value = "SSAP" is a return from SSAP. */
			for (Node attribute : getAttributes(doc, element)) {
				if ("name".equalsIgnoreCase(attribute.getNodeName()) &&
						"SERVICE_PROTOCOL".equalsIgnoreCase(attribute.getNodeValue())) {
					if ("SSAP".equalsIgnoreCase(element.getTextContent().trim())) {
						setTypeSsa();
						return;
					} else if ("EPN-TAP".equalsIgnoreCase(element.getTextContent())) {
						setTypeEpnTap();
						return;
					}
				}
			}
		}
	}

	/**
	 * Parse TABLE element.
	 *
	 * @param doc The document.
	 */
	private void parseTable(Document doc) {
		if (isTypeKnown()) {
			return;
		}
		for (Node element : getElements(doc, "TABLE")) {
			for (Node attribute : getAttributes(doc, element)) {
				if ("name".equalsIgnoreCase(attribute.getNodeName())) {
					/* A TABLE with an attribute name = "SSAP" is a return from SSAP. */
					if ("SSAP".equalsIgnoreCase(attribute.getNodeValue())) {
						setTypeSsa();
						return;
					/* A TABLE with an attribute name = "epn_core" is a return from EPN-TAP */
					} else if ("epn_core".equalsIgnoreCase(attribute.getNodeValue())) {
						setTypeEpnTap();
						return;
					/* A TABLE with an attribute name = "spectrum" is a Spectrum */
					} else if ("spectrum".equalsIgnoreCase(attribute.getNodeValue())) {
						setTypeSpectrum();
						return;
					}
				} else if ("utype".equalsIgnoreCase(attribute.getNodeName())) {
					/* A TABLE with an attribute utype = "spec:Spectrum" is a Spectrum */
					if ("spec:Spectrum".equalsIgnoreCase(attribute.getNodeValue())) {
						setTypeSpectrum();
						return;
					}
				}
			}
		}
	}

	/**
	 * Parse FIELDs elements.
	 *
	 * @param doc The document.
	 */
	private void parseFields(Document doc) {
		if (isTypeKnown()) {
			return;
		}
		for (Node field : getElements(doc, "FIELD")) {
			for (Node attribute : getAttributes(doc, field)) {
				if ("utype".equalsIgnoreCase(attribute.getNodeName())) {
					/* If an attribute from a FIELD contains the mandatory utype for SSAP to access the data is a return from SSAP. */
					if ("ssa:access.reference".equalsIgnoreCase(attribute.getNodeValue())
							|| "Access.Reference".equalsIgnoreCase(attribute.getNodeValue())
							|| "ssa.Access.Reference".equalsIgnoreCase(attribute.getNodeValue())) {
						setTypeSsa();
						return;
					} else if (attribute.getNodeValue().toLowerCase()
							.contains("Spectrum.Data.SpectralAxis.Value".toLowerCase())
							|| attribute.getNodeValue().toLowerCase().contains(
									"Spectrum.Data.FluxAxis.Value".toLowerCase())) {
						setTypeSpectrum();
						return;
					}
				}
			}
		}
	}

	/**
	 * Try some specific check for badly written response from SVO Theoretical Data Access Service:
	 *  - TLUSTY-BSTAR
	 *  - TLUSTY OSTAR2002 Grid: O-type stars,  Teff = 27500K - 55000 K
	 *  - TLUSTY OSTAR2002+BSTAR2006 Grid
	 *  - Model generated with Sed@.0 code
	 *  - PopStar Evolutionary synthesis models using Kroupa (2002) IMF.
	 *  - PopStar Evolutionary synthesis models using Ferrini, Penco, Palla (1990) IMF.
	 *  - NextGen
	 *  - Husfeld et al models for non-LTE Helium-rich stars
	 *  - DUSTY00
	 *  - Synthetic stellar library by P. Coelho, fully described in Coelho et al. (2005)
	 *
	 * @param doc The Document.
	 */
	private void trySpecialSsaCase(Document doc) {
		if (isTypeKnown()) {
			return;
		}
		if (haveQueryStatus(doc) && haveDatalink(doc)) {
			setTypeSsa();
		}
	}

	/**
	 * Parse the FIELD elements and search for data link.
	 *
	 * @param doc The document.
	 * @return true if there is a data link, false otherwise.
	 */
	private boolean haveDatalink(Document doc) {
		for (Node info : getElements(doc, "FIELD")) {
			boolean foundNameSpectrum = false;
			boolean foundUcdDataLink = false;
			for (Node attribute : getAttributes(doc, info)) {
				if (!foundNameSpectrum && "name".equals(attribute.getNodeName())
						&& "Spectrum".equalsIgnoreCase(attribute.getNodeValue())) {
					foundNameSpectrum = true;
				} else if (!foundUcdDataLink && "ucd".equals(attribute.getNodeName())
						&& "DATA_LINK".equalsIgnoreCase(attribute.getNodeValue())) {
					foundUcdDataLink = true;
				}
				if (foundNameSpectrum && foundUcdDataLink) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Search if there is a QUERY_STATUS INFO.
	 *
	 * @param doc The document.
	 * @return true if there is a QUERY_STATUS INFO, false otherwise.
	 */
	private boolean haveQueryStatus(Document doc) {
		for (Node info : getElements(doc, "INFO")) {
			for (Node attribute : getAttributes(doc, info)) {
				if ("name".equalsIgnoreCase(attribute.getNodeName()) &&
						"QUERY_STATUS".equalsIgnoreCase(attribute.getNodeValue())) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Try special case for trying to detect it as a spectrum.
	 *
	 * @param doc The document.
	 */
	private void trySpecialSpectrumCase(Document doc) {
		if (isTypeKnown()) {
			return;
		}
		List<Node> fields = getElements(doc, "FIELD");
		if ((fields.size() >= 2 && haveTwoAxisFields(doc)) || isOldVotableCassisSpectrum(fields)) {
			setTypeSpectrum();
		}
	}

	/**
	 * Return if this is an old VOTable Spectrum from CASSIS.
	 *
	 * @param fields The fields.
	 * @return true if this is an old VOTable spectrum from CASSIS, false otherwise.
	 */
	private boolean isOldVotableCassisSpectrum(List<Node> fields) {
		List<String> fieldsNames = getFieldsNames(fields);
		String[] expectedFields = { "FreqencyLSB", "VelocityLSB", "FreqencyUSB",
				"VelocityUSB", "intensity", "deltaV", "deltaF" };
		for (String field : expectedFields) {
			if (!fieldsNames.contains(field)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Check then return if there is two declared axis; searching using the UCD.
	 *
	 * @param doc The document.
	 * @return true if two axis were found, false otherwise.
	 */
	private boolean haveTwoAxisFields(Document doc) {
		List<String> ucdList = getFieldsUcd(doc);
		return VotableUcdUtil.isContainsFluxUcd(ucdList)
				&& VotableUcdUtil.isContainsWaveUcd(ucdList);
	}

	/**
	 * Return the list of values of the UCDs attributes from FIELD elements.
	 *
	 * @param doc The document.
	 * @return the list of values of the UCDs attributes from FIELD elements.
	 */
	private List<String> getFieldsUcd(Document doc) {
		List<Node> fields = getElements(doc, "FIELD");
		List<String> fieldsUcdList = new ArrayList<>(fields.size());
		for (Node field : fields) {
			Node attribute = getAttributes(field, "ucd");
			if (attribute != null) {
				fieldsUcdList.add(attribute.getNodeValue());
			}
		}
		return fieldsUcdList;
	}

	/**
	 * Return the Node attributes from a given element name.
	 *
	 * @param doc The document.
	 * @param elementName The element name.
	 * @return The list of Node attributes.
	 */
	private List<Node> getAttributesFromElementName(Document doc, String elementName) {
		List<Node> elements = getElements(doc, elementName);
		if (elements.size() != 1) {
			return Collections.emptyList();
		}
		return getAttributes(doc, elements.get(0));
	}

	/**
	 * Search then return the elements with the given name.
	 *
	 * @param doc The document.
	 * @param elementName The element name.
	 * @return the elements with the given name.
	 */
	private List<Node> getElements(Document doc, String elementName) {
		NodeList elements = doc.getElementsByTagName(elementName);
		List<Node> elementsList = new ArrayList<>(elements.getLength());
		if (elements.getLength() == 0) {
			return elementsList;
		}
		for (int i = 0; i < elements.getLength(); i++) {
			elementsList.add(elements.item(i));
		}
		return elementsList;
	}

	/**
	 * Return the list of attributes as a List of Node, from a given Node.
	 *
	 * @param doc The document.
	 * @param node The node from which we want the attributes.
	 * @return the list of attributes.
	 */
	private List<Node> getAttributes(Document doc, Node node) {
		List<Node> attributesList = new ArrayList<>(node.getAttributes().getLength());
		NamedNodeMap attributes = node.getAttributes();
		for (int i = 0; i < attributes.getLength(); i++) {
			attributesList.add(attributes.item(i));
		}
		return attributesList;
	}

	/**
	 * Return an attribute as a Node, from a given Node and with the given name.
	 *
	 * @param node The node from which we want the attributes.
	 * @param name The name.
	 * @return the attribute if found or null.
	 */
	private Node getAttributes(Node node, String name) {
		Node attribute;
		NamedNodeMap attributes = node.getAttributes();
		for (int i = 0; i < attributes.getLength(); i++) {
			attribute = attributes.item(i);
			if (name.equals(attribute.getNodeName())) {
				return attribute;
			}
		}
		return null;
	}

	/**
	 * Return the list of values of the name attributes from FIELD elements.
	 *
	 * @param fields The list of FIELD elements.
	 * @return the list of values of the name attributes from FIELD elements.
	 */
	private List<String> getFieldsNames(List<Node> fields) {
		List<String> fieldsNameList = new ArrayList<>(fields.size());
		for (Node field : fields) {
			Node attribute = getAttributes(field, "name");
			if (attribute != null) {
				fieldsNameList.add(attribute.getNodeValue());
			}
		}
		return fieldsNameList;
	}

	/**
	 * Return if the type is known.
	 *
	 * @return true if the type is known, false otherwise.
	 */
	private boolean isTypeKnown() {
		return getType() != VotableType.UNKNOWN;
	}

	/**
	 * Set the file as a list of spectrum from SSA.
	 */
	private void setTypeSsa() {
		type = VotableType.LIST_SPECTRA;
		specializedType = VotableType.SSA;
	}

	/**
	 * Set the file as a list of spectrum from EPN-TAP.
	 */
	private void setTypeEpnTap() {
		type = VotableType.LIST_SPECTRA;
		specializedType = VotableType.EPN_TAP;
	}

	/**
	 * Set the file as a spectrum.
	 */
	private void setTypeSpectrum() {
		type = VotableType.SPECTRUM;
		specializedType = VotableType.SPECTRUM;
	}

	/**
	 * Return if there was an error while parsing the file.
	 *
	 * @return true is there was an error, false otherwise.
	 */
	public boolean containsError() {
		return containsError;
	}

	/**
	 * Return if this is a result from an SSA service.
	 *
	 * @return true if this is the result from a SSA service, false otherwise.
	 */
	public boolean isSsaResult() {
		return specializedType == VotableType.SSA;
	}

	/**
	 * Return if this is a result from an EPN-TAP service.
	 *
	 * @return true if this is the result from a EPN-TAP service, false otherwise.
	 */
	public boolean isEpnTapResult() {
		return specializedType == VotableType.EPN_TAP;
	}

	/**
	 * Return if this is a spectrum.
	 *
	 * @return true if this is a spectrum, false otherwise.
	 */
	public boolean isSpectrum() {
		return type == VotableType.SPECTRUM;
	}

	/**
	 * Return if this VOTable contains a list of spectra.
	 *
	 * @return true if this VOTable contains a list of spectra, false otherwise.
	 */
	public boolean isSpectraList() {
		return type == VotableType.LIST_SPECTRA;
	}

	/**
	 * Return the type of the file.
	 *
	 * @return the type of the file.
	 */
	public VotableType getType() {
		return type;
	}

	/**
	 * Return the specialized type of the file.
	 *
	 * @return the type of the file.
	 */
	public VotableType getSpecializedType() {
		return specializedType;
	}

}
