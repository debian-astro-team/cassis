/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file.votable;

import java.util.ArrayList;
import java.util.List;

/**
 * Simple utility class for working with VOTable's UCDs.
 *
 * @author M. Boiziot
 */
public class VotableUcdUtil {

	private static final List<String> UCD_FLUX_LIST;
	private static final List<String> UCD_WAVE_LIST;


	static {
		UCD_FLUX_LIST = new ArrayList<>(28);
		UCD_FLUX_LIST.add("arith.rate;phot.count");
		UCD_FLUX_LIST.add("arith.ratio;phot.flux.density");
		UCD_FLUX_LIST.add("phot.antennaTemp");
		UCD_FLUX_LIST.add("phot.count");
		UCD_FLUX_LIST.add("phot.fluence;em.wl");
		UCD_FLUX_LIST.add("phot.flux.density");
		UCD_FLUX_LIST.add("phot.flux.density;em.energy");
		UCD_FLUX_LIST.add("phot.flux.density;em.energy;meta.number");
		UCD_FLUX_LIST.add("phot.flux.density;em.freq");
		UCD_FLUX_LIST.add("phot.flux.density;em.wl");
		UCD_FLUX_LIST.add("phot.flux.density;em.wl");
		UCD_FLUX_LIST.add("phot.flux.density;em.wl;phys.polarization");
		UCD_FLUX_LIST.add("phot.flux.density;em.wl;spect.continuum");
		UCD_FLUX_LIST.add("phot.flux.density;instr.beam");
		UCD_FLUX_LIST.add("phot.flux.density.sb;em.freq");
		UCD_FLUX_LIST.add("phot.flux.density.sb;em.wl");
		UCD_FLUX_LIST.add("phot.mag");
		UCD_FLUX_LIST.add("phot.mag.sb");
		UCD_FLUX_LIST.add("phys.area;phys.transmission");
		UCD_FLUX_LIST.add("phys.energy.density");
		UCD_FLUX_LIST.add("phys.luminosity;em.energy");
		UCD_FLUX_LIST.add("phys.luminosity;em.freq");
		UCD_FLUX_LIST.add("phys.luminosity;em.wl");
		UCD_FLUX_LIST.add("phys.luminosity;phys.angArea;em.wl");
		UCD_FLUX_LIST.add("phys.polarization");
		UCD_FLUX_LIST.add("phys.temperature");
		UCD_FLUX_LIST.add("phys.transmission");
		// Not in SpectrumDM but used by some services
		UCD_FLUX_LIST.add("spect.line.intensity");
		UCD_FLUX_LIST.add("phot.fluxDens");

		UCD_WAVE_LIST = new ArrayList<>(10);
		UCD_WAVE_LIST.add("em.wl");
		UCD_WAVE_LIST.add("em.freq");
		UCD_WAVE_LIST.add("em.energy");
		UCD_WAVE_LIST.add("instr.pixel;em.wl");
		UCD_WAVE_LIST.add("em.wavenumber");
		UCD_WAVE_LIST.add("em.wl;obs.atmos");
		UCD_WAVE_LIST.add("spect.dopplerVeloc");
		UCD_WAVE_LIST.add("spect.dopplerVeloc.radio");
		UCD_WAVE_LIST.add("spect.dopplerVeloc.opt");
		UCD_WAVE_LIST.add("spect.dopplerVeloc");
	}

	/**
	 * Check then return if the given UCD represent a flux UCD.
	 *
	 * @param ucd The UCD.
	 * @return true if the given UCD represent a flux UCD, false otherwise.
	 */
	public static boolean isFluxUcd(String ucd) {
		return UCD_FLUX_LIST.contains(ucd);
	}

	/**
	 * Check then return if the given UCD represent a wave UCD.
	 *
	 * @param ucd The UCD.
	 * @return true if the given UCD represent a wave UCD, false otherwise.
	 */
	public static boolean isWaveUcd(String ucd) {
		return UCD_WAVE_LIST.contains(ucd);
	}

	/**
	 * Check then return if in the given list of UCD values, there is at least
	 *  one flux UCD.
	 *
	 * @param ucdList A list of UCD values.
	 * @return true if there is a flux UCD in the given list.
	 */
	public static boolean isContainsFluxUcd(List<String> ucdList) {
		for (String ucd : ucdList) {
			if (isFluxUcd(ucd)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Check then return if in the given list of UCD values, there is at least
	 *  one wave UCD.
	 *
	 * @param ucdList A list of UCD values.
	 * @return true if there is a wave UCD in the given list.
	 */
	public static boolean isContainsWaveUcd(List<String> ucdList) {
		for (String ucd : ucdList) {
			if (isWaveUcd(ucd)) {
				return true;
			}
		}
		return false;
	}
}
