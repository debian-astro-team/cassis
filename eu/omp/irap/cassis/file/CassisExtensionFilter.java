/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * @author thomas
 */
public class CassisExtensionFilter extends FileFilter {

	/**
	 * Creates a file filter that accepts only cassis files.
	 */
	public CassisExtensionFilter() {
		super();
	}

	/**
	 * Return true if this file should be shown in the directory pane, false if
	 * it shouldn't.<BR>
	 * Accepts directories and cassis files.
	 *
	 * @param file
	 *            to check
	 * @return "true" if this file should be shown in the directory pane,
	 *         "false" if not
	 */
	@Override
	public boolean accept(final File file) {
		boolean isAccepted = false;

		if (file != null) {
			if (file.isDirectory()) {
				isAccepted = true;
			}

			final String name = file.getName().toLowerCase();

			if (CassisFileFilter.isClassFile(name) || name.endsWith(".txt") || name.endsWith(".fits")
					|| name.endsWith(".fca") || name.endsWith(".votable") || name.endsWith(".fus")
					|| name.endsWith(".lis") || name.endsWith(".sam") || name.endsWith(".ltm") || name.endsWith("lcm")
					|| name.endsWith(".lbm") || name.endsWith(".lam") || name.endsWith(".tec")) {
				isAccepted = true;
			}
		}

		return isAccepted;
	}

	/**
	 * Returns the human readable description of this filter.
	 *
	 * @return description
	 */
	@Override
	public String getDescription() {
		return "All Cassis files";
	}
}
