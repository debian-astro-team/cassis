/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * Creates a file filter that accepts only *.10m, *.30m, *.bas, and *.txt cassis
 * files.
 *
 * @author Roedy Green
 */
public class CassisFileFilter extends FileFilter {

	public static String[] extClass = { "30m", "hifi", "bas", "jcmt", "cso", "10m", "15m", "50m", "100m", "500m",
			"apex", "bur", "bure", "gbt", "iram", "sest" };
	public static String[] extFits = { "fits", "fts" };
	public static String[] extVotable = { "votable", "vot", "xml" };


	/**
	 * Creates a file filter that accepts only cassis files.
	 */
	public CassisFileFilter() {
		super();
	}

	/**
	 * Return true if this file should be shown in the directory pane, false if
	 * it shouldn't.<BR>
	 * Accepts directories and cassis files.
	 *
	 * @param file
	 *            to check
	 * @return "true" if this file should be shown in the directory pane,
	 *         "false" if not
	 */
	@Override
	public boolean accept(final File file) {
		boolean isAccepted = false;

		if (file != null) {
			if (file.isDirectory()) {
				isAccepted = true;
			}

			boolean isDataForCassis = isDataFileForCassis(file.getName());

			if (isDataForCassis) {
				isAccepted = true;
			}
		}

		return isAccepted;
	}

	/**
	 * @param file
	 * @return if the extension of the file is known by cassis
	 */
	public static boolean isDataFileForCassis(final String file) {
		final String name = file.toLowerCase();

		return (isClassFile(name) || name.endsWith(".txt") || isFitsFile(name)
				|| name.endsWith(".fca") || isVotableFile(name) || name.endsWith(".fus") || name
				.endsWith(".lis"));
	}

	public static boolean isClassFile(String val) {
		for (String ext : extClass) {
			if (val.endsWith(ext))
				return true;
		}
		return false;
	}

	public static boolean isVotableFile(String val) {
		for (String ext : extVotable) {
			if (val.endsWith(ext))
				return true;
		}
		return false;
	}

	public static boolean isFitsFile(String val) {
		for (String ext : extFits) {
			if (val.endsWith(ext))
				return true;
		}
		return false;
	}

	/**
	 * Returns the human readable description of this filter.
	 *
	 * @return description
	 */
	@Override
	public String getDescription() {
		StringBuilder extensions = new StringBuilder();
		for (String ext : extClass) {
			extensions.append("*.").append(ext).append(' ');
		}
		extensions.append("(from class) ");
		for (String ext : extFits) {
			extensions.append("*.").append(ext).append(' ');
		}
		for (String ext : extVotable) {
			extensions.append("*.").append(ext).append(' ');
		}
		extensions.append("*.fus *.fca *.lis (cassis files)");

		return extensions.toString();
	}
}
