/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.TreeMap;

public enum EXTENSION {
	/*
	 * Association extension type de fichier.
	 */
	FUS(".fus", "AsciiCassis"), FCA(".fca", "Ascii"), FITS(".fits", "Fits"), ROT(".rot", "AsciiCassis"), BAS(".bas",
			"Class"), LIS(".lis", "AsciiCassis"), PDF(".pdf", "Pdf"), PNG(".png", "Png"), SPEC(".spec", "SimpleData"), VOTABLE(
			".votable", "VOTable"), XML(".xml", "VOTable");

	private final String extensionName;
	private final String methodeName;

	private static TreeMap<String, EXTENSION> map = getByExtension();
	private static TreeMap<String, String> map2 = getMehodeByExt();


	private EXTENSION(String extName, String methodeName) {
		this.extensionName = extName;
		this.methodeName = methodeName;
	}

	private static TreeMap<String, String> getMehodeByExt() {
		TreeMap<String, String> localMap = new TreeMap<String, String>();

		for (EXTENSION ext : EXTENSION.values()) {
			localMap.put(ext.extensionName, ext.methodeName);
		}
		return localMap;

	}

	public static EXTENSION getExtensionByExtensionName(String extensionName) {
		for (EXTENSION ext : EXTENSION.values()) {
			if (ext.extensionName.equals(extensionName)) {
				return ext;
			}
		}
		return null;
	}

	private static TreeMap<String, EXTENSION> getByExtension() {
		TreeMap<String, EXTENSION> localMap = new TreeMap<String, EXTENSION>();

		for (EXTENSION ext : EXTENSION.values()) {
			localMap.put(ext.extensionName, ext);
		}
		return localMap;
	}

	/**
	 *
	 * @param ext
	 *            extension ie: ".png"
	 * @return if the extension exists
	 */
	public static boolean exists(String ext) {
		return map.containsKey(ext);
	}

	public static InterfaceFileManager getInstanceFileManager(String ext, File file) throws NoSuchMethodException,
			ClassNotFoundException, IllegalArgumentException, InstantiationException, IllegalAccessException,
			InvocationTargetException {
		@SuppressWarnings("rawtypes")
		Class[] args = { File.class };

		String packageTemp = InterfaceFileManager.class.getPackage().getName();
		Class<?> cl = Class.forName(packageTemp + "." + "FileManager" + map2.get(ext));

		@SuppressWarnings("rawtypes")
		Constructor constructor = cl.getConstructor(args);

		return (InterfaceFileManager) constructor.newInstance(file);
	}
}
