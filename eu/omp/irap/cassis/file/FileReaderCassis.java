/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.file.util.GzipUtil;

/**
 * @author glorian
 */
public class FileReaderCassis {

	private InterfaceFileManager fileManager;

	public InterfaceFileManager getFileManager() {
		return fileManager;
	}

	/**
	 * Create a CassisSpectrum list from file.
	 *
	 * @param file
	 *            The file to transform in CassisSpectrum List
	 * @return the CassisSpectrum list
	 */
	public List<CassisSpectrum> createCassisSpectrumListFromFile(File file) {
		List<CassisSpectrum> cassisSpectrumList = null;
		try {
			String ext = file.getName().substring(file.getName().lastIndexOf(".")).toLowerCase();
			fileManager = EXTENSION.getInstanceFileManager(ext, file);
			cassisSpectrumList = fileManager.readAll();
		} catch (Exception e) {
			List<CassisSpectrum> spectrums = FileReaderCassis2.tryAllReader(file,  7,
					new ArrayList<CassisMetadata>());
			fileManager = FileReaderCassis2.fileManager;
			return spectrums;
		}
		return cassisSpectrumList;
	}

	/**
	 * Create a CassisSpectrum list from file and an additional metadata list.
	 *
	 * @param file The file to transform in CassisSpectrum list.
	 * @param additionalMetadata The additional metadata list.
	 * @return the CassisSpectrum list.
	 */
	public List<CassisSpectrum> createCassisSpectrumListFromFile(File file, List<CassisMetadata> additionalMetadata) {
		List<CassisSpectrum> cassisSpectrumList = null;
		try {
			String ext = file.getName().substring(file.getName().lastIndexOf(".")).toLowerCase();
			fileManager = EXTENSION.getInstanceFileManager(ext, file);
			fileManager.setAdditionalMetadataList(additionalMetadata);
			cassisSpectrumList = fileManager.readAll();
		} catch (Exception e) {
			List<CassisSpectrum> spectrums = FileReaderCassis2.tryAllReader(file, 7, additionalMetadata);
			fileManager = FileReaderCassis2.fileManager;
			return spectrums;
		}
		return cassisSpectrumList;
	}

	/**
	 * Create a CassisSpectrum from file.
	 *
	 * @param file
	 *            The file to transform in CassisSpectrum
	 * @return cassis spectrum merged
	 */
	public static CassisSpectrum createCassisSpectrumFromFile(File file) {
		InterfaceFileManager fileManager;
		List<CassisSpectrum> cassisSpectrumList = null;
		try {
			String ext = file.getName().substring(file.getName().lastIndexOf(".")).toLowerCase();
			fileManager = EXTENSION.getInstanceFileManager(ext, file);
			cassisSpectrumList = fileManager.readAll();
		} catch (Exception e) {
			return CassisSpectrum.mergeCassisSpectrumList(FileReaderCassis2.tryAllReader(file, 7,
					new ArrayList<CassisMetadata>()));
		}
		return CassisSpectrum.mergeCassisSpectrumList(cassisSpectrumList);
	}

	public static class FileReaderCassis2 {
		static InterfaceFileManager fileManager;

//		static List<CassisSpectrum> tryAllReader(final File file, int nbReader,
//				) {
//			List<CassisSpectrum> cassisSpectrumList = new ArrayList<CassisSpectrum>();
//			nbReader--;
//			try {
//				switch (nbReader) {
//				case 6:
//					fileManager = new FileManagerClass(file, false);
//					break;
//				case 5:
//					fileManager = new FileManagerFits(file, false);
//					fileManager.setAdditionalMetadataList(additionalMetadatas);
//					break;
//				case 4:
//					fileManager = new FileManagerVOTable(file, false);
//					fileManager.setAdditionalMetadataList(additionalMetadatas);
//					break;
//				case 3:
//					fileManager = new FileManagerAscii(file, false);
//					break;
//				case 2:
//					fileManager = new FileManagerAsciiCassis(file, false);
//					break;
//				case 1:
//					fileManager = new FileManagerSimpleData(file, false);
//					break;
//				default:
//					return null;
//				}
//				cassisSpectrumList = fileManager.readAll();
//				if (cassisSpectrumList == null || cassisSpectrumList.isEmpty()) {
//					cassisSpectrumList = tryAllReader(file, nbReader);
//				}
//			} catch (Exception e2) {
//				cassisSpectrumList = tryAllReader(file, nbReader);
//			}
//			return cassisSpectrumList;
//		}

		static List<CassisSpectrum> tryAllReader(final File file, int nbReader, List<CassisMetadata> additionalMetadatas) {
			List<CassisSpectrum> cassisSpectrumList = new ArrayList<CassisSpectrum>();
			nbReader--;
			try {
				switch (nbReader) {
				case 6:
					fileManager = new FileManagerClass(file, false);
					break;
				case 5:
					fileManager = new FileManagerFits(file, false);
					fileManager.setAdditionalMetadataList(additionalMetadatas);
					break;
				case 4:
					fileManager = new FileManagerVOTable(file, false);
					fileManager.setAdditionalMetadataList(additionalMetadatas);
					break;
				case 3:
					fileManager = new FileManagerAscii(file, false);
					break;
				case 2:
					fileManager = new FileManagerAsciiCassis(file, false);
					break;
				case 1:
					fileManager = new FileManagerSimpleData(file, false);
					break;
				default:
					// Check if the file is a GNU Zip file.
					if (file != null && file.getAbsolutePath().endsWith(".gz")) {
						File fileout = new File(file.getAbsolutePath().substring(0,
								file.getAbsolutePath().length() - 4));
						if (GzipUtil.decompressFile(file, fileout)) {
							return tryAllReader(fileout, 7, additionalMetadatas);
						}
					}
					return null;
				}
				cassisSpectrumList = fileManager.readAll();
				if (cassisSpectrumList == null || cassisSpectrumList.isEmpty()) {
					cassisSpectrumList = tryAllReader(file, nbReader, additionalMetadatas);
				}
			} catch (Exception e2) {
				cassisSpectrumList = tryAllReader(file, nbReader, additionalMetadatas);
			}
			return cassisSpectrumList;
		}
	}
}
