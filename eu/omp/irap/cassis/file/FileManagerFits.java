/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.file;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eso.fits.cassis.Fits;
import org.eso.fits.cassis.FitsColumn;
import org.eso.fits.cassis.FitsData;
import org.eso.fits.cassis.FitsException;
import org.eso.fits.cassis.FitsFile;
import org.eso.fits.cassis.FitsHDUnit;
import org.eso.fits.cassis.FitsHeader;
import org.eso.fits.cassis.FitsKeyword;
import org.eso.fits.cassis.FitsMatrix;
import org.eso.fits.cassis.FitsTable;

import eu.omp.irap.cassis.common.CassisMetadata;
import eu.omp.irap.cassis.common.CassisSpectrum;
import eu.omp.irap.cassis.common.CommentedSpectrum;
import eu.omp.irap.cassis.common.MultiScanCassisSpectrum;
import eu.omp.irap.cassis.common.TypeFrequency;
import eu.omp.irap.cassis.common.axes.UNIT;
import eu.omp.irap.cassis.common.axes.XAxisCassis;
import eu.omp.irap.cassis.common.axes.XAxisFrequency;
import eu.omp.irap.cassis.common.axes.XAxisVelocity;
import eu.omp.irap.cassis.common.axes.YAxisCassis;
import eu.omp.irap.cassis.file.fits.util.FileFitsUtil;
import eu.omp.irap.cassis.file.fits.util.FitsCassisMetaData;
import eu.omp.irap.cassis.file.fits.util.Tree;
import eu.omp.irap.cassis.file.gui.ColumnInformation;
import eu.omp.irap.cassis.file.gui.ColumnsDetected;
import eu.omp.irap.cassis.file.util.FilenameUtils;
import uk.ac.starlink.fits.FitsTableWriter;
import uk.ac.starlink.table.ColumnInfo;
import uk.ac.starlink.table.RandomStarTable;
import uk.ac.starlink.table.StarTable;
import uk.ac.starlink.table.StarTableOutput;

/**
 * @author Lea Foissac, JM Glorian
 */
public final class FileManagerFits extends FileManager implements SaveSpectrumInterface {

	private FitsFile fitsFile;

	private List<CassisSpectrum> cassisSpectrumList;

	private List<ArrayList<CassisMetadata>> metadatasAllHdu;
	private List<ArrayList<CassisMetadata>> metadatasSpectrum;

	private int nbHdu;

	private List<Integer> indexOfHduToKeep;
	private List<Integer> indexOfHduToIgnore;
	private List<Integer> indexOfHduToKeepForSpectrum;

	private Tree<Integer> finalTree;

	private List<ColumnsDetected> xColumnsDetected;
	private List<ColumnsDetected> yColumnsDetected;


	/**
	 * Construct a FileManagerFits with exceptions displayed.
	 *
	 * @param file The input File.
	 */
	public FileManagerFits(File file) {
		this(file, true);
	}

	/**
	 * Construct a FileManagerFits.
	 *
	 * @param file The input File
	 * @param displayException If the exceptions should be displayed.
	 */
	public FileManagerFits(File file, boolean displayException) {
		super(file, displayException);
	}

	@Override
	public CassisSpectrum read() {
		List<CassisSpectrum> readAll = readAll();
		return CassisSpectrum.mergeCassisSpectrumList(readAll);
	}

	@Override
	public List<CassisSpectrum> readAll() {
		if (this.cassisSpectrumList == null) {
			try {
				openFile();
				this.cassisSpectrumList = new ArrayList<CassisSpectrum>();
				if (this.fitsFile != null && readNbCassisSpectra() > 0) {
					List<ArrayList<CassisMetadata>> metadatasSpectrum = geMetadatasSpectrum();
					int index = 0;
					CassisSpectrum cassisSpectrum = null;

					for (int i = 0; i < getNbHdu(); i++) {
						if (getIndexOfHduToKeepForSpectrum().contains(i)) {
							List<CassisMetadata> metaDatas = metadatasSpectrum.get(index);
							FitsHDUnit hdu = this.fitsFile.getHDUnit(i);

							if (isWbsSpectrum(metaDatas, hdu.getData())){
								cassisSpectrum = readWbsSpectrum(fitsFile.getName(),hdu.getData(), metaDatas);
							}
//							else if (isHifiSpectrumDataset(metaDatas)) {
//								cassisSpectrum = readHifiSpectrumDataSet(metaDatas, hdu.getData(), this.fitsFile.getName());
//							}
							else if (! isGildas(metaDatas)){
								cassisSpectrum = readStandardOv(metaDatas,hdu.getData(),
										FilenameUtils.getBaseName(fitsFile.getName()), index);
							} else {
								FitsData dm = hdu.getData();
								FitsCassisMetaData fitsCassisMetaData = new FitsCassisMetaData(metaDatas, displayException);
								cassisSpectrum = readGildasFile(dm, fitsCassisMetaData);
							}

							if (cassisSpectrum != null) {
								cassisSpectrum.setOriginalMetadataList(metaDatas);
								CassisMetadata originalMetadata = cassisSpectrum.getOriginalMetadata("fileName");
								if (originalMetadata != null){
									cassisSpectrum.setTitle(originalMetadata.getValue());
								}
								if (hdu != null && hdu.getHeader() != null){
									String name = hdu.getHeader().getName();
									if (name != null && ! name.isEmpty()){
										if ("NONE".equals(name) && i==0){
											name = "Primary";
										}
										cassisSpectrum.addOriginalMetadata(
												new CassisMetadata(CassisMetadata.HDU_NAME, name, "", ""), false);
									}
								}
								cassisSpectrum.addCassisMetadata(new CassisMetadata(CassisMetadata.FILE_TYPE,
										FileManager.FileType.FITS.name(), null, null), true);
								this.cassisSpectrumList.add(cassisSpectrum);
								cassisSpectrum = null;
							}
							index++;
						}
					}
				}
			} finally {
				if (this.fitsFile != null) {
					this.fitsFile.closeFile();
				}
			}
		}
		return this.cassisSpectrumList;
	}

	private CassisSpectrum readGildasFile(FitsData dm, FitsCassisMetaData fitsCassisMetaData) {
		CassisSpectrum cassisSpectrum;
		double restFrequency = fitsCassisMetaData.getRestFrequency();
		double imageFreq = fitsCassisMetaData.getImageFreq();
		int numXAxis = fitsCassisMetaData.getXIndex();
		CassisSpectrum spectrum = null;

		if (fitsCassisMetaData.getType() == Fits.IMAGE && fitsCassisMetaData.isGildasFile()) {

			int[] naxis = dm.getNaxis();

			if (!(naxis == null || naxis.length == 0) && naxis.length > numXAxis) {
				final int nbPoints = naxis[numXAxis];
				float[] data = new float[nbPoints];
				try {
					((FitsMatrix) dm).getFloatValues(numXAxis, nbPoints, data);
				} catch (Exception e ) {
					e.printStackTrace();
					spectrum = null;
				}
				double[] signalFrequencies = new double[data.length];
				double[] intensities = new double[data.length];

				for (int cpt = 0; cpt < data.length; cpt++) {
					signalFrequencies[cpt] = (restFrequency + fitsCassisMetaData.getCdelta() * (cpt + 1 - fitsCassisMetaData.getCrPix())) / 1E6;
					intensities[cpt] = data[cpt];
				}
				spectrum = CassisSpectrum.generateCassisSpectrum(fitsCassisMetaData.getTitle(),
						fitsCassisMetaData.getBlanckValue() * fitsCassisMetaData.getBScale() + fitsCassisMetaData.getBZero(),
						signalFrequencies, intensities, 0.0, fitsCassisMetaData.getVlsrValue(),
						XAxisCassis.getXAxisFrequency(), YAxisCassis.getYAxisKelvin());
				double loFrequency = 0.;
				if (imageFreq != 0.)
					loFrequency = (restFrequency + (imageFreq - restFrequency) / 2.0) / 1E6;
				spectrum.setLoFrequency(loFrequency);

				spectrum.addCassisMetadata(new CassisMetadata(
							CassisMetadata.FITS_TYPE, "readCassisSpectrumOtherFits", "from gildas", ""), true);
			}
		}
		if (spectrum != null) {
			if (!fitsCassisMetaData.isFoundXUnit() ||
					UNIT.toUnit(fitsCassisMetaData.getXUnit()) == UNIT.UNKNOWN) {
				spectrum.setxError(true);
			}
			if (!fitsCassisMetaData.isFoundYUnit()) {
				spectrum.setyError(true);
			}
		}
		cassisSpectrum = spectrum;
		return cassisSpectrum;
	}

	public static boolean isGildas(List<CassisMetadata> metaDatas) {
		boolean res = false;
		CassisMetadata cassisMetadata = CassisMetadata.getCassisMetadata("ORIGIN", metaDatas);
		if (cassisMetadata != null && cassisMetadata.getValue()!= null){
			res = cassisMetadata.getValue().trim().equals("CLASS-Grenoble");
		}
		return res;
	}

	private CassisSpectrum readWbsSpectrum(String name, FitsData data, List<CassisMetadata> metaDatas) {
		CassisMetadata waveNameMetaData = CassisMetadata.getCassisMetadata("wavename", metaDatas);
		String nameWaveColumn = waveNameMetaData.getValue().trim();
		final String nameFluxColumn = "flux";
		CassisMetadata channelsMetadata = CassisMetadata.getCassisMetadata("channels", metaDatas);
		int nbPoints = Integer.parseInt(channelsMetadata.getValue().trim());

		CassisMetadata loFrequencyMetadata = CassisMetadata.getCassisMetadata("loFrequency", metaDatas);
		double loFrequency = Double.parseDouble(loFrequencyMetadata.getValue());
		UNIT loFrequencyUnit =  UNIT.toUnit(loFrequencyMetadata.getUnit());
		FitsTable fitsTable = (FitsTable) data;

		double[] waves = new double[nbPoints];
		double[] flux = new double[nbPoints];
		int nbSideBand = 4;
		int cpt = 0;

		XAxisCassis waveAxis = XAxisCassis.getXAxisCassis(fitsTable.getColumn(nameWaveColumn+"_"+1).getUnit());
		YAxisCassis fluxAxis = YAxisCassis.getYAxisCassis(fitsTable.getColumn(nameFluxColumn+"_"+1).getUnit());

		//Can be LoFrequency&
		FitsColumn loFrequencyColumn = fitsTable.getColumn("LoFrequency");
		if (loFrequencyColumn != null){
			loFrequency = loFrequencyColumn.getReal(0);
		}
		for (int i = 1; i < nbSideBand+1; i++) {
			FitsColumn waveColumn = fitsTable.getColumn(nameWaveColumn+"_"+i);
			double[] waveTemp = waveColumn.getReals(0);
			FitsColumn fluxColumn = fitsTable.getColumn(nameFluxColumn+"_"+i);
			double[] fluxTemp = fluxColumn.getReals(0);



			if (!waveAxis.isInverted()){
				for (int j = 0; j < waveTemp.length; j++) {
					waves[cpt+j] = waveAxis.convertToMHzFreq(waveTemp[j]);
					flux[cpt+j] = fluxTemp[j];
				}
			}
			cpt= cpt + fluxTemp.length;
		}

		CassisSpectrum cassisSpectrum =
				CassisSpectrum.generateCassisSpectrum(name, waves, flux, 0.0, waveAxis,fluxAxis);

		cassisSpectrum.setTypeFrequency(TypeFrequency.SKY);
		cassisSpectrum.setLoFrequency(XAxisCassis.getXAxisCassis(loFrequencyUnit).convertToMHzFreq(loFrequency));

		double vlsr = getVlsr(metaDatas);
		if (vlsr != 0.0 && !Double.isNaN(vlsr)) {
			cassisSpectrum.addOriginalMetadata(
					new CassisMetadata("skyVlsr", String.valueOf(vlsr),
							"VLSR given by HIPE, not used as SKY frequencies.", null),
					true);
		}
		if (cassisSpectrum != null){
			cassisSpectrum.addCassisMetadata(new CassisMetadata(CassisMetadata.FITS_TYPE, "WBS Herschel", "", ""), true);
		}
		return cassisSpectrum;
	}

	/**
	 * Check if the list of metadata and the data identify a WbsSPectrum
	 * @param metaDatas
	 * @param fitsData
	 * @return a Wbs spectrum has been identified
	 */
	public static boolean isWbsSpectrum(List<CassisMetadata> metaDatas, FitsData fitsData) {
		boolean res = CassisMetadata.isIn(metaDatas, "instrument", "HIFI") &&
				CassisMetadata.isIn(metaDatas, "description", "WBS Spectrum Dataset");
		CassisMetadata waveNameMetaData = CassisMetadata.getCassisMetadata("wavename", metaDatas);
		if (waveNameMetaData == null ||  !(fitsData instanceof FitsTable)){
			res = false;
		}else{
			String nameWaveColumn = waveNameMetaData.getValue().trim();
			FitsTable fitsTable = (FitsTable) fitsData;
			FitsColumn firstWaveColumn = fitsTable.getColumn(nameWaveColumn+"_"+1);
			if (firstWaveColumn == null){
				res = false;
			}
		}

		return res;
	}

	@Override
	public CassisSpectrum read(int spectrumIndex) {
		return readAll().get(spectrumIndex);
	}

	@Override
	public List<CassisMetadata> readCassisMetadata(int spectrumIndex) {
		return geMetadatasSpectrum().get(spectrumIndex);
	}

	@Override
	public List<CassisMetadata> getCommonCassisMetadataList() {
		List<CassisMetadata> finalCassisMetadataList = null;
		List<CassisMetadata> cassisMetadataList = null;
		List<ArrayList<CassisMetadata>> hduList = getAllHduList();
		Tree<Integer> finalTree = getFinalTree();
		if (finalTree.getNbChildHead() == 1) {
			finalCassisMetadataList = new ArrayList<CassisMetadata>();
			int i = finalTree.getValueOfChildHead(0);
			cassisMetadataList = hduList.get(i);
			cassisMetadataList = FileFitsUtil.changeHierarch(cassisMetadataList);
			for (CassisMetadata cassisMetadata : cassisMetadataList) {
				if (!cassisMetadata.getName().contains("DS_") && !cassisMetadata.getName().contains("DSETS_")) {
					try {
						CassisMetadata cassisMetadataClone = cassisMetadata.clone();
						finalCassisMetadataList.add(cassisMetadataClone);
					} catch (CloneNotSupportedException e) {
						if (displayException) {
							e.printStackTrace();
						}
					}
				}
			}
			finalCassisMetadataList = extractCommentFromCassisMetadataList(cassisMetadataList);
		}
		return finalCassisMetadataList;
	}

	@Override
	public boolean save(File file, CommentedSpectrum commentedSpectrum) {
		StarTableOutput sto = new StarTableOutput();

		StarTable table = getStarTableToSave(commentedSpectrum);

		try {
			FitsTableWriter voWriter = new FitsTableWriter();
			voWriter.writeStarTable(table, file.getAbsolutePath(), sto);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		FitsFile fitsFile = null;
		try {
			fitsFile = new FitsFile(file);
			fitsFile.getHDUnit(1).getHeader().addKeyword(new FitsKeyword("vlsr",
					commentedSpectrum.getVlsr(), "[km s-1]" + " Velocity of reference channel"));
			final double loFreq = commentedSpectrum.getLoFreq();
			if (!Double.isNaN(loFreq)) {
				fitsFile.getHDUnit(1).getHeader().addKeyword(new FitsKeyword("LOFREQ",
						XAxisCassis.getXAxisCassis(UNIT.GHZ).convertFromMhzFreq(loFreq), "[GHz]" + " Frequency of the LO"));
			}

			if (commentedSpectrum.getCassisMetadataList() != null) {
				for (CassisMetadata metadata : commentedSpectrum.getCassisMetadataList()) {

					String unit = "["+metadata.getUnit()+"]";
					String comment2 = metadata.getComment();
					if (metadata.getUnit() == null) {
						unit= "";
					}
					if (comment2 == null) {
						comment2 = "";
					}
					String value = metadata.getValue();
					if (value == null) {
						value = "";
					}
					fitsFile.getHDUnit(1).getHeader().addKeyword(
							new FitsKeyword(metadata.getName(), value, unit+comment2));
				}
			}
			fitsFile.saveFile();
		} catch (IOException | FitsException e) {
			e.printStackTrace();
		} finally {
			if (fitsFile != null) {
				fitsFile.closeFile();
			}
		}
		return true;
	}

	private static StarTable getStarTableToSave(final CommentedSpectrum commentedSpectrum) {
		return new RandomStarTable() {

			@Override
			public String getName() {
				return commentedSpectrum.getTitle();
			}

			ColumnInfo[] colInfos_ = new ColumnInfo[2];
			{
				ColumnInfo waveInfo = new ColumnInfo("wave", Double.class, "wave co-ordinate");
				waveInfo.setUtype("spec:Spectrum.Data.SpectralAxis.Value");
				waveInfo.setUnitString("MHz");
				colInfos_[0] = waveInfo;

				ColumnInfo fluxInfo = new ColumnInfo("flux", Double.class, "flux co-ordinate");
				fluxInfo.setUtype("spec:Spectrum.Data.FluxAxis.value");
				fluxInfo.setUnitString("K");
				colInfos_[1] = fluxInfo;
			}

			@Override
			public ColumnInfo getColumnInfo(int arg0) {
				return colInfos_[arg0];
			}

			@Override
			public int getColumnCount() {
				return 2;
			}

			@Override
			public long getRowCount() {
				return commentedSpectrum.getSize();
			}

			public Object getCell(long lrow, int icol) {
				int irow = checkedLongToInt(lrow);
				switch (icol) {
				case 0:
					return commentedSpectrum.getFrequenciesSignal()[irow];
				case 1:
					return commentedSpectrum.getIntensities()[irow];
				default:
					throw new IllegalArgumentException();
				}
			}
		};
	}

	@Override
	public FileType getType() {
		return FileType.FITS;
	}

	@Override
	public CassisMetadata getCassisMetadata(String name, int spectrumIndex) {
		CassisMetadata finalCassisMetadata = null;
		ArrayList<CassisMetadata> cassisMetadataList = geMetadatasSpectrum().get(spectrumIndex);
		for (CassisMetadata cassisMetadata : cassisMetadataList) {
			if (cassisMetadata.getName().equalsIgnoreCase(name)) {
				finalCassisMetadata = cassisMetadata;
			}
		}
		return finalCassisMetadata;
	}

	@Override
	public int readNbCassisSpectra() {
		return getIndexOfHduToKeepForSpectrum().size();
	}

//
//	public static boolean isHifiSpectrumDataset(List<CassisMetadata> metaDatas) {
//		return CassisMetadata.getCassisMetadata("TYPE", metaDatas) != null && CassisMetadata.getCassisMetadata("TYPE", metaDatas).getValue().contains("HifiSpectrumDataset") &&
//				CassisMetadata.getCassisMetadata("WAVEUNIT", metaDatas) != null &&
//			CassisMetadata.getCassisMetadata("CLASS___", metaDatas) != null && !CassisMetadata.getCassisMetadata("CLASS___", metaDatas).getValue().contains("Spectrum1d");
//	}


//	/**
//	 * This method make the same thing that the other method readHifiSpectrumDataSet
//	 * but this method use the Jfits library
//	 *
//	 * @param cassisMetadataList
//	 *            The metadata list
//	 * @param dm
//	 *            The data of the fits T
//	 * @param name
//	 * 			  The name of the CassisSpectrum
//	 *
//	 * @return a CassisSpectrum
//	 */
//	private CassisSpectrum readHifiSpectrumDataSet(List<CassisMetadata> cassisMetadataList,
//			FitsData dm, String name) {
//
//		/* extract the name of the wave column and the number of points  from the metas */
//		int nbPoints = 0;
//		String nameWaveColumn = "";
//		for (CassisMetadata cassisMetadata : cassisMetadataList) {
//			if (nbPoints == 0 || nameWaveColumn.isEmpty()) {
//				if (cassisMetadata.getName().equals("channels") ||
//					cassisMetadata.getComment().contains("Number of channels")) {
//					nbPoints = Integer.parseInt(cassisMetadata.getValue());
//				} else if (cassisMetadata.getName().equals("wavename") ||
//						   cassisMetadata.getComment().contains("Actual name of the WaveColumn")) {
//					nameWaveColumn = cassisMetadata.getValue();
//				}
//			}
//		}
//
//		/* extract the XAxis from the metas */
//		XAxisCassis xAxisCassis = XAxisCassis.getXAxisCassis(
//				CassisMetadata.getCassisMetadata("WAVEUNIT", cassisMetadataList).getValue());
//
//		double loFrequency = 0;
//		final int maxSideBand = 4;
//		double[] signalFrequencies = new double[nbPoints * maxSideBand];
//		double[] intensities = new double[nbPoints * maxSideBand];
//		int cpt = 0;
//		int[] naxisTab = dm.getNaxis();
//		boolean find = false;
//
//		if (!xAxisCassis.isInverted()) {
//			for (int i = 0; i < maxSideBand; i++) {
//				for (int j = 0; j < naxisTab.length; j++) {
//					FitsColumn column = ((FitsTable) dm).getColumn(j);
//					double[] row = column.getReals(j);
//					if (column.getLabel().equals(nameWaveColumn + "_" + (i + 1))) {
//						double[] freqTabSubBand = row;
//						for (int k = 0; k < freqTabSubBand.length; k++) {
//							signalFrequencies[cpt] = xAxisCassis.convertToMHzFreq(freqTabSubBand[k]);
//							cpt++;
//						}
//						find = true;
//					} else if (column.getLabel().equals("flux" + "_" + (i + 1))) {
//						double[] intensitiesTabSubBand = row;
//						int l = cpt;
//						for (int m = 0; m < intensitiesTabSubBand.length; m++) {
//							intensities[l] = intensitiesTabSubBand[m];// intensity
//							l++;
//						}
//						find = true;
//					} else if (column.getLabel().equals("LoFrequency&")) {
//						loFrequency = row[j]; //lofreq same for all the column
//						find = true;
//					}
//				}
//			}
//			if (!find) {
//				return null;
//			}
//		}
//		signalFrequencies = Arrays.copyOf(signalFrequencies, cpt);
//		intensities = Arrays.copyOf(intensities, cpt);
//		CassisSpectrum cassisSpectrum =
//				CassisSpectrum.generateCassisSpectrum(name, signalFrequencies, intensities, 0.0,
//									XAxisCassis.getXAxisFrequency(),
//									YAxisCassis.getYAxisKelvin());
//
//		cassisSpectrum.setTypeFrequency(TypeFrequency.SKY);
//		cassisSpectrum.setLoFrequency(xAxisCassis.convertToMHzFreq(loFrequency));
//
//		double vlsr = getVlsr(cassisMetadataList);
//		if (vlsr != 0.0 && !Double.isNaN(vlsr)) {
//			cassisSpectrum.addOriginalMetadata(
//					new CassisMetadata("skyVlsr", String.valueOf(vlsr),
//							"VLSR given by HIPE, not used as SKY frequencies.", null),
//					true);
//		}
//		if (cassisSpectrum != null){
//			cassisSpectrum.addCassisMetadata(new CassisMetadata(CassisMetadata.FITS_TYPE, "Hifi Spectrum Dataset", "", ""), true);
//		}
//		return cassisSpectrum;
//	}

	/**
	 * This method make the same thing that the other method readStandardOv but this method
	 * use the Jfits library
	 *
	 * @param cassisMetadataList
	 *            The metadata list
	 * @param hdu
	 *            The HDU
	 * @param fitsFile
	 *            The FitsFile
	 * @param index
	 *            Index of the spectrum in hduToKeep
	 * @return CassisSpectrum
	 * @throws Exception
	 *             if FreqMin = FreqMax
	 */
	private CassisSpectrum readStandardOv(List<CassisMetadata> cassisMetadataList,
			FitsData dm, String baseName, int index) {

		CassisSpectrum res = null;
		double vlsr = getVlsr(cassisMetadataList);
		if (Double.isNaN(vlsr)) {
			vlsr = 0.0;
		}
		double loFrequency = getLoFreq(cassisMetadataList);
		if (Double.isNaN(loFrequency)) {
			loFrequency = 0.0;
		}

		if (dm.getType() == Fits.IMAGE){
			res = readStandardFitsImage(cassisMetadataList, vlsr, baseName, (FitsMatrix)dm);

		} else {
			res = readStandardFitsTable(cassisMetadataList, vlsr, loFrequency, baseName, (FitsTable) dm, index);
		}
		return res;
	}

	private CassisSpectrum readStandardFitsTable(List<CassisMetadata> cassisMetadataList,
			double vlsr, double loFrequency, final String baseName, final FitsTable fitsTable, int index) {
		CassisSpectrum res;
		cassisMetadataList.addAll(additionalMetadataList);
		ColumnsDetected waveColumnsDetected = getWaveColumnsDetected().get(index);
		ColumnsDetected fluxColumnsDetected = getFluxColumnsDetected().get(index);

		if (waveColumnsDetected == null ||
			fluxColumnsDetected	== null){
				return null;
		}

		ColumnInformation waveColumn = waveColumnsDetected.getColumn(waveColumnsDetected.getColumnUsed());
		ColumnInformation fluxColumn = fluxColumnsDetected.getColumn(fluxColumnsDetected.getColumnUsed());


		final XAxisCassis xAxisCassis = waveColumn.getXAxisCassis();
		final YAxisCassis yAxisCassis = fluxColumn.getYAxisCassis();

		if (isHifiSpectrum(cassisMetadataList)) {
			res = getCassisSpectrum(fitsTable, 0.0, xAxisCassis, yAxisCassis,
					waveColumn.getIndex(),
					fluxColumn.getIndex(), baseName, index);
			res.setTypeFrequency(TypeFrequency.SKY);
			res.addOriginalMetadata(new CassisMetadata(
					"skyVlsr", String.valueOf(vlsr), "VLSR given by HIPE, not used as SKY frequencies.", null),true);
		} else {
			res = getCassisSpectrum(fitsTable, vlsr, xAxisCassis, yAxisCassis, waveColumn.getIndex(),
					fluxColumn.getIndex(), baseName, index);
		}

		if (res != null) {
			if (waveColumn.isErrorUnit() || UNIT.toUnit(waveColumn.getUnitString()) == UNIT.UNKNOWN) {
				res.setxError(true);
			}
			res.setyError(fluxColumn.isErrorUnit());
			if (loFrequency != 0)
				res.setLoFrequency(loFrequency);
			if (res.getFreqMin().equals(res.getFreqMax()))
				res = null;

		}
		if (res != null){
			res.addCassisMetadata(new CassisMetadata(CassisMetadata.FITS_TYPE, "readStandardOv FitsTable", "", ""), true);
		}
		return res;
	}

	/**
	 *
	 * @param cassisMetadataList
	 * @param vlsr
	 * @param baseName
	 * @param dm
	 * @return
	 */
	private CassisSpectrum readStandardFitsImage(List<CassisMetadata> cassisMetadataList, double vlsr,
			final String baseName, FitsMatrix matrix) {
		CassisSpectrum res =null;
		cassisMetadataList.addAll(additionalMetadataList);
		FitsCassisMetaData fitsCassisMetaData = new FitsCassisMetaData(cassisMetadataList, displayException);

		int nbPoints = matrix.getNaxis()[fitsCassisMetaData.getXIndex()];
		int nbSpectra = matrix.getNaxis()[fitsCassisMetaData.getYIndex(matrix.getNaxis())];
		if (matrix.getNaxis().length < 2){
			nbSpectra = 1;
		}
		if (matrix.getNoValues() == nbPoints){
			nbSpectra = 1;
		}


		double[] signalFrequencies = fitsCassisMetaData.computeFrequency(nbPoints);
		boolean needReverse = signalFrequencies[0] > signalFrequencies[signalFrequencies.length - 1];
		if (needReverse) {
			signalFrequencies = reverse(signalFrequencies);
		}
		double[] intensities = new double[nbPoints];


		try {

			if (nbSpectra > 1) {
				double[][] arrayValuesSubScans = new double[nbSpectra][nbPoints];
				for (int i = 0; i < nbSpectra; i++) {
					double[] computeIntensities = fitsCassisMetaData.computeIntensities(
																matrix.getFloatValues(i * nbPoints, nbPoints, null));
					if (needReverse) {
						computeIntensities = reverse(computeIntensities);
					}
					for (int j = 0; j < nbPoints; j++) {
						arrayValuesSubScans[i][j] = computeIntensities[j];
					}
				}
				res =  MultiScanCassisSpectrum.createMultiScanCassisSpectrum(
						baseName, signalFrequencies, arrayValuesSubScans,
						vlsr, fitsCassisMetaData.getXAxisCassis(), fitsCassisMetaData.getYAxisCassis());
				res.addCassisMetadata(new CassisMetadata(CassisMetadata.FITS_TYPE, "MultiScanCassisSpectrum", "", ""), true);
			} else {
				double[] computeIntensities = fitsCassisMetaData.computeIntensities(

						matrix.getFloatValues(0, nbPoints, null));
				if (needReverse) {
					computeIntensities = reverse(computeIntensities);
				}
				for (int j = 0; j < nbPoints; j++) {
					intensities[j] = computeIntensities[j];
				}

				res = CassisSpectrum.generateCassisSpectrum(baseName,
						signalFrequencies, intensities, vlsr,
						fitsCassisMetaData.getXAxisCassis(), fitsCassisMetaData.getYAxisCassis());
				if (fitsCassisMetaData.getXAxisCassis() instanceof XAxisVelocity){
					res.setxAxisOrigin(XAxisFrequency.getXAxisFrequency(UNIT.MHZ));
				}
				res.addCassisMetadata(new CassisMetadata(CassisMetadata.FITS_TYPE, "readStandardOv Image", "", ""), true);

			}
		} catch (FitsException e) {
			res= null;
			e.printStackTrace();
		}
		return res;
	}


	private double[] reverse(double[] array) {
		for (int i = 0; i < array.length / 2; i++) {
			double tmp = array[i];
			array[i] = array[array.length - i - 1];
			array[array.length - i - 1] = tmp;
		}
		return array;
	}


	/**
	 * Search and return the LO Frequency (in MHz). This method search in order
	 *  of priority the keywords "loFreqAvg", "LoFrequency", "loFrequency",
	 *  "lofrequency" regardless of case then LOFREQ.
	 *
	 * @param cassisMetadataList The metadata list
	 * @return The LO Frequency according to a metadata list, or Double.NaN if
	 *  not found.
	 */
	private double getLoFreq(List<CassisMetadata> cassisMetadataList) {
		double loFreq = Double.NaN;
		if (CassisMetadata.isMetadataNameCase("loFreqAvg", cassisMetadataList)) {
			CassisMetadata loFreqMeta = CassisMetadata.getCassisMetadataCase("loFreqAvg", cassisMetadataList);
			loFreq = Double.valueOf(loFreqMeta.getValue());
			loFreq = XAxisCassis.getXAxisCassis(loFreqMeta.getUnit()).convertToMHzFreq(loFreq);
		} else if (CassisMetadata.isMetadataNameCase("LoFrequency", cassisMetadataList)) {
			CassisMetadata loFreqMeta = CassisMetadata.getCassisMetadataCase("LoFrequency", cassisMetadataList);
			loFreq = Double.valueOf(loFreqMeta.getValue());
			loFreq = XAxisCassis.getXAxisCassis(loFreqMeta.getUnit()).convertToMHzFreq(loFreq);
		} else if (CassisMetadata.isMetadataNameCase("loFrequency", cassisMetadataList)) {
			CassisMetadata loFreqMeta = CassisMetadata.getCassisMetadataCase("loFrequency", cassisMetadataList);
			loFreq = Double.valueOf(loFreqMeta.getValue());
			loFreq = XAxisCassis.getXAxisCassis(loFreqMeta.getUnit()).convertToMHzFreq(loFreq);
		} else if (CassisMetadata.isMetadataName("lofrequency", cassisMetadataList)) {
			CassisMetadata loFreqMeta = CassisMetadata.getCassisMetadata("lofrequency", cassisMetadataList);
			loFreq = Double.valueOf(loFreqMeta.getValue());
			loFreq = XAxisCassis.getXAxisCassis(loFreqMeta.getUnit()).convertToMHzFreq(loFreq);
		} else if (CassisMetadata.isMetadataName("LOFREQ", cassisMetadataList)) {
			loFreq = Double.valueOf(CassisMetadata.getCassisMetadata("LOFREQ", cassisMetadataList).getValue());
			Pattern p = Pattern.compile("\\[(.*?)\\]");
			Matcher m = p.matcher(CassisMetadata.getCassisMetadata("LOFREQ", cassisMetadataList).getComment());
			if (m.find()) {
				String unit = m.group(1);
				loFreq = XAxisCassis.getXAxisCassis(unit).convertToMHzFreq(loFreq);
			}
		}
		return loFreq;
	}

	/**
	 * This method make the same thing that the other method getVlsr but this method use the Jfits library
	 *
	 * @param cassisMetadataList
	 *            The metadata list
	 * @return The Vlsr according to the cassisMetadata list
	 */
	private double getVlsr(List<CassisMetadata> cassisMetadataList) {
		double vlsr = Double.NaN;
		if (CassisMetadata.isMetadataName("vlsr", cassisMetadataList) ||
				CassisMetadata.isMetadataName("VELO-LSR", cassisMetadataList)
				|| CassisMetadata.isMetadataName("phys.veloc", cassisMetadataList)
				|| CassisMetadata.isMetadataName("VELOSYS", cassisMetadataList)) {

			if (CassisMetadata.isMetadataName("vlsr", cassisMetadataList)) {
				vlsr = Double.valueOf(CassisMetadata.getCassisMetadata("vlsr", cassisMetadataList).getValue());
			} else if (CassisMetadata.isMetadataName("VELO-LSR", cassisMetadataList)) {
				vlsr = Double.valueOf(CassisMetadata.getCassisMetadata("VELO-LSR", cassisMetadataList).getValue());
			} else if (CassisMetadata.isMetadataName("phys.veloc", cassisMetadataList)) {
				vlsr = Double.valueOf(CassisMetadata.getCassisMetadata("phys.veloc", cassisMetadataList).getValue());
			}else if (CassisMetadata.isMetadataName("VELOSYS", cassisMetadataList)) {
				CassisMetadata cassisMetadata = CassisMetadata.getCassisMetadata("VELOSYS", cassisMetadataList);
				vlsr = Double.valueOf(cassisMetadata.getValue());
				String unit = cassisMetadata.getUnit();
				if ("m/s".equals(unit)){
					vlsr = vlsr/1E3;
				}

			}
		} else {
			for (CassisMetadata cassisMetadata : cassisMetadataList) {
				if (cassisMetadata.getComment().toLowerCase().contains("radial velocity") ||
					cassisMetadata.getComment().toLowerCase().contains("vlsr")
						&& !cassisMetadata.getComment().toLowerCase().contains("spacecraft")) {
					try {
						vlsr = Double.valueOf(cassisMetadata.getValue());
					} catch (NumberFormatException e) {
					}
				}
			}
		}

		return vlsr;
	}

	private boolean isYAxisField(String field, List<CassisMetadata> cassisMetadataList, int cpt) {
		boolean isYAxisField = false;
		if ((field != null) &&
			(field.equalsIgnoreCase("flux") ||
			 field.equalsIgnoreCase("hot_cold") ||
			 field.equalsIgnoreCase("swaaflux") ||
			 field.equalsIgnoreCase("flux_cal") ||
			 field.equalsIgnoreCase("flux_spb") ||
			 field.equalsIgnoreCase("spec_emis") ||
			 field.equalsIgnoreCase("fit") ||
			 field.equalsIgnoreCase("energy") ||
			 field.contains("flux"))) {
			isYAxisField = true;
		} else if (CassisMetadata.isMetadataName("TUTYP" + (cpt + 1), cassisMetadataList)
				&& (CassisMetadata.getCassisMetadata("TUTYP" + (cpt + 1), cassisMetadataList).getValue().equals("Data.FluxAxis.Value")
				|| CassisMetadata.getCassisMetadata("TUTYP" + (cpt + 1), cassisMetadataList).getValue().equals("spec:Spectrum.Data.FluxAxis.value"))) {
			isYAxisField = true;
		}
		return isYAxisField;
	}

	private boolean isXAxisField(String field, List<CassisMetadata> cassisMetadataList, int cpt) {
		boolean isXAxisField = false;
		if ((field != null) &&
			(field.equalsIgnoreCase("wave") || field.equalsIgnoreCase("wavelength") ||
			 field.equalsIgnoreCase("freq") || field.equalsIgnoreCase("usbfrequency")||
			 field.equalsIgnoreCase("frequency") || field.equalsIgnoreCase("lofrequency") ||
			 field.equalsIgnoreCase("swaawave") || field.equalsIgnoreCase("lambda")||
			 field.equalsIgnoreCase("spec_wave") ||
			 field.contains("freq"))) {
			isXAxisField = true;
		} else {
			final String tuttype = "TUTYP" + (cpt + 1);
			if (CassisMetadata.isMetadataName(tuttype, cassisMetadataList) &&
					   (CassisMetadata.getCassisMetadata(tuttype, cassisMetadataList).getValue().equals("Data.SpectralAxis.Value")
					|| CassisMetadata.getCassisMetadata(tuttype, cassisMetadataList).getValue().equals("spec:Spectrum.Data.SpectralAxis.Value"))) {
				isXAxisField = true;
			}
		}
		return isXAxisField;
	}

	/**
	 * @param index
	 * This method make the same thing that the other method getCassisSpectrum but this method use the Jfits library
	 *
	 * @param fitsTable
	 * @param vlsrValue
	 * @param xAxisCassis
	 * @param yAxisCassis
	 * @param columnFreqIndex
	 * @param columnIntensityIndex
	 * @param baseName
	 * @return
	 * @throws
	 */
	private CassisSpectrum getCassisSpectrum(FitsTable fitsTable, double vlsrValue,
			XAxisCassis xAxisCassis, YAxisCassis yAxisCassis, int columnFreqIndex,
			int columnIntensityIndex, String baseName, int index) {
		int nbPoints = fitsTable.getNoRows();
		double[] signalFrequencies = null;
		double[] intensities = null;
		if (nbPoints == 1) {
			if (fitsTable.getNoColumns() < 2) {
				return null;
			}
			double[] signalFrequenciesTemp = fitsTable.getColumn(columnFreqIndex).getReals(0);
			double[] intensitiesTemp = fitsTable.getColumn(columnIntensityIndex).getReals(0);
			if (signalFrequenciesTemp.length!= intensitiesTemp.length){
				return null;
			}
			nbPoints = signalFrequenciesTemp.length;
			signalFrequencies = new double[nbPoints];
			intensities = new double[nbPoints];
			if (xAxisCassis.isInverted()) {
				for (int i = 0; i < nbPoints; i++) {
					signalFrequencies[i] = xAxisCassis.convertToMHzFreq(signalFrequenciesTemp[nbPoints - i - 1]);
					intensities[i] = intensitiesTemp[nbPoints - i - 1];
				}
			} else {
				for (int i = 0; i < nbPoints; i++) {
					signalFrequencies[i] = xAxisCassis.convertToMHzFreq(signalFrequenciesTemp[i]);
					intensities[i] = intensitiesTemp[i];
				}
			}
		} else {
			signalFrequencies = new double[nbPoints];
			intensities = new double[nbPoints];

			if (!xAxisCassis.isInverted()) {
				for (int i = 0; i < nbPoints; i++) {
					signalFrequencies[i] = xAxisCassis.convertToMHzFreq(fitsTable.getColumn(columnFreqIndex).getReal(i));
					intensities[i] = fitsTable.getColumn(columnIntensityIndex).getReal(i);
				}
			} else {
				int i = nbPoints - 1;
				while (i >= 0) {
					signalFrequencies[i] = xAxisCassis.convertToMHzFreq(fitsTable.getColumn(columnFreqIndex).getReal(i));
					intensities[i] = fitsTable.getColumn(columnIntensityIndex).getReal(i);
					i--;
				}
			}
		}
		setColumnUsed(fitsTable.getColumn(columnFreqIndex).getLabel(),
				fitsTable.getColumn(columnFreqIndex).getUnit(),
				columnFreqIndex, "x", index);

		setColumnUsed(fitsTable.getColumn(columnIntensityIndex).getLabel(),
				fitsTable.getColumn(columnIntensityIndex).getUnit(),
				columnIntensityIndex, "y", index);

		XAxisCassis xAxisCassis2 = XAxisCassis.getXAxisFrequency();

		try {
			xAxisCassis2 = xAxisCassis.clone();
		} catch (CloneNotSupportedException e) {
			if (displayException) {
				e.printStackTrace();
			}
		}

		return CassisSpectrum.generateCassisSpectrum(baseName, signalFrequencies, intensities, vlsrValue, xAxisCassis2, yAxisCassis);
	}

	/**
	 * Extract unit situated in String
	 *
	 * @param comment
	 *            String in which one there is unit to extract
	 * @return The unit extracted from comment
	 */
	public String extractUnitFromComment(String comment) {
		String unit = null;
		Pattern pattern = Pattern.compile("^\\[.*?\\]");
		if (pattern.matcher(comment).find()) {
			int debut = comment.indexOf("[");
			int fin = comment.indexOf("]", debut);
			unit = comment.substring((debut + 1), fin);
		}
		return unit;
	}

	/**
	 * Create a List<ArrayList<CassisMetadata>> corresponding to all HDU contain in a fits Files
	 *
	 * @return all HDU in a List<ArrayList<CassisMetadata>>
	 */
	public List<ArrayList<CassisMetadata>> getAllHduList() {
		if (this.metadatasAllHdu == null) {
			this.metadatasAllHdu = new ArrayList<ArrayList<CassisMetadata>>();
			FitsFile fitsFile = openFile();
			if (fitsFile != null) {
				for (int i = 0; i < this.nbHdu; i++) {
					ArrayList<CassisMetadata> cassisMetadataList = new ArrayList<CassisMetadata>();
					FitsHDUnit hdu = fitsFile.getHDUnit(i);
					FitsHeader hdr = hdu.getHeader();
					@SuppressWarnings("rawtypes")
					ListIterator itr = hdr.getKeywords();
					while (itr.hasNext()) {
						FitsKeyword kw = (FitsKeyword) itr.next();
						cassisMetadataList.add(new CassisMetadata(kw.getName(), kw.getString(), kw.getComment(), extractUnitFromComment(kw.getComment())));
					}
					cassisMetadataList.add(new CassisMetadata("type_data", String.valueOf(hdr.getType()), "Type of data", "None"));
					metadatasAllHdu.add(cassisMetadataList);
				}
			}
		}
		return metadatasAllHdu;
	}

	/**
	 * Open a fits file with the JFits library
	 *
	 * @return a Fits File
	 */
	private FitsFile openFile() {
		if (this.fitsFile == null) {
			try {
				this.fitsFile = new FitsFile(file);
				this.nbHdu = fitsFile.getNoHDUnits();
			} catch (IOException | FitsException e1) {
				if (displayException) {
					e1.printStackTrace();
				}
				this.fitsFile = null;
			}
		}
		return this.fitsFile;
	}

	/**
	 * Create an Integer list containing index of HDU to keep
	 *
	 * @return an index list of HDU to keep
	 */
	private List<Integer> getIndexOfHduToKeep() {
		if (this.indexOfHduToKeep == null) {
			this.indexOfHduToKeep = new ArrayList<Integer>();
			List<ArrayList<CassisMetadata>> allHduList = getAllHduList();
			int index = 0;
			for (ArrayList<CassisMetadata> cassisMetadataList : allHduList) {
				boolean keep = false;
				if (CassisMetadata.isMetadataName("NAXIS", cassisMetadataList)) { // Test for NAXIS rules
					try {
						int naxis = Integer.parseInt(CassisMetadata.getCassisMetadata("NAXIS", cassisMetadataList).getValue());
						if (naxis < 0) {
							keep = false;
						} else if (naxis >= 0 && naxis <= 2) {
							if (naxis == 0) {
								keep = true;
							} else if (CassisMetadata.isMetadataName("NAXIS1", cassisMetadataList) &&
									!CassisMetadata.getCassisMetadata("NAXIS1", cassisMetadataList).getValue().equals("0")) {
								keep = true;
							}
						} else if (naxis >= 3) {
							if (CassisMetadata.isMetadataName("NAXIS1", cassisMetadataList) &&
									CassisMetadata.isMetadataName("NAXIS2", cassisMetadataList)) {
								int naxis1 = Integer.parseInt(CassisMetadata.getCassisMetadata("NAXIS1", cassisMetadataList).getValue());
								int naxis2 = Integer.parseInt(CassisMetadata.getCassisMetadata("NAXIS2", cassisMetadataList).getValue());
								if ((naxis1 > 1 && naxis1 < 10) || naxis2 > 1) {
									keep = false;
								} else {
									keep = true;
								}
							} else {
								keep = false;
							}
						}
					} catch (Exception e) {
						keep = false;
					}
				}
				if (CassisMetadata.isMetadataName("EXTNAME", cassisMetadataList)) { // Test for extname rule
					String extnameValue = CassisMetadata.getCassisMetadata("EXTNAME", cassisMetadataList).getValue();
					if (extnameValue.toLowerCase().contains("history") ||
						extnameValue.toLowerCase().contains("imageindex")||
						extnameValue.toLowerCase().contains("fcanor")) {
						keep = false;
					}
				}
				if (CassisMetadata.isMetadataName("VOTMETA", cassisMetadataList) &&
						CassisMetadata.isMetadataName("EXTEND", cassisMetadataList)) {
					String votmeta = CassisMetadata.getCassisMetadata("VOTMETA", cassisMetadataList).getValue();
					String extend = CassisMetadata.getCassisMetadata("EXTEND", cassisMetadataList).getValue();
					if (votmeta.equalsIgnoreCase("true") && extend.equalsIgnoreCase("true")) {
						keep = false;
					}
				}
				if (keep) { // Add index to keep
					this.indexOfHduToKeep.add(index);
				}
				index++;
			}
		}
		return this.indexOfHduToKeep;
	}

	/**
	 * Create an Integer list containing index of HDU to keep to create spectrum
	 *
	 * @return an index list of HDU to keep to create a spectrum
	 */
	public List<Integer> getIndexOfHduToKeepForSpectrum() {
		if (this.indexOfHduToKeepForSpectrum == null) {
			this.indexOfHduToKeepForSpectrum = new ArrayList<Integer>();
			List<ArrayList<CassisMetadata>> allHduList = getAllHduList();
			List<Integer> indexOfHduToKeep = getIndexOfHduToKeep();
			for (int i = 0; i < allHduList.size(); i++) {
				if (indexOfHduToKeep.contains(i)) {
					ArrayList<CassisMetadata> cassisMetadataList = allHduList.get(i);
					if (!CassisMetadata.getCassisMetadata("NAXIS", cassisMetadataList).getValue().equals("0")) {
						indexOfHduToKeepForSpectrum.add(i);
					}
				}
			}
		}
		return this.indexOfHduToKeepForSpectrum;
	}

	/**
	 * Create an Integer list containing index of HDU to ignored
	 *
	 * @return an index list of HDU to ignored
	 */
	public List<Integer> getIndexOfHduToIgnore() {
		if (this.indexOfHduToIgnore == null) {
			this.indexOfHduToIgnore = new ArrayList<Integer>();
			List<Integer> indexOfHduToKeep = getIndexOfHduToKeep();
			int nbHdu = getNbHdu();
			for (int i = 0; i < nbHdu; i++) {
				if (!indexOfHduToKeep.contains(i)) {
					this.indexOfHduToIgnore.add(i);
				}
			}
		}
		return this.indexOfHduToIgnore;
	}

	/**
	 * Return a List<ArrayList<CassisMetadata>> containing metadata list corresponding to spectrum with all traitment realized
	 *
	 * @return metadatasSpectrum the metadata list corresponding to spectrum with all traitment realized
	 */
	public List<ArrayList<CassisMetadata>> geMetadatasSpectrum() {
		if (this.metadatasSpectrum == null) {
			metadatasSpectrum = new ArrayList<ArrayList<CassisMetadata>>();
			List<ArrayList<CassisMetadata>> hduList = FileFitsUtil.getHduListWithMetadataMerge(
					getFinalTree(), getAllHduList());
			List<Integer> indexOfHduToKeepForSpectrum = getIndexOfHduToKeepForSpectrum();
			for (int i = 0; i < hduList.size(); i++) {
				if (indexOfHduToKeepForSpectrum.contains(i)) {
					this.metadatasSpectrum.add(hduList.get(i));
				}
			}
			metadatasSpectrum = extractCommentFromHdu(metadatasSpectrum);
		}
		return this.metadatasSpectrum;
	}

	/**
	 * For each hdu, collect the comment metadata (which no contain name and value) in a new metadata named ALLCOMMENT
	 *
	 * @param hduList
	 *            The hdu list
	 * @return a hdu list
	 */
	private List<ArrayList<CassisMetadata>> extractCommentFromHdu(List<ArrayList<CassisMetadata>> hduList) {
		List<ArrayList<CassisMetadata>> finalHduList = new ArrayList<ArrayList<CassisMetadata>>();
		for (List<CassisMetadata> cassisMetadataList : hduList) {
			finalHduList.add(extractCommentFromCassisMetadataList(cassisMetadataList));
		}
		return finalHduList;
	}

	/**
	 * For an hdu, collect the comment metadata (which no contain name and value) in a new metadata named ALLCOMMENT
	 *
	 * @param cassisMetadataList
	 *            The metadata list
	 * @return a metadata list with the new metadata ALLCOMMENT
	 */
	private ArrayList<CassisMetadata> extractCommentFromCassisMetadataList(List<CassisMetadata> cassisMetadataList) {
		ArrayList<CassisMetadata> cassisMetadataListFinal = new ArrayList<CassisMetadata>();
		StringBuilder sb = new StringBuilder("<html>");
		boolean containsComments = false;
		for (CassisMetadata cassisMetadata : cassisMetadataList) {
			if ((cassisMetadata.getName() == null || cassisMetadata.getName().isEmpty())
					&& (cassisMetadata.getValue() == null || cassisMetadata.getValue().isEmpty())
					&& !cassisMetadata.getComment().isEmpty()) {
				containsComments = true;
				sb.append(cassisMetadata.getComment());
				sb.append("<br>");
			} else {
				try {
					cassisMetadataListFinal.add(cassisMetadata.clone());
				} catch (CloneNotSupportedException e) {
					e.printStackTrace();
				}
			}
		}
		if (containsComments) {
			sb.append("</html>");
			cassisMetadataListFinal.add(new CassisMetadata("ALLCOMMENT", "", sb.toString(), ""));
		}
		return cassisMetadataListFinal;
	}

	public Tree<Integer> getFinalTree() {
		if (finalTree == null){
			finalTree = FileFitsUtil.getFinalTree(getIndexOfHduToIgnore(), getAllHduList());
		}
		return finalTree;
	}

	/**
	 * Return the number of HDU
	 *
	 * @return The number of HDU
	 */
	public int getNbHdu() {
		return this.metadatasAllHdu.size();
	}

	/**
	 * Return if a spectrum is a HIFI spectrum as there is some particular
	 *  change for them.
	 *
	 * @param cassisMetadataList The metadata list.
	 * @return true if the spectrum with the given metadatas list is an HIFI spectrum,
	 *  false otherwise.
	 */
	public static boolean isHifiSpectrum(List<CassisMetadata> cassisMetadataList) {
		CassisMetadata classe = CassisMetadata.getCassisMetadata("CLASS___", cassisMetadataList);
		CassisMetadata instrument = CassisMetadata.getCassisMetadata("instrument", cassisMetadataList);
		return classe != null && instrument != null &&
				classe.getValue().startsWith("herschel.") &&
				instrument.getValue().equalsIgnoreCase("hifi");

	}

	@Override
	public List<ColumnsDetected> getWaveColumnsDetected() {
		if (xColumnsDetected == null) {
			xColumnsDetected = new ArrayList<ColumnsDetected>();

			final List<Integer> indexSpectrum = getIndexOfHduToKeepForSpectrum();
			for (int index = 0; index < indexSpectrum.size(); index++) {
				FitsData data = this.fitsFile.getHDUnit(indexSpectrum.get(index)).getData();
				if (data instanceof FitsTable){
					xColumnsDetected.add(getColumnsDetected("x", index));
				}else {
					xColumnsDetected.add(null);
				}
			}

		}
		return xColumnsDetected;
	}

	@Override
	public List<ColumnsDetected> getFluxColumnsDetected() {
		if (yColumnsDetected == null) {
			yColumnsDetected = new ArrayList<ColumnsDetected>();
			final List<Integer> indexSpectrum = getIndexOfHduToKeepForSpectrum();
			for (int index = 0; index < indexSpectrum.size(); index++) {
				FitsData data = this.fitsFile.getHDUnit(indexSpectrum.get(index)).getData();
				if (data instanceof FitsTable){
					yColumnsDetected.add(getColumnsDetected("y", index));
				}else {
					yColumnsDetected.add(null);
				}
			}
		}
		return yColumnsDetected;
	}


	private ColumnsDetected getColumnsDetected(String axis, int spectrumIndex) {
		ArrayList<CassisMetadata> cassisMetadataList = geMetadatasSpectrum().get(spectrumIndex);
		FitsHDUnit hdUnit = this.fitsFile.getHDUnit(getIndexOfHduToKeepForSpectrum().get(spectrumIndex));
		FitsTable fitsTable = (FitsTable)hdUnit.getData();
		ColumnsDetected res = null;


		List<ColumnInformation> listColumns = new ArrayList<>();
		for (int j = 0; j < fitsTable.getNoColumns(); j++) {
			boolean errorXAxis = false;
			boolean errorYAxis = false;
			String unitX = "MHz";
			String unitY = "K";

			FitsColumn field = fitsTable.getColumn(j);



			if (axis.equalsIgnoreCase("x") && isXAxisField(field.getLabel(), cassisMetadataList, j)) {

				if ((field.getUnit() == null)||field.getUnit().isEmpty()) {
					CassisMetadata waveUnitMetadata =
							CassisMetadata.getCassisMetadata("ssa:dataset.spectralsi", additionalMetadataList);
					if (waveUnitMetadata != null){
						unitX = waveUnitMetadata.getValue();
					} else {
						waveUnitMetadata =
								CassisMetadata.getCassisMetadata("ssa:dataset.spectralsi", cassisMetadataList);
						if (waveUnitMetadata != null){
							unitX = waveUnitMetadata.getValue();
						} else {
							errorXAxis = true;
						}
					}

				} else {
					unitX = field.getUnit();
				}
				listColumns.add(new ColumnInformation(field.getLabel(), unitX , errorXAxis, j));
			}




			else if (axis.equalsIgnoreCase("y") && isYAxisField(field.getLabel(), cassisMetadataList, j)) {

				if ((field.getUnit() == null)||field.getUnit().isEmpty()) {
					CassisMetadata fluxUnitMetadata =
							CassisMetadata.getCassisMetadata("ssa:dataset.fluxsi", additionalMetadataList);
					if (fluxUnitMetadata != null){
						unitY = fluxUnitMetadata.getValue();
					} else {
						fluxUnitMetadata =
								CassisMetadata.getCassisMetadata("ssa:dataset.fluxsi", cassisMetadataList);
						if (fluxUnitMetadata != null){
							unitY = fluxUnitMetadata.getValue();
						} else {
							errorYAxis = true;
						}
					}
				} else {
					unitY = field.getUnit();
				}
				listColumns.add(new ColumnInformation(field.getLabel(), unitY, errorYAxis,j));
			}
		}

		final int index = 0;
		if (axis.equalsIgnoreCase("x")){
			CassisMetadata waveAxisMetadata = CassisMetadata.getMetadata(cassisMetadataList,
					additionalMetadataList, WAVEAXIS_KEYWORD);
			if (listColumns.size() > 0) {
				String name = listColumns.get(index).getName();
				if (waveAxisMetadata != null && isIn(waveAxisMetadata.getValue(), listColumns)){
					name = waveAxisMetadata.getValue();
				}
				res = new ColumnsDetected(name, listColumns);
			}else if (fitsTable.getNoColumns()>1){
				listColumns.add(new ColumnInformation( fitsTable.getColumn(0).getLabel(), "Mhz", true,0));
				res = new ColumnsDetected(fitsTable.getColumn(0).getLabel(), listColumns);
			}

		}else if (axis.equalsIgnoreCase("y") ){
			CassisMetadata fluxAxisMetadata = CassisMetadata.getMetadata(cassisMetadataList,
					additionalMetadataList, FLUXAXIS_KEYWORD);
			if (listColumns.size() > 0) {
				String name = listColumns.get(index).getName();
				if (fluxAxisMetadata != null && isIn(fluxAxisMetadata.getValue(), listColumns)){
					name = fluxAxisMetadata.getValue();
				} else {
					fluxAxisMetadata =
							CassisMetadata.getCassisMetadata("ssa:Dataset.FluxAxis", additionalMetadataList);
					if (fluxAxisMetadata != null && isIn(fluxAxisMetadata.getValue(), listColumns)){
						name = fluxAxisMetadata.getValue();
					}

				}
				res = new ColumnsDetected(name, listColumns);
			}else if (fitsTable.getNoColumns()>1){
				listColumns.add(new ColumnInformation( fitsTable.getColumn(1).getLabel(), "K", true,1));
				res = new ColumnsDetected(fitsTable.getColumn(1).getLabel(), listColumns);
			}
		}

		return res;
	}


	private boolean isIn(String name, List<ColumnInformation> listColumns) {
		boolean res = false;

		for (ColumnInformation columnInformation : listColumns) {
			if (columnInformation.getName().equals(name)){
				res = true;
				break;
			}
		}
		return res;
	}

	/**
	 * Set the column used.
	 *
	 * @param name The name of the column used.
	 * @param unit The unit of the values in the column.
	 * @param indexColumn The index of the column used.
	 * @param type The type of the column "x" or "y".
	 * @param spectrumIndex the index of the spectrum
	 */
	private void setColumnUsed(String name, String unit, int indexColumn, String type, int spectrumIndex
			) {

		ColumnsDetected detected =
				"x".equals(type) ? getWaveColumnsDetected().get(spectrumIndex) : getFluxColumnsDetected().get(spectrumIndex);
		if (detected != null) {

			detected.setColumnUsed(name);
		}
	}
}
