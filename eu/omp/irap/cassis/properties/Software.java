/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.properties;

import java.io.File;
import java.io.FilenameFilter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.TelescopePathDefault;
import eu.omp.irap.cassis.common.TelescopeUtil;
import eu.omp.irap.cassis.common.gui.BookmarkPropertiesInterface;
import eu.omp.irap.cassis.common.gui.CassisJFileChooser;
import eu.omp.irap.cassis.database.access.DatabaseProperties;
import eu.omp.irap.cassis.database.access.InfoDataBase;
import eu.omp.irap.cassis.database.access.TypeDataBase;
import eu.omp.irap.cassis.database.configuration.DatabaseConfiguration;
import eu.omp.irap.cassis.database.configuration.InterfaceDatabaseModuleConfiguration;
import eu.omp.irap.cassis.gui.template.ListTemplateManager;
import eu.omp.irap.cassis.gui.template.ManageTemplateControl;
import radex.ModeRadexManager;

/**
 * @author glorian
 *
 */
public class Software {

	private static final Logger LOGGER = LoggerFactory.getLogger(Software.class);

	public static final String NAME = "CASSIS";
	public static final String NAME_EXT = "CASSIS Interactive Spectrum Analyzer";
	private static String cassisPath = null;

	private static final String CONFIG_FOLDER = "config";
	private static final String CONTINUUM_FOLDER = "continuum";
	private static final String TELESCOPE_FOLDER = "telescope";
	private static final String TEMPLATE_FOLDER = "template";
	private static final String ENABLE_TEMPLATE_FOLDER = "enable";
	private static final String DISABLE_TEMPLATE_FOLDER = "disable";
	private static final String SAVE_FOLDER = "save";
	private static final String DATABASE_NAME = "database";
	public static final String PROPERTIES_FOLDER = "properties";
	public static final String RADEX_COLLISION_FOLDER = "lamda";
	private static final String RADEX_COLLISION_USER_FOLDER = "lamda_user";
	private static final String LAST_FOLDER = "lastFolder.properties";
	public static final String USER_COLLISIONS_LIST = "user_collision_files.txt";

	public static final String USER_OPEN_RECENT_FILE = "openRecent.properties";

	private static ModePath modePath = null;
	private static ModeRadexManager modeRadexManager = null;
	private static ModeUtil modeUtil = null;

	private static UserConfiguration userConfiguration = null;
	private static CassisConfiguration cassisConfiguration = null;
	private static boolean isOnlineMode = false;

	public static final String DATABASE_FOLDER = "database";

	public static final String EMAIL_USER = "noname@nodsn.com";
	public static final String CLIENT = "CASSIS";

	static {
		setApp();
		CassisJFileChooser.setBookmarkProperties(new BookmarkPropertiesInterface() {

			@Override
			public void saveBookMarkProperties(Properties properties) {
				CassisConfiguration.saveBookMarkProperties(properties);
			}

			@Override
			public Properties getBookmarkProperties() {
				return CassisConfiguration.getBookmarkProperties();
			}

			@Override
			public String getCassisFolder() {
				return Software.getCassisPath();
			}
		});
	}

	public static String getCassisPath() {
		if (cassisPath == null) {
			if (isOnlineMode()) {
				cassisPath = getOnlineModePath();
			} else {
				try {
					final String ressource = Software.class.getName().replace('.', '/') + ".class";

					ClassLoader loader = Thread.currentThread().getContextClassLoader();
					URL url = null;
					try {
						url = loader.getResource(ressource);
					} catch (Exception e) {
						LOGGER.warn("Error when getting ressource {} to get the CASSIS path", ressource, e);
					}

					String path = "";
					if (url != null) {
						path = URLDecoder.decode(url.toString(), "UTF-8");

						if (path.startsWith("jar:file:")) {
							// suppression de jar:file: de l'url d'un jar
							// ainsi que du path de la classe dans le jar
							path = path.substring("jar:file:".length(), path.indexOf('!'));
							// suppression de la classe ou du jar du path de l'url
							path = path.substring(0, path.lastIndexOf('/'));
						} else if (path.startsWith("jar:http")) {
							// We can not use that. However, if we have that
							// this is an online session.
							return getOnlineModePath();
						} else {
							// suppresion du file: de l'url si c'est une classe en
							// dehors d'un jar
							// et suppression du path du package si il est présent.
							path = path.substring("file:".length(), path.length());
							// suppression de la classe ou du jar du path de l'url
							path = path.substring(0, path.lastIndexOf('/'));
							Package pack = Software.class.getPackage();
							if (null != pack) {
								String packPath = pack.getName().replace('.', '/');
								if (path.endsWith(packPath)) {
									path = path.substring(0, (path.length() - packPath.length()) - 1);
								}
							}
						}
						if (path.endsWith("/classes"))
							path = path.replace("/classes", "");
						else if(path.endsWith("/generated-classes/cobertura"))
							path = path.replace("/generated-classes/cobertura", "");


						File file = new File(path);
						path = file.getParent();

					}
					cassisPath = path;
				} catch (UnsupportedEncodingException e) {
					LOGGER.error("An encoding error occured while trying to get CASSIS path", e);
				} catch (StringIndexOutOfBoundsException se) {
					LOGGER.error("StringIndexOutOfBoundsException occured while trying to get CASSIS path", se);
				}
			}
		}
		return cassisPath;
	}

	public static String getTemplateEnablePath() {
		return getTemplatePath() + File.separator + ENABLE_TEMPLATE_FOLDER;
	}

	public static String getTemplateDisablePath() {
		return getTemplatePath() + File.separator + DISABLE_TEMPLATE_FOLDER;
	}

	public static String getCassisRadexCollisionsPath() {
		return userConfiguration.getRadexCollisionPath();
	}

//	public static String getLogPath() {
//		return getCassisPath() + File.separator + DATABASE_FOLDER  + File.separator + LAMDA_FOLDER;
//	}

	public static String getPropertiesPath() {
		return getCassisPath() + File.separator + PROPERTIES_FOLDER;
	}

	public static String getLamdaUserPath() {
		return getCassisPath() + File.separator + DATABASE_FOLDER  + File.separator + RADEX_COLLISION_USER_FOLDER;
	}

	public static String getHelpPath() {
		return getCassisPath() + File.separator + "doc" + File.separator + "help";
	}

	public static String getDataPath() {
		return getCassisPath() + File.separator + "delivery" + File.separator + "data";
	}

	public static String getConfigPath() {
		return getCassisPath() + File.separator + "delivery" + File.separator + CONFIG_FOLDER;
	}

	public static String getContinuumPath() {
		return getCassisPath() + File.separator + "delivery" + File.separator + CONTINUUM_FOLDER;
	}

	public static String getDatabasePath() {
		return getCassisPath() + File.separator + DATABASE_NAME;
	}

	public static String getSavePath() {
		return getCassisPath() + File.separator  + "delivery" + File.separator + SAVE_FOLDER;
	}

	public static String getTelescopePath() {
		return getCassisPath() + File.separator +  "delivery" + File.separator + TELESCOPE_FOLDER;
	}

	public static String getTemplatePath() {
		return getDatabasePath()  + File.separator + TEMPLATE_FOLDER;
	}

	public static String getScriptPath() {
		return getCassisPath()  + File.separator + "delivery" + File.separator + "script";
	}

	/**
	 * Return the default directory for Fit configuration.
	 *
	 * @return the default directory for Fit configuration.
	 */
	public static String getFitConfigPath() {
		return getConfigPath() + File.separatorChar + "fit";
	}

	/**
	 * Return the default directory for Lab Absorption configuration.
	 *
	 * @return the default directory for Lad Absorption configuration.
	 */
	public static String getLabAbsorptionConfigPath() {
		return getConfigPath() + File.separatorChar + "lab_absorption-model";
	}

	/**
	 * Return the default directory for Line Analysis configuration.
	 *
	 * @return the default directory for Line Analysis configuration.
	 */
	public static String getLineAnalysisConfigPath() {
		return getConfigPath() + File.separatorChar + "line-analysis";
	}

	/**
	 * Return the default directory for Spectrum Analysis configuration.
	 *
	 * @return the default directory for Spectrum Analysis configuration.
	 */
	public static String getSpectrumAnalysisConfigPath() {
		return getConfigPath() + File.separatorChar + "spectrum-analysis";
	}

	/**
	 * Return the default directory for LteRadex configuration.
	 *
	 * @return the default directory for LteRadex configuration.
	 */
	public static String getLteRadexConfigPath() {
		return getConfigPath() + File.separatorChar + "lte_radex-model";
	}

	/**
	 * Return the default directory for Loomis configuration.
	 *
	 * @return the default directory for Loomis configuration.
	 */
	public static String getLoomisAnalysisConfigPath() {
		return getConfigPath() + File.separatorChar + "loomis-analysis";
	}

	/**
	 * Return the default directory for Comet configuration.
	 *
	 * @return the default directory for Comet configuration.
	 */
	public static String getCometConfigPath() {
		return getConfigPath() + File.separatorChar + "comet-model";
	}

	/**
	 * Return the default directory for Rotational Diagram configuration.
	 *
	 * @return the default directory for Rotational Diagram configuration.
	 */
	public static String getRotationalDiagramConfigPath() {
		return getConfigPath() + File.separatorChar + "rotational-model";
	}

	/**
	 * Return the default directory for Database configuration.
	 *
	 * @return the default directory for Database configuration.
	 */
	public static String getDatabaseConfigPath() {
		return getConfigPath() + File.separatorChar + "database";
	}

	/**
	 * Return the directory for Partition Mole.
	 *
	 * @return the directory for Partition Mole.
	 */
	public static String getPartitionMolePath() {
		return getDatabasePath() + File.separatorChar + "sqlPartitionMole";
	}

	public static void setOnlineMode() {
		isOnlineMode = true;
		cassisPath = null;
		String path = getCassisPath();
		setCommon(path);
		configureOnlineDatabase();
	}

	public static void setApp() {
		isOnlineMode = false;
		String path = getCassisPath();
		setCommon(path);
	}

	private static void setCommon(String path) {
		modePath = new ModePath(path);
		modeUtil = new ModeUtil(path);
		userConfiguration = new UserConfiguration(modeUtil);

		cassisConfiguration = null;
		modeRadexManager = new ModeRadexManager(
				new File(getUserCollisionFiles()),
				new File(Software.getPropertiesPath()),
				new File(getLamdaUserPath()),
				new File(getCassisRadexCollisionsPath()) );
		initDatabaseProperties();
		TelescopeUtil.setITelescopePath(new TelescopePathDefault(
				Software.getTelescopePath()));
	}

	public static String getUserCollisionFiles() {
		return Software.getPropertiesPath() + File.separator + USER_COLLISIONS_LIST;
	}

	public static boolean isOnlineMode() {
		return isOnlineMode;
	}

	/**
	 * @return the modePath
	 */
	public static ModePath getModePath() {
		return modePath;
	}

	public static ModeRadexManager getRadexMoleculesManager() {
		return modeRadexManager;
	}

	/**
	 * @return the userConfiguration
	 */
	public static UserConfiguration getUserConfiguration() {
		return userConfiguration;
	}

	/**
	 * @return the modeUtil
	 */
	public static ModeUtil getModeUtil() {
		return modeUtil;
	}

	/**
	 * @return the cassisConfiguration
	 */
	public static CassisConfiguration getCassisConfiguration() {
		if (cassisConfiguration == null)
			cassisConfiguration = new CassisConfiguration();
		return cassisConfiguration;
	}

	public static String getVersion() {
		return CassisVersion.VERSION;
	}

	public static String getSociety() {
		return CassisVersion.SOCIETY;
	}

	public static String getYear() {
		return CassisVersion.YEAR;
	}

	public static String getOnlineModePath() {
		if (System.getProperty("os.name").startsWith("Windows")) {
			return System.getenv("APPDATA") + File.separator + "cassis";
		} else {
			return System.getProperty("user.home") + File.separator + ".cassis";
		}
	}

	public static String getProperties(String fileName, String typeData, String dataPath) {
		String res = "";
		Properties properties = Software.getModePath().getProperties(fileName);
		if (properties != null)
			res = properties.getProperty(typeData, dataPath);
		return res;
	}

	public static void setProperties(String fileName, String typeData, String dataValue) {
		Properties properties = Software.getModePath().getProperties(fileName);

		if (properties != null) {
			properties.setProperty(typeData, dataValue);
			Software.getModePath().saveProperties(fileName, properties);
		}
	}

	public static String getFriendlyVersion() {
		String version = getVersion();
		String[] splitVersion = version.split("-");
		StringBuilder sb = new StringBuilder("Cassis ");
		if (splitVersion.length >= 2 && !Software.isOnlineMode()) {
			sb.append(splitVersion[1]);
		} else if (Software.isOnlineMode()) {
			sb.append("Online");
		}
		if (version.toLowerCase().contains("beta")) {
			sb.append(" beta");
		}
		return sb.toString();
	}

	/**
	 * Initialize the parameters of DatabaseProperties.
	 */
	public static void initDatabaseProperties() {
		DatabaseProperties.setDefaultPath(Software.getCassisPath());
		DatabaseProperties.setPropertyFile(Software.getCassisPath()
				+ File.separatorChar + Software.PROPERTIES_FOLDER
				+ File.separatorChar + CassisConfiguration.CASSIS_PROPERTIES_FILE);
		DatabaseProperties.setHistoryPath(Software.getPropertiesPath());
		DatabaseProperties.setPartionMolePath(Software.getCassisPath()
				+ File.separatorChar + "database" + File.separatorChar
				+ "sqlPartitionMole");
		InfoDataBase.getInstance().setPostDatabaseChangeAction(new Runnable() {
			@Override
			public void run() {
				ListTemplateManager.getInstance().refreshTemplates();
				ManageTemplateControl.refreshTemplates();
			}
		});
		DatabaseConfiguration.setInstance(new InterfaceDatabaseModuleConfiguration() {
			@Override
			public void setLastFolder(String key, String path) {
				Software.setLastFolder(key, path);
			}
			@Override
			public String getPartionMolePath() {
				return Software.getPartitionMolePath();
			}
			@Override
			public String getLastFolder(String key) {
				return Software.getLastFolder(key);
			}
			@Override
			public String getDatabasePath() {
				return Software.getDatabasePath();
			}
			@Override
			public String getDatabaseConfigPath() {
				return Software.getDatabaseConfigPath();
			}
			@Override
			public boolean isOnlineMode() {
				return Software.isOnlineMode();
			}
		});
	}

	/**
	 * Configure the database to use on online mode.
	 */
	private static void configureOnlineDatabase() {
		if (userConfiguration.isForceMiniDbOnOnlineMode()) {
			File dbFile = Software.getMiniDatabase();
			if (dbFile == null) {
				InfoDataBase.getInstance().setTypeDb(TypeDataBase.NO);
			} else {
				InfoDataBase.getInstance().setDataBaseType("SQLITE", dbFile.getAbsolutePath(), false);
			}
		}
	}

	/**
	 * Search on database folder for a "mini database" file, then return it if
	 *  found or return null.
	 *
	 * @return The minidatabase file, or null if not found.
	 */
	private static File getMiniDatabase() {
		File dbDirectory = new File(getDatabasePath());
		FilenameFilter miniDbFilter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name != null && name.endsWith(".db")
						&& name.startsWith("mini");
			}
		};
		for (File file : dbDirectory.listFiles(miniDbFilter)) {
			if (file.isFile() && file.length() != 0) {
				return file;
			}
		}
		return null;
	}

	/**
	 * Return the last folder path for the given key or null if not found.
	 *
	 * @param key The key.
	 * @return the last folder path for the given key or null if not found.
	 */
	public static String getLastFolder(String key) {
		return Software.getProperties(Software.LAST_FOLDER, key, null);
	}

	/**
	 * Set the last folder path for a key.
	 *
	 * @param key The key.
	 * @param path The path to set.
	 */
	public static void setLastFolder(String key, String path) {
		Software.setProperties(Software.LAST_FOLDER, key, path);
	}
}
