/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.properties;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author glorian
 * @author M.Boiziot
 */
public class ModePath {

	private static final Logger LOGGER = LoggerFactory.getLogger(ModePath.class);

	private String path;


	public ModePath(String cassisPath) {
		this.path = cassisPath;
	}

	public Properties getProperties(String fi) {
		Properties properties = new Properties();

		String fullPath = path + File.separator + Software.PROPERTIES_FOLDER + File.separator + fi;
		if (!new File(fullPath).exists()) {
			try {
				File file = new File(fullPath);
				if (!file.createNewFile()) {
					LOGGER.error("Error while trying to create file {}", file.getAbsolutePath());
				}
			} catch (IOException ioe) {
				LOGGER.error("Error while trying to create file", ioe);
				return null;
			}
		}
		try (FileReader reader = new FileReader(fullPath)) {
			properties.load(reader);
		} catch (IOException e) {
			LOGGER.debug("Error while reading the Properties file {}", fullPath, e);
			properties = null;
		}

		return properties;
	}

	public URL getIcon(String icon) {
		URL url = null;
		try {
			url = new URL("file:" + path + icon);
		} catch (MalformedURLException e) {
			LOGGER.warn("URL is malformed, unable to get the URL of the icon \"{}\"", icon, e);
		}
		return url;
	}

	public InputStream getAuthors() {
		InputStream authors = null;
		File file = new File(path + File.separator + "ressource" + File.separator + "authors.txt");
		try {
			authors = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			LOGGER.warn("Authors file is not found", e);
		}
		return authors;
	}

	public String getBdd(String name) {
		return path + File.separator + name;
	}

	public BufferedReader getContinuum(String continuum) {
		BufferedReader bufferedReader = null;
		try {
			if (new File(continuum).exists()) {
				bufferedReader = new BufferedReader(new FileReader(continuum));
			}
			else {
				bufferedReader = new BufferedReader(new FileReader(Software.getContinuumPath() + File.separator
						+ continuum));
			}
		} catch (FileNotFoundException e) {
			LOGGER.warn("Continuum file is not found for continuum {}", continuum, e);
		}

		return bufferedReader;
	}

	public void saveProperties(String propertiesFileName, Properties properties) {
		try (FileWriter writer = new FileWriter(path + File.separator +
				Software.PROPERTIES_FOLDER + File.separator + propertiesFileName)) {
			properties.store(writer, "");
		} catch (IOException e) {
			LOGGER.warn("Error while saving properties to file {}", propertiesFileName, e);
		}
	}
}
