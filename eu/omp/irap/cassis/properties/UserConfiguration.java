/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.properties;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.cassisd.model.synthetic.RadexConfiguration;
import eu.omp.irap.cassis.common.Template;
import eu.omp.irap.cassis.fit.util.enums.FitType;
import eu.omp.irap.cassis.gui.help.UpdateView.UpdateRegular;
import eu.omp.irap.cassis.gui.plot.curve.Rendering;
import eu.omp.irap.cassis.gui.plot.tools.ToolsUtil;
import radex.ReadRadexFile;


/**
 * Class to save and read the user configuration.
 *
 * @author glorian
 * @author M. Boiziot
 */
public class UserConfiguration {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserConfiguration.class);

	public static final String UPDATE_REGULAR_PROPERTIE = "update-regular";
	public static final String LAST_UPDATE_PROPERTIE = "last-update";
	public static final String UPDATE_PATH = "update-path";
	public static final String UPDATE_PATH_DEFAULT = "http://cassis.irap.omp.eu/download/";
	public static final String USER_CONFIG_FILE = "config.properties";
	public static final String TEST_MODE_PROPERTY = "test-mode";

	private static final String SAMP_AUTO_START_PROPERTY = "sampOnAutoStart";
	private static final String RADEX_FORTRAN_PATH = "radexFortranPath";
	private static final String RADEX_COLLISION_PATH = "radexCollisionPath";
	private static final String RADEX_DATA_MAXPART = "radexDataMaxPart";
	private static final String RADEX_DATA_MAXTEMP = "radexDataMaxTemp";
	private static final String RADEX_DATA_MAXLEV = "radexDataMaxLev";
	private static final String RADEX_DATA_MAXLINE = "radexDataMaxLine";
	private static final String RADEX_DATA_MAXCOLL = "RadexDataMaxColl";
	private static final String OPEN_FILE_DIRECTLY = "openFileDirectly";
	private static final String FORCE_MINI_DB_ON_ONLINE = "forceMiniDbOnOnlineMode";
	private static final String TOOLS_MAX_NB_POINTS_PROPERTY = "toolsMaxNbPoints";
	private static final String RENDERING_PROPERTY = "rendering";
	private static final String TEMPLATE_PROPERTY = "defaultTemplate";

	private static final String FIT_DEFAULT_COMPONENT = "fitDefaultComponent";
	private static final String FIT_AUTO_ESTIMATE = "fitAutoEstimate";
	private static final String FIT_AUTO_SAVE = "fitAutoSave";

	private static final String AUTO_OTHER_SPECIES_DISPLAYED = "autoOtherSpeciesDisplayed";

	private UpdateRegular updateRegular;
	private String lastUpdate;
	private String updatePath;
	private boolean testMode;
	private boolean sampOnAutoStart;
	private boolean openFileDirectly;
	private final ModeUtil modeUtil;
	private String radexFortranPath;
	private String radexCollisionPath;
	private int radexDataMaxPart;
	private int radexDataMaxTemp;
	private int radexDataMaxLev;
	private int radexDataMaxLine;
	private int radexDataMaxColl;
	private int toolsMaxNbPoints;
	private boolean forceMiniDbOnOnlineMode;
	private String rendering;
	private String userDefaultTemplate;

	private FitType fitDefaultType;
	private boolean fitAutoEstimate;
	private boolean fitAutoSave;

	private boolean autoOtherSpeciesDisplayed;


	/**
	 * Constructor.
	 *
	 * @param modeUtil The modeutil.
	 */
	public UserConfiguration(ModeUtil modeUtil) {
		this.modeUtil = modeUtil;
		readPropertiesUserConfiguration();
	}

	/**
	 * Read the CASSIS settings from the configuration file.
	 */
	private void readPropertiesUserConfiguration() {
		Properties properties = Software.getModePath().getProperties(USER_CONFIG_FILE);

		/* If the file do not exist properties will be null here.
		 Create an empty properties, in order to load the default values.
		 Needed for web start, where the USER_CONFIG_FILE does not exist.
		 */
		if (properties == null) {
			properties = new Properties();
		}

		String updateRegularStr = properties.getProperty(UPDATE_REGULAR_PROPERTIE);

		updateRegular = UpdateRegular.getValue(updateRegularStr);
		lastUpdate = properties.getProperty(LAST_UPDATE_PROPERTIE);
		updatePath = properties.getProperty(UPDATE_PATH, UPDATE_PATH_DEFAULT);
		testMode = Boolean.parseBoolean(properties.getProperty(TEST_MODE_PROPERTY, "false"));
		openFileDirectly = openFileDirectly(properties);
		if ("false".equalsIgnoreCase(properties.getProperty(SAMP_AUTO_START_PROPERTY, "true"))) {
			sampOnAutoStart = false;
		} else {
			sampOnAutoStart = true;
		}

		RadexConfiguration.destroy();
		readRadexProperties(properties);
		readUpdateToolsProperties(properties);
		forceMiniDbOnOnlineMode = Boolean.parseBoolean(properties.getProperty(FORCE_MINI_DB_ON_ONLINE, "true"));
		rendering = properties.getProperty(RENDERING_PROPERTY, Rendering.HISTOGRAM.name());
		userDefaultTemplate = properties.getProperty(TEMPLATE_PROPERTY, Template.DEFAULT_TEMPLATE);

		fitDefaultType = FitType.valueOf(properties.getProperty(FIT_DEFAULT_COMPONENT, "GAUSS"));
		fitAutoEstimate = Boolean.parseBoolean(properties.getProperty(FIT_AUTO_ESTIMATE, "false"));
		fitAutoSave = Boolean.parseBoolean(properties.getProperty(FIT_AUTO_SAVE, "false"));
		if ("false".equalsIgnoreCase(properties.getProperty(SAMP_AUTO_START_PROPERTY, "false"))) {
			autoOtherSpeciesDisplayed = false;
		} else {
			autoOtherSpeciesDisplayed = true;
		}
	}

	/**
	 * Determines whether a file should be opened directly in Full Spectrum or
	 * if we should ask the user the viewer to use for watching his spectrums.
	 *
	 * @param properties The properties files.
	 * @return if the file should be opened directly with Full Spectrum.
	 */
	private boolean openFileDirectly(Properties properties) {
		boolean directlyOpenFile = false;
		boolean propReadingOk = false;
		if (properties.containsKey(OPEN_FILE_DIRECTLY)) {
			try {
				directlyOpenFile = Boolean.parseBoolean(
						properties.getProperty(OPEN_FILE_DIRECTLY));
				propReadingOk = true;
			} catch (Exception e) {
				LOGGER.debug("An exception occured", e);
				// Do nothing as propReadingOk is already false.
			}
		}
		if (!propReadingOk) {
			directlyOpenFile = Software.isOnlineMode();
		}
		return directlyOpenFile;
	}

	/**
	 * Read the Radex properties from the given {@link Properties}.
	 *
	 * @param prop The Properties used to read the values.
	 */
	private void readRadexProperties(Properties prop) {
		radexFortranPath = prop.getProperty(RADEX_FORTRAN_PATH,
				RadexConfiguration.getInstance().getPath());
		RadexConfiguration.getInstance().setPath(radexFortranPath);
		radexCollisionPath = prop.getProperty(RADEX_COLLISION_PATH,
				RadexConfiguration.getInstance().getCollisionPath());
		RadexConfiguration.getInstance().setCollisionPath(radexCollisionPath);

		radexDataMaxPart = UserConfiguration.getIntProperties(prop,
				RADEX_DATA_MAXPART, ReadRadexFile.getMaxPart());
		radexDataMaxTemp = UserConfiguration.getIntProperties(prop,
				RADEX_DATA_MAXTEMP, ReadRadexFile.getMaxTemp());
		radexDataMaxLev = UserConfiguration.getIntProperties(prop,
				RADEX_DATA_MAXLEV, ReadRadexFile.getMaxLev());
		radexDataMaxLine = UserConfiguration.getIntProperties(prop,
				RADEX_DATA_MAXLINE, ReadRadexFile.getMaxLine());
		radexDataMaxColl = UserConfiguration.getIntProperties(prop,
				RADEX_DATA_MAXCOLL, ReadRadexFile.getMaxColl());
		updateRadexConfig();
	}

	/**
	 * Return the value from the given properties with the given key
	 * and the given default value if there was an exception.
	 *
	 * @param prop The properties to use.
	 * @param key The key to search.
	 * @param defaultValue The default value.
	 * @return The value as an int.
	 */
	private static int getIntProperties(Properties prop, String key, int defaultValue) {
		String tmp = prop.getProperty(key);
		if (tmp == null) {
			return defaultValue;
		} else {
			int value;
			try {
				value = Integer.parseInt(tmp);
			} catch (NumberFormatException nfe) {
				LOGGER.debug("Properties {} is not a number, setting the value to default: {}",
						key, defaultValue, nfe);
				value = defaultValue;
			}
			return value;
		}
	}

	/**
	 * Return the schedule of the update check.
	 *
	 * @return the schedule to the update check.
	 */
	public final UpdateRegular getUpdateRegular() {
		return updateRegular;
	}

	/**
	 * Return the time of the last check for update.
	 *
	 * @return the time of the last check for update.
	 */
	public final String getLastUpdate() {
		return lastUpdate;
	}

	/**
	 * Return the Update path.
	 *
	 * @return the update path.
	 */
	public final String getUpdatePath() {
		return updatePath;
	}

	/**
	 * Set the schedule of the update check.
	 *
	 * @param updateRegular the schedule ofthe update check.
	 */
	public final void setUpdateRegular(UpdateRegular updateRegular) {
		this.updateRegular = updateRegular;
		modeUtil.savePropertiesUserConfiguration(UserConfiguration.UPDATE_REGULAR_PROPERTIE, updateRegular.toString());
	}

	/**
	 * Update the time of the last check for update.
	 *
	 * @param lastUpdate the time of the last check for update.
	 */
	public final void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
		modeUtil.savePropertiesUserConfiguration(UserConfiguration.LAST_UPDATE_PROPERTIE, lastUpdate);
	}

	/**
	 * Return if CASSIS is in test mode.
	 *
	 * @return if CASSIS is in test mode.
	 */
	public final boolean isTestMode() {
		return testMode;
	}

	/**
	 * Return if CASSIS should create or connect to a samp hub on starting.
	 *
	 * @return if CASSIS should create or connect to a samp hub on starting.
	 */
	public boolean isSampOnAutoStart() {
		return sampOnAutoStart;
	}

	/**
	 * Set the new value of the auto start samp : determine if CASSIS should
	 * create a samp hub or connect to an existing one on start automatically.
	 *
	 * @param value true to start samp automatically, false otherwise.
	 */
	public void setSampOnAutoStart(boolean value) {
		sampOnAutoStart = value;
		modeUtil.savePropertiesUserConfiguration(SAMP_AUTO_START_PROPERTY, String.valueOf(value));
	}

	/**
	 * Return the Radex Fortran path.
	 *
	 * @return the Radex Fortran path.
	 */
	public String getRadexFortranPath() {
		return radexFortranPath;
	}

	/**
	 * Set the new value of the Radex Fortran path.
	 *
	 * @param radexFortranPath the Radex Fortran path.
	 */
	public void setRadexFortranPath(String radexFortranPath) {
		this.radexFortranPath = radexFortranPath;
		modeUtil.savePropertiesUserConfiguration(RADEX_FORTRAN_PATH, radexFortranPath);
		RadexConfiguration.getInstance().setPath(radexFortranPath);
	}


	/**
	 * Return the Radex Collision path.
	 *
	 * @return the Radex Collision path.
	 */
	public String getRadexCollisionPath() {
		return radexCollisionPath;
	}


	/**
	 * Set the new value of the Radex Collision path.
	 *
	 * @param radexCollisionPath the Radex Collision path.
	 */
	public void setRadexCollisionPath(String radexCollisionPath) {
		this.radexCollisionPath = radexCollisionPath;
		modeUtil.savePropertiesUserConfiguration(RADEX_COLLISION_PATH, radexCollisionPath);
		RadexConfiguration.getInstance().setCollisionPath(radexCollisionPath);
	}

	/**
	 * Return if CASSIS should open file directly when received from SAMP.
	 *
	 * @return if CASSIS should open file directly when received from SAMP.
	 */
	public boolean isOpenFileDirectly() {
		return openFileDirectly;
	}

	/**
	 * Set the new value of the open file directly functionality.
	 *
	 * @param value the open file directly value.
	 */
	public void setOpenFileDirectly(boolean value) {
		openFileDirectly = value;
		modeUtil.savePropertiesUserConfiguration(OPEN_FILE_DIRECTLY, String.valueOf(value));
	}

	/**
	 * Return if CASSIS should start with the  "mini" database (true), or with
	 *  the configured database (false) on online mode.
	 *
	 * @return the force mini database on online mode properties.
	 */
	public boolean isForceMiniDbOnOnlineMode() {
		return forceMiniDbOnOnlineMode;
	}

	/**
	 * Set the new value of the force database on online mode.
	 * Determine if CASSIS start with "mini" database on online mode (true)
	 * or with the configured database.
	 *
	 * @param force true if CASSIS should start with "mini" database on online mode,
	 *  false if CASSIS should start with the configured database.
	 */
	public void setForceMiniDbOnOnlineMode(boolean force) {
		this.forceMiniDbOnOnlineMode = force;
		modeUtil.savePropertiesUserConfiguration(FORCE_MINI_DB_ON_ONLINE,
				String.valueOf(forceMiniDbOnOnlineMode));
	}

	/**
	 * Read in the given properties files the maximum number of points allowed
	 *  in a tools operation then update it.
	 *
	 * @param prop The Properties used to read the value.
	 */
	private void readUpdateToolsProperties(Properties prop) {
		toolsMaxNbPoints = UserConfiguration.getIntProperties(prop,
				TOOLS_MAX_NB_POINTS_PROPERTY, ToolsUtil.NB_MAX_POINTS_DEFAULT);
		ToolsUtil.setNbMaxPoints(toolsMaxNbPoints);
	}

	/**
	 * Return the maximum number of points allowed in a tools operation from
	 *  CASSIS settings. The actual value on the library can be different.
	 *
	 * @return the maximum number of points allowed in a tools operation.
	 */
	public int getToolsMaxNbPoints() {
		return toolsMaxNbPoints;
	}

	/**
	 * Set the new maximum number of points allowed in a tools operation.
	 *
	 * @param toolsMaxNbPoints The maximum number of points allowed in a tools operation.
	 */
	public void setToolsMaxNbPoints(int toolsMaxNbPoints) {
		this.toolsMaxNbPoints = toolsMaxNbPoints;
		modeUtil.savePropertiesUserConfiguration(TOOLS_MAX_NB_POINTS_PROPERTY,
				String.valueOf(toolsMaxNbPoints));
		ToolsUtil.setNbMaxPoints(toolsMaxNbPoints);
	}

	/**
	 * Return the maximum number of collision partners for collisions files reading from
	 *  CASSIS settings. The actual value on the library can be different.
	 *
	 * @return the maximum number of collision partners for collisions files reading from CASSIS settings.
	 */
	public int getRadexDataMaxPart() {
		return radexDataMaxPart;
	}

	/**
	 * Set the new maximum number of collision partners for collisions files reading.
	 * This only update the value on the CASSIS settings. To update the values on
	 * the Radex lib, call {@link #updateRadexConfig()}.
	 *
	 * @param radexDataMaxPart The maximum number of collision partners on collisions files reading.
	 */
	public void setRadexDataMaxPart(int radexDataMaxPart) {
		this.radexDataMaxPart = radexDataMaxPart;
		modeUtil.savePropertiesUserConfiguration(RADEX_DATA_MAXPART, String.valueOf(radexDataMaxPart));
	}

	/**
	 * Return the maximum number of collision temperatures for collisions files reading from
	 *  CASSIS settings. The actual value on the library can be different.
	 *
	 * @return the maximum number of collision temperatures for collisions files reading from CASSIS settings.
	 */
	public int getRadexDataMaxTemp() {
		return radexDataMaxTemp;
	}

	/**
	 * Set the new maximum number of collision temperatures for collisions files reading.
	 * This only update the value on the CASSIS settings. To update the values on
	 * the Radex lib, call {@link #updateRadexConfig()}.
	 *
	 * @param radexDataMaxTemp The maximum number of collision temperatures on collisions files reading.
	 */
	public void setRadexDataMaxTemp(int radexDataMaxTemp) {
		this.radexDataMaxTemp = radexDataMaxTemp;
		modeUtil.savePropertiesUserConfiguration(RADEX_DATA_MAXTEMP, String.valueOf(radexDataMaxTemp));
	}

	/**
	 * Return the maximum number of energy level for collisions files reading from
	 *  CASSIS settings. The actual value on the library can be different.
	 *
	 * @return the maximum number of energy level for collisions files reading from CASSIS settings.
	 */
	public int getRadexDataMaxLev() {
		return radexDataMaxLev;
	}

	/**
	 * Set the new maximum number of energy level for collisions files reading.
	 * This only update the value on the CASSIS settings. To update the values on
	 * the Radex lib, call {@link #updateRadexConfig()}.
	 *
	 * @param radexDataMaxLev The maximum number of energy level on collisions files reading.
	 */
	public void setRadexDataMaxLev(int radexDataMaxLev) {
		this.radexDataMaxLev = radexDataMaxLev;
		modeUtil.savePropertiesUserConfiguration(RADEX_DATA_MAXLEV, String.valueOf(radexDataMaxLev));
	}

	/**
	 * Return the maximum number of radiative transitions for collisions files reading from
	 *  CASSIS settings. The actual value on the library can be different.
	 *
	 * @return the maximum number of radiative transitions for collisions files reading from CASSIS settings.
	 */
	public int getRadexDataMaxLine() {
		return radexDataMaxLine;
	}

	/**
	 * Set the maximum number of radiative transitions for collisions files reading.
	 * This only update the value on the CASSIS settings. To update the values on
	 * the Radex lib, call {@link #updateRadexConfig()}.
	 *
	 * @param radexDataMaxLine The maximum number of radiative transitions on collisions files reading.
	 */
	public void setRadexDataMaxLine(int radexDataMaxLine) {
		this.radexDataMaxLine = radexDataMaxLine;
		modeUtil.savePropertiesUserConfiguration(RADEX_DATA_MAXLINE, String.valueOf(radexDataMaxLine));
	}

	/**
	 * Return the maximum number of collisional transitions for collisions files reading from
	 *  CASSIS settings. The actual value on the library can be different.
	 *
	 * @return the maximum number of collisional transitions for collisions files reading from CASSIS settings.
	 */
	public int getRadexDataMaxColl() {
		return radexDataMaxColl;
	}

	/**
	 * Set the maximum number of collisional transitions for collisions files reading.
	 * This only update the value on the CASSIS settings. To update the values on
	 * the Radex lib, call {@link #updateRadexConfig()}.
	 *
	 * @param radexDataMaxColl The maximum number of collisional transitions on collisions files reading.
	 */
	public void setRadexDataMaxColl(int radexDataMaxColl) {
		this.radexDataMaxColl = radexDataMaxColl;
		modeUtil.savePropertiesUserConfiguration(RADEX_DATA_MAXCOLL, String.valueOf(radexDataMaxColl));
	}

	/**
	 * Update Radex settings.
	 */
	public void updateRadexConfig() {
		ReadRadexFile.setMaxPart(radexDataMaxPart);
		ReadRadexFile.setMaxTemp(radexDataMaxTemp);
		ReadRadexFile.setMaxLev(radexDataMaxLev);
		ReadRadexFile.setMaxLine(radexDataMaxLine);
		ReadRadexFile.setMaxColl(radexDataMaxColl);
	}

	/**
	 * Return the default rendering to use.
	 *
	 * @return the default rendering to use.
	 */
	public String getRendering() {
		return rendering;
	}

	/**
	 * Change the rendering to use by default.
	 *
	 * @param rendering the rendering to use by default.
	 */
	public void setRendering(Rendering rendering) {
		this.rendering = rendering.name();
		modeUtil.savePropertiesUserConfiguration(RENDERING_PROPERTY, this.rendering);
	}

	/**
	 * Return the user default template to use.
	 *
	 * @return the user default template to use.
	 */
	public String getUserDefaultTemplate() {
		return userDefaultTemplate;
	}

	/**
	 * Change the user default template to use by default.
	 *
	 * @param template the user default template to use by default.
	 */
	public void setUserDefaultTemplate(String template) {
		this.userDefaultTemplate = template;
		modeUtil.savePropertiesUserConfiguration(
				TEMPLATE_PROPERTY, this.userDefaultTemplate);
	}

	/**
	 * Sets if the fit configuration should be automatically estimated
	 * from the lines when available
	 *
	 * @param fitAutoEstimate a boolean to automatic or not the estimation
	 */
	public void setFitAutoEstimate(boolean fitAutoEstimate) {
		this.fitAutoEstimate = fitAutoEstimate;
		modeUtil.savePropertiesUserConfiguration(FIT_AUTO_ESTIMATE, String.valueOf(this.fitAutoEstimate));
	}

	/**
	 * @return True if the fit configuration should be automatically estimated
	 * from the lines when available
	 */
	public boolean isFitAutoEstimate() {
		return fitAutoEstimate;
	}

	/**
	 * Sets if the result of the fit should be automatically saved
	 *
	 * @param fitAutoSave a boolean to automatic or not the saving
	 */
	public void setFitAutoSave(boolean fitAutoSave) {
		this.fitAutoSave = fitAutoSave;
		modeUtil.savePropertiesUserConfiguration(FIT_AUTO_SAVE, String.valueOf(this.fitAutoSave));
	}

	/**
	 * @return True if the result of the fit must be automatically saved after each fit
	 */
	public boolean isFitAutoSave() {
		return fitAutoSave;
	}

	/**
	 * Sets the default component to use when estimating a fit configuration
	 * from a set of lines
	 *
	 * @param fitDefaultType the default fit component
	 */
	public void setFitDefaultType(FitType fitDefaultType) {
		this.fitDefaultType = fitDefaultType;
		modeUtil.savePropertiesUserConfiguration(FIT_DEFAULT_COMPONENT, String.valueOf(this.fitDefaultType));
	}

	/**
	 * @return The default component to use when estimating a fit configuration
	 * from a set of lines
	 */
	public FitType getFitDefaultType() {
		return fitDefaultType;
	}

	/**
	 * Return if the other species is displayed without click on Display Button.
	 *
	 * @return if the other species is displayed without click on Display Button.
	 */
	public boolean isAutoOtherSpeciesDisplayed() {
		return autoOtherSpeciesDisplayed;
	}



	/**
	 * Set the new value of the auto start samp : determine if CASSIS should
	 * create a samp hub or connect to an existing one on start automatically.
	 *
	 * @param value true to start samp automatically, false otherwise.
	 */
	public void setAutoOtherSpeciesDisplayed(boolean value) {
		autoOtherSpeciesDisplayed = value;
		modeUtil.savePropertiesUserConfiguration(AUTO_OTHER_SPECIES_DISPLAYED, String.valueOf(value));
	}

}
