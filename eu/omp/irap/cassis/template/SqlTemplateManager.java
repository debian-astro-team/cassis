/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.cassis.template;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.omp.irap.cassis.common.Molecule;
import eu.omp.irap.cassis.common.MoleculeDescription;
import eu.omp.irap.cassis.common.Template;
import eu.omp.irap.cassis.database.access.AccessDataBase;
import eu.omp.irap.cassis.database.access.DataBaseConnection;
import eu.omp.irap.cassis.database.access.SimpleMoleculeDescriptionDB;
import eu.omp.irap.cassis.gui.util.FileUtils;
import eu.omp.irap.cassis.properties.Software;

public class SqlTemplateManager {

	private static final Logger LOGGER = LoggerFactory.getLogger(SqlTemplateManager.class);
	private static SqlTemplateManager templateManager;


	public static SqlTemplateManager getInstance() {
		if (templateManager == null)
			templateManager = new SqlTemplateManager();
		return templateManager;
	}

	public List<String> getTemplatesName() {
		File file = new File(Software.getTemplateEnablePath());
		String[] list2 = file.list();
		List<String> templates = getFullTemplatesName();
		if (list2 != null) {
			for (int i = 0; i < list2.length; i++) {
				if (list2[i].endsWith(".tec") && !list2[i].startsWith("Full ")) {
					templates.add(list2[i].replaceFirst("template", "").replace(".tec", ""));
				}
			}
		}
		return templates;
	}

	/**
	 * Return the List of molecule of the given template.
	 *
	 * @param template The template name.
	 * @return The list of molecule of the given template. Return an empty list
	 *  if there is no line in the template file or if first line doesn't begin
	 *  with '@'.
	 */
	public List<MoleculeDescription> getTemplateMolecules(String template) {
		if (Template.FULL_TEMPLATE.equals(template)) {
			List<MoleculeDescription> list = getAllMolecules();
			String col;
			for (MoleculeDescription mol : list) {
				if (Software.getRadexMoleculesManager().isRADEXMolecules(Integer.toString(mol.getTag()))) {
					col = Software.getRadexMoleculesManager().getCollisionParDefault(mol.getTag());
				} else {
					col = Molecule.NO_COLLISION;
				}
				mol.setCollision(col);
			}
			return list;
		} else if (isFullTemplate(template)) {
			return getFullTemplateMolecule(template);
		} else {
			File file = new File(Software.getTemplateEnablePath() +
					File.separator + "template" + template + ".tec");
			return getMoleculeFromTemplateFile(file);
		}
	}

	public List<MoleculeDescription> getMoleculeFromTemplateFile(File file) {
		List<MoleculeDescription> moleculeList = new ArrayList<>();
		try (BufferedReader in = new BufferedReader(new FileReader(file))) {
			String line = in.readLine();

			if (line == null || !line.startsWith("@")) {
				LOGGER.warn("The template file {} is invalide", file.getAbsolutePath());
				return new ArrayList<>();
			}

			while ((line = in.readLine()) != null) {
				if (!line.startsWith("//")) {
					String[] lineDescription = line.split("\\t");
					final int tag = Integer.parseInt(lineDescription[0]);
					String name = AccessDataBase.getDataBaseConnection().getMolName(tag);
					if (DataBaseConnection.NOT_IN_DATABASE.equals(name))
						continue;

					moleculeList.add(SqlTemplateManager.createMoleculeDescription(tag,
							name, lineDescription[1], Double.valueOf(lineDescription[3]),
							Double.valueOf(lineDescription[4]), Double.valueOf(lineDescription[5]),
							Double.valueOf(lineDescription[6]), Double.valueOf(lineDescription[7]),
							Double.valueOf(lineDescription[8]), Double.valueOf(lineDescription[9]),
							Double.valueOf(lineDescription[10])));
				}
			}
		} catch (NumberFormatException | IOException e) {
			LOGGER.error("The template file {} is invalide", file.getAbsolutePath(), e);
		}
		return moleculeList;
	}

	private List<MoleculeDescription> getAllMolecules() {
		List<MoleculeDescription> moleculeList = new ArrayList<>();
		List<SimpleMoleculeDescriptionDB> allMoleculeDescriptionDB =
				AccessDataBase.getDataBaseConnection().getAllMoleculeDescriptionDB();
		for (SimpleMoleculeDescriptionDB simpleMolDescDB : allMoleculeDescriptionDB) {
			MoleculeDescription mol = new MoleculeDescription();
			mol.setTag(simpleMolDescDB.getTag());
			mol.setName(simpleMolDescDB.getName());
			mol.setMolecularMass(Double.valueOf(simpleMolDescDB.getTag() /1000));
			if (simpleMolDescDB.getSource() != null) {
				mol.setDataSource(simpleMolDescDB.getSource());
			}
			moleculeList.add(mol);
		}
		return moleculeList;
	}

	public void exportTemplate(String template, File file) throws IOException {
		if (Template.FULL_TEMPLATE.equals(template)) {
			List<MoleculeDescription> molecules = SqlTemplateManager.getInstance()
					.getTemplateMolecules(template);
			createTemplate(template, molecules, file);
		} else {
			FileUtils.copy(new File(Software.getTemplateEnablePath() +
				File.separator + "template" + template + ".tec"), file);
		}
	}

	public boolean isTemplateExist(String templateName) {
		return getTemplatesName().contains(templateName);
	}

	public boolean createTemplate(String templateName,
			List<MoleculeDescription> moleculesSelected, File file) {
		MoleculeDescription moleculeDescription;
		String moleculeColl;
		try (BufferedWriter out = new BufferedWriter(new FileWriter(file))) {
			// Write the first line to describe the template name
			out.write("@" + templateName + "\n");

			// Write the second line to describe the column headers
			out.write("//Tag\tDB\tColl\tC-Dens\tAbun\tbeta\tTex\tTKin\tFwhm\tSize\tV0\n");

			// Write the molecule description for all chosen templates
			for (int i = 0; i < moleculesSelected.size(); i++) {
				moleculeDescription = moleculesSelected.get(i);

				if (Software.getRadexMoleculesManager().isRADEXMolecules(Integer.toString(moleculeDescription.getTag()))) {
					moleculeColl = Software.getRadexMoleculesManager().getCollisionParDefault(moleculeDescription.getTag());
				} else {
					moleculeColl = Molecule.NO_COLLISION;
				}

				String description = String.valueOf(moleculeDescription.getTag()) + "\t"
						+ moleculeDescription.getDataSource() + "\t" + moleculeColl + "\t"
						+ moleculeDescription.getDensity() + "\t"
						+ moleculeDescription.getRelativeAbundance() + "\t"
						+ moleculeDescription.getBeta() + "\t"
						+ moleculeDescription.getTemperature() + "\t"
						+ moleculeDescription.getTKin() + "\t"
						+ moleculeDescription.getVelocityDispersion() + "\t"
						+ moleculeDescription.getSourceSize() + "\t" + moleculeDescription.getVexp() + "\n";

				out.write(description);
				out.flush();
			}
			LOGGER.info("Export template {} to file {}", templateName, file.getAbsolutePath());
		} catch (IOException e) {
			LOGGER.error("Error while creating a template at {}", file.getAbsolutePath(), e);
			return false;
		}
		LOGGER.info("Template {} sucessfully created", templateName);
		return true;
	}

	public boolean createTemplate(String templateName,
			List<MoleculeDescription> moleculesSelected) {
		File fileName = new File(Software.getTemplateEnablePath() +
				File.separator + "template" + templateName + ".tec");
		return createTemplate(templateName, moleculesSelected, fileName);
	}

	public String importTemplate(File templateFile) throws IOException {
		try (BufferedReader in = new BufferedReader(new FileReader(templateFile))) {
			String line = in.readLine();

			if (line == null || !line.startsWith("@")) {
				LOGGER.info("File " + templateFile.toString() + " invalide");
				return null;
			}

			String templateName = line.substring(1);
			FileUtils.copy(templateFile, new File(Software.getTemplateEnablePath() +
					File.separator + "template" + templateName + ".tec"));
			return templateName;
		}
	}

	public boolean deleteTemplate(String template) {
		File file = new File(Software.getTemplateEnablePath() +
				File.separator + "template" + template + ".tec");
		return file.delete();
	}

	/**
	 * Return all "sources" from the current database.
	 *
	 * @return all "sources" from the current database.
	 */
	public Set<String> getAllSources() {
		Set<String> sourceList = new HashSet<>(4);
		List<SimpleMoleculeDescriptionDB> allMoleculeDescriptionDB =
				AccessDataBase.getDataBaseConnection().getAllMoleculeDescriptionDB();

		for (SimpleMoleculeDescriptionDB mol : allMoleculeDescriptionDB) {
			if (!sourceList.contains(mol.getSource())) {
				sourceList.add(mol.getSource());
			}
		}
		return sourceList;
	}

	/**
	 * Return the names of the full templates.
	 *
	 * @return the names of the full templates.
	 */
	private List<String> getFullTemplatesName() {
		List<String> fullTemplate = new ArrayList<>();
		fullTemplate.add(Template.FULL_TEMPLATE);
		Set<String> sources = getAllSources();
		for (String source : sources) {
			fullTemplate.add("Full " + source);
		}
		return fullTemplate;
	}

	/**
	 * Return if the given template name is a full template (start with "Full ").
	 * @param templateName The name of the template to test.
	 * @return true if this is a full template, false otherwise.
	 */
	private boolean isFullTemplate(String templateName) {
		return templateName != null && templateName.startsWith("Full ");
	}

	/**
	 * Return the list of {@link MoleculeDescription} for the given full template.
	 *
	 * @param fullTemplateName The full template name.
	 * @return The list of {@link MoleculeDescription}.
	 */
	private List<MoleculeDescription> getFullTemplateMolecule(String fullTemplateName) {
		List<MoleculeDescription> moleculeList = new ArrayList<>();
		String nameDb = fullTemplateName.replaceFirst("Full ", "");
		for (SimpleMoleculeDescriptionDB mol :
			AccessDataBase.getDataBaseConnection().getAllMoleculeDescriptionDB()) {
			if (!mol.getSource().equals(nameDb)) {
				continue;
			}
			moleculeList.add(SqlTemplateManager.createMoleculeDescription(
					mol.getTag(), mol.getName(), mol.getSource(), 7.0E14,
					7E14 / 7.5E22, 2.0E-5, 35.0, 35.0, 1.0, 3.0, 0.0));
		}
		return moleculeList;
	}

	/**
	 * Create a {@link MoleculeDescription} with the given parameters.
	 *
	 * @param tag The tag.
	 * @param name The name.
	 * @param source The source.
	 * @param density The density.
	 * @param relativeAbundance The relative abundance.
	 * @param beta The beta.
	 * @param temperature The temperature.
	 * @param tKin The kinetic temperature.
	 * @param velocityDispersion The velocity.
	 * @param sourceSize The source size.
	 * @param vexp The vo.
	 * @return The created {@link MoleculeDescription}.
	 */
	private static MoleculeDescription createMoleculeDescription(int tag,
			String name, String source, double density, double relativeAbundance,
			double beta, double temperature, double tKin, double velocityDispersion,
			double sourceSize, double vexp) {
		MoleculeDescription mol = new MoleculeDescription();
		mol.setTag(tag);
		mol.setName(name);
		mol.setMolecularMass(tag / 1000.0);
		mol.setDataSource(source);
		mol.setDensity(density);
		mol.setRelativeAbundance(relativeAbundance);
		mol.setBeta(beta);
		mol.setTemperature(temperature);
		mol.setTKin(tKin);
		mol.setVelocityDispersion(velocityDispersion);
		mol.setSourceSize(sourceSize);
		mol.setVexp(vexp);
		mol.setCompute(false);
		if (Software.getRadexMoleculesManager().isRADEXMolecules(String.valueOf(tag))) {
			mol.setCollision(Software.getRadexMoleculesManager().getCollisionParDefault(tag));
		} else {
			mol.setCollision(Molecule.NO_COLLISION);
		}
		return mol;
	}
}
