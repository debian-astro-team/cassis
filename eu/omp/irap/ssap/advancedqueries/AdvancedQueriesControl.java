/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.advancedqueries;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.JDialog;

import eu.omp.irap.ssap.QueryInterface;
import eu.omp.irap.ssap.request.RequestData;
import eu.omp.irap.ssap.util.UtilFunction;
import eu.omp.irap.ssap.value.Constants;

/**
 * Control for the Advanced Queries functionnality.
 *
 * @author M. Boiziot
 */
public class AdvancedQueriesControl {

	private AdvancedQueriesModel model;
	private AdvancedQueriesView view;
	private JDialog dialog;
	private QueryInterface queryInterface;


	/**
	 * Constructor.
	 *
	 * @param data The data used.
	 * @param queryInterface The interface used for doing the query.
	 */
	public AdvancedQueriesControl(List<RequestData> data, QueryInterface queryInterface) {
		this.queryInterface = queryInterface;
		this.model = new AdvancedQueriesModel(data);
		this.view = new AdvancedQueriesView(this);
	}

	/**
	 * Return the model.
	 *
	 * @return the model.
	 */
	public AdvancedQueriesModel getModel() {
		return model;
	}

	/**
	 * Close the window then call the interface to do the query.
	 */
	public void doQuery() {
		view.getTable().validateChange();
		List<RequestData> requestDataList = model.getRequestDataList();
		exit();
		queryInterface.doQueries(requestDataList);
	}

	/**
	 * Copy the finals URLs to the clipboard.
	 */
	public void copyUrls() {
		List<RequestData> requestDataList = model.getRequestDataList();
		if (!requestDataList.isEmpty()) {
			StringBuilder sb = new StringBuilder();
			for (RequestData rd : requestDataList) {
				sb.append(rd.getUrl()).append(Constants.NEW_LINE);
			}
			UtilFunction.copyToClipboard(sb.toString());
		}
	}

	/**
	 * Copy URLs from selected services to the clipboard.
	 */
	public void copySelectedUrls() {
		List<String> listUrl = view.getTable().getSelectedUrls();
		if (!listUrl.isEmpty()) {
			StringBuilder sb = new StringBuilder();
			for (String url : listUrl) {
				sb.append(url).append(Constants.NEW_LINE);
			}
			UtilFunction.copyToClipboard(sb.toString());
		}
	}

	/**
	 * Reset user specified URLs.
	 */
	public void resetUrls() {
		boolean change = false;
		for (RequestData rd : model.getRequestDataList()) {
			if (rd.haveSavedAdvancedUrl()) {
				rd.resetSavedAdvancedUrl();
				change = true;
			}
		}
		if (change) {
			model.getTableModel().fireTableDataChanged();
		}
	}

	/**
	 * Close the window.
	 */
	public void exit() {
		if (dialog != null) {
			dialog.dispose();
		}
	}

	/**
	 * Display the view in the provided {@link JDialog}.
	 *
	 * @param dialog The dialog in which we display the view.
	 */
	public void show(JDialog dialog) {
		if (this.dialog != null) {
			throw new IllegalStateException("There is already a JDialog.");
		}
		this.dialog = dialog;

		dialog.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				AdvancedQueriesControl.this.exit();
			}
		});
		dialog.setTitle("Advanced Queries");
		dialog.setContentPane(view);
		dialog.setModalityType(JDialog.DEFAULT_MODALITY_TYPE);
		dialog.setSize(view.getPreferredSize());
		dialog.setLocationRelativeTo(null);
		dialog.setVisible(true);
	}
}
