/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.advancedqueries;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;


/**
 * View for the Advanced Queries functionnality.
 *
 * @author M. Boiziot
 */
public class AdvancedQueriesView extends JPanel {

	private static final long serialVersionUID = -4690192456422970032L;
	private AdvancedQueriesControl control;
	private JButton cancelButton;
	private JButton queryButton;
	private JButton copyButton;
	private JButton copyAllButton;
	private JButton resetButton;
	private AdvancedQueriesTable table;


	/**
	 * Constructor.
	 *
	 * @param control The controller.
	 */
	public AdvancedQueriesView(AdvancedQueriesControl control) {
		super(new BorderLayout());
		this.control = control;
		this.setPreferredSize(new Dimension(900, 530));

		JScrollPane scrollPane = new JScrollPane(getTable());
		scrollPane.setPreferredSize(new Dimension(900, 500));
		this.add(scrollPane, BorderLayout.CENTER);

		JPanel buttonPanel = new JPanel();
		buttonPanel.add(getCancelButton());
		buttonPanel.add(getCopyButton());
		buttonPanel.add(getCopyAllButton());
		buttonPanel.add(getResetButton());
		buttonPanel.add(getQueryButton());
		this.add(buttonPanel, BorderLayout.SOUTH);
	}

	/**
	 * Create if needed then return the Cancel {@link JButton}.
	 *
	 * @return the Cancel button.
	 */
	public JButton getCancelButton() {
		if (cancelButton == null) {
			cancelButton = new JButton("Cancel");
			cancelButton.setToolTipText("Close the window.");
			cancelButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.exit();
				}
			});
		}
		return cancelButton;
	}
	/**
	 * Create if needed then return the Copy {@link JButton}.
	 *
	 * @return the Copy button.
	 */
	public JButton getCopyButton() {
		if (copyButton == null) {
			copyButton = new JButton("Copy");
			copyButton.setToolTipText("Copy URLs from selected services to the clipboard.");
			copyButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.copySelectedUrls();
				}
			});
		}
		return copyButton;
	}

	/**
	 * Create if needed then return the Copy All {@link JButton}.
	 *
	 * @return the Copy All button.
	 */
	public JButton getCopyAllButton() {
		if (copyAllButton == null) {
			copyAllButton = new JButton("Copy All");
			copyAllButton.setToolTipText("Copy all URLs to the clipboard.");
			copyAllButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.copyUrls();
				}
			});
		}
		return copyAllButton;
	}

	/**
	 * Create if needed then return the Query {@link JButton}.
	 *
	 * @return the Query button.
	 */
	public JButton getQueryButton() {
		if (queryButton == null) {
			queryButton = new JButton("Query");
			queryButton.setToolTipText("Submit query.");
			queryButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.doQuery();
				}
			});
		}
		return queryButton;
	}

	/**
	 * Create if needed then return the Reset {@link JButton}.
	 *
	 * @return the Reset button.
	 */
	public JButton getResetButton() {
		if (resetButton == null) {
			resetButton = new JButton("Reset");
			resetButton.setToolTipText("Reset all user specified URLs.");
			resetButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.resetUrls();
				}
			});
		}
		return resetButton;
	}

	/**
	 * Create if needed then return the table.
	 *
	 * @return the table
	 */
	public AdvancedQueriesTable getTable() {
		if (table == null) {
			table = new AdvancedQueriesTable(control.getModel().getTableModel());
			table.setFillsViewportHeight(true);
		}
		return table;
	}
}
