/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.advancedqueries;

import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import eu.omp.irap.ssap.request.RequestData;

/**
 * JTable for {@link RequestData}.
 *
 * @author M. Boiziot
 */
public class AdvancedQueriesTable extends JTable {

	private static final long serialVersionUID = 2909337938317497804L;


	/**
	 * Constructor.
	 *
	 * @param model The model.
	 */
	public AdvancedQueriesTable(AdvancedQueriesTableModel model) {
		super(model);
		getTableHeader().setReorderingAllowed(false);
		setShowGrid(true);
		updateColumnsSize();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * javax.swing.JTable#prepareRenderer(javax.swing.table.TableCellRenderer,
	 * int, int)
	 */
	@Override
	public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
		Component c = super.prepareRenderer(renderer, row, column);
		boolean selected = isCellSelected(row, column);
		if (selected) {
			c.setBackground(new Color(184, 207, 229));
		} else {
			AdvancedQueriesTableModel model = (AdvancedQueriesTableModel) getModel();
			boolean userSpecificUrl = model.getData().get(row).haveSavedAdvancedUrl();
			if (userSpecificUrl) {
				c.setBackground(new Color(250, 235, 240));
			} else {
				if (row % 2 == 0) {
					c.setBackground(new Color(240, 250, 250));
				} else {
					c.setBackground(getBackground());
				}
			}
		}
		return c;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see javax.swing.JTable#getScrollableTracksViewportWidth()
	 */
	@Override
	public boolean getScrollableTracksViewportWidth() {
		return getPreferredSize().width < getParent().getWidth();
	}

	/**
	 * Update the columns size.
	 */
	private void updateColumnsSize() {
		int width;
		Component comp;
		for (int column = 0; column < getColumnCount(); column++) {
			comp = getTableHeader().getDefaultRenderer().getTableCellRendererComponent(this,
					getColumnModel().getColumn(column).getHeaderValue(), false, false, 0, 0);
			width = comp.getPreferredSize().width;
			for (int row = 0; row < getRowCount(); row++) {
				TableCellRenderer renderer = getCellRenderer(row, column);
				comp = prepareRenderer(renderer, row, column);
				width = Math.max(comp.getPreferredSize().width, width);
			}
			// Add 12 for margin.
			width += 12;
			getColumnModel().getColumn(column).setPreferredWidth(width);
		}
	}

	/**
	 * Return list of URL from selected rows.
	 *
	 * @return list of URL from selected rows.
	 */
	public List<String> getSelectedUrls() {
		int[] selectedRows = getSelectedRows();
		List<String> listUrl = new ArrayList<>(selectedRows.length);
		for (int row : selectedRows) {
			listUrl.add((String)getModel().getValueAt(row,
							AdvancedQueriesTableModel.QUERY_INDEX));
		}
		return listUrl;
	}

	/**
	 * Validate possible ongoing change on the table.
	 */
	public void validateChange() {
		TableCellEditor editor = getCellEditor();
		if (editor != null) {
			editor.stopCellEditing();
		}
	}
}
