/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.util;

import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Simple util function.
 *
 * @author M. Boiziot
 */
public final class UtilFunction {

	private static final Logger LOGGER = Logger.getLogger("UtilFunction");


	private UtilFunction() {
	}


	/**
	 * Format an URL ready for adding parameters for doing a request.
	 *
	 * @param url The base URL.
	 * @return The formated URL.
	 * @throws IllegalArgumentException if the given URL is invalid.
	 */
	public static String formatUrlForParameters(String url) throws IllegalArgumentException {
		if (url == null || url.isEmpty()) {
			throw new IllegalArgumentException("The provided URL is bad.");
		}
		String returnUrl = url;
		if (!url.contains("?")) {
			returnUrl += "?";
		} else if (url.charAt(url.length()-1) != '?' &&
				url.charAt(url.length()-1) != '&') {
			returnUrl += "&";
		}
		return returnUrl;
	}

	/**
	 * Return the extension of a file.
	 *
	 * @param filePath the path of the file.
	 * @return the extension.
	 */
	public static String getFileExtension(String filePath) {
		File tmpFile = new File(filePath);
		int dotIndex = tmpFile.getName().lastIndexOf('.');
		if (dotIndex > 0 && dotIndex <= tmpFile.getName().length() - 2) {
			return tmpFile.getName().substring(dotIndex + 1);
		}
		return "";
	}

	/**
	 * Check if each element of a {@link List} of {@link String} is a number.
	 *
	 * @param list The list to check.
	 * @return if each element of the list is a number.
	 */
	public static boolean isAllNumber(List<String> list) {
		@SuppressWarnings("unused")
		double tmpDouble;
		for (String st : list) {
			try {
				tmpDouble = Double.parseDouble(st);
			} catch (NumberFormatException nfe) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Copy some text to the system clipboard.
	 *
	 * @param text The text to copy to the system clipboard.
	 */
	public static void copyToClipboard(String text) {
		try {
			StringSelection selection = new StringSelection(text);
			Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			clipboard.setContents(selection, null);
		} catch (IllegalStateException ise) {
			LOGGER.log(Level.WARNING, "Can not copy to the clipboard.");
		}
	}

	/**
	 * Disconnect a connection.
	 *
	 * @param connection The connection to disconnect.
	 */
	public static void disconnect(HttpURLConnection connection) {
		if (connection != null) {
			connection.disconnect();
		}
	}

	/**
	 * Close a closeable if not null and handle Exception.
	 *
	 * @param closeable The object to close.
	 */
	public static void close(Closeable closeable) {
		if (closeable != null) {
			try {
				closeable.close();
			} catch (IOException ioe) {
				// Nothing to do here.
			}
		}
	}

	/**
	 * Check then return if a searched value if in an String. This ignore case.
	 *
	 * @param string The String to search in.
	 * @param searchValue The searched value.
	 * @return true if the searched value is in the searched String,
	 *  false otherwise.
	 */
	public static boolean containsIgnoreCase(String string, String searchValue) {
		return string != null && !string.isEmpty() &&
				string.toLowerCase(Locale.US).contains(
						searchValue.toLowerCase(Locale.US));
	}

	/**
	 * Check then return if a searched value if in a List of String. This ignore
	 *  case.
	 *
	 * @param strings The List of String to to search in.
	 * @param searchValue The searched value.
	 * @return true if the searched value is at least once in the given list of
	 *  String, false otherwise.
	 */
	public static boolean containsIgnoreCase(List<String> strings,
			String searchValue) {
		if (strings != null && !strings.isEmpty()) {
			for (String searchedString : strings) {
				if (UtilFunction.containsIgnoreCase(searchedString, searchValue)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Return a {@link Rectangle} describing the screen on which is a provided {@link Window}.
	 * If the window is between two screen, the first one is chosen.
	 *
	 * @param window The window for which we want screen rectangle.
	 * @return The rectangle describing the screen on which is the window.
	 */
	public static Rectangle getScreenRectangle(Window window) {
		GraphicsDevice[] screens = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();
		if (window != null) {
			for (int i = 0; i < screens.length; i++) {
				GraphicsConfiguration screenConfig = screens[i].getDefaultConfiguration();
				Rectangle rectangle = screenConfig.getBounds();
				if (rectangle.contains(window.getLocation())) {
					return rectangle;
				}
			}
		}
		return screens[0].getDefaultConfiguration().getBounds();
	}

	/**
	 * Center a {@link Window} on the screen where is a reference {@link Window}.
	 * If the reference window is null, the JFrame to center is centered on the first screen.
	 *
	 * @param windowRef The reference window.
	 * @param windowToCenter The window to center.
	 */
	public static void center(Window windowRef, Window windowToCenter) {
		Rectangle r = UtilFunction.getScreenRectangle(windowRef);
		int widthFrame = windowToCenter.getWidth();
		int heightFrame = windowToCenter.getHeight();
		int x = r.x + (r.width / 2) - (widthFrame / 2);
		int y = r.y + (r.height / 2) - (heightFrame / 2);
		windowToCenter.setLocation(x, y);
	}
}
