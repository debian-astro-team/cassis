/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.util.jtable;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

/**
 * Cell Editor for a Values, hightly based on the code of DefaultCellEditor.
 *
 * @author M. Boiziot
 */
public class ValuesEditor extends AbstractCellEditor implements TableCellEditor {

	private static final long serialVersionUID = -2710331686170353418L;
	private JComboBox<String> jcb;
	private DefaultComboBoxModel<String> model;
	protected int clickCountToStart = 1;
	private EditorDelegate delegate;


	/**
	 * Constructor.
	 */
	public ValuesEditor() {
		jcb = new JComboBox<>();
		jcb.setEditable(true);
		model = (DefaultComboBoxModel<String>) jcb.getModel();
		jcb.putClientProperty("JComboBox.isTableCellEditor", Boolean.TRUE);
		delegate = new EditorDelegate();
		jcb.addActionListener(delegate);
	}

	/* (non-Javadoc)
	 * @see javax.swing.CellEditor#getCellEditorValue()
	 */
	@Override
	public Object getCellEditorValue() {
		return delegate.getCellEditorValue();
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.TableCellEditor#getTableCellEditorComponent(javax.swing.JTable, java.lang.Object, boolean, int, int)
	 */
	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		Values values = (Values) value;
		model.removeAllElements();
		if (values.getList() != null) {
			for (String val : values.getList()) {
				model.addElement(val);
			}
		}
		delegate.setValue(values.getSelected());
		model.setSelectedItem(values.getSelected());
		return jcb;
	}

	/* (non-Javadoc)
	 * @see javax.swing.AbstractCellEditor#isCellEditable(java.util.EventObject)
	 */
	@Override
	public boolean isCellEditable(EventObject anEvent) {
		return delegate.isCellEditable(anEvent);
	}

	/* (non-Javadoc)
	 * @see javax.swing.AbstractCellEditor#shouldSelectCell(java.util.EventObject)
	 */
	@Override
	public boolean shouldSelectCell(EventObject anEvent) {
		return delegate.shouldSelectCell(anEvent);
	}

	/* (non-Javadoc)
	 * @see javax.swing.AbstractCellEditor#stopCellEditing()
	 */
	@Override
	public boolean stopCellEditing() {
		return delegate.stopCellEditing();
	}

	/* (non-Javadoc)
	 * @see javax.swing.AbstractCellEditor#cancelCellEditing()
	 */
	@Override
	public void cancelCellEditing() {
		delegate.cancelCellEditing();
	}


	private class EditorDelegate implements ActionListener, ItemListener, Serializable {

		private static final long serialVersionUID = 1L;

		/**
		 * Returns the value of this cell.
		 *
		 * @return the value of this cell
		 */
		public Object getCellEditorValue() {
			return jcb.getEditor().getItem();
		}

		/**
		 * Sets the value of this cell.
		 *
		 * @param value
		 *            the new value of this cell
		 */
		public void setValue(Object value) {
			if (value instanceof Values) {
				jcb.setSelectedItem(((Values) value).getSelected());
			} else {
				jcb.setSelectedItem(value);
			}
		}

		/**
		 * Returns true if <code>anEvent</code> is <b>not</b> a
		 * <code>MouseEvent</code>. Otherwise, it returns true if the necessary
		 * number of clicks have occurred, and returns false otherwise.
		 *
		 * @param anEvent
		 *            the event
		 * @return true if cell is ready for editing, false otherwise
		 */
		public boolean isCellEditable(EventObject anEvent) {
			if (anEvent instanceof MouseEvent) {
				return ((MouseEvent) anEvent).getClickCount() >= clickCountToStart;
			}
			return true;
		}

		/**
		 * Returns true to indicate that the editing cell may be selected.
		 *
		 * @param anEvent
		 *            the event
		 * @return true
		 * @see #isCellEditable
		 */
		public boolean shouldSelectCell(EventObject anEvent) {
			if (anEvent instanceof MouseEvent) {
				MouseEvent e = (MouseEvent) anEvent;
				return e.getID() != MouseEvent.MOUSE_DRAGGED;
			}
			return true;
		}

		/**
		 * Stops editing and returns true to indicate that editing has stopped.
		 * This method calls <code>fireEditingStopped</code>.
		 *
		 * @return true
		 */
		public boolean stopCellEditing() {
			if (jcb.isEditable()) {
				// Commit edited value.
				jcb.actionPerformed(new ActionEvent(ValuesEditor.this, 0, ""));
			}
			fireEditingStopped();
			return true;
		}

		/**
		 * Cancels editing. This method calls <code>fireEditingCanceled</code>.
		 */
		public void cancelCellEditing() {
			fireEditingCanceled();
		}

		/**
		 * When an action is performed, editing is ended.
		 *
		 * @param e the action event
		 * @see #stopCellEditing
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			ValuesEditor.this.stopCellEditing();
		}

		/**
		 * When an item's state changes, editing is ended.
		 *
		 * @param e
		 *            the action event
		 * @see #stopCellEditing
		 */
		@Override
		public void itemStateChanged(ItemEvent e) {
			ValuesEditor.this.stopCellEditing();
		}
	}
}
