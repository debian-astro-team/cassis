/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Class to manipulate positions.
 *
 * @author M. Boiziot
 */
public class Position {

	private static final NumberFormat HHMM_FORMAT = new DecimalFormat("00");
	private static final NumberFormat DECIMAL_FORMAT = new DecimalFormat("00.0##", new DecimalFormatSymbols(Locale.US));

	/* Position type */
	public static final int RA = 0;
	public static final int DEC = 1;

	/* Position writing type */
	public static final int INVALID = 0;
	public static final int DEGREES_DEC = 1;
	public static final int SEXAGESIMAL_DEC = 2;
	public static final int SEXAGESIMAL_HOUR = 3;


	/**
	 * Utility class, do not use.
	 */
	private Position() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * Convert a position to decimal degrees (as needed for SSA).
	 *
	 * @param currentPosition The current position.
	 * @param type The type of position (RA or DEC).
	 * @return The position in decimal degrees.
	 */
	public static String convertToDegrees(String currentPosition, int type) {
		int currentWritingType = getPositionWriting(currentPosition, type);
		String degreesPos = null;

		switch (currentWritingType) {
			case DEGREES_DEC:
				degreesPos = currentPosition;
				break;

			case SEXAGESIMAL_DEC:
			case SEXAGESIMAL_HOUR:
				degreesPos = convertSexagesimalToDegrees(currentPosition, currentWritingType);
				break;

			case INVALID:
			default:
				throw new IllegalArgumentException("This is not a valid position.");
		}
		return degreesPos;
	}

	/**
	 * Convert position to decimal degrees
	 *
	 * @param position The position to convert.
	 * @param type The origin type (must be SEXAGESIMAL_DEC or SEXAGESIMAL_HOUR)
	 * @return The position in decimal degrees.
	 */
	private static String convertSexagesimalToDegrees(String position, int type) {
		String degreesPos;
		String[] split = position.split(":");
		boolean negativeValue = false;

		if (split[0].charAt(0) == '+') {
			split[0] = split[0].substring(1);
		} else if (split[0].charAt(0) == '-') {
			negativeValue = true;
		}

		int xx = Integer.parseInt(split[0]);
		int mm = Integer.parseInt(split[1]);
		double ss = Double.parseDouble(split[2]);

		double x = negativeValue ? xx - ((mm + (ss / 60.)) / 60.) : xx + ((mm + (ss / 60.)) / 60.);

		if (type == SEXAGESIMAL_HOUR) {
			x *= 15.;
		}
		degreesPos = String.valueOf(x);
		return degreesPos;
	}

	/**
	 * Convert a position to sexagesimal degrees.
	 *
	 * @param degreesPosition The position in degrees.
	 * @param truncated if the result position is truncated.
	 * @return The position in sexagesimal degrees.
	 */
	public static String convertToSexagesimalDec(String degreesPosition, boolean truncated) {
		if (!isDecimalDegreesPosition(degreesPosition, DEC)) {
			throw new IllegalArgumentException("This is not a valid DEC decimal degrees position.");
		}
		double degreesDec = Double.parseDouble(degreesPosition);

		double x = degreesDec;

		int xx = (int) x;
		double tmp = Math.abs((x - xx) * 60.);
		int mm = (int) Math.abs(tmp);
		double ss = (tmp - mm) * 60.;

		String tmpString = Position.createSexagesimalPositionString(xx, mm, ss, truncated);
		if (xx >= 0) {
			tmpString = '+' + tmpString;
		}
		return tmpString;
	}

	/**
	 * Convert a position to sexagesimal hour.
	 *
	 * @param degreesPosition The position in degrees.
	 * @param truncated if the result position is truncated.
	 * @return The position in sexagesimal hour.
	 */
	public static String convertToSexagesimalHour(String degreesPosition, boolean truncated) {
		if (!isDecimalDegreesPosition(degreesPosition, RA)) {
			throw new IllegalArgumentException("This is not a valid RA decimal degrees position.");
		}
		double degreesRa = Double.parseDouble(degreesPosition);
		double x = degreesRa / 15.;

		int xx = (int) Math.floor(x);
		double tmp = (x - xx) * 60.;
		int mm = (int) Math.floor(tmp);
		double ss = (tmp - mm) * 60.;

		return Position.createSexagesimalPositionString(xx, mm, ss, truncated);
	}

	/**
	 * Create a Sexagesimal position.
	 *
	 * @param xx The hours/degrees.
	 * @param mm The minutes
	 * @param ss The seconds.
	 * @param truncated if the result position is truncated.
	 * @return The created sexagesimal position.
	 */
	private static String createSexagesimalPositionString(int xx, int mm, double ss, boolean truncated) {
		StringBuilder sb = new StringBuilder();
		sb.append(HHMM_FORMAT.format(xx));
		sb.append(':');
		sb.append(HHMM_FORMAT.format(mm));
		sb.append(':');
		if (truncated) {
			sb.append(DECIMAL_FORMAT.format(ss));
		} else {
			sb.append(ss);
		}
		return sb.toString();
	}

	/**
	 * Check if a position is valid.
	 *
	 * @param position The position to check.
	 * @param type The type of position (RA or DEC)
	 * @return if the position is valid.
	 */
	private static int getPositionWriting(String position, int type) {
		if (position == null || position.isEmpty()) {
			return INVALID;
		} else {
			if (isDecimalDegreesPosition(position, type)) {
				return DEGREES_DEC;
			} else {
				return isSexagesimalPosition(position, type);
			}
		}
	}

	/**
	 * Return if a position is valid.
	 *
	 * @param position The position to check.
	 * @param type The type of position (RA or DEC).
	 * @return if the position is valid.
	 */
	public static boolean isPositionValid(String position, int type) {
		return Position.getPositionWriting(position, type) > 0;
	}

	/**
	 * Check if a position is a valid one in decimal degrees.
	 * A degrees position is a double,
	 * between 0 and 360 (RA) or between -90 and 90 (DEC).
	 *
	 * @param position The position to check.
	 * @return if position is a valid degrees position.
	 */
	private static boolean isDecimalDegreesPosition(String position, int type) {
		if (type != RA && type != DEC) {
			throw new IllegalArgumentException("Provided type is invalid");
		}
		try {
			double value = Double.parseDouble(position);
			if (type == RA && value <= 360 && value >= 0) {
				return true;
			} else if (type == DEC && value <= 90 && value >= -90){
				return true;
			}
		} catch (NumberFormatException nfe) {
			// This is not a position in degres...
		}
		return false;
	}

	/**
	 * Check if a position is a valid sexagesimal position.
	 *
	 * @param position The position to check.
	 * @param type The type of position (RA or DEC)
	 * @return (INVALID, SEXAGESIMAL_HOUR or SEXAGESIMAL_DEC)
	 */
	private static int isSexagesimalPosition(String position, int type) {
		if (type != RA && type != DEC) {
			throw new IllegalArgumentException("Provided type is invalid");
		}
		try {
			String[] split = position.split(":");
			if (split.length == 3) {
				if (split[0].charAt(0) == '+') {
					split[0] = split[0].substring(1);
				}
				int hourOrDeg = Integer.parseInt(split[0]);
				int min = Integer.parseInt(split[1]);
				@SuppressWarnings("unused")
				double sec = Double.parseDouble(split[2]);

				return getSexagesimalType(type, hourOrDeg, min);
			}
		} catch (Exception e) {
			// This is not an sexagesimal position...
		}

		return INVALID;
	}

	/**
	 * Search for writing type for a sexagesimal position then return it.
	 *
	 * @param type The type of position (RA or DEC)
	 * @param hourOrDeg Time in hours or degrees.
	 * @param min Time in minutes.
	 * @return (INVALID, SEXAGESIMAL_HOUR or SEXAGESIMAL_DEC)
	 */
	private static int getSexagesimalType(int type, int hourOrDeg, int min) {
		if (type == RA &&
				hourOrDeg >= 0 && hourOrDeg < 24 &&
				min >= 0 && min < 60) {
			return SEXAGESIMAL_HOUR;
		} else if (type == DEC &&
				hourOrDeg >= -90 && hourOrDeg <= 90 &&
				min >= 0 && min < 60) {
			return SEXAGESIMAL_DEC;
		}
		return INVALID;
	}
}
