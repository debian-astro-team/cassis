/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.util;

import eu.omp.irap.ssap.ssaparameters.SizeUnit;

/**
 * Class to convert some value in different size unit.
 *
 * @author M. Boiziot
 */
public class Size {

	/**
	 * Utility class, do not use.
	 */
	private Size() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * Convert a value in degrees.
	 *
	 * @param value The value as a String who should be a number.
	 * @param unit The size unit.
	 * @return The converted value.
	 */
	public static String convertToDegrees(String value, SizeUnit unit) {
		double val = convertToDouble(value);
		double degrees;
		switch (unit) {
		case ARC_MIN:
			degrees = convertArcMinToDegrees(val);
			break;
		case ARC_SEC:
			degrees = convertArcSecToDegrees(val);
			break;
		case RADIANS:
			degrees = convertRadiansToDegrees(val);
			break;
		case DEGREES:
			degrees = val;
			break;
		default:
			throw new IllegalArgumentException("Unknown unit");
		}
		return String.valueOf(degrees);
	}

	/**
	 * Try to convert a String to a double then return it or an
	 *  IllegalArgumentException if this is not a double.
	 *
	 * @param value The value to convert as a String.
	 * @return The value converted as a double.
	 */
	public static double convertToDouble(String value) {
		try {
			return Double.parseDouble(value);
		} catch (NumberFormatException nfe) {
			throw new IllegalArgumentException(
					"The provided value must be a number.", nfe);
		}
	}

	/**
	 * Convert a value in arc seconds to degrees.
	 *
	 * @param value The value to convert.
	 * @return The value converted in degrees.
	 */
	public static double convertArcSecToDegrees(double value) {
		return value / 3600;
	}

	/**
	 * Convert a value in arc minutes to degrees.
	 *
	 * @param value The value to convert.
	 * @return The value converted in degrees.
	 */
	public static double convertArcMinToDegrees(double value) {
		return value / 60;
	}

	/**
	 * Convert a value in radians to degrees.
	 *
	 * @param value The value to convert.
	 * @return The value converted in degrees.
	 */
	public static double convertRadiansToDegrees(double value) {
		return value * 180 / Math.PI;
	}

	/**
	 * Check then return is the provided value is a number.
	 *
	 * @param value The value to check.
	 * @return true if the value is a number, false otherwise.
	 */
	public static boolean isNumber(String value) {
		boolean isNumber;
		try {
			Double.parseDouble(value);
			isNumber = true;
		} catch (NumberFormatException nfe) {
			isNumber = false;
		}
		return isNumber;
	}

	/**
	 * Check then return if the provided size is valid.
	 *
	 * @param value The value to check.
	 * @return true if the value if valid, false otherwise.
	 */
	public static boolean isSizeValid(String value) {
		return value != null && !value.isEmpty() && isNumber(value);
	}
}
