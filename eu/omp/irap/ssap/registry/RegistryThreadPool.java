/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.registry;

import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;



/**
 * Custom Thread Pool who don't need a call to shutdown to have an end result
 * and allow to do a double check of the end with a delay.
 *
 * @author M. Boiziot
 */
public class RegistryThreadPool {

	private ThreadPoolExecutor executor;
	private Thread check;
	private boolean isStopped;
	private boolean isStarted;
	private final ThreadPoolFinishInterface endHandler;
	private final int nThreads;
	private int msSleepDelay;


	/**
	 * Constructor.
	 *
	 * @param nThreads max number of concurrent threads.
	 * @param msSleepDelay sleep delay in ms before the second check for the end of tasks.
	 * @param endInterface The end handler.
	 */
	public RegistryThreadPool(int nThreads, int msSleepDelay, ThreadPoolFinishInterface endInterface) {
		this.endHandler = endInterface;
		this.nThreads = nThreads;
		this.msSleepDelay = msSleepDelay;
		createExecutor();
	}

	/**
	 * Create a thread who will check the status of the thread pool and launch the
	 * function from the handler for the end.
	 */
	private void createAndStartCheckThread() {
		if (check == null) {
			check = new CheckThread();
			check.start();
		}
	}

	/**
	 * Create the executor.
	 */
	private void createExecutor() {
		executor = new ThreadPoolExecutor(nThreads, nThreads,
				0L, TimeUnit.MILLISECONDS,
				new LinkedBlockingQueue<Runnable>());
	}

	/**
	 * Submit a new task to the executor.
	 *
	 * @param runnable The runnable.
	 * @return a Future.
	 */
	public Future<?> submit(Runnable runnable) {
		if (isStarted) {
			return executor.submit(runnable);
		} else {
			if (executor == null || executor.isShutdown()) {
				executor = null;
				createExecutor();
			}
			Future<?> objs = executor.submit(runnable);
			isStopped = false;
			isStarted = true;

			createAndStartCheckThread();
			return objs;
		}
	}

	/**
	 * Return if the tasks are terminated.
	 *
	 * @return if the tasks are terminated.
	 */
	public boolean isTerminated() {
		if (executor.isShutdown()) {
			executor.isTerminated();
		}
		return executor.getActiveCount() == 0;
	}

	/**
	 * Stop the executor and associated threads.
	 */
	public void stopAll() {
		isStopped = true;
		executor.shutdownNow();
		isStarted = false;
		check = null;
	}

	/**
	 * Set the sleep time before the second check for the completion of tasks.
	 *
	 * @param msTime The time in ms. A number inferior or equals to 0 disable
	 *  the double check.
	 */
	public void setSleepDelay(int msTime) {
		this.msSleepDelay = msTime;
	}

	/**
	 * A Thread who will check the status of the thread pool and launch the
	 * function from the handler for the end.
	 *
	 * @author M. Boiziot
	 */
	private class CheckThread extends Thread {

		@Override
		public void run() {
			sleepUntilReady();
			while (isStarted && !isStopped) {
				if (isTerminated()) {
					boolean stillTerminated = true;
					if (msSleepDelay > 0) {
						try {
							Thread.sleep(msSleepDelay);
							stillTerminated = isTerminated();
						} catch (InterruptedException e) {
							Thread.currentThread().interrupt();
						}
					}
					if (stillTerminated) {
						stopAll();
						endHandler.handleTasksEnd();
					}
				} else {
					doSleep(200);
				}
			}
		}

		/**
		 * Simple sleep with InterruptedException catched.
		 *
		 * @param ms time in milliseconds to sleep.
		 */
		private void doSleep(long ms) {
			try {
				Thread.sleep(ms);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}

		/**
		 * Sleep until the query has started and while it is not stopped.
		 */
		private void sleepUntilReady() {
			while (!isStarted && !isStopped) {
				doSleep(100);
			}
		}
	}
}
