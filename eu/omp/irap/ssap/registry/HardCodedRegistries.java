/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.registry;

import java.util.ArrayList;
import java.util.List;

/**
 * Simple class to hard-code some registries URLs.
 *
 * @author M. Boiziot
 */
public class HardCodedRegistries {

	private static final List<String> RI_REGISTRIES;
	private static final List<String> REGTAP_REGISTRIES;


	static {
		RI_REGISTRIES = createRiRegistries();
		REGTAP_REGISTRIES = createRegtapRegistries();
	}


	/**
	 * Utility class, do not use.
	 */
	private HardCodedRegistries() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * Return the list {@link RegistryProtocol#REGISTRY_INTERFACES} registries URLs.
	 *
	 * @return the list {@link RegistryProtocol#REGISTRY_INTERFACES} registries URLs.
	 */
	public static List<String> getListRiRegistries() {
		return RI_REGISTRIES;
	}

	/**
	 * Return the list {@link RegistryProtocol#TAP_REG_EXT} registries URLs.
	 *
	 * @return the list {@link RegistryProtocol#TAP_REG_EXT} registries URLs.
	 */
	public static List<String> getListRegtapRegistries() {
		return REGTAP_REGISTRIES;
	}

	/**
	 * Create the list of initials {@link RegistryProtocol#REGISTRY_INTERFACES} registries URLs.
	 *
	 * @return the list of initials {@link RegistryProtocol#REGISTRY_INTERFACES} registries URLs.
	 */
	private static List<String> createRiRegistries() {
		List<String> riRegistries = new ArrayList<>(2);
		riRegistries.add("http://registry.euro-vo.org/services/RegistrySearch");
		riRegistries.add("http://registry.astrogrid.org/astrogrid-registry/services/RegistryQueryv1_0");
		return riRegistries;
	}

	/**
	 * Create the list of initials {@link RegistryProtocol#TAP_REG_EXT} registries URLs.
	 *
	 * @return the list of initials {@link RegistryProtocol#TAP_REG_EXT} registries URLs.
	 */
	private static List<String> createRegtapRegistries() {
		List<String> regtapRegistries = new ArrayList<>(4);
		regtapRegistries.add("http://reg.g-vo.org/tap");
		regtapRegistries.add("http://dc.zah.uni-heidelberg.de/tap");
		regtapRegistries.add("http://gavo.aip.de/tap");
		regtapRegistries.add("http://voparis-cdpp.obspm.fr/tap");
		return regtapRegistries;
	}

	/**
	 * Return the list of URLs of registries as String for the given
	 *  {@link RegistryProtocol}.
	 *
	 * @param protocol The Registry protocol
	 * @return the list of URLs of registries as String for the given protocol.
	 */
	public static List<String> getRegistries(RegistryProtocol protocol) {
		List<String> registries;
		switch (protocol) {
		case REGISTRY_INTERFACES:
			registries = getListRiRegistries();
			break;
		case TAP_REG_EXT:
			registries = getListRegtapRegistries();
			break;
		default:
			throw new IllegalArgumentException("Unknow registry protocol");
		}
		return registries;
	}
}
