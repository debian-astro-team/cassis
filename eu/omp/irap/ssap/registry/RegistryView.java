/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.registry;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import eu.omp.irap.ssap.registry.search.SearchDialog;
import eu.omp.irap.ssap.service.SsaService;
import eu.omp.irap.ssap.service.SsaServiceView;
import eu.omp.irap.ssap.util.UtilFunction;

/**
 * View for the registry.
 *
 * @author M. Boiziot
 */
public class RegistryView extends JPanel {

	private static final long serialVersionUID = -7008204723128541296L;
	private RegistryControl control;

	private JComboBox<RegistryProtocol> registryProtocolComboBox;
	private JComboBox<String> registryComboBox;
	private JButton queryButton;
	private JPanel servicePanel;
	private JButton addServiceButton;
	private JButton selectAllButton;
	private JButton deselectAllButton;
	private JButton findButton;
	private List<SsaServiceView> listServiceView;
	private JScrollPane jsp;


	/**
	 * Constructor.
	 *
	 * @param theController The registry controller.
	 */
	public RegistryView(RegistryControl theController) {
		this.control = theController;
		this.setBorder(BorderFactory.createTitledBorder("Registry & Services selection"));
		this.setLayout(new BorderLayout());
		this.setPreferredSize(new Dimension(500, 200));
		this.listServiceView = new ArrayList<>(control.getModel().getListSsaService().size());

		add(createTopPanel(), BorderLayout.NORTH);

		servicePanel = new JPanel(new GridBagLayout());
		jsp = new JScrollPane();
		jsp.getVerticalScrollBar().setUnitIncrement(10);
		jsp.getViewport().add(servicePanel);
		add(jsp, BorderLayout.CENTER);
		updateServices();

		// Buttons
		JPanel bottomPanel = new JPanel();
		bottomPanel.add(getDeselectAllButton());
		bottomPanel.add(getSelectAllButton());
		bottomPanel.add(getAddServiceButton());
		bottomPanel.add(getFindButton());

		add(bottomPanel, BorderLayout.SOUTH);
	}

	/**
	 * Create if needed then return the registry JComboBox.
	 *
	 * @return the registry JComboBox.
	 */
	public JComboBox<String> getRegistryComboBox() {
		if (registryComboBox == null) {
			registryComboBox = new JComboBox<>(control.getModel().getListRegistryUrl().toArray(
					new String[control.getModel().getListRegistryUrl().size()]));
			registryComboBox.setSelectedItem(control.getModel().getCurrentRegistryUrl());
			registryComboBox.setEditable(true);
			registryComboBox.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
			registryComboBox.getEditor().getEditorComponent().addKeyListener(
				new KeyAdapter() {

					@Override
					public void keyReleased(KeyEvent event) {
						if (event.getKeyChar() == KeyEvent.VK_ENTER) {
							control.doQuery();
						}
					}
				}
			);
		}
		return registryComboBox;
	}

	/**
	 * Create if needed then return the Query button.
	 *
	 * @return the Query button.
	 */
	public JButton getQueryButton() {
		if (queryButton == null) {
			queryButton = new JButton("Query");
			queryButton.setMargin(new Insets(2, 2, 2, 2));
			queryButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.doQuery();
				}
			});
		}
		return queryButton;
	}

	/**
	 * Display a message on a pop-up.
	 *
	 * @param title The title of the dialog.
	 * @param msg The message to display.
	 * @param isErrorMsg true for displaying an error message, false for
	 *  displaying a warning message.
	 */
	public void displayPopup(final String title, final String msg,
			final boolean isErrorMsg) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				int messageType = isErrorMsg ? JOptionPane.ERROR_MESSAGE
						: JOptionPane.WARNING_MESSAGE;
				JOptionPane.showMessageDialog(RegistryView.this, msg, title, messageType);
			}
		});
	}

	/**
	 * Create if needed then return the Add service button.
	 *
	 * @return the Add service button.
	 */
	public JButton getAddServiceButton() {
		if (addServiceButton == null) {
			addServiceButton = new JButton("Add service");
			addServiceButton.setToolTipText("Add a new service");
			addServiceButton.setPreferredSize(new Dimension(120, 24));
			addServiceButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					AddServicePopup asp = new AddServicePopup(null, control);
					asp.setVisible(true);
				}
			});
		}
		return addServiceButton;
	}

	/**
	 * Create if needed then return the Select all button.
	 *
	 * @return the Select all button.
	 */
	public JButton getSelectAllButton() {
		if (selectAllButton == null) {
			selectAllButton = new JButton("Select all");
			selectAllButton.setToolTipText("Select all services.");
			selectAllButton.setPreferredSize(new Dimension(120, 24));
			selectAllButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.selectAllServices();
				}
			});
		}
		return selectAllButton;
	}

	/**
	 * Create if needed then return the Deselect all button.
	 *
	 * @return the Deselect all button.
	 */
	public JButton getDeselectAllButton() {
		if (deselectAllButton == null) {
			deselectAllButton = new JButton("Deselect all");
			deselectAllButton.setToolTipText("Deselect all services.");
			deselectAllButton.setPreferredSize(new Dimension(120, 24));
			deselectAllButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.deselectAllServices();
				}
			});
		}
		return deselectAllButton;
	}

	/**
	 * Remove all services from the view only.
	 */
	public void removeAllServices() {
		for (SsaServiceView service : listServiceView) {
			service.removeListeners();
		}
		listServiceView.clear();
		servicePanel.removeAll();
		servicePanel.repaint();
	}

	/**
	 * Update the services in the view.
	 */
	public void updateServices() {
		removeAllServices();
		final List<SsaService> listService = control.getModel().getListSsaService();

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				GridBagConstraints gbc = new GridBagConstraints(0, GridBagConstraints.RELATIVE,
						1, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE,
						new Insets(0, 0, 0, 0), 0, 0);
				for (SsaService service : listService) {
					SsaServiceView ssv = new SsaServiceView(service);
					listServiceView.add(ssv);
					servicePanel.add(ssv, gbc);
				}
				servicePanel.revalidate();
				servicePanel.repaint();
			}
		});
	}

	/**
	 * Update the registry {@link JComboBox} with an URL.
	 *
	 * @param registryUrl The URL.
	 */
	public void updateRegistry(String registryUrl) {
		JComboBox<String> regCom = getRegistryComboBox();
		if (!regCom.getSelectedItem().equals(registryUrl)) {
			DefaultComboBoxModel<String> model = (DefaultComboBoxModel<String>) regCom.getModel();
			if (model.getIndexOf(registryUrl) == -1) {
				model.addElement(registryUrl);
			}
			model.setSelectedItem(registryUrl);
		}
	}

	/**
	 * Place the scroll bar to the provided service.
	 *
	 * @param service The service where to place the scrollbar.
	 */
	public void scrollTo(SsaService service) {
		SsaServiceView associatedServiceView = null;
		for (SsaServiceView serviceView : listServiceView) {
			if (serviceView.getService() == service) {
				associatedServiceView = serviceView;
				break;
			}
		}

		if (associatedServiceView != null) {
			jsp.getViewport().setViewPosition(
					new Point(0, associatedServiceView.getY()));
		}
	}

	/**
	 * Create if needed then return the registry protocol JComboBox.
	 *
	 * @return the registry protocol JComboBox.
	 */
	public JComboBox<RegistryProtocol> getRegistryProtocolComboBox() {
		if (registryProtocolComboBox == null) {
			registryProtocolComboBox = new JComboBox<>(
					control.getModel().getListRegistryProtocol().toArray(
							new RegistryProtocol[control.getModel().getListRegistryProtocol().size()]));
			registryProtocolComboBox.setSelectedItem(control.getModel().getRegistryProtocol());
			registryProtocolComboBox.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
			getRegistryProtocolComboBox().addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					RegistryProtocol protocol = (RegistryProtocol)
							registryProtocolComboBox.getSelectedItem();
					control.getModel().setRegistryProtocol(protocol);
				}
			});
		}
		return registryProtocolComboBox;
	}

	/**
	 * Update the registry list with the given element and the one to select.
	 *
	 * @param elements The elements to set.
	 * @param selectedElement The element to select.
	 */
	public void updateRegistryList(List<String> elements, String selectedElement) {
		JComboBox<String> jcb = getRegistryComboBox();
		jcb.setModel(new DefaultComboBoxModel<String>(elements.toArray(
				new String[elements.size()])));
		updateRegistry(selectedElement);
	}

	/**
	 * Create then return the top JPanel.
	 *
	 * @return the top JPanel.
	 */
	private JPanel createTopPanel() {
		JPanel protoPanel = new JPanel(new BorderLayout());
		protoPanel.add(createTopLabel("Protocol:"), BorderLayout.WEST);
		protoPanel.add(getRegistryProtocolComboBox(), BorderLayout.CENTER);

		JPanel urlPanel = new JPanel(new BorderLayout());
		urlPanel.add(createTopLabel("Registry:"), BorderLayout.WEST);
		urlPanel.add(getRegistryComboBox(), BorderLayout.CENTER);

		JPanel centerPanel = new JPanel();
		BoxLayout blCenter = new BoxLayout(centerPanel, BoxLayout.PAGE_AXIS);
		centerPanel.setLayout(blCenter);
		centerPanel.add(protoPanel);
		centerPanel.add(urlPanel);

		JPanel topPanel = new JPanel(new BorderLayout(5, 5));
		topPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		topPanel.add(getQueryButton(), BorderLayout.EAST);
		topPanel.add(centerPanel, BorderLayout.CENTER);

		return topPanel;
	}

	/**
	 * Create a {@link JLabel} with the given text and configure it.
	 *
	 * @param text The text to set.
	 * @return the created JLabel.
	 */
	private JLabel createTopLabel(String text) {
		JLabel label = new JLabel(text);
		label.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		return label;
	}

	/**
	 * Create if needed then return the Find button.
	 *
	 * @return the Find button.
	 */
	public JButton getFindButton() {
		if (findButton == null) {
			findButton = new JButton("Find");
			findButton.setToolTipText("Find services by keywords.");
			findButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					Window window = SwingUtilities.getWindowAncestor(RegistryView.this);
					SearchDialog sd = new SearchDialog(window, control.getModel().getListSsaService());
					if (window != null) {
						UtilFunction.center(window,  sd);
					}
					sd.setVisible(true);
				}
			});
		}
		return findButton;
	}
}
