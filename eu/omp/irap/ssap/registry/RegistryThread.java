/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.registry;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import eu.omp.irap.ssap.registry.regtap.RegTapParser;
import eu.omp.irap.ssap.registry.regtap.RegTapQuery;
import eu.omp.irap.ssap.service.VoCapability;
import eu.omp.irap.ssap.service.SsaService;
import uk.ac.starlink.registry.BasicRegistryClient;
import uk.ac.starlink.registry.BasicResource;
import uk.ac.starlink.registry.RegistryRequestFactory;
import uk.ac.starlink.registry.SoapClient;
import uk.ac.starlink.registry.SoapRequest;

/**
 * Thread for querying a registry.
 *
 * @author M. Boiziot
 */
public class RegistryThread extends Thread {

	private static final Logger LOGGER = Logger.getLogger("RegistryThread");
	private static final String ADQLS = "capability/@standardID = 'ivo://ivoa.net/std/SSA'";
	private Registry registry;
	private RegistryInterface regInterface;
	private volatile Thread currentThread;


	/**
	 * Constructor.
	 *
	 * @param registry The registry to use.
	 * @param regInterface The interface for handling the result.
	 */
	public RegistryThread(Registry registry, RegistryInterface regInterface) {
		this.registry = registry;
		this.regInterface = regInterface;
	}

	/**
	 * Stop the work.
	 */
	public void stopThread() {
		currentThread.interrupt();
	}

	/**
	 * Do the query.
	 *
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {
		currentThread = Thread.currentThread();

		try {
			List<SsaService> listSsaService = getListSsaService();

			if (listSsaService != null) {
				// Sort alphabetically
				if (listSsaService.size() > 1 && !currentThread.isInterrupted()) {
					RegistryUtils.sortServicesAlphabetically(listSsaService);
				}

				if (!currentThread.isInterrupted()) {
					regInterface.setListService(listSsaService);
				}
			}
		} catch (Exception ie) {
			LOGGER.log(Level.WARNING, "An Exception occured on RegistryThread : " + ie.getMessage());
		} finally {
			if (!currentThread.isInterrupted()) {
				regInterface.destroyRequestThread();
			}
		}
	}

	/**
	 * Create a {@link SsaService} from a {@link BasicResource}.
	 *
	 * @param listShortName The list of short name of the existing service (it must be unique).
	 * @param listSsaService The list of existing {@link SsaService}.
	 * @param res The resource from which the service is created.
	 * @return The created {@link SsaService}.
	 */
	private SsaService createService(List<String> listShortName,
			List<SsaService> listSsaService, BasicResource res) {
		List<String> listSubjects = Arrays.asList(res.getSubjects());
		List<VoCapability> listCapabilities = RegistryUtils.convert(Arrays.asList(res.getCapabilities()));

		// Hack for multiple service with the same name like Pollux.
		String title = res.getTitle();
		int nbServiceName = getServiceNumberWithName(listSsaService, title);
		if (nbServiceName >= 1) {
			title = title + " " + (nbServiceName+1);
		}

		// We want an unique no null short name.
		if (res.getShortName() == null) {
			res.setShortName(RegistryUtils.createShortName(title));
		}
		while (listShortName.contains(res.getShortName())) {
			int num = 2;
			String shortName = res.getShortName();
			while (listShortName.contains(shortName + num)) {
				num++;
			}
			res.setShortName(shortName + num);
		}

		return new SsaService(title, res.getShortName(), res.getIdentifier(),
				res.getPublisher(), res.getContact(), res.getReferenceUrl(),
				listSubjects, listCapabilities);
	}

	/**
	 * Compute the number of the service with the provided name.
	 *
	 * @param listSsaService The list where to search.
	 * @param name The name.
	 * @return The number of the service with the name.
	 */
	public int getServiceNumberWithName(List<SsaService> listSsaService, String name) {
		if (name == null) {
			return 0;
		}
		int nb = 0;
		for (SsaService service : listSsaService) {
			if (name.equals(service.getTitle())) {
				nb++;
			}
		}
		return nb;
	}

	/**
	 * Query the registry with a "registry interfaces" protocol then return the
	 *  list of services.
	 *
	 * @return the list of services.
	 */
	private List<SsaService> queryRiRegistry() {
		List<String> listShortName = new ArrayList<>();
		List<SsaService> listSsaService = new ArrayList<>();
		try {
			URL url = new URL(registry.getUrl());
			SoapClient soapClient = new SoapClient(url);
			BasicRegistryClient registryClient = new BasicRegistryClient(soapClient);

			SoapRequest req;

			req = RegistryRequestFactory.adqlsSearch(ADQLS);

			Iterator<BasicResource> iterator = registryClient.getResourceIterator(req);

			while (iterator.hasNext() && !currentThread.isInterrupted()) {
				BasicResource res = iterator.next();
				SsaService service = createService(listShortName, listSsaService, res);

				listSsaService.add(service);
				listShortName.add(res.getShortName());
			}
		} catch (IOException e) {
			if (!currentThread.isInterrupted()) {
				regInterface.notifyError();
			}
		}
		return listSsaService;
	}

	/**
	 * Query the registry with a RegTAP protocol then return the list of services.
	 *
	 * @return the list of services.
	 */
	private List<SsaService> queryRegTapRegistry() {
		List<SsaService> listSsaService = null;
		try {
			String votable = RegTapQuery.doQuery(registry.getUrl());
			RegTapParser rtp = new RegTapParser(votable);
			if (rtp.isContainsError() && !currentThread.isInterrupted()) {
				regInterface.notifyError();
			} else {
				listSsaService = rtp.getServices();
			}
		} catch (IOException e) {
			if (!currentThread.isInterrupted()) {
				regInterface.notifyError();
			}
			listSsaService = Collections.emptyList();
		}
		return listSsaService;
	}

	/**
	 * Query the registry then return the list of services.
	 *
	 * @return the list of services.
	 */
	private List<SsaService> getListSsaService() {
		List<SsaService> listSsaService = null;
		switch (registry.getProtocol()) {
		case REGISTRY_INTERFACES:
			listSsaService = queryRiRegistry();
			break;
		case TAP_REG_EXT:
			listSsaService = queryRegTapRegistry();
			break;
		default:
			throw new IllegalArgumentException("Unknown registry protocol");
		}
		return listSsaService;
	}

}
