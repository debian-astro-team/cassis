/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.registry;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import eu.omp.irap.ssap.service.VoCapability;
import eu.omp.irap.ssap.service.SsaService;
import uk.ac.starlink.registry.BasicCapability;

/**
 * Simple utility class for registry.
 *
 * @author M. Boiziot
 */
public class RegistryUtils {


	/**
	 * Utility class, do not use.
	 */
	private RegistryUtils() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * Create a short name based on the first char of each word.
	 *
	 * @param name The name used for creating the short name.
	 * @return The created short name.
	 */
	public static String createShortName(String name) {
		String[] words = name.split("\\s+");
		StringBuilder sb = new StringBuilder();
		for (String word : words) {
			sb.append(word.charAt(0));
		}
		return sb.toString();
	}

	/**
	 * Sort the services alphabetically.
	 *
	 * @param listSsaService The list of the services.
	 */
	public static void sortServicesAlphabetically(List<SsaService> listSsaService) {
		Collections.sort(listSsaService, new Comparator<SsaService>() {
			public int compare(final SsaService service1, final SsaService service2) {
				return service1.getTitle().toLowerCase(Locale.ENGLISH).
						compareTo(service2.getTitle().toLowerCase(Locale.ENGLISH));
			}
		});
	}

	/**
	 * Convert a List of {@link BasicCapability} objects to {@link VoCapability}.
	 *
	 * @param lists The List to convert
	 * @return the converted List of Capability objects.
	 */
	public static List<VoCapability> convert(List<BasicCapability> lists) {
		List<VoCapability> newList = new ArrayList<>(lists.size());
		for (BasicCapability bc : lists) {
			newList.add(new VoCapability(bc.getAccessUrl(), bc.getStandardId(), bc.getVersion()));
		}
		return newList;
	}
}
