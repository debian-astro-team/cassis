/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.registry.search;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import eu.omp.irap.ssap.service.SsaService;
import eu.omp.irap.ssap.service.SsaServiceView;

/**
 * View of the services finder.
 *
 * @author M. Boiziot
 */
public class SearchView extends JPanel {

	private static final long serialVersionUID = -1928697911786179266L;

	private SearchModel model;
	private JTextField searchField;
	private JButton searchButton;
	private JButton selectAllButton;
	private JButton closeButton;
	private JPanel centerPanel;
	private List<SsaServiceView> services;
	private Runnable closeAction;


	/**
	 * Constructor.
	 *
	 * @param model The model.
	 */
	public SearchView(SearchModel model) {
		super(new BorderLayout());
		this.model = model;
		this.services = new ArrayList<>();

		add(createTopPanel(), BorderLayout.NORTH);
		JScrollPane scrollPane = new JScrollPane(getCenterPanel());
		scrollPane.getVerticalScrollBar().setUnitIncrement(10);
		add(scrollPane, BorderLayout.CENTER);
		add(createBottomPanel(), BorderLayout.SOUTH);
	}

	/**
	 * Create the return the center panel.
	 *
	 * @return the center panel.
	 */
	private JPanel getCenterPanel() {
		if (centerPanel == null) {
			centerPanel = new JPanel(new GridBagLayout());
			search();
		}
		return centerPanel;
	}

	/**
	 * Create then return the top panel.
	 *
	 * @return the top panel.
	 */
	private JPanel createTopPanel() {
		JPanel topPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 5));
		JLabel label = new JLabel("Keyword(s):");
		label.setToolTipText("The keyword(s) to search separated by spaces");
		topPanel.add(label);
		topPanel.add(getSearchField());
		topPanel.add(getSearchButton());
		return topPanel;
	}

	/**
	 * Create then return the bottom panel.
	 *
	 * @return the bottom panel.
	 */
	private JPanel createBottomPanel() {
		JPanel bottomPanel = new JPanel();
		bottomPanel.add(getSelectAllButton());
		bottomPanel.add(getCloseButton());
		return bottomPanel;
	}

	/**
	 * Create if needed then return the Search button.
	 *
	 * @return the Search button.
	 */
	public JButton getSearchButton() {
		if (searchButton == null) {
			searchButton = new JButton("Search");
			searchButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					search();
				}
			});
		}
		return searchButton;
	}

	/**
	 * Create if needed then return the Select All button.
	 *
	 * @return the Select All button.
	 */
	public JButton getSelectAllButton() {
		if (selectAllButton == null) {
			selectAllButton = new JButton("Select All");
			selectAllButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					selectAllServices();
					close(true);
				}
			});
		}
		return selectAllButton;
	}

	/**
	 * Create if needed then return the Close button.
	 *
	 * @return the Close button.
	 */
	public JButton getCloseButton() {
		if (closeButton == null) {
			closeButton = new JButton("Close");
			closeButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					close(true);
				}
			});
		}
		return closeButton;
	}

	/**
	 * Create if needed then return the search text field.
	 *
	 * @return the search text field.
	 */
	public JTextField getSearchField() {
		if (searchField == null) {
			searchField = new JTextField();
			searchField.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					search();
				}
			});
			Dimension size = new Dimension(280, 24);
			searchField.setSize(size);
			searchField.setPreferredSize(size);
		}
		return searchField;
	}

	/**
	 * Remove previous services then add the given services.
	 *
	 * @param servicesToAdd The services to add.
	 */
	private void updateServices(List<SsaService> servicesToAdd) {
		getCenterPanel().removeAll();
		removeListenersAndServices();
		GridBagConstraints gbc = new GridBagConstraints(0, GridBagConstraints.RELATIVE,
				1, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0), 0, 0);
		for (SsaService service : servicesToAdd) {
			SsaServiceView serviceView = new SsaServiceView(service);
			services.add(serviceView);
			getCenterPanel().add(serviceView, gbc);
		}
		revalidate();
		repaint();
	}

	/**
	 * Remove the listeners for each services then remove the services.
	 */
	public void removeListenersAndServices() {
		if (services == null) {
			return;
		}
		getCenterPanel().removeAll();
		Iterator<SsaServiceView> it = services.iterator();
		SsaServiceView currentService;
		while (it.hasNext()) {
			currentService = it.next();
			currentService.removeListeners();
			it.remove();
		}
		revalidate();
		repaint();
	}

	/**
	 * Select all visible services.
	 */
	private void selectAllServices() {
		for (SsaServiceView service : services) {
			service.getService().setSelected(true);
		}
	}

	/**
	 * Do a research with the currents keywords.
	 */
	private void search() {
		updateServices(model.getServices(getKeywordsList()));
	}

	/**
	 * Return the keywords list.
	 *
	 * @return the keywords list.
	 */
	private List<String> getKeywordsList() {
		String text = getSearchField().getText().trim();
		String[] words = text.split(" ");
		if (words.length == 0) {
			return Collections.emptyList();
		}
		List<String> list = new ArrayList<>();
		for (String word : words) {
			if (word != null && !word.trim().isEmpty()) {
				list.add(word);
			}
		}
		return list;
	}

	/**
	 * Remove the listeners then run the close action.
	 *
	 * @param runAction true to run the close action, false to do not. This
	 *  parameter allow to avoid having dispose calling himself in an infinite
	 *  loop.
	 */
	public void close(boolean runAction) {
		removeListenersAndServices();
		if (closeAction != null && runAction) {
			closeAction.run();
		}
	}

	/**
	 * Set a close action.
	 *
	 * @param closeAction The close action to set.
	 */
	public void setCloseAction(Runnable closeAction) {
		this.closeAction = closeAction;
	}
}
