/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.registry.search;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.JDialog;

import eu.omp.irap.ssap.service.SsaService;

/**
 * Dialog for the services finder.
 *
 * @author M. Boiziot
 */
public class SearchDialog extends JDialog {

	private static final long serialVersionUID = -6422290511031569282L;


	/**
	 * Constructor.
	 *
	 * @param owner The window from which the dialog is displayed.
	 * @param services The base services list.
	 */
	public SearchDialog(Window owner, List<SsaService> services) {
		super(owner);
		setTitle("Services finder");
		final SearchView searchView = createSearchView(services);
		setContentPane(searchView);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				searchView.close(false);
			}
		});
		setPreferredSize(new Dimension(520, 360));
		pack();
		setModal(true);
		setAlwaysOnTop(true);
		setLocationRelativeTo(null);
	}

	/**
	 * Create the SearchView.
	 *
	 * @param services The base services list
	 * @return the created SearchView.
	 */
	private SearchView createSearchView(List<SsaService> services) {
		SearchView searchView = new SearchView(new SearchModel(services));
		searchView.setCloseAction(createCloseAction());
		return searchView;
	}

	/**
	 * Create the close action.
	 *
	 * @return the created close action.
	 */
	private Runnable createCloseAction() {
		return new Runnable() {
			@Override
			public void run() {
				dispose();
			}
		};
	}

}
