/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.registry;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import eu.omp.irap.ssap.service.SsaService;
import eu.omp.irap.ssap.util.SsapListenerManager;
import eu.omp.irap.ssap.util.SsapModelChangedEvent;

/**
 * Model class for the registry.
 *
 * @author M. Boiziot
 */
public class RegistryModel extends SsapListenerManager implements RegistryInterface {

	public static final String NEW_LIST_SERV_RESP = "newListServiceWithResponse";
	public static final String REGISTRY_CHANGE = "registryChange";
	public static final String REGISTRY_ERROR = "currentRegistryError";
	public static final String REGISTRY_LIST_CHANGE = "registryListChange";
	public static final String NEW_SERVICES = "newServices";
	public static final String NEED_QUERY_UPDATE = "needQueryUpdate";
	public static final String PROTOCOL_CHANGE = "protocolChange";

	private String currentRegistry;
	private RegistryProtocol registryProtocol;
	private List<RegistryProtocol> listRegistryProtocol;
	private List<String> listRegistryUrl;
	private List<SsaService> listSsaService;
	private RegistryThread registryThread;
	private List<String> listServiceWithResponse;


	/**
	 * Constructor.
	 */
	public RegistryModel() {
		listRegistryProtocol = Arrays.asList(RegistryProtocol.values());
		registryProtocol = RegistryProtocol.TAP_REG_EXT;
		listRegistryUrl = HardCodedRegistries.getRegistries(registryProtocol);
		listSsaService = new ArrayList<>();
		this.listServiceWithResponse = new ArrayList<>();
		setCurrentRegistry(listRegistryUrl.get(0));
	}

	/**
	 * Return the list of registry URL.
	 *
	 * @return the list of registry URL.
	 */
	public List<String> getListRegistryUrl() {
		return listRegistryUrl;
	}

	/**
	 * Change the selected registry and launch a thread who will connect to
	 * the registry and parse the result.
	 *
	 * @param registryUrl The URL of the registry to connect
	 */
	public void setCurrentRegistry(String registryUrl) {
		if (registryThread != null && !registryThread.isInterrupted()) {
			destroyRequestThread();
		}
		currentRegistry = registryUrl;
		registryThread = new RegistryThread(getRegistry(), this);
		registryThread.start();
		fireDataChanged(new SsapModelChangedEvent(RegistryModel.REGISTRY_CHANGE));
	}

	/**
	 * Set a list of service.
	 *
	 * @see eu.omp.irap.ssap.registry.RegistryInterface#setListService(java.util.List)
	 */
	public void setListService(List<SsaService> listServices) {
		for (SsaService service : this.listSsaService) {
			service.removeAllListener();
		}
		this.listSsaService.clear();
		this.listSsaService = listServices;
		fireDataChanged(new SsapModelChangedEvent(RegistryModel.NEW_SERVICES));
	}

	/**
	 * Return the uneditable list of SsaServices.
	 *
	 * @return the uneditable list of SsaServices.
	 */
	public final List<SsaService> getListSsaService() {
		return listSsaService;
	}

	/**
	 * Notify an error.
	 */
	public void notifyError() {
		fireDataChanged(new SsapModelChangedEvent(RegistryModel.REGISTRY_ERROR));
	}

	/**
	 * Stop and destroy the request thread.
	 */
	public void destroyRequestThread() {
		if (registryThread != null) {
			registryThread.stopThread();
		}
		registryThread = null;
	}

	/**
	 * Search in the service list for the service with the provided name.
	 *
	 * @param shortName The short name of the service.
	 * @return The service or null if there is not service with the provided short name.
	 */
	public SsaService getServiceWithShortName(String shortName) {
		List<SsaService> services = getListSsaService();
		for (SsaService service : services) {
			if (service.getShortName().equals(shortName)) {
				return service;
			}
		}
		return null;
	}

	/**
	 * Return the list of {@link SsaService} who are selected.
	 *
	 * @return The list of {@link SsaService} who are selected.
	 */
	public List<SsaService> getSelectedServices() {
		List<SsaService> selectedServices = new ArrayList<>();
		for (SsaService service : listSsaService) {
			if (service.isSelected()) {
				selectedServices.add(service);
			}
		}
		return selectedServices;
	}

	/**
	 * Return the string URL of the current registry.
	 *
	 * @return the string URL of the current registry.
	 */
	public String getCurrentRegistryUrl() {
		return currentRegistry;
	}

	/**
	 * Set the list of the services (theirs short name) who responded to a previous query with a result.
	 *
	 * @param list The list of services short name.
	 */
	public void setServiceWithResponse(List<String> list) {
		listServiceWithResponse.clear();
		listServiceWithResponse.addAll(list);
		fireDataChanged(new SsapModelChangedEvent(RegistryModel.NEW_LIST_SERV_RESP));
	}

	/**
	 * Return the list of services short name from the services who responded to a previous query with a result.
	 *
	 * @return the list of services short name from the services who responded to a previous query with a result.
	 */
	public List<String> getListServiceWithResponse() {
		return listServiceWithResponse;
	}

	/**
	 * Clear the list of {@link SsaService}.
	 */
	public void clearServiceList() {
		listSsaService.clear();
	}

	/**
	 * Return the list of known registry protocols.
	 *
	 * @return the list of known registry protocols.
	 */
	public List<RegistryProtocol> getListRegistryProtocol() {
		return listRegistryProtocol;
	}

	/**
	 * Return the selected registry protocol.
	 *
	 * @return the selected registry protocol.
	 */
	public RegistryProtocol getRegistryProtocol() {
		return registryProtocol;
	}

	/**
	 * Change the registry protocol.
	 *
	 * @param protocol The new protocol to set.
	 */
	public void setRegistryProtocol(RegistryProtocol protocol) {
		boolean change = registryProtocol != protocol;
		if (change) {
			registryProtocol = protocol;
			fireDataChanged(new SsapModelChangedEvent(RegistryModel.PROTOCOL_CHANGE));
			updateRegistryProtocol();
		}
	}

	/**
	 * Update the list of known registries URLs for the selected registry protocol.
	 */
	private void updateRegistryProtocol() {
		listRegistryUrl = HardCodedRegistries.getRegistries(registryProtocol);
		currentRegistry = listRegistryUrl.get(0);
		fireDataChanged(new SsapModelChangedEvent(RegistryModel.REGISTRY_LIST_CHANGE));
	}

	/**
	 * Create and return a {@link Registry} object for the selected protocol and URL.
	 *
	 * @return a {@link Registry} object for the selected protocol and URL.
	 */
	private Registry getRegistry() {
		return new Registry(registryProtocol, currentRegistry);
	}
}
