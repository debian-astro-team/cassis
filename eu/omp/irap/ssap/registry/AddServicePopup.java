/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.registry;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import eu.omp.irap.ssap.service.VoCapability;
import eu.omp.irap.ssap.service.SsaService;
import eu.omp.irap.ssap.util.HintTextFieldUI;

/**
 * Simple panel to manually add a SsaService.
 *
 * @author M. Boiziot
 */
public class AddServicePopup extends JDialog {

	private static final long serialVersionUID = -5021712646493191956L;
	private RegistryControl registryControl;
	private JTextField titleTextField;
	private JTextField shortNameTextField;
	private JTextField identifierTextField;
	private JTextField publisherTextField;
	private JTextField contactTextField;
	private JTextField referenceTextField;
	private JTextField capaTypeTextField;
	private JTextField capaUrlTextField;
	private JButton cancelButton;
	private JButton addButton;


	/**
	 * Constructor.
	 *
	 * @param owner The owner.
	 * @param registryControl The registry control.
	 */
	public AddServicePopup(Frame owner, RegistryControl registryControl) {
		super(owner, true);
		this.registryControl = registryControl;
		this.setLayout(new BorderLayout());
		this.setTitle("Add service");

		JPanel centerPanel = new JPanel(new GridLayout(0, 2));
		centerPanel.add(new JLabel("Title:"));
		centerPanel.add(getTitleTextField());
		centerPanel.add(new JLabel("Short name:"));
		centerPanel.add(getShortNameTextField());
		centerPanel.add(new JLabel("Identifier:"));
		centerPanel.add(getIdentifierTextField());
		centerPanel.add(new JLabel("Publisher:"));
		centerPanel.add(getPublisherTextField());
		centerPanel.add(new JLabel("Contact:"));
		centerPanel.add(getContactTextField());
		centerPanel.add(new JLabel("Reference URL:"));
		centerPanel.add(getReferenceTextField());
		centerPanel.add(new JLabel("Capabilities type:"));
		centerPanel.add(getCapaTypeTextField());
		centerPanel.add(new JLabel("Capabilities URL:"));
		centerPanel.add(getCapaUrlTextField());

		JPanel southPanel = new JPanel();
		southPanel.add(getCancelButton());
		southPanel.add(getAddButton());


		this.add(centerPanel, BorderLayout.CENTER);
		this.add(southPanel, BorderLayout.SOUTH);
		this.pack();
	}

	/**
	 * Create if needed then return the Title text field.
	 *
	 * @return the Title text field.
	 */
	private JTextField getTitleTextField() {
		if (titleTextField == null) {
			titleTextField = new JTextField();
			titleTextField.setPreferredSize(new Dimension(260, 24));
			titleTextField.setUI(new HintTextFieldUI("Service title. Can not be empty"));
		}
		return titleTextField;
	}

	/**
	 * Create if needed then return the ShortName text field.
	 *
	 * @return the ShortName text field.
	 */
	private JTextField getShortNameTextField() {
		if (shortNameTextField == null) {
			shortNameTextField = new JTextField();
			shortNameTextField.setPreferredSize(new Dimension(260, 24));
			shortNameTextField.setUI(new HintTextFieldUI("Service short name. Can not be empty."));
		}
		return shortNameTextField;
	}

	/**
	 * Create if needed then return the Identifier text field.
	 *
	 * @return the Identifier text field.
	 */
	private JTextField getIdentifierTextField() {
		if (identifierTextField == null) {
			identifierTextField = new JTextField();
			identifierTextField.setPreferredSize(new Dimension(260, 24));
			identifierTextField.setUI(new HintTextFieldUI("Service identifier. Can be empty."));
		}
		return identifierTextField;
	}

	/**
	 * Create if needed then return the Publisher text field.
	 *
	 * @return the Publisher text field.
	 */
	private JTextField getPublisherTextField() {
		if (publisherTextField == null) {
			publisherTextField = new JTextField();
			publisherTextField.setPreferredSize(new Dimension(260, 24));
			publisherTextField.setUI(new HintTextFieldUI("Service Publisher. Can be empty."));
		}
		return publisherTextField;
	}

	/**
	 * Create if needed then return the Contact text field.
	 *
	 * @return the Contact text field.
	 */
	private JTextField getContactTextField() {
		if (contactTextField == null) {
			contactTextField = new JTextField();
			contactTextField.setPreferredSize(new Dimension(260, 24));
			contactTextField.setUI(new HintTextFieldUI("Service contact. Can be empty"));
		}
		return contactTextField;
	}

	/**
	 * Create if needed then return the Reference text field.
	 *
	 * @return the Reference text field.
	 */
	private JTextField getReferenceTextField() {
		if (referenceTextField == null) {
			referenceTextField = new JTextField();
			referenceTextField.setPreferredSize(new Dimension(260, 24));
			referenceTextField.setUI(new HintTextFieldUI("Service reference URL. Can be empty"));
		}
		return referenceTextField;
	}

	/**
	 * Create if needed then return the Capabilities Type text field.
	 *
	 * @return the Capabilities Type text field.
	 */
	private JTextField getCapaTypeTextField() {
		if (capaTypeTextField == null) {
			capaTypeTextField = new JTextField(SsaService.SSA_STANDARD_ID);
			capaTypeTextField.setPreferredSize(new Dimension(260, 24));
		}
		return capaTypeTextField;
	}

	/**
	 * Create if needed then return the Capabilities URL text field.
	 *
	 * @return the Capabilities URL text field.
	 */
	private JTextField getCapaUrlTextField() {
		if (capaUrlTextField == null) {
			capaUrlTextField = new JTextField();
			capaUrlTextField.setPreferredSize(new Dimension(260, 24));
			capaUrlTextField.setUI(new HintTextFieldUI("Capabilities URL. Can not be empty"));
		}
		return capaUrlTextField;
	}

	/**
	 * Create if needed then return the Cancel button.
	 *
	 * @return the Cancel button.
	 */
	private JButton getCancelButton() {
		if (cancelButton == null) {
			cancelButton = new JButton("Cancel");
			cancelButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					registryControl = null;
					dispose();
				}
			});
		}
		return cancelButton;
	}

	/**
	 * Create if needed then return the Add button.
	 *
	 * @return the Add button.
	 */
	private JButton getAddButton() {
		if (addButton == null) {
			addButton = new JButton("Add");
			addButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					doAddAction();
				}
			});
		}
		return addButton;
	}

	/**
	 * Check parameters then if it OK add a service and close the popup.
	 */
	private void doAddAction() {
		if (parametersAreValid()) {
			VoCapability capa = new VoCapability(getCapaUrlTextField().getText(), getCapaTypeTextField().getText(), "1.1");
			List<VoCapability> listCapa = new ArrayList<>(1);
			listCapa.add(capa);

			SsaService service = new SsaService(
					getTitleTextField().getText(),
					getShortNameTextField().getText(),
					getIdentifierTextField().getText(),
					getPublisherTextField().getText(),
					getContactTextField().getText(),
					getReferenceTextField().getText(),
					new ArrayList<String>(), listCapa);

			registryControl.addService(service);
			registryControl = null;
			dispose();
		}
	}

	/**
	 * Check if a {@link JTextField} is empty.
	 * Display a error message and request the focus if it's the case.
	 *
	 * @param jtextfield The {@link JTextField} to check.
	 * @param name The name of the parameter.
	 * @return if the {@link JTextField} is empty.
	 */
	private boolean checkIfEmpty(JTextField jtextfield, String name) {
		if (jtextfield.getText().isEmpty()) {
			JOptionPane.showMessageDialog(this,
					name + " parameter can not be empty.",
					"Parameter error", JOptionPane.ERROR_MESSAGE);
			jtextfield.requestFocus();
			return true;
		}
		return false;
	}

	/**
	 * Check if the parameters are valid and display an error message if not.
	 *
	 * @return if the parameters are valid.
	 */
	private boolean parametersAreValid() {
		if (checkIfEmpty(getTitleTextField(), "Title") ||
				checkIfEmpty(getShortNameTextField(), "Short name") ||
				checkIfEmpty(getCapaTypeTextField(), "Capabilities type") ||
				checkIfEmpty(getCapaUrlTextField(), "Capabilities URL")) {
			return false;
		}
		if (registryControl.getModel().getServiceWithShortName(
				getShortNameTextField().getText()) != null) {
			JOptionPane.showMessageDialog(this,
					"The service short name must be unique.",
					"Short name invalid", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}
}
