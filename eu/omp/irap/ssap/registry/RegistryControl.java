/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.registry;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.omp.irap.ssap.service.MetadataThreadError;
import eu.omp.irap.ssap.service.ServiceMetadataThread;
import eu.omp.irap.ssap.service.SsaService;
import eu.omp.irap.ssap.util.SsapModelChangedEvent;
import eu.omp.irap.ssap.util.SsapModelListener;
import eu.omp.irap.ssap.util.UtilFunction;
import eu.omp.irap.ssap.value.Constants;

/**
 * Control class for the registry.
 *
 * @author M. Boiziot
 */
public class RegistryControl implements SsapModelListener, ThreadPoolFinishInterface {

	private static final int MAX_METADATA_THREADS = 10;
	private static final int THREADPOOL_MS_DELAY = 5000;
	private RegistryModel model;
	private RegistryView view;
	private RegistryThreadPool threadPool;


	/**
	 * Constructor.
	 */
	public RegistryControl() {
		this(new RegistryModel());
	}

	/**
	 * Constructor.
	 *
	 * @param pModel The model to use.
	 */
	public RegistryControl(RegistryModel pModel) {
		model = pModel;
		view = new RegistryView(this);
		threadPool = new RegistryThreadPool(MAX_METADATA_THREADS, THREADPOOL_MS_DELAY, this);
		model.addModelListener(this);
		registerServices();
	}

	/**
	 * Return the model.
	 *
	 * @return the model.
	 */
	public RegistryModel getModel() {
		return model;
	}

	/**
	 * Return the view.
	 *
	 * @return the view.
	 */
	public RegistryView getView() {
		return view;
	}

	/**
	 * Handle event.
	 *
	 * @param event The event.
	 * @see eu.omp.irap.ssap.util.SsapModelListener#dataChanged(eu.omp.irap.ssap.util.SsapModelChangedEvent)
	 */
	public void dataChanged(SsapModelChangedEvent event) {
		if (RegistryModel.REGISTRY_CHANGE.equals(event.getSource())) {
			updateRegistry();
		} else if (RegistryModel.NEW_SERVICES.equals(event.getSource())) {
			handleNewServices();
		} else if (RegistryModel.REGISTRY_ERROR.equals(event.getSource())) {
			handleRegistryError();
		} else if (RegistryModel.PROTOCOL_CHANGE.equals(event.getSource())) {
			updateRegistryProtocol();
		} else if (RegistryModel.REGISTRY_LIST_CHANGE.equals(event.getSource())) {
			updateRegistryList();
		} else if (SsaService.SELECTED_UPDATE.equals(event.getSource())) {
			SsaService service = (SsaService) event.getValue();
			runServiceThread(service);
			relayEvent(event);
		} else if (SsaService.NEW_PARAMETER.equals(event.getSource())) {
			relayEvent(event);
		} else if (SsaService.COPY.equals(event.getSource())) {
			SsaService service = (SsaService) event.getValue();
			UtilFunction.copyToClipboard(service.getInformations(false));
		} else if (SsaService.COPY_SELECTED.equals(event.getSource())) {
			UtilFunction.copyToClipboard(getServicesInformations(true));
		} else if (SsaService.COPY_ALL.equals(event.getSource())) {
			UtilFunction.copyToClipboard(getServicesInformations(false));
		} else if (SsaService.GO_TO.equals(event.getSource())) {
			SsaService service = (SsaService) event.getValue();
			view.scrollTo(service);
		}
	}

	/**
	 * Send a {@link ServiceMetadataThread} to threadPool if needed.
	 *
	 * @param service The service for which we want to do that.
	 */
	private void runServiceThread(SsaService service) {
		if (service.isSelected() && !service.isThreadInstancied()) {
			ServiceMetadataThread thread = service.getThread();
			if (thread != null) {
				threadPool.submit(thread);
			}
		}
	}

	/**
	 * Relay the event if it is not already relayed and mark it as relayed.
	 *
	 * @param event The event to relay.
	 */
	private void relayEvent(SsapModelChangedEvent event) {
		if (!event.isRelayed()) {
			event.setRelayed();
			model.fireDataChanged(event);
		}
	}

	/**
	 * Handle new services.
	 */
	private void handleNewServices() {
		stopMetadataThreads();
		registerServices();
		view.updateServices();
	}

	/**
	 * Handle an error with the registry.
	 */
	private void handleRegistryError() {
		stopMetadataThreads();
		view.displayPopup("Registry Error", "The provided registry is invalid or can not be reached", true);
		view.removeAllServices();
	}

	/**
	 * Update the registry.
	 */
	private void updateRegistry() {
		String url = model.getCurrentRegistryUrl();
		view.updateRegistry(url);
	}

	/**
	 * Check the registry URL then set it to the model or display an
	 * error if the registry URL is empty or malformed.
	 */
	public void doQuery() {
		String registryUrl = (String) view.getRegistryComboBox().getSelectedItem();

		// Do some check on the provided registry URL.
		if (registryUrl.isEmpty()) {
			view.displayPopup("Empty URL", "The registry URL is empty", false);
			return;
		}
		try {
			@SuppressWarnings("unused")
			URL url = new URL(registryUrl);
		} catch (MalformedURLException e) {
			view.displayPopup("URL Error", "The provided registry URL is not valid.", false);
			return;
		}

		stopMetadataThreads();
		view.removeAllServices();
		model.clearServiceList();
		model.setCurrentRegistry(registryUrl);
	}

	/**
	 * Select all the services who are not deactivated.
	 */
	public void selectAllServices() {
		List<SsaService> listServices = model.getListSsaService();
		threadPool.setSleepDelay(0);
		for (SsaService service : listServices) {
			if (!service.isDeactivated()) {
				service.setSelected(true);
			}
		}
	}

	/**
	 * Deselect all the services.
	 */
	public void deselectAllServices() {
		List<SsaService> listServices = model.getListSsaService();
		for (SsaService service : listServices) {
			service.setSelected(false);
		}
	}

	/**
	 * Select all service who repsonded to the previous request.
	 */
	public void selectPreviousServiceWithResponse() {
		List<String> servicesWithResponse = model.getListServiceWithResponse();
		if (!servicesWithResponse.isEmpty()) {
			List<SsaService> serviceList = model.getListSsaService();
			for (SsaService service : serviceList) {
				service.setSelected(servicesWithResponse.contains(service.getShortName()));
			}
		}
		model.fireDataChanged(new SsapModelChangedEvent(RegistryModel.NEED_QUERY_UPDATE));
	}

	/**
	 * Create an error message using a map.
	 *
	 * @param map The map used creating the error message.
	 * @return The error message.
	 */
	private String constructErrorMessage(Map<MetadataThreadError, List<String>> map) {
		StringBuilder sb = new StringBuilder();

		for (MetadataThreadError error : map.keySet()) {
			sb.append(error.getErrorListMessage()).append(Constants.NEW_LINE);
			for (String service : map.get(error)) {
				sb.append("  - ").append(service).append(Constants.NEW_LINE);
			}
			sb.append(Constants.NEW_LINE);
		}
		return sb.toString();
	}

	/**
	 * Add a service.
	 *
	 * @param service The service to add.
	 */
	public void addService(SsaService service) {
		model.getListSsaService().add(service);
		service.addModelListener(this);
		view.updateServices();
	}

	/**
	 * Construct and return a String with informations on services.
	 *
	 * @param restrictToSelected restrict the information on selected services.
	 * @return a String with informations on services.
	 */
	private String getServicesInformations(boolean restrictToSelected) {
		StringBuilder sb = new StringBuilder();
		for (SsaService service : model.getListSsaService()) {
			if (!restrictToSelected || service.isSelected()) {
				sb.append(service.getInformations(false));
			}
		}
		return sb.toString();
	}

	/**
	 * Stop all Metadata threads and restore the default delay.
	 */
	private void stopMetadataThreads() {
		threadPool.stopAll();
		threadPool.setSleepDelay(THREADPOOL_MS_DELAY);
	}

	/**
	 * Display error message if needed.
	 */
	@Override
	public void handleTasksEnd() {
		Map<MetadataThreadError, List<String>> map = new HashMap<>();
		for (SsaService service : model.getListSsaService()) {
			if (service.isDeactivated() && !service.isErrorDisplayed()) {
				if (!map.containsKey(service.getErrorId())) {
					List<String> list = new ArrayList<>(1);
					map.put(service.getErrorId(), list);
				}
				map.get(service.getErrorId()).add(service.getTitle());
				service.errorDisplayed();
			}
		}
		if (!map.isEmpty()) {
			view.displayPopup("Warning : some services errors", constructErrorMessage(map), false);
		}
	}

	/**
	 * Add listener for each services.
	 */
	private void registerServices() {
		for (SsaService service : model.getListSsaService()) {
			service.removeModelListener(this);
			service.addModelListener(this);
		}
	}

	/**
	 * Change the model value of the registry protocol.
	 *
	 * @param protocol The registry protocol to set.
	 */
	public void changeRegistryProtocol(RegistryProtocol protocol) {
		model.setRegistryProtocol(protocol);
	}

	/**
	 * Update the registry protocol.
	 */
	private void updateRegistryProtocol() {
		RegistryProtocol protocol = model.getRegistryProtocol();
		view.getRegistryProtocolComboBox().setSelectedItem(protocol);
	}

	/**
	 * Update the registry list.
	 */
	private void updateRegistryList() {
		view.updateRegistryList(model.getListRegistryUrl(), model.getCurrentRegistryUrl());
	}
}
