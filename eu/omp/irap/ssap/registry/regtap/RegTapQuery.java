/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.registry.regtap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import eu.omp.irap.ssap.util.UtilFunction;

/**
 * Simple class to do a RegTap query.
 *
 * @author M. Boiziot
 */
public class RegTapQuery {

	private static final String QUERY = "SELECT ivoid, res_title, short_name, base_role,"
			+ " role_name, email, reference_url, access_url, res_subjects, standard_id"
			+ " FROM rr.resource AS res NATURAL JOIN rr.interface"
			+ " NATURAL JOIN rr.capability NATURAL LEFT OUTER JOIN rr.res_role"
			+ " NATURAL LEFT OUTER JOIN ("
			+ "  SELECT ivoid, ivo_string_agg(res_subject, ', ') AS res_subjects"
			+ "  FROM rr.res_subject GROUP BY ivoid) AS sbj"
			+ " WHERE (base_role = 'contact' OR base_role = 'publisher' OR base_role IS NULL)"
			+ "  AND (standard_id = 'ivo://ivoa.net/std/ssa' OR standard_id = 'ivo://ivoa.net/std/tsa')"
			+ "  AND intf_type = 'vs:paramhttp'";


	/**
	 * Utility class, do not use.
	 */
	private RegTapQuery() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * Do the query at the given registry URL then return the result as a String.
	 *
	 * @param registryUrl The registry URL
	 * @return the result as a String.
	 * @throws IOException In case of error during the query.
	 */
	public static String doQuery(String registryUrl) throws IOException {
		String fixedUrl = fixTapUrl(registryUrl);
		URL url = new URL(fixedUrl);
		HttpURLConnection co = (HttpURLConnection) url.openConnection();
		co.setRequestMethod("POST");
		co.setDoOutput(true);
		String postParam = "REQUEST=doQuery&LANG=ADQL&QUERY=" + QUERY;
		try (OutputStreamWriter writer = new OutputStreamWriter(co.getOutputStream())) {
			writer.write(postParam);
			writer.flush();
		}

		StringBuilder response = new StringBuilder();
		try (BufferedReader in = new BufferedReader(new InputStreamReader(co.getInputStream()))) {
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
		}
		UtilFunction.disconnect(co);

		return response.toString();
	}

	/**
	 * Add the missing /sync to the registry URL.
	 *
	 * @param registryUrl The registry URL
	 * @return the fixed URL.
	 */
	private static String fixTapUrl(String registryUrl) {
		String newUrl;
		if (registryUrl.endsWith("tap")) {
			newUrl = registryUrl + "/sync";
		} else if (registryUrl.endsWith("tap/")) {
			newUrl = registryUrl + "sync";
		} else {
			newUrl = registryUrl;
		}
		return newUrl;
	}

}
