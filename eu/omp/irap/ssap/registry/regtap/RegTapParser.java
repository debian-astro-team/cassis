/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.registry.regtap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.omp.irap.ssap.registry.RegistryUtils;
import eu.omp.irap.ssap.service.SsaService;
import eu.omp.irap.ssap.service.VoCapability;

/**
 * Class to handle VOTable from a RegTap query and convert it in {@link SsaService}.
 *
 * @author M. Boiziot
 */
public class RegTapParser {

	private List<SsaService> services;
	private boolean containsError;
	private int indexIvoid;
	private int indexTitle;
	private int indexShortName;
	private int indexBaseRole;
	private int indexRoleName;
	private int indexEmail;
	private int indexReferenceUrl;
	private int indexAccessUrl;
	private int indexResSubjects;
	private int indexStandardId;


	/**
	 * Constructor.
	 *
	 * @param votable The VOTable content.
	 */
	public RegTapParser(String votable) {
		this.services = new ArrayList<>();
		BasicVotableParser bvp = new BasicVotableParser(votable);
		if (bvp.isContainsError()) {
			this.containsError = true;
		} else {
			updateIndexes(bvp);
			createsServices(bvp);
		}
	}

	/**
	 * Update the indexes of the columns.
	 *
	 * @param bvp The BasicVotableParser of the VOTable.
	 */
	private void updateIndexes(BasicVotableParser bvp) {
		indexIvoid = bvp.getIndex("ivoid");
		indexTitle = bvp.getIndex("res_title");
		indexShortName = bvp.getIndex("short_name");
		indexBaseRole = bvp.getIndex("base_role");
		indexRoleName = bvp.getIndex("role_name");
		indexEmail = bvp.getIndex("email");
		indexReferenceUrl = bvp.getIndex("reference_url");
		indexAccessUrl = bvp.getIndex("access_url");
		indexResSubjects = bvp.getIndex("res_subjects");
		indexStandardId = bvp.getIndex("standard_id");
	}

	/**
	 * Merge the same services (as a List of String (a row with all columns)) based
	 *  on theirs ivoid and put them in a List then put that on a Map using the ivoid
	 *  as key.
	 *
	 * @param bvp The BasicVotableParser of the VOTable.
	 * @return the Map of the services.
	 */
	private Map<String, List<List<String>>> mergeSameServices(BasicVotableParser bvp) {
		Map<String, List<List<String>>> map = new HashMap<>();
		List<String> row;
		for (int i = 0; i < bvp.getRowsSize(); i++) {
			row = bvp.getRow(i);
			String ivoid = row.get(indexIvoid);
			List<List<String>> l;
			if (map.containsKey(ivoid)) {
				l = map.get(ivoid);
				l.add(row);
			} else {
				l = new ArrayList<>();
				l.add(row);
				map.put(ivoid, l);
			}
		}
		return map;
	}

	/**
	 * Creates the services.
	 *
	 * @param bvp The BasicVotableParser of the VOTable.
	 */
	private void createsServices(BasicVotableParser bvp) {
		List<String> listShortName = new ArrayList<>();
		Map<String, List<List<String>>> map = mergeSameServices(bvp);
		for (List<List<String>> value : map.values()) {
			SsaService service = createService(value, listShortName);
			listShortName.add(service.getShortName());
			services.add(service);
		}
	}

	/**
	 * Create a service.
	 *
	 * @param lists The rows relative to the service
	 * @param listShortName The list of known shorts names
	 * @return the created service.
	 */
	private SsaService createService(List<List<String>> lists, List<String> listShortName) {
		List<String> firstElement = lists.get(0);
		String aTitle = firstElement.get(indexTitle);
		String aShortName = firstElement.get(indexShortName);
		String anIdentifier = firstElement.get(indexIvoid);
		String aPublisher = getPublisher(lists);
		String aContact = getContact(lists);
		String aReferenceUrl = firstElement.get(indexReferenceUrl);
		List<String> someSubjects = Arrays.asList(firstElement.get(indexResSubjects).split(","));
		List<VoCapability> listCapabilities = getCapabilities(lists);

		// We want an unique no null short name.
		if (aShortName == null || aShortName.isEmpty()) {
			aShortName = RegistryUtils.createShortName(aTitle);
		}
		while (listShortName.contains(aShortName)) {
			int num = 2;
			String shortName = aShortName;
			while (listShortName.contains(shortName + num)) {
				num++;
			}
			aShortName = shortName + num;
		}

		return new SsaService(aTitle, aShortName, anIdentifier,
				aPublisher, aContact, aReferenceUrl,
				someSubjects, listCapabilities);
	}

	/**
	 * Return the capabilities of the service based on raw data for a service.
	 *
	 * @param lists The list of data
	 * @return the capabilities of the service.
	 */
	private List<VoCapability> getCapabilities(List<List<String>> lists) {
		List<VoCapability> capabilities = new ArrayList<>();
		for (List<String> row : lists) {
			VoCapability capability = new VoCapability(row.get(indexAccessUrl), row.get(indexStandardId), "1.1");
			boolean add = true;
			for (VoCapability c : capabilities) {
				if (c.equals(capability)) {
					add = false;
					break;
				}
			}
			if (add) {
				capabilities.add(capability);
			}
		}
		return capabilities;
	}

	/**
	 * Return the contact of the service based on raw data for a service.
	 *
	 * @param lists The list of data
	 * @return The contact of the service.
	 */
	private String getContact(List<List<String>> lists) {
		List<String> contactRow = getContactRow(lists);
		String contact = null;
		if (contactRow != null) {
			contact = contactRow.get(indexRoleName) + " <" + contactRow.get(indexEmail) + ">";
		}
		return contact;
	}

	/**
	 * Return the publisher of the service based on raw data for a service.
	 *
	 * @param lists The list of data
	 * @return The publisher of the service.
	 */
	private String getPublisher(List<List<String>> lists) {
		List<String> publisherRow = getPublisherRow(lists);
		String publisher = null;
		if (publisherRow != null) {
			publisher = publisherRow.get(indexRoleName);
		}
		return publisher;
	}

	/**
	 * Return the contact row of the service based on raw data for a service.
	 *
	 * @param lists The list of data
	 * @return the contact row of the service.
	 */
	private List<String> getContactRow(List<List<String>> lists) {
		for (List<String> list : lists) {
			if ("contact".equals(list.get(indexBaseRole))) {
				return list;
			}
		}
		return null;
	}

	/**
	 * Return the publisher row of the service based on raw data for a service.
	 *
	 * @param lists The list of data
	 * @return the publisher row of the service.
	 */
	private List<String> getPublisherRow(List<List<String>> lists) {
		for (List<String> list : lists) {
			if ("publisher".equals(list.get(indexBaseRole))) {
				return list;
			}
		}
		return null;
	}

	/**
	 * Return the list of services.
	 *
	 * @return the list of services.
	 */
	public List<SsaService> getServices() {
		return services;
	}

	/**
	 * Return if the parser had an error while parsing the VOTable file or if the
	 *  query status was not OK.
	 *
	 * @return true if the parser had an error while parsing the VOTable file or if the
	 *  query status was not OK, false otherwise.
	 */
	public boolean isContainsError() {
		return containsError;
	}
}
