/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.registry.regtap;

/**
 * Enumeration for the possible VOTable data type.
 *
 * @author M. Boiziot
 */
public enum VotableDatatype {

	BOOLEAN("boolean", 1),
	BIT("bit", 0),
	UNSIGNED_BYTE("unsignedByte", 1),
	SHORT("short", 2),
	INT("int", 4),
	LONG("long", 8),
	CHAR("char", 1),
	UNICODE_CHAR("unicodeChar", 2),
	FLOAT("float", 4),
	DOUBLE("double", 8),
	FLOAT_COMPLEX("floatComplex", 8),
	DOUBLE_COMPLEX("doubleComplex", 16);

	private String votableName;
	private int size;


	/**
	 * Constructor.
	 *
	 * @param votableName The name as found in the VOTables.
	 * @param size The size in the encoded stream as described in
	 *  http://www.ivoa.net/documents/REC/VOTable/VOTable-20040811.html#ToC11
	 */
	private VotableDatatype(String votableName, int size) {
		this.votableName = votableName;
		this.size = size;
	}

	/**
	 * Return the name as found in the VOTables.
	 *
	 * @return the name as found in the VOTables.
	 */
	public String getVotableName() {
		return votableName;
	}

	/**
	 * Return the size in the encoded stream as described in
	 *  http://www.ivoa.net/documents/REC/VOTable/VOTable-20040811.html#ToC11
	 *
	 * @return the size in the encoded stream3
	 */
	public int getSize() {
		return size;
	}

	/**
	 * Return the VOTable data type for the given value.
	 *
	 * @param value The value
	 * @return the VOTable data type
	 */
	public static VotableDatatype getType(String value) {
		for (VotableDatatype datatype : VotableDatatype.values()) {
			if (datatype.getVotableName().equals(value)) {
				return datatype;
			}
		}
		throw new IllegalArgumentException("Unknown Datatype");
	}

}
