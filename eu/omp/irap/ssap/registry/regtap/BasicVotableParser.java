/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.registry.regtap;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Basic VOTable parser designed specifically to handle RegTap query. It
 *  <b>can not</b> handle all VOTable content/cases.
 *
 * @author M. Boiziot
 */
public class BasicVotableParser {

	private String votable;
	private List<BasicField> fields;
	private List<List<String>> datas;
	private boolean containsError;


	/**
	 * Constructor.
	 *
	 * @param votable The content of the VOTable.
	 */
	public BasicVotableParser(String votable) {
		this.votable = votable;
		this.fields = new ArrayList<>();
		this.datas = new ArrayList<>();
		parse();
	}

	/**
	 * Return the list of the columns/fields in the VOTable.
	 *
	 * @return the list of the columns/fields in the VOTable.
	 */
	public List<String> getColumnsNames() {
		List<String> names = new ArrayList<>(fields.size());
		for (BasicField field : fields) {
			names.add(field.getName());
		}
		return Collections.unmodifiableList(names);
	}

	/**
	 * Return the name of the column/field at the given index.
	 *
	 * @param columnIndex The index for which we want the column name
	 * @return the name of the column at the given columnIndex.
	 */
	public String getColumnName(int columnIndex) {
		return fields.get(columnIndex).getName();
	}

	/**
	 * Return the index of the given column name.
	 *
	 * @param columnName The name of the column
	 * @return the index of the given column name or -1 if the column name is
	 *  null or not found.
	 */
	public int getIndex(String columnName) {
		if (columnName == null) {
			return -1;
		}
		List<String> names = getColumnsNames();
		for (int i = 0; i < getColumnsSize(); i++) {
			if (columnName.equals(names.get(i))) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Return the row as a List of String (for each column) for the given rowIndex.
	 *
	 * @param rowIndex The index of the row
	 * @return the row as a List of String (for each column).
	 */
	public List<String> getRow(int rowIndex) {
		return datas.get(rowIndex);
	}

	/**
	 * Return the value for the given row and column index.
	 *
	 * @param rowIndex The row index
	 * @param columnIndex The column index
	 * @return the value for the given row and column index.
	 */
	public String getValue(int rowIndex, int columnIndex) {
		return datas.get(rowIndex).get(columnIndex);
	}

	/**
	 * Return the number of rows.
	 *
	 * @return the number of rows.
	 */
	public int getRowsSize() {
		return datas.size();
	}

	/**
	 * Return the number of columns.
	 *
	 * @return the number of columns.
	 */
	public int getColumnsSize() {
		return fields.size();
	}

	/**
	 * Create a Document from a response (String).
	 *
	 * @param response The response as a String.
	 * @return The Document.
	 * @throws ParserConfigurationException In case of parser Exception
	 * @throws SAXException In case of SAX Exception.
	 * @throws IOException In case of IOException.
	 */
	private Document createDocument(String response) throws ParserConfigurationException,
			SAXException, IOException {
		Document doc;
		try (InputStream stream = new ByteArrayInputStream(response.getBytes());) {
			DocumentBuilderFactory docBuilderFact = DocumentBuilderFactory.newInstance();
			docBuilderFact.setFeature(
					"http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
			docBuilderFact.setFeature(
					"http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			DocumentBuilder docBuilder = docBuilderFact.newDocumentBuilder();

			doc = docBuilder.parse(stream);
			doc.getDocumentElement().normalize();
		}
		return doc;
	}

	/**
	 * Parse the VOTable.
	 */
	private void parse() {
		try {
			Document doc = createDocument(votable);

			if (!parseQueryStatus(doc)) {
				containsError = true;
				return;
			}
			parseFields(doc);

			if (!parseData(doc)) {
				containsError = true;
			}
		} catch (Exception e) {
			containsError = true;
		}
	}


	/**
	 * Parse the fields of the VOTable.
	 *
	 * @param doc The VOTable document
	 */
	private void parseFields(Document doc) {
		NodeList docFields = doc.getElementsByTagName("FIELD");
		int columnCount = docFields.getLength();

		Element element;
		for (int i = 0; i < columnCount; i++) {
			element = (Element) docFields.item(i);

			String name = getName(element);
			VotableDatatype vDatatype = getDatatype(element);
			String arraySize = getArraySize(element);

			fields.add(new BasicField(name, vDatatype, arraySize));
		}
	}

	/**
	 * Parse then return the data type for the given element.
	 *
	 * @param element The element
	 * @return the data type for the given element.
	 */
	private VotableDatatype getDatatype(Element element) {
		String datatype = null;
		if (element.hasAttribute("datatype")) {
			datatype = element.getAttribute("datatype");
		} else if (element.hasAttribute("DATATYPE")) {
			datatype = element.getAttribute("DATATYPE");
		}
		return VotableDatatype.getType(datatype);
	}

	/**
	 * Parse then return the name for the given element.
	 *
	 * @param element The element
	 * @return the name for the given element.
	 */
	private String getName(Element element) {
		String name = null;
		if (element.hasAttribute("name")) {
			name = element.getAttribute("name");
		} else if (element.hasAttribute("NAME")) {
			name = element.getAttribute("NAME");
		} else if (element.hasAttribute("id")) {
			name = element.getAttribute("id");
		} else if (element.hasAttribute("ID")) {
			name = element.getAttribute("ID");
		}
		return name;
	}

	/**
	 * Parse then return the array size for the given element.
	 *
	 * @param element The element
	 * @return the array size for the given element.
	 */
	private String getArraySize(Element element) {
		String arraysize = null;
		if (element.hasAttribute("arraysize")) {
			arraysize = element.getAttribute("arraysize");
		} else if (element.hasAttribute("ARRAYSIZE")) {
			arraysize = element.getAttribute("ARRAYSIZE");
		}
		return arraysize;
	}

	/**
	 * Parse then return the query status.
	 *
	 * @param doc The VOTable document
	 * @return the query status.
	 */
	private boolean parseQueryStatus(Document doc) {
		Element queryStatusElement = getQueryStatusElement(doc);
		boolean queryStatusOk;
		if (queryStatusElement != null && queryStatusElement.hasAttribute("value")
				&& "OK".equals(queryStatusElement.getAttribute("value"))) {
			queryStatusOk = true;
		} else {
			queryStatusOk = false;
		}
		return queryStatusOk;
	}

	/**
	 * Return the element who contains the query status.
	 *
	 * @param doc The VOTable document
	 * @return the element who contains the query status.
	 */
	private Element getQueryStatusElement(Document doc) {
		NodeList infos = doc.getElementsByTagName("INFO");
		Element element;
		for (int i = 0; i < infos.getLength(); i++) {
			element = (Element) infos.item(i);

			if (element.hasAttribute("name") &&
					"QUERY_STATUS".equals(element.getAttribute("name"))) {
				return element;
			}
		}
		return null;
	}

	/**
	 * Parse the data part of the VOTable.
	 *
	 * @param doc The VOTable document
	 * @return true for success, false otherwise.
	 */
	private boolean parseData(Document doc) {
		String encodedStream = getEncodedStream(doc);
		if (encodedStream == null) {
			return false;
		}
		String strStream = encodedStream.replaceAll("(\\r|\\n)", "");
		ByteBuffer stream = ByteBuffer.wrap(DatatypeConverter.parseBase64Binary(strStream));
		int nbColumns = getColumnsSize();

		List<String> row = null;
		int nValue = 0;
		while (stream.hasRemaining()) {
			int colId = nValue % nbColumns;
			VotableDatatype vdt = fields.get(colId).getType();

			int length = getLength(fields.get(colId), stream);

			if (colId == 0) {
				row = new ArrayList<>(nbColumns);
			}
			if (length == 0) {
				row.add("");
			} else {
				row.add(processDataBlock(stream, vdt, length));
			}

			if (colId == nbColumns - 1) {
				datas.add(row);
			}
			nValue++;
		}
		return true;
	}

	/**
	 * Process then return the String value of the encoded data block.
	 *
	 * @param stream The stream
	 * @param vdt The data type of the block to parse
	 * @param length The length
	 * @return the String value of the parsed block.
	 */
	private String processDataBlock(ByteBuffer stream, VotableDatatype vdt, int length) {
		String val;
		switch (vdt) {
		case BOOLEAN:
			val = String.valueOf(stream.getInt() != 0);
			break;
		case CHAR:
			val = processCharDataBlock(stream, length);
			break;
		case DOUBLE:
			val = String.valueOf(stream.getDouble());
			break;
		case FLOAT:
			val = String.valueOf(stream.getFloat());
			break;
		case INT:
			val = String.valueOf(stream.getInt());
			break;
		case LONG:
			val = String.valueOf(stream.getLong());
			break;
		case SHORT:
			val = String.valueOf(stream.getShort());
			break;
		case UNICODE_CHAR:
			val = processUnicodeCharDataBlock(stream, length);
			break;
		case UNSIGNED_BYTE:
			val = String.valueOf(stream.get());
			break;
		case BIT:
		case DOUBLE_COMPLEX:
		case FLOAT_COMPLEX:
		default:
			throw new IllegalArgumentException("Can not handle this datatype");
		}
		return val;
	}

	/**
	 * Process unicode char data block.
	 *
	 * @param stream The stream
	 * @param length The length
	 * @return the String value of the parsed block.
	 */
	private String processUnicodeCharDataBlock(ByteBuffer stream, int length) {
		StringBuilder sb = new StringBuilder(length);
		for (int i = 0; i < length; i += 2) {
			sb.append(stream.getChar());
		}
		return sb.toString().trim();
	}

	/**
	 * Process char data block.
	 *
	 * @param stream The stream
	 * @param length The length
	 * @return the String value of the parsed block.
	 */
	private String processCharDataBlock(ByteBuffer stream, int length) {
		StringBuilder sb = new StringBuilder(length);
		for (int i = 0; i < length && stream.position() < stream.limit(); i++) {
			sb.append((char) stream.get());
		}
		return sb.toString().trim();
	}

	/**
	 * Return the length for the given field.
	 *
	 * @param field The field
	 * @param stream The stream
	 * @return the length for the given field.
	 */
	private int getLength(BasicField field, ByteBuffer stream) {
		VotableDatatype dataType = field.getType();
		int blockSize = dataType.getSize();
		int arraySize;
		if (field.getArraySize() == null) {
			arraySize = blockSize;
		} else if ("*".equals(field.getArraySize())) {
			try {
				arraySize = stream.getInt() * blockSize;
			} catch (BufferUnderflowException e) {
				throw new BufferUnderflowException();
			}
		} else {
			arraySize = Integer.parseInt(field.getArraySize()) * blockSize;
		}
		if (arraySize != blockSize && !(dataType.equals(VotableDatatype.CHAR)
				|| dataType.equals(VotableDatatype.UNICODE_CHAR))) {
			throw new UnsupportedOperationException("Numeric data as array are not supported.");
		}
		return arraySize;
	}

	/**
	 * Return the encoded part of the VOTable as a String.
	 *
	 * @param doc The VOTable document
	 * @return the encoded part of the VOTable as a String.
	 */
	private String getEncodedStream(Document doc) {
		String encodedStream;
		try {
			Element resource = (Element) doc.getElementsByTagName("RESOURCE").item(0);
			Element table = (Element) resource.getElementsByTagName("TABLE").item(0);
			Element data = (Element) table.getElementsByTagName("DATA").item(0);
			Element binary = (Element) data.getElementsByTagName("BINARY").item(0);
			Element stream = (Element) binary.getElementsByTagName("STREAM").item(0);
			encodedStream = stream.getTextContent().trim();
		} catch (NullPointerException npe) {
			encodedStream = null;
		}
		return encodedStream;
	}

	/**
	 * Return if the parsing returned an error or if the query status was not OK.
	 *
	 * @return true if the parsing returned an error or if the query status was
	 *  not OK, false otherwise.
	 */
	public boolean isContainsError() {
		return containsError;
	}



	/**
	 * Basic representation of a field with a name, data type and array size.
	 *
	 * @author M. Boiziot
	 */
	private class BasicField {

		private final String name;
		private final VotableDatatype type;
		private final String arraySize;


		/**
		 * Constructor.
		 *
		 * @param name The name
		 * @param type The data type
		 * @param arraySize The array size
		 */
		public BasicField(String name, VotableDatatype type, String arraySize) {
			this.name = name;
			this.type = type;
			this.arraySize = arraySize;
		}

		/**
		 * Return the name.
		 *
		 * @return the name.
		 */
		public String getName() {
			return name;
		}

		/**
		 * Return the data type.
		 *
		 * @return the data type.
		 */
		public VotableDatatype getType() {
			return type;
		}

		/**
		 * Return the array size.
		 *
		 * @return the array size.
		 */
		public String getArraySize() {
			return arraySize;
		}
	}
}
