/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.request;

import eu.omp.irap.ssap.service.SsaService;


/**
 * Simple containing the data needed for a service queryData request.
 *
 * @author M. Boiziot
 */
public class RequestData {

	private final SsaService service;
	private String url;
	private String originUrl;
	private boolean knowAllParam;


	/**
	 * Constructor.
	 *
	 * @param service The service on which the request is made.
	 * @param url The URL for the query.
	 * @param knowAllParameters If the service know all the optionals parameters of the request.
	 */
	public RequestData(SsaService service, String url, boolean knowAllParameters) {
		this.service = service;
		this.url = url;
		this.originUrl = url;
		this.knowAllParam = knowAllParameters;
	}

	/**
	 * Return the service.
	 *
	 * @return the service.
	 */
	public SsaService getService() {
		return service;
	}

	/**
	 * Return the URL for the query.
	 *
	 * @return the URL for the query.
	 */
	public String getUrl() {
		return haveSavedAdvancedUrl() ? service.getSavedAdvancedUrl() : url;
	}

	/**
	 * Return if the service know all the parameters of the query.
	 *
	 * @return if the service know all the parameters of the query.
	 */
	public boolean knowAllParam() {
		return knowAllParam;
	}

	/**
	 * Change the URL.
	 *
	 * @param newUrl The new URL.
	 */
	public void setUrl(String newUrl) {
		this.url = newUrl;
		if (originUrl.equals(newUrl)) {
			this.service.setSavedAdvancedUrl(null);
		} else {
			this.service.setSavedAdvancedUrl(newUrl);
		}
	}

	/**
	 * Change the knowAllParam value.
	 *
	 * @param newKnowAllParam The new value.
	 */
	public void setKnowAllParam(boolean newKnowAllParam) {
		this.knowAllParam = newKnowAllParam;
	}

	/**
	 * Return if the service have an advanced query URL specified by the user.
	 *
	 * @return true if the service have an advanced query URL specified by the
	 *  user, false otherwise.
	 */
	public boolean haveSavedAdvancedUrl() {
		return service.haveSavedAdvancedUrl();
	}

	/**
	 * Reset user modified URL and restore default URL.
	 */
	public void resetSavedAdvancedUrl() {
		service.setSavedAdvancedUrl(null);
		url = originUrl;
	}
}
