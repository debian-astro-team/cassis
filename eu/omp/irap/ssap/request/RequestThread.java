/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.request;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import eu.omp.irap.ssap.service.SsaService;
import eu.omp.irap.ssap.util.UtilFunction;
import eu.omp.irap.ssap.value.Constants;

/**
 * Thread for doing the final request.
 *
 * @author M. Boiziot
 */
public class RequestThread extends Thread {

	private SsaService service;
	private String completeUrl;
	private boolean knowAllParameters;
	private RequestInterface reqInterface;
	private volatile Thread currentThread;


	/**
	 * Constructor.
	 *
	 * @param service The service on which the request is made.
	 * @param completeUrl The complete URL for the request.
	 * @param knowAllParameters If the service know all the optionals parameters of the request.
	 * @param reqInterface The interface who will handle the result.
	 */
	public RequestThread(SsaService service, String completeUrl,
			boolean knowAllParameters, RequestInterface reqInterface) {
		this.service = service;
		this.completeUrl = completeUrl;
		this.knowAllParameters = knowAllParameters;
		this.reqInterface = reqInterface;
	}

	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {
		currentThread = Thread.currentThread();
		// Connect to the service and keep the response.
		String response = null;
		HttpURLConnection connection = null;
		BufferedReader br = null;
		try {
			URL url = new URL(completeUrl);
			connection = (HttpURLConnection) url.openConnection();
			br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String newLine = null;
			StringBuilder sb = new StringBuilder();
			while ((newLine = br.readLine()) != null && !currentThread.isInterrupted()) {
				sb.append(newLine).append(Constants.NEW_LINE);
			}
			response = sb.toString();
			if (!currentThread.isInterrupted()) {
				reqInterface.addNewResult(service, this.knowAllParameters, response);
			}
		} catch (Exception e) {
			if (!currentThread.isInterrupted()) {
				reqInterface.addNewResult(service, this.knowAllParameters, e.getMessage());
			}
		} finally {
			UtilFunction.disconnect(connection);
			UtilFunction.close(br);
		}
	}
}
