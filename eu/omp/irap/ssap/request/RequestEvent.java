/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.request;

import java.util.List;

/**
 * Event for the result of a new request query.
 *
 * @author M. Boiziot
 */
public class RequestEvent {

	private String name;
	private RequestEventType eventType;
	private List<Object> result;
	private String votable;


	/**
	 * Constructor.
	 *
	 * @param name The event name.
	 * @param eventType The event type.
	 * @param objs The object (List of String, File or Integer) depending the eventType.
	 * @param votable The VOTable.
	 */
	public RequestEvent(String name, RequestEventType eventType, List<Object> objs, String votable) {
		this(name, eventType, objs);
		this.votable = votable;
	}

	/**
	 * Constructor.
	 *
	 * @param name The event name.
	 * @param eventType The event type.
	 * @param objs The object (List of String, File or Integer) depending the eventType.
	 */
	public RequestEvent(String name, RequestEventType eventType, List<Object> objs) {
		this.name = name;
		this.eventType = eventType;
		this.result = objs;
	}

	/**
	 * Return the name of event.
	 *
	 * @return the name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Return the event type.
	 *
	 * @return the event type.
	 */
	public RequestEventType getEventType() {
		return eventType;
	}

	/**
	 * Return the result.
	 *
	 * @return the result.
	 */
	public List<Object> getResult() {
		return result;
	}

	/**
	 * Return the VOTable associated with the event. <b>It is only available on
	 *  VOTABLE type event</b>.
	 *
	 * @return the VOTable.
	 */
	public String getVotable() {
		return votable;
	}

}
