/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import eu.omp.irap.ssap.advancedqueries.AdvancedQueriesControl;
import eu.omp.irap.ssap.registry.RegistryControl;
import eu.omp.irap.ssap.registry.RegistryModel;
import eu.omp.irap.ssap.request.RequestData;
import eu.omp.irap.ssap.request.RequestDisplay;
import eu.omp.irap.ssap.results.ResultsControl;
import eu.omp.irap.ssap.results.ResultsModel;
import eu.omp.irap.ssap.service.ServiceParameter;
import eu.omp.irap.ssap.service.SsaService;
import eu.omp.irap.ssap.ssaparameters.OptionalsParametersModel;
import eu.omp.irap.ssap.ssaparameters.Parameter;
import eu.omp.irap.ssap.ssaparameters.SsaParametersControl;
import eu.omp.irap.ssap.util.SsapModelChangedEvent;
import eu.omp.irap.ssap.util.SsapModelListener;
import eu.omp.irap.ssap.util.UtilFunction;
import eu.omp.irap.ssap.value.Constants;

/**
 * The controller of Ssap.
 *
 * @author M. Boiziot
 */
public class SsapControl implements SsapModelListener, QueryInterface {

	private static final Logger LOGGER = Logger.getLogger("SsapControl");
	private static final int SIZE_BEFORE_DOWNLOAD_WARNING = 10;
	private RegistryControl registry;
	private SsaParametersControl ssaParameters;
	private ResultsControl results;
	private SsapView view;
	private SsapModel model;


	/**
	 * Constructor.
	 */
	public SsapControl() {
		registry = new RegistryControl();
		ssaParameters = new SsaParametersControl();
		results = new ResultsControl();
		model = new SsapModel(registry.getModel(), ssaParameters.getModel(), results.getModel());
		view = new SsapView(this);
		addModelListeners();
		addListenersOnButtons();
	}

	/**
	 * Constructor.
	 *
	 * @param ssapModel The model to use.
	 */
	public SsapControl(SsapModel ssapModel) {
		registry = new RegistryControl(ssapModel.getRegistry());
		ssaParameters = new SsaParametersControl(ssapModel.getSsapParameters());
		results = new ResultsControl(ssapModel.getResults());
		model = ssapModel;
		view = new SsapView(this);
		addModelListeners();
		addListenersOnButtons();
	}

	/**
	 * Add this as model listener for registry, ssaParameters and results.
	 */
	private void addModelListeners() {
		registry.getModel().addModelListener(this);
		ssaParameters.getModel().addModelListener(this);
		results.getModel().addModelListener(this);
	}

	/**
	 * Add listeners on some buttons.
	 */
	private void addListenersOnButtons() {
		ssaParameters.getView().getAdvancedButton().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				openAdvancedQueries();
			}
		});

		ssaParameters.getView().getQueryButton().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				doQueries(getListRequestData());
			}
		});

		results.getView().getDisplaySelectedButton().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				displaySelection(RequestDisplay.DIRECT_DISPLAY);
			}
		});

		results.getView().getDisplayAllButton().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				displayAll();
			}
		});

		results.getView().getOpenButton().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				displaySelection(RequestDisplay.CHOICE_DISPLAY);
			}
		});
	}

	/**
	 * Return the controller of registry.
	 *
	 * @return the controller of registry.
	 */
	public RegistryControl getRegistry() {
		return registry;
	}

	/**
	 * Return the controller of SsaParameters.
	 *
	 * @return The controller of SsaParameters.
	 */
	public SsaParametersControl getSsaParameters() {
		return ssaParameters;
	}

	/**
	 * Return the controller of Results.
	 *
	 * @return the controller of Results.
	 */
	public ResultsControl getResults() {
		return results;
	}

	/**
	 * Return the view.
	 *
	 * @return the view.
	 */
	public SsapView getView() {
		return view;
	}

	/**
	 * Return the model.
	 *
	 * @return the model.
	 */
	public SsapModel getModel() {
		return model;
	}

	/**
	 * Handle event.
	 *
	 * @param event The event.
	 * @see eu.omp.irap.ssap.util.SsapModelListener#dataChanged(eu.omp.irap.ssap.util.SsapModelChangedEvent)
	 */
	public void dataChanged(SsapModelChangedEvent event) {
		if (SsaService.NEW_PARAMETER.equals(event.getSource())) {
			SsaService service = (SsaService) event.getValue();
			handleNewsParameters(service);
		} else if (SsaService.SELECTED_UPDATE.equals(event.getSource())) {
			SsaService service = (SsaService) event.getValue();
			handleServiceSelectionUpdate(service);
		} else if (ResultsModel.END_QUERY.equals(event.getSource())) {
			@SuppressWarnings("unchecked")
			List<String> list = (List<String>) event.getValue();
			handleEndQuery(list);
		} else if (ResultsModel.QUERY_STOPPED.equals(event.getSource())) {
			view.setCursor(Cursor.getDefaultCursor());
		} else if (RegistryModel.NEED_QUERY_UPDATE.equals(event.getSource())) {
			ssaParameters.updateQuery();
		} else if (ResultsModel.DESELECT_SERVICE.equals(event.getSource())) {
			String serviceShortName = (String) event.getValue();
			deselectService(serviceShortName);
		} else if (RegistryModel.REGISTRY_CHANGE.equals(event.getSource())) {
			ssaParameters.getOptionalsParameters().getModel().removeAllParameters();
		}
	}

	/**
	 * Deselect a service.
	 *
	 * @param serviceShortName The service short name.
	 */
	private void deselectService(String serviceShortName) {
		SsaService service =
				registry.getModel().getServiceWithShortName(serviceShortName);
		if (service != null) {
			service.setSelected(false);
		}
	}

	/**
	 * Handle the end of the query.
	 *
	 * @param list The short names of the services.
	 */
	private void handleEndQuery(List<String> list) {
		registry.getModel().setServiceWithResponse(list);
		registry.selectPreviousServiceWithResponse();
		view.setCursor(Cursor.getDefaultCursor());
	}

	/**
	 * Handle selection change on a service.
	 *
	 * @param service The service.
	 */
	private void handleServiceSelectionUpdate(SsaService service) {
		boolean selected = service.isSelected();
		OptionalsParametersModel optModel = ssaParameters.getOptionalsParameters().getModel();

		if (service.getParameters() != null && !service.getParameters().isEmpty()) {
			if (selected) {
				for (ServiceParameter param : service.getParameters()) {
					if (isOptionalParameter(param) && !isHiddenParameter(param)) {
						optModel.addParameters(param);
					}
				}
			} else {
				for (ServiceParameter param : service.getParameters()) {
					if (isOptionalParameter(param)) {
						optModel.removeServiceParameters(param, param.getName());
					}
				}
			}
		}
	}

	/**
	 * Handle news parameters on a service.
	 *
	 * @param service The service
	 */
	private void handleNewsParameters(SsaService service) {
		OptionalsParametersModel optModel = ssaParameters.getOptionalsParameters().getModel();

		for (ServiceParameter param : service.getParameters()) {
			if (isOptionalParameter(param) && !isHiddenParameter(param)) {
				optModel.addParameters(param);
			}
		}
	}

	/**
	 * Check if a parameter is optional.
	 *
	 * @param parameter The parameter to check.
	 * @return if the parameter is optional.
	 */
	private static boolean isOptionalParameter(ServiceParameter parameter) {
		String paramName = parameter.getName();
		String[] arrayReqParam = { "POS", "SIZE", "BAND", "TIME", "FORMAT" };

		for (String par : arrayReqParam) {
			if (paramName.equalsIgnoreCase(par)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Check if a parameter should be hidden.
	 *
	 * @param parameter The parameter to check.
	 * @return if the parameter should be hidden.
	 */
	private static boolean isHiddenParameter(ServiceParameter parameter) {
		String paramName = parameter.getName();
		String[] arrayReqParam = { "REQUEST", "VERSION" };

		for (String par : arrayReqParam) {
			if (paramName.equalsIgnoreCase(par)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Compute then return the list of {@link RequestData} with the current services and parameters.
	 *
	 * @return the list of request data.
	 */
	private List<RequestData> getListRequestData() {
		List<SsaService> listServices = registry.getModel().getSelectedServices();
		List<RequestData> listRequest = new ArrayList<>(listServices.size());
		String param = ssaParameters.getModel().getCompleteRequest(true);
		String serviceUrl;
		String completeUrl;

		List<String> listParam = new ArrayList<>();
		for (Parameter parameter :
			ssaParameters.getOptionalsParameters().getModel().getListConsideredParameter()) {
			listParam.add(parameter.getName());
		}

		for (SsaService service : listServices) {
			try {
				serviceUrl = UtilFunction.formatUrlForParameters(service.getBetterUrl());
				// The ISO Data Archive do not accept REQUEST parameter...
				if (serviceUrl.startsWith("http://archives.esac.esa.int/ida/aio/jsp/siap.jsp")) {
					completeUrl = serviceUrl + param;
				} else {
					completeUrl = serviceUrl + "REQUEST=queryData&" + param;
				}
				boolean knowAll = service.containsAllParameters(listParam);
				listRequest.add(new RequestData(service, completeUrl, knowAll));
			} catch (IllegalArgumentException iae) {
				LOGGER.log(Level.WARNING, "Can not format the URL for parameters for " + service.getTitle());
			}
		}

		return listRequest;
	}

	/**
	 * Display the files.
	 *
	 * @param requestDisplayId The {@link RequestDisplay} type.
	 * @param votable The VOTable.
	 * @param idxs The list of selected index (starting at 0).
	 * @param urlFiles The list of URL of the files.
	 */
	private void display(String requestDisplayId, String votable,
			List<Integer> idxs, List<String> urlFiles) {
		if (idxs.isEmpty()) {
			JOptionPane.showMessageDialog(view, "Nothing selected!",
					"Download", JOptionPane.WARNING_MESSAGE);
			return;
		}

		if (idxs.size() > SIZE_BEFORE_DOWNLOAD_WARNING) {
			int result = JOptionPane.showConfirmDialog(view,
					"You have selected " + urlFiles.size() + " files to display."
					+ Constants.NEW_LINE
					+ "The download can take some time depending on your connexion."
					+ Constants.NEW_LINE
					+ "Do you want to continue?",
					"Display",
					JOptionPane.YES_NO_OPTION);
			if (result != JOptionPane.YES_OPTION) {
				return;
			}
		}

		model.fireRequestListeners(requestDisplayId, votable, idxs, urlFiles);
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.ssap.QueryInterface#doQueries(java.util.List)
	 */
	@Override
	public void doQueries(List<RequestData> requestDataList) {
		if (requestDataList == null || requestDataList.isEmpty()) {
			view.displayError("No service selected", "Please select the services you want to query.");
		} else {
			view.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			results.doQueries(requestDataList);
		}
	}

	/**
	 * Open the Advanced Queries dialog.
	 */
	private void openAdvancedQueries() {
		AdvancedQueriesControl eqc = new AdvancedQueriesControl(getListRequestData(), this);
		JDialog dialog = new JDialog();
		dialog.setVisible(false);
		eqc.show(dialog);
	}

	/**
	 * Display the selection
	 *
	 * @param requestDisplayId The {@link RequestDisplay} type.
	 */
	private void displaySelection(String requestDisplayId) {
		String votable = results.getView().getVoTable();
		List<String> selectedUrls = results.getView().getSelectedUrls();
		List<Integer> idxs = results.getView().getSelectedIndex();

		display(requestDisplayId, votable, idxs, selectedUrls);
	}

	/**
	 * Display all.
	 */
	private void displayAll() {
		String votable = results.getView().getVoTable();
		List<String> urls = results.getView().getAllUrls();
		List<Integer> idxs = results.getView().getAllIndex();
		display(RequestDisplay.DIRECT_DISPLAY, votable, idxs, urls);
	}
}
