/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.service;

import java.util.ArrayList;
import java.util.List;

import eu.omp.irap.ssap.util.SsapListenerManager;
import eu.omp.irap.ssap.util.SsapModelChangedEvent;
import eu.omp.irap.ssap.util.UtilFunction;
import eu.omp.irap.ssap.value.Constants;


/**
 * An SsaService.
 *
 * @author M. Boiziot
 */
public class SsaService extends SsapListenerManager implements ServiceMetadataInterface {

	public static final String SELECTED_UPDATE = "selectedUpdate";
	public static final String NEW_PARAMETER = "newParameter";
	public static final String DEACTIVATED = "serviceDeactivated";
	public static final String SSA_STANDARD_ID = "ivo://ivoa.net/std/SSA";
	private static final String TSA_STANDARD_ID = "ivo://ivoa.net/std/TSA";
	public static final String COPY = "serviceCopy";
	public static final String COPY_SELECTED = "serviceCopySelected";
	public static final String COPY_ALL = "serviceCopyAll";
	public static final String GO_TO = "serviceGoTo";

	private String title;
	private String shortName;
	private String identifier;
	private String publisher;
	private String contact;
	private String referenceUrl;
	private String savedAdvancedUrl;
	private List<String> subjects;
	private List<VoCapability> capabilities;

	private List<ServiceParameter> parameters;
	private boolean selected;
	private boolean threadComplete;
	private boolean threadInstancied;
	private MetadataThreadError errorId;
	private ServiceMetadataThread thread;
	private boolean deactivated;
	private boolean errorDisplayed;


	/**
	 * Constructor.
	 *
	 * @param aTitle Title of service.
	 * @param aShortName Short name of service.
	 * @param anIdentifier Identifier of service.
	 * @param aPublisher Publisher of service.
	 * @param aContact Contact of service.
	 * @param aReferenceUrl Reference URL of service.
	 * @param someSubjects Subjects of service.
	 * @param listCapabilities Capabilities of service.
	 */
	public SsaService(String aTitle, String aShortName, String anIdentifier,
			String aPublisher, String aContact, String aReferenceUrl,
			List<String> someSubjects, List<VoCapability> listCapabilities) {
		this.title = aTitle;
		this.shortName = aShortName;
		this.identifier = anIdentifier;
		this.publisher = aPublisher;
		this.contact = aContact;
		this.referenceUrl = aReferenceUrl;
		this.subjects = someSubjects;
		this.capabilities = listCapabilities;
		this.selected = false;
	}

	/**
	 * Return the title.
	 *
	 * @return the title.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Return the short name.
	 *
	 * @return the short name.
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * Return the identifier.
	 *
	 * @return the identifier.
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * Return the publisher.
	 *
	 * @return the publisher.
	 */
	public String getPublisher() {
		return publisher;
	}

	/**
	 * Return the contact.
	 *
	 * @return the contact.
	 */
	public String getContact() {
		return contact;
	}

	/**
	 * Return the reference URL.
	 *
	 * @return the reference URL.
	 */
	public String getReferenceUrl() {
		return referenceUrl;
	}

	/**
	 * Return the list of subjects.
	 *
	 * @return the subjects.
	 */
	public List<String> getSubjects() {
		return subjects;
	}

	/**
	 * Return a formated String with the subjects.
	 *
	 * @return a String with the subjects.
	 */
	public String getFormatedStringSubjects() {
		StringBuilder sb = new StringBuilder();
		int subNb = subjects.size();
		if (subNb >= 1) {
			for (int i = 0; i < subNb - 1; i++) {
				sb.append(subjects.get(i)).append(", ");
			}
			sb.append(subjects.get(subNb-1));
		}
		return sb.toString();
	}

	/**
	 * Return the list of the capabilities.
	 *
	 * @return the capabilities.
	 */
	public List<VoCapability> getAllCapabilities() {
		return capabilities;
	}

	/**
	 * Return the list of Ssa and Tsa capabilites.
	 *
	 * @return the ssa and tsa capabilities.
	 */
	private List<VoCapability> getSsaAndTsaCapabilities() {
		List<VoCapability> listSsaCapabilities = new ArrayList<>();
		for (VoCapability capability : capabilities) {
			if (SsaService.SSA_STANDARD_ID.equalsIgnoreCase(capability.getStandardId()) ||
					SsaService.TSA_STANDARD_ID.equalsIgnoreCase(capability.getStandardId())) {
				listSsaCapabilities.add(capability);
			}
		}
		return listSsaCapabilities;
	}

	/**
	 * Return if the {@link SsaService} is selected.
	 *
	 * @return if the service is selected.
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * Set the selected state.
	 *
	 * @param isSelected the new selected value.
	 */
	public void setSelected(boolean isSelected) {
		this.selected = isSelected;
		fireDataChanged(new SsapModelChangedEvent(SsaService.SELECTED_UPDATE, this));
	}

	/**
	 * Return the better URL to do the query as some service can have several URL.
	 *
	 * @return the better URL to do the query.
	 */
	public String getBetterUrl() {
		String url = null;
		List<VoCapability> listCapa = getSsaAndTsaCapabilities();
		if (listCapa.size() == 1) {
			url = listCapa.get(0).getAccessUrl();
		} else if (listCapa.size() > 1) {
			int betterIndice = 0;
			double currentVersion = 0.0;
			for (int i = 0; i < listCapa.size(); i++) {
				VoCapability capability = listCapa.get(i);
				if (SSA_STANDARD_ID.equalsIgnoreCase(capability.getStandardId()) &&
						capability.getVersion() != null &&
						!capability.getVersion().isEmpty() &&
						Double.parseDouble(capability.getVersion()) > currentVersion) {
					currentVersion = Double.parseDouble(capability.getVersion());
					betterIndice = i;
				}
			}
			url = listCapa.get(betterIndice).getAccessUrl();
		}
		return url;
	}

	/**
	 * Create a new metadata thread then return it.
	 *
	 * @return the new metadata thread or null if the thread was already launched/completed.
	 */
	public ServiceMetadataThread getThread() {
		if (threadComplete || threadInstancied) {
			return null;
		}

		threadInstancied = true;
		errorDisplayed = false;
		errorId = null;
		String accessUrl = getBetterUrl();
		thread = new ServiceMetadataThread(accessUrl, this);

		return thread;
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.ssap.service.ServiceMetadataInterface#setParameter(java.util.List)
	 */
	@Override
	public void setParameter(List<ServiceParameter> serviceParameters) {
		parameters = addServiceInformation(serviceParameters);
		threadComplete = true;
		deactivated = false;
		fireDataChanged(new SsapModelChangedEvent(SsaService.NEW_PARAMETER, this));
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.ssap.service.ServiceMetadataInterface#notifyError(eu.omp.irap.ssap.service.MetadataThreadError)
	 */
	@Override
	public void notifyError(MetadataThreadError errorMessage) {
		errorId = errorMessage;
		deactivated = true;
		setSelected(false);
		fireDataChanged(new SsapModelChangedEvent(SsaService.DEACTIVATED));
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.ssap.service.ServiceMetadataInterface#destroyRequestThread()
	 */
	@Override
	public void destroyRequestThread() {
		if (thread != null) {
			thread.stopThread();
			thread = null;
			threadInstancied = false;
		}
	}

	/**
	 * Return if the MetadataThread for this service is instancied.
	 *
	 * @return if the MetadataThread for this service is instancied.
	 */
	public boolean isThreadInstancied() {
		return threadInstancied;
	}

	/**
	 * Return if the service is deactivated (after an error).
	 *
	 * @return if the service is deactivated.
	 */
	public boolean isDeactivated() {
		return deactivated;
	}

	/**
	 * Return if the eventual error from the metadata thread has been displayed.
	 *
	 * @return if the eventual error has been displayed.
	 */
	public boolean isErrorDisplayed() {
		return errorDisplayed;
	}

	/**
	 * Set the eventual error as displayed.
	 */
	public void errorDisplayed() {
		errorDisplayed = true;
	}

	/**
	 * Return the {@link MetadataThreadError} type.
	 *
	 * @return the {@link MetadataThreadError} type.
	 */
	public MetadataThreadError getErrorId() {
		return errorId;
	}

	/**
	 * Return the list of {@link ServiceParameter} for this service.
	 *
	 * @return the list of {@link ServiceParameter} for this service.
	 */
	public List<ServiceParameter> getParameters() {
		return parameters;
	}

	/**
	 * Return the parameter from the parameters of this service with the given name.
	 *
	 * @param parameterName The parameter name to search.
	 * @return The parameter or null if not found.
	 */
	public ServiceParameter getParameter(String parameterName) {
		if (parameters == null) {
			return null;
		}
		for (ServiceParameter param : parameters) {
			if (param.getName().equalsIgnoreCase(parameterName)) {
				return param;
			}
		}
		return null;
	}

	/**
	 * Return if the service contains all the parameters with the given names.
	 *
	 * @param listParameterName The list of parameters name to check.
	 * @return if the service contains the all the parameters with the given names.
	 */
	public boolean containsAllParameters(List<String> listParameterName) {
		for (String parameterName : listParameterName) {
			if (getParameter(parameterName) == null) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Return a String with informations on the {@link SsaService}.
	 *
	 * @param withHtml if the returned string include HTML tags.
	 * @return a String with informations on the service.
	 */
	public String getInformations(boolean withHtml) {
		StringBuilder sb = new StringBuilder();
		if (withHtml) {
			sb.append("<html>");
			sb.append("<b>").append(getTitle()).append("</b><br>");
			sb.append("<b>").append("Short Name:</b> ").append(getShortName()).append("<br>");
			if (!getSubjects().isEmpty()) {
				sb.append("<b>").append("Subjects:</b> ").append(getFormatedStringSubjects()).append("<br>");
			}
			sb.append("<b>").append("Publisher:</b> ").append(getPublisher()).append("<br>");
			if (getContact() != null && !getContact().isEmpty()) {
				sb.append("<b>").append("Contact:</b> ").append(getContact().replaceAll("<", "&#60;").replaceAll(">", "&#62;")).append("<br>");
			}
			sb.append("<b>").append("Reference URL:</b> ").append(getReferenceUrl()).append("<br>");
			sb.append("<b>").append("Identifier:</b> ").append(getIdentifier()).append("<br>");
			sb.append("<b>").append("Access URL:</b> ").append(getBetterUrl()).append("<br>");
			if (getErrorId() != null && isDeactivated()) {
				sb.append("<font color='red'><center>");
				sb.append(getErrorId().getSimpleError());
				sb.append("</center></font>");
			}
			sb.append("</html>");
		} else {
			sb.append(getTitle()).append(Constants.NEW_LINE);
			sb.append("Short Name: ").append(getShortName()).append(Constants.NEW_LINE);
			if (!getSubjects().isEmpty()) {
				sb.append("Subjects: ").append(getFormatedStringSubjects()).append(Constants.NEW_LINE);
			}
			sb.append("Publisher: ").append(getPublisher()).append(Constants.NEW_LINE);
			sb.append("Contact: ").append(getContact()).append(Constants.NEW_LINE);
			sb.append("Reference URL: ").append(getReferenceUrl()).append(Constants.NEW_LINE);
			sb.append("Identifier: ").append(getIdentifier()).append(Constants.NEW_LINE);
			sb.append("Access URL: ").append(getBetterUrl()).append(Constants.NEW_LINE);
			if (getErrorId() != null && isDeactivated()) {
				sb.append(getErrorId().getSimpleError()).append(Constants.NEW_LINE);
			}
			sb.append(Constants.NEW_LINE);
		}
		return sb.toString();
	}

	/**
	 * Search if this service contains the given keywords.
	 *
	 * @param keywords The keywords to search. This ignore case.
	 * @return true if all the keywords are present or false otherwise.
	 */
	public boolean containsKeywords(List<String> keywords) {
		for (String keyword : keywords) {
			if (!containsKeyword(keyword)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Search if this service contains the given keyword.
	 *
	 * @param keyword The keyword to search. This ignore case.
	 * @return true if this keyword is present or false otherwise.
	 */
	public boolean containsKeyword(String keyword) {
		return UtilFunction.containsIgnoreCase(title, keyword)
				|| UtilFunction.containsIgnoreCase(subjects, keyword)
				|| UtilFunction.containsIgnoreCase(shortName, keyword)
				|| UtilFunction.containsIgnoreCase(identifier, keyword)
				|| UtilFunction.containsIgnoreCase(publisher, keyword)
				|| UtilFunction.containsIgnoreCase(contact, keyword)
				|| UtilFunction.containsIgnoreCase(referenceUrl, keyword);
	}

	/**
	 * Add the service information (title and shortName to the given parameters
	 *  list) then return it.
	 *
	 * @param parameters The list of service parameters.
	 * @return The modified list of service parameters.
	 */
	private List<ServiceParameter> addServiceInformation(List<ServiceParameter> parameters) {
		if (parameters != null && !parameters.isEmpty()) {
			for (ServiceParameter param : parameters) {
				param.setService(this);
			}
		}
		return parameters;
	}

	/**
	 * Set a user defined URL for advanced query.
	 *
	 * @param url The URL for advanced query.
	 */
	public void setSavedAdvancedUrl(String url) {
		savedAdvancedUrl = url;
	}

	/**
	 * Return the URL for advanced query in case it was be modified by user.
	 *
	 * @return the URL for advanced query in case it was be modified by user.
	 */
	public String getSavedAdvancedUrl() {
		return savedAdvancedUrl;
	}

	/**
	 * Return if the service have an advanced query URL specified by the user.
	 *
	 * @return true if the service have an advanced query URL specified by the
	 *  user, false otherwise.
	 */
	public boolean haveSavedAdvancedUrl() {
		return savedAdvancedUrl != null;
	}
}
