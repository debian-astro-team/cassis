/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.service;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import eu.omp.irap.ssap.util.SsapModelChangedEvent;
import eu.omp.irap.ssap.util.SsapModelListener;

/**
 * View representation for the {@link SsaService}.
 *
 * @author M. Boiziot
 */
public class SsaServiceView extends JPanel implements SsapModelListener {

	private static final long serialVersionUID = -5756409925299572732L;
	private SsaService service;
	private JCheckBox checkbox;
	private JLabel label;


	/**
	 * Constructor.
	 *
	 * @param service The service to display.
	 */
	public SsaServiceView(SsaService service) {
		this.service = service;
		checkbox = new JCheckBox();
		checkbox.setSelected(service.isSelected());
		String labelTxt;
		if (service.getTitle().length() > 55) {
			labelTxt = service.getTitle().substring(0, 52) + "...";
		} else {
			labelTxt = service.getTitle();
		}
		label = new JLabel(labelTxt);

		add(checkbox);
		add(label);

		addListeners();
		updateToolTipText();
	}

	/**
	 * Add the listeners where needed.
	 */
	private void addListeners() {
		service.addModelListener(this);
		checkbox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				boolean selected = checkbox.isSelected();
				service.setSelected(selected);
			}
		});
		/* Same as if on the label... but must not be on label
		 * else the tooltip will not work anymore...
		 * See : http://bugs.java.com/bugdatabase/view_bug.do?bug_id=4413412
		 */
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent me) {
				if (SwingUtilities.isRightMouseButton(me)) {
					displayPopup(me);
				}
			}
		});
	}

	/**
	 * Update the tooltip text.
	 */
	private void updateToolTipText() {
		this.setToolTipText(service.getInformations(true));
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.ssap.util.SsapModelListener#dataChanged(eu.omp.irap.ssap.util.SsapModelChangedEvent)
	 */
	@Override
	public void dataChanged(final SsapModelChangedEvent event) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				if (SsaService.SELECTED_UPDATE.equals(event.getSource())) {
					boolean selected = service.isSelected();
					if (selected != checkbox.isSelected()) {
						checkbox.setSelected(selected);
					}
				} else if (SsaService.DEACTIVATED.equals(event.getSource())) {
					label.setForeground(Color.GRAY);
					updateToolTipText();
				} else if (SsaService.NEW_PARAMETER.equals(event.getSource())) {
					label.setForeground(Color.BLACK);
					updateToolTipText();
				}
			}
		});
	}

	/**
	 * Remove the listeners.
	 */
	public void removeListeners() {
		service.removeModelListener(this);
		ActionListener[] als = checkbox.getActionListeners();
		for (ActionListener al : als) {
			checkbox.removeActionListener(al);
		}
	}

	/**
	 * Return the SsaService associated at this object.
	 *
	 * @return the SsaService associated at this object.
	 */
	public SsaService getService() {
		return service;
	}

	/**
	 * Create an display a popup for copying to clipboard some informations.
	 *
	 * @param me The MouseEvent.
	 */
	public void displayPopup(MouseEvent me) {
		JPopupMenu popupMenu = new JPopupMenu();

		JMenuItem copyMenuItem = new JMenuItem("Copy informations from this service");
		copyMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				service.fireDataChanged(new SsapModelChangedEvent(SsaService.COPY, service));
			}
		});

		JMenuItem copySelectedMenuItem = new JMenuItem("Copy informations from selected services");
		copySelectedMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				service.fireDataChanged(new SsapModelChangedEvent(SsaService.COPY_SELECTED, service));
			}
		});

		JMenuItem copyAllMenuItem = new JMenuItem("Copy informations from all services");
		copyAllMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				service.fireDataChanged(new SsapModelChangedEvent(SsaService.COPY_ALL, service));
			}
		});

		popupMenu.add(copyMenuItem);
		popupMenu.add(copySelectedMenuItem);
		popupMenu.add(copyAllMenuItem);
		popupMenu.show(me.getComponent(), me.getX(), me.getY());
	}
}
