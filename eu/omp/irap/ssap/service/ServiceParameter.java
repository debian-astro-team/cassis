/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.service;

import java.util.List;

/**
 * Describe a Parameter known by a service.
 *
 * @author M. Boiziot
 */
public class ServiceParameter {

	private SsaService service;
	private final String name;
	private final String description;
	private final String unit;
	private final List<String> providedValues;
	private final String defaultValue;


	/**
	 * Constructor.
	 *
	 * @param name The name of the parameter.
	 * @param description The description of the parameter.
	 * @param unit The unit of the parameter.
	 * @param providedValues The provided values.
	 * @param defaultValue The default value.
	 */
	public ServiceParameter(String name, String description, String unit,
			List<String> providedValues, String defaultValue) {
		this.name = name;
		this.description = description;
		this.unit = unit;
		this.providedValues = providedValues;
		this.defaultValue = defaultValue;
	}

	/**
	 * Set the {@link SsaService}.
	 *
	 * @param service The service to set.
	 */
	public void setService(SsaService service) {
		this.service = service;
	}

	/**
	 * Return the {@link SsaService}.
	 *
	 * @return the service.
	 */
	public SsaService getService() {
		return service;
	}

	/**
	 * Return the name of the parameter.
	 *
	 * @return the name of the parameter.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Return the provided values.
	 *
	 * @return the provided values.
	 */
	public List<String> getProvidedValues() {
		return providedValues;
	}

	/**
	 * Return the description.
	 *
	 * @return the description.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Return the unit.
	 *
	 * @return the unit.
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * Return the default value.
	 *
	 * @return the default value.
	 */
	public String getDefaultValue() {
		return defaultValue;
	}
}
