/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import eu.omp.irap.ssap.util.UtilFunction;

/**
 * Thread who do the request the Metadata request and parse the result.
 *
 * @author M. Boiziot
 */
public class ServiceMetadataThread extends Thread {

	private String url;
	private ServiceMetadataInterface smInterface;
	private volatile Thread currentThread;


	/**
	 * Constructor.
	 *
	 * @param accessUrl The acces URL of the service.
	 * @param smInterface The interface.
	 */
	public ServiceMetadataThread(String accessUrl, ServiceMetadataInterface smInterface) {
		this.url = accessUrl;
		this.smInterface = smInterface;
	}

	/**
	 * Properly stop the thread.
	 */
	public void stopThread() {
		currentThread.interrupt();
	}

	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {
		currentThread = Thread.currentThread();
		try {
			// Format URL
			String formatedUrl = null;
			try {
				formatedUrl = UtilFunction.formatUrlForParameters(url);
				formatedUrl += "REQUEST=queryData&FORMAT=METADATA";
			} catch (IllegalArgumentException iae) {
				notifyError(MetadataThreadError.URL);
				return;
			}

			String response = null;
			if (!currentThread.isInterrupted()) {
				try {
					response = connect(formatedUrl);
				} catch (Exception e) {
					notifyError(MetadataThreadError.CONNECT);
					return;
				}
			}

			if (!currentThread.isInterrupted()) {
				try {
					Document doc = createDocument(response);

					String queryStatus = ServiceMetadataThread.extractQueryStatus(doc);

					if ("ERROR".equals(queryStatus)) {
						notifyError(MetadataThreadError.RETURN);
						return;
					}

					smInterface.setParameter(extractParams(doc));
				} catch (Exception e) {
					notifyError(MetadataThreadError.PARSING);
					return;
				}
			}
		} catch (Exception e) {
			notifyError(MetadataThreadError.UNKNOW);
		} finally {
			if (!currentThread.isInterrupted()) {
				smInterface.destroyRequestThread();
			}
		}
	}

	/**
	 * Create a Document from a response (String).
	 *
	 * @param response The response as a String.
	 * @return The Document.
	 * @throws ParserConfigurationException In case of parser Exception
	 * @throws SAXException In case of SAX Exception.
	 * @throws IOException In case of IOException.
	 */
	private Document createDocument(String response) throws ParserConfigurationException,
			SAXException, IOException {
		Document doc;
		try (InputStream stream = new ByteArrayInputStream(response.getBytes());) {
			DocumentBuilderFactory docBuilderFact = DocumentBuilderFactory.newInstance();
			docBuilderFact.setFeature(
					"http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
			docBuilderFact.setFeature(
					"http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			DocumentBuilder docBuilder = docBuilderFact.newDocumentBuilder();

			doc = docBuilder.parse(stream);
			doc.getDocumentElement().normalize();
		}
		return doc;
	}

	/**
	 * Notify an error if necessary.
	 *
	 * @param errorMessage The error message.
	 */
	private void notifyError(MetadataThreadError error) {
		if (currentThread != null && !currentThread.isInterrupted()) {
			smInterface.notifyError(error);
		}
	}

	/**
	 * Connect to an URL and return the response.
	 *
	 * @param formatedUrl The URL to connect.
	 * @return The String response of the connection.
	 * @throws IOException
	 */
	private String connect(String formatedUrl) throws IOException {
		// Connect to the service and keep the response.
		String response = null;
		HttpURLConnection connection = null;
		BufferedReader br = null;

		try {
			URL urlToConnect = new URL(formatedUrl);
			connection = (HttpURLConnection) urlToConnect.openConnection();
			connection.setConnectTimeout(4000);
			connection.setReadTimeout(4000);
			br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String newLine = null;
			StringBuilder sb = new StringBuilder();
			while (!"</VOTABLE>".equalsIgnoreCase(newLine) && (newLine = br.readLine()) != null && !currentThread.isInterrupted()) {
				sb.append(newLine);
			}
			response = sb.toString();
		} finally {
			UtilFunction.disconnect(connection);
			UtilFunction.close(br);
		}
		return response;
	}

	/**
	 * Parse the doc and return the list of parameters found on it.
	 *
	 * @param doc The document to parse.
	 * @return The list of service parameters.
	 * @throws InterruptedException If the Thread was interrupted.
	 */
	private List<ServiceParameter> extractParams(Document doc) throws InterruptedException {
		List<ServiceParameter> paramList = new ArrayList<>();
		Element resource = null;
		if (doc.getElementsByTagName("RESOURCE").getLength() >= 1) {
			resource = (Element) doc.getElementsByTagName("RESOURCE").item(0);
		} else {
			return paramList;
		}

		NodeList nl = resource.getElementsByTagName("PARAM");
		String name;
		String description;
		String defaultValue;
		String unit;
		List<String> values;

		for (int i = 0; i < nl.getLength(); i++) {
			if (currentThread.isInterrupted()) {
				throw new InterruptedException();
			}
			Element parameter = (Element) nl.item(i);

			// Use only param directly child of resource node.
			if (!parameter.getParentNode().equals(resource) || isInvalidParameter(parameter)) {
				continue;
			}

			name = getName(parameter);
			defaultValue = getAttributeValue(parameter, "value");
			unit = getAttributeValue(parameter, "unit");
			description = getDescription(parameter);
			values = getValues(parameter);

			paramList.add(new ServiceParameter(name, description, unit, values, defaultValue));
		}
		return paramList;
	}

	/**
	 * Return if the given parameter is an invalid parameter (name not starting
	 *  by "INPUT:").
	 *
	 * @param parameterElement The Element of the parameter.
	 * @return true if the given Element of a parameter is an invalid parameter,
	 * false otherwise.
	 */
	private boolean isInvalidParameter(Element parameterElement) {
		String name = parameterElement.getAttribute("name").trim();
		return name == null || !name.startsWith("INPUT:");
	}

	/**
	 * Search then return the name of the parameter from his element.
	 * This also remove "INPUT:" value from the name in case it is present.
	 *
	 * @param parameterElement The Element of the parameter.
	 * @return the name of the parameter.
	 */
	private String getName(Element parameterElement) {
		String name = parameterElement.getAttribute("name").trim();
		if (name.startsWith("INPUT:")) {
			name = name.replaceAll("INPUT:", "");
		}
		return name;
	}

	/**
	 * Search then return the value of the given attribute for the given
	 *  parameter element.
	 *
	 * @param parameterElement The Element of the parameter.
	 * @param attribute The name of the attribute.
	 * @return The attribute value or null if not found.
	 */
	private String getAttributeValue(Element parameterElement, String attribute) {
		String value = null;
		if (parameterElement.hasAttribute(attribute)) {
			value = parameterElement.getAttribute(attribute);
		}
		return value;
	}

	/**
	 * Search then return the description for a given parameter element.
	 *
	 * @param parameterElement The Element of the parameter.
	 * @return The description value or null if not found.
	 */
	private String getDescription(Element parameterElement) {
		String description = null;
		if (parameterElement.getElementsByTagName("DESCRIPTION").getLength() == 1) {
			description = parameterElement.getElementsByTagName("DESCRIPTION")
					.item(0).getTextContent().trim();
		}
		return description;
	}

	/**
	 * Search then return the list of provided values for a given parameter element.
	 *
	 * @param parameterElement The Element of the parameter.
	 * @return The list of values or null if not found.
	 */
	private List<String> getValues(Element parameterElement) {
		List<String> values = null;
		NodeList valNodeList = parameterElement.getElementsByTagName("VALUES");
		Element valuesElement = (Element) valNodeList.item(0);
		if (valuesElement != null && valuesElement.getElementsByTagName("OPTION").getLength() >= 1) {
			NodeList options = valuesElement.getElementsByTagName("OPTION");
			values = new ArrayList<>(options.getLength());
			int nbOptions = options.getLength();
			Element currentElement;
			for (int j = 0; j < nbOptions; j++) {
				currentElement = (Element) options.item(j);
				values.add(getValue(currentElement));
			}
		}
		return values;
	}

	/**
	 * Return the value of the value attribute for an element.
	 *
	 * @param element The element.
	 * @return The value.
	 */
	private String getValue(Element element) {
		String val;
		if (element.hasAttribute("value")) {
			val = element.getAttribute("value").trim();
		} else {
			val = element.getTextContent().trim();
		}
		return val;
	}

	/**
	 * Extract the Query Status from a document...
	 * Or at least try as lot of service did'nt return it...
	 *
	 * @param doc The document to parse.
	 * @return The query status or "NOT FOUND".
	 */
	private static String extractQueryStatus(Document doc) {
		try {
			NodeList nl = doc.getElementsByTagName("INFO");
			for (int i = 0; i < nl.getLength(); i++) {
				Element element = (Element) nl.item(i);
				if ("QUERY_STATUS".equalsIgnoreCase(element.getAttribute("name"))) {
					return element.getAttribute("value");
				}
			}
		} catch (Exception e) {
			return "NOT FOUND";
		}
		return "NOT FOUND";
	}
}
