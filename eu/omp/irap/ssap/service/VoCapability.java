/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.service;

/**
 * Basic description a VO capability.
 *
 * @author M. Boiziot
 */
public class VoCapability {

	private final String accessUrl;
	private final String standardId;
	private final String version;


	/**
	 * Constructor.
	 *
	 * @param accessUrl The access URL
	 * @param standardId The standard id
	 * @param version The version
	 */
	public VoCapability(String accessUrl, String standardId, String version) {
		this.accessUrl = accessUrl;
		this.standardId = standardId;
		this.version = version;
	}

	/**
	 * Return the access URL.
	 *
	 * @return the access URL.
	 */
	public String getAccessUrl() {
		return accessUrl;
	}

	/**
	 * Return the standard id.
	 *
	 * @return the standard id.
	 */
	public String getStandardId() {
		return standardId;
	}

	/**
	 * Return the version.
	 *
	 * @return the version.
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Create the hashCode.
	 *
	 * @return the hashCode.
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accessUrl == null) ? 0 : accessUrl.hashCode());
		result = prime * result + ((standardId == null) ? 0 : standardId.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	/**
	 * Test the equality with another object (based on accessUrl, standardId and version)
	 *
	 * @return true if the two objects are equals, false otherwise.
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VoCapability other = (VoCapability) obj;
		if (accessUrl == null) {
			if (other.accessUrl != null)
				return false;
		} else if (!accessUrl.equals(other.accessUrl))
			return false;
		if (standardId == null) {
			if (other.standardId != null)
				return false;
		} else if (!standardId.equals(other.standardId))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

}
