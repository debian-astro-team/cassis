/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.service;

/**
 * Enum for Metadata request possible errors.
 *
 * @author M. Boiziot
 */
public enum MetadataThreadError {

	URL("Error while formating the url for the metadata request for:",
			"Error while formating the url for the metadata request."),

	CONNECT("Services not responding:", "The service does not respond."),

	PARSING("Services returning incorrect result:",
			"The service does not return correct result."),

	RETURN("Services returning an error:", "The service returned an error."),

	UNKNOW("Unknow error for:", "Unknow error.");

	private final String errorListMsg;
	private final String simpleError;


	/**
	 * Constructor.
	 *
	 * @param errorList The message to display on the error list.
	 * @param simpleError The message to display on the service popup.
	 */
	private MetadataThreadError(String errorList, String simpleError) {
		this.errorListMsg = errorList;
		this.simpleError = simpleError;
	}

	/**
	 * Return the message to display on the error list.
	 *
	 * @return the message to display on the error list.
	 */
	public String getErrorListMessage() {
		return errorListMsg;
	}

	/**
	 * Return the message to display on the service popup.
	 *
	 * @return the message to display on the service popup.
	 */
	public String getSimpleError() {
		return simpleError;
	}
}
