/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import eu.omp.irap.ssap.registry.RegistryModel;
import eu.omp.irap.ssap.request.RequestEvent;
import eu.omp.irap.ssap.request.RequestEventType;
import eu.omp.irap.ssap.request.RequestListener;
import eu.omp.irap.ssap.request.RequestTypeListener;
import eu.omp.irap.ssap.results.ResultsModel;
import eu.omp.irap.ssap.ssaparameters.SsaParametersModel;
import eu.omp.irap.ssap.util.SsapListenerManager;
import eu.omp.irap.ssap.util.UtilFunction;

/**
 * The model for Ssap.
 *
 * @author M. Boiziot
 */
public class SsapModel extends SsapListenerManager {

	private static final Logger LOGGER = Logger.getLogger("SsapModel");
	private static final int BUFFER_SIZE = 4096;
	private static final String TMP_DIR = System.getProperty("java.io.tmpdir");
	private RegistryModel registry;
	private SsaParametersModel ssapParameters;
	private ResultsModel results;
	private List<RequestTypeListener> requestListenerList;


	/**
	 * Constructor.
	 */
	public SsapModel() {
		this(new RegistryModel(), new SsaParametersModel(), new ResultsModel());
	}

	/**
	 * Constructor.
	 *
	 * @param registryModel The registry model.
	 * @param ssaParametersModel the SsaParameters model.
	 * @param resultsModel The results model.
	 */
	public SsapModel(RegistryModel registryModel, SsaParametersModel ssaParametersModel,
			ResultsModel resultsModel) {
		registry = registryModel;
		ssapParameters = ssaParametersModel;
		results = resultsModel;
		requestListenerList = new ArrayList<>();
	}

	/**
	 * Return the registry model.
	 *
	 * @return the registry model.
	 */
	public RegistryModel getRegistry() {
		return registry;
	}

	/**
	 * Return the SsaParemeters model.
	 *
	 * @return the SsaParameters model.
	 */
	public SsaParametersModel getSsapParameters() {
		return ssapParameters;
	}

	public ResultsModel getResults() {
		return results;
	}

	/**
	 * Add a new listener for the result as String.
	 *
	 * @param reqListener The listener to add.
	 */
	public void addStringListener(RequestListener reqListener) {
		requestListenerList.add(new RequestTypeListener(RequestEventType.STRING, reqListener));
	}

	/**
	 * Add a new listener for the result as File.
	 *
	 * @param fileListener The listener to add.
	 */
	public void addFileListener(RequestListener fileListener) {
		requestListenerList.add(new RequestTypeListener(RequestEventType.FILE, fileListener));
	}

	/**
	 * Remove a listener from the listener list.
	 *
	 * @param listener The listener to remove.
	 */
	public void removeRequestListener(RequestListener listener) {
		List<RequestTypeListener> listToRemove = new ArrayList<>();
		for (RequestTypeListener rtl : requestListenerList) {
			if (rtl.getListener().equals(listener)) {
				listToRemove.add(rtl);
			}
		}
		for (RequestTypeListener rtl : listToRemove) {
			requestListenerList.remove(rtl);
		}
	}

	/**
	 * Remove all request listeners.
	 */
	public void removeAllRequestListener() {
		requestListenerList.clear();
	}

	/**
	 * Create an event and send it to the String listeners.
	 *
	 * @param stringListeners The String listeners.
	 * @param name The name of the event.
	 * @param response The list of URL of the files.
	 */
	private void fireStringListeners(List<RequestListener> stringListeners,
			List<String> response, String name) {
		List<Object> listObj = new ArrayList<>(response);
		RequestEvent event = new RequestEvent(name, RequestEventType.STRING, listObj);
		for (RequestListener rl : stringListeners) {
			rl.newRequestResult(event);
		}
	}

	/**
	 * Create an event and send it to the File listeners.
	 *
	 * @param stringListeners The File listeners (must not be null or empty).
	 * @param name The name of the event.
	 * @param response The list of URL of the files.
	 */
	private void fireFileListeners(final List<RequestListener> fileListeners,
			final List<String> response, final String name) {
		new Thread() {
			@Override
			public void run() {
				List<Object> fileList = new ArrayList<>();

				for (String stringUrl : response) {
					File file = downloadFile(stringUrl);
					if (file != null) {
						fileList.add(file);
					}
				}

				RequestEvent event = new RequestEvent(name, RequestEventType.FILE, fileList);
				for (RequestListener rl : fileListeners) {
					rl.newRequestResult(event);
				}
			}
		}.start();
	}

	/**
	 * Download a file.
	 *
	 * @param stringUrl The URL of the file.
	 * @return the downloaded file or null if there was an error.
	 */
	private File downloadFile(String stringUrl) {
		HttpURLConnection connection = null;
		InputStream is = null;
		FileOutputStream fos = null;
		String filePath;
		File returnFile = null;
		try {
			URL url = new URL(stringUrl);
			connection = (HttpURLConnection) url.openConnection();

			// Determine where to save the file.
			String ext = UtilFunction.getFileExtension(stringUrl);
			String fileName = getFileName(stringUrl, connection, ext);
			filePath = TMP_DIR + File.separatorChar + fileName;

			try {
				fos = new FileOutputStream(filePath);
			} catch (FileNotFoundException fnfe) {
				String extFile = UtilFunction.getFileExtension(fileName);
				filePath = getUniqueFilePath(fileName, extFile);
				fos = new FileOutputStream(filePath);
			}

			is = connection.getInputStream();
			download(is, fos);

			returnFile = new File(filePath);
		} catch (Exception e) {
			LOGGER.log(Level.WARNING,
					"Error while trying to download " + stringUrl + ". File ignored.");
		} finally {
			closeStreams(connection, is, fos);
		}
		return returnFile;
	}

	/**
	 * Download from the {@link InputStream} and write the result to the {@link FileOutputStream}.
	 *
	 * @param is The {@link InputStream}.
	 * @param fos The {@link FileOutputStream}.
	 * @throws IOException In case of error during the download.
	 */
	private void download(InputStream is, FileOutputStream fos) throws IOException {
		byte[] buffer = new byte[BUFFER_SIZE] ;
		int bytesRead = -1;
		while ((bytesRead = is.read(buffer)) != -1) {
			fos.write(buffer, 0, bytesRead);
		}
	}

	/**
	 * Get a path to a non existing file.
	 *
	 * @param fileNameOrigin The name of the file to start with.
	 * @param extFile The extension of the file.
	 * @return The unique path.
	 */
	private String getUniqueFilePath(String fileNameOrigin, String extFile) {
		String filePath;
		String fileName;
		File tmpFile;
		int val = 1;
		do {
			val++;
			if (extFile.isEmpty() || extFile.length() >= 10) {
				fileName = fileNameOrigin + "_" + val;
			} else {
				int index = fileNameOrigin.indexOf(extFile);
				fileName = fileNameOrigin.substring(0, index - 1) + "_" + val + "." + extFile;
			}
			filePath = TMP_DIR + File.separatorChar + fileName;
			tmpFile = new File(filePath);
		} while (tmpFile.exists());
		return filePath;
	}

	/**
	 * Search in HTTP header for a name to use for the file, if not present try with the URL.
	 *
	 * @param stringUrl The URL as a String.
	 * @param connection The {@link HttpURLConnection}.
	 * @param ext The extension of the file.
	 * @return The file name to use.
	 */
	private String getFileName(String stringUrl, HttpURLConnection connection,
			String ext) {
		String fileName;
		if (ext.isEmpty() || ext.startsWith("php") || ext.length() >= 8) {
			String header = connection.getHeaderField("Content-Disposition");
			if (header != null && header.indexOf('=') != -1) {
				fileName = header.split("=")[1].replaceAll("\"", "").replaceAll("[^a-zA-Z0-9.-]", "_");
			} else {
				fileName = new File(stringUrl).getName().replaceAll("[^a-zA-Z0-9.-]", "_");
			}
		} else {
			fileName = new File(stringUrl).getName();
		}
		return fileName;
	}

	/**
	 * Close streams and connection.
	 *
	 * @param connection The {@link HttpURLConnection}.
	 * @param is The {@link InputStream}.
	 * @param fos The {@link FileOutputStream}.
	 */
	private void closeStreams(HttpURLConnection connection, InputStream is,
			FileOutputStream fos) {
		UtilFunction.disconnect(connection);
		UtilFunction.close(is);
		UtilFunction.close(fos);
	}

	/**
	 * Create an event and send it to the listeners.
	 *
	 * @param name The name of the event.
	 * @param votable The VOTable.
	 * @param idxs The list of selected index (starting at 0).
	 * @param response The list of URL of the files.
	 */
	public void fireRequestListeners(final String name, String votable,
			List<Integer> idxs, final List<String> response) {
		if (!requestListenerList.isEmpty()) {
			final List<RequestListener> stringListeners = new ArrayList<>();
			final List<RequestListener> fileListeners = new ArrayList<>();
			final List<RequestListener> votableListeners = new ArrayList<>();

			for (RequestTypeListener rtl : requestListenerList) {
				if (rtl.getType() == RequestEventType.STRING) {
					stringListeners.add(rtl.getListener());
				} else if (rtl.getType() == RequestEventType.FILE) {
					fileListeners.add(rtl.getListener());
				} else if (rtl.getType() == RequestEventType.VOTABLE) {
					votableListeners.add(rtl.getListener());
				}
			}

			if (!stringListeners.isEmpty()) {
				fireStringListeners(stringListeners, response, name);
			}
			if (!fileListeners.isEmpty()) {
				fireFileListeners(fileListeners, response, name);
			}
			if (!votableListeners.isEmpty()) {
				fireVotableListeners(votableListeners, votable, idxs, name);
			}
		}
	}

	/**
	 * Return an unmodifiable List of request listeners.
	 *
	 * @return an unmodifiable List of request listeners.
	 */
	protected List<RequestTypeListener> getRequestListeners() {
		return Collections.unmodifiableList(requestListenerList);
	}

	/**
	 * Add a new listener for the result as VOTable.
	 *
	 * @param reqListener The listener to add.
	 */
	public void addVotableListener(RequestListener reqListener) {
		requestListenerList.add(new RequestTypeListener(RequestEventType.VOTABLE, reqListener));
	}

	/**
	 * Create an event and send it to the VOTable listeners.
	 *
	 * @param votableListeners The VOTable listeners.
	 * @param votable The VOTable.
	 * @param idxs The list of selected index (starting at 0).
	 * @param name The name of the event.
	 */
	private void fireVotableListeners(List<RequestListener> votableListeners,
			String votable, List<Integer> idxs, String name) {
		List<Object> listObj = new ArrayList<>(idxs);
		RequestEvent event = new RequestEvent(name, RequestEventType.VOTABLE, listObj, votable);
		for (RequestListener rl : votableListeners) {
			rl.newRequestResult(event);
		}
	}
}
