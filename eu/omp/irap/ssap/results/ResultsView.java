/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.results;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Collections;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;

import eu.omp.irap.ssap.util.SsapModelChangedEvent;
import eu.omp.irap.ssap.value.Constants;

/**
 * View for the Results.
 *
 * @author M. Boiziot
 */
public class ResultsView extends JPanel {

	private static final long serialVersionUID = -4895845447313280857L;
	private static final Color TAB_COLOR_KNOW_ALL = Color.GREEN.darker();
	private static final Color TAB_COLOR_DONT_KNOW_ALL = Color.ORANGE.darker();
	private static final int SIZE_BEFORE_DOWNLOAD_WARNING = 10;
	private ResultsControl control;
	private JTabbedPane tabbedPane;
	private JButton deselectAllButton;
	private JButton downloadSelectedButton;
	private JButton downloadAllButton;
	private JButton displaySelectedButton;
	private JButton displayAllButton;
	private JButton clearResultsButton;
	private JButton openButton;


	/**
	 * Constructor.
	 *
	 * @param control The controller.
	 */
	public ResultsView(ResultsControl control) {
		super(new BorderLayout());
		this.control = control;
		this.setBorder(BorderFactory.createTitledBorder("Results"));
		this.setPreferredSize(new Dimension(600, 300));
		add(getTabbedPane(), BorderLayout.CENTER);
		add(initsButtonsPanel(), BorderLayout.SOUTH);
	}

	/**
	 * Create if needed then return the {@link JTabbedPane} for the ResultView.
	 *
	 * @return the {@link JTabbedPane} for the ResultView.
	 */
	public JTabbedPane getTabbedPane() {
		if (tabbedPane == null) {
			tabbedPane = new JTabbedPane();
			for (Result result : control.getModel().getResults()) {
				addComponent(result);
			}
		}
		return tabbedPane;
	}

	/**
	 * Create a JPanel and add the buttons to it.
	 *
	 * @return the panel.
	 */
	private JPanel initsButtonsPanel() {
		JPanel panel = new JPanel();
		panel.add(getDeselectAllButton());
		panel.add(getDownloadSelectedButton());
		panel.add(getDownloadAllButton());
		panel.add(getDisplaySelectedButton());
		panel.add(getDisplayAllButton());
		panel.add(getOpenButton());
		panel.add(getClearResultsButton());
		return panel;
	}

	/**
	 * Create if needed then return the Deselect all {@link JButton}.
	 *
	 * @return the Deselect all button.
	 */
	public JButton getDeselectAllButton() {
		if (deselectAllButton == null) {
			deselectAllButton = new JButton("Deselect all");
			deselectAllButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (getTabbedPane().getTabCount() >= 1) {
						ResultView rv = (ResultView) getTabbedPane().getSelectedComponent();

						rv.getResultTable().clearSelection();
					}
				}
			});
		}
		return deselectAllButton;
	}

	/**
	 * Create if needed then return the Download selected {@link JButton}.
	 *
	 * @return the Download selected button.
	 */
	public JButton getDownloadSelectedButton() {
		if (downloadSelectedButton == null) {
			downloadSelectedButton = new JButton("Download selected");
			downloadSelectedButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					downloadFiles(getSelectedUrls());
				}
			});
		}
		return downloadSelectedButton;
	}

	/**
	 * Create if needed then return the Download all {@link JButton}.
	 *
	 * @return the Download all button.
	 */
	public JButton getDownloadAllButton() {
		if (downloadAllButton == null) {
			downloadAllButton = new JButton("Download all");
			downloadAllButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					downloadFiles(getAllUrls());
				}
			});
		}
		return downloadAllButton;
	}

	/**
	 * Create if needed then return the Display selected {@link JButton}.
	 *
	 * @return the Display selected button.
	 */
	public JButton getDisplaySelectedButton() {
		if (displaySelectedButton == null) {
			displaySelectedButton = new JButton("Display selected");
			// Listener set in SsapControl.
		}
		return displaySelectedButton;
	}

	/**
	 * Create if needed then return the Display all {@link JButton}.
	 *
	 * @return the Display all button.
	 */
	public JButton getDisplayAllButton() {
		if (displayAllButton == null) {
			displayAllButton = new JButton("Display all");
			// Listener set in SsapControl.
		}
		return displayAllButton;
	}

	/**
	 * Create if needed then return the Clear results {@link JButton}.
	 *
	 * @return the Clear results button.
	 */
	public JButton getClearResultsButton() {
		if (clearResultsButton == null) {
			clearResultsButton = new JButton("Clear results");
			clearResultsButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					control.clearResults();
					getOpenButton().setEnabled(true);
				}
			});
		}
		return clearResultsButton;
	}

	/**
	 * Create if needed then return the Open {@link JButton}.
	 *
	 * @return the Open with button.
	 */
	public JButton getOpenButton() {
		if (openButton == null) {
			openButton = new JButton("Open");
			// Listener set in SsapControl
		}
		return openButton;
	}

	/**
	 * Add a new Result to the {@link JTabbedPane}.
	 *
	 * @param result The result to add.
	 */
	public void addComponent(final Result result) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				createResultView(result);
			}
		});
	}

	/**
	 * Create the {@link ResultView} with the data from the given {@link Result} and
	 *  add it to the {@link JTabbedPane}.
	 *
	 * @param result The {@link Result} for which we create the view.
	 */
	private void createResultView(final Result result) {
		final ResultView rv = new ResultView(result, control);
		final TabComponentButton tcb =
				new TabComponentButton(result, tabbedPane, control);

		getTabbedPane().addTab(result.getServiceShortName(), rv);
		getTabbedPane().setTabComponentAt(tabbedPane.getTabCount() - 1, tcb);

		tcb.getDeleteButton().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deleteClick(result, rv, tcb);
			}
		});
		updateTabColor(result, tcb);
	}

	/**
	 * Update the color of the tab depending on the result.
	 *
	 * @param result The result.
	 * @param tcb The tab to update.
	 */
	private void updateTabColor(final Result result, final TabComponentButton tcb) {
		if (result.isServiceKnowAllParam()) {
			tcb.setColor(TAB_COLOR_KNOW_ALL);
		} else {
			tcb.setColor(TAB_COLOR_DONT_KNOW_ALL);
		}
	}

	/**
	 * Action to do when the user click on the delete button.
	 *
	 * @param result The {@link Result} associated with the click.
	 * @param rv The {@link ResultView} associated with the click.
	 * @param tcb The {@link TabComponentButton} associated with the click.
	 */
	private void deleteClick(final Result result, final ResultView rv,
			final TabComponentButton tcb) {
		getTabbedPane().remove(rv);
		tcb.removeDeleteButtonListeners();
		rv.getResultTable().removeListeners();
		control.getModel().removeResult(result);
		control.getModel().fireDataChanged(
				new SsapModelChangedEvent(
						ResultsModel.DESELECT_SERVICE,
						result.getServiceShortName()));
		if (getTabbedPane().getTabCount() == 0) {
			getOpenButton().setEnabled(true);
		}
	}

	/**
	 * Remove all ResultView from the {@link JTabbedPane}.
	 */
	public void removeAllComponents() {
		getTabbedPane().removeAll();
	}

	/**
	 * Return the list of URL of the selected element on the current Tab/{@link ResultView}.
	 *
	 * @return the list of URL of the selected element on the current Tab/{@link ResultView}
	 * or null if there is not {@link ResultView} on the current {@link JTabbedPane}.
	 */
	public List<String> getSelectedUrls() {
		if (getTabbedPane().getTabCount() >= 1) {
			ResultView rv = (ResultView) getTabbedPane().getSelectedComponent();

			return rv.getResultTable().getSelectedUrls();
		}
		return Collections.emptyList();
	}

	/**
	 * Return the list of URL of the elements on the current Tab/{@link ResultView}.
	 *
	 * @return the list of URL of the elements on the current Tab/{@link ResultView}.
	 */
	public List<String> getAllUrls() {
		if (getTabbedPane().getTabCount() >= 1) {
			ResultView rv = (ResultView) getTabbedPane().getSelectedComponent();

			return rv.getResultTable().getAllUrls();
		}
		return Collections.emptyList();
	}

	/**
	 * Check the number of files to download then start the download of the files.
	 *
	 * @param urlFiles the list of String URL of files to download.
	 */
	private void downloadFiles(List<String> urlFiles) {
		if (urlFiles.isEmpty()) {
			JOptionPane.showMessageDialog(this, "Nothing selected!",
					"Download", JOptionPane.WARNING_MESSAGE);
			return;
		}

		if (urlFiles.size() > SIZE_BEFORE_DOWNLOAD_WARNING) {
			int result = JOptionPane.showConfirmDialog(this,
					"You have selected " + urlFiles.size() + " files to download."
					+ Constants.NEW_LINE
					+ "The download can take some time depending on your connexion."
					+ Constants.NEW_LINE
					+ "Do you want to continue?",
					"Download",
					JOptionPane.YES_NO_OPTION);
			if (result != JOptionPane.YES_OPTION) {
				return;
			}
		}

		JFileChooser folderChooser = new JFileChooser();
		folderChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		folderChooser.setMultiSelectionEnabled(false);
		int result = folderChooser.showSaveDialog(this);
		if (result == JFileChooser.APPROVE_OPTION) {
			File folder = folderChooser.getSelectedFile();
			if (folder.exists() && folder.isDirectory()) {
				new DownloadPane(folder, urlFiles).execute();
			}
		}
	}

	/**
	 * Return the list of index of the selected element on the current Tab/{@link ResultView}.
	 *
	 * @return the list of index of the selected element on the current Tab/{@link ResultView}
	 * or null if there is not {@link ResultView} on the current {@link JTabbedPane}.
	 */
	public List<Integer> getSelectedIndex() {
		if (getTabbedPane().getTabCount() >= 1) {
			ResultView rv = (ResultView) getTabbedPane().getSelectedComponent();
			return rv.getResultTable().getSelectedIndex();
		}
		return Collections.emptyList();
	}

	/**
	 * Return the list all index in the table starting at 0 not at 1 as in GUI
	 *  on the current Tab/{@link ResultView}.
	 *
	 * @return the list all index in the table starting at 0 not at 1 as in GUI
	 *  on the current Tab/{@link ResultView}.
	 */
	public List<Integer> getAllIndex() {
		if (getTabbedPane().getTabCount() >= 1) {
			ResultView rv = (ResultView) getTabbedPane().getSelectedComponent();
			return rv.getResultTable().getAllIndex();
		}
		return Collections.emptyList();
	}

	/**
	 * Return the VOTable on the current Tab/{@link ResultView} as a String.
	 *
	 * @return the VOTable on the current Tab/{@link ResultView} as a String.
	 */
	public String getVoTable() {
		if (getTabbedPane().getTabCount() >= 1) {
			ResultView rv = (ResultView) getTabbedPane().getSelectedComponent();
			return rv.getVoTable();
		}
		return null;
	}
}
