/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.results;

/**
 * Class who represent a field.
 *
 * @author M. Boiziot
 */
public class Field {

	private String name;
	private String datatype;
	private String unit;
	private String ucd;
	private String utype;
	private String description;


	/**
	 * Constructor.
	 */
	public Field() {
		super();
	}

	/**
	 * Return the name of the field.
	 *
	 * @return the name of the field.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set the name of the field.
	 *
	 * @param name The name to set.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Return the datatype of the field.
	 *
	 * @return the datatype of the field.
	 */
	public String getDatatype() {
		return datatype;
	}

	/**
	 * Set the datatype of the field.
	 *
	 * @param datatype the datatype to set.
	 */
	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}

	/**
	 * Return the unit of the field.
	 *
	 * @return the unit of the field.
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * Set the unit of the field.
	 *
	 * @param unit the unit to set.
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * Return the Unified Content Descriptor (UCD) of the field.
	 *
	 * @return the Unified Content Descriptor (UCD) of the field.
	 */
	public String getUcd() {
		return ucd;
	}

	/**
	 * Set the Unified Content Descriptor (UCD) of the field.
	 *
	 * @param ucd the Unified Content Descriptor (UCD) to set.
	 */
	public void setUcd(String ucd) {
		this.ucd = ucd;
	}

	/**
	 * Return the utype of the field.
	 *
	 * @return the utype of the field.
	 */
	public String getUtype() {
		return utype;
	}

	/**
	 * Set the utype of the field.
	 *
	 * @param utype the utype to set.
	 */
	public void setUtype(String utype) {
		this.utype = utype;
	}

	/**
	 * Return the Description of the field.
	 *
	 * @return the Description of the field.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Set the description of the field.
	 *
	 * @param description the description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Field [name=" + name + ", datatype=" + datatype + ", unit=" + unit + ", ucd=" + ucd + ", utype="
				+ utype + "]";
	}
}
