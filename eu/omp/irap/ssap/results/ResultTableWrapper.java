/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.results;

import javax.swing.table.AbstractTableModel;

/**
 * Wrapper class for a result as AbstractTableModel to display it to a JTable.
 *
 * @author M. Boiziot
 */
public class ResultTableWrapper extends AbstractTableModel {

	private static final long serialVersionUID = -6102383494193028764L;
	private static final int COLUMN_INDEX = 0;
	private Result result;


	/**
	 * Constructor.
	 *
	 * @param result The {@link Result} to wrap.
	 */
	public ResultTableWrapper(Result result) {
		if (result == null) {
			throw new NullPointerException("Provided result is null.");
		}
		this.result = result;
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount() {
		return result.getData().size();
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return result.getFieldList().size() + 1;
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if (columnIndex == COLUMN_INDEX) {
			return rowIndex + 1;
		} else {
			return result.getData().get(rowIndex).get(columnIndex - 1);
		}
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#isCellEditable(int, int)
	 */
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	@Override
	public String getColumnName(int column) {
		if (column == 0) {
			return "Index";
		} else {
			return result.getFieldList().get(column - 1).getName();
		}
	}

	/**
	 * Create the header tooltip message.
	 *
	 * @param column The column for which the message is created.
	 * @return The tooltip message.
	 */
	public String getHeaderToolTip(int column) {
		StringBuilder sb = new StringBuilder();
		if (column == 0) {
			return "Result index";
		}

		Field field = result.getFieldList().get(column - 1);

		sb.append("<html>");
		if (field.getName() != null) {
			sb.append("<b>").append(field.getName()).append("</b>").append("<br>");
		}
		if (field.getDescription() != null) {
			sb.append(field.getDescription()).append("<br>");
		}
		if (field.getUnit() != null) {
			sb.append("Unit: ").append(field.getUnit()).append("<br>");
		}
		if (field.getUcd() != null) {
			sb.append("UCD: ").append(field.getUcd()).append("<br>");
		}
		if (field.getUtype() != null) {
			sb.append("Utype: ").append(field.getUtype()).append("<br>");
		}
		sb.append("</htmlt>");
		return sb.toString();
	}

	/**
	 * Return the URL for an element.
	 *
	 * @param row The row of the element in the Table.
	 * @return The URL of the element.
	 */
	public String getUrl(int row) {
		return result.getUrl(row);
	}
}
