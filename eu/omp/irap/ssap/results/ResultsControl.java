/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.results;

import java.util.List;

import eu.omp.irap.ssap.request.RequestData;
import eu.omp.irap.ssap.util.SsapModelChangedEvent;
import eu.omp.irap.ssap.util.SsapModelListener;
import eu.omp.irap.ssap.util.UtilFunction;

/**
 * Control class for the Results.
 *
 * @author M. Boiziot
 */
public class ResultsControl implements SsapModelListener, CopyServicesInformations, ResultSelectionChange {

	private ResultsView view;
	private ResultsModel model;


	/**
	 * Constructor.
	 */
	public ResultsControl() {
		this(new ResultsModel());
	}

	/**
	 * Constructor.
	 *
	 * @param pModel The model to use.
	 */
	public ResultsControl(ResultsModel pModel) {
		model = pModel;
		view = new ResultsView(this);
		model.addModelListener(this);
	}

	/**
	 * Return the view.
	 *
	 * @return the view.
	 */
	public ResultsView getView() {
		return view;
	}

	/**
	 * Return the model.
	 *
	 * @return the model.
	 */
	public ResultsModel getModel() {
		return model;
	}

	/**
	 * Call the model to do queryData queries.
	 *
	 * @param listRequest The list of data for the requests to do.
	 */
	public void doQueries(List<RequestData> listRequest) {
		model.doQueries(listRequest);
	}

	/**
	 * Clear the results (model and view).
	 */
	public void clearResults() {
		model.clearResults(false);
		view.removeAllComponents();
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.ssap.util.SsapModelListener#dataChanged(eu.omp.irap.ssap.util.SsapModelChangedEvent)
	 */
	@Override
	public void dataChanged(SsapModelChangedEvent event) {
		if (ResultsModel.CLEAR_RESULTS.equals(event.getSource())) {
			view.removeAllComponents();
		} else if (ResultsModel.NEW_RESULT.equals(event.getSource())) {
			Result result = (Result) event.getValue();
			view.addComponent(result);
		}
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.ssap.results.CopyServicesInformations#copyServicesInformations()
	 */
	@Override
	public void copyServicesInformations() {
		UtilFunction.copyToClipboard(model.getInformations());
	}

	@Override
	public void selectionChange(int number) {
		view.getOpenButton().setEnabled(number <= 1);
	}
}
