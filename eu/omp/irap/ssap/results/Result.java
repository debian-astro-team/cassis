/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.results;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import eu.omp.irap.ssap.service.SsaService;


/**
 * Represent a result of a queryData request.
 *
 * @author M. Boiziot
 */
public class Result  {

	private SsaService service;
	private boolean containsError;
	private boolean containsElements;
	private List<Field> fieldList;
	private List<ArrayList<String>> data;
	private int indiceUrl = -1;
	private boolean serviceKnowAllParam;
	private String response;


	/**
	 * Constructor.
	 *
	 * @param service The service on which the request is made.
	 * @param knowAllParam If the service know all the optionals parameters of the request.
	 * @param response The response of the service.
	 */
	public Result(SsaService service, boolean knowAllParam, String response) {
		this.service = service;
		this.serviceKnowAllParam = knowAllParam;
		this.fieldList = new ArrayList<>();
		this.data = new ArrayList<>();
		this.response = response;
		parse(response);
	}

	/**
	 * Parse the response.
	 *
	 * @param response The votable response.
	 */
	private void parse(String response) {
		if (response == null || response.isEmpty()) {
			containsError = true;
			return;
		}

		try {
			Document doc = createDocument(response);

			NodeList fields = doc.getElementsByTagName("FIELD");
			int columnCount = fields.getLength();

			Element element;
			for (int i = 0; i < columnCount; i++) {
				element = (Element) fields.item(i);
				fieldList.add(createFieldFromElement(element));
			}

			indiceUrl = Result.getIndiceUrl(fieldList);
			if (indiceUrl == -1) {
				containsError = true;
				return;
			}

			int nbElement;
			NodeList trs = null;
			try {
				trs = ((Element)doc.getElementsByTagName("TABLEDATA").item(0)).getElementsByTagName("TR");
				nbElement = trs.getLength();
				if (nbElement >= 1) {
					containsElements = true;
				}
			} catch (NullPointerException e) {
				nbElement = 0;
			}

			ArrayList<String> tr;
			for (int nb = 0; nb < nbElement; nb++) {
				Element elementt = (Element) trs.item(nb);
				NodeList tds  = elementt.getElementsByTagName("TD");
				tr = new ArrayList<>(columnCount);
				for (int num = 0; num < columnCount; num++) {
					tr.add(tds.item(num).getTextContent().trim());
				}
				data.add(tr);
			}
		} catch (Exception e) {
			containsError = true;
		}
	}

	/**
	 * Create a Document from a response (String).
	 *
	 * @param response The response as a String.
	 * @return The Document.
	 * @throws ParserConfigurationException In case of parser Exception
	 * @throws SAXException In case of SAX Exception.
	 * @throws IOException In case of IOException.
	 */
	private Document createDocument(String response) throws ParserConfigurationException,
			SAXException, IOException {
		Document doc;
		try (InputStream stream = new ByteArrayInputStream(response.getBytes());) {
			DocumentBuilderFactory docBuilderFact = DocumentBuilderFactory.newInstance();
			docBuilderFact.setFeature(
					"http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
			docBuilderFact.setFeature(
					"http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			DocumentBuilder docBuilder = docBuilderFact.newDocumentBuilder();

			doc = docBuilder.parse(stream);
			doc.getDocumentElement().normalize();
		}
		return doc;
	}

	/**
	 * Return the list of the fields from the parsed response.
	 *
	 * @return the list of fields.
	 */
	public List<Field> getFieldList() {
		return fieldList;
	}

	/**
	 * Return the data from the parsed response.
	 * Each top list represent a new element,
	 * each string of this list represent a parameter of this element.
	 *
	 * @return the data from the parsed response.
	 */
	public List<ArrayList<String>> getData() {
		return data;
	}

	/**
	 * Return if the response contains error.
	 *
	 * @return if the response contains error.
	 */
	public boolean containsError() {
		return containsError;
	}

	/**
	 * Return if the result contains elements.
	 *
	 * @return if the result contains elements.
	 */
	public boolean containsElements() {
		return containsElements;
	}

	/**
	 * Search and return the indice of the URL in a list of {@link Field}.
	 *
	 * @param listField The list of Field where to search.
	 * @return The indice of the URL or -1 if there is not URL field.
	 */
	private static int getIndiceUrl(List<Field> listField) {
		int nb = listField.size();
		int i = 0;
		Field currentField;
		while (i < nb) {
			currentField = listField.get(i);
			if ("ssa:access.reference".equalsIgnoreCase(currentField.getUtype())) {
				return i;
			}

			if ("DATA_LINK".equals(currentField.getUcd()) ||
					"meta.ref.url".equals(currentField.getUcd())) {
				return i;
			}
			i++;
		}
		return -1;

	}

	/**
	 * Create a {@link Field} from an element.
	 *
	 * @param element The element used to create the {@link Field}.
	 * @return The created {@link Field}.
	 */
	private static Field createFieldFromElement(Element element) {
		Field field = new Field();
		handleFieldName(element, field);
		handleDataType(element, field);
		handleUnit(element, field);
		handleUcd(element, field);
		handleUtype(element, field);
		handleDescription(element, field);
		return field;
	}

	/**
	 * Search for description value if present in the given element and set in the given field.
	 *
	 * @param element The element to get the information from.
	 * @param field The field to set the value if found.
	 */
	private static void handleDescription(Element element, Field field) {
		NodeList nl = element.getElementsByTagName("DESCRIPTION");
		if (nl.getLength() == 1) {
			field.setDescription(nl.item(0).getTextContent().trim());
		}
	}

	/**
	 * Search for utype value if present in the given element and set in the given field.
	 *
	 * @param element The element to get the information from.
	 * @param field The field to set the value if found.
	 */
	private static void handleUtype(Element element, Field field) {
		if (element.hasAttribute("utype")) {
			field.setUtype(element.getAttribute("utype"));
		} else if (element.hasAttribute("UTYPE")) {
			field.setUtype(element.getAttribute("UTYPE"));
		}
	}

	/**
	 * Search for UCD value if present in the given element and set in the given field.
	 *
	 * @param element The element to get the information from.
	 * @param field The field to set the value if found.
	 */
	private static void handleUcd(Element element, Field field) {
		if (element.hasAttribute("ucd")) {
			field.setUcd(element.getAttribute("ucd"));
		} else if (element.hasAttribute("UCD")) {
			field.setUcd(element.getAttribute("UCD"));
		}
	}

	/**
	 * Search for unit value if present in the given element and set in the given field.
	 *
	 * @param element The element to get the information from.
	 * @param field The field to set the value if found.
	 */
	private static void handleUnit(Element element, Field field) {
		if (element.hasAttribute("unit")) {
			field.setUnit(element.getAttribute("unit"));
		} else if (element.hasAttribute("UNIT")) {
			field.setUnit(element.getAttribute("UNIT"));
		}
	}

	/**
	 * Search for datatype value if present in the given element and set in the given field.
	 *
	 * @param element The element to get the information from.
	 * @param field The field to set the value if found.
	 */
	private static void handleDataType(Element element, Field field) {
		if (element.hasAttribute("datatype")) {
			field.setDatatype(element.getAttribute("datatype"));
		} else if (element.hasAttribute("DATATYPE")) {
			field.setDatatype(element.getAttribute("DATATYPE"));
		}
	}

	/**
	 * Search for name value if present in the given element and set in the given field.
	 *
	 * @param element The element to get the information from.
	 * @param field The field to set the value if found.
	 */
	private static void handleFieldName(Element element, Field field) {
		if (element.hasAttribute("name")) {
			field.setName(element.getAttribute("name"));
		} else if (element.hasAttribute("NAME")) {
			field.setName(element.getAttribute("NAME"));
		} else if (element.hasAttribute("id")) {
			field.setName(element.getAttribute("id"));
		} else if (element.hasAttribute("ID")) {
			field.setName(element.getAttribute("ID"));
		}
	}

	/**
	 * Return the service short name.
	 *
	 * @return the service short name.
	 */
	public String getServiceShortName() {
		return service.getShortName();
	}

	/**
	 * Return the URL from an element.
	 *
	 * @param numElement The number of the element on which we want the URL.
	 * @return The URL of the element.
	 */
	public String getUrl(int numElement) {
		return data.get(numElement).get(indiceUrl);
	}

	/**
	 * Return if the service know all the optionals parameters of the request.
	 *
	 * @return If the service know all the optionals parameters of the request.
	 */
	public boolean isServiceKnowAllParam() {
		return serviceKnowAllParam;
	}

	/**
	 * Return the {@link SsaService} on which the request is made.
	 *
	 * @return the service on which the request is made.
	 */
	public SsaService getService() {
		return service;
	}

	/**
	 * Return the response of the service (who should be a VOTable) as a String.
	 *
	 * @return the response of the service.
	 */
	public String getResponse() {
		return response;
	}
}
