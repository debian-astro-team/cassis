/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.results;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import eu.omp.irap.ssap.util.UtilFunction;
import eu.omp.irap.ssap.value.Constants;

/**
 * SwingWorker with a JDialog to download a list of file.
 *
 * @author M. Boiziot
 */
public class DownloadPane extends SwingWorker<Void, Void> {

	private File folder;
	private static final int BUFFER_SIZE = 4096;
	private List<String> urls;
	private JDialog dialog;
	private JProgressBar progressBar;
	private JButton cancelButton;
	private JTextArea messageArea;
	private JButton closeButton;


	/**
	 * Constructor.
	 *
	 * @param folder The folder where to save the downloaded files.
	 * @param urlFiles The list of URL of the files to download.
	 */
	public DownloadPane(File folder, List<String> urlFiles) {
		super();
		this.urls = urlFiles;
		this.folder = folder;
		if (!folder.exists()) {
			folder.mkdirs();
		}
		Container container = getDialog().getContentPane();
		container.add(getProgressBar(), BorderLayout.NORTH);

		JScrollPane jsp = new JScrollPane(getMessageArea(),
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		jsp.setPreferredSize(new Dimension(800, 300));
		container.add(jsp, BorderLayout.CENTER);
		container.add(getCancelButton(), BorderLayout.SOUTH);
		getDialog().pack();
	}

	/**
	 * Create if needed then return the {@link JDialog}.
	 *
	 * @return the dialog.
	 */
	private JDialog getDialog() {
		if (dialog == null) {
			dialog = new JDialog();
			dialog.setModal(false);
			dialog.setTitle("Download...");
			dialog.getContentPane().setLayout(new BorderLayout());
			dialog.setVisible(true);
			dialog.setLocationRelativeTo(null);
		}
		return dialog;
	}

	/**
	 * Create if needed then return the {@link JProgressBar}.
	 *
	 * @return the progress bar.
	 */
	private JProgressBar getProgressBar() {
		if (progressBar == null) {
			progressBar = new JProgressBar(0, urls.size());
			progressBar.setIndeterminate(false);
			progressBar.setStringPainted(true);
			progressBar.setPreferredSize(new Dimension(200, 24));
		}
		return progressBar;
	}

	/**
	 * Create if needed then return the message area ({@link JTextArea}).
	 *
	 * @return the message area.
	 */
	private JTextArea getMessageArea() {
		if (messageArea == null) {
			messageArea = new JTextArea() {
				private static final long serialVersionUID = 1L;

				@Override
				public void append(String str) {
					super.append(str + Constants.NEW_LINE);
					this.setCaretPosition(getDocument().getLength());
				}
			};
			messageArea.setEditable(false);
			messageArea.append("Downloading...");
		}
		return messageArea;
	}

	/**
	 * Create if needed then return the Cancel {@link JButton}.
	 *
	 * @return the Cancel button.
	 */
	private JButton getCancelButton() {
		if (cancelButton == null) {
			cancelButton = new JButton("Cancel");
			cancelButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					DownloadPane.this.cancel(true);
				}
			});
		}
		return cancelButton;
	}

	/**
	 * Create if needed then return the Close {@link JButton}.
	 *
	 * @return the Close button.
	 */
	private JButton getCloseButton() {
		if (closeButton == null) {
			closeButton = new JButton("Close");
			closeButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					getDialog().dispose();
				}
			});
		}
		return closeButton;
	}

	/* (non-Javadoc)
	 * @see javax.swing.SwingWorker#doInBackground()
	 */
	@Override
	protected Void doInBackground() throws Exception {
		for (int index = 0; index < urls.size(); index++) {
			if (isCancelled()) {
				break;
			}
			String stringUrl = urls.get(index);
			downloadFileAndUpdateGui(stringUrl, index);

		}
		return null;
	}

	/**
	 * Download the file with the given URL and update the GUI.
	 *
	 * @param stringUrl The URL as a String.
	 * @param index The index of the file downloaded.
	 */
	private void downloadFileAndUpdateGui(String stringUrl, int index) {
		boolean downloaded = false;
		HttpURLConnection connection = null;
		InputStream is = null;
		FileOutputStream fos = null;
		try {
			URL url = new URL(stringUrl);
			connection = (HttpURLConnection) url.openConnection();

			// Determine the where to save the file.
			String filePath = getFilePath(stringUrl, connection);

			is = connection.getInputStream();
			fos = new FileOutputStream(filePath);

			byte[] buffer = new byte[BUFFER_SIZE] ;
			int bytesRead = -1;
			while ((bytesRead = is.read(buffer)) != -1) {
				fos.write(buffer, 0, bytesRead);
			}
			downloaded = true;
		} catch (Exception e) {
			downloaded = false;
		} finally {
			closeStreams(connection, is, fos);

			String msg = createMessage(downloaded, stringUrl);
			updateGui(msg, index);
		}
	}

	/**
	 * Get a file path to use to download the file.
	 *
	 * @param stringUrl The URL as a String.
	 * @param connection The {@link HttpURLConnection} used to download the file.
	 * @return the file path.
	 */
	private String getFilePath(String stringUrl, HttpURLConnection connection) {
		String ext = UtilFunction.getFileExtension(stringUrl);
		String fileName = getFileName(stringUrl, connection, ext);
		return folder.getAbsolutePath() + File.separatorChar + fileName;
	}

	/**
	 * Close streams and connection.
	 *
	 * @param connection The {@link HttpURLConnection}.
	 * @param is The {@link InputStream}.
	 * @param fos The {@link FileOutputStream}.
	 */
	private void closeStreams(HttpURLConnection connection, InputStream is,
			FileOutputStream fos) {
		UtilFunction.disconnect(connection);
		UtilFunction.close(is);
		UtilFunction.close(fos);
	}

	/**
	 * Create a message (URL + state of the download).
	 *
	 * @param downloaded The state of the download.
	 * @param stringUrl The URL.
	 * @return The message.
	 */
	private String createMessage(boolean downloaded, String stringUrl) {
		String msg;
		if (downloaded) {
			msg = stringUrl + "... OK.";
		} else {
			msg = stringUrl + "... ERROR.";
		}
		return msg;
	}

	/**
	 * Update the GUI with state of the download.
	 *
	 * @param msg The message to add.
	 * @param fIndex The progress.
	 */
	private void updateGui(final String msg, final int fIndex) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				getMessageArea().append(msg);
				getProgressBar().setValue(fIndex + 1);
			}
		});
	}

	/**
	 * Search in HTTP header for a name to use for the file, if not present try with the URL.
	 *
	 * @param stringUrl The URL as a String.
	 * @param connection The {@link HttpURLConnection}.
	 * @param ext The extension of the file.
	 * @return The file name to use.
	 */
	private String getFileName(String stringUrl, HttpURLConnection connection,
			String ext) {
		String fileName;
		if (ext.isEmpty() || ext.startsWith("php") || ext.length() >= 8) {
			String header = connection.getHeaderField("Content-Disposition");
			if (header != null && header.indexOf('=') != -1) {
				fileName = header.split("=")[1].replaceAll("\"", "").replaceAll("[^a-zA-Z0-9.-]", "_");
			} else {
				fileName = new File(stringUrl).getName().replaceAll("[^a-zA-Z0-9.-]", "_");
			}
		} else {
			fileName = new File(stringUrl).getName();
		}
		return fileName;
	}

	/* (non-Javadoc)
	 * @see javax.swing.SwingWorker#done()
	 */
	@Override
	protected void done() {
		getDialog().getContentPane().remove(getCancelButton());
		getDialog().getContentPane().add(getCloseButton(), BorderLayout.SOUTH);
		if (isCancelled()) {
			getMessageArea().append("Download cancelled.");
		} else {
			getMessageArea().append("Done.");
		}
		getDialog().validate();
		getDialog().repaint();
	}
}
