/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.results;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import eu.omp.irap.ssap.request.RequestData;
import eu.omp.irap.ssap.request.RequestInterface;
import eu.omp.irap.ssap.request.RequestThread;
import eu.omp.irap.ssap.service.SsaService;
import eu.omp.irap.ssap.util.SsapListenerManager;
import eu.omp.irap.ssap.util.SsapModelChangedEvent;


/**
 * Model for the Results.
 *
 * @author M. Boiziot
 */
public class ResultsModel extends SsapListenerManager implements RequestInterface {

	public static final String DESELECT_SERVICE = "deselectService";
	public static final String END_QUERY = "endQuery";
	public static final String CLEAR_RESULTS = "clearResults";
	public static final String NEW_RESULT = "newResult";
	public static final String QUERY_STOPPED = "queryStopped";
	private static final int MAX_THREADS = 10;
	private ExecutorService threadPool;
	private List<Result> listResult;


	/**
	 * Constructor.
	 */
	public ResultsModel() {
		listResult = Collections.synchronizedList(new ArrayList<Result>());
	}

	/**
	 * Stop the running and planing queries and remove the current results.
	 *
	 * @param fireEventClearResults if we have to fire a CLEAR_RESULTS event.
	 */
	public synchronized void clearResults(boolean fireEventClearResults) {
		if (threadPool != null) {
			threadPool.shutdownNow();
			fireDataChanged(new SsapModelChangedEvent(ResultsModel.QUERY_STOPPED));
		}
		listResult.clear();

		if (fireEventClearResults) {
			fireDataChanged(new SsapModelChangedEvent(ResultsModel.CLEAR_RESULTS));
		}
	}

	/**
	 * Do the queryData queries.
	 *
	 * @param listRequest The list of data for the requests to do.
	 */
	public synchronized void doQueries(List<RequestData> listRequest) {
		clearResults(true);

		threadPool = Executors.newFixedThreadPool(ResultsModel.MAX_THREADS);
		for (RequestData request : listRequest) {
			threadPool.submit(new RequestThread(request.getService(),
					request.getUrl(), request.knowAllParam(), this));
		}
		threadPool.shutdown();

		new Thread(new Runnable() {
			@Override
			public void run() {
				while (!threadPool.isTerminated()) {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						Thread.currentThread().interrupt();
					}
				}
				List<String> listRes = new ArrayList<>();
				for (Result result : listResult) {
					if (result.containsElements()) {
						listRes.add(result.getServiceShortName());
					}
				}
				fireDataChanged(new SsapModelChangedEvent(ResultsModel.END_QUERY, listRes));
			}
		}).start();
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.ssap.request.RequestInterface#addNewResult(eu.omp.irap.ssap.service.SsaService, boolean, java.lang.String)
	 */
	@Override
	public void addNewResult(SsaService service, boolean knowAllParam, String resultString) {
		Result result = new Result(service, knowAllParam, resultString);
		if (!result.containsError() && result.containsElements()) {
			addResult(result);
			fireDataChanged(new SsapModelChangedEvent(ResultsModel.NEW_RESULT, result));
		}
	}

	/**
	 * Add a new {@link Result} to the list of {@link Result}
	 *
	 * @param result The {@link Result} to add.
	 */
	private synchronized void addResult(Result result) {
		listResult.add(result);
	}

	/**
	 * Remove the {@link Result} from the result list.
	 *
	 * @param result the {@link Result} to remove.
	 */
	public synchronized void removeResult(Result result) {
		listResult.remove(result);
	}

	/**
	 * Return a String with informations for all services related to a result.
	 *
	 * @return a String with informations for all services related to a result.
	 */
	public synchronized String getInformations() {
		StringBuilder sb = new StringBuilder();
		for (Result result : listResult) {
			sb.append(result.getService().getInformations(false));
		}
		return sb.toString();
	}

	/**
	 * Return the list of Result.
	 *
	 * @return the list of Result.
	 */
	public synchronized List<Result> getResults() {
		return listResult;
	}
}
