/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.results;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;


/**
 * View for displaying a {@link Result}.
 *
 * @author M. Boiziot
 */
public class ResultView extends JPanel {

	private static final long serialVersionUID = -4454211565968927061L;
	private ResultTable table;
	private Result result;
	private ResultSelectionChange selectionInterface;


	/**
	 * Constructor.
	 *
	 * @param result The result to display.
	 * @param selectionInterface The interface to notify of a selection change.
	 */
	public ResultView(Result result, ResultSelectionChange selectionInterface) {
		super(new GridLayout(1,0));
		this.result = result;
		this.selectionInterface = selectionInterface;

		JScrollPane scrollPane = new JScrollPane(getResultTable(),
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setPreferredSize(new Dimension(600, 400));
		add(scrollPane);
	}

	/**
	 * Create if needed then return the {@link ResultTable}.
	 *
	 * @return the result table.
	 */
	public ResultTable getResultTable() {
		if (table == null) {
			table = new ResultTable(new ResultTableWrapper(result), selectionInterface);
		}
		return table;
	}

	/**
	 * Return if the result know all the optionals parameters of the query.
	 *
	 * @return if the result know all the optionals parameters of the query.
	 */
	public boolean isServiceKnowAllParam() {
		return result.isServiceKnowAllParam();
	}

	/**
	 * Return the VOTable as a String.
	 *
	 * @return the VOTable as a String.
	 */
	public String getVoTable() {
		return result.getResponse();
	}
}
