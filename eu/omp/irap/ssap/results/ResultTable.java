/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.results;

import java.awt.Component;
import java.awt.Point;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;

/**
 * JTable for {@link Result}.
 *
 * @author M. Boiziot
 */
public class ResultTable extends JTable {

	private static final long serialVersionUID = 2946976999846070368L;
	private ResultTableWrapper rtw;
	private ResultSelectionChange selectionInterface;
	private ListSelectionListener selectionListener;
	private FocusAdapter focusAdapter;


	/**
	 * Constructor.
	 *
	 * @param resultTableWrapper The {@link ResultTableWrapper} used as model.
	 * @param selectionInterface The interface to notify of a selection change.
	 */
	public ResultTable(ResultTableWrapper resultTableWrapper, ResultSelectionChange selectionInterface) {
		super(resultTableWrapper);
		this.selectionInterface = selectionInterface;
		this.rtw = resultTableWrapper;
		getTableHeader().setReorderingAllowed(false);
		setShowGrid(true);
		setFillsViewportHeight(true);
		setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		updateColumnsSize();
		addListeners();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see javax.swing.JTable#createDefaultTableHeader()
	 */
	@Override
	protected JTableHeader createDefaultTableHeader() {
		return new JTableHeader(columnModel) {
			private static final long serialVersionUID = -8010864518102935038L;

			@Override
			public String getToolTipText(MouseEvent e) {
				Point p = e.getPoint();
				int index = columnModel.getColumnIndexAtX(p.x);
				int realIndex = columnModel.getColumn(index).getModelIndex();
				return rtw.getHeaderToolTip(realIndex);
			}
		};
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see javax.swing.JTable#getScrollableTracksViewportWidth()
	 */
	@Override
	public boolean getScrollableTracksViewportWidth() {
		return getPreferredSize().width < getParent().getWidth();
	}

	/**
	 * Update the columns size.
	 */
	private void updateColumnsSize() {
		int width;
		Component comp;
		for (int column = 0; column < getColumnCount(); column++) {
			comp = getTableHeader().getDefaultRenderer().getTableCellRendererComponent(this,
					getColumnModel().getColumn(column).getHeaderValue(), false, false, 0, 0);
			width = comp.getPreferredSize().width;
			for (int row = 0; row < getRowCount(); row++) {
				TableCellRenderer renderer = getCellRenderer(row, column);
				comp = prepareRenderer(renderer, row, column);
				width = Math.max(comp.getPreferredSize().width, width);
			}
			// Add 12 for margin.
			width += 12;
			getColumnModel().getColumn(column).setPreferredWidth(width);
		}
	}

	/**
	 * Return the list of the URL for selected elements in the table.
	 *
	 * @return the list of the URL for selected elements in the table.
	 */
	public List<String> getSelectedUrls() {
		int[] selectedRows = getSelectedRows();
		List<String> list = new ArrayList<>(selectedRows.length);

		for (int row : selectedRows) {
			list.add(rtw.getUrl(row));
		}
		return list;
	}

	/**
	 * Return the list of the URL for all elements in the table.
	 *
	 * @return the list of the URL for all elements in the table.
	 */
	public List<String> getAllUrls() {
		List<String> list = new ArrayList<>(getRowCount());
		for (int i = 0; i < getRowCount(); i++) {
			list.add(rtw.getUrl(i));
		}
		return list;
	}

	/**
	 * Add listeners to the {@link ResultTable}.
	 */
	private void addListeners() {
		selectionListener = new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent lse) {
				if (lse.getSource() == getSelectionModel()) {
					selectionInterface.selectionChange(getSelectedRowCount());
				}
			}
		};
		focusAdapter = new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				selectionInterface.selectionChange(getSelectedRowCount());
			}
		};

		this.getSelectionModel().addListSelectionListener(selectionListener);
		this.addFocusListener(focusAdapter);
	}

	/**
	 * Remove the listeners added by addListeners();
	 */
	public void removeListeners() {
		this.getSelectionModel().removeListSelectionListener(selectionListener);
		this.removeFocusListener(focusAdapter);
		this.selectionInterface = null;
	}

	/**
	 * Return the list of selected rows index (starting at 0, not 1 as in GUI).
	 *
	 * @return the list of selected rows index.
	 */
	public List<Integer> getSelectedIndex() {
		int[] selectedRowsArray = getSelectedRows();
		List<Integer> selectedRowsList = new ArrayList<>(selectedRowsArray.length);
		for (int rowIndex : selectedRowsArray) {
			selectedRowsList.add(rowIndex);
		}
		return selectedRowsList;
	}

	/**
	 * Return the list all index in the table starting at 0 not at 1 as in GUI.
	 *
	 * @return the list all index in the table
	 */
	public List<Integer> getAllIndex() {
		List<Integer> rowsList = new ArrayList<>(getRowCount());
		for (int i = 0; i < getRowCount(); i++) {
			rowsList.add(i);
		}
		return rowsList;
	}
}
