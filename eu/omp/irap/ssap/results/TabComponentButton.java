/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.results;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;

import eu.omp.irap.ssap.service.SsaService;
import eu.omp.irap.ssap.util.SsapModelChangedEvent;
import eu.omp.irap.ssap.util.UtilFunction;


/**
 * Class to display a {@link JLabel} and a {@link JButton} in a tab for a {@link JTabbedPane}.
 *
 * @author M. Boiziot
 */
public class TabComponentButton extends JPanel {

	private static final long serialVersionUID = 612304100704633111L;
	private static Icon closeIconGray;
	private static Icon closeIconRed;
	private JButton deleteButton;
	private JLabel label;
	private Result result;
	private JTabbedPane tabbedPane;
	private CopyServicesInformations copyInterface;


	/**
	 * Constructor.
	 *
	 * @param result The result associated with this tab.
	 * @param tabbedPane The {@link JTabbedPane} on which the tab is.
	 * @param copyInterface The {@link CopyServicesInformations} interface.
	 */
	public TabComponentButton(Result result, JTabbedPane tabbedPane, CopyServicesInformations copyInterface) {
		super(new BorderLayout());
		this.result = result;
		this.tabbedPane = tabbedPane;
		this.copyInterface = copyInterface;
		setOpaque(false);
		label = new JLabel(result.getServiceShortName());
		label.setOpaque(false);
		label.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
		setToolTipText(result.getService().getInformations(true));
		add(label, BorderLayout.WEST);
		add(getDeleteButton(), BorderLayout.EAST);

		addMouseListener();
		setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));
	}

	/**
	 * Create if needed then return the Delete {@link JButton}.
	 *
	 * @return the delete {@link JButton}.
	 */
	public JButton getDeleteButton() {
		if (deleteButton == null) {
			deleteButton = new JButton(getCloseIconGray());
			deleteButton.setToolTipText("Close Tab.");
			deleteButton.setSize(2 * 10, 15);
			deleteButton.setPreferredSize(new Dimension(2 * 10, 15));
			deleteButton.setMargin(new Insets(2, 2, 2, 2));
			deleteButton.setContentAreaFilled(false);
			deleteButton.setFocusable(false);
			deleteButton.setBorderPainted(false);
			deleteButton.setRolloverIcon(TabComponentButton.getCloseIconRed());
			deleteButton.setPressedIcon(TabComponentButton.getCloseIconRed());
		}
		return deleteButton;
	}

	/**
	 * Remove the {@link ActionListener} on the delete {@link JButton}.
	 */
	public void removeDeleteButtonListeners() {
		ActionListener[] als = deleteButton.getActionListeners();
		for (ActionListener al : als) {
			deleteButton.removeActionListener(al);
		}
	}

	/**
	 * Change the {@link Color} of the label.
	 *
	 * @param color The {@link Color} to set.
	 */
	public void setColor(Color color) {
		label.setForeground(color);
		label.repaint();
	}

	/**
	 * Create if needed then return the gray close {@link Icon}.
	 *
	 * @return The gray close {@link Icon}
	 */
	private static Icon getCloseIconGray() {
		if (closeIconGray == null) {
			closeIconGray = new ImageIcon(TabComponentButton.class.getResource("/icons/close_gray.gif"));
		}
		return closeIconGray;
	}

	/**
	 * Create if needed then return the red close {@link Icon}.
	 *
	 * @return The red close {@link Icon}
	 */
	private static Icon getCloseIconRed() {
		if (closeIconRed == null) {
			closeIconRed = new ImageIcon(TabComponentButton.class.getResource("/icons/close_red.gif"));
		}
		return closeIconRed;
	}

	/**
	 * Add a {@link MouseListener}.
	 */
	private void addMouseListener() {
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent me) {
				if (SwingUtilities.isRightMouseButton(me)) {
					displayPopupMenu(me);
				} else {
					result.getService().fireDataChanged(new SsapModelChangedEvent(SsaService.GO_TO, result.getService()));
					for (int i = 0; i < tabbedPane.getTabCount(); i++) {
						if (tabbedPane.getTabComponentAt(i) == TabComponentButton.this) {
							tabbedPane.setSelectedIndex(i);
							return;
						}
					}
				}
			}
		});
	}

	/**
	 * Display a popup menu.
	 *
	 * @param me The MouseEvent from the click.
	 */
	private void displayPopupMenu(MouseEvent me) {
		JPopupMenu popupMenu = new JPopupMenu();

		JMenuItem copyMenuItem = new JMenuItem("Copy informations from this service");
		copyMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				result.getService().fireDataChanged(new SsapModelChangedEvent(SsaService.COPY, result.getService()));
			}
		});

		JMenuItem copySelectedMenuItem = new JMenuItem("Copy informations from services with results");
		copySelectedMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				copyInterface.copyServicesInformations();
			}
		});

		JMenuItem goToMenuItem = new JMenuItem("Place the scroll bar to this service");
		goToMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				result.getService().fireDataChanged(new SsapModelChangedEvent(SsaService.GO_TO, result.getService()));
			}
		});

		JMenuItem copyResponseMenuItem = new JMenuItem("Copy the response of this service");
		copyResponseMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				UtilFunction.copyToClipboard(result.getResponse());
			}
		});

		popupMenu.add(copyMenuItem);
		popupMenu.add(copySelectedMenuItem);
		popupMenu.add(goToMenuItem);
		popupMenu.add(copyResponseMenuItem);
		popupMenu.show(me.getComponent(), me.getX(), me.getY());
	}
}
