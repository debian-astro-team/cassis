/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.ssaparameters;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import eu.omp.irap.ssap.util.SsapListenerManager;
import eu.omp.irap.ssap.util.SsapModelChangedEvent;


/**
 * Simple class for defining a parameter.
 *
 * @author M. Boiziot
 */
public class Parameter extends SsapListenerManager implements Comparable<Parameter> {

	public static final String VALUE_UPDATE = "currentValueUpdate";
	public static final String NEED_REFRESH = "needRefresh";
	private static final Logger LOGGER = Logger.getLogger("Parameter");
	protected final String name;
	private final String description;
	protected List<String> values;
	private String currentValue;
	private String hint;


	/**
	 * Constructor.
	 *
	 * @param name Name of the parameter.
	 * @param description Description of the parameter.
	 * @param valuesProv List of values eventually provided by the service.
	 * @param defaultValue The default value.
	 */
	public Parameter(String name, String description, List<String> valuesProv, String defaultValue) {
		this(name, description, valuesProv, defaultValue, null);
	}

	/**
	 * Constructor.
	 *
	 * @param name Name of the parameter.
	 * @param description Description of the parameter.
	 * @param valuesProv List of values eventually provided by the service.
	 * @param defaultValue The default value.
	 * @param hint An optional hint to display.
	 */
	public Parameter(String name, String description, List<String> valuesProv, String defaultValue, String hint) {
		this.name = name;
		this.description = description;
		this.values = valuesProv == null ? new ArrayList<String>(0) : new ArrayList<String>(valuesProv);
		this.currentValue = defaultValue;
		this.hint = hint;
	}

	/**
	 * Return the name of the parameter.
	 *
	 * @return the name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Change the current value for the parameter.
	 *
	 * @param value The new value.
	 */
	public void setCurrentValue(String value) {
		this.currentValue = value;
		fireDataChanged(new SsapModelChangedEvent(Parameter.VALUE_UPDATE, value, name));
		fireDataChanged(new SsapModelChangedEvent(Parameter.NEED_REFRESH));
	}

	/**
	 * Return the description of the parameter.
	 *
	 * @return the description of the parameter.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Create a string with the formated request for this parameter.
	 *
	 * @param encoded Encoding the URL. True to encode it, false to do not.
	 * @return The formated request.
	 */
	public String getFormatedRequest(boolean encoded) {
		String request;
		if (encoded) {
			try {
				request = URLEncoder.encode(name, "UTF-8") + "=";
				if (currentValue != null) {
					request += URLEncoder.encode(currentValue, "UTF-8");
				}
			} catch (UnsupportedEncodingException e) {
				LOGGER.log(Level.WARNING,
						"UnsupportedEncodingException... do not encode the parameter "
				+ this.name + " for the URL.");
				request = getFormatedRequest(false);
			}
		} else {
			request = name + "=";
			if (currentValue != null) {
				request += currentValue;
			}
		}
		return request;
	}

	/**
	 * Return the provided values.
	 *
	 * @return The values.
	 */
	public List<String> getProvidedValues() {
		return values;
	}

	/**
	 * Return if the parameter is considered.
	 *
	 * @return if the parameter is considered.
	 */
	public boolean isConsidered() {
		return !(currentValue == null || currentValue.isEmpty() || "none".equals(currentValue));
	}

	/**
	 * Return the current value of the parameter.
	 *
	 * @return the current value of the parameter.
	 */
	public String getCurrentValue() {
		return currentValue;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Parameter o) {
		return name.toLowerCase(Locale.US).compareTo(o.getName().toLowerCase(Locale.US));
	}

	/**
	 * Return the hint of the parameter.
	 *
	 * @return the hint of the parameter (this can be null).
	 */
	public String getHint() {
		return hint;
	}
}
