/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.ssaparameters;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * View for the globals parameters.
 *
 * @author M. Boiziot
 */
public class GlobalsParametersView extends JPanel {

	private static final long serialVersionUID = -3141702025835627895L;
	private GlobalsParametersControl control;
	private JTextField objectTextField;
	private JButton resolveButton;


	/**
	 * Constructor.
	 *
	 * @param theControl The controller.
	 */
	public GlobalsParametersView(GlobalsParametersControl theControl) {
		this.control = theControl;
		this.setBorder(BorderFactory.createTitledBorder("Global Parameters"));
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.setPreferredSize(new Dimension(340, 213));

		add(createObjectPanel());
		add(new PosParameterView(control.getModel().getPosParameter()));
		add(new SizeParameterView(control.getModel().getSizeParameter()));
		add(new ParameterView(control.getModel().getBandParameter()));
		add(new ParameterView(control.getModel().getTimeParameter()));
		add(new ParameterView(control.getModel().getFormatParameter()));
	}

	/**
	 * Create the object panel.
	 *
	 * @return the object panel.
	 */
	private JComponent createObjectPanel() {
		JPanel panel = new JPanel();
		FlowLayout fl = new FlowLayout(FlowLayout.CENTER, 5, 5);
		panel.setLayout(fl);

		JLabel label = new JLabel("Object name:");
		Dimension size = new Dimension(100, 24);
		label.setSize(size);
		label.setPreferredSize(size);
		panel.add(label);
		panel.add(getObjectTextField());
		panel.add(getResolveButton());

		size = new Dimension(310, 24);
		panel.setSize(size);
		panel.setPreferredSize(size);

		return panel;
	}

	/**
	 * Create if necessary and return the object field.
	 *
	 * @return the object field.
	 */
	public JTextField getObjectTextField() {
		if (objectTextField == null) {
			objectTextField = new JTextField();
			objectTextField.setPreferredSize(new Dimension(104, 20));
			objectTextField.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					String objectName = getObjectTextField().getText();
					if (objectName == null || objectName.trim().length() == 0) {
						return;
					}
					control.doResolveAction(objectName);
				}
			});
		}
		return objectTextField;
	}

	/**
	 * Create if necessary and return the "Resolve" button.
	 *
	 * @return the resolve button.
	 */
	public JButton getResolveButton() {
		if (resolveButton == null) {
			resolveButton = new JButton("Resolve");
			resolveButton.setPreferredSize(new Dimension(90, 24));
			resolveButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					String objectName = getObjectTextField().getText();
					if (objectName == null || objectName.trim().length() == 0) {
						return;
					}
					control.doResolveAction(objectName);
				}
			});
		}
		return resolveButton;
	}

	/**
	 * Display an error.
	 *
	 * @param title Title of the error.
	 * @param msg Message of the error.
	 */
	public void displayError(String title, String msg) {
		JOptionPane.showMessageDialog(this, msg, title, JOptionPane.ERROR_MESSAGE);
	}
}
