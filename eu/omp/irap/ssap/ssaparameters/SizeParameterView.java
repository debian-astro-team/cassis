/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.ssaparameters;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import eu.omp.irap.ssap.util.Size;
import eu.omp.irap.ssap.util.SsapModelChangedEvent;
import eu.omp.irap.ssap.util.SsapModelListener;

/**
 * View for {@link SizeParameter}.
 *
 * @author M. Boiziot
 */
public class SizeParameterView  extends JPanel implements SsapModelListener {

	private static final long serialVersionUID = -6469374368863929405L;
	private JTextField sizeTextField;
	private JComboBox<SizeUnit> sizeUnitCombobox;
	private SizeParameter parameter;


	/**
	 * Constructor.
	 *
	 * @param parameter The {@link SizeParameter}.
	 */
	public SizeParameterView(SizeParameter parameter) {
		super();
		FlowLayout fl = new FlowLayout(FlowLayout.CENTER, 5, 5);
		setLayout(fl);
		this.parameter = parameter;
		parameter.addModelListener(this);

		JLabel label = new JLabel("SEARCH RADIUS:");
		label.setToolTipText(parameter.getDescription());
		label.setSize(new Dimension(120, 20));
		label.setPreferredSize(new Dimension(120, 20));
		add(label);
		add(getSizeTextField());
		add(getSizeUnitComboBox());

		Dimension size = new Dimension(310, 24);
		this.setSize(size);
		this.setPreferredSize(size);
	}

	/**
	 * Create if needed then return the size {@link JTextField}.
	 *
	 * @return the size text field.
	 */
	public JTextField getSizeTextField() {
		if (sizeTextField == null) {
			sizeTextField = new JTextField();
			sizeTextField.setSize(90, 20);
			sizeTextField.setPreferredSize(new Dimension(90, 20));
			sizeTextField.setText(parameter.getCurrentValue());
			sizeTextField.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					valueChanged();
				}
			});
			sizeTextField.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					valueChanged();
				}
			});
		}
		return sizeTextField;
	}

	/**
	 * Create if needed then return the size unit {@link JComboBox}.
	 *
	 * @return the size unit combo box.
	 */
	public JComboBox<SizeUnit> getSizeUnitComboBox() {
		if (sizeUnitCombobox == null) {
			sizeUnitCombobox = new JComboBox<>(SizeUnit.values());
			sizeUnitCombobox.setSize(86, 20);
			sizeUnitCombobox.setPreferredSize(new Dimension(86, 20));
			sizeUnitCombobox.setSelectedItem(parameter.getUnit());
			sizeUnitCombobox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					SizeUnit unit = (SizeUnit) sizeUnitCombobox.getSelectedItem();
					parameter.setUnit(unit);
				}
			});
		}
		return sizeUnitCombobox;
	}

	/**
	 * Handle a change of the value in the size {@link JTextField}.
	 */
	private void valueChanged() {
		String value = getSizeTextField().getText();
		if (value.isEmpty() || Size.isSizeValid(value)) {
			getSizeTextField().setBackground(Color.WHITE);
			if (!value.equals(parameter.getCurrentValue())) {
				parameter.setCurrentValue(value);
			}
		} else {
			getSizeTextField().setBackground(Color.PINK);
		}
	}

	/**
	 * Handle a change in the model.
	 *
	 * @param event The event.
	 * @see eu.omp.irap.ssap.util.SsapModelListener#dataChanged(eu.omp.irap.ssap.util.SsapModelChangedEvent)
	 */
	@Override
	public void dataChanged(SsapModelChangedEvent event) {
		if (SizeParameter.VALUE_UPDATE.equals(event.getSource())) {
			getSizeTextField().setText(parameter.getCurrentValue());
			valueChanged();
		} else if (SizeParameter.UNIT_VALUE_CHANGED.equals(event.getSource())) {
			getSizeUnitComboBox().setSelectedItem(parameter.getUnit());
		}
	}
}
