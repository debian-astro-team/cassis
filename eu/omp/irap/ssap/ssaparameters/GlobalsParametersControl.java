/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.ssaparameters;

import javax.swing.SwingUtilities;

import eu.omp.irap.ssap.nameresolving.NameResolver;
import eu.omp.irap.ssap.util.OperationException;
import eu.omp.irap.ssap.value.Constants;

/**
 * Controller for the globals parameters.
 *
 * @author M. Boiziot
 */
public class GlobalsParametersControl {

	private GlobalsParametersModel model;
	private GlobalsParametersView view;


	/**
	 * Constructor.
	 *
	 * @param model The used model.
	 */
	public GlobalsParametersControl(GlobalsParametersModel model) {
		this.model = model;
		view = new GlobalsParametersView(this);
	}

	/**
	 * Return the model.
	 *
	 * @return the model.
	 */
	public GlobalsParametersModel getModel() {
		return model;
	}

	/**
	 * Return the view.
	 *
	 * @return the view.
	 */
	public GlobalsParametersView getView() {
		return view;
	}

	/**
	 * Try to resolve the position from a given object name
	 * then add on the POS field.
	 *
	 * @param objectName The name of the object.
	 */
	public void doResolveAction(final String objectName) {
		view.getResolveButton().setEnabled(false);
		new Thread() {
			@Override
			public void run() {
				NameResolver nr = null;
				try {
					nr = NameResolver.resolve(objectName);
				} catch (OperationException e) {
					// Error handled latter as nr remain null.
				}
				updateParameters(nr);
			}
		}.start();
	}

	/**
	 * Update position parameter.
	 *
	 * @param nr The NameResolver object.
	 */
	private void updateParameters(final NameResolver nr) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				PosParameter posParameter = model.getPosParameter();
				if (nr == null) {
					view.displayError("Resolving error",
							"An error occured while trying to resolve the provided object."
							+ Constants.NEW_LINE
							+ "The name was not resolved.");
					posParameter.resetData();
				} else {
					posParameter.setRa(nr.getRa());
					posParameter.setDec(nr.getDec());
				}
				view.getResolveButton().setEnabled(true);
			}
		});
	}
}
