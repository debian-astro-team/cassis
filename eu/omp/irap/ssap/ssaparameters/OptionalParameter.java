/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.ssaparameters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import eu.omp.irap.ssap.service.ServiceParameter;
import eu.omp.irap.ssap.service.SsaService;
import eu.omp.irap.ssap.util.SsapModelChangedEvent;
import eu.omp.irap.ssap.util.StringNumComparator;
import eu.omp.irap.ssap.util.UtilFunction;

/**
 * Simple class for defining a parameter who is optional.
 *
 * @author M. Boiziot
 */
public class OptionalParameter extends Parameter {

	public static final String CONSIDERED_UPDATE = "consideredUpdate";
	private List<ServiceParameter> servicesParameters;
	private boolean considered;


	/**
	 * Constructor.
	 *
	 * @param serviceParameter The service parameter.
	 */
	public OptionalParameter(ServiceParameter serviceParameter) {
		super(serviceParameter.getName(), serviceParameter.getDescription(), null, null);
		servicesParameters = new ArrayList<>();
		this.considered = false;
		addService(serviceParameter);
	}

	/**
	 * Add an {@link ServiceParameter}.
	 *
	 * @param serviceParameter The service to add.
	 */
	public void addService(ServiceParameter serviceParameter) {
		if (!servicesParameters.contains(serviceParameter)) {
			servicesParameters.add(serviceParameter);
		}
	}

	/**
	 * Return the list of {@link SsaService} who know this parameter.
	 *
	 * @return the list of service who know this parameter.
	 */
	public List<SsaService> getServices() {
		List<SsaService> services = new ArrayList<>(servicesParameters.size());
		for (ServiceParameter sp : servicesParameters) {
			services.add(sp.getService());
		}
		return services;
	}

	/**
	 * Remove an {@link ServiceParameter} who know this parameter.
	 *
	 * @param serviceParameter The service parameter to remove.
	 */
	public void removeService(ServiceParameter serviceParameter) {
		servicesParameters.remove(serviceParameter);
	}

	/**
	 * Change the considered state (used on the final request or not)
	 *
	 * @param newConsideredValue The new considered value.
	 */
	public void setConsidered(boolean newConsideredValue) {
		this.considered = newConsideredValue;
		fireDataChanged(new SsapModelChangedEvent(OptionalParameter.CONSIDERED_UPDATE, considered, getName()));
		fireDataChanged(new SsapModelChangedEvent(Parameter.NEED_REFRESH));
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.ssap.ssaparameters.Parameter#isConsidered()
	 */
	@Override
	public boolean isConsidered() {
		return considered;
	}

	/**
	 * Compute then return the provided values.
	 *
	 * @return the provided values.
	 * @see eu.omp.irap.ssap.ssaparameters.Parameter#getProvidedValues()
	 */
	@Override
	public List<String> getProvidedValues() {
		List<String> values = new ArrayList<>();
		Iterator<ServiceParameter> it = servicesParameters.iterator();
		while (it.hasNext()) {
			ServiceParameter sp = it.next();
			if (sp.getProvidedValues() == null || sp.getProvidedValues().isEmpty()) {
				continue;
			}
			for (String aValue : sp.getProvidedValues()) {
				if (!values.contains(aValue)) {
					values.add(aValue);
				}
			}
		}
		Collections.sort(values, new StringNumComparator(UtilFunction.isAllNumber(values)));
		return values;
	}

	/**
	 * Return a copy of the service parameter list.
	 *
	 * @return a copy of the service parameter list.
	 */
	public List<ServiceParameter> getServiceParameterList() {
		return new ArrayList<>(servicesParameters);
	}
}
