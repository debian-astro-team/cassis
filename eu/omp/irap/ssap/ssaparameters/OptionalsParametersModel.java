/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.ssaparameters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

import eu.omp.irap.ssap.service.ServiceParameter;
import eu.omp.irap.ssap.util.SsapListenerManager;
import eu.omp.irap.ssap.util.SsapModelChangedEvent;
import eu.omp.irap.ssap.util.SsapModelListener;

/**
 * Model for the optionals parameters.
 *
 * @author M. Boiziot
 */
public class OptionalsParametersModel extends SsapListenerManager implements SsapModelListener {

	public static final String NEW_PARAMETER = "newParameter";
	public static final String REMOVE_PARAMETER = "removeParameter";
	public static final String REMOVE_ALL_PARAMETER = "removeAllParameter";
	private Vector<OptionalParameter> parameters;


	/**
	 * Constructor.
	 */
	public OptionalsParametersModel() {
		parameters = new Vector<>();
	}

	/**
	 * Add a parameter to the list of known parameters.
	 *
	 * @param serviceParameter The service parameter to add.
	 */
	public synchronized void addParameters(ServiceParameter serviceParameter) {
		OptionalParameter tmpParam = getParameter(serviceParameter.getName());
		if (tmpParam == null) {
			OptionalParameter newOptionalParameter = new OptionalParameter(serviceParameter);
			newOptionalParameter.addModelListener(this);
			parameters.add(newOptionalParameter);
			Collections.sort(parameters);
			fireDataChanged(new SsapModelChangedEvent(
					OptionalsParametersModel.NEW_PARAMETER, serviceParameter));
		} else {
			tmpParam.addService(serviceParameter);
		}
	}

	/**
	 * Remove an {@link ServiceParameter} from an {@link OptionalParameter}.
	 *
	 * @param serviceParameter The {@link ServiceParameter} to remove.
	 * @param parameterName The name of the Parameter where we want to remove the service.
	 */
	public synchronized void removeServiceParameters(ServiceParameter serviceParameter,
			String parameterName) {
		OptionalParameter parameter = getParameter(parameterName);
		if (parameter != null) {
			parameter.removeService(serviceParameter);
			if (parameter.getServices().isEmpty()) {
				removeParameter(parameter);
			}
		}
	}

	/**
	 * Remove an {@link OptionalParameter} from the list of known parameters.
	 *
	 * @param parameter The {@link OptionalParameter} to remove.
	 */
	private void removeParameter(OptionalParameter parameter) {
		parameters.remove(parameter);
		parameter.removeModelListener(this);
		fireDataChanged(new SsapModelChangedEvent(OptionalsParametersModel.REMOVE_PARAMETER, parameter));
	}

	/**
	 * Remove all parameters.
	 */
	public synchronized void removeAllParameters() {
		for (OptionalParameter param : parameters) {
			param.removeAllListener();
		}
		parameters.clear();
		fireDataChanged(new SsapModelChangedEvent(OptionalsParametersModel.REMOVE_ALL_PARAMETER));
	}

	/**
	 * Search a parameter with a given name in the list of known parameters then return it.
	 *
	 * @param parameterName The name of the parameter to search.
	 * @return The parameter or null if the parameter is unknown.
	 */
	private synchronized OptionalParameter getParameter(String parameterName) {
		for (OptionalParameter parameter : parameters) {
			if (parameter.getName().equalsIgnoreCase(parameterName)) {
				return parameter;
			}
		}
		return null;
	}

	/**
	 * Return the list of known {@link OptionalParameter}.
	 *
	 * @return the list of known parameters.
	 */
	public synchronized Vector<OptionalParameter> getParameters() {
		return parameters;
	}

	/**
	 * Return the String request for the query URL.
	 *
	 * @param encoded if we want to encode the URL.
	 * @return the request for the query URL.
	 */
	public String getRequest(boolean encoded) {
		StringBuilder sb = new StringBuilder();
		for (Parameter parameter : getListConsideredParameter()) {
			sb.append(parameter.getFormatedRequest(encoded)).append('&');
		}
		if (sb.length() >= 1) {
			sb.deleteCharAt(sb.length()-1);
		}
		return sb.toString();
	}

	/**
	 * Return the list of considered parameters.
	 *
	 * @return the list of considered parameters.
	 */
	public List<Parameter> getListConsideredParameter() {
		List<Parameter> list = new ArrayList<>();
		for (Parameter param : parameters) {
			if (param.isConsidered()) {
				list.add(param);
			}
		}
		return list;
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.ssap.util.SsapModelListener#dataChanged(eu.omp.irap.ssap.util.SsapModelChangedEvent)
	 */
	@Override
	public void dataChanged(SsapModelChangedEvent event) {
		if (Parameter.NEED_REFRESH.equals(event.getSource())) {
			fireDataChanged(event);
		}
	}
}
