/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.ssaparameters;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

import eu.omp.irap.ssap.service.ServiceParameter;

/**
 * JFrame to display information for a parameter.
 *
 * @author M. Boiziot
 */
public class ParameterInformationFrame extends JFrame {

	private static final long serialVersionUID = -7877479600048814703L;
	private List<ServiceParameter> serviceParameterList;


	/**
	 * Constructor.
	 *
	 * @param parameterName The name of the parameter.
	 * @param serviceParameterList The service parameter list.
	 */
	public ParameterInformationFrame(String parameterName,
			List<ServiceParameter> serviceParameterList) {
		super("Details for \"" + parameterName + "\" parameter");
		this.serviceParameterList = serviceParameterList;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setContentPane(createContent());
		int height = serviceParameterList.size() == 1 ? 200 : 400;
		setSize(660, height);
	}

	/**
	 * Create then return the content of the frame.
	 *
	 * @return the content of the frame.
	 */
	private JPanel createContent() {
		JPanel mainPanel = new JPanel(new BorderLayout());

		JPanel centerPanel = new JPanel();
		BoxLayout boxLayout = new BoxLayout(centerPanel, BoxLayout.Y_AXIS);
		centerPanel.setLayout(boxLayout);

		for (ServiceParameter serviceParameter : serviceParameterList) {
			centerPanel.add(createPanelService(serviceParameter));
		}

		JPanel buttonPanel = new JPanel();
		JButton closeButton = new JButton("Close");
		closeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ParameterInformationFrame.this.dispose();
			}
		});
		buttonPanel.add(closeButton);

		mainPanel.add(createScrollPane(centerPanel), BorderLayout.CENTER);
		mainPanel.add(buttonPanel, BorderLayout.SOUTH);

		return mainPanel;
	}

	/**
	 * Create then return a JScrollPane for a given panel.
	 *
	 * @param panel The panel
	 * @return the created JScrollPane.
	 */
	private JScrollPane createScrollPane(JPanel panel) {
		JScrollPane jsp = new JScrollPane();
		jsp.getVerticalScrollBar().setUnitIncrement(10);
		jsp.getViewport().add(panel);
		jsp.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		jsp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		return jsp;
	}

	/**
	 * Create then return the panel for a service.
	 *
	 * @param serviceParameter The corresponding service parameter.
	 * @return The created panel.
	 */
	private JPanel createPanelService(ServiceParameter serviceParameter) {
		JPanel servicePanel = new JPanel(new GridBagLayout());
		servicePanel.setBorder(new CompoundBorder(new EmptyBorder(3, 2, 3, 2),
				BorderFactory.createTitledBorder(
						serviceParameter.getService().getTitle())));
		GridBagConstraints gbc = new GridBagConstraints(0, GridBagConstraints.RELATIVE,
				1, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0), 0, 0);
		servicePanel.add(createDescriptionPanel(serviceParameter), gbc);
		if (serviceParameter.getUnit() != null
				&& !serviceParameter.getUnit().isEmpty()) {
			servicePanel.add(createUnitPanel(serviceParameter), gbc);
		}
		if (serviceParameter.getDefaultValue() != null
				&& !serviceParameter.getDefaultValue().isEmpty()) {
			servicePanel.add(createDefaultValuePanel(serviceParameter), gbc);
		}
		if (serviceParameter.getProvidedValues() != null
				&& !serviceParameter.getProvidedValues().isEmpty()) {
			servicePanel.add(createProvidedValuesPanel(serviceParameter), gbc);
		}
		return servicePanel;
	}

	/**
	 * Create then return the panel for the "description" attribute.
	 *
	 * @param serviceParameter The service parameter.
	 * @return the created panel.
	 */
	private JPanel createDescriptionPanel(ServiceParameter serviceParameter) {
		return createGenericParameterPanel("Description: ", serviceParameter.getDescription());
	}

	/**
	 * Create then return the panel for the "unit" attribute.
	 *
	 * @param serviceParameter The service parameter.
	 * @return the created panel.
	 */
	private JPanel createUnitPanel(ServiceParameter serviceParameter) {
		return createGenericParameterPanel("Unit: ", serviceParameter.getUnit());
	}

	/**
	 * Create then return the panel for the "default value" attribute.
	 *
	 * @param serviceParameter The service parameter.
	 * @return the created panel.
	 */
	private JPanel createDefaultValuePanel(ServiceParameter serviceParameter) {
		return createGenericParameterPanel("Default value: ", serviceParameter.getDefaultValue());
	}

	/**
	 * Create then return the panel for an attribute of a parameter.
	 *
	 * @param label The label of the attribute.
	 * @param value The value of the attribute.
	 * @return The created panel.
	 */
	private JPanel createGenericParameterPanel(String label, String value) {
		JPanel panel = new JPanel();
		panel.add(getTitleLabel(label));
		panel.add(getContentLabel(value));
		return panel;
	}

	/**
	 * Create then return the title label of an attribute.
	 *
	 * @param title The title.
	 * @return the created label.
	 */
	private JLabel getTitleLabel(String title) {
		JLabel label = new JLabel(title);
		label.setPreferredSize(new Dimension(124, 20));
		return label;
	}

	/**
	 * Create then return the label for the value of an attribute.
	 *
	 * @param value The value of the attribute.
	 * @return the created label.
	 */
	private JLabel getContentLabel(String value) {
		return new JLabel("<html><body style='width: 380px'>" + value
				+ "</body></html>");
	}

	/**
	 * Create then return the panel for the "provided value" attribute.
	 *
	 * @param serviceParameter The service parameter.
	 * @return The created panel.
	 */
	private JPanel createProvidedValuesPanel(ServiceParameter serviceParameter) {
		JPanel panel = new JPanel();
		JLabel titleLabel = getTitleLabel("Provided values: ");
		JLabel valuesLabel = getContentLabel(getComponentProvidedValues(
				serviceParameter.getProvidedValues()));
		panel.add(titleLabel);
		panel.add(valuesLabel);
		return panel;
	}

	/**
	 * Create then return a String for all provided values.
	 *
	 * @param providedValues The list of provided values.
	 * @return the created String.
	 */
	private String getComponentProvidedValues(List<String> providedValues) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < providedValues.size(); i++) {
			sb.append(providedValues.get(i));
			if (i != providedValues.size() - 1) {
				sb.append("; ");
			}
		}
		return sb.toString();
	}
}
