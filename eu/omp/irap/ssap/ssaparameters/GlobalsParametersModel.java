/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.ssaparameters;

import java.util.ArrayList;
import java.util.List;

import eu.omp.irap.ssap.util.SsapListenerManager;
import eu.omp.irap.ssap.util.SsapModelChangedEvent;
import eu.omp.irap.ssap.util.SsapModelListener;

/**
 * Model for the globals parameters.
 *
 * @author M. Boiziot
 */
public class GlobalsParametersModel extends SsapListenerManager implements SsapModelListener {

	public static final int POS_INDEX = 0;
	public static final int SIZE_INDEX = 1;
	public static final int BAND_INDEX = 2;
	public static final int TIME_INDEX = 3;
	public static final int FORMAT_INDEX = 4;
	private List<Parameter> listParameters;


	/**
	 * Constructor.
	 */
	public GlobalsParametersModel() {
		listParameters = new ArrayList<>(4);
		Parameter posParameter = new PosParameter();
		Parameter sizeParameter = new SizeParameter();

		List<String> formatList = new ArrayList<>();
		formatList.add("none");
		formatList.add("compliant");
		formatList.add("native");
		formatList.add("votable");
		formatList.add("fits");
		Parameter formatParameter = new Parameter("FORMAT",
				"The FORMAT parameter defines the data formats the client is"
				+ " interested in retrieving the data.", formatList, "none");

		listParameters.add(POS_INDEX, posParameter);
		listParameters.add(SIZE_INDEX, sizeParameter);
		listParameters.add(BAND_INDEX, createBandParameter());
		listParameters.add(TIME_INDEX, createTimeParameter());
		listParameters.add(FORMAT_INDEX, formatParameter);

		for (Parameter param : listParameters) {
			param.addModelListener(this);
		}
	}

	/**
	 * Return the pos parameter.
	 *
	 * @return the pos parameter.
	 */
	public PosParameter getPosParameter() {
		return (PosParameter) listParameters.get(POS_INDEX);
	}

	/**
	 * Return the size parameter.
	 *
	 * @return the size parameter.
	 */
	public SizeParameter getSizeParameter() {
		return (SizeParameter) listParameters.get(SIZE_INDEX);
	}

	/**
	 * Return the band parameter.
	 *
	 * @return the band parameter.
	 */
	public Parameter getBandParameter() {
		return listParameters.get(BAND_INDEX);
	}

	/**
	 * Return the time parameter.
	 *
	 * @return the time parameter.
	 */
	public Parameter getTimeParameter() {
		return listParameters.get(TIME_INDEX);
	}

	/**
	 * Return the format parameter.
	 *
	 * @return the format parameter.
	 */
	public Parameter getFormatParameter() {
		return listParameters.get(FORMAT_INDEX);
	}

	/**
	 * Return the String request for the query URL.
	 *
	 * @param encoded if we want to encode the URL.
	 * @return the request for the query URL.
	 */
	public String getRequest(boolean encoded) {
		StringBuilder sb = new StringBuilder();
		for (Parameter param : listParameters) {
			if (param.isConsidered()) {
				sb.append(param.getFormatedRequest(encoded)).append('&');
			}
		}
		if (sb.length() >= 1) {
			sb.deleteCharAt(sb.length() - 1);
		}
		return sb.toString();
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.ssap.util.SsapModelListener#dataChanged(eu.omp.irap.ssap.util.SsapModelChangedEvent)
	 */
	@Override
	public void dataChanged(SsapModelChangedEvent event) {
		if (Parameter.NEED_REFRESH.equals(event.getSource())) {
			fireDataChanged(event);
		}
	}

	/**
	 * Create then return a new band parameter.
	 *
	 * @return the band parameter.
	 */
	private Parameter createBandParameter() {
		String bandDescription =
				"<html>Spectral bandpass. Can be specified either (a) numerically or"
				+ " (b) textually.<br>"
				+ "(a) Wavelength in vacuum in meters. May be:<br>"
				+ "- single value: dataset matches if it contains this value;<br>"
				+ "- closed or open range: dataset matches if any portion of it"
				+ " overlaps the given spectral region.<br>"
				+ "Format: min/max, min/, /max ; min and max are inclusive.<br>"
				+ "The spectral rest frame may optionally be qualified as either "
				+ "\"source\" or \"observer\", e.g., \"1E-7/3E-6;source\"<br>"
				+ "(b) Instrumental bandpass or filter name (e.g., H, J, K, U, V,"
				+ " B, R, I, H-alpha, [OIII], etc).<br>"
				+ "Warning: a service should return an error if it does not support"
				+ " query by name or if it does not recognize the given name.</html>";
		return new Parameter("BAND", bandDescription, null, null,
				"Spectral range; can be empty");
	}

	/**
	 * Create then return a new time parameter.
	 *
	 * @return the time parameter.
	 */
	private Parameter createTimeParameter() {
		String timeDescription = "<html>The time coverage (in UTC). Allowable formats"
				+ " include the date<br>"
				+ "(e.g., yyyy-mm-dd with the month and day fields being optional,"
				+ " the minimum being yyyy),<br>"
				+ "or the date-time (e.g., yyyy-mm-ddThh:mm:ss).<br>"
				+ "May be a single value (a dataset matches if its time"
				+ " coverage includes the specified value)<br>"
				+ " or an open or closed range (format: min/max, min/,"
				+ " /max; min and max are inclusive;<br>"
				+ " a dataset matches if any portion of it overlaps the given"
				+ " temporal region).</html>";
		return new Parameter("TIME", timeDescription, null, null,
				"Time coverage; can be empty");
	}
}
