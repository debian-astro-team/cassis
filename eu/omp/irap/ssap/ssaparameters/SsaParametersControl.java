/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.ssaparameters;

import javax.swing.SwingUtilities;

import eu.omp.irap.ssap.util.SsapModelChangedEvent;
import eu.omp.irap.ssap.util.SsapModelListener;
import eu.omp.irap.ssap.util.UtilFunction;

/**
 * Control class for SsaParameters.
 *
 * @author M. Boiziot
 */
public class SsaParametersControl implements SsapModelListener {

	private SsaParametersView view;
	private SsaParametersModel model;
	private GlobalsParametersControl globalParameter;
	private OptionalsParametersControl optionalsParameters;


	/**
	 * Constructor.
	 */
	public SsaParametersControl() {
		this(new SsaParametersModel());
	}

	/**
	 * Constructor.
	 *
	 * @param pModel The model to use.
	 */
	public SsaParametersControl(SsaParametersModel pModel) {
		model = pModel;
		globalParameter = new GlobalsParametersControl(model.getGlobalParameters());
		optionalsParameters = new OptionalsParametersControl(model.getOptionalsParameters());
		view = new SsaParametersView(this);
		model.addModelListener(this);
		model.getGlobalParameters().addModelListener(this);
		model.getOptionalsParameters().addModelListener(this);
		updateQuery();
	}

	/**
	 * Return the model.
	 *
	 * @return the model.
	 */
	public SsaParametersModel getModel() {
		return model;
	}

	/**
	 * Return the view.
	 *
	 * @return the view.
	 */
	public SsaParametersView getView() {
		return view;
	}

	/**
	 * Return the {@link GlobalsParametersControl}.
	 *
	 * @return the {@link GlobalsParametersControl}.
	 */
	public GlobalsParametersControl getGlobalParameters() {
		return globalParameter;
	}

	/**
	 * Return the {@link OptionalsParametersControl}.
	 *
	 * @return the {@link OptionalsParametersControl}.
	 */
	public OptionalsParametersControl getOptionalsParameters() {
		return optionalsParameters;
	}

	/**
	 * Update the query.
	 */
	public void updateQuery() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				String query = "<SERVER>" + "?REQUEST=queryData&" + model.getCompleteRequest(false);
				view.updateQuery(query);
			}
		});
	}

	/**
	 * Handle event.
	 *
	 * @param event The event.
	 * @see eu.omp.irap.ssap.util.SsapModelListener#dataChanged(eu.omp.irap.ssap.util.SsapModelChangedEvent)
	 */
	public void dataChanged(SsapModelChangedEvent event) {
		if (Parameter.NEED_REFRESH.equals(event.getSource())) {
			updateQuery();
		}
	}

	/**
	 * Copy the generic query to clipboard.
	 */
	public void copyQueryToClipboard() {
		String query = view.getQueryTextField().getText();
		UtilFunction.copyToClipboard(query);
	}
}
