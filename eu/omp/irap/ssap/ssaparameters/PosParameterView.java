/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.ssaparameters;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import eu.omp.irap.ssap.util.Position;
import eu.omp.irap.ssap.util.SsapModelChangedEvent;
import eu.omp.irap.ssap.util.SsapModelListener;

/**
 * View for the {@link PosParameter}.
 *
 * @author M. Boiziot
 */
public class PosParameterView extends JPanel implements SsapModelListener{

	private static final long serialVersionUID = -7409048256168706291L;
	private PosParameter parameter;
	private JTextField raTextField;
	private JTextField decTextField;


	/**
	 * Constructor.
	 *
	 * @param parameter The {@link PosParameter}.
	 */
	public PosParameterView(PosParameter parameter) {
		super();
		FlowLayout fl = new FlowLayout(FlowLayout.CENTER, 5, 5);
		setLayout(fl);
		this.parameter = parameter;
		parameter.addModelListener(this);
		Dimension labelsDimension = new Dimension(37, 20);

		JLabel raLabel = new JLabel("RA:");
		raLabel.setPreferredSize(labelsDimension);
		raLabel.setToolTipText("<html>Right Ascension of the center of the"
				+ " region of interest in ICRS coordinate system (ep=J2000).<br>"
				+ "This field accepts decimal degrees (e.g., 68.98016279) and"
				+ " sexagesimal hours (e.g., 04:35:55.239).</html>");
		add(raLabel);
		add(getRaTextField());

		JLabel decLabel = new JLabel("DEC:");
		decLabel.setPreferredSize(labelsDimension);
		decLabel.setToolTipText("<html>Declination of the center of the region"
				+ " of interest in ICRS coordinate system (ep=J2000).<br>"
				+ "This field accepts decimal degrees (e.g., 16.509302351) and"
				+ " sexagesimal degrees (e.g., +16:30:33.488).</html>");
		add(decLabel);
		add(getDecTextField());

		Dimension size = new Dimension(310, 24);
		this.setSize(size);
		this.setPreferredSize(size);
	}

	/**
	 * Create if needed then return the Right Ascension {@link JTextField}.
	 *
	 * @return the RA text field.
	 */
	public JTextField getRaTextField() {
		if (raTextField == null) {
			raTextField = new JTextField();
			raTextField.setText(parameter.getRaView());
			raTextField.setPreferredSize(new Dimension(108, 20));
			raTextField.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					raChanged();
				}
			});

			raTextField.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					raChanged();
				}
			});
		}
		return raTextField;
	}

	/**
	 * Create if needed then return the declination {@link JTextField}.
	 *
	 * @return the DEC text field.
	 */
	public JTextField getDecTextField() {
		if (decTextField == null) {
			decTextField = new JTextField();
			decTextField.setText(parameter.getDecView());
			decTextField.setPreferredSize(new Dimension(108, 20));
			decTextField.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					decChanged();
				}
			});

			decTextField.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					decChanged();
				}
			});
		}
		return decTextField;
	}

	/**
	 * Handle change in DEC.
	 */
	private void decChanged() {
		String val = getDecTextField().getText();
		if (val.isEmpty() || Position.isPositionValid(val, Position.DEC)) {
			getDecTextField().setBackground(Color.WHITE);
			if (!val.equals(parameter.getDec()) && !val.equals(parameter.getDecView())) {
				parameter.setDec(val);
			}
		} else {
			parameter.setDecInvalid();
			getDecTextField().setBackground(Color.PINK);
		}
	}

	/**
	 * Handle change in RA.
	 */
	private void raChanged() {
		String val = getRaTextField().getText();
		if (val.isEmpty() || Position.isPositionValid(val, Position.RA)) {
			getRaTextField().setBackground(Color.WHITE);
			if (!val.equals(parameter.getRa()) && !val.equals(parameter.getRaView())) {
				parameter.setRa(val);
			}
		} else {
			parameter.setRaInvalid();
			getRaTextField().setBackground(Color.PINK);
		}
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.ssap.util.SsapModelListener#dataChanged(eu.omp.irap.ssap.util.SsapModelChangedEvent)
	 */
	@Override
	public void dataChanged(SsapModelChangedEvent event) {
		if (PosParameter.RA_VALUE_CHANGED.equals(event.getSource())) {
			String newRa = parameter.getRa();
			String newRaView = parameter.getRaView();
			if (!getRaTextField().getText().equals(newRa)
					&& !getRaTextField().getText().equals(newRaView)) {
				getRaTextField().setText(newRaView);
			}
			getRaTextField().setBackground(Color.WHITE);
		} else if (PosParameter.DEC_VALUE_CHANGED.equals(event.getSource())) {
			String newDec = parameter.getDec();
			String newDecView = parameter.getDecView();
			if (!getDecTextField().getText().equals(newDec)
					&& !getDecTextField().getText().equals(newDecView)) {
				getDecTextField().setText(newDecView);
			}
			getDecTextField().setBackground(Color.WHITE);
		}
	}
}
