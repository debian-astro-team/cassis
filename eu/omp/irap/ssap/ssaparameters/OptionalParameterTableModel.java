/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.ssaparameters;

import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import eu.omp.irap.ssap.service.SsaService;
import eu.omp.irap.ssap.util.jtable.Values;

/**
 * TableModel for {@link OptionalParameterTable}.
 *
 * @author M. Boiziot
 */
public class OptionalParameterTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 8921492933472878583L;
	public static final int CONSIDERED_INDEX = 0;
	public static final int NAME_INDEX = 1;
	public static final int VALUE_INDEX = 2;

	private String[] columnNames = { "Use", "Name", "Value" };
	private Vector<OptionalParameter> data;


	/**
	 * Constructor.
	 *
	 * @param parameterList The list of {@link OptionalParameter}.
	 */
	public OptionalParameterTableModel(Vector<OptionalParameter> parameterList) {
		data = parameterList;
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount() {
		return data.size();
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return 3;
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		OptionalParameter parameter = data.get(rowIndex);
		switch (columnIndex) {
			case CONSIDERED_INDEX:
				return parameter.isConsidered();
			case NAME_INDEX:
				return parameter.getName();
			case VALUE_INDEX:
				return new Values(parameter.getProvidedValues(), parameter.getCurrentValue());
			default:
				return null;
		}
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#setValueAt(java.lang.Object, int, int)
	 */
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		OptionalParameter parameter = data.get(rowIndex);
		switch (columnIndex) {
			case CONSIDERED_INDEX:
				boolean considered = ((Boolean) aValue).booleanValue();
				parameter.setConsidered(considered);
				break;
			case VALUE_INDEX:
				String value = (String) aValue;
				parameter.setCurrentValue(value);
				parameter.setConsidered(!(value == null || value.isEmpty()));
				fireTableCellUpdated(rowIndex, CONSIDERED_INDEX);
				break;
			default:
				break;
		}
		fireTableCellUpdated(rowIndex, columnIndex);
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#isCellEditable(int, int)
	 */
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		switch (columnIndex) {
			case CONSIDERED_INDEX:
			case VALUE_INDEX:
				return true;
			default:
				return false;
		}
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#getColumnClass(int)
	 */
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
		case CONSIDERED_INDEX:
			return Boolean.class;
		case NAME_INDEX:
			return String.class;
		case VALUE_INDEX:
			return Values.class;
		default:
			return Object.class;
		}
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	@Override
	public String getColumnName(int column) {
		return columnNames[column];
	}

	/**
	 * Construct an HTML String listing {@link SsaService} who are know by an
	 * {@link OptionalParameter} from a given row of the {@link JTable}.
	 *
	 * @param row The row of the {@link JTable}.
	 * @return The String.
	 */
	public String getServicesToolTip(int row) {
		OptionalParameter op = data.get(row);
		StringBuilder sb = new StringBuilder("<html>");
		sb.append("This paramater is know by:<br>");
		for (SsaService service : op.getServices()) {
			sb.append(" - ").append(service.getTitle()).append("<br>");
		}
		sb.append("</html>");
		return sb.toString();
	}

	/**
	 * Return the OptionalParameter with the given index.
	 *
	 * @param index The index.
	 * @return the OptionalParameter with the given index, or null if not found.
	 */
	public OptionalParameter getParameter(int index) {
		if (index <= data.size()) {
			return data.get(index);
		}
		return null;
	}
}
