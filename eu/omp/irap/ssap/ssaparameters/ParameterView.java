/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.ssaparameters;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import eu.omp.irap.ssap.util.HintTextFieldUI;
import eu.omp.irap.ssap.util.SsapModelChangedEvent;
import eu.omp.irap.ssap.util.SsapModelListener;

/**
 * View for a Parameter.
 *
 * @author M. Boiziot
 */
public class ParameterView extends JPanel implements SsapModelListener {

	private static final long serialVersionUID = -2910688946024710452L;
	private Parameter parameter;
	private JComponent valueComponent;


	/**
	 * Constructor.
	 *
	 * @param parameter The parameter.
	 */
	public ParameterView(Parameter parameter) {
		super();
		FlowLayout fl = new FlowLayout(FlowLayout.CENTER, 5, 5);
		setLayout(fl);
		this.parameter = parameter;
		parameter.addModelListener(this);
		JLabel label = new JLabel(parameter.getName() + ":");
		if (parameter.getDescription() != null &&
				!parameter.getDescription().isEmpty()) {
			label.setToolTipText(parameter.getDescription());
		}
		label.setSize(60, 24);
		label.setPreferredSize(new Dimension(70, 24));
		add(label);
		add(createValueComponent(parameter.getProvidedValues()));

		Dimension size = new Dimension(310, 24);
		this.setSize(size);
		this.setPreferredSize(size);
	}

	/**
	 * Create the value component.
	 *
	 * @param values The eventual values.
	 * @return The value component.
	 */
	private JComponent createValueComponent(List<String> values) {
		/* If the service have provided somes values we use an editable
		 * JCombobox with the provided values else we use a simple JTextfield.
		 */
		if (values != null && !values.isEmpty()) {
			valueComponent = new JComboBox<String>(values.toArray(new String[values.size()]));
			@SuppressWarnings("unchecked")
			JComboBox<String> valueComp = (JComboBox<String>) valueComponent;
			valueComp.setSize(230, 20);
			valueComp.setPreferredSize(new Dimension(230, 20));
			valueComp.setSelectedItem(parameter.getCurrentValue());
			valueComp.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					valueChanged(valueComponent);
				}
			});
		} else {
			valueComponent = new JTextField();
			JTextField valueComp = (JTextField) valueComponent;
			valueComp.setSize(230, 20);
			valueComp.setPreferredSize(new Dimension(230, 20));
			valueComp.setText(parameter.getCurrentValue());
			if (parameter.getHint() != null) {
				valueComp.setUI(new HintTextFieldUI(parameter.getHint()));
			}
			valueComp.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					valueChanged(valueComponent);
				}
			});

			valueComp.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					valueChanged(valueComponent);
				}
			});
		}
		if (parameter.getDescription() != null &&
				!parameter.getDescription().isEmpty()) {
			valueComponent.setToolTipText(parameter.getDescription());
		}
		return valueComponent;
	}

	/**
	 * Do the necessary when a value is changed.
	 *
	 * @param jcomponent The component where the value is changed.
	 */
	@SuppressWarnings("unchecked")
	private void valueChanged(JComponent jcomponent) {
		String stringValue = null;
		if (jcomponent instanceof JComboBox) {
			stringValue = (String) ((JComboBox<String>)jcomponent).getSelectedItem();
		} else if (jcomponent instanceof JTextField) {
			stringValue = ((JTextField)jcomponent).getText();
		} else {
			return;
		}
		parameter.setCurrentValue(stringValue);
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.ssap.util.SsapModelListener#dataChanged(eu.omp.irap.ssap.util.SsapModelChangedEvent)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public void dataChanged(SsapModelChangedEvent event) {
		if (Parameter.VALUE_UPDATE.equals(event.getSource())) {
			String value = (String)event.getValue();
			String guiValue = null;
			if (valueComponent instanceof JComboBox) {
				guiValue = (String) ((JComboBox<String>)valueComponent).getSelectedItem();
			} else if (valueComponent instanceof JTextField) {
				guiValue = ((JTextField)valueComponent).getText();
			}

			if (!value.equals(guiValue)) {
				if (valueComponent instanceof JComboBox) {
					 ((JComboBox<String>)valueComponent).setSelectedItem(value);
				} else if (valueComponent instanceof JTextField) {
					((JTextField)valueComponent).setText(value);
				}
			}
		}
	}
}
