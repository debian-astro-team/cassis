/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.ssaparameters;

import java.awt.Color;
import java.awt.Component;
import java.awt.Window;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.TableCellRenderer;

import eu.omp.irap.ssap.util.UtilFunction;
import eu.omp.irap.ssap.util.jtable.Values;
import eu.omp.irap.ssap.util.jtable.ValuesCellRenderer;
import eu.omp.irap.ssap.util.jtable.ValuesEditor;

/**
 * {@link JTable} to display the {@link OptionalParameter}s.
 *
 * @author M. Boiziot
 */
public class OptionalParameterTable extends JTable {

	private static final long serialVersionUID = 5076517169052564875L;
	private OptionalParameterTableModel tableModel;


	/**
	 * Constructor.
	 *
	 * @param datas The {@link OptionalParameter}s.
	 */
	public OptionalParameterTable(Vector<OptionalParameter> datas) {
		super();
		tableModel = new OptionalParameterTableModel(datas);
		setModel(tableModel);
		getTableHeader().setReorderingAllowed(false);
		getColumnModel().getColumn(OptionalParameterTableModel.CONSIDERED_INDEX).setMaxWidth(30);
		setShowGrid(true);
		getColumnModel().getColumn(OptionalParameterTableModel.NAME_INDEX)
				.setCellRenderer(new ServiceToolTipRenderer());
		setDefaultEditor(Values.class, new ValuesEditor());
		setDefaultRenderer(Values.class, new ValuesCellRenderer());
		addMouseListener();
	}

	/* (non-Javadoc)
	 * @see javax.swing.JTable#prepareRenderer(javax.swing.table.TableCellRenderer, int, int)
	 */
	@Override
	public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
		Component c = super.prepareRenderer(renderer, row, column);
		if (row % 2 == 0)
			c.setBackground(new Color(240, 250, 250));
		else
			c.setBackground(getBackground());

		if (isCellSelected(row, column)) {
			Color c2 = new Color(184, 207, 229);
			c.setBackground(c2);
		}
		return c;
	}

	/**
	 * Refresh the table from the datas.
	 */
	public void refreshData() {
		tableModel.fireTableDataChanged();
	}

	/**
	 * Add a mouse listener handling right click on name column to display more
	 *  information on a given parameter.
	 */
	private void addMouseListener() {
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent me) {
				int row = OptionalParameterTable.this.rowAtPoint(me.getPoint());
				int col = OptionalParameterTable.this.columnAtPoint(me.getPoint());
				if (SwingUtilities.isRightMouseButton(me)
						&& col == OptionalParameterTableModel.NAME_INDEX
						&& row != -1) {
					OptionalParameter param = tableModel.getParameter(row);
					ParameterInformationFrame frame = new ParameterInformationFrame(
							param.getName(), param.getServiceParameterList());
					Window window = SwingUtilities.getWindowAncestor(OptionalParameterTable.this);
					if (window != null) {
						UtilFunction.center(window, frame);
					}
					frame.setVisible(true);
				}
			}
		});
	}
}
