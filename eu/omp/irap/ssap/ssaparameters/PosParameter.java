/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.ssaparameters;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;

import eu.omp.irap.ssap.util.Position;
import eu.omp.irap.ssap.util.SsapModelChangedEvent;

/**
 * Define a Position parameter.
 *
 * @author M. Boiziot
 */
public class PosParameter extends Parameter {

	public static final String RA_VALUE_CHANGED = "raValueChanged";
	public static final String DEC_VALUE_CHANGED = "decValueChanged";
	private static final Logger LOGGER = Logger.getLogger("PosParameter");
	private String ra;
	private String dec;
	private String raView;
	private String decView;
	private boolean raInvalid;
	private boolean decInvalid;


	/**
	 * Constructor.
	 */
	public PosParameter() {
		super("POS", "The center of the region of interest.", null, null);
		this.raInvalid = false;
		this.decInvalid = false;
	}

	/**
	 * Return the Right Ascension as specified by the user/sesame.
	 *
	 * @return the right ascension.
	 */
	public String getRa() {
		return ra;
	}

	/**
	 * Return the right ascension in sexagesimal hour.
	 *
	 * @return the right ascension in sexagesimal hour.
	 */
	public String getRaView() {
		return raView;
	}

	/**
	 * Change the Right Ascension.
	 *
	 * @param newValue The new ra value.
	 */
	public void setRa(String newValue) {
		this.ra = newValue;
		this.raInvalid = false;
		try {
			this.raView = Position.convertToSexagesimalHour(newValue, true);
		} catch (IllegalArgumentException iae) {
			this.raView = newValue;
		}
		fireDataChanged(new SsapModelChangedEvent(RA_VALUE_CHANGED, newValue));
		fireDataChanged(new SsapModelChangedEvent(NEED_REFRESH));
	}

	/**
	 * Return the Declination.
	 *
	 * @return the declination.
	 */
	public String getDec() {
		return dec;
	}

	/**
	 * Return the right ascension in sexagesimal degrees.
	 *
	 * @return the right ascension in sexagesimal degrees.
	 */
	public String getDecView() {
		return decView;
	}

	/**
	 * Change the declination.
	 *
	 * @param newValue the new dec value.
	 */
	public void setDec(String newValue) {
		this.dec = newValue;
		this.decInvalid = false;
		try {
			this.decView = Position.convertToSexagesimalDec(newValue, true);
		} catch (IllegalArgumentException iae) {
			this.decView = newValue;
		}
		fireDataChanged(new SsapModelChangedEvent(PosParameter.DEC_VALUE_CHANGED, newValue));
		fireDataChanged(new SsapModelChangedEvent(Parameter.NEED_REFRESH));
	}

	/**
	 * Return the current value in degrees (as needed for the SSA request).
	 *
	 * @return the current value in degrees (as needed for the SSA request).
	 */
	private String getDegresValue() {
		return Position.convertToDegrees(ra, Position.RA) + ',' + Position.convertToDegrees(dec, Position.DEC);
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.ssap.ssaparameters.Parameter#isConsidered()
	 */
	@Override
	public boolean isConsidered() {
		return isRaValid() && isDecValid();
	}

	/* (non-Javadoc)
	 * @see eu.omp.irap.ssap.ssaparameters.Parameter#getFormatedRequest(boolean)
	 */
	@Override
	public String getFormatedRequest(boolean encoded) {
		String request = null;
		if (encoded) {
			try {
				request = URLEncoder.encode(getName(), "UTF-8") + "=" + URLEncoder.encode(getDegresValue(), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				LOGGER.log(Level.WARNING, "UnsupportedEncodingException... do not encode the parameter " + getName() + " for the URL.");
				request = getFormatedRequest(false);
			}
		} else {
			request =  getName() + "=" + getDegresValue();
		}
		return request;
	}

	/**
	 * Set declination value as invalid.
	 */
	public void setDecInvalid() {
		decInvalid = true;
		fireDataChanged(new SsapModelChangedEvent(Parameter.NEED_REFRESH));
	}

	/**
	 * Set right ascension as invalid.
	 */
	public void setRaInvalid() {
		raInvalid = true;
		fireDataChanged(new SsapModelChangedEvent(Parameter.NEED_REFRESH));
	}

	/**
	 * Reset the value for RA and DEC.
	 */
	public void resetData() {
		setDec("");
		setRa("");
	}

	/**
	 * Check if RA value is valid.
	 *
	 * @return true if RA value is valid, false otherwise.
	 */
	private boolean isRaValid() {
		return !(raInvalid || ra == null || ra.isEmpty());
	}

	/**
	 * Check if DEC value is valid.
	 *
	 * @return true if DEC value is valid, false otherwise.
	 */
	private boolean isDecValid() {
		return !(decInvalid || dec == null || dec.isEmpty());
	}
}
