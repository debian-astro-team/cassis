/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.ssaparameters;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;

import eu.omp.irap.ssap.util.Size;
import eu.omp.irap.ssap.util.SsapModelChangedEvent;

/**
 * Define a Size (Search Radius in the view) parameter.
 *
 * @author M. Boiziot
 */
public class SizeParameter extends Parameter {

	public static final String UNIT_VALUE_CHANGED = "sizeUnitChanged";
	private static final Logger LOGGER = Logger.getLogger("SizeParameter");
	private SizeUnit unit;


	/**
	 * Constructor.
	 */
	public SizeParameter() {
		super("SIZE", "The radius of the search region.", null, null);
		this.unit = SizeUnit.ARC_SEC;
		this.setCurrentValue("10");
	}

	/**
	 * Return the unit.
	 *
	 * @return the unit.
	 */
	public SizeUnit getUnit() {
		return unit;
	}

	/**
	 * Change the unit.
	 *
	 * @param unit The new unit to set.
	 */
	public void setUnit(SizeUnit unit) {
		this.unit = unit;
		fireDataChanged(new SsapModelChangedEvent(UNIT_VALUE_CHANGED, unit));
		fireDataChanged(new SsapModelChangedEvent(NEED_REFRESH));
	}

	/**
	 * Create a string with the formated request for this parameter.
	 *
	 * @param encoded Encoding the URL. True to encode it, false to do not.
	 * @return The formated request.
	 * @see eu.omp.irap.ssap.ssaparameters.Parameter#getFormatedRequest(boolean)
	 */
	@Override
	public String getFormatedRequest(boolean encoded) {
		String request = null;
		if (encoded) {
			try {
				request = URLEncoder.encode(getName(), "UTF-8") + "="
						+ URLEncoder.encode(getDegreesValue(), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				LOGGER.log(Level.WARNING,
						"UnsupportedEncodingException... do not encode the parameter "
								+ getName() + " for the URL.");
				request = getFormatedRequest(false);
			}
		} else {
			request =  getName() + "=" + getDegreesValue();
		}
		return request;
	}

	/**
	 * Return if the parameter is considered.
	 *
	 * @return if the parameter is considered.
	 * @see eu.omp.irap.ssap.ssaparameters.Parameter#isConsidered()
	 */
	@Override
	public boolean isConsidered() {
		return Size.isSizeValid(getCurrentValue());
	}

	/**
	 * Convert the current value to degrees then return it.
	 *
	 * @return return the current value to degrees.
	 */
	private String getDegreesValue() {
		return Size.convertToDegrees(getCurrentValue(), unit);
	}

}
