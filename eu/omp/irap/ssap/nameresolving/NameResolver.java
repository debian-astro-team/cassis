/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package eu.omp.irap.ssap.nameresolving;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import eu.omp.irap.ssap.util.OperationException;
import eu.omp.irap.ssap.util.UtilFunction;


/**
 * Simple class for getting the result of a name resolving after a call to parse(args).
 * This class use Sesame (http://cds.u-strasbg.fr/cgi-bin/Sesame).
 *
 * @author M. Boiziot
 */
public final class NameResolver {

	private static final String QUERY_URL_SESAME = "http://cdsweb.u-strasbg.fr/cgi-bin/nph-sesame/-oxp/?";
	private static final String QUERY_URL_MIRROR = "http://vizier.cfa.harvard.edu/viz-bin/nph-sesame/-oxp/?";
	private static final String RA_ELEMENT = "jradeg";
	private static final String DEC_ELEMENT = "jdedeg";
	private String objectName;
	private String ra;
	private String dec;


	/**
	 * Constructor.
	 *
	 * @param objectName The name of the astronomical object.
	 * @param ra The ra.
	 * @param dec The rec.
	 */
	private NameResolver(String objectName, String ra, String dec) {
		this.objectName = objectName;
		this.ra = ra;
		this.dec = dec;
	}

	/**
	 * Return the name of the astronomical object.
	 *
	 * @return The name of the astronomical object.
	 */
	public String getObjectName() {
		return objectName;
	}

	/**
	 * Return the Right ascension of the object in decimal degrees.
	 *
	 * @return Return the ra (Right Ascension).
	 */
	public String getRa() {
		return ra;
	}

	/**
	 * Return the Declination of the object in decimal degrees.
	 *
	 * @return the dec (Declination).
	 */
	public String getDec() {
		return dec;
	}

	/**
	 * Resolve the ra et dec for an astronomical object.
	 *
	 * @param objectName The name of the object.
	 * @return a NameResolver
	 * @throws OperationException If the object was not resolved.
	 */
	public static NameResolver resolve(String objectName) throws OperationException {
		InputStream is = null;
		String ra = null;
		String dec = null;
		try {
			is = queryService(objectName);
			if (is != null) {
				DocumentBuilderFactory docBuilderFact = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder;

				docBuilder = docBuilderFact.newDocumentBuilder();
				Document doc = docBuilder.parse(is);
				doc.getDocumentElement().normalize();

				NodeList nodeListResolver = doc.getElementsByTagName("Resolver");
				if (nodeListResolver.getLength() >= 1) {
					Element element = (Element) nodeListResolver.item(0);
					ra = element.getElementsByTagName(RA_ELEMENT).item(0)
							.getChildNodes().item(0).getNodeValue().trim();
					dec = element.getElementsByTagName(DEC_ELEMENT).item(0)
							.getChildNodes().item(0).getNodeValue().trim();
				} else {
					throw new OperationException(
							"The provided object was not found on Sesame.");
				}
			}
		} catch (Exception e) {
			throw new OperationException(e.getMessage(), e);
		} finally {
			UtilFunction.close(is);
		}

		return new NameResolver(objectName, ra, dec);
	}

	/**
	 * Do a connection to the given service for the given object name.
	 *
	 * @param serviceUrl The service URL.
	 * @param objectName The object name.
	 * @return the InputStream for the connection (can be null in case of error).
	 * @throws IOException In case of IO error.
	 */
	private static InputStream queryService(String serviceUrl, String objectName) throws IOException {
		try {
			URL url = new URL(serviceUrl + URLEncoder.encode(objectName, "UTF-8"));
			URLConnection connection = url.openConnection();
			return connection.getInputStream();
		} catch (MalformedURLException | UnsupportedEncodingException e) {
			return null;
		}
	}

	/**
	 * Create a connection to service to resolve position of a given object Name.
	 * If the first service in not working, try to connect to the mirror.
	 *
	 * @param objectName The object name to resolve.
	 * @return the InputStream for the connection (can be null in case of error).
	 */
	private static InputStream queryService(String objectName) {
		InputStream is = null;
		try {
			is = queryService(QUERY_URL_SESAME, objectName);
		} catch (IOException ioe) {
			try {
				is = queryService(QUERY_URL_MIRROR, objectName);
			} catch (IOException ioeMirror) {
				// is stay null; error will be handled latter.
			}
		}
		return is;
	}
}
