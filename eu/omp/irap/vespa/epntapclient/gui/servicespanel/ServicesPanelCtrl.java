/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 * This file is a part of EpnTAPClient.
 * This program aims to provide EPN-TAP support for software clients, like CASSIS spectrum analyzer.
 * See draft specifications: https://voparis-confluence.obspm.fr/pages/viewpage.action?pageId=559861
 * Copyright (C) 2016 Institut de Recherche en Astrophysique et Planétologie.
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package eu.omp.irap.vespa.epntapclient.gui.servicespanel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import eu.omp.irap.vespa.epntapclient.epntap.service.Queries;
import eu.omp.irap.vespa.epntapclient.epntap.service.ServiceCore;
import eu.omp.irap.vespa.epntapclient.epntap.service.ServicesList;
import eu.omp.irap.vespa.epntapclient.gui.mainpanel.MainPanelListener;
import eu.omp.irap.vespa.votable.Consts;
import eu.omp.irap.vespa.votable.votable.VOTableCtrl;

/**
 * @author N. Jourdane
 */
public class ServicesPanelCtrl extends VOTableCtrl implements ServicesPanelListener {

	/** The separator used between each services in the custom service text fields. */
	private static final String CUSTOM_SERVICES_SEPARATOR = ";";

	/** The listener of the main panel. */
	private MainPanelListener listener;

	/** The list of services target Urls selected by the user on the service panel. */
	private ServicesList services;

	/** The view of the services panel. */
	private ServicesPanelView view;


	/**
	 * Constructor of ServicesPanelCtrl
	 *
	 * @param listener The listener of the main panel.
	 */
	public ServicesPanelCtrl(MainPanelListener listener) {
		this.listener = listener;
		services = new ServicesList();
		view = new ServicesPanelView(this);
	}

	/** Download and parse the list of services. */
	public void acquire() {
		String getServicesQuery = String.format(Queries.SELECT_ALL_TAP_SERVICES_WHERE_CORE,
				ServiceCore.EPNCORE);
		acquireVOTable(Consts.DEFAULT_REGISTRY_URL, getServicesQuery);
	}

	@Override
	public void displayError(String message, Exception error) {
		listener.displayError(message, error);
	}

	@Override
	public void displayInfo(String shortMessage, String detailledMessage) {
		listener.displayInfo(shortMessage, detailledMessage);
	}

	@Override
	public void displayTable() {
		view.fillTable(voTableData);
	}

	/**
	 * @return The list of services target Urls selected by the user on the service panel.
	 */
	public ServicesList getServices() {
		return services;
	}

	/**
	 * @return The view of the services panel. Used in MainPanelCtrl to add panels in the main
	 *         window.
	 */
	public ServicesPanelView getView() {
		return view;
	}

	@Override
	public void onRowsSelected() {
		List<String> newTablesNames = new ArrayList<>();
		List<String> newServicesUrls = new ArrayList<>();

		int idxTableName = view.getTable().getColumn("table_name").getModelIndex();
		int idxServiceUrl = view.getTable().getColumn("access_url").getModelIndex();

		for (int row : view.getSelectedRows()) {
			newTablesNames.add((String) view.getValueAt(idxTableName, row));
			newServicesUrls.add((String) view.getValueAt(idxServiceUrl, row));
		}
		services.updateSelectedServices(newTablesNames, newServicesUrls);
		listener.updateQuery();
	}

	@Override
	public void onServiceListUpdated() {
		String newTableName = view.getTableNameTextField().getText();
		String newTargetUrl = view.getServiceUrlTextField().getText();
		List<String> customTableNames = Arrays
				.asList(newTableName.split(CUSTOM_SERVICES_SEPARATOR));
		List<String> customServicesUrls = Arrays
				.asList(newTargetUrl.split(CUSTOM_SERVICES_SEPARATOR));

		if (!newTableName.isEmpty() && !newTargetUrl.isEmpty()
				&& customTableNames.size() == customServicesUrls.size()) {
			services.updateCustomServices(customTableNames, customServicesUrls);
		}
		listener.updateQuery();
	}

	@Override
	public void setWaitMode(boolean enableWaitMode) {
		listener.setWaitMode(enableWaitMode);
	}
}
