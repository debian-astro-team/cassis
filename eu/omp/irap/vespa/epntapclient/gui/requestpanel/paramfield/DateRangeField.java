/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 * This file is a part of EpnTAPClient.
 * This program aims to provide EPN-TAP support for software clients, like CASSIS spectrum analyzer.
 * See draft specifications: https://voparis-confluence.obspm.fr/pages/viewpage.action?pageId=559861
 * Copyright (C) 2016 Institut de Recherche en Astrophysique et Planétologie.
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package eu.omp.irap.vespa.epntapclient.gui.requestpanel.paramfield;

import java.awt.Color;
import java.awt.Dimension;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.swing.JLabel;
import javax.swing.JTextField;

import eu.omp.irap.vespa.epntapclient.gui.requestpanel.RequestPanelListener;

/**
 * The date range field is used for couples of parameter with both a `Date` type (actually only
 * `time_min` and `time_max` parameters is concerned for now). These are JTextFields which check if
 * the content is a valid date, according to DATE_FORMAT. If the parameter is valid or if it is
 * empty, then the dates value are sent to the controller, in Julian Day format.
 *
 * @author N. Jourdane
 */
public class DateRangeField extends ParamField implements TextFieldListener {

	/** The date format used in the DateRange field */
	private static final String DATE_FORMAT = "dd/MM/yyyy";

	/** The regex used to validate the Date fields */
	private static final String DATE_REGEX = "(^(((0[1-9]|1[0-9]|2[0-8])[\\/](0[1-9]|1[012]))|((29|30|31)[\\/](0[13578]|1[02]))|((29|30)[\\/](0[4,6,9]|11)))[\\/](19|[2-9][0-9])\\d\\d$)|(^29[\\/]02[\\/](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)";

	/** The serial version UID. */
	private static final long serialVersionUID = 1L;

	/** The JTextField used to put the parameter maximum value of the range. */
	private JTextField fieldMax;

	/** The JTextField used to put the parameter minimum value of the range. */
	private JTextField fieldMin;


	/**
	 * Method constructor
	 *
	 * @param requestPanelListener The listener of the request panel.
	 * @param paramName The name of the parameter.
	 */
	public DateRangeField(RequestPanelListener requestPanelListener, String paramName) {
		super(requestPanelListener, paramName);
		this.add(new JLabel("min "));
		fieldMin = new JTextField();
		fieldMin.setName(MIN_SUFFIX);
		fieldMin.setPreferredSize(
				new Dimension(MIN_FIELD_WIDTH, FIELD_HEIGHT));
		addChangeListener(this, fieldMin);
		this.add(fieldMin);

		this.add(new JLabel("max "));
		fieldMax = new JTextField();
		fieldMax.setName(MAX_SUFFIX);
		fieldMax.setPreferredSize(
				new Dimension(MIN_FIELD_WIDTH, FIELD_HEIGHT));
		addChangeListener(this, fieldMin);
		this.add(fieldMax);
	}

	/**
	 * This method is called each time the field is modified.
	 */
	@Override
	public void update(JTextField field) {
		DateFormat df = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);
		if (field.getText().isEmpty()) {
			field.setBackground(Color.WHITE);
			requestPanelListener.onParameterRemoved(paramName + field.getName());
		} else if (field.getText().matches(DATE_REGEX)) {
			try {
				long date = df.parse(field.getText()).getTime();
				date = Math.round(date / 86400000.0 + 2440587.5); // to JD
				requestPanelListener.onParameterUpdated(paramName + field.getName(),
						date);
				field.setBackground(Color.WHITE);
			} catch (ParseException e) {
				field.setBackground(Color.PINK);
			}
		} else {
			field.setBackground(Color.PINK);
		}
	}
}
