/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 * This file is a part of EpnTAPClient.
 * This program aims to provide EPN-TAP support for software clients, like CASSIS spectrum analyzer.
 * See draft specifications: https://voparis-confluence.obspm.fr/pages/viewpage.action?pageId=559861
 * Copyright (C) 2016 Institut de Recherche en Astrophysique et Planétologie.
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package eu.omp.irap.vespa.epntapclient.gui.requestpanel.paramfield;

import java.awt.Color;

import javax.swing.JTextField;

import eu.omp.irap.vespa.epntapclient.gui.requestpanel.RequestPanelListener;

/**
 * The float range field is used for couples of parameter with both a `Float` class. These are
 * JTextFields which check if the content is a valid float. If the parameter is valid or if it is
 * empty, then the float value are sent to the controller.
 *
 * @author N. Jourdane
 */
public class FloatRangeField extends ParamField implements TextFieldListener {

	/** The serial version UID. */
	private static final long serialVersionUID = 1L;

	/** The JTextField used to put the parameter maximum value of the range. */
	private JTextField fieldMax;

	/** The JTextField used to put the parameter minimum value of the range. */
	private JTextField fieldMin;


	/**
	 * Method constructor
	 *
	 * @param requestPanelListener The listener of the request panel.
	 * @param paramName The name of the parameter.
	 */
	public FloatRangeField(RequestPanelListener requestPanelListener, String paramName) {
		super(requestPanelListener, paramName);
		fieldMin = new JTextField();
		fieldMin.setName(MIN_SUFFIX);
		addChangeListener(this, fieldMin);
		this.add(fieldMin);

		fieldMax = new JTextField();
		fieldMax.setName(MAX_SUFFIX);
		addChangeListener(this, fieldMax);
		this.add(fieldMax);
	}

	/**
	 * This method is called each time the field is modified.
	 */
	@Override
	public void update(JTextField field) {
		if (field.getText().isEmpty()) {
			field.setBackground(Color.WHITE);
			requestPanelListener.onParameterRemoved(paramName + field.getName());
		} else {
			try {
				requestPanelListener.onParameterUpdated(paramName + field.getName(),
						Float.parseFloat(field.getText()));
				field.setBackground(Color.WHITE);
			} catch (NumberFormatException e) {
				field.setBackground(Color.PINK);
			}
		}
	}
}
