/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 * This file is a part of EpnTAPClient.
 * This program aims to provide EPN-TAP support for software clients, like CASSIS spectrum analyzer.
 * See draft specifications: https://voparis-confluence.obspm.fr/pages/viewpage.action?pageId=559861
 * Copyright (C) 2016 Institut de Recherche en Astrophysique et Planétologie.
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package eu.omp.irap.vespa.epntapclient.gui.requestpanel.paramfield;

import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import eu.omp.irap.vespa.epntapclient.gui.requestpanel.RequestPanelListener;

/**
 * A field used to set a service parameter to build the query (in the parameter panel). ParamField
 * is an abstract method and all type of parameter field should extend it. See
 * https://voparis-confluence.obspm.fr/display/VES/4+-+EPN-TAP+queries to get all parameters
 * details.
 *
 * @author N. Jourdane
 */
public abstract class ParamField extends JPanel {

	/** The serial version UID. */
	private static final long serialVersionUID = 1L;

	/** The preferred field height. */
	protected static final int FIELD_HEIGHT = 20;

	/** The preferred label width. */
	protected static final int LABEL_WIDTH = 140;

	/** The maximum width of the field. */
	protected static final int MAX_FIELD_WIDTH = 400;

	/** The suffix used in REG-TAP parameters names, indicating that it is a end of a range. */
	protected static final String MAX_SUFFIX = "max";

	/** The minimum width of the field. */
	protected static final int MIN_FIELD_WIDTH = 30;

	/** The suffix used in REG-TAP parameters names, indicating that it's a beginning of a range. */
	protected static final String MIN_SUFFIX = "min";

	/** The parameter name of the field */
	protected String paramName;

	/** The main view of the application. */
	protected RequestPanelListener requestPanelListener;


	/**
	 * Method constructor for the parameter field abstract class, which do all common action for all
	 * type of field, such as displaying the name of the parameter.
	 *
	 * @param requestPanelListener The listener of the request panel.
	 * @param paramName The name of the parameter.
	 */
	public ParamField(RequestPanelListener requestPanelListener, String paramName) {
		super();

		this.requestPanelListener = requestPanelListener;
		this.paramName = paramName;

		buildParamField();
	}

	/**
	 * To add the listener. @see TextFieldListener
	 *
	 * @param changeListener The listener of text fields.
	 * @param field The field to listen.
	 */
	public static void addChangeListener(final TextFieldListener changeListener,
			final JTextField field) {

		field.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void changedUpdate(DocumentEvent de) {
				changeListener.update(field);
			}

			@Override
			public void insertUpdate(DocumentEvent de) {
				changeListener.update(field);
			}

			@Override
			public void removeUpdate(DocumentEvent de) {
				changeListener.update(field);
			}

		});
	}

	/**
	 * Build the panel and add GUI elements on it.
	 */
	private void buildParamField() {
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));

		setMaximumSize(new Dimension(MAX_FIELD_WIDTH, FIELD_HEIGHT));

		String strLabel = paramName.replaceAll("_", " ").trim();
		JLabel label = new JLabel(strLabel.substring(0, 1).toUpperCase() + strLabel.substring(1));
		label.setPreferredSize(new Dimension(LABEL_WIDTH, FIELD_HEIGHT));

		this.add(label);
	}
}
