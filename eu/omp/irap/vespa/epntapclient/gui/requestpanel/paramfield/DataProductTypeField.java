/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 * This file is a part of EpnTAPClient.
 * This program aims to provide EPN-TAP support for software clients, like CASSIS spectrum analyzer.
 * See draft specifications: https://voparis-confluence.obspm.fr/pages/viewpage.action?pageId=559861
 * Copyright (C) 2016 Institut de Recherche en Astrophysique et Planétologie.
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package eu.omp.irap.vespa.epntapclient.gui.requestpanel.paramfield;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;

import eu.omp.irap.vespa.epntapclient.gui.requestpanel.RequestPanelListener;

/**
 * The data product type field is used only for the `dataproduct_type` parameter. It is a ComboBox
 * filled with a list of static data product types (see
 * https://voparis-confluence.obspm.fr/display/VES/4+-+EPN-TAP+queries#id-4-EPN-TAPqueries-
 * __RefHeading__35_312326667_Toc3037660444.2.4DataProductType). The parameter is sent to the
 * controller each time it is updated.
 *
 * @author N. Jourdane
 */
public class DataProductTypeField extends ParamField {

	/**
	 * An enumeration of all available data product types. Each values comes with an id because
	 * EPN-TAP table can possibly be filled with the id instead of the name, so the query have to
	 * ask for both of them.
	 *
	 * @author N. Jourdane
	 */
	@SuppressWarnings("javadoc")
	public enum DataProductType {
		// @noformat
		ALL("All", "all"),            CA("Catalog", "ca"),         CU("Cube", "cu"),
		DS("Dynamic spectrum", "ds"), IM("Image", "im"), MO("Movie", "mo"),
		PR("Profile", "pr"),           SC("Spectral cube", "sc"),         SP("Spectrum", "sp"),
		SV("Spatial vector", "sv"),      TS("Time series", "ts"),       VO("Volume", "vo");
		// @format

		/** The id of the data product type, such as `ds`. */
		private String id = "";

		/** The full name of the data product type, such as `Dynamic spectrum`. */
		private String name = "";


		/**
		 * Method constructor for the enumeration.
		 *
		 * @param name The full name of the data product type, such as `Dynamic spectrum`.
		 * @param id The id of the data product type, such as `ds`.
		 */
		DataProductType(String name, String id) {
			this.name = name;
			this.id = id;
		}

		/**
		 * @return A list of two strings, containing the name (formated for the query) and the id,
		 *         used in the query. A list is used instead of a array because the getQuery
		 *         function ( @see Queries) needs to know the parameter type.
		 */
		public List<String> query() {
			List<String> item = new ArrayList<>();
			item.add(name.replace(" ", "-").toLowerCase());
			item.add(id);
			return item;
		}

		@Override
		public String toString() {
			return name;
		}
	}


	/** The serial version UID. */
	private static final long serialVersionUID = 1L;

	/** The comboBox used to select the data product type. */
	private JComboBox<DataProductType> comboBox;


	/**
	 * Method constructor
	 *
	 * @param requestPanelListener The listener of the request panel.
	 * @param paramName The name of the parameter.
	 */
	public DataProductTypeField(RequestPanelListener requestPanelListener, String paramName) {
		super(requestPanelListener, paramName);
		this.add(getComboBox());
	}

	/**
	 * Returns the combo-box of the DataProductType field, and build it if it doesn't exist.
	 *
	 * @return The combo-box.
	 */
	public JComboBox<DataProductType> getComboBox() {
		if (comboBox == null) {
			comboBox = new JComboBox<>(DataProductType.values());
			comboBox.setSelectedItem(DataProductType.ALL);
			comboBox.setPreferredSize(new Dimension(MIN_FIELD_WIDTH, FIELD_HEIGHT));
			comboBox.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					update();
				}
			});
		}

		return comboBox;
	}

	/**
	 * This method is called each time the field is modified.
	 */
	public void update() {
		DataProductType item = (DataProductType) comboBox.getSelectedItem();
		if (DataProductType.ALL.equals(item)) {
			requestPanelListener.onParameterRemoved(paramName);
		} else {
			requestPanelListener.onParameterUpdated(paramName, item.query());
		}
	}

}
