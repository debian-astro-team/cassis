/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 * This file is a part of EpnTAPClient.
 * This program aims to provide EPN-TAP support for software clients, like CASSIS spectrum analyzer.
 * See draft specifications: https://voparis-confluence.obspm.fr/pages/viewpage.action?pageId=559861
 * Copyright (C) 2016 Institut de Recherche en Astrophysique et Planétologie.
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package eu.omp.irap.vespa.epntapclient.gui.requestpanel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import eu.omp.irap.vespa.epntapclient.gui.requestpanel.paramfield.DataProductTypeField;
import eu.omp.irap.vespa.epntapclient.gui.requestpanel.paramfield.DateRangeField;
import eu.omp.irap.vespa.epntapclient.gui.requestpanel.paramfield.FloatRangeField;
import eu.omp.irap.vespa.epntapclient.gui.requestpanel.paramfield.ParamField;
import eu.omp.irap.vespa.epntapclient.gui.requestpanel.paramfield.TargetNameField;

/**
 * The view of the panel where the user builds the query.
 *
 * @author N. Jourdane
 */
public class RequestPanelView extends JPanel {

	/** The height of the buttons panel. */
	private static final int BUTTON_PANEL_HEIGHT = 20;

	/** The default serial version UID. */
	private static final long serialVersionUID = 1L;

	/** The GUI element of the button to move the file in the specified location. */
	private JButton buttonSave;

	/** The GUI element of the button to send the query. */
	private JButton buttonSend;

	/** A field to set the dataproduct_type parameter. */
	private DataProductTypeField dataProductTypeField;

	/** The parameters fields for the request. */
	private List<ParamField> paramFields;

	/** The text area where the user write the query. */
	private JTextArea queryArea;

	/** A field to set the spectral_range parameter. */
	private FloatRangeField spectralRangeField;

	/** A field to set the target_name parameter. */
	private TargetNameField targetNameField;

	/** A field to set the time_range parameter. */
	private DateRangeField timeRangeField;

	/** The listener for the request panel. */
	private RequestPanelListener listener;


	/**
	 * Method constructor
	 *
	 * @param requestPanelListener The listener of the request panel view.
	 */
	public RequestPanelView(RequestPanelListener requestPanelListener) {
		listener = requestPanelListener;
		buildRequestPanel();
	}

	/**
	 * Get the GUI element of the send button. If it doesn't exist, create it.
	 *
	 * @return The button to download and parse the VOTable.
	 */
	public JButton getButtonSend() {
		if (buttonSend == null) {
			buttonSend = new JButton("Send query");
			buttonSend.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					listener.onSendButtonClicked();
				}
			});
		}
		return buttonSend;
	}

	/**
	 * Get the GUI element of the data product field. If it doesn't exist, create it.
	 *
	 * @return The data product field.
	 */
	public DataProductTypeField getDataProductTypeField() {
		if (dataProductTypeField == null) {
			dataProductTypeField = new DataProductTypeField(listener,
					"dataproduct_type");
		}

		return dataProductTypeField;
	}

	/**
	 * Get the GUI element of the parameters fields list. If it doesn't exist, create it.
	 *
	 * @return The parameters fields list.
	 */
	public List<ParamField> getParamFields() {
		if (paramFields == null) {
			paramFields = new ArrayList<>();
			paramFields.add(getTargetNameField());
			paramFields.add(getTimeRangeField());
			paramFields.add(getSpectralRangeField());
			paramFields.add(getDataProductTypeField());
		}
		return paramFields;
	}

	/**
	 * Get the GUI element of the query area. If it doesn't exist, create it.
	 *
	 * @return The target query area.
	 */
	public JTextArea getQueryArea() {
		if (queryArea == null) {
			queryArea = new JTextArea("");
			queryArea.setToolTipText("The query sent to the service(s).");
			queryArea.setLineWrap(true);
		}
		return queryArea;
	}

	/**
	 * Get the GUI element of the spectral_range field. If it doesn't exist, create it.
	 *
	 * @return The spectral_range field.
	 */
	public FloatRangeField getSpectralRangeField() {
		if (spectralRangeField == null) {
			spectralRangeField = new FloatRangeField(listener, "spectral_range_");
		}

		return spectralRangeField;
	}

	/**
	 * Get the field where the user enter the target name. If it doesn't exist, create it.
	 *
	 * @return The TargetNameField object of the field.
	 */
	public TargetNameField getTargetNameField() {
		if (targetNameField == null) {
			targetNameField = new TargetNameField(listener, "target_name");
		}

		return targetNameField;
	}

	/**
	 * Get the GUI element of the time range field. If it doesn't exist, create it.
	 *
	 * @return The time range field.
	 */
	public DateRangeField getTimeRangeField() {
		if (timeRangeField == null) {
			timeRangeField = new DateRangeField(listener, "time_");
		}

		return timeRangeField;
	}

	/**
	 * Get the button to save the VOTable in the specified location. If it doesn't exist, create it.
	 *
	 * @return The JButton object.
	 */
	public JButton saveFileButton() {
		if (buttonSave == null) {
			buttonSave = new JButton("Save File");
			buttonSave.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent evt) {
					final JFileChooser fc = new JFileChooser();
					fc.showOpenDialog(RequestPanelView.this);
					listener.onSaveButtonClicked(fc.getSelectedFile());
				}
			});
		}
		return buttonSave;
	}

	/**
	 * Update the query in the JTextArea.
	 *
	 * @param query The string literal to put in the text-area, which will override the old content.
	 */
	public void updateQueryArea(String query) {
		queryArea.setText(query);
	}

	/**
	 * Add GUI elements to the request panel.
	 */
	private void buildRequestPanel() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		JPanel queryPanel = new JPanel();
		queryPanel.setBorder(BorderFactory.createTitledBorder("Query for the selected service(s)"));
		queryPanel.setLayout(new BorderLayout());
		queryPanel.add(getQueryArea(), BorderLayout.CENTER);

		JPanel paramPanel = new JPanel();
		paramPanel.setLayout(new BoxLayout(paramPanel, BoxLayout.Y_AXIS));
		paramPanel.setBorder(BorderFactory.createTitledBorder("Query parameters"));
		for (ParamField field : getParamFields()) {
			paramPanel.add(field);
		}

		JPanel buttonPanel = new JPanel();
		buttonPanel.add(getButtonSend());
		buttonPanel.setMaximumSize(new Dimension(1000, BUTTON_PANEL_HEIGHT));

		this.add(paramPanel, this);
		this.add(queryPanel, this);
		this.add(buttonPanel, this);
	}
}
