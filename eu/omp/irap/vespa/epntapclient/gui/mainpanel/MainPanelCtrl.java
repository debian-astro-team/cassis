/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 * This file is a part of EpnTAPClient.
 * This program aims to provide EPN-TAP support for software clients, like CASSIS spectrum analyzer.
 * See draft specifications: https://voparis-confluence.obspm.fr/pages/viewpage.action?pageId=559861
 * Copyright (C) 2016 Institut de Recherche en Astrophysique et Planétologie.
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package eu.omp.irap.vespa.epntapclient.gui.mainpanel;

import java.awt.Cursor;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import eu.omp.irap.vespa.epntapclient.epntap.EpnTapController;
import eu.omp.irap.vespa.epntapclient.epntap.service.ServicesList;
import eu.omp.irap.vespa.epntapclient.gui.requestpanel.RequestPanelCtrl;
import eu.omp.irap.vespa.epntapclient.gui.resultpanel.ResultsPanelCtrl;
import eu.omp.irap.vespa.epntapclient.gui.servicespanel.ServicesPanelCtrl;

/**
 * @author N. Jourdane
 */
public class MainPanelCtrl extends EpnTapController implements MainPanelListener {

	/** The logger for the class MainPanelCtrl. */
	private static final Logger LOGGER = Logger.getLogger(MainPanelCtrl.class.getName());

	/** The controller of the request panel. */
	private RequestPanelCtrl requestPanelCtrl;

	/** The controller of the result panel. */
	private ResultsPanelCtrl resultsPanelCtrl;

	/** The controller of the services panel. */
	private ServicesPanelCtrl servicesPanelCtrl;

	/** The main view of the application. */
	private MainPanelView view;


	/**
	 * Constructor of MainPanelCtrl.
	 */
	public MainPanelCtrl() {
		servicesPanelCtrl = new ServicesPanelCtrl(this);
		requestPanelCtrl = new RequestPanelCtrl(this);
		resultsPanelCtrl = new ResultsPanelCtrl(this);
		view = new MainPanelView(this);
	}

	@Override
	public void acquireServices() {
		servicesPanelCtrl.acquire();
	}

	@Override
	public void displayError(String message, Exception error) {
		LOGGER.log(Level.SEVERE, message, error);
		JOptionPane.showMessageDialog(view, error.getMessage(), message, JOptionPane.ERROR_MESSAGE);
	}

	@Override
	public void displayInfo(String shortMessage, String detailledMessage) {
		if (detailledMessage == null) {
			LOGGER.info(shortMessage);
		} else {
			LOGGER.info(shortMessage + ": " + detailledMessage);
		}
		view.getStatusBarPanelView().setStatusBarText(shortMessage);
	}

	/** @return The controller of the request panel. */
	@Override
	public RequestPanelCtrl getRequestCtrl() {
		return requestPanelCtrl;
	}

	/** @return The controller of the result panel. */
	public ResultsPanelCtrl getResultsCtrl() {
		return resultsPanelCtrl;
	}

	/**
	 * @return The controller of the service panel.
	 */
	@Override
	public ServicesPanelCtrl getServicesCtrl() {
		return servicesPanelCtrl;
	}

	/**
	 * @return The main view of the application.
	 */
	public MainPanelView getView() {
		return view;
	}

	@Override
	public void saveCurrentVOTable(File file) {
		try {
			Files.copy(Paths.get(resultsPanelCtrl.getFocusedVOTablePath()),
					Paths.get(file.getAbsolutePath()));
		} catch (IOException e) {
			displayError("Can not save the VOTable file.", e);
		}
	}

	@Override
	public void sendQueries() {
		resultsPanelCtrl.getView().clearResult();
		ServicesList services = getServicesCtrl().getServices();
		List<String> queries = new ArrayList<>();
		for (String tableName : services.getTableNames()) {
			queries.add(requestPanelCtrl.getQuery(tableName));
		}
		resultsPanelCtrl.sendQueries(services, queries);
	}

	/* @see MainPanelListener */
	@Override
	public void setWaitMode(boolean enableWaitMode) {
		int cursor = enableWaitMode ? Cursor.WAIT_CURSOR : Cursor.DEFAULT_CURSOR;
		view.setCursor(Cursor.getPredefinedCursor(cursor));
	}

	@Override
	public void updateQuery() {
		requestPanelCtrl.updateQuery();
	}
}
