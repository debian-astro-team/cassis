/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 * This file is a part of EpnTAPClient.
 * This program aims to provide EPN-TAP support for software clients, like CASSIS spectrum analyzer.
 * See draft specifications: https://voparis-confluence.obspm.fr/pages/viewpage.action?pageId=559861
 * Copyright (C) 2016 Institut de Recherche en Astrophysique et Planétologie.
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package eu.omp.irap.vespa.epntapclient.gui.mainpanel;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import eu.omp.irap.vespa.epntapclient.gui.requestpanel.RequestPanelView;
import eu.omp.irap.vespa.epntapclient.gui.resultpanel.ResultsPanelView;
import eu.omp.irap.vespa.epntapclient.gui.servicespanel.ServicesPanelView;
import eu.omp.irap.vespa.epntapclient.gui.statusbarpanel.StatusBarPanelView;

/**
 * The main view of the application, which manage all the other views.
 *
 * @author N. Jourdane
 */
public class MainPanelView extends JPanel {

	/** The default serial version UID. */
	private static final long serialVersionUID = 1L;

	/** The JPanel where the user build the query. */
	private RequestPanelView requestPanel;

	/** The JPanel where the VOTable results is displayed. */
	private ResultsPanelView resultPanel;

	/** The JPanel where the list of services is displayed. */
	private ServicesPanelView servicesPanel;

	/** The JPanel where the list of services is displayed. */
	private StatusBarPanelView statusBarPanel;


	/**
	 * The main view constructor, which create all the panels.
	 *
	 * @param mainPanelCtrl The controller of the main panel.
	 */

	public MainPanelView(MainPanelCtrl mainPanelCtrl) {
		servicesPanel = mainPanelCtrl.getServicesCtrl().getView();
		resultPanel = mainPanelCtrl.getResultsCtrl().getView();
		requestPanel = mainPanelCtrl.getRequestCtrl().getView();
		statusBarPanel = new StatusBarPanelView();
		buildMainView();
	}

	/**
	 * Display an error message. Usually used each time an error happens.
	 *
	 * @param title The title of the error.
	 * @param message The message of the error.
	 */
	public void displayError(String title, String message) {
		JOptionPane.showMessageDialog(this, message, title, JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * @return The JPanel containing the GUI elements to build the query.
	 */
	public RequestPanelView getRequestPanel() {
		return requestPanel;
	}

	/**
	 * @return The JPanel where the VOTable result is displayed.
	 */
	public ResultsPanelView getResultsPanel() {
		return resultPanel;
	}

	/**
	 * @return The JPanel where the list of services is displayed.
	 */
	public ServicesPanelView getServicesPanel() {
		return servicesPanel;
	}

	/**
	 * @return The view of the status bar panel.
	 */
	public StatusBarPanelView getStatusBarPanelView() {
		return statusBarPanel;
	}

	/**
	 * Build the panel and add GUI elements on it.
	 */
	private void buildMainView() {
		setLayout(new BorderLayout());

		JSplitPane northPanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, servicesPanel,
				requestPanel);
		JSplitPane mainPanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT, northPanel, resultPanel);
		add(mainPanel, BorderLayout.CENTER);
		add(statusBarPanel, BorderLayout.SOUTH);

		setSizes();
		revalidate();
	}

	/**
	 * Set the preferred and minimum size of panels.
	 */
	private void setSizes() {
		servicesPanel.setPreferredSize(
				new Dimension(GUIDim.LEFT_PANEL_WIDTH, GUIDim.TOP_PANEL_HEIGHT));
		servicesPanel.setMinimumSize(
				new Dimension(GUIDim.LEFT_PANEL_MIN_WIDTH, GUIDim.TOP_PANEL_MIN_HEIGHT));

		requestPanel.setPreferredSize(
				new Dimension(GUIDim.RIGHT_PANEL_WIDTH, GUIDim.TOP_PANEL_HEIGHT));
		requestPanel.setMinimumSize(
				new Dimension(GUIDim.RIGHT_PANEL_MIN_WIDTH, GUIDim.TOP_PANEL_MIN_HEIGHT));

		resultPanel.setPreferredSize(
				new Dimension(GUIDim.LEFT_PANEL_MIN_WIDTH + GUIDim.RIGHT_PANEL_MIN_WIDTH,
						GUIDim.BOTTOM_PANEL_HEIGHT));
		resultPanel.setMinimumSize(
				new Dimension(GUIDim.LEFT_PANEL_MIN_WIDTH + GUIDim.RIGHT_PANEL_MIN_WIDTH,
						GUIDim.BOTTOM_PANEL_MIN_HEIGHT));
	}

}
