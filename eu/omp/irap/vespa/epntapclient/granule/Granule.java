/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 * This file is a part of EpnTAPClient.
 * This program aims to provide EPN-TAP support for software clients, like CASSIS spectrum analyzer.
 * See draft specifications: https://voparis-confluence.obspm.fr/pages/viewpage.action?pageId=559861
 * Copyright (C) 2016 Institut de Recherche en Astrophysique et Planétologie.
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package eu.omp.irap.vespa.epntapclient.granule;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author N. Jourdane
 */
public class Granule {

	/** Estimate file size in kbyte (with this spelling) */
	private int accessEstsize;

	/**  */
	private String accessFormat;

	/** MD5 Hash for the file when available (real file) */
	private String accessMd5;

	/**  */
	private String accessUrl;

	/** Provides alternative target name if more common (e.g. comets) */
	private String altTargetName;

	/** Bibcode, doi, or other biblio id, URL */
	private String bibReference;

	/** Max of first coordinate. */
	private Double c1Max;

	/** Min of first coordinate. */
	private Double c1Min;

	/** Max resolution in first coordinate. */
	private Double c1ResolMax;

	/** Min resolution in first coordinate. */
	private Double c1ResolMin;

	/** Max of second coordinate. */
	private Double c2Max;

	/** Min of second coordinate. */
	private Double c2Min;

	/** Max resolution in second coordinate. */
	private Double c2ResolMax;

	/** Min resolution in second coordinate. */
	private Double c2ResolMin;

	/** Max of third coordinate. */
	private Double c3Max;

	/** Min of third coordinate. */
	private Double c3Min;

	/** Max resolution in third coordinate. */
	private Double c3ResolMax;

	/** Min resolution in third coordinate. */
	private Double c3ResolMin;

	/** Date of first entry of this granule */
	private Date creationDate;

	/**
	 * If access_format indicates a detached label, this parameter is mandatory and points to the
	 * corresponding data file - both will be handled by the client before samping it to tools or
	 * downloading
	 */
	private String dataAccessUrl;

	/** Organization of the data product, from enumerated list. */
	private String dataproductType;

	/** Declination */
	private double dec;

	/** Max emergence angle. */
	private Double emergenceMax;

	/** Min emergence angle. */
	private Double emergenceMin;

	/**  */
	private String featureName;

	/** Name of the data file only, case sensitive */
	private String fileName;

	/**
	 * Common to granules of same type (e.g. same map projection, or geometry data products). Can be
	 * alphanum.
	 */
	private String granuleGid;

	/** Internal table row index. Unique ID in data service, also in v2. Can be alphanum. */
	private String granuleUid;

	/** Max incidence angle (solar zenithal angle). */
	private Double incidenceMax;

	/** Min incidence angle (solar zenithal angle). */
	private Double incidenceMin;

	/** Standard name of the observatory or spacecraft. */
	private String instrumentHostName;

	/** Standard name of instrument */
	private String instrumentName;

	/** Local time at observed region */
	private double localTimeMax;

	/** Local time at observed region */
	private double localTimeMin;

	/** UCD(s) defining the data */
	private String measurementType;

	/** Date of last modification (used to handle mirroring) */
	private Date modificationDate;

	/**
	 * Associates granules derived from the same data (e.g. various representations / processing
	 * levels). Can be alphanum., may be the ID of original observation.
	 */
	private String obsId;

	/**  */
	private double particleSpectralRangeMax;

	/**  */
	private double particleSpectralRangeMin;

	/**  */
	private double particleSpectralResolutionMax;

	/**  */
	private double particleSpectralResolutionMin;

	/**  */
	private double particleSpectralSamplingStepMax;

	/**  */
	private double particleSpectralSamplingStepMin;

	/**  */
	private String particleSpectralType;

	/** Max phase angle. */
	private Double phaseMax;

	/** Min phase angle. */
	private Double phaseMin;

	/** CODMAC calibration level in v1 */
	private Integer processingLevel;

	/** Resource publisher */
	private String publisher;

	/**  */
	private double ra;

	/**  */
	private Date releaseDate;

	/**  */
	private String serviceTitle;

	/** Max Solar longitude Ls (location on orbit / season) */
	private double solarLongitudeMax;

	/** Min Solar longitude Ls (location on orbit / season) */
	private double solarLongitudeMin;

	/** ID of specific coordinate system and version */
	private String spatialCoordinateDescription;

	/** Flavor of coordinate system, defines the nature of coordinates. From enumerated list. */
	private String spatialFrameType;

	/** Defines the frame origin */
	private String spatialOrigin;

	/** Identifies a chemical species, case sensitive */
	private String species;

	/** Max spectral range (frequency). */
	private Double spectralRangeMax;

	/** Min spectral range (frequency). */
	private Double spectralRangeMin;

	/** Max spectral resolution. */
	private Double spectralResolutionMax;

	/** Min spectral resolution. */
	private Double spectralResolutionMin;

	/** Max spectral sampling step. */
	private Double spectralSamplingStepMax;

	/** Min spectral sampling step. */
	private Double spectralSamplingStepMin;

	/** ObsCore-like footprint, assume spatial_coordinate_description. */
	private String sRegion;

	/** Type of target, from enumerated list. */
	private String targetClass;

	/** Observer-target distance */
	private double targetDistanceMax;

	/** Observer-target distance */
	private double targetDistanceMin;

	/** Standard IAU name of target (from a list related to target class), case sensitive. */
	private String targetName;

	/**  */
	private String targetRegion;

	/**  */
	private double targetTimeMax;

	/**  */
	private double targetTimeMin;

	/** URL of a thumbnail image with predefined size (png ~200 pix, for use in a client only) */
	private String thumbnailUrl;

	/** Max integration time. */
	private Double timeExpMax;

	/** Min integration time. */
	private Double timeExpMin;

	/**
	 * Acquisition stop time (in JD). UTC measured at time_origin location (default is observer's
	 * frame).
	 */
	private Double timeMax;

	/**
	 * Acquisition start time (in JD). UTC measured at time_origin location (default is observer's
	 * frame)
	 */
	private Double timeMin;

	/**  */
	private String timeOrigin;

	/** Max time sampling step. */
	private Double timeSamplingStepMax;

	/** Min time sampling step. */
	private Double timeSamplingStepMin;

	/**  */
	private String timeScale;


	/**
	 * Constructor of Granule
	 *
	 * @param granuleUid The granule identifier.
	 */
	public Granule(String granuleUid) {
		this.granuleUid = granuleUid;
	}

	/**
	 * Convert the Granule to a Map, as <Granule name, Granule value>.
	 *
	 * @return the Granule as a map.
	 */
	public Map<String, Object> asMap() {
		Map<String, Object> map = new HashMap<>();
		map.put(GranuleEnum.GRANULE_UID.toString(), granuleUid);
		map.put(GranuleEnum.GRANULE_GID.toString(), granuleGid);
		map.put(GranuleEnum.OBS_ID.toString(), obsId);
		map.put(GranuleEnum.DATAPRODUCT_TYPE.toString(), dataproductType);
		map.put(GranuleEnum.TARGET_NAME.toString(), targetName);
		map.put(GranuleEnum.TARGET_CLASS.toString(), targetClass);
		map.put(GranuleEnum.TIME_MIN.toString(), timeMin);
		map.put(GranuleEnum.TIME_MAX.toString(), timeMax);
		map.put(GranuleEnum.TIME_SAMPLING_STEP_MIN.toString(), timeSamplingStepMin);
		map.put(GranuleEnum.TIME_SAMPLING_STEP_MAX.toString(), timeSamplingStepMax);
		map.put(GranuleEnum.TIME_EXP_MIN.toString(), timeExpMin);
		map.put(GranuleEnum.TIME_EXP_MAX.toString(), timeExpMax);
		map.put(GranuleEnum.SPECTRAL_RANGE_MIN.toString(), spectralRangeMin);
		map.put(GranuleEnum.SPECTRAL_RANGE_MAX.toString(), spectralRangeMax);
		map.put(GranuleEnum.TIME_SAMPLING_STEP_MIN.toString(), timeSamplingStepMin);
		map.put(GranuleEnum.TIME_SAMPLING_STEP_MAX.toString(), timeSamplingStepMax);
		map.put(GranuleEnum.SPECTRAL_RESOLUTION_MIN.toString(), spectralResolutionMin);
		map.put(GranuleEnum.SPECTRAL_RESOLUTION_MAX.toString(), spectralResolutionMax);
		map.put(GranuleEnum.C1MIN.toString(), c1Min);
		map.put(GranuleEnum.C1MAX.toString(), c1Max);
		map.put(GranuleEnum.C2MIN.toString(), c2Min);
		map.put(GranuleEnum.C2MAX.toString(), c2Max);
		map.put(GranuleEnum.C3MIN.toString(), c3Min);
		map.put(GranuleEnum.C3MAX.toString(), c3Max);
		map.put(GranuleEnum.S_REGION.toString(), sRegion);
		map.put(GranuleEnum.C1_RESOL_MIN.toString(), c1ResolMin);
		map.put(GranuleEnum.C1_RESOL_MAX.toString(), c1ResolMax);
		map.put(GranuleEnum.C2_RESOL_MIN.toString(), c2ResolMin);
		map.put(GranuleEnum.C2_RESOL_MAX.toString(), c2ResolMax);
		map.put(GranuleEnum.C3_RESOL_MIN.toString(), c3ResolMin);
		map.put(GranuleEnum.C3_RESOL_MAX.toString(), c3ResolMax);
		map.put(GranuleEnum.SPATIAL_FRAME_TYPE.toString(), spatialFrameType);
		map.put(GranuleEnum.INCIDENCE_MIN.toString(), incidenceMin);
		map.put(GranuleEnum.INCIDENCE_MAX.toString(), incidenceMax);
		map.put(GranuleEnum.EMERGENCE_MIN.toString(), emergenceMin);
		map.put(GranuleEnum.EMERGENCE_MAX.toString(), emergenceMax);
		map.put(GranuleEnum.PHASE_MIN.toString(), phaseMin);
		map.put(GranuleEnum.PHASE_MAX.toString(), phaseMax);
		map.put(GranuleEnum.INSTRUMENT_HOST_NAME.toString(), instrumentHostName);
		map.put(GranuleEnum.INSTRUMENT_NAME.toString(), instrumentName);
		map.put(GranuleEnum.MEASUREMENT_TYPE.toString(), measurementType);
		map.put(GranuleEnum.PROCESSING_LEVEL.toString(), processingLevel);
		map.put(GranuleEnum.CREATION_DATE.toString(), creationDate);
		map.put(GranuleEnum.MODIFICATION_DATE.toString(), modificationDate);
		map.put(GranuleEnum.RELEASE_DATE.toString(), releaseDate);
		map.put(GranuleEnum.SERVICE_TITLE.toString(), serviceTitle);
		return map;
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}

		if (!(that instanceof Granule)) {
			return false;
		}

		Granule thatGranule = (Granule) that;
		for (Map.Entry<String, Object> parameter : asMap().entrySet()) {
			Object val1 = parameter.getValue();
			Object val2 = thatGranule.asMap().get(parameter.getKey());
			if (!val1.equals(val2)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @return the accessEstsize
	 */
	public int getAccessEstsize() {
		return accessEstsize;
	}

	/**
	 * @return the accessFormat
	 */
	public String getAccessFormat() {
		return accessFormat;
	}

	/**
	 * @return the accessMd5
	 */
	public String getAccessMd5() {
		return accessMd5;
	}

	/**
	 * @return the accessUrl
	 */
	public String getAccessUrl() {
		return accessUrl;
	}

	/**
	 * @return the altTargetName
	 */
	public String getAltTargetName() {
		return altTargetName;
	}

	/**
	 * @return the bibReference
	 */
	public String getBibReference() {
		return bibReference;
	}

	/**
	 * @return the c1Max
	 */
	public Double getC1Max() {
		return c1Max;
	}

	/**
	 * @return the c1Min
	 */
	public Double getC1Min() {
		return c1Min;
	}

	/**
	 * @return the c1ResolMax
	 */
	public Double getC1ResolMax() {
		return c1ResolMax;
	}

	/**
	 * @return the c1ResolMin
	 */
	public Double getC1ResolMin() {
		return c1ResolMin;
	}

	/**
	 * @return the c2Max
	 */
	public Double getC2Max() {
		return c2Max;
	}

	/**
	 * @return the c2Min
	 */
	public Double getC2Min() {
		return c2Min;
	}

	/**
	 * @return the c2ResolMax
	 */
	public Double getC2ResolMax() {
		return c2ResolMax;
	}

	/**
	 * @return the c2ResolMin
	 */
	public Double getC2ResolMin() {
		return c2ResolMin;
	}

	/**
	 * @return the c3Max
	 */
	public Double getC3Max() {
		return c3Max;
	}

	/**
	 * @return the c3Min
	 */
	public Double getC3Min() {
		return c3Min;
	}

	/**
	 * @return the c3ResolMax
	 */
	public Double getC3ResolMax() {
		return c3ResolMax;
	}

	/**
	 * @return the c3ResolMin
	 */
	public Double getC3ResolMin() {
		return c3ResolMin;
	}

	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * @return the dataAccessUrl
	 */
	public String getDataAccessUrl() {
		return dataAccessUrl;
	}

	/**
	 * @return the dataproductType
	 */
	public String getDataproductType() {
		return dataproductType;
	}

	/**
	 * @return the dec
	 */
	public double getDec() {
		return dec;
	}

	/**
	 * @return the emergenceMax
	 */
	public Double getEmergenceMax() {
		return emergenceMax;
	}

	/**
	 * @return the emergenceMin
	 */
	public Double getEmergenceMin() {
		return emergenceMin;
	}

	/**
	 * @return the featureName
	 */
	public String getFeatureName() {
		return featureName;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @return the granuleGid
	 */
	public String getGranuleGid() {
		return granuleGid;
	}

	/**
	 * @return the granuleUid
	 */
	public String getGranuleUid() {
		return granuleUid;
	}

	/**
	 * @return the incidenceMax
	 */
	public Double getIncidenceMax() {
		return incidenceMax;
	}

	/**
	 * @return the incidenceMin
	 */
	public Double getIncidenceMin() {
		return incidenceMin;
	}

	/**
	 * @return the instrumentHostName
	 */
	public String getInstrumentHostName() {
		return instrumentHostName;
	}

	/**
	 * @return the instrumentName
	 */
	public String getInstrumentName() {
		return instrumentName;
	}

	/**
	 * @return the localTimeMax
	 */
	public double getLocalTimeMax() {
		return localTimeMax;
	}

	/**
	 * @return the localTimeMin
	 */
	public double getLocalTimeMin() {
		return localTimeMin;
	}

	/**
	 * @return the measurementType
	 */
	public String getMeasurementType() {
		return measurementType;
	}

	/**
	 * @return the modificationDate
	 */
	public Date getModificationDate() {
		return modificationDate;
	}

	/**
	 * @return the obsId
	 */
	public String getObsId() {
		return obsId;
	}

	/**
	 * @return the particleSpectralRangeMax
	 */
	public double getParticleSpectralRangeMax() {
		return particleSpectralRangeMax;
	}

	/**
	 * @return the particleSpectralRangeMin
	 */
	public double getParticleSpectralRangeMin() {
		return particleSpectralRangeMin;
	}

	/**
	 * @return the particleSpectralResolutionMax
	 */
	public double getParticleSpectralResolutionMax() {
		return particleSpectralResolutionMax;
	}

	/**
	 * @return the particleSpectralResolutionMin
	 */
	public double getParticleSpectralResolutionMin() {
		return particleSpectralResolutionMin;
	}

	/**
	 * @return the particleSpectralSamplingStepMax
	 */
	public double getParticleSpectralSamplingStepMax() {
		return particleSpectralSamplingStepMax;
	}

	/**
	 * @return the particleSpectralSamplingStepMin
	 */
	public double getParticleSpectralSamplingStepMin() {
		return particleSpectralSamplingStepMin;
	}

	/**
	 * @return the particleSpectralType
	 */
	public String getParticleSpectralType() {
		return particleSpectralType;
	}

	/**
	 * @return the phaseMax
	 */
	public Double getPhaseMax() {
		return phaseMax;
	}

	/**
	 * @return the phaseMin
	 */
	public Double getPhaseMin() {
		return phaseMin;
	}

	/**
	 * @return the processingLevel
	 */
	public Integer getProcessingLevel() {
		return processingLevel;
	}

	/**
	 * @return the publisher
	 */
	public String getPublisher() {
		return publisher;
	}

	/**
	 * @return the ra
	 */
	public double getRa() {
		return ra;
	}

	/**
	 * @return the releaseDate
	 */
	public Date getReleaseDate() {
		return releaseDate;
	}

	/**
	 * @return the serviceTitle
	 */
	public String getServiceTitle() {
		return serviceTitle;
	}

	/**
	 * @return the solarLongitudeMax
	 */
	public double getSolarLongitudeMax() {
		return solarLongitudeMax;
	}

	/**
	 * @return the solarLongitudeMin
	 */
	public double getSolarLongitudeMin() {
		return solarLongitudeMin;
	}

	/**
	 * @return the spatialCoordinateDescription
	 */
	public String getSpatialCoordinateDescription() {
		return spatialCoordinateDescription;
	}

	/**
	 * @return the spatialFrameType
	 */
	public String getSpatialFrameType() {
		return spatialFrameType;
	}

	/**
	 * @return the spatialOrigin
	 */
	public String getSpatialOrigin() {
		return spatialOrigin;
	}

	/**
	 * @return the species
	 */
	public String getSpecies() {
		return species;
	}

	/**
	 * @return the spectralRangeMax
	 */
	public Double getSpectralRangeMax() {
		return spectralRangeMax;
	}

	/**
	 * @return the spectralRangeMin
	 */
	public Double getSpectralRangeMin() {
		return spectralRangeMin;
	}

	/**
	 * @return the spectralResolutionMax
	 */
	public Double getSpectralResolutionMax() {
		return spectralResolutionMax;
	}

	/**
	 * @return the spectralResolutionMin
	 */
	public Double getSpectralResolutionMin() {
		return spectralResolutionMin;
	}

	/**
	 * @return the spectralSamplingStepMax
	 */
	public Double getSpectralSamplingStepMax() {
		return spectralSamplingStepMax;
	}

	/**
	 * @return the spectralSamplingStepMin
	 */
	public Double getSpectralSamplingStepMin() {
		return spectralSamplingStepMin;
	}

	/**
	 * @return the sRegion
	 */
	public String getsRegion() {
		return sRegion;
	}

	/**
	 * @return the targetClass
	 */
	public String getTargetClass() {
		return targetClass;
	}

	/**
	 * @return the targetDistanceMax
	 */
	public double getTargetDistanceMax() {
		return targetDistanceMax;
	}

	/**
	 * @return the targetDistanceMin
	 */
	public double getTargetDistanceMin() {
		return targetDistanceMin;
	}

	/**
	 * @return the targetName
	 */
	public String getTargetName() {
		return targetName;
	}

	/**
	 * @return the targetRegion
	 */
	public String getTargetRegion() {
		return targetRegion;
	}

	/**
	 * @return the targetTimeMax
	 */
	public double getTargetTimeMax() {
		return targetTimeMax;
	}

	/**
	 * @return the targetTimeMin
	 */
	public double getTargetTimeMin() {
		return targetTimeMin;
	}

	/**
	 * @return the thumbnailUrl
	 */
	public String getThumbnailUrl() {
		return thumbnailUrl;
	}

	/**
	 * @return the timeExpMax
	 */
	public Double getTimeExpMax() {
		return timeExpMax;
	}

	/**
	 * @return the timeExpMin
	 */
	public Double getTimeExpMin() {
		return timeExpMin;
	}

	/**
	 * @return the timeMax
	 */
	public Double getTimeMax() {
		return timeMax;
	}

	/**
	 * @return the timeMin
	 */
	public Double getTimeMin() {
		return timeMin;
	}

	/**
	 * @return the timeOrigin
	 */
	public String getTimeOrigin() {
		return timeOrigin;
	}

	/**
	 * @return the timeSamplingStepMax
	 */
	public Double getTimeSamplingStepMax() {
		return timeSamplingStepMax;
	}

	/**
	 * @return the timeSamplingStepMin
	 */
	public Double getTimeSamplingStepMin() {
		return timeSamplingStepMin;
	}

	/**
	 * @return the timeScale
	 */
	public String getTimeScale() {
		return timeScale;
	}

	@Override
	public int hashCode() {
		final int salt = 31;
		int hash = 1;
		for (Map.Entry<String, Object> parameter : asMap().entrySet()) {
			hash = salt * hash
					+ (parameter.getValue() == null ? 0 : parameter.getValue().hashCode());
		}
		return hash;
	}

	/**
	 * A granule is valid if all mandatory parameters are filled.
	 *
	 * @return true if the Granule is valid, false otherwise.
	 */
	public boolean isValid() {
		for (Map.Entry<String, Object> parameter : asMap().entrySet()) {
			if (parameter.getValue() == null) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @return a string containing all the parameters of the Granule, as strings (key: value). Used
	 *         to be printed for debug purposes.
	 */
	public String printAll() {
		String s = "";
		for (Map.Entry<String, Object> parameter : asMap().entrySet()) {
			s += parameter.getKey() + ": " + parameter.getValue() + "\n";
		}
		return s;
	}

	/**
	 * @param accessEstsize the accessEstsize to set
	 */
	public void setAccessEstsize(int accessEstsize) {
		this.accessEstsize = accessEstsize;
	}

	/**
	 * @param accessFormat the accessFormat to set
	 */
	public void setAccessFormat(String accessFormat) {
		this.accessFormat = accessFormat;
	}

	/**
	 * @param accessMd5 the accessMd5 to set
	 */
	public void setAccessMd5(String accessMd5) {
		this.accessMd5 = accessMd5;
	}

	/**
	 * @param accessUrl the accessUrl to set
	 */
	public void setAccessUrl(String accessUrl) {
		this.accessUrl = accessUrl;
	}

	/**
	 * @param altTargetName the altTargetName to set
	 */
	public void setAltTargetName(String altTargetName) {
		this.altTargetName = altTargetName;
	}

	/**
	 * @param bibReference the bibReference to set
	 */
	public void setBibReference(String bibReference) {
		this.bibReference = bibReference;
	}

	/**
	 * @param c1Max the c1Max to set
	 */
	public void setC1Max(Double c1Max) {
		this.c1Max = c1Max;
	}

	/**
	 * @param c1Min the c1Min to set
	 */
	public void setC1Min(Double c1Min) {
		this.c1Min = c1Min;
	}

	/**
	 * @param c1ResolMax the c1ResolMax to set
	 */
	public void setC1ResolMax(Double c1ResolMax) {
		this.c1ResolMax = c1ResolMax;
	}

	/**
	 * @param c1ResolMin the c1ResolMin to set
	 */
	public void setC1ResolMin(Double c1ResolMin) {
		this.c1ResolMin = c1ResolMin;
	}

	/**
	 * @param c2Max the c2Max to set
	 */
	public void setC2Max(Double c2Max) {
		this.c2Max = c2Max;
	}

	/**
	 * @param c2Min the c2Min to set
	 */
	public void setC2Min(Double c2Min) {
		this.c2Min = c2Min;
	}

	/**
	 * @param c2ResolMax the c2ResolMax to set
	 */
	public void setC2ResolMax(Double c2ResolMax) {
		this.c2ResolMax = c2ResolMax;
	}

	/**
	 * @param c2ResolMin the c2ResolMin to set
	 */
	public void setC2ResolMin(Double c2ResolMin) {
		this.c2ResolMin = c2ResolMin;
	}

	/**
	 * @param c3Max the c3Max to set
	 */
	public void setC3Max(Double c3Max) {
		this.c3Max = c3Max;
	}

	/**
	 * @param c3Min the c3Min to set
	 */
	public void setC3Min(Double c3Min) {
		this.c3Min = c3Min;
	}

	/**
	 * @param c3ResolMax the c3ResolMax to set
	 */
	public void setC3ResolMax(Double c3ResolMax) {
		this.c3ResolMax = c3ResolMax;
	}

	/**
	 * @param c3ResolMin the c3ResolMin to set
	 */
	public void setC3ResolMin(Double c3ResolMin) {
		this.c3ResolMin = c3ResolMin;
	}

	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @param dataAccessUrl the dataAccessUrl to set
	 */
	public void setDataAccessUrl(String dataAccessUrl) {
		this.dataAccessUrl = dataAccessUrl;
	}

	/**
	 * @param dataproductType the dataproductType to set
	 */
	public void setDataproductType(String dataproductType) {
		this.dataproductType = dataproductType;
	}

	/**
	 * @param dec the dec to set
	 */
	public void setDec(double dec) {
		this.dec = dec;
	}

	/**
	 * @param emergenceMax the emergenceMax to set
	 */
	public void setEmergenceMax(Double emergenceMax) {
		this.emergenceMax = emergenceMax;
	}

	/**
	 * @param emergenceMin the emergenceMin to set
	 */
	public void setEmergenceMin(Double emergenceMin) {
		this.emergenceMin = emergenceMin;
	}

	/**
	 * @param featureName the featureName to set
	 */
	public void setFeatureName(String featureName) {
		this.featureName = featureName;
	}

	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @param granuleGid the granuleGid to set
	 */
	public void setGranuleGid(String granuleGid) {
		this.granuleGid = granuleGid;
	}

	/**
	 * @param granuleUid the granuleUid to set
	 */
	public void setGranuleUid(String granuleUid) {
		this.granuleUid = granuleUid;
	}

	/**
	 * @param incidenceMax the incidenceMax to set
	 */
	public void setIncidenceMax(Double incidenceMax) {
		this.incidenceMax = incidenceMax;
	}

	/**
	 * @param incidenceMin the incidenceMin to set
	 */
	public void setIncidenceMin(Double incidenceMin) {
		this.incidenceMin = incidenceMin;
	}

	/**
	 * @param instrumentHostName the instrumentHostName to set
	 */
	public void setInstrumentHostName(String instrumentHostName) {
		this.instrumentHostName = instrumentHostName;
	}

	/**
	 * @param instrumentName the instrumentName to set
	 */
	public void setInstrumentName(String instrumentName) {
		this.instrumentName = instrumentName;
	}

	/**
	 * @param localTimeMax the localTimeMax to set
	 */
	public void setLocalTimeMax(double localTimeMax) {
		this.localTimeMax = localTimeMax;
	}

	/**
	 * @param localTimeMin the localTimeMin to set
	 */
	public void setLocalTimeMin(double localTimeMin) {
		this.localTimeMin = localTimeMin;
	}

	/**
	 * @param measurementType the measurementType to set
	 */
	public void setMeasurementType(String measurementType) {
		this.measurementType = measurementType;
	}

	/**
	 * @param modificationDate the modificationDate to set
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	/**
	 * @param obsId the obsId to set
	 */
	public void setObsId(String obsId) {
		this.obsId = obsId;
	}

	/**
	 * @param particleSpectralRangeMax the particleSpectralRangeMax to set
	 */
	public void setParticleSpectralRangeMax(double particleSpectralRangeMax) {
		this.particleSpectralRangeMax = particleSpectralRangeMax;
	}

	/**
	 * @param particleSpectralRangeMin the particleSpectralRangeMin to set
	 */
	public void setParticleSpectralRangeMin(double particleSpectralRangeMin) {
		this.particleSpectralRangeMin = particleSpectralRangeMin;
	}

	/**
	 * @param particleSpectralResolutionMax the particleSpectralResolutionMax to set
	 */
	public void setParticleSpectralResolutionMax(double particleSpectralResolutionMax) {
		this.particleSpectralResolutionMax = particleSpectralResolutionMax;
	}

	/**
	 * @param particleSpectralResolutionMin the particleSpectralResolutionMin to set
	 */
	public void setParticleSpectralResolutionMin(double particleSpectralResolutionMin) {
		this.particleSpectralResolutionMin = particleSpectralResolutionMin;
	}

	/**
	 * @param particleSpectralSamplingStepMax the particleSpectralSamplingStepMax to set
	 */
	public void setParticleSpectralSamplingStepMax(double particleSpectralSamplingStepMax) {
		this.particleSpectralSamplingStepMax = particleSpectralSamplingStepMax;
	}

	/**
	 * @param particleSpectralSamplingStepMin the particleSpectralSamplingStepMin to set
	 */
	public void setParticleSpectralSamplingStepMin(double particleSpectralSamplingStepMin) {
		this.particleSpectralSamplingStepMin = particleSpectralSamplingStepMin;
	}

	/**
	 * @param particleSpectralType the particleSpectralType to set
	 */
	public void setParticleSpectralType(String particleSpectralType) {
		this.particleSpectralType = particleSpectralType;
	}

	/**
	 * @param phaseMax the phaseMax to set
	 */
	public void setPhaseMax(Double phaseMax) {
		this.phaseMax = phaseMax;
	}

	/**
	 * @param phaseMin the phaseMin to set
	 */
	public void setPhaseMin(Double phaseMin) {
		this.phaseMin = phaseMin;
	}

	/**
	 * @param processingLevel the processingLevel to set
	 */
	public void setProcessingLevel(Integer processingLevel) {
		this.processingLevel = processingLevel;
	}

	/**
	 * @param publisher the publisher to set
	 */
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	/**
	 * @param ra the ra to set
	 */
	public void setRa(double ra) {
		this.ra = ra;
	}

	/**
	 * @param releaseDate the releaseDate to set
	 */
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	/**
	 * @param serviceTitle the serviceTitle to set
	 */
	public void setServiceTitle(String serviceTitle) {
		this.serviceTitle = serviceTitle;
	}

	/**
	 * @param solarLongitudeMax the solarLongitudeMax to set
	 */
	public void setSolarLongitudeMax(double solarLongitudeMax) {
		this.solarLongitudeMax = solarLongitudeMax;
	}

	/**
	 * @param solarLongitudeMin the solarLongitudeMin to set
	 */
	public void setSolarLongitudeMin(double solarLongitudeMin) {
		this.solarLongitudeMin = solarLongitudeMin;
	}

	/**
	 * @param spatialCoordinateDescription the spatialCoordinateDescription to set
	 */
	public void setSpatialCoordinateDescription(String spatialCoordinateDescription) {
		this.spatialCoordinateDescription = spatialCoordinateDescription;
	}

	/**
	 * @param spatialFrameType the spatialFrameType to set
	 */
	public void setSpatialFrameType(String spatialFrameType) {
		this.spatialFrameType = spatialFrameType;
	}

	/**
	 * @param spatialOrigin the spatialOrigin to set
	 */
	public void setSpatialOrigin(String spatialOrigin) {
		this.spatialOrigin = spatialOrigin;
	}

	/**
	 * @param species the species to set
	 */
	public void setSpecies(String species) {
		this.species = species;
	}

	/**
	 * @param spectralRangeMax the spectralRangeMax to set
	 */
	public void setSpectralRangeMax(Double spectralRangeMax) {
		this.spectralRangeMax = spectralRangeMax;
	}

	/**
	 * @param spectralRangeMin the spectralRangeMin to set
	 */
	public void setSpectralRangeMin(Double spectralRangeMin) {
		this.spectralRangeMin = spectralRangeMin;
	}

	/**
	 * @param spectralResolutionMax the spectralResolutionMax to set
	 */
	public void setSpectralResolutionMax(Double spectralResolutionMax) {
		this.spectralResolutionMax = spectralResolutionMax;
	}

	/**
	 * @param spectralResolutionMin the spectralResolutionMin to set
	 */
	public void setSpectralResolutionMin(Double spectralResolutionMin) {
		this.spectralResolutionMin = spectralResolutionMin;
	}

	/**
	 * @param spectralSamplingStepMax the spectralSamplingStepMax to set
	 */
	public void setSpectralSamplingStepMax(Double spectralSamplingStepMax) {
		this.spectralSamplingStepMax = spectralSamplingStepMax;
	}

	/**
	 * @param spectralSamplingStepMin the spectralSamplingStepMin to set
	 */
	public void setSpectralSamplingStepMin(Double spectralSamplingStepMin) {
		this.spectralSamplingStepMin = spectralSamplingStepMin;
	}

	/**
	 * @param sRegion the sRegion to set
	 */
	public void setSRegion(String sRegion) {
		this.sRegion = sRegion;
	}

	/**
	 * @param targetClass the targetClass to set
	 */
	public void setTargetClass(String targetClass) {
		this.targetClass = targetClass;
	}

	/**
	 * @param targetDistanceMax the targetDistanceMax to set
	 */
	public void setTargetDistanceMax(double targetDistanceMax) {
		this.targetDistanceMax = targetDistanceMax;
	}

	/**
	 * @param targetDistanceMin the targetDistanceMin to set
	 */
	public void setTargetDistanceMin(double targetDistanceMin) {
		this.targetDistanceMin = targetDistanceMin;
	}

	/**
	 * @param targetName the targetName to set
	 */
	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}

	/**
	 * @param targetRegion the targetRegion to set
	 */
	public void setTargetRegion(String targetRegion) {
		this.targetRegion = targetRegion;
	}

	/**
	 * @param targetTimeMax the targetTimeMax to set
	 */
	public void setTargetTimeMax(double targetTimeMax) {
		this.targetTimeMax = targetTimeMax;
	}

	/**
	 * @param targetTimeMin the targetTimeMin to set
	 */
	public void setTargetTimeMin(double targetTimeMin) {
		this.targetTimeMin = targetTimeMin;
	}

	/**
	 * @param thumbnailUrl the thumbnailUrl to set
	 */
	public void setThumbnailUrl(String thumbnailUrl) {
		this.thumbnailUrl = thumbnailUrl;
	}

	/**
	 * @param timeExpMax the timeExpMax to set
	 */
	public void setTimeExpMax(Double timeExpMax) {
		this.timeExpMax = timeExpMax;
	}

	/**
	 * @param timeExpMin the timeExpMin to set
	 */
	public void setTimeExpMin(Double timeExpMin) {
		this.timeExpMin = timeExpMin;
	}

	/**
	 * @param timeMax the timeMax to set
	 */
	public void setTimeMax(Double timeMax) {
		this.timeMax = timeMax;
	}

	/**
	 * @param timeMin the timeMin to set
	 */
	public void setTimeMin(Double timeMin) {
		this.timeMin = timeMin;
	}

	/**
	 * @param timeOrigin the timeOrigin to set
	 */
	public void setTimeOrigin(String timeOrigin) {
		this.timeOrigin = timeOrigin;
	}

	/**
	 * @param timeSamplingStepMax the timeSamplingStepMax to set
	 */
	public void setTimeSamplingStepMax(Double timeSamplingStepMax) {
		this.timeSamplingStepMax = timeSamplingStepMax;
	}

	/**
	 * @param timeSamplingStepMin the timeSamplingStepMin to set
	 */
	public void setTimeSamplingStepMin(Double timeSamplingStepMin) {
		this.timeSamplingStepMin = timeSamplingStepMin;
	}

	/**
	 * @param timeScale the timeScale to set
	 */
	public void setTimeScale(String timeScale) {
		this.timeScale = timeScale;
	}

	@Override
	public String toString() {
		return granuleUid;
	}
}
