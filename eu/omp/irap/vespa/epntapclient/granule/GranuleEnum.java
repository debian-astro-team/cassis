/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 * This file is a part of EpnTAPClient.
 * This program aims to provide EPN-TAP support for software clients, like CASSIS spectrum analyzer.
 * See draft specifications: https://voparis-confluence.obspm.fr/pages/viewpage.action?pageId=559861
 * Copyright (C) 2016 Institut de Recherche en Astrophysique et Planétologie.
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package eu.omp.irap.vespa.epntapclient.granule;

import java.util.Date;

/**
 * Enumeration list generated with the Python script in main/resources/scripts.
 *
 * @author N. Jourdane
 */
@SuppressWarnings("javadoc")
public enum GranuleEnum {
	//@noformat

	ACCESS_ESTSIZE("access_estsize", Integer.class, false, false, "kbyte", "", "Estimate file size in kbyte (with this spelling)"),
	ACCESS_FORMAT("access_format", String.class, false, false, "", "meta.code.mime", ""),
	ACCESS_MD5("access_md5", String.class, false, false, "", "", "MD5 Hash for the file when available (real file)"),
	ACCESS_URL("access_url", String.class, false, false, "", "meta.ref.url;meta.file", ""),
	ALT_TARGET_NAME("alt_target_name", String.class, false, false, "", "meta.id;src", "Provides alternative target name if more common (e.g. comets)"),
	BIB_REFERENCE("bib_reference", String.class, false, false, "", "meta.bib", "Bibcode, doi, or other biblio id, URL"),
	C1_RESOL_MAX("c1_resol_max", Double.class, true, false, "", "pos.resolution;stat.max", "Max resolution in first coordinate"),
	C1_RESOL_MIN("c1_resol_min", Double.class, true, false, "", "", "Min resolution in first coordinate"),
	C1MAX("c1max", Double.class, true, false, "", "pos;stat.max", "Max of first coordinate"),
	C1MIN("c1min", Double.class, true, false, "", "pos;stat.min", "Min of first coordinate"),
	C2_RESOL_MAX("c2_resol_max", Double.class, true, false, "", "pos.resolution;stat.max", "Max resolution in second coordinate"),
	C2_RESOL_MIN("c2_resol_min", Double.class, true, false, "", "pos.resolution;stat.min", "Min resolution in second coordinate"),
	C2MAX("c2max", Double.class, true, false, "", "pos;stat.max", "Max of second coordinate"),
	C2MIN("c2min", Double.class, true, false, "", "pos;stat.min", "Min of second coordinate"),
	C3_RESOL_MAX("c3_resol_max", Double.class, true, false, "", "pos.resolution;stat.max", "Max resolution in third coordinate"),
	C3_RESOL_MIN("c3_resol_min", Double.class, true, false, "", "pos.resolution;stat.min", "Min resolution in third coordinate"),
	C3MAX("c3max", Double.class, true, false, "", "pos;stat.max", "Max of third coordinate"),
	C3MIN("c3min", Double.class, true, false, "", "pos;stat.min", "Min of third coordinate"),
	CREATION_DATE("creation_date", Date.class, true, true, "", "time.creation", "Date of first entry of this granule"),
	DATA_ACCESS_URL("data_access_url", String.class, false, false, "", "", "If access_format indicates a detached label, this parameter is mandatory and points to the corresponding data file - both will be handled by the client before samping it to tools or downloading"),
	DATAPRODUCT_TYPE("dataproduct_type", String.class, true, true, "", "meta.code.class", "Organization of the data product, from enumerated list"),
	DEC("dec", Double.class, false, false, "deg", "pos.eq.dec;meta.main", "Declination"),
	EMERGENCE_MAX("emergence_max", Double.class, true, false, "deg", "pos.posAng;stat.max", "Max emergence angle"),
	EMERGENCE_MIN("emergence_min", Double.class, true, false, "deg", "pos.posAng;stat.min", "Min emergence angle"),
	FEATURE_NAME("feature_name", String.class, false, false, "", "meta.id;pos", ""),
	FILE_NAME("file_name", String.class, false, false, "", "meta.id;meta.file", "Name of the data file only, case sensitive"),
	GRANULE_GID("granule_gid", String.class, true, true, "", "meta.id", "Common to granules of same type (e.g. same map projection, or geometry data products). Can be alphanum."),
	GRANULE_UID("granule_uid", String.class, true, true, "", "meta.id", ""),
	INCIDENCE_MAX("incidence_max", Double.class, true, false, "deg", "pos.posAng;stat.max", "Max incidence angle (solar zenithal angle)"),
	INCIDENCE_MIN("incidence_min", Double.class, true, false, "deg", "pos.posAng;stat.min", "Min incidence angle (solar zenithal angle)"),
	INSTRUMENT_HOST_NAME("instrument_host_name", String.class, true, false, "", "", "Standard name of the observatory or spacecraft"),
	INSTRUMENT_NAME("instrument_name", String.class, true, false, "", "meta.id;instr", "Standard name of instrument"),
	LOCAL_TIME_MAX("local_time_max", Double.class, false, false, "h", "time.phase;stat.max?", "Local time at observed region"),
	LOCAL_TIME_MIN("local_time_min", Double.class, false, false, "h", "time.phase;stat.min?", "Local time at observed region"),
	MEASUREMENT_TYPE("measurement_type", String.class, true, false, "", "meta.ucd", "UCD(s) defining the data"),
	MODIFICATION_DATE("modification_date", Date.class, true, true, "", "time.update", "Date of last modification (used to handle mirroring)"),
	OBS_ID("obs_id", String.class, true, true, "", "meta.id", "Associates granules derived from the same data (e.g. various representations / processing levels). Can be alphanum., may be the ID of original observation."),
	PARTICLE_SPECTRAL_RANGE_MAX("particle_spectral_range_max", Double.class, false, false, "", "", ""),
	PARTICLE_SPECTRAL_RANGE_MIN("particle_spectral_range_min", Double.class, false, false, "", "", ""),
	PARTICLE_SPECTRAL_RESOLUTION_MAX("particle_spectral_resolution_max", Double.class, false, false, "", "spect.resolution;stat.max", ""),
	PARTICLE_SPECTRAL_RESOLUTION_MIN("particle_spectral_resolution_min", Double.class, false, false, "", "spect.resolution;stat.min", ""),
	PARTICLE_SPECTRAL_SAMPLING_STEP_MAX("particle_spectral_sampling_step_max", Double.class, false, false, "", "", ""),
	PARTICLE_SPECTRAL_SAMPLING_STEP_MIN("particle_spectral_sampling_step_min", Double.class, false, false, "", "", ""),
	PARTICLE_SPECTRAL_TYPE("particle_spectral_type", String.class, false, false, "", "", ""),
	PHASE_MAX("phase_max", Double.class, true, false, "deg", "pos.phaseAng;stat.max", "Max phase angle"),
	PHASE_MIN("phase_min", Double.class, true, false, "deg", "pos.phaseAng;stat.min", "Min phase angle"),
	PROCESSING_LEVEL("processing_level", Integer.class, true, false, "", "meta.code;obs.calib", "CODMAC calibration level in v1"),
	PUBLISHER("publisher", String.class, false, false, "", "meta.name", "Resource publisher"),
	RA("ra", Double.class, false, false, "deg or h:m:s?", "pos.eq.ra;meta.main", ""),
	RELEASE_DATE("release_date", Date.class, true, true, "", "time.release", ""),
	S_REGION("s_region", String.class, true, false, "", "instr.fov", "ObsCore-like footprint, assume spatial_coordinate_description"),
	SERVICE_TITLE("service_title", String.class, true, true, "", "meta.title", ""),
	SOLAR_LONGITUDE_MAX("solar_longitude_max", Double.class, false, false, "deg", "pos.posangle (TBC)", "Max Solar longitude Ls (location on orbit / season)"),
	SOLAR_LONGITUDE_MIN("solar_longitude_min", Double.class, false, false, "deg", "pos.posangle (TBC)", "Min Solar longitude Ls (location on orbit / season)"),
	SPATIAL_COORDINATE_DESCRIPTION("spatial_coordinate_description", String.class, false, false, "", "", "ID of specific coordinate system and version"),
	SPATIAL_FRAME_TYPE("spatial_frame_type", String.class, true, false, "", "meta.code.class;pos.frame", "Flavor of coordinate system, defines the nature of coordinates. From enumerated list"),
	SPATIAL_ORIGIN("spatial_origin", String.class, false, false, "", "meta.ref;pos.frame", "Defines the frame origin"),
	SPECIES("species", String.class, false, false, "", "meta.id;phys.atmol", "Identifies a chemical species, case sensitive"),
	SPECTRAL_RANGE_MAX("spectral_range_max", Double.class, true, false, "Hz", "em.freq;stat.max", "Max spectral range (frequency)"),
	SPECTRAL_RANGE_MIN("spectral_range_min", Double.class, true, false, "Hz", "em.freq;stat.min", "Min spectral range (frequency)"),
	SPECTRAL_RESOLUTION_MAX("spectral_resolution_max", Double.class, true, false, "Hz", "spect.resolution;stat.max", "Max spectral resolution"),
	SPECTRAL_RESOLUTION_MIN("spectral_resolution_min", Double.class, true, false, "Hz", "spect.resolution;stat.min", ""),
	SPECTRAL_SAMPLING_STEP_MAX("spectral_sampling_step_max", Double.class, true, false, "Hz", "", "Max spectral sampling step"),
	SPECTRAL_SAMPLING_STEP_MIN("spectral_sampling_step_min", Double.class, true, false, "Hz", "", "Min spectral sampling step"),
	TARGET_CLASS("target_class", String.class, true, true, "", "meta.code.class;src", "Type of target, from enumerated list"),
	TARGET_DISTANCE_MAX("target_distance_max", Double.class, false, false, "km", "pos.distance;stat.max", "Observer-target distance"),
	TARGET_DISTANCE_MIN("target_distance_min", Double.class, false, false, "km", "pos.distance;stat.min", "Observer-target distance"),
	TARGET_NAME("target_name", String.class, true, true, "", "meta.id;src", "Standard IAU name of target (from a list related to target class), case sensitive"),
	TARGET_REGION("target_region", String.class, false, false, "", "meta.id;class", ""),
	TARGET_TIME_MAX("target_time_max", Double.class, false, false, "d", "", ""),
	TARGET_TIME_MIN("target_time_min", Double.class, false, false, "d", "", ""),
	THUMBNAIL_URL("thumbnail_url", String.class, false, false, "", "meta.ref.url;meta.file", "URL of a thumbnail image with predefined size (png ~200 pix, for use in a client only)"),
	TIME_EXP_MAX("time_exp_max", Double.class, true, false, "s", "time.duration;obs.exposure;stat.max", "Max integration time"),
	TIME_EXP_MIN("time_exp_min", Double.class, true, false, "s", "", "Min integration time"),
	TIME_MAX("time_max", Double.class, true, false, "d (date as JD)", "time.end", ""),
	TIME_MIN("time_min", Double.class, true, false, "d (date as JD)", "time.start", ""),
	TIME_ORIGIN("time_origin", String.class, false, false, "", "", ""),
	TIME_SAMPLING_STEP_MAX("time_sampling_step_max", Double.class, true, false, "s", "time.interval;stat.max", "Max time sampling step"),
	TIME_SAMPLING_STEP_MIN("time_sampling_step_min", Double.class, true, false, "s", "time.interval;stat.min", "Min time sampling step"),
	TIME_SCALE("time_scale", String.class, false, false, "", "time.scale", "");

	//@format

	/** A short text describing the granule parameter. */
	private String description;

	/** true if the granule parameter is mandatory, false if it is optional. */
	private boolean isMandatory;

	/** true if the granule parameter must be actually filled, false if the value can be null. */
	private boolean mustBeFilled;

	/** The name of the granule parameter. */
	private String name;

	/** The type of the granule parameter. */
	private Class<?> type;

	/** The UCD of the granule parameter. */
	private String ucd;

	/** The unit of the granule parameter (ie. degree, km, etc.). */
	private String unit;


	/**
	 * Contructor of the granule.
	 *
	 * @param name The name of the granule parameter.
	 * @param type The type of the granule parameter.
	 * @param isMandatory true if the granule parameter is mandatory, false if it is optional.
	 * @param mustBeFilled true if the granule parameter must be actually filled.
	 * @param unit The unit of the granule parameter (ie. degree, km, etc.).
	 * @param ucd The UCD of the granule parameter.
	 * @param description A short text describing the granule parameter.
	 */
	GranuleEnum(String name, Class<?> type, boolean isMandatory, boolean mustBeFilled, String unit,
			String ucd, String description) {
		this.name = name;
		this.type = type;
		this.isMandatory = isMandatory;
		this.mustBeFilled = mustBeFilled;
		this.unit = unit;
		this.ucd = ucd;
		this.description = description;
	}

	/**
	 * @return A short text describing the granule parameter.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return The name of the granule parameter.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return The type of the granule parameter.
	 */
	public Class<?> getType() {
		return type;
	}

	/**
	 * @return The UCD of the granule parameter.
	 */
	public String getUcd() {
		return ucd;
	}

	/**
	 * @return The unit of the granule parameter (ie. degree, km, etc.).
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * @return true if the granule parameter is mandatory, false if it is optional.
	 */
	public boolean isMandatory() {
		return isMandatory;
	}

	/**
	 * @return true if the granule parameter must be actually filled false if the value can be null.
	 */
	public boolean isMustBeFilled() {
		return mustBeFilled;
	}

	@Override
	public String toString() {
		return name;
	}

}
