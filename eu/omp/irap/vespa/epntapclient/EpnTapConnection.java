/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 * This file is a part of EpnTAPClient.
 * This program aims to provide EPN-TAP support for software clients, like CASSIS spectrum analyzer.
 * See draft specifications: https://voparis-confluence.obspm.fr/pages/viewpage.action?pageId=559861
 * Copyright (C) 2016 Institut de Recherche en Astrophysique et Planétologie.
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package eu.omp.irap.vespa.epntapclient;

import java.util.ArrayList;
import java.util.List;

import eu.omp.irap.vespa.epntapclient.epntap.EpnTapInterface;
import eu.omp.irap.vespa.epntapclient.epntap.service.Queries;
import eu.omp.irap.vespa.epntapclient.epntap.service.ServiceCore;
import eu.omp.irap.vespa.epntapclient.epntap.service.ServiceCtrl;
import eu.omp.irap.vespa.epntapclient.granule.Granule;
import eu.omp.irap.vespa.epntapclient.granule.GranuleCtrl;
import eu.omp.irap.vespa.epntapclient.voresource.VOResourceCtrl;
import eu.omp.irap.vespa.epntapclient.voresource.VOResourceException;
import eu.omp.irap.vespa.epntapclient.voresource.model.Resource;
import eu.omp.irap.vespa.epntapclient.votable.model.VOTABLE;
import eu.omp.irap.vespa.votable.utils.Debug;
import eu.omp.irap.vespa.votable.utils.StringJoiner;
import eu.omp.irap.vespa.votable.votable.VOTableCtrl;
import eu.omp.irap.vespa.votable.votable.VOTableException;
import eu.omp.irap.vespa.votable.votabledata.VOTableData;

/**
 * @author N. Jourdane
 */
public class EpnTapConnection implements EpnTapInterface {

	// *** Resource ***

	@Override
	public String getEPNCoreTableName(String ivoid) throws VOTableException {
		return (String) ServiceCtrl.getParameter(ivoid, "table_name");
	}

	// *** Resources ***

	@Override
	public VOTABLE getEPNService(String ivoid) throws VOTableException {
		String query = String.format(Queries.SELECT_ALL_TAP_SERVICES_WHERE_IVOID, ivoid);
		return ServiceCtrl.getVoTable(query);
	}

	@Override
	public VOTABLE getEPNService(String ivoid, List<String> attributes)
			throws VOTableException {
		String select = StringJoiner.join(attributes);
		String query = String.format(Queries.SELECT_TAP_SERVICES_WHERE_IVOID, select, ivoid);
		return ServiceCtrl.getVoTable(query);
	}

	// *** Service ***

	@Override
	public VOTABLE getEPNServices() throws VOTableException {
		String query = String.format(Queries.SELECT_ALL_TAP_SERVICES_WHERE_CORE,
				ServiceCore.EPNCORE);
		return ServiceCtrl.getVoTable(query);
	}

	@Override
	public VOTABLE getEPNServices(List<String> attributes) throws VOTableException {
		String select = StringJoiner.join(attributes);
		String query = String.format(Queries.SELECT_TAP_SERVICES, select);
		return ServiceCtrl.getVoTable(query);
	}

	// *** Services ***

	@Override
	public VOTABLE getEPNServices(List<String> keywords, List<String> attributes)
			throws VOTableException {
		attributes.add("res_subject");
		String select = StringJoiner.join(attributes);
		List<String> whereList = new ArrayList<>();
		for (String keyword : keywords) {
			whereList.add("res_subject = '" + keyword + "'");
		}
		String where = StringJoiner.join(whereList, " OR ");
		String query = String.format(Queries.SELECT_TAP_SERVICES_WHERE_SUBJECT, select, where);
		return ServiceCtrl.getVoTable(query);
	}

	@Override
	public Resource getEPNVOresource(String ivoid) throws VOResourceException {
		return VOResourceCtrl.getVOresource(ivoid);
	}

	@Override
	public List<Resource> getEPNVOResources() throws VOResourceException {
		List<String> ivoids = VOResourceCtrl.getIvoidResources(ServiceCore.EPNCORE);
		return VOResourceCtrl.getVOResources(ivoids);
	}

	// *** Getters ***

	@Override
	public List<Resource> getEPNVOResources(List<String> keywords)
			throws VOResourceException {
		List<String> ivoids = VOResourceCtrl.getVOResources(ServiceCore.EPNCORE, keywords);
		return VOResourceCtrl.getVOResources(ivoids);
	}

	@Override
	public String getTAPURL(String ivoid) throws VOTableException {
		return (String) ServiceCtrl.getParameter(ivoid, "access_url");
	}

	// *** Queries ***

	@Override
	public List<Granule> sendADQLQuery(String tapURL, String adqlQuery) throws VOTableException {
		VOTableCtrl voTableCtrl = new VOTableCtrl();
		voTableCtrl.acquireVOTableBlocking(tapURL, adqlQuery);
		VOTableData data = voTableCtrl.getVOTableData();

		List<Granule> granules;
		GranuleCtrl gc = new GranuleCtrl(data);
		granules = gc.getGranules();
		return granules;
	}

	@Override
	public List<Granule> sendQuery(String tapURL, String schemaName, Query enumeratedQuery)
			throws VOTableException {
		String query = String.format(enumeratedQuery.toString(), schemaName);
		VOTableCtrl voTableCtrl = new VOTableCtrl();
		voTableCtrl.acquireVOTableBlocking(tapURL, query);
		VOTableData data = voTableCtrl.getVOTableData();
		Debug.writeObject("data", data);

		List<Granule> granules;
		GranuleCtrl gc = new GranuleCtrl(data);
		granules = gc.getGranules();
		return granules;
	}

}
