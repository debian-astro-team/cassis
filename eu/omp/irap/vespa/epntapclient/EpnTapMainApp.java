/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 * This file is a part of EpnTAPClient.
 * This program aims to provide EPN-TAP support for software clients, like CASSIS spectrum analyzer.
 * See draft specifications: https://voparis-confluence.obspm.fr/pages/viewpage.action?pageId=559861
 * Copyright (C) 2016 Institut de Recherche en Astrophysique et Planétologie.
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package eu.omp.irap.vespa.epntapclient;

import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import com.google.gson.Gson;

import eu.omp.irap.vespa.epntapclient.gui.mainpanel.MainPanelCtrl;

/**
 * Simple class to have a main function to launch the EPNTap client.
 *
 * @author N. Jourdane
 */
public class EpnTapMainApp {

	/** The logger for the class EpnTapMainApp. */
	private static final Logger LOGGER = Logger.getLogger(EpnTapMainApp.class.getName());

	/** Error message when the user put a wrong command. */
	private static final String WRONG_COMMAND = "Usage: EpnTapMainApp";


	/** Private constructor to hide the implicit public one. */
	private EpnTapMainApp() {
	}

	/**
	 * Main function to start the application as standalone.
	 *
	 * @param args The program arguments (not used).
	 */
	public static void main(final String[] args) {
		// RepaintManager.setCurrentManager(new CheckThreadViolationRepaintManager());
		LOGGER.info("Lauching EPNTAP app with arguments: " + new Gson().toJson(args));
		if (args.length != 0) {
			System.out.println(WRONG_COMMAND);
			return;
		}

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				MainPanelCtrl guiCtrl = new MainPanelCtrl();
				guiCtrl.acquireServices();
				JFrame frame = new JFrame("EpnTAP client");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setContentPane(guiCtrl.getView());
				frame.setVisible(true);
				frame.setSize(1000, 700);
				frame.setLocationRelativeTo(null);

			}
		});
	}
}
