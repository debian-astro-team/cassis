/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 * This file is a part of EpnTAPClient.
 * This program aims to provide EPN-TAP support for software clients, like CASSIS spectrum analyzer.
 * See draft specifications: https://voparis-confluence.obspm.fr/pages/viewpage.action?pageId=559861
 * Copyright (C) 2016 Institut de Recherche en Astrophysique et Planétologie.
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package eu.omp.irap.vespa.epntapclient.epntap;

import java.util.List;

import eu.omp.irap.vespa.epntapclient.epntap.request.RequestCtrl;
import eu.omp.irap.vespa.epntapclient.epntap.service.Queries;
import eu.omp.irap.vespa.epntapclient.epntap.service.ServiceCore;
import eu.omp.irap.vespa.epntapclient.epntap.service.ServicesList;
import eu.omp.irap.vespa.votable.Consts;
import eu.omp.irap.vespa.votable.votable.VOTableCtrl;
import eu.omp.irap.vespa.votable.votable.VOTableException;

/**
 * The main controller which manage views and controllers.
 *
 * @author N. Jourdane
 */
public abstract class EpnTapController {

	/** The request controller, to manage requests. */
	private RequestCtrl requestCtrl;

	/** The controller of the VOTable displaying the result. */
	private List<VOTableCtrl> resultsCtrls;

	/** The controller of the VOTable displaying the list of services. */
	private VOTableCtrl servicesCtrl;

	/**
	 * The path of the VOTable to parse. Will be affected to a temporary folder if not assigned
	 * through the controller.
	 */
	private String voTablePath;


	/**
	 * Get the services from the XML path or the targetURL / query.
	 *
	 * @throws VOTableException Can not read the services.
	 */
	public void acquireServices() throws VOTableException {
		String query = String.format(Queries.SELECT_ALL_TAP_SERVICES_WHERE_CORE,
				ServiceCore.EPNCORE);
		getServicesCtrl().acquireVOTable(Consts.DEFAULT_REGISTRY_URL, query);
	}

	/**
	 * @return The request controller.
	 */
	public RequestCtrl getRequestCtrl() {
		return requestCtrl;
	}

	/**
	 * @return The controller of the VOTable which displays the result of the query.
	 */
	public List<VOTableCtrl> getResultsCtrls() {
		return resultsCtrls;
	}

	/**
	 * @return The controller of the VOTable which displays the list of services.
	 */
	public VOTableCtrl getServicesCtrl() {
		return servicesCtrl;
	}

	/**
	 * @return the path of the XML VOTable file.
	 */
	public String getVOTablePath() {
		return voTablePath;
	}

	/**
	 * Send the query to all services.
	 *
	 * @param services The services to send the queries.
	 */
	public void sendQueries(ServicesList services) {
		for (int i = 0; i < services.getNbServices(); i++) {
			VOTableCtrl resultCtrl = new VOTableCtrl();
			String query = requestCtrl.getQuery(services.getTableNames().get(i));
			resultCtrl.acquireVOTable(services.getTargetUrls().get(i), query);
		}
	}
}
