/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 * This file is a part of EpnTAPClient.
 * This program aims to provide EPN-TAP support for software clients, like CASSIS spectrum analyzer.
 * See draft specifications: https://voparis-confluence.obspm.fr/pages/viewpage.action?pageId=559861
 * Copyright (C) 2016 Institut de Recherche en Astrophysique et Planétologie.
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package eu.omp.irap.vespa.epntapclient.epntap.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.XMLGregorianCalendar;

import eu.omp.irap.vespa.epntapclient.voresource.model.ContentLevel;
import eu.omp.irap.vespa.epntapclient.voresource.model.Creator;
import eu.omp.irap.vespa.epntapclient.voresource.model.Resource;
import eu.omp.irap.vespa.epntapclient.voresource.model.Type;
import eu.omp.irap.vespa.epntapclient.votable.model.Table;
import eu.omp.irap.vespa.epntapclient.votable.model.VOTABLE;
import eu.omp.irap.vespa.votable.Consts;
import eu.omp.irap.vespa.votable.utils.StringJoiner;
import eu.omp.irap.vespa.votable.votable.VOTableCtrl;
import eu.omp.irap.vespa.votable.votable.VOTableException;
import eu.omp.irap.vespa.votable.votable.VOTableException.CanNotParseDataException;
import eu.omp.irap.vespa.votable.votabledata.VOTableData;
import eu.omp.irap.vespa.votable.votabledata.VOTableDataParser;

/**
 * @author N. Jourdane
 */
public class ServiceCtrl {

	/** The date format used to serialize date parameters. */
	public static final String DATE_FORMAT = "yyyy/MM/dd HH:mm:ss";


	/** Private constructor to hide the implicit public one. */
	private ServiceCtrl() {
	}

	/**
	 * @param ivoid The ivoid of the service.
	 * @param parameter The unique parameter to get.
	 * @return The value of the parameter.
	 * @throws VOTableException Can not get the VOTable.
	 */
	public static Object getParameter(String ivoid, String parameter)
			throws VOTableException {
		String query = String.format(Queries.SELECT_TAP_SERVICES_WHERE_IVOID, parameter, ivoid);
		return getVoTableData(query).getCell(0, 0);
	}

	/**
	 * @param ivoid The ivoid of the service.
	 * @param parameters A list of parameters to get.
	 * @return A map of <parameter name, value> resulting the request.
	 * @throws VOTableException Can not get the VOTable.
	 */
	public static Map<String, Object> getParameters(String ivoid, List<String> parameters)
			throws VOTableException {
		return getParameters(ivoid, StringJoiner.join(parameters));
	}

	/**
	 * @param ivoid The ivoid of the service.
	 * @param parameters The parameters to get, separated by commas.
	 * @return A map of <parameter name, value> resulting the request.
	 * @throws VOTableException Can not get the VOTable.
	 */
	public static Map<String, Object> getParameters(String ivoid, String parameters)
			throws VOTableException {
		String query = String.format(Queries.SELECT_TAP_SERVICES_WHERE_IVOID, parameters, ivoid);
		return getVoTableData(query).getRowMap(0);
	}

	/**
	 * Parse a Resource and returns the corresponding Service.
	 *
	 * @param resource the resource to parse.
	 * @return The corresponding service.
	 */
	public static final Service getService(Resource resource) {
		Service service = new Service(resource.getIdentifier());
		service.setTitle(resource.getTitle());
		service.setShortName(resource.getShortName());
		StringJoiner types = new StringJoiner(", ");
		for (Type type : resource.getContent().getType()) {
			types.add(type.toString());
		}
		service.setType(types.toString());
		service.setDescription(resource.getContent().getDescription());
		StringJoiner creators = new StringJoiner(", ");
		for (Creator creator : resource.getCuration().getCreator()) {
			creators.add(creator.getName().getValue());
		}
		service.setCreator(creators.toString());
		StringJoiner contentLevels = new StringJoiner(", ");
		for (ContentLevel contentLevel : resource.getContent().getContentLevel()) {
			contentLevels.add(contentLevel.value());
		}

		service.setContentLevel(contentLevels.toString());
		service.setReferenceURL(resource.getContent().getReferenceURL());
		service.setCreated(ServiceCtrl.xmlDateToString(resource.getCreated()));
		service.setUpdated(ServiceCtrl.xmlDateToString(resource.getUpdated()));
		return service;
	}

	/**
	 * Parse a VOTableData and returns the corresponding list of services.
	 *
	 * @param data The VOTableData of the service, created by the VOTable parser. Each row of the
	 *            data set is a service.
	 * @return The corresponding list of service.
	 * @throws CanNotParseDataException The column name was not found in the list.
	 */
	public static final List<Service> getServices(VOTableData data)
			throws CanNotParseDataException {
		List<Service> services = new ArrayList<>();
		for (int i = 0; i < data.getNbRows(); i++) {
			Service service = new Service((String) data.getCell(i, "ivoid"));
			service.setTitle((String) data.getCell(i, "res_title"));
			service.setShortName((String) data.getCell(i, "short_name"));
			service.setType((String) data.getCell(i, "content_type"));
			service.setDescription((String) data.getCell(i, "res_description"));
			service.setCreator((String) data.getCell(i, "creator_seq"));
			service.setContentLevel((String) data.getCell(i, "content_level"));
			service.setReferenceURL((String) data.getCell(i, "reference_url"));
			service.setCreated((String) data.getCell(i, "created"));
			service.setUpdated((String) data.getCell(i, "updated"));
			services.add(service);
		}
		return services;
	}

	/**
	 * @param query The query.
	 * @return The VOTable resulting the query.
	 * @throws VOTableException Can not get the VOTable.
	 */
	public static VOTABLE getVoTable(String query) throws VOTableException {
		VOTableCtrl voTableCtrl = new VOTableCtrl();
		voTableCtrl.acquireVOTableBlocking(Consts.DEFAULT_REGISTRY_URL, query);
		return voTableCtrl.getVOTable();
	}

	/**
	 * @param query The query to get the service.
	 * @return The VOTableData
	 * @throws VOTableException Can not get the VOTable.
	 */
	public static VOTableData getVoTableData(String query) throws VOTableException {
		return getVoTableData(getVoTable(query));
	}

	/**
	 * Parse a VOTable and returns the corresponding VOTableData.
	 *
	 * @param voTable The VOTable parsed.
	 * @return The corresponding VOTableData.
	 * @throws VOTableException Can not get the VOTable.
	 */
	public static VOTableData getVoTableData(VOTABLE voTable) throws VOTableException {
		VOTableCtrl ctrl = new VOTableCtrl();
		ctrl.acquireVOTableBlocking(voTable);
		Table table = (Table) voTable.getRESOURCE().get(0).getLINKAndTABLEOrRESOURCE().get(0);
		VOTableDataParser dataParser = new VOTableDataParser("Services list", table);
		dataParser.parseData();
		return dataParser.getData();
	}

	/**
	 * Convert a date from XMLGregorianCalendar to a string literal.
	 *
	 * @param date The date to convert
	 * @return The serialized date.
	 */
	private static String xmlDateToString(XMLGregorianCalendar date) {
		SimpleDateFormat sdf = new SimpleDateFormat(ServiceCtrl.DATE_FORMAT);
		return sdf.format(date.toGregorianCalendar().getTime());
	}
}
