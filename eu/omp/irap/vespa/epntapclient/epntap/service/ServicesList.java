/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 * This file is a part of EpnTAPClient.
 * This program aims to provide EPN-TAP support for software clients, like CASSIS spectrum analyzer.
 * See draft specifications: https://voparis-confluence.obspm.fr/pages/viewpage.action?pageId=559861
 * Copyright (C) 2016 Institut de Recherche en Astrophysique et Planétologie.
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package eu.omp.irap.vespa.epntapclient.epntap.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import eu.omp.irap.vespa.votable.utils.StringJoiner;

/**
 * @author N. Jourdane
 */
public class ServicesList {

	/** The logger for the class ServicesList. */
	private static final Logger LOGGER = Logger.getLogger(ServicesList.class.getName());

	/** The list of table names entered in the custom table name field. */
	private List<String> customTableNames;

	/** The list of target URLs entered in the custom target URL field. */
	private List<String> customTargetUrls;

	/** The list of table names selected in the services table. */
	private List<String> selectedTableNames;

	/** The list of target URLs selected in the services table. */
	private List<String> selectedTargetUrls;


	/**
	 * Constructor of LightServicesList
	 */
	public ServicesList() {
		customTableNames = new ArrayList<>();
		customTargetUrls = new ArrayList<>();
		selectedTableNames = new ArrayList<>();
		selectedTargetUrls = new ArrayList<>();
	}

	/** @return The numbers of services (custom + selected) */
	public int getNbServices() {
		return customTargetUrls.size() + selectedTargetUrls.size();
	}

	/**
	 * @return the tableName
	 */
	public List<String> getTableNames() {
		List<String> tableNames = selectedTableNames;
		tableNames.addAll(customTableNames);
		return tableNames;
	}

	/**
	 * @return the targetUrl
	 */
	public List<String> getTargetUrls() {
		List<String> targetUrls = selectedTargetUrls;
		targetUrls.addAll(customTargetUrls);
		return targetUrls;
	}

	/**
	 * Update the services (table name and target URLs) entered in the custom services fields.
	 *
	 * @param customTableNamesToAdd The list of the table names to update.
	 * @param customTargetUrlsToAdd The list of the target URLs to update.
	 */
	public void updateCustomServices(List<String> customTableNamesToAdd,
			List<String> customTargetUrlsToAdd) {
		customTableNames = customTableNamesToAdd;
		customTargetUrls = customTargetUrlsToAdd;
		LOGGER.fine("Updated custom tables names: " + StringJoiner.join(customTableNames));
		LOGGER.fine("Updated custom services URLs: " + StringJoiner.join(customTargetUrls));
	}

	/**
	 * Update the services (table name and target URLs) selected in the services table.
	 *
	 * @param selectedTableNamesToAdd The list of the table names to update.
	 * @param selectedTargetUrlsToAdd The list of the target URLs to update.
	 */
	public void updateSelectedServices(List<String> selectedTableNamesToAdd,
			List<String> selectedTargetUrlsToAdd) {
		selectedTableNames = selectedTableNamesToAdd;
		selectedTargetUrls = selectedTargetUrlsToAdd;
		LOGGER.fine("Updated selected tables names: " + StringJoiner.join(selectedTableNames));
		LOGGER.fine("Updated selected services URLs: " + StringJoiner.join(selectedTargetUrls));
	}
}
