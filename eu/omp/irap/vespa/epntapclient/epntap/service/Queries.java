/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 * This file is a part of EpnTAPClient.
 * This program aims to provide EPN-TAP support for software clients, like CASSIS spectrum analyzer.
 * See draft specifications: https://voparis-confluence.obspm.fr/pages/viewpage.action?pageId=559861
 * Copyright (C) 2016 Institut de Recherche en Astrophysique et Planétologie.
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package eu.omp.irap.vespa.epntapclient.epntap.service;

/**
 * Defines the queries and the query patterns usually used in the application.
 *
 * @author N. Jourdane
 */
public final class Queries {

	/** The default FROM keyword content of the query, which links all required tables. */
	private static final String FROM = "FROM rr.resource "
			+ "NATURAL JOIN rr.res_schema "
			+ "NATURAL JOIN rr.res_table "
			+ "NATURAL JOIN rr.interface "
			+ "NATURAL JOIN rr.res_detail "
			+ "NATURAL JOIN rr.capability ";

	/** The default ORDER BY keyword content of the query. */
	private static final String ORDER_BY = "ORDER BY short_name, table_name";

	/** The default SELECT keyword */
	private static final String SELECT = "SELECT DISTINCT ";

	/**
	 * The default SELECT keyword content of the query, which select standard service column names.
	 */
	private static final String SELECT_SERVICE = "SELECT DISTINCT short_name, res_title, ivoid, "
			+ "access_url, table_name, content_type, res_description, creator_seq, content_level, "
			+ "reference_url, created, updated ";

	/** The default WHERE keyword content of the query, in order to get only TAP services. */
	private static final String WHERE_TAP = "WHERE "
			+ "standard_id='ivo://ivoa.net/std/tap' AND "
			+ "intf_type='vs:paramhttp' AND "
			+ "detail_xpath='/capability/dataModel/@ivo-id' ";

	/** Query to select all columns of any table. */
	public static final String SELECT_ALL = "SELECT DISTINCT * FROM %s";

	/** Query to get all TAP services. */
	public static final String SELECT_ALL_TAP_SERVICES = SELECT_SERVICE + FROM + WHERE_TAP
			+ ORDER_BY;

	/** Query to get TAP services which implement the specified core (%s #1). */
	public static final String SELECT_ALL_TAP_SERVICES_WHERE_CORE = SELECT_SERVICE + FROM
			+ WHERE_TAP
			+ "AND 1=ivo_nocasematch(detail_value, 'ivo://vopdc.obspm/std/%s%%') "
			+ "AND table_name LIKE '%%.epn_core' " + ORDER_BY;

	/** Query to get the TAP service with the specified ivoid (%s #1). */
	public static final String SELECT_ALL_TAP_SERVICES_WHERE_IVOID = SELECT_SERVICE + FROM
			+ WHERE_TAP + "AND ivoid = '%s'";

	/** Query to get the specified column names (%s #1) of any table. */
	public static final String SELECT_FROM = SELECT + "%s FROM %s";

	/** Query to get the specified columns (%s #1) of all TAP services. */
	public static final String SELECT_TAP_SERVICES = SELECT + "%s " + FROM + WHERE_TAP;

	/**
	 * Query to get the specified column names (%s #1) of TAP service with the specified ivoid (%s
	 * #2).
	 */
	public static final String SELECT_TAP_SERVICES_WHERE_IVOID = SELECT + "%s " + FROM + WHERE_TAP
			+ "AND ivoid = '%s'";

	/**
	 * Query to get the specified column names (%s #1) of TAP service with the specified subject (%s
	 * #2).
	 */
	public static final String SELECT_TAP_SERVICES_WHERE_SUBJECT = SELECT + "%s " + FROM
			+ "NATURAL JOIN rr.res_subject " + WHERE_TAP + "AND (%s)";




	/** Constructor to hide the implicit public one. */
	private Queries() {
	}

}
