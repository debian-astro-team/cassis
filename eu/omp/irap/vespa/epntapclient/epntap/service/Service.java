/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 * This file is a part of EpnTAPClient.
 * This program aims to provide EPN-TAP support for software clients, like CASSIS spectrum analyzer.
 * See draft specifications: https://voparis-confluence.obspm.fr/pages/viewpage.action?pageId=559861
 * Copyright (C) 2016 Institut de Recherche en Astrophysique et Planétologie.
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package eu.omp.irap.vespa.epntapclient.epntap.service;

/**
 * @author N. Jourdane
 */
public class Service {

	/**
	 * The content_level of the service: <br/>
	 * <em> A hash-separated list of content levels specifying the intended audience.</em>
	 */
	private String contentLevel;

	/**
	 * The created date of the service, as a string: <br/>
	 * <em>The UTC date and time this resource metadata de- scription was created. This timestamp must
	 * not be in the future. This time is not required to be accurate; it should be at least
	 * accurate to the day. Any insignificant time elds should be set to zero.</em>
	 */
	private String created;

	/**
	 * The creator of the service: <br/>
	 * <em>The creator(s) of the resource in the order given by the resource record author, separated by
	 * semicolons.</em>
	 */
	private String creator;

	/**
	 * The description of the service: <br/>
	 * <em>An account of the nature of the resource.</em>
	 */
	private String description;

	/**
	 * The ivoid of the service: <br/>
	 * <em>Unambiguous reference to the resource conforming to the IVOA
	 * standard for identifiers.</em>
	 */
	private String ivoid;

	/**
	 * The reference_rul of the service: <br/>
	 * <em>URL pointing to a human-readable document describing this resource.</em>
	 */
	private String referenceURL;

	/**
	 * The short_name of the service: <br/>
	 * <em> A short name or abbreviation given to something. This name will be used where brief
	 * annotations for the re- source name are required. Applications may use it to refer to the
	 * resource in a compact display. One word or a few letters is recommended. No more than sixteen
	 * characters are allowed.</em>
	 */
	private String shortName;

	/**
	 * The title of the service: <br/>
	 * <em>The full name given to the resource.</em>
	 */
	private String title;

	/**
	 * The type of the service: <br/>
	 * <em>Resource type (something like vs:datacollection, vs:catalogservice, etc).</em>
	 */
	private String type;

	/**
	 * The updated date of the service, as a string: <br/>
	 * <em> The UTC date this resource metadata description was last updated. This timestamp must not be
	 * in the future. This time is not required to be accurate; it should be at least accurate to
	 * the day. Any insignificant time elds should be set to zero.</em>
	 */
	private String updated;


	/**
	 * Constructor of Service
	 *
	 * @param ivoid The ivoID of the service.
	 */
	public Service(String ivoid) {
		this.ivoid = ivoid;
	}

	/**
	 * Get the service {@link #contentLevel}.
	 *
	 * @return The service {@link #contentLevel}.
	 */
	public String getContentLevel() {
		return contentLevel;
	}

	/**
	 * Get the service {@link #created} date.
	 *
	 * @return The service {@link #created} date.
	 */
	public String getCreated() {
		return created;
	}

	/**
	 * Get the service {@link #creator}.
	 *
	 * @return The service {@link #creator}.
	 */
	public String getCreator() {
		return creator;
	}

	/**
	 * Get the service {@link #description}.
	 *
	 * @return The service {@link #description}.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Get the service {@link #ivoid}.
	 *
	 * @return The service {@link #ivoid}.
	 */
	public String getIvoid() {
		return ivoid;
	}

	/**
	 * Get the service {@link #referenceURL}.
	 *
	 * @return The service {@link #referenceURL}.
	 */
	public String getReferenceURL() {
		return referenceURL;
	}

	/**
	 * Get the service {@link #shortName}.
	 *
	 * @return The service {@link #shortName}.
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * Get the service {@link #title}.
	 *
	 * @return The service {@link #title}.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Get the service {@link #type}.
	 *
	 * @return The service {@link #type}.
	 */
	public String getType() {
		return type;
	}

	/**
	 * Get the service {@link #updated} date.
	 *
	 * @return The service {@link #updated} date.
	 */
	public String getUpdated() {
		return updated;
	}

	/**
	 * @return true if all parameter of the service are not null.
	 */
	public boolean isValid() {
		boolean isValid = ivoid != null && title != null && shortName != null;
		isValid = isValid && type != null && description != null && creator != null;
		isValid = isValid && contentLevel != null && referenceURL != null && created != null;
		return isValid && updated != null;
	}

	/**
	 * Set the service {@link #contentLevel}.
	 *
	 * @param contentLevel The service {@link #contentLevel}
	 */
	public void setContentLevel(String contentLevel) {
		this.contentLevel = contentLevel;
	}

	/**
	 * Set the service {@link #created}.
	 *
	 * @param created The service {@link #created}
	 */
	public void setCreated(String created) {
		this.created = created;
	}

	/**
	 * Set the service {@link #creator}.
	 *
	 * @param creator The service {@link #creator}
	 */
	public void setCreator(String creator) {
		this.creator = creator;
	}

	/**
	 * Set the service {@link #description}.
	 *
	 * @param description The service {@link #description}
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Set the service {@link #referenceURL}.
	 *
	 * @param referenceURL The service {@link #referenceURL}
	 */
	public void setReferenceURL(String referenceURL) {
		this.referenceURL = referenceURL;
	}

	/**
	 * Set the service {@link #shortName}.
	 *
	 * @param shortName The service {@link #shortName}
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * Set the service {@link #title}.
	 *
	 * @param title The service {@link #title}
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Set the service {@link #type}.
	 *
	 * @param type The service {@link #type}
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Set the service {@link #updated}.
	 *
	 * @param updated The service {@link #updated}
	 */
	public void setUpdated(String updated) {
		this.updated = updated;
	}

}
