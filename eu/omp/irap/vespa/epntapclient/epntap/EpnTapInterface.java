/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 * This file is a part of EpnTAPClient.
 * This program aims to provide EPN-TAP support for software clients, like CASSIS spectrum analyzer.
 * See draft specifications: https://voparis-confluence.obspm.fr/pages/viewpage.action?pageId=559861
 * Copyright (C) 2016 Institut de Recherche en Astrophysique et Planétologie.
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package eu.omp.irap.vespa.epntapclient.epntap;

import java.util.List;

import eu.omp.irap.vespa.epntapclient.Query;
import eu.omp.irap.vespa.epntapclient.granule.Granule;
import eu.omp.irap.vespa.epntapclient.granule.GranuleEnum;
import eu.omp.irap.vespa.epntapclient.voresource.VOResourceException;
import eu.omp.irap.vespa.epntapclient.voresource.model.Resource;
import eu.omp.irap.vespa.epntapclient.votable.model.VOTABLE;
import eu.omp.irap.vespa.votable.votable.VOTableException;

/**
 * @author N. Jourdane
 */
public interface EpnTapInterface {

	// *** Resource ***

	/**
	 * Returns the name of the EPNCore Table related to a service.
	 *
	 * @param ivoid The ivoid of the service.
	 * @return The table name of the service, used usually in the 'FROM' keyword in a ADQL query.
	 * @throws VOTableException Can not get the VOTable.
	 */
	String getEPNCoreTableName(String ivoid) throws VOTableException;

	// *** Resources ***

	/**
	 * Returns a VOTable containing the attributes of the corresponding service (from a predefined
	 * list).
	 *
	 * @param ivoid The ivoid of the service to get.
	 * @return A VOTable containing the service.
	 * @throws VOTableException Can not get the VOTable.
	 */
	VOTABLE getEPNService(String ivoid) throws VOTableException;

	/**
	 * Returns a VOTable containing the attributes of the corresponding service (from the list of
	 * attributes).
	 *
	 * @param ivoid The ivoid of the service to get.
	 * @param attributes A list of attributes, which are the column names to get.
	 * @return A VOTable containing the service.
	 * @see GranuleEnum The EpnTapv2 column names.
	 * @throws VOTableException Can not get the VOTable.
	 */
	VOTABLE getEPNService(String ivoid, List<String> attributes)
			throws VOTableException;

	// *** Service ***

	/**
	 * Returns a VOTable containing the list of EPN-TAP services and their attributes (from a
	 * predefined list).
	 *
	 * @return A VOTable containing the services.
	 * @throws VOTableException Can not get the VOTable.
	 */
	VOTABLE getEPNServices() throws VOTableException;

	/**
	 * Returns a VOTable containing the list of EPN-TAP services and their attributes (from the list
	 * of attributes)
	 *
	 * @param attributes A list of attributes, which are the column names to get.
	 * @return A VOTable containing the services.
	 * @throws VOTableException Can not get the VOTable.
	 */
	VOTABLE getEPNServices(List<String> attributes) throws VOTableException;

	// *** Services ***

	/**
	 * Returns a VOTable containing the list of EPN-TAP services corresponding to the keywords and
	 * their attributes (from the list of attributes).
	 *
	 * @param keywords A list of keywords, which are the *res_subject* column content of the table
	 *            rr.res_subject of a registry.
	 * @param attributes A list of attributes, which are the column names to get.
	 * @return A VOTable containing the services.
	 * @throws VOTableException Can not get the VOTable.
	 */
	VOTABLE getEPNServices(List<String> keywords, List<String> attributes)
			throws VOTableException;

	/**
	 * Returns the VOResource element of the service identified by the ivoID.
	 *
	 * @param ivoid the ivoid of the service.
	 * @return The Resource of a service corresponding to the ivoid
	 * @throws VOResourceException Can not get the VOResource.
	 */
	Resource getEPNVOresource(String ivoid) throws VOResourceException;

	/**
	 * Returns a set of VOResource elements (one per EPN-TAP service).
	 *
	 * @return A list containing the VOResources of all EpnTap services.
	 * @throws VOResourceException Can not get the VOResource.
	 */
	List<Resource> getEPNVOResources() throws VOResourceException;

	// *** Getters ***

	/**
	 * Returns a set of VOREsource elements (one per EPN-TAP service corresponding to the keywords).
	 * The way keywords are defined is still to be defined.
	 *
	 * @param keywords A list of keywords, which are the content of the *subject* JSON node in the
	 *            query.
	 * @see <a href=
	 *      "http://voparis-registry.obspm.fr/vo/ivoa/1/voresources/search?keywords=standardid:%22ivo://ivoa.net/std/TAP%22%20subjects:%22Spectroscopy%22&max=100">
	 *      this example request</a>
	 * @return A list containing the selected VOResources.
	 * @throws VOResourceException Can not get the VOResource.
	 */
	List<Resource> getEPNVOResources(List<String> keywords) throws VOResourceException;

	/**
	 * Returns the Access URL of an EPN-TAP Service.
	 *
	 * @param ivoid the ivoid of the service.
	 * @return The target URL of the service, used usually to process requests on it.
	 * @throws VOTableException Can not get the VOTable.
	 */
	String getTAPURL(String ivoid) throws VOTableException;

	// *** Queries ***

	/**
	 * Returns the list of granules which are compliant with the ADQL Query, in VOTable format .
	 * TAPURL is build from elements taken in VOResource. "ADQLQuery" is created by the Client. It
	 * is a full query containing the name of the EPNCore table, taken in VOResource.
	 *
	 * @param tapURL The URL of the service.
	 * @param adqlQuery The ADQL query.
	 * @return A list of Granules resulting the query.
	 * @throws VOTableException Can not get the VOTable.
	 */
	List<Granule> sendADQLQuery(String tapURL, String adqlQuery) throws VOTableException;

	/**
	 * Returns the list of granules which are compliant with the Query, in VOTable format. "Query"
	 * is not an ADQL query. It is taken from a list of predefined queries. This list must be
	 * created.
	 *
	 * @param tapURL The URL of the service.
	 * @param schemaName The name of the service schema.
	 * @param query The query, from a list a predefined queries.
	 * @return A list of granules resulting the query.
	 * @throws VOTableException Can not get the VOTable.
	 */
	List<Granule> sendQuery(String tapURL, String schemaName, Query query) throws VOTableException;
}
