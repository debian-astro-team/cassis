/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 * This file is a part of EpnTAPClient.
 * This program aims to provide EPN-TAP support for software clients, like CASSIS spectrum analyzer.
 * See draft specifications: https://voparis-confluence.obspm.fr/pages/viewpage.action?pageId=559861
 * Copyright (C) 2016 Institut de Recherche en Astrophysique et Planétologie.
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package eu.omp.irap.vespa.epntapclient.epntap.request;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import eu.omp.irap.vespa.votable.utils.CantSendQueryException;
import eu.omp.irap.vespa.votable.utils.Network;
import eu.omp.irap.vespa.votable.utils.StringJoiner;

/**
 * @author N. Jourdane
 */
public class RequestCtrl {

	/** The text in the query to replaced by the actual table name. */
	private static final String KEYWORD_TABLE_NAME = "#tablename#";

	/** The logger for the class RequestCtrl. */
	private static final Logger LOGGER = Logger.getLogger(RequestCtrl.class.getName());

	/** The query template to get granules. */
	private static final String QUERY_TEMPLATE = "SELECT TOP %s %s FROM "
			+ KEYWORD_TABLE_NAME + "%s";

	/** The URL of the resolver used for the `target name` field. */
	private static final String RESOLVER_URL = "https://api.ssodnet.imcce.fr/quaero/1/sso/instant-search";

	/** The names of the columns used in the SELECT keyword in the query. */
	private List<String> columnNames;

	/** The maximum number of rows returned by the query. */
	private int nbMaxResult;

	/** The parameters fields for the request. */
	private Map<String, Object> parameters = new HashMap<>();

	/**
	 * The query to send, containing {@link #KEYWORD_TABLE_NAME} which will be replaced by the
	 * actual name of each table.
	 */
	private String query;


	/** Default constructor of RequestCtrl. */
	public RequestCtrl() {
		List<String> newColumnNames = new ArrayList<>();
		newColumnNames.add("target_name");
		newColumnNames.add("target_class");
		columnNames = newColumnNames;
		nbMaxResult = 20;
	}

	/**
	 * Constructor of RequestCtrl.
	 *
	 * @param nbMaxResult The maximum number of rows returned by the query.
	 * @param columnNames The names of the columns used in the SELECT keyword in the query.
	 */
	public RequestCtrl(int nbMaxResult, List<String> columnNames) {
		this.nbMaxResult = nbMaxResult;
		this.columnNames = columnNames;
	}

	/**
	 * The method used to get target names propositions by asking to the resolver.
	 *
	 * @param begining The beginning of the target_name.
	 * @return An array of Strings corresponding to the target names got.
	 * @throws CantSendQueryException Can not read the JSON file.
	 */
	public static String[] getTargetNames(String begining) throws CantSendQueryException {
		Map<String, String> params = new HashMap<>();
		params.put("q", "\"" + begining + "\"");

//		String query = Network.buildGetRequest(RESOLVER_URL, params);
		String query = RESOLVER_URL + "?q=" + begining;
		JsonObject root = Network.readJson(query);
		JsonArray data = root.getAsJsonArray("data");
		int count = data.size();
		String[] targetNames = new String[count];
//		JsonArray hits = root.getAsJsonArray("hits");
		for (int i = 0; i < count; i++) {
			JsonObject elmt = data.get(i).getAsJsonObject();
			targetNames[i] = elmt.get("name").toString().replace("\"", "");
		}
		return targetNames;
	}

	/**
	 * @return The names of the columns used in the SELECT keyword in the query.
	 */
	public List<String> getColumnNames() {
		return columnNames;
	}

	/**
	 * @return The maximum number of rows returned by the query.
	 */
	public int getNbMaxResult() {
		return nbMaxResult;
	}

	/**
	 * Get the parameters.
	 *
	 * @return A map of couple <parameter name, parameter value>.
	 */
	public Map<String, Object> getParameters() {
		return parameters;
	}

	/**
	 * @return The query to send, with the name of the table is replaced by
	 *         {@link #KEYWORD_TABLE_NAME}.
	 */
	public String getQuery() {
		return query;
	}

	/**
	 * Get the query and replace the {@link #KEYWORD_TABLE_NAME} by the specified tableName.
	 *
	 * @param tableName The name of the table, to put in the FROM ADQL keyword.
	 * @return The ADQL query, containing the specified table name.
	 */
	public String getQuery(String tableName) {
		return query.replace(KEYWORD_TABLE_NAME, tableName);
	}

	/**
	 * Remove a parameter from the query.
	 *
	 * @param paramName The name of the parameter to remove.
	 */
	public void removeParameter(String paramName) {
		parameters.remove(paramName);
		LOGGER.fine("removed " + paramName);
	}

	/**
	 * Set the names of the columns used in the SELECT keyword in the query.
	 *
	 * @param columnNames The columns names.
	 */
	public void setColumnNames(List<String> columnNames) {
		this.columnNames = columnNames;
	}

	/**
	 * Set the maximum number of rows returned by the query.
	 *
	 * @param nbRows The number of rows.
	 */
	public void setNbMaxResult(int nbRows) {
		nbMaxResult = nbRows;
	}

	/**
	 * Set the query, containing {@link #KEYWORD_TABLE_NAME}.
	 *
	 * @param query The query.
	 */
	public void setQuery(String query) {
		this.query = query;
	}

	/**
	 * Update a parameter in the query. If the parameter do not exists yet, create it.
	 *
	 * @param paramName The name of the parameter.
	 * @param paramValue The value of the parameter.
	 */
	public void updateParameter(String paramName, Object paramValue) {
		parameters.put(paramName, paramValue);
		LOGGER.fine("updated " + paramName + ": " + paramValue);

	}

	/**
	 * Update the ADQL query, from the column names, the table name, the parameter values and the
	 * max results.
	 */
	public void updateQuery() {
		StringJoiner addJoin = new StringJoiner(" AND ");
		for (Map.Entry<String, Object> param : parameters.entrySet()) {
			if (param.getValue() instanceof ArrayList) {
				StringJoiner orJoin = new StringJoiner(" OR ");
				@SuppressWarnings("unchecked")
				List<String> possibleValues = (List<String>) param.getValue();
				for (String possibleValue : possibleValues) {
					orJoin.add(param.getKey() + " LIKE '" + possibleValue + "'");
				}
				addJoin.add("(" + orJoin + ")");
			} else if (param.getValue() instanceof String) {
				addJoin.add(param.getKey() + " LIKE '" + param.getValue() + "'");
			} else {
				addJoin.add(param.getKey() + " = " + param.getValue().toString());
			}
		}
		String where = addJoin.isEmpty() ? "" : " WHERE " + addJoin;
		query = String.format(QUERY_TEMPLATE, nbMaxResult, StringJoiner.join(columnNames), where);

	}

}
