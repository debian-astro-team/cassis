/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 * This file is a part of EpnTAPClient.
 * This program aims to provide EPN-TAP support for software clients, like CASSIS spectrum analyzer.
 * See draft specifications: https://voparis-confluence.obspm.fr/pages/viewpage.action?pageId=559861
 * Copyright (C) 2016 Institut de Recherche en Astrophysique et Planétologie.
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package eu.omp.irap.vespa.votable.utils;

import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import eu.omp.irap.vespa.votable.Consts;

/**
 * @author N. Jourdane
 */
public class Debug {

	/** The logger for the class Debug. */
	private static final Logger LOGGER = Logger.getLogger(Debug.class.getName());


	/** Private constructor to hide the implicit public one. */
	private Debug() {
	}

	/**
	 * @param object An object to serialize.
	 * @return The serialized JSon representation of a given object.
	 */
	public static String toJson(Object object) {
		return new GsonBuilder().serializeSpecialFloatingPointValues().create().toJson(object);
	}

	/**
	 * Print the specified object in JSON format in a file on the temporary directory.
	 *
	 * @param title The name of the file.
	 * @param obj the object to print in a file.
	 * @return The path of the file.
	 */
	public static String writeObject(String title, Object obj) {
		Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().setPrettyPrinting()
				.create();
		String json = gson.toJson(obj);
		String path = Consts.TMP_DIR + "/" + title + ".json";
		try (FileWriter writer = new FileWriter(path)) {
			writer.write(json);

		} catch (IOException e) {
			Debug.LOGGER.warning("Can not print in the file " + path + e);
		}
		Debug.LOGGER.info("A json file representing " + title + " (" + obj.getClass().getName()
				+ ") has been created on " + path);

		return path;
	}
}
