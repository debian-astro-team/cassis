/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 * This file is a part of EpnTAPClient.
 * This program aims to provide EPN-TAP support for software clients, like CASSIS spectrum analyzer.
 * See draft specifications: https://voparis-confluence.obspm.fr/pages/viewpage.action?pageId=559861
 * Copyright (C) 2016 Institut de Recherche en Astrophysique et Planétologie.
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package eu.omp.irap.vespa.votable.utils;

import java.util.Arrays;
import java.util.List;

/**
 * StringJoiner has the same purpose of Java 8 StringJoiner, it has been rewritten for Java 7
 * compatibility.
 *
 * @author N. Jourdane
 */
public class StringJoiner {

	/** A separator to put between each word, ie. ';'. */
	private String separator;

	/** The resulting string, ie. "foo;bar". */
	private String string;


	/**
	 * Constructor of StringJoiner.
	 *
	 * @param list A list of words to join.
	 * @param separator A separator to put between each word, ie. ';'.
	 */
	public StringJoiner(List<?> list, String separator) {
		this.separator = separator;
		string = new String();
		add(list);
	}

	/**
	 * Method constructor for the String joiner.
	 *
	 * @param separator A separator to put between each word, ie. ';'.
	 */
	public StringJoiner(String separator) {
		this.separator = separator;
		string = new String();
	}

	/**
	 * Static method to directly join a string from a list, without declaring StringJoiner object.
	 * It uses the default separator ', '.
	 *
	 * @param list A list of words to join.
	 * @return The joined string.
	 */
	public static String join(List<?> list) {
		return new StringJoiner(list, ", ").toString();
	}

	/**
	 * Static method to directly join a string from a list and a separator, without declaring
	 * StringJoiner object.
	 *
	 * @param list A list of words to join.
	 * @param separator A separator to put between each word, ie. ';'.
	 * @return The joined string.
	 */
	public static String join(List<?> list, String separator) {
		return new StringJoiner(list, separator).toString();
	}

	/**
	 * Static method to directly join a string from a list, without declaring StringJoiner object.
	 * It uses the default separator ', '.
	 *
	 * @param array An array of words to join.
	 * @return The joined string.
	 */
	public static String join(Object[] array) {
		return new StringJoiner(Arrays.asList(array), ", ").toString();
	}

	/**
	 * Static method to directly join a string from an array and a separator, without declaring
	 * StringJoiner object.
	 *
	 * @param array An array of words to join.
	 * @param separator A separator to put between each word, ie. ';'.
	 * @return The joined string.
	 */
	public static String join(Object[] array, String separator) {
		return new StringJoiner(Arrays.asList(array), separator).toString();
	}

	/**
	 * ...
	 *
	 * @param list A list of words to join.
	 */
	public void add(List<?> list) {
		for (Object e : list) {
			add(e.toString());
		}
	}

	/**
	 * Add a new word to the joiner.
	 *
	 * @param text The word to add.
	 */
	public void add(Object text) {
		if (string.isEmpty()) {
			string = text.toString();
		} else {
			string += separator + text;
		}
	}

	/**
	 * @return true if the string joiner content is empty.
	 */
	public boolean isEmpty() {
		return string.isEmpty();
	}

	@Override
	public String toString() {
		return string;
	}

}
