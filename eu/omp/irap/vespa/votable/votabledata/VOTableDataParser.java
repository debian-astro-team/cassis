/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 * This file is a part of EpnTAPClient.
 * This program aims to provide EPN-TAP support for software clients, like CASSIS spectrum analyzer.
 * See draft specifications: https://voparis-confluence.obspm.fr/pages/viewpage.action?pageId=559861
 * Copyright (C) 2016 Institut de Recherche en Astrophysique et Planétologie.
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package eu.omp.irap.vespa.votable.votabledata;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import eu.omp.irap.vespa.epntapclient.votable.model.Data;
import eu.omp.irap.vespa.epntapclient.votable.model.Field;
import eu.omp.irap.vespa.epntapclient.votable.model.Table;
import eu.omp.irap.vespa.votable.utils.StringJoiner;
import eu.omp.irap.vespa.votable.votable.VOTableException;
import eu.omp.irap.vespa.votable.votable.VOTableException.VOTableIsNotValidException;

/**
 * @author N. Jourdane
 */
public class VOTableDataParser {

	/** The logger for the class */
	private static final Logger LOGGER = Logger.getLogger(VOTableDataParser.class.getName());

	/** The data to parse, as an VOTableData object. */
	private VOTableData data;

	/** The Data XML node in the VOTable, which contains the VOTabla data. */
	private Data dataNode;

	/**
	 * A list of all XML Field nodes of the VOTable, which contains informations about the columns,
	 * like their name.
	 */
	private List<Field> fieldsNodes;


	/**
	 * In VOTables, data can be stored in 4 different serializations: BINARY, BINARY2, TABLEDATA and
	 * FITS. This class aims to provide a unique way to get the data table, regardless of its
	 * serialization type. DO NOT PARSE DATA: data parsing requires parseData() call.
	 *
	 * @param datasetTitle The title of the data set.
	 * @param table The table on which we want get the data.
	 */
	public VOTableDataParser(String datasetTitle, Table table) {

		dataNode = table.getDATA();
		fieldsNodes = new ArrayList<>();

		for (Object obj : table.getFIELDOrPARAMOrGROUP()) {
			if (obj.getClass() == Field.class) {
				fieldsNodes.add((Field) obj);
			}
		}
		String[] columnsNames = getColumnsName(fieldsNodes);
		data = new VOTableData(datasetTitle, columnsNames);
		LOGGER.info("Columns names:  " + StringJoiner.join(columnsNames));
	}

	/**
	 * Parse the list of Field node and returns the list a columns names.
	 *
	 * @param fieldsNodes A list of all XML Field nodes of the VOTable, which contains informations
	 *            about the columns, like their name.
	 * @return The list of column names corresponding to the list of Field nodes.
	 */
	private static String[] getColumnsName(List<Field> fieldsNodes) {
		String[] columnsName = new String[fieldsNodes.size()];
		for (int i = 0; i < fieldsNodes.size(); i++) {
			columnsName[i] = fieldsNodes.get(i).getName();
		}
		return columnsName;
	}

	/**
	 * @return the data stored in the Table.
	 */
	public VOTableData getData() {
		return data;
	}

	/**
	 * Parse the data stored in the VOTable.
	 *
	 * @return The data stored in the VOTable, as a VOTableData object.
	 * @throws VOTableException The VOTable is not valid, or can not be parsed.
	 */
	public VOTableData parseData() throws VOTableException {
		DataParser parser;
		if (dataNode.getBINARY() != null) {
			parser = new BinaryStreamParser(dataNode.getBINARY().getSTREAM(), fieldsNodes);
		} else if (dataNode.getBINARY2() != null) {
			parser = new Binary2StreamParser(dataNode.getBINARY2().getSTREAM(), fieldsNodes);
		} else if (dataNode.getTABLEDATA() != null) {
			parser = new TableDataParser(dataNode.getTABLEDATA(), fieldsNodes);
		} else if (dataNode.getFITS() != null) {
			parser = new FitsParser(dataNode.getFITS(), fieldsNodes);
		} else {
			throw new VOTableIsNotValidException("Can not locate the data in the VOTable.");
		}
		data.setData(parser.parse());

		return data;
	}

	/**
	 * @return the fieldsNodes
	 */
	public List<Field> getFieldsNodes() {
		return fieldsNodes;
	}

}
