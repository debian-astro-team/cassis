/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 * This file is a part of EpnTAPClient.
 * This program aims to provide EPN-TAP support for software clients, like CASSIS spectrum analyzer.
 * See draft specifications: https://voparis-confluence.obspm.fr/pages/viewpage.action?pageId=559861
 * Copyright (C) 2016 Institut de Recherche en Astrophysique et Planétologie.
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package eu.omp.irap.vespa.votable.votable;

/**
 * VOTableException is the global exception for all the VOTable application. All other exceptions
 * should extend it.
 *
 * @author N. Jourdane
 */
public abstract class VOTableException extends Exception {

	/** The serial version UID. */
	private static final long serialVersionUID = 1L;


	/**
	 * Constructor of CantGetVOTableException with a message
	 *
	 * @param message The message describing the exception.
	 */
	public VOTableException(String message) {
		super(message);
	}

	/**
	 * Constructor of CantGetVOTableException with a message and the exception.
	 *
	 * @param message The message describing the exception.
	 * @param e The exception.
	 */
	public VOTableException(String message, Exception e) {
		super(message, e);
	}


	/** An exception thrown when we can not parse the VOTable data (ie. wrong column name). */
	public static class CanNotParseDataException extends VOTableException {

		/** The serial version UID. */
		private static final long serialVersionUID = 1L;


		/**
		 * @param reason A message explaining why we can't parse data.
		 */
		public CanNotParseDataException(String reason) {
			super("Can not parse data in the VOTable: " + reason);
		}

		/**
		 * @param reason A message explaining why we can't parse data.
		 * @param e The exception thrown.
		 */
		public CanNotParseDataException(String reason, Exception e) {
			super("Can not parse data in the VOTable: " + reason, e);
		}
	}

	/** An exception thrown when we can not get the VOTable, ie. an error in the query. */
	public static class CantAlterVOTableException extends VOTableException {

		/** The serial version UID. */
		private static final long serialVersionUID = 1L;


		/**
		 * @param reason A message explaining why we can't alter the VOTable.
		 * @param e The exception thrown.
		 */
		public CantAlterVOTableException(String reason, Exception e) {
			super("Can not alter the XML file of the VOTable: " + reason, e);
		}
	}

	/** An exception thrown when we can not get the VOTable, ie. an error in the query. */
	public static class CantDisplayVOTableException extends VOTableException {

		/** The serial version UID. */
		private static final long serialVersionUID = 1L;


		/**
		 * @param reason A message explaining why we can't display the VOTable.
		 */
		public CantDisplayVOTableException(String reason) {
			super("Can not display the VOTable: " + reason);
		}
	}

	/** An exception thrown when we can not get the VOTable, ie. an error in the query. */
	public static class CantGetVOTableException extends VOTableException {

		/** The serial version UID. */
		private static final long serialVersionUID = 1L;


		/**
		 * @param reason A message explaining why we can't get the VOTable.
		 */
		public CantGetVOTableException(String reason) {
			super("Can not get the VOTable: " + reason);
		}

		/**
		 * @param reason A message explaining why we can't get the VOTable.
		 * @param e The exception thrown.
		 */
		public CantGetVOTableException(String reason, Exception e) {
			super("Can not get the VOTable: " + reason, e);
		}
	}

	/** An exception thrown when we can not read the VOTable. */
	public static class CantParseVOTableException extends VOTableException {

		/** The serial version UID. */
		private static final long serialVersionUID = 1L;


		/**
		 * @param reason A message explaining why we can't get the VOTable.
		 * @param e The exception thrown.
		 */
		public CantParseVOTableException(String reason, Exception e) {
			super("Can not read the VOTable: " + reason, e);
		}
	}

	/** An exception thrown when we can not get the VOTable, ie. an error in the query. */
	public static class CantSaveVOTableException extends VOTableException {

		/** The serial version UID. */
		private static final long serialVersionUID = 1L;


		/**
		 * @param reason A message explaining why we can't get the VOTable.
		 * @param e The exception thrown.
		 */
		public CantSaveVOTableException(String reason, Exception e) {
			super("Can not save the VOTable: " + reason, e);
		}
	}

	/**
	 * An exception thrown when the VOTable is not valid, ie. it don't have the expected body.
	 */
	public static class VOTableIsNotValidException extends VOTableException {

		/** The serial version UID. */
		private static final long serialVersionUID = 1L;


		/**
		 * @param reason A message explaining why we can't get the VOTable.
		 */
		public VOTableIsNotValidException(String reason) {
			super("The VOTable is not valid: " + reason);
		}
	}
}
