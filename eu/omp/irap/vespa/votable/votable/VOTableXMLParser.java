/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 * This file is a part of EpnTAPClient.
 * This program aims to provide EPN-TAP support for software clients, like CASSIS spectrum analyzer.
 * See draft specifications: https://voparis-confluence.obspm.fr/pages/viewpage.action?pageId=559861
 * Copyright (C) 2016 Institut de Recherche en Astrophysique et Planétologie.
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package eu.omp.irap.vespa.votable.votable;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import eu.omp.irap.vespa.epntapclient.votable.model.VOTABLE;
import eu.omp.irap.vespa.votable.utils.XmlUtils;
import eu.omp.irap.vespa.votable.votable.VOTableException.CantParseVOTableException;

/**
 * @author N. Jourdane
 */
public final class VOTableXMLParser {

	/**
	 * The version of the VOTable with which VOTables will be parsed with JAXB (ie. the last
	 * published VOTable version by IVOA).
	 */
	private static final String REQUIRED_VOTABLE_VERSION = "1.3";

	/** The VOTable package where are the generated .java which represents the VOTable model. */
	private static final String VOTABLE_MODEL_PACKAGE = "eu.omp.irap.vespa.epntapclient.votable.model";

	/** The path of the VOTable to verify the VOTable XML file. */
	private static final String VOTABLE_SHEMA = "http://www.ivoa.net/xml/VOTable/v";


	/** Constructor to hide the implicit public one. */
	private VOTableXMLParser() {
	}

	/**
	 * @param voTablePath The path of the VOTable.
	 * @return The VOTable resulting of the XML document.
	 * @throws CantParseVOTableException Can not modify XML VOTable or parse th VOTable.
	 */
	public static VOTABLE parse(String voTablePath) throws CantParseVOTableException {
		VOTABLE voTable;
		JAXBContext jc;
		try {
			changeVOTableSchemaLocation(voTablePath);
		} catch (IOException e) {
			throw new CantParseVOTableException(
					"Can not modify the VOTable XML file " + voTablePath,
					e);
		}
		try {
			jc = JAXBContext.newInstance(VOTableXMLParser.VOTABLE_MODEL_PACKAGE);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			voTable = (VOTABLE) unmarshaller.unmarshal(new File(voTablePath));
		} catch (JAXBException e) {
			throw new CantParseVOTableException("Can not get a VOTable object from the XML file.",
					e);
		}

		return voTable;
	}

	/**
	 * Change the version of the VOTable to the last supported version (1.3 as 2016/02/05). This
	 * allows JAXB to parse the VOTable even it's not the same namespace than the XSD. Because
	 * VOTables schemas are retro-compatibles, there is no problem to parse a VOTable with a XML
	 * schema with a higher version. In the same way, this method check if the file is a valid XML.
	 *
	 * @param voTablePath The path of the VOTable XML file.
	 * @throws IOException Can not read or write the XML file.
	 */
	private static void changeVOTableSchemaLocation(String voTablePath) throws IOException {
		Map<String, String> attributes = new HashMap<>();
		attributes.put("xmlns", VOTABLE_SHEMA + REQUIRED_VOTABLE_VERSION);
		XmlUtils.changeRootAttributes(voTablePath, attributes);
	}
}
