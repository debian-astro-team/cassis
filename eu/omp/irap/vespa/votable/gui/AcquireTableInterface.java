/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 * This file is a part of EpnTAPClient.
 * This program aims to provide EPN-TAP support for software clients, like CASSIS spectrum analyzer.
 * See draft specifications: https://voparis-confluence.obspm.fr/pages/viewpage.action?pageId=559861
 * Copyright (C) 2016 Institut de Recherche en Astrophysique et Planétologie.
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package eu.omp.irap.vespa.votable.gui;

import eu.omp.irap.vespa.epntapclient.votable.model.VOTABLE;
import eu.omp.irap.vespa.votable.votable.VOTableException;
import eu.omp.irap.vespa.votable.votable.VOTableException.CantGetVOTableException;

/**
 * @author N. Jourdane
 */
public interface AcquireTableInterface {

	/**
	 * Display an error message to the user.
	 *
	 * @param message A short version of the message.
	 * @param error The exception related to the error.
	 */
	void displayError(String message, Exception error);

	/**
	 * Display an informative message to the user.
	 *
	 * @param shortMessage A short version of the message.
	 * @param detailledMessage The full message to display (optional, can be null).
	 */
	void displayInfo(String shortMessage, String detailledMessage);

	/**
	 * If the voTablePath is not present, download the VOTable.
	 *
	 * @throws CantGetVOTableException The VOTable can not be downloaded.
	 */
	void downloadVOTable() throws CantGetVOTableException;

	/**
	 * Fill the table in the view with the parsed table.
	 */
	void displayTable();

	/**
	 * @return The VOTable as a VOTABLE object (corresponding to the XSD).
	 */
	VOTABLE getVOTable();

	/**
	 * @return the path of the XML VOTable file.
	 */
	String getVOTablePath();

	/**
	 * Parse the data inside the VOTable (ie., from the stream) and append the data to voTableData.
	 *
	 * @throws VOTableException The VOTable data can not be parsed.
	 */
	void parseVOTableData() throws VOTableException;

	/**
	 * Parse the XML file of the VOTable to a VOTABLE object (a Java representation of the XML root
	 * element).
	 *
	 * @throws VOTableException The XML file of the VOTable can not be parsed.
	 */
	void parseVOTableXML() throws VOTableException;

	/**
	 * Set the cursor in the VOTable view.
	 *
	 * @param enableWaitMode True to set the Wait cursor, false to set the default cursor.
	 */
	void setWaitMode(boolean enableWaitMode);

	/**
	 * Return if an URL for a query is set.
	 *
	 * @return true if an URL for a query is set, false otherwise.
	 */
	boolean isUrlSet();
}
