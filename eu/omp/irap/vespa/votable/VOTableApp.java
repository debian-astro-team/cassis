/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 * This file is a part of EpnTAPClient.
 * This program aims to provide EPN-TAP support for software clients, like CASSIS spectrum analyzer.
 * See draft specifications: https://voparis-confluence.obspm.fr/pages/viewpage.action?pageId=559861
 * Copyright (C) 2016 Institut de Recherche en Astrophysique et Planétologie.
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package eu.omp.irap.vespa.votable;

import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import eu.omp.irap.vespa.votable.gui.VOTablePanelCtrl;
import eu.omp.irap.vespa.votable.utils.StringJoiner;

/**
 * Simple class to have a main function to display a voTable from a XML file.
 *
 * @author N. Jourdane
 */
public class VOTableApp {

	/** The message displayed to the user when the number of argument is incorrect. */
	private static final String WRONG_COMMAND = "Usage: VOtableApp path/to/VOTable.xml\n"
			+ "OR: VOtableApp http://url.to.service/or/registry type language \"YOUR QUERY\"";

	/** The logger for the class VOTableApp. */
	protected static final Logger LOGGER = Logger.getLogger(VOTableApp.class.getName());


	/** Private constructor to hide the implicit public one. */
	private VOTableApp() {
	}

	/**
	 * Main function to start the application as standalone.
	 *
	 * @param args The program arguments
	 */
	public static void main(final String[] args) {
		LOGGER.info("Lauching VOTable app with arguments: " + StringJoiner.join(args));

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				VOTablePanelCtrl ctrl = new VOTablePanelCtrl();
				if (args.length == 1) {
					ctrl.acquireVOTable(args[0]);
				} else if (args.length == 2) {
					ctrl.acquireVOTable(args[0], args[1]);
				} else {
					System.console().writer().println(VOTableApp.WRONG_COMMAND);
					return;
				}

				JFrame frame = new JFrame("VOTable app");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setContentPane(ctrl.getView());
				frame.setVisible(true);
				frame.setSize(800, 600);
				frame.setLocationRelativeTo(null);
			}
		});
	}

}
