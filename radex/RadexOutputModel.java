/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package radex;

/**
 * Class describing a result from RADEX model.
 */
public class RadexOutputModel {

	private String qnumM;
	private String qnumN;
	private double eup;
	private double spfreq;
	private double wavel;
	private double tex;
	private double taul;
	private double ta;
	private double xpopM;
	private double xpopN;
	private double kkms;
	private double ergs;


	public RadexOutputModel(String qnumM, String qnumN, double eup, double spfreq, double wavel,
				double tex, double taul, double ta, double xpopM, double xpopN, double kkms, double ergs){
		this.qnumM = qnumM;
		this.qnumN = qnumN;
		this.eup = eup;
		this.spfreq = spfreq;
		this.wavel = wavel;
		this.tex = tex;
		this.taul = taul;
		this.ta = ta;
		this.xpopM = xpopM;
		this.xpopN = xpopN;
		this.kkms = kkms;
		this.ergs = ergs;
	}

	public double getErgs() {
		return ergs;
	}

	public void setErgs(double ergs) {
		this.ergs = ergs;
	}

	public double getKkms() {
		return kkms;
	}

	public void setKkms(double kkms) {
		this.kkms = kkms;
	}

	public String getQnumM() {
		return qnumM;
	}

	public void setQnumM(String qnumM) {
		this.qnumM = qnumM;
	}

	public String getQnumN() {
		return qnumN;
	}

	public void setQnumN(String qnumN) {
		this.qnumN = qnumN;
	}

	public double getSpfreq() {
		return spfreq;
	}
	public void setSpfreq(double spfreq) {
		this.spfreq = spfreq;
	}

	public double getTa() {
		return ta;
	}

	public void setTa(double ta) {
		this.ta = ta;
	}

	public double getTaul() {
		return taul;
	}

	public void setTaul(double taul) {
		this.taul = taul;
	}

	public double getTex() {
		return tex;
	}

	public void setTex(double tex) {
		this.tex = tex;
	}

	public double getWavel() {
		return wavel;
	}

	public void setWavel(double wavel) {
		this.wavel = wavel;
	}

	public double getXpopM() {
		return xpopM;
	}
	public void setXpopM(double xpopM) {
		this.xpopM = xpopM;
	}

	public double getXpopN() {
		return xpopN;
	}

	public void setXpopN(double xpopN) {
		this.xpopN = xpopN;
	}

	public double getEup() {
		return eup;
	}

	public void setEup(double eup) {
		this.eup = eup;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RadexOutputModel [qnumM=" + qnumM + ", qnumN=" + qnumN + ", eup=" + eup + ", spfreq=" + spfreq
				+ ", wavel=" + wavel + ", tex=" + tex + ", taul=" + taul + ", ta=" + ta + ", xpopM=" + xpopM
				+ ", xpopN=" + xpopN + ", kkms=" + kkms + ", ergs=" + ergs + "]";
	}

}
