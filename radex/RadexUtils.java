/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package radex;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import radex.exception.OutOfBoundCollisionPartnersException;
import radex.exception.OutOfBoundCollisionRatesException;
import radex.exception.OutOfBoundCollisionTemperatureException;
import radex.exception.OutOfBoundEnergyLevelsException;
import radex.exception.OutOfBoundSpectralLinesException;

/**
 * Utility class for Radex/Collisions file.
 *
 * @author M. Boiziot
 */
public class RadexUtils {

	/**
	 * Validate the format of the given file.
	 *
	 * @param collisionFile The file.
	 * @return true if the file is correctly formated (event in case of
	 *  MoleculeException), false if there is {@link NumberFormatException}
	 * (file wrongly formated).
	 */
	public static boolean validateCollisionFile(File collisionFile) {
		boolean res = true;
		try {
			validateCollisionFileWithException(collisionFile);
		} catch (IOException | OutOfBoundEnergyLevelsException |
				OutOfBoundSpectralLinesException | OutOfBoundCollisionPartnersException |
				OutOfBoundCollisionRatesException | OutOfBoundCollisionTemperatureException e) {
			res = false;
		}
		return res;
	}

	/**
	 * Validate the format of the given file.
	 *
	 * @param collisionFile The file.
	 * @throws OutOfBoundEnergyLevelsException If there is a out of bound
	 *  energy level number.
	 * @throws OutOfBoundSpectralLinesException If there is a out of bound
	 *  lines number.
	 * @throws OutOfBoundCollisionPartnersException If there is a out of bound
	 *  collisions partners number.
	 * @throws OutOfBoundCollisionRatesException If there is a out of bound
	 *  number of collision.
	 * @throws OutOfBoundCollisionTemperatureException If there is a out of
	 *  bound temperature number.
	 * @throws IOException If case of read error.
	 */
	public static void validateCollisionFileWithException(File collisionFile)
			throws OutOfBoundEnergyLevelsException, OutOfBoundSpectralLinesException,
			OutOfBoundCollisionPartnersException, OutOfBoundCollisionRatesException,
			OutOfBoundCollisionTemperatureException, IOException {
		try (BufferedReader dataBR = new BufferedReader(new FileReader(collisionFile))) {
			ReadRadexFile rrf = new ReadRadexFile();
			rrf.readData(dataBR);
		}
	}

	/**
	 * Read the given file then return the list of collisions partners.
	 *
	 * @param directoryPath The directory where is the file.
	 * @param molfile The name of the file.
	 * @return the list of collisions partners.
	 */
	public static List<PartnerModel> getCollisionsPartners(String directoryPath, String molfile) {
		return ReadRadexFile.getCollisionsPartners(new File (directoryPath + File.separatorChar + molfile));
	}

}
