/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package radex;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author glorian
 *  @author thachtn
 */
public class ModeRadexManager {

	private static final Logger LOGGER = LoggerFactory.getLogger(ModeRadexManager.class);
	private Map<String, File> collisionFiles;
	private File collisionPropertiesPath;


	public static final String NB_MOLES_PROPERTY_V1 = "nbmoles";
	public static final String MOLECULE_CASSIS_PROPERTY_V1 = "mole";
	public static final String NB_RADEX_COLLISION_FILES_PROPERTY_V2 = "nbfichiers";
	public static final String NB_RADEX_COLLISION_FILES_PROPERTY_V1 = "nbfichiers_";
	public static final String FICHIER_LAMDA_PROPERTY_V2 = "fichier";
	public static final String FICHIER_LAMDA_PROPERTY_V1 = "fichier_";
	public static final String COLLISION_PROPERTY_V2 = "collision_";
	public static final String COLLISION_PROPERTY_V1 = "collision";
	public static final String TAGS_PROPERTY_V2 = "tag";
	public static final String TAGS_PROPERTY_V1 = "tag_";
	public static final String MOLES_RADEX_CASSIS_FILE = "molesRADEXCASSIS.properties";

	private List<MoleculeLAMDACASSIS> molecules = null;
//	private File defaultUserCollisionPath ;
	private File defaultCollisionPath ;

	/**
	 * Constructor
	 * @param customCollisionListFile
	 * @param collisionPropertiesFile
	 * @param defaultUserCollisionPath
	 * @param defaultCollisionPath
	 */
	public ModeRadexManager(File customCollisionListFile, File collisionPropertiesFile,
			File defaultUserCollisionPath, File defaultCollisionPath) {
		this.collisionFiles = getAllCollisionFiles(customCollisionListFile,
												   defaultUserCollisionPath, defaultCollisionPath);
		this.collisionPropertiesPath = collisionPropertiesFile;
//		this.defaultUserCollisionPath = defaultUserCollisionPath;
		this.defaultCollisionPath = defaultCollisionPath;
		molecules = loadMolesRadexProperties();
		molecules = checkRadexPropertiesFile(molecules);
	}

	/**
	 * Get all the collision files from lamda_user and lamda file and
	 * from list of file
	 * @return
	 */
	private Map<String, File> getAllCollisionFiles(File customCollisionListFile,
			File userCollisionPath, File defaultCollisionPath) {
		 Map<String, File>  collisionFileMap = new HashMap<String, File>();

		List<File> files = new ArrayList<File>();
		if (defaultCollisionPath.exists()) {
			fillDatFile(defaultCollisionPath.getAbsolutePath(), files);
		} else {
			LOGGER.warn("the collision files directory "+ defaultCollisionPath.getAbsolutePath() +
					" does not exists !");
		}
		if ( userCollisionPath.exists()) {
			fillDatFile(userCollisionPath.getAbsolutePath(), files);
		}
		Path path = Paths.get(customCollisionListFile.getAbsolutePath());
		if ( !path.toFile().exists()) {
			try {
				boolean created = path.toFile().createNewFile();
				if (created) {
					LOGGER.debug(path.toString() + "is created");
				}
			} catch (IOException e) {
				LOGGER.warn("Impossible to create " + path.toAbsolutePath().toString());
			}
		}
		List<String> readAllLines;
		try {
			readAllLines = Files.readAllLines(path, StandardCharsets.UTF_8);


			for (int i = 0; i < files.size(); i++) {
				final File file = files.get(i);
				if (!collisionFileMap.containsKey(file.getName())) {
					collisionFileMap.put(file.getName(), file);
				}
			}
			for (int i = 0; i < readAllLines.size(); i++) {
				final File file = new File(readAllLines.get(i));
				if (!collisionFileMap.containsKey(file.getName())) {
					collisionFileMap.put(file.getName(), file);
				}
			}
		} catch (IOException e) {
			LOGGER.warn("Impossible to read " + path, e);
		}

		return collisionFileMap;
	}

	/**
	 * @param ROOT
	 * @param files
	 */
	public void fillDatFile(String ROOT, final List<File> files) {
		FileVisitor<Path> fileProcessor = new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				if (file.toString().endsWith(".dat")){
					files.add(file.toFile());
				}
				return FileVisitResult.CONTINUE;
			}
		};
		try {
			Files.walkFileTree(Paths.get(ROOT), fileProcessor);
		} catch (IOException e) {
			LOGGER.warn("Problem with " + ROOT);
		}
	}


	/**
	 * Check that the collision in the molecules have the collision file in lamda_user or lamda path
	 * if not the collision file is removed
	 * if there is no collision file in the molecules then the molecules is removed
	 * @param molecules
	 * @return the list of molecules with file collision existing in the  lamda_user or lamda path
	 */
	public List<MoleculeLAMDACASSIS> checkRadexPropertiesFile(List<MoleculeLAMDACASSIS> molecules) {

		int nbMoles = molecules.size();
		for (int i = nbMoles - 1; i >= 0; i--) {
			MoleculeLAMDACASSIS mole = molecules.get(i);
			int nbCollisionFile = mole.getFichierLAMDA().size();

			for (int j = nbCollisionFile - 1; j >= 0; j--) {
				final String collisionFile = mole.getFichierLAMDA().get(j);
				if (!getCollisionFiles().keySet().contains(collisionFile)) {
					removeFileFromFromProperties(molecules, mole, collisionFile);
				}
			}
		}

		try {
			saveToPropertiesFile(molecules);
		} catch (IOException ioe) {
			LOGGER.error("Error while saving radex properties file", ioe);
		}
		return molecules;
	}


	/**
	 * @author thachtn remove lamda file from config file
	 * @param mole
	 * @param fileName
	 */
	private static void removeFileFromFromProperties(List<MoleculeLAMDACASSIS> molecules, MoleculeLAMDACASSIS mole,
			String fileName) {
		int nbCollisionFile = mole.getFichierLAMDA().size();
		for (int i = nbCollisionFile - 1; i >= 0; i--)
			if (mole.getFichierLAMDA().get(i).equals(fileName))
				mole.getFichierLAMDA().remove(i);

		if (mole.getFichierLAMDA().isEmpty())
			molecules.remove(mole);

	}

	public void saveToPropertiesFileV1(List<MoleculeLAMDACASSIS> molecules) throws IOException {
		File file = new File(collisionPropertiesPath.getAbsolutePath() + File.separator + MOLES_RADEX_CASSIS_FILE);
		try (BufferedWriter out = new BufferedWriter(new FileWriter(file))) {

			writeProperty(out,NB_MOLES_PROPERTY_V1 , Integer.toString(molecules.size()));
			out.write("#############");
			out.newLine();
			for (int i = 0; i < molecules.size(); i++) {
				MoleculeLAMDACASSIS mole = molecules.get(i);
				writeProperty(out, MOLECULE_CASSIS_PROPERTY_V1 + (i + 1), mole.getCASSISName());
				if (mole.getFichierLAMDA().size() > 1)
					writeProperty(out, NB_RADEX_COLLISION_FILES_PROPERTY_V1 + mole.getCASSISName(),
							Integer.toString(mole.getFichierLAMDA().size()));
				for (int j = 0; j < mole.getFichierLAMDA().size(); j++) {
					writeProperty(out, FICHIER_LAMDA_PROPERTY_V1 + (j + 1) + mole.getCASSISName(), mole
							.getFichierLAMDA().get(j));
				}
				writeProperty(out, COLLISION_PROPERTY_V1 + mole.getCASSISName(), mole.getCollision());
				writeProperty(out, TAGS_PROPERTY_V1 + mole.getCASSISName(), mole.getTags());
				out.write("#############");
				out.newLine();
			}
			out.flush();
		}
	}

	public void saveToPropertiesFile(List<MoleculeLAMDACASSIS> molecules) throws IOException {
		File file = new File(collisionPropertiesPath.getAbsolutePath() + File.separator + MOLES_RADEX_CASSIS_FILE);
		try (BufferedWriter out = new BufferedWriter(new FileWriter(file))) {

			writeProperty(out, "VERSION" , "2");
			out.write("##########################");
			out.newLine();
			for (int i = 1; i < molecules.size()+1; i++) {
				MoleculeLAMDACASSIS mole = molecules.get(i-1);

				if (!mole.getFichierLAMDA().isEmpty()) {
					writeProperty(out, TAGS_PROPERTY_V2 + i, mole.getTags());
					writeProperty(out, NB_RADEX_COLLISION_FILES_PROPERTY_V2 + i,
							Integer.toString(mole.getFichierLAMDA().size()));
					for (int j = 1; j < mole.getFichierLAMDA().size()+1; j++) {
						writeProperty(out, FICHIER_LAMDA_PROPERTY_V2 + i + "_" + j, mole
								.getFichierLAMDA().get(j-1));
					}
				out.write("##########################");
				}
				out.newLine();
			}
			out.flush();
		}
	}




	private void writeProperty(BufferedWriter out, String key, String val) throws IOException {
		out.write(key + "=" +val);
		out.newLine();
	}


	/**
	 * @return the list of MoleculeLAMDACASSIS
	 */
	public List<MoleculeLAMDACASSIS> loadMolesRadexProperties() {
		List<MoleculeLAMDACASSIS> res = new ArrayList<MoleculeLAMDACASSIS>();

		Properties properties = new Properties();
		String fullPath = collisionPropertiesPath.getAbsolutePath() + File.separator + MOLES_RADEX_CASSIS_FILE;
		try (FileReader reader = new FileReader(fullPath )) {
			properties.load(reader);
			String version = properties.getProperty("VERSION");
			if (version == null) {
				res = readVersion1(properties);
				convertV1ToCurrentVersion(res);
			}else if ("2".equals(version)) {
				res = readVersion2(properties);

			}else {

			}
		} catch (IOException e) {
			LOGGER.debug("Error while reading the Properties file {}", fullPath, e);
			properties = null;
		}
		return res;
	}


	private void convertV1ToCurrentVersion(List<MoleculeLAMDACASSIS> molCollision) throws IOException {
		File file = new File(collisionPropertiesPath.getAbsolutePath() + File.separator  + MOLES_RADEX_CASSIS_FILE);
		try (BufferedWriter out = new BufferedWriter(new FileWriter(file))) {
			writeProperty(out, "VERSION", "2");
			out.write("#############");
			out.newLine();
			int nbTag = 1;
			for (int i = 0; i < molCollision.size(); i++) {
				MoleculeLAMDACASSIS mole = molCollision.get(i);
				if (! mole.getFichierLAMDA().isEmpty()) {
					for (String tag : mole.getListTag()) {
						writeProperty(out, TAGS_PROPERTY_V1 + nbTag, tag);

						for (int j = 0; j < mole.getFichierLAMDA().size(); j++) {
						writeProperty(out, TAGS_PROPERTY_V1 + nbTag +  "_"+ COLLISION_PROPERTY_V1 + Integer.toString(j+1),
								mole.getFichierLAMDA().get(j));
						}
						nbTag++;
						out.write("#############");
						out.newLine();
					}

				}

			}
			out.flush();
		}
	}

	/**
	 * @param properties
	 * @return  the list of MoleculeLAMDACASSIS
	 */
	public List<MoleculeLAMDACASSIS> readVersion2(Properties properties) {
		List<MoleculeLAMDACASSIS> molecules = new ArrayList<MoleculeLAMDACASSIS>();
		int numMol = 1;
		boolean nextMol = properties.containsKey(TAGS_PROPERTY_V2 + numMol);
		while (nextMol) {
			String tag = properties.getProperty(TAGS_PROPERTY_V2 + numMol);
			String nbFichiersString = properties.getProperty(NB_RADEX_COLLISION_FILES_PROPERTY_V2 + numMol);
			int nbFichiers = 1;
			if (nbFichiersString != null) {
				nbFichiers = Integer.valueOf(nbFichiersString);
			}
			List<String> listFichierLAMDA = new ArrayList<String>();
			String collisionParameter = "";
			for (int j = 1; j < nbFichiers + 1; j++) {
				String fichierLAMDA = properties.getProperty(FICHIER_LAMDA_PROPERTY_V2 + numMol + "_" + j);
				listFichierLAMDA.add(fichierLAMDA);
				final File file = getCollisionFiles().get(fichierLAMDA);
				String collisionsPartnersString = "";
				if (file != null) {
					String collisionPath = file.getParent();
					List<PartnerModel> collisionsPartners = RadexUtils.getCollisionsPartners(collisionPath, fichierLAMDA);
	//				System.out.println(fichierLAMDA);
					if (collisionsPartners != null) {
						for (PartnerModel partnerModel : collisionsPartners) {
							collisionsPartnersString = collisionsPartnersString + "&(" +
									partnerModel.getTypePartner()+ ";" + partnerModel.getDensity() + ")";
						}
					}
				}
				collisionParameter = fichierLAMDA + collisionsPartnersString;
			}
			MoleculeLAMDACASSIS mole = new MoleculeLAMDACASSIS(String.valueOf(numMol), listFichierLAMDA, collisionParameter, tag);
			molecules.add(mole);
			numMol++;
			nextMol = properties.containsKey(TAGS_PROPERTY_V2 + numMol);
		}
		return molecules;
	}


	/**
	 * @param properties
	 * @return  the list of MoleculeLAMDACASSIS
	 */
	public List<MoleculeLAMDACASSIS> readVersion1(Properties properties) {
		List<MoleculeLAMDACASSIS> molecules = new ArrayList<MoleculeLAMDACASSIS>();
		int nbMoles = Integer.parseInt(properties.getProperty(NB_MOLES_PROPERTY_V1));
		for (int i = 1; i < nbMoles + 1; i++) {
			String cassisName = properties.getProperty(MOLECULE_CASSIS_PROPERTY_V1 + i);
			String nbFichiersString = properties.getProperty(NB_RADEX_COLLISION_FILES_PROPERTY_V1
					+ cassisName);
			String collision = properties.getProperty(COLLISION_PROPERTY_V1 + cassisName);
			String tags = properties.getProperty(TAGS_PROPERTY_V1 + cassisName);
			int nbFichiers = 1;
			if (nbFichiersString != null)
				nbFichiers = Integer.parseInt(nbFichiersString);
			List<String> listFichierLAMDA = new ArrayList<String>();
			for (int j = 1; j < nbFichiers + 1; j++) {
				String fichierLAMDA = properties.getProperty(FICHIER_LAMDA_PROPERTY_V1 + j
						+ cassisName);
				listFichierLAMDA.add(fichierLAMDA);
			}
			MoleculeLAMDACASSIS mole = new MoleculeLAMDACASSIS(cassisName, listFichierLAMDA,
					collision, tags);
			molecules.add(mole);
		}
		return molecules;
	}

	public List<MoleculeLAMDACASSIS> getMolecules() {
		return molecules;
	}

	/**
	 * Substring comma when molName have a comma.
	 *
	 * @param molName The molecule name.
	 * @return String The molecule without the part after the comma.
	 */
	public  String subStringCommaInName(String molName) {
		validateSpaceInName(molName);
		if (molName.indexOf(',') != -1) {
			return molName.substring(0, molName.indexOf(','));
		} else {
			return molName;
		}
	}

	/**
	 * Replace space in molecule name by underscore.
	 *
	 * @param molName The molecule name.
	 * @return the valide name of the molecule.
	 */
	public String validateSpaceInName(String molName) {
		return molName.replaceAll(" ", "_");
	}

	public  boolean isRADEXMolecules(String tag) {
		for (int i = 0; i < molecules.size(); i++) {
			MoleculeLAMDACASSIS mole = molecules.get(i);
			if (mole.getListTag().contains(tag))
				return true;
		}
		return false;
	}

	/**
	 * @param tag the tag of the molecules
	 * return the names of the collision lamda file
	 */
	public List<String> cassisNameToMoleFile(int tag) {
		List<String> res = new ArrayList<String>();
		for (int i = 0; i < molecules.size(); i++) {
			MoleculeLAMDACASSIS mole = molecules.get(i);
			if (mole.getListTag().contains(String.valueOf(tag)))
				res.addAll(mole.getFichierLAMDA());
		}

		return res;

	}

	public boolean addNewRADEXMoleculeIntoCASSIS(File collisionFile,
			String cassisMoleName, String tag, String collision) throws IOException {
		String collisionFileName = collisionFile.getName();
		boolean addInNewMolecule = true;
		boolean duplicate = false;
		for (int i = 0; !duplicate && i < molecules.size(); i++) {
			MoleculeLAMDACASSIS mole = molecules.get(i);
			List<String> listFichierLAMDA = mole.getFichierLAMDA();
			if (mole.getListTag().contains(tag)) {
				// Molecule is already in lamda;
				for (int j = 0; !duplicate && j < listFichierLAMDA.size(); j++) {
					String fichier = listFichierLAMDA.get(j);
					if (fichier.equals(collisionFileName)) {
						LOGGER.info("Collision file already exists for this molecule.");
						duplicate = true;
					}
				}

				// Ajouter le fichier de collision correspondant la molecule
				listFichierLAMDA.add(collisionFileName);
//				saveToPropertiesFile(molecules);
				addInNewMolecule = false;
			}
		}
		if (!duplicate) {
			if (addInNewMolecule) {
				// Molecule not in the radex molecules files
				List<String> listFichierLAMDA = new ArrayList<String>();
				listFichierLAMDA.add(collisionFileName);
				String name = subStringCommaInName(cassisMoleName);
				MoleculeLAMDACASSIS mole = new MoleculeLAMDACASSIS(name, listFichierLAMDA, collision, tag);
				molecules.add(mole);
			}
			saveToPropertiesFile(molecules);
			collisionFiles.put(collisionFileName, collisionFile);
		}
		return !duplicate;
	}


	/**
	 * @param collisionFileName
	 * @return true if the collisionFileName exist, False otherwise
	 */
	public boolean isAlreadyExist(String collisionFileName) {
		boolean duplicate = false;

		// Check molecule existe dans le fichier property
		for (int i = 0; !duplicate && i < molecules.size(); i++) {
			MoleculeLAMDACASSIS mole = molecules.get(i);
			List<String> listFichierLAMDA = mole.getFichierLAMDA();

			for (int j = 0; !duplicate && j < listFichierLAMDA.size(); j++) {
				String fichier = listFichierLAMDA.get(j);
				if (fichier.equals(collisionFileName)) {
					LOGGER.info("Collision file already exists.");
					duplicate = true;
				}
			}
		}
		return duplicate;
	}

	public boolean deleteLamdaFileInCassis(String collisionFileName, int tag)
			throws IOException {

		boolean delete = false;

		// Check molecule existe dans le fichier property
		for (int i = 0; !delete && i < molecules.size(); i++) {
			MoleculeLAMDACASSIS mole = molecules.get(i);
			if (mole.getListTag().contains(String.valueOf(tag))) {
				// Molecule is already in lamda;
				List<String> listFichierLAMDA = mole.getFichierLAMDA();
				for (int j = 0; !delete && j < listFichierLAMDA.size(); j++) {
					String fichier = listFichierLAMDA.get(j);
					if (fichier.equals(collisionFileName)) {
						listFichierLAMDA.remove(j);
						if (listFichierLAMDA.isEmpty()) {
							molecules.remove(i);
						}
						delete = true;
					}
				}
			}
		}
		if (delete) {
			saveToPropertiesFile(molecules);
		} else {
			LOGGER.info("Collision file or Moelcule does not exist");
		}
		return delete;
	}

	/**
	 * get the name of the collisionner
	 * @param tag tag of the molecule
	 * @return the name of the collisionner
	 */
	public  String getCollisionParDefault(int tag) {
		String res = null;
		for (int i = 0; i < molecules.size(); i++) {
			MoleculeLAMDACASSIS mole = molecules.get(i);
			if (mole.getListTag().contains(String.valueOf(tag)))
				return mole.getCollision();
		}

		return res;
	}

	/**
	 * @return the collisionFiles
	 */
	public Map<String, File> getCollisionFiles() {
		return collisionFiles;
	}

	/**
	 * @return the collisionFiles
	 */
	public File getPathCollisionFile(String fileName) {
		return collisionFiles.get(fileName);
	}

	public List<PartnerModel> getPartners(String moleFile) {
		File file = getPathCollisionFile(moleFile);
		List<PartnerModel> listPartners = new ArrayList<PartnerModel>();
		if (file != null) {
			listPartners = RadexUtils.getCollisionsPartners(file.getParent(), moleFile);
		}

		return listPartners;
	}

	public boolean isInCollisionCassisPath(String collisionFile) {
		String pathCollisionFile = getPathCollisionFile(collisionFile).getAbsolutePath();
		return pathCollisionFile.startsWith(defaultCollisionPath.getAbsolutePath());
	}
}
