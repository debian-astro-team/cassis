/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package radex;

import java.util.List;

/**
 * Class describing parameters needed for RADEX model.
 */
public class RadexInputModel {

	private String radat;
	private String moleFile;
	private double fmin;
	private double fmax;
	private double tkin;
	private int npart;
	private List<PartnerModel> partners;
	private double tbg;
	private double cdmol;
	private double deltav;
	private int geometry;


	/**
	 * Empty constructor.
	 */
	public RadexInputModel() {
	}

	/**
	 * Full constructor.
	 *
	 * @param fmin The minimum frequency (in GHz).
	 * @param fmax The maximum frequency (in GHz).
	 * @param tbg The Tbg (in Kelvin).
	 * @param geometry The geometry.
	 * @param radat The path of the folder with collision file.
	 * @param moleFile The filename.
	 * @param npart The number of collisions partners.
	 * @param tkin The kinetic temperature (in Kelvin).
	 * @param deltav The deltav/FWHM (in km/s).
	 * @param cdmol The density of the molecule.
	 * @param partners The collisions partners.
	 */
	public RadexInputModel(double fmin, double fmax, double tbg, int geometry,
			String radat, String moleFile, int npart, double tkin, double deltav,
			double cdmol, List<PartnerModel> partners) {
		this.fmin = fmin;
		this.fmax = fmax;
		this.tbg = tbg;
		this.geometry = geometry;
		this.radat = radat;
		this.moleFile = moleFile;
		this.npart = npart;
		this.tkin = tkin;
		this.deltav = deltav;
		this.cdmol = cdmol;
		this.partners = partners;
	}

	/**
	 * Return the density of the molecule.
	 *
	 * @return the density of the molecule.
	 */
	public double getCdmol() {
		return cdmol;
	}

	/**
	 * Change the density of the molecule.
	 *
	 * @param cdmol The new density of the molecule to set.
	 */
	public void setCdmol(double cdmol) {
		this.cdmol = cdmol;
	}

	/**
	 * Return the deltav/FWHM (in km/s).
	 *
	 * @return the deltav/FWHM (in km/s).
	 */
	public double getDeltav() {
		return deltav;
	}

	/**
	 * Change the deltav/FWHM.
	 *
	 * @param deltav The new deltav/FWHM (in km/s) to set.
	 */
	public void setDeltav(double deltav) {
		this.deltav = deltav;
	}

	/**
	 * Return the maximum frequency (in GHz).
	 *
	 * @return the maximum frequency (in GHz).
	 */
	public double getFmax() {
		return fmax;
	}

	/**
	 * Change the maximum frequency.
	 *
	 * @param fmax The new maximum frequency (in GHz) to set.
	 */
	public void setFmax(double fmax) {
		this.fmax = fmax;
	}

	/**
	 * Return the minimum frequency (in GHz).
	 *
	 * @return the minimum frequency (in GHz).
	 */
	public double getFmin() {
		return fmin;
	}

	/**
	 * Change the minimum frequency.
	 *
	 * @param fmin The new minimum frequency (in GHz) to set.
	 */
	public void setFmin(double fmin) {
		this.fmin = fmin;
	}

	/**
	 * Return the number of collisions partners.
	 *
	 * @return the number of collisions partners.
	 */
	public int getNpart() {
		return npart;
	}

	/**
	 * Change the number of collisions partners.
	 *
	 * @param npart The new number of collisions partners to set.
	 */
	public void setNpart(int npart) {
		this.npart = npart;
	}

	/**
	 * Return the collisions partners.
	 *
	 * @return the collisions partners.
	 */
	public List<PartnerModel> getPartners() {
		return partners;
	}

	/**
	 * Change the collisions partners.
	 *
	 * @param partners The news collisions partners to set.
	 */
	public void setPartners(List<PartnerModel> partners) {
		this.partners = partners;
	}

	/**
	 * Return the tbg (in Kelvin).
	 *
	 * @return the tbg (in Kelvin).
	 */
	public double getTbg() {
		return tbg;
	}

	/**
	 * Change the tbg.
	 *
	 * @param tbg The news tbg (in Kelvin) to set.
	 */
	public void setTbg(double tbg) {
		this.tbg = tbg;
	}

	/**
	 * Return the kinetic temperature (in Kelvin).
	 *
	 * @return the kinetic temperature (in Kelvin).
	 */
	public double getTkin() {
		return tkin;
	}

	/**
	 * Change the kinetic temperature.
	 *
	 * @param tkin The new kinetic temperature (in Kelvin) to set.
	 */
	public void setTkin(double tkin) {
		this.tkin = tkin;
	}

	/**
	 * Return the geometry.
	 *
	 * @return the geometry.
	 */
	public int getGeometry() {
		return geometry;
	}

	/**
	 * Change the geometry.
	 *
	 * @param geometry The new geometry to set.
	 */
	public void setGeometry(int geometry) {
		this.geometry = geometry;
	}

	/**
	 * Return the filename.
	 *
	 * @return the filename.
	 */
	public String getMoleFile() {
		return moleFile;
	}

	/**
	 * Change the filename.
	 *
	 * @param moleFile The new filename to set.
	 */
	public void setMoleFile(String moleFile) {
		this.moleFile = moleFile;
	}

	/**
	 * Return the path of the folder with the collision file
	 *  (known as radat parameter in RADEX Fortran in radex.inc file).
	 *
	 * @return the path of the folder with the collision file.
	 */
	public String getRadat() {
		return radat;
	}

	/**
	 * Change the path of the folder with the collision file
	 *  (known as radat parameter in RADEX Fortran in radex.inc file).
	 *
	 * @param radat The new path of the folder with the collision file.
	 */
	public void setRadat(String radat) {
		this.radat = radat;
	}
}
