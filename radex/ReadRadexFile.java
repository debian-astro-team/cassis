/**
 * Copyright IRAP (CNRS/UPS/CNES)
 * 
 * The complete list of authors can be seen on CASSIS website at
 * the following URL "http://cassis.irap.omp.eu/?page=authors".
 * 
 * This software is a computer program whose purpose is to display, analyze
 * and create model of astrophysical spectra. It allow to use database and
 * related virtual observatory tools.
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package radex;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import radex.exception.OutOfBoundCollisionPartnersException;
import radex.exception.OutOfBoundCollisionRatesException;
import radex.exception.OutOfBoundCollisionTemperatureException;
import radex.exception.OutOfBoundEnergyLevelsException;
import radex.exception.OutOfBoundSpectralLinesException;


/**
 * Class to read radex file, must be used with static functions.
 */
public class ReadRadexFile {


	/**
	 * The maximum number of collisions partners.
	 */
	private static int maxPart = 7;

	/**
	 * The maximum number of collision temperatures.
	 */
	private static int maxTemp = 99;

	/**
	 * The maximum number of energy levels.
	 */
	private static int maxLev = 305;

	/**
	 * The maximum number of radiative transitions.
	 */
	private static int maxLine = 35000;

	/**
	 * The maximum number of collisional transitions.
	 */
	private static int maxColl = 35000;


//	private double[] eterm; // energy levels (1/cm)
	private List<PartnerModel> listPartners;


	public ReadRadexFile() {
	}

	/**
	 * Return the list of collisions partners.
	 *
	 * @param file The file to read.
	 * @return the list of collisions parteners.
	 */
	public static List<PartnerModel> getCollisionsPartners(File file) {

		List<PartnerModel> res = null;
		if (file != null && file.exists()) {
			try (BufferedReader dataBR = new BufferedReader(new FileReader(file))) {
				res = getCollisionsPartners(dataBR);

			} catch (OutOfBoundEnergyLevelsException | OutOfBoundSpectralLinesException |
					OutOfBoundCollisionPartnersException | OutOfBoundCollisionRatesException |
					OutOfBoundCollisionTemperatureException | IOException e) {
				e.printStackTrace();
			}
		}

		return res;
	}

	/**
	 * @param dataBR
	 * @return the list of PartnerModel
	 * @throws IOException
	 * @throws OutOfBoundEnergyLevelsException
	 * @throws OutOfBoundSpectralLinesException
	 * @throws OutOfBoundCollisionPartnersException
	 * @throws OutOfBoundCollisionRatesException
	 * @throws OutOfBoundCollisionTemperatureException
	 */
	public static List<PartnerModel> getCollisionsPartners(BufferedReader dataBR) throws IOException,
			OutOfBoundEnergyLevelsException, OutOfBoundSpectralLinesException, OutOfBoundCollisionPartnersException,
			OutOfBoundCollisionRatesException, OutOfBoundCollisionTemperatureException {
		List<PartnerModel> res;
		ReadRadexFile rrf1 = new ReadRadexFile();
		rrf1.readData(dataBR);
		res = rrf1.listPartners;
		return res;
	}




	public static int getMaxPart() {
		return maxPart;
	}


	public static int getMaxTemp() {
		return maxTemp;
	}


	public static int getMaxLev() {
		return maxLev;
	}


	public static int getMaxLine() {
		return maxLine;
	}


	public static int getMaxColl() {
		return maxColl;
	}

	/**
	 * Change the maximum number of collisions partners allowed.
	 *
	 * @param maxPart The new maximum number of collisions partners to set.
	 */
	public static void setMaxPart(int maxPart) {
		ReadRadexFile.maxPart = maxPart;
	}

	/**
	 * Change the maximum number of collision temperatures allowed.
	 *
	 * @param maxTemp The new maximum number of collision temperatures to set.
	 */
	public static void setMaxTemp(int maxTemp) {
		ReadRadexFile.maxTemp = maxTemp;
	}

	/**
	 * Change the maximum number of energy levels allowed.
	 *
	 * @param maxLev The new maximum number of energy levels to set.
	 */
	public static void setMaxLev(int maxLev) {
		ReadRadexFile.maxLev = maxLev;
	}

	/**
	 * Change the maximum number of radiative transitions allowed.
	 *
	 * @param maxLine The new maximum number of radiative transitions to set.
	 */
	public static void setMaxLine(int maxLine) {
		ReadRadexFile.maxLine = maxLine;
	}

	/**
	 * Change the maximum number of collisional transitions allowed.
	 *
	 * @param maxColl The new maximum number of collisional transitions to set.
	 */
	public static void setMaxColl(int maxColl) {
		ReadRadexFile.maxColl = maxColl;
	}

	/**
	 * @param dataBR
	 * @throws IOException
	 * @throws OutOfBoundEnergyLevelsException
	 * @throws OutOfBoundSpectralLinesException
	 * @throws OutOfBoundCollisionPartnersException
	 * @throws OutOfBoundCollisionRatesException
	 * @throws OutOfBoundCollisionTemperatureException
	 */
	public void readData(BufferedReader dataBR) throws IOException, OutOfBoundEnergyLevelsException,
			OutOfBoundSpectralLinesException, OutOfBoundCollisionPartnersException, OutOfBoundCollisionRatesException,
			OutOfBoundCollisionTemperatureException {
		int[] id = new int[ReadRadexFile.maxPart + 1];
		readNameOftheMolecule(dataBR);
		readMassMolecular(dataBR);

		readEnergyLevels(dataBR);
		readRadiativeTransitions(dataBR);
		int npart = readNbPartner(dataBR);

		listPartners = new ArrayList<PartnerModel>();
		for (int ipart = 1; ipart < npart + 1; ipart++) {
			final PartnerModel partnerCollision = getPartnerCollision(dataBR, ipart, id);
			listPartners.add(partnerCollision);
			int ncoll = readNbCollTrans(dataBR);
			int ntemp = readNbCollTemps(dataBR);

			double[] readTemps = readTemps(dataBR, ntemp);
			partnerCollision.setKineticTemperatures(readTemps);
			readCollTrans(dataBR, ncoll);
		}
	}

	/**
	 * @param dataBR
	 * @param ncoll
	 * @throws IOException
	 */
	private void readCollTrans(BufferedReader dataBR, int ncoll) throws IOException {
		dataBR.readLine(); //!TRANS+ UP+ LOW+ COLLRATES(cm^3 s^-1)
		for (int icoll = 1; icoll < ncoll + 1; icoll++) {
			dataBR.readLine();
		}
	}

	/**
	 * @param dataBR
	 * @param ntemp
	 * @throws IOException
	 */
	private double[] readTemps(BufferedReader dataBR, int ntemp) throws IOException {
		dataBR.readLine(); //!COLL TEMPS
		String line;
		line = dataBR.readLine();
		line = ReadRadexFile.reduce(line);
		double[] temp = new double[ntemp]; // collision temperatures

		String[] ss = line.split(" ");
		for (int itemp = 0; itemp < ntemp; itemp++) {
			temp[itemp] = Double.parseDouble(ss[itemp]);
		}
		return temp;
	}

	/**
	 * @param dataBR
	 * @throws IOException
	 * @throws OutOfBoundCollisionTemperatureException
	 */
	private int readNbCollTemps(BufferedReader dataBR) throws IOException, OutOfBoundCollisionTemperatureException {
		dataBR.readLine();
		int ntemp = Integer.parseInt(dataBR.readLine().trim());
		if (ntemp < 0 || ntemp > ReadRadexFile.maxTemp) {
			throw new OutOfBoundCollisionTemperatureException(ntemp);

		}
		return ntemp;
	}

	/**
	 * @param dataBR
	 * @throws IOException
	 * @throws OutOfBoundCollisionRatesException
	 */
	private int readNbCollTrans(BufferedReader dataBR) throws IOException, OutOfBoundCollisionRatesException {
		dataBR.readLine();
		int ncoll = Integer.parseInt(dataBR.readLine().trim()); //!NUMBER OF COLL TRANS
		if (ncoll < 1 || ncoll > ReadRadexFile.maxColl) {
			throw new OutOfBoundCollisionRatesException(ncoll);
		}
		return ncoll;
	}

	/**
	 * @param dataBR
	 * @throws IOException
	 * @throws OutOfBoundCollisionPartnersException
	 */
	private int  readNbPartner(BufferedReader dataBR) throws IOException, OutOfBoundCollisionPartnersException {
		/* Number of collision partners */
		dataBR.readLine();
		int npart = Integer.parseInt(dataBR.readLine().trim());
		if (npart < 1 || npart > ReadRadexFile.maxPart) {
			throw new OutOfBoundCollisionPartnersException(npart);
		}
		return npart;
	}

	/**
	 * @param dataBR
	 * @throws IOException
	 */
	private double readMassMolecular(BufferedReader dataBR) throws IOException {
		dataBR.readLine(); //!MOLECULAR WEIGHT
		return Double.parseDouble(dataBR.readLine().trim()); // molecular mass
	}

	/**
	 * @param dataBR
	 * @throws IOException
	 */
	private String readNameOftheMolecule(BufferedReader dataBR) throws IOException {

		dataBR.readLine(); //!MOLECULE
		String readLine = dataBR.readLine(); // name of the molecule
		return readLine;
	}

	/**
	 * Read the energy levels.
	 *
	 * @param dataBR The {@link BufferedReader} used to read the energy levels.
	 * @throws IOException If case of IO error.
	 * @throws OutOfBoundEnergyLevelsException If there is a problem with the energy levels.
	 */
	private int readEnergyLevels(BufferedReader dataBR)
			throws IOException, OutOfBoundEnergyLevelsException {
		int ilev;
		dataBR.readLine();
		int nlev = Integer.parseInt(dataBR.readLine().trim());
		if (nlev < 1 || nlev > ReadRadexFile.maxLev) {
			throw new OutOfBoundEnergyLevelsException(nlev);
		}
		double[] eterm = new double[nlev];
		double[] gstat = new double[nlev];
		String[] qnum = new String[nlev];
		/* Term energies and statistical weights */
		dataBR.readLine();
		for (ilev = 0; ilev < nlev ; ilev++) {
			String nextLine = dataBR.readLine();
			nextLine = ReadRadexFile.reduce(nextLine);
			String[] termEnergie = nextLine.split(" ");
			eterm[ilev] = Double.parseDouble(termEnergie[1]);
			gstat[ilev] = Double.parseDouble(termEnergie[2]);
			qnum[ilev] = readQuanticNumber(nextLine);
		}
		return nlev;
	}

	/**
	 * Read and format quantic numbers.
	 *
	 * @param line The line to read the QN.
	 * @return The formated QN.
	 */
	public static String readQuanticNumber(String line) {
		line = ReadRadexFile.reduce(line);
		String[] termEnergie = line.split(" ");
		String[] val = Arrays.copyOfRange(termEnergie, 3, termEnergie.length);
		String res = "";
		for (int i = 0; i < val.length; i++) {
			String[] split = val[i].split("/");
			if (split.length == 2) {
				val[i] = String.valueOf(Double.parseDouble(split[0])
						/ Double.parseDouble(split[1]));
			}
			res = res + "_" + val[i];
		}
		return res.substring(1);
	}

	/**
	 * Reduce the given line.
	 *
	 * @param line The line.
	 * @return the reduced line.
	 */
	private static String reduce(String line){
		line = line.replace('\t', ' ');
		line = line.trim();
		while (line.indexOf("  ") != -1) {
			line = line.replaceAll("  ", " ");
		}
		return line;
	}

	/**
	 * Read radiative transitions.
	 *
	 * @param dataBR The {@link BufferedReader} used to read the radiative transitions.
	 * @throws IOException If case of IO error.
	 * @throws OutOfBoundSpectralLinesException If there is no lines or if the
	 *  number is superior to the max number of line allowed in {@link #maxLine}.
	 */
	private int readRadiativeTransitions(BufferedReader dataBR)
			throws IOException, OutOfBoundSpectralLinesException {
		int iline;
		/* Radiative upper & lower levels and Einstein coefficients */
		dataBR.readLine();

		int nline = Integer.parseInt(dataBR.readLine().trim());
		dataBR.readLine();

		if (nline < 1 || nline > ReadRadexFile.maxLine) {
			throw new OutOfBoundSpectralLinesException(nline);
		}
		int[] iupp = new int[nline];
		int[] ilow = new int[nline];
		double[] aeinst = new double[nline];
		double[] spfreq = new double[nline];
		double[] eup = new double[nline];
//		double[] xnu = new double[nline];

		for (iline = 0; iline < nline; iline++) {
			String[] ss = ReadRadexFile.reduce(dataBR.readLine()).split(" ");
			iupp[iline] = Integer.parseInt(ss[1]);
			ilow[iline] = Integer.parseInt(ss[2]);
			aeinst[iline] = Double.parseDouble(ss[3]);
			spfreq[iline] = Double.parseDouble(ss[4]);
			eup[iline] = Double.parseDouble(ss[5]);
//			xnu[iline] = eterm[iupp[iline]] - eterm[ilow[iline]];
		}
		return nline;
	}

	/**
	 * Add partner collision.
	 *
	 * @param dataBR The {@link BufferedReader} used to read the partner collision.
	 * @param ipart Index of value in id.
	 * @param id The id array.
	 * @return The collider detected
	 * @throws IOException If case of IO error.
	 */
	private PartnerModel getPartnerCollision(BufferedReader dataBR, int ipart, int[] id)
			throws IOException {
		dataBR.readLine();
		String[] collisionInfos = ReadRadexFile.reduce(dataBR.readLine()).split(" ");
		id[ipart] = Integer.parseInt(collisionInfos[0]);
		String typePartner = null;
		if (id[ipart] == 1)
			typePartner = "H2";
		else if (id[ipart] == 2)
			typePartner = "p-H2";
		else if (id[ipart] == 3)
			typePartner = "o-H2";
		else if (id[ipart] == 4)
			typePartner = "e";
		else if (id[ipart] == 5)
			typePartner = "H";
		else if (id[ipart] == 6)
			typePartner = "He";
		else if (id[ipart] == 7)
			typePartner = "H+";

		return new PartnerModel(id[ipart], typePartner, 1e4);

	}
}
